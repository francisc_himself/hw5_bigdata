#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include<cstdlib>

#define nul -1

int m,N=9973,n,nrAccese=0,maxAccese=0;

 void init(int T[])
 {
	 for(int i=0;i<N;i++)
		 T[i]=nul;
 }

int h(int k,int i)
{
	int c1=7,c2=17;

	return (k%N+c1*i+c2*i*i)%N;
}

int dispersie_inserare(int T[],int k)
{
	int i=0,j;
	do
	{
		j=h(k,i);
		if(T[j]==nul)
		{
			T[j]=k;
			return j;
		}
		else
			i++;
	}while(i<N);
	printf("eroare depasire tabela ");
	return -1;
}

int dispersie_cauta(int T[],int k)
{
	int i=0,j;
	do
	{
		nrAccese++;
		j=h(k,i);
		if(T[j]==k)
			return j;
		i++;
	}while(T[j]!=nul&&i<N);
	return -1;
}


void test(int T[])
{
	int x,i;
	int a[]={8000,2700,12000,560000,30000,750000,90,70100,17000};//elemente de inserat
	int caut[]={2700,600,20,70100,30000,300};//elemente de cautat
	for(i=0;i<9;i++)
	{
		x=dispersie_inserare(T,a[i]);
		printf("T[%d]=%d\n",x,a[i]);
	}
	for(i=0;i<6;i++)
	{
		x=dispersie_cauta(T,caut[i]);
		printf("valoarea %d se gaseste pe pozitia %d\n",caut[i],x);
	}
}


void generare_aleator(int T[])
{
	float alfa[]={0.8,0.85,0.9,0.95,0.99};
	int a[10000];
	int avgGas=0,avgNegas=0,maxGas,maxNegas,maxFinalA,maxFinalN,g,ng,j;
	m=3000;
	a[3]=9973;
	a[8]=29919;
	a[20]=19946;
	FILE *f=fopen("mediu1.csv","w");
	fprintf(f,"factor de umplere,avg effort found,max effort found,avg effort not-found,max effort not-found\n");

	srand(time(NULL));

	for(int i=0;i<5;i++)//pentru alfa
	{
		n=N*alfa[i];
		printf("%d ",n);
		avgNegas=0;
		avgGas=0;
		maxFinalA=0;
		maxFinalN=0;

		for(int k=0;k<5;k++)//pentru caz mediu...operatii de 5 ori si apoi media
		{
			init(T);

			for(j=0;j<n;j++)
			{

				a[j]=1+rand()*rand()%50000;
				dispersie_inserare(T,a[j]);
			}

			maxNegas=0;
			for(ng=0;ng<m/2;ng++)
			{
				nrAccese=0;
				dispersie_cauta(T,rand()+50002);
				avgNegas+=nrAccese;
				if(nrAccese>maxNegas)
					maxNegas=nrAccese;
			}
			maxFinalN+=maxNegas;
			maxGas=0;
			for(g=0;g<m/2;g++)
			{
				nrAccese=0;
				dispersie_cauta(T,a[rand()%n]);
				avgGas+=nrAccese;
				if(nrAccese>maxGas)
					maxGas=nrAccese;
			}
			maxFinalA+=maxGas;
		}
		avgGas/=(5*m/2);
		avgNegas/=(5*m/2);
		maxFinalA/=5;
		maxFinalN/=5;
		fprintf(f,"%.2f,%d,%d,%d,%d\n",alfa[i],avgGas,maxFinalA,avgNegas,maxFinalN);
	}
}


void main()
{
	int T[9973];
	init(T);
	test(T);
	generare_aleator(T);
	getch();


}
