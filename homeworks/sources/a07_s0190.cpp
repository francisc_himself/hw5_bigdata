#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>

using namespace std;

struct nodR2{
	int inf;
	int fi[100];
	int n;
}NodR2;

struct nodR3{
	int inf;
	nodR3 *fiu;
	nodR3 *frate;
}NodR3;

void initLista(nodR2 * list[], int n)
{
	for(int i=0;i<n;i++)
		list[i]=NULL;
}

void initBinary(nodR3 * binary[], int n)
{
	for(int i=0;i<n;i++)
		binary[i]=NULL;
}

/*
void prettyPrint(nodR3 *lista[], int n, int capLista)
{
	nodR3 *cap;
	nodR3 *parcurge;
	cap = lista[capLista];

	//inceput lista
	//cat timp mai exista fi, mergem mai departe
	if(cap->fiu!=NULL)
	{
		printf("%d \n",cap->inf);
		parcurge=cap->fiu;
		while(parcurge->frate!=NULL)
		{
			printf("\t %d", parcurge->inf);
			parcurge=parcurge->frate;
		}
		prettyPrint(lista, n, cap->fiu->inf);
	}

}*/

void prettyPrint(nodR3 *lista[], int n, int capLista)
{
	nodR3 *cap;
	nodR3 *parcurge;
	parcurge=NULL;
	cap = lista[capLista];

	while(cap!=NULL)
	{
		//printam informatia
		printf("\nparinte\n ");
		printf("%d\n",cap->inf);
		printf("fii\n ");
		parcurge=cap->fiu;
		//luam toti fii
					
		while(parcurge!=NULL)
		{
			printf("%d ",parcurge->inf);
			parcurge=parcurge->frate;
		}
		if(cap->fiu!=NULL)
		{
			printf("intram cu numarul %d",cap->fiu->inf);
			prettyPrint(lista,n,cap->fiu->inf);
		}
	}
}

void prittyPrintCaAsaTrebe(nodR2 ****ANONIM***2, nodR2 *lista[], int level)
{
	//printf("\nnodul %d are fii: ",***ANONIM***2->inf);

	for(int i=0;i<***ANONIM***2->n;i++)
	{
		if(lista[i]!=NULL)
		{
			for(int j=0;j<level;j++)
				printf("\t");
			printf("%d",lista[i]->inf);
			prittyPrintCaAsaTrebe(lista[***ANONIM***2->fi[i]],lista, level+1);
		}
	}
	
	printf("\n");
}

void padding ( char ch, int n )
{
  int i;
  for ( i = 0; i < n; i++ )
    printf( "%c",ch );
}

void afisare(nodR3 *t, int level )
{
  int i;
  if ( t == NULL ) {
    padding ( ' ', level );
    printf ( "~" );
  }
  else {
	afisare ( t->frate, level + 1 );
    padding (' ', level );
	printf ( "%d\n", t->inf );
	afisare ( t->fiu, level + 1 );
  }
}

void parcurgerePrerdine(nodR3 *t, int nivel)
{
	if(t!=NULL)
	{
		
		for(int i=0;i<nivel;i++)
			printf("\t");
		printf("%d \n",t->inf);
		if(t->fiu!=NULL)
			parcurgerePrerdine(t->fiu, nivel+1);
		if(t->frate!=NULL)
			parcurgerePrerdine(t->frate,nivel);
	}
}


int main()
{
	int parinte[]={2,7,5,2,7,7,-1,5,2};
	int n=9;	//numarul de noduri

	//R1 ,liniar OK
	int fiu[100];
	for(int i=0;i<n;i++)
	{
		int s;
		fiu[i]=i;
	}

	//R2, liniar OK
	//sirul de noduri
	nodR2 *lista[100];
	nodR2 *nodNou;
	//initializam lista
	initLista(lista,100);
	for(int i=0;i<n;i++)
	{
		//generam nodul daca nu exista
		if(parinte[i]>0)
		{
			if(lista[parinte[i]]==NULL)
			{
				//printf("if ");
				nodNou = (nodR2*) malloc(sizeof(nodR2));
				nodNou->inf=parinte[i];
				nodNou->n=0;

				//il adaugam in lista
				lista[parinte[i]]=nodNou;

				//adaugam pruncul
				lista[parinte[i]]->fi[lista[parinte[i]]->n]=fiu[i]+1;
				lista[parinte[i]]->n=(lista[parinte[i]]->n)++;
			}
			else
			{
				//printf("else ");
				lista[parinte[i]]->fi[lista[parinte[i]]->n]=fiu[i]+1;
				lista[parinte[i]]->n=(lista[parinte[i]]->n)++;
			}
		}
	}

	//prittyPrintCaAsaTrebe(lista[7],lista,0);
	//R3, liniar OK
	/*
	nodR3 *binary[100];
	nodR3 *newNod;
	nodR2 *nodPrel;
	nodR3 *newNodAnterior;
	nodR3 *parinti[100];
	initBinary(parinti, n);
	for(int i=0;i<n;i++)
	{
		printf("i = %d \n\n", i);
		if(lista[i]!=NULL)
		{
			newNodAnterior=NULL;
			nodPrel=lista[i];
			for(int j=0;j<(nodPrel->n);j++)
			{
				printf("\t j = %d\n", j);
				newNod = (nodR3*) malloc(sizeof(nodR3));	//cream nodul
				//punem informatiile din celi n fii
				newNod->inf=nodPrel->fi[j];					//am pus elementul fiului din lista
				printf("\t \t %d \n",newNod->inf );
				binary[newNod->inf]=newNod;
				
				newNod->frate=newNodAnterior;				//primul oricum e null
				newNodAnterior=newNod;

			}
		}

		//punem fii
	}*/

	nodR3 *noduri[100];
	nodR3 *newNod;
	nodR3 *newNodTata;
	nodR2 *nodPrel;
	nodR3 *newNodAnterior;
	initBinary(noduri, 50);
	for(int i=0;i<n;i++)
	{
			if(lista[i]!=NULL)
			{
				nodPrel=lista[i];				//avem lista de tip R2
				newNodAnterior=NULL;

				printf("+");
				//verificam daca exista nodul tata, daca nu il cream
				if(noduri[nodPrel->inf]==NULL)
				{
					printf("tata creat %d \n",nodPrel->inf);
					//cream tatal
					newNodTata = (nodR3*) malloc(sizeof(nodR3));	//cream nodul
					newNodTata->inf=lista[i]->inf;
					newNodTata->frate=NULL;
					newNodTata->fiu=NULL;
					//il punem in lista
					noduri[newNodTata->inf]=newNodTata;
				}
				else
				{
					newNodTata=noduri[nodPrel->inf];
				}

				//cream fii
				for(int j=0;j<lista[i]->n;j++)
				{
					if(noduri[lista[i]->fi[j]]==NULL)
					{
						printf("cream fiul %d ",lista[i]->fi[j]);
						//punem informatia
						newNod = (nodR3*) malloc(sizeof(nodR3));	//cream nodul
						newNod->inf = lista[i]->fi[j];
						newNod->fiu=NULL;
						//newNodTata->fiu=NULL;

						if(j>0)
							printf(" cu fratele %d \n",newNodAnterior->inf);
							//facem legaturile la frate
						newNod->frate=newNodAnterior;				//primul oricum e null	
						//il punem in lista
						newNodAnterior=newNod;
						noduri[newNod->inf]=newNod;
					}
					else
					{
						printf("fiu deja existent, il prelucram \n");
						//deja exista, numai ii facem legaturile la frate
						printf("+");
						noduri[lista[i]->fi[j]]->frate=newNodAnterior;
						printf("+");
						newNodAnterior=noduri[lista[i]->fi[j]];
					}

					//punem fiul
					if(j==(lista[i]->n)-1)
						newNodTata->fiu=noduri[newNod->inf];
					else
						newNodTata->fiu=NULL;
		
				}
			}
	}


	printf("\n\n\n");
	//afisam
	//prettyPrint(noduri,n,7);
	//afisare(noduri[7],0);
	parcurgerePrerdine(noduri[7],0);

	//printf("%d %d %d",noduri[6]->inf,noduri[6]->frate->inf,noduri[6]->frate->frate->inf);
	getch();
	return 0;
}