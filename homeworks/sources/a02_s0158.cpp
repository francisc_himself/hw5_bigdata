/*
-----------------BOTTOM_UP----------------------------
	The MAX-HEAPIFY procedure, which runs in O(lg n) 
time, is the key to maintaining the max-heap property.
	The BUILD-MAX-HEAP procedure, which runs in linear 
time, produces a max-heap from an unordered input array.
	Since the max-heapify function is called for every 
node on a path from root down to leaf and this path can 
be of maximum log(n) so the worst-case time complexity is log(n).
worst-case: array already sorted

-----------------TOP_DOWN-----------------------------
worst-case: array already sorted
Observations: bottom_up takes less operations than top_down approach
	          for building the heap in average case
*/
#include "stdio.h"
#include "math.h"
#include "Profiler.h"
#define MINUSINFINITY -30000
#define MAX_SIZE 10001
void Build_Max_Heap_Bottom_Up(int *A, int n);
void Build_Max_Heap_Top_Down(int *A, int n);

// global variables
int heap_size;
Profiler profiler("Heap Construction");
int Array[MAX_SIZE];
int repeat = 5;

/* copy the content of Array into aux */
void copy(int *aux, int n) {
    for(int i = 1; i <= n; i++)
        aux[i] = Array[i];
}

void averageCase() {
	int aux[MAX_SIZE]; // auxiliar array in which we copy the random generated one
	for(int n = 101; n < 10001; n += 100) {
		FillRandomArray(Array, n);
		for(int i = 1; i <= 5; i++) { // do the measurements 5 times on each set of input data
			copy(aux, n);
			Build_Max_Heap_Bottom_Up(aux, n);
			copy(aux, n);
			Build_Max_Heap_Top_Down(aux, n);		
			
		}	
	}
	profiler.createGroup("Bottom_Up_vs_Top_Down", "top_down_count", "bottom_up_count");
	profiler.showReport();
}

void worstCase() {
	int aux[MAX_SIZE]; // auxiliar array in which we copy the random generated one
	for(int n = 101; n < 10001; n += 100) {
		FillRandomArray(Array, n, 10, 30000, false, 1);
		copy(aux, n);
		Build_Max_Heap_Bottom_Up(aux, n);
		//FillRandomArray(Array, n, 10, 30000, false, 1);
		copy(aux, n);
		Build_Max_Heap_Top_Down(aux, n);		
	}
	profiler.createGroup("Bottom_Up_vs_Top_Down_Worst", "top_down_count", "bottom_up_count");
	profiler.showReport();
}

void Max_Heapify(int *A, int i, int n) {
	int l = 2 * i;
	int r = 2 * i + 1;
	int largest;
	if(l <= n && A[l] > A[i]) {
		largest = l;
	}
	else largest = i;
	if(r <= n && A[r] > A[largest]) {
		largest = r;
	}
	profiler.countOperation("bottom_up_count", n - 1, 2);
	if(largest != i) {
		profiler.countOperation("bottom_up_count", n - 1, 3);
		int temp = A[i];
		A[i] = A[largest];
		A[largest] = temp;
		Max_Heapify(A, largest, n);
	}

}

void Build_Max_Heap_Bottom_Up(int *A, int n) {
	//profiler.countOperation("bottom_up_count", n, 0); 
	for(int i = n/2; i >= 1; i--) // take all non-leaf nodes
		Max_Heapify(A, i, n);
}

void Build_Max_Heap_Top_Down_DC(int *A, int n, int i) {
	if(i <= n/2) {
		Build_Max_Heap_Top_Down_DC(A, n, 2 * i);
		Build_Max_Heap_Top_Down_DC(A, n, 2 * i + 1);
		Max_Heapify(A, i, n);
	}

}

void Heap_Increase_Key(int *A, int k, int key, int n) {
	int i = k;
	if(key < A[i]) {
		printf("new key is smaller than current key");
		return;
	}
	profiler.countOperation("top_down_count", n - 1);
	A[i] = key;
	profiler.countOperation("top_down_count", n - 1, 2); // + 1 for while exit
	while(i > 1 && A[i/2] < A[i]){
		profiler.countOperation("top_down_count", n - 1, 4);
		int temp = A[i];
		A[i] = A[i/2];
		A[i/2] = temp;
		i = i/2;
	}

}

void Max_Heap_Insert(int *A, int key, int n) {
	heap_size++;
	A[heap_size] = MINUSINFINITY;
	profiler.countOperation("top_down_count", n - 1);
	Heap_Increase_Key(A, heap_size, key, n);
}

void Build_Max_Heap_Top_Down(int *A, int n) {
	//profiler.countOperation("top_down_count", n, 0);
	heap_size = 1;
	for(int i = 2; i <= n; i++)
		Max_Heap_Insert(A, A[i], n);
}

void heapSort(int *A, int n) {
	heap_size = n;
	Build_Max_Heap_Bottom_Up(A, n);
	for(int i = n; i >= 2; i--) {
		int temp = A[1];
		A[1] = A[i];
		A[i] = temp;
		heap_size--;
		Max_Heapify(A, 1, heap_size);
	}
}

void inorder(int *A, int k, int level, int n) {
	int i;
	if (k <= n) {
		inorder(A, 2 * k + 1, level + 1, n) ;
		for (i = 0; i <= level ; i++) 
			printf("   "); /* for nice listing */
		printf ("%d\n", A[k]) ;
		inorder(A, 2 * k, level + 1, n) ;
	}
}

int main() {
	int n = 10;
	int A[MAX_SIZE] = {-10000, 4, 1, 3, 2, 16, 9, 10, 14, 8, 7}; 
	int B[MAX_SIZE] = {-10000, 4, 1, 3, 2, 16, 9, 10, 14, 8, 7};

	heapSort(A, n);
	//Build_Max_Heap_Top_Down(B, n);
	//Heap_Increase_Key(B, 9, 15);
	//heap_size = 10;
	//Max_Heap_Insert(B, 25);
	//Build_Max_Heap_Top_Down(B, n);

	for(int i = 1; i <= n; i++)
		printf("%d ", A[i]);
	
	//inorder(A, 1, 0, n);
	//printf("\n");
	//inorder(B, 1, 0, n);
	//averageCase();
	//worstCase();
	return 0;
}