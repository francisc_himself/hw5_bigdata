#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#define n 10007  //dimensiunea tabelului
#define NIL -1
#define M 3000 //cautare a m elemente aleator
#define c1 1
#define c2 1

int ef;
int efG;
int efMaxG;
int efNeg;
int efMaxNeg;

using namespace std;

//hash
int h(int k, int i)
{
	return (((k% n)+ c1* i+ c2* i* i)% n);
}

//inserare in tabela de dispersie
int inserare(int *HT, int k)
{
	int i=0;
	int j;
	do
	{
		j=h(k, i);
		if (HT[j]==NIL)
		{
			HT[j]=k;
			return j;
		}
		else
		{
			i++;
		}
	}
	while (i!=(n-1));
	return NIL;
}

//cautare in tabela de dispersie
int cautare(int *HT, int k, int *ef)
{
	int i=0;
	int j;
	do
	{
		(*ef)++;
		j=h(k, i);
		if (HT[j]==k)
		{
			return j;
		}
		else
		{
			i++;
		}
	}
	while ((HT[j]!=NIL) && (i!=(n-1)));
	return NIL;
}

void init(int *HT)
{
	for (int i=0; i<n; i++)
		HT[i]=NIL;
}

/*jumatate din elemente aleatoare le punem astfel:
cu random generam un numar pana la n, daca elementul pe poz i
nu este nula il punem in HT2
cealalta jumatate il facem cu random*/
void constructie_vector(int *HT, int *HT2)
{
	int j = 0;
	while(j!=M/2) 
	{
		int i=rand()%n;
		if (HT[i] != NIL)   
		{
			HT2[j] = HT[i];
			j++;
		}

	}
	
	for (int i = M / 2; i < M; i++)
	{
		HT2[i] = rand()+33000;
	}
}


int main()
{	srand(time(NULL));
	int HT[n];
	int HT2[3000];
	
	FILE *f = fopen("Tabela cu date.txt", "w");
	srand(time(NULL));
	fprintf(f, "%s\t%s\t%s\t%s\t%s\n", "alfa", "EfMed gasite","EfMax gasite" ,"EfMed negasit" , "EfMax neg");

	double ALFA[] = {0.80, 0.85, 0.90, 0.95, 0.99};

	for (int a = 0; a < 5; a++)
	{
		ef = efG = efNeg = 0;
		efMaxG = INT_***ANONIM***;
		efMaxNeg = INT_***ANONIM***;
	
		for (int caz = 0; caz < 5; caz++)
		{
			init(HT);
			
			// inserare n elemente
		
			int nn = 0;
			double alfa = (double) nn / n;
			while (alfa < ALFA[a])
			{
				int k = rand();

				inserare(HT, k);

				alfa = (double) ++nn /n;
			}
			constructie_vector(HT, HT2);
			
			// cautare elemente gasite
			for (int l = 0; l < (M / 2); l++)
			{
				ef=0;
				cautare(HT, HT2[l], &ef); 
			
				efG += ef;
				if (ef > efMaxG)
				{
					efMaxG = ef;
				}
			}
			// cautare elemente negasite
			ef = 0;
			for (int l = (M / 2); l < M; l++)
			{
				ef=0;
				cautare(HT, HT2[l], &ef); 
				efNeg += ef;
				if (ef> efMaxNeg)
				{
					efMaxNeg = ef;
				}
			}
		}
		
		double medG = (double) efG / n;
		double medNeg = (double) efNeg / n;

	
	fprintf(f, "%-1.2f\t%s%-4.2lf\t%s%-4d\t%s%-4.2lf\t%s%-4d\n", ALFA[a],"    ", medG,"        ",
			efMaxG,"          ", medNeg, "         ",efMaxNeg);
	}

	fclose(f);

	system("pause");
	return 0;
}