/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Algoritmul Josephus

Cerinta:
Avem n persoane asezate intr-un cerc si un numar intreg pozitiv m<=n. Incepand cu o persoana stabilita, se parcurge cercul
si fiecare a m-a persoana este scoasa din joc. Dupa ce persoana a fost eliminata, numararea continua cu celelalte persoane 
ramase. Procesul de eliminare continua pana cand toate cele n perosane sunt eliminate din joc.

Algoritm de rezolvare:
Eu am ales un algoritm care consta in construirea unui arbore binar de cautare cu cheile de la 1 la n, reprezentand practic 
persoanele din cerc. Pentru rezolvara problemei selectam elementul corespunzator dupa numarare si il stergem din arborele 
binar de cautare. Bineinteles la fiecare stergere modificam dimensiunile nodurilor.

Complexitatea algoritmului este de O(n*log(n)).
Din grafic va rezulta o dependenta liniara a operatiilor fata de numarul de persoane n.

*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

typedef struct _NOD{
	int value;
	int dimensiune;
	struct _NOD *stg,*dr;
}NOD;

//constructie arbore echilibrat
NOD* Constructie_Arbore_Echilibrat(int stanga, int dreapta)
{
	int mijloc;
	NOD *d=new NOD;
	if (stanga<=dreapta)
	{
		mijloc=(stanga+dreapta)/2;
		d->value=mijloc;
		d->stg=Constructie_Arbore_Echilibrat(stanga,mijloc-1);
		d->dr=Constructie_Arbore_Echilibrat(mijloc+1,dreapta);
		
		if((d->stg!=NULL) && (d->dr!=NULL))
			d->dimensiune=d->stg->dimensiune+d->dr->dimensiune+1;
		else
			if((d->stg==NULL) && (d->dr!=NULL))
				d->dimensiune=d->dr->dimensiune+1;
			else
				if((d->stg!=NULL) && (d->dr==NULL))
					d->dimensiune=d->stg->dimensiune+1;
				else
					d->dimensiune=1;
		
	}
	else
		d=NULL;
	
	return d;
	
}

//implementare functie OS_SELECT
NOD* OS_SELECT(NOD *x,int i)
{
	int r;
	if(x->stg!=NULL)
	{	
		r=x->stg->dimensiune+1;
	
		if (i==r)
			return x;
		else
			if (i<r)
				return OS_SELECT(x->stg,i);
			else
				return OS_SELECT(x->dr,i-r);
	}
}

//afisare in ordine a arborelui
void afiseaza_arbore_inordine(NOD* rad, int depth)
{
	int k;
	if(rad!=NULL)
	{
		for(k=0; k<depth; k++)
		{
			printf("     ");
		}
		
		afiseaza_arbore_inordine(rad->stg, depth+1);
		printf("%d, %d\n", rad->value, rad->dimensiune);
		afiseaza_arbore_inordine(rad->dr, depth+1);
	}
}

void afiseazaArboreInOrdine(NOD *rad,int depth)
{
	int k;
	if(rad!=NULL)
	{
		afiseazaArboreInOrdine(rad->stg,depth+1);
		for(k=0;k<depth;k++)
			printf("\t");
		printf("%d,%d \n", rad->value,rad->dimensiune);
		afiseazaArboreInOrdine(rad->dr,depth+1);
	}
}

//functie pentru arbore vid
bool ArboreVid(NOD *arbore)
{
       return arbore == NULL;
}

// Functie de stergere a unui nod
void StergeNod(NOD **legParinte)
{
	// salvam un pointer la nodul de sters
    NOD *nod = *legParinte;
 
    // daca avem un subarbore drept
    if (nod->dr != NULL)
    {
		// facem legatura
        *legParinte = nod->dr;
		
        // daca avem si un subarbore stang
        if (nod->stg != NULL)
        {
			// cautam cel mai mic element din subarborele drept
            NOD* temp = nod->dr;
			while (temp->stg != NULL)
				temp = temp->stg;
			
            // si adaugam subarborele stang
			temp->dimensiune=nod->dimensiune-1;
			temp->stg = nod->stg;
        }
    }
    else
		// daca avem doar un subarbore stang
        if (nod->stg != NULL)
		{
			// facem legatura la acesta
            *legParinte = nod->stg;
			nod->stg->dimensiune=nod->stg->dimensiune-1;
		}
		else
		{	
			// daca nu avem nici un subnod
            *legParinte = NULL;
			nod->dimensiune=nod->dimensiune-1;
		}		
	// stergem nodul
    delete nod;
}
 
// Sterge un nod dintr-un arbore de cautare
void OS_DELETE(NOD *arbore, int date)
{
	// Cazul 1: arbore vid
    if (ArboreVid(arbore))
	    return;
 
	// Cazul 2: stergere radacina
    if (arbore->value == date)
	{
  		// salvam un pointer la radacina
        NOD *nod = arbore;
 
		// daca avem un subarbore drept
        if (nod->dr!=NULL)
        {
			// facem legatura
            arbore = nod->dr;
 
            // daca avem si un subarbore stang
            if (nod->stg!=NULL)
			{
				// cautam cel mai mic element din subarborele drept
                NOD *temp = nod->stg;
                while (temp->stg != NULL)
					temp = temp->stg;
 
                // si adaugam subarborele stang
                temp->stg = nod->stg;
            }
        }
        else
			// daca avem doar un subarbore stang
            if (nod->stg != NULL)
				// facem legatura la acesta
                arbore = nod->stg;
            else
                // daca nu avem nici un subnod
                arbore = NULL;
      
		// stergem vechea radacina
        delete nod;
		
        return;
    }
      
    // Cazul 3: stergere nod in arbore nevid
 
    // cautam legatura la nod in arbore
    // si stergem nodul (daca exista)
    NOD *nodCurent = arbore;
    while ((nodCurent!=NULL)&&(nodCurent->value!=date))
    {     
		if (date < nodCurent->value)
			if (nodCurent->stg == NULL)
				break; // nodul nu exista
            else
				if (nodCurent->stg->value == date)
				{
					// nodul de sters este descendentul stang
					nodCurent->dimensiune=nodCurent->dimensiune-1;
					StergeNod(&nodCurent->stg);
				}
				else
					// continuam cautarea in subarborele stang
                    nodCurent = nodCurent->stg;
        else
	        if (nodCurent->dr == NULL)
				break; // nodul nu exista
            else
                if (nodCurent->dr->value == date)
				{	
					// nodul de sters este descendentul drept
					nodCurent->dimensiune=nodCurent->dimensiune-1;
                    StergeNod(&nodCurent->dr);
				}
				else
                    // continuam cautarea in subarborele drept
                    nodCurent = nodCurent->dr;
       }
		
}
 
/*void OS_DELETE(NOD* T, NOD* x)
{
	
}*/

//functia de permutare JOSEFHUS 
void JOSEPHUS(int n, int m)
{
	NOD *T,*x;

	int j,k;
	T = Constructie_Arbore_Echilibrat(1,n);
	j=1;
	for(k=n;k>=1;k--)
	{
		j=((j+m-2)%k)+1;
		x=OS_SELECT(T,j);
		printf("%d ",x->value);
		OS_DELETE(T,x->value);
	}
}
int main()
{
	NOD *p,*q;
	int n,m;
	
	/*p=Constructie_Arbore_Echilibrat(1,10);
	
	afiseazaArboreInOrdine(p,0);
	
	q=OS_SELECT(p,5);
	printf("\nCheie nod q= %d\n",q->value);

	OS_DELETE(p,8);
	printf("\nArborele dupa stergerea nodului 8\n");
	afiseazaArboreInOrdine(p,0);
	*/

	p=Constructie_Arbore_Echilibrat(1,10);
	afiseazaArboreInOrdine(p,0);
	JOSEPHUS(10,3);
	

	/*printf("\nDati n= ");
	scanf("%d",&n);
	printf("\nDati m= ");
	scanf("%d",&m);
	  
	JOSEPHUS(n,m);*/

	getch();
}
