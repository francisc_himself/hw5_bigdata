#include<stdio.h>
#include<conio.h>

int PV[] = { 1, 6, 4, 1, 6, 6, -1, 4, 5};
int nr = 9;
//int nr;
//int PV[100];

typedef struct Tree{
   int val;
   int childNo;
   Tree* child[100]; // alocam dinamic
}Tree;
Tree* a[100];
Tree* prim; 

typedef struct Binar
{
  int val;
  Binar* brother;
  Binar* son;
}BINAR;
BINAR* b[100];   // doar o radacina


void MWRepresentation(Tree *p){ //afisam in reprezentare multii cai
	int i;
	if(p!=NULL) {	//verificam daca nodul exista
		  if(p->childNo==0)	//verificam daca nodul ii frunza
			  printf("\n%d does not have any children",p->val);
		  else{
				printf("\nChildren of %d are:",p->val);
				for(i=1;i<=p->childNo;i++)
					 printf(" %d ",p->child[i]->val);
				for(i=1;i<=p->childNo;i++)
					 MWRepresentation(p->child[i]);
		  }
	 }
}

void PrettyPrint(int ind,int h){ //prettyprint pt arborele binar
   int i;
   printf("\n");
   for(i=0;i<=3*h;i++) // lasa 3 spatii pt fiecare nivel
	   printf(" ");
   printf("%d",ind);

   if(b[ind]->son!=NULL)// facem recursiv pt urmatorul nod
	   PrettyPrint(b[ind]->son->val,h+1);
     if(b[ind]->brother!=NULL)
	   PrettyPrint(b[ind]->brother->val,h);
}

void T1(){ //transforma intr-un arbore  multi cai
   int i,aux;
   for(i=0;i<=nr;i++){
	   if(PV[i]==-1)	// cautam radacina
			prim=a[i];
	   else{
	   a[PV[i]]->childNo++;		
	   aux = a[PV[i]]->childNo;
	   a[PV[i]]->child[aux]=a[i];
	   }
   }
}

void T2(Tree *p){  // transofrmarea in arbore binar
	int i;
	if(p!=NULL)
	{
		for(i=1;i<=p->childNo;i++)
		{
			if(i==1)
				b[p->val]->son=b[p->child[1]->val];
			else
				b[p->child[i-1]->val]->brother=b[p->child[i]->val];

			T2(p->child[i]);
		}
	}
}

void main(){
	int i;
	for(i=0;i<=nr;i++){
	   a[i]=new Tree();  
	   a[i]->val=i;
	   a[i]->childNo=0;
	   a[i]->child[1]=NULL;

	   b[i]=new Binar();  
	   b[i]->val=i;
	   b[i]->son=b[i]->brother=NULL;
	}

	T1();
	printf("\nAfter the first transformation:");
	MWRepresentation(prim);

	T2(prim);
	printf("\nAfter the second transformation:\n");
	PrettyPrint(prim->val,0);


	getch();

}