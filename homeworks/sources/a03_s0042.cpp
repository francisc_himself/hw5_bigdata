/*in acest program sunt prezentati cei mai eficienti algoritmi de sortare Heeap sort si quick sort.
doi algoritmi simplu de implementatat si de inteles si cu grad de eficienta ridicat.
heep sort algoritm care creaza un arbore cu elementul maxim in varful acestuia iar dupa acel pas
se ia elementul si se pozitioneaza la final dupa care se apeleaza din nou crearea heepului doar ca se decrementeaza
dimensiunea acestuia. Iar quick sort sorteaza sirulul impartindul in mai multe subsiruri.
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int n,dim;
int as=0,com=0;
int asq=0,comq=0;
int v[10001],c[10001],h[10001];

//copia vectorului pentru a folosi acelasi vector pentru ambele sortari
void retine(int *v,int *c)
{
	int i;
	for(i=1;i<n+1;i++)
	{
		v[i]=c[i];
	}
}

//generarea sir numere aleatoare
void generare(int *v, int n)
{
	int i;
	for (i=1;i<n;i++)
		v[i]=rand();
}

//generare sir numere crescatoare
void generareC(int *v, int n)
{
	int i;
	for (i=1;i<n;i++)
		v[i]=i;
}
//generare numere descrescatoare
void generareD(int *v, int n)
{
	int i;
	for (i=0;i<n;i++)
		v[i]=n-1;
}

//construirea heapului Button Up
void Reconstructie(int *v, int i) 
{ 
  int stg,dr,max;
  stg=2*i;
  dr=(2*i)+1;
  com++;
  if((stg<=n) && (v[stg]>v[i])) 
  {
    max=stg;
  }
  else 
  {
    max=i;
  }
  com++;
  if((dr<=n) && (v[dr]>v[max])) 
  {
    max=dr;
  }
  com++;
  if(max!=i)
  {
	as+=3;
    int temp=v[i];
    v[i]=v[max];
    v[max]=temp;
    Reconstructie(v, max);
  }
}

void Constructie(int *v)
{
	int i;
	for (i=n/2; i > 0;i--) 
	{
		Reconstructie(v, i); 
	}
}
/*medota de sortare a heepului , prin luarea elementului din varful acestuia, respectiv
primul element al sirului, pozitionarea lui pe ultimu element al sirului ,decrementarea
marimii heapului si reapelarea construirii heapului
*/
void sortareHeap(int *v)
{
	int aux,nn;
    nn=n;
	Constructie(v);
	for(int i=n;i>0;i--)
	{
    aux=v[1];
	v[1]=v[n];
	v[n]=aux;
	as+=3;
	n=n-1;
	Reconstructie(v,1);
	}
	n=nn;
}
//------------------------------------------
//quick sort
int Partitionare(int *v, int p, int r)
{
 
 int x=v[p];
 int i=p-1;
 int j=r+1;
 int aux;
 asq++;
 while (true)
 {
	do
	{
		j--;
		comq++;
	}
	while(v[j]>x);
	do{
		i++;
		comq++;
		}
	while(v[i]<x);
	comq++;
	if(i<j)
	{
		aux=v[i];
		v[i]=v[j];
		v[j]=aux;
		asq+=3;
	}
	else return j;
 }
}
void quick_sort(int *v,int st,int dr)
{
	int q=0;
	comq++;
	if(st<dr)
	{
		q=Partitionare(v,st,dr);
		quick_sort(v,st,q);
		quick_sort(v,q+1,dr);
	}
}

//afisarea preety pentru vizualizaera heepului
void Preety_print(int *v,int k,int nivel)
{
	int i;
	if (k>n)
		return;
	Preety_print(v,2*k+1,nivel+1);
		for(i=1; i<=nivel;i++)
			printf("  ");
		printf("%d\n",v[k]);
	Preety_print(v,2*k,nivel+1);
}

int main()
{
	FILE *f;
	f=fopen("heapvsquick.csv","w");
	//siruri folosite pentru a retine operatiile efectuate pentru cele 4 cazuri de sortari
	int asignari[4],comparari[4];

	/*declarae unui vector si introducearea unor valori direct din program pentru a 
	test corectitudinea algoritmului heeap*/
	int test[8];
	test[1]=20;test[2]=15;test[3]=1;test[4]=3;test[5]=5;test[6]=23;test[7]=65;n=7;
	printf("test pentru construirea heepului si pentru sortarea acestuia\n");
	Constructie(test);
	Preety_print(test,1,1);
	sortareHeap(test);
	for(int i=1;i<=n;i++)
	{
		printf("%d\n",test[i]);
	}

	//repetarea testului de mai sus doar ca pentru algoritmul heap
	printf("test pentru algoritmul quick\n");
	int test1[8];
	test1[1]=20;test1[2]=15;test1[3]=1;test1[4]=3;test1[5]=5;test1[6]=23;test1[7]=65;n=7;
	sortareHeap(test1);
	quick_sort(test1,1,n);
	for(int i=1;i<=n;i++)
	{
		printf("%d\n",test1[i]);
	}
	

	/*inceperea preluarii datelor pentru sortarea vectorilor generati aleator pentru ambele metode de sortare
	si sortarea sirurilor crescatoare si descrescatoare cu quick sort*/
  
	fprintf(f,"n,asignari heap,comparari heap,operatii,asignari quick,comparatii quick, operatii quick,as_qC,com_qC,op_qC,as_qD,com_qD,op_qD \n"); 
	for(n=100;n<=10000;n=n+100)
	{
		asignari[0]=0;asignari[1]=0;
	    comparari[0]=0;comparari[1]=0;
		asignari[2]=0;asignari[3]=0;
		comparari[2]=0;comparari[3]=0;

		for(int m=1;m<=5;m++)
		{	
			generare(v,n);
			retine(c,v);
			
			com=0,as=0;
			sortareHeap(v);
			asignari[0]=asignari[0]+ as;
			comparari[0]=comparari[0]+com ;
			
			asq=0;
			comq=0;
			quick_sort(v,1,n);
			asignari[1]=asignari[1]+ asq;
			comparari[1]=comparari[1]+ comq;

			generareC(v,n);
			asq=0;
			comq=0;
			quick_sort(v,1,n);
			asignari[2]=asignari[2]+ asq;
			comparari[2]=comparari[2]+ comq;

			generareD(v,n);
			asq=0;
			comq=0;
			quick_sort(v,1,n);
			asignari[3]=asignari[3]+ asq;
			comparari[3]=comparari[3]+ comq;
		}
		//afisarea in tabel a rezultatelor obtinute
		fprintf(f,"%d,",n);
		fprintf(f, "%d,%d,%d,",asignari[0]/5,comparari[0]/5,(asignari[0]+comparari[0])/5);	
		fprintf(f,"%d,%d,%d,",asignari[1]/5,comparari[1]/5,(asignari[1]+comparari[1])/5 );
		fprintf(f,"%d,%d,%d,",asignari[2]/5,comparari[2]/5,(asignari[2]+comparari[2])/5 );
		fprintf(f,"%d,%d,%d\n",asignari[3]/5,comparari[3]/5,(asignari[3]+comparari[3])/5 );
	}
	fclose(f);
	//final program
}