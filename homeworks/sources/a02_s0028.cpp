﻿// tema2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#include<fstream>


#define MAX 10000

int B[MAX];
int n=10;
int dimensiune_heap;
int nr_comp[2];
int nr_atrib[2];
int X[MAX],Y[MAX];
int nr;

int parinte(int i)
{
	return (i/2);
}

int stanga(int i)
{
	return(2*i);
}

int dreapta(int i)
{
	return(2*i+1);
}

void reconstituie_heap(int A[], int i)
{
	int aux,maxim,l,r;
	l=stanga(i);
	r=dreapta(i);
	nr_comp[1]=nr_comp[1]+1;
	if((l<=dimensiune_heap)&&(A[l]>A[i]))
		maxim=l;
	else
		maxim=i;
	nr_comp[1]=nr_comp[1]+1;
	if((r<=dimensiune_heap)&&(A[r]>A[maxim]))
		maxim=r;
	if(maxim!=i)
	{
		nr_atrib[1]=nr_atrib[1]+3;
		aux=A[i];
		A[i]=A[maxim];
		A[maxim]=aux;
	
		reconstituie_heap(A,maxim);
	}
	
}


void construieste_heap(int A[])
{
	dimensiune_heap=n;
	for(int j=n/2;j>=1;j--)
		reconstituie_heap(A,j);

}





void insereaza_in_heap(int A[],int cheie)
{
	int aux,i;
	dimensiune_heap=dimensiune_heap+1;
	i=dimensiune_heap;
	nr_comp[2]=nr_comp[2]+1;
	while((i>1)&&(A[parinte(i)]<cheie))
	{
		aux=A[parinte(i)];
		A[parinte(i)]=A[i];
		A[i]=aux;
		i=parinte(i);
		nr_atrib[2]=nr_atrib[2]+3;
	}
	A[i]=cheie;
	nr_atrib[2]=nr_atrib[2]+1;
}

void copiere_sir(int destinatie[], int n, int sursa[])
{
	int j;
	for (j = 1; j <= n; j++)
		destinatie[j] = sursa[j];
}


void main()
{
	

	nr_atrib[1]=0;
	nr_comp[1]=0;
	nr_atrib[2]=0;
	nr_comp[2]=0;

	B[1]=4;
	B[2]=1;
	B[3]=3;
	B[4]=2;
	B[5]=16;
	B[6]=9;
	B[7]=10;
	B[8]=14;
	B[9]=8;
	B[10]=7;
	for(int j=1;j<=10;j++)
		printf("%d ",B[j]);
	printf("\n");
	
	construieste_heap(B);

	for(int j=1;j<=10;j++)
		printf("%d ",B[j]);
	printf("\n%d %d %d",nr_atrib[1],nr_comp[1],(nr_atrib[1]+nr_comp[1]));
	printf("\n");

	for(int j=1;j<=10;j++)
		B[j]=0;
		
	dimensiune_heap=0;
	insereaza_in_heap(B,4);
	insereaza_in_heap(B,1);
	insereaza_in_heap(B,3);
	insereaza_in_heap(B,2);
	insereaza_in_heap(B,16);
	insereaza_in_heap(B,9);
	insereaza_in_heap(B,10);
	insereaza_in_heap(B,14);
	insereaza_in_heap(B,8);
	insereaza_in_heap(B,7);	
	printf("\n");
	for(int j=1;j<=10;j++)
		printf("%d ",B[j]);
	printf("\n%d %d %d",nr_atrib[2],nr_comp[2],(nr_atrib[2]+nr_comp[2]));




	FILE *f;
	f = fopen("heap.csv","w");
	
	srand (time(NULL));

	fprintf(f, "n,nr_AtribSiComp_BottonUp,nr_AtribSiComp_TopDown\n");

	for (n = 100; n <= 10000; n = n + 100)
	 {
		nr_atrib[1] = 0;
		nr_atrib[2] = 0;
		nr_comp[1] = 0;
		nr_comp[2] = 0;

		fprintf(f, "%d,", n);

		for (int j = 1; j <= 5; j++)
		{
			for (int k = 1; k <= n; k++)
			{
				X[k] = rand() % 10000;
				Y[k] = X[k];
			}
			
			// construire heap "bottom-up"
			construieste_heap(X);
			copiere_sir(X, n, Y);

			// construire heap "top-down"
			for(int k=1;k<=n;k++)
				B[k]=0;
			dimensiune_heap=0;
			for(int k=1;k<=n;k++)
			{
				nr=X[k];
				insereaza_in_heap(B,nr);
			}
			copiere_sir(X, n, Y);
		}

	fprintf(f, "%d,%d\n", (nr_atrib[1]+nr_comp[1]) / 5, (nr_atrib[2]+nr_comp[2]) / 5);

	}
	fclose(f);
	printf("\ndatele au fost scrise\n");

	getch();
}