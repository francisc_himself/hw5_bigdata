#include <iostream>
#include <conio.h>
#include "Profiler.h"
using namespace std;

Profiler profiler("demo");

int n, size, j=0, a[100000];

void swap(int &x,int &y)
{
    int temp=x;
    x=y;
    y=temp;
}

void heapify(int x)
{
    int left=(2*x);
    int right=(2*x)+1;
    int large;
	profiler.countOperation("CompareBU", n);
	j++;
    if(a[left]>a[x] && left <= n)
    {
        large=left;
    }
    else
    {
        large=x;
    }
	profiler.countOperation("CompareBU", n);
    if(a[right]>a[large] && right <= n)
    {
        large=right;
    }
    if(x!=large)
    {
        swap(a[x],a[large]);
		profiler.countOperation("AssignBU", n, 3);
        heapify(large);
    }
}

void bottom_up()
{
	for(int i = n/2; i>0;i--)
		heapify(i);
}

void  HeapIncreaseKey(int  i, int  key)
{
	a[i]=key;
	profiler.countOperation("AssignTD", n);
	profiler.countOperation("CompareTD", n);
	while(a[i/2]<a[i] && i>0)
	{
		swap(a[i],a[i/2]);
		profiler.countOperation("AssignTD", n, 3);
		profiler.countOperation("CompareTD", n);
		i=i/2;
	}
	
}

void insert(int  key)
{
	size++;
	a[size]=-100000;
	profiler.countOperation("AssignTD", n, 2);
	HeapIncreaseKey(size,key);
}

void top_down()
{
	int i;
	size=1;
	for(i=2;i<=n;i++)
		insert(a[i]);
}

void  prettyPrint ()
{
	int idx, endIt = 2, j;
	for(j=1;j<n-idx+1;j=j*2)
		printf("    ");
	printf("    ");
    for(idx=0;idx<n;idx++)
    {
        if(idx==endIt-1)
        {  
			printf("\n");
			for(j=1;j<n-idx+1;j=j*2)
			printf("    ");
            endIt=endIt*2;   
        }
		printf("    ");
		printf("%d", a[idx]);
      }
   }

void testBU()
{
	printf("Buttom-Up. Enter n: ");
	scanf("%d", &n);

	for(int i=1;i<=n;i++)
	{
		printf("a[%d] = ", i);
		scanf("%d", &a[i]);
	}
	printf("\n");
	top_down();
	for(int i=1;i<=n;i++)
		cout<<a[i]<<" ";
	
	printf("\n");
	
	//prettyPrint();
	printf("\n");
	getch();
}


void testTD()
{
	printf("Top-Down. Enter n: ");
	scanf("%d", &n);

	for(int i=1;i<=n;i++)
	{
		printf("a[%d] = ", i);
		scanf("%d", &a[i]);
	}
	printf("\n");

	bottom_up();
	for(int i=1;i<=n;i++)
		cout<<a[i]<<" ";
	
	printf("\n");
	//prettyPrint();
	printf("\n");
	getch();
}

void main()
{	
	testBU();
	//testTD();

	for (n=0;n<=10000;n=n+300)
	{
		FillRandomArray(a, n+1, 0, 10000);
		top_down();
		
		FillRandomArray(a, n+1, 0, 10000);
		bottom_up();
	}

	profiler.createGroup("Compare","CompareTD","CompareBU");
	profiler.createGroup("Assign","AssignTD","AssignBU");

	profiler.addSeries("BU","CompareBU","AssignBU");
	profiler.addSeries("TD","CompareTD","AssignTD");

	profiler.createGroup("C plus A","TD","BU");

	profiler.showReport();
}