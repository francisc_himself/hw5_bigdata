#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define N 10000     

typedef struct NOD {
    int inf;
    int height;
    struct NOD *parinte;
} nod;

nod *nodes[N];
int nr;

nod *makeSet (int x)
{
    nr++;

    nod *q;
    q = (nod*) malloc(sizeof(nod));

    q->inf = x;
    q->height = 0;
    q->parinte = q;

    return q;
}

nod *findSet (nod *x)
{
    nr++;

    if (x != x->parinte)
    {
        x->parinte = findSet(x->parinte);
    }

    return x->parinte;
}

void link(nod *x, nod *y)
{
    if(x->height > y->height)
    {
        y->parinte = x;
    }
    else
    {
        x->parinte = y;
        if(x->height = y->height)
            y->height = y->height + 1;
    }
}

nod *setUnion (nod *x, nod *y)
{
    nr++;
    link(findSet((x)), findSet(y));
	return 0;
}

int main()
{
   /* int i;
    int a = 0, b =0;

    for (i = 1; i <= 5; i++)
        nodes[i] = makeSet(i);

	setUnion(nodes[1], nodes[2]);
	setUnion(nodes[2], nodes[3]);
	setUnion(nodes[4], nodes[5]);

    for (i = 1; i <= 6; i++)
        printf("%d cu parintele %d\n", nodes[i]->inf, nodes[i]->parinte->inf);
		*/
		
    FILE *fout = fopen ("set.csv", "w");

    int l;
    int i;
    int a , b;

	fprintf(fout, "Laturi , Op\n");

    for (l = 10000; l <= 60000; l += 1000) //laturi
    {
        nr = 0;

        for (i = 0; i < N; i++)
        {
            nodes[i] = makeSet(i);
        }

        for (i = 0; i < l; i++)
        {
            a = rand()%N;
            b = rand()%N;

            if (findSet(nodes[a]) != findSet(nodes[b]))
            {
                setUnion(nodes[a], nodes[b]);
            }
        }

        printf("Step: %d, count %d\n", l, nr);
        fprintf(fout, "%d,%d\n", l, nr);

        for (i = 0; i < N; i++)
        {
            free(nodes[i]);
        }
    }
	
    fclose(fout);
	getch();
    return 0;
}
