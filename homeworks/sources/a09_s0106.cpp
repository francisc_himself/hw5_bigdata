#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>
#include <time.h>
/*
Breadth-first search constructs a breadth-first tree, initially containing only its
root, which is the source vertex s. Whenever the search discovers a white vertex 
in the course of scanning the adjacency list of an already discovered vertex u, the
vertex  and the edge (u,v) are added to the tree.
The breadth-first-search procedure BFS below assumes that the input graph
 is represented using adjacency lists. It attaches several additional
attributes to each vertex in the graph. We store the color of each vertex u 2 V
in the attribute u:color and the predecessor of u in the attribute u:. If u has no
predecessor (for example, if u D s or u has not been discovered), then u: D NIL.
The attribute u:d holds the distance from the source s to vertex u computed by the
algorithm. The algorithm also uses a first-in, first-out queue Q (see Section 10.1)
to manage the set of gray vertices.
*/
int op=0;
int N=10;
int parent[11];
int presence[11];
/***************************************TRANSFORMATION***********************************************/
typedef struct nodemulty
{
	int key;
	nodemulty ** children;
	int nrchld;
}nodeM;
typedef struct nodebinary
{
	int key;
	nodebinary* parent;
	nodebinary* left;
	nodebinary* right;
}nodeB;
nodeM * Multy;
nodeB * Binary;
void transform()
{
    int i;

    for (i=0; i<N; i++)
    {
       Binary[i].key = i;
	   Binary[i]. left = NULL;
	   Binary[i]. right = NULL;
    }

    for (i=0; i<N; i++)
    {
		//the first child node
		if(Multy[i].nrchld!=0)
			{
				if ( Multy[i].children[0] != NULL)
						Binary[i].left = &Binary[Multy[i].children[0]->key];}
				else
						Binary[i].left = NULL;

       // the brother on the right (i.e. the next brother node)
       if (Binary[i] .left != NULL)
            {
				for(int j=1;j<Multy[i].nrchld;j++)
					 if (Multy[i].children[j] != NULL)
							Binary[Multy[i].children[j-1]->key]. right = &Binary[Multy[i].children[j]->key];     
            }
		
    }
	
}

void pretty_print (nodeB *p, int tabs)
{	

	int i;
	nodeB *q;

	for(i=0; i<tabs; i++)
		printf(" ");
	printf("%d\n",p->key);
	
	if(p->left != NULL)
		pretty_print(p->left,tabs+2);
	
	q = p->right;
	if (q != NULL)
	{
		pretty_print(q,tabs);
	}
}

/**************************************BFS***********************************************/
//node need for the graph
		typedef struct nod {
			int key;
			int dist;
			int color; //0-white,1-gray,2-black
		} vert;

		//node for the adjecy list
		typedef struct node {
			vert *v;
			struct node *next;
		} List;

		//queue node used in the BFS algorithm
		typedef struct queue {
			vert *q[200];
			int tail;
			int head;
		} Queue;




		//operation on queue,enqueue
		void enqueue(Queue *que,vert *s) {
			(*que).q[(*que).tail]=s;
			(*que).tail++;
		}
		//operation on queue,dequeue
		vert *dequeue(Queue *que) {
			(*que).head++;
			return (*que).q[(*que).head-1];
		}



		//BFS algorithm
				void BFS(List *adj[],vert *s,int n) {
						Queue que;
						vert *u;
						List *p;
						//initialize the parrent vector
						for(int i=0;i<=10;i++)
							{	if(i==s->key)
								parent[s->key]=-1;
								else
								parent[i]=0;
							}
							//initialize de distance to infinity(-1) and color to white
							for (int i=0;i<n;i++) {
									adj[i]->v->color=0;
									adj[i]->v->dist=-1;
									op=op+2;
													}
							//color of s is gray and distance is 0
							s->color=1;
							s->dist=0;
							//initilize the tail and head of the queue
							que.head=0;
							que.tail=0;
							enqueue(&que,s);
							op=op+3;
							while (que.head != que.tail) {
								u=dequeue(&que);
								p=adj[u->key];
								op=op+3;
								while (p != NULL) {
									op=op+1;
									if (p->v->color==0) {
											p->v->color=1;
											p->v->dist=u->dist+1;
											op=op+3;
											if(parent[p->v->key]!=-1)
												parent[p->v->key]=u->key;
												//printf("%d has parent node: %d \n",p->v->key,u->key);
												enqueue(&que,p->v);
												op=op+2;
														}
										p=p->next;
													}
										u->color=2;//set the color to clack
										op=op+1;
					}
						/*	for(int i=0;i<=10;i++)
								printf("%d has parent node: %d \n",i,parent[i]);*/

					}

	//program to create de adjency lists
		void create_adjlist(List *adj[],int n,int m)
				{		List *p;
						vert *v1,*v2;
						int noE=0;
						int ok=1;
						srand(time(NULL));
						//allocate all the vertices
						for (int i=0;i<n;i++) {
							adj[i]=(List *)malloc(sizeof(List));
							adj[i]->v=(vert*)malloc(sizeof(vert));
							adj[i]->v->key=i;
							adj[i]->next=NULL;
												}
						//while there are nodes to be added
						while (noE <= m) 
							{
								v1=adj[rand()%n]->v;
								v2=adj[rand()%n]->v;
								p=adj[v1->key];
								ok=1;
						while (p->next!=NULL)//verify if the edge has be already added 
						{
								if (p->v->key==v2->key)
								{
										ok=0;
									break;
								}
							p=p->next;
						}
						if (p->v->key==v2->key) 
								ok=0;
						//add the new egde
						if (ok) 
						{
							p->next=(List *)malloc(sizeof(List));
							p->next->v=v2;
							p->next->next=NULL;
							noE++;
							}
						}
					}

		

int main() 
{
int k[11];
int root;
List *adj[200],*p;
FILE *g=fopen("Case1.txt","w");
FILE *f = fopen("Case2.txt","w");
/*
srand(time(NULL));
create_adjlist(adj,10,20);

printf("\nAdjacency list: \n");
for (int i=0;i<10;i++) 
{
p=adj[i];
while (p!=NULL) {
printf("%d ",p->v->key);
p=p->next;
}
printf("\n");
}

for (int i=0;i<10;i++) {
printf("\nBFS traversal: ");
BFS(adj,adj[i]->v,10);
printf("\n\n");
///************Transform*************
for(int i=0;i<N;i++)
		if(parent[i]!=-1)
		presence[parent[i]]++;
		else
			root=i;

	Multy=(nodemulty *)malloc(9 * sizeof(nodemulty));
	for(i=0;i<N;i++)
		{
			Multy[i].key=i;	
			if(presence[i]>0)
				Multy[i].children=(nodemulty **)malloc(presence[i] * sizeof(nodemulty));
			else
				Multy[i].children=NULL;
			Multy[i].nrchld=presence[i];
			k[i]=0;
	
	}
	
printf("Number of childern  :\n\n");
for(int i=0; i<N; i++)
		printf("Node %d has %d children\n",i,Multy[i].nrchld);
		
for(int i=0; i<N; i++){
		
		if(parent[i]>=0 && Multy[parent[i]].nrchld>0) 
			{Multy[parent[i]].children[k[parent[i]]]=&Multy[i];
				k[parent[i]]++;
		}
	}

Binary=(nodebinary *)malloc(9 * sizeof(nodebinary));
transform();
printf("\nBinary  :\n\n");
pretty_print(&Binary[root],2);
} 
*/


for (int n=1000;n<=5000;n+=100)
	{
	create_adjlist(adj,100,n);

	//for (int i=0;i<100;i++)
	BFS(adj,adj[rand()%100]->v,100);
	fprintf(g,"%ld %ld\n",n,op);
	}

for (int n=100;n<=200;n+=10) 
		{
		op=0;
		create_adjlist(adj,n,9000);
		//	for (int i=0;i<n;i++)
		BFS(adj,adj[rand()%n]->v,n);
		fprintf(f,"%ld %ld\n",n,op);

		}
	//getch();
	return 0;
}