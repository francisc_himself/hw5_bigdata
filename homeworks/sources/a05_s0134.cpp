#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

#define dim 9973
#define m 3000

fstream f("hashtable.csv",ios::out);
long H[dim];
long searchH[3001];

int found_total_effort, notFound_total_effort, max_found, max_notFound, nr_elem_found, nr_elem_notFound;
double found_medium_effort, notFound_medium_effort;

int hashFunction(long k,int i)
{
    int hFirst;
    int x;
    hFirst=k%dim;
    x=(hFirst+(1*i)+(2*i*i))%dim;
    return x;
}

void add(long k)										//add element to the hashtable
{
    int i=0;
    int j=hashFunction(k,i);							//j gets the hash code of k that we want to add
    while ((H[j]!=-1) && (i<dim) )				 		//we go through the hash table until we find an element equal to -1
    {
        i++;
        j=hashFunction(k,i);
    }

    if (i!=dim)
    {
														//at the end of the cycle, j will be a value where we can add to the hash table
        H[j]=k;                     					//value k is added at the generated code
    }
    else cout<<"Overflow";
}

void init(long H[],int size)
{
    for (int i = 0 ; i < size ; i++)
        H[i] = -1;
}

int search(long k)
{
    int i=0;
    int j=hashFunction(k,i);
    while ((H[j]!=-1)&&(H[j]!=k) && (i<dim))			//we search for element k in the hash table
    {
        i++;
        j=hashFunction(k,i);
    }

    if ( (H[j]==-1) || (i == dim) )
    {													//if the value of the hash with the dispersion code is -1, return -1
        nr_elem_notFound++;								
        notFound_total_effort+=i;							//code not found
        max_notFound=max(max_notFound,i);	
        return -1;
    }

    max_found = max(max_found,i);
    nr_elem_found++;
    found_total_effort+=i;	
	return j;											//if value found, return the hashcode of k
}

double alfa [] = {0.8, 0.85, 0.9, 0.95, 0.99 };			//filling factor alfa

int main ()
{
	/*
   int n;
    long x,y;
    srand((unsigned)time(NULL));
    int cnt_aux=0;

    f<<"Filling factor"<<","<<"Avg. effort FOUND"<<",";
    f<<"Max. effort FOUND"<<","<<"Avg. effort NOT FOUND"<<",";
    f<<"Max. effort NOT FOUND"<<endl;

    for (int i=0; i<5; i++)								//execute five runs and calculate average
    {
        found_medium_effort=0;
        notFound_medium_effort=0;
        for (int k=1; k<=5; k++)						//5 inserts and 5 searches for every filling factor
        {	
            n=(int)(alfa[i]*dim);						//calculate filling factor           alfa=n/dim
            init(H,dim);                			    //all fields set to -1
            found_total_effort=notFound_total_effort=nr_elem_found=nr_elem_notFound=0;
            cnt_aux=0;
            for (int j=1; j<=n; j++ )					//introduce filling factor nr of elements
            {
                x = (rand()*rand())%100000;
                add(x);
                if (j%7==0)
                {
                    searchH[cnt_aux++] = x;
                }
            }
            for (int j = cnt_aux ; j <=m;j++)
                {
                    searchH[j] = rand() + 100000;
                }
            for (int j=1; j<=m; j++)					//we look for the m=3000 values
            {
                y = search(searchH[j]);					//we look the number

            }
            found_medium_effort+=((double)found_total_effort/(double)nr_elem_found);		//medium effort is calculated
            notFound_medium_effort+=((double)notFound_total_effort/(double)nr_elem_notFound);
        }
        found_medium_effort = found_medium_effort /5;				//medium effort for the 5 runs
        notFound_medium_effort = notFound_medium_effort /5;
        f<<alfa[i]<<","<<found_medium_effort<<","<<max_found<<","<<notFound_medium_effort<<","<<max_notFound<<endl;
    }
	*/
	//demo
	int n=7;
	init(H,n);
	add(3);
	add(6);
	add(-5);
	add(2);
	add(24);
	add(14);  //overflows
	add(26);
	for (int i=0; i<n; i++)
	{
		cout<<H[i]<<" ";
	}
	cout<<endl;
	int j=search(19);  //not found
	cout<<j<<endl;
	j=search(24);  //found
	cout<<j;
   // return 0;
	getch();
}
