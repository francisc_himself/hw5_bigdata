// ***ANONIM*** ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***

// Pentru AVG am facut graficele pentru HEAPSORT si QUICKSORT pe vectori cu valori aleatoare. 
//Am observat ca QSORT-ul este mai eficient.

//Pentru WORST am facut graficul pentru QSORT. 
//O(n^2);

//Pentru BEST graficul pentru QSORT2. Este un alt algoritm pentru acest caz.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"
#define MAX_SIZE 10000

Profiler profiler("Lab3");

int i,m,n,l,r,largest,k,nivel,aux,j;



//************HEAPSORT*****************

void max_heapify(int *a,int i)
{
	l=2*i;//left i
	r=2*i+1;//right i
	profiler.countOperation("hs",n);
	if((l<=m)&&(a[l]>a[i]))
	{
		largest=l;
	}
	else
	{
		largest=i;
	}
	
	profiler.countOperation("hs",n);
	if((r<=m)&&(a[r]>a[largest]))
	{
		largest=r;
	}
	if(largest!=i)
	{
		aux=a[i];
		a[i]=a[largest]; 
		a[largest]=aux;
		profiler.countOperation("hs",n,3);
		max_heapify(a,largest);
	}
}

void build_max_heap(int *a)
{
	m=n;
	for(i=n/2;i>0;i--)
	{
		max_heapify(a,i);
	}
}

void heapsort(int *a)
{
	build_max_heap(a);
	for(i=n;i>0;i--)
	{
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		m=m-1;
		profiler.countOperation("hs",n,3);
		max_heapify(a,1);
	}
}

//******************* QUICKSORT ****************

int partition(int *a2,int p, int r)
{
	profiler.countOperation("qs",n);
	int x=a2[r];
	i=p-1;
	for(j=p;j<=r-1;j++)
	{
		profiler.countOperation("qs",n);
		if(a2[j]<=x)
		{
			i=i+1;
			aux=a2[i];
			a2[i]=a2[j];
			a2[j]=aux;
			profiler.countOperation("qs",n,3);
		}
	}
	aux=a2[i+1];
	a2[i+1]=a2[r];
	a2[r]=aux;
	profiler.countOperation("qs",n,3);
	return (i+1);
}

void quicksort(int *a2,int p,int r)
{
	
	if (p<r)
	{
		int q=partition(a2,p,r);
		quicksort(a2,p,q-1);
		quicksort(a2,q+1,r);
	}
}

//***************** QSORT 2 **********************
int hoare_partition(int *a,int p,int r)
	{
		int x=a[p];
		i=p-1;
		j=r+1;
		profiler.countOperation("qs2",n);
		while(TRUE) // ????
		{
			do
			{
				j=j-1;
				profiler.countOperation("qs2",n);
			}while(a[j]<=x);

			do
			{
				i=i+1;
				profiler.countOperation("qs2",n);
			}while(a[j]>=x);

			if (i<j)
			{
				aux=a[i];
				a[i]=a[j];
				a[j]=aux;
				profiler.countOperation("qs2",n,3);
			}
			else
				return j;
		}
	}

	void quicksort2(int *a,int p,int r)
{
	
	if (p<r)
	{
		int q=hoare_partition(a,p,r);
		quicksort2(a,p,q-1);
		quicksort2(a,q+1,r);
	}
}

void afisare(int *v,int n,int k,int nivel)
{
	if(k>n)
		return;
	afisare(v,n,2*k+1,nivel+1);
	for(i=1;i<=nivel;i++)
		printf("   ");
	printf("%d \n",v[k]);
	afisare(v,n,2*k,nivel+1);
}

void main()
{
	int a[MAX_SIZE];
	int a2[MAX_SIZE];
	int v[MAX_SIZE];
	

	// ***************** AVG *********************
	/*for (j=1;j<=5;j++)
	{

	for (n=100;n<10000;n=n+100)
	{
 
	FillRandomArray(v, n+1, 0, 20000, true, 0);
 
	memcpy(a,v,n*sizeof(int));
	memcpy(a2,v,n*sizeof(int));

	m=0;
	heapsort(a);

	quicksort(a2,1,n);

	}
	}
	
	profiler.createGroup("rezultat","hs","qs");
*/

	//******************** WORST *****************
	/*for (n=100;n<10000;n=n+100)
	{
 
	FillRandomArray(v, n+1, 0, 20000, true,2);
 
	memcpy(a,v,n*sizeof(int));
	memcpy(a2,v,n*sizeof(int));

	m=0;

	quicksort(a2,1,n);

	}*/
	
	//************** BEST ****************

	for (n=100;n<10000;n=n+100)
	{
 
	FillRandomArray(v, n+1, 0, 20000, true,0);
 
	memcpy(a,v,n*sizeof(int));

	m=0;

	quicksort2(a,1,n);

	}

	//---------------------------
	//Pentru un vector dat de la tastatura:

	/*n=7;
	m=n;
	int a[20]={0,2,12,1,4,5,6,10};
	int a2[20]={0,2,12,1,4,5,6,10};
	
	heapsort(a);
	afisare(a,n,1,0);

	quicksort(a2,1,n);
	//afisare(a2,n,1,0);
	for(i=1;i<=n;i++)
	{
		printf("%d, ",a2[i]);
	}
	*/

	profiler.showReport();

} 