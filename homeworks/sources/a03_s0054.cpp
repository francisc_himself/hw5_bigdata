/* \***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***
In acest laborator am avut de comparat algoritmul de sortare heapsort cu cel quicksort. chiar daca quick_sortul este 
mai elegant este mai putin eficient decat heap-sortul iar acest lucru rezulta din tabele si din faptul ca in cazul
quick sort se fac n^2 operatii, in timp ce pentru heap-sort se fac doar n*log(n) operatii
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
#include "profiler.h"

Profiler profiler ("***ANONIM***");

using namespace std;
#define NEG_INF INT_MIN

int lung, dimens;
// in aceasta parte a proiectului definim subarborele stang, drept si parintele
int stg(int i) 
	{	return (2*i);
	}

int dr(int i) 
	{	return (2*i+1);
	}

int parinte(int i) 
{	return (i/2);
}

// algoritmulul Heap_sort
// aceasta functie gaseste elementul maxim si il pozitioneaza dupa caz
void MAX_HEAPIFY(int *A, int i) 
{
	int l, r, ln, aux;
	l = stg(i);
	r = dr(i);
		profiler.countOperation("Heap_sort",lung);
		if(l <= dimens && A[l] > A[i])
			ln = l;
		else 
			ln = i;
		profiler.countOperation("Heap_sort",lung);
		if(r <= dimens && A[r] > A[ln])
			ln = r;
		if(ln != i)
		{
			profiler.countOperation("Heap_sort",lung,3);
			aux = A[i];
			A[i] = A[ln];
			A[ln] = aux;
			MAX_HEAPIFY(A, ln);
	}
}
// constructia propriuzisa
void BUILD_MAX_HEAP_BU(int *A) 
{
	dimens = lung;
		for(int i = lung/2; i >0; i--)
			MAX_HEAPIFY(A,i);
}
//sortarea heap-ului construit
void HEAPSORT(int *A) 
{
	int aux;
	BUILD_MAX_HEAP_BU(A);
	for(int i = lung; i >= 2; i--)
	{
			
			aux = A[i];
			A[i] = A[1];
			A[1] = aux;
			profiler.countOperation("Heap_sort",lung,3);
			dimens --;
			MAX_HEAPIFY(A, 1);
	}
}
// aprtitionarea
int partition(int A[], int p, int r)
{
	int x, i,aux, j;
	x=A[r];
	i=p-1;

	for (j=p; j<=r-1; j++)
	{profiler.countOperation("quick_sort",lung,1);
		if (A[j]<=x)
		{profiler.countOperation("quick_sort",lung,3);
			i=i+1;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
		}
	}
	profiler.countOperation("quick_sort",lung,3);
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;
	return(i+1);
}
// algoritmul quick_sort
void quick_sort(int A[], int p, int r)
{
	int q;
	if (p<r)
	{
		q=partition(A, p, r);
		quick_sort(A, p, q-1);
		quick_sort(A, q+1, r);
		profiler.countOperation("quick_sort",lung,3);
	}
}





// main
void main()
{
	// afisarea graficelor 
	// crearea unui vector si umplerea acestuia cu numere random si apoi aplicarea vectorului respectiv
	//la fiecare caz
int A[10001],B[10001];
		for(int i=1; i <= 5; i++)
			for(lung = 100; lung <= 10000; lung += 100)
			{
				FillRandomArray(A,lung);
				  memcpy(B,A,lung*sizeof(int));
				 BUILD_MAX_HEAP_BU(A);
				 quick_sort(B,1,lung );
			
			}
		
			profiler.createGroup("Rezultate","Heap_sort","quick_sort");
			 profiler.showReport();



getch();
}



