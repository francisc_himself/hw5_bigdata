#include<stdlib.h>
#include<stdio.h>
#include<iostream>

#include "Profiler.h" 
#define V_MAX 10000
#define E_MAX 60000

Profiler profiler("***ANONIM***");
typedef struct node{
	int rank;
	struct node *p;
} node;


typedef struct graph{
	int nrm;
	int nrn;
}graph;
int test;
node v[V_MAX];
int marg[E_MAX];

void makeset (node *x){
	//profiler.countOperation("OP", test);
	test++;
	x->p=x;
	x->rank=0;
}
	

node *findset(node *x){
	//profiler.countOperation("OP", test);
	test++;
	if (x!=x->p)

		x->p=findset(x->p);
	return x->p;
}

void link(node *x, node *y){

	if (x->rank>y->rank)
		y->p=x;
	else {
		x->p=y;
		if (x->rank==y->rank)
			y->rank=y->rank+1;
	}
}

void Union(node *x, node *y){
//	profiler.countOperation("OP", test);
	test++;
	link(findset(x),findset(y));
}

void cc (graph *G){
	for(int i=0; i< G->nrn; i++)
		makeset(&v[i]);
	FillRandomArray(marg, G->nrm, 0, G->nrn*G->nrn-1, true);
	for(int i=0; i<G->nrm; i++)
	{

		int a=marg[i]/G->nrn; //noduri
		//printf("Nodul:%d", a);
		int b=marg[i]%G->nrn;//muchii
		//printf("Muchia:%d", b);
		if(findset(&v[a])!=findset(&v[b]))
		Union(&v[a], &v[b]);
	}
}


int main() {
	graph *G=(graph*)malloc(sizeof(graph));
	G->nrm=3;
	G->nrn=5;
	cc(G);
	FILE*fd;
	fd=fopen("asfis.csv","w");
	fprintf(fd,"i; lung\n");
/////////////////////////////
	G->nrn=V_MAX;
	for(int i=V_MAX; i<=E_MAX; i+=1000){
		printf("%d",i);
		G->nrm=i;
		test=i;
		cc(G);
		fprintf(fd,"%d ; %d\n",i,test);
	}
	
	//

	///////////////////
	return 0;
}
