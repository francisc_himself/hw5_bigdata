#include<iostream>
#include<time.h>
#include<conio.h>
/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
QuickSort vs HeapSort
In cazul mediu statistic, quicksort are o performanta mai buna decat heapsort
In cazul devaforabil, quicksort are complexitate O(N^2) in timp ce heapsort are o(N log N)
*/


int dim1=0, dim2=0;				
long int asignari=0, comparari=0;


void generare_sir(int n, int *v, int *u) {		//generez sirul de elemente random
	int i;
	//*v=rand()%10000;
	for(i=1;i<=n;i++) {
		*(v+i)=rand()%10000;
		*(u+i)=*(v+i);
	}
}

int p(int i) {				//parintele elementului de pe pozitia i
	return i/2;
}

int st(int i) {				//fiul din stanga al elementului de pe pozitia i
	return 2*i;
}

int dr(int i) {				//fiul din dreapta al elementului de pe pozitia i
	return 2*i+1;
}



void reconstructie_heap(int *v, int i) {		//parcurge sirul si verifica daca elementele sunt in ordinea corespunzatoare structurii de heap
	int maxim;									//(in cazul nostru, parintele are valoare mai mare decat fiii lui)
	int aux, m;
	maxim=i;
	comparari++;
	if(st(i)<=dim1 && *(v+st(i))>*(v+i))
		maxim=st(i);
	else
		maxim=i;
	comparari++;
	if(dr(i)<=dim1 && *(v+dr(i))> *(v+maxim))
		maxim=dr(i);
	if(maxim!=i) {
		aux=*(v+i);
		*(v+i)=*(v+maxim);
		*(v+maxim)=aux;
		asignari+=3;
		reconstructie_heap(v, maxim);
	}
}

void constructie_bu(int *v,  int n) {			//se construieste heap-ul bottom up, mai exact, se apeleaza reconstructie pentru elementele cu
												//de la 1 la n/2(pentru frunze nu se mai apeleaza pentru ca ele nu au niciun fiu)	
	int i;
	dim1=n;
	for(i=n/2;i>=1;i--)
		reconstructie_heap(v,  i);
}

void heap_sort(int *v, int n) {
	int t, i;
	asignari=0;
	comparari=0;
	constructie_bu(v, n);
	for(i=n;i>=2;i--) {
		t=*(v+1);
		*(v+1)=*(v+i);
		*(v+i)=t;
		asignari+=3;
		dim1--;
		reconstructie_heap(v, 1);
	}

}

void sort_descresc(int *v, int n) {
	int i, j, t;
	for(i=1;i<n;i++)
		for(j=i+1;j<=n;j++)
			if(*(v+i)<*(v+j)) {
				t=*(v+i);
				*(v+i)=*(v+j);
				*(v+j)=t;
			}
}


int partition(int *v, int st, int dr) {
	int i, j, x, t, y;
	y=rand()%(dr-st+1) +st;

	t=*(v+y);
	*(v+y)=*(v+dr);
	*(v+dr)=t;
	x=*(v+dr);


	i=st-1;
	for(j=st; j<=dr-1;j++) {
		comparari++;
		if(*(v+j)<=x) {
			i++;
			t=*(v+i);
			*(v+i)=*(v+j);
			*(v+j)=t;
			asignari+=3;
		}
	}
	t=*(v+i+1);
	*(v+i+1)=*(v+dr);
	*(v+dr)=t;
	asignari+=3;
	return i+1;
}

void quicksort(int *v, int st, int dr) {
	int m;
	if(st<dr) {
		m=partition(v, st, dr);
		quicksort(v, st, m-1);
		quicksort(v, m+1, dr);
	}
}

int partition_best(int *v, int st, int dr) {
	int i, j, x, t, y;

	y=(dr-st)/5 + st;
	t=*(v+y);
	*(v+y)=*(v+dr);
	*(v+dr)=t;
	x=*(v+dr);
	asignari+=4;

	i=st-1;
	for(j=st; j<=dr-1;j++) {
		comparari++;
		if(*(v+j)<=x) {
			i++;
			t=*(v+i);
			*(v+i)=*(v+j);
			*(v+j)=t;
			asignari+=3;
		}
	}
	t=*(v+i+1);
	*(v+i+1)=*(v+dr);
	*(v+dr)=t;
	asignari+=3;
	return i+1;
}

void quick_best(int *v, int st, int dr) {
	int m;
	if(st<dr) {
		m=partition_best(v, st, dr);
		quicksort(v, st, m-1);
		quicksort(v, m+1, dr);
	}
}

int partition_worst(int *v, int st, int dr) {
	int i, j, x, t, y;
	x=*(v+dr);
	asignari+=4;

	i=st-1;
	for(j=st; j<=dr-1;j++) {
		comparari++;
		if(*(v+j)<=x) {
			i++;
			t=*(v+i);
			*(v+i)=*(v+j);
			*(v+j)=t;
			asignari+=3;
		}
	}
	t=*(v+i+1);
	*(v+i+1)=*(v+dr);
	*(v+dr)=t;
	asignari+=3;
	return i+1;
}

void quick_worst(int *v, int st, int dr) {
	int m;
	if(st<dr) {
		m=partition_worst(v, st, dr);
		quick_worst(v, st, m-1);
		quick_worst(v, m+1, dr);
	}
}


int main () {
	int *sir1, *sir2;
	long int amed_heap=0, amed_quick=0, cmed_heap=0, cmed_quick=0;
	int i, jj;
	sir1=(int*)malloc((10001)*sizeof(int));
	sir2=(int*)malloc((10001)*sizeof(int));
	srand(time(NULL));
	/*
	int n=10;
	generare_sir(n, sir1, sir2);
	for(i=1;i<=n;i++)
		printf("%d ", *(sir1+i));
	printf("\n ^ Sirul dupa generare\n");
	heap_sort(sir1, n);
	for(i=1;i<=n;i++)
		printf("%d ", *(sir1+i));
	printf("\n ^ Sirul dupa heapsort\n");
	quicksort(sir2, 1, n);
	for(i=1;i<=n;i++)
		printf("%d ", *(sir1+i));
	printf("\n ^ Sirul dupa quicksort\n");
	*/
	
	FILE *p, *q, *r;
	p=fopen("average.csv", "w");
	q=fopen("best.csv", "w");
	r=fopen("worst.csv", "w");
	fprintf(p, "n,a_heap,c_heap,a+c_heap,a_quick,c_quick,a+c_quick\n");
	fprintf(q, "n, a_heap, c_heap, a+c_heap, a_quick, c_quick, a+c_quick\n");
	fprintf(r, "n, a_heap, c_heap, a+c_heap, a_quick, c_quick, a+c_quick\n");

	for(i=100; i<=10000;i+=100) {
		amed_heap=0;
		amed_quick=0;
		cmed_heap=0;
		cmed_quick=0;
		for(jj=1;jj<=5;jj++) {
			dim1=i;
			generare_sir(i, sir1, sir2);

			asignari=0;
			comparari=0;
			heap_sort(sir1, i);
			amed_heap+=asignari;
			cmed_heap+=comparari;
			asignari=0;
			comparari=0;

			quicksort(sir2, 1, i);
			amed_quick+=asignari;
			cmed_quick+=comparari;

		}
		amed_heap/=5;
		amed_quick/=5;
		cmed_heap/=5;
		cmed_quick/=5;
		fprintf(p, "%d,%d,%d,%d,%d,%d,%d\n", i, amed_heap, cmed_heap, amed_heap+cmed_heap, amed_quick, cmed_quick, amed_quick+cmed_quick);
		printf("1");
	
		//best case					pentru quick in cazul best case, sirul este sortat crescator iar partitia se face cu raport constant 
									//pentru heap in cazul best case, sirul este sortat descrescator( are structura de max_heap)
		generare_sir(i, sir1, sir2);
		quicksort(sir1, 1, i);
		asignari=0;
		comparari=0;
		quick_best(sir1, 1, i);
		amed_quick=asignari;
		cmed_quick=comparari;


		sort_descresc(sir2, i);

		heap_sort(sir2, i);

		amed_heap=asignari;
		cmed_heap=comparari;
		fprintf(q, "%d,%d,%d,%d,%d,%d,%d\n", i, amed_heap, cmed_heap, amed_heap+cmed_heap, amed_quick, cmed_quick, amed_quick+cmed_quick);
		printf("2");

		
		//worst case				pentru quick in cazul worst case, sirul este sortat descrescator iar partitionarea se face la un singur element
									//pentru heap in cazul worst case, sirul este sortat crescator, deci este nevoie de un numar maxim de operatii pentru a-l aduce la structura de heap
		
		generare_sir(i, sir1, sir2);
		quicksort(sir1, 1, i);
		heap_sort(sir1, i);
		amed_heap=asignari;
		cmed_heap=comparari;
		sort_descresc(sir2, i);
		quick_worst(sir2, 1, i);
		amed_quick=asignari;
		cmed_quick=comparari;
		fprintf(r, "%d,%d,%d,%d,%d,%d,%d\n", i, amed_heap, cmed_heap, amed_heap+cmed_heap, amed_quick, cmed_quick, amed_quick+cmed_quick);

		
		printf("3");
	}

	fclose(p);
	fclose(q);
	fclose(r);
	
	return 0;
}