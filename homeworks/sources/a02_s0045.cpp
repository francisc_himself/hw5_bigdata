/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
In cazul mediu statistic cei doi algoritmi sunt foarte apropiati ca si numarul de operatii
dar totusi putem spune ca metoda top-down este putin mai eficienta.
In cazul cel mai defavorabil metoda bottom-up este mult mai eficienta decat top-down.
*/

#include <stdio.h>
#include <conio.h>
#include "Profiler.h"

#define MAX_SIZE 5000

Profiler profiler("demo");

int v[MAX_SIZE];
int v_copy[MAX_SIZE];
int n=0;
int heap_size=0;
int largest=0;
int cwb[MAX_SIZE]; //count worst bottom-up
int cwt[MAX_SIZE]; //count worst top-down

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}

void max_heapify(int *v, int i)
{
	int l=left(i);
	int r=right(i);
	profiler.countOperation("bottom-up", n, 2);
	cwb[n]+=2;

	if (l<=heap_size && v[l]>v[i])
	{
		largest=l;
		profiler.countOperation("bottom-up", n);
		cwb[n]++;
	}
	else
	{
		largest=i;
	}
	profiler.countOperation("bottom-up", n, 2);
	cwb[n]+=2;

	if (r<=heap_size && v[r]>v[largest])
	{
		largest=r;
		profiler.countOperation("bottom-up", n, 2);
		cwb[n]+=2;
	}
	profiler.countOperation("bottom-up", n);
	cwb[n]++;

	if (largest != i)
	{
		int aux=v[i];
		v[i]=v[largest];
		v[largest]=aux;
		profiler.countOperation("bottom-up", n, 3);
		cwb[n]+=3;

		max_heapify(v, largest);
	}
	profiler.countOperation("bottom-up", n);
	cwb[n]++;
}

void build_max_heap_bottom_up(int *v)
{
	heap_size=n;
	profiler.countOperation("bottom-up", n);
	cwb[n]++;

	for (int i=n/2; i>0; i--)
	{
		max_heapify(v, i);
	}
}

void pritty_print(int *v, int n, int k, int nivel)
{
	if (k>n)
	{
		return;
	}

	pritty_print(v,n,2*k+1,nivel+1);
	for (int i=1; i<=nivel; i++)
	{
		printf("   ");
	}
	printf("%d\n", v[k]);
	pritty_print(v,n,2*k,nivel+1);
}

void heap_increase_key(int *v, int i, int key)
{
	if (key < v[i])
	{
		perror("new key is smaller than current key");
	}

	v[i]=key;

	profiler.countOperation("top-down", n, 2);
	cwt[n]+=2;

	while (i>1 && v[parent(i)] < v[i])
	{
		int aux=v[i];
		v[i]=v[parent(i)];
		v[parent(i)]=aux;
		i=parent(i);
		profiler.countOperation("top-down", n, 5);
		cwt[n]+=5;
	}
	profiler.countOperation("top-down", n);
	cwt[n]++;
}

void max_heap_insert(int *v, int key)
{
	heap_size++;
	v[heap_size]=INT_MIN;

	profiler.countOperation("top-down", n);
	cwt[n]++;

	heap_increase_key(v, heap_size, key);
}

void build_max_heap_top_down(int *v)
{
	heap_size=1;

	profiler.countOperation("top-down", n);
	cwt[n]++;

	for (int i=2; i<=n; i++)
	{
		max_heap_insert(v, v[i]);
	}
}

void main()
{
	//testare top-down
	n=10;
	FillRandomArray(v, n+1, 1, n+1, true);

	memcpy(v_copy, v, (n+1)*sizeof(int));
	
	build_max_heap_top_down(v);
	pritty_print(v, n, 1, 1);

	int limit=5000;

	//average case - generare date top-down
	for (int i=100; i<=limit; i+=100)
	{
		n=i;
		FillRandomArray(v, n+1, 1, n+1, true);
		build_max_heap_top_down(v);
	}

	//testare bottom-up
	n=10;
	printf("\n\n");

	build_max_heap_bottom_up(v_copy);
	pritty_print(v_copy, n, 1, 1);

	//average case - generare date bottom-up
	for (int i=100; i<=limit; i+=100)
	{
		n=i;
		FillRandomArray(v, n+1, 1, n+1, true);
		build_max_heap_bottom_up(v);
	}

	profiler.createGroup("heap", "bottom-up", "top-down");

	profiler.showReport();

	//worst case

	//bottom up

	//initializare counter
	for (int i=0; i < MAX_SIZE; i++)
	{
		cwb[i]=cwt[i]=0;
	}

	for (int i=100; i<=limit; i+=100)
	{
		n=i;
		FillRandomArray(v, n+1, 1, n+1, true, 1);
		build_max_heap_bottom_up(v);
	}

	//top down
	for (int i=100; i<=limit; i+=100)
	{
		n=i;
		FillRandomArray(v, n+1, 1, n+1, true, 1);
		build_max_heap_top_down(v);
	}
		
	FILE *f=fopen("heap_worst_case.csv","w");

	fprintf(f, "size,bottom-up,top-down\n");
	for (int i=100; i<=limit; i+=100)
	{
		fprintf(f, "%d,%d,%d\n", i, cwb[i], cwt[i]);
	}

	fclose(f);
	
}