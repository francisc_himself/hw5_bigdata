#include<conio.h>
#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *next;
};

struct vertex
{
	int color;
	int d;
	int p;
}VERTEX[11];


	int	matrix[11][11] = { {1,1,1,1,1,1,1,1,1,1},
			{0,0,0,0,0,0,1,1,0,1,0},
			{0,0,0,1,1,0,1,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,1,1,0,0,1,0,0,0,0,0},
			{0,1,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,1,0},
			{0,1,0,0,0,0,0,0,1,0,1},
			{0,0,0,0,0,0,0,0,0,1,0}
		};


struct node *prim,*last;

int is_empty(struct node *prim)
{
    if(prim==NULL)
        return 1; //TRUE
    else
        return 0; //FALSE
}
struct node *newnode(int d)
{
    struct node *temp;
    temp=(struct node *) malloc(sizeof(node));
    temp->data=d;
    temp->next=NULL;
    return temp;
}
struct node *insert(struct node *prim, int d)
{ // insertBack
    struct node *temp, *primtemp;
    temp=newnode(d);
    if(prim==NULL) {
        prim=temp;
        return prim;
    }
    primtemp=prim;
    while(primtemp->next!=NULL)
    {
        primtemp=primtemp->next;
    }
    primtemp->next=temp;
    return prim;
}
int pop ()
{ // deleteFront
    struct node *temp;
	int val;
    if(prim==NULL)
        return 0;
    temp=prim;
	val = temp->data;
    prim=prim->next;
    free(temp);
    return val;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void add_items(int data)
{
   
prim=insert(prim,data);
   
}

void show_items()
{
    struct node* tmp;
    tmp=NULL;
   
    for(tmp=prim;tmp!=NULL;tmp=tmp->next)
    {
        printf("%d",tmp->data);
    }
    printf("\n");
}

void BFS(int s)
{
	int i,j,u,l=10;
	for(i=1;i<=10;i++)
	{
		VERTEX[i].color=0;
		VERTEX[i].d=999;
		VERTEX[i].p=-1;
	}
	VERTEX[s].color=1;
	VERTEX[s].d=0;
	VERTEX[s].p=-1;
	 
	prim=NULL;

	add_items(s);

	while(is_empty(prim)==0)
	
	{
		
		u=pop();
		
		for(i=1;i<=10;i++)
		{
			if(matrix[u][i]==1)
			{
				if(VERTEX[i].color == 0)
				{
					VERTEX[i].color=1;
					VERTEX[i].d = VERTEX[u].d+1;
					VERTEX[i].p = u;
					add_items(i);
					
				}
			}
			
			VERTEX[u].color=2;
		}
	
	}
	
}

int main()
{
   BFS(1);
   int i;
   
   for(i=1;i<=10;i++)
   {
	   printf("nod: %d, culoare: %d, distanta: %d, parinte: %d",i,VERTEX[i].color,VERTEX[i].d,VERTEX[i].p);
	   printf("\n");
   }
	


	getch();
  
	return 0;
}

