#include <stdio.h>
#include <malloc.h>

typedef struct multiNod
{
	int key;
	int nrFii;
	struct multiNod *fii[50];

}arboreMulti;
typedef struct nod
{
	int key;
	struct nod *stg,*dr;

}arboreB;


arboreB *insertStg(arboreB *rad, int key)
{
	
	arboreB *p=(arboreB*)malloc(sizeof (arboreB));
	p->key=key;
	p->stg=NULL;
	p->dr=NULL;
	
	rad->stg=p;
	return p;
	
	
}

arboreB *insertDr(arboreB *rad, int key)
{
	
	arboreB *p=(arboreB*)malloc(sizeof (arboreB));
	p->key=key;
	p->stg=NULL;
	p->dr=NULL;
	
	rad->dr=p;
	return p;
	
	
}

arboreMulti* createAM(int key)
{
	arboreMulti *p;
	 p = (arboreMulti *)malloc(sizeof(arboreMulti));
	 p->key=key;
	 p->nrFii=0;
	return p;

}
void insertAM(arboreMulti *parent, arboreMulti *child)
{
	
	parent->fii[parent->nrFii]=child;
	parent->nrFii++;


}
void prettyPrintMN(arboreMulti* nodes, int nivel=0)

{
	for(int i=0;i<nivel;i++)
	{
		printf("    ");
	}
		printf("%d\n",nodes->key);

	

	for(int i=0;i<nodes->nrFii;i++)
	{
		prettyPrintMN(nodes->fii[i],nivel+1);

	}
	
}
int validare(int t[], int size)
{
	
	int cond=0;
	for (int i=0;i<size;i++)
	{
		if (t[i]==-1) cond++;

		
		/*for(int j=0;j<size;j++)
		{
			if(t[i]==t[j]) cond++;
		}*/
		
	}
	if (cond==1) return 1;
		else return 0;
}
arboreMulti* transformare1(int t[],int size)
{	
	arboreMulti *rad;
	arboreMulti *nodes[50];
	for(int i=0;i<size;i++)
	{
		if (validare(t,size)==1)
		{
			nodes[i]=createAM(i);

		}

	}
	for(int i=0;i<size;i++)
	{
		if (t[i]==-1)
		{
			rad=nodes[i];
		}else
		{
			insertAM(nodes[t[i]],nodes[i]);
		}
	}
	return rad;


	

}
void arboreBinar(multiNod *a,arboreB *rad)
{
	if(a->nrFii==0)
	{
		return;
	}
	rad=insertStg(rad,a->fii[0]->key);
	arboreBinar(a->fii[0],rad);
	for(int i=1;i<a->nrFii;i++)
	{
		//prettyPrintMN(nodes->fii[i],nivel+1);

		rad=insertDr(rad,a->fii[i]->key);
		arboreBinar(a->fii[i],rad);

	}

}
arboreB* initB(multiNod *a)
{
	arboreB *rad;
	rad=(arboreB*)malloc(sizeof(arboreB));
	rad->stg=NULL;
	rad->dr=NULL;

	rad->key=a->key;
	
	arboreBinar(a,rad);
	return rad;
}
void inordine(arboreB *p, int nivel)
{ 
  int i;
  if (p!=0){
		 inordine(p->stg,nivel+1);
		 for(i=0;i<=nivel;i++) printf("  ");
		 printf("%2d\n",p->key);
		 inordine(p->dr,nivel+1);
	       }
}
void afis(arboreB *p,int nivel=0)
{
	if(p!=NULL)
	{
		for(int i=0;i<nivel; i++)
		{
			printf("   ");
			
		}
		printf("%d\n",p->key);

		afis(p->stg,nivel+1);
		afis(p->dr,nivel);
		
	}

}


int main()
{


	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(t[0]);
	arboreMulti* rad;
	rad=transformare1(t,size);
	prettyPrintMN(rad,4);

	arboreB* AB=initB(rad);
	//inordine(AB,0);
	afis(AB);

	return 0;
}