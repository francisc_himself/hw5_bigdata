#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

/*
	Requirement: implement the basic operations for disjoint sets: Make-Set(x), Union(x, y) and Find-Set(x) 
	We identify each set by a representative, which is some member of the set.
	Since the sets are disjoint, each UNION operation reduces the number of sets by one.
	After n -1 UNION operations, therefore, only one set remains. The number of UNION operations is thus at most n-1.

	Disjoint-set forests: we represent sets by rooted trees, with each node containing one member and each tree representing one set
		-each member points only to its parent
		-2 heuristics��union by rank� and �path compression��we can achieve an asymptotically optimal disjoint-set data structure
		-A MAKE-SET operation simply creates a tree with just one node
		-We perform a FIND-SET operation by following parent pointers until we find the root of the tree
		-The nodes visited on this simple path toward the root constitute the find path
		-A UNION operation, causes the root of one tree to point to the root of the other

	Union by rank: For each node, we maintain a rank, which is an upper bound on the height of the node. In union by rank, we
	make the root with smaller rank point to the root with larger rank during a UNION operation.

	Path compression: we use it during FIND-SET operations to make each node on the find path point directly to the root.
						Path compression does not change any ranks.

	When we use both union by rank and path compression, the worst-case running time is O(m*alpha(n)), where alpha(n) 
	is a very slowly growing function; we can view the running time as linear in m in all practical situations.


*/



#define V 10000  //nr of vertices

long calls;  //count the number of calls to Make-Set(x), Union(x, y) and Find-Set(x)

typedef struct node{
	int key;
	int rank;     //the number of edges in the longest simple path between x and a descendant leaf
	node *parent;
}nodeT;

typedef struct edge{
	int nod1;
	int nod2;
}edgeT;

//creates a new set whose only member (and thus representative) is x =>singleton set
nodeT* make_set(int x){
	
	nodeT *p = new nodeT;
	p->key = x;
	p->rank= 0;   //initial rank of 0.
	p ->parent = p;
	return p;
}

//returns a pointer to the representative of the (unique) set containing x
nodeT* find_set(nodeT *x){

	if(x != x->parent){
		x->parent = find_set(x->parent); //it makes one pass up the find path to find the root
										 //it makes a second pass back down the find path to update each node to point directly to the root
	}
	return x->parent;
}

//suroutine called by UNION
void link(nodeT *x, nodeT *y){

	if(x->rank > y-> rank) //If the roots have unequal rank, we make the root with higher rank the parent of the root with lower rank
		y->parent = x;
	else if(x->rank < y->rank)
		x->parent = y;
	else if(x->rank == y->rank){  //If the roots have equal ranks, we arbitrarily choose one of the roots as the parent and increment its rank.
		x->parent = y;
		y->rank = y->rank + 1;
	}
}

//unites the dynamic sets that contain x and y, into a new set that is the union of these two sets.
void union_set(nodeT *x, nodeT *y){
	link(find_set(x),find_set(y));
}

bool same_component(nodeT* x, nodeT* y)
{
	if (find_set(x) == find_set(y))
			return true;
		else 
			return false;
}


//initially places each vertex  in its own set. Then, for each edge (u,v) it unites the sets containing u and v
void connected_components(int ed)
{
	nodeT *initial[V];
	int a,b;
	//calls=0;
		edgeT *edges = new edgeT[ed];
		
		for(int i=0;i<V;i++){

			initial[i] = make_set(i);
			calls++;
		}

		for(int i=0;i<ed;i++){

			edges[i].nod1 = rand()%V;
			edges[i].nod2 = rand()%V;
		}

		for(int i=0; i<ed; i++){
			a= edges[i].nod1;
			b= edges[i].nod2;
			nodeT *x = initial[a];
			nodeT *y = initial[b];
			calls+=2;
			if(same_component(x,y)==false){
				union_set(x,y);
				calls+=3;
			}
		}	
}


int  main(){
	srand(time(NULL));

	//------------------------------------
	/*
	nodeT *initial[10];
	edgeT *edg = new edgeT[6];
	int a,b;
	calls=0;
	for(int i=1;i<=10;i++){
		initial[i] = make_set(i);
		calls++;
    }
	
	for(int i=1;i<=6;i++)
	{
		edg[i].nod1=rand()%10+1;
		edg[i].nod2=rand()%10+1;
	}
    
	printf("\n");
	for(int i=1; i<=6; i++){
		a= edg[i].nod1;
		b= edg[i].nod2;
		printf("%d %d \n",a,b);
		nodeT *x = initial[a];
		nodeT *y = initial[b];
		//printf("%d ",find_set(initial[a])->key);
		//printf("%d ",find_set(initial[b])->key);
		calls+=2;
		if(same_component(x,y)==false){
		        union_set(x,y);
				calls+=3;
		}
	}

	printf("\nNumber of calls: %d\n", calls);

	for(int i=1; i<=10; i++){
		printf("%d ",find_set(initial[i])->key);
	}
	*/
	//---------------------------------------------

	FILE *f = fopen("sets.txt","w");
	for(int e=10000;e<=60000;e=e+500){
		calls=0;
		connected_components(e);
		fprintf(f,"%ld %ld\n",e,calls);
	}

	fclose(f);
   // getch();
	return 0;
}