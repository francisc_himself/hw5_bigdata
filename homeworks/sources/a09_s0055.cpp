#include<iostream>
#include <list>
#include "Profiler.h"
using namespace std;
#define e_max 60000 
class Graph{
    int V; 
    list<int> *adj;    
	int edges[e_max];
public:
    Graph(int V);  
    void addEdge(int v, int w); 
    void BFS(int s);  
	void generate(int m, int n);
};
 
Graph::Graph(int V){
    this->V = V;
    adj = new list<int>[V];
}
 
void Graph::addEdge(int v, int w){
    adj[v].push_back(w);
}
 
void Graph::BFS(int s){
    
    bool *visited = new bool[V];
    for(int i = 0; i < V; i++)
        visited[i] = false;
 
    list<int> queue;
	visited[s] = true;
    queue.push_back(s);
 
    list<int>::iterator i;
 
    while(!queue.empty())
    {
       
        s = queue.front();
        cout << s << " ";
        queue.pop_front();

        for(i = adj[s].begin(); i != adj[s].end(); ++i)
        {
            if(!visited[*i])
            {
                visited[*i] = true;
                queue.push_back(*i);
            }
        }
    }
}
 
int main()
{
	  
 //   Graph g(4);
 //   g.addEdge(0, 1);
 //   g.addEdge(0, 2);
 //   g.addEdge(1, 2);
 //   g.addEdge(2, 0);
 //   g.addEdge(2, 3);
 //   g.addEdge(3, 3);
 //   for(int i=0;i<4;i++)
	//{
 //   cout << "BFS starting from vertex " << i << " is: ";
	//g.BFS(i);
	//cout << endl;
	//}
	
	int n,m;
	cout << "Size of graph:";
	cin >> n; cout << endl;
	cout << "Nr of edges:";
	cin >> m; cout << endl;
	Graph g(n);
	g.generate(n,m);
	for (int i=0;i<n;i++)
	{
		cout << "BFS visiting from vertex " << i << " is: ";
		g.BFS(i);
		cout << endl;

	}
    return 0;
}
void Graph::generate(int n, int m)
{
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		adj[a].push_back(b);
	}
}