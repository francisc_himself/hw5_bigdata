/* ***ANONIM*** Vasile-***ANONIM***, grupa ***GROUP_NUMBER***
Din punct de vedere al graficului generat, in cazul constructiei bottom-up a HEAP-ului numarul de atribuiri si comparatii este mai mic decat in
cazul constructiei top-down, insa top-down este eficient in cazul lucrului cu inserari.
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define MIN 100
#define MAX 10001
#define error -1

/*variabile globale in care am numarat comparatiile, respectiv asignarile si care vor fi reinitializate 
la inceputul fiecarui algoritm de constructie HEAP: */
long nr_cmp,nr_asig;


//functie ce returneaza indexul minimului dintre un nod si fiii sai
int index_min(int v[],int i,int dim_v)
{
	int i_min=i;
	nr_cmp++;
		if((2*i<=dim_v) && (v[i]>v[2*i]))
			i_min=2*i;
	nr_cmp++;
		if((2*i+1<=dim_v)&&(v[i]>v[2*i+1]))
			i_min=2*i+1;
	return i_min;
}


//functia de reconstructie a HEAP-ului
void reconst_heap(int v[],int i,int dim_v)
{
	int ind,aux;
	ind=index_min(v,i,dim_v);
	nr_cmp++;
	if(ind!=i)
	{
		nr_asig+=3;
		aux=v[i];
		v[i]=v[ind];
		v[ind]=aux;
		reconst_heap(v,ind,dim_v);
	}
}


//crearea unui HEAP vid
void h_init(int v[],int dim_v)
{
	dim_v=0;
}


//extragerea minimului din HEAP
int h_pop(int v[],int dim_v)
{
	int x;
	if(dim_v<=0)
		return error;
	else 
	{
		nr_asig++;
		x=v[1];
		v[1]=v[dim_v];
		dim_v--;
		reconst_heap(v,1,dim_v);
	}
	return x;
}

//functie ce returneaza indexul parintelui unui nod 
int parinte(int i)
{
	return i/2;
}

//introducerea unui element in HEAP
void h_push(int v[],int x,int *dim_v)
{
	int i,aux;
	(*dim_v)++;
	nr_asig++;
	v[(*dim_v)]=x;
	i=(*dim_v);
	while((i>1)&&(v[parinte(i)]>v[i]))
	{
		nr_cmp++;
		nr_asig+=3;
		aux=v[i];
		v[i]=v[parinte(i)];
		v[parinte(i)]=aux;
		i=parinte(i);
	}
}

//constructia BOTTOM-UP a unui HEAP
void bottom_up(int v[], int dim_v)
{
	nr_cmp=0;
	nr_asig=0;
	int i,aux,m=dim_v;

	for(i=dim_v/2;i>0;i--)
		reconst_heap(v,i,dim_v);
}

//constructia TOP-DOWN a HEAP-ului
void top_down(int a[],int dim_a,int b[],int dim_b)
{
	int i;
	nr_cmp=0;
	nr_asig=0;
	h_init(b,dim_b);
	for(i=1;i<=dim_a;i++)
	{
		h_push(a,a[i],&dim_b);
	}
}

//sirul generat aleator
void genereaza_rand(int v[],int dim_v,int parametru_srand)
{
	int i;

	srand(parametru_srand);
	for(i=0;i<dim_v;i++)
		v[i]=rand()%10000;
}

//sirul generat crescator
void genereaza_crescator(int v[],int dim_v)
{
	int i;
	for(i=0;i<dim_v;i++)
		v[i]=i+1;
}

//sirul generat descrescator
void genereaza_descrescator(int v[],int dim_v)
{
	int i;
	for(i=0;i<dim_v;i++)
		v[i]=dim_v-i;
}


int main()
{
	int a[MAX],b[MAX];
	int i;
	FILE *med;
	long na=0,nb=0,asig,comp;
	long avgasig,avgcomp;

	med=fopen("mediu_statistic.csv","w+");
	fprintf(med,"n,BU_comp,BU_asig,BU_comp+asig,TD_comp,TD_asig,TD_comp+asig\n");

	for(na=MIN;na<=MAX;na+=100)
	{

		//cazul mediu statistic
		asig=0; comp=0;
		for(i=1;i<=5;i++)
		{
			genereaza_rand(a,na,i);
			bottom_up(a,na);
			asig+=nr_asig;
			comp+=nr_cmp;
		}
		avgasig=asig/5;
		avgcomp=comp/5;
		fprintf(med,"%ld,%ld,%ld,%ld,",na,avgcomp,avgasig,avgcomp+avgasig);
		
		asig=0; comp=0;
		for(i=1;i<=5;i++)
		{
			genereaza_rand(a,na,i);
			top_down(a,na,b,nb);
			asig+=nr_asig;
			comp+=nr_cmp;
		}
		avgasig=asig/5;
		avgcomp=comp/5;
		fprintf(med,"%ld,%ld,%ld\n",avgcomp,avgasig,avgcomp+avgasig);
	}

	fclose(med);
	return 0;
}