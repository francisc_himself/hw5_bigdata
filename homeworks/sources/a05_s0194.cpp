/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

Codul de fata are rolul de a implementa un tabel de dispersie si de a verifca numarul de operatii efectuate 
in cazul cautarilor unor elemente din el.

Tabelul de dispersie implementat de mine utilizeaza adresare deschisa. 

Functia de hash este:
		(value+index*c1+index*index*c2)%HASH_SIZE

Am considerat c1 = 11 , c2 = 13 ,HASH_SIZE = 9973;

Rezultatele analizei algoritmului se gasesc in urmatorul tabel:

		==========================================================================
		=	Filling   =      Avg.Eff  =  Max.Eff =   AvgEff    =   Max Eff       =
		=	factor    =      fownd    =  fownd   =  not fownd  =   not fowned    =
		==========================================================================
		=	  0.80    =        0      =   29     =      5      =      31         =
		=     0.85    =        1      =   44     =      7      =      47         =
		=     0.90    =        1      =   37     =     11      =      95         =
		=     0.95    =        2      =   81     =     20      =     129         =
		=     0.99    =        2      =   96     =    100      =     875         =
		==========================================================================

Interpretari:
	Chiar daca tabelul de dispersie este aproape plin, gasirea elementelor se face
	intr-un timp foarte scurt.
	Daca un element se gaseste in tabel, atunci el este gasit foarte repede. In cazul in 
	care un element cautat nu se gaseste in tabel, atunci efortul este mai mare, ajungand 
	in caz extremal sa fie egal cu dimensiunea tabelului.

	Complexitati :
			
			caz mediu statistic    caz defavorabil
	inserare         O(1)               O(n)                 -- n = dimensiunea hash-ului
	cautare	         O(1)               O(n)                 -- n = dimensiunea hash-ului


***********
Solutie pentru stergerea transparenta a elementelor:
	 
	-- utilizam doua tabele de hash. Unul din ele va fi disponibil utilizatorilor, celalalt va fi 
folosit ca buffer. Periodic construim un buffer actualizat si schimbam tabelul disponibil cu tabelul de hash;

Avantaje:
	-- Utilizatorii nu sunt afectati de schimbarile de buffer. Nu exista timpi in care ei sa nu poata sa 
efectueze cautari in tabela de hash.

Dezavantaje:
	-- Memoria folosita este dubla;
	-- Actuaizarea obiectelor din tabela de hash nu se face in timp real, ci la intervale programate.
	


*/
#include<stdio.h>
#include"Profiler.h"

#define HASH_SIZE 9973
#define SEARCH_SIZE 3000


namespace global{
int HashTable[HASH_SIZE];
}

int efort;

using namespace global;

//================================================================
// functii pentru hash

// initializaza tabela de dispersie
void InitHash(	int* HashTable = global::HashTable,			// tableul de hash
				int size = HASH_SIZE						// dimensiunea tablelui
			   )
{
	for(int i=0;i<size;i++)
	{
		HashTable[i] = -1;
	}
}

// functia de hash
int HashFunction(
				 int value,				//valoare
				 int index,				//index
				 int c1 = 11,			//constanta1 pentru functia de hash
				 int c2 =13)			//constatna2 pentru functia de hash
 // return: valoarea functiei de inserare
{
	return (value+index*c1+index*index*c2)%HASH_SIZE;
}

//insereaza elementul cu valoarea value in heap
int HashInsert(
				int value,							//valoarea
				int*HashTable = global::HashTable,	//tabel de hash
				int c1=11,							//c1 pentru functia de hash
				int c2=13							//c2 pentru functia de hash
				)
// return: index-ul la care se insereaza;
{
	int i =0;
	int tableIndex;
	do
	{
		tableIndex =HashFunction(value,i,c1,c2);
		if(HashTable[tableIndex]==-1)
		{
			HashTable[tableIndex] = value;
			return tableIndex;
		}
		else
		{
			i++;
		}
	} 
	while(i<HASH_SIZE);
	return -1;
}

// cauta o valoare in hash
int HashSearch(
			    int value,							//valoare cautata
				int* HashTable = global::HashTable,	// tabel de hash
				int c1=11,							//c1 pentru functia de hash
				int c2=13							//c2 pentru functia de hash  
				)
// returneaza indexul valorii sau -1 in cazul in care valoare nu este gasita
{
	int i =0;
	int tableIndex;
	do
	{
		tableIndex = HashFunction(value,i,c1,c2);
		if(HashTable[tableIndex] == value)
		{
			return tableIndex;
		}
		i++;
		efort++;
		if(efort > HASH_SIZE)
		{
			printf("aciii");
		}
	}
	while(HashTable[tableIndex]!=-1 && i<HASH_SIZE);
	return -1;
}
//================================================================
//functii pentru task-uri

void VerificaCorectitudine()
{
	int hashIndex;
	InitHash();

	for(int i = 1; i <= 30;i++)
		HashInsert(i);
	
	printf("Tabela de disperise este: \n");
	for(int i = 1; i <= 30;i++)
	{
		printf("%d\n",HashTable[i]);
	}
	printf("Cautare:\n");
	for(int i = 1; i <= 50;i+=5)
	{
		if((hashIndex=HashSearch(i))!=-1)
		{
			printf("am gasit valoarea %d pe pozitia %d\n", i, hashIndex);
		}
		else 
		{
			printf("nu am gasit valoarea %d\n", i);
		}


	}
}

void MakeTableLine(
				   int factorDeUmplere // factorul de umplere
				   )
{
	FILE *fout;
	int samples[HASH_SIZE+SEARCH_SIZE/2];
	int indices[SEARCH_SIZE/2];
	int efortFound =0;
	int maxEfortFound=0;
	int efortNotFound =0;
	int maxEfortNotFound=0;
	
	
	InitHash();

	int n = factorDeUmplere*HASH_SIZE/100; 
	
	if((fout = fopen("tabel.txt","a"))==NULL)
	{
		perror("Eroare de deschidere pentru tabel.txt");
		return;
	}

	
	FillRandomArray(samples,n+SEARCH_SIZE/2,1,1000000,true,0);
	FillRandomArray(indices,SEARCH_SIZE/2,0,n-1,true,0);

	//formam hash-ul
	printf("inserting...\n");
	for(int i = 0;i<n;i++)
	{
		if(HashInsert(samples[i])==-1)
		{	
			printf("faillll");
			return ;
		}
			
	}
	
	//found
	printf("generateing found...\n");
	for(int i= 0;i<SEARCH_SIZE/2;i++)
	{
		printf("i= %d...\n",i);
		efort =0;
		if(HashSearch(samples[indices[i]])==-1)
		{
			printf("fail fownd");
			return ;
		}
		if(maxEfortFound < efort)
		{
			maxEfortFound = efort;
		}
		efortFound+=efort;
	}
	
	printf("generateing not found...\n");

	for(int i= 0;i<SEARCH_SIZE/2;i++)
	{
		printf("i= %d...\n",i);
		efort =0;
		HashSearch(samples[n+i]);
		if(maxEfortNotFound < efort)
		{
			maxEfortNotFound = efort;
		}
		efortNotFound+=efort;
	}
	
	fprintf(fout,"%10.2f %10d %10d %10d %10d\n",factorDeUmplere/100.0,
		2*efortFound/SEARCH_SIZE,maxEfortFound,2*efortNotFound/SEARCH_SIZE,maxEfortNotFound);
	fclose(fout);
}


int main()
{
	//VerificaCorectitudine();
	//MakeTableLine(80);
	//MakeTableLine(85);
	// MakeTableLine(90);
	//MakeTableLine(95);
	//MakeTableLine(99);

}
