/*
Comparing the three algorithms in the average case, we observe Selection sort performs the least number of assignments,
the graph looks almost linear (but it isn't), while Insertion sort has a lot fewer comparisons than Selection sort (almost
twice as less). When we compare the total number of operations, Insertion and Selection sort behave similarly, while Bubble
sort is clearly the least efficient of the three tested algorithms, not only in total operations performed, but at the
number of assignments and comparisons as well.

Comparing the three algorithms in the best case, Bubble sort has the best behaviour when it comes to assignments (it has 0
assignments), followed by Insertion and Selection sort, with Selection sort having the most assignments in best case.
When we look at the number of comparisons, Insertion sort has the least (the graph seems linear, but it is not), followed
by Selection sort and Bubble sort being in last place. Comparing the total number of operations in the best case, Insertion
has the least, while Bubble sort has the most total operations.

Comparing the three algorithms in the worst case, Selection sort performs the least number of assignments (once again, it seems
linear), with Bubble sort performing the most. When we look at the number of comparisons, Insertion and Selection sort
have similar behaviour, their curves overlapping. Comparing the total number of operations in the worst case, Selection
sort has the least, while Bubble has by far the most total operations.

Selection sort
Worst case performance	O(n^2)
Best case performance	O(n^2)
Average case performance	O(n^2)

Insertion sort
Worst case performance	O(n^2) comparisons, swaps
Best case performance	O(n) comparisons, O(1) swaps
Average case performance	O(n^2) comparisons, swaps

Bubble sort
Worst case performance	O(n^2)
Best case performance	O(n)
Average case performance	O(n^2)
*/

#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <math.h>
#include <fstream>
using namespace std;
#define MAX_SIZE 10

void main() {
	int i,j,k,k1,k2,buff,aux,a[100],b[100],c[100];
	int final=0, comp=0, assi=0;
	int n=100;

	for (i=0;i<100;i++)
	{
		a[i]= rand() % 1000;
		b[i]=a[i];
		c[i]=a[i];
	}

	//INSERT SORT
	cout<<"Insert sort"<<"\n";
	for (j=1;j<100;j++)
	{	
		aux=a[j];
		assi++;
		k=j-1;
		while ((a[k] > aux) && (k>=0))
		{
			a[k+1]=a[k];
			k--;
			assi++;
			comp++;
		}
		comp++;
		a[k+1]=aux;
		assi++;
	}
	for (int i = 0;i<n;i++)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;

	//SELECT SORT
	cout<<"Selection sort"<<"\n";
	for (k1=0;k1<=n-2;k1++)
	{
		buff=k1;
		for (k2=k1+1;k2<=n-1;k2++)
		{
			comp++;
			if (b[buff]>b[k2])
			{
				buff=k2;
			}
		}
		if (buff!=k2)
		{
			comp++;
			aux=b[k1];
			b[k1]=b[buff];
			b[buff]=aux;
			assi+=3;

		}
	}
	for (int i = 0;i<n;i++)
	{
		cout<<b[i]<<" ";
	}
	cout<<endl;

	//BUBBLE SORT
	cout<<"Bubble sort"<<"\n";
	for(i=0; i<n; i++)
	{
		for(j=0; j<n-1; j++)
		{
			comp++;
			if(c[j]>c[j+1])
			{
				aux = c[j+1];
				c[j+1] = c[j];
				c[j] = aux;
				assi+=3;
			}
		}
	}
	for (int i = 0;i<n;i++)
	{
		cout<<c[i]<<" ";
	}


	getch();
}