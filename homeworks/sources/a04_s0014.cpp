#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <fstream>
#include <iostream>

using namespace std;

fstream f1("generare1.csv", ios::out);
fstream f2("PrettyPrint.txt", ios::out);

typedef struct nod
{
	int info;
	nod *urm;
}NOD;

typedef struct element
{
	int poz;
	int elem;
}ELEMENT;

struct liste
{
	NOD *prim;
	NOD *ultim;
};

struct liste CAP[500];

NOD *interprim;
NOD *interultim;
long A, C;
ELEMENT Heap[500];

void pretty_print(ELEMENT A[], int i, int depth, int size)
{
	if (i<=size)
	{
		pretty_print(A, 2*i, depth+1, size);
		for (int j=0; j<=depth; j++)
			f2 << "   ";
		f2 << A[i].elem << " " << A[i].poz;
		f2 << "\n";
		pretty_print(A, 2*i+1, depth+1, size);
	}
}

//adauga un element in lista
void adaug(NOD **prim, NOD **ultim, int val)
{
	if ((*prim)==NULL)		//caz special daca lista este vida
	{
		(*prim)=new NOD;
		(*prim)->info=val;
		(*prim)->urm=NULL;
		*ultim=(*prim);
	}
	else
	{
		NOD *elem=new NOD;
		elem->urm=NULL;
		elem->info=val;
		(*ultim)->urm=elem;
		(*ultim)=elem;
	}
}

//se aranjeaza heapul astfel incat radacina este ***ANONIM***
void reconstrHeap(ELEMENT Heap[], int i, int size)
{
	int l,r, ***ANONIM***;
	ELEMENT aux;
	l=2*i;
	r=2*i+1;
	//se retine care dintre fii este mai mic decat parintele
	if ((l<=size) && (Heap[l].elem < Heap[i].elem))
		***ANONIM***=l;
	else ***ANONIM***=i;
	if ((r<=size) && (Heap[r].elem < Heap[***ANONIM***].elem))
		***ANONIM***=r;
	C+=2;
	//se inlocuieste in caz ca s-a gasit un fiu mai mic decat parintele
	if (***ANONIM***!=i)
	{
		aux=Heap[i];
		Heap[i]=Heap[***ANONIM***];
		Heap[***ANONIM***]=aux;
		A+=3;

		f2 << "\n";
		pretty_print(Heap, 1, 0, size);
		f2 << "\n";

		reconstrHeap(Heap, ***ANONIM***, size);
	}
}

//se adauga un element la heap
void inserare_heap(ELEMENT Heap[], ELEMENT x, int *size)
{
	int i=(*size);
	ELEMENT aux;

	(*size)++;
	Heap[(*size)]=x;
	C++;
	while ((i>1) && (Heap[i].elem < Heap[i/2].elem))
	{
		C++;
		aux=Heap[i];
		Heap[i]=Heap[i/2];
		Heap[i/2]=aux;
		A+=3;
		i=i/2;
	}
}

//returneaza primul element din heap
ELEMENT extrage_heap(ELEMENT Heap[], int *size)
{
	ELEMENT x;
	if ((*size)>0)
	{
		x=Heap[1];
		Heap[1]=Heap[(*size)];
		A+=2;
		(*size)--;
		reconstrHeap(Heap, 1, (*size));
	}
	return x;
}

void interclasare(liste CAP[], int k)
{
	int size=0;
	ELEMENT v, pop;
	//se pune in heap k elemente, cele mai mici
	for (int i=1; i<=k; i++)
	{
		v.poz=i;
		v.elem=CAP[i].prim->info;
		A++;
		inserare_heap(Heap, v, &size);
	}

	while (size>0)
	{
		pop=extrage_heap(Heap, &size);
		int poz=pop.poz;
		adaug(&interprim, &interultim, pop.elem);
		CAP[poz].prim=CAP[poz].prim->urm;
		A++;
		C++;
		if (CAP[poz].prim!=NULL)
		{
			v.poz=pop.poz;
			v.elem=CAP[v.poz].prim->info;
			A++;
			inserare_heap(Heap, v, &size);	//se adauga in heap din nou cel mai mic element din liste
		}
	}
}

void create_k_lists(liste CAP[], int k, int n)
{
	int nr=0;
	for (int i=1; i<=k; i++)
		CAP[i].prim = CAP[i].ultim = NULL;
	for (int j=1; j<=n/k; j++)
	{
		for (int i=1; i<=k; i++)
		{
			nr= nr+ rand();
			adaug(&CAP[i].prim, &CAP[i].ultim, nr);
		}
	}
}

void listare(NOD *prim, NOD *ultim)
{
	NOD *c;
    c=prim;
    while(c!=0)		//cat timp mai sunt in lista
    {
        cout<<c->info<<"  ";
        c=c->urm;		//avansez in lista trecand la urmatoarea adresa
    }
    cout<<endl;
}

void drop_all(liste CAP[],int k)
{
    NOD *p;
    for(int i=1; i<=k; i++)
    {
        while ( CAP[i].prim!=0)
        {
            p=CAP[i].prim;
            CAP[i].prim=CAP[i].prim->urm;
            delete p;
        }
        delete CAP[i].prim;
        delete CAP[i].ultim;
    }

    while (interprim!=NULL)
    {
        p = interprim;
        interprim = interprim->urm;
        delete p;
    }
    delete interprim;
    delete interultim;
}

int main()
{
    int k=0;
	f2<<"Pretty print";
    drop_all(CAP,3);
	A = C = 0;
	create_k_lists(CAP,3,10);
	interclasare(CAP,3);

        for (int n = 100 ; n <= 10000; n+=300)
        {
            A = C = 0;
            for (int l=1; l<=3; l++)
            {
                drop_all(CAP,k);
                A = C = 0;
                if (l==1)
                    k=5;
                else if (l==2)
                    k=10;
                else
                    k=100;

                create_k_lists(CAP,k,n);
                interclasare(CAP,k);

                    f1<<n<<","<<A+C<<",";
                f1<<endl;
                f1<<"\n\n\n";
			}
        int   n=10000;
       for (int k = 10 ; k<=500 ; k+=10)
            {
                C = A = 0;
                drop_all(CAP,k);
                create_k_lists(CAP,k,n);
                interclasare(CAP,k);
                f1<<k<<","<<A+C<<"\n";
            }
    }
    f1.close(); 
	f2.close();
    return 0;
}