/*

Am determinat di graficul rezultat ca top down si
botom up au cam aceleasi perfomanta.
In cazul valorilor aleatori, top down pare sa faca 
2% mai putine operatii.
In cazul cel mai rau top downul face doua ori mai
multe operatii.
In cazul optim, fac excat aceleasi numar de comparati
si nici o asignare.

*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int counter = 0;

void swap(int &a, int &b)
{
	int x = a;
	a = b;
	b = x;
}

class dvector
{
public:
	int* data;
	int size;

	dvector(dvector& other)
	{
		size = other.size;
		data = new int[size];
		memcpy(data,other.data,size*sizeof(int));
	}

	dvector()
	{
		size=0;
		data=0;
	}

	~dvector()
	{
		if (data!=0)
		{
			delete[] data;
		}
	}

	int& operator [] (int i)
	{
		if (i<size)
		{
			return data[i];
		}
		else
		{
			int *ndata = new int[i+1];

			if (data != 0)
			{
				memcpy(ndata,data,size*sizeof(int));
				delete[] data;
			}
			for (int j = size;j<i+1; j++)
			{
				ndata[j]=0;
			}
			data=ndata;
			size=i+1;
			return data[i];
		}
	}

};

class heap
{
public:
	dvector* vec;
	int heap_size;
	
	heap()
	{
		vec = new dvector;
		heap_size=0;
	}
	~heap()
	{
		delete vec;
	}
	int& operator [] (int i)
	{
		int &d = (*vec)[i];
		
		if (i>heap_size)
		{
			heap_size=i;
		}
		
		return d;
	}

	void max_up_heapify(int i)
	{
		if (i>1) //boundary check
		{
			int &a = (*vec)[i]; //current key
			int &b = (*vec)[i>>1]; //parent key
			counter ++;
			if (b<a)
			{
				counter +=3;
				swap(a,b);
				//print(); //step by step print;
				max_up_heapify(i>>1);
			}
		}
	}

	void max_down_heapify(int i)
	{
		int ch[2];
		ch[0] = i<<1;
		ch[1] = ch[0]+1;
		for (int k=0; k<2; k++) //for each immediate child
		{
			int &c = ch[k];
			if (c<=heap_size) //boundary check
			{
				int& iv = (*this)[i];
				int& cv = (*this)[c];
				counter++;
				if (iv<cv)
				{
					counter+=3;
					swap(iv,cv);
					//print();//step by step print
					max_down_heapify(c);
				}
			}
		}
	}
	

	void bottom_up()
	{
		for (int i=this->heap_size>>1; i>0; i--)
		{
			max_down_heapify(i);	
		}
	}

	void top_down()
	{
		for (int i=1; i<=this->heap_size; i++)
		{
			max_up_heapify(i);
		}
	}

	void set_data(dvector *vec2)
	{
		delete this->vec;
		vec=new dvector(*vec2); //copy
		heap_size=vec->size;

		(*vec)[vec->size]=0;

		for (int i=heap_size; i>0; i--)//align
		{
			vec->data[i] = vec->data[i-1];
		}
		(*vec)[0]=-1;
	}

	void bottom_up(dvector *vec2)
	{
		set_data(vec2);
		bottom_up();
	}

	void top_down(dvector *vec2)
	{
		set_data(vec2);
		top_down();
	}

	void insert(int value)
	{
		(*this)[heap_size+1]=value;		
		max_up_heapify(heap_size);
	}

	void print(int i,int depth=0)
	{
		int c1 = i<<1;
		int c2 = c1+1;

		if (c1<=heap_size)
			print(c1,depth+1);
	
		for(int k=0; k<depth; k++)
			printf("\t");

		printf("%d\n",(*vec)[i]);

		if (c2<=heap_size)
			print(c2,depth+1);
	}

	void print()
	{
		print(1,0);
	}
};

int main()
{
	dvector a;

	for (int i=0; i<10; i++)
	{
		a[i]=rand()%0xff;
	}

	{
		printf("bottomup\n");
		heap b;

		b.bottom_up(&a);
		
		b.print();
	}

	{
		printf("topdown\n");

		heap b;
		b.top_down(&a);

		b.print();
	}
	
	//int test_table[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000};

	FILE *output = fopen("out.txt","w");
	fprintf(output,"Test Size, Bottom Up Rnd, Top Down Rnd, Bottom Up Worst, Top Down Worst, Bottom Up Best, Top Down Best\n");

	//for (int i=0; i<sizeof(test_table)/sizeof(int); i++)
	for (int i=0; i<=240; i++)
	{
		int test_size = i*100;

		//random vector

		dvector test_case[3];
		int outv[6];


		for (int i=test_size-1; i>=0; i--)
		{
			test_case[0][i]=rand()&0xfff; //random
			test_case[1][i]=i; //worst case (asc sorted)
			test_case[2][i]=test_size-i; //best case (desc sorted)
		}

		for (int test = 0; test<3; test++)
		{
			heap bu,td;
			
			counter = 0;
			bu.bottom_up(&test_case[test]);
			outv[(test<<1)]=counter;

			counter = 0;
			td.top_down(&test_case[test]);
			outv[(test<<1)+1]=counter;
		}

		fprintf(output,"%d,%d,%d,%d,%d,%d,%d\n",test_size,outv[0],outv[1],outv[2],outv[3],outv[4],outv[5]);

	}

	fclose(output);
	
	return 0;
}