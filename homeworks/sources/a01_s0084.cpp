#include "stdafx.h"
#include "Profiler.h"
Profiler profiler("Tema 1 - Sortari");
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define MAX_SIZE 10000

void buble_sort(int *v,int size,int *nrAtr=NULL,int *nrComp=NULL,int *nrOp=NULL)
{
	int sortat=0;

	if(nrAtr && nrComp)
	{
		*nrAtr=0;
		*nrComp=0;
	}
	while(!sortat)
	{
		sortat=1;
		for(int j=0;j<size-1;j++)
		{
		
			(*nrComp)++;
			if(v[j]>v[j+1])
			{
				(*nrAtr)++;
				
				int x=v[j];
				v[j]=v[j+1];
				v[j+1]=x;
				sortat=0;
			}
		}
	}
	if(nrOp)
		*nrOp=(*nrComp)+(*nrAtr);
}

void ins_sort(int v[],int n,int *nrAtr=NULL,int *nrComp=NULL,int *nrOp=NULL)
{
	if(nrAtr && nrComp)
	{
		*nrAtr=0;
		*nrComp=0;
	}

	for(int i=1;i<n;i++)
	{
		int x=v[i];
		int j=0;
		(*nrComp)++;
		while(v[j]<x)
		{
			
			j+=1;
		}
		for(int k=i;k>j;k--)
		{
			(*nrAtr)++;	
			v[k]=v[k-1];
		}
		(*nrAtr)++;
		v[j]=x;
	}
	if(nrOp)
		*nrOp=*nrComp+*nrAtr;
}
void sel_sort(int *v,int n,int *nrAtr=NULL,int *nrComp=NULL,int *nrOp=NULL)
{
	int minx,minj;


	if(nrAtr && nrComp)
	{
		*nrAtr=0;
		*nrComp=0;
	}

	for(int i=0;i<n;i++)
	{
		minj=i;
		minx=v[i];
		for(int j=i+1;j<n;j++)
		{
			(*nrComp)++;
			if(v[j]<minx)
			{
				(*nrAtr)++;
				minj=j;
				minx=v[j];
			}
		}
		(*nrAtr)++;
		v[minj]=v[i];
		v[i]=minx;
	}
	if(nrOp)
		*nrOp=*nrComp+*nrAtr;
}

int main(void){
	int v[MAX_SIZE],v2[MAX_SIZE];
	FILE *f,*f1,*f2;
	int nrOp,nrAtr,nrComp,nrOp1,nrAtr1,nrComp1,nrOp2,nrAtr2,nrComp2,nrOp3,nrAtr3,nrComp3;


	//----------------------------Cazul favorabil = cand numerele sunt sortate crescator -------------------------
/*	f=fopen("cfav_op.csv","w");
	fprintf(f,"Nr operatii (=suma atribuirilor si comparatiilor) in cazul favorabil\n"); 
	fprintf(f,",nrOpBub,nrOpIns,nrOpSel\n");
	for(int i=100;i<=10000;i+=100)
	{
		FillRandomArray(v,i,1,100000,true,1); //am folosit functia FillRandomArray din biblioteca Profiler.h ca sa pun in vector numere sortate crescator
											// ultimul parametru : int sorted=1 => nr sunt sortare crescator si nu se repeta pt ca penultimul param : bool unique=true
		memcpy(v2,v,sizeof(v));   // copiez cate 100/200/.../10000 de elem = sizeof(v) din vectorul v in vectorul v2
		buble_sort(v2,i,&nrAtr,&nrComp,&nrOp); // sortez 100/200/.../10000 de numere cu functia buble_sort
		int nrOp1=nrOp; // nrOp1 = nr operatiilor pt 100 de elem , apoi pt 200 elem , etc.
		memcpy(v2,v,sizeof(v));   
		ins_sort(v2,i,&nrAtr,&nrComp,&nrOp); 
		int nrOp2=nrOp;
		memcpy(v2,v,sizeof(v));
		sel_sort(v2,i,&nrAtr,&nrComp,&nrOp);
		int nrOp3=nrOp;
		fprintf(f,"%d,%d,%d,%d\n",i,nrOp1,nrOp2,nrOp3); // scriu in fisier nrOp1 pt buble_sort , nrOp2 pt ins_sort si nrOp3 pt sel_sort + i=pt cate numere am calculat nr operatiilor efectuate
	}
	fclose(f);*/


	//----------------------------Cazul defavorabil = cand numerele sunt sortate descrescator -------------------------
	f=fopen("cdef_op.csv","w");
	fprintf(f,"Nr operatii (=suma atribuirilor si comparatiilor) in cazul cel mai defavorabil\n");
	fprintf(f,",nrOpBub,nrOpIns,nrOpSel\n");
	for(int i=100;i<=1000;i+=100)
	{
		FillRandomArray(v,i,1,100000,true,2); //am folosit functia FillRandomArray din biblioteca Profiler.h ca sa pun in vector numere sortate descrescator
		// ultimul parametru : int sorted=2 => nr sunt sortate descrescator si nu se repeta pt ca penultimul param : bool unique=true
		memcpy(v2,v,sizeof(v));
		buble_sort(v2,i,&nrAtr,&nrComp,&nrOp);
		int nrOp1=nrOp;
		memcpy(v2,v,sizeof(v));
		ins_sort(v2,i,&nrAtr,&nrComp,&nrOp);
		int nrOp2=nrOp;
		memcpy(v2,v,sizeof(v));
		sel_sort(v2,i,&nrAtr,&nrComp,&nrOp);
		int nrOp3=nrOp;
		fprintf(f,"%d,%d,%d,%d\n",i,nrOp1,nrOp2,nrOp3);
	}
	fclose(f);
	

	//----------------------------Cazul mediu = cand numerele sunt random -------------------------
/*	f=fopen("cmed_op.csv","w");
	f1=fopen("cmed_comp.csv","w");
	f2=fopen("cmed_atr.csv","w");
	fprintf(f,"Nr operatii (=suma atribuirilor si comparatiilor) in cazul mediu\n");
	fprintf(f,",nrOpBub,nrOpIns,nrOpSel\n");
	fprintf(f1,"Numarul comparatiilor in cazul mediu\n");
	fprintf(f1,",CompBub,nrCompIns,nrCompSel\n");
	fprintf(f2,"Numarul atribuirilor in cazul mediu\n");
	fprintf(f2,",nrAtrBub,nrAtrIns,nrAtrSel\n");
	for(int i=100;i<=1000;i+=100)
	{
		nrAtr1=0; // pt buble_sort
		nrComp1=0;
		nrOp1=0;
		nrAtr2=0; // pt ins_sort
		nrComp2=0;
		nrOp2=0;
		nrAtr3=0; // pt sel_sort
		nrComp3=0;
		nrOp3=0;
		for(int j=5;j>0;j--) // conform cerintei , voi repeta masuratorile de 5 ori
							// numerele din vector sunt random asa ca numarul operatiilor realizate pt sortare poate fi diferit pt fiecare sir de numere
		{
			FillRandomArray(v,i,1,100000);  //am folosit functia FillRandomArray din biblioteca Profiler.h ca sa pun in vector numere random
											//nu am mai scris ultimul parametru : int sorted pt ca el e 0 by default => nr sunt random si se repeta pt ca penultimul param : bool unique= false by default
			memcpy(v2,v,sizeof(v)); // copiez cate 100/200/.../10000 de elem = sizeof(v) din vectorul cu nr random in alt vector
			buble_sort(v2,i,&nrAtr,&nrComp,&nrOp); // cu buble_sort sortez numerele random 
			nrAtr1+=nrAtr;
			nrComp1+=nrComp;
			nrOp1+=nrOp;
			memcpy(v2,v,sizeof(v)); // in v2 voi avea 100/200/.../10000 de numere random
			ins_sort(v2,i,&nrAtr,&nrComp,&nrOp);  // numerele random din v2 le sortez cu fc ins_sort
			nrAtr2+=nrAtr;
			nrComp2+=nrComp;
			nrOp2+=nrOp;
			memcpy(v2,v,sizeof(v)); 
			sel_sort(v2,i,&nrAtr,&nrComp,&nrOp);
			nrAtr3+=nrAtr;
			nrComp3+=nrComp;
			nrOp3+=nrOp;
		}
		nrAtr1/=5; // media rezultatelor
		nrComp1/=5;
		nrOp1/=5;
		nrAtr2/=5;
		nrComp2/=5;
		nrOp2/=5;
		nrAtr3/=5;
		nrComp3/=5;
		nrOp3/=5;
		fprintf(f,"%d,%d,%d,%d\n",i,nrOp1,nrOp2,nrOp3);
		fprintf(f1,"%d,%d,%d,%d\n",i,nrComp1,nrComp2,nrComp3);
		fprintf(f2,"%d,%d,%d,%d\n",i,nrAtr1,nrAtr2,nrAtr3);
	}
	fclose(f);
	fclose(f1);
	fclose(f2);
	*/
	getch();
	return 0;
}
