/*Interpretarea graficelor
***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***
Cazul favorabil:
	- atribuiri:	* Bubble 0 atrbuiri, cel mai eficient in acest caz
					* Selectie cele mai multe atrbuiri, mai putin eficient
					* Insertie atrbuire medii(in jur de 2n)
	- comparatii:	* Bubble cele mai putin comparatii
					* Selectie 
					* Insertie cele mai multe comparatii
	- atribuire+comparatii:	* Bubble n
							* Selectie n^2
							* Insertie n^2
Concluzii cazul favorabil: Bubble este cel mai eficient, cu complexitate O(n), Selectia si Insertia avand complexitatea O(n^2).

Cazul defavorabil si mediu_statistic au ***ANONIM*** aceasi comportare:
	- atribuiri:	* Bubble cele mai multe atribuiri
					* Seleectie cele mai putine atribuiri
					* Insertie numar de atribuiri mediu intre Bubble si Sortare
	- comparatii:	* Bubble cele mai multe comparatii
					* Selectie 0
					* Insertie 0
	- atribuiri+comparatii:	* Bubble cele mai multe atribuiri+comparatii
							* Selectie cele mai putine atribuiri+comparatii
							* Insertie atribuiri+comparatii medii
Concluzii cazul defavorabil si mediu_statistic: Bubble este cel mai putin optim.
Toate metodele au aceasi eficienta O(n^2).

	*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <cstdlib>
#include <time.h>

int atrb=0, compb=0, atri=0, compi=0, atrs=0, comps=0;

void bubble(int a[],int n)
{
	int ok = 0;
	int x,i,k=0;
	while(!ok)
	{
		ok = 1;
		for(i=0; i<n-k-1; i++)
		{
		 compb++;	
		if(a[i]>a[i+1])
		{
			atrb+=3;
			x=a[i];
			a[i]=a[i+1];
			a[i+1]=x;
			ok = 0;
			
		}
		}
		k++;		
	}
}
void selectie(int a[],int n)
{
	int min, i, imin, j;
	for(i=0; i<n-1; i++)
	{ 
		
		min=a[i];
		imin=i;
		for(j=i+1; j<n; j++)
		{
			comps++;
			if(a[j]<min)
			{
				
				min=a[j];
				imin=j;
			}

		 }
		a[imin]=a[i];
		a[i]=min;
		atrs+=3;
	}
}
void insertie(int a[],int n)
{
	int i, j, k, x;
	for(i=1; i<n; i++)
	{
		x=a[i];
		j=0;
		atri++;
		compi++;
		while(a[j]<x)
			j++;
			compi++;
		for(k=i; k>=j; k--)
		{
			atri++;
			a[k]=a[k-1];
		}
		a[j]=x;
		atri++;
	}

}
void afisare(int n,int v[])
{
	for(int i=0;i<n;i++)
		printf("%d ",v[i]);
}

int main()
{
	int n, a[10000], b[10000], c[10000];
	//FILE *f=fopen("favorabil.csv","w");
	//FILE *f=fopen("defavorabil.csv","w");
	FILE *f=fopen("mediu_statistic.csv","w");

	for(n=100; n<10000; n+=100)
	{
		atrb=0;
		compb=0;
		atrs=0;
		comps=0;
		atri=0;
		compi=0;
		for(int s=0; s<=5; s++)
		{
			srand(time(NULL));

			for(int k=0; k<n; k++)  //generam sirul
			{
				//a[k]=k; //caz favorabil
				//b[k]=k;
				//c[k]=k;
				//a[k]=n-k; //caz defavorabil
				//b[k]=n-k;
				//c[k]=n-k;
				a[k]=rand(); //statistic
				b[k]=rand();
				c[k]=rand();
			}
		srand(1);
		bubble(a,n);
		selectie(a,n);
		insertie(a,n);
	
		}
	fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n, atrb/5, compb/5, atrs/5, comps/5, atri/5, compi/5,(atrb+compb)/5, (atrs+comps)/5, (atri+compi)/5);

	//fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n, atrb, compb, atrs, comps, atri, compi,atrb+compb, atrs+comps, atri+compi);
	}
	getch();
	
}