/* ***ANONIM*** ***ANONIM*** - ***ANONIM***  
	grupa ***GROUP_NUMBER***

	Eficienta acestui program este O(n * logk). Am obtinut aceasta eficienta parcurgand k liste de lungime n aceasta operatie avand eficienta O(n * k).
	Pentru noi, valoarea k este cunoscuta, adica este o constanta, iar din aceasta rezulta ca eficienta parcurgerii a k liste de lungime n este O(n).
	Operatiile heap-ului, pop(), push() si heap() au eficienta O(logk), fiecare. Pentru a avea eficienta finala a programului O(n * logk) folosim
	scrierea si citirea in/din lista rezultat, aceste operatii avand eficienta O(1) si doi poantori indicand capul si sfarsitul listei rezultat.
	
	Mentionez faptul ca nu am putut sa ajung la 10000 de elemente deoarece am avut o eroare care spunea ca nu pot sa scriu la adresa 
	0x0000000, de aceea am ajuns doar la 6000.

*/


#include <time.h>
#include <list>
#include <conio.h>
#include <stdlib.h>

int na,nc;	//numarr de atribuiri, numar de comparatii

typedef struct struct_NOD
{
	int key;
	struct_NOD *urm,*prec;
}NOD;

struct element{
	int valoare; 
	int index;	
};

element Heap[1001];

NOD *cap[1000];
NOD *coada[1000];
NOD *capul, *coadaaa;
NOD *prim, *ultim;


int parinte(int i){
	return i/2;
}
int dreapta(int i){
	return 2*i+1;
}
int stanga(int i){
	return 2*i;
}

//functia reconstituie heap-ul
void reconstructieHeap(int i, int n) 
{
	int min,d,s;   
	s=stanga(i);
	d=dreapta(i);
	if((s <= n) && Heap[s].valoare < Heap[i].valoare){
		min = s;
	}
	else{
		min = i;
	}
	if((d <= n) && (Heap[d].valoare < Heap[min].valoare)){
		min=d;
	}
	nc+=2;
	if (min != i) {
		element temp;
		temp = Heap[i];
		Heap[i]=Heap[min];
		Heap[min]=temp;
		reconstructieHeap(min,n);
		na+=3;
	}

}

//functia care initializeaza heap-ul
void initializeazaHeap(int *dim){
	*dim=0;
}

//functia care extrage valoare minima din heap
element scoateDinHeap(int *dim){
	element e;
	if(*dim > 0){
		e=Heap[1];
		Heap[1]=Heap[*dim];
		*dim = *dim-1;
		na+=2;
		reconstructieHeap(1, *dim);
	}
	return e;
}

//functia care adauga in heap
void puneInHeap(int *dim,int value,int ind){	 
	*dim = *dim+1;
	int n;
	n = *dim;
	na += 1;
	Heap[*dim].index=ind;
	Heap[*dim].valoare=value;
	while (n >= 1 && Heap[n].valoare < Heap[parinte(n)].valoare){  
		nc+=1;
		na+=3;
		element temp;
		temp = Heap[n];
		Heap[n] = Heap[parinte(n)];
		Heap[parinte(n)] = temp;
		n = parinte(n);

	}
	nc += 1;
}

//functia care citeste elementele listei
NOD* citeste(NOD **ccap,NOD **ccoada)
{
	NOD *p;
	if(*ccap == NULL)
	{
		return NULL;
	}
	p = *ccap; 
	*ccap = p->urm;
	return p;

}


//functia care creaza o lista vida
void createEmptyLists (NOD **prim, NOD **ultim)
{
	*prim = 0;
	*ultim = 0;
}

//functia care insereaza in lista
void insert(NOD **ccap,NOD **ccoada,int val)
{
	NOD *p=(NOD *) malloc(sizeof(NOD));

	p->key=val;

	if((*ccap) == NULL){

		*ccap=p;
		*ccoada=p;
	}
	else {

		(*ccoada)->urm=p;
		*ccoada=p;
	}
	p->urm=NULL;
}

//functia care sterge din lista
void deleteList(NOD **ccap,NOD **ccoada, NOD **p){
	if ((*p)->prec!=NULL){
		(*p)->prec->urm=(*p)->urm;
	}
	else{
		(*p)=(*p)->urm;
	}
	if ((*ccap)->urm!=NULL){
		(*p)->urm->prec=(*p)->prec;
	}
}

//functia care afiseaza listele
void afis(NOD *p)
{
	while(p!=NULL){
		printf("%d ,",p->key);
		p=p->urm;
	}
}


void mergeKLists(NOD **headd, NOD **taill, int k, int *dim)
{

	NOD *p;
	initializeazaHeap(dim);
	createEmptyLists(&capul, &coadaaa);
	for(int i = 0; i < k; i++){	
		na+=1;
		p = citeste(&(headd[i]), &(taill[i]));
		puneInHeap(dim, p->key, i);
	}
	
	while(*dim > 0){
		element el;
		el = scoateDinHeap(dim); 
		insert(&capul, &coadaaa, el.valoare);
		p = citeste(&(headd[el.index]), &(taill[el.index]));
		if(p != NULL){
			puneInHeap(dim, p->key, el.index);
		}
		na+=2;
		nc+=1;
	}
}

void creazaLista(int i,int n)
{
	int adaos = 10;
	int a = 1;
	
	for (int i = 0; i < n; i++){
		a = rand() % adaos + a;
		insert(&(cap[i]), &(coada[i]), a);
	}
}
int main()
{   
	srand((unsigned)time(NULL));
	
	//test
	int v1[21]={15, 25, 35, 45, 55, 65, 75, 85, 95, 105, 115, 125, 135, 145, 155, 165, 175, 185, 195, 205};
	int v2[21]={20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400};
	int v3[21]={30, 50, 70, 90, 110, 130, 150, 170, 190, 210, 230, 250, 270, 290, 310, 330, 350, 370, 390, 410};

	for(int j=0; j<3; j++){
		int v[21]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		if(j==0){ 
			memcpy(v, v1, (20) * sizeof(int));
		}
		if(j==1){
			memcpy(v, v2, (20) * sizeof(int));
		}
		if(j==2){ 
			memcpy(v, v3, (20) * sizeof(int));
		}
		for(int i=0; i<20;i++){
		insert(&(cap[j]), &(coada[j]),v[i]);
		}
		printf("\nLista %d:", j);
		afis(cap[j]);
		printf("\n");
	}
	int size = 20;
	mergeKLists(cap, coada, 3,&size);
	printf("\nLista sortata este: ");
	afis(capul);

	//cerinta a
	createEmptyLists(cap, coada);
	int k;

	FILE *pf1=fopen ("cerinta_a.csv","w");
	fprintf(pf1,"n, k10, k20, k100\n");
	
	for(int n=100;n<6000;n+=100){
		for(int i=0;i<3;i++){
			if(i==0){ 
				k=5;
			}
			if(i==1){
				k=10;
			}
			if(i==2){ 
				k=100;
			}
			na=0,nc=0;
			for(int j=0;j<k;j++){
			//lungime=n;
			printf("\n n=%d   k=%d\n", n, k);
			creazaLista(j,n);
			}
		
			mergeKLists(cap, coada, k, &size);
			switch (k)
			{
				case 5:
						fprintf (pf1, "%d, %d,", n, na+nc); 
					
						break;
				case 10:
						fprintf (pf1, "%d, ", na+nc);
					
						break;
				case 100:
						fprintf (pf1, "%d \n", na+nc);
						
						break;
			}
		
		}
	}


	//cerinta b
	createEmptyLists(cap,coada);

	FILE *pf2;

	pf2 = fopen("cerinta_b.csv","w");
	fprintf(pf2,"k, n, na+nc\n");
	createEmptyLists(cap, coada);
	
	int n =10000;
	for(int k=10; k<=500; k+=10){
			
		for(int j=0;j<k;j++)	
			{


			//lungime=n;
			printf("\n n=%d   k=%d\n", n, k);
			creazaLista(j, n/k);
			
			}
		
		na=0,nc=0;
		mergeKLists(cap, coada, k, &size);
		fprintf (pf2,"%d , %d , %d\n", k, n, na+nc);
		
		
	}
	fclose(pf1);
	fclose(pf2);	
}