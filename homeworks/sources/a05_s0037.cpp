#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <time.h> 
#include<conio.h>

#define N 9973

int T[N];
int op_gasire,op_negasire;



//int hash_aux(int k)
//{
//	int l,m;
//	double n,A;
//	A=(sqrt(5.0)-1)/2;
//	m=(int)(k*A);
//	n=k*A-m;
//	l=(int) (N*n);
//	return l;
//}

//functia de hash
//quadratic probing
//h(k,i)=(h_aux(k) +c1*i + c2*i*i)mod m
//c1,c2-constante
int hash(int k, int i)
{
	int a,c1=7,c2=17;
	a=(k%N +c1*i + c2*i*i)%N;
	return a;
}
 ///functia de inserare 
int Hash_Insert(int *T, int k)   
{
   int i=0,j;
   do{
       j=hash(k,i);
       if(T[j]==NULL)
        {
           T[j]=k;
		   //printf("poz %d \n",j);
	       return j;
        }
       else i++;

   }while(i!=N);

return -1;
}

// functia de cautare 
int Hash_Search(int *T,int k)
{
  int i=0,j,nr=0;

   do{
      j=hash(k,i);
      nr++;
       if(T[j]==k)
        {
			 op_gasire=nr; //daca numarul a fost gasit atunci va retuna numarul de operatii efectuate pana la gasirea acestuia
			 //printf("%dpozg: %d ",k,j );
			 return j;
        }
        else i++;
   }while((T[j]!=NULL)&&(i!=N));

op_negasire=nr;// daca numarul nu a fost gasit atunci va returna numarul total de opartii efectuate
return -1;
  }

void main()
{
    FILE *pf;
	int i,j,n,k,l,m,max1,max2;
	int elem_max=0,mediu2,mediu1;
	int medie_gas,medie_neg;
	double ump[5]={0.8,0.85,0.9,0.95,0.99};// fact umplere
	int a[10000];

 pf=fopen("hash.csv","w");
 fprintf(pf,"factor umplere ,med_g ,max_g ,med_neg ,max_neg\n"); 

 srand(time(NULL));

for(i=0;i<5;i++)
{
	  n=ump[i]*N;//n=numarul de elemente ce vor fi introduse in tabela
      medie_gas=0;
	  medie_neg=0;
	  max1=0;// numarul maxim de operatii pt gasirea elementului in tabele
	  max2=0;// numarul maxim de operatii pt elemente neexistente in tabela
	  
    for(j=0;j<5;j++)//pentru fiecare valoarea a factorului de umplere se testeaza 5 cazuri diferite (mediere)
    {
		 for(m=0;m<N;m++) //reinit tabela
		    T[m]=NULL;

        
	     for(l=0;l<n;l++)
	     {
            k=rand(); 
			//if(l<n/2) //pun doar jumatate din elemente
				a[l]=k;
			if(k>elem_max) //elem max pt cautare numerele care nu se gasesc 
				elem_max=k;    
	        Hash_Insert(T,k);
			

	      }
		
		 //cautare elem existente(jumatate)
		 mediu1=0;
		 for(l=0;l<1500;l++)  
		  {
             op_gasire=0;
			 k=a[rand()%n];
			
			   Hash_Search(T,k);
				if(op_gasire>max1)
					max1=op_gasire; 
				mediu1=mediu1+op_gasire;
			 
	 	  }
		  medie_gas=medie_gas + mediu1/1500;//nr mediu operatii pentru gasire element
		 
		   //cautare elem inexistente(cealalta jumatate)
		  mediu2=0;
		  for(l=0;l<1500;l++)  
		  {    op_negasire=0;   

			  k=rand()+ elem_max;
			  Hash_Search(T,k);
			  mediu2=mediu2+op_negasire;
	          if(op_negasire>max2)
				  max2=op_negasire; 						
		  }
		  medie_neg=medie_neg+mediu2/1500;
		
	  }	
    if(medie_gas>=5) 
		medie_gas=medie_gas/5;
    fprintf(pf,"%1.2f , %d , %d ,%d ,%d \n",ump[i],medie_gas,max1,medie_neg/5,max2); 
	}

 fclose(pf);
 printf("gata");


 //~~~~~~~~~~~~caz particular ~~~~~~
/*	int a[10000];
	n=ump[0]*N;
	printf("vector \n");
	 srand(time(NULL));

	     for(l=0;l<n;l++)
	     {
            k=rand(); 
			printf("elem %d ",k);
			a[l]=k;
			if(k>elem_max) //elem max pt cautare numerele care nu se gasesc 
				elem_max=k;    
	        Hash_Insert(T,k);
			
	      }
		 printf("\n");
		 
		  for(l=0;l<n/2;l++)  
		  {
			 k=a[l];
			 Hash_Search(T,k);
				
	 	  }
		  */

 getch();

}

