#include "Profiler.h"
#include<iostream>
#include<math.h>
#include<conio.h>

#define MAX 10000
using namespace std;

Profiler profiler("Heapsort vs Quicksort");

int size,n;

// Second print method
void PrettyPrint(int A[],int n){
	double  height,i,j,k,nr=0,ok=1,level;

	do{
	
		if(n>pow(2,nr) && n<=pow(2,nr+1))
			ok=0;
		nr++;
		
	}while(ok!=0);
	cout<<"                 ";
	height=nr;nr=0;
	level=height;
	for(i=1;i<=height;i++){
		for(j=pow(2,nr);j<pow(2,nr+1);j++){
			if(j==pow(2,nr)){
				for(k=1;k<=pow(2,level-1)-1;k++) cout<<" ";
			}
			if(j<=n){
			cout<<A[(int)j];
			for(k=1;k<=pow(2,level)-1;k++) cout<<" ";
			}
		}cout<<"\n\n                 ";
	nr++;level--;
	}


}

//Heapify function - metoda bottom-up
void heapify(int A[],int i,int n){
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
	
	profiler.countOperation("CompHeapsort",n);
	if(l<=size && A[l]>A[i])
	{largest=l;}
	else 
	{largest=i;}

	profiler.countOperation("CompHeapsort",n);
	if(r<=size && A[r]>A[largest])
	{largest=r;}


	if(largest!=i)
	{
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		profiler.countOperation("AssHeapsort",n,3);
		heapify(A,largest,n);
	}
	

}


void BuildMaxHeap(int A[],int n){
	size=n;
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}

void HeapSort(int A[]){
	int i,aux;
	BuildMaxHeap(A,n);
	for(i=n;i>=2;i--){
		aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		profiler.countOperation("AssHeapsort",n,3);
		size--;
		heapify(A,1,n);
	}

}



void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
	profiler.countOperation("AssQuicksort",n);
      
      while (i <= j) {
				profiler.countOperation("CompQuicksort",n);
            while (arr[i] < pivot){ 
				i++;
				profiler.countOperation("CompQuicksort",n);
			}
		profiler.countOperation("CompQuicksort",n);
            while (arr[j] > pivot)
				{ j--;
				profiler.countOperation("CompQuicksort",n);}

            if (i <= j) {
				profiler.countOperation("AssQuicksort",n,3);
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      }
 
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}


void main(){

int A[MAX],B[MAX],C[MAX];
int i,j,op;

cout<<"0.Exit! \n1.Manual Input. \n2.Run automatic(random)!\n3.Worst case.\nType your coice:";
cin>>op;

switch(op){
	case 0 :return; break;
	case 1: cout<<"Insert n=";cin>>n;
			for(i=1;i<=n;i++)
			{cout<<"A["<<i<<"]=";cin>>A[i];}
			
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

				quickSort(B,1,n);
				HeapSort(C);

				PrettyPrint(A,n);
				cout<<"\n\n";
				PrettyPrint(B,n);
				cout<<"\n\n";
				PrettyPrint(C,n);

			break;
	case 2:	
		
		for(i=100;i<10000;i=i+100){
			FillRandomArray(A,i,0,10000);
				n=i;
			for(int k=1;k<=5;k++){
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

				quickSort(B,1,n);
				HeapSort(C);

		}}

				break;

	case 3:for(i=100;i<=10000;i=i+100){
			FillRandomArray(A,i,0,10000,false,2);
			n=i;
		
		   	for(j=1;j<=i;j++)
				{B[j]=A[j];C[j]=A[j];}

					quickSort(B,1,n);
				HeapSort(C);
		   }
		break;
	default: cout<<"Invalid input"; return;
		break;
}


profiler.addSeries("Heapsort","CompHeapsort","AssHeapsort");
profiler.addSeries("Quicksort","CompQuicksort","AssQuicksort");

profiler.createGroup("Comparisons","CompHeapsort","CompQuicksort");
profiler.createGroup("Assignments","AssHeapsort","AssQuicksort");
profiler.createGroup("Sum","Heapsort","Quicksort");

if(op==2 || op==3)
	profiler.showReport();


 /*cout<<"Insert n=";cin>>n;
		for(i=1;i<=n;i++)
			{cout<<"A["<<i<<"]=";cin>>A[i];}
			
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

			cout<<"\nUnsorted: \n\n";
			PrettyPrint(A,n);
			QuickSort(B,1,n);
			HeapSort(C);
			cout<<"\nSorted with Quicksort: \n\n";
			PrettyPrint(B,n);
			cout<<"\nSorted with Heapsort: \n\n";
			PrettyPrint(C,n);*/

getch();
}


//2*N*log(N)-comparatii
//worst case and average case-O(N*(log(N))
//heap sort nu e stabil