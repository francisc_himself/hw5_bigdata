#include <stdlib.h>
#include <stdio.h>

int operatii;

typedef struct NOD
{
	int key;
	int rank;
	struct NOD *parent;
}Nod;

Nod *noduri[10001];
  
Nod * MakeSet(int x)
{
	Nod *nod;
	nod = (Nod *)malloc(sizeof(Nod));
	noduri[x] = nod;
	nod->key = x;
	nod->parent = nod;
	nod->rank = 0;
	
	operatii+=4;
	return nod;
}

Nod *Find(int x)
{
	if(noduri[x]->parent != noduri[x])
	{
		operatii++;
		noduri[x]->parent = Find(noduri[x]->parent->key);
	}
	operatii++;
	return noduri[x]->parent;
}
 
void Union(int x, int y)
{
	Nod *xRoot, *yRoot;
	xRoot = Find(x);
	yRoot = Find(y);

	operatii+=3;
	if(xRoot->key == yRoot->key)
	{
		return;
	}
	
	operatii++;
	if(xRoot->rank < yRoot->rank)
	{
		
		xRoot->parent = yRoot;
	}
	else if(xRoot->rank > yRoot->rank)
	{
		operatii+=2;
		yRoot->parent = xRoot;
	}
	else
	{
		operatii+=2;
		yRoot->parent = xRoot;
		xRoot->rank = xRoot->rank + 1;
	}
}

void CreeazaMuchii(int maxNr, int m)
{
	int x,y,i;
	for(i = 1; i<=m; i++)
	{
		operatii+=2;
		x = rand()%maxNr;
		y = rand()%maxNr;
		while(x==y)
		{
			y = rand()%maxNr;
		}
		Union(x,y);
	}
}

int main()
{
	int m, i, x, y;
	FILE *f;
	f=fopen("result.csv","w");
	
	for(m = 10000; m<=60000;m+=100)
	{
		operatii=0;
		for(int i = 0; i<=10000;i++)
		{
			noduri[i] = NULL;
		}
		for(i = 0; i<=9999;i++)
		{
			MakeSet(i);
		}
		CreeazaMuchii(10000,m);
		fprintf(f,"%d,%d\n",m,operatii);
	}
	fclose(f);
	
	return 0;
}