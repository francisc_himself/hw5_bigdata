#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"
#include "stddef.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 4 - Merging k ordered lists
/***************************************************/

/**************************************************************************************
/
/							OBSERVATIONS, INTERPRETATIONS
/
/	1. When merging k lists and varying the number of elements, we can observe that the 
/	   number of operations is directly proportional with the number of lists to be 
/	   merged. This happens, because of the number of comparisons and pops and 
/	   heapifies. The more the lists, the bigger the heap is.
/	2. When merging k lists and varying the size of the merged list, we can see a
/	   linear behavior. This happens, because the worst case behavior of the algorithm
/	   is O(nlgk) and n is varying while k is constant.
/	3. When merging k lists and varying the number of lists we can observe on the chart
/	   a logarithmic behavior. This happens, because in O(nlgk) n is constant and k
/	   varies.
/
/**************************************************************************************/

#define MAX_NR 10000
#define inf 999999
#define MAX_HEAP 500

#define left(node) (2*node)
#define right(node) (2*node+1)
#define parent(node) (node/2)

int a[MAX_NR];

//size of a single sorted array.
//int size;
int size = 5;
int nbLists;
int heapSize;

//the profiler
Profiler profiler("MergeKArrays");

//define a node
typedef struct nodetype
{
	int belongsTo;
	int value;
	struct nodetype *next;
}NodeT;

typedef NodeT *NodePtr;
NodePtr first[MAX_HEAP+1];
NodePtr last[MAX_HEAP+1];

#define SWAP(x,y) NodePtr t;t=x;x=y;y=t;

NodePtr heap[MAX_HEAP];

//some forward declarations
void append(int value, int k);
void append(NodePtr node, int k);

//heap operations
void heapify(int index)
{
	int smallest = 0;
	int left = left(index);
	int right = right(index);
	switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeComparisonsK5",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeComparisonsK10",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeComparisonsK100",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	default:
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	}
	if((left <= heapSize) && (heap[left]->value < heap[index]->value))
	{
		smallest = left;
	}
	else
	{
		smallest = index;
	}
	switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeComparisonsK5",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeComparisonsK10",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeComparisonsK100",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	default: 
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	}
	if((right <= heapSize) && (heap[right]->value < heap[smallest]->value))
	{
		smallest = right;
	}
	if (smallest != index)
	{
		switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeAssignmentK5",size,3);
		//profiler.countOperation("mergeAssignment",nbLists,3);
		break;
	case 10:
		profiler.countOperation("mergeAssignmentK10",size,3);
		//profiler.countOperation("mergeAssignment",nbLists,3);
		break;
	case 100:
		profiler.countOperation("mergeAssignmentK100",size,3);
		//profiler.countOperation("mergeAssignment",nbLists,3);
		break;
	default: 
		//profiler.countOperation("mergeAssignment",nbLists,3);
		break;
	}
		SWAP(heap[index],heap[smallest]);
		heapify(smallest);
	}
}

NodePtr pop()
{
	NodePtr min = 0;
	if (heapSize < 1)
	{
		printf("Error. Heap underflow.\n");
		return NULL;
	}
	min = heap[1];
	switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeAssignmentK5",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeAssignmentK10",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeAssignmentK100",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	default: 
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	}
	heap[1] = heap[heapSize];
	heapSize--;
	heapify(1);
	return min;
}

void increaseKey(int index, NodePtr node)
{
	switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeComparisonsK5",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeComparisonsK10",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeComparisonsK100",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	default:
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	}
	while((index > 1) && (heap[parent(index)]->value > node->value))
	{
		heap[index] = heap[parent(index)];
		index = parent(index);
		switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeComparisonsK5",size,1);
		profiler.countOperation("mergeAssignmentK5",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeComparisonsK10",size,1);
		profiler.countOperation("mergeAssignmentK10",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeComparisonsK100",size,1);
		profiler.countOperation("mergeAssignmentK100",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	default: 
		//profiler.countOperation("mergeAssignment",nbLists,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	}
	}
	switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeAssignmentK5",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeAssignmentK10",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeAssignmentK100",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	default:
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	}
	heap[index] = node;
	return;
}

void push(NodePtr node)
{
	if(node != NULL)
	{
	heapSize++;
	switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeAssignmentK5",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeAssignmentK10",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeAssignmentK100",size,1);
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	default: 
		//profiler.countOperation("mergeAssignment",nbLists,1);
		break;
	}
	heap[heapSize] = NULL;
	increaseKey(heapSize,node);
	}
}

void merge(int nbLists)
{
	NodePtr temp;
	heapSize = 0;
	for(int i = 0; i < nbLists; i++)
	{
		push(first[i]);
	}
	while(heapSize != 0)
	{
		NodePtr node = pop();
		append(node,MAX_HEAP);
		first[node->belongsTo] = first[node->belongsTo]->next;
		if(first[node->belongsTo] != NULL)
		{
			push(first[node->belongsTo]);
		}
		temp = NULL;
	}
}

void initializeLists(int nbLists)
{
	for(int i = 0; i < nbLists; i++)
	{
		first[i] = NULL;
		last[i] = NULL;
	}
}

//to generate one sorted list
void generateList(int size, int k)
{
	FillRandomArray(a,size,-2000,5000,false,1);
	for(int i = 0; i < size; i++)
	{
		append(a[i],k);
	}
}

//add a freshly created element to the end of the specified list
void append(int value, int k)
{
	NodePtr newNode = (NodeT*)malloc(sizeof(NodeT));
	if(newNode != NULL)
	{
		newNode->value = value;
		newNode->belongsTo = k;
		newNode->next = NULL;
		if(first[k] == NULL)
		{
			first[k] = newNode;
			last[k] = newNode;
		}
		else
		{
			last[k]->next = newNode;
			last[k] = newNode;
		}
	}
}

//overloaded version of append, an already existing element added to the end of the list
void append(NodePtr node, int k)
{
	if(node != NULL)
	{
		switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeComparisonsK5",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 10:
		profiler.countOperation("mergeComparisonsK10",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	case 100:
		profiler.countOperation("mergeComparisonsK100",size,1);
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	default:
		//profiler.countOperation("mergeComparisons",nbLists,1);
		break;
	}
		if(first[k] == NULL)
		{
			switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeAssignmentK5",size,2);
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	case 10:
		profiler.countOperation("mergeAssignmentK10",size,2);
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	case 100:
		profiler.countOperation("mergeAssignmentK100",size,2);
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	default: 
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	}
			first[k] = node;
			last[k] = node;
		}
		else
		{
			switch(nbLists)
	{
	case 5:
		profiler.countOperation("mergeAssignmentK5",size,2);
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	case 10:
		profiler.countOperation("mergeAssignmentK10",size,2);
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	case 100:
		profiler.countOperation("mergeAssignmentK100",size,2);
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	default: 
		//profiler.countOperation("mergeAssignment",nbLists,2);
		break;
	}
			last[k]->next = node;
			last[k] = node;
		}
	}
}

//print the list
void printList(int k)
{
	NodePtr node = first[k];
	if(node == NULL)
	{
		printf("List is empty.\n");
	}
	else
	{
		while(node != NULL)
		{
			printf("%d ",node->value);
			if(node->next!=NULL)
			{
				printf("-> ");
			}
			node = node->next;
		}
		printf("\n\n");
	}
}

//deletes the lists (frees memory)
void deleteLists(int nbLists)
{
	for(int i = 0; i < nbLists; i++)
	{
		NodePtr node;
		node = first[i];
		while(node != NULL)
		{
			NodePtr temp = node;
			node = node->next;
			free(temp);
		}
	}
	initializeLists(nbLists); //to be sure that lists can be reused
}

//used for testing min-heap
void printHeap(int node, int level)
{
    if (node <= size && node > 0)
	{
        printHeap(right(node), level + 1 );
        for (int i = 0; i <= level; i++ )
            printf( "       " ); /* for nice listing */
        printf( "%d \n", heap[node]);
        printHeap(left(node), level + 1 );
    }
}

void main()
{
	
	//PROVING CORRECTNESS
	int k = 4;
	initializeLists(k);
	for(int i = 0; i < k; i++)
	{
		generateList(size,i);
		printList(i);
	}
	merge(k);
	printList(MAX_HEAP);
	getch();
	

	/*
	//TESTING IN AVERAGE CASE
	
	
	//VARYING N - THE SIZE OF LISTS
	//for k1 = 5
	nbLists = 5;
	initializeLists(nbLists);
	for(size = 100; size <=10000; size+=100)
	{
		for(int i = 0; i < nbLists; i++)
		{
			generateList(size/nbLists,i);
		}
		merge(nbLists);
		deleteLists(nbLists);
		profiler.addSeries("mergeTotalK5","mergeComparisonsK5","mergeAssignmentK5");
		printf("%d\n",size);
	}
	
	//for k2 = 10
	nbLists = 10;
	initializeLists(nbLists);
	for(size = 100; size <=10000; size+=100)
	{
		for(int i = 0; i < nbLists; i++)
		{
			generateList(size/nbLists,i);
		}
		merge(nbLists);
		deleteLists(nbLists);
		profiler.addSeries("mergeTotalK10","mergeComparisonsK10","mergeAssignmentK10");
		printf("%d\n",size);
	}

	//for k2 = 100
	nbLists = 100;
	initializeLists(nbLists);
	for(size = 100; size <=10000; size+=100)
	{
		for(int i = 0; i < nbLists; i++)
		{
			generateList(size/nbLists,i);
		}
		merge(nbLists);
		deleteLists(nbLists);
		profiler.addSeries("mergeTotalK100","mergeComparisonsK100","mergeAssignmentK100");
		printf("%d\n",size);
	}
	profiler.createGroup("mergeComparisonsN","mergeComparisonsK5","mergeComparisonsK10","mergeComparisonsK100");
	profiler.createGroup("mergeAssignmentN","mergeAssignmentK5","mergeAssignmentK10","mergeAssignmentK100");
	profiler.createGroup("mergeTotalN","mergeTotalK5","mergeTotalK10","mergeTotalK100");
	profiler.showReport();
	*/

	/*
	//VARYING K - THE NR OF LISTS
	size = 10000;
	for(nbLists = 10; nbLists <= 500; nbLists += 10)
	{
		for(int i = 0; i < nbLists; i++)
		{
			generateList(size/nbLists,i);
		}
		merge(nbLists);
		deleteLists(nbLists);
		profiler.addSeries("mergeTotal","mergeComparisons","mergeAssignment");
		printf("%d\n",nbLists);
	}
	profiler.createGroup("mergeComparisons","mergeComparisons");
	profiler.createGroup("mergeAssignment","mergeAssignment");
	profiler.createGroup("mergeTotal","mergeTotal");
	profiler.showReport();*/
}