/****ANONIM*** ***ANONIM***
	***GROUP_NUMBER***
	Tema 1
	data: 3.14.2013

Au fost construite 3 .csv care contin tabelele sortarilor in cazurile
favorabil, defavorabil si mediu statistic.

In cazul favorabil:

	la atribuiri: Insertia este liniara, iar bule si selectia sunt patratice si aproape la fel
	la comparatii: Inserta si selectia sunt patratice si aproape la fel, iar bula este liniara
	la suma lor: bula este liniara, iar selectia si insertia sunt patratice

In cazul mediu statistic:

	toate sunt patratice

In cazul defavorabil:

	insertia la atribuiri este liniara, iar restul sunt patratice
*/


#include <fstream>
#include <iostream>

using namespace std;

ofstream g("best.csv");
ofstream h("mediu.csv");
ofstream w("worst.csv");

int sortat=0;
int i,a[10000],b[10000],l,aux,n,j;
int atr, comp;
int suma, sumc;

void bule(int v[], int n, int* a, int* c)
{
	sortat=0;
	while(sortat == 0)
	{

		sortat = 1;
		
		for(i=1;i<n;i++)
		{
			(*c)++;
			if(v[i]>v[i+1])
			{
				
				aux = v[i];
				v[i] = v[i+1];
				v[i+1] = aux;
				sortat = 0;
				(*a) += 3;
			}
		}
	}
}

void insertie(int v[], int n, int* a, int* c)
{
	int x;
	for(i=2;i<=n;i++)
	{
		x = v[i];
		(*a)=(*a)+1;
		j = 0;
		(*c)=(*c)+1;
			while(v[j] < x)
			{
				(*c)=(*c)+1;
				j++;
			}

		for(int k=i-1; k > j+1; k--)
		{
			(*a)=(*a)+2;
			v[k] = v[k-1];
			v[j] = x;
		}
	}
}

void selectie(int v[], int n, int* a, int* c)
{
	int i,min,imin,j;
	for (i=0; i<n-1; i++)
	{
		min=v[i];
		(*a)=(*a)+1;
		imin=i;
		for (j=i+1; j<n; j++)
		{
			(*c)=(*c)+1;
			if (v[j]<min)
			{
				min=v[j];
				imin=j;
				(*a)=(*a)+1;
			}
		v[i]=v[imin];
		(*a)=(*a)+1;
		}
	}
}


void main()
{
	
	
		g<<"n"<<","<<"Atribuiri bule"<<","<<"Comp bule"<<","<<"Suma bule"<<","<<"Atribuiri insertie"<<","<<"Comp insertie"<<","<<"Suma insertie"<<","<<"Atribuiri selectie"<<","<<"Comp selectie"<<","<<"Suma selectie"<<endl;
		for(n=100;n<10000;n=n+100)
		{
			
				for (int k = 1; k<=n; k++)
					a[k] = k;

				bule(a,n,&atr,&comp);
				g<<n<<","<<atr<<","<<comp<<","<<(atr+comp)<<",";
				atr = comp = 0;
				

				insertie(a,n,&atr,&comp);
				g<<atr<<","<<comp<<","<<(atr+comp)<<",";
				atr = comp = 0;
				

				selectie(a,n,&atr,&comp);
				g<<atr<<","<<comp<<","<<(atr+comp);
				atr = comp = 0;
				g<<endl;
		}
		g.close();
	
		h<<"n,"<<"Atribuiri bule,"<<"Comp bule,"<<"Suma bule"<<", "<<"Atribuiri insertie, "<<"Comp insertie,"<<"Suma insertie"<<", "<<"Atribuiri selectie, "<<"Comp selectie,"<<"Suma selectie"<<endl;
	
		l = 5;
		suma = sumc = 0;
		
	for(n=100;n<10000;n=n+100)
	{for(int k = 0; k<n; k++)
					b[k] = rand();
			
				while(l>0)
					{
			
						for (int k = 1; k<=n; k++)
							a[k] = b[k];
						
							bule(a,n,&atr,&comp);
							suma = suma + atr; 
							sumc = sumc + comp;
						
							atr = comp = 0;
							l--;
					}
					h<<n<<","<<suma/5<<","<<sumc/5<<","<<(suma/5+sumc/5)<<",";
			
					l=5;
					suma = sumc = 0;
					
					while(l>0)
					
					{
						for (int k = 1; k<=n; k++)
						a[k] = b[k];

						insertie(a,n,&atr,&comp);
						suma = suma + atr; 
						sumc = sumc + comp; 
					
						l--;
						atr = comp = 0;
					}
					h<<suma/5<<","<<sumc/5<<","<<(suma/5+sumc/5)<<",";
					suma=sumc=0;
					l=5;
				
					
					while(l>0)
					{
					for (int k = 1; k<=n; k++)
						a[k] = b[k];

					selectie(a,n,&atr,&comp);
					suma = suma + atr; 
					sumc = sumc + comp;
		
					atr = comp = 0;
					l--;
					}
					h<<suma/5<<","<<sumc/5<<","<<(suma/5+sumc/5);
					h<<endl;
			
	}
			h.close();
		w<<"n"<<","<<"Atribuiri bule"<<","<<"Comp bule"<<","<<"Suma bule"<<","<<"Atribuiri insertie"<<","<<"Comp insertie"<<","<<"Suma insertie"<<","<<"Atribuiri selectie"<<","<<"Comp selectie"<<","<<"Suma selectie"<<endl;
		for(n=100;n<10000;n=n+100)
		{
			for (int k = 1; k<=n; k++)
					a[k] = n-k;

				bule(a,n,&atr,&comp);
				w<<n<<","<<atr<<","<<comp<<","<<(atr+comp)<<",";
				atr = comp = 0;
				
				for (int k = 1; k<=n; k++)
					a[k] = n-k;
				insertie(a,n,&atr,&comp);
				w<<atr<<","<<comp<<","<<(atr+comp)<<",";
				atr = comp = 0;
				
				for (int k = 1; k<=n; k++)
					a[k] = n-k;
				selectie(a,n,&atr,&comp);
				w<<atr<<","<<comp<<","<<(atr+comp);
				atr = comp = 0;
				w<<endl;
		} 
			w.close();
}


	