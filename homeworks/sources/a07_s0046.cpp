// laborator 7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>



typedef struct _MULTI_NODE{
	int key;
	int count;
	struct _MULTI_NODE *child[50];   
}MULTI_NODE;

MULTI_NODE *createMN(int key)
{
	MULTI_NODE *p=(MULTI_NODE *)malloc(sizeof(MULTI_NODE));
	p->key=key;
	p->count=0;
	return p;
}

void insertMN(MULTI_NODE *parent,MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
	
	//parent->count++;
}

MULTI_NODE *transform(MULTI_NODE *nodes[],int t[],int size)
{
	MULTI_NODE *root=NULL;
	

for (int i=0;i<size;i++)
{
	nodes[i]=createMN(i);
	if (t[i]==-1) root=nodes[i];
}
for (int i=0;i<size;i++)
{
	if (t[i]!=-1)
		insertMN(nodes[t[i]],nodes[i]);

}

	return root;
}

void prettyprint(MULTI_NODE *nod,int nivel=0)
{
	for(int i=0;i<nivel;i++){
	
	    printf(" ");
	}
	printf("%d\n",nod->key);
	for(int i=0;i<nod->count;i++)
		{   
			printf("+");
			prettyprint(nod->child[i],nivel+1);
	}
	
}


typedef struct nod{
	
	int key;
	struct nod *stg,*dr;
}Btree;

Btree *insertStg(Btree *rad, int key){
	
	Btree *p=(Btree*)malloc(sizeof (Btree));
	p->key=key;
	p->stg=NULL;
	p->dr=NULL;
	
	rad->stg=p;
	return p;
}
Btree *insertDR(Btree *rad, int key){
	
	Btree *p=(Btree*)malloc(sizeof (Btree));
	p->key=key;
	p->stg=NULL;
	p->dr=NULL;
	
	rad->dr=p;
	return p;
}

Btree* inititB(MULTI_NODE *a){
	
	Btree *x;
	x=(Btree*)malloc(sizeof(Btree));
	
	x->dr=NULL;
	x->stg=NULL;

	x->key=a->key;
	

	return x;
}


int main()
{
	MULTI_NODE *node[50];

	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(int);
	int nivel=0;
	MULTI_NODE *root=transform(node,t,size);
	prettyprint(root,nivel);
	
	return 0;
}