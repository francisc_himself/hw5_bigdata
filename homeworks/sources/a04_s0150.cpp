#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
//***ANONIM*** ***ANONIM*** gr ***GROUP_NUMBER***

//###############
//Complexitatea algoritmului este: O(nlogk)
//La variatiile lui n si k se observa urmatoarele:
//  La variatia lui n ni se arata punctele de pe dreapta
//  La variatia lui k se schimba panta dreptei
//###############

struct element{
    int n,l;
};
struct LISTA{
    struct element e;
    struct LISTA * urm;
};


element rezultat[20000];
LISTA * liste[600];
element heap[600];
double nrop;
double operatii[200];


void afis(element *a,int length)
{
	printf("\n");
	for(int i=1;i<=length;i++)
		printf("%d ",a[i].n);
}

void scrie_rez(FILE *f,double *a,int length)
{
	fprintf(f,"\n");
	for(int i=1;i<=length;i++)
		fprintf(f,"%.0f;",a[i]);
}

void atribuie(element *e1,element *e2)
{
    e1->n=e2->n;
    e1->l=e2->l;
	nrop+=2;
}


LISTA* lpush(LISTA *l,int n,int lista)
{
    LISTA *aux1=(LISTA *)malloc(sizeof(LISTA));
    aux1->e.l=lista;
    aux1->e.n=n;
	aux1->urm=NULL;
    LISTA *aux2=l;

    if(aux2==NULL)
    {
        return aux1;
    }
    while(aux2->urm!=NULL)
    {
        aux2=aux2->urm;
    }
    aux2->urm=aux1;
    return l;
}

LISTA* LPOP(LISTA *l)
{
	LISTA *aux=l;
    l=l->urm;
	free(aux);
	nrop+=4;
    return l;
}

void heapify(element *a,int poz,int n)
{
	int j=poz;
	bool b=true;
	int min;
	element aux;
	while(b && j<=n/2)
	{
		if(((2*j+1)<=n) && a[j].n>a[2*j+1].n)
		{
			min=2*j+1;
		}else
		{
			min=j;
		}
		if(a[min].n>a[2*j].n){
			min=2*j;
			nrop+=1;
		}
		nrop+=4;
		if(min!=j)
		{
			aux.n=a[min].n;
            a[min].n=a[j].n;
            a[j].n=aux.n;
            aux.l=a[min].l;
            a[min].l=a[j].l;
            a[j].l=aux.l;
            j=min;
			nrop+=7;
		}else
		{
			b=false;
		}
	}
}

void bottom_up(element *a,int n)
{
	int i,j,min;
	element aux;
	bool b;
	for(i=n/2;i>=1;i--)
	{
		heapify(a,i,n);
	}
}

void mergeklists(LISTA **liste,int n, int k)
{
	int list;
	int lung=k;
	int l=1;
	int poz=1;
    for(int i=1;i<=k;i++)
    {
        atribuie(&heap[i],&(liste[i]->e));
        liste[i]=LPOP(liste[i]);
    }
    bottom_up(heap,k);
	while(k>0)
	{
		atribuie(&rezultat[poz],&heap[1]);
		poz++;
		nrop+=1;
		if(liste[heap[1].l]!=NULL)
		{
			atribuie(&heap[1],&(liste[heap[1].l]->e));
			heapify(heap,1,k);
			liste[heap[1].l]=LPOP(liste[heap[1].l]);
		}else
		{

			heap[1]=heap[k];
			k--;
			nrop+=1;
			heapify(heap,1,k);
		}


	}

}


int main(){
	FILE *f;
	srand(time(0));
	//#########################
	//test algoritm
	//#########################
    int k=4;
    int n=20;
    int lung=n/k;
    int i,j,aux;
    for(i=1;i<=k;i++)
    {
        aux=rand()%10000;
		if(i==k) lung+=n%k;
		printf("Lista%d :",i);
        for(j=1;j<=lung;j++)
        {
			aux+=abs(rand())%300+1;
			printf("%d ",aux);
            liste[i]=lpush(liste[i],aux,i);
        }
		printf("\n");
    }
	mergeklists(liste,n,k);
	printf("Lista interclasata :");
	afis(rezultat,n);

	//#########################
	//n variable algoritm
	//#########################
	f=fopen("nrandom.csv","w");
	k=5;
	for(n=100;n<=10000;n+=100)
	{
		lung=n/k;
		for(i=1;i<=k;i++)
		{
			aux=rand()%1000;
			if(i==k) lung+=n%k;
			for(j=1;j<=lung;j++)
			{
				aux+=abs(rand())%3+1;
				liste[i]=lpush(liste[i],aux,i);
			}
		}
		nrop=0;
		mergeklists(liste,n,k);
		operatii[n/100]=nrop;
	}
	scrie_rez(f,operatii,100);

	k=10;
	for(n=100;n<=10000;n+=100)
	{
		lung=n/k;
		for(i=1;i<=k;i++)
		{
			aux=rand()%1000;
			if(i==k) lung+=n%k;
			for(j=1;j<=lung;j++)
			{
				aux+=abs(rand())%3+1;
				liste[i]=lpush(liste[i],aux,i);
			}
		}
		nrop=0;
		mergeklists(liste,n,k);
		operatii[n/100]=nrop;
	}
	scrie_rez(f,operatii,100);

	k=100;
	for(n=100;n<=10000;n+=100)
	{
		lung=n/k;
		for(i=1;i<=k;i++)
		{
			aux=rand()%1000;
			if(i==k) lung+=n%k;
			for(j=1;j<=lung;j++)
			{
				aux+=abs(rand())%3+1;
				liste[i]=lpush(liste[i],aux,i);
			}
		}
		nrop=0;
		mergeklists(liste,n,k);
		operatii[n/100]=nrop;
	}
	scrie_rez(f,operatii,100);

	fclose(f);

	//#########################
	//k variable algoritm
	//#########################

	f=fopen("krandom.csv","w");
	n=10000;
	for(k=10;k<=500;k+=10)
	{
		lung=n/k;
		for(i=1;i<=k;i++)
		{
			aux=rand()%1000;
			if(i==k) lung+=n%k;
			for(j=1;j<=lung;j++)
			{
				aux+=abs(rand())%3+1;
				liste[i]=lpush(liste[i],aux,i);
			}
		}
		nrop=0;
		mergeklists(liste,n,k);
		operatii[k/10]=nrop;
	}
	scrie_rez(f,operatii,50);

	fclose(f);

	printf("\n\n####Sfirsit####");
	getch();
	return 0;
}
