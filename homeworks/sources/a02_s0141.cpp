﻿// ***ANONIM*** ***ANONIM***-Petre, ***GROUP_NUMBER***
//Tema Nr. 2: Analiza și Compararea a două metode de construire Heap: “De jos în sus” (Bottom-up) vs. “De sus în jos” (Top-down)


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <math.h>

#define NMAX 10000
int a[NMAX]={0};
int b[NMAX]={0};

char fis1[50]="D:\\bottomUp.txt";
char fis2[50]="D:\\topDown.txt";

FILE *f1, *f2;
int dim_heap;

int left(int* i)
{
	return 2 * (*i);
}

int right(int* i)
{
	return 2* (*i) + 1;
}

int parent(int* i)
{
	return ((*i)/2);
}



void prettyPrint(int* a,int n,int x, int d)
{

	if (x< n) 
	{
	int st=2*x;
	int dr=2*x+1;
	prettyPrint(a,n,dr,d+1);
	for (int i=0;i<d;i++)
		printf("     ");
	printf("%d\n",a[x]);
	prettyPrint(a,n,st,d+1);
	}
}



void maxHeapify(int* a, int i, int dimensiune,int& atr, int& comp )
{   
	int aux,maxim;
	int fiu_stang = 2*i;//left(&i);
	int fiu_drept = 2*i+1;//right(&i);
	comp+=2;
	if (fiu_stang < dimensiune && a[fiu_stang] > a[i])
		maxim = fiu_stang; 	
	else 
		maxim=i;
	if (fiu_drept < dimensiune && a[fiu_drept] > a[maxim]) 
	maxim = fiu_drept;
	
	if(maxim!=i)
	{   
		aux=a[i];
		a[i]=a[maxim];
		a[maxim]=aux;
		atr+=3;
		maxHeapify(a,maxim,dimensiune,atr,comp);
	} 
}   


void bottomUp( int* a, int dimensiune,int& atr, int& comp )
{   
	int i;
	for (i = dimensiune/2; i >= 1; i--)
		maxHeapify(a, i, dimensiune,atr,comp);

}   
void insert(int *a,int cheie, int &dim_heap, int &comp, int &attr)
{
	dim_heap++;
	int i=dim_heap;
	comp++;
	while(i>1 && a[int(i/2)] < cheie)
	{	comp++; 
	    attr+=1;
		a[i]=a[int(i/2)];
		i=i/2;
		
	}
	a[i]=cheie;
	attr+=1;

}


void topDown( int* a,int dimensiune,int& atr, int& comp )
{   
	dim_heap=1;
	for(int j=2;j<=dimensiune;j++)
	{
		insert(a,a[j],dim_heap,comp,atr);
	}
} 


void genereazaSirRandom(int n,int *a)
{
	 int ok=1;
	
	srand(time(NULL));
		for(int j=1;j<=n;j++)
		{
					a[j]=rand()%n;	
		}
}

void scrieInFisier(FILE *f,int n, int atr, int comp)
{

	fprintf(f,"%d,%d\n",n,atr+comp);

}

void main()
{   
	f1=fopen(fis1,"w");
	f2=fopen(fis2,"w");


	fprintf(f1,"n,na+nc\n");
	fprintf(f2,"n,na+nc\n");


	    int n=10;
		int	atr1=0,comp1=0;
		int	atr2=0,comp2=0;
		int comp=0, atr=0;
		genereazaSirRandom(n,a);
		for(int y = 1; y<n ;y++)
			printf("%d ", a[y]);
		printf("\n\n\n\n\n");
			
			for (int j=0;j<n;j++)
				b[j]=a[j];
			
				bottomUp(a, n, atr, comp);
				prettyPrint(a,n,1,0);

				printf("\n\n");

				topDown(b, n, atr, comp);
				prettyPrint(b,n,1,0);
				
        
		for(int n=100;n<=NMAX;n+=200)
		{

			for (int k=1; k<=5; k++)
			{			
			genereazaSirRandom(n,a);
	     	atr=0;
			comp=0;
			
			for (int j=0;j<n;j++)
				b[j]=a[j];
			bottomUp(a, n, atr, comp );
			atr1=atr1+atr;
			comp1=comp1+comp;			

		

			topDown(b, n, atr, comp);
			atr2=atr2+atr;
			comp2=comp2+comp;
			
			}
			scrieInFisier(f1,n,atr1/5,comp1/5);
			scrieInFisier(f2,n,atr2/5,comp2/5);

		atr1=0;
		atr2=0;
		comp1=0;
		comp2=0;

}
		fclose(f1);
		fclose(f2);

		getch();
}
