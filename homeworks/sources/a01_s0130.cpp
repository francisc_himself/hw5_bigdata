#include <iostream>
#include <ctime>
#include <fstream>
#include "Profiler.h"

int a[10000];
Profiler profiler("demo");


void sel_sort(int a[], int size) //SELECTION SORT
{
	int aux, min;
	profiler.countOperation("compare_select", size, 0);
	profiler.countOperation("assign_select", size, 0);
	
	for(int i=0;i<size;i++)
	{
		min=i;
		for(int j=i+1;j<size;j++)
		{
			profiler.countOperation("compare_select", size);
			if(a[j]<a[min])
			{
				min=j;
			}
		}
		if(min!=i)
		{
			aux=a[i];
			a[i]=a[min];
			a[min]=aux;
			profiler.countOperation("assign_select", size, 3);
		}	
	}
}

void  ins_sort(int  a[], int  size) //INSERTION SORT
{
	profiler.countOperation("compare_insert", size, 0);
	profiler.countOperation("assign_insert", size, 0);
	int  aux, k;

	for(int  i=1;i<size;i++)
	{
		aux=a[i];
		profiler.countOperation("assign_insert", size);
		k=i;

		profiler.countOperation("compare_insert", size);
		while(a[k-1]>aux && k>0)
		{
			a[k]=a[k-1];
			profiler.countOperation("assign_insert", size);
			profiler.countOperation("compare_insert", size);
			k--;
		}
		a[k]=aux;
		profiler.countOperation("assign_insert", size);
	}
}

void bubb_sort(int a[], int size)
{
	profiler.countOperation("compare_bubble", size, 0);
	profiler.countOperation("assign_bubble", size, 0);
	int aux, ok=0;

	while(ok==0)
	{
		ok=1;
		for(int i=0;i<size-1;i++)
		{
			profiler.countOperation("compare_bubble", size);
			if(a[i]>a[i+1])
			{
				ok=0;
				aux=a[i];
				a[i]=a[i+1];
				a[i+1]=aux;
				profiler.countOperation("assign_bubble", size, 3);
			}
		}
	}
}

void best_case()
{
	for(int i=0;i<10000;i++)
	{
		a[i]=i;
	}
	for(int n=100;n<10000;n+=100)
	{
		sel_sort(a, n);
		ins_sort(a, n);
		bubb_sort(a, n);
	} 

	profiler.createGroup("Best Case Comp","compare_select","compare_insert","compare_bubble");
	profiler.createGroup("Best Case Assign","assign_select","assign_insert","assign_bubble");

	profiler.addSeries("Select","compare_select","assign_select");
	profiler.addSeries("Insert","compare_insert","assign_insert");
	profiler.addSeries("Bubble","compare_bubble","assign_bubble");

	profiler.createGroup("Best Case Comp and Assign","Select", "Insert","Bubble");

	profiler.showReport();
}

void average_case()
{
	for(int n=100;n<10000;n+=100)
	{
		
		FillRandomArray(a, 10000);
		sel_sort(a, n);

		FillRandomArray(a, 10000);
		ins_sort(a, n);

		FillRandomArray(a, 10000);
		bubb_sort(a, n);
	} 
	
	profiler.createGroup("Average Case Comp","compare_select","compare_insert","compare_bubble");
	profiler.createGroup("Average Case Assign","assign_select","assign_insert","assign_bubble");

	profiler.addSeries("Select","compare_select","assign_select");
	profiler.addSeries("Insert","compare_insert","assign_insert");
	profiler.addSeries("Bubble","compare_bubble","assign_bubble");

	profiler.createGroup("Average Case Comp and Assign","Select", "Insert","Bubble");

	profiler.showReport();
}

void worst_case()
{
	for(int n=100;n<10000;n+=100)
	{
		int aux=1;
		for(int i=0;i<n-1;i++) //selection sort
		{
			a[i]=i;	
		}
		a[n-1]=-1;
		sel_sort(a,n);

		for(int i=0;i<n;i++) //insertion sort
		{
			a[i]=n-i;
		}
		ins_sort(a,n);

		for(int i=0;i<n;i++) //bubble sort
		{
			a[i]=n-i;
		}
		bubb_sort(a,n);
	}
		
	profiler.createGroup("Worst Case Comp","compare_select","compare_insert","compare_bubble");
	profiler.createGroup("Worst Case Assign","assign_select","assign_insert","assign_bubble");

	profiler.addSeries("Select","compare_select","assign_select");
	profiler.addSeries("Insert","compare_insert","assign_insert");
	profiler.addSeries("Bubble","compare_bubble","assign_bubble");

	profiler.createGroup("Worst Case Comp and Assign","Select", "Insert","Bubble");

	profiler.showReport();
}

int main()
{
	//best_case();
	//average_case();
	worst_case();
}