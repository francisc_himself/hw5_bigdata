// ***ANONIM*** ***ANONIM*** - ***GROUP_NUMBER***
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <math.h>

#define N 10007 // numarul de elemente
int T[N];
int n;
int gasite=0,negasite=0,maxg=0,maxn=0;
int x=0;

int hash(int k, int i)
{
    int aux, x;

    aux= k % N;
    x = (aux+(1*i)+(2*i*i)) % N;
    return x;
}

void init()
{
    for (int i=0; i<N; i++)
        T[i]=0;
}


int hash_insert(int k) 
{
    int i=0, j;

    do {
        j=hash(k,i);
        if (T[j]==NULL) {
            T[j]=k;
            return j;
        }
        else
            i=i+1;
    }
    while (i<N);
    return NULL;
}

int hash_search(  int k) 
{
	int i=0, j;int nr=0;

    do {
        j=hash(k, i);
        nr++;
        if (T[j]==k) { 
            gasite += nr;
            if (maxg<nr) {
                maxg=nr;
            }
            return T[j]; 
        }
        else
            i++;
		 negasite += nr;
        if (maxn<nr) {
            maxn=nr;
			}
        
    } while (T[j]!=0 && i!=N);
     
    return NULL;
}

void test(){

	FILE *f;
	
	double alfa[]={0.8,0.85,0.9,0.95,0.99};  // factorul de umplere
    int k;

    srand(time_t(NULL));
    fopen_s(&f,"Tabel.txt","w");
	fprintf_s(f,"umplere  mediu gasite max gasite mediu negasite maxim negasite\n");
		
	   for(int i=0; i<5; i++) {
		
		
		{
			gasite=0;
        negasite=0;
        maxg=0;
        maxn=0; 
		n=(int)(alfa[i]*N);
         for(int b=0; b<5; b++) {
            init();
			for (int j=0; j<n; j++) {
                k=rand()%3000;
                hash_insert(k);
				hash_search(k);
          }
           
        }

		fprintf_s(f,"%1.2lf     %d               %d              %d              %d \n",alfa[i],(gasite/3000)/5,maxg,(negasite/3000)/5,maxn);

			
		}
	   }
	   fprintf(f,"\n");
    fclose(f);
}
int main()
{
    test();
      double a=0.8;
	int val;
	init();
	for (int i=1;i <=a*7;i++) 
		{
			int ins=i;
			hash_insert(ins);
	    }

	for (int i=1;i<=5;i++)
	printf("%d\n",T[i]);
	printf("Verificati daca exista valoarea:\n");
	scanf_s("%d",&val);
	x=hash_search(val);
	if(x!=0)
		printf("Valoarea a fost gasita");
	else printf("Valoarea nu a fost gasita");
    _getch();
	return 0;
}