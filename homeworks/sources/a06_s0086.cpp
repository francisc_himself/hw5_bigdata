#include "stdio.h"
#include "conio.h"
#include "stdlib.h"
//creeaza o structura de tip arbore, unde fiecare nod retine o valoare si dimensiunea arborelui, si retine
//nodul de pe stanga, cel de pe dreapta si parintele
typedef struct tip_nod
			{ int nr,dimensiune;
			  tip_nod *stg,*dr,*parinte;
		    } TIP_NOD;
int vec[10000];
FILE *f;
int comparari,atribuiri;

//returneaza cea mai ***ANONIM*** stanga frunza al unui arbore
TIP_NOD* frunza_stanga(TIP_NOD *arb)
{
	while (arb->stg!=NULL)
	{
		atribuiri++;
        arb=arb->stg;
	}
  return arb;
}

//returneaza succesorul unui nod
TIP_NOD* succesor(TIP_NOD *arb)
{ TIP_NOD *y;
  comparari++;
  if (arb->dr!=NULL)//daca subarborele drept nu e nul se returneaza fiul stang al sub. drept
     return frunza_stanga(arb->dr);
  atribuiri++;
  y=arb->parinte;//se retine parintele arborelui transmis ca parametru
  while ( (y!=NULL) && (arb==y->dr) )
	{
	  atribuiri+=2;
	  arb=y;//arborele devine fostul parinte
	  y=y->parinte;//fostul parinte devine parintele sau
	}
  return y;//se returneaza
}

//construieste un arbore pe baza unui interval s...d si si stabileste dimensiunea subarborilor stang si drept
TIP_NOD *construire(int s, int d, int *dim)
{ TIP_NOD *p;
  int n=sizeof(TIP_NOD);
  int dim1=0,dim2=0;
  comparari++;
  if (s>d)
	  { *dim=0;
   	    return NULL;
	  }
	  else
	      {
            atribuiri=atribuiri+5;
			p=(TIP_NOD*) malloc(n);
	        p->nr=(s+d)/2;//se retine mijlocul intervalului
			p->stg = NULL;//subarborele stang devine null
			p->dr = NULL;//subarborele drept devin null
			p->dimensiune=d-s+1;//se calculeaza dimensiunea
			if(s<d){//se merge recursiv in constructia arborelui pe cele 2 ramuri
				p->stg=construire(s,(s+d)/2-1,&dim1);
				if(p->stg != NULL)
				{	p->stg->parinte = p;  atribuiri++; }
				p->dr=construire((s+d)/2+1,d,&dim2);
				if(p->dr != NULL)
				{	p->dr->parinte = p; atribuiri++;}
			}
	      }
  atribuiri++;
  *dim=p->dimensiune;//se retine dimensiunea
  return p;
}

//se scade dimensiunea unui arbore pana la radacina
void scade_dim(TIP_NOD *z)
{ while (z!=NULL)
	{
      atribuiri=atribuiri+2;
	  z->dimensiune--;
	  z=z->parinte;
	}
}
//sterge un nod ***ANONIM*** arbore
void sterge_nod(TIP_NOD **arb, TIP_NOD *z)
{ TIP_NOD *y,*x;
  comparari=comparari+5;
  if ( (z->stg==NULL) || (z->dr==NULL) )
     { y=z; atribuiri++;
     }
     else
	 {y=succesor(z); atribuiri++; }
  if (y->stg!=NULL)
  { x=y->stg; atribuiri++; }
     else
	 {	 x=y->dr; atribuiri++; }
  if (x!=NULL)
     {   atribuiri++;
		 x->parinte=y->parinte;
     }
  scade_dim(y->parinte);
  if (y->parinte==NULL)
     *arb=x;
     else
	 if (y==y->parinte->stg)
	 { y->parinte->stg=x; atribuiri++;}
	    else
		{ y->parinte->dr=x; atribuiri++; }
  if (y!=z)
     {   atribuiri++;
		 z->nr=y->nr;
     }
  free(y);
}

//functia SO_SELECTIE returneaza radacina subarborelui cu dimensiunea i a unui arbore
TIP_NOD* SO_SELECTIE(TIP_NOD *arb, int i)
{ int r;
  comparari=comparari+3;
  atribuiri++;
  if (arb->stg!=NULL) r=arb->stg->dimensiune+1;//r retine dimensiunea subarborelui stang
     else r=1;

  if (i==r)//se retine arborele initial daca acesta are dimensiunea ceruta
  return arb;
  if (i<r)//daca dimensiunea ceruta e mai mica se cauta in subarborele stang
     return SO_SELECTIE(arb->stg,i);
     else//daca dimensiunea ceruta e mai maica se cauta in subarborele drept
	 return SO_SELECTIE(arb->dr,i-r);
}
//functia josephus primeste ca parametrii numarul de elemente si pasul de numarare
void josephus(int n, int m)
{
  TIP_NOD *rad,*x;
  int h,k,elem;
  int index=0;
  elem=n;
  rad=construire(1,elem,&h);//construieste arborele cu elemente intre 1 si n si dimensiunea h
  rad->parinte=NULL;//parintele radacinii va fi nul

  k=m;//se incepe cu numarul corespondent pasului de numarare
  while (elem>0)
	{
	  x=SO_SELECTIE(rad,k);//se retine radacina subarborelui cu dimensiunea x a arborelui
	  vec[index]=x->nr;//se pune in vector valoarea
	  index++;//incrementeaza dim. vectorului
	  sterge_nod(&rad,x);//se sterge nodul respectiv
	  elem--;
	  if (elem)
	     {
		   k=(k+m-1)%elem;
	       if (k==0)
		  k=elem;
	     }
	}
//fprintf(f,"Pentru %d elemente numarate ***ANONIM*** %d in %d avem or***ANONIM***ea de iesire\n",n,m,m);
//for (int i=0;i<u;i++)
//	fprintf(f," %d",vec[i]);
}

int main()
{
  f=fopen("rez.txt","w");
  for (int n=100;n<10000;n+=400)
  {
    atribuiri=0;
	comparari=0;
    int m=n/10;
    josephus(n,m);
    fprintf(f,"%d %d \n",n,atribuiri+comparari);
  }
	//josephus(7,3);
    fclose(f);
    return 0;
}
