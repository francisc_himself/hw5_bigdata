#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include "time.h"

/**
Permutarea Josephus are ca scop stergerea din m in m a n noduri a unui arbore binar.
Fiecare nod a arborelui are si atasat un size, care arata cate noduri are subarborele lui.
OS select numara al m-lea nod, si tree_delete il sterge. Tree_delete apeleaza functiile
succesor si treemin, care cauta nodul cu care o sa fie inlocuit; si decrease_size, care dupa
stergerea unui nod, reactualizeaza sizeul tuturor nodurilor atinsi.
OS_select si tree_delete au eficienta O(ln n) iar Functia Josephus O(n*log n).
*/

typedef struct tip_nod
{ 
	int key;
    int size;
	struct tip_nod *stg;
	struct tip_nod *dr;
	struct tip_nod *parent;
} TIP_NOD;

FILE *f;
TIP_NOD *rad;
int a[100][100], A[100][100];
int cmp,atr;

int niv;

//construieste un arbore pe baza unui interval s...d si si stabileste dimensiunea subarborilor stang si drept

TIP_NOD *construire(int s, int d, int dim)
{ 
	TIP_NOD *p;
	int n=sizeof(TIP_NOD);
	int dim1=0,dim2=0;
	cmp++;//
	if (s>d)
	{ 
			dim=0;
   			return NULL;
	}
	else
	{
		p=(TIP_NOD*) malloc(n);
		p->key=(s+d)/2;		//se pune valoare medianei intervalului
		p->stg = NULL;		
		p->dr = NULL;		
		p->size=d-s+1;        //fomula dimensiunii
		atr+=4;//
		if(s<d)
		{						
			//se merge recursiv in constructia arborelui pe cele 2 ramuri
			p->stg=construire(s,(s+d)/2-1,dim1);
			if(p->stg != NULL)
			{
				p->stg->parent = p;  
				atr++;//
			}
			p->dr=construire((s+d)/2+1,d,dim2);
			if(p->dr != NULL)
			{
				p->dr->parent = p; 
				atr++;//
			}
			cmp+=2;
			niv++;
		}
	}
	dim=p->size;//se retine dimensiunea
	atr++;//
	return p;
}

TIP_NOD *os_select (TIP_NOD *x, int i) // i = cate numeri
{
	//if(x==NULL || x->key==0) 
		//return NULL;//just in case; 
	int r=1;
	cmp++;//
	if(x->stg!=NULL)
	{
		r=(x->stg->size)+1;
	}
	cmp++;//
	if(i==r)
		return x;
	else if (i<r)
	{
		cmp++;//
		return os_select(x->stg, i);
	}
	else 
	{
		cmp++;//
		return os_select(x->dr, i-r);
	}
}

void decrease_size (TIP_NOD *x)
{
	while(x!=rad)
	{
		atr+=2;//
		x=x->parent;
		x->size--;
	}
}

// returneaza succesorul stang folosit la rotatie 
TIP_NOD *treemin(TIP_NOD *x)
{
	while(x->stg!=NULL)
	{	
		//x->size=x->size-1;
		x=x->stg;
		atr++;//
	}
	return x;
}

TIP_NOD *succesor (TIP_NOD *x)
{
	cmp++;//
	if(x->dr!=NULL)
		return treemin(x->dr);
	TIP_NOD *y;
	y=x->parent;
	atr++;//
	while ((y!=NULL) && (x==y->dr))
	{		
		x=y;
		y=y->parent;
		atr++;//
	}
	printf("\n succesor in succesor: %d\n", y->key);
	return y;
}

//Din Caiet
TIP_NOD *tree_delete(TIP_NOD *z)
{
	TIP_NOD *y;
	cmp++;//
	atr++;//
	if((z->stg==NULL) || (z->dr==NULL))
		 y=z;
	else
		y=succesor(z);
	//printf("\n succesor: %d\n", y->key);
	TIP_NOD *x;
	
	x=y;
	atr++;//
	decrease_size(x);
	
	x=NULL;
	cmp++;//
	atr++;//
	if (y->stg!=NULL) 
		x=y->stg;
	else 
		x=y->dr;
	cmp++;//
	if (x!=NULL) 
	{
		atr++;//
		x->parent=y->parent; 
	}
	cmp++;//
	if(y->parent == NULL)
	{
		rad=x;
		atr++;//
	}
	else 
		{
			cmp++;//
			atr++;//
			if (y==y->parent->stg)///e radacina !!!!
				y->parent->stg=x;
			else y->parent->dr=x;
		}
	cmp++;//
	if (y!=z)
	{
		atr++;//
		 z->key=y->key;
	}
	free(y);
	return rad;
}




void preordine(TIP_NOD *p, int nivel)//, int nivel)
{
    if (p!=0)
	{
		if (nivel+1>niv)
			niv=nivel+1;
		printf("%d ", p->key);
		printf("%d\n ", p->size);
		preordine((p)->stg, nivel+1);
		preordine((p)->dr, nivel+1);
    }
}

void matrice(TIP_NOD *p, int i, int j, int k)
{
    int b;
    if (p!=NULL)
    {
		a[i][j]=(char)(((int)'0')+p->key);
		if(p->stg!=NULL)
			for (b=1; b<=k; b++) 
				a[i+b][j-b]='/';
		if(p->dr!=NULL)
			for (b=1; b<=k; b++) 
				a[i+b][j+b]='\\';
		matrice(p->stg, i+1+k, j-k, k/2);
		matrice(p->dr, i+1+k, j+k, k/2);
    }
}

void matrice2 (TIP_NOD *p, int i, int j, int k)
{
    int b;
    if (p!=NULL)
    {
		A[i][j]=(char)(((int)'0')+p->size);
		if(p->stg!=NULL)
			for (b=1; b<=k; b++) 
				A[i+b][j-b]='/';
		if(p->dr!=NULL)
			for (b=1; b<=k; b++) 
				A[i+b][j+b]='\\';
		matrice2(p->stg, i+1+k, j-k, k/2);
		matrice2(p->dr, i+1+k, j+k, k/2);
    }
}

int putere (int a)
{
	int i, rez=1;
	for(i=1;i<=a;i++)
		rez=rez*2;
	return rez;
}

void inita()
{
	for(int i=0; i<100; i++)
		for(int j=0; j<100; j++)
			a[i][j]=NULL;
}

void initA()
{
	for(int i=0; i<100; i++)
		for(int j=0; j<100; j++)
			A[i][j]=NULL;
}

void pretty_print (TIP_NOD* rad, int depth)
{
	int ceva=putere(niv-1);
    int l=niv+ceva-1;   // numarul de linii din matrice
    int c=2*ceva-1;     // numarul de coloane din matrice
    matrice(rad, 1, ceva ,ceva/2);
	matrice2(rad, 1, ceva ,ceva/2);
	for(int i=1;i<=l; i++)
	{ 
		for(int j=1;j<=c;j++)
			printf("%c ", a[i][j]);
		printf("       ");
		for(int j=1;j<=c;j++)
			printf("%c ", A[i][j]);

		printf("\n");
	}
	inita();
	initA();
}

void JP (int n, int m)
{
	//TIP_NOD *rad;
	int nr=n;
	rad=construire(1,nr,0);
	rad->parent=NULL;
	int i=m;
	TIP_NOD *x;
	if(n<50) pretty_print(rad, 0);
	while((rad->stg!=NULL) || (rad->dr!=NULL))
	{
		x=os_select(rad, i);
		if(n<50) printf("\nsterg nodul: %d  cu sizeul: %d\n", x->key, x->size);
		//sterge_nod(&rad, x);
		tree_delete(x);
		if(n<50) pretty_print(rad, 0);
		nr=nr-1;
		//i==(i+m-2) % nr+1;
		 if (nr)
	     {
			i=(i+m-1)%nr;
			if (i==0)
				i=nr;
	     }
	}
	if(n<50) printf("\nsterg nodul: %d  cu sizeul: %d\n", rad->key, rad->size);
	free(rad);
}

void main()
{
	//DEMO
	JP(7,3);

	//PROGRAM
	int n=0;//nr de Prizonieri
	int m=n/2;//cate numaram ca sa impuscam un Prizonier

	FILE *f;
	f=fopen("out.txt","w");
	fprintf(f,"n op\n");
	for(n=100; n<=10000; n=n+100)
	{
		cmp=atr=0;
		int m=n/2;
		JP(n,m);
		//printf("pas %d op %d\n",n,cmp+atr);
		fprintf(f,"%d %d\n",n,cmp+atr);
	}
	fclose(f);
	printf("Am terminat %c",7);
	getch();
}