#include<stdio.h>
#include<conio.h>
#include<cstring>
#include<cstdlib>

int* randArray(int n)
{
	int* v = new int[n+1];
	for(int i = 1; i <= n; ++i)
	{
		v[i] = rand();
	}
	return v;
}


void swap(int &a, int &b){
	int aux = a;
	a = b;
	b = aux;
}

void heapify_down(int* v, int n, int i, int &a, int &c){
	
	int largest = i;

	if(2*i <= n)
	{
		c++;
		if(v[largest] < v[2*largest])
			largest *= 2;
		
		if(2*i+1 <= n)
		{
			c++;
			if(v[largest] < v[largest+1])
			largest++;
		}

	}
	if(largest != i)
	{
		a+=3;
		swap(v[i], v[largest]);
		heapify_down(v, n, largest, a, c);
	}


}

int* buildHeapBottomUp(int* v, int n, int &a, int &c){
	int* heap = new int[n+1];

	for(int i = 1; i <= n; ++i)
		heap[i] = v[i];

	for(int i = n/2; i > 0; --i)
		heapify_down(heap, n, i, a, c);
	return heap;
}

int* heapsort(int* v, int n, int &a, int &c)
{
	
	int* heap = buildHeapBottomUp(v, n, a, c);
	int heapSize = n;

	for(int i = heapSize; i >= 2; --i)
	{
		a += 3;
		swap(heap[1], heap[heapSize]);
		heapSize--;
		heapify_down(heap, heapSize, 1, a, c);
	}


	return heap;
}

void printArray(int* v, int n)
{
	for(int i = 1; i <= n; ++i)
		printf("%d ", v[i]);
	printf("\n");
}

int	partition(int*v, int p, int r, int &a, int &c)
{

	int x = v[r];
	int i = p-1;
	for(int j = p; j <= r-1; ++j)
	{
		c++;
		if(v[j] <= x)
		{
			i++;
			swap(v[i], v[j]);
			a+=3;
		}
	}
	swap(v[i+1], v[r]);
	a+=3;
	return i+1;

}

void quickSort(int* v, int p, int r, int &a, int &c)
{
	if(p < r)
	{
		int q = partition(v, p, r, a, c);
		quickSort(v, p, q-1, a, c);
		quickSort(v, q+1, r, a, c);
	}
}

int* performQuickSort(int* v, int n, int &a, int &c)
{
	int* sortedArray = new int[n+1];

	for(int i = 1; i <= n; ++i)
		sortedArray[i] = v[i];
	
	quickSort(sortedArray, 1, n, a, c);
	return sortedArray;
}

void demo()
{
	int v[] = {0, 9, 8, 7, 6,5, 4,3, 2, 1, 0};
	int n = 10;
	int dummy, dummy2;

	int* heapSortedArray = heapsort(v, n, dummy, dummy2);
	int* quickSortedArray = performQuickSort(v, n, dummy, dummy2);
	
	printf("Initial array: \n");
	printArray(v, n);

	printf("Heap sorted array: \n");
	printArray(heapSortedArray, n);

	printf("Quick Sorted array: \n");
	printArray(quickSortedArray, n);
}

int main()
{
	printf("Demo:\n");
	demo();
	printf("Performing algorithms on big data, please wait ... \n");
	FILE *foutAVG = fopen("sortingAVG.csv", "w");
	FILE *foutWORST = fopen("sortingWORST.csv", "w");
	FILE *foutBEST = fopen("sortingBEST.csv", "w");

	fprintf(foutAVG, "n,heapSort,quickSort\n");
	fprintf(foutWORST, "n,heapSort,quickSort\n");
	fprintf(foutBEST, "n,heapSort,quickSort\n");

	for(int n = 100; n <= 10000; n += 100)
	{
		//Best case
		{
			int* v = new int[n+1];
			for(int q = 1; q <= n; ++q)
			{
				v[q] = q;
			}

			int a1 = 0, c1 = 0, a2 = 0, c2 = 0;
			int* heapSortedArray = heapsort(v, n, a1, c1);
			int* quickSortedArray = performQuickSort(v, n, a2, c2);

			fprintf(foutBEST, "%d,%d,%d\n", n, a1+c1, a2+c2);

			delete[] v;
			delete[] heapSortedArray;
			delete[] quickSortedArray;
		}

		//Average case
		
		int A1 = 0, C1 = 0, A2 = 0, C2 = 0;
		for(int q = 5; q != 0; --q)
		{
			int a1 = 0, c1 = 0, a2 = 0, c2 = 0;
			int *v = randArray(n);
			int* heapSortedArray = heapsort(v, n, a1, c1);
			int* quickSortedArray = performQuickSort(v, n, a2, c2);

			A1 += a1;
			C1 += c1;
			A2 += a2;
			C2 += c2;

			delete[] v;
			delete[] heapSortedArray;
			delete[] quickSortedArray;
		}
		fprintf(foutAVG, "%d,%d,%d\n", n, (A1+C1)/5, (A2+C2)/5);

		//Worst case
		{
			int* v = new int[n+1];
			for(int q = 1; q <= n; ++q)
			{
				v[q] = n - q + 1;
			}

			int a1 = 0, c1 = 0, a2 = 0, c2 = 0;
			int* heapSortedArray = heapsort(v, n, a1, c1);
			int* quickSortedArray = performQuickSort(v, n, a2, c2);

			fprintf(foutWORST, "%d,%d,%d\n", n, a1+c1, a2+c2);

			delete[] v;
			delete[] heapSortedArray;
			delete[] quickSortedArray;
		}
	}

	fclose(foutAVG);
	fclose(foutWORST);

	printf("Done!\nPress any key to continue ... \n");
	getch();
	return 0;
}