/*
***ANONIM*** Vasile-***ANONIM***, grupa ***GROUP_NUMBER***
Algoritmul de permutare Josephus este liniar pentru m=constant si are eficienta 
O(nlogn) daca m nu este constant.
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define MIN 100
#define MAX 10001 
#define error -1

typedef struct tree_nod
{
	int val;
	struct tree_nod *left, *right;
	int dim;
}BinaryTree;

BinaryTree* sortedListToBST(int start, int end) {
  if (start > end) return NULL;
  // same as (start+end)/2, avoids overflow
  int mid = start + (end - start) / 2;
  BinaryTree *parent=(BinaryTree*)malloc(sizeof(BinaryTree));
  parent->val=mid;
  parent->dim=1;
  parent->right = sortedListToBST(mid+1, end);
  if(parent->right!=NULL) parent->dim++;
  parent->left = sortedListToBST(start, mid-1);
  if(parent->left!=NULL) parent->dim++;
  return parent;
}

BinaryTree* OS_SELECT(BinaryTree *x, int i)
{
	int r;
	r=x->left->dim+1;
	if (i==r) 
		return x;
	else if(i<r) 
		return OS_SELECT(x->left,i);
	else
		return OS_SELECT(x->right,i-r);
}



void pretty_print(BinaryTree *root)
{
	for(int i=0;i<root->dim;i++)
		printf("  ");
	printf("%d\n",root->val);
	
	if(root->left!=NULL)
		pretty_print(root->left);
	
	if(root->right)
		pretty_print(root->right);
	printf("\n");
}


int main()
{
	int n;
	printf("n=");
	scanf("%d",&n);

	BinaryTree *root=sortedListToBST(1,n);

	pretty_print(root);
	system("pause");

}