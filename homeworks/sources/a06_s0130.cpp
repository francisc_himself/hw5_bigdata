#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
using namespace std;

typedef struct nod
{
	int key;
	int dim;
	nod *left, *right, *parent;
} node;


node* generate(int x, int dim, node *st, node *dr)
{
	node *q;
	q= new node;
	q->key=x;
	q->left=st;
	q->right=dr;
	q->dim=dim;
	
	if(st!=NULL)
		st->parent=q;
	if(dr!=NULL)
		dr->parent=q;
	q->parent=NULL;
	return q;
}

node* build(int stg,int dr)
{
	node *ds, *d;
	if(stg>dr)
		return NULL;
	if(stg==dr)
		return generate (stg,1,NULL,NULL);
	else
	{
		int mid=(stg+dr)/2;
		ds=build(stg,mid-1);
		d=build(mid+1,dr);
		return generate(mid,dr-stg+1,ds,d);
	}
}

void print(node *p, int level)
{
	if(p!=NULL)
	{
		print(p->left,level+1);
		for(int i=0;i<=level;i++)
			cout<<"\t";
		cout<<p->key<<endl;
		print(p->right,level+1);
	}
}

int main()
{	
	node *p;
	int a[100],n;
	cout<<"n=";
	cin>>n;
	cout<<"elementele:";
	for(int i=0;i<n;i++)
		cin>>a[i];
	p=build(a[0],a[n-1]);

	print(p,0);
	getch();


}
