#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
using namespace std;

#define M 10007

int table[M];
long InTable[1501];
long NotInTable[1501];
int cost;


void init()
{
    int i;
    for (i=0;i<M;i++)
    {
        table[i]=NULL;
    }
}



int h2(int k) //basic hash    
{
  int x;
 
  x = k%M;
  return x;
}

int h(int k,int i) //quadratic probing hash       
{
  int x;
  int c1=1,c2=2;

  x = (h2(k)+c1*i+c2*i*i)%M;
  return x;
}


int hash_insert(long k)
{
    int i=0,j;
    do
    {
        j=h(k,i);
        if (table[j] == NULL)
        {
			table[j]=k;   
			return j;
        }
		else i++;
    }while (i!=M);
	return -1;
}

int searchHash (long k)
{
    int j;
    int i=0;
    do
    {
        j=h(k,i);
		cost++;
		if (table[j]==k)        
			return j;       
        else i++;        
    }while ((table[j]!=NULL)&&(i!=M));
	return -1;
}

void main()
{
	int avg_found=0,max_found=0,avg_not_found=0,max_not_found=0;
	int i,nr=0,max,x, costInt;
	long key;
	double ff[]={0.8,0.85,0.9,0.95,0.99};
	
	//
	srand(time(NULL));

/*	int iarray[]={1,5,11,7,9,8,6,2};
	int sarray[]={5,8,6,1};

	for(i=0;i<8;i++)
		hash_insert(iarray[i]);

	for(i=0;i<4;i++){
		cout<<searchHash(iarray[i]);
	}*/
	

	
	for(int k=0;k<5;k++){

		for(int m=0;m<5;m++){
	
	init();

	//insert keys based on the filling factor ff
	for (i=1; i<= (ff[k] * M) ; i++)
	{		
		key=rand();
		if(i<=1500)
			 InTable[i]=key;//create an array of 1500 elements from the table
		hash_insert(key);
	}
	
	nr=1;

	//create an array of elements which surely are not in the table
	for (i=1;i<=1500;i++)
       NotInTable[i] =100000+InTable[i];
	
	
	cost = 0;
	max = 0;
	int aa;
	//search elements which we know that are sure in the table
	for (i=1;i<=1500;)
	{	
		aa = table[rand()%M];
		if(aa != NULL){
			costInt = cost;
			searchHash(aa);
			costInt = cost - costInt;
			if (costInt > max)
				max = costInt;
			i++;
		}
	}

	avg_found+=cost/1500;
	max_found+=max;

	cost=0;
	max=0;
	//search elements we know that are not in the table
	for (i=1;i<=1500;i++)
	{
		costInt=cost;
		searchHash(NotInTable[i]);
		costInt=cost-costInt;
		if (costInt>max)
			max=costInt;
	}
	
	avg_not_found+=cost/1500;
	max_not_found+=max;

	}
	printf("\n %1.2f \t %d \t %d \t%d \t%d ",ff[k],avg_found/5,max_found/5,avg_not_found/5,max_not_found/5); 
	avg_not_found=avg_found=max_found=max_not_found=0;
	}
	getch();
}