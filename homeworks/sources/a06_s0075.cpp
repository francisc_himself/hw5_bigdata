/*
***ANONIM*** ***ANONIM*** Mihai grupa ***GROUP_NUMBER***, Lucrare 6 Algoritmi fundamentali
In aceasta lucrare de laborator am studiat permutarea Josephson. Pentru efectuarea acestei permutari exista doua variante de implementare:
folosind liste circulare, si folosind arbori de cautare echilibrati. Eu am ales varianta cu liste, avand complexitatea O(n^2). Varianta cu arbori
este, insa, varianta optima, complexitatea acesteia fiind O(n*log(n)).
*/

#include<stdio.h>
#include<conio.h>
#include<malloc.h>
//definitia nodului
struct nod
	{
		int value;
		struct nod *next;
	};
nod *start=NULL;
//functia care afiseaza cum arata lista, dupa fiecare eliminare
void afisare(int i){
	nod *p=start;
	printf("%d",p->value);
	p=p->next;
	do{
		printf(",%d",p->value);
		p=p->next;
		i--;
	}while (i>1);
	printf("\n\n");
}
//functia care efectueaza permutarea
void josephus(int n, int m){
	nod *ptr=start;
	nod *n1,*prev=NULL;
	int count;
	while(ptr->next!=NULL)
		{
			ptr=ptr->next;
		}
	ptr->next=start;
	ptr=start;
	while(1)
		{
			count=1;
			while(count!=m)
				{
					prev=ptr;
					ptr=ptr->next;
					count++;
				}
		prev->next=ptr->next;
		//printf("%d\n ",ptr->value);
		if(start==ptr) start=ptr->next;
		//afisare(n-1);
		n--;
		free(ptr);
		if(ptr==prev)
		break;
		ptr=prev->next;
	}
}


int main()
{
	int n,m,i,count;
	nod *n1,*ptr,*prev=NULL;
	//TESTARE
	/*printf("Nr de noduri: \n");
	scanf("%d",&n);
	printf("Introduceti nr din cate in cate sa stearga: \n");
	scanf("%d",&m);
	printf("Permutarea Josephus\n");
	for(i=n;i>0;i--)	
		{
			n1=(nod *)malloc(sizeof(nod));
			n1->value=i;
			n1->next=start;
			start=n1;
			
		}josephus(n,m);
	
	*/
	for(n=100;n<=10000;n=n+100){
		for(i=n;i>0;i--)	
		{
			n1=(nod *)malloc(sizeof(nod));
			n1->value=i;
			n1->next=start;
			start=n1;
			
		}
		josephus(n,n/2);
		start=NULL;
		printf("\r%d%c",n/100,'%');
		//getch();
		
	}
	
		
	getch();

	return 0;
}