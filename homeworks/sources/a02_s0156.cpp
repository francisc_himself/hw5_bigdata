#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/** Building a heap from an unsorted array can be done in a bottom-up manner or in a top-down manner.
When using the bottom-up manner  the trees are fixed in a bottom-way in order to maintain the heap property.
For the top down method heaps are built bu succesively inserting elements into an empty hap.
The bottom-up strategy should be used when all the elements are known.
The top- down strategy should be used when elements are inserted dinamically.
For the bottom-up manner the time complexity is O(n) and for the top-down O(nlogn).
*/
int a[10001],b[10001];
int t,heapsize,n,m, i,as,c,as2,c2;
/**Function parent returns the index of the parent of an index i, given as a parameter
The index of the parent is i/2.
The array used to build the heap has the elements at indexes 1..n;
*/
int parent(int i ){
    return  i/2;
}
/**Function left returns the index of the left child of the value at index i
The index of the left child is 2*i;

*/
int left(int i ){
    return  2*i;
}
/**Function right returns the index of the right child of the value at index i
The index of the right child is 2*i+1;

*/
int right(int i){
    return 2*i+1;
}
//int a[]={0,16,14,10,8,7,9,3,2,4,1};

/**Function average has as parameters an array and the length of the array.
It returns an array of random integers using rand().
This represent the average case for building a heap.


*/
void average(int a[], int n){

 for(i=1;i<=n;i++)
 {a[i]= rand()%n;
// b[i]=a[i];
 }
}

/**The procedure max_Heapify assumes that the binary trees rooted at left(i) and right(i) are max heaps.
If a[i] is smaller than its children the max-heap property is not fulfilled
The procedure places a[i] so that the subtree rooted at index i obeys the max heap property.

*/
void max_Heapify(int a[],int i){

    int largest;
    int l=left(i);
    int r=right(i);
    c++;
    if(l<=n && a[l]>a[i]){
        largest=l;
    }
    else{
        largest=i;
    }
    c++;
    if(r<= n && a[r]>a[largest]){
        largest=r;
    }
    if (largest!=i){
         t=a[i];
        a[i]=a[largest];
        a[largest]=t;
        as+=3;
    max_Heapify(a,largest);}
}
/** The build function is used for the bottom-up method for building a heap.
The parameters are the array containing the elements, and the size.
The function max_Heapify,with the arguments the array and the index, from size/2 to 1 is called.
The elements in the subarray a[([n/2]+1...n] are all leaves of the tree.
The function goes through the other elements  and runs max_Heapify on each one
*/
void build(int a[],int size){
     as=0;
        c=0;
      for (i=(size/2);i>=1;i--)
          max_Heapify(a,i);


 }

 /** The procedure increase_key uses the index i to identify the priority-queue element whose key we want to increase
The key of a[i] is updated to its new value.
The procedure traverses  a path from the node to the root to find the place where the newly increased key should be inserted
 */
 void increase_key(int a[],int i, int key){
     c2++;
 if(key<a[i])
 printf("error");
 as2++;
 a[i]=key;

 while(i>1&&a[parent(i)]<a[i]){
           c2++;

         t=a[parent(i)];
        a[parent(i)]=a[i];
        a[i]=t;
        as2+=3;
       i=parent(i);

 }

 }
 /**The procedure max_heap_insert implements the insert operation
 The procedure expands the max-heap by adding a new key to the tree
 It calls increase_key to set the key of the new node to the correct value to mantain the max_heap property
 */
void max_heap_insert(int a[], int key){
heapsize=heapsize+1;
a[heapsize]=-1000000;
as2++;
increase_key(a, heapsize, key);

}
/**The procedure buildmaxheap is used for the top down implementation.
For the elements at indexes 2..n the procedure applies max_heap_insert.
*/
 void buildmaxheap( int a[], int n){
          as2=0;
        c2=0;
     heapsize=1;
     for(i=2;i<=n;i++)
   max_heap_insert(a,a[i]);

 }
int main(){
    //int i;
/*a[1]=4;
a[2]=1;
a[3]=3;
a[4]=2;
a[5]=16;
a[6]=9;
a[7]=10;
a[8]=14;
a[9]=8;
a[10]=7; */

            FILE *f = fopen("average.csv", "w");
            FILE *g = fopen("average2.csv", "w");

  fprintf(f, "n,bottomup,topdown\n");
 fprintf(g, "n,ass,comp,comp+ass\n");
     srand(time(NULL));
for(m=0;m<5;m++)
    for(n=100;n<=10000;n+=100){

   average(a,n);
   for(i=1;i<=n;i++)
   b[i]=a[i];

    build(a,n);
     buildmaxheap(b,n);
    // for (i=1;i<=n;i++){
         //printf("%d\n",a[i]);
           if (n<=10000) {
   fprintf(f, "%d,%d,%d\n",n,as+c,as2+c2);

   fprintf(g, "%d,%d,%d,%d\n",n,as2,c2,as2+c2);


  }
     }

    fclose(f);
    fclose(g);
return 0;
}
