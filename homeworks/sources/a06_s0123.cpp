//The function is O(nlgn)
//The program does more assignments than comparisons 

#include<iostream>
#include<conio.h>
#include<fstream>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;

int n, m, countSize;

typedef struct elem{
	int key;
	elem *left;
	elem *right;
	elem *parent;
	int size;
}NODE;
NODE* root;

NODE* generateRoot(int value, int size, NODE *genLeft, NODE *genRight){
    NODE *genRoot = new NODE;
    genRoot->key = value;		
	genRoot->size = size;	
    genRoot->left = genLeft;				
    genRoot->right = genRight;						
	if (genRight != NULL) {
        genRight->parent = genRoot;
    }
	if (genLeft != NULL) {
        genLeft->parent = genRoot;		
    }
    genRoot->parent = NULL;	
	profiler.countOperation("attr",countSize,1);
    return genRoot;
}

NODE* create(int genLeft, int genRight){
    NODE *r1, *r2;
    if (genLeft > genRight)
        return NULL;
    if (genLeft == genRight)
        return generateRoot(genLeft, 1, NULL, NULL);		
    else {
        int mid = (genLeft + genRight) / 2;
        r1 = create(genLeft, mid-1);
        r2  = create(mid+1, genRight);
        return generateRoot(mid, genRight - genLeft + 1, r1, r2);
    }
}

NODE* OS_SELECT(NODE *x, int i){
    int r;
    if (x->left != NULL)
        r = x->left->size+1;					
    else r = 1;
    if (r == i) return x;
    else {
        if (i < r) {
            return OS_SELECT(x->left, i);
        }
        else {
            return OS_SELECT(x->right, i-r);
        }
    }
}

void decSize(NODE *node){
    while (node != NULL){
		profiler.countOperation("comp",countSize);
		profiler.countOperation("attr",countSize,2);
        node->size --;			
        node = node->parent;
    }
}

NODE* minLeft(NODE* x){
	
    while(x->left != NULL){
		profiler.countOperation("comp",countSize,1);
		profiler.countOperation("attr",countSize,1);
        x = x->left;
    }
    return x;						
}

NODE* minRight(NODE* nodeN){
    NODE *node1;
	profiler.countOperation("comp",countSize,1);
    if(nodeN->right!=NULL){						
        return minLeft(nodeN->right);			
	}
	profiler.countOperation("attr",countSize,1);
    node1 = nodeN->parent;	
	profiler.countOperation("comp",countSize,1);
    while (node1 != NULL && nodeN == node1->right){										
        nodeN = node1;								
        node1 = node1->parent;
		profiler.countOperation("attr",countSize,1);
    }
    return node1;								
}

NODE* OS_DELETE(NODE* delNode){
    NODE *node1;
    NODE *node2;
	profiler.countOperation("comp",countSize,1);
    if ((delNode->left == NULL) || delNode->right == NULL) {	//if leaf
        node1 = delNode;
		profiler.countOperation("attr",countSize); //delete leaf
    }
    else
        node1 = minRight(delNode); //find successor

	profiler.countOperation("comp",countSize);
    if (node1->left != NULL) {
        node2 = node1->left;
		profiler.countOperation("attr",countSize);
    }
    else{                                           
        node2 = node1->right;
		profiler.countOperation("attr",countSize);
    }
	profiler.countOperation("comp",countSize,1);
    if (node2 != NULL){
        node2->parent = node1->parent;
    }
    if (node1->parent == NULL) //is the root
        root = node2;
    else{ 
		profiler.countOperation("comp",countSize);
		if (node1 == node1->parent->left){
			profiler.countOperation("attr",countSize);
			node1->parent->left = node2;
		}
		else{
			profiler.countOperation("attr",countSize);
			node1->parent->right = node2;
		}
	}
	profiler.countOperation("comp",countSize,1);
    if (node1 != delNode){
		profiler.countOperation("attr",countSize,1);
        delNode->key = node1->key;
    }
    return node1;
}

void printTree(NODE *node, int level){
	if(node == NULL)
		return;
	printTree(node->right, level+1);
	for(int j = 0; j < level; j++)
		printf("      ", level);
	printf("%d(%d)\n", node->key, node->size);
	printTree(node->left, level+1);	
}

void josephus(int n, int m){
    NODE *node1,*node2;
    int help = n+1;
    int middle = m;

    root = create(1, n);				

    printf("The tree is:\n");
	printTree(root,0);
	printf("\n\n\n");
    for (int i = 1; i < help; i++)	{
        node1 = OS_SELECT(root, m);	  
		printf("The key to be deleted: %d \n", node1->key);
        node2 = OS_DELETE(node1);					
        printf("The tree is:\n");
		printTree(root,0);
		printf("\n\n\n");
        decSize(node2);					
		n--;
        if (n > 0)				
            m = (m - 1 + middle) % n;
        if (m == 0) m = n;
    }

}

int main(){
	
	printf("The initial vector: ");
	for(int k=1; k<=7; k++){
		printf("%d ",k);
	}
	printf("\n");
	josephus(7,3);
	/*
		for(int k = 100; k <= 10000; k += 100){
		countSize = k;
		josephus(k, k/2);
	}
	profiler.addSeries("Operations","comp","attr");
	*/
	printf("Completely deleted!");
	getch();

	
	//profiler.showReport();
}