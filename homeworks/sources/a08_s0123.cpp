// The program runs in o(n) time!
// The function grows linearly

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

#include "Profiler.h"
Profiler profiler("demo");

#define INF INT_MIN

typedef struct node {
	int info;
	int rank;
	node * next;
	node * prev;
}NODE;

typedef struct edge {
	int nodeS;
	int nodeF;
}EDGE;

NODE *sets[10000][2];
int genEdges[10000][10000];

void generate(int v){
	NODE *x = (NODE*)malloc(sizeof(NODE));
	x->rank = 1;
	x->info = v;
	x->prev= x;
	x->next = NULL;

	sets[v][0] = sets[v][1] = x;
}

int find(int x){
	return  (sets[x][0]->prev)->info;
}

void unionSet( int info1, int info2){
	info1 = find(info1);
	info2 = find(info2);
	NODE *help;

	if( sets[info1][0]->rank >= sets[info2][0]->rank ) {
		sets[info2][0]->rank = INF;
		help = sets[info2][0];

		while( help != NULL ) {
			help->prev = sets[info1][0];
			sets[info1][0]->rank++;
			sets[info1][1]->next = help;
			sets[info1][1] = help;
			help = help->next;
		}
		sets[info2][1] = NULL;
	}
	else {
		sets[info1][0]->rank = INF;
		
		NODE *help = sets[info1][0];

		while( help != NULL ) {
			help->prev = sets[info2][0];
			sets[info2][0]->rank++;
			sets[info2][1]->next = help;
			sets[info2][1] = help;
			help = help->next;
		}
		sets[info1][1] = NULL;
	}
}


void connComp( edge *edges, int edgesNr){
	for( int i = 0; i < 10000; i++){
		generate(i);
		profiler.countOperation("operations",edgesNr);
	}
	
	for( int i = 0; i < edgesNr; i++){
		if( find(edges[i].nodeS) != find(edges[i].nodeF)) {
			unionSet(edges[i].nodeS,edges[i].nodeF);
			profiler.countOperation("operations",edgesNr,3);
		}
		profiler.countOperation("operations",edgesNr,2);
	}

}

void createEdges( edge *edges, int edgesNr){
	int count=0, start, final;

	for (int i =0; i< 10000; i++) 
        for (int j =0; j< 10000; j++)
            genEdges[i][j]=0; //init, no edges

	while (count < edgesNr) {
		start = rand()%1000;
		final = rand()%1000;
		if (genEdges[start][final] == 0 && genEdges[start][final] == 0 && start != final) {
            genEdges[final][start] = genEdges[start][final] = 1;
			edges[count].nodeS = start;
            edges[count].nodeF = final;
			//printf("%d  %d\n", edges[count].nodeS, edges[count].nodeF);
            count++;
        }
	}
}

int main() {
	/*
	EDGE edges[60000];
	printf("Please wait!\n");
	
    for (int edgesNr = 10000; edgesNr<=60000; edgesNr+=1000)
    {
        createEdges(edges,edgesNr);
        connComp(edges,edgesNr);
		profiler.createGroup("Function calls","operations");
    }
	
	*/
	EDGE edges[5];
	createEdges(edges,5);
	connComp(edges,5);
		//printf("%d  %d\n", &sets[i][0], &sets[i][1]);
	
	printf("Press enter!");
	getch();

	profiler.showReport();
	return 0;
}