#include<iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
int n, a[100],root,x;
typedef struct arbore
{
	int key;
	int nrcopil;
	arbore* copilas[100];
};
arbore *arb[100];
typedef struct binar
{
	int key;
	int pozitie;
	binar *primcop;
	binar *fraprim;
};
binar *bin[100];
void citire()
{
	printf("nr de noduri = ");
	scanf("%d",&n);
	printf("Input Parent representation: \n");
	for(int i=0;i<n;i++)
	{
		printf("a[%d]=",i);
		scanf("%d",&a[i]);
		if (a[i]==-1)
			root = i;
		arb[i] = new arbore;
		arb[i]->nrcopil=0;
		bin[i] = new binar;
	}
	printf("Parent representation: \n");
	for(int i=0;i<n;i++)
		printf("%d   ",a[i]);
	printf("\n");
	for(int i=0;i<n;i++)
		printf("%d   ",i+1);
	printf("\n");
}

void transform1()
{
	for( int i=0; i < n; i++ )
	{
		arb[i] -> key = i + 1;
		if( a[i] != -1 )
		{
			arb[a[i]-1]->copilas[arb[a[i]-1]->nrcopil] = arb[i];
			arb[a[i]-1] -> nrcopil++;
			bin[i] -> pozitie = arb[a[i]-1]->nrcopil-1;
	
		}
	}
}
void transform2()
{
	for( int i=0; i < n; i++ )
	{
		bin[i] -> key = i+1;
		if( arb[i] ->nrcopil >= 1 )
		{
			bin[i] -> primcop = bin[arb[i]->copilas[0]->key-1];
		}
		else 
		{
			bin[i] -> primcop = NULL;
		}
		if(a[i] != -1 && bin[i]->pozitie < arb[a[i]-1]->nrcopil - 1)
		{
			bin[i] -> fraprim= bin[arb[a[i]-1] -> copilas[bin[i]->pozitie + 1]->key -1];
		}
		else 
			bin[i] -> fraprim=NULL;

	}
}
void prettyprint(binar* q, int level)
{
	if(q!=NULL)
	{ 
		
		for(int j=0; j<level; j++) 
			printf("	");
		printf("%d", q -> key);
		printf("\n");
		prettyprint(q->primcop,level+1);
		prettyprint(q->fraprim,level);
	}
}
void printmulty(arbore* q, int level)
{	
	if (q!=NULL)
	{
		printf("%d :",q->key);
		if(q->nrcopil>=1)
			for(int j=0;j<q->nrcopil;j++)
				printf("%d ",q->copilas[j]->key);
		else
			printf(" nu are copii.");
		printf("\n");
		for(int j=0;j<q->nrcopil;j++)
			printmulty(q->copilas[j],level);
		level++;
	}

}
void main()
{
	citire();
	transform1();
	printf("MultiwayTree:\n");
	printmulty(arb[root],0);
    transform2();
	printf("BinaryTree:\n");
	prettyprint(bin[root],0);
	getch();
}

