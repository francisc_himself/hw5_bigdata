#include <cstdio>
#include <conio.h>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

//n=size| nr_swp=assignments| nr_cmp=comparisons| nr_tot=assigm+comp
int nr_swp=0, nr_cmp=0, nr_tot=0;
int const minArr=100, maxArr=5000;

int Left(int i) {
	return 2*i;
}
int Right(int i) {
	return 2*i + 1;
}
void swap(int arr[],int x, int y){
	int temp = arr[x];
	arr[x] = arr[y];
	arr[y] = temp;
}



void HeapifyTD(int heap_size,int arr[],int i){
	int l = Left(i); //left child
	int r = Right(i); //right child
	int largest;

	if(l <= heap_size && arr[l] > arr[i])
		largest = l;
	else
		largest = i;
	if(r <= heap_size && arr[r] > arr[largest])
		largest = r;
	if(largest != i){//unul din cei 2 fii este mai mare decat arr[i]
		swap(arr,i,largest);//swap parinte cu child
		HeapifyTD(heap_size,arr,largest);
	}
}
void HeapifyBU(int heap_size,int arr[],int i){
	int l = Left(i); //left child
	int r = Right(i); //right child
	int smallest;

	if(l <= heap_size && arr[l] < arr[i])
		smallest = l;
	else
		smallest = i;
	if(r <= heap_size && arr[r] < arr[smallest])
		smallest = r;
	if(smallest != i){//unul din cei 2 fii este mai mic decat arr[i]
		swap(arr,i,smallest);//swap parinte cu child
		HeapifyBU(heap_size,arr,smallest);
	}
}

void OutputHeap(int arr[], int heap_size) {
	 // Find the largest power of two, That is the depth
	int iDepth = 0;
	int iCopy = heap_size;
	while (iCopy > 0) {
		iCopy >>= 1;
		++iDepth;
	}

	int iMaxWidth = (1 << iDepth);
	int iCharWidth = 4*iMaxWidth;
	int iEntry = 0;

	for (int i = 0; i < iDepth; ++i) {
		int iPowerOf2 = (1 << i);
		for (int j = 0; j < iPowerOf2; ++j) {
			int iSpacesBefore = ((iCharWidth/(1 << (i + 1))) - 1);
			 // Spaces before number
			for (int k = 0; k < iSpacesBefore; ++k) {
				printf(" ");
			}
			 // Output an extra space if the number is less than 10
			if (arr[iEntry] < 10) {
				printf(" ");
			}
			 // Output the entry and the spaces after it
			printf("%d",arr[iEntry]);
			++iEntry;
			if (iEntry >= heap_size) {
				printf("\n");
				return;
			}
			for (int k = 0; k < iSpacesBefore; ++k) {
				printf(" ");
			}
		}
		printf("\n\n");
	}
}

void build_heapTD(int arr[],int heap_size){//Top-down
	for(int i=heap_size/2;i>=0;i--)
		HeapifyTD(heap_size,arr,i);
}
void build_heapBU(int arr[],int heap_size){//Bottom-up
	for(int i=heap_size/2;i>=0;i--)
		HeapifyBU(heap_size,arr,i);
}
void HeapsortTD(int arr[],int heap_size){//Heapsort
	build_heapTD(arr,heap_size);//Top-down
	for(int i=0;i<heap_size;i++)//heap in array
		printf("%d ",arr[i]);
	printf("\n_______________________________________________________________________\n");
	OutputHeap(arr,heap_size);
	printf("_______________________________________________________________________\n");
	for(int i=heap_size-1;i>=1;i--){
		swap(arr,0,i);
		HeapifyTD(i-1,arr,0);
	}
}
void HeapsortTD_Afis(int arr[],int heap_size){//Heapsort
	build_heapTD(arr,heap_size);//Top-down
	for(int i=0;i<heap_size;i++)//heap in array
		printf("%d ",arr[i]);
	printf("\n_______________________________________________________________________\n");
	OutputHeap(arr,heap_size);
	printf("_______________________________________________________________________\n");
	for(int i=heap_size-1;i>=1;i--){
		swap(arr,0,i);
		HeapifyTD(i-1,arr,0);
	}
}
void HeapsortBU(int arr[],int heap_size){//Heapsort
	build_heapBU(arr,heap_size);//Bottom-up
	for(int i=0;i<heap_size;i++)//heap in array
		printf("%d ",arr[i]);
	printf("\n_______________________________________________________________________\n");
	OutputHeap(arr,heap_size);
	printf("_______________________________________________________________________\n");
	for(int i=heap_size;i>=1;i--){
		swap(arr,0,i);
		HeapifyBU(i-1,arr,0);
	}
}
void HeapsortBU_Afis(int arr[],int heap_size){//Heapsort
	build_heapBU(arr,heap_size);//Bottom-up
	for(int i=0;i<heap_size;i++)//heap in array
		printf("%d ",arr[i]);
	printf("\n_______________________________________________________________________\n");
	OutputHeap(arr,heap_size);
	printf("_______________________________________________________________________\n");
	for(int i=heap_size;i>=1;i--){
		swap(arr,0,i);
		HeapifyBU(i-1,arr,0);
	}
}

int main(){
	int arr[] = {1,4,10,9,7,2,3,8,5,6};
	int arr2[] = {1,4,10,9,7,2,3,8,5,6};
	int tdArr[maxArr],buArr[maxArr];
	int heap_size = sizeof(arr)/sizeof(arr[0]);
	int TDavg_swp,TDavg_cmp,TDavg_tot, BUavg_swp,BUavg_cmp,BUavg_tot;
	ofstream data;

	HeapsortTD_Afis(arr,heap_size);
	for(int i=0;i<heap_size;i++)//array sortat top-down
		printf("%d ",arr[i]);
	printf("\n**********************************************************************\n");
	HeapsortBU_Afis(arr2,heap_size-1);
	for(int i=0;i<heap_size;i++)//array sortat bottom-up
		printf("%d ",arr2[i]);



	data.close();
return 0;
	getch();
}
