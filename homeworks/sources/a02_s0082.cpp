
#include<stdlib.h>
#include<conio.h>
#include<stdio.h>

#define MAX_SIZE 1000;
const int size = 1000;

// Funtie pentru afisare
void afisare(int a[ size ], int size)
{
	int i;
	for( i = 1; i < size ; i++ )
	{
		printf("%d",a[i]); 
		printf("\n");
	}
	printf("\n");
}

//Functie pentru constrirea heap-ului
void Constr_heap( int a[ size ], int size, int i )
{
	int st = 2*i, dr = 2*i + 1, min;
	if(( st <= size ) && ( a[ st ] < a[ i ] ) )
	{
		min = st;
	}

	else
	{
		min = i;
	}

	if ( ( dr <= size ) && ( a[ dr ] < a[ min ] ) )
	{
		min = dr;
	}

	if( min != i )
	{
		int aux = a[ i ];
		a[ i ] = a[ min ];
		a[ min ] = aux;
		Constr_heap( a, size, min );
	}
}



void Build_Heap_BU(int a[ size ], int size)
{
	int i;
	for( i = ( size/2 ) ; i >= 1 ; i-- )
	{
		Constr_heap( a, size, i );
	}
}

//functie pentru adaugare a unui element in heap
void H_Push(int a[ size ], int &size, int x )
{
	 int i = size, aux;
	 size ++;
	 a[ size ] = x;
   
	while( i > 1 && a[ i ] < a[ i/2 ] )
	{
		aux = a[ i ];
		a[ i ] = a[ i/2 ];
		a[ i/2 ] = aux;
		i = i/2;
	}

}


void Build_Heap_TD( int a[ size ] , int sizeA, int b[ size ], int &sizeB )
{
	int i;
	for( i = 1 ; i <= sizeA ; i++ )
	{
		H_Push( b, sizeB, a[ i ] );
	}
}

int main(void)
{
	int i, a[ 10 ], sizea = 10, b[ 10 ], sizeb = 0;
	FILE *fout = fopen("text.csv","w");

	a[1] = 6;
	a[2] = 7; 
	a[3] = 22; 
	a[4] = 10;
	a[5] = 15;
	a[6] = 17;
	a[7] = 2;
	a[8] = 8;
	a[9] = 19;
	a[10] = 5;

	//afisam heap-ul initial
	printf("sirul de numere:\n");
	afisare( a,sizea+1 );
	printf("\n");

	//construim heap-ul bottm-up
	printf("Build Heap Bu:\n");
	Build_Heap_BU( a,sizea );
	afisare( a,sizea );
	printf("\n");

	//construim heap-ul top-down
	printf("Build Heap Td:\n");
	Build_Heap_TD( a, sizea, b, sizeb );
	afisare( b, sizea );
	printf("\n");

	getch();
	fclose(fout);
	return 0;
}