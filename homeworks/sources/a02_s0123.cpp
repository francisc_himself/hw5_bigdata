//The top-down method needs more operations so the bottom up method is more efficients

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;
#define NEG_INF INT_MIN

int a[10000], v[10000], length, heapsize,m;

int left(int );
int right(int);
int parent(int);
void MAXHEAPIFY(int [],int);
void BUILDMAXHEAPBU(int []);
void HEAPSORT(int []);
int HEAPMAX(int []);
int HEAPEXTRACTMAX(int []);
void HEAPINCREASEKEY(int [], int, int);
void MAXHEAPINSERT(int[], int);
void BUILDMAXHEAPTD(int []);
void copy();
void printOut(int , int);
void averageCase();

//left leaf
int left(int i){
	return 2*i;
}

//right leaf
int right(int i){
	return 2*i+1;
}

//parent of leaf
int parent(int i){
	return i/2;
}

//BOTTOM UP method
void MAXHEAPIFY(int heap[], int i){
	int l, r, largest, help;
	l = left(i);
	r = right(i);
	profiler.countOperation("assCompBU",length);
	if(l <= heapsize && a[l] > a[i])
		largest = l;
	else 
		largest = i;
	profiler.countOperation("assCompBU",length);
	if(r <= heapsize && a[r] > a[largest])
		largest = r;
	if(largest != i)
	{
		profiler.countOperation("assCompBU",length,3);
		help = a[i];
		a[i] = a[largest];
		a[largest] = help;
		MAXHEAPIFY(a, largest);
	}
}

void BUILDMAXHEAPBU(int a[])
{
	heapsize = length;
	for(int i = length/2; i >= 1; i--)
		MAXHEAPIFY(a, i);
}

//TOP DOWN METHOD
int HEAPMAX(int a[])
{
	return a[1];
}

int HEAPEXTRACTMAX(int a[])
{
	int maxim;
	profiler.countOperation("assCompTD",length);
	if(heapsize < 1)
		printf("\nError! Heap underflow!");
	profiler.countOperation("assCompTD",length,2);
	maxim = a[1];
	a[1] = a[heapsize];
	heapsize--;
	MAXHEAPIFY(a, 1);
	return maxim;
}

void HEAPINCREASEKEY(int a[], int i, int key)
{
	int help;
	profiler.countOperation("assCompTD",length);
	if(key < a[i])
		printf("\nNew key is smaller than current key");
	profiler.countOperation("assCompTD",length,2);
	a[i] = key;
	while(i > 1 && a[parent(i)] < a[i])
	{
		profiler.countOperation("assCompTD",length,3);
		help = a[i];
		a[i] = a[parent(i)];
		a[parent(i)] = help;
		i = parent(i);
	}
}

void MAXHEAPINSERT(int a[], int key)
{
	heapsize++;
	profiler.countOperation("assCompTD",length);
	a[heapsize] = NEG_INF;
	HEAPINCREASEKEY(a, heapsize, key);
}

void BUILDMAXHEAPTD(int a[])
{
	heapsize = 1;
	for(int i = 2; i <= length; i++)
		MAXHEAPINSERT(a, a[i]);
}

//make a copy of the array
void copy()
{
	for(int i=1; i <= length; i++)
		v[i] = a[i];
}

//print out the array
void printOut(int m,int level)
{
	
	int j;
        printOut(2*m,level+1);
        for(j=0;j<=level;j++)
            printf("         ");
        printf("%s\n", a[m]);
		m++;
        printOut(2*m+1,level+1);

}

//the average case for heap sorts
void averageCase()
{
	printf("Creating information for the chart.. please wait!");
	for(int i=1; i <= 5; i++)
		for(length = 100; length <= 10000; length += 100)
		{
			FillRandomArray(a,length);
			copy();
			BUILDMAXHEAPBU(a);
			BUILDMAXHEAPTD(v);
		}
	profiler.createGroup("HeapSort","assCompTD","assCompBU");
	profiler.showReport();
}

int main()
{
	averageCase();
//	printOut(a[0],3);
  //getch();
	return 0;
}

