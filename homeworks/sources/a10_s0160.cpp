/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Cerinta:
	Se cere implementarea corecta si eficienta a algoritmului DFS pentru un graf reprezentat prin listele vecinilor.
Etichetarea muchiilor pentru un graf orientat cu un numar dat de noduri. Muchiile vor fi etichetate in muchii de arbore,
muchii inainte, muchii inapoi si muchii transversale.

Algoritm de functionare DFS:
	Pentru fiecare varf se parcurge primul dintre vecinii lui neparcursi inca. Dupa vizitarea varfului initial x1, se exploreaza
primul varf adiacent lui, fie acesta x2, se trece apoi la primul varf adiacent cu x2 si care nu a fost parcurs inca , s.a.m.d.
Fiecare varf se parcurge cel mult odata.

Interpretari:
	DFS are eficenta O(E+V);  E= numarul de muchii, V=numarul de varfuri
	DFS_VISIT are eficenta O(E);


*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>


#define nr_vf  200   //numarul maxim de noduri din graf

//definirea culorilor
#define alb 0        
#define ***ANONIM*** 1
#define negru 2

typedef struct _NOD
{
	int key;            
	struct _NOD *urm;   
}NOD;                    

NOD *Adj[nr_vf];         //vector in care fiecare casuta va retine referinta la lista de adiacenta a vecinilor nodului din graf aflat la indexul respectiv    

int top[nr_vf];

int pi[nr_vf],d[nr_vf],f[nr_vf],cul[nr_vf];  //vectorii de parinte,timpul descoperirii, timpul terminarii, si culoare
int timp=0;                                    // variabila ce retine timpul

int numar_de_varfuri; // retine numarul de varfuri ale grafului        

long nr_operatii=0;

int nr_top=0;

int ok;

//Algoritm de parcurgere si etichetare muchii
void DFS_VISIT(int u)
{
  NOD *v;
  

  cul[u]=***ANONIM***;  
  timp++;      
  d[u]=timp;  

  v=Adj[u];    
  nr_operatii+=4;
  while(v!=0)    //parcurgere lista de adiacenta a vecinilor
  {
	  nr_operatii++;
	  if(cul[v->key]==alb)     //testeaza daca varful e nevizitat
		  {
			  pi[v->key]=u;  
			  nr_operatii++;
			  printf("\n (%d,%d) muchie arbore",u,v->key);
			  DFS_VISIT(v->key);
	       }
	       else 
	       {   
			 nr_operatii++;
		     if(cul[v->key]==***ANONIM***)   
			 {	
				 printf("\n (%d,%d) muchie inapoi",u,v->key);        //afisare si etichetare muchii
				 ok=0;
			 }
			 else
	         {
				nr_operatii++;
		        if(d[u]>f[v->key])
				{
					printf("\n (%d,%d) muchie transversala",u,v->key); //afisare si etichetare muchii
					ok=0;				
				}
				else
				{	
					printf("\n (%d,%d) muchie inainte",u,v->key);
					ok=0;
				}
			 }
	       }
	 v=v->urm;  
	 nr_operatii++;
  }

  cul[u]=negru;  
  timp++;       
  f[u]=timp;   
  nr_operatii+=3;
 
  if (ok==1)
  {
		nr_top++;
		top[nr_top]=u;
  }
  else
	  printf("\nNu se poate sorta topologic");
  
}

//algoritmul de parcurgere in adancime
void DFS()
{
   int u;

   for(u=1;u<numar_de_varfuri+1;u++)  
   {
	   cul[u]=alb;
       nr_operatii++;
   }
   timp=0;   
   for(u=1;u<numar_de_varfuri+1;u++)
   {   
	  nr_operatii++;
	  if(cul[u]==alb)
	      DFS_VISIT(u);
	   
   }

}

//algoritm de generare grafuri aleatoare
void generare_graf(int numar_varfuri)
{
	int u,v,nr,ok;
	int numar_muchii=2*numar_varfuri;
	NOD *vecin,*p,*q;
  
	for(nr=0;nr<nr_vf;nr++) 
		Adj[nr]=0;

	numar_de_varfuri=numar_varfuri;
  
	for(nr=0;nr<numar_muchii;nr++)
	{
    
		ok=0;
		while(ok==0)
		{
			ok=1;
	
			u=rand()%(numar_varfuri+1);
			v=rand()%(numar_varfuri+1);

			if((v==0)||(u==0))
				ok=0;
			else
			{
				if(Adj[u]==0)
				{
					p=(NOD*)malloc(sizeof(NOD));
					p->key=v;
					p->urm=0;
					Adj[u]=p;
				}
				else
				{
					vecin=Adj[u];
					while(vecin!=0)
					{
						if(vecin->key==v)
							ok=0;
						q=vecin;
						vecin=vecin->urm;
					}
					if(ok==1) 
					{
						p=(NOD*)malloc(sizeof(NOD));
						p->key=v;
						p->urm=0;
						q->urm=p;
					}
				}
			}
		}
	}
}

//functie folosita la afisarea grafului sub forma de varfuri carora sunt atasate listele de adiacenta ce contin vecinii acestora
void afisare()   
{
    int i;
	NOD *q;

	for(i=1;i<numar_de_varfuri+1;i++)
	{
		printf("\n Vecinii lui %d: ",i);
		q=Adj[i];
		while(q!=0)
		{ 
			 printf(" %d ",q->key);
			 q=q->urm;
		}
	}
    
	printf("\n");
}

//generam un graf de 6 varfuri pe care verificam daca algoritmul functioneaza
void test_caz_particular()  
 {
	NOD *p,*q,*r;
	int nrvarfuri=6;

    //lista adiacenta nod 1
	p=(NOD*)malloc(sizeof(NOD));
	p->key=2;
	p->urm=0;

	q=(NOD*)malloc(sizeof(NOD));
	q->key=6;
	q->urm=0;
	p->urm=q;

	Adj[1]=p;
   
	//lista adiacenta nod 2
	p=(NOD*)malloc(sizeof(NOD));
	p->key=3;
	p->urm=0;

	q=(NOD*)malloc(sizeof(NOD));
	q->key=5;
	q->urm=0;
	p->urm=q; 

    Adj[2]=p;
   
	//lista adiacenta nod 3
    p=(NOD*)malloc(sizeof(NOD));
	p->key=4;
	p->urm=0;

    Adj[3]=p;
   
	Adj[4]=NULL;
    
	Adj[5]=NULL;
   
	Adj[6]=NULL;

	generare_graf(nrvarfuri);
	afisare();
	printf("\n");
	
	ok=1;
	DFS();
}

int main()
{
	FILE *f_varf_const;
	FILE *f_muchie_const;
	int i;
	int nr_muchii = 0;
	int nr_varfuri = 0;

	f_varf_const = fopen("varf_constant.csv","w");
	f_muchie_const = fopen("muchie_constant.csv","w");

	//pe caz particular
	//test_caz_particular();

	printf("\n");
	for(i=nr_top;i>=1;i--)
		printf("%d ",top[i]);

	//--------------------------pe caz general --------------------------------------//
	//pt nr de varfuri constante=100 si variem numarul de muchii de la 1000 la 5000 cu un increment de 100
	
	srand(time(NULL));
	nr_varfuri = 100;
	for(i = 1000; i < 5000; i += 100)
	{
		printf("i=%d\n",i);
		generare_graf(nr_varfuri);
		DFS();
		fprintf(f_varf_const,"%d,%d\n",i,nr_operatii);
		
	}
	
	
	//pt nr de muchii constante=9000 si variem numarul de varfuri de la 100 la 200 cu un increment de 10
	nr_muchii = 9000;
	nr_operatii = 0;
	for(i = 110; i <= 200; i += 10)
	{
		printf("i=%d\n",i);
		generare_graf(nr_varfuri);
		DFS();
		fprintf(f_muchie_const,"%d,%d\n",i,nr_operatii);
		
	}
	
	fclose(f_varf_const);
	fclose(f_muchie_const);

	
	_getch();
	return 0;
}