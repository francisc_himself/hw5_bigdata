

//#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include  <stdlib.h>
#include <time.h>
#include<math.h>


int atr1=0,comp1=0,atr2=0,comp2=0;


void generator_aleator(int* v, int n)
{
	
	int i; //contor
	
	srand (time(NULL));
	for (i=0;i<n;i++)
	{
		
		*(v+i) = rand() % 100000;
	}

}


int index_min(int *a,int i,int nr_elem)
{
    if(2*i>nr_elem)
    {
        return i;
    }

    int min =a[i]<a[2*i]?i:(2*i);

    if (2*i+1>nr_elem) return min;
    min=a[min]<a[2*i+1]?min:(2*i+1);
    return min;
}

void Rec_H(int A[], int n, int i)//reconstructie heapului
{
	int ind=0,x,y,aux;
	x=2*i;
	y=2*i+1;
	ind=index_min(A,i,n);
	comp2+=2;

	if(ind!=i)
	{
		aux=A[i];
		A[i]=A[ind];
		A[ind]=aux;
		atr2+=3;
	Rec_H(A,n,ind);
	}
}
int H_Pop(int A[],int n)
{
	int x;
	x=A[1];
	A[1]=A[n];
	atr1++;
	n=n-1;
	Rec_H(A,n,1);
	return x;
}

void H_Push(int A[], int n, int x)
{
	n=n+1;
	A[n]=x;
	atr1++;
	int i=n;	
	while(i>1 && A[i]<A[i/2] && comp1++)
	{
		
		int y=A[i];
		A[i]=A[i/2];
		A[i/2]=y;
		i=i/2;
		atr1+=3;
	}
}


void afiseaza(int v[],int nr_elem,int i)
{
    if(i>nr_elem) return;

    int tab = i;

    afiseaza(v,nr_elem,2*i+1);

    while (tab>0){
        printf("   ");
        tab=tab/2;
    }
    printf("%d\n",v[i]);

    afiseaza(v,nr_elem,2*i);
}


void Top_Down(int A[], int B[],int n)
{
	int m=0;
	for(int i=1;i<=n;i++)
		H_Push(B,i-1,A[i]);

}

void bottom_up(int A[],int n)
{
    int i;
    for(i=n/2;i>0;i--)
    {
        Rec_H(A,n,i);

    }
}



int main()
{
	int v[10000], g[10000], heap[10000];
	int n, j,k;
	FILE *f;

	/*printf("n=");
	scanf("%d", &n);

	for(int i=1;i<=n;i++)
		{
			printf("\nv[%d]=", i);
			scanf("%d", &v[i]);
		}
		*/

	f=fopen("fisier.csv", "w");
	fprintf(f, "n, atr_TD, comp_TD, atr+comp TD, atr BU, comp BU, atr+comp BU \n");

	for(n=100; n<=10000; n=n+100)
	{
		atr1=0;comp1=0;atr2=0;comp2=0;
		
		for(j=0;j<5;j++)
		{
			generator_aleator(v,n);
			for(int k=0; k<n; k++)
			{
				g[k]=v[k];
			}

			Top_Down(v,heap,n);
			bottom_up(g,n);
		}
	fprintf(f, "%d, %d, %d, %d, %d, %d, %d \n", n, atr1/5, comp1/5, (atr1+comp1)/5, atr2/5, comp2/5, (atr2+comp2)/5);
	printf("%d\n",n);
	}


	/*
	Top_Down(v,heap, n);
	afiseaza(heap,n,1);

	bottom_up(v, n);

	printf("%d, %d " , atr, comp);
	printf("%d, %d " , atr2, comp2);*/

	getch();
}
