
/* ***ANONIM*** ***ANONIM***-***ANONIM*** gr.***GROUP_NUMBER***
	Algoritmul urmator implementeaza parcurgerea in latime a grafurilor, adica se porneste dintr-un nod al grafului si se viziteaza pe rand
	toti vecinii acestuia.Graful este parcurs complet daca dupa apelul BFS culoarea tuturor nodurilor este negru. Deoarece algoritmul 
	traverseaza graful nod cu nod eficienta lui este O(V) si muchie cu muchie O(E) adica eficienta totala O(V+E).
*/
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("demo");

#define White 0
#define Gray 1
#define Black 2
#define V_max 10000
#define E_max 60000
#define inf 1000000
typedef struct _graphNODE {
	struct _graphNODE *parent;//(pi)
	int distance;//(d)
	int color;//(c)
	int key;
}GRAPH_NODE;

typedef struct _NODE{
	int key;
	struct _NODE *next;
}NODE;

typedef struct _Graph{
	int nrV,nrM;
}Graph;


NODE *adj[V_max];
GRAPH_NODE graph[V_max],*Q[V_max];
int edges[E_max];
int head,tail,lung;

int count;

void generate(int n, int m){
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE *nod=new NODE;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;
	}
}

void ENQUEUE(GRAPH_NODE *x){
	Q[tail]=x;
	if(tail==V_max)
		tail=1;
	else tail++;
	lung++;
}
GRAPH_NODE* DEQUEUE(){
	GRAPH_NODE *x;
	//x=(GRAPH_NODE*)malloc(sizeof(GRAPH_NODE));
	x=Q[head];
	if(head==V_max)
		head=1;
	else head++;
	lung--;
	return x;
}

void BFS(Graph *G,GRAPH_NODE *s){
	for(int i=0;i<G->nrV;i++){
		profiler.countOperation("operatii",count,1);
		if(&graph[i]!=s){
			graph[i].color=White;
			graph[i].distance=inf;
			graph[i].parent=NULL;
			graph[i].key=i;
		}
	}
	s->color=Gray;
	s->distance=0;
	s->parent=NULL;
	head=0;tail=0;lung=0;
	ENQUEUE(s);
	profiler.countOperation("operatii",count,2);
	while(lung!=0){
		
		GRAPH_NODE *u=DEQUEUE();
		profiler.countOperation("operatii",count,1);
		NODE *v=adj[u->key];
		while(v!=NULL){
			//printf("%d",lung);
			profiler.countOperation("operatii",count,1);
			if(graph[v->key].color==White){
				graph[v->key].color=Gray;
				graph[v->key].distance=u->distance+1;
				graph[v->key].parent=u;
				profiler.countOperation("operatii",count,1);
				ENQUEUE(&graph[v->key]);
			}
			if(v!=NULL)
				v=v->next;
		}
		u->color=Black;
	}
}
void afisAdj(int n){
	
	for(int i=0;i<n;i++){
		NODE *v=adj[i];
		printf("%d",i);
		while(v!= NULL){
			printf("->");
			printf("%d",v->key);
			if(v!=NULL)
			{	v=v->next;
			}
		}
		printf("\n");
	}
}
void print(GRAPH_NODE *s,GRAPH_NODE *v){
	
	if(s==v) printf("%d",s->key);
	else if(v->parent==NULL){ printf("nu e drum");return;}
		else {
			print(s,v->parent);
			printf("->%d ",v->key);
			}
	
}
void main(){
	Graph *g;
	g=(Graph*)malloc(sizeof(Graph));
	generate(5,9);

	g->nrV=5;
	g->nrM=9;
	
	BFS(g,&graph[0]);
	afisAdj(g->nrV);
	for(int i=0;i<g->nrV;i++)
		for(int j=0;j<g->nrV;j++)
		{printf("\ndrum de la %d la %d ",graph[i].key,graph[j].key);
		print(&graph[i],&graph[j]);}
		
	count=0;

	//for(int nrM=1000;nrM<=5000;nrM+=100){
	//	count=nrM;
	//	g->nrV=100;
	//	g->nrM=nrM;
	//	generate(100,nrM);
	//	for(int j=0;j<100;j++)
	//		BFS(g,&graph[j]);
	//}
	//printf("varfuri fix");
	//profiler.showReport();
	
	//count=0;
	//for(int nrV=100;nrV<=200;nrV+=10){
	//	{
	//		g->nrV=nrV;
	//		g->nrM=9000;
	//		generate(nrV,9000);
	//		count=nrV;
	//		for(int j=0;j<nrV;j++)
	//		BFS(g,&graph[j]);
	//	}
	//}
	//printf("muchii Fix");
	//profiler.showReport();
}