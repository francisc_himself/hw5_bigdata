#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"
#include "stddef.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 7 - Transforming Multi-way Trees
/***************************************************/

#define MAX_NR 9

Profiler profiler("MultiwayTester");

int parents[MAX_NR+1];
int size;

typedef struct node1type
{
	int value;
	int nbChild;
	int actualChild;
	struct node1type* parent;
	struct node1type** children;
} Node1T;

typedef Node1T* Node1Ptr;

Node1Ptr arr[MAX_NR+1];

Node1Ptr root1;

typedef struct node2type
{
	int value;
	node2type* left;
	node2type* right;
}Node2T;

typedef Node2T* Node2Ptr;

Node2Ptr root2;


//taking O(3n) <=> O(n)
void t1(int n)
{
	int i;
	int j;
	//create the nodes
	for(i = 1; i <= n; i++)
	{
		arr[i] = (Node1Ptr)malloc(sizeof(Node1T));
		arr[i]->value = i;
		arr[i]->nbChild = 0;
		arr[i]->actualChild = 0;
		arr[i]->children = NULL;
		arr[i]->parent = NULL;
	}
	//count the number of children
	for(i = 1; i <= n; i++)
	{
		if(parents[i] != -1)
		{
		arr[parents[i]]->nbChild = arr[parents[i]]->nbChild + 1;
		arr[parents[i]]->actualChild = 0;
		}
		else
		{
			root1 = arr[i];
		}
	}
	//allocate children vector
	for(i = 1; i <=n; i++)
	{
		arr[i]->children = (Node1Ptr*)malloc((arr[i]->nbChild+1) * sizeof(Node1Ptr));
		for(j = 0; j <= arr[i]->nbChild; j++)
		{
			arr[i]->children[j] = NULL;
		}
	}
	//set the pointers
	for(i = 1; i <= n; i++)
	{
		if(parents[i] != -1)
		{

		arr[i]->parent = arr[parents[i]];
		arr[parents[i]]->children[arr[parents[i]]->actualChild] = arr[i];
		//printf("LOG.added %d ", arr[parents[i]]->children[arr[parents[i]]->actualChild]->value);
		arr[parents[i]]->actualChild = arr[parents[i]]->actualChild + 1;
		}
	}
}

//TODO AVOID UNINITIALIZED CALLS

Node2Ptr transformNode(Node1Ptr node1, int act)
{
	Node2Ptr node2 = (Node2Ptr)malloc(sizeof(Node2T));
	if(node1 != NULL)
	{	
	node2->value = node1->value;
	if(act < node1->parent->nbChild)
	{
	node2->right = transformNode(node1->parent->children[act+1],act+1);
	}
	else
	{
		node2->right = NULL;
	}
	node2->left = transformNode(node1->children[0],0);
	return node2;
	}
	else
	{
		return NULL;
	}
}

void t2()
{
	root2 = (Node2Ptr)malloc(sizeof(Node2T));
	root2->value = root1->value;
	root2->right = NULL;
	root2->left = transformNode(root1->children[0], 0);
}

void prettyprint(Node2Ptr node, int level)
{
	if(node != NULL)
	{
		Node2Ptr aux = node;
		for(int i = 0; i <= level; i++)
		{
			printf("    ");
		}
		printf("%d\n",aux->value);
		prettyprint(node->left,level+1);
		prettyprint(node->right,level);
	}
}

void main()
{
	size = 9;
	parents[1] = 2; parents[2] = 7; parents[3] = 5; parents[4] = 2;
	parents[5] = 7; parents[6] = 7; parents[7] = -1; parents[8] = 5; parents[9] = 2;
	t1(size);
	t2();
	printf("After transformation 2:\n\n");
	prettyprint(root2,0);
	getch();
}