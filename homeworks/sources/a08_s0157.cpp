#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include <time.h>   

int V=10000,E,nr;
int make[65],find[65],unions[65];
//int a[10005][10005];
int ax[60005],ay[60005],edg;

bool isin(int x,int y)
{
int i;
for(i=0;i<edg;i++)
if(ax[i]==x &&ay[y]==y) return false;
return true;
}

void clr()
{int i;
for(i=0;i<E;i++)
{
ax[i]=0;ay[i]=0;}
}
void fill()
{edg=0;
int x,y;
while(edg<E)
{
x=rand()%V+1;
y=rand()%V+1;
if(isin(x,y)&&x!=y){ax[edg]=x;ay[edg]=y; edg++;}
}


}

typedef struct tree
{
	tree *p;
    int val;
	int rank;
}BT;

BT* makeset(int x)
{ make[nr]++;
BT *p;
p=(BT* )malloc(sizeof(BT));
p->val=x;
p->p=p;
p->rank=0;
return p;
}



void linkset(BT *x,BT *y)
{

	if(x->rank>y->rank)
		y->p=x;
	else x->p=y;
	if(x->rank==y->rank)
		y->rank++;
}

BT* findset(BT *x)
{	find[nr]++;
	if(x!=x->p)
x->p=findset(x->p);
return x->p;
}


void unionset(BT* x,BT* y)
{unions[nr]++;
linkset(findset(x),findset(y));
}




/*
void clear()
{int i,j;
for(i=1;i<=V;i++)
for(j=1;j<=V;j++)
a[i][j]=0;
}*/

bool samecomponent(BT *x,BT *y)
{
if(findset(x)==findset(y)) return true;
else return false;
}

/*
void connectedcomponents()
{
	BT* tr[10005];
	int i,x,y,j;
for(i=1;i<=V;i++)
{
	tr[i]=makeset(i);
}
clear();
int e=0;
while(e<E)
{
x=rand()%V+1;
y=rand()%V+1;
if(a[x][y]==0 &&x!=y){a[x][y]++; e++;}
}
for(i=1;i<=V;i++)
for(j=1;j<=V-i+1;j++)
if(a[i][j]==1)
if(findset(tr[i])!=findset(tr[j]))
unionset(tr[i],tr[j]);

}*/
void connectedcomponents2()
{
	BT* tr[10005];
	int i,x,y,j;
for(i=1;i<=V;i++)
{
	tr[i]=makeset(i);
}
clr();
fill();

for(i=0;i<E;i++)
if(findset(tr[ax[i]])!=findset(tr[ay[i]]))
unionset(tr[ax[i]],tr[ay[i]]);
}

void connectedcomponents3()
{
	BT* tr[10005];
	int i,x,y,j;
for(i=1;i<=V;i++)
{
	tr[i]=makeset(i);
}
clr();
fill();

for(i=0;i<E;i++)
if(findset(tr[ax[i]])!=findset(tr[ay[i]]))
unionset(tr[ax[i]],tr[ay[i]]);
for(i=1;i<=V;i++)
{printf("%d-%d\n",i,tr[i]->p->rank);}
}

int main()
{  srand (time(NULL));
	int i;

nr=0;
E=10000;
while(E<=60000)
{
printf("%d",E);
connectedcomponents2();
E+=1000;nr++;
}

FILE *f;
f=fopen("disj.csv","w");
fprintf(f,"make,find,unions\n");
for(i=0;i<=50;i++)
{fprintf(f,"%d,%d,%d\n",make[i],find[i],unions[i]);}
printf("done");

/*
	V=20;E=20;
	connectedcomponents3();
*/
getch();
}