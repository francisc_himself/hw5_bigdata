/*
	The number of operations increases linearly with respect to input size in both cases, top-down, bottom-up.
	AVG case:
		The bottom-up approach seems to perform approximatly half of the operations performed by the top-down approach.
	Worst case:
		Although both linear, the bottom-up performs approximatly 9 times fewer operation than top-down

	It is clearly now, which the best approach for building heaps is: bottom-up.

*/

#include<cstdlib>
#include<cstdio>
#include<conio.h>
#include<math.h>



void swap(int &a, int &b){
	int aux = a;
	a = b;
	b = aux;
}

void heapify_down(int* v, int n, int i, int &a, int &c){
	
	int largest = i;

	if(2*i <= n)
	{
		c++;
		if(v[largest] < v[2*largest])
			largest *= 2;
		
		if(2*i+1 <= n)
		{
			c++;
			if(v[largest] < v[largest+1])
			largest++;
		}

	}
	if(largest != i)
	{
		a+=3;
		swap(v[i], v[largest]);
		heapify_down(v, n, largest, a, c);
	}


}

int* buildHeapBottomUp(int* v, int n, int &a, int &c){
	int* heap = new int[n+1];

	for(int i = 1; i <= n; ++i)
		heap[i] = v[i];

	for(int i = n/2; i > 0; --i)
		heapify_down(heap, n, i, a, c);
	return heap;
}

void postOrder(int* v, int n, int i, int ident){
	if(i > n){
		for(int i = 0; i < ident; ++i)	printf("\t");
		printf("~\n");
		return;
	}

	if(2*i < n) 	postOrder(v, n, 2*i, ident+1);
	
	for(int i = 0; i < ident; ++i)	printf("\t");
	printf("%d\n", v[i]);
	
	if(2*i+1 < n) 	postOrder(v, n, 2*i+1, ident+1);
	
}



void printArray(int* v, int n)
{
	int linii = (int)(log((double)n)/log(2.0))+1;
	int count = 0;

	for(int i = 0; i < linii && count < n; ++i)
	{
		
		for(int j = 1; j <= 1<<i && count < n; ++j)
		{
			for(int q = 0; q <= linii - i; ++q)
				printf(" ");
			printf("%d", v[++count]);
		}
		printf("\n");
	}
		
}

void promote(int* v, int pos, int &a, int &c)
{
	if(pos <= 1 )
		return;
	
	c++;
	if(v[pos] > v[pos/2])
	{
		a += 3;
		swap(v[pos], v[pos/2]);
		promote(v, pos/2, a, c);
	}
}

void push(int* v, int x, int pos, int &a, int &c)
{
	v[pos] = x;
	promote(v, pos, a, c);

	
}

int* buildHeapUpBottom(int* v, int n, int &a, int &c)
{
	int* heap = new int[n+1];
	for(int i = 1; i <= n; ++i)
		push(heap, v[i], i, a, c);
	return heap;
}

int* randArray(int n)
{
	int* v = new int[n+1];
	for(int i = 1; i <= n; ++i)
	{
		v[i] = rand();
	}
	return v;
}

int* sortedArray(int n)
{
	int* v = new int[n+1];
	for(int i = 1; i <= n; ++i)
	{
		v[i] = i;
	}
	return v;
}

void demo()
{
	int n = 10, a = 0, b = 0;
	int v[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	printf("Initial array : \n");
	printArray(v, n);

	int* heap = buildHeapBottomUp(v, n,a, b);
	printf("Bottom Up heap : \n");
	printArray(heap, n);

	int* heap2 = buildHeapUpBottom(v, n,a, b);
	printf("Top Down heap : \n");
	printArray(heap2, n);
}

int main(){

	demo();
	printf("Demo Done! \nRunning algorithm on greater input size ... \n");
	FILE *file = fopen("Heap.csv", "w");
	fprintf(file, "n,a1,c1,a1+c1,a2,c2,a2+c2\n");

	FILE *file2 = fopen("HeapWorst.csv", "w");
	fprintf(file2, "n,a1,c1,a1+c1,a2,c2,a2+c2\n");
	for(int n = 100; n <= 10000; n += 100)
	{
		int A1 = 0, C1 = 0, A2 = 0, C2 = 0;
		for(int q = 5; q != 0; --q)
		{
			int* v = randArray(n);							
			int a1 = 0, c1 = 0, a2 = 0, c2 = 0;		
			int* heap = buildHeapUpBottom(v,n, a1, c1);	
			int* heap2 = buildHeapBottomUp(v,n,a2, c2);	
			A1 += a1;
			C1 += c1;
			A2 += a2;
			C2 += c2;

			delete heap;
			heap = NULL;

			delete heap2;
			heap2 = NULL;
			
			delete v;
			v = NULL;
			
		}
		fprintf(file, "%d,%d,%d,%d,%d,%d,%d\n", n, A1/5, C1/5, (A1+C1)/5, A2/5, C2/5, (A2+C2)/5);

		//worst case
		int* v = sortedArray(n);
		int a1 = 0, c1 = 0, a2 = 0, c2 = 0;		
		int* heap = buildHeapUpBottom(v,n, a1, c1);	
		int* heap2 = buildHeapBottomUp(v,n,a2, c2);	
		fprintf(file2, "%d,%d,%d,%d,%d,%d,%d\n", n, a1, c1, a1+c1, a2, c2, a2+c2);
	}
	fclose(file);
	fclose(file2);
	printf("Done! \nPress any key to continue ... \n");
	getch();

	return 0;
}
