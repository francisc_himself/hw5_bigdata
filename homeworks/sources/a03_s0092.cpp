/**
Student: ***ANONIM*** Diana ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: Analysis & comparison of advanced sorting methods - heapsort & quicksort
Comments:
	Heapsort: More efficient than quicksort in average case; has a linear growth;running time of O(nlgn)
	Quicksort: efficient in average case.running time of O(nlgn);sorting in place;uses divide-and-conquer paradigm
				In the worst case quicksort is very slow, having a running time of O(nlgn), thus and quadratic growth. 
	**/
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
int nr=0;
int care=1;
int heapsize=0;
int a[10001],b[10001];
int partition(int p, int r)
{
	int x;
	//alegere pivot
	if (care==1)
	{
		
		
		//int aux=rand()%r;
		//while (aux<p)
		//	aux=rand()%r;
		int aux=rand()%(r-p)+p;
		int i=b[r];
		b[r]=b[aux];
		b[aux]=i;
		nr++;
		x=b[r];
	}
	else
		if (care==3)
			x=b[p+1];
		else 
		{
			int aux=(r+p)/2;
			x=b[aux];
		}
	

	nr++;
	int i=p-1;
	for(int j=p;j<=r-1;j++)
	{	
		nr++;
		if (b[j]<=x)
		{
			i++;
			long aux=b[i];
			b[i]=b[j];
			b[j]=aux;
			nr+=3;
		}
	}
	long aux2=b[i+1];
	b[i+1]=b[r];
	b[r]=aux2;
	nr+=3;
	return i+1;
}
void quicksort(int p, int r)
{
	if (p<r)
	{
		int q=partition(p,r);
		quicksort(p,q-1);
		quicksort(q+1,r);
	}
}
int right(int i)
{
	return 2*i+1;
}
int left(int i)
{
	return 2*i;
}
void max_heapify(int i)
{
	int largest,l=left(i),r=right(i);
	if ((l<=heapsize)&&(a[l]>a[i]))
		largest=l;
	else largest=i;
	nr++;
	if ((r<=heapsize)&&(a[r]>a[largest]))
		largest=r;
	nr++;
	if (largest!=i)
	{
		int aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		max_heapify(largest);
		nr+=3;
	}
}
void build_heap(int n)
{
	heapsize=n;
	for(int i=n/2;i>=1;i--)
		max_heapify(i);
}
void heapsort(int n)
{
	nr=0;
	build_heap(n);
	for(int i=n;i>=2;i--)
	{
		int aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		heapsize--;
		nr+=3;
		max_heapify(1);
	}
}
int main()
{
	int n;
	srand(time(NULL));
	FILE *f1,*f2;
	f1=fopen("avg.csv","w");
	f2=fopen("quick.csv","w");
	fprintf(f2,"n,best,average,worst\n");
	fprintf(f1,"n,c_h+a_h,c_q+a_q\n");
	
	for(int n=100;n<=10000;n=n+100)
	{
		//average case for heapsort and quicksort
		care=1;
		long total[2];
		total[0]=0;total[1]=0;
		for (int j=0;j<5;j++)
		{
			for(int i=1;i<=n;i++)
			{
				a[i]=rand()%10000;
				b[i]=a[i];
			}
			heapsize=0;
			heapsort(n);
			total[0]=total[0]+nr;

			nr=0;
			quicksort(1,n);
			total[1]=total[1]+nr;
		}
		total[0]/=5;
		total[1]/=5;
		fprintf(f1,"%d,%d,%d\n",n,total[0],total[1]);
	}
	
	for(int n=100;n<=10000;n=n+100)
	{

		for (int i=1;i<=n;i++)
		{
			b[i]=i;
		}
		//best case quicksort
		nr=0;
		care=2;
		quicksort(1,n);
		fprintf(f2,"%d,%d,",n,nr);
		
		//average case quicksort
		care=1;
		int vector=0;
		for (int j=0;j<5;j++)
		{
			nr=0;
			for(int i=1;i<=n;i++)
			{
				b[i]=rand()%10000;
			}
			
			quicksort(1,n);
			vector+=nr;
		}
		vector/=5;
		fprintf(f2,"%d,",vector);
		
		for (int i=1;i<=n;i++)
		{
			b[i]=i;
		}
		//worst case quicksort
		nr=0;		
		care=3;
		quicksort(1,n);
		fprintf(f2,"%d\n",nr);
	}
	fclose(f2);	
	
	
	
	/**
	// heapsort check
	n=10;
	for(int i=1;i<=n;i++)
			{
				a[i]=rand()%100;
			}
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
	heapsort(n);
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	getch();
	
	//quicksort check
	
	n=10;care=1;
	for(int i=1;i<=n;i++)
			{
				b[i]=rand()%100;
				//b[i]=i;
			}
	for(int i=1;i<=n;i++)
		printf("%d ",b[i]);
	printf("\n");
	quicksort(1,n);
	for(int i=1;i<=n;i++)
		printf("%d ",b[i]);
	getch();
	**/
}