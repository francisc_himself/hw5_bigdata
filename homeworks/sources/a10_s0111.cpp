#include <conio.h>
#include <stdio.h>
#include "Profiler.h"


Profiler profiler("DFS");

typedef struct vert {
	int key;
	int d;
	int f;
	char color[10];
	struct vert *p;
} Vertex;

typedef struct node {
	Vertex *v;
	struct node *next;
} List;

typedef struct node2 {
	Vertex *v; 
	struct node2 *next;
} LinkedList;

int tm,back=1;


void insertFront(LinkedList **first,Vertex *v) {
	LinkedList *node;
	node=(LinkedList *)malloc(sizeof(LinkedList));
	node->v=v;
	node->next=*first;
	*first=node;
}

void DFS_VISIT_topo(LinkedList **first,List *adj[],Vertex *u) {
	List *p;
	int ok=1;
	tm++;
	u->d=tm;
	strcpy(u->color,"Gray");
	if (adj[u->key]->next == NULL) {
		p=adj[u->key];
		ok=0;
	} else
		p=adj[u->key]->next;

	while (p!=NULL) {
		if (strcmp(p->v->color,"White") == 0) {
			p->v->p=u;
			DFS_VISIT_topo(first,adj,p->v);
		} else if (strcmp(p->v->color,"Gray") == 0 && ok==1) {
			back=0;
			break;
		}
		p=p->next;
	}
	strcpy(u->color,"Black");
	tm++;
	u->f=tm;
	insertFront(first,u);
}


void DFS_topo(LinkedList **first,List *adj[],int n) {
	for(int i=0;i<n;i++) {
		strcpy(adj[i]->v->color,"White");
		adj[i]->v->p=NULL;
	}
	tm=0;
	for(int i=0;i<n;i++) {
		if (strcmp(adj[i]->v->color,"White")==0) {
			DFS_VISIT_topo(first,adj,adj[i]->v);
		}
	}
}



LinkedList *topologicalSort(List *adj[],int n) {
	LinkedList *first=NULL;
	DFS_topo(&first,adj,n);
	if (back==0) 
		return NULL;
	else
		return first;
}


void DFS_VISIT(List *adj[],Vertex *u,int *op) {
	List *p;
	int ok=1;

	tm++;
	u->d=tm;
	strcpy(u->color,"Gray");
	if (adj[u->key]->next == NULL) {
		p=adj[u->key];
		ok=0;
	}
	else
		p=adj[u->key]->next;
	*op=*op+5;
	while (p!=NULL) {
		(*op)++;
		if (strcmp(p->v->color,"White") == 0) {
			printf("\n(%d, %d) - tree edge",u->key,p->v->key);
			p->v->p=u;
			(*op)++;
			DFS_VISIT(adj,p->v,op);
		} else if (strcmp(p->v->color,"Gray") == 0 && ok==1) {
			printf("\n(%d, %d) - back edge",u->key,p->v->key);
		} else if (strcmp(p->v->color,"Black") == 0) {
			if (u->d < p->v->d) {
				printf("\n(%d, %d) - forward edge",u->key,p->v->key);
			} else if (u->d > p->v->d) {
				printf("\n(%d, %d) - cross edge",u->key,p->v->key);
			}
		}
		p=p->next;
	}
	strcpy(u->color,"Black");
	tm++;
	u->f=tm;
	*op=*op+3;
	//printf("\nNod %d with discovery time %d and finish time %d",u->key,u->d,u->f);
}


void DFS(List *adj[],int n,int *op) {
	for(int i=0;i<n;i++) {
		strcpy(adj[i]->v->color,"White");
		adj[i]->v->p=NULL;
		*op=*op+2;
	}
	tm=0;
	for(int i=0;i<n;i++) {
		(*op)++;
		if (strcmp(adj[i]->v->color,"White")==0) {
			printf("\n\nNod start: %d\n\n",adj[i]->v->key);
			DFS_VISIT(adj,adj[i]->v,op);
		}
	}
}


int main() {
	List *adj[200],*p;
	LinkedList *first;
	int v1,v2;
	int noEdges=0,ok=1,op=0;

	srand(5);

	for (int i=0;i<10;i++) {
		adj[i]=(List *)malloc(sizeof(List));
		adj[i]->v=(Vertex*)malloc(sizeof(Vertex));
		adj[i]->v->key=i;
		adj[i]->next=NULL;
	}

	while (noEdges < 20) {
		v1=rand()%10;
		v2=rand()%10;	
		p=adj[v1];
		ok=1;
		while (p->next!=NULL) {
			if (p->v->key==v2) {
				ok=0;
				break;
			}
			p=p->next;
		}
		if (p->v->key==v2) 
			ok=0;
		if (ok) {
			p->next=(List *)malloc(sizeof(List));
			p->next->v=adj[v2]->v;
			p->next->next=NULL;
			noEdges++;
		}
	}

	printf("Adjacency list:\n");

	for (int i=0;i<10;i++) {
		p=adj[i];
		while (p!=NULL) {
			printf("%d ",p->v->key);
			p=p->next;
		}
		printf("\n");
	}

	DFS(adj,10,&op);
	
	first=topologicalSort(adj,10);
	if (first==NULL) 
		printf("Cyclic graph! No topological sort!");
	else {
		LinkedList *c=first;
		printf("Topological sort: ");
		while (c!=NULL) {
			printf("%d ",c->v->key);
			c=c->next;
		}
	}
	
/*
	srand(time(NULL));

	for (int n=1000;n<=5000;n+=100) {
		op=0;
		noEdges=0;
		for (int i=0;i<100;i++) {
			adj[i]=(List *)malloc(sizeof(List));
			adj[i]->v=(Vertex*)malloc(sizeof(Vertex));
			adj[i]->v->key=i;
			adj[i]->next=NULL;
		}

		while (noEdges < n) {
			v1=rand()%100;
			v2=rand()%100;			
			p=adj[v1];
			ok=1;
			while (p->next!=NULL) {
				if (p->v->key==v2) {
					ok=0;
					break;
				}
				p=p->next;
			}
			if (p->v->key==v2) 
				ok=0;
			if (ok) {
				p->next=(List *)malloc(sizeof(List));
				p->next->v=adj[v2]->v;
				p->next->next=NULL;
				noEdges++;
			}
		}

		DFS(adj,100,&op);

		profiler.countOperation("DFS_vary_edges",n,op);
	}

	
	for (int n=100;n<=200;n+=10) {
		op=0;
		noEdges=0;
		for (int i=0;i<n;i++) {
			adj[i]=(List *)malloc(sizeof(List));
			adj[i]->v=(Vertex*)malloc(sizeof(Vertex));
			adj[i]->v->key=i;
			adj[i]->next=NULL;
		}

		while (noEdges <= 9000) {
			v1=rand()%n;
			v2=rand()%n;
			p=adj[v1];
			ok=1;
			while (p->next!=NULL) {
				if (p->v->key==v2) {
					ok=0;
					break;
				}
				p=p->next;
			}
			if (p->v->key==v2) 
				ok=0;
			if (ok) {
				p->next=(List *)malloc(sizeof(List));
				p->next->v=adj[v2]->v;
				p->next->next=NULL;
				noEdges++;
			}
		}

		DFS(adj,n,&op);

		profiler.countOperation("DFS_vary_vertices",n,op);
	}

	profiler.showReport(); 
	*/


	getch();
	return 0;
}