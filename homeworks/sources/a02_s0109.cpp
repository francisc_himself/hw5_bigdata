#include "conio.h"
#include "stdio.h"
#include "time.h"
#include "stdlib.h"

int heapsize=0;
int a=0;
int c=0;

/*Creates an array with random values,between 1 and n */
void average(int A[], int n)
{
for (int i=1; i<=n; i++)
{
	A[i]=rand()%n;
}
}

/*Returns left child*/
int left(int k)
{
	return 2*k;
}
/*Retuns right child*/
int right(int k)
{
	return 2*k+1;
}
/*Returns parent node*/
int parent(int k)
{
	return k/2;
}

/*swaps two integers*/
void swap(int &a,int &b)
{int aux=a;
 a=b;
 b=aux;
}

//bottom up

/*method that takes a node and knowing that its two subtrees are heaps creates
a new heap*/
void heapify(int A[],int i)
    {
	int largest=i;
    int l=left(i);
	int r=right(i);
	c++;
	if ((l<=heapsize) && (A[l]>A[i]))
		largest=l;
	else
		largest=i;
	c++;
     if ((r<=heapsize) && (A[r]>A[largest]))
		 largest=r;
	c++;
	 if (largest!=i)
		{a=a+3;
	     swap(A[largest],A[i]);
		 heapify(A,largest);
        }
}

/*method that builds a heap starting from its leaves, that are already heaps
and goes up to the root by calling heapify*/
void build_heap(int A[],int n)
{
	for(int i=n/2;i>0;i--)
	{
		heapify(A,i);
	}
}


//top down

/*compare the new value to the value of the parent and move up until find 
a place to place the new value*/
void heapincreasekey(int A[],int i,int key)
{
	if (key<A[i])
	printf("New key is smaller than current key");
	else {
    a++;
	A[i]=key;
	c++;
	while (i>1 && A[parent(i)]<A[i])
	{
		c++;
		a=a+3;
		swap(A[parent(i)],A[i]);
		i=parent(i);
	}
	}
}
/* increases heapsize and adds element on desired position */
void maxheapinsert(int A[],int key)
{
	heapsize++;
	heapincreasekey(A,heapsize,key);
}


/* builds heap; top down manner */
void build_heap_top_down(int *A,int n)
{
	heapsize=1;
	for(int i=2;i<=n;i++)
		maxheapinsert(A,A[i]);
}


int main()
{
FILE *f;
		int *A;
		int *B;
		f=fopen("heap_bu_vs_td.csv","w");
		for(int n=100;n<=10000;n=n+100)
		{   a=0;
		    c=0;
			fprintf(f,"%d, ",n);
			A=new int[n+1];
			B=new int[n+1];
			average(A,n);
			average(B,n);
			build_heap_top_down(A,n);
			fprintf(f,"%d, ",a+c);
			a=0;
			c=0;
			build_heap(B,n);
			fprintf(f,"%d, ",a+c);
			fprintf(f,"\n");
		}

		int n=20;
		A=new int[n+1];
		B=new int[n+1];
		
		average(A,n);
		for(int i=1;i<=n;i++)
		printf("%d ",A[i]);
		printf("\n");
		printf("Top down: ");
		build_heap_top_down(A,n);
		for(int i=1;i<=n;i++)
		printf("%d ",A[i]);
		printf("\n");

		average(B,n);
		for(int i=1;i<=n;i++)
		printf("%d ",B[i]);
		printf("\n");
		printf("Bottom up: ");
		build_heap(B,n);
		for(int i=1;i<=n;i++)
		printf("%d ",B[i]);
		printf("\n");
		getch();

return 0;
}