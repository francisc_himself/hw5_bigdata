/*
L?***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***

this algorithm has a complexity of O(nlgn)
n because every element has to be evaluated and every element takes lgn time to search and delete

the "take" function finds the element with the specified rank and takes out from the tree.



*/


#include<stdio.h>
#include<stdlib.h>
#include<conio.h>



int a[10002],lvl,r[102];
FILE *f;


typedef struct tree
{
    struct tree *right;
	struct tree *left;
	int size;
    int val;
}PBST;

PBST* build(int f,int l)
{
PBST *p;
p=(PBST* )malloc(sizeof(PBST));


if(f<=l)
{p->val=a[(f+l)/2];  r[lvl/100]++;
p->left=build(f, (f+l-2)/2);
p->right=build((f+l)/2+1, l);
p->size=1;
if(p->left!=NULL)
p->size+=p->left->size;
if(p->right!=NULL)
p->size+=p->right->size;

return p;}
else return NULL;
}


void Delete(PBST * &ptr, int key)       
{
		r[lvl/100]++;

        if (key < ptr->val)
		 Delete(ptr->left, key);
		else 
			if (key > ptr->val)
                Delete(ptr->right, key);
        else
        {
                PBST *temp; 
			

                if (ptr->left==NULL)
                {
                        temp = ptr->right; 
                        free( ptr); 
                        ptr = temp;  
                }
                else if (ptr->right==NULL)
                {
                        temp = ptr->left; 
                         free( ptr); 
                        ptr = temp;  
				}
		}
}



int take(PBST * &p, int x)
{	
	r[lvl/100]++;

	int pl=0;
	if(p->left==NULL) pl=0;
	else pl=p->left->size;
	int pr=0;
	if(p->right==NULL) pr=0;
	else pr=p->right->size;

	
	if(p->size - pr  ==x)
	{	int y=p->val; 
		p->size--;
	if(p->left==NULL&&p->right==NULL)	
		{p=NULL; return y;}

	if(p->left!=NULL&&p->right==NULL)	
		{	PBST *temp;
			temp = p->left;  
                    free( p); 
                        p = temp;
						return y;
	}

	if(p->left==NULL&&p->right!=NULL)	
		{	PBST *temp;
			temp = p->right; 
                    free( p); 
                        p = temp; 
						return y;}

	if(p->left!=NULL&&p->right!=NULL)	
		{
			PBST *temp,*parent;
			 temp = p->right; 
			 temp->size--;
                        parent = NULL;
					
                        while(temp->left!=NULL)
                        {

							r[lvl/100]++;
                                parent = temp;  
                                temp = temp->left;  
								temp->size--;
								r[lvl/100]++;
                        }
                        p->val = temp->val;  
                        if (parent != NULL)
                                Delete(parent->left, parent->left->val);
						else
                                Delete(p->right, p->right->val);
	
	return y;}
		}
	
	if(p->size>1)
	{
	if(p->size-pr>x)
	{	if(p->left!=NULL)
		{--p->size;	return take(p->left,x);}
		else return -1;}
	else {if(p->right!=NULL)
	{--p->size;return take(p->right,x-pl-1);}
		else return -1;}
	}
	else return -1;

}


void printkey(int x,int y,int depth)
{
for(int i=1;i<=depth;i++)
printf(" ");
printf("%d,%d\n",x,y);
}

void rekur(PBST *p, int depth)
{
	if(p->right!=NULL)	
rekur(p->right,depth+5);

printkey(p->val,p->size,depth);

	if(p->left!=NULL)
rekur(p->left,depth+5);
}

void josephus(int n,int m)
{int j,k,x;
PBST *p;
p=build(1,n);
j=1;
for(k=n;k>=1;k--)
{
j=((j+m-2)% k)+1;
x=take(p,j);
//printf("%d ",x);
}

}



int main()
{
int i,n;
for(i=1;i<=10000;i++)
{a[i]=i;}

josephus(7,3);
//rekur(p,1);


for(lvl=100;lvl<=10000;lvl+=100)
josephus(lvl,lvl/2);

f=fopen("rez.csv","w");

	for(int i=1;i<=100;i++)
	{	fprintf(f,"%d,",i*100);
		fprintf(f,"%d\n",r[i]);
	}

printf("done");
getch();
}