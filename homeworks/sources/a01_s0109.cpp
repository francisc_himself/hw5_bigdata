#include "conio.h"
#include "stdio.h"
#include "time.h"
#include "stdlib.h"


void average(int *a, int n)
{
for (int i=0; i<n; i++)
{
a[i]=rand();
}
}

void worst1(int *a, int n)
{
for (int i=0; i<n; i++)
{
if (i>0) a[i] = i;
else a[i]=n;
}
}

void worst2(int *a, int n)
{
for (int i=0; i<n; i++)
{
a[i] = n-i;
}
}

void best(int *a, int n)
{
for (int i=0; i<n; i++)
{
a[i]=i;
}
}

void insertionSort(int A[],int n,long *a,long *c)
{
	int i,j,buff;

  for(i=1;i<n;i++)
	{   buff=A[i];
        (*a)++;
		j=i-1;
		while (A[j]>buff && j>=0)
		{
			A[j+1]=A[j];
			j=j-1;
			(*a)++;
			(*c)++;
		}

		A[j+1]=buff;
		(*a)++;
		(*c)++;

	}
   /*for (i=0;i<n;i++)
	{
		printf ("%d ",A[i]);
	}
   printf("\nInsertion sort:%d,%d",a,c);*/
}

void selectionSort(int A[],int n,long *a,long *c)
{
	int i,j,pos,aux;

	for (j=0;j<n-1;j++)
	{
		pos=j;
			for(i=j+1;i<=n;i++)
			{
				if (A[i]<A[pos])
				{pos=i;}
				(*c)++;
	        }
	 if (pos!=j)
	 {
		 aux=A[j];
		 A[j]=A[pos];
		 A[pos]=aux;
		 (*a)=(*a)+3;
	 }
	}
 /*for (i=0;i<n;i++)
	{
		printf ("%d ",A[i]);
	}
   printf("\nSelect sort:%d",a);*/
}

void bubbleSort(int A[], int n,long *a,long *c)
{
  int i,j,aux;

  for (i=n-1;i>0;i--)
  {
    for (j=1;j<=i;j++)
    {
      if (A[j-1]>A[j])
      {
        aux=A[j-1];
        A[j-1]=A[j];
        A[j]=aux;
		(*a)=(*a)+3;
      }
	  (*c)++;
    }
  }/*printf("\n");
 for (i=0;i<n;i++)
	{
		printf ("%d ",A[i]);
	}*/
  
}



int main()
{
FILE *f, *g, *h;
		int *b;
		long a=0,c=0;
		f=fopen("sorting_best.csv","w");
		g=fopen("sorting_worst.csv","w");
		h=fopen("sorting_average.csv","w");
		

	//best case
		for (int n=100; n<=10000; n = n + 100)
		{  fprintf(f,"%d, ",n);
			b = new int[n];
			a=0;c=0;
			best(b,n);
			bubbleSort(b,n,&a,&c);	
			fprintf(f,"%d, %d, %d, ",a,c,a+c);

			a=0;c=0;		
			best(b,n);
			selectionSort(b,n,&a,&c);
			fprintf(f,"%d, %d, %d, ",a,c,a+c);

			a=0;c=0;	
			best(b,n);
			insertionSort(b,n,&a,&c);
			fprintf(f,"%d, %d, %d",a,c,a+c);
			fprintf(f,"\n");
		}

	//average case
		for (int n=100; n<=10000; n = n + 100)
		{  fprintf(g,"%d, ",n);
			b = new int[n];
			a=0;c=0;
			average(b,n);
			bubbleSort(b,n,&a,&c);	
			fprintf(g,"%d, %d, %d, ",a,c,a+c);

			a=0;c=0;		
			average(b,n);
			selectionSort(b,n,&a,&c);
			fprintf(g,"%d, %d, %d, ",a,c,a+c);

			a=0;c=0;	
			average(b,n);
			insertionSort(b,n,&a,&c);
			fprintf(g,"%d, %d, %d",a,c,a+c);
			fprintf(g,"\n");
		}

	//worst case
		for (int n=100; n<=10000; n = n + 100)
		{  fprintf(h,"%d, ",n);
			b = new int[n];
			a=0;c=0;
			worst2(b,n);
			bubbleSort(b,n,&a,&c);	
			fprintf(h,"%d, %d, %d, ",a,c,a+c);

			a=0;c=0;		
			worst1(b,n);
			selectionSort(b,n,&a,&c);
			fprintf(h,"%d, %d, %d, ",a,c,a+c);

			a=0;c=0;	
			worst2(b,n);
			insertionSort(b,n,&a,&c);
			fprintf(h,"%d, %d, %d",a,c,a+c);
			fprintf(h,"\n");
		}
		return 0;
}

