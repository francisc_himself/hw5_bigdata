/*

Metoda bottom-up - Complexitate O(n). 
Metoda top-down - Complexitate O(n*lgn).

*/


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#define MAX_SIZE 10000



int parinte(int i){return i/2;}
int stanga(int i){return 2*i;}
int dreapta(int i){return 2*i+1;}

void insert_in_heap(int *v,int cheie,int *dimens_h,int *numar_operatii){//inserare element in heap
	(*dimens_h)++;                                         //cauta locul elementului care trebuie inserat
	int i=(*dimens_h);
	while(i>1 && v[parinte(i)]<cheie)
	{
		(*numar_operatii)++;
		v[i]=v[parinte(i)];
		i=parinte(i);
	}
	(*numar_operatii)++;
	v[i]=cheie;
}
void reconstituire_heap(int *v,int i,int dimens_h,int *numar_operatii){    //reconstituie proprietatea de heap 
	int maximul,temp;
	int l=stanga(i);
	int r=dreapta(i);
	(*numar_operatii)++;
	if(l<=dimens_h && v[l]>v[i])
	{
		(*numar_operatii)++;
		maximul=l;
	}
	else
	{
		(*numar_operatii)++;
		maximul=i;
	}
	if(r<=dimens_h && v[r]>v[maximul])
	{
		(*numar_operatii)++;
		maximul=r;
	}
	if (maximul!=i)
	{
		(*numar_operatii)++;
		temp=v[i];
		v[i]=v[maximul];
		v[maximul]=temp;
		reconstituire_heap(v,maximul,dimens_h,numar_operatii);
	}
}
int* top_down_heap(int *v,int n,int *numar_operatii){// constructia heap-ului in modul top down
	int dimens_h=1;
	for(int i=2;i<n+1;i++)
	{
		(*numar_operatii)++;
		insert_in_heap(v,v[i],&dimens_h,numar_operatii);
	}
	return v;
}
int* bottom_up_heap(int *v,int n,int *numar_operatii){//constructia heap-ului in modul bottom up
	int dimens_h=n+1;
	for(int i=(n+1)/2;i>0;i--)
	{
		(*numar_operatii)++;
		reconstituire_heap(v,i,dimens_h,numar_operatii);
	}
	return v;
}



int main(){
	int v[MAX_SIZE];
	int numar_operatii=0,numar_operatii1=0,numar_operatii2=0;
	
    FILE *fout = fopen("text.csv","w");
	fprintf(fout,"Nunar operatii bottom up, top down \n");

	for(int i=100;i<=MAX_SIZE;i+=100)
	{
		numar_operatii1=0;
		numar_operatii2=0;
		for(int j=5;j>=0;j--)
		{
			int* v=(int*) malloc((i+1)*sizeof(int));
		
			numar_operatii=0;
			bottom_up_heap(v,i,&numar_operatii);
			numar_operatii1=numar_operatii1 + numar_operatii;
			numar_operatii=0;
			top_down_heap(v,i,&numar_operatii);
			numar_operatii2 = numar_operatii2 + numar_operatii;
		}
		numar_operatii1=numar_operatii1/5;
		numar_operatii2=numar_operatii2/5;
		fprintf(fout,"%d, %d, %d \n",i,numar_operatii1,numar_operatii2);
	}
	fclose(fout);


	return 0;
}