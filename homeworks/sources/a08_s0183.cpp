#include <iostream>
#include <stdio.h>
#include <time.h>
#include<stdlib.h>
using namespace std;
typedef struct disj
{
    int info,adanc;
    struct disj *par;

}disset;

typedef struct muchii
{
    int st,dr;

}muchii;

FILE *f;
disset *graf[10000];
int nrMkSet,nrOfUnions,nrOfFindSet;
muchii m[60000];


void Make_set (disset *x)
{

    nrMkSet++;
    x->par=x;
    x->adanc=0;
}

disset *Find_set ( disset *x )
{
    nrOfFindSet++;
    if (x!=x->par)
        x->par=Find_set(x->par);
    return x->par;
}

void Link (disset *x,disset *y)
{
    if (x->adanc>y->adanc)
        y->par=x;
    else
        x->par=y;
    if (x->adanc == y->adanc)
        y->adanc++;
}

void UNION (disset *x,disset *y)
{
    if (Find_set(x)!=Find_set(y))
    {nrOfUnions++;
    Link(Find_set(x),Find_set(y));
}}

void generare (int mu)
{

    int i,j,st,dr;
    nrOfFindSet=nrMkSet=nrOfUnions=0;
    for ( i=0;i<10000;i++ )
    {
        graf[i]=( disset* ) malloc( sizeof ( disset ) );
        Make_set(graf[i]);
    }
    for(i=0;i<mu;i++)
    {
    st=dr=0;
        do
        {
        st=rand()%10000;
        dr=rand()%10000;
        }
    while (st<=dr );

        m[i].st=st;
        m[i].dr=dr;

        for (j=0;j<i;j++)
        {

            if(m[i].st==m[j].st && m[i].dr==m[j].dr )
            {

                i--;
            }
        }
    }
        //union pt fiecare muchie in parte
        for(i=0;i<mu;i++)
        {
            UNION (graf[m[i].st],graf[m[i].dr]);
        }
        for (i=0;i<10000;i++)
        {
            free(graf[i]);
        }

        fprintf(f,"%d, %d, %d, %d \n",mu,nrMkSet,nrOfUnions,nrOfFindSet);
        printf("%d ",mu);

}
int main()
{
//    cout << "Hello world!" << endl;
    int i;
    f=fopen("date.csv","w");
    srand(time(NULL));
    fprintf(f,"nr muchii,  NrMkset, Nr of UNions , nrOfFind set");
    for ( i=10000;i<60000;i+=1000)
        generare(i);
    return 0;
}
