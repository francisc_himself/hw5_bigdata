/*
***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Complexitatea pentru average case este O(1) atat pentru insert cat si pentru search
Cautarea elementelor scade invers proportional cu factorul de umplere .Este mai usor in cazul hash tabel-urilor sa gasesti o valoare stocata intr-un array
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include "Profiler.h"


#define HASH_SIZE 9973
#define SEARCH_SIZE 3000

int samples[HASH_SIZE + SEARCH_SIZE /2 ];
int indices [SEARCH_SIZE /2 +1];
int k,i;
int T[HASH_SIZE];
int c1,c2;
float cauta;
float alfa[]={0.8, 0.85, 0.9, 0.95, 0.99};
int Ff;
int h(int k,int i)
{
	  return ((k % HASH_SIZE + c1 * i + c2 * i * i) % HASH_SIZE);
}

void init_vector()
{
	for(int i=0;i<HASH_SIZE;i++)
	      T[i]=0;
}

int Hash_Insert(int T[],int k)
{
   int i=0;
   do{
   int j=h(k,i);
   if (T[j]==0)
   {
	   T[j]=k;
	   return j;
   }else i=i+1;
   }while(i!=HASH_SIZE);
   perror("hash table overflow");
   return -1;
}
int Hash_Search(int T[],int k)
{
	int i=0,j;
   do
   {
	   j=h(k,i);
	   cauta=cauta+1;
	   if (T[j]==k)
	   {
		   Ff=1;
		   return j;
	   }
	   i=i+1;
   }while((T[j]!=0) && (i<HASH_SIZE));
   return -1;
}

void main()
{
	c1=1;
	c2=1;
	int n;
	int x;
	init_vector();
		Hash_Insert(T,3213);
		Hash_Insert(T,43232);
		Hash_Insert(T,7437);

		//pentru elementele care nu se gasesc:
		    Ff=0;
			Hash_Search(T,232);
	
			if(Ff==1) printf("numarul a fost gasit \n ");
			else  printf("numarul nu a fost gasit \n");

		//pentru elementele care se gasesc:		
			Ff=0;
			Hash_Search(T,7437);
			
			if(Ff==1) printf(" numarul a fost gasit \n");
			else  printf("numarul nu a fost gasit\n");
	
		FILE * f=fopen("fis.csv","w");
		fprintf (f,"Filling factor, Avg.Effort (found),Max.Effort(found),Avg.Effort (not-found),Max.Effort(not-found) \n");
	for(int i=0;i<5;i++)
	{
	    float Not_found=0;
		float Max_not=0;
		float Found=0;
		float Max_found=0;

		init_vector();
		n=alfa[i]*HASH_SIZE;
		printf(" %d ",(SEARCH_SIZE+n)/2);
		FillRandomArray(samples,n+SEARCH_SIZE/2,1,1000000,true,0);
		for(int j=0;j<n;j++)
		{
			Hash_Insert(T,samples[j]);
		}

			FillRandomArray(indices,(SEARCH_SIZE/2),0,n-1,true,0);
			//gasite
			for (int j1=0;j1<SEARCH_SIZE/2;j1++)
			{ 
			   cauta=0;
			   x=Hash_Search(T,samples[indices[j1]]);
			   if(x==-1)
			   {
				    Not_found=Not_found+cauta+1;
					if(Not_found>Max_not) Max_not=Not_found;
			   }else{
				   Found=Found+cauta;
				   if(cauta>Max_found) Max_found=cauta;
			   }
			
			}
	
			//negasit
			for (int j2=n;j2<n+SEARCH_SIZE/2;j2++)
			{ 
			   cauta=0;
			   x=Hash_Search(T,samples[j2]);
			   if(x==-1){
			   Not_found=Not_found+cauta;
			   if(cauta>Max_not) Max_not=cauta;}
			}
	printf("%f",Not_found);
	fprintf(f,"%f, %f, %f, %f, %f \n",alfa[i],Found/(SEARCH_SIZE/2),Max_found,Not_found/(SEARCH_SIZE/2),Max_not);
	}
	fclose(f);
}