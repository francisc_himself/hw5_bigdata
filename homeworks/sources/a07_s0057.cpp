#include <stdlib.h>
#include <stdio.h>

typedef struct multi_nod
{
	int key;
	int count;
	struct multi_nod *child[50];
	struct multi_nod *brth;
}multi_nod;

typedef struct bin_nod
{
	int key;
	int count;
	struct bin_nod*child;
	struct bin_nod*brth;
}bin_nod;

multi_nod* createMultinod(int key){
	multi_nod *x=(multi_nod*)malloc(sizeof(multi_nod));
	x->key=key;
	x->count=0;
	return x;
}

void insertMultinod(multi_nod *parent,multi_nod *child){
	
	parent->child[parent->count++]=child;
}

multi_nod* transform1(int t[],int size){
	multi_nod*nodes[50];
	multi_nod*root=NULL;
	for(int i=0;i<size;i++)
	{
		nodes[i]=createMultinod(i);
	}
	for(int i=0;i<size;i++)
	{
		if(t[i]==-1)
			root=nodes[i];
		else insertMultinod(nodes[t[i]],nodes[i]);
	}
	return root;
}

void transform2(multi_nod*root,bin_nod*rad){
	
	if(root->count!=NULL){
		rad->child=(bin_nod*)malloc(sizeof(bin_nod));
		rad->child->key=root->child[0]->key;
		rad->child->count=1;
		transform2(root->child[0],rad->child);
		rad=rad->child;
		if(root->count>1)
		{
			for(int i=1;i<root->count;i++)
			{
				rad->brth=(bin_nod*)malloc(sizeof(bin_nod));
				rad->brth->key=root->child[i]->key;				
				transform2(root->child[i],rad->brth);
				rad=rad->brth;				
			}
			rad->brth=NULL;			
		}
		else rad->brth=NULL;
	}
	else rad->child=NULL;
	
}

void afisare(multi_nod *nod,int nivel=0)
{
	for(int i=0;i<nivel;i++)
	{
		printf("   ");
	}
	printf("%d,%d\n",nod->key,nod->count);
	for(int i=0;i<nod->count;i++)
		afisare(nod->child[i],nivel+1);
}

void afisare1(bin_nod *nod,int nivel=0)
{
	if(nod!=NULL){
	
	for(int i=0;i<nivel;i++)
		{
			printf("   ");
		}
	printf("%d\n",nod->key);
			

		if(nod->child!=NULL)
			afisare1(nod->child,nivel+1);
	
		if(nod->brth!=NULL)
			afisare1(nod->brth,nivel);	
	}
}

int main()
{
	bin_nod*rad=(bin_nod*)malloc(sizeof(bin_nod));
	int t[]={0,2,7,5,2,7,7,-1,5,2};
	int size=sizeof(t)/sizeof(t[0]);
	multi_nod*root=transform1(t,size);
	afisare(root,0);
	printf("\n");
	rad->key=root->key;
	rad->brth=NULL;
	rad->count=1;
	transform2(root,rad);
	afisare1(rad,0);

	return 0;
}