#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#define N 10007
#define C1 13
#define C2 11
//***ANONIM*** ***ANONIM*** gr ***GROUP_NUMBER***
//in cazul mediu datele existente intrun hashTable pot fi gasite in interval de 4 si 5 iteratii
//in cazul mediu datele pot fi depistate ca sunt inexistente in hashTable in interval de 20 si 120 iteratii
//numarul maxim de iteratii pentru cautarea unor date existente in hashTable umplut 99% este in jur de 100 pentru N=10007
//numarul maxim de iteratii pentru cautarea unor date inexistente in hashtable umplut 99% este in jur de 800 pentru N=10007
int hashTable[N];
float alfa[]={0.8f,0.85f,0.9f,0.95f,0.99f};
int finded[N];
int hashfunc(int n,int i)
{
	return (n + i*C1 + i*i*C2)%N;
}

void insert(int n,int i)
{
	int poz=hashfunc(n,i);
	if(hashTable[poz]==-1)
		hashTable[poz]=n;
	else
		insert(n,i+1);
}

int find(int n,int i,int *nr)
{
	*nr=i+1;
	int poz=hashfunc(n,i);
	if(hashTable[poz]==-1)
		return -1;
	if(hashTable[poz]==n)
		return poz;
	else
		return find(n,i+1,nr);
}

void newHash()
{
	for(int i=0;i<N;i++) 
		hashTable[i]=-1;
}
int main(){
	int n=0;
	srand(time(0));
	printf("|%12s|%12s|%12s|%12s|%12s|\n","Filling","Avg.Effort","Max Effort","Avg.Effort","Max Effort");
	printf("|%12s|%12s|%12s|%12s|%12s|\n","factor","found","found","not-found","not-found");
	printf("_______________________________________________________________________________\n");
	for(int i=0;i<5;i++)
	{
		newHash();
		n=(int)(alfa[i]*N);
		//Inserare
		for(int j=0;j<n;j++)
		{
			int aux=abs(rand())%3000;
			insert(aux,0);
			finded[j]=aux;
		}

		//cautare
		int maxfound=0;
		float sumfound=0;
		int el;
		int times;
		for(int j=0;j<3000;j++)
		{
			el=abs(rand())%n;
			times=0;
			find(finded[el],0,&times);
			sumfound+=times;
			if(times>maxfound)
				maxfound=times;
		}
		int maxnotfound=0;
		float sumnotfound=0;
		for(int j=0;j<3000;j++)
		{
			el=abs(rand())%3000+10000;
			times=0;
			find(el,0,&times);
			sumnotfound+=times;
			if(times>maxnotfound)
				maxnotfound=times;
		}
		//Afisare rezultat
		printf("|%12.2f|%12.2f|%12d|%12.2f|%12d|\n",alfa[i],sumfound/3000,maxfound,sumnotfound/3000,maxnotfound);
		printf("_______________________________________________________________________________\n");
	}
	printf("\n\n####Sfirsit####");
	getch();
	return 0;
}
