#include<stdlib.h>
#include<iostream>
#include"Profiler.h"

Profiler profiler("***ANONIM***");

#define white 0
#define gray 1
#define black 2
#define v_max 10000
#define e_max 60000
#define max 10000

typedef struct graph_node{
	struct graph_node *parent;
	int dist;
	int color;
	int key;
}graph_node;

typedef struct _node{
	int key;
	struct _node *next;
}node;

typedef struct{
	int nrnod;
	int nrmuc;
}numar;
node *adj[v_max];
graph_node graph[v_max], Q[max];
int edges[e_max];
int cap, coada, lung;
int test;

void generate(int n, int m){
	memset(adj, 0, n*sizeof(node*));
	FillRandomArray(edges, m, 0, n*n-1, true);
	for(int i=0; i<m;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		node *nod=new node;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;
	}
}

void enqueue(graph_node *Q, graph_node *x)
{
    Q[coada]=*x;
	if(coada=lung) coada=1;
	else coada=coada+1;
	lung++;
}

graph_node* dequeue(graph_node *Q)
{
	graph_node *x=&Q[cap];
	if(cap=lung)
		cap=1;
	else cap=cap+1;
	lung--;
	return x;
}

void bfs(numar *G, graph_node *s)
{
	
	for(int i=1; i<G->nrnod; i++)
	{	//profiler.countOperation("OP", test,1);
		test++;
		if(&graph[i]!=s)
		{
			graph[i].color=white;
			graph[i].dist=max;
			graph[i].parent=NULL;
			graph[i].key=i;

		}
	}
	s->color=gray;
	s->dist=0;
	s->parent=NULL;
	//s->key=0;
	cap=0;
	coada=0;
	lung=0;
	test+=2;
	//profiler.countOperation("OP", test,2);
	enqueue(Q,s);
	while(lung!=0){
		graph_node *u;
		u=dequeue(Q);
		node *v=adj[u->key];
		test++;
		//profiler.countOperation("OP", test,1);
		while(v!=NULL)
		{
			if (graph[v->key].color=white)
			{
				graph[v->key].color=gray;
				graph[v->key].dist= u->dist +1;
				graph[v->key].parent=u;
				enqueue(Q, &graph[v->key]);
			}
			v=v->next;
		}
		u->color=black;
	}
}


void print(numar G, graph_node *s, graph_node *v)
{
	if(s==v) printf("Path: %d\n", s->key);
	else if(v->parent==NULL) printf("No path from %d to %d exists\n", s->key, v->key);
	else {print(G, s, v->parent);
	printf("Path: %d\n", v->key);
	}

}

void main()
{

	numar *G=(numar *)malloc(sizeof(numar));

	////////////Exemplu///////////
	generate(5,9);
	for(int i=0; i<6; i++)
	{
		if(adj[i]!=NULL)
		printf("%d", adj[i]->key);
	}
	G->nrmuc=5;
	G->nrnod=9;
	bfs(G, &graph[0]);
	for(int i=0;i<G->nrnod;i++)
		for(int j=0;j<G->nrmuc; j++)
			print(*G,&graph[j], &graph[i]);

		

	////// noduri constante, muchii variabile
/*FILE*fd;
	fd=fopen("afisare1.csv","w");
	fprintf(fd,"i;test\n");*/
	/*for(int i=1000; i<5000; i+=100)
	{
		printf("i=%d\n", i);
		test=i;
		G->nrnod=100;
		G->nrmuc=i;
		generate(G->nrnod, G->nrmuc);
		for(int j=0;j<G->nrnod;j++)
			bfs(G,&graph[j]);
		fprintf(fd,"%d ; %d\n",i,test);
	}
	*/


	///noduri variabile, muchii constante
	/*for(int i=100; i<500; i+=10)
	{
		printf("i=%d\n", i);
		test=i;
		G->nrnod=i;
		G->nrmuc=9000;
		generate(G->nrnod, G->nrmuc);
		for(int j=0;j<G->nrnod;j++)
			bfs(G,&graph[j]);
			fprintf(fd,"%d ; %d\n",i,test);
	}
	*/
	//profiler.showReport();




}
	



