/*		Student:					***ANONIM*** ***ANONIM***
		Group:						***GROUP_NUMBER***

	    Requirements:			    You are required to implement correctly and efficiently 3 direct sorting methods 
									(Bubble Sort, Insertion Sort � using either linear or binary insertion and Selection Sort)
									Input: sequence of numbers <a1,a2,...,an>
									Output: an ordered permutation of the input sequence <a1',a2',...,an'>

	  Special requirements:			Generate input data for the cases :best and worse and , for each algorithm (sorting method);
									For the average case you willl take m random cases , and compute their mean ("median"). One 
									will use the same m arrays as input for each method (assume m>=5).The analysis for average 
									case consists on repeated measurements that will be counted (but only the assignments and 
									comparisons will be counted).
	  Conclusions,Interpretations:  From this laboratory work I have learned about execution time for different sorting algorithms,
									also the worst and best case possible for each method. I found it interesting to work with random 
									generator function srand() , to make some graphs based on the algorithms applied for large input data.
									Also, after this assignment I know better which sorting method is appropiate to use , in different situatians.
									For Bubble Sort the best case possible is when the array is already sorted ( nondecreasing - 
									is the case when bubble sort is optimal) , and the worst case possible is when the vector is sorted in reverse order.
									For insertion sort the best case scenarios and the worst case are similar with the Bubble Sort.
									For selection sort there is unsignificantly difference for arrays which are sorted asscendig and descending , because
									this method provides approximately the same number of operations for any state of the array.
									After drawing the graph I noticed some interesting behaviours for the worse case and average case selection sort ( for assignments
									count ) has a linear time.
	  
	 
*/








#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define nmax 10000
#define max 10000

int v[nmax],a[max];
int n;

/*
nraS -number of selection assignments
nrcS -number of selection comparison 
nraB -number of bubble assignments
nrcB -number of bubble comparison 
nraI- number of insertion assignments
nrcI- number of insertion comparison 
*/

int nraS=0;
int nrcS=0;
int nraB=0;
int nrcB=0;
int nraI=0;
int nrcI=0;


/*
This function rebuilds the vector randomly  generated.
*/
void recons(int t,int v[],int a[])
{
	int i;
	for(i=0;i<t;i++)
	{
		v[i]=a[i];
	}
}
/*
This function builds a vector with t elements , with random values.
*/
void random(int t)
{
	int i;
	srand(time(NULL));
	
	for(i=0;i<t;i++)
	{
		v[i]=rand()%max;
	}
}
/*
This function builds a vector with t elements with random values in ascending order.
*/
void randomC(int t)
{
	int i;
	srand(time(NULL));
	v[0]=rand()%50;

	for(i=1;i<t;i++)
	{
		v[i]=v[i-1]+rand()%100;
	}
}
/*
This function builds a vector with t elements with random values in descending order.
*/
void randomD(int t)
{
	int i,aux;
	srand(time(NULL));
	randomC(t);
	for(i=0;i<t/2;i++)
	{
		aux=v[i];
		v[i]=v[t-1-i];
		v[t-1-i]=aux;
	}
}

/*
Bubble Sort function
*/

void bubble()
{
	int i,j,aux;
	for(i=0;i<n-1;i++)
		for(j=0;j<n-1;j++)
		{
			nrcB++;
			if (v[j]>v[j+1])
			{
				nraB=nraB+2;
				aux=v[j];
				v[j]=v[j+1];
				v[j+1]=aux;
			}
			}
}

/*
Selection Sort function
*/
void selectie()
{
	int min,poz,aux,i,j;
	for(i=0;i<n-1;i++)
	{
		poz=i;
		for(j=i+1;j<n;j++)
		{
			nrcS++;
			if (v[poz]>v[j]) 
			{
				poz=j;
			}
		}
		nraS=nraS+3;
		aux=v[i];
		v[i]=min;
		v[poz]=aux;
	}
}
/*
Insertion Sort function
*/
void insertie()
{
	int i,j,k,aux;
	for(i=1;i<n;i++)
	{
		aux=v[i];
		j=i-1;
		nrcI++;
		while ((aux<v[j]) && (j>=0))
		{
			nrcI=nrcI+1;
			v[j+1]=v[j];
			nraI=nraI+1;
			j=j-1;
		}
		v[j+1]=aux;
		nraI=nraI+1;
	}
}

void main()
{
	int i,j;
	int ind;

	//Remember the average total operations done for the sorting algorithms
	int media,mediaS,mediaI,mediaB;
	int t;
	int aux[nmax];

	FILE* f1;
	FILE* f2;
	FILE* f3;
	//f1 - file for the values of average case
	
	f1=fopen("average.csv","w");
	//f2 - file for the values of worse case
	
	f2=fopen("best.csv","w");
	//f3 -  file for the values of worse case
	
	f3=fopen("worse.csv","w");
	

	// will choose the sorting option for a vector introduced from the keyboard
	printf("Choose ind := \n 1 - for reading a vector from the keyboard or \n 0 - for the all test cases:\n");
	scanf("%d",&ind);

	//sort the vector introduced from the keyboard
	if (ind==1)
	{
		printf("Give the number of elements n=");
		scanf("%d",&n);
		for(i=0;i<n;i++)
		{
		printf("v[%d]=",i);
		scanf("%d",&v[i]);
		}
		recons(n,a,v);
	
    //rebuild the array using bubble sort
	recons(n,v,a);
	bubble();
	printf("\nAfter Bubble sort the array is    :");
	for(i=0;i<n;i++)
		printf("%d ",v[i]);

	recons(n,v,a);
   //sort the vector using the selection sort
	selectie();
	printf("\nAfter Selection sort the array is :");
	for(i=0;i<n;i++)
		printf("%d ",v[i]);

	recons(n,v,a);
   //sort the vector using the insertion sort
	insertie();
	printf("\nAfter Insertion sort the array is : ");
	for(i=0;i<n;i++)
		printf("%d ",v[i]);

	}
	//start the test cases for the porblem 
if (ind==0)
   {
		t=100;
		fprintf(f1,"n;nraS;nrcS;nraS+nrcS;nraB;nrcB;nraB+nrcB;nraI;nrcI;nraI+nrcI\n");
		 /* first - all the tests for average case are generated	
			100 test cases will be generated , each case having 5 vectors to sort 
		 */
	
	   for(i=1;i<=100;i++)
		 {  //reset the variables which hold the number of operations for each test
		
			nraI=0;
			nrcI=0;
			nraB=0;
			nrcB=0;
			nraS=0;
			nrcS=0;
		
		for(j=1;j<=5;j++)
			{
			random(t);
			n=t;
			recons(t,a,v);
	
			selectie();
			recons(t,v,a);

			insertie();
			recons(t,v,a);
			bubble();
			}
	
			mediaS=(nraS+nrcS)/5;
			mediaB=(nraB+nrcB)/5;
			mediaI=(nraI+nrcI)/5;

			//introduce the datas found in the f1 file
		   fprintf(f1,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;\n",t,nraS/5,nrcS/5,mediaS,nraB/5,nrcB/5,mediaB,nraI/5,nrcI/5,mediaI);
			printf("mediu test %d\n",i);
			t=t+100;
	   }

	t=100;
	fprintf(f2,"n;nrcS;nraS;nrcS+nraS;nrcI;nraI;nrcI+nraI;nrcB;nraB;nrcB+nraB\n");
	fprintf(f3,"n;nrcS;nraS;nrcS+nraS;nrcI;nraI;nrcI+nraI;nrcB;nraB;nrcB+nraB\n");
	/*do the tests for best and worse case scenarious */
	
	for(i=1;i<=100;i++)
		{
				
			nraI=0;
			nrcI=0;
			nraB=0;
			nrcB=0;
			nraS=0;
			nrcS=0;
			n=t;
			//do the tests for best case
			randomC(t);
			recons(t,a,v);

			selectie();
			recons(t,v,a);

			insertie();
			recons(t,v,a);
			bubble();
			    //introduce in f2 the datas for best case scenarious
				fprintf(f2,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;\n",t,nrcS,nraS,nrcS+nraS,nrcI,nraI,nrcI+nraI,nrcB,nraB,nrcB+nraB);
				printf("Best Case test %d\n",i);
	
			//do the tests for worse case
			randomD(t);
			recons(t,a,v);
			
			selectie();
			recons(t,v,a);
			
			insertie();
			recons(t,v,a);
			bubble();
	
			
			//introduce in f3 the datas for worse case scenarious
				fprintf(f3,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d;\n",t,nrcS,nraS,nrcS+nraS,nrcI,nraI,nrcI+nraI,nrcB,nraB,nrcB+nraB);
				printf("Worse Case test %d\n",i);
		 t=t+100;
	}
	
  }
	fclose(f1);
	fclose(f2);
	fclose(f3);
	
	getch();
}
