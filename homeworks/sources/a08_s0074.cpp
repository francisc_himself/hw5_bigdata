//***ANONIM*** ***ANONIM***
//Grupa ***GROUP_NUMBER***
//Operatii de creare, union si cautare pe multimi disjuncte
//complexitatea algoritmului este O(1) in analiza amortizata
#include <stdio.h>
#include<stdlib.h>
#include <conio.h>

int nr_op;
struct node
{
	int val;
	int rank;
	node *p;
};
struct arc
{
	int x;
	int y;
};

void makeSet(node* &x)
{
	nr_op++;
	x->p=x;
	x->rank=0;
}


void link(node *x,node *y)
{
	if(x->rank>y->rank)
		y->p=x;
	else
		x->p=y;
	if(x->rank==y->rank)
		y->rank=y->rank+1;
}

node* findSet(node*x)
{
	nr_op++;
	if(x!=x->p)
		x->p=findSet(x->p);
	return x->p;
}

void Union(node* x, node* y)
{
	nr_op++;
	link(findSet(x),findSet(y));
}

int main()
{
	
	int size=10000;
	int nrarcs=10;
	FILE* pf;
	pf=fopen("assignement8.csv","a");
	node** n=(node**)malloc(size*sizeof(node*));

	
	for(int i=0;i<size;i++)
	{
		n[i]=(node*)malloc(sizeof(node));
		n[i]->val=i;
	}
	for(int i=0;i<size;i++)
	{
		makeSet(n[i]);
	}

	for(nrarcs=10000;nrarcs<60000;nrarcs+=100)
	{
	nr_op=0;
	

		for(int i=0;i<nrarcs;i++)
	          Union(n[rand()%size],n[rand()%size]);
		printf("%d\n",nr_op);
		fprintf(pf,"%d,%d\n",nrarcs,nr_op);
	}
		
	fclose(pf);
	getch();
	return 0;
}