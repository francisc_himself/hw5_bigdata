//***ANONIM*** ***ANONIM***
//Grupa ***GROUP_NUMBER***
//Transformari intre diferite reprezentari ale arborilor multicai
//complexitatea algoritmului este O(n)
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>

struct node{
	int val;
	node *child;
	int nr_ch;
};

struct binary{
	int val;
	binary *son,*bro;
};
binary *rootB;
node *noduri;
int root;
//transformarea din reprezentare sub forma de vector de tati in reprezentare multi-way
void T1(int *t,int n)
{
	noduri=new node[n];
	int pos[100],i;

	memset(pos,0,sizeof(pos));

	for(i=0;i<n;i++)
	{
		noduri[i].nr_ch=0;
		noduri[i].child=NULL;
	}
	for(i=0;i<n;i++)
		if(t[i]>=0)
			noduri[t[i]].nr_ch++; 
		else
			root=i;
	for(i=0;i<n;i++)
		if(noduri[i].nr_ch>0)
			noduri[i].child=new node[noduri[i].nr_ch];
	for(i=0;i<n;i++)
	{
		noduri[i].val=i;
		if(t[i]>=0)
		{
			noduri[t[i]].child[pos[t[i]]]=noduri[i];   
			pos[t[i]]++; 
		}
	}

}
//transformare din reprezentarea multi-way in reprezentare binara (fiecare nod contine o legatura catre fiul cel mai din stanga si fratele din dreapta)
void T2(binary *current, int Mroot)
{
	if(noduri[Mroot].nr_ch>0)
	{
		current->son=new binary;
		current->son->son=NULL;
		current->son->bro=NULL;
		current->son->val=noduri[Mroot].child[0].val;

		int i;
		binary *son=current->son;  

		for(i=1;i<noduri[Mroot].nr_ch;i++)
		{
			son->bro=new binary;
			son->bro->bro=NULL;
			son->bro->son=NULL;
			son->bro->val=noduri[Mroot].child[i].val;
			son=son->bro; 
		}
	}

	if(current->son!=NULL)
		T2(current->son,current->son->val);

	if(current->bro!=NULL)
		T2(current->bro,current->bro->val);
}
//functie de pretty print
//se apeleaza pe modul de reprezentare binara
void print(binary *r,int space)
{
	int i;

	for(i=1;i<=space;i++)
		printf(" ");

	printf("%d\n",r->val);

	if(r->son!=NULL)
		print(r->son,space+1);

	if(r->bro!=NULL)
		print(r->bro,space);
}

int main()
{
	int n=9,t[9]={1, 6, 4, 1, 6, 6, -1, 4, 1};
	
	
	T1(t,n);

	rootB=new binary;
	rootB->bro=NULL;
	rootB->son=NULL;
	rootB->val=root;
	T2(rootB,root);

	print(rootB,0);
	getch();
	return 0;
}