#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <stdlib.h>

//sir in ordine crescatoare
int sircresc(int n, int sir[])
{
	for (int i=0; i<=n; i++)
	{
		sir[i]=i+1;
	}
	return 0;
}

//sir un in ordine descrescatoare
int sirdesc(int n, int sir[])
{
	for (int i=0; i<=n; i++)
	{
		sir[i]=n-i;
	}
	return 0;
}

// sir cu elemente aleatorii
int sirrand(int n, int sir[])
{
	for (int i=0; i<=n; i++)
	{
		sir[i]=rand();
	}
	return 0;
}

//din sursa cautam ***ANONIM*** si interschimbam cu sir[i] si crestem destinatia cu 1
int selectie(int n, int sir[], int *AS, int *CS)
{
	int ***ANONIM***=0;
	int j***ANONIM***,aux=0;
	long a=0,c=0;
	for (int i=0; i<=n-1; i++)
	{
		a++;
		***ANONIM***=sir[i];
		j***ANONIM***=i;
		for (int j=i+1; j<=n; j++)
		{
			c++;
			if (sir[j]<***ANONIM***)
			{
				a++;
				***ANONIM***=sir[j];
				j***ANONIM***=j;
			}
		}
		aux=sir[i];
		a+=3;
		sir[i]=sir[j***ANONIM***];
		sir[j***ANONIM***]=aux;
	}
	*AS=a;
	*CS=c;
	return 0;
}

//inseram elementul i in destinatie la locul lui
int insertie(int n, int sir[], int *AI, int *CI)
{
	int k,x=0;
	int a=0,c=0;
	for (int i=1; i<=n; i++)
	{
		a++;
		x=sir[i];
		k=1;
		while (x>sir[k])
		{ k++; 
		c++;}
		c++;
		for (int j=i; j>=k+1; j--)
		{ 
			sir[j]=sir[j-1];
			a++;	
		}
		a++;	
		sir[k]=x;
	}
	*AI=a;
	*CI=c;
	return 0;
}

// test de vecinatate- daca sir[i]>sir[i+1] le interschimbam
int bubble(int n, int sir[], int *AB, int *CB)
{
	int aux=0;
	int a=0,c=0;
	for (int i=0; i<=n-1; i++)
	{
		for (int j=0; j<=n-i; j++)
		{
			c++;
			if (sir[j]>sir[j+1])
			{
				a+=3;
				aux=sir[j];
				sir[j]=sir[j+1];
				sir[j+1]=aux;
			}
		}
	}
	*AB=a;
	*CB=c;
	return 0;
}

int main()
{
	int i;
	int AS=0,CS=0,AI=0,CI=0,AB=0,CB=0;
	int sir1[10000],sir2[10000],sir3[10000];
	FILE *pf;
	srand(time(NULL));
	
	//caz favorabil
	pf=fopen("Favorabil.csv", "w");
	fprintf(pf, " n , ASel , CSel , TSel , AIns , CIns , TIns , ABub , CBub , TBub \n");
	for (i=100; i<=10000; i+=100)
	{
		//pt. fiecare sortare generam un sir crescator
		sircresc(i, sir1);
		sircresc(i, sir2);
		sircresc(i, sir3);
		//facem sortarile
		selectie(i, sir1, &AS, &CS);
		insertie(i, sir2, &AI, &CI);
		bubble(i, sir3, &AB, &CB);
		//scriem valorile in fisier
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n", i, AS, CS, AS+CS, AI, CI, AI+CI, AB, CB, AB+CB);
		AS=0;
		CS=0;
		AI=0;
		CI=0;
		AB=0;
		CB=0;
	}
	fclose(pf);

	//caz defavorabil
	pf=fopen("Defavorabil.csv", "w");
	fprintf(pf, " n , ASel , CSel , TSel , AIns , CIns , TIns , ABub , CBub , TBub \n");
	for (i=100; i<=10000; i+=100)
	{
		//pt. fiecare sortare generam un sir descrescator
		sirdesc(i, sir1);
		sirdesc(i, sir2);
		sirdesc(i, sir3);
		//facem sortarile
		selectie(i, sir1, &AS, &CS);
		insertie(i, sir2, &AI, &CI);
		bubble(i, sir3, &AB, &CB);
		//scriem valorile in fisier
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n", i, AS, CS, AS+CS, AI, CI, AI+CI, AB, CB, AB+CB);
		AS=0;
		CS=0;
		AI=0;
		CI=0;
		AB=0;
		CB=0;
	}
	fclose(pf);

	//caz mediu statistic
	pf=fopen("Mediu.csv", "w");
	fprintf(pf, " n , ASel , CSel , TSel , AIns , CIns , TIns , ABub , CBub , TBub \n");
	long nAS=0,nCS=0;
	long nAI=0,nCI=0;
	long nAB=0,nCB=0;
	for (i=100; i<=10000; i+=100)
	{
		for (int k=0; k<5; k++)
		{
			sirrand(i, sir1);  //generam sir aleator
			selectie(i, sir1, &AS, &CS);   //facem sortarea
			nAS+=AS;  //adunam valorile obtinute pt a deter***ANONIM***a mediul
			nCS+=CS;
			AS=0;
			CS=0;
		}
		for (int k=0; k<5; k++)
		{
			sirrand(i, sir2);   //generam sir aleator
			insertie(i, sir2, &AI, &CI);   //facem sortarea
			nAI+=AI;   //adunam valorile obtinute pt a deter***ANONIM***a mediul
			nCI+=CI;
			AI=0;
			CI=0;
		}
		for (int k=0; k<5; k++)
		{
			sirrand(i, sir3);   //generam sir aleator
			bubble(i, sir3, &AB, &CB);   //facem sortarea
			nAB+=AB;  //adunam valorile obtinute pt a deter***ANONIM***a mediul
			nCB+=CB;
			AB=0;
			CB=0;
		}
		//scriem valorile in fisier
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n", i, nAS/5, nCS/5, (nAS+nCS)/5, nAI/5, nCI/5, (nAI+nCI)/5, nAB/5, nCB/5, (nAB+nCB)/5);
			nAS=0;nCS=0;
		nAI=0;nCI=0;
		nAB=0;nCB=0;
	}
	fclose(pf);

	return 0;
}

