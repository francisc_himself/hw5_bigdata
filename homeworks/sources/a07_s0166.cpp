/*
Pentru a realiza implementarea celor doi algoritmi de transformare in arbore multicai si apoi in arbore binar am utilizat doua structuri: nod_child,
resprectiv nod_fii. In functie de cum se reprezinta un arbore multicai sau un arbore binar am definit si structurile, apoi am implementat cele doua operatii
de transformare, ambele fiind de complexitate O(n).
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int radacina;
FILE *fis1,*fis2,*fis3;
typedef struct nod_child{
		int cheie;
		int nr_child;
		struct nod_child** fii;
}T_1;

typedef struct nod_fii{
		int cheie;
		struct nod_fii* fiu;
		struct nod_fii* frate;
}T_2;

nod_child* T1(int *pi,int dim)
{ nod_child* radacina1;
  radacina1 =(nod_child *)malloc(dim*sizeof(nod_child));//alocarea memoriei pt radacina
  for (int i=0;i<dim;i++)
  {
	  radacina1[i].cheie=i;//se retine numarul fiilor pana la nodul respectiv
	  radacina1[i].nr_child=0;//se initializeaza cu 0 nr copiilor pt fiecare nod
	  
	  if (pi[i]!=-1) radacina1[pi[i]].nr_child++;//se calculeaza numarul fiilor
	  else radacina=i+1;
  }
  
  for (int i=0;i<dim;i++)
  {
	  radacina1[i].fii=(nod_child**)malloc(radacina1[i].nr_child*sizeof(nod_child*));//se aloca memorie pentru fii
	  radacina1[i].nr_child=-1;//se initializeaza la -1 numarul fiilor
  }
  for (int i=0;i<dim;i++)
  {
	  int j=pi[i];
	  if (j!=-1)
	  { 
		  radacina1[j].nr_child++;//creste numarul fiilor pentru nodul specificat in pi
	      radacina1[j].fii[radacina1[j].nr_child]=&radacina1[i];//se retin numarul de fii 		  
	  }
  }  
  return radacina1;
}

nod_fii* T2(nod_child *par,nod_child *r,int k)
{ 
  nod_fii* p=(nod_fii*)malloc(sizeof(nod_fii));//se aloca memoria
  p->cheie=r->cheie;
  p->frate=0;
  p->fiu=0;
  fprintf(fis3,"nodul fiu:%d parintele:%d \n",p->cheie,par->cheie);
  if (r->nr_child!=-1){
	  p->fiu=T2(r,r->fii[0],0);
  }
  else p->fiu=0;
  if (par->nr_child>k){
	  p->frate=T2(par,par->fii[k+1],k+1);
  }
  else p->frate=0;
  return p;
}
void pretty(nod_fii* t,int nivel)
{  
	fprintf(fis1,"%d	",t->cheie);
	if (t->fiu!=0) pretty(t->fiu,nivel+1);
	fprintf(fis1,"\n");
	for (int i=0;i<nivel;i++)
		fprintf(fis1,"	");
    if (t->frate!=0) pretty(t->frate,nivel);	
	
}


int main()
{int pi[]={0,2,7,5,2,7,7,-1,5,2};
fopen_s(&fis1,"pretty.txt","w");
fopen_s(&fis2,"t1.txt","w");
fopen_s(&fis3,"t2.txt","w");
  nod_child* radacina1;
  nod_fii* radacina2;
 
 int dim=9;
 fprintf(fis2,"---------transformarea I----------\n");
 radacina1=T1(pi,dim);
 for (int i=0;i<dim;i++){
	 fprintf(fis2,"nodul %d:",radacina1[i].cheie);
	 for (int j=0;j<=radacina1[i].nr_child;j++){
		  fprintf(fis2,"%d ", radacina1[i].fii[j]->cheie);
	  }
	  fprintf(fis2,"\n");
  }
  nod_child* p=(nod_child*)malloc(sizeof (nod_child));
  p->cheie=-1;
  p->nr_child=0;
  p->fii=(nod_child **)malloc(sizeof(nod_child));
  p->fii[0]=&radacina1[radacina-1];
  fprintf(fis3,"---------transformarea II----------\n");
  radacina2=T2(p,&radacina1[radacina-1],0);
  fprintf(fis1,"---------Afisare pretty----------\n");
   pretty(radacina2,0);
  _getch();
  
	return 0;
}

