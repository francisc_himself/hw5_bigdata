#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>


/*
Heap-data structure viewed as a complete BT
Build- heap- applies in case the dimension is known in advance

Bottom-up approapch:
-1/2 out of all nodes are heaps from the beginning('leaves' in a complete BT)
-apply heapify to the first non-leaf node, that has the largest index and has at least one child
-go to the next indexed node(sibling to the left of the first processed element
-continue the procedure until reaching the root
Running time: for 1 element processing: O(h); for n elements: O(n)

Top-down approach:
-adds a new element at the bottom
-rebuild the heap, a bottom up approach(bubble the bottom element upper in the heap until it finds a larger value parent)
Running time: for 1 element processing: O(h); for n elements: O(nlgn)

Analyzing the graphics obtained in average case, we can say that the BOTTOM UP approach is more efficient than the TOP DOWN approach;
Bottom up performs between 440-47030 operations, while top-down between 519-60719, for an input data in the range 100-10000 wih an increment of 100
In worst case, definitely bottom up procedure is more efficient than top down, the last one performing between 2019-464523 operations


As a conclusion: talking about advantages, bottom-up procedure is faster applied on a fixed dimension(half of the array), 
				 while the top down approach applies on the whole array, dealing with variable dimension=>slower
				 Regarding their usage: Bottom-up is used for sorting, and top-down as priority queues(have an associated key)
				 Running time=> both approaches are liniar

*/

long compUp=0;
long assignUp=0;
long compDown=0;
long assignDown=0;
int dime=0;

void heapify(long A[], int i, int n)  //i=index of the root, elem. to be added
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    compUp=compUp+1;
    if (left<=n && A[left]>A[i]) 
        largest=left;	//root, left or right child index
    else
        largest=i;

    compUp=compUp+1;
    if (right<=n && A[right]>A[largest])
        largest=right;
    if (largest!=i) //one of the children larger than the root
        {
            assignUp+=3;
			//swap root with largest child
            aux=A[i];
            A[i]=A[largest];
            A[largest]=aux;
            heapify(A,largest,n);  //continue the process on the heap
        }
}

void bh_bottomup(long A[],int n)
{
    int i;
    for (i=n/2;i>=1;i--) //from the non-leave nodes until we reach the root
        heapify(A,i,n); //build the heap out of 2 already built heaps and 1 node
}

void insert_heap(long A[],long key,int *dim) //takes as an input the key of the new element to be inserted into max-heap A.
{
    (*dim)=(*dim)+1;  //first expands the max-heap by adding to the tree a new leaf
    int k;
	long aux;
    k=(*dim);
    while (k>1 && A[k/2]<key) //calls HEAP-INCREASE-KEY to set the key of this new node to its correct value and maintain the max-heap property
    {   
        compDown++;
        assignDown+=3;
		aux=A[k];
        A[k]=A[k/2];
		A[k/2]=aux;
        k=k/2;   //!parent of a node is computed as i/2
    }
   compDown++;
  // A[k]=key;
}

//We can build a heap by repeatedly calling MAX-HEAP-INSERT to insert the elements into the heap
void bh_topdown(long A[],int n)
{
    int i;
	dime=1;
    for (i=2;i<=n;i++){
        insert_heap(A,A[i],&dime);  //A[i] is the 'key'
}
}

void pretty_print(long a[],int n){
	int i,j,pow=2,level=1,nr=5; 
	printf("             %d\n\n ",a[1]); //print the first element in the array
	for(i=2;i<n;){
		level=pow; //save the value of pow
		for(j=0;j<nr;j++) 
			printf("  ");  //print a number of spaces at the beginning of each level
		while(i<n && level>0){  //at each level print 2^i elements
			printf(" %d ",a[i]);
			i++;  //increment the index in the array
			level--;  //decrement the pow
		}
			printf("\n\n");	
			pow=pow*2;  //prepare the power for the next level display
			nr--;   //decrement the nr of spaces to be displayed on the next level
	}
	
}


int main()
{   
    srand(time(NULL));
    long bu[10001];
    long td[10001];
    int i,j,k,nr;
    long compup,compdown,assignup,assigndown;
    FILE *f,*w;
    f=fopen("average.txt","w");
	w=fopen("worst.txt","w");

	//verify the 2 procedures on an array of length 10
	printf("Bottom-up procedure: \n ");
	long a[11]={0,4,16,14,8,10,9,1,2,3,5};
	bh_bottomup(a,11);
	pretty_print(a,11);
	
	printf("\n");
	printf("Top-down procedure: \n ");
	long a1[]={0,4,16,14,8,10,9,1,2,3,5};
    bh_topdown(a1,11);
	pretty_print(a1,11);
	
	//average case
    for (i=100;i<=10000;i=i+100)
	{
		//sum of comparisons and assignments 
		compUp=compDown=assignUp=assignDown=0;

		for (j=1;j<=5;j++)
		{
			nr=0;
			compup=0;compdown=0;
			assignup=0;assigndown=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				bu[nr]=td[nr]=rand();
			}
			bh_bottomup(bu,i);
			bh_topdown(td,i);
			compup+=compUp;
			compdown+=compDown;
			assignup+=assignUp;
			assigndown+=assignDown;
		}
			fprintf(f,"%d %ld %ld %ld %ld %ld %ld\n",i,compup/5,assignup/5,compup/5+assignup/5,compdown/5,assigndown/5,compdown/5+assigndown/5);
			
	}


	//worst case
	for (i=100;i<=10000;i=i+100)
	{
		compUp=compDown=assignUp=assignDown=0;
		nr=1;
		for (j=1;j<=i;j++) //sorted 
		{
			bu[nr]=j;
			td[nr]=j;
			nr++;
		}
		bh_bottomup(bu,i);
		bh_topdown(td,i);
		fprintf(w,"%d %ld %ld %ld %ld %ld %ld\n",i,compUp,assignUp,compUp+assignUp,compDown,assignDown,compDown+assignDown);
		
	}

	fclose(f);
	fclose(w);
	getch();
    return 0;
}
