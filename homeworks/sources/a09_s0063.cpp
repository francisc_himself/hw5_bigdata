#include<conio.h>
#include<stdio.h>
#include<stdlib.h>

struct nod
{
    int data;
    struct nod *next;
};

struct vertex
{
	int culoare;
	int d;
	int p;
}VERTEX[11];

int	matrix[11][11] = { {0,0,0,0,0,0,1,1,0,1,0},
	{0,0,0,0,0,0,1,1,0,1,0},
	{0,0,0,1,1,0,1,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0,0},
	{0,1,1,0,0,1,0,0,0,0,0},
	{0,1,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,1,0},
	{0,1,0,0,0,0,0,0,1,0,1},
	{0,0,0,0,0,0,0,0,0,1,0}
};
struct nod *header,*last;

int is_empty(struct nod *header)
{
    if(header==NULL)
        return 1; //TRUE
    else
        return 0; //FALSE
}
struct nod *newnod(int d)
{
    struct nod *temp;
    temp=(struct nod *) malloc(sizeof(nod));
    temp->data=d;
    temp->next=NULL;
    return temp;
}
struct nod *insert(struct nod *header, int d)
{ 
    struct nod *temp, *headertemp;
    temp=newnod(d);
    if(header==NULL) {
        header=temp;
        return header;
    }
    headertemp=header;
    while(headertemp->next!=NULL)
    {
        headertemp=headertemp->next;
    }
    headertemp->next=temp;
    return header;
}
int pop ()
{ // deleteFront
    struct nod *temp;
	int val;
    if(header==NULL)
        return 0;
    temp=header;
	val = temp->data;
    header=header->next;
    free(temp);
    return val;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void add_items(int data)
{
   
header=insert(header,data);
   
}

void show_items()
{
    struct nod* tmp;
    tmp=NULL;
   
    for(tmp=header;tmp!=NULL;tmp=tmp->next)
    {
        printf("%d",tmp->data);
    }
    printf("\n");
}

void BFS(int s)
{
	int i,j,u,l=10;
	for(i=1;i<=10;i++)
	{
		VERTEX[i].culoare=0;
		VERTEX[i].d=999;
		VERTEX[i].p=-1;
	}
	VERTEX[s].culoare=1;
	VERTEX[s].d=0;
	VERTEX[s].p=-1;
	 
	header=NULL;

	add_items(s);

	while(is_empty(header)==0)
	
	{
		u=pop();
		
		for(i=1;i<=10;i++)
		{
			if(matrix[u][i]==1)
			{
				if(VERTEX[i].culoare == 0)
				{
					VERTEX[i].culoare=1;
					VERTEX[i].d = VERTEX[u].d+1;
					VERTEX[i].p = u;
					add_items(i);
					
				}
			}
			
			VERTEX[u].culoare=2;
		}
		show_items();
	
	}
}

int main()
{
   BFS(1);
   for(int i=1;i<=10;i++)
   {
	   printf("nod: %d, culoare: %d, distanta: %d, parinte: %d",i,VERTEX[i].culoare,VERTEX[i].d,VERTEX[i].p);
	   printf("\n");
   }
	getch();
	return 0;
}

