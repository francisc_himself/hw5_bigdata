/*Assign 5
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***
04.04.2013->11.04.2013
Operatiile de cautare si stergere intr-o tabela de dispersie folisind adresarea deschisa si verificarea patratica.

In functie de cheia transmisa ca parametru cele doua functii parcurg aceeasi secventa de pozitii in tabela.

OBSERVATII:
-> numarul operatiilor este puternic influentat de numerele care sunt puse in tabela.
-> cele doua constante c1, c2 modifica considerabil rezultatele.
-> pentru chei negasite se efectueaza mult mai multe accese decat pentru cele gasite.
*/
#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>

#define DIM_MAX 9973

int T[DIM_MAX];
int m, n, N;
int c1=2, c2=129;
int acc=0;

int h(int  k, int i)
{
	return ((k%N + c1*i + c2*i*i)%N);
}

int HASH_INSERT(int T[],int k)
{
	int i = 0, j;
	do{
		j = h(k, i);
		if(T[j]==NULL)
		{
			T[j]=k;
			return j;
		}
		i++;
	}while(i < N);

	printf("Tabela plina!!");
	return -2;
}

int HASH_SEARCH(int T[],int k)
{
	int i=0, j;

	do{
		j = h(k, i);
		/****/ acc++;
		if(T[j]==k)
			return j;
		i++;
	}while(T[j]!=NULL && i<N);

	return NULL;
}

void HASH_PRINT(int T[])
{
	int i;

	printf("Tabela de dispersie este: \n");
	for(i=0; i<N; i++)
		printf("%d ", T[i]);

	getch();
}

void testeaza()
{
	int poz, i;
	N=9;
	n=5;

	for(i=0;i<N;i++)
		T[i]=NULL;

	HASH_INSERT(T, 28);
	HASH_INSERT(T, 4);
	HASH_INSERT(T, 179);
	HASH_INSERT(T, 7);
	HASH_INSERT(T, 3769);

	HASH_PRINT(T);

	if((poz=HASH_SEARCH(T, 7))!=NULL)
		printf(" \n 7 gasit pe poz: %d \n", poz);
	else
		printf("\n Cheia 7 nu a fost gasita!");

	if((poz=HASH_SEARCH(T, 3769))!=NULL)
		printf(" \n 3769 gasit pe poz: %d \n", poz);
	else
		printf("\n Cheia 3769 nu a fost gasita!");
}

void genereaza()
{
	int i, j, caz;
	FILE *pf;
	N = 9973;
	m=3000;
	float a[]={0.8, 0.85, 0.9, 0.95, 0.99};
	int medG=0, medNG=0, maxG=0, maxNG=0, medTG=0, medTNG=0, maxTG=0, maxTNG=0;

	srand(time(NULL));

	pf = fopen("hashuri.csv", "w");
	fprintf(pf, "Factor umplere, Medie gasite, Max gasite, Medie negasite, Max negasite\n");

	for(i=0;i<5;i++)
	{
		n = a[i] *N;

		for(caz=1; caz<=5; caz++)
		{
			medG=0, medNG=0, maxG=0, maxNG=0;
			/*** INITIALIZAZ TABELA  ***/
			for(int ind=0; ind<N; ind++)
					T[ind]=NULL;

			/*** GENEREZ NUMERELE SI LE INSEREZ ***/
			for(j=0; j<n; j++)
				HASH_INSERT(T, 1+rand()%90000);

			/*** CAUT ELEMENTE NEGASITE ***/
			for(j=0; j<m/2; j++)
			{
				acc=0;
				HASH_SEARCH(T, rand()+90000);
				medNG += acc;
				if(acc>maxNG)
					maxNG=acc;
			}

			/*** CAUT ELEMENTE GASITE ***/
			for(j=0; j<m/2; j++)
			{
				acc=0;
				int aux = T[rand()%N];
				if(aux == NULL)
				{
				    j--;
				    continue;
				}
				HASH_SEARCH(T, aux);
				medG += acc;
				if(acc>maxG)
					maxG=acc;
			}
			medTG+=medG;
			medTNG+=medNG;
			maxTG+=maxG;
			maxTNG+=maxNG;
		}
        medTG=medTNG=maxTG=maxTNG=0;
		fprintf(pf, "%.2f, %d, %d, %d, %d\n", a[i], medTG/(5*1500), maxTG/5, medTNG/(5*1500), maxTNG/5);
	}
}

int main(){
	//testeaza();
	genereaza();
	printf("\nApasa o tasta pt finalizare!");
	getch();
	return 0;
}
