//***ANONIM*** ***ANONIM***
//Grupa ***GROUP_NUMBER***
//Tema nr. 6

#include<stdio.h>
#include<conio.h>
#include<stdlib.h>

int n,m;
long c1=0;
long c2=0;

typedef struct nod
{
	int key;
	int size;
	struct nod*stg;
	struct nod*dr;
	struct nod*parinte;
}TIP_NOD;

TIP_NOD* ***ANONIM***;

TIP_NOD* OS_SELECT(TIP_NOD *x, int i)//O(log n)
{
	int r=1;
	if (x->stg != NULL)
		r = x->stg->size+1;
	else
		r = 1;
	if (r == i)
		return x;
    else
	{
		if (i < r)
		{
			return OS_SELECT(x->stg,i);
		}
        else
		{
			return OS_SELECT(x->dr,i-r);
		}
    }
}

TIP_NOD* CONSTRUIRE_1_NOD(int x, int dim, TIP_NOD *stg, TIP_NOD *dr)
{
	TIP_NOD *q;
	q= (TIP_NOD*)malloc(sizeof(TIP_NOD));
	q->key=x;
	q->stg=stg;
	q->dr=dr;
	q->size=dim;
	c1=c1+4;
	if (stg!=NULL)
	{
		stg->parinte = q;
	}
	if (dr!=NULL)
	{
		dr->parinte=q;
	}
	q->parinte=NULL;
	c1++;
	return q;
}

TIP_NOD* CONSTR_BST(int stg, int dr)
{
	TIP_NOD *sub_stg,*sub_dr;
	if (stg > dr)
		return NULL;
	if (stg == dr)
		return CONSTRUIRE_1_NOD(stg, 1, NULL, NULL);
	else
	{
		int m =(stg + dr)/2;
		sub_stg = CONSTR_BST(stg, m-1);
		sub_dr  = CONSTR_BST(m+1, dr);
		return CONSTRUIRE_1_NOD(m, dr-stg+1,sub_stg, sub_dr);
	}
}

TIP_NOD* MINIM(TIP_NOD* x)
{
	while(x->stg!=NULL)
	{
		x = x->stg;
		c1++;
		c2++;
	}
	c2++;
	return x;
}

TIP_NOD* SUCCESOR(TIP_NOD* x)
{
	TIP_NOD *y;
	if(x->dr!=NULL)
	{
		c2++;
		return MINIM(x->dr);
	}
	c1++;
	y = x->parinte;
	while (y != NULL && x == y->dr)
	{
		c2+=2;
		x = y;
		y = y->parinte;
		c1+=2;
	}
	return y;
}

TIP_NOD* STERGERE(TIP_NOD* z)
{
	TIP_NOD *y;
	TIP_NOD *x;

	if ((z->stg == NULL) || z->dr == NULL)
	{
		c1++;
		y=z;
	}
	else
		y = SUCCESOR(z);
	c2++;
	if (y->stg != NULL)
	{
		c1++;
		x = y->stg;
	}
	else
	{
		c1++;
		x = y->dr;
	}
	c2++;
	if (x != NULL)
	{
		c1++;
		x->parinte = y->parinte;
	}
	c2++;
	if (y->parinte == NULL)
		***ANONIM*** = x;
	else
		if (y == y->parinte->stg)
		{
			c1++;
			y->parinte->stg = x;
		}
		else
		{
			c1++;
			y->parinte->dr = x;
		}
	c2+=2;
	if (y != z)
	{
		c1++;
		z->key = y->key;
	}
	return y;
}


void REFACERE_DIMENSIUNE(TIP_NOD *p)
{
	while (p != NULL)
	{
		c1++;
		p->size--;
		p=p->parinte;
		c1++;
	}
}


void JOSEPHUS(int n, int k)
{
	TIP_NOD *y,*z;
	int aux = n;
	int m = k;

	***ANONIM*** = CONSTR_BST(1, n);
	int i;
	for (i = 1; i < aux; i++)

	{
		y = OS_SELECT(***ANONIM***, k);
		printf("%d ",y->key);

		z = STERGERE(y);
		REFACERE_DIMENSIUNE(z);

		free(z);
		n--;
        k=(k+m-2)%n+1;
	}
}

int main()
{

	JOSEPHUS(7,3);
	printf("%d",***ANONIM***->key);
	/*
    FILE* f;
	f=fopen("fisier.csv","w");
	fprintf(f,"n,c1+c2\n");
	int i;
	for(i=100; i<=10000; i+=100)
	{
		c1=0;
		c2=0;
		JOSEPHUS(i,i/2);
		fprintf(f,"%d, %d\n",i,c1+c2);
	}
fclose(f);*/
	getch();
}
