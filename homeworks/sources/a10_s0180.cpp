#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
#define WHITE 0
#define GRAY 1
#define BLACK 2



#include "Profiler.h" 
Profiler profiler("DFS");


struct nod{
	int v;
	nod *next;
};

nod * first[60001], *last[60001];
int c[60001],p[60001],d[60001],f[60001],t;
int V, E;
bool VERTEX;


void generate(int n,int nr)
{
	int i,u,v;
	for(i=0;i<=n;i++)
		first[i]=last[i]=NULL;
	for(i=1;i<=nr;i++)
	{
		u = rand() % n + 1;
		v = rand() % n + 1;
		if(first[u] == NULL)
		{
			first[u]=(nod *)malloc(sizeof(nod *));
			while(v == u)
				v = rand() % n + 1;
			first[u]->v = v;
			first[u]->next = NULL;
			last[u] = first[u];
		}
		else {
			nod *p, *x;
			p = (nod *)malloc(sizeof(nod *));
			label1: x = first[u];
			while(x != NULL){
				if(x->v == v || v == u){
					v = rand() % n + 1;
					goto label1;
				}
				x = x->next; 
			}
			p->v = v;
			p->next = NULL;
			last[u]->next = p;
			last[u] = p;
		}
	}
}

void print(int n)
{
	int i;
	nod *p;
	for(i=1; i<=n; i++)
	{
		printf("%d : ",i);
		p=first[i];
		while(p != NULL)
		{
			printf("%d,",p->v);
			p = p->next;
		}
		printf("\n");
	}
}

void dfs_visit(int u)
{
	c[u] = GRAY;
	d[u] = t;
	t++;
	nod *x;
	int v;
	x = first[u];
	if(VERTEX)
		profiler.countOperation("Total - vertex", V, 1);
	else
		profiler.countOperation("Total - edge", E, 1);
	while(x != NULL)
	{
		v = x->v;
		x = x->next;
		if(VERTEX)
			profiler.countOperation("Total - vertex", V, 1);
		else
			profiler.countOperation("Total - edge", E, 1);
		if(c[v] == WHITE) 
		{
			p[v] = u;
			if(VERTEX)
				profiler.countOperation("Total - vertex", V, 1);
			else
				profiler.countOperation("Total - edge", E, 1);
			//printf("(%d %d) Tree\n",u,v);
			//printf("%d\n",v);
			dfs_visit(v);
		}
		else {
			if(VERTEX)
				profiler.countOperation("Total - vertex", V, 1);
			else
				profiler.countOperation("Total - edge", E, 1);
			
			//if(c[v] == GRAY) printf("(%d %d) Back\n",u,v);
			//else if(d[u] < f[v]) printf("(%d %d) Forward\n",u,v);
			//else printf("(%d %d) Cross\n",u,v);
			}
	}
	c[u] = BLACK;
	f[u] = t;
	t = t+1;
}

void initialize(int n)
{
	t=0;
	int i;
	for(i=1;i<=n;i++)
	{
		f[i]=0;
		d[i]=0;
		p[i]=0;
		c[i]=0;
	}
}

void dfs(int n)
{
	int u;
	for(u=1;u<=n;u++)
	{
		c[u]=WHITE;
		p[u]=0;
	}
	for( u=1;u<=n;u++)
	{
		if(VERTEX)
			profiler.countOperation("Total - vertex", V, 1);
		else
			profiler.countOperation("Total - edge", E, 1);
		if(c[u] == WHITE)
		{
		//	printf("%d\n",u);
			dfs_visit(u);
		}
	}
}

void test()
{
	int n,nr;
	n=5;
	nr=7;
	generate(n,nr);
	print(n);
	initialize(n);
	dfs(n);
}


void main()
{
	//test();
	int i;
	/*V = 100;
	VERTEX = false;
	for(E=1000; E<=5000; E=E+100){
		generate(V, E);
		initialize(V);
		dfs(V);
		printf("Done for %d\n", E);
		
	}*/
	E = 9000;
	VERTEX = true;
	for(V=110; V<=200; V=V+10){
		generate(V, E);
		initialize(V);
		dfs(V);
		printf("Done for %d\n", V);
	}
	profiler.showReport();
getch();
}