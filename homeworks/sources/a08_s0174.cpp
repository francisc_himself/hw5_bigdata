#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

/****
*   @Autor ***ANONIM*** ***ANONIM*** Grupa ***GROUP_NUMBER***
*   @Comments: Calculand numarul de operatii efectuate de algoritmul meu
*   am observat ca el este liniar si constant in raport cu numarul de Arce
*   pentru 10000 arce nr de operatii este 10000, la 50000 => 50000 de operatii
*   @Comment: Timpul de rulare al programului este ~0.56 secunde.
****/

#define nmax 60000

void makeSet(int x);

typedef struct node {
	int value, height;
	struct node *left, *right;
} node;

node *firstSet;
node *secondSet;
struct node * mine[60000];
FILE * myFile;

int completeazaTabel(int n){
	int i, sum = 0;
	for(i = 0; i < n; i++){
        free(mine[i]);
	}
	for(i = 0; i < n; i++){
        makeSet(i);
        sum++;
	}
	return sum;
}

void makeSet(int x){
    mine[x] = (node *)malloc(sizeof(node));
    mine[x]->left = mine[x]->right = NULL;
    mine[x]->value = x;
    mine[x]->height = 0;
}

void insertTreeinTree(node ** root, node ** rootInserted, int height){
    if(!(*root)) {
        *root = *rootInserted;
        return;
    }
    else if((*root)->left == NULL)
            insertTreeinTree(&(*root)->left, rootInserted, height);
         else
            insertTreeinTree(&(*root)->right, rootInserted, height);

}

node* findShortestBranch(node * root){
    if((root->left == NULL) || (root->right == NULL))
        return root;
    else {
        if((findShortestBranch(root->left) < findShortestBranch(root->right)))
            return findShortestBranch(root->left);
        else
            return findShortestBranch(root->right);
    }
    return (node*)-1;
}

int findShortestBranchHeight(node * root){
    if((root->left == NULL) || (root->right == NULL))
        return root->height;
    else {
        if((findShortestBranchHeight(root->left) < findShortestBranchHeight(root->right)))
            return findShortestBranchHeight(root->left);
        else
            return findShortestBranchHeight(root->right);
    }
    return -1;
}

void printSet(node * tree){
   if(tree->left) printSet(tree->left);
        printf("%d\n",tree->value);
   if(tree->right) printSet(tree->right);
}

void prettyPrint(node *root, int nivel){
    int i;
    if(root->left){
        prettyPrint(root->left, nivel + 1);
    }
    printf("\n");
    for(i = 0; i <= 5 * nivel; i++)
	{
        printf(" ");
    }
    printf("%d",  root->value);
    if(root->right)
        prettyPrint(root->right, nivel + 1);
}

void refreshRefferences(node ** root, int x){
    if(!(*root)){
        mine[(*root)->value] = mine[x];
        return;
    }
    else{
        mine[(*root)->value] = mine[x];
        if((*root)->left != NULL)
        refreshRefferences(&(*root)->left, x);
        if((*root)->right != NULL)
        refreshRefferences(&(*root)->right, x);
    }
}

void edgeUnion(int x, int y){
    node * aux1, *aux2;
    int aux1Height, aux2Height;
	firstSet = mine[x];
	secondSet = mine[y];
	aux1 = findShortestBranch(firstSet);
	aux2 = findShortestBranch(secondSet);
	aux1Height = findShortestBranchHeight(firstSet);
	aux2Height = findShortestBranchHeight(secondSet);
	if(aux1Height <= aux2Height){
        insertTreeinTree(&aux1, &secondSet, aux1Height);
        refreshRefferences(&firstSet, x);
	}
	else{
        insertTreeinTree(&aux2, &firstSet, aux2Height);
        refreshRefferences(&secondSet, y);
	}
	return;
}

int generateRandom(int i){
    srand((int)time(NULL));
    return rand() % i;;
}

void componenteConexe(){
    int i, j, x, y, operatii;

    for(i = 10000; i <= 60000; i += 500){
        operatii = 0;
        operatii += completeazaTabel(i);
        for(j = 0; j < 10000; j++){
            x = generateRandom(i);
            y = generateRandom(i);
            if(mine[x] != mine[y]){
                edgeUnion(x, y);
                operatii++;
            }
        }
        fprintf(myFile, "%d,", operatii);
        printf("%d\n", i);
    }
}

int main(){
    myFile = fopen("result.csv","w");
    componenteConexe();
    fclose(myFile);
	return 0;
}
