/*Assignment 3
***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***

Heap sort: complexitate n*log(n)
QUick sort: complexitate n^2

Si din grafice rezulta faptul ca heap sortul este o metoda mai eficienta decat Quick sortul

*/

#include <stdio.h>
#include <conio.h>
#include <iostream>

#include "Profiler.h"
Profiler profiler ("Sortari: heap sort & quick sort");
using namespace std;
#define NEG_INF INT_MIN

int dimnesiune,lungime ;


int parinte(int i)
{ return i/2; }
int stanga(int i)
{ return 2*i;}
int dreapta(int i)
{ return 2*i+1;}


// ************************** Quick sort *******************************


int partitie(int *B, int p,int r)
{
int x,i,j,aux;
profiler.countOperation("Quick sort",lungime,1);
x=B[r]; i=p-1; // initializari
for(j=p;j<=r-1;j++)
{
   profiler.countOperation("Quick sort",lungime,1); // o comparatie
    if(B[j]<=x)
	{
		 i++;
		 profiler.countOperation("Quick sort",lungime,3); //3 asignari
		 // interchimbam
		 aux=B[i];
		 B[i]=B[j];
		B[j]=aux;
     }
}
profiler.countOperation("Quick Sort",lungime,3); // 3 asignari
// interchimbam
aux=B[i+1];
B[i+1]=B[r];
B[r]=aux;
return i+1;
}

void QSORT(int *B,int p,int r)
{ 
	int q;
	if(p<r) {
	        q=partitie(B,p,r);
			QSORT(B,p,q-1);
			QSORT(B,q+1,r);
	        }
}


//  ****************** Heap sort **************************

void MAX_HEAPIFY(int *A, int i)
{
	int l, r, aux, maiMare;
	// initializam variabilele r (right), respectiv l (left) cu functiile dreapta(), respectiv stanga()
	l = stanga(i);
	r = dreapta(i);
	profiler.countOperation("Heap sort",lungime); //o comparare
	if(l <= dimnesiune && A[l] > A[i])
		maiMare = l;
	else 
		maiMare = i;
	profiler.countOperation("Heap sort",lungime);// o comparare
	if(r <= dimnesiune && A[r] > A[maiMare])
		maiMare = r;
	if(maiMare != i)
	{
		profiler.countOperation("Heap sort",lungime,3); // 3 asignari
		// interschimbam
		aux = A[i];
		A[i] = A[maiMare];
		A[maiMare] = aux;
		MAX_HEAPIFY(A, maiMare);
	}

}
void BUILD_MAX_HEAP_BU(int *A)
{
	dimnesiune = lungime;
	for(int i = lungime/2; i >= 1; i--)
		MAX_HEAPIFY(A,i);
}
void HEAPSORT(int *A)
{
	int aux;
	BUILD_MAX_HEAP_BU(A);
	for(int i = lungime; i >= 2; i--)
	{
		profiler.countOperation("Heap sort",lungime,3); //3 asignari
		// interchimbam
		aux = A[1];
		A[1] = A[i];
		A[i] = aux;
		dimnesiune --;
		MAX_HEAPIFY(A, 1);
	}
}

// afisari

void afis_simpla(int *A, int n) // asisarea unui vector (ca un sir de elemente)
{
int i;
for(i=1;i<=n;i++)
	cout<<A[i]<<"  ";
}

void afisare(int *A,int n,int k,int nivel) // functia de afisare pretty-print
{
	if(k>n) return;
	afisare(A,n,2*k+1,nivel+1);
		for(int i=1;i<=nivel;i++)
			{ cout<<"   ";}
		cout<<endl<<A[k];
	afisare(A,n,2*k,nivel+1);
}

// cazul favorabil, mediu si defavorabil 

void QSORT_FAVORABIL (int *A, int p, int r)
{
    int i=p, j=r,aux;
    int x=A[(p+r)/2];
	profiler.countOperation("Quick sort - caz favorabil",lungime,1); //o comparare
    do
    {  
        while (A[i]<x )
			{
				i++;
				profiler.countOperation("Quick sort - caz favorabil",lungime,1); // o asignare
	    	}
        while (A[j]>x)
			{
				j--;
				profiler.countOperation("Quick sort - caz favorabil",lungime,1); // o asignare
		    }
        if (i<=j)
        {//interchimbare
            aux=A[i];
			A[i]=A[j];
			A[j]=aux;
            profiler.countOperation("Quick sort - caz favorabil",lungime,3); //3 asignari
			i++; j--;
        }
    } while (i<=j);
    if (p<j) QSORT_FAVORABIL(A, p, j);
    if (i<r) QSORT_FAVORABIL(A, i, r);
}


void mediu()
{
	int A[10001],B[10001];
	for(int i=1; i <= 5; i++) // 5 cazuri
		for(lungime = 100; lungime <= 10000; lungime += 100)
		{
			FillRandomArray(A,lungime); //vectori neordonat
			memcpy(B,A,lungime*sizeof(int));
			HEAPSORT(A);
			QSORT(B,1,lungime); 
		}
		profiler.createGroup("Caz mediu","Heap sort","Quick sort");
	    profiler.showReport();
}

void worst()
{
  int A[10001];
  for(lungime = 100; lungime <= 10000; lungime += 100)
	  {
	    FillRandomArray(A,lungime,10,50000,false,2);
		QSORT(A,1,lungime);
      }
  profiler.createGroup("Caz defavorabil","Quick sort");
  profiler.showReport();
}


void caz_favorabil()
{
int A[10001];
for(lungime = 100; lungime <= 1000; lungime += 100)
	  {
	    FillRandomArray(A,lungime,10,50000,false,2);
		QSORT(A,1,lungime);
      }
  profiler.createGroup("CAZ_FAVORABIL","QSORT_favorabil");
  profiler.showReport();
}

void main()
{
int A[10001],B[10001];
/*cout<<"dati nr de elemnte=";
cin>>lungime;
for(int i=1;i<=lungime;i++)
{
	cout<<"dati elemntul "<<i<<" =" ;
	cin>>A[i];
	B[i]=A[i];
}
//folosim heap sortul
HEAPSORT(A);
afisare(A,lungime,1,0); //afisam pretty print
cout<<"sortare quicsort: "<<endl;
QSORT(B,1,lungime);
afis_simpla(B,lungime); //afisam sirul
//cazuri: fav, mediu & worst (defavorabil)
mediu();
worst();*/
caz_favorabil();
}