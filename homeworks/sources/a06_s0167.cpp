

/**
   ***ANONIM*** ***ANONIM*** Daniela grupa ***GROUP_NUMBER***

Daca valoarea lui m este constanta eficienra algoritmului este O(n), iar daca m nu este constant eficienta este
O(n lg n).In cazul de fata fata l-am luat pe n sa varieze intre 100 si 10000 iar valoarea lui m fiind n/2.
Graficul pentru suma dintre comparatii si atribuiri ale algoritmului contine o curba logaritmica.
**/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <conio.h>

typedef struct tip_nod{
		int key;
		int dim;
		struct tip_nod *p,*st,*dr;
	}NOD;
	
NOD *sant;
int nrOperatii;

void init_sant(void){
	//initializare santinela
	sant=(NOD*)malloc(sizeof(NOD));
	sant->key=0;
	sant->dim=0;
	sant->st=NULL;
	sant->dr=NULL;
	sant->p=NULL;
}

NOD *ec_arb(int a,int b){
	if(a>b) return sant;
	int m=(a+b)/2;//valoarea de mijloc
	NOD *x=(NOD*)malloc(sizeof(NOD));//aloca nod nou
	x->key=m;
	x->p=sant;//deocamdata initializam parintele cu santinela(va ramane asa doar la radacina)
	x->st=ec_arb(a,m-1);//fiul stang
	x->st->p=x;
	x->dr=ec_arb(m+1,b);//fiul drept
	x->dr->p=x;
	x->dim=x->st->dim+x->dr->dim+1;//calculeaza dimensiunea
	nrOperatii++;
	return x;

}

NOD *so_selectie(NOD *x,int i){
	if(x==sant) return NULL; 
	int r=x->st->dim+1;//numarul de noduri mai mici decat x->key
	if(i==r){//suntem chiar pe nodul nr. i
		return x;
	}else if(i<r){//suntem pe un nr. mai mare ca i
		return so_selectie(x->st,i);
	}else{//suntem pe un numar mai mic ca i
		return so_selectie(x->dr,i-r);
	}
}

NOD *minim(NOD *x){
	while(x->st!=sant)

		x=x->st;
	nrOperatii++;
	nrOperatii++;
	return x;
}

NOD *succesor(NOD *x){
	nrOperatii++;
	if(x->dr!=sant)
		return minim(x->dr);
	NOD *y=x->p;
	while(y!=sant && x==y->dr){
		x=y;
		y=y->p;
		nrOperatii++;
		nrOperatii++;
	}
	return y;
}

void stergere(NOD **rad,NOD *z){
	NOD *y,*x;
	//vedem ce nod urmeaza a fi sters
	if(z->st==sant || z->dr==sant){
		nrOperatii++;
		y=z;
	}else{
		y=succesor(z);
		nrOperatii++;
	}
	//toti de deasupra nodului ce se va sterge, vor pierde 1 la dimensiune
	x=y;
	while(x!=sant && x!=NULL){
		--(x->dim);
		x=x->p;
		nrOperatii++;
	}
	x=NULL;
	//nodul ce se va sterge are cel mult un fiu
	if(y->st!=NULL && y->st!=sant){
		x=y->st;
		nrOperatii++;
	}else{
		x=y->dr;
		nrOperatii++;
	}
	//daca fiul exista, sa il legam direct de parintele lui y
	if(x!=NULL && x!=sant){
		x->p=y->p;
	}
	//daca nodul ce se sterge era radacina, sa o fixam la x
	if(y->p==NULL || y->p==sant){
		x->p=sant;
		*rad=x;
		nrOperatii++;
		nrOperatii++;
	}else if(y==y->p->st){
		y->p->st=x;
		nrOperatii++;
	}else{
		y->p->dr=x;
		nrOperatii++;
	}
	if(y!=z){
		z->key=y->key;
		nrOperatii++;
	}
	free(y);
}

void inord(NOD *x){
	if(x==sant) return;
	inord(x->st);
	printf("%d ",x->key);
	inord(x->dr);
}

void pprint(NOD *rad,int align){
	//rad- nodul curent
	//align - cu cate spatii este aliniat la dreapta
	if(rad==sant) return;
	char s[20];
	int i;
	sprintf(s,"%d(%d) ",rad->key,rad->dim-1);//il scriem intai intr-un string, ca sa-i putem determina lungimea
	align+=strlen(s);//incrementam align-ul, cu lungimea lui
	printf("%s",s);//il scriem efectiv
	if(rad->st!=sant)
		pprint(rad->st,align);//fiul stang il scriem imediat in continuare
	if(rad->dr!=sant){
		//pentru fiul drept, trecem la linia urmatoare
		printf("\n");
		for(i=0;i<align;++i)
			printf(" ");//punem spatiile corespunzatoare aliniamentului
		pprint(rad->dr,align);//il scriem
	}
}

void permutare_j(int n,int m){
	NOD *x=ec_arb(1,n),*y;//construim un arbore de cautare imbunatatit
	//inord(x);printf("\n");
	pprint(x,0);printf("\n");
	int nr=n,s=m;
	//nr - numarul de elemente ramase
	//s - numarul elementului ce trebuie selectat, din cele ramase
	while(x!=NULL && x!=sant){
		//cat timp mai exista noduri
		s=s%nr;
		if(s==0) s=nr;//daca cumva avem mai putine noduri decat numarul selectiei
		y=so_selectie(x,s);//selectam
		printf("Joseph de: %d\n",y->key);//afisam
		stergere(&x,y);//stergem
		pprint(x,0);printf("\n");
		s+=m-1;//adunam m-1, in loc de m, pentru ca un nod a fost deja sters
		--nr;
	}
	//printf("\n");
}


int main(){
	init_sant();
	permutare_j(7,3);

	//FILE *f;
//f=fopen("fisier.csv","w");
//fprintf(f,"Rez");
//	fprintf(f,",nrOperatii\n");
	//for(int i=100;i<10000;i+=100){
	//nrOperatii=0;
	//permutare_j(i,i/2);
	//fprintf(f,"%d, %d\n",i,nrOperatii);
	//}

	//fclose(f);
	return 0;
	getch();
}
