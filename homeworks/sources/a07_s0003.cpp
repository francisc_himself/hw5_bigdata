#include <iostream>

using namespace std;

typedef struct Multi_Node
{
	int key;
	int count;
	struct Multi_Node *child[50];
} Multi_Node;

typedef struct Nod
{
    int key;
    Nod *st, *dr;
} Nod;

 Nod *rad;
void insertMN ( Multi_Node *parent , Multi_Node *child)
{
	parent->child[parent->count++]=child;
}

Multi_Node *createMN ( int key )
{
	Multi_Node *x = new Multi_Node;
	x->key=key;
	x->count=0;
	return x;
}

Multi_Node *transform1 ( int t[], int size)
{

	Multi_Node *root = NULL;
	Multi_Node *nodes[50];
	//create nodes
	for( int i=0; i<size; i++)
		nodes[i]=createMN(i);
	//legaturi
	for( int i=0; i<size ; i++)
		if( t[i] != -1)
			insertMN ( nodes[t[i]], nodes[i]);
		else
			root = nodes[i];
	return root;
}

void adaug ( Nod *nod, int key)
{
    nod->dr=new Nod;
    nod->dr->key=key;
}


void pretty2 ( Nod *nod , int nivel)
{
    if(nod!=NULL)
    {
        cout<<"intra in pretty"<<endl;
        for(int i=0; i<nivel; i++)
            cout<<"  ";
        cout<<nod->key<<endl;
		pretty2 ( nod->st, nivel+1);
		pretty2 (nod->dr, nivel+1);
    }
}

void transform2 ( Multi_Node *nod, Nod *n)
{
//    cout<<"intra"<<endl;
    int i;
    n = new Nod ;
    n->st= new Nod;
    n->dr = new Nod;
    n->key = nod->key;
    cout<<"imi pun in nod: "<<nod->key<<endl;
    pretty2(rad,0);
    if(nod->count==1)
    {
        cout<<"intra in if pt "<<nod->key<<" care are copil nenul: "<<nod->child[0]->key<<endl;
        n->st->key=nod->child[0]->key;
        n->st->dr=new Nod;
        n->st->dr=NULL;
        cout<<"am adaugat la stanga nodului "<<n->key<<" "<<n->st->key<<endl;
        transform2 ( nod->child[0], n->st);
    }
    else
        if(nod->count==2)
        {
           cout<<"intra in if2 pt "<<nod->key<<" care are copil nenul: "<<nod->child[0]->key<<" si "<<nod->child[1]->key<<endl;
           n->st->key=nod->child[0]->key;
           cout<<"am adaugat la stanga nodului "<<n->key<<" "<<n->st->key<<endl;
           n->st->dr=new Nod;
           n->st->dr->key=nod->child[1]->key;
           cout<<"am adaugat la dreapta nodului "<<n->st->key<<" "<<n->st->dr->key<<endl;
           cout<<"urmeaza recursivitate"<<endl;
           transform2(nod->child[0],n->st);
           transform2(nod->child[1],n->st->dr);
        }
   
        else
        {
            cout<<"nodul "<<nod->key<<" nu intra in if"<<endl;
            n->st=NULL;
            n->dr=NULL;
        }
   
}

void pretty ( Multi_Node *nod, int nivel=0)
{
	for(int i=0; i<nivel; i++)
		cout<<"  ";
	cout<<nod->key<<endl;
	for( int i=0; i<nod->count; i++)
		pretty ( nod->child[i], nivel+1);
}

int main()
{
	int t[]={4,2,3,-1,3,2};
	int size = sizeof(t)/ sizeof(t[0]);
	Multi_Node *root = transform1 ( t, size);

    pretty ( root);

    rad= new Nod;
	transform2(root,rad);

    cout<<rad->key;
	return 0;
}
