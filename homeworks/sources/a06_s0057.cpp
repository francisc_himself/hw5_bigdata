/* ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***
	In acest program am extins problema lui Josephus,care ne spune sa scoatem anumite numere, dintr-un sir de alte numere, pe baza unei numaratori simple.
	Acest algoritm la o prima vedere pare simplu,dar eficienta lui creste destul de mult odata cu aleatorizarea numarului pe baza caruia facem numaratoarea.
Adica daca am lua acest numar ca fiind un numar constant,algoritmul lui Josephus ar avea o eficienta de O(n),iar daca am lua acest numar variabil atunci 
eficienta lui va creste pana la O(nlogn),lucru care il putem vedea si din diagrama rezultata.Eficienta finala este obtinuta din eficientele functiilor folosite,
adica majoritatea functiilor sunt functii de lucru cu arbori,iar intr-un arbore de o inaltime h,toate operatiile facute au o efiecienta de O(h).
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Profiler.h"
Profiler profiler("demo");
typedef struct nod{
	int ch;
	int dimens;
	struct nod *stg,*dr,*p;
}nod;
nod *T;
int k;
nod* Build_tree(int stg,int dr)
{
	
	int ds,dd;
	nod*x=(nod*)malloc(sizeof(nod));
	nod*y;//=(nod*)malloc(sizeof(nod));
	nod*z;//=(nod*)malloc(sizeof(nod));
	if(stg<=dr)
	{
		int m=(stg+dr)/2;

		x->ch=m;

		x->p=0;
		x->stg=Build_tree(stg,m-1);
		profiler.countOperation("Assign+comp",k,5);

		if(x->stg!=NULL){
				x->stg->p=x;
				profiler.countOperation("Assign+comp",k,2);
		}
		y=x->stg;
		if(y!=NULL)
			ds=y->dimens;
		else ds=0;

		
		x->dr=Build_tree(m+1,dr);
		if(x->dr!=NULL){
				x->dr->p=x;
				profiler.countOperation("Assign+comp",k,2);
		}
		z=x->dr;
		if(z!=NULL)
			dd=z->dimens;
		else dd=0;
		x->dimens=ds+dd+1;

		return x;
	}
	else
	{	
		return NULL;
	}

}

nod* OS_Selectie(nod*x,int i)
{
	int r;
	profiler.countOperation("Assign+comp",k,1);
	if(x->stg!=NULL)
	{
		r=x->stg->dimens+1;
			
	}
	else r=1;
	if(i==r)
		 return x;
	else if(i<r){
			if(x->stg!=NULL)
				 return OS_Selectie(x->stg,i);
	}
	else
		{
			if(x->dr!=NULL)
				return OS_Selectie(x->dr,i-r);
	}
}

nod* Tree_min(nod*x)
{
	while(x->stg!=NULL){
		x->dimens--;
		x=x->stg;
		
		profiler.countOperation("Assign+comp",k,2);
	}
	return x;
}
void Transplant(nod*u,nod*v)
{
	//nod *w;
	profiler.countOperation("Assign+comp",k,1);
	if(u->p==NULL){
		profiler.countOperation("Assign+comp",k,1);
		T=v;
	}
	else if(u==u->p->stg){
				u->p->stg=v;
				profiler.countOperation("Assign+comp",k,1);
			}
	else {
				//u->p->dr->dimens=v->dimens+1;
				u->p->dr=v;
				profiler.countOperation("Assign+comp",k,1);
				}
	profiler.countOperation("Assign+comp",k,2);
	if(v!=NULL){
		v->p=u->p;
		v->dimens=u->dimens-1;
		profiler.countOperation("Assign+comp",k,1);
	}


}
void OS_Delete(nod*z)
{
	//decrementare(z);
	nod*y,*x;
	x=z;
	//profiler.countOperation("Assign+comp",k,1);
	while(z->p!=NULL)
	{
		profiler.countOperation("Assign+comp",k,3);
		z=z->p;
		z->dimens--;
	}
	z=x;
	profiler.countOperation("Assign+comp",k,2);
	if(z->stg==NULL){
		Transplant(z,z->dr);
		
	}
	else			
			if(z->dr==NULL){
			Transplant(z,z->stg);
		
			}
	else
		{
			profiler.countOperation("Assign+comp",k,2);
			y=Tree_min(z->dr);
		
			if(y->p!=z)
			{
				Transplant(y,y->dr);
				profiler.countOperation("Assign+comp",k,2);
				y->dr=z->dr;
				y->dr->p=y;
			}
			Transplant(z,y);
			profiler.countOperation("Assign+comp",k,2);
			y->stg=z->stg;
			y->stg->p=y;
		}
	free(z);



}


void Inordine(nod*x,int n)
{
	if(x!=0){
		Inordine(x->dr,n+1);
		
		for(int i=0;i<=n;i++){
			printf("     ");
		}
		
		printf("%d[%d]\n",x->ch,x->dimens);
		Inordine(x->stg,n+1);
		
	}

	
}
void Josephus(int n,int m)
{
	nod*x=(nod*)malloc(sizeof(nod));
	profiler.countOperation("Assign+comp",k,1);
	x=Build_tree(1,n);
	x->p=NULL;
	T=x;
	//Inordine(T,0);
	//printf("\n");
	int i=m;
	while(T!=NULL&&n>0)
	{
		profiler.countOperation("Assign+comp",k,2);
		x=OS_Selectie(T,i);
		OS_Delete(x);
		//Inordine(T,0);
		//printf("\n");
		//printf("\n");
		if(n==941)
			printf("DA\N");

		n--;
		if(n!=0)
			i=(i+m-1)%n;
		else
			printf("s-a terminat\n");
		if(i==0) i=n;
	}
}
void main()
{
	
	T=(nod*)malloc(sizeof(nod));
	T=Build_tree(0,10);
	Inordine(T,0);
	nod*z=(nod*)malloc(sizeof(nod));
	nod* y=(nod*)malloc(sizeof(nod));	
	y=T->stg;
	OS_Delete(y);
	printf("\n");
	Inordine(T,0);
	Josephus(100,50);


	int i;
	for(i=100;i<10000;i+=100)
	{
		k=i;
		printf("n=%d\n",i);
		Josephus(i,i/2);
	}
	profiler.showReport();
	//Inordine(T,0);
}