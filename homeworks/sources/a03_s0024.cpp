#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include "Profiler.h"
#define NMAX 20000

int sir1[NMAX], sir2[NMAX], sir3[NMAX], dim_heap;
int HScmp, QScmp, HSatr, QSatr;

int Partitionare(int a[NMAX],int st,int dr)
{
	int x,i,j,aux;
	x=a[st];
	i=st-1;
	j=dr+1;
	while(true)
	{
		do{
			QScmp++;
			j=j-1;
		}while(a[j]>x);

		do{
			QScmp++;
			i=i+1;
		}while(a[i]<x);
		
		if(i<j)
			{
				aux=a[i];
				a[i]=a[j];
				a[j]=aux;
				QSatr+=3;
		}
		else return j;
	}
}


void quickSort(int a[NMAX],int st, int dr)
{
	int m;
	if(st<dr)
	{
		m=Partitionare(a,st,dr);
		quickSort(a,st,m);
		quickSort(a,m+1,dr);
	}
}


void ReBuild(int a[NMAX],int i)
{
    int ind,aux,l,r;
	l= 2*i;
	r= 2*i+1;
	HScmp++;
	if(l<= dim_heap && a[l]>a[i])
		ind = l;
	else
		ind = i;
	HScmp++;
	if(r<=dim_heap && a[r]>a[ind])
		ind = r;
	if(ind!=i)
	{
		aux=a[i];
		a[i]=a[ind];
		a[ind]=aux;
		HSatr+=3;
		ReBuild(a,ind);
	}
}

void bottom_up(int a[NMAX], int n)
{
	int i;
	dim_heap = n;
    for(i=n/2;i>=1;i--)
	{
		ReBuild(a,i);
		
	}
	
}

void heapSort(int a[NMAX], int n)
{
	int aux,i;
	bottom_up(a,n);
	for(i=n;i>=2;i--)
	{
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		HSatr+=3;
		dim_heap--;
		ReBuild(a,1);
	}

}

void main()
{
	int i, HScmps, HSatrs, QSatrs, QScmps,k;
	int a[7]={10,23,1,43,5,7,34};
	int	b[8]={0,10,23,1,43,5,7,34};
	//for(i=0;i<7;i++)
		//scanf("%d",&a[i]);
	
	heapSort(b,7);
	quickSort(a,0,6);
	printf("quickSort:\n");
	for(i=0;i<7;i++)
		printf(" %d ",a[i]);
	printf("\nheapSort:\n");
	for(i=1;i<8;i++)
	printf(" %d ",b[i]);
	//cazul mediu
	/*FILE *pf;
	pf = fopen("file1.csv","w");
	if(pf==NULL)
	{
		perror("Error opening file!");
		exit(1);
	}
	
	fprintf(pf,"n, HSatr, HScmp, HSsum, QSatr, QScmp, QSsum\n");
	
	for(i=100;i<=10000; i=i+100)
	{
		HScmps=0; HSatrs=0; QSatrs=0; QScmps=0;
		
		for(k=1;k<=5;k++){
			QScmp=0; QSatr=0; HScmp=0; HSatr=0;
			FillRandomArray(sir1,i,1,20000,true,0);

			for(int j=1;j<=i;j++)
			{
				sir2[j]=sir1[j];
				sir3[j]=sir1[j];
			}
			
			heapSort(sir2,i);

			HSatrs = HSatrs + HSatr;
			HScmps = HScmps +HScmp;

			quickSort(sir3,1,i);
			QSatrs = QSatrs + QSatr;
			QScmps = QScmps +QScmp;
		}
		fprintf(pf,"%d, %d, %d, %d, %d, %d, %d\n",i, HSatrs/5, HScmps/5,(HSatrs+HScmps)/5, QSatrs/5,QScmps/5,(QSatrs+QScmps)/5);
	}
	fclose(pf);*/


	//------------------------------------------
	
	//Cazul favorabil
	/*FILE *pf1;
	pf1 = fopen("file2.csv","w");
	
	if(pf1==NULL)
	{
		perror("Error opening file!");
		exit(1);
	}
	fprintf(pf1,"n, HSatr, HScmp, HSsum, QSatr, QScmp, QSsum\n");
	QScmp=0; QSatr=0; HScmp=0; HSatr=0;

	for(i=100;i<=10000; i=i+100)
	{
			FillRandomArray(sir1,i,1,20000,true,2);

			for(int j=1;j<=i;j++)
			{
				sir2[j]=sir1[j];
				sir3[j]=sir1[j];
			}
			
			heapSort(sir2,i);
			

			quickSort(sir3,1,i);
			
			fprintf(pf1,"%d, %d, %d, %d, %d, %d, %d\n",i, HSatr, HScmp,(HSatr+HScmp), QSatr,QScmp,(QSatr+QScmp));
	}
	fclose(pf1);*/
	
	//Cazul defavorabil
	/*FILE *pf2;
	pf2 = fopen("file3.csv","w");
	
	if(pf2==NULL)
	{
		perror("Error opening file!");
		exit(1);
	}
	fprintf(pf2,"n, HSatr, HScmp, HSsum, QSatr, QScmp, QSsum\n");
	QScmp=0; QSatr=0; HScmp=0; HSatr=0;
	for(i=100;i<=10000; i=i+100)
	{
			FillRandomArray(sir1,i,1,20000,true,1);

			for(int j=1;j<=i;j++)
			{
				sir2[j]=sir1[j];
				sir3[j]=sir1[j];
			}
			
			heapSort(sir2,i);

			quickSort(sir3,1,i);
			
			fprintf(pf2,"%d, %d, %d, %d, %d, %d, %d\n",i, HSatr, HScmp,(HSatr+HScmp), QSatr,QScmp,(QSatr+QScmp));
	}
	fclose(pf2);*/

	printf("done`");
	getch();
}

/*
Pe baza graficului se poate observa ca algoritmul quickSort este mai eficient decat heapSort
*/