/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
   Assignment No.8:  Disjoint Sets
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

/** This program generates randomly an array of edges representing a graph. Then, using the functions make_set, find_set, link,
my_union found in Cormen (cap 21.3) we make an algorithm for finding the connected components of a graph (21.1 from Cormen).

Make_set function allocates memory for the structure defined as a node and in find_set it makes
the graph as a structure with links to the parent node. If the parents of two "sets"
have an edge between them, the union function links the two sets, forming a connected component.
Display is done for each node by printing its parent (first node) and all the keys corresponding
to the set.

The running time of this algorithm depends on the number of vertices and edges.
For 10000 vertices and 10000 edges generated randomly, the algorith does 151763 (a logaritmic value depending on nrv and nrm). 
*/

typedef struct edge
{
	int x, y; 
}
EDGE;

EDGE edges[60001];   //the array of edges that will be generated and used

typedef struct node
{
	int key, rank;
	struct node* parent;
} NODE;

NODE *V[10001];   //used for finding the connected components

int op;

void init()
{
	int i;
	for(i=1;i<=60001;i++)
		edges[i]=*(EDGE*)malloc(sizeof(EDGE));
}

bool verif(int x, int y, int nrm)  //used when generating the edges (we don't want to have edges between the same nodes)
{
	int i;
	for(i=1; i<nrm; i++)
	{
		if(((edges[i].x==x) && (edges[i].y==y)) || ((edges[i].y==x) && (edges[i].x==y)))
		{
			return false;
		}
	}
	return true;
}

void generate(int nrv, int nrm)  //generating nrm edges between nrv vertices
{
	int i,x,y;
	int nre=1;   //the current number of generated edges
	while(nre<=nrm)
		for(i = 1; i<=nrv; i++)
		{
			x = (rand()%(nrv))+1;
			y = (rand()%(nrv))+1;
			if((x != y) && (verif(x,y,nrm)))
			{
				edges[nre].x = x;
				edges[nre].y = y;
				nre++;
			}
		}
}


NODE *make_set(int x)  //init the sets
{
	NODE *p;
	p=(NODE*)malloc(sizeof(NODE));
	p->key=x;
	p->parent=p;
	p->rank=0;
	op+=3;
	return p;
}

NODE *find_set(NODE *p)
{
	op++;
	if(p!=p->parent)
	{
		p->parent=find_set(p->parent);
		op++;
		//printf("am pus parintele lui %d pe %d in find set\n",p->key,p->parent->key);
	}
	return p->parent;
}

void link(NODE *x, NODE *y)  //links two connected components 
{
	op++;
	if(x->rank > y->rank)
	{
		y->parent=x;
		op++;
		//printf("am legat %d de %d\n",y->key,x->key);
	}
	else
	{
		x->parent=y;
		op++;
		//printf("am legat %d de %d\n",x->key,y->key);
		op++;
		if(x->rank == y->rank)
		{
			op++;
			y->rank=y->rank+1;
		}
	}
	
}

void my_union(NODE *x, NODE *y)
{
	link(find_set(x),find_set(y));
}

//////////////////////////////////////////////////////

void connected_components(int nrv, int nrm)
{
	int i;
	for(i=1; i<=nrv; i++)
	{
		op++;
		V[i]=make_set(i);
		//printf("key: %d, parent: %d, rank: %d\n",V[i]->key,V[i]->parent->key,V[i]->rank);
	}
	for(i=1;i<=nrm;i++)
	{
		op++;
		if(find_set(V[edges[i].x])!=find_set(V[edges[i].y]))
		{
			my_union(V[edges[i].x], V[edges[i].y]);
			//printf("key: %d, parent: %d, rank: %d\n",V[edge[i].x]->key,V[edge[i].x]->parent->key,V[edge[i].x]->rank);
			//printf("key: %d, parent: %d, rank: %d\n",V[edge[i].y]->key,V[edge[i].y]->parent->key,V[edge[i].y]->rank);
		}
	}
}

void print_edges (int nrm)
{
	int i;
	for(i=1;i<=nrm; i++)
	{
		printf("[%d, %d]\n", edges[i].x, edges[i].y);
	}
}

void print(int nrm, int nrv)
{
	int i,j;
	for(i=1;i<=nrv;i++)
		printf("the node %d has the parent %d\n",V[i]->key,V[i]->parent->key,V[i]->rank);

	for(i=1;i<=nrv;i++)
	{
		printf("Connected Component number %d: ",i);
		for(j=1;j<=nrv;j++)
		{
			if(find_set(V[j])->key==i)
				printf("%d ",V[j]->key);
			
		}
		printf("\n");
	}
}

void main()
{
	//test
	int nrv=6;
	int nrm=3;
	srand(time(NULL));
	generate (nrv, nrm);
	print_edges(nrm);
	connected_components(nrv,nrm);
	print(nrm,nrv);

	/*FILE *f;
	f=fopen("rez.txt","w");
	nrv=10000;
	fprintf(f,"muchii op\n");
	for(nrm=10000; nrm<=60000; nrm+=1000)
	{
		op=0;
		init();
		generate(nrv,nrm);
		connected_components(nrv,nrm);
		fprintf(f,"%d %d\n",nrm,op);
	}
	fclose(f);
	printf("The End!%c",7);*/

	getch();
}