/*
	Interclasarea a k liste ordonate crescator.
	O tema care mi-a pus destule probleme la anumite secvente de cod care in mod normal nu ar fi trebuit sa fie atat
	de problematice. Dupa depistarea unei erori de rulare si intelegerea enuntului programul a fost destul de usor
	de implementat. 
	Ca si o prima impresie dupa implementarae si rularea corecta a codului a fost data de viteza cu care a rulat programul
	Partea cea mai grea , in opiniea mea , si partea care mi-a dat cele mai mari batai de cap din cauza unei neatentii a fost
	functia de extragere a unui element din lista corespunzatoare si plasarae acestuia in vectorul folosit pentru construirea Heap

	Stunden : ***ANONIM*** ***ANONIM*** ***ANONIM***
*/


#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct nod{
	int ***ANONIM***;
	nod *urm;
}NOD;

typedef struct indici{
	int ***ANONIM***;
	int in;
}IN;

NOD *head[1000];
NOD *tail[1000];
int k,com,as,nrele;
IN el[10000];
NOD *finalhead;
NOD *finaltail;
void afisare(nod *h)
{
	NOD *p;
	p=h;
	while(p!=NULL)
	{
		printf("%d ",p->***ANONIM***);
		p=p->urm;
	}

}
void inserare(NOD **h,NOD **t,int ***ANONIM***)
{
	NOD *p;
	p=(NOD *)malloc(sizeof(NOD));
	p->***ANONIM***=***ANONIM***;
	if (*h==0)
	{
		as+=2;
		*h=p;
		*t=p;
	}
	else
	{
		as+=2;
		(*t)->urm=p;
		*t=p;
	}
	as++;
	p->urm=NULL;

}

void add(NOD **h,int i)
{
	NOD *p;
	as++;
	p=*h;
	com++;
	if(p!=NULL)
	{
		as+=3;
		nrele++;
		el[nrele].***ANONIM***=p->***ANONIM***;
		el[nrele].in=i;
	}
}
void sterge(NOD **h,NOD **t){
	NOD *p;
	p=*h;
	as++;
	com++;
	if(p==*t)
	{
		as++;
		*h=NULL;
	}
	else
	{
		as++;
		*h=p->urm;
		free(p);
	}
}

//construirea heapului Button Up
void Reconstructie(IN *v, int i) 
{ 
  int stg,dr,min,aux***ANONIM***,auxin;
  stg=2*i;
  dr=(2*i)+1;
  com++;
  
  if((stg<=nrele) && (v[stg].***ANONIM***<v[i].***ANONIM***)) 
  {
	  as++;
    min=stg;
  }
  else 
  {
	  as++;
    min=i;
  }
  com++;
  if((dr<=nrele) && (v[dr].***ANONIM***<v[min].***ANONIM***)) 
  {
	  as++;
    min=dr;
  }
  com++;
  if(min!=i)
  {
	as+=6;
	aux***ANONIM***=v[i].***ANONIM***;
	auxin=v[i].in;
	v[i].***ANONIM***=v[min].***ANONIM***;
	v[i].in=v[min].in;
	v[min].***ANONIM***=aux***ANONIM***;
	v[min].in=auxin;
    Reconstructie(v, min);
  }
}

void Constructie(IN *v)
{
	int i;
	for (i=nrele/2; i > 0;i--) 
	{
		Reconstructie(v, i); 
	}
}

void initializare()
{
	int i;
	for(i=1;i<=k;i++)
	{
		add(&head[i],i);
	}
}

void interclasare()
{
	int i,ok=1;
	nrele=0;
	initializare();
	while(nrele>0){
	Constructie(el);
	sterge(&head[el[1].in],&tail[el[1].in]);
	as++;
	i=el[1].in;
	inserare(&finalhead,&finaltail,el[1].***ANONIM***);
	as+=3;
	el[1].***ANONIM***=el[nrele].***ANONIM***;
	el[1].in=el[nrele].in;
	nrele--;
	com++;
	if(head[i]!=NULL) add(&head[i],i);
	}	
}
int main()
{
	int j,i,nr,n;
	FILE *f;
	f=fopen("interclasare.csv","w");
	//test verificarea algoritm
	printf("test pentru a arata corectitudinea algoritmului\npentru 4 liste cu 5 elemente:\n");
	k=4;
	for(j=1;j<k+1;j++)
	{
		nr=0;
		for(i=1;i<6;i++)
			{
				nr=nr+rand()%100;
				inserare(&head[j],&tail[j],nr);
			}
		afisare(head[j]);
		printf("\n");
	}

	printf("\n");
	printf("\n");
	interclasare();
	afisare(finalhead);
		printf("\n");

	//caz 1 k=5

	k=5;
	fprintf(f,"k=%d\nn,operatii\n",k);
	for(n=100;n<10001;n=n+100)
	{
		as=0;com=0;
		for(j=1;j<k+1;j++)
		{
			nr=0;
			for(i=1;i<n/k;i++)
			{
				nr=nr+rand()%100;
				inserare(&head[j],&tail[j],nr);
			}
		}
		interclasare();
		fprintf(f,"%d,%d\n",n,as+com);
	}

	fprintf(f,"\n");
	//caz 2 k=10

	k=10;
	fprintf(f,"k=%d\nn,operatii\n",k);
	for(n=100;n<10001;n=n+100)
	{
		as=0;com=0;
		for(j=1;j<k+1;j++)
		{
			nr=0;
			for(i=1;i<n/k;i++)
			{
				nr=nr+rand()%100;
				inserare(&head[j],&tail[j],nr);
			}
		}
		interclasare();
		fprintf(f,"%d,%d\n",n,as+com);
	}



	fprintf(f,"\n");
	//caz 3 k=100

	k=100;
	fprintf(f,"k=%d\nn,operatii\n",k);
	for(n=100;n<10001;n=n+100)
	{
		as=0;com=0;
		for(j=1;j<k+1;j++)
		{
			nr=0;
			for(i=1;i<n/k;i++)
			{
				nr=nr+rand()%100;
				inserare(&head[j],&tail[j],nr);
			}
		}
		interclasare();
		fprintf(f,"%d,%d\n",n,as+com);
	}



	//caz 4 n=10 000
	
	n=10000;
	fprintf(f,"\n");
	fprintf(f,"n=%d\n",n);
	fprintf(f,"k,operatii\n");
	for(k=100;k<501;k=k+10)
	{
		as=0;com=0;
		for(j=1;j<k+1;j++)
		{
			nr=0;
			for(i=1;i<n/k;i++)
			{
				nr=nr+rand()%100;
				inserare(&head[j],&tail[j],nr);
			}
		}
		interclasare();
		fprintf(f,"%d,%d\n",k,as+com);
	}
}