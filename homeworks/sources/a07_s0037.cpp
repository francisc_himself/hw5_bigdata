#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

//multi way node
typedef struct mwnod{
    int ch, nr;
    struct mwnod *kids[30];
} mwnod;

//binary nod
typedef struct bnod{
    int ch;
    struct bnod *fc, *rb;
} bnod;

//struct aux pt parinti
typedef struct parent{
	int nr,kids[30];
}parent;
//global

//rootmw = (mwnod*)malloc(sizeof(mwnod));


//BUILD multi-way tree
void build_mwtree(mwnod **rad, parent x[])
{
    int i;
   // printf("parinte: %d\n", (*rad)->ch);
    for(i=0; i < x[(*rad)->ch].nr; i++)
    {   (*rad)->kids[i] = (mwnod*)malloc(sizeof(mwnod));
        (*rad)->kids[i]->ch = x[(*rad)->ch].kids[i];
        (*rad)->kids[i]->nr = 0;
        (*rad)->nr++;
       // printf("fiu %d: %d\n", (*rad)->nr, (*rad)->kids[i]->ch);
        build_mwtree(&((*rad)->kids[i]), x);
    }
}

//print preordine
void print_multi(mwnod *rad, int n)
{
    int i;
    for(i = 0; i <= n; i++)
        printf("  ");
    printf("%d\n", rad->ch);
    for(i = 0; i < rad->nr; i++)
        print_multi(rad->kids[i], n+1);
}
//r2 to r3
void build_bintree(mwnod *mw,bnod **bin)
{
    bnod *aux;
    int i;
    if(mw->nr > 0)
    {
        (*bin)->fc = (bnod*)malloc(sizeof(bnod));
        (*bin)->fc->ch = mw->kids[0]->ch;
        (*bin)->fc->rb = NULL; (*bin)->fc->fc = NULL;
       
        aux = (*bin)->fc;
        for(i = 1; i < mw->nr; i++)
        {
            aux->rb = (bnod*)malloc(sizeof(bnod));
            aux->rb->ch = mw->kids[i]->ch;
            aux->rb->rb = NULL; aux->rb->fc = NULL;
            
            build_bintree(mw->kids[i-1],&aux);
            aux = aux->rb;
        }

    }
}
//print final
void print_bin(bnod *r, int n)
{   if(r!=NULL)
	{ 
		for(int i=0;i<n;i++)
			printf("  ");
		printf("%d\n", r->ch);

		 if(r->fc != NULL)  print_bin(r->fc, n+1);
		 if(r->rb != NULL)  print_bin(r->rb, n);
		
	}
}  





int main()
{	//~~~~~~~~~~~~~~~~ vectorul pt R1~~~~~~
	int n=9,k,i,v[10];
	v[1]=2 ; v[2]=7 ; v[3]=5 ; v[4]=2 ; v[5]=7 ; v[6]=7 ; v[7]=-1 ; v[8]= 5; v[9]=2 ;
	parent p[10]; 
	mwnod *rootmw = NULL;
	bnod *root= NULL;


	printf("R1:\n");
	for(i=1;i<10;i++)
		printf("f:%d p:%d / ",i, v[i]);
    printf("\n");
	
	//~~~~~~~~~~~~~~ parent_rep ~~~~~~~~
	for(i=1;i<=n;i++) //initializare
		p[i].nr=0;

	for(i=1;i<=n;i++){ //caut fii si numarul lor pt fiecare parinte
		if(v[i]!=-1)
		{							    //v[i] parintele
			p[v[i]].kids[p[v[i]].nr]=i;	//copii
			p[v[i]].nr++;				//nr copiilor
			//printf("%d  \n",p[v[i]].kids[p[v[i]].nr-1]);
		}
	}
	//print struct ajutatoare
	printf("\nparent rep:\n");
	for(i=1;i<=n;i++)
	{
		if(0<p[i].nr && p[i].nr<10)
		{ printf("p:%d f: ",i);
		  for(int j=0;j<p[i].nr;j++)
			  printf("%d ",p[i].kids[j]);			
		}
		printf("\n");
	}
	//~~~~~~~~~    T1  ~~~~~~ ~~
	for(i=1;i<=n;i++)
		if(v[i]==-1)
		{
			rootmw = (mwnod*)malloc(sizeof(mwnod));
			rootmw->ch=i;
			rootmw->nr=0; // !!am uitat sa init cu 0
			break;
		}
	
	build_mwtree(&rootmw,p);//contruire multi way
	//~~~~~~~~~    R2  ~~~~~~~~
	printf("\nR2:\n");
	print_multi(rootmw,0);
	printf("\n");

	//~~~~~~~~~~   T2  ~~~~~~~~

	root = (bnod*)malloc(sizeof(bnod));
	root->ch= rootmw->ch;
	root->fc=NULL;
	root->rb=NULL;

	//transform(rootmw,root,NULL,0);
	build_bintree(rootmw,&root);
	//pretty print R3
	printf("\nR3:\n");
	print_bin(root,0);

	getch();
    return 0;
}
