#include "Profiler.h"
#include <stdio.h>
#include <conio.h>

Profiler profiler("demo");
const int VMAX = 5000;
const int VMIN = 0;

/*
  average case 
            - bubble sort seems to be the most inefficient amongst the 3 methods
  best case 
            - selection sort seems to be the most inefficient amongst the 3 methods 
            - also both insertion ***ANONIM*** selection sort have a linear growth when it comes to assignments
            - selection sort O(n^2)
  worst case 
            - bubble sort seems to be the most inefficient amongst the 3 methods
			- overall, selection sort seems to be the most effcient sorting method (almost linear when
			it comes to assignments)
*/
void insertionSort (int ar[], int n) // insertion sort function
{
	profiler.countOperation("assign_insertion",n,0);
	profiler.countOperation("count_insertion",n,0);
	
	int k,i,j,buff;
	for (i=1; i<n; i++)
	{
		k = i-1;
		buff=ar[i];
		while ((ar[k]>buff) && (k>=0))
		{
		 ar[j=k+1]=ar[k];profiler.countOperation("assign_insertion",n);
		 k--;profiler.countOperation("count_insertion",n);
		}
		profiler.countOperation("count_insertion",n);
     ar[j=k+1] = buff;profiler.countOperation("assign_insertion",n,2);
	}
}

void selectionSort (int ar[], int n) // selection sort function
{
  int auxil,min,i,j;
	
  profiler.countOperation("assign_selection",n,0);
  profiler.countOperation("count_selection",n,0);
	for(i=0;i<n-1;i++)
	{
		min = i;
		for (j=i+1;j<n;j++)
		{
			profiler.countOperation("count_selection",n);
			if (ar[j]<ar[min]) min = j;
		}
			auxil = ar[min];
			ar[min] = ar[i]; profiler.countOperation("assign_selection",n,3);
			ar[i] = auxil;
	}
}

void bubbleSort(int ar[],int n) // bubble sort function
{
	profiler.countOperation("assign_bubble",n,0);
	profiler.countOperation("count_bubble",n,0);

int sw,temp;
	do
	{
		sw = 0;
		for (int i = 0; i<n-1; i++)
		{
			if (ar[i] > ar[i+1]) 
			{
				temp = ar[i];
				ar[i] = ar[i+1];
				ar[i+1] = temp;
				sw = 1;
				profiler.countOperation("assign_bubble",n,3);
			}
			profiler.countOperation("count_bubble",n);
		}
	}
	while(sw);

}

int* generateR***ANONIM***om(int n)
{
  int *a = new int[n];
  sr***ANONIM***(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=r***ANONIM***()%(VMAX-VMIN) +VMIN;
  return a;
}

int* generateOrdered(int n)
{
 int* a=new int[n]; 
 int mxdiffer=5;
 a[0]=r***ANONIM***()%mxdiffer;
  for (int i=1;i<n;i++)
    a[i]=a[i-1]+r***ANONIM***()%mxdiffer;
  return a;
}

int* generateDecreasing(int n)
{
 int* a=new int[n]; 
 int mxdiffer=5;
 a[n-1]=r***ANONIM***()%mxdiffer;
  for (int i=n-2;i>0;i--)
    a[i]=a[i+1]+r***ANONIM***()%mxdiffer;
  return a;
}

int * deepCopy(int *a, int n)
{
	int *a1 = new int[n];
	for(int i = 0; i<n;i++)
	{
		a1[i] = a[i];
	}
	return a1;
}

void main()
{

// Average case
	/*
	printf("Testing average case \n");
	for (int j = 0;j<5;j++)
	{
	printf("Step %d out of 5 \n",j+1);
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *x = generateR***ANONIM***om(n);
		int *y = deepCopy(x,n);
		int *z = deepCopy(x,n);
		printf("Insertionsort.. \n");
		insertionSort(x,n);
		printf("Selectionsort.. \n");
		selectionSort(y,n);
		printf("Bubblesort.. \n");
		bubbleSort(z,n);
	}
	}
	*/
profiler.createGroup("count","count_insertion","count_selection","count_bubble");
profiler.createGroup("assign","assign_insertion","assign_selection","assign_bubble");
profiler.addSeries("total_insertion","assign_insertion","count_insertion");
profiler.addSeries("total_selection","assign_selection","count_selection");
profiler.addSeries("total_bubble","assign_bubble","count_bubble");
profiler.createGroup("total","total_insertion","total_selection","total_bubble");
profiler.showReport();

// Bubblesort best case
	/*
	for (int n=100;n<10000;n = n + 100)
	{
		int *a = generateOrdered(n);
		bubbleSort(a,n);
	}
	profiler.addSeries("total_bubble","assign_bubble","count_bubble");
	profiler.showReport();

	// Bubblesort worst case
	*/
	printf("Bubblesort worst case.. \n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateDecreasing(n);
		bubbleSort(a,n);
	}
	profiler.addSeries("total_bubble","assign_bubble","count_bubble");
	profiler.showReport();

	// Insertionsort best case
	/*
	for (int n=100;n<10000;n = n + 100)
	{
		int *a = generateOrdered(n);
		insertionSort(a,n);
	}
	profiler.addSeries("total_insertion","assign_insertion","count_insertion");
	profiler.showReport();
	
	// Insertionsort worst case
	*/
	printf("Insertionsort worst case.. \n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateDecreasing(n);
		insertionSort(a,n);
	}
	profiler.addSeries("total_insertion","assign_insertion","count_insertion");
	profiler.showReport();
	
	// Selectionsort best case
	/*
	printf("Selectionsort best case.. \n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateOrdered(n);
		selectionSort(a,n);
	}
	profiler.addSeries("total_selection","assign_selection","count_selection");
	profiler.showReport();
	
	// Selectionsort worst case
	*/
	printf("Selectionsort worst case.. \n");
	int min;
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateOrdered(n);
		min = a[0];
		for(int i=0;i<n-1;i++)
			a[i] = a[i+1];
		a[n-1] = min;
		selectionSort(a,n);
	}
	profiler.addSeries("total_selection","assign_selection","count_selection");
	profiler.showReport();
}