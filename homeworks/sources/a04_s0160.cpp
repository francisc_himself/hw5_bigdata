/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Cerinta problema:
-pentru interclasarea interna se considera doua sau mai multe liste de inregistrari ordonate
si se genereaza o singura lista continand toate inregistrarile listelor de intrare, de  asemenea ordonate.
Algoritm de rezolvare:
-pentru a interclasa cele k liste de n elemente vom folosi un algoritm de eficienta O(n*log k) si anume
vom lua primele elemente din fiecare lista, vom gasi minimul si vom incrementa contorul listei in care
am gasit elementul minim.

In ceea ce priveste graficele, eu intuiesc ca numarul de operatii va creste o data cu numarul de liste.
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_SIZE 1000000
#define MAX_SIZE1 1000


typedef struct _NOD{
	int value;
	struct _NOD *next;
}NOD;

NOD *head[MAX_SIZE];
NOD *tail[MAX_SIZE];
NOD *p1[1000000],*p2[1000000];
NOD *lista_finala[MAX_SIZE],*lista_finala1[MAX_SIZE],*lista_finala2[MAX_SIZE],*lista_finala3[MAX_SIZE];
int dimHeap;

void max_heapify(NOD *A[], int i, int dimA, int &nr_total_operatii)
{
	int l, r, largest, aux;

	l = 2*i;
	r = 2*i + 1;

	if(l <= dimHeap && A[l]->value < A[i]->value)
	{
		largest = l;
	}
	else
	{
		largest = i;
	}

	if( r <= dimHeap && A[r]->value < A[largest]->value)
	{
		largest = r;
	}
	nr_total_operatii += 2;

	if(largest != i)
	{
		A[0] = A[i];
		A[i] = A[largest];
		A[largest] = A[0];

		nr_total_operatii += 3;

		max_heapify(A, largest, dimA, nr_total_operatii);
	}
}

//constructie Heap metoda Bottom-up
void buildH_BU(NOD *A[], int dimA, int &nr_total_operatii)
{
	int i;
	
	dimHeap=dimA;
	for(i = dimHeap/2; i>0; i--)
	{	
		max_heapify(A, i, dimA, nr_total_operatii);
	}
}

//afisarea listei - afiseaza continutul listei construite
void showList(NOD *h, NOD *ultim)
{
	NOD *p1;
	p1=h; 
	while(p1 != NULL) 
	{
		printf("%d ", p1->value); 
		p1 = p1->next; 
	}
}

//crearea unei liste prin adaugarea succesiva de noduri
void creareList(NOD **h, NOD **t, int valoare)
{
	if((*h)==NULL)
	{
		(*h)=new NOD;
		(*h)->next=NULL;
		(*h)->value=valoare;
		(*t)=(*h);
	}
	else
	{
		NOD *p=new NOD;
		p->next=NULL;
		p->value=valoare;
		(*t)->next=p;
		(*t)=p;
	}
}


int main()
{
	//crearea si afisarea elementelor unei liste
	/*int n,valoare;
	NOD *h=0;
	NOD *t=0;
	int i;

	n=10;
	for(i=0;i<n;i++)
	{
		printf("5dati valoare= ");
		scanf("%d",&valoare);
		creareList(&h,&t,valoare);
	}
	showList(h,t);*/
	
	FILE* f1; 
	FILE* f2;
	NOD *h=0;
	NOD *t=0;
	int valoare,i,j,n,k,dim,dim1,tt,ttt,contor;
	int nr_total_operatii1=0,nr_total_operatii2=0,nr_total_operatii3=0;
	srand(time(NULL));
	f1=fopen("k_constant.csv","w");
	f2=fopen("n_constant.csv","w");
	
	//verificare pentru 4 liste care contin in total 20 de elemente (fiecare lista contine 5 elemente)
	k=4;
	n=20;
	for (i=1;i<=k;i++)
	{
		valoare=rand() % (k*5)+1;
		creareList(&head[i],&tail[i],valoare);
		for (j=2;j<=n/k;j++)
		{
			valoare= valoare + rand() % (k*5)+1;
			creareList(&head[i],&tail[i],valoare);
		}
		printf("\nLista %d este:\n",i);
		showList(head[i],tail[i]);
		p1[i]=head[i];
		
	}
	dim=k;
	contor=1;
	buildH_BU(p1,dim,nr_total_operatii1);

	while(dim!=0)
	{
		lista_finala[contor]=new NOD;
		lista_finala[contor]->value=p1[1]->value;
		p1[1]=p1[1]->next;
		lista_finala[contor]->next=NULL;
		contor++;
		if (p1[1]==NULL)
		{
			p1[1]=p1[dim];
			dimHeap--;
			dim--;
		}

		max_heapify(p1,1,dim,nr_total_operatii1);
	
	}
	printf("\n");
	for(j=1;j<=n;j++)
	{
		printf("%d ",lista_finala[j]->value);
	}

	//pt k=5,10,100
	//k=5;
	for(dim=100;dim<=1000;dim+=100)
	{
		printf("\n %d",dim);
		
		//pt k=5
		k=5;
		nr_total_operatii1=0;
		for (j=1;j<=k;j++)
		{
		    for (ttt=1;ttt<=k;ttt++)
			{
				valoare=rand() % (k*5)+1;
				creareList(&head[ttt],&tail[ttt],valoare);
				for (tt=2;tt<=dim/k;tt++)
				{
					valoare=valoare + rand() % (k*5)+1;
					creareList(&head[ttt],&tail[ttt],valoare);
				}
				p1[ttt]=head[ttt];
			}
			dim1=k;
			contor=1;
			buildH_BU(p1,dim1,nr_total_operatii1);

			while(dim1!=0)
			{
				lista_finala[contor]=new NOD;
				lista_finala[contor]->value=p1[1]->value;
				p1[1]=p1[1]->next;
				lista_finala[contor]->next=NULL;
				contor++;
				if (p1[1]==NULL)
				{
					p1[1]=p1[dim1];
					dimHeap--;
					dim1--;
				}

				max_heapify(p1,1,dim1,nr_total_operatii1);
	
			}
		}
		
		//pt k=10
		k=10;
		nr_total_operatii2=0;
		for (j=1;j<=k;j++)
		{
		    for (ttt=1;ttt<=k;ttt++)
			{
				valoare=rand() % (k*5)+1;
				creareList(&head[ttt],&tail[ttt],valoare);
				for (tt=2;tt<=dim/k;tt++)
				{
					valoare=valoare + rand() % (k*5)+1;
					creareList(&head[ttt],&tail[ttt],valoare);
				}
				p1[ttt]=head[ttt];
			}
			dim1=k;
			contor=1;
			buildH_BU(p1,dim1,nr_total_operatii2);

			while(dim!=0)
			{
				lista_finala[contor]=new NOD;
				lista_finala[contor]->value=p1[1]->value;
				p1[1]=p1[1]->next;
				lista_finala1[contor]->next=NULL;
				contor++;
				if (p1[1]==NULL)
				{
					p1[1]=p1[dim1];
					dimHeap--;
					dim1--;
				}

				max_heapify(p1,1,dim1,nr_total_operatii2);
	
			}
		}

		//pt k=100
		k=100;
		nr_total_operatii3=0;
		for (j=1;j<=k;j++)
		{
		    for (ttt=1;ttt<=k;ttt++)
			{
				valoare=rand() % (k*5)+1;
				creareList(&head[ttt],&tail[ttt],valoare);
				for (tt=2;tt<=dim/k;tt++)
				{
					valoare=valoare + rand() % (k*5)+1;
					creareList(&head[ttt],&tail[ttt],valoare);
				}
				p1[ttt]=head[ttt];
			}
			dim1=k;
			contor=1;
			buildH_BU(p1,dim1,nr_total_operatii3);

			while(dim1!=0)
			{
				lista_finala[contor]=new NOD;
				lista_finala[contor]->value=p1[1]->value;
				p1[1]=p1[1]->next;
				lista_finala[contor]->next=NULL;
				contor++;
				if (p1[1]==NULL)
				{
					p1[1]=p1[dim1];
					dimHeap--;
					dim1--;
				}

				max_heapify(p1,1,dim1,nr_total_operatii3);
	
			}
		}

		fprintf(f1, "%d, %d, %d %d\n",dim,nr_total_operatii1/5,nr_total_operatii2/10,nr_total_operatii3/100);
	}
	
	
	
	
	//pt n=10000
	n=10000;
	for(dim=10;dim<=500;dim+=10)
	{
		printf("\n %d",dim);
		nr_total_operatii1=0;
		for (j=1;j<=5;j++)
		{
		    for (ttt=1;ttt<=k;ttt++)
			{
				valoare=rand() % (k*5) + 1;
				creareList(&head[ttt],&tail[ttt],valoare);
				for (tt=2;tt<=10000/i;tt++)
				{
					valoare=valoare + rand() % (k*5)+1;
					creareList(&head[ttt],&tail[ttt],valoare);
				}
				p1[ttt]=head[ttt];
			}
			dim1=i;
			contor=1;
			buildH_BU(p1,dim1,nr_total_operatii1);

			while(dim1!=0)
			{
				lista_finala[contor]=new NOD;
				lista_finala[contor]->value=p1[1]->value;
				p1[1]=p1[1]->next;
				lista_finala[contor]->next=NULL;
				contor++;
				if (p1[1]==NULL)
				{
					p1[1]=p1[dim1];
					dimHeap--;
					dim1--;
				}

				max_heapify(p1,1,dim1,nr_total_operatii1);
	
			}
		}
		fprintf(f2, "%d, %d\n",dim,nr_total_operatii1/5);
	}
	
	getch();
}