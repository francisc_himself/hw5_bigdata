﻿#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <vector>
#include <math.h>
#include <limits.h>
#include "Profiler.h"

using namespace std;

Profiler profiler;

/**
***ANONIM*** ***ANONIM***, group ***GROUP_NUMBER***
Top down and bottom-up methods of building a heap


Top Down method of heap building is much slower than Bottom Up heap building method. Bottom up method is almost two times faster.
This is because the top down method does a swap every time the new element is inserted and is greater than the parent. But if its sibiling,
element that is inserted next, is larger than both the previous parent and the new parent (resulted after the recent swap), another unncecessary 
swap is made. This is avoided in Bottom Up because the swap is only made when we are certain which of the two sibilings is larger (if they are both
larger than the parent).
Runtime of bottom up is O(n), runtime of top down is O(n*log n) 

Bottom up method is used when we already have all elements of the heap. Top down allows for unknown elements and dynamic insertion in the heap.

The two approaches do not result in the same output, as the heap is not a unique result for a given input.


**/

int left(int i){
    return 2*i+1;
}

int right(int i){
    return 2*i + 2;
}

int parent(int i){
	return (i-1)/2;
}	
void max_heapify(int A[], int i, int heapsize){
    int l = left(i);
    int r = right(i);
    int largest = 0;

	profiler.countOperation("BottomUp", heapsize, 1);//comparison
    if(l < heapsize && A[l]>A[i]) {
        largest = l;
    }
    else largest = i;

	profiler.countOperation("BottomUp", heapsize, 1);
    if(r < heapsize && A[r]>A[largest]) { //comparison
        largest = r;
    }
    if(largest != i){
        int aux = A[i];
        A[i] = A[largest];
        A[largest] = aux;
		profiler.countOperation("BottomUp", heapsize, 3);//assignment
        max_heapify(A, largest, heapsize);
    }
}

void bottomUpBuildHeap(int A[], int heapsize){
    for(int i = heapsize/2; i>=0; i--){
        max_heapify(A, i, heapsize);
    }
}

// increase_key(A, i, key)
void increase_key(int A[], int i, int key, int inputSize){
	profiler.countOperation("TopDown",inputSize,1); // comparison
	if(key < A[i]) {
		printf("%s","Heap property maintained.");
		goto out;
	}
	profiler.countOperation("TopDown",inputSize,1);//assignment
	A[i] = key;
	while(i >= 1 && A[parent(i)] < A[i]){
		int aux = A[parent(i)];
		A[parent(i)] = A[i];
		A[i] = aux;
		profiler.countOperation("TopDown",inputSize,3);//assignment
		i = parent(i);
		profiler.countOperation("TopDown",inputSize,1);//comparison
	}
out: ;
}

// heap_insert(heap, heapsize, key)
void heap_insert(int A[], int *heapsize, int key, int inputSize){
	(*heapsize) ++;
	profiler.countOperation("TopDown",inputSize,1); // assignment
	A[*heapsize] = -111111;//random val = -infinity
	increase_key(A, *heapsize, key, inputSize);
}

void topDownBuildHeap(int A[], int length){
	int *heapsize;
	int k = 0;
	heapsize = &k;

	for(int i=1; i<length;i++)
		heap_insert(A, heapsize, A[i], length);
}
//generate same random sequence for both
void generateRandom(int A[], int B[], int length){
	FillRandomArray(A,length,100,10000,false,0);
	for(int i = 0; i< length; i++)
		B[i] = A[i];
}

//elements are in reverse order
void generateWorst(int A[], int B[], int length){
	for(int i=0;i<length;i++)
	{
		A[i] = i;
		B[i] = i;
	}
}

int main()
{
	/*hard-coded input for BOTTOM-UP heap building 
    int A[10]={4,1,3,2,16,9,10,14,8,7};

	bottomUpBuildHeap(A,10);
	for(int j=0;j<10;j++)
        cout<<A[j]<<" ";
	cout<<endl;

	for(int i = 0; i<5;i++){
		printf("\t");
	} 
	printf("%d\n", A[0]);//root level
	for(int i = 0;i<3;i++){
		printf("\t");
	}
	printf("%d				%d\n", A[1], A[2]);//first level 
	for(int i = 0; i<2;i++){
		printf("\t");
	}
	printf("%d		%d", A[3], A[4]);//2nd level, first pair
	for(int i = 0; i<2;i++){
		printf("\t");
	}
	printf("%d		%d\n", A[5], A[6]);// 2nd level, second pair
	for(int i = 0; i<1;i++){
		printf("\t");
	}
	printf("%d		%d %d", A[7], A[8], A[9]);// 3rd level, the two pairs
	*/

	/*hard-coded input for TOP-DOWN heap building 
	int B[10]={4,1,3,2,16,9,10,14,8,7};

	topDownBuildHeap(B,10);
    for(int j=0;j<10;j++)
      cout<<B[j]<<" ";
	cout<<endl;

	
	for(int i = 0; i<5;i++){
		printf("\t");
	} 
	printf("%d\n", B[0]); //root level
	for(int i = 0;i<3;i++){
		printf("\t");
	}
	printf("%d				%d\n", B[1], B[2]);//first level 
	for(int i = 0; i<2;i++){
		printf("\t");
	}
	printf("%d		%d", B[3], B[4]);//2nd level, first pair
	for(int i = 0; i<2;i++){
		printf("\t");
	}
	printf("%d		%d\n", B[5], B[6]);// 2nd level, second pair
	for(int i = 0; i<1;i++){
		printf("\t");
	}
	printf("%d		%d %d", B[7], B[8], B[9]);// 3rd level, the two pairs
	*/

	//ACTUAL COMPUTATIONS

	int A[10001], B[10001], n;

	//AVERAGE CASE
	/*for(int i=1;i<=5;i++){
		for(n=100; n<=10000; n+=100){
			generateRandom(A,B,n);
			bottomUpBuildHeap(A,n);
			topDownBuildHeap(B,n);
		}
	}
	profiler.createGroup("Average Number of Operations","TopDown","BottomUp");
	profiler.showReport();*/

	//WORST CASE
	for(n=100; n<=10000; n+=100){
		generateWorst(A,B,n);
		bottomUpBuildHeap(A,n);
		topDownBuildHeap(B,n);
	}
	profiler.createGroup("Worst Case Number of Operations","TopDown","BottomUp");
	profiler.showReport();

	getch();
    return 0;
}