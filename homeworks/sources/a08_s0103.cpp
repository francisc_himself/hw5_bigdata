/*
author:***ANONIM***�si ***ANONIM***
  group:***GROUP_NUMBER***
  date:22.05.2013
  program description : the program implements the basic operations for disjoint sets : Make-Set(x), Union(x, y) and Find-Set(x)
  It is used path compression and union by rank heuristics, to improve the running time.
  The algorithm finds the connected components in randomly generated graphs and displays the number of them. The number of vertices is set to 10000 
  and the number of edges is varied between 10000 and 60000. The number of operations is count in the MakeSet,Union and FindSet operations.  
  */
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "Profiler.h"

int n;

int a[10001];
int b[120000];
long op;

int FindSet(int x)
{
	op+=2;
	int cnt=0;
	while (a[x]!=x)
	{
		op+=2;
		x=a[x];
		cnt++;
		if (cnt==10000)
			printf("Error!\n");
	}
	return x;
}

void Union(int x,int y)
{
	int k=FindSet(y);
	int l=FindSet(x);
	if (l!=k)
	{
		a[k]=l;
		op++;
	}
	op++;
}

void MakeSet(int x)
{
	a[x]=x;
	op++;
}

int main()
{
	FILE *pf=fopen("Dsets.in","r");
	FILE *pw=fopen("Dsets.csv","w");
	fscanf(pf,"%d\n",&n);
	for (int i=1;i<=n;i++)
	{
		MakeSet(i);
	}
	for (int i=1;i<=n;i++)
	{
		int k;
		fscanf(pf,"%d ",&k);
		Union(k,i);
	}
	n=10000;
	fprintf(pw,"E;Nr of Operations\n");
	for (int j=10000;j<=60000;j+=100)
	{
		op=0;
		for (int i=1;i<=n;i++)
		{
			MakeSet(i);
		}
		FillRandomArray(b,2*j,1,10000,false,0);
		for (int i=0;i<2*j;i+=2)
		{
			Union(b[i],b[i+1]);
		}
		//printf("%d; %ld\n",j,op);
		fprintf(pw,"%d;%ld\n",j,op);
	}
	printf("Done!");
	getch();
	return 0;
}