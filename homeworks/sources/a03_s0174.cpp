/*
*   Rulind acest program si vazand rezultatele, am observat ca ambele
*   metode de sortare se comporta la fel in cazul mediu statistic, avand
*   o eficienta O(n*log(n)).
*
*   Deasemenea am observat ca algoritmul QuickSort se comporta foarte rau in
*   cazul in care este deja sortat, avand eficienta: O(n^2).
*/


/*
*   Autor:  ***ANONIM*** ***ANONIM***
*   Functionalitate:  Sortarea unor vectori creati aleator prin metodele HeapSort
*   si QuickSort, cu introducerea ulterioara a datelor de sortare intr-un fisier csv.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define nmax 10000

int mainArray[nmax];
int sir[nmax];
int worstCase[nmax];
int worst[nmax];

int atribuiriPlusComparatii[1] = {0};

/*
*   Functia care genereaza un cel mai rau caz pentru QuickSort;
*/
void generateWorstCase(){
    int i;
	srand((int)time(NULL));
	worstCase[0] = rand();
	for(i = 1; i < nmax; i++){
		worstCase[i] = worstCase[i - 1] + rand();
	}
}

/*
*   Functia care genereaza un Array aleator;
*/
void generateArray(){
	int i;
	srand((int)time(NULL));
	for(i = 0; i < nmax; i++){
		mainArray[i] = rand();
	}
}

/*
*   Functia care copie valorile din array-ul primului argument;
*   in cel de-al doilea array;
*/
void copyArray(int sir1[], int sir2[], int n){
int i;
for(i = 0; i < n; i++){
    sir2[i] = sir1[i];
}
}

/*
*   Functia de partitionare a intervalului;
*/
int partitionare(int sir[], int left, int right){
	int aux;
	int i = left - 1;
	int x = sir[right];
	int j;

	for(j = left; j < right - 1; j++){
		if (sir[j] <= x){
			i = i + 1;
            aux = sir[j];
			sir[j] = sir[i];
			sir[i] = aux;
		}
		atribuiriPlusComparatii[0]++;

	}
	i++;
    aux = sir[i];
	sir[i] = sir[right];
	sir[right] = aux;

    return i;
	}

/*
*   Functia quick sort;
*/
void quickSort(int sir[], int left, int right){
	int pivotNewIndex = 0;
	if(left < right) {
          atribuiriPlusComparatii[0]++;
          pivotNewIndex = partitionare(sir, left, right);
          quickSort(sir, left, pivotNewIndex - 1);
          quickSort(sir, pivotNewIndex + 1, right);
	}
}

/*
*   Parcurgerea vectorului ca un Heap, si sortarea acestuia.
*/
void bottomUpHeapSort(int sir[], int n)
 {
   int val, parent, child;
   int root = n;
   int count = 0;

   for ( ; ; )
   {
     if ( root ) {
       parent= --root;
       val= sir[root];
     }
     else
     if ( --n ) {
       val= sir[n];
       sir[n]= sir[0];
       parent= 0;
     }
     else
       break;

     while ( (child= (parent + 1) << 1) < n )
     {
       if ( ++count, sir[child-1] > sir[child] )
         --child;

       sir[parent]= sir[child];
       parent= child;
     }

     if ( child == n )
     {
       if ( ++count, sir[--child] >= val ) {
         sir[parent]= sir[child];
         sir[child]= val;
         continue;
       }

       child= parent;
     }
     else
     {
       if ( ++count, sir[parent] >= val ) {
         sir[parent]= val;
         continue;
       }

       child= (parent - 1) >> 1;
     }

     while ( child != root )
     {
       parent= (child - 1) >> 1;
       if ( ++count, sir[parent] >= val )
         break;

       sir[child]= sir[parent];
       child= parent;
     }

     sir[child]= val;
   }
    atribuiriPlusComparatii[0] = count;
}

/*
*   Main;
*/
int main(){
	int n;
    FILE *df;
	df = fopen("result.csv", "w");

    generateArray();
    generateWorstCase();
    for(n = 100; n <= nmax; n += 100){
        //HeapSort
        copyArray(mainArray, sir, n);
        bottomUpHeapSort(sir, n);
        fprintf(df, "%d,", atribuiriPlusComparatii[0]);
        atribuiriPlusComparatii[0] = 0;

        //Average Case QuickSort
        copyArray(mainArray, sir, n);
        quickSort(sir, 0, n);
        fprintf(df, "%d, ", atribuiriPlusComparatii[0]);
        atribuiriPlusComparatii[0] = 0;

        //Worst Case QuickSort
        copyArray(worstCase, worst, n);
        quickSort(worst, 0, n);
        fprintf(df, "%d, ", atribuiriPlusComparatii[0]);
        atribuiriPlusComparatii[0] = 0;

        fprintf(df, "\n");
    }

fclose(df);
return 0;
}
