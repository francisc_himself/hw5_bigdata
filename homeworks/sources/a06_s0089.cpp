
#include "Profiler.h"

Profiler profiler("***ANONIM***");


typedef struct tree_node{ 
	int number;
	int size; 
	tree_node *left;
	tree_node *right;
	tree_node *parent;
} tree_node;

typedef struct node{ 
	int number;
	node *next;
} node;


node *first, *last;
int comparisons,assignments;


void listInsert(int value){
	node *p;
	p=(node *)malloc(sizeof(node));
    p->number=value;
    p->next=NULL;
	if (first==NULL) {
		first=p;
		last=p;
		last->next=NULL;
	}
	else {
		last->next=p;
        last=p; 
	}
}

void listPrint(node *firstNode){
    node *p;
	p=firstNode;
    while(p!=NULL){
		printf("%d  ",p->number);
		p=p->next;
    }
    printf("\n");
}

void inorder(tree_node *p,int nivel){ 
	int i;
	if (p!=0){ 
		inorder(p->left,nivel+1);
	    for (i=0;i<=nivel;i++)
	        printf("    ");
	    printf("%d (%d)\n",p->number, p->size);
	    inorder(p->right,nivel+1);
	}
}
 
tree_node* leftMost(tree_node *tree){ 
	while (tree->left!=NULL)	{     
		assignments++; 
        tree=tree->left;
	}
	return tree;
}

tree_node* succesor(tree_node *tree){ 
	tree_node *y;
	comparisons++;
	if (tree->right!=NULL)
		return leftMost(tree->right);
	assignments++;
	y=tree->parent;
	while ( (y!=NULL) && (tree==y->right) ){ 
		assignments=assignments+2;
		tree=y;
		y=y->parent;
	}
	return y;
}

tree_node *treeBuild(int left, int right, int *dim){ 
	tree_node *p;
	int dim1=0;
	int dim2=0;
	comparisons++;
	if (left>right){ 
		*dim=0;
   	    return NULL;
	}
	else{ 
		assignments=assignments+5;
		p=(tree_node*) malloc(sizeof(tree_node));
	    p->number=(left+right)/2;
		p->left = NULL;
		p->right = NULL;
		p->size=right-left+1;
		if(left<right){
			p->left=treeBuild(left,(left+right)/2-1,&dim1);
			if(p->left != NULL){
				p->left->parent = p;  
				assignments++; 
			}
			p->right=treeBuild((left+right)/2+1,right,&dim2);
			if(p->right != NULL){	
				p->right->parent = p; 
				assignments++;
			}
		}
	        
	}
	assignments++;
	*dim=p->size;
	return p;
}


void dimension(tree_node *node){ 
	while (node!=NULL)
	{  
      assignments=assignments+2; 
	  node->size--;
	  node=node->parent;
	}
}

void OS_DELETE(tree_node **tree, tree_node *z){ 
	tree_node *y,*x;
	comparisons=comparisons+5;
	if ( (z->left==NULL) || (z->right==NULL) ){ 
		y=z; 
		assignments++;
    }
    else{
		y=succesor(z); 
		assignments++; 
	}
	if (y->left!=NULL){ 
		x=y->left; 
		assignments++; 
	}
    else{	 
		x=y->right; 
		assignments++; 
	}
	if (x!=NULL){   
		assignments++;
		x->parent=y->parent;
    }
	dimension(y->parent);
	if (y->parent==NULL)
		*tree=x;
    else
		if (y==y->parent->left){ 
			y->parent->left=x; 
			assignments++;
		}
		else{ 
			y->parent->right=x; 
			assignments++; 
		}
	if (y!=z){   
		assignments++;
		z->number=y->number;
    }
	free(y);
}


tree_node* OS_SELECT(tree_node *tree, int i){ 
	int r;
	comparisons += 3; 
	assignments++;
	if (tree->left!=NULL) 
		r=tree->left->size+1;
    else 
		r=1;

	if (i==r)
		return tree;
	if (i<r)
		return OS_SELECT(tree->left,i);
    else
	return OS_SELECT(tree->right,i-r);
}

void josephus(int n, int m){ 
	tree_node *root,*x;
	int h;
	int k;
	root=treeBuild(1,n,&h);
	root->parent=NULL;

	k=m%n; 
	if (k==0)
		k=n;
	while (n>0){ 
		x=OS_SELECT(root,k);
		listInsert(x->number);
		OS_DELETE(&root,x);
		n--;
		if (n){ 
			k=(k+m-1)%n;
			if (k==0)
				k=n;
	    }
	}
}

void main()
{ 

	profiler.createGroup("Operations", "Assignments", "Comparisons", "TotalOp");

	int n,m;
	for (int i=100; i<10000; i+=100)
	{
		assignments=0;
		comparisons=0;
		n=i;
		m=n/2;
		josephus(n,m);

		//profiler.countOperation("Comparisons", i, comparisons);
		//profiler.countOperation("Assignments", i, assignments);
		profiler.countOperation("TotalOp", i, assignments + comparisons);

	}

	profiler.showReport();
    
}
