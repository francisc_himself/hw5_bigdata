#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler profiler("demo");
const int VMAX = 5000;
const int VMIN = 0;

int comp=0;
int attr=0;

void selection_sort(int *a,int n) //SELECTION SORT
{ 
  //comp=0;
  //attr=0;
  int buff;
	profiler.countOperation("Assignments SelectionSort",n,0);
	profiler.countOperation("Comparisons SelectionSort",n,0);
  for (int i=0;i<n-1;i++)
  {
    int min=i;
    for (int j=i+1;j<n;j++)
	{
	 profiler.countOperation("Comparisons SelectionSort",n);
     //comp++;
     if (a[j]<a[min])
		min=j;
    }
	buff=a[min];
	a[min]=a[i];
	profiler.countOperation("Assignments SelectionSort",n,3);
	a[i]=buff;
    //attr+=3
  }
//printf("Selection sort: ");
	//for(int i=0;i<n;i++)
	//{
		//printf("%d",a[i]);
		//printf(" ");
	//}	
//printf("\n");
//printf("Comparisons = %d\n",comp);
//printf("Assignments = %d\n",attr);
}


void bubble_sort(int *a, int n) //BUBBLE SORT
{
  int i,aux;
  int swap;
  //comp=0;
  //attr=0;
  profiler.countOperation("Assignments BubbleSort",n,0);
  profiler.countOperation("Comparisons BubbleSort",n,0);
  do
  {
	swap = 0;
	for(i=0;i<n-1;i++)
	{
		//comp++;
		if(a[i]>a[i+1])
		{
			//attr++;
			aux=a[i];
			a[i]=a[i+1];
			a[i+1]=aux;
			swap = 1;
			profiler.countOperation("Assignments BubbleSort",n,3);
		}
		profiler.countOperation("Comparisons BubbleSort",n);
	}
  }
  while(swap);

//printf("Bubble sort: ");
//for(int i=0;i<n;i++)
//{
	//printf("%d",a[i]);
	//printf(" ");
//}
//printf("\n");
//printf("Comparisons = %d\n",comp);
//printf("Assignments = %d\n",attr);
}


void insertion_sort(int *a, int n) //INSERTION SORT
{
    int select;
    int  j;
	//comp=0;
	//attr=0;
    for (int i = 1; i < n ; i++)
    {
        select = a[i];
        j = i-1;
        //attr++;
        //comp++;
		profiler.countOperation("Assignments InsertionSort",n);
		profiler.countOperation("Comparisons InsertionSort",n);
        while ((j >=0 ) && (a[j] > select))
        {
            a[j+1] = a[j];
			profiler.countOperation("Assignments InsertionSort",n);
            j--;
			profiler.countOperation("Comparisons InsertionSort",n);
            //attr++;
            //comp++;
        }
        a[j+1] = select;
		profiler.countOperation("Assignments InsertionSort",n);
        //comp++;
    }
//printf("Insertion sort: ");
//for(int i=0;i<n;i++)
//{	
	//printf("%d",a[i]);
	//printf(" ");
//}
//printf("\n");
//printf("Comparisons = %d\n",comp);
//printf("Assignments = %d\n",attr);
}


int* generateRandom(int n)// GENERATE RANDOM NUMBER ARRAY
{
  int *a = new int[n];
  srand(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=rand()%(VMAX-VMIN) + VMIN;
  return a;
}


int* generateOrdered(int n)// GENERATE ORDERED NUMBERS
{
 int* a=new int[n]; 
 int maxdiff=5;
 a[0]=rand()%maxdiff;
  for (int i=1;i<n;i++)
    a[i]=a[i-1]+rand()%maxdiff;
  return a;
}


int* generateDecreasing(int n)
{
 int* a=new int[n]; 
 int maxdiff=5;
 a[n-1]=rand()%maxdiff;
  for (int i=n-2;i>0;i--)
    a[i]=a[i+1]+rand()%maxdiff;
  return a;
}


int * deepCopy(int *a, int n)
{
	int *b = new int[n];
	for(int i = 0; i<n;i++)
	{
		b[i] = a[i];
	}
	return b;
}


void main()
{
//BASIC PROGRAM TO SHOW FUNCTION OF SORTING ALGORITHMS
	/*
	int n;
	int a[100];
	int vers;
	printf("Enter the array length = ");
	scanf("%d",&n);

	for(int i=0;i<n;i++)
	{
		printf("a[%d]= ",i);
		scanf("%d", &a[i]);
	
	}
	printf("\n");
	printf("Choose algorithm: \n");
	printf("1 - for Selection sort \n");
	printf("2 - for Bubble sort \n");
	printf("3 - for Insertion sort \n\n");
	printf("Choose variant = "); 
	scanf("%d",&vers);
	printf("\n");
	
	while ((vers==1) || (vers==2) || (vers==3))
	{
	if ((vers!=1) && (vers!=2) && (vers!=3))
		break;

	if (vers==1)
		selection_sort(a,n);
	if (vers==2)
		bubble_sort(a,n);
	if (vers==3)
		insertion_sort(a,n);

	printf("\n");
	printf("Choose variant = "); 
	scanf("%d",&vers);
	
    }
	*/



//TO TEST IF IT WORKS
	/*
	int *a = generateRandom(500);
	int *b = deepCopy(a,500);
	int *c = deepCopy(a,500);
	bubble_sort(c,500);
	insertion_sort(a,500);
	selection_sort(b,500);
	for (int i = 0;i<500;i++)
		printf("%d ",a[i]);
	printf("\n \n");
	for(int i = 0;i<500;i++)
		printf("%d ",b[i]);
	printf("\n \n");
	for(int i = 0;i<500;i++)
		printf("%d ",c[i]);
	*/



//BUBBLESORT BEST CASE
	/*for (int n=100; n<10000; n=n+100)
	{
		printf("nr= %d\n",n);
		int *a = generateOrdered(n);
		bubble_sort(a,n);
	}
	profiler.createGroup("Best Case BubbleSort","Assignments BubbleSort");
    profiler.createGroup("Best Case BubbleSort","Comparisons BubbleSort");
	profiler.addSeries("Total BubbleSort","Assignments BubbleSort","Comparisons BubbleSort");
	profiler.createGroup("Best Case BubbleSort","Total BubbleSort");
	profiler.showReport();
	*/


	 
//BUBBLESORT WORST CASE
	/*printf("BUBBLESORT WORST CASE SORT\n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("nr= %d\n",n);
		int *a = generateDecreasing(n);
		bubble_sort(a,n);
	}
	profiler.createGroup("Worst Case BubbleSort","Assignments BubbleSort");
    profiler.createGroup("Worst Case BubbleSort","Comparisons BubbleSort");
	profiler.addSeries("Total BubbleSort","Assignments BubbleSort","Comparisons BubbleSort");
	profiler.createGroup("Worst Case BubbleSort","Total BubbleSort");
	profiler.showReport();
	*/
	



//INSERTIONSORT BEST CASE
	/*
	for (int n=100;n<10000;n = n + 100)
	{
		printf("nr= %d\n",n);
		int *a = generateOrdered(n);
		insertion_sort(a,n);
	}
	profiler.createGroup("Best Case InsertionSort","Assignments InsertionSort");
    profiler.createGroup("Best Case InsertionSort","Comparisons InsertionSort");
	profiler.addSeries("Total InsertionSort","Assignments InsertionSort","Comparisons InsertionSort");
	profiler.createGroup("Best Case InsertionSort","Total InsertionSort");
	profiler.showReport();
	*/



//INSERTIONSORT WORST CASE
	
	/*printf("INSERTIONSORT WORST CASE SORT\n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("nr= %d\n",n);
		int *a = generateDecreasing(n);
		insertion_sort(a,n);
	}
	profiler.createGroup("Worst Case InsertionSort","Assignments InsertionSort");
    profiler.createGroup("Worst Case InsertionSort","Comparisons InsertionSort");
	profiler.addSeries("Total InsertionSort","Assignments InsertionSort","Comparisons InsertionSort");
	profiler.createGroup("Worst Case InsertionSort","Total InsertionSort");
	profiler.showReport();
	*/



//SELECTIONSORT BEST CASE
	/*printf("SELECTIONSORT BEST CASE SORT\n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("nr= %d\n",n);
		int *a = generateOrdered(n);
		selection_sort(a,n);
	}
	profiler.createGroup("Best Case SelectionSort","Assignments SelectionSort");
    profiler.createGroup("Best Case SelectionSort","Comparisons SelectionSort");
	profiler.addSeries("Total SelectionSort","Assignments SelectionSort","Comparisons SelectionSort");
	profiler.createGroup("Best Case SelectionSort","Total SelectionSort");
	profiler.showReport();
	*/


//SELECTIONSORT WORST CASE
	/*printf("SELECTION SORT WORST CASE SORT\n");
	int min;
	for (int n=100;n<10000;n = n + 100)
	{
		printf("nr= %d\n",n);
		int *a = generateOrdered(n);
		min = a[0];
		for(int i=0;i<n-1;i++)
			a[i] = a[i+1];
		a[n-1] = min;
		selection_sort(a,n);
	}
	profiler.createGroup("Worst Case SelectionSort","Assignments SelectionSort");
    profiler.createGroup("Worst Case SelectionSort","Comparisons SelectionSort");
	profiler.addSeries("Total SelectionSort","Assignments SelectionSort","Comparisons SelectionSort");
	profiler.createGroup("Worst Case SelectionSort","Total SelectionSort");
	profiler.showReport();
	*/


//AVERAGE CASE TEST
	
	/*printf("AVERAGE CASE TEST \n");
	for (int j = 0;j<5;j++)
	{
	printf("step %d/5 \n",j+1);
	for (int n=100;n<10000;n = n + 100)
	{
		printf("nr= %d\n",n);
		int *f = generateRandom(n);
		int *d = deepCopy(f,n);
		int *e = deepCopy(f,n);
		printf("INSERTIONSORT:\n");
		insertion_sort(f,n);
		printf("SELECTIONSORT:\n");
		selection_sort(d,n);
		printf("BUBBLESORT:\n");
		bubble_sort(e,n);
	}
	}
	profiler.createGroup("Average Assignments","Assignments InsertionSort","Assignments SelectionSort","Assignments BubbleSort");
	profiler.createGroup("Average Comparisons","Comparisons InsertionSort","Comparisons SelectionSort","Comparisons BubbleSort");
	profiler.addSeries("Total SelectionSort","Assignments SelectionSort","Comparisons SelectionSort");
	profiler.addSeries("Total BubbleSort","Assignments BubbleSort","Comparisons BubbleSort");
	profiler.addSeries("Total InsertionSort","Assignments InsertionSort","Comparisons InsertionSort");
	profiler.createGroup("Total Average","Total SelectionSort","Total BubbleSort","Total InsertionSort");

	
	
	profiler.showReport();
	*/
}
