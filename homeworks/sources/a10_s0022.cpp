#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <math.h>
#include <string.h>

/*
Strategia urmata de DFS este, dupa cum sugereaza si numele, pentru a cauta cat mai 
"ad�nc" �n graf ori de c�te ori este posibil. Adancimea de cautare exploreaza muchiile din
din v�rful ***ANONIM*** mai recent descoperit care �nca e neexplorat.
Dupa ce toate marginile au fost explorate, DFS se va "�ntoarce" pentru a explora muchiile 
ramase las�nd v�rfurile ce au fost descoperite. Acest proces continua p�na c�nd
s-au descoperit toate nodurile care sunt accesibile din sursa originala.

Timpul total de functionare a procedurii BFS este O(E+V). Astfel, BFS se executa �n timp liniar.
*/
#define ALB 0
#define GRI 1
#define NEGRU 2
#define INF 0


typedef struct status{
	int cul;
	int par;
	int dist;
	int fin;
}STATUS;

typedef struct nod{
	int key;
	struct nod *urm;
}NOD;

typedef struct coada{
	int key;
	struct coada *urm;
}COADA;

COADA *queue;
NOD *edgsH[60001], *edgsT[60001];
//statusul unui nod
STATUS sts[60001];

int que[10001];
//Coada si indi***ANONIM***e cozii
int k;
int Q[60001];
int timp;
int lvl=1;
int operations;

//
void print(int lvl,int v,int u);
void DFS_VISIT(int u,int flag);
//

/*
	DFS - cautarea in adancime
	 - src : sursa grafului
	 - vertexs : numarul de noduri
*/
void DFS (int vertexs,int flag){
	
	//parcurgerea fiecarui nod, neluand in considerare sursa
	int u;
	for (u=1; u<=vertexs; u++){
			//marcarea nodurilor in ALB
			sts[u].cul=ALB;
			sts[u].par=NULL;
			sts[u].dist = sts[u].fin=INF;
	}
	timp = 0;
	for(u=1;u<=vertexs;u++){
		operations+=1;
		if(sts[u].cul==ALB){
			DFS_VISIT(u,flag);
		}
	}
}

void DFS_VISIT(int u,int flag){
	timp+=1;
	sts[u].dist = timp;
	sts[u].cul=GRI;
	NOD *aux;
	aux=edgsH[u];
	operations++;
	while(aux!=NULL){
		int v=aux->key;
		aux=aux->urm;
		operations+=3;
		if(sts[v].cul==ALB){
			sts[v].par=u;
			operations+=1;
			DFS_VISIT(v,flag);
		}
	}
	sts[u].cul = NEGRU;
	timp+=1;
	sts[u].fin = timp;
	operations+=2;
	if(flag!=0){
		for(int i=0;i<sts[u].par;i++)
			printf(" ");
		printf("(%d) %d\n",sts[u].par, u);
	}
}
/*
*/
bool ifExists (int x, int y){
	NOD *p;
	p = edgsH[x];
	while(p!=NULL){
		if(y==p->key)
			return true;	
		p=p->urm;
	}

	return false;

}

/*
	generate - generarea random a grafului
	 - vertexs : numarul de varfuri
	 - edges : numarul de edges
*/
void generate(int vertexs,int edges){
	int u,v;
	int i=1;
	//initializarea vectorului de muchii
	for(i=0;i<=vertexs;i++)
		edgsH[i]=edgsT[i]=NULL;
	i=1;
	//generarea efectiva a vectorului de muchii
	while (i<=edges){
		u=rand()%vertexs+1;
		v=rand()%vertexs+1;
		if (u!=v){
				if(edgsH[u]==NULL){
					edgsH[u]=(NOD *)malloc(sizeof(NOD *));
					edgsH[u]->key=v;
					edgsH[u]->urm=NULL;
					edgsT[u]=edgsH[u];

					if(!ifExists(v, u)){
						if(edgsH[v]==NULL){
							edgsH[v]=(NOD *)malloc(sizeof(NOD *));
							edgsH[v]->key=u;
							edgsH[v]->urm=NULL;
							edgsT[v]=edgsH[v];
						}else{
							NOD *p;
							p=(NOD *)malloc(sizeof(NOD *));
							p->key=u;
							p->urm=NULL;

							NOD *qu = edgsH[v];
							while(qu->urm!=NULL){
								qu=qu->urm;	
							}
							qu->urm = p;
							//edgsT[v]->urm=p;
							edgsT[v]=p;
						}
					}
					//i++;
				}else {
					if(!ifExists(u,v)){
						NOD *p;
							p=(NOD *)malloc(sizeof(NOD *));
							p->key=v;
							p->urm=NULL;

							NOD *qu = edgsH[u];
							while(qu->urm!=NULL){
								qu=qu->urm;	
							}
							qu->urm = p;
							edgsT[u]=p;
						if (!ifExists(v, u)){
							if(edgsH[v]!=NULL){
								NOD *p1;
								p1=(NOD *)malloc(sizeof(NOD *));
								p1->key=u;
								p1->urm=NULL;
								NOD *qu1 = edgsH[v];
								while(qu1->urm!=NULL){
									qu1=qu1->urm;	
								}
								qu1->urm = p1;
								edgsT[v]=p1;
							}else{
								edgsH[v]=(NOD *)malloc(sizeof(NOD *));
								edgsH[v]->key=u;
								edgsH[v]->urm=NULL;
								edgsT[v]=edgsH[v];
							}
						}
					}
				}
		}
		i++;
	}
}

void print(int lvl,int v,int u){
	for(int op=0; op<lvl; op++)
					printf(" ");

	printf("(%d) %d \n",u, v);
}

void afis(int vertexs){
	int i;
	NOD *p;
	for(i=1;i<=vertexs;i++)
	{
		printf("%d -> ",i);
		p=edgsH[i];
		while(p!=NULL)
		{
			printf("%d ",p->key);
			p=p->urm;
		}
		printf("\n");
	}
}

void main(){

	int nre, nrv;
	/*
	//TEST
	nrv=5;
	nre=10;
	srand(time(NULL));
	generate(nrv, nre);
	afis(nrv);
	DFS(nrv,1);
	*/
	
	//SIMULATION
	FILE *f;

	f=fopen ("DFS1.csv", "w");
	fprintf(f, "nre, operations\n");
	nrv=100;
	for (nre = 1000; nre<=5000; nre=nre+100)
	{
		operations=0;
		generate(nrv, nre);
		DFS(nrv,0);
		fprintf(f, "%d, %d\n", nre, operations);
	}
	fclose(f);

	f=fopen ("DFS2.csv", "w");
	fprintf(f, "nrv, operations\n");
	nre=9000;
	for (nrv = 110; nrv<=200; nrv=nrv+10)
	{
		operations=0;
		generate(nrv, nre);
		DFS(nrv,0);
		fprintf(f, "%d, %d\n", nrv, operations);
	}
	fclose(f);
	
}
