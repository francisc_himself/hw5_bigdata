#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

//***ANONIM*** Mariana ***ANONIM***, grupa ***GROUP_NUMBER***
//Prof. Vlad Baja
//Algoritmul Josephus. Presupunem ca n persoane sunt asezate in cerc si ca exista un numar intreg pozitiv m<=n. Incepand cu o persoana
//stabilita, se parcurge cercul si fiecare a m-a persoana se scoate din configuratie. Dupa ce persoana a fost eliminata, numararea 
//continua cu configuratia ramasa. Procesul de eliminare continua pana cand toate cele n perosane au fost scoase din configuratie.
//Algoritmul implementat mai jos se bazeaza pe construirea unui arbore binar de cautare care contine cheile de la 1 la n, care reprezinta
//persoanele. Dupa construirea arborelui selectam nodul care trebuie sters(tot al m-ulea) si il scoatem din arbore. Dupa apelarea 
//algoritmului "permutare" obtinem permutarea Josephus, care e exemplificata pe date de intrare mici, de exemplu permutare(8,3). Aceasta 
//va afisa 3,6,1,5,2,8,4,7. Dupa verificarea corectitudinii, variem n-ul pana la 10000, din 100 in 100, iar valoarea lui m, care trebuie 
//sa fie neconstanta, o alegem ca fiind n/5. Intr-o variabila contor am numarat asignarile si comparatiile care se fac pe durata intregii 
//executii a programului, complexitatea acestuia fiind de O(n*log2n). Dupa cum rezulta si din grafic, algoritmul e implementat eficient, 
//dependenta contorului de numarul n de persoane fiind una liniara.

typedef struct tip_nod{
		int key;
		int dim;
		struct tip_nod *p,*st,*dr;
	}NOD;
	
NOD *nod;
int contor;

//initializarea nodului radacina
void initializare(void)
{
	
	nod=(NOD*)malloc(sizeof(NOD));
	nod->key=0;
	nod->dim=0;
	nod->st=NULL;
	nod->dr=NULL;
	nod->p=NULL;
	contor++;
}

NOD *build(int stanga,int dreapta){
	if(stanga>dreapta) return nod;
	int m=(stanga+dreapta)/2;// alegem valoarea de mijloc
	contor++;
	NOD *x=(NOD*)malloc(sizeof(NOD));//alocam nod nou(x)
	x->key=m;
	x->p=nod;
	x->st=build(stanga,m-1);
	x->st->p=x;
	x->dr=build(m+1,dreapta);
	x->dr->p=x;
	x->dim=x->st->dim+x->dr->dim+1;//calculam dimensiunea
	return x;
}

NOD *so_selectie(NOD *x,int i)
{
	if(x==NULL || x->key==0) return NULL; 
	int r=x->st->dim+1;//numarul de noduri mai mici decat x->key
	
	if(i==r)
	{
		contor++;
		return x;
	}
	else if(i<r)
	       {
			contor++;
			return so_selectie(x->st,i);
			}
		else{
			contor++;
			return so_selectie(x->dr,i-r);
				}
}

NOD *minim(NOD *x)
{
	while(x->st!=nod)
	{
		contor++;
		x=x->st;
	}
	return x;
}

NOD *succesor(NOD *x)
{
	contor++;
	if(x->dr!=nod)
		return minim(x->dr);
	NOD *y=x->p;
	contor++;
	while(y!=nod && x==y->dr)
	{
		x=y;
		y=y->p;
		contor+=2;
	}
	return y;
}

void stergere(NOD **rad, NOD *z)
{
	
	NOD *y,*x;
	
	contor+=2;
	if(z->st==nod || z->dr==nod)
		y=z;
	else
		y=succesor(z);
	
	//toti de deasupra nodului ce se va sterge, sa piarda 1 la dimensiune
	x=y;
	contor++;
	while(x!=nod && x!=NULL)
	{
		contor+=2;
		x->dim=x->dim-1;
		x=x->p;
	}
	x=NULL;
	

	contor+=2;
	if(y->st!=NULL && y->st!=nod)
		x=y->st;
	else
		x=y->dr;
	

	contor++;
	if(x!=NULL && x!=nod){
		x->p=y->p;
		contor++;
	}

	contor++;
	if(y->p==NULL || y->p==nod)
	{
		*rad=x;
		++contor;
	}
	else if(y==y->p->st)
	       {
		        y->p->st=x;
		        contor+=2;
			}
		else
		   {
			y->p->dr=x;
			contor+=2;
			}

	contor++;
	if(y!=z)
	{
		z->key=y->key;
		++contor;
	}
	free(y);
}

void inord(NOD *x) 
{
	if(x==nod) return;
	inord(x->st);
	printf("%d ",x->key);
	inord(x->dr);
}

void permutare(int n,int m)
{

	NOD *x=build(1,n),*y;

	inord(x);printf("\n");
	
	int nr=n,s=m;
	while(x!=NULL && x!=nod){
		s=s%nr;
		if(s==0) 
			 s=nr;
		y=so_selectie(x,s);
		contor+=3;
		printf("%d ",y->key);
		stergere(&x,y);
		s+=m-1;
		--nr;
		contor+=3;
	}
	printf("\n");

}



int main(void){
	contor=0;
	initializare();
  //  permutare(8,3); //pentru verificarea algoritmului
    
	
	FILE *rez=fopen("josephus.csv","w");
    int n;

   initializare();
	for( n=100;n<=10000;n+=100)
	{
		contor=0;
		permutare(n,n/5);
	
	     fprintf(rez,"%d %d\n",n,contor);
	}

	fclose(rez);
	getch();
	return 0;
}

