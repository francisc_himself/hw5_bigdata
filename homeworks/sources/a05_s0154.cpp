#include <cstdio>
#include <cstdlib>
#include <conio.h>

#define DIM ((double)10007.0)
#define HASH_DIM (int)DIM
#define c1 (double)1
#define c2 (double)1/2

//does not support 0's, they mean EMPTY
int hash[HASH_DIM], hash_size;
int last_element;
int op;
int h(int k, int i)
{
	return (k%HASH_DIM +int ( 3*i+5*i*i ))%HASH_DIM;
}

int insert(int k){
//	printf("inserting %d\n", k);
	int i = 0, j;
	do{
		j = h(k, i);
		if(hash[j] == 0)
		{
			hash[j] = k;
			++hash_size;
			return j;
		}
		else
			++i;
	}while(i != HASH_DIM);
	return -1;
}

int search(int k){
	int i = 0, j;
	do
	{
		++op;
		j = h(k ,i);
		if( hash[j] == k){
			return j;
		}
		++i;
	}while(!(hash[j] == 0 || i == hash_size));
	return 0;
}

int next_rand()
{
	return (rand() << 16) | rand();
	
}

void clean_hash(){
	hash_size = 0;
	for(int i = 0; i < HASH_DIM; ++i)
		hash[i] = 0;
}

void print_hash(){
	printf("print hash: ");
	for(int i = 0; i < HASH_DIM; ++i)
		printf("%d ", hash[i]);
	printf("\n");
	getch();
}

int main()
{
	double fill_factor = 0;
	FILE*f = fopen("Result.csv", "w");
	fprintf(f, "Filling Factor,Avg Effort Found,Max Effort found,Avg Effort not found,Max Effort not found\n");
	for(int step = 0; step < 5; ++step)
	{
		clean_hash();
		switch(step)
		{
		case 0:
			fill_factor = 0.8;
			break;
		case 1:
			fill_factor = 0.85;
			break;
		case 2:
			fill_factor = 0.9;
			break;
		case 3:
			fill_factor = 0.95;
			break;
		case 4:
			fill_factor = 0.99;
			break;
		}
		//init
		hash_size = 0;
		last_element = 0;
		
		//inserting
		srand(step);
		while(((double)(hash_size))/DIM <= fill_factor) {	
			
			int index = insert(rand()%30000+1);
			if (index == -1){
				printf("ERROR at insert!\n");
				getch();
			}
	//		printf("hash size=%d\nfill=%lf\n",hash_size, ((double)(hash_size))/DIM );
		}
		printf("hash size = %d\n",hash_size);
		//searching successfully
		srand(step);
		double avgEff = 0, maxEff = 0;
		for(int q = 1; q <= 1500; ++q){
			op = 0;
			int k;
			do{
				k = rand()%hash_size;
			}while(hash[k] == 0);

			if(search(hash[k])){
	//			printf("search success ... OK!\n");
			}			
			else
 				printf("search failed! ... ERROR!\n");

			avgEff += op;
			if(maxEff < op)
				maxEff = op;
		} 
		avgEff /= 1500;
		//searching not successfully 
		double avgEffNot = 0, maxEffNot = 0;
		for(int q = 1; q <= 1500; ++q){
			op = 0;
			if(!search(rand()%30000+30000)){
	//			printf("search failed! ... OK!\n");
			}
			else
				printf("search success ... ERROR!\n");

			avgEffNot += op;
			if(maxEffNot < op)
				maxEffNot = op;
		} 

		avgEffNot /= 1500;

		fprintf(f, "%lf,%lf,%lf,%lf,%lf\n",fill_factor, avgEff, maxEff, avgEffNot, maxEffNot);
	}
	printf("Done!\n");
	fclose(f);
	getch();
	return 0;
}