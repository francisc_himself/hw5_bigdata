
#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>

#ifndef MAX
    #define MAX(a,b) ((a) < (b) ? (a) : (b))
#endif

using namespace std;

typedef struct NOD{
	int inf;
	int inaltime;
	struct NOD *stg;
	struct NOD *dr;

} nod;

nod ****ANONIM***;
int assig=0;
int	comp=0;

nod *stergeArbore(nod *t)
{
	if(t!=NULL)
	{
		stergeArbore(t->stg);
		stergeArbore(t->dr);
		free(t);
		t = NULL;
	}
	return NULL;
}

int inaltime(nod *t)
{
	if(t == NULL)
		return -1;
	else
		return t->inaltime;
}


nod *roteste_stanga(nod *n2)
{
	nod *n1;

	n1 = n2->stg;
	n2->stg = n1->dr;
	n1->dr = n2;

	n2->inaltime = MAX(inaltime(n2->stg), inaltime(n2->dr))+1;
	n1->inaltime = MAX(inaltime(n1->stg), inaltime(n1->dr))+1;
	
	return n1;
}


nod *roteste_dreapta(nod *n2)
{
	nod *n1;

	n1 = n2->dr;
	n2->dr = n1->stg;
	n1->stg = n2;

	n2->inaltime = MAX(inaltime(n2->stg), inaltime(n2->dr))+1;
	n1->inaltime = MAX(inaltime(n1->stg), inaltime(n1->dr))+1;
	
	return n1;
}

nod *roteste_dublu_stanga(nod *n3)
{
	n3->stg=roteste_dreapta(n3->stg);
	return roteste_stanga(n3);
}


nod *roteste_dublu_dreapta(nod *n3)
{
	n3->stg=roteste_stanga(n3->stg);
	return roteste_dreapta(n3);
}



nod *search(int inf, nod *t)
{
	if(t==NULL)
		return NULL;

	if(inf<t->inf)
		return search(inf, t->stg);
	else if(inf > t->inf)
		return search(inf, t->dr);
	else
		return t;
}

nod *gaseste_min(nod *t)
{
	if(t==NULL)
	{
		return NULL;
	}
	else if(t->stg==NULL)
	{
		return t;
	}
	else
	{
		return gaseste_min(t->stg);
	}
}

nod *insert(int inf, nod *t)
{
	nod *nodNou;

	if(t==NULL)
	{
		nodNou = (nod*) malloc(sizeof(nod));
		if(nodNou == NULL)
			return t;

		nodNou->inf=inf;
		nodNou->inaltime=0;
		nodNou->stg = nodNou->dr = NULL;
		return nodNou;
	}
	else if(inf < t->inf)
	{
		t->stg = insert(inf, t->stg);
		if(inaltime(t->stg)- inaltime(t->dr)==2)
		{
			if(inf < t->stg->inf)
			{
				t = roteste_stanga(t);
			}
			else
			{
				t = roteste_dublu_stanga(t);
			}
		}
	}
	else if(inf > t->inf)
	{
		t->dr = insert(inf, t->dr);
		if(inaltime(t->dr)- inaltime(t->stg)==2)
		{
			if(inf>t->dr->inf)
			{
				t = roteste_dreapta(t);
			}
			else
			{
				 roteste_dublu_dreapta(t);
			}
		}
	}

	t->inaltime = MAX(inaltime(t->stg), inaltime(t->dr))+1;
	return t;
}

nod *stergeNod(int inf, nod *t)
{
	nod *n;
	nod *nod_tmp;

	if(t==NULL)
	{
													comp++;
		return NULL;
	}
													comp++;
	if(inf < t->inf)
	{
													assig++;
		t->stg = stergeNod(inf, t->stg);
		
		if(inaltime(t->dr)  - inaltime(t->stg) >=2)
		{
													comp++;
			if(t->stg && (inf < t->stg->inf))
			{
													assig++;
				t = roteste_dublu_dreapta(t);
			}
			else
			{
													assig++;
				t = roteste_dreapta(t);
			}
		}
	}

	else if(inf > t->inf)
	{
														assig++;
		t->dr = stergeNod(inf, t->dr);
														comp++;
		if(inaltime(t->stg)- inaltime(t->dr)>=2)
		{
			if(t->dr && (inf > t->dr->inf))
			{
														assig++;
				t = roteste_dublu_stanga(t);
			}
			else
			{
														assig++;	
				t = roteste_stanga(t);
			}
		}
	}
												
	else if(t->stg && t->dr)
	{
		nod_tmp = gaseste_min(t->dr);
		t->inf = nod_tmp->inf;
		t->dr = stergeNod(t->inf, t->dr);
	}
	else
	{
		nod_tmp = t;
		if(t->stg == NULL)
			t = t->dr;
		else if (t->dr == NULL)
			t = t->stg;
		free(nod_tmp);
		nod_tmp = NULL;
	}

	return t;
}


nod *initArbore(int n)
{
	for(int i=1;i<=n;i++)
	{
		***ANONIM*** = insert(i, ***ANONIM***);
	}
	return ***ANONIM***;
}

void parcurgereInordine(nod *t, int nivel)
{
	if(t!=NULL)
	{
		parcurgereInordine(t->stg, nivel+1);
		for(int i=0;i<nivel;i++)
			printf("\t");
		printf("%d \n",t->inf);
		parcurgereInordine(t->dr,nivel+1);
	}
}

nod *parcurgereInordineSearch(nod *t, int nr, int k)
{
	if(t!=NULL)
	{
		if(nr!=k)
		{
			parcurgereInordineSearch(t->stg, nr+1, k);
			//printf("%d",t->inf);
			parcurgereInordineSearch(t->dr, nr+1, k);
		}
		else
			return t;
	}
}

nod *searchEficient(nod *t, int key)
{
	if(t==NULL)
		return NULL;
	if(key == t->inf)
		return t;
	else
		if(key<t->inf)
			searchEficient(t->stg,key);
		else
			searchEficient(t->dr,key);
}


nod *os_select(nod *x, int i)
{
	int r=1;							comp++;
	if(x->stg!=NULL)
	{
		assig++;
		r = x->stg->inaltime+1;
	}
										comp++;
	if(i==r)
	{
		//printf("os_select: return %d \n",x->inf);
		return x;
	}
	else
	{
										comp++;
		if(i<r)
			return os_select(x->stg,i);
		else
			return os_select(x->dr,i-r);
	}
}


void josephus(int n, int m)
{
	nod *sterge;
	int j=1;
	
	for(int k=n;k>1;k--)
	{
											assig++;
		j=((j+m-2)%k)+1;
		//j=k;
		printf("J:am de sters %d\n", j);
		sterge = os_select(***ANONIM***,j);				
		printf("\t \t \t cu informatia: %d \n ",sterge->inf);
		***ANONIM***=stergeNod(sterge->inf, ***ANONIM***);			
	}
}



/*
void josephus(int n, int m, int a[])
{
	int key=1;
	nod *sterge;
	int j=1;
	for(int k=n; k>1;k--)
	{
		j=((j+m-2)%k)+1;
		//j=k;
		//printf("J:am de sters %d\n", j);
		sterge = searchEficient(***ANONIM***,key);					
		//printf("\t \t \t cu informatia: %d \n ",sterge->inf);
		***ANONIM***=stergeNod(sterge->inf, ***ANONIM***);								

		a[key]=0;

		int acc=0;
		int parcurge=key;
		while(acc<m)
		{
			if(a[parcurge]<0)
				acc++;
			parcurge++;
			if(parcurge>n)
				parcurge=1;
		}

		while(a[parcurge]==0)
		{
			parcurge++;
			if(parcurge>n)
				parcurge=1;
		}
		key = parcurge;
	}
}*/


int main()
{
	/*for(int i=1;i<10;i++)
		***ANONIM*** = insert(i, ***ANONIM***);*/		// mere, sper

	//init arbore
	FILE *fis;

	fis=fopen("IO.csv","w");

	int a[10001];

	//luam exemplul de 7-3

	***ANONIM***=stergeArbore(***ANONIM***);
	***ANONIM*** =initArbore(7);
	josephus(7, 3);



	/*
	fprintf(fis,"atrib,comp,suma\n",assig,comp, assig+comp);
	for(int i=10;i<=1000;i=i+10)
	{
		for(int j=1;j<=i;j++)
			a[j]=-1;
		***ANONIM***=stergeArbore(***ANONIM***);
		assig=0;
		comp=0;
		***ANONIM*** =initArbore(i);
		//josephus(i, i/2,a);
		josephus(i, i/2);
		fprintf(fis,"%d,%d,%d\n",assig,comp, assig+comp);
	}*/
	
	fclose(fis);
	
	return 0;
}









//-----------------------------------------------printing
/*void padding ( char ch, int n )
{
  int i;
  for ( i = 0; i < n; i++ )
    printf( "%c",ch );
}

void structure (nod *t, int level )
{
  int i;
  if ( t == NULL ) {
    padding ( '\t', level );
    printf ( "~" );
  }
  else {
    structure ( t->dr, level + 1 );
    padding ( '\t', level );
    printf ( "%d\n", ***ANONIM***->inf );
    structure ( ***ANONIM***->stg, level + 1 );
  }
}*/
//-----------------------------------------------final printing



/*
int size(nod ****ANONIM***, int s);
void insert(nod ****ANONIM***, int n);
nod* os_select(nod *x, int i);
nod* largest(nod ****ANONIM***);
nod* os_delete(nod ****ANONIM***, int x);
nod* deleteThis(nod ****ANONIM***, int x);
void josephus(int n, int m);

int main()
{
	josephus(10,5);
	return 0;
}




void insert(nod ****ANONIM***, int n)
{
	nod *y=NULL;
	nod *x = ***ANONIM***;
	nod *z;
	int i;
	for(i=0;i<n;i++)
	{
		z=(nod*)malloc(sizeof(nod));
		z->inf=i;
		z->stg=NULL;
		z->dr=NULL;
		while (x!=NULL)
		{
			y=x;
			if(z->inf < x->inf)
				x = x->stg;
			else
				x = x->dr;
		}
		if(y=NULL)
			***ANONIM***=z;
		else
			if(z->inf<y->inf)
				y->stg=z;
			else
				y->dr=z;
	}
}*/

/*void insert(nod ****ANONIM***, int val)
{
	nod *temp = NULL;
	if(!(****ANONIM***))
	{
		temp = (nod*)malloc(sizeof(nod));
		temp->stg=temp->dr=NULL;
		temp->inf=val;
		****ANONIM***=temp;
		return;
	}
	if(val < (****ANONIM***)->inf)
		insert(&(****ANONIM***)->stg,val);
	else if(val > (****ANONIM***)->inf)
		insert(&(****ANONIM***)->dr,val);
}*/
/*

int size(nod ****ANONIM***, int s)
{
	if(***ANONIM***!=NULL)
	{
		s++;
		size(***ANONIM***->stg,s);
		s++;
		size(***ANONIM***->dr,s);
	}
	return s;
}

nod* os_select(nod *x, int i)
{
	int r = size(x->stg,1);
	if(r==i)
		return x;
	else
		if(i<r)
			return os_select(x->stg,i);
		else
			return os_select(x->stg,i-r);

}

nod* largest(nod ****ANONIM***)
{
	if(***ANONIM***->dr==NULL)
		return ***ANONIM***;
	largest(***ANONIM***->dr);
	return NULL;
}

nod* os_delete(nod ****ANONIM***, int x)
{
	if(***ANONIM***==NULL)
		return NULL;
	if(x==***ANONIM***->inf)
		deleteThis(***ANONIM***,x);
	if(x<***ANONIM***->inf)
		***ANONIM***->stg=os_delete(***ANONIM***->stg,x);

	else
		***ANONIM***->dr=os_delete(***ANONIM***->dr,x);
}

nod* deleteThis(nod ****ANONIM***, int x)
{
	nod *p;
	if((***ANONIM***->stg!=NULL)&&(***ANONIM***->dr!=NULL))
	{
		p=largest(***ANONIM***->stg);
		***ANONIM***->inf=p->inf;
		***ANONIM***->stg=os_delete(***ANONIM***->stg,p->inf);
		return ***ANONIM***;
	}
	if((***ANONIM***->stg==NULL)&&(***ANONIM***->dr!=NULL))
		return ***ANONIM***->dr;
	else
		return NULL;
}



void josephus(int n, int m)
{
	nod ****ANONIM***;
	***ANONIM***=NULL;
	nod *x;
	//construim arborele
	insert(***ANONIM***,n);
	//----------------
	int j=1;
	for(int k=n;k>1;k--)
	{
		j=((j+m-2)%k)+1;
		
		x=os_select(***ANONIM***,j);
		os_delete(***ANONIM***,x->inf);
	}
}*/