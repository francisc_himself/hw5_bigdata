#include <conio.h>
#include <stdio.h>
#include <string.h>
#include "Profiler.h"

Profiler profiler("BFS");

typedef struct vert {
	int key;
	int dist;
	char color[10];
	struct vert **c;
	int noCh;
	struct vert *p;
} Vertex;


typedef struct node {
	Vertex *v;
	struct node *next;
} List;

typedef struct queue {
	Vertex *q[200];
	int rear;
	int front;
} Queue;

typedef struct node2 {
	int key;
	struct node2 *c,*s; 
} Tree;

void enqueue(Queue *que,Vertex *s) {
	(*que).q[(*que).rear]=s;
	(*que).rear++;
}

Vertex *dequeue(Queue *que) {
	(*que).front++;
	return (*que).q[(*que).front-1];
}


void BFS_tree(Vertex *t1,Tree *t2,int n) {
	int i;
	if (n==0) {
		t2->c=NULL;
		t2->s=NULL;
	}
	if (n==1) {
		t2->s=NULL;
	}
	for (i=0;i<n;i++) {
		if (i==0) {
			t2->c=(Tree *)malloc(sizeof(Tree));
			t2->c->key=t1->c[0]->key;
			BFS_tree(t1->c[0],t2->c,t1->c[0]->noCh);
			t2->s=NULL;
			t2=t2->c;	
		} else {
			t2->s=(Tree *)malloc(sizeof(Tree));
			t2->s->key=t1->c[i]->key;
			t2=t2->s;
			BFS_tree(t1->c[i],t2,t1->c[i]->noCh);
		}
	}
}


void BFS(List *adj[],Vertex *s,int n,int *op) {
	Queue que;
	Vertex *u;
	List *p;

	for (int i=0;i<n;i++) {
		strcpy(adj[i]->v->color,"white");
		adj[i]->v->dist=MAXINT;
		adj[i]->v->p=NULL;
		adj[i]->v->noCh=0;
		adj[i]->v->c=NULL;
		*op=*op+2;
	}
	strcpy(s->color,"gray");
	s->dist=0;

	que.front=0;
	que.rear=0;

	enqueue(&que,s);
	*op=*op+5;
	while (que.front != que.rear) {
		u=dequeue(&que);
		//printf("%d ",u->key);
		p=adj[u->key];
		while (p != NULL) {
			*op=*op+3;
			if (strcmp(p->v->color,"white")==0) {
				strcpy(p->v->color,"gray");
				p->v->dist=u->dist+1;
				u->noCh++;
				p->v->p=u;
				u->c=(Vertex **)realloc(u->c,u->noCh*sizeof(Vertex *));
				u->c[u->noCh-1]=p->v;
				enqueue(&que,p->v);
				*op=*op+3;
			}
			p=p->next;
		}
		strcpy(u->color,"black");
		*op=*op+5;
	}
}

void preorder(Tree *p, int nivel) {
	if (p!=NULL) {		
		for(int i=0;i<nivel;i++)
			printf("   ");
		printf("%d\n\n",p->key);
		preorder(p->c,nivel+1);
		preorder(p->s,nivel);
	}
}


int main() {
	List *adj[200],*p;
	Tree *root;
	Vertex *v1,*v2;
	int noEdges=0,ok=1,op=0;
/*
	srand(time(NULL));

	for (int i=0;i<10;i++) {
		adj[i]=(List *)malloc(sizeof(List));
		adj[i]->v=(Vertex*)malloc(sizeof(Vertex));
		adj[i]->v->key=i;
		adj[i]->next=NULL;
	}

	while (noEdges < 20) {
		v1=adj[rand()%10]->v;
		v2=adj[rand()%10]->v;	
		p=adj[v1->key];
		ok=1;
		while (p->next!=NULL) {
			if (p->v->key==v2->key) {
				ok=0;
				break;
			}
			p=p->next;
		}
		if (p->v->key==v2->key) 
			ok=0;
		if (ok) {
			p->next=(List *)malloc(sizeof(List));
			p->next->v=v2;
			p->next->next=NULL;
			noEdges++;
		}
	}


	printf("\nAdjacency list: \n");
	for (int i=0;i<10;i++) {
		p=adj[i];
		while (p!=NULL) {
			printf("%d ",p->v->key);
			p=p->next;
		}
		printf("\n");
	}

	root=(Tree*)malloc(sizeof(Tree));
	for (int i=0;i<10;i++) {
		printf("\nBFS traversal: ");

		BFS(adj,adj[i]->v,10,&op);

		printf("\n\n");

		root->key=adj[i]->v->key;
		root->s=NULL;
		BFS_tree(adj[i]->v,root,adj[i]->v->noCh);
		preorder(root,0);
	} */

	srand(time(NULL));

	for (int n=1000;n<=5000;n+=100) {
		op=0;
		noEdges=0;
		for (int i=0;i<100;i++) {
			adj[i]=(List *)malloc(sizeof(List));
			adj[i]->v=(Vertex*)malloc(sizeof(Vertex));
			adj[i]->v->key=i;
			adj[i]->next=NULL;
		}

		while (noEdges < n) {
			v1=adj[rand()%100]->v;
			v2=adj[rand()%100]->v;			
			p=adj[v1->key];
			ok=1;
			while (p->next!=NULL) {
				if (p->v->key==v2->key) {
					ok=0;
					break;
				}
				p=p->next;
			}
			if (p->v->key==v2->key) 
				ok=0;
			if (ok) {
				p->next=(List *)malloc(sizeof(List));
				p->next->v=v2;
				p->next->next=NULL;
				noEdges++;
			}
		}

		//for (int i=0;i<100;i++)
			BFS(adj,adj[50]->v,100,&op);

		profiler.countOperation("BFS_vary_edges",n,op);
	}
	//profiler.showReport();

	
	for (int n=100;n<=200;n+=10) {
		op=0;
		noEdges=0;
		for (int i=0;i<n;i++) {
			adj[i]=(List *)malloc(sizeof(List));
			adj[i]->v=(Vertex*)malloc(sizeof(Vertex));
			adj[i]->v->key=i;
			adj[i]->next=NULL;
		}

		while (noEdges <= 9000) {
			v1=adj[rand()%n]->v;
			v2=adj[rand()%n]->v;
			p=adj[v1->key];
			ok=1;
			while (p->next!=NULL) {
				if (p->v->key==v2->key) {
					ok=0;
					break;
				}
				p=p->next;
			}
			if (p->v->key==v2->key) 
				ok=0;
			if (ok) {
				p->next=(List *)malloc(sizeof(List));
				p->next->v=v2;
				p->next->next=NULL;
				noEdges++;
			}
		}

	//	for (int i=0;i<n;i++)
			BFS(adj,adj[50]->v,n,&op);

		profiler.countOperation("BFS_vary_vertices",n,op);
	}

	profiler.showReport(); 
	getch();
	return 0;
}