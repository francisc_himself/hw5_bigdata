/**
Student: ***ANONIM*** Diana ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: Search Operation in Hash Tables - Open Addressing with Quadratic Probing
Comments:
	
**/

#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>

#define m 9973
int n,T[m],b[10001],nr=0,minnf=0,minf=0,c1=2,c2=3;

int h2(int k)
{
	return k%m;
}
long h(int k, int i)
{
	return (h2(k)+c1*i+c2*i*i)%m;
}
long hash_search(int T[], int k)
{
	int i=0;
	long j;
	//nrnf++;
	do
	{	
		j=h(k,i);
		nr++;
		if (T[j]==k)
			
			return j;
		i++;
	}
	while ((T[j]!=0)&&(i<m));
	//nrnf++;
}
int	hash_insert(int T[],int k)
{
	int i=0;
	do
	{
		int j=h(k,i);
		if (T[j]==0)
		{
			T[j]=k;
			//printf("%d\n",j);
			return j;
		}
		else i++;
	}
	while (i!=m);
	printf("Hash table overflow");
}

int main()
{
	double a[5];
	FILE *f;
	f=fopen("hash.txt","w");
	srand(time(NULL));
	a[0]=0.8;
	a[1]=0.85;
	a[2]=0.9;
	a[3]=0.95;
	a[4]=0.99;
	for (int i=0;i<m;i++)
		T[i]=0;
	
	fprintf(f,"a     avg_f   max_f   avg_nf  max_nf\n");
	for (int i=0;i<5;i++)
	{
		fprintf(f,"%.2f   ",a[i]);
		n=(int)m*a[i];
		for (int i=0;i<m;i++)
			T[i]=0;
		for (int j=0;j<n;j++)
		{
			int aux=rand()*rand()%100000;//genereaza <1000
			hash_insert(T,aux);
		}
		for (int x=0;x<=10000;x++)
			b[x]=0;
		int j=0;
		while (j<1500)
			{
				int x=rand()%m;
				if (T[x]!=0)
					{
						b[j]=T[x];
						j++;
					}
			}
		minf=0;int avg1=0;
		for (int x=0;x<1500;x++)//1500
		{
			//found
			nr=0;
			hash_search(T,b[x]);
			avg1=avg1+nr;
			if (nr>minf)
				minf=nr;
		}
		avg1=avg1/1500;
		minnf=0;int avg2=0;
		for (int x=0;x<1500;x++)
		{	
			//not found
			nr=0;
			int valnotfound=rand()+100000; //genereaza intre 1000 si 2000
			hash_search(T,valnotfound);
			avg2=avg2+nr;
			if (nr>minnf)
				minnf=nr;
		}
		avg2=avg2/1500;
		fprintf(f,"%d       %d       %d       %d\n",avg1,minf,avg2,minnf);
	}
	fclose(f);
}