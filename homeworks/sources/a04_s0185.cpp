#include <stdio.h>;
#include <stdlib.h>
#include <fstream>;
#include <iostream>;

using namespace std;

ofstream g("kconst.csv");
ofstream h("knecinst.csv");

int n,k,sizeh,sizec;

int sizeA[10001],c[10001];
int nr;

typedef struct elem{
	int key;
	int lista;
}Elem;

typedef struct list{
	Elem e;
	struct list *next;
}List;

List *a[10001];
Elem H[10001];

void Heapify(Elem H[],int i)
{
    int l;
    int stg = 2*i;
    int drp = 2*i+1;

	if((stg<=sizeh) && (H[stg].key<H[i].key))
		l = stg;
	else
		l = i;
    
	if((drp<=sizeh) && (H[drp].key<H[l].key))
		l = drp;
    
	nr+=2;

    if(l != i)
	{
		Elem aux = H[i];
		H[i] = H[l];
        H[l] = aux;
		nr = nr+3;
        Heapify(H,l);
    }
}

void buildHeap(Elem H[]){
    int i;
	for(i=sizeh/2;i>=1;i--){
        Heapify(H,i);
    }
}

void afis(int a[],int n)
{
	int i;
	for(i=1;i<=n; i++)
	cout<<a[i]<<" ";
	cout<<endl;
}

void afisElem(Elem a[],int n)
{
	for(int i=1;i<=n; i++)
	cout<<a[i].key<<" "<<a[i].lista<<" ";
	cout<<endl;
}

void afisList(int i,int n)
{
	int j=1;
	List *p = a[i];

	while(j<=sizeA[i])
		{
		cout<<p->e.key;
			p=p->next;
			j++;
		}
	free(p);
	
}



void genListe(int n,int k)
{
	int i,j;
	
	for(i=1;i<k;i++)
	{
		nr++;
		sizeA[i] = n/k;
		a[i] = NULL;
	}

	a[k] = NULL;
	nr++;
	sizeA[k] = n/k +n%k;
	for(i=1;i<=k;i++)
	{
		int z;
		nr++;
		List *p, *q;
		List *incep;
		p = (List *)malloc(sizeof(List));
		z = rand()%100;
		p->e.key = z;
		p->e.lista = i;
		p->next = NULL;
		incep = p;
		
		for(j=2;j<=sizeA[i];j++)
		{
			q = (List *)malloc(sizeof(List));
			z = z + rand()%100;
			q->e.key = z;
			nr++;
			q->e.lista = i;
			q->next = NULL;
			p->next = q;
			p = p->next;
			q=q->next;
			free(q);	
		}
		
		p->next = NULL;
		a[i] = incep;
		p=p->next;
		nr++;
		incep = p;
		free(p);
		//afisList(i,sizeA[i]);
		free(incep);
	}
}


Elem deletefromlist(int i)
{
	Elem el;
	if(sizeA[i]!=0)
	{
		nr+=2;
		el = a[i]->e;
		sizeA[i]--;
		a[i] = a[i]->next;
	}
	else
	
	{
		nr++;
		el.key = 0;
		el.lista = 0;
	
	}
	return el;
}



Elem Pop(Elem H[])
{
	Elem x = H[1];
	nr+=2;
	H[1] = H[sizeh];
	sizeh--;
	//afisElem(H,sizeh);
	Heapify(H,1);
	//afisElem(H,sizeh);
	return x;
}


void Push(Elem H[],Elem x)
{
	sizeh++;
	H[sizeh] = x;
	int i = sizeh;
	nr+=2;
	while((i>1) && (H[i/2].key>H[i].key))
	{
		Elem aux;
		nr+=3;
		aux= H[i/2];
		H[i/2] = H[i];
		H[i] = aux;
		i = i/2;
	}
}

void writel(int c[],int x)
{
	sizec++;
	nr++;
	c[sizec] = x;
}


void interclasare(int k,int c[],Elem b[])
{
	sizeh = 0;
	sizec = 0;
	for(int i=1;i<=n;i++){
		b[i].key = 0;
		b[i].lista = 0;
		
		
	}
	for(int i=1;i<=k;i++)
	{
		Elem x = deletefromlist(i);
		nr++;
		if(x.lista != 0)
			Push(b,x);
		
	}
	
	afisElem(b,sizeh);
	while(sizeh>0)
	{
		Elem x;
		x = Pop(H);
		writel(c,x.key);
		//afis(c,sizec);
		
		x = deletefromlist(x.lista);
		if (x.lista != 0)
		{
			Push(b,x);
			//afisElem(b,sizeh);
		}
	}
}


void main(){

	int k1,k2,k3;
	k1 = 5;
	k2 = 10;
	k3 = 100;

	/*g<<"n;"<<"atr+comp -> k1;"<<"atr+comp -> k2;"<<"atr+comp -> k3;"<<endl;

	for(n = 100; n <= 10000; n=n+100)
		{
		nr = 0;
			genListe(n,k1);
			interclasare(k1,c,H);
			g<<n<<";"<<nr; 
			
		
		nr = 0;

		
			genListe(n,k2);
			interclasare(k2,c,H);
			g<<";"<<nr;
		
		nr = 0;
	
		
			genListe(n,k3);
			interclasare(k3,c,H);
			g<<";"<<nr;
		
		nr = 0;
		g<<endl;	
	}
	h<<"n;"<<"operatii"<<endl;
	for(k=10; k<=500; k=k+10)
	{
		nr = 0;
		genListe(10000,k);
		h<<10000<<";"<<nr<<endl;
	}
	*/
	
	genListe(10,3);
	interclasare(3,c,H);
	afis(c,sizec);

}

