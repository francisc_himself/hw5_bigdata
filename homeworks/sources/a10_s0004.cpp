/*
Assign 10
***ANONIM*** ***ANONIM*** Razvan
Grupa ***GROUP_NUMBER***

Cerinta: implementarea algoritmului DFS, precum si realizarea clasificarii muchiilor si 
testarea respectiv executarea sortarii topologice

Am creat o structura de tip Nod, care are toate proprietatile necesare pentru parcurgerea
grafului cu ajutorul algoritmului descris in cartea lui Cormen. Fiecare camp e descris prin comentariu.
Am creat o lista de noduri, si pentru fiecare nod avem lista cu vecinii sai.

Procedura principala DFS o apeleaza pe cea denumita DFS_visit, pentru a parcurge subarborele nodului 
transmis ca parametru. 

In plus, am mai creat si o functie care generaza muchii in ordine aleatoare, si una ce le afiseaza.

Sortarea topologica se poate face doar daca graful e aciclic, iar asta o verificam in DFS_visit.
Tot in DFS_visit vom face si sortarea propriu-zisa, in caz ca e posibil.

Timpul de executie la DFS_visit : O(V+E)
*/

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>

typedef struct _nod {
	_nod *next;	//lista vecini
	int val;
	char culoare;
	int p;	//parinte
	int d;	//momentul cand nodul devine gri = timp de descoperire
	int f;	//momentul cand nodul devine negru = timp de terminare
}Nod;

Nod *first[10000], *last[10000];	//pt liste
Nod *nod[10000];	//nodurile
Nod *list[10000];
int OP, timp;
int aciclic=1;	//verif daca e aciclic sau nu
int list_contor=0;


void DFS_Visit(int s)
{
	Nod *x;
	int t;

	nod[s]->culoare = 'g';		//facem nodul sa fie gri = e atins, dar mai are venici nevizitati
	nod[s]->d = timp;		//timp de descoperire
	timp++;
	printf("%d ", s);
	
	OP++;
	x = first[s];
	
	while(x!=NULL)		//parcurgem vecinii nodului, impreuna cu toti subarborii
	{
		t = x->val;
		x = x->next;
		OP++;

		if (nod[t]->culoare=='a')	//daca gasim un nod alb, ii atribuim parintele si il parcurgem
		{
			nod[t]->p = s;
			OP++;
			DFS_Visit(t);
		}
		else
		{
			if (nod[t]->culoare=='g')	//daca gasim un nod gri => a fost descoperit deja
				aciclic=0;				//inseamna ca graful are cicluri
		}
	}

	nod[s]->culoare='n';		//facem nodul negru dupa ce am parcurs toti fiii
	nod[s]->f = timp;		//atribuim timpul final
	timp++;

	list[list_contor]=nod[s];	//adaugam nodul in lista sortarii topologice
	list[list_contor]->val = s;
	list_contor++;

}


void DFS(int nrNoduri)
{
	//initializari
	timp=0;
	OP=0;
	for(int s=1; s<=nrNoduri; s++)
	{
		nod[s]=(Nod *)malloc(sizeof(Nod));	//la inceput , toate nodurile sunt albe
		nod[s]->culoare='a';
		nod[s]->p=0;	//timpii si parintii sunt 0
		nod[s]->f=0;
		nod[s]->d=0;
	}

	//DFS propriu-zis
	for (int s=1; s<=nrNoduri; s++)
	{
		nod[s]->culoare = 'a';		//facem toate nodurile sa fie albe
		nod[s]->p=0;
	}

	for(int s=1;s<=nrNoduri;s++)
	{
		OP++;
		if (nod[s]->culoare=='a')	//daca nodul e alb, adica nu a fost atins inca 
			DFS_Visit(s);		//atunci apelam DFS visit
	}
}



void lista_de_adiac(int nrNoduri, int nrMuchii)
{
	int u,v;
	int i;

	for(i=0;i<=nrNoduri;i++)
		first[i]=last[i]=NULL;	//lista de vecini
	i=1;

	while(i<=nrMuchii)
	{
		u=rand()%nrNoduri+1;	//generare valori pt muchii
		v=rand()%nrNoduri+1;

		if (u!=v)
		{
			if (first[u]==NULL)		//v este adaugat in lista lui u
			{
				first[u] = (Nod *)malloc(sizeof(Nod));
				first[u]->val = v;
				first[u]->next=NULL;
				last[u]=first[u];
				i++;
			}
			else
			{
				bool g=true;	//daca u are deja vecini
				Nod *p;
				p=first[u];
				while(p!=NULL)	//ajugem la ultimul vecin
					{
						if (v==p->val)
							g=false;
						p=p->next;
					}

				if (g==true)	//daca v nu a fost gasit, e adaugat
				{
					Nod *q;
					q=(Nod *)malloc(sizeof(Nod));
					q->val=v;
					q->next=NULL;
					last[u]->next=q;
					last[u]=q;
					i++;
				}
			}
		}

	}
}


//afisare liste de adiacente
void afisare(int nrNoduri)
{
	Nod *q;
	
	for(int i=1;i<=nrNoduri;i++)
	{
		printf("%d : ",i);
		q=first[i];
		while (q!=NULL)
		{
			printf("%d, ",q->val);
			q=q->next;
		}
	printf("\n");
	}
}



int main()
{
	int nrNoduri, nrMuchii;
	int nrv,nre;
	bool g;

	nrNoduri=8;
	nrMuchii=5;

	srand(time(NULL));
	/*lista_de_adiac(nrNoduri,nrMuchii);
	afisare(nrNoduri);

	printf("\n algoritmul DFS:\n");
	

	DFS(nrNoduri);
	if(aciclic==0)
	{
		printf("\ngraful nu poate fi sortat topologic");
		g=false;
	}
	else
	{
		printf("\npoate fi sortat topologic\n");
		g=true;
	}

	if (g==true)
		for(int i=list_contor-1;i>0;i--)
			printf("\n lista[%d] = %d, f=%d", i, list[i]->val, list[i]->f);
	
	*/
	FILE *f1, *f2;
	f1=fopen("fisier1.csv", "w");
	fprintf(f1,"nrNoduri,nrMuchii,Operatii\n");

	nrNoduri=100;
	for(nrMuchii=1000; nrMuchii<=5000; nrMuchii+=100)
	{
		lista_de_adiac(nrNoduri, nrMuchii);
		DFS(nrNoduri);
		fprintf(f1,"%d,%d,%d\n", nrNoduri,nrMuchii,OP);
	}

	f2=fopen("fisier2.csv","w");
	fprintf(f2,"nrMuchii, nrNoduri, Operatii\n");
	nrMuchii=9000;
	for(nrNoduri=110;nrNoduri<=200;nrNoduri+=10)
	{
		lista_de_adiac(nrNoduri,nrMuchii);
		DFS(nrNoduri);
		fprintf(f2,"%d,%d,%d\n",nrMuchii,nrNoduri,OP);
	}

	fclose(f1);
	fclose(f2);

	getch();
	return 0;
}