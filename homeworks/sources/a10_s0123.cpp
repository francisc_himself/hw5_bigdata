#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler profiler("demo");

typedef struct NODE{
     int key;
	 NODE *next;
}NODE;

typedef struct node
{
   node *next;
   int vertex;
}node;

typedef struct Queue
{
   int data[60000];
   int first,last;
}Queue;

NODE *adj[10000];
int edges[60000], operations, visited[60000], verticeNr, edgeNr;
node *headLL[10000];
void insert(int, int);

int empty(Queue *point) {
   profiler.countOperation("Operations",operations);
   if(point->first==-1)
      return 1;
   return 0;
}

void generate(int verticeNr, int edgeNr) {
	int ins1, ins2;
	memset(adj, 0, verticeNr*sizeof(NODE *));
	FillRandomArray(edges, edgeNr, 0, verticeNr*verticeNr-1, true);
	for(int i = 0; i < edgeNr; i++) {
		ins1 = edges[i] / verticeNr;
		ins2 = edges[i] % verticeNr;

		insert(ins1, ins2);
		insert(ins2, ins1);
	}
}

void enqueue(Queue *point, int x) {
	profiler.countOperation("Operations", operations);
    if(point->first == -1) {
	    profiler.countOperation("Operations", operations);
        point->first = point->last = 0;
        point->data[point->first] = x;
    }
    else {
	    profiler.countOperation("Operations", operations, 2);
        point->first = point->first + 1;
        point->data[point->first] = x;
    }
}

int dequeue(Queue *point) {
   int x = point->data[point->last];
   profiler.countOperation("Operations", operations);
   if(point->first == point->last) {
		point->first = point->last = -1;
		profiler.countOperation("Operations", operations);
   }
   else {
	   point->last = point->last + 1;
	   profiler.countOperation("Operations", operations); 
   }
   return x;
}

void insert(int vi, int vj) {
   node *p, *q;
   q = (node *)malloc(sizeof(node));
   profiler.countOperation("Operations", operations);
   q->vertex = vj;
   q->next = NULL;
   if(headLL[vi] == NULL)
      headLL[vi] = q;
   else {
      p = headLL[vi];
      while(p->next != NULL)
         p = p->next;
      p->next = q;
   }
}

void DFS(int i) {
   node *p;
   int v;
   Queue queue;
   if(verticeNr < 90 && edgeNr < 90) printf("%d ", i);
   p = headLL[i];
   queue.first = queue.last = -1;
   enqueue(&queue, i);
   visited[i] = 1;
   while(p != NULL) {
	   v = dequeue(&queue);
       i = p->vertex;
       if(!visited[i])
          DFS(i);
       p = p->next;
   }
}

int main() {
	/*
   printf("\nVertices :");
   scanf("%d",&verticeNr);
   printf("\nEdges :");
   scanf("%d",&edgeNr);
   for(int i=0;i<verticeNr;i++)
      headLL[i]=NULL;
   generate(verticeNr,edgeNr);
   for(int i=0; i<verticeNr;i++) {
	   printf("\n%d : ",i);
	   DFS(i);
   }
 
   
    verticeNr = 100;
    printf("Please wait!");
	for(edgeNr = 1000; edgeNr <= 5000; edgeNr += 100) {
		operations = edgeNr;
		generate(verticeNr,edgeNr);
		DFS(edgeNr);
	}
	profiler.createGroup("Edges-DFS","operations");
	
	*/
	edgeNr=9000;
	printf("Please wait!");
	for(verticeNr = 100; verticeNr <= 200; verticeNr += 10) {
		operations = verticeNr;
		generate(verticeNr, edgeNr);
		DFS(verticeNr);
	}
	profiler.createGroup("Vertices-DFS", "Operations");
	
	printf("\nReady!");
	getch();
	profiler.showReport();
	return 0;
}