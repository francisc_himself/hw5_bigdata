#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define M 10007

int table[M];
long elemsInTable[1501];
long elemsNotInTable[1501];
int cost;

void initialise()
{
    int i;
    for (i=0;i<M;i++)
    {
        table[i]=NULL;
    }
}

int h2(int k)    
{
  int x;
  x = k%M;
  return x;
}

int h(int k, int i)       
{
  int x;
  int c1=1, c2=2;
  x = (h2(k)+c1*i+c2*i*i)%M;
  return x;
}


int hash_insert(long k)
{
    int i=0,j;
    do
    {
        j=h(k,i);
        if (table[j] == NULL)
        {
			table[j]=k;   
			return j;
        }
		else i++;
    }while (i!=M);
	return -1;
}

int searchHash(long k)
{
    int j, i=0;
    do
    {
        j=h(k,i);
		cost++;
		if (table[j]==k)        
			return j;       
        else i++;        
    }while ((table[j]!=NULL)&&(i!=M));
	return -1;
}

void main()
{
	int avg_found=0, max_found=0, avg_not_found=0, max_not_found=0;
	int i, nr=0, max, x, costInt;
	long key;
	double ff[]={0.8,0.85,0.9,0.95,0.99};

	srand(time(NULL));

	int iarray[]={19,23,2,10009,5,7,9,15,13,1};
	int sarray[]={10,10009,5,11,2,20};

	for(i=0;i<10;i++)
		hash_insert(iarray[i]);

	for(i=0;i<6;i++)
	{
		printf("%d ", searchHash(sarray[i]));
	}



	printf("\n\n ff \t avg_found \t max_found \t avg_not_found \t max_not_found \n");
	for(int k=0;k<5;k++)
	{
		for(int m=0;m<5;m++)
		{
			initialise();
			
			for (i=1; i<= (ff[k] * M) ; i++)
			{		
				key=rand();
				if(i<=1500)
					 //save 1500 elements from the table
					 elemsInTable[i]=key;
				hash_insert(key);
			}
			nr=1;

			//create an array of elements which are not in the table
			for (i=1;i<=1500;i++)
			   elemsNotInTable[i] =100000+elemsInTable[i];

			cost = 0;
			max = 0;
			int aux;
			

			//search for elements which ARE in the table
			for (i=1;i<=1500;)
			{	
				aux = table[rand()%M];
				if(aux != NULL)
				{
					costInt = cost;
					searchHash(aux);
					costInt = cost - costInt;
					if (costInt > max)
						max = costInt;
					i++;
				}
			}

			avg_found+=cost/1500;
			max_found+=max;

			cost=0;
			max=0;
			
			//search for elements which ARE NOT in the table
			for (i=1;i<=1500;i++)
			{
				costInt=cost;
				searchHash(elemsNotInTable[i]);
				costInt=cost-costInt;
				if (costInt>max)
					max=costInt;
			}
	
			avg_not_found+=cost/1500;
			max_not_found+=max;

		}
		printf("\n %1.2f \t %d \t\t %d \t\t %d \t\t %d ", ff[k], avg_found/5, max_found/5, avg_not_found/5, max_not_found/5); 
		avg_not_found = avg_found = max_found = max_not_found = 0;
	}
	getch();
}