/****ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER***
	Assign9_Breadth_First_Search
	Eficienta algoritmului este liniara O(|E|+|V|), unde E multimea muchiilor si V multimea varfurilor
	*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <conio.h>

#define alb 0
#define gri 1
#define negru 2
#define infinit 99999

typedef struct vf
{
	int color, d, parent;
} vf;


int nr_op=0;

typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

NODE *first[60001], *last[60001];
vf V[60001];
int nr_elements;
int Q[60001];

void BFS (int start, int nr_varfuri)
{
	int u;
	nr_elements = 0;
	for (u = 1; u <= nr_varfuri; u++)
		if (u != start)
		{
			V[u].color = alb;
			V[u].d = infinit;
			V[u].parent = 0;
			nr_op++;
		}
	nr_op++;
	V[start].color = gri;
	V[start].d = 0;
	V[start].parent = 0;
	nr_elements++;
	Q[nr_elements] = start;
	int i;
	while(nr_elements != 0)
	{
		u = Q[1];
		nr_op++;
		printf("%d\t",u);

		for ( i = 1; i < nr_elements; i++)
			Q[i] = Q[i+1];
		nr_elements--;
		Node *aux;
		aux = first[u];
		while (aux != NULL)
		{
			nr_op++;
			int v = aux->val;
			aux = aux->next;
			if (V[v].color == alb)
			{
				V[v].color = gri;
				V[v].d = V[u].d + 1;
				V[v].parent = u;
				nr_elements++;
				Q[nr_elements] = v;
			}
		}
		V[u].color = negru;
		nr_op++;
	}
	printf("\n");
}

bool not_duplicate (int x, int y)
{
	NODE *p;
	p = first[x];
	while(p != NULL)
	{
		if(y == p->val)
			return false;
		p = p->next;
	}
	return true;

}

void generare(int nr_varfuri,int nr_muchii)
{
	int i,u,v;
	NODE *p;
	for(i = 0; i <= nr_varfuri; i++)
		first[i] = last[i] = NULL;
	i = 1;
	while (i <= nr_muchii)
	{
		printf("%d\n",i);
		u = rand()%nr_varfuri+1;
		v = rand()%nr_varfuri+1;
		if (u != v)
		{
			if(first[u] == NULL)
			{
				first[u] = (NODE *)malloc(sizeof(NODE *));
				first[u]->val = v;
				first[u]->next = NULL;
				last[u] = first[u];
				i++;
				if(first[v] == NULL)
					{
						first[v] = (NODE *)malloc(sizeof(NODE));
						first[v]->val = u;
						first[v]->next = NULL;
						last[v] = first[v];
					}
					else
					{		
						p = (NODE *)malloc(sizeof(NODE));
						p->val = u;
						p->next = NULL;
						last[v]->next = p;
						last[v] = p;
					}
			}
			else 
			{
				if (not_duplicate (u, v) == true)// && not_duplicate (v, u) == true)
				{
					
					p = (NODE *)malloc(sizeof(NODE));
					p->val = v;
					p->next = NULL;
					last[u]->next = p;
					last[u] = p;
					if(first[v] == NULL)
					{
						first[v] = (NODE *)malloc(sizeof(NODE));
						first[v]->val = u;
						first[v]->next = NULL;
						last[v] = first[v];
					}
					else
					{		
						p = (NODE *)malloc(sizeof(NODE));
						p->val = u;
						p->next = NULL;
						last[v]->next = p;
						last[v] = p;
					}
					i++;
				}

			}			
		}
	}
}


void print(int nr_varfuri)
{
	int i;
	NODE *p;
	for(i = 1; i <= nr_varfuri; i++)
	{
		printf("%d : ",i);
		p = first[i];
		while(p != NULL)
		{
			printf("%d,",p->val);
			p = p->next;
		}
		printf("\n");
	}
}

int main()
{
	int nr_muchii, nr_varfuri;
	nr_varfuri = 5;
	nr_muchii = 8;
	srand(time(NULL));
	generare(nr_varfuri, nr_muchii);
	printf("\nLista de adiacenta:\n");
	print(nr_varfuri);
	printf("\nBFS:\n");
	BFS(1, nr_varfuri);
	getch();
	
	FILE *f = fopen("bfs1.xls","w");
	for(nr_muchii = 1000; nr_muchii < 5000; nr_muchii += 100)
	{
		nr_op=0;
		nr_varfuri = 100;
		generare(nr_varfuri,nr_muchii);
		printf("*%d\n",nr_muchii);
		BFS(1,nr_varfuri);
		fprintf(f,"%d\t %d\n",nr_muchii,nr_op);
	}
	fclose(f);
	printf("Terminare simulare1!\n");
	FILE *g = fopen("bfs2.xls","w");
	for(nr_varfuri = 100; nr_varfuri <= 200; nr_varfuri += 10)
	{
		nr_op=0;
		nr_muchii =4900;
		generare(nr_varfuri,nr_muchii);
		BFS(1,nr_varfuri);
		fprintf(g,"%d\t %d\n",nr_varfuri,nr_op);
	}
	fclose(g);
	printf("Terminare simulare2!\n");
	getch();
    return 0;
}

