/*
	Name: ***ANONIM*** ***ANONIM***�n
	Group: ***GROUP_NUMBER***
	Problem specification:  Implement correctly and efficiently 3 direct sorting methods (Bubble Sort, 
							Insertion Sort � using either linear or binary insertion and Selection Sort)
	Best case:
		-Insertion sort: A: O(n), C: O(n), A+C: O(n)
		-Selection sort: A: 0, C: O(n^2), A+C: O(n^2)
		-Bubble sort: A: 0, C: O(n), A+C: O(n)
	Average case:
		-Insertion sort: A: O(n^2), C: O(n^2), A+C: O(n^2)
		-Selection sort: A: O(n), C: O(n^2), A+C: O(n^2)
		-Bubble sort: A: O(n^2), C: O(n^2), A+C: O(n^2)
	Worst case:
		-Insertion sort: A: O(n^2), C: O(n^2), A+C: O(n^2)
		-Selection sort: A: O(n), C: O(n^2), A+C: O(n^2)
		-Bubble sort: A: O(n^2), C: O(n^2), A+C: O(n^2)
	Running time:
		-Insertion sort: best O(n), average O(n^2), worst O(n^2)
		-Selection sort: best O(n^2), average O(n^2), worst O(n^2)
		-Bubble sort: best O(n), average O(n^2), worst O(n^2)
	Observation: Selection sort performs pretty well in terms of comparisons+assignments in average and worst case, 
				beating insertion and bubble sort. This is not true in the best case.
	Memory: in situ sorting algs, so memory complexity O(n)
	Stability: all of them are stable
*/
#include<stdio.h>

int I, T, n, A[10001], aux[10001];
int i,j;
FILE *f;
int AI, CI, AS, CS, AB, CB;


void read()
{
	scanf("%d", &n);
	for(i=1;i<=n;i++)
	{
		scanf("%d", &A[i]);
		aux[i]=A[i];
	}
}

void restore_array()
{
	for(i=1;i<=n;i++)
		A[i]=aux[i];
}

void print_array()
{
	for(i=1;i<=n;i++)
		printf("%d ", A[i]);
	printf("\n");
}

void insertion_sort()
{
	int comp=0;
	int assign=0;

	int buffer;
	for(i=2;i<=n;i++)
	{
		buffer=A[i];
		assign++;
		for(j=i-1;A[j]>buffer && j>0;j--)
		{
			A[j+1]=A[j];
			assign++;comp++;
		}
		A[j+1]=buffer; assign++; comp++;
	}
	AI+=assign;
	CI+=comp;
	//printf("%d,%d,%d,", assign, comp, assign+comp);
}

void selection_sort()
{
	int comp=0;
	int assign=0;

	int pos;
	for(j=1;j<n;j++)
	{
		pos=j;
		for(i=j+1;i<=n;i++)
		{
			if(A[i]<A[pos])
				pos=i;
			comp++;
		}
		if(j!=pos)
		{
			int aux=A[pos];
			A[pos]=A[j];
			A[j]=aux;
			assign+=3;
		}
	}
	AS+=assign;
	CS+=comp;
	//printf("%d,%d,%d,", assign, comp, assign+comp);
}

void bubble_sort()
{
	int comp=0;
	int assign=0;

	int first=1, last=n, last_temp=n, first_temp=1, aux;
	bool swap=true;
	while(first<last && swap==true)
	{
		swap=false;
		for(i=first;i<last;i++)
		{
			if(A[i]>A[i+1])
			{
				swap=true;
				last_temp=i;
				aux=A[i+1];
				A[i+1]=A[i];
				A[i]=aux;
				assign+=3;
			}
			comp++;
		}
		last=last_temp;
		for(i=last;i>first;i--)
		{
			comp++;
			if(A[i]<A[i-1])
			{
				swap=true;
				first_temp=i;
				aux=A[i];
				A[i]=A[i-1];
				A[i-1]=aux;
				assign+=3;
			}
		}
		first=first_temp;
	}
	AB+=assign;
	CB+=comp;
	//printf("%d,%d,%d\n", assign, comp, assign+comp);

}

void test()
{
	//test
	n=6;
	A[1]=324;A[2]=-2;A[3]=5;A[4]=0;A[5]=234;A[6]=-423;
	printf("Initial array:\n");
	for(i=1;i<=6;i++)
		aux[i]=A[i];
	print_array();
	printf("Insertion sort result:\n");
	insertion_sort();
	print_array();
	restore_array();
	printf("Selection sort result:\n");
	selection_sort();
	print_array();
	restore_array();
	printf("Bubble sort result:\n");
	bubble_sort();
	print_array();
}

int main()
{
	test();
	//read data
	freopen("average.txt", "r", stdin);
	f=freopen("sort_average.csv", "w", stdout);
	printf("n,AI,CI,AI+CI,AS,CS,AS+CS,AB,CB,AB+CB\n");


	scanf("%d", &T);
	for(I=1;I<=T;I++)
	{
		AI=CI=AS=CS=AB=CB=0;
		for(int K=1;K<=5;K++)
		{
			read();
			//printf("%d,", n);
			insertion_sort();
			restore_array();
			selection_sort();
			restore_array();
			bubble_sort();
		}
		printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n", n, AI/5, CI/5, AI/5+CI/5, AS/5, CS/5, AS/5+CS/5, AB/5, CB/5, AB/5+CB/5);
	}
	return 0;
}