#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include <Windows.h>

#define MAX_SIZE 10000

FILE *f,*f2,*f3;
int compB=5,atribB,compS,compI,atribS,atribI;

void creare(int *v,int n,int low,int high)
{
	int i,aux;
	
	for(i=0;i<n;i++)
	{
		aux=rand()%(high-low+1);
		aux=aux+low;
		v[i]=aux;
	}

}

void creare_worst(int *v,int n,int low,int high)
{
	int i,aux;
	
	for(i=0;i<n;i++)
	{

		aux=rand()%(high-low+1);
		aux=aux+low;
		if(i==0)
		{
			v[i]=aux;
		}
		else
		{
		v[i]=v[i-1]-aux;
		}
	}

}

void creare_best(int *v,int n,int low,int high)
{
	int i,aux;
	
	for(i=0;i<n;i++)
	{

		aux=rand()%(high-low+1);
		aux=aux+low;
		if(i==0)
		{
			v[i]=aux;
		}
		else
		{
		v[i]=v[i-1]+aux;
		}
	}

}


void bubblesort(int *v,int n)
{
	int i,j,k;
	
	for(i=0;i<n-1;i++)
		for(j=0;j<n-i-1;j++)
		{
			compB++;
			if(v[j]>v[j+1])
			{
				
				k=v[j];
				v[j]=v[j+1];
				v[j+1]=k;
				atribB=atribB+3;
			}
		}
	
}



void select_sort(int *v, int n)
{
	int i,j,min,temp;
	
	for(i=0;i<n;i++)
	{
		min=i;
		for(j=i+1;j<n;j++)
		{
			compS++;
			if(v[min]>v[j])
			{
				
				min=j;
				
			}
		}
			temp=v[i];
			v[i]=v[min];
			v[min]=temp;
			atribS=atribS+3;
		
	}


}



void insert_sort(int *v, int n)
{
	int i,k,temp,x;
	
	for(i=1;i<n;i++)
	{
		x=v[i];
		k=i;
		while(k>0 && x<v[k-1])
		{
			compI++;
			temp=v[k];
			v[k]=v[k-1];
			v[k-1]=temp;
			k--;
			atribI=atribI+3;
		}
		v[k]=x;
		atribI++;
	}
}

void copy(int *v1, int *v2,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v2[i]=v1[i];
	}
	
}

void afisare(int *v,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("%d ",v[i]);
	}
	printf("\n");
}


int main()
{	
	int n,m=5,test=25;
	//float mComp1=0,mAtrib1=0,mComp2=0,mComp3=0,mAtrib2=0,mAtrib3=0;   // medie comparati/atribuiri pt fiecare din metodele de sortare, bubble,selectie,insertie
	
	f=fopen("mediu.csv","w");
	f2=fopen("worst.csv","w");
	f3=fopen("best.csv","w");
	srand(time(NULL));
	
	int* v1=(int*)malloc(test*sizeof(int));
	int* v2=(int*)malloc(test*sizeof(int));
	int* v3=(int*)malloc(test*sizeof(int));
	
	creare(v1,test,0,test);
	copy(v1,v2,test);
	copy(v1,v3,test);
	afisare(v1,test);
	
	bubblesort(v1,test);
	afisare(v1,test);			
	select_sort(v2,test);
	afisare(v2,test);			
	insert_sort(v3,test);
	afisare(v3,test);
	

			compB=0;
			compS=0;
			compI=0;
			atribB=0;
			atribS=0;
			atribI=0;
	

	for(n=100;n<MAX_SIZE;n=n+100)
	{
			int* v1=(int*)malloc(n*sizeof(int));
			int* v2=(int*)malloc(n*sizeof(int));
			int* v3=(int*)malloc(n*sizeof(int));
			printf(" %d",n);
			for(int i=0;i<m;i++)
			{
				creare(v1,n,1,1000);
				
				copy(v1,v2,n);
				copy(v1,v3,n);
				
				bubblesort(v1,n);
				
				select_sort(v2,n);
				
				insert_sort(v3,n);
				
				
				
			}
			fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compB/m,compS/m,compI/m,atribB/m,atribS/m,atribI/m,(compB+atribB)/5,(compS+atribS)/5,(compI+atribI)/5);
			
			compB=0;
			compS=0;
			compI=0;
			atribB=0;
			atribS=0;
			atribI=0;
			
			creare_worst(v1,n,1,102);
			copy(v1,v2,n);
			copy(v1,v3,n);
			
			

			bubblesort(v1,n);
			select_sort(v2,n);
			insert_sort(v3,n);
			fprintf(f2,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compB,compS,compI,atribB,atribS,atribI,compB+atribB,compS+atribS,compI+atribI);
			
			compB=0;
			compS=0;
			compI=0;
			atribB=0;
			atribS=0;
			atribI=0;

			creare_best(v1,n,1,102);
			copy(v1,v2,n);
			copy(v1,v3,n);

			bubblesort(v1,n);
			select_sort(v2,n);
			insert_sort(v3,n);
			fprintf(f3,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compB,compS,compI,atribB,atribS,atribI,compB+atribB,compS+atribS,compI+atribI);
			
			compB=0;
			compS=0;
			compI=0;
			atribB=0;
			atribS=0;
			atribI=0;
			
			
			free(v1);
			free(v2);
			free(v3);


	}


	fclose(f);
	fclose(f2);
	fclose(f3);
	return 0;
}


	

