
#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <stdlib.h>

void randm(long int v[],long int n)
{
	for(int i=1;i<=n;i++)
	{
		v[i] = rand()%1000;
	}
}

void randm_crescator(long int v[],long int n)
{
	v[0] = rand() % 10;
	for(int i=1;i<=n;i++)
	{
		v[i] = v[i-1] + rand() % 10;
	}
}

void randm_descrescator(long int v[],long int n)
{
	v[0] = rand() % 100 + n;
	for(int i=1;i<=n;i++)
	{
		v[i] = v[i-1] - rand() % 10;
	}
}

void bubble_sort(long int A[],long int n, long &na, long &nc)
{
	int aux;

	for (int i=1; i<=n-1; i++)
		for (int j=1; j<=n-1; j++)
		{
			nc++;
			if (A[j] > A[j+1])
			{
				na = na +3;
				aux = A[j];
				A[j] = A[j+1];
				A[j+1] = aux;
			}
		}
}

void selectie(long int A[],long int n,long &na, long &nc)
{
	int imin,aux,i;

	for(i=1;i<=n;i++)
	{
		imin = i;
		for(int j=i+1;j<=n;j++)
		{
			nc++;
			if(A[j]<A[imin])
			{
				imin = j;
			}
		}	
		na=na+3;
		aux=A[i];
		A[i]=A[imin];
		A[imin]=aux;
	}
}

void insertie(long int A[],long int n, long &na, long &nc)
{
	int x,j;

	for(int i=2;i<=n;i++)
	{
		na++;
		x=A[i];
		j=1;
		nc++;
		while(j<i && A[j]<x)
		{
			nc++;
			j++;
		}
		for(int k=i-1;k>=j;k--)
		{
			na++;
			A[k+1]=A[k];
		}
		na++;
		A[j]=x;

	}
}

void copiere(long int A[], long int B[],long int n)
{
	for (int i=1; i<=n; i++)
		B[i] = A[i];
}

void caz_fav()
{
	long int A[10001],B[10001];
	long int n;
	long na = 0;
	long nc = 0;

	FILE *f1;
	f1 = fopen ("bubble_favorabil.txt","w");
	
	FILE *f2;
	f2 = fopen ("selectie_favorabil.txt","w");
	
	FILE *f3;
	f3 = fopen ("insertie_favorabil.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		
		randm_crescator(A,n);
		printf("%d  ",n);
		//bubble_sort
		na = 0;
		nc = 0;
		copiere(A,B,n);
		bubble_sort(B,n,na,nc);
		fprintf(f1,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//selectie
		na = 0;
		nc = 0;
		copiere(A,B,n);
		selectie(B,n,na,nc);
		fprintf(f2,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//insertie
		na = 0;
		nc = 0;
		copiere(A,B,n);
		insertie(B,n,na,nc);
		fprintf(f3,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		
	}
	
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

void caz_defav ()
{
	long int A[10001], B[10001];
	long int n;
	long na = 0;
	long nc = 0;

	FILE *f1;
	f1 = fopen ("bubble_dfav.txt","w");
	FILE *f2;
	f2 = fopen ("selectie_dfav.txt","w");
	FILE *f3;
	f3 = fopen ("insertie_dfav.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		randm_descrescator(A,n);
		printf("%d  ",n);
		//bubble_sort
		na = nc = 0;
		copiere(A,B,n);
		bubble_sort(B,n,na,nc);
		fprintf(f1,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//sortare selectie
		na = nc = 0;
		copiere(A,B,n);
		selectie(B,n,na,nc);
		fprintf(f2,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//sortare_insertie
		na = nc = 0;
		copiere(A,B,n);
		insertie(B,n,na,nc);
		fprintf(f3,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);
	}
}

void caz_mediu ()
{
	long int A[10001],B[10001];
	long int n;
	long na1,na2,na3,nc1,nc2,nc3;
	long na,nc;

	FILE *f1;
	f1 = fopen ("bubble_med.txt","w");
	FILE *f2;
	f2 = fopen ("selectie_med.txt","w");
	FILE *f3;
	f3 = fopen ("insertie_med.txt","w");

	//srand (time(NULL));

	for (n=100; n<=10000; n=n+100)
	{
		na1 = 0;
		na2 = 0;
		na3 = 0;
		nc1 = 0;
		nc2 = 0;
		nc3 = 0;

		for (int k=1; k<=5; k++)
		{
			randm(A,n);
			printf("%d  ",n);
			//bubble_sort
			na=0;
			nc=0;
			copiere(A,B,n);
			bubble_sort(B,n,na,nc);
			na1+=na;
			nc1+=nc;

			//sortare selectie
			na=0;
			nc=0;
			copiere(A,B,n);
			selectie(B,n,na,nc);
			na2+=na;
			nc2+=nc;

			//sortare_insertie
			na=nc=0;
			copiere(A,B,n);
			insertie(B,n,na,nc);
			na3+=na;
			nc3+=nc;
		}

		na1/=4;nc1/=4;
		na2/=4;nc2/=4;
		na3/=4;nc3/=4;

		fprintf (f1,"%d \t %d \t %d \t %d\n",n,na1,nc1,na1+nc1);
		fprintf (f2,"%d \t %d \t %d \t %d\n",n,na2,nc2,na2+nc2);
		fprintf (f3,"%d \t %d \t %d \t %d\n",n,na3,nc3,na3+nc3);
	}
}

int main()
{
    long int a[]={1,3,2,9,10,4};
    long int n=5;
    long na=0,nc=0;
	int i;
	printf("\n Sirul initial=");
	for(i=0; i<=n; i++)
		printf("%d ", a[i]);
	printf("\n Sirul dupa insertie=");
    insertie(a, n, na,nc);
	for(i=0; i<=n; i++)
		printf("%d ", a[i]);



	//caz_favorabil();
	//caz_defavorabil();
	//caz_mediu();
	getch();
	return 0;
}
