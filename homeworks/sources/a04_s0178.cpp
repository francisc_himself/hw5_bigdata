/*
Heapsort is a comparison sort algorithm, which is part of the selection sort family. Although slower than quicksort on 
average cases, heapsort has the advantage of having a worst case running time of O(n logn), compared to quicksort's
O(n^2). It's average running time is also O(n log n). Heapsort is an in place algorithm, but it is not stable.
QuickSort is a very efficient sorting algorithm, that in an average case has a O(n log n) running time, but it is faster
in practice than other O(n log n) algorithms (like heapsort).
It's worst case performance is O(n^2), whereas its best one is O(n log n).
*/

#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>

using namespace std;
ofstream fout;

int left (int i);
int right (int i);
int parent (int i);
void maxHeapify (int a[], int n, int i, long long *comp, long long *atr);
void buildMaxHeap (int a[], int n, long long *comp, long long *atr);
void maxHeapify (int a[], int n, int i, long long *comp, long long *atr);
void heapSort (int a[], int n, long long *comp, long long *atr);
int partition (int a[], int p, int r, long long *comp, long long *atr);
void quickSort (int a[], int p, int q, long long *comp, long long *atr, int pivot);
void writeToFile (long long atr, long long comp, char * fileName);

void main(){
	int a[10000], b[10000], n;
	long long comparatii, atribuiri, x, y;
	printf ("Type in n ");
	scanf("%d", &n);
	printf ("Type in the array ");
	for (int i=1;i<=n;i++) {
		scanf ("%d", &a[i]);
		b[i]=a[i];
	}

	heapSort (a, n+1, &x, &y);
	for (int i=2;i<n+2;i++)
		printf ("%d ", a[i]);
	printf("\n");
	x=0; y=0;
	quickSort(b, 1, n+1, &x, &y, 2);
	for (int i=2;i<n+2;i++)
		printf ("%d ", b[i]);
	printf("\n");

	for (int i=100;i<10000;i=i+100){
		long long avgAtrHeap=0, avgCompHeap=0, avgAtrQuick=0, avgCompQuick=0;
		for (int k=0;k<5;k++){
			for (int j=1; j<=i; j++){
				a[j]=rand();
				b[j]=a[j];
			}


			comparatii=0;
			atribuiri=0;


			heapSort(a, i+1, &comparatii, &atribuiri);
			avgAtrHeap+=atribuiri;
			avgCompHeap+=comparatii;

			comparatii=0;
			atribuiri=0;
			
			quickSort (b, 1, i+1, &comparatii, &atribuiri, 0);
			avgAtrQuick+=atribuiri;
			avgCompQuick+=comparatii;

		}
		fout.open ("average.csv", ios::app );
		fout<<i;
		fout.close();


		avgAtrHeap=avgAtrHeap/5;
		avgCompHeap=avgCompHeap/5;
		writeToFile (avgAtrHeap, avgCompHeap, "average.csv");

		avgAtrQuick=avgAtrQuick/5;
		avgCompQuick=avgCompQuick/5;
		writeToFile (avgAtrQuick, avgCompQuick, "average.csv");

		fout.open ("average.csv", ios::app );
		fout<<'\n';
		fout.close();

		
	}


	for (int i=100;i<=10000;i=i+100){
		for (int j=1;j<=i;j++){
			a[j]=j;
			b[j]=j;
		}

		comparatii=0;
		atribuiri=0;

		fout.open ("best.csv", ios::app );
		fout<<i;
		fout.close();

		quickSort (a, 1, i+1, &comparatii, &atribuiri, 1);
		writeToFile (atribuiri, comparatii, "best.csv");

		fout.open ("best.csv", ios::app );
		fout<<'\n';
		fout.close();

		comparatii=0;
		atribuiri=0;

		fout.open ("worst.csv", ios::app );
		fout<<i;
		fout.close();

		quickSort (b, 1, i+1, &comparatii, &atribuiri, 2);
		writeToFile (atribuiri, comparatii, "worst.csv");

		fout.open ("worst.csv", ios::app );
		fout<<'\n';
		fout.close();

	}


	getch();
}


int left (int i){
	return 2*i;
}




int right (int i) {
	return 2*i+1;
}




int parent (int i) {
	return (int)floor((float)i/2);
}






void buildMaxHeap(int a[], int n, long long *comp, long long *atr) {
	long long x, y;
	for (int i=(int)floor((float)n/2);i>=1;i--) {
		x=0; y=0;
		(*atr)=(*atr)+2;
		maxHeapify (a, n, i, &x, &y);
		(*comp) = (*comp) + x;
		(*atr) = (*atr) + y;
	}
}

void maxHeapify (int a[], int n, int i, long long *comp, long long *atr) {
	int l, r, largest, aux;
	long long x, y;
	l=left(i);
	r=right(i);
	if (l<=n && a[l]>a[i])
		largest=l;
	else
		largest=i;
	(*comp)++;
	if (r<=n && a[r]>a[largest])
		largest =r;
	(*comp)++;
	if (largest!=i) {
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		(*atr)=(*atr)+5;
		x=0; y=0;
		maxHeapify(a, n, largest, &x, &y);
		(*atr)=(*atr)+y;
		(*comp)=(*comp)+x;
	}
}

void heapSort (int a[], int n, long long *comp, long long *atr) {
	int aux;
	long long x, y;
	x=0; y=0;
	buildMaxHeap(a, n, &x, &y);
	(*atr)=(*atr)+y;
	(*comp)=(*comp)+x;
	for (int i=n;i>=2;i--) {
		aux = a[i];
		a[i]=a[1];
		a[1]=aux;
		n=n-1;
		(*atr)=(*atr)+3;
		x=y=0;
		(*atr)=(*atr)+2;
		maxHeapify(a, n, 1, &x, &y);
		(*atr)=(*atr)+y;
		(*comp)=(*comp)+x;
	}

}



int partition (int a[], int p, int r, long long *comp, long long *atr, int pivot) {
	int x, i, aux;
	if (pivot==1) {
		aux=a[(p+r)/2];
		a[(p+r)/2]=a[r];
		a[r]=aux;
	}
	else
		if (pivot==2) {
			aux=a[p+1];
			a[p+1]=a[r];
			a[r]=aux;
		}
	x=a[r];
	
	(*comp)++;
	(*atr)++;
	i = p-1;
	for (int j=p;j<r;j++){
		if (a[j]<=x) {
			i++;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
			(*atr)+=3;
		}
		(*comp)++;
	}
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;
	(*atr)+=3;
	
	return i+1;
}



void quickSort (int a[], int p, int r, long long *comp, long long *atr, int pivot) {
	int q;
	long long x=0, y=0;
	if (p<r) {
		q=partition (a, p, r, &x, &y, pivot);
		(*comp)+=x;
		(*atr)+=y;
		x=y=0;
		quickSort(a, p, q-1, &x, &y, pivot);
		(*comp)+=x;
		(*atr)+=y;
		x=y=0;
		quickSort(a, q+1, r, &x, &y, pivot);
		(*comp)+=x;
		(*atr)+=y;
	}
	(*comp)++;
}

void writeToFile (long long atr, long long comp, char * fileName) {
	fout.open (fileName, ios::app );
	fout<<','<<atr<<','<<comp<<','<<(atr+comp);
	fout.close();
}
