#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include<cstdlib>
#include "Profiler.h"
#define NMAX 20000

int dim_h = 0, op_critica = 0;

typedef struct nod {
	int cheie;
	struct nod *urm;
}NOD;

NOD *prim = NULL, *ultim = NULL, *lista[NMAX];

typedef struct detalii {
	int valoare;
	int pozitie;
}INFO;

//operatii pe liste:

void adaugaNod(int val) {
	
	NOD *p=(NOD *)malloc(sizeof(NOD));
	p->cheie=val;
	if(prim==NULL)
	{
		prim=p;
		ultim=prim;
	}
	else
	{
		ultim->urm=p;
		ultim=p;
	}
	ultim->urm=NULL;
}

NOD *stergeNod(int i) {//steregerea primului nod
	
	NOD *p=lista[i]->urm;
	if(p!=NULL)
		lista[i]->urm=p->urm;
	return p;
}

void adaugaLista(int i) {
	
	lista[i]=(NOD *)malloc(sizeof(NOD));
	lista[i]->urm=prim;
}

//construirea heap`ului

void reBuild(INFO a[], int i) {
	
	int stg, dr, min;
	INFO aux;
	stg=2*i;
	dr=2*i+1;
	op_critica++;
	if(stg<=dim_h && a[stg].valoare<a[i].valoare)
		min=stg;
	else
		min=i;
	op_critica++;
	if(dr<=dim_h && a[dr].valoare<a[min].valoare)
		min=dr;
	if(min!=i)
	{
		aux=a[i];
		a[i]=a[min];
		a[min]=aux;
		op_critica+=3;
		reBuild(a,min);
	}
}

void init_h() {
	dim_h = 0;
}

void push_h(INFO a[],int v, int p) {
	
	int i;
	INFO aux;
	dim_h++;
	op_critica+=2;
	a[dim_h].valoare=v;
	a[dim_h].pozitie=p;
	i=dim_h;
	while(i>1 && a[i].valoare < a[i/2].valoare)
	{
		aux=a[i];
		a[i]=a[i/2];
		a[i/2]=aux;
		i=i/2;
		op_critica+=4;
	}
	op_critica++;
}

INFO pop_h(INFO a[]) {

	INFO x=a[1];
	op_critica++;
	a[1]=a[dim_h];
	dim_h--;
	reBuild(a,1);
	return x;
}

void interclasare(int k)
{
	int v;
	INFO a[1000];
	NOD *p;
	INFO info;
	prim=NULL;
	ultim=NULL;
	init_h();
	for(int i=1; i<=k; i++)
	{
		v = (stergeNod(i))->cheie;
		op_critica++;
		push_h(a,v,i);
	}
	while(dim_h>0)
	{
		info=pop_h(a);//extrag din varf
		adaugaNod(info.valoare);//adaugare la lista finala
		p=stergeNod(info.pozitie);//extrag primul nod din lista din care era ultimul nod
		op_critica+=2;
		if(p!=NULL)
			push_h(a,p->cheie,info.pozitie);
	}
}

//void creareListeOrdonateAleator(int k,int n)
//{
//	int sir1[NMAX];
//	op_critica=0;
//	for(int i=1;i<=k;i++)
//	{
//		prim=NULL;
//		ultim=NULL;
//
//		FillRandomArray(sir1,n,1,20000,true,1);
//		
//		for(int j=0;j<n;j++)
//		{
//			adaugaNod(sir1[j]);
//		}
//		adaugaLista(i);
//	}
//	interclasare(k);
//}

void creareListeOrdonateAleator(int k,int n) {
	int nr,nra=0;
	op_critica=0;
	srand(time(NULL));
	for(int i=1;i<=k;i++)
	{
		prim=NULL;
		ultim=NULL;
		for(int j=1;j<=n/k;j++)
		{
			nr=nra+rand();
			adaugaNod(nr);
			nra=nr;
		}
		adaugaLista(i);
	}
	interclasare(k);
	srand(1);
}

void punctulUnu() {
	int n,k,k1,k2,k3;
	FILE *pf1=fopen("file1.csv","w");
	fprintf(pf1,"n,k=5,k=10,k=100\n");
	fprintf(pf1,"\n");
	for(n=100;n<=10000;n+=100)
	{
		
		k=5;
		op_critica=0;
		creareListeOrdonateAleator(k,n);
		k1=op_critica;
		
		k=10;
		op_critica=0;
		creareListeOrdonateAleator(k,n);
		k2=op_critica;
		
		k=100;
		op_critica=0;
		creareListeOrdonateAleator(k,n);
		k3=op_critica;

		fprintf(pf1,"%d,%d,%d,%d\n",n,k1,k2,k3);	
	}
	fclose(pf1);
}

void punctulDoi() {
	int n,k;
	FILE *pf2=fopen("file2.csv","w");
	fprintf(pf2,"k,operatii\n");
	fprintf(pf2,"\n");
	n=10000;
	for(k=10;k<=500;k+=10)
	{
		op_critica=0;
		creareListeOrdonateAleator(k,n);
		fprintf(pf2,"%d,%d\n",k,op_critica);
	}
	fclose(pf2);
}

void exemplu() {
	int a[]={1,5,9,13};
	int b[]={2,6,10,14,18};
	int c[]={0,3,8,16};
	int d[]={4,7,11,15,17};
	int k=4;

	for(int i=0;i<4;i++)
		adaugaNod(a[i]);
	adaugaLista(1);

	prim=NULL;
	ultim=NULL;
	for(int i=0;i<5;i++)
		adaugaNod(b[i]);
	adaugaLista(2);

	prim=NULL;
	ultim=NULL;
	for(int i=0;i<4;i++)
		adaugaNod(c[i]);
	adaugaLista(3);

	prim=NULL;
	ultim=NULL;
	for(int i=0;i<5;i++)
		adaugaNod(d[i]);
	adaugaLista(4);

	printf("Interclasarea: \n");
	interclasare(k);
	for(NOD *q=prim;q!=NULL;q=q->urm)
		printf("%d ",q->cheie);
}



int main(void)
{
	exemplu();  //pentru a arata functionalitatea programului
	punctulUnu();
	punctulDoi();
	printf("done`");
	getch();
	return 0;
}