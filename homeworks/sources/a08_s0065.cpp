/*
***ANONIM*** ***ANONIM*** ***ANONIM*** 
GRUPA ***GROUP_NUMBER***

 - programul utilizeaza un algoritm eficient de creare a unor seturi disjuncte precum si realizarea legaturilor dintre acestea
 - initial intr-o matrice avand coeficientii i si j , se introduce valoarea 1 unde intre nodul i si j avem muchie
 - dupa citirea matricii se creeaza nodurile aferente si se realizeaza seturile disjuncte


*/

#include<stdlib.h>
#include<stdio.h>


bool first1=true,first2=true;
int head1,head2;


typedef struct NODE
{
	int key;
	int rank;
	struct NODE *parent;
	struct NODE *representative;

} node;



void setLink(node *x,node *y)
{
	if(x->rank>y->rank)
	{
		y->parent=x;
	}
	else
	{
		x->parent=y;
		if(x->rank==y->rank)
		{
			y->rank=y->rank+1;
		}
	}
}


void makeSet(node *x)
{
	x->parent=x;
	x->rank=0;
}

node *findSet(node *x)
{
	if(x!=x->parent)
	{
		x->parent=findSet(x->parent);
	}
	return x->parent;
}

bool sameComponent(node *u, node *v)
{
	if(findSet(u)==findSet(v))
	{
		return true;
	}
	return false;
}




void setUnion( node *x, node *y)
{
	setLink(findSet(x),findSet(y));
}

void connectedComponents(int matrice1[100][100],int n,node *nodes1[100])
{
	for(int i=1;i<=n;i++)
	{
		makeSet(nodes1[i]);
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			if(matrice1[i][j]==1)
			{
			if(findSet(nodes1[j])!=findSet(nodes1[i]))
			{
				setUnion(nodes1[j],nodes1[i]);
			}
			}
		}
		
	}
}
void initNodes(int n,node *nodes1[100])
{
	for(int i=1;i<=n;i++)
	{
	    nodes1[i] = (node *)malloc(sizeof(node));
		nodes1[i]->key=i;
		
	}
}

void readMatrix(int matrice1[100][100])
{
	int i=0,j=0,value;
	printf("Initializare Matrice 1\n\n");
	while(1)
	{
	printf("Matrice1[");
	scanf("%d",&i);
	printf("][");
	scanf("%d",&j);
	if(i<0 || j<0)
	{
		break;
	}
	printf("]=");
	scanf("%d",&value);
	printf("\n");
	matrice1[i][j]=value;
	}

}

int main()
{
	int n;
	printf("Introduceti numarul de elemente ale grafurilor : ");
	scanf("%d",&n);

	int matrice1[100][100];
	
	node *nodes[100];
	readMatrix(matrice1);
	
	initNodes(n,nodes);
	connectedComponents(matrice1,n,nodes);
	
	printf("\nDONE\n");

	return 0;
}

