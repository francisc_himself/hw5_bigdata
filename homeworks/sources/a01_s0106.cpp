#include<iostream>
#include<fstream>
#include<conio.h>
#include<time.h>
using namespace std;
/*
Best Case:
	-Assigments:As we can observe from the chart the two algorithms that behave very efficients performing 0 assigments are Bubble sort and Selection sort 
	and the one that behaves not so efficient is the LinearInsertion sorting algorithm that performs from 198 to 19998 assigments for different size input from 100 to 10 000

	-Comparisons:In terms of comparisons the algorithms that behave the best are the Bubble sort and Linearinsertion sort performing for a given input in the
	100 10000 range from 99 to 9999 and the worst behaving algorithm is the Selections for that makes from 4950 to 49995000 comparisons

	-Overall:Analizing the overal efficiency in terms of both comparisons and assiments we can easly decide the most efficient algorithn that is Bubble sort performing
	a sum of comparisons and assigments of 99 for 100 length input to 9999 for 10 000 input length compared with the Insertions sort that gives a sum of 297 to 29 997.So
	we cand easly deduce now that the worst behaving algorithm on best case is the selection sort which performs from 4950 to 49995000 comparisons and assignments
Average Case:
	-Assignments:From the first graf we can easily deduce that the best behaving algorithm in terms of assignments is the Selection sort algorith which for a given input of
	length 100 to 10 000 performs from 280 to 29970 operations,the second best is the Insertion sort algorithm doing from 2647 to 25006777,and the one that remains and is 
	as we can see the less efficient in terms of comparisons is the Bubble sort wich performs from 7521 to 75039468 operantions for the given input length.
	
	-Comparisons:Lets now take a look at the number of comparisons,and we can easly see that the sortiong method with the fewest is the Linear insertion sort with a number of 2548
to 24996778.Second in the ranking of comparisons efficiency is the Selecion sort algorith performing from 4950 to 49995000.The last algorithm regardin coparisons number is the Bubble sort,
wich for the given input length of 10 to 10000 gives a number of 8870 to 98596139 comparisons.
	
	-Overall:Now we cand analize the algorithms from the overall point of view,comparisons+assigments,and as we can easily see on the chart the less efficient one is the Bubble sort algorith
		that performs the most number of comparisons for given input in the range 10 to 10000,this is followed by the Selection sort algorith .So the best behaving algorithm is case of a average input is the
		Linear insertion sort algorith,close to the Selection sort but very far from the worst behaving algorith that is Bubble sort.
Worst Case:
	-Assignments:Lets now see how they behave int he worst input case,in terms of assigments we can see from the plot that the slection sort is the best one having from 297 to 29997 .
	Second is the Linear insertion sort that performs from 5148 to 50014998  operations.Last one is the bubble sort that compared with the oders has a very low officiency in this case,
	having from 14850 to 149985000 assigments  for the given input in the range 100 10000.
	
	-Comparisons:Now analizing from the comparisons point of view,we can see the worst one is the Bubble sort that performs from 9900 to 99990000 for 100 input length to for 10 000 input length. 
Second in the rankings is the Linear insertion sort that goes from 5049 to 50004999 , on the edge with Selection sort which is the most efficient from this point of view
and has from 4950 to 49995000 comparisons.

	-Overall:Analizing the worst case from total number of operations perspective,we can see the winner that is Selection sort best behaving algorith,followed by the Linear insertion sort and last is the 
	Bubble sort that compared with other two algorithms gives a dissapointing result.

Conclusion:After analizing the 3 cases we can draw the conclusion,and we can promote the winner to be the Linear insertion sort that even if id not the bet in nor of the 3 cases is the stable one giving
good numbers for all 3 cases,second one is the Selection sort that even if it behaves badly in the best case ,is the best in the other two cases,and this makes it the second stable and efficient algorithm.
Last in the ranking is the Bubble sort algorithm that is veru good dealing with the best case but the worst in the other two cases.
*/
//bestcase ordered.worst case reverse ordered
void BubbleSort(long n,long *c,long *a,long in[])
{
int i,aux,ok=0;
do{
	ok=0;
	for(i=1;i<n;i++)
	{
	(*c)=(*c)+1;
		if(in[i]>in[i+1])
		{	(*a)=(*a)+3;
			aux=in[i];
			in[i]=in[i+1];
			in[i+1]=aux;
			ok=1;
		}	
	}
}while(ok==1);
}
//best case ordered,worst case 2,...,100,1,oredren until last element
void SelectionSort(long n,long *c,long *a,long in[])
{int i,j,pos,aux;
for(j=1;j<n;j++)
	{
		pos=j;
	for(i=j+1;i<=n;i++)
		{	(*c)=(*c)+1;
			if(in[i]<in[pos])
				pos=i;
		}
	if(j!=pos)
		{aux=in[j];
		in[j]=in[pos];
		in[pos]=aux;
		(*a)=(*a)+3;
		}
	}


}
//bestcase ordered.worst case reverse ordered
void LiniarInsertionSort(long n,long *c,long *a,long in[])
{int i,j;
for(i=2;i<=n;i++)
{		in[0]=in[i];
		(*a)=(*a)+1;
		j=i-1;
		while(in[j]>in[0] && j>0)
			{(*a)=(*a)+1;
			(*c)=(*c)+1;
			in[j+1]=in[j];
			j=j-1;
			}
		(*a)=(*a)+1;
		(*c)=(*c)+1;
		in[j+1]=in[0];
}


}
int main()
{
	FILE *worst,*best,*averange;
	long ab=0,cb=0,as=0,cs=0,ai=0,ci=0;
	long abs=0,cbs=0,ass=0,css=0,ais=0,cis=0;
	long binb[10001],bins[10001],bini[10001];
	long i,j,c;
	worst=fopen ("worst.csv","w");
	best=fopen ("best.csv","w");
	averange=fopen ("averange.csv","w");

	for(i=100;i<=10000;i+=100)
	{ab=0,cb=0,as=0,cs=0,ai=0,ci=0;
		for(j=1;j<=i;j++)
		{
			binb[j]=j;
			bins[j]=j;
			bini[j]=j;
		}
		BubbleSort(i,&cb,&ab,binb);
		SelectionSort(i,&cs,&as,bins);
		LiniarInsertionSort(i,&ci,&ai,bini);
		fprintf(best,"%ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld\n",i,ab,cb,cb+ab,as,cs,cs+as,ai,ci,ai+ci);
	}
	

	for(i=100;i<=10000;i+=100)
	{ab=0,cb=0,as=0,cs=0,ai=0,ci=0;
	c=1;
		for(j=i;j>=1;j--)
		{
			bini[c]=j;
			binb[c]=j;
			c++;
			
		}
		
		for(j=1;j<i;j++)
		bins[j]=j+1;
		bins[i]=1;

		BubbleSort(i,&cb,&ab,binb);
		SelectionSort(i,&cs,&as,bins);
		LiniarInsertionSort(i,&ci,&ai,bini);
		fprintf(worst,"%ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld\n",i,ab,cb,cb+ab,as,cs,cs+as,ai,ci,ai+ci);
	}

	long nr;
	for(i=100;i<=10000;i+=100)
		{abs=0;cbs=0;ass=0;css=0;ais=0;cis=0;
			for(c=1;c<=5;c++)
				{ab=0;cb=0;as=0;cs=0;ai=0;ci=0;
					nr=0;
					for(j=1;j<=i;j++)
		
						{
							nr++;
						bini[nr]=bins[nr]=binb[nr]=rand();
			
						}
		
		BubbleSort(i,&cb,&ab,binb);
		abs+=ab;
		cbs+=cb;
		SelectionSort(i,&cs,&as,bins);
		ass+=as;
		css+=cs;
		LiniarInsertionSort(i,&ci,&ai,bini);
		ais+=ai;
		cis+=ci;
		
			}
		fprintf(averange,"%ld, %ld, %ld, %ld, %ld, %ld ,%ld ,%ld ,%ld ,%ld\n",i,abs/5,cbs/5,cbs/5+abs/5,ass/5,css/5,css/5+ass/5,ais/5,cis/5,ais/5+cis/5);
	}

/*Small test to verify the algorithms
	for(i=n;i>=1;i--)
	{in[i]=rand();
	j++;
	}
	for(i=n;i>=1;i--)
	{in[i]=j;
	j++;
	}
	for(i=1;i<=n;i++)
		cout<<in[i]<<" ";
	cout<<endl;
	SelectionSort(n,&cb,&ab,in);
	
	for(i=1;i<=n;i++)
		cout<<in[i]<<" ";*/
	fclose(worst);
	fclose(best);
	fclose(averange);
	//getch();
	return 0;
}