/* 
***ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER*** sg 2
Tema 5



Tabela de dispersie

- este vorba despre un tabel al carei elemente sunt un pointer catre o lista
- cautarea si inserarea este foarte rapida folosind aceasta metoda deoarece
folosim un camp cheie
-  prin acest camp avem acces la toate elementele vectorului 






*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

FILE *f;


int hash[10001], gasit[10001], negasit[10001], aux[10001];

int s = 1500, N = 10001;

float factumpl[] = {0.80, 0.85, 0.9, 0.96, 0.99};
float efort;
float efortGasit,efortNegasit,efortGasitMediu,efortNegasitMediu;
float gasitAux, negasitAux;
float gasitToate, negasitToate;


int h(int k,int i){
	int aux = (k%N+i+(i*i))%N;
	return aux;
}



int inserare(int k)
{
	int i=0;
	int j;
	efort = 0;
	do{
		j=h(k,i);
		if(hash[j] == k)
			return 0;
		if(hash[j]==NULL){
			hash[j] = k;
			return j;
		}else{
			i++;
			efort++;
		}
	}while(i!=N);
	
	return 0;
}

int cautare(int k)
{
	int i = 0;
	efort = 0;
	int j;
	do{
		j = h(k,i);
		if(hash[j]==k){ 
			return j;
		}
		i++;
		efort = (float)i+1;
	}while(hash[j]!=NULL && i!=N);
	return 0;
}

void afisare(int hash[],int N)
{
	int i=0;
	for(i=0;i<N;i++)
	{
		printf("%d \n", i);
		if(hash[i]!=0){
			printf("%d \n", hash[i]);
		}
	}
}

int generare(int n)
{
	int x = (1 + rand()*rand())%n;
	return x;
}

void gasitGenerat(int n)
{
	int i;
	for(i=0;i<s;i++){
		int x=rand()%n;
		while(aux[x] ==0)
			x=rand()%n;

		gasit[i]=aux[x];
		aux[x] = 0;
	}
}

void negasitGenerat(int n)
{
	int i;
	for(i=1;i<s;i++){
		negasit[i]= n+rand();
	}
}

void main()
{
	f= fopen("data.csv", "w");

	int nr=0;
	float alfa;
	int x,i,init;
	srand(time(NULL));
	

	/*N=10;
	s = N/3;
	nr = alfa = x = 0;

	for(int q = 0;q<N;q++)  //init
		hash[q] = 0;
	
	for(i=0;i<=s;i++)
	{
		gasit[i]=0;
	}

	init=0;

	do
	{ 
		x=generare(200);
		printf("\n element generat  = %d",x);
		if(cautare(x)==0){
		int key = inserare(x);
		printf("\n numarul generat : %d si pus pe pozitia %d",x,key);
			if(key)
			{
				nr++;
				if(nr %3 ==0)
				{
					gasit[nr/3] = x;
				}
			}
		}
		alfa = (float) nr/N;
	}while(alfa<0.85);
	
	afisare(hash,N);
	
	negasitGenerat(10);

	int j;
	for(j=0;j<s;j++){
		
		printf("\n elementul %d s-a gasit :\n - pe pozitia %d \n - cu efortul %d",gasit[j], cautare(gasit[j]), efortGasit);
		efortGasit = efort;
		gasitAux = gasitAux + efortGasit;
		if(efortGasit>efortGasitMediu)
		{
			efortGasitMediu = efortGasit;
		}
		efort = 0;
		
		printf("\n elementul %d nu s-a gasit :\n - efortul cautarii %d",negasit[j], efortNegasit);
		efortNegasit = efort;
		negasitAux = negasitAux + efortNegasit;
		if(efortNegasit>efortNegasitMediu){
			efortNegasitMediu = efortNegasit;
		}
		efort = 0;
	}
	
	printf("Factor de umplere \n Efortul mediu pentru elemente gasite \n Efortul maxim pentru elemente gasite  \n Efortul mediu pentru elemente negasite \n Efortul maxim pentru elemente negasite \n",(float)alfa,(float)gasitAux/s,(float)efortGasitMediu,(float)negasitAux/s,(float)efortNegasitMediu);
	
	///////////////////////////////////////////////////////
	*/

	fprintf(f, "FactorUmplere, EfortGasitMediu, EfortGasitMaxim, EfortNegasitMediu, EfortNegasitMaxim\n");
	
	N = 10001;
	for(i=0;i<5;i++)
	{
		int k;
		for(k=0;k<5;k++)
		{
			for(int q = 0;q<N;q++)  //init
				hash[q] = 0;

			alfa = nr = 0;
			do{
				x = generare(1000000);
				if(cautare(x)==0){
					if(inserare(x))
					{
						nr++;
						aux[nr] = x;
						init = x;
					}
			}
			alfa = (float) nr/N;
			}while (alfa<factumpl[i]);
			
			gasitGenerat(nr);
			negasitGenerat(100000);

			int j;
			for(j = 0;j<s;j++){
				
				cautare(gasit[j]);
				efortGasit = efort;
				gasitAux += efortGasit;
				if(efortGasit>efortGasitMediu){
					efortGasitMediu = efortGasit;
				}
				efort = 0;

				cautare(negasit[j]);
				efortNegasit = efort;
				negasitAux += efortNegasit;
				if(efortNegasit>efortNegasitMediu){
					efortNegasitMediu = efortNegasit;
				}
				efort = 0;
			}
			gasitToate = gasitToate + (float)gasitAux/s;
			negasitToate = negasitToate + (float)negasitAux/s;


			gasitAux = 0;
			negasitAux =0;
		}

		fprintf(f,"%d, %d, %d, %d, %d \n",factumpl[i], gasitToate/5, efortGasitMediu, negasitToate/5, efortNegasitMediu);

		nr=0;
		efortNegasitMediu =0;
		gasitToate =0;
		negasitToate =0;
		efortGasit =0;
		efortNegasit =0;
	}
	

}