/**
***ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***
Graficul in cazul de contruire bottomUp creste liniar
**/
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#define max 10000
#define val 5
int n;
int heapSize;
int compbottom,atrbottom;
int parinte(int i)
{
	return i/2;
}
int stanga(int i)
{
	return 2*i;
}
int dreapta(int i)
{
	return 2*i+1;
}
void maxHeapify(int *a,int i,int heapsize)
{	
	int largest;
	int l=stanga(i);
	int r=dreapta(i);
	compbottom=compbottom+3;
	if (l<=heapSize && a[l]>a[i])
		largest = l;
	else largest = i;
	if (r<=heapSize && a[r]>a[largest])
		largest= r;
	if (largest!=i)
	{
		int change=a[i];
			a[i]=a[largest];
			a[largest]=change;
			atrbottom=atrbottom+3;
			maxHeapify(a,largest,heapsize);
	}
}
//insereaza valori in heap
void inserareValori(int *a,int n)
{
	srand(time(NULL));
	for(int i=1;i<=n;i++)
		a[i]=rand()%20;
}
//afiseaza valori din heap
void afisareValori(int *a,int n)
{	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
}

void bottomUpHeap(int *a,int n)
{int heapsize;
	heapSize=n;
	for(int i=heapSize/2;i>0;i--)
	{
		maxHeapify(a,i,heapSize);
	}
}
void main()
{
	int m=9;
	FILE *f=fopen("mediustatistic.csv","w");
	fprintf(f,"compbotom, atrbottom\n");
	int *c=(int*)malloc(sizeof(int)*m);
	inserareValori(c,m);
	afisareValori(c,m);
	bottomUpHeap(c,m);
	afisareValori(c,m);
	free(c);
	for(n=100;n<=10000;n=n+100)
	{
	int *a=(int*)malloc(sizeof(int)*n);
	heapSize=n-1;
		for(int i=0;i<5;i++)
		{
			inserareValori(a,heapSize);
			bottomUpHeap(a,heapSize);
			
		}
		
		
		fprintf(f,"%d,%d\n",n,compbottom/val,atrbottom/val);
		compbottom=0;
		atrbottom=0;
		free(a);}
	getch();
}