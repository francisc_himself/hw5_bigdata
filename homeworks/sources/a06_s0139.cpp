/*
***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***
17 Mai 2013
Tema 6. Algoritmul lui Josephus

Algoritmul lui josephus este folosit pentru a implementa arbori binari echilibrati cu adancime. acestia sunt extrem de folositori deoarece
fiecare nod are asignat un camp care ne poate ajuta sa il gasim foarte usor al catelea este in lista. Complexitatile accesarilor, inserarilor
si stergerilor sunt O(log(n)), fata de o lista la care sunt O(n), deci structura este eficienta.

Algoritmul lui Josephus are complexitatea totala de O(nlog(n)) deoarece trebuie parcurse toate nodurile

*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include "Profiler.h"

#define NMIN 100
#define NMAX 10000
#define INC 100

struct Nod {
	int dim;
	int key;
	struct Nod *st,*dr,*p;
};

Nod *arbore;

Profiler profiler("Josephus");

	int n;


Nod* construireArboreEchilibrat(int st, int dr){
	int m, dd, ds;
	Nod *newn;
	profiler.countOperation("operatii",n);
	if (st<=dr){
		m = (st+dr)/2;
		newn = (Nod*)malloc(sizeof(Nod));
		newn->key = m;
		newn->st = construireArboreEchilibrat(st,m-1);
		newn->dr = construireArboreEchilibrat(m+1,dr);
		if(newn->st == NULL){
			ds = 0;
		}
		else{
			ds = newn->st->dim;
			newn->st->p = newn;
		}
		if(newn->dr == NULL){
			dd = 0;
		}
		else{
			dd = newn->dr->dim;
			newn->dr->p = newn;
		}
		newn->dim = ds + dd + 1;
		newn->p = NULL;
		return newn;
	}
	else{
		return NULL;
	}
}

void afisareArbore(Nod *nod, int depth){
	int level;
	if(nod == NULL){
	return;
	}
	afisareArbore(nod->st,depth+1);
	for(level=0; level<depth; level++)
		printf("       ");
	printf("%d:%d\n",nod->key,nod->dim);
	afisareArbore(nod->dr,depth+1);
}

Nod* OSSelect(Nod *nod, int ind){
	int cur;
	profiler.countOperation("operatii",n);	
	if(nod != NULL){
		if(nod->st!= NULL){
			cur = nod->st->dim + 1;
		}
		else{
			cur = 1;
		}

		if(cur == ind){
			return nod;
		}
		else{
			if( ind < cur){
				return OSSelect(nod->st,ind);
			}		
			else{
				return OSSelect(nod->dr,ind-cur);
			}
		}
	}
	return NULL;
}

void OSDelete(int ind){
	int dimCur,dims,dimd;
	Nod *pivot,*x,*y,*nod;
	nod = arbore;
	if(nod->st!= NULL){
			dimCur = nod->st->dim + 1;
		}
		else{
			dimCur = 1;
		}
		
	while (ind!=dimCur){	
		if (ind<dimCur){
			nod = nod->st;
			profiler.countOperation("operatii",n);
	
		}
		else{
			ind = ind - dimCur;				
			nod = nod->dr;
			profiler.countOperation("operatii",n);
	
		}
		if(nod->st!= NULL){
		dimCur = nod->st->dim + 1;
		}
		else{
			dimCur = 1;
		}
	}
	
	if (nod->st == NULL || nod->dr == NULL){
		y = nod;
				
	}
	else{
		y = nod->dr;
		while (y->st != NULL){
			y = y->st;
					profiler.countOperation("operatii",n);
		}
	}
	if (y->st !=NULL){
		x = y->st;
	}
	else{
		x = y->dr;
	}
			profiler.countOperation("operatii",n);
	if (x !=NULL){
		x->p = y->p;
	}
	if (y->p ==NULL){
		arbore = x;
	}
	else{
		if(y==y->p->st){
			y->p->st = x;
		}
		else{
			y->p->dr = x;
		}
				profiler.countOperation("operatii",n);
	}

	if(y != nod){
		nod->key = y->key;
	}

	x = y;
	while(x!=NULL){
		if(x->st == NULL){
			dims = 0;
		}
		else{
			dims = x->st->dim;
		}
		if(x->dr == NULL){
			dimd = 0;
		}
		else{
			dimd = x->dr->dim;
		}
		x->dim = dims+dimd+1;
		x = x->p;
	}
	free(y);
}

void Josephus(int n, int m,int ok){
	int j,k;
	Nod *x;
	arbore = construireArboreEchilibrat(1,n);	
	if(ok==1){	
	afisareArbore(arbore,1);
	}
	j = 1;
	for(k = n; k>=1; k--){
		j = ((j+m-2) % k) + 1;
		x = OSSelect(arbore,j);
		if(ok==1){
			printf("\n%d \n",x->key);}
		OSDelete(j);
		if(ok==1){
			afisareArbore(arbore,1);}
	}
}

int main(){
	
	Josephus(7,3,1);
	for(n = NMIN; n<=NMAX; n+=INC){
		printf("%d\n",n);
		Josephus(n,n/2,0);
	}
	profiler.showReport();
}