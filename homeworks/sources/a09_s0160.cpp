/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Breadth-First Search
	La explorarea in latime, dupa vizitarea varfului initial, se exploreaza toate varfurile adiacente lui, se trece apoi la 
primul varf adiacent si se exploreaza toate varfurile adiacente acestuia si neparcurse inca, s.a.m.d. Fiecare varf se parcurge
cel mult o data.

Eficienta:
	Pentru a parcurge un graf cu n varfuri si m muchii timpul este in: 
		i) O(n+m), daca reprezentam graful prin liste de adiacenta;
		ii) O(n^2), daca reprezentam graful printr-o matrice de adiacenta.
In cazul nostru eficienta este O(n+m), deoarece am folosit liste de adiacenta.
Aceasta liniaritate a algoritmului in functie de numarul de muchii si de varfuri rezulta si din cele 2 grafice.

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

//definim constante pentru culori
#define alb 0
#define ***ANONIM*** 1
#define negru 2
#define infinit 99999

//structura unui nod din graf
typedef struct _VARF
{
	int culoare;
	int d;
	int	parinte;
}VARF;

//structura nod din lista
typedef struct _NOD
{
    int cheie;
    struct _NOD* urm;
}NOD;

NOD *prim[10000], *ultim[10000];
VARF varf[10000];
int nr_elementeCoada;
int Coada[10000];
int nr_operatii=0;

//verifica daca 2 noduri au aceeasi cheie
bool same(int x, int y)
{
	NOD *p;
	
	p = prim[x];
	while(p!= NULL)
	{
		nr_operatii++;
		if(y == p->cheie)
			return false;
		p = p->urm;
	}
	nr_operatii++;
	
	return true;
}

//metoda de traversare in latime a grafului
void BFS (int nod_start, int nr_varfuri)
{
	int u;
	int i;
	
	nr_elementeCoada = 0;
	nr_operatii = 0;
	//pt fiecare nod diferit de nodul de start setam atributele
	for (u = 1; u <= nr_varfuri; u++)
		if (u != nod_start)
		{
			varf[u].culoare = alb;
			varf[u].d = infinit;
			varf[u].parinte = 0;
			nr_operatii++;
		}
	nr_operatii++;
	varf[nod_start].culoare = ***ANONIM***;
	varf[nod_start].d = 0;
	varf[nod_start].parinte = 0;
	nr_elementeCoada++;
	Coada[nr_elementeCoada] = nod_start; //punem in coada nodul de start
	
	while(nr_elementeCoada != 0) //cat timp coada nu e vida
	{
		NOD *p;
		int v;

		//scoatem primul element din coada
		u = Coada[1];
		nr_operatii++;
		printf("Nod ales: %d\n",u);
		for ( i = 1; i < nr_elementeCoada; i++)
			Coada[i] = Coada[i+1];
		nr_elementeCoada--;
		
		//facem pentru fiecare varf vecin cu u
		p = prim[u];
		nr_operatii++;
		while (p!=NULL)
		{
			nr_operatii++;
			v = p->cheie;
			p = p->urm;
			if (varf[v].culoare == alb)
			{
				varf[v].culoare = ***ANONIM***;
				varf[v].d = varf[u].d + 1;
				varf[v].parinte = u;
				nr_elementeCoada++;
				Coada[nr_elementeCoada] = v;  //adaugam elemente la coada
			}
		}
		varf[u].culoare = negru;
		nr_operatii++;
	}
}

//functie de creare a listei de adiacenta pentru noduri
void creareListaAdiacenta(int nr_varfuri,int nr_muchii)
{
	int i;
	int	u,v;

	//initializam cu null pt toate varfurile
	for(i = 0; i <= nr_varfuri; i++)
		prim[i] = ultim[i] = NULL;

	i = 1;
	while (i <= nr_muchii)
	{
		//generam arbitrar extremitatile
		u = rand()%nr_varfuri+1;
		v = rand()%nr_varfuri+1;
		if (u != v)
		{
				//initializam nodul prim
				if(prim[u] == NULL)
				{
					prim[u] = (NOD*)malloc(sizeof(NOD*));
					prim[u]->cheie = v;
					prim[u]->urm = NULL;
					ultim[u] = prim[u];
					i++;
				}
				else 
				{
					//initializam nodul ultim
					if ((same(u,v) == true) && (same(v,u) == true)) //daca nodurile u si v sunt diferite
					{
							NOD *p;
							p = (NOD *)malloc(sizeof(NOD *));
							p->cheie = v;
							p->urm = NULL;
							ultim[u]->urm = p;
							ultim[u] = p;
							i++;
					}
				}
		}
	}
}

//functia de afisare 
void print(int nr_varfuri)
{
	int i;
	NOD *p;
	for(i = 1; i <= nr_varfuri; i++)
	{
		printf("%d : ",i);
		p = prim[i];
		while(p != NULL)
		{
			printf("%d,",p->cheie);
			p = p->urm;
		}
		printf("\n");
	}
}

//functia principala main
int main()
{
	FILE *f_varf_const;
	FILE *f_muchie_const;
	int i;
	int nr_muchii = 0;
	int nr_varfuri = 0;

	f_varf_const = fopen("varf_constant.csv","w");
	f_muchie_const = fopen("muchie_constant.csv","w");
	
	srand(time(NULL));
	
	//pentru un caz arbitrar
	/*nr_varfuri = 6;
	nr_muchii = 11;
	
	creareListaAdiacenta(nr_varfuri, nr_muchii);
	printf("\nLista de adiacenta este:\n");
	print(nr_varfuri);
	printf("\nBFS:\n");
	BFS(1, nr_varfuri);
	*/

	//--------------------------pe caz general --------------------------------------//
	//pt nr de varfuri constante=100 si variam numarul de muchii de la 1000 la 5000 cu un increment de 100
	nr_varfuri = 100;
	for(i = 1000; i < 5000; i += 100)
	{
		printf("i=%d\n",i);
		creareListaAdiacenta(nr_varfuri,i);
		BFS(1,nr_varfuri);
		fprintf(f_varf_const,"%d,%d\n",i,nr_operatii);
	}
	
	
	//pt nr de muchii constante=9000 si variam numarul de varfuri de la 100 la 200 cu un increment de 10
	nr_muchii = 9000;
	for(i = 150; i <= 200; i += 10)
	{
		printf("i=%d\n",i);
		creareListaAdiacenta(i,nr_muchii);
		BFS(1,i);
		fprintf(f_muchie_const,"%d,%d\n",i,nr_operatii);
		nr_operatii = 0;
	}
	
	fclose(f_varf_const);
	fclose(f_muchie_const);

	_getch();
    return 0;
}
