/**
Student: ***ANONIM*** Diana ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: Analysis & comparison of bottom-up and top-down build heap approaches
Comments:
  AVERAGE CASE:
   -Input data: an array with random elements; 5 measurements and the graph represents the average of the 5 measurements
   -Comparison:
		Top-down: linearly, faster;
		Bottom-up: linearly, slower then top-down;
  WORST CASE:
   -Input data: for bottom-up is an increasing array of elements; for top-down is a decresing array of elements;
   -Comparison:
		Top-down: linearly, much faster;
		Bottom-up: linearly, slower than top-down;the difference is very obvious here;
Running time: Bottom-up: O(n)
			  Top-down: O(nlgn)
  **/
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
long nr=0;
int heapsize=0;
long a[10001],b[10001];

int parent(int i)
{
	return i/2;
}
void increase_key(int i,int key)
{
	if (key<b[i])
		printf("Key smaller than current key");
	nr++;
	b[i]=key;
	nr++;
	while ((i>1)&&(b[parent(i)]<b[i]))
	{
		int aux=b[i];
		b[i]=b[parent(i)];
		b[parent(i)]=aux;
		i=parent(i);
		nr+=3;
		nr++;
	}
	nr++;
}
void heap_insert(int key)
{
	heapsize=heapsize+1;
	b[heapsize]=-INT_MAX;
	nr++;
	increase_key(heapsize,key);
}
int right(int i)
{
	return 2*i+1;
}
int left(int i)
{
	return 2*i;
}
void max_heapify(int i)
{
	int largest,l=left(i),r=right(i);
	if ((l<=heapsize)&&(a[l]>a[i]))
		largest=l;
	else largest=i;
	nr++;
	if ((r<=heapsize)&&(a[r]>a[largest]))
		largest=r;
	nr++;
	if (largest!=i)
	{
		int aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		max_heapify(largest);
		nr+=3;
	}
}
void bottom_up(int n)
{
	nr=0;
	heapsize=n;
	for(int i=n/2;i>=1;i--)
		max_heapify(i);
}
void top_down(int n)
{
	nr=0;
	heapsize=1;
	for(int i=2;i<=n;i++)
		heap_insert(b[i]);
}
void pretty_print(long T[],int i,int depth)
{
	if(i<=heapsize)
	{ 
		pretty_print(T,(2*i+1),(depth+1));
		for(int j=1;j<=depth;j++) printf("	");
			printf("%d", T[i]);
		
	printf("\n");
	
	pretty_print(T,2*i,(depth+1));
	
	}

}
int main()
{
	int n;
	srand(time(NULL));
	/**
	FILE *f1,*f2;
	f1=fopen("avg.csv","w");
	f2=fopen("worst.csv","w");
	fprintf(f1,"n,c_td+a_td,c_bu+a_bu\n");
	fprintf(f2,"n,c_td+a_td,c_bu+a_bu\n");
	
	for(int n=100;n<=10000;n=n+100)
	{
		//Average Case
		int total[2];
		total[0]=0;total[1]=0;
		for (int j=0;j<5;j++)
		{
			for(int i=1;i<=n;i++)
			{
				a[i]=rand()%10000;
				b[i]=a[i];
			}
			heapsize=0;
			top_down(n);
			total[0]=total[0]+nr;
			heapsize=0;
			bottom_up(n);
			total[1]=total[1]+nr;
		}
		total[0]/=5;
		total[1]/=5;
		fprintf(f1,"%d,%d,%d\n",n,total[0],total[1]);

		//Worst Case
		for(int i=1;i<=n;i++)
			b[i]=i;
		top_down(n);
		fprintf(f2,"%d,%d,",n,nr);
		for(int i=1;i<=n;i++)
			a[i]=i;
		bottom_up(n);
		fprintf(f2,"%d\n",nr);
	}
	fclose(f1);
	fclose(f2);
	**/
	n=10;
	for(int i=1;i<=n;i++)
	{
			a[i]=rand()%20;
			b[i]=a[i];
	}
	
	printf("Vector randomizat:\n");
	for(int i=1;i<=n;i++)
			printf("%d ",a[i]);
			
	printf("\nTop-down:\n");
	
	top_down(n);
	pretty_print(b,1,0);
	
	printf("\nBottom-up:\n");
	
	bottom_up(n);
	pretty_print(a,1,0);
	
	getch();
	
}
