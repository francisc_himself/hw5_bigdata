#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>



FILE *f;
#define M 60000
#define N 10000
 int call;

struct NodeT{

   
    int key;
	int rank;
	NodeT *parent;

};

struct Vertex{
	int key;
	NodeT* disj;

};

struct Edge{
	Vertex* v1;
	Vertex* v2;

};


Vertex* vertices[N];
Edge* edges[M];

void makeSet(NodeT* x)
{
	x->parent=x;
	x->rank=0;
}

void link(NodeT* x, NodeT* y)
{
	if (x->rank>y->rank)
		y->parent=x;
	else {
		x->parent=y;
		if (x->rank==y->rank)
			y->rank=y->rank+1;
	}
}

NodeT* findSet(NodeT* x)
{
	if (x!=x->parent)
		x->parent=findSet(x->parent);
	return x->parent;

}

void uni(NodeT* x, NodeT* y)
{
	link(findSet(x),findSet(y));
	call+=2;
}

bool sameComp2(NodeT* u, NodeT* v)
{
	if(findSet(u)==findSet(v))
		return true;
	else return false;
}

int sameComp(Vertex* u, Vertex* v)
{
	if(findSet(u->disj)==findSet(v->disj))
		return 1;
	else return 0;
}

void connectedComp(int m)
{
	for(int i=0; i<N; i++)
	{
		makeSet(vertices[i]->disj);
		call++;
	}
	for(int i=0; i<m; i++)
	{
		if(edges[i]->v1!=NULL && edges[i]->v2!=NULL)
		if(findSet(edges[i]->v1->disj)!=findSet(edges[i]->v2->disj))
		{
			uni(edges[i]->v1->disj, edges[i]->v2->disj);
			call++;
		}
		call+=2;
	}


}

void randomGraph(int m)
{
	int temp1;
	int temp2;

	for(int i=0; i<m; i++)
	{	
		temp1=rand()%N;
		edges[i]->v1=vertices[temp1];
		do
		{
			temp2=rand()%N;
		} while(temp1==temp2); 
		edges[i]->v2=vertices[temp2];

	}

}

void main(){
	srand(time(NULL));
	f=fopen("out.txt", "w");

	for(int i=0; i<N; i++)
	{
		vertices[i]=new Vertex;
		vertices[i]->key=i;
		vertices[i]->disj=new NodeT;
	}
/*
	for(int i=0; i<M; i++)
	{
		edges[i]=new Edge;
		edges[i]->v1=NULL;
		edges[i]->v2=NULL;
		
	}
	
	edges[0]->v1=vertices[0];
	edges[0]->v2=vertices[1];

	edges[1]->v1=vertices[0];
	edges[1]->v2=vertices[2];

	edges[2]->v1=vertices[1];
	edges[2]->v2=vertices[2];

	edges[3]->v1=vertices[1];
	edges[3]->v2=vertices[3];

	edges[4]->v1=vertices[4];
	edges[4]->v2=vertices[5];

	edges[5]->v1=vertices[4];
	edges[5]->v2=vertices[6];
	
	edges[6]->v1=vertices[7];
	edges[6]->v2=vertices[8];

	for(int i=0; i<7; i++)
	{
		printf("%d, %d\n", edges[i]->v1->key, edges[i]->v2->key);

	}

	connectedComp(1000);

	printf("%d\n", sameComp(vertices[0],vertices[1]));
	printf("%d\n", sameComp(vertices[0],vertices[2]));
	printf("%d\n", sameComp(vertices[1],vertices[2]));
	printf("%d\n", sameComp(vertices[1],vertices[3]));
	printf("%d\n", sameComp(vertices[1],vertices[4]));
	printf("%d\n", sameComp(vertices[8],vertices[9]));
	printf("%d\n", sameComp(vertices[7],vertices[8]));
	*/
	
	fprintf(f, "edges,calls\n");

	for (int n=10000; n<60000; n+=1000)
	{
		call=0;
		for(int i=0; i<n; i++)
		{
			edges[i]=new Edge;
			edges[i]->v1=NULL;
			edges[i]->v2=NULL;
		
		}
		randomGraph(n);
		connectedComp(n);
		//printf("%d\n",n);
		fprintf(f, "%d,%d\n",n,call);

	}
	
}