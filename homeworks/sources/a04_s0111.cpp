#include <stdio.h>
#include <conio.h>
#include "Profiler.h"
#define LEFT(i) 2*i+1;
#define RIGHT(i) 2*i+2;

/*
 Merge k sorted lists algorithm is based on using heap structure.
 Firstly we create k sorted lists with equal number of elements,then apply merge function on them 
to generate a sorted array with n elements (n=k*dim of each list).
 Merge function takes successively first element from each list and builds a min-heap; first element is the smallest and is put on the new array, then
is found the position of the element in the list and deleted; in the heap is pushed a next element from the same list.
The procedure is repeated till heap will be empty(n times).
 A special remark: when the first list become empty, that place is exchanged with the last list, and the number of lists is decremented;
if the empty one is the last one, only the size is decremented; after that the first element on the heap is popped and the procedure repeats.
 For each push of an element in the heap we spend O(log k) time.Since there are n such pushes the total time needed for merging is O(nlogk).
 In the average case, when n is the variable, we observe a liniar complexity and when k varies complexity is logk which shows overall O(nlogk) complexity
of the algorithm.
*/

Profiler profiler("Merge_K_Lists");

typedef struct list {
	int num;
	struct list *next;
} LIST;

typedef struct heap {
	int key;
	int lpos;
} HEAP;


LIST* head(LIST *l) {
	LIST *p=l;
	if (p==NULL)
		return NULL;
	else
		while(p->next!=NULL)
			p=p->next;
	return p;
}


void insert(LIST **l,int el) {
	LIST **p;
	if (*l==NULL) {
		*l=(LIST*)malloc(sizeof(LIST));
	    (*l)->num=el;
		(*l)->next=NULL;
	} else {
		p=(LIST**)malloc(sizeof(LIST*));
		*p=head(*l);
		(*p)->next=(LIST*)malloc(sizeof(LIST));
		(*p)->next->num=el;
		(*p)->next->next=NULL;
	}
}


void minHeapify(HEAP a[],int i,int size,int *op) {
   HEAP temp;
   int l,r,smallest;
   l=LEFT(i);
   r=RIGHT(i);
  
  (*op)++;
  if (l<size && a[l].key<a[i].key ) 
	smallest=l;
  else 
	smallest=i;
 
  (*op)++;
  if ( r<size && a[r].key<a[smallest].key )  
	smallest=r;
 
  if ( smallest != i ) {
	  temp=a[i];
	  a[i]=a[smallest];
	  a[smallest]=temp;
	  *op+=3;
	  minHeapify(a,smallest,size,op);
  }
}


void buildHeap(HEAP a[],int size,int *op) {
	 for (int j=(size/2)-1;j>=0;j--) {
		minHeapify(a,j,size,op);	
	 }
}


void merge(int arr[],LIST *liste[],int k,int n,int *op) {
	HEAP a[2000];
	int size=0,j=0,t=0,pos,dim=n/k,len=k;
	for (int i=0;i<len;i++) {
		*op=*op+2;
		a[i].key=liste[i]->num;
		a[i].lpos=i;
	}
	buildHeap(a,len,op);
	while (t<n-1) {
		*op=*op+2;
		arr[j++]=a[0].key;
		pos=a[0].lpos;		
		if (liste[pos]->next!=NULL) {
			liste[pos]=liste[pos]->next;
			a[0].key=liste[pos]->num;
			*op=*op+2;
		} else {
			a[0]=a[len-1];
			len--;
			(*op)++;	
		}	
		minHeapify(a,0,len,op);
		t++;
	}
	(*op)++;
	arr[j]=a[0].key;
}

void setNull(LIST *l[],int k) {
	for(int i=0;i<k;i++)
		l[i]=NULL;
}

int main() {
/*	LIST *l[3],*l1[5],*l2[10],*l3[100];
	int a[2000];
	int b[10000],c[10000],d[10000];
	int dim,op1=0,op2=0,op3=0,sum1=0,sum2=0,sum3=0;
	for (int n=100;n<=10000;n+=100) {
		sum1=sum2=sum3=0;
		for (int i=0;i<5;i++) {
			op1=op2=op3=0;
			setNull(&(*l1),5);
			setNull(&(*l2),10);
			setNull(&(*l3),100);

			dim=n/5;
			for (int i=0;i<5;i++) {
				FillRandomArray(a,dim,10,10000,false,1);
				for(int j=0;j<dim;j++)
					insert(&l1[i],a[j]);
			}
	
			dim=n/10;
			for (int i=0;i<10;i++) {
				FillRandomArray(a,dim,10,10000,false,1);
				for(int j=0;j<dim;j++)
					insert(&l2[i],a[j]);
			}
		
			dim=n/100;
			for (int i=0;i<100;i++) {
				FillRandomArray(a,dim,10,10000,false,1);
				for(int j=0;j<dim;j++)
					insert(&l3[i],a[j]);
			}
		
		   merge(b,l1,5,n,&op1);
		   merge(c,l2,10,n,&op2);
		   merge(d,l3,100,n,&op3);

			if (IsSorted(b,n))
				printf(" yes");
			else
				printf(" no");

			if (IsSorted(c,n))
				printf(" yes ");
			else
				printf(" no "); 
	

			if (IsSorted(d,n))
				printf(" yes ");
			else
				printf(" no ");
		   sum1+=op1;
		   sum2+=op2;
		   sum3+=op3;
		}
		profiler.countOperation("List5",n,sum1/5);
		profiler.countOperation("List10",n,sum2/5);
		profiler.countOperation("List100",n,sum3/5);
	}

	//profiler.createGroup("average_case_n","List5","List10","List100");
	profiler.showReport();*/
	
	int p[1000],c[1000],r[1000],b[100],op;
	LIST *l[2];
//	int a[10000]={1,5,8,10,11,14,21,22,24,26,29,30,32,34,35,38,39,41,43,50};
//	int c[100]=  {51,52,54,56,57,58,59,60,61,62,75,78,79,83,84,85,88,90,97,99};
//	int r[10000]={-30,105,108,110,111,114,122,124,126,129,130,132,134,135,138,139,140,143,150,899};
	setNull(&(*l),3);
	FillRandomArray(p,20,10,100,false,1);	
	FillRandomArray(r,20,10,100,false,1);	
	FillRandomArray(c,20,10,100,false,1);

	
	for(int i=0;i<20;i++)
		insert(&l[0],p[i]);

	for(int i=0;i<20;i++)
		insert(&l[1],c[i]);

	for(int i=0;i<20;i++)
		insert(&l[2],r[i]);

	printf("First:\n");
	for (LIST *i=l[0];i!=NULL;i=i->next) 
		printf("%d ",i->num);
	printf("\nSecond:\n");
	for (LIST *i=l[1];i!=NULL;i=i->next) 
		printf("%d ",i->num);
	printf("\nThird:\n");
	for (LIST *i=l[2];i!=NULL;i=i->next) 
		printf("%d ",i->num);

	merge(b,l,3,60,&op);



	printf("\nMerged\n\n");
	for (int i=0;i<60;i++)
		printf("%d ",b[i]);

	if (IsSorted(b,60)) 
		printf("\n\nYES\n\n");
	else 
		printf("\n\nNO\n\n");
	getch();
	
/*	LIST *list[1000];
	int rez[10000],a[1000];
	int n=10000,op=0,sum=0,dim;


	for (int k=10;k<=500;k+=10) {
		sum=0;
		for (int p=0;p<5;p++) {
			op=0;
			setNull(&(*list),k);
			for (int i=0;i<k;i++) {
				dim=floor(n/k);
				FillRandomArray(a,dim,10,10000,false,1);
				for(int j=0;j<dim;j++)
					insert(&list[i],a[j]);	
			}
			merge(rez,list,k,dim*k,&op);
			sum+=op;

			if (IsSorted(rez,dim*k)) printf("YES ");
			else printf("NO ");
		}
		profiler.countOperation("List",k,sum/5);
	}
	profiler.showReport();*/

return 0;
}