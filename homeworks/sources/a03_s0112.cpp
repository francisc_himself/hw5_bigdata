#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int heapsize=0;
int A=0;
int C=0;


int left(int k){
	return 2*k;
}
int right(int k){
	return 2*k+1;
}
int parent(int k){
	return k/2;
}

void swap(int &a, int &b){
int aux=a;
a=b;
b=aux;
}

//heap sort bottom up----------------------------------------
/*method that takes a node and knowing that its two aubtrees are heaps creates
a new heap*/

void heapify(int *a, int i){
int largest=i;
int aux;
int l=left(i);
int r=right(i);
C++;
if((l<=heapsize)&&(a[l]>a[i])) largest=l;
						  else largest=i;
C++;
if((r<=heapsize)&&(a[r]>a[largest])) largest=r;
C++;
if(largest!=i){
	A=A+3;
	swap(a[largest],a[i]);
	heapify(a,largest);
}
}
/*method that builds a heap starting from its leaves, that are already heaps
and goes up to the root by calling heapify*/

void build_heap(int *a,int n){
	for(int i=n/2;i>0;i--){
		heapify(a,i);
}
}
/*method that sorts an array building a heap and using the heap property,
and reconstructs the heap at every step with heapify*/

void heap_sort(int *a, int n){
A=0;
C=0;
heapsize=n;
build_heap(a,n);
for(int i=n;i>=2;i--){
	swap(a[1],a[i]);
	A=A+3;
	heapsize--;
	heapify(a,1);
}
}


//QUICK SORT
int partition(int v[], int p, int r){
 A++;
 int x=v[p];
 int i=p-1;
 int j=r+1;
 int aux;
 while (true){
	do{
		j--;
		C++;
	}while(v[j]>x);
	do{
		i++;
		C++;
	}while(v[i]<x);
	if(i<j){
		A=A+3;
		aux=v[i];
		v[i]=v[j];
		v[j]=aux;
	}
	else return j;
 
 }
}
void quick_sort(int v[],int p,int r){
	int q=0;
	if(p<r){
		q=partition(v,p,r);
		quick_sort(v,p,q);
		quick_sort(v,q+1,r);
	}
}
//----------------------------------------


//array functions
void array_average(int v[], int n){
int i;
for(i=1;i<n+1;i++){
v[i]=rand()%n;
}
}

void print_array(int v[],int n){
int i;
for(i=1;i<n+1;i++)
	printf("%d ",v[i]);
printf("\n");
printf("\n");
printf("\n");

}
/*
b<-a;
*/
void copy_array(int a[], int b[], int n){
	for(int i=1;i<=n;i++)
		b[i]=a[i];
}



int main(){
int *a;
int n=10;
a=new int[n+1];
int *b;

FILE *f=fopen("HeapSortvs_QuickSort.csv","w");
int corect=0;
for(n=100;n<=10000;n=n+100){
	printf("%d ",n);
	fprintf(f,"%d, ",n);
	a=new int[n+1];
	b=new int[n+1];
	array_average(a,n);
	
	copy_array(a,b,n);
	heap_sort(a,n);
	fprintf(f,"%d, ",C+A);
	A=0;
	C=0;
	

	copy_array(b,a,n);
	quick_sort(a,1,n);
	fprintf(f,"%d, ",C+A);
	fprintf(f,"\n");
	
	
}

	//DEMO Heap Sort
	int array1[10]={0,8,1,6,4,5,2};
	int array2[10];
	printf("\n");
	printf("array1:");
	print_array(array1,7);
	copy_array(array1,array2, 7);
	printf("array1 after HeapSort: ");
	heap_sort(array1,7);
	print_array(array1,7);

	//DEMO Quick Sort
	copy_array(array2,array1, 7);
	printf("array1:");
	print_array(array1,7);
	printf("array1 after QuickSort: ");
	quick_sort(array1, 1, 7);
	print_array(array1, 7);




fclose(f);

getch();
return 0;
}