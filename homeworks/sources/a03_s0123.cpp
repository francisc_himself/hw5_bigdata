/*
Average Case
- the quicksort is moore efficient than heap sort
- heat sort is linear and quick sort is like a parabola
-the sorting algorithms are not stable

Best Case
- the best case is when the array contains very random numbers

Worst Case
- the worst case is when the numbers are already sorted

*/


#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include "Profiler.h"

Profiler profiler("demo");
//using namespace std;


int a[10001], v[10001], length, heapsize, n;

int left(int );
int right(int);
int parent(int);
void MAXHEAPIFY(int [],int);
void BUILDMAXHEAPBU(int []);
void HEAPSORT(int []);
int PARTITION(int [], int, int);
void QUICKSORT( int [], int, int);
void copy(int[], int[]);
void averageCase();
void bestCase();
void worstCase();

void copy(int a[], int v[])
{
	for(int i=1; i <= length; i++)
		v[i] = a[i];
}

//left leaf
int left(int i){
	return 2*i;
}

//right leaf
int right(int i){
	return 2*i+1;
}

//parent of leaf
int parent(int i){
	return i/2;
}

//BOTTOM UP method
void MAXHEAPIFY(int heap[], int i){
	int l, r, largest, help;
	l = left(i);
	r = right(i);
	profiler.countOperation("assCompBU",length);
	if(l <= heapsize && a[l] > a[i])
		largest = l;
	else 
		largest = i;
	profiler.countOperation("assCompBU",length);
	if(r <= heapsize && a[r] > a[largest])
		largest = r;
	if(largest != i)
	{
		profiler.countOperation("assCompBU",length,3);
		help = a[i];
		a[i] = a[largest];
		a[largest] = help;
		MAXHEAPIFY(a, largest);
	}
}

void BUILDMAXHEAPBU(int a[])
{
	heapsize = length;
	for(int i = length/2; i >= 1; i--)
		MAXHEAPIFY(a, i);
}

void HEAPSORT(int a[])
{
	int help;
	BUILDMAXHEAPBU(a);
	for(int i = length; i >= 2; i--)
	{
		help = a[1];
		a[1] = a[i];
		a[i] = help;
		heapsize --;
		profiler.countOperation("assCompBU",length,3);
		MAXHEAPIFY(a, 1);
	}
}

void QUICKSORT( int a[], int p, int r) {
	int q;
	if (p < r){
		q = PARTITION(a,p,r);
		QUICKSORT(a,p,q-1);
		QUICKSORT(a,q+1,r);
	}
}

int PARTITION (int a[], int p, int r){
	int j, help;
	profiler.countOperation("assCompQuick",length);
	int x=a[r];
	int i=p-1;
	for( j = p; j<=r-1; j++)
		profiler.countOperation("assCompQuick",length);
		if (a[j] <=x)
		{
			i++;
			profiler.countOperation("assCompQuick",length,3);
			help = a[i];
			a[i] = a[j];
			a[j] = help;
		}
	help = a[i+1];
	a[i+1] = a[r];
	a[r] = help;
	profiler.countOperation("assCompQuick",length,3);
return i++;
}

void averageCase()
{
	printf("Creating information for the chart.. please wait!");
	for(int i=1; i <= 5; i++)
		for(length = 100; length <= 10000; length += 100)
		{
			FillRandomArray(a,length);
			copy(v,a);
			HEAPSORT(a);
			QUICKSORT(v,0,length);

		}
	profiler.createGroup("Sorting","assCompBU", "assCompQuick");
	profiler.createGroup("Heap-Sorting","assCompBU");
	profiler.showReport();
}

//worst case for quicksort
void worstCase(){
	length=100;
	printf("Creating information for the chart.. please wait!");
	while(length <= 10000)	{
		FillRandomArray(a,length,1,10000,true,1);//already sorted
		QUICKSORT(a,0,length);
		length+=100;
	}
	profiler.createGroup("WorstCase-QuickSort","assCompQuick");
	profiler.showReport();
}

//best case for quicksort
void bestCase(){
	length=100;
	printf("Creating information for the chart.. please wait!");
	while(length <= 10000)	{
		FillRandomArray(a,length);//very random
		QUICKSORT(a,0,length);
		length+=100;
	}
	profiler.createGroup("BestCase-QuickSort","assCompQuick");
	profiler.showReport();
}



int main(){
	//averageCase();
	//worstCase();
	bestCase();
	return 0;
}