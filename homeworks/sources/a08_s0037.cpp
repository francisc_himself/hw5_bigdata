#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

// que struct
struct queue {
	int items[1024];
	int front, rear;
} q;

enum colors
{
	alb,
	gri,
	negru
}nod_colors;

int matr[201][201],d[201],p[201],flag;
colors color[201];
int at,comp;




// verif daca coada e goala sau nu
int isEmpty() {
	int i = (q.rear == q.front)?1:0;
	return i;
}

// punere in coada
void enq(int item, int length){
	if(q.rear == (length - 1))
		q.rear = 0;
	else 
		q.rear++;

	q.items[q.rear] = item;
}

//scoatere din cap
int deq(int length) {
	if(isEmpty())
		return -1;

	if(q.front == (length - 1))
		q.front = 0;
	else 
		q.front++;

	return q.items[q.front];
}




void bfs(int graph[201][201], int seed, int length) {
	
	int i = 0, u = 0, v = 0 ;

	if(seed > length) 
	{	
		flag=1;
		return;
	}
	for(u = 0; u < length; u++)
	{
		color[u] = alb;
		d[u] = length; //infinit :))
		p[u] = NULL;
		at+=3;
	}

	//notam sursa ca nod vizitat
	color[seed] = gri;
	d[seed] = 0;
	p[seed] = NULL;
	q.front = q.rear = length - 1;
	at++;

	//adaugam sursa in coada pt a verifica vecinii
	enq(seed, length);

	
	while(!isEmpty()) {
		u = deq(length);
		at++;
		
		//verif vecinii nodului
		for(v = 0; v < length; v++) {
			if(graph[u][v] > 0) {

				//daca nu a fost vizitat...il vizitam
				comp++;
				if( color[v] == alb) 
				{
					color[v] = gri;
					p[v]=u;
					d[v]=d[u]+1;
					at+=3;
					enq(v, length);
					
				}
			}
		}
		color[u] = negru;
		at++;
		printf("nod vizitat:%d \n", u+1); //se com la generarea rapoartelor :D
	}


}


void gen(int nr_muchii,int nr_varfuri)
{
	memset(matr,0,sizeof(matr));  //initializare matrice
	if(nr_varfuri*(nr_varfuri-1)/2 < nr_muchii)
	{
		flag=1;
		return;
	}

	int i=0,x,y;
	while(i<nr_muchii)
	{	
		x =rand() % nr_varfuri;
		y =rand() % nr_varfuri;
		if((matr[x][y]==0) && (x!=y) && (matr[y][x]!=1))
		{
			matr[x][y]=1;
			i++;
		}
	}
}

void test()
{
	
	int s,nr_noduri,nr_muchii;
	
	printf("nr noduri= ");
	scanf("%d",&nr_noduri);

	printf("nr muchii= ");
	scanf("%d",&nr_muchii);

	printf("nod sursa= ");
	scanf("%d",&s);

	gen(nr_muchii,nr_noduri);

	if(flag==0)
		bfs(matr, s-1, nr_noduri);
	
	int aux=flag;
	flag=2;
	for(int i = 0; i < nr_noduri; i++)
		if(matr[s][i]!=0) 
			flag=aux;
		
	if(flag==2)
		{
			printf("\nMatrice adiacenta:\n");
			for(int i = 0; i < nr_noduri; i++) 
			{
				for(int j = 0; j < nr_noduri; j++)
					printf("%d ", matr[i][j]);
				printf("\n");
			}
			printf("\nNu este vizitat niciun nod\n");
		}
	else if(flag==0)
	{
		printf("\nMatrice adiacenta:\n");
		for(int i = 0; i < nr_noduri; i++) 
		{
			for(int j = 0; j < nr_noduri; j++)
				printf("%d ", matr[i][j]);
			printf("\n");
		}

		for(int i = 0; i < nr_noduri; i++)
				printf("p%d f%d  ~~ ", p[i]+1,i+1); //verif vecotr de parinti
		printf("\n\n");

		for(int i = 0; i < nr_noduri; i++)
				printf("nod%d d%d  ~~ ",i+1 , d[i]); //verif vector de distanta
	}
	else 
		printf("datele introduse sunt gresite !!!\n mai incearca :)\n");
	   
}

//Set |V| = 100 and vary |E| between 1000 and 5000, using a 100 increment.
//Run the BFS algorithm for each <|V|, |E|> pair value and count the 
//number of operations performed; generate the corresponding chart (i.e. the variation of 
//the number of operations with |E|).
void tema1()
{
	FILE *f1;
	f1=fopen("bfs1.csv","w");
	int v=100,e;

	fprintf(f1,"muchii, atr, comp, atr+comp \n");

	for(e=1000;e<=4900;e=e+100) //!!!! pt 100 de varfuri nr maxim de muchii in graf este de 4950 nu 5000 !!!!
	{
		gen(e,v);
		at=0;comp=0;
		bfs(matr,rand()%v,v);
		fprintf(f1,"%d, %d, %d, %d \n",e,at,comp,at+comp);
	}
	fclose(f1);
}

//Set |E| = 9000 and vary |V| between 100 and 200, using an increment equal to 10. 
//Repeat the procedure above to generate the chart which gives the variation of the 
//number of operations with |V|.
void tema2()
{
	FILE *f2;
	f2=fopen("bfs2.csv","w");

	int v,e=9000;
	fprintf(f2,"noduri, atr, comp, atr+comp \n");

	for(v=135;v<=200;v=v+5) //!!!! un graf cu mai 100 v nu poate avea 9000 de muchii !!!!! minim de varfuri 135 pt 9000 de muchii
	{
		gen(e,v);
		at=0;comp=0;
		bfs(matr,rand()%v,v);
		fprintf(f2,"%d, %d, %d, %d \n",v,at,comp,at+comp);
	}

	

	fclose(f2);
}

void main(int argc, char** argv) 
{
	srand ( time(NULL) );
	
	test();
	//tema1();
	//tema2();
	
	getch();
	return;
}