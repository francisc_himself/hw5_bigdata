
#include "stdafx.h"
#include<iostream>
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
using namespace std;

typedef struct Node
{
	int color;
	int dist;  //discovery time
	Node *pred;
	int index;
	int f; 
};

typedef struct Queue
{
	Queue *next;
	int data;
	int length;
};

typedef struct Edge
{
	Node *f;
	Node *s;
	int type;
};



Node *Q[20000];
Queue *Adj[20000];
Node *g[20000];
Edge *edg[60000];
int size;
int head = 0;
int vertexNr;
int edgeNr;
int op = 0;
int discoveryTime;
int eI = 0;


void enqueue(Node *x)
{
	Q[size] = x;
	size++;
}
Node *dequeue()
{
	Node *x;
	x = Q[head - 1];
	head--;
	return x;
}


void getInput()
{
	cout << "Number of vertices:";
	cin >> vertexNr;
	edgeNr=0;
	cout << "Adjacency lists" <<endl;
	int neighbourNr,b;
	Queue *p, *s;
	for( int i=0; i < vertexNr; i++ )
	{
		Adj[i]=new Queue;
		Adj[i]->next = NULL;
		Adj[i]->data = i;

	}
	for( int i=0; i < vertexNr; i++ )

	{
		p=Adj[i];
		cout << "Number of neighbours ("<<i+1<<") :";
		cin >> neighbourNr;
		Adj[i]->length = neighbourNr+1;
		for( int j=0; j < neighbourNr; j++)
		{
			cin >> b;
			s=new Queue;
			s->next=NULL;
			s->data = b-1;
			p->next = s;
			p = s;
			edgeNr++;
		}


	}
	for(int i=0; i < edgeNr; i++)
	{
		edg[i]= new Edge;
	}
}


void DFS_VISIT(Node *u)
{
	Queue *t;
	op+=3;
	discoveryTime++; //discovery time (also distance)
	u->dist = discoveryTime;
	u->color = 1;
	t = Adj[u->index];
	if(t->next != NULL)
	{
		t=t->next;

	while( t != NULL ) {
		edg[eI]->f = u;
		edg[eI]->s = g[t->data];
		eI++;
		if( g[t->data]->color == 0 )
		{
			op++;
			g[t->data]->pred = u;
			edg[eI-1]->type = 0;
			DFS_VISIT(g[t->data]);
		}
		else if( g[t->data]->color == 1 )
		{
			edg[eI-1]->type = 1;
		}
		else if( g[t->data]->pred == u )
		{
			edg[eI-1]->type = 2;
		}
			else 
		{
			edg[eI-1]->type = 3;
		}

		op++;
		t = t->next;
	}
	}
	op = op+2;
	u->color = 2;
	discoveryTime++;
	u->f = discoveryTime;
	enqueue(u);

}

void DFS()
{
	for( int i=0; i < vertexNr; i++ )
	{	
		g[i]=new Node;
		g[i]->index = i;
		g[i]->color = 0;
		g[i]->dist = -1000;
		g[i]-> pred= NULL;
	}
	discoveryTime=0;
	for( int i=0; i < vertexNr; i++ )
	{
		if( g[i]->color == 0 )
			DFS_VISIT(g[i]);
	}
}



void demo()
{
	Queue *t;
	Node *u;
	int ok = 1;
	eI=0;
	getInput();
	for( int i=0; i < vertexNr; i++ )
	{
		cout << "List "<<i+1<<":";
		t = Adj[i];	
		if(t->next != NULL)
		{
			t=t->next;
			while( t != NULL ) {
				cout <<t->data + 1<<",";
				t = t->next;
			}
		}
		cout << endl;
	}
	
	for( int i=0; i < vertexNr; i++ )
			{
				Q[i]=NULL;

			}
			
	size = 0;
	DFS();
	head = size;
	cout<<endl;
	for( int i=0; i < eI; i++ )
	{
				cout << "Edge <"<<edg[i]->f->index+1<<","<<edg[i]->s->index+1<<">"<<"-";
				if(edg[i]->type == 0)
				{
					cout<<"tree edge"<<endl;
				}
				else 	if(edg[i]->type == 1)
				{
					cout<<"back edge"<<endl;
					ok = 0;
				}
				else 	if(edg[i]->type == 2)
				{
					cout<<"forward edge"<<endl;
				}
				else 
				{
					cout<<"cross edge"<<endl;
				}

			
	}

	cout<<endl;
	if( ok == 1 )
	{
	cout << "Topologycal sort:";
	while(head > 0)
	{	
		u = dequeue();
		cout << u->index +1 << " ("<<u->f<<") "; 
	}
	cout << endl;
	}
	else
	{
		cout << "The graph has no topological sort because it contains a cycle!";
	}
}




void varyEdges()
{
	vertexNr = 100;
	int j,neighbourNr,b;
	int ok;
	Queue *p, *q;
	FILE *f;
	f=fopen ("varyEdge.csv","w");
	fprintf(f,"edges, operations\n");
	srand (time(NULL));

	for(edgeNr=1000; edgeNr<=5000; edgeNr=edgeNr+100) {
		for( int i=0; i < vertexNr; i++ )
			Q[i]=NULL;
		eI=0;
		for(int i=0; i < edgeNr; i++)
	{
		edg[i]= new Edge;
	}

		for( int i=0; i < vertexNr; i++ )
		{
			Adj[i]=new Queue;
			Adj[i]->next = NULL;
			Adj[i]->data = i;
		}

		op = 0;
		j = 0;
		while( j < edgeNr )
		{
			ok=1;
			neighbourNr = rand() %vertexNr;
			b = rand() %vertexNr;
			p = Adj[neighbourNr];
			while(p->next != NULL)
			{
				if(p->data == b)
				{
					ok=0;
				}
				p = p->next;
			}
			if( ok == 1 && p->data != b )
			{

				j++;
				q=new Queue;
				q->data=b;
				q->next=NULL;
				p->next=q;
			}

		}
		size = 0;
		DFS();


		fprintf(f,"%d,%d\n",edgeNr,op);
	}

}

void varyVertices()
{

	edgeNr= 9000;
	int j,neighbourNr,b;
	int ok;
	Queue *p, *q;
	FILE *f;
	f=fopen ("varyVertex.csv","w");
	fprintf(f,"nodes, operations\n");
	srand (time(NULL));

	for(vertexNr=110; vertexNr <= 200; vertexNr+=10) {
		for( int i=0; i < vertexNr; i++ )
		{
			Q[i]=NULL;
		}
		eI=0;
		for(int i=0; i < edgeNr; i++)
		{
			edg[i]= new Edge;
		}
		for( int i=0; i < vertexNr; i++ )
		{
			Adj[i]=new Queue;
			Adj[i]->next = NULL;
			Adj[i]->data = i;
		}

		op = 0;
		j = 0;
		while( j < edgeNr )
		{
			ok=1;
			neighbourNr = rand() %vertexNr;
			b = rand() %vertexNr;
			p = Adj[neighbourNr];
			while(p->next != NULL)
			{
				if(p->data == b)
				{
					ok=0;
				}
				p = p->next;
			}
			if( ok == 1 && p->data != b )
			{

				j++;
				q=new Queue;
				q->data=b;
				q->next=NULL;
				p->next=q;
			}

		}
		size = 0;
		DFS();
		fprintf(f,"%d,%d\n",vertexNr,op);
	}
}

int main()
{
	demo();
	//varyEdges();
	//varyVertices();
	getch();
	return 0;
}

