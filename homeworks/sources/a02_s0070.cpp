/****ANONIM*** ***ANONIM*** - ***ANONIM***
  Grupa ***GROUP_NUMBER***

  Comparand cele graficele, putem spune ca metoda de constructie HEAP TOP-DOWN nu este la fel de eficienta ca si metoda de
  constructie HEAP BOTTOM-UP, deoarece numarul de asignari al metodei BOTTOM-UP este mai mic decat numarul de asignari al 
  metodei TOP-DOWN, iar numarul de comparatii sunt aproximativ egale pentru cele 2 metode.

  Mentionez faptul ca am schimbat putin implementarea algoritmului TOP-DOWN fata de ceea ce am lucrat la seminar
  deoarece folosind algoritmul de la seminar nu am reusit sa construiesc graficele bine si nu am reusit sa imi dau seama
  unde a fost eroarea in acel algoritm. Algoritmul folosit aici pentru implementare este cel gasit in Cormen.

  Multumesc!
*/

#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define Iinfinit 10000

Profiler profiler("demo");
int lungime;
int size=0;

int parinte (int i)
{
	return i/2;
}

int stanga (int i)
{
	return 2*i;
}

int dreapta (int i)
{
	return 2*i+1;
}


//Construim heap-ul buttom - up
void reconstructieHeap (int *v, int n, int i, int dim_heap)
{
	int st,dr,min,aux;

	st = stanga (i);
	dr = dreapta (i);

	min = i;  

	//cautam indexul minim dintre v[i], st[i] si dr[i]

	if (st<=dim_heap && v[st] > v[min])
		min = st;
	
	if (dr <= dim_heap && v[dr] > v[min])
			min = dr;
	profiler.countOperation("CompBU",lungime,2);
	

	if (min != i)
	{
		aux = v[i];
		v[i]=v[min];
		v[min] = aux;
		reconstructieHeap(v, n, min, dim_heap);
		profiler.countOperation("AssignBU",lungime,3);
	}	
}

void construiesteHeap (int *v, int n, int dim_heap)
{
	int i;
	for (i=dim_heap/2; i>=1; i--)
		reconstructieHeap (v, n, i, dim_heap);
}

//Construim heap-ul top - down
/*void initH (int *v, int &dim_v)
{
	dim_v = 0;
}

void hPush (int *v, int &dim_v, int x)
{
	int i,aux;

	dim_v = dim_v + 1;
	profiler.countOperation("hPush_AssignHPush", lungime, 1);
	v[dim_v] = x;
	i = dim_v;
	while (i>1 && v[i]<v[parinte(i)])
	{
		profiler.countOperation("hPush_CompHPush",lungime,1);
		profiler.countOperation("hPush_AssignHPush",lungime,3);
		aux = v[i];
		v[i] = v[parinte(i)];
		v[parinte(i)] = aux;
		i = parinte (i);
	}
	profiler.countOperation("hPush_CompHPush",dim_v,1);
}

void construiesteHeapTD (int *v, int *a, int n)
{
	int dim_a;
	initH (a, dim_a);

	for (int i=1; i<=n; i++)
		hPush (a,dim_a,v[i]);
}*/

void increaseKey(int* v,int i,int key,int n)
{
	if(key<v[i]) perror("new key is smaller than current key");
	v[i]=key;
	profiler.countOperation("CompTD",lungime,1);
	profiler.countOperation("AssignTD",lungime,1);
	while((i>1)&&(v[parinte(i)]<v[i]))
	{
		int aux=v[i];
		v[i]=v[parinte(i)];
		v[parinte(i)]=aux;
		i=parinte(i);
		profiler.countOperation("CompTD",lungime,1);
		profiler.countOperation("AssignTD",lungime,3);
	}

}

void insertTD(int* v,int key,int n)
{
	size+=1;
	v[size]=-Iinfinit;
	profiler.countOperation("AssignTD",lungime,1);
	increaseKey(v,size,key,n);
}

void constructieTD(int* v, int n)
{
	size=1;
	for(int i=2;i<=n;i++)
		insertTD(v,v[i],n);
}

void afisare(int A[],int n)
{
	int i, j, pow=2, level=1, nr=5; 
	
	printf("        %d\n\n ",A[1]); 
	
	for(i=2;i<n;)
	{
		level=pow; 
		for(j=0;j<nr;j++) 
		printf(" "); 
		
		while(i<n && level>0)
		{ 
			printf(" %d ",A[i]);
			i++; 
			level--; 
		}
	
	printf("\n\n");	
	pow = pow * 2; 
	nr--; 
	}
}

void main()
{
	int vect[]={4,5,0,1,2,3};
	int vect2[]={4,5,0,1,2,3};
	int vect3[]={0};
	
	for (int k = 0; k < 6; k++){
		printf("a[%d]=%d\n", k, vect[k]);
	}

	printf("buttom-up\n");
	construiesteHeap(vect,6,6);
	afisare(vect, 6);
	
	printf("\n");
	printf("top-down/n");
	printf("\n");
	printf("\n");

	constructieTD(vect2,6);
	afisare(vect2, 6);

	int n, v[10000], v1[10000];
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<10000; n+=100)
		{
			lungime = n;
			printf("n=%d\n", n);
			FillRandomArray(v,n);
			
			//construieste heap buttom-up
			memcpy(v1, v, n * sizeof(int));
			construiesteHeap (v1,n,n);

			//construieste heap top-down
			memcpy (v1,v,n * sizeof(int));
			constructieTD(v1,n);
		}
	}

	profiler.addSeries("BUSerie","CompBU","AssignBU");
	profiler.addSeries("TDSerie","CompTD","AssignTD");
	profiler.createGroup("DiferentaSerii","BUSerie","TDSerie");
	profiler.createGroup("Comp BU + TD","CompTD","CompBU");
	profiler.createGroup("Assign BU + TD","AssignBU","AssignTD");
	profiler.createGroup("ALL","AssignTD","CompTD","AssignBU","CompBU");	
	profiler.showReport(); 

	getchar();

}