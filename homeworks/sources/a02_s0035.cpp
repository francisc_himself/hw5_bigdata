#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

int n,dim;
int buAt=0,buComp=0,tdAt=0,tdComp=0;

//print vector
void print(int a[]) {
  for (int i = 1; i <=10; i++) 
  {
    printf(" %d  ",a[i]);
  }
  printf("\n");
}

//build heap bottom up
int left(int i) {
  return (2 * i) ;
}

int right(int i) {
  return (2 * i) + 1;
}

void heapify(int a[], int i) {
  
  int l = left(i), great;
  int r = right(i);
  buComp++;
  if (  (l <= n) && (a[l] > a[i])) {
    great = l;
  }
  else {
    great = i;
  }
  buComp++;
  if ( (r <= n) && (a[r] > a[great])) {
    great = r;
  }
  if (great != i) {
	  buAt++;
    int temp = a[i];
    a[i] = a[great];
    a[great] = temp;
    heapify(a, great);
  }
}

void BuildHeapBU(int a[]){
	int i,n=10;
	buComp=0,buAt=0;
	for (i=n/2; i > 0;i--) 
	{
		heapify(a, i); 
		//print(a);
	}
}

//build heap top down

int parent(int i) {
	return (i / 2);
}


void heapInsert(int a[],int key){ //heapPush
	dim++;
	tdAt++;
	a[dim]=key;
	int i=dim;
	tdComp++;
	while(i>1 && a[parent(i)]>a[i])
	{	tdComp++;
		tdAt++;
		int aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}

}


void initHeap(int a[]){
	dim=0;
}

void BuildHeapTD(int b[] ,int a[]){
	tdComp=0,tdAt=0;
	initHeap(b); 
	for(int i=1;i<=n;i++){ 
		heapInsert(b,a[i]);
		//print(b);
	}
	
	
}



int main(){
	int a[10001],d[10001],e[10000],c[10001];//gen valori aleatoare in ei
	int b[10001],j; // folosit pentru heap la metoda Top-Down

	FILE *f;

	f=fopen("fisier.csv","w");
	int auxAt[1],auxComp[1];
	

	fprintf(f,"n, heap_bu_at,  heap_bu_comp,heap_bu_at+comp,heap_td_at,  heap_td_comp,heap_td_at+comp \n"); //cap tabel

	
	// buAt=0,buComp=0,tdAt=0,tdComp=0;
	for(n=100;n<=10000;n=n+100){

		auxAt[0]=0;auxAt[1]=0;
	    auxComp[0]=0;auxComp[1]=0;

		printf("%d \n",n);
		for(int m=1;m<=5;m++)
		{	int i;
			srand (time(NULL));
			for(i=1;i<=n;i++){				
				a[i]=rand()%1000;
				c[i]=a[i];
			}	

			BuildHeapBU(c);
			auxAt[0]=auxAt[0]+ buAt;
			auxComp[0]=auxComp[0]+buComp ;

			BuildHeapTD(b,a);
			auxAt[1]=auxAt[1]+ tdAt;
			auxComp[1]=auxComp[1]+ tdComp;

		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d \n",n,auxAt[0]/5,auxComp[0]/5,(auxAt[0]+auxComp[0])/5 ,  auxAt[1]/5,auxComp[1]/5,(auxAt[1]+auxComp[1])/5 );
		
	}
	for(j=1;j<=10;j++)
	{
		d[j]=rand()%100;
	}
	BuildHeapBU(d);
	print(d);
	
	getch();
	
	return 0;
}