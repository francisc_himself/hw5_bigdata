#include<iostream>
#include<stdlib.h>
#include<stdio.h>

using namespace std;


//the complexity in the worst and average case in O(n^2) and in the best case is O(n)
void insertionSort(long n, long *a,long *c, long A[])
{
	long i,j,buff;
    (*a) = 0;
	(*c) = 0;
    for(i=1;i<n;i++)
	{
	    buff=A[i];
        (*a)++;
		j=i-1;
		while (A[j]>buff && j>=0)
		{
			A[j+1]=A[j];
			j=j-1;
			(*a)++;
			(*c)++;
		}
		A[j+1]=buff;
		(*a)++;
		(*c)++;

	}


/*
    for(int i = 0; i<n; i++)
    {
        printf("A[%d] = %ld \n ",i,A[i]);
    }
*/
}


//the complexity io O(n^2)
void selectionSort(long n, long *a,long *c, long A[])
{
	long i,j,pos,aux;
	(*a) = 0;
	(*c) = 0;

	for (i = 0; i < n - 1; i++)
	{
		pos=i;
		for(j=i;j<n;j++)
		{
		    (*c)++;
			if (A[j] < A[pos])
			{
                pos=j;
            }
        }
		aux=A[pos];
		A[pos]=A[i];
		A[i]=aux;
		(*a)=(*a)+3;
	}

/*
    for(int i = 0; i<n; i++)
    {
        printf("A[%d] = %ld \n ",i,A[i]);
    }
*/
}

//the complexity of bubble sort in a the average and worst case is O(n^2) and in the best case, the complexity is O(n).
void bubbleSort(long n, long *a, long *c, long A[])
{

    long aux;
    (*a) = 0;
	(*c) = 0;
    for (int i=n-1;i>0;i--)
    {
        for (int j=1;j<=i;j++)
        {
        if (A[j-1]>A[j])
        {
            aux=A[j-1];
            A[j-1]=A[j];
            A[j]=aux;
            (*a)=(*a)+3;
            (*c)++;
        }
        }
    }

/*
    for(int i = 0; i<n; i++)
    {
        printf("A[%d] = %ld \n ",i,A[i]);
    }
*/
}


int main()
{
    //Check if the sorting algorithms work poperly
/*
    printf("Program started\n");
    long i,n,a=0,c=0;
    long A[10];
    printf("Input N:\n");
    scanf("%d",&n);

    for(i = 0; i<n; i++)
        {
            printf("A[%d] = ",i);
            scanf("%d",&A[i]);
        }

    printf("Bubble Sort Starting\n");
    bubbleSort(n,&a,&c,A);
    printf("a=%d and c=%d\n",a,c);


    printf("Selection Sort Starting\n");
    selectionSort(n,&a,&c,A);
    printf("a=%d and c=%d\n",a,c);


    printf("Insertion Sort Starting\n");
    insertionSort(n,&a,&c,A);
    printf("a=%d and c=%d\n",a,c);
    return 0;
*/

    FILE *worst, *best, *average;

    worst = fopen("worst.csv","w");
    best = fopen("best.csv","w");
    average = fopen("average.csv","w");

    long ab,cb,as,cs,ai,ci;
    long i,j,k,coef;
    long sumab,sumcb,sumas,sumcs,sumai,sumci;
    long outb[10001],outi[10001],outs[10001];

    //Best case, the array is sorted

    fprintf(best,"n Ass Comp Ass+Comp Ass Comp Ass+Comp Ass Comp Ass+Comp\n");

    for(i = 100; i<=10000; i=i+100)
    {
        ab=0;cb=0;as=0;cs=0;ai=0;ci=0;
        for(j=1;j<=i;j++)
        {
            outb[j]=j;
        }
        bubbleSort(i,&ab,&cb,outb);
        insertionSort(i,&ai,&ci,outb);
        selectionSort(i,&as,&cs,outb);

        fprintf(best,"%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,ab,cb,cb+ab,ai,ci,ci+ai,as,cs,cs+as);
    }

    //Worst case, the array is in reversed order

    fprintf(worst,"n Ass Comp Ass+Comp Ass Comp Ass+Comp Ass Comp Ass+Comp\n");

    for(i = 100; i <= 10000; i=i+100)
    {
        ab=0;cb=0;as=0;cs=0;ai=0;ci=0;
        coef = 1;
        for(j = i; j>=0; j--)
        {
            outb[coef]=j;
			outi[coef]=j;
			coef++;
        }

        for (j=1;j<i;j++) //different for selection sort
			outs[j]=j+1;
		outs[i]=1;

        bubbleSort(i,&ab,&cb,outb);
		insertionSort(i,&ai,&ci,outi);
		selectionSort(i,&as,&cs,outs);
		fprintf(worst,"%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,ab,cb,cb+ab,ai,ci,ci+ai,as,cs,cs+as);

    }

    //Average case, we use random values for the array and the meassurement is repeated 5 times

    fprintf(average,"n Ass Comp Ass+Comp Ass Comp Ass+Comp Ass Comp Ass+Comp\n");

    for (i=100;i<=10000;i=i+100)
	{
		sumcb=0;sumci=0;sumcs=0;
		sumab=0;sumai=0;sumas=0;

		for (j=1;j<=5;j++)
		{
			coef=0;
            ab=0;cb=0;as=0;cs=0;ai=0;ci=0;
			for (k=1;k<=i;k++)
			{
				coef++;
				outb[coef]=outi[coef]=outs[coef]=rand();
			}
			bubbleSort(i,&ab,&cb,outb);
			sumcb+=cb;
			sumab+=ab;
			insertionSort(i,&ai,&ci,outi);
			sumci+=ci;
			sumai+=ai;
			selectionSort(i,&as,&cs,outs);
			sumcs+=cs;
			sumas+=as;
		}
			fprintf(average,"%ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,sumab/5,sumcb/5,sumcb/5+sumab/5,sumai/5,sumci/5,sumci/5+sumai/5,sumas/5,sumcs/5,sumcs/5+sumas/5);
	}

}
