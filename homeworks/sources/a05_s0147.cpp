/*Tema 5
***ANONIM*** ***ANONIM***
***ANONIM*** ***GROUP_NUMBER***

Operatiile de cautare si stergere folisind adresarea deschisa si verificarea patratica.
Obs. Cele doua constante au influenta semnificativa asupra rezultatelor. Numarul operatiilor este influentat semnificativ de numerele din tabela.

*/
#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>

#define DIM_MAX 9973

int T[DIM_MAX];
int m, n, N;
int c1=3, c2=78;
int acc=0;

int h(int  k, int i)
{
	return ((k%N + c1*i + c2*i*i)%N);
}

int HASH_INSERT(int T[],int k)
{
	int i = 0, j;
	do{
		j = h(k, i);
		if(T[j]==NULL)
		{
			T[j]=k;
			return j;
		}
		i=i+1;
	}while(i < N);

	printf("hash table overflow!!");
	return -2;
}

int HASH_SEARCH(int T[],int k)
{
	int i=0, j;
	
	do{
		j = h(k, i);
		acc++;
		if(T[j]==k)
			return j;
		i=i+1;
	}
	while(T[j]!=NULL && i<N);

	return NULL;
}

void HASH_PRINT(int T[])
{
	int i;

	printf("Hash Table Print: \n");
	for(i=0; i<N; i++)
		printf("%d ", T[i]);

	getch();
}

void generate()
{
	int i, j, run;
	FILE *f;
	N = 9973;
	m=3000;
	float a[]={0.8, 0.85, 0.9, 0.95, 0.99};
	int medG=0, medNG=0, maxG=0, maxNG=0, medTG=0, medTNG=0, maxTG=0, maxTNG=0;

	srand(time(NULL));

	f = fopen("HashTables.csv", "w");

	fprintf(f, "Factor umplere, Medie gasite, Max gasite, Medie negasite, Max negasite\n");

	for(i=0;i<5;i++)
	{
		n = a[i] *N;
				for(run=1; run<=5; run++)
		{
			medG=0, medNG=0, maxG=0, maxNG=0;
			
			for(int ind=0; ind<N; ind++) //init cu 0
					T[ind]=NULL;

			for(j=0; j<n; j++) //generare si inserare
				HASH_INSERT(T, 1+rand()%99999);
			
			for(j=0; j<m/2; j++) //cauta elemente negasite
			{
				acc=0;
				HASH_SEARCH(T, rand()+99999);
				medNG += acc;
				if(acc>maxNG)
					maxNG=acc;
			}

		
			for(j=0; j<m/2; j++) //cauta elemente gasite
			{
				acc=0;
				HASH_SEARCH(T, T[rand()%N]);
				medG += acc;
				if(acc>maxG)
					maxG=acc;
			}
			medTG+=medG;
			medTNG+=medNG;
			maxTG+=maxG;
			maxTNG+=maxNG;
		}

		fprintf(f, "%.2f, %d, %d, %d, %d\n", a[i], medTG/(5*1500), maxTG/5, medTNG/(5*1500), maxTNG/5);
	}
}

int main(){
	generate();
	int poz, i;
	N=7;
	n=5;

	for(i=0;i<N;i++)
		T[i]=NULL;

	HASH_INSERT(T, 14);
	HASH_INSERT(T, 45);
	HASH_INSERT(T, 3);
	HASH_INSERT(T, 48);
	HASH_INSERT(T, 39);

	HASH_PRINT(T);

	if((poz=HASH_SEARCH(T, 7))!=NULL)
		printf(" \n 7 gasit pe poz: %d \n", poz);
	else
		printf("\n Cheia 7 nu a fost gasita!");

	if((poz=HASH_SEARCH(T, 39))!=NULL)
		printf(" \n Cheia 39 gasit pe pozitia: %d \n", poz);
	else
		printf("\n Cheia 39 nu a fost gasita!");
	getch();
	return 0;
}
