#include "stdio.h"
#include "Profiler.h"

//macros
#define MAX_SIZE 10001

//global variables
Profiler profiler("merge_k_ordered_arrays");
int Array[MAX_SIZE];
int dim; // values on x axis of our graphs
int heap_size;
int k;
/*
void count(int times) {
	if(k == 5)
		profiler.countOperation("k=5", dim - 1, times);
	else if(k == 10)
		profiler.countOperation("k=10", dim - 1, times);
	else if(k == 100)
		profiler.countOperation("k=100", dim - 1, times);
} */

void count(int times) {
	profiler.countOperation("operations", k, times);
}

struct node {
  int data;
  struct node *next;
};

struct heap_el {
  int data;
  int provenience_list;
};

struct node* add(struct node *head, int data){
    struct node *tmp;
 
    if(head == NULL){
        head = (struct node *)malloc(sizeof(struct node));
        if(head == NULL){
            printf("Error! memory is not available\n");
            exit(0);
        }
        head->data = data;
        //head->next = head; for circular linked lists
		head->next = NULL;
    }else{
        tmp = head;
 
        //while (tmp-> next != head) for circular linked lists
		while (tmp-> next != NULL)
            tmp = tmp-> next;
        tmp-> next = (struct node *)malloc(sizeof(struct node));
        if(tmp -> next == NULL)
        {
            printf("Error! memory is not available\n");
            exit(0);
        }
        tmp = tmp-> next;
        tmp-> data = data;
        //tmp-> next = head; for circular linked lists
		tmp-> next = NULL;
    }
    return head;
}

int get_first_el(struct node **head){
	/*	
	Return the value stored in the first element of the 
	list, and delete the first node of the list
	*/
    struct node *tmp;
 
    if(*head == NULL){       
		printf("Error! the pointer is NULL\n");
		exit(0);
	}
	else {
		int data = (*head)->data;
		struct node *tmp;
		tmp = *head;
		*head = (*head)->next;
		free(tmp);
		return data;
    }
}

void printlist(struct node *head)
{
    struct node *current;
    current = head;
    if(current!= NULL)
    {
        do
        {
            printf("%d ",current->data);
            current = current->next;
        //} while (current != head); for circular linked lists
		} while (current != NULL);
        printf("\n");
    }
    else
        printf("The list is empty\n");
 
}

void printarray(int *A, int n) {
	printf("The array is: ");
		for(int i = 0; i < n; i++) {
			printf("%d ", A[i]);
		}
	printf("\n");
}

void Min_Heapify(heap_el **A, int i) {
	int l = 2 * i;
	int r = 2 * i + 1;
	int min;
	if(l <= heap_size && A[l]->data < A[i]->data) {
		min = l;
	}
	else min = i;
	if(r <= heap_size && A[r]->data < A[min]->data) {
		min = r;
	}
	count(2);
	if(min != i) {
		count(3);
		struct heap_el *temp = A[i];
		A[i] = A[min];
		A[min] = temp;
		Min_Heapify(A, min);
	}
}

void Build_Min_Heap_Bottom_Up(heap_el **A, int n) {
	for(int i = n/2; i >= 1; i--) // take all non-leaf nodes
		Min_Heapify(A, i);
}

struct heap_el *extract_min(heap_el **A) {
	if(heap_size < 1)
		return NULL;
	struct heap_el *min = (struct heap_el *)malloc(sizeof(struct heap_el));
	min->data = A[1]->data;
	min->provenience_list = A[1]->provenience_list;
	count(3);
	A[1] = A[heap_size];
	heap_size--;
	Min_Heapify(A, 1);
	return min;
}

void inorder(struct heap_el **A, int k, int level, int n) {
	int i;
	if (k <= n) {
		inorder(A, 2 * k + 1, level + 1, n) ;
		for (i = 0; i <= level ; i++) 
			printf("   "); /* for nice listing */
		printf ("%d\n", A[k]->data) ;
		inorder(A, 2 * k, level + 1, n) ;
	}
}

void make_list_from_array(struct node **head, int *A, int n) {
	for(int i = 0; i < n; i++)
		*head = add(*head, A[i]);
}


struct heap_el *returnMin(struct node **arrays, int k) {
	int min = 31000; // max possible value
	int pozmin = -1;
	for(int i = 0; i < k; i++) {
		if(arrays[i] != NULL) {
			count(1);
			if(arrays[i]->data < min) {
				count(1);
				min = arrays[i]->data;
				pozmin = i;
			}
		}
	}
	if(pozmin != -1) {
		struct heap_el *el = (struct heap_el *)malloc(sizeof(struct heap_el));
		el->data = min;
		el->provenience_list = pozmin;
		return el;
	} else {
		return NULL;
	}
}

void Merge_sorted_lists(int n) {
	k = 4;
	heap_size = k;
	int A[MAX_SIZE]; // this array is used for storing the random generated numbers
	int final[MAX_SIZE]; // the final MERGED sorted array
	int final_size = 0; // initially empty
	struct node *arrays[MAX_SIZE]; // the array of k  sorted lists
	for(int i = 0; i < k; i++) {
		arrays[i] = NULL;
	}
	int j = 0;
	for(int i = 0; i < k; i++) { // generate k random increasingly sorted lists each of n/k length
		FillRandomArray(A, n/k, 10, 30000, false, 1);
		//printarray(A, n/k);
		//printf("The  list corresponding to the array is: ");
		make_list_from_array(&(arrays[j++]), A, n/k);
		printlist(arrays[i]);
	}
	struct heap_el *B[MAX_SIZE]; // heap containing first el from all lists
	for(int i = 0; i < k; i++) {
			struct heap_el *el = (struct heap_el *)malloc(sizeof(struct heap_el));
			el->data = get_first_el(&(arrays[i]));
			el->provenience_list = i;
		B[i + 1] = el;
	}
	Build_Min_Heap_Bottom_Up(B, heap_size); // build min heap from the first el of our sorted lists
	//inorder(B, 1, 0, k);

	while(heap_size != 0) {
		final[final_size++] = B[1]->data; // place the minimum in the final MERGED sorted array
			count(1);
			if(arrays[B[1]->provenience_list] != NULL) {//if there are more elements in the lists from which the min was taken
				struct heap_el *el = (struct heap_el *)malloc(sizeof(struct heap_el));
				el->data = get_first_el(&(arrays[B[1]->provenience_list])); // take the next element from the list in which the minimum was
				el->provenience_list = B[1]->provenience_list;
				B[1] = el; // replace the min el by the next el from the list in which min was
			}
			else if(returnMin(arrays, k) != NULL) {	//if there are no more elements in the lists from which the min was taken
				B[1] = returnMin(arrays, k); // replace the min el by the min from first positions of all lists
				struct heap_el *el = (struct heap_el *)malloc(sizeof(struct heap_el));
				el->data = get_first_el(&(arrays[B[1]->provenience_list]));
				el->provenience_list = B[1]->provenience_list;
				B[1] = el;
			}
			else {// if there are no more elements in the lists
				extract_min(B); // get the min el from the heap
			}
				
		Min_Heapify(B, 1);
	}
	
	printarray(final, final_size);
}

void average() {
	
	int kvalues[] = {5, 10, 100};
	for(int j = 0; j < 3; j++ ) { // for each  value of k
		k = kvalues[j];
			for(int n = 100; n < 10000; n += 100) {
			dim = n + 1;
			//for(int i = 1; i <= 5; i++) { // do the measurements 5 times on each set of input data
				Merge_sorted_lists(n);			
			//}	
		}
	}
	profiler.createGroup("Average Case", "k=5", "k=10", "k=100");
	profiler.showReport();
}

void second() {	
	int n = 10000;
	dim = n + 1;
	for(k = 10; k <= 500; k += 10) {
		Merge_sorted_lists(n);					
	}
	profiler.createGroup("dimension = 10000 k/from 10 to 500 by a factor of 10", "operations");
	profiler.showReport();
}

int main() {
	int n = 10;
	int A[MAX_SIZE] = {-10000, 4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
	Merge_sorted_lists(12);
	//average();
	//second();
	//Build_Min_Heap_Bottom_Up(A, n);
	/*
	for(int i = 1; i <= n; i++)
		printf("%d ", A[i]);
	printf("\n");
	inorder(A, 1, 0, n); */
	
	/*
	struct node *head = NULL;
    head = add(head, 1); 
    printlist(head);
    head = add(head, 20);
    printlist(head);
    head = add(head, 10);
    printlist(head);
    head = add(head, 5); 
    printlist(head);
	printf("%d\n", get_first_el(&head));
	printlist(head);
	*/

	return 0;
}