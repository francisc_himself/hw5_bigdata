
#include "stdafx.h"
#include<iostream>
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
using namespace std;

typedef struct Node
{
	int color;
	int dist;
	Node *pred;
	int index;
};

typedef struct Queue
{
	Queue *next;
	int data;
	int length;
};



Node *Q[20000];
Queue *Adj[20000];
Node *g[20000];
int size;
int head = 0;
int vertexNr;
int edgeNr;
int op;


void enqueue(Node *x)
{
	Q[size] = x;
	size++;
	op++;
}

Node *dequeue()
{
	Node *x;
	x = Q[head];
	head++;
	op++;
	return x;
}

void prettyPrint(int i,int depth,int dep, int s)
{
	if(s < size)
	{ 
		
		for(int j=0; j<depth; j++) 
			printf("	");
		
		printf("%d", Q[i]->index+1);
		printf("\n");
		if(Q[i+1] != NULL)
		if( Q[i+1]->dist > dep )
			prettyPrint(i+1,depth+1,Q[i+1]->dist,s+1);
		else 
			prettyPrint(i+1,depth,Q[i+1]->dist,s+1);
	}

}

void getInput()
{
	cout << "Number of vertices:";
	cin >> vertexNr;
	cout << "Adjacency lists" <<endl;
	int neighbourNr,b;
	Queue *p, *s;
	for( int i=0; i < vertexNr; i++ )
		{
			Adj[i]=new Queue;
			Adj[i]->next = NULL;
			Adj[i]->data = i;
			
		}
	for( int i=0; i < vertexNr; i++ )
		
	{
			p=Adj[i];
			cout << "Number of elements ("<<i+1<<") :";
			cin >> neighbourNr;
			Adj[i]->length = neighbourNr+1;
			for( int j=0; j < neighbourNr; j++)
			{
				cin >> b;
				s=new Queue;
				s->next=NULL;
				s->data = b-1;
				p->next = s;
				p = s;
			}
			

		}
}
void initialize()
{
	for( int i=0; i < vertexNr; i++ )
	{	
		g[i]=new Node;
		g[i]->index = i;
		g[i]->color = 0;
		g[i]->dist = -1000;
		g[i]-> pred= NULL;
		}
}
void BFS(int sourceIndex)
{
	Node *u;
	op+=3;
	g[sourceIndex]->color = 1;
	g[sourceIndex]->dist = 0;
	g[sourceIndex]->pred = NULL;
	
	for( int i=0; i < vertexNr; i++ )
	{
		Q[i] = NULL;
	}
	enqueue(g[sourceIndex]);
	while( Q[head] != NULL )
	{
		u = dequeue();
		Queue *t;
		t = Adj[u->index];
		
		while( t != NULL ) {
			if( g[t->data]->color == 0 )
			{
				op+=4;
				g[t->data]->color = 1;
				g[t->data]->dist = u->dist + 1;
				g[t->data]->pred = u;
				enqueue(g[t->data]);
			}
			op++;
			t = t->next;
		}
		op++;
		u->color = 2;
	}

}

void demo()
{
	getInput();
	initialize();
	for( int i=0; i< vertexNr; i++)
		if(g[i]->color == 0)
			{
				size=0;
				head=0;
				BFS(i);
				prettyPrint(0,0,0,0);
				cout<<endl<<"----------"<<endl;
			}
}

void varyEdges()
{
	vertexNr = 100;
	int j,neighbourNr,b;
	int ok;
	Queue *p, *q;
	FILE *f;
	f=fopen ("varyEdge.csv","w");
	fprintf(f,"edges, operations\n");
	srand (time(NULL));
	
	for(edgeNr=1000; edgeNr<=5000; edgeNr=edgeNr+100) {
		for( int i=0; i < vertexNr; i++ )
			Q[i]=NULL;

		for( int i=0; i < vertexNr; i++ )
		{
		Adj[i]=new Queue;
		Adj[i]->next = NULL;
		Adj[i]->data = i;
		}

		op = 0;
		j = 0;
		while( j < edgeNr )
		{
			ok=1;
			neighbourNr = rand() %vertexNr;
			b = rand() %vertexNr;
			p = Adj[neighbourNr];
			while(p->next != NULL)
			{
				if(p->data == b)
				{
					ok=0;
				}
				p = p->next;
			}
			if( ok == 1 && p->data != b )
			{
		
				j++;
				q=new Queue;
				q->data=b;
				q->next=NULL;
				p->next=q;

			}
			
		}
		initialize();
		for( int i=0; i< vertexNr; i++)
		{
				if(g[i]->color == 0) {
					
					size=0;
					head=0;
					BFS(i);
				}
		}
				
		
		fprintf(f,"%d,%d\n",edgeNr,op);
	}
	
}

void varyVertices()
{
	
	edgeNr= 9000;
	int j,neighbourNr,b;
	int ok;
	Queue *p, *q;
	FILE *f;
	f=fopen ("varyVertex.csv","w");
	fprintf(f,"nodes, operations\n");
	srand (time(NULL));
	
	for(vertexNr=100; vertexNr <= 200; vertexNr+=10) {
		for( int i=0; i < vertexNr; i++ )
			{
				Q[i]=NULL;
			}
		for( int i=0; i < vertexNr; i++ )
		{
		Adj[i]=new Queue;
		Adj[i]->next = NULL;
		Adj[i]->data = i;
		}

		op = 0;
		j = 0;
		while( j < edgeNr )
		{
			ok=1;
			neighbourNr = rand() %vertexNr;
			b = rand() %vertexNr;
			p = Adj[neighbourNr];
			while(p->next != NULL)
			{
				if(p->data == b)
				{
					ok=0;
				}
				p = p->next;
			}
			if( ok == 1 && p->data != b )
			{
		
				j++;
				q=new Queue;
				q->data=b;
				q->next=NULL;
				p->next=q;

			}
			
		}
		initialize();
		for( int i=0; i< vertexNr; i++)
		{if(g[i]->color == 0){
				size=0;
				head=0;
				BFS(i);
		}
		}
		fprintf(f,"%d,%d\n",vertexNr,op);
	}
}

int main()
{
	demo();
	//varyEdges();
	//varyVertices();
	getch();
	return 0;
}

