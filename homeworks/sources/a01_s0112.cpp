#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>

int ass=0;
int comp=0;

void insertionSort(int a[],int n)
{	int i,j,buff;
	
	for(i=1;i<n;i++)
	{	buff=a[i];
		ass++;
		j=i-1;

		while ((a[j]>buff) && (j>=0))
		{
			
			a[j+1]=a[j];
			comp++;
			ass++;
			j=j-1;
		}
		a[j+1]=buff;
		ass++;
		comp++;
	}
	
	}


/*void bubbleSort(int numbers[], int size)
{
  int i, j, aux;
 
 
  for (i = 0; i <(size-1); i++)
  {
    for (j = i+1; j <size; j++)
    {	comp++;
      if (numbers[i] > numbers[j])
      {	
        aux = numbers[j];
        numbers[j] = numbers[i];
        numbers[i] = aux;
		ass+=3;
      }
    }
  }
  
}*/

void bubbleSort(int *a, int n)
{
	int flag = 0;
	int i;
	int ok;
	int temp;
	

	do {
		ok = 0;
		temp= 0;
		i = 0;
		while (i<n)
		{
			if (a[i] > a[i+1])
			{
				comp++;
				temp = a[i];
				a[i]= a[i+1];
				ass+=3;
				a[i+1]=temp;
				flag=i;
				ok=1;
			}
			i++;
		}
	}while (flag!=0 && ok == 1);
}



void selectionSort(int *a, int n){
	int minp, aux;
	

    for (int p = 0; p < n - 1; p++)
    {
        minp = p;
        for (int j = p; j < n; j++)
        {	
			comp++;
            if (a[j] < a[minp])
            {
                minp = j;
            }
        }
            aux = a[minp];
            a[minp] = a[p];
            a[p] = aux;
			ass+=3;
	} 

	}
	

void generate_best(int *a, int n)
{
	for (int i=0; i<n; i++)
	{
		a[i]=i;
	}
}
void generate_avg(int *a, int n)
{
	for (int i=0; i<n; i++)
	{
		a[i]=rand();
	}
}

void generate_worst(int *a, int n)
{	
	for (int i=0; i<n; i++)
	{
		if (i>0) a[i] = i;
		else  a[i]=n;
	}
}

void generate_worst1(int *a, int n)
{	
	for (int i=0; i<n; i++)
	{
		a[i] = n-i;
	}
}

void printArray(FILE *f,int *a,int n)
	{
		for (int i=0; i<n; i++)
		{
			fprintf(f,"%d,  ",a[i]);
		}
	}

void copy(int *a, int *b, int n){
	for(int i=0;i<n;i++){
	a[i]=b[i];
	}
	}


int main()
{
	FILE *f, *g, *h;
		f=fopen("sorting_best.csv","w");
		g=fopen("sorting_worst.csv","w");
		h=fopen("sorting_average.csv","w");
	
		int *a;
		int *b;

		
		for (int n=100; n<10000; n = n + 100)
		{
			a = new int[n];
			b=new int[n];
			printf("%d \n",n);
			fprintf(f,"%d,",n);
			fprintf(g,"%d,",n);
			fprintf(h,"%d,",n);
			
			//BEST
			generate_best(a,n);
			bubbleSort(a,n);
			fprintf(f,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			
			
			generate_best(a,n);
			selectionSort(a,n);
			fprintf(f,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;

			generate_best(a,n);
			insertionSort(a,n);
			fprintf(f,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			fprintf(f,"\n");
			
			//WORST
			generate_worst1(a,n);
			bubbleSort(a,n);
			fprintf(g,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;

			generate_worst(a,n);
			selectionSort(a,n);
			fprintf(g,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			
			generate_worst1(a,n);
			insertionSort(a,n);
			fprintf(g,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			fprintf(g,"\n");

			
			
			//AVERAGE
			
			generate_avg(a,n);
			copy(b,a,n); //b<-a
			bubbleSort(a,n);
			fprintf(h,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			copy(a,b,n);//a<-b
			selectionSort(a,n);
			fprintf(h,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			copy(a,b,n);//a<-b
			insertionSort(a,n);
			fprintf(h,"%d,%d,%d,",comp,ass,ass+comp);
			comp=0;
			ass=0;
			fprintf(h,"\n");
			
			
		}

	fclose(f);
	fclose(g);
	fclose(h);
	getch();
	return 0;
	
	
}
