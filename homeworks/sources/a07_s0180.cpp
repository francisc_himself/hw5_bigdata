// Multiway.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int a[] = { 1, 2, 4, 5, 6, 6, -99, 4, 2}; //parents
int n = 9; //number of nodes
int s[9]; //sons

struct multyNode {
	int key;
	int nrchildren;
	multyNode ** children;
}tree[9];

struct binaryNode {
	int key;
	binaryNode * child;
	binaryNode * brother;
};
int theRoot;

void multiway() //transform in multyway
{
	for ( int i = 0; i < n; i++)
	{		
		tree[i].key = i;
		if( a[i] == -99 )theRoot = i;
		else s[a[i]]++;			
	}	

	for( int i = 0; i <n; i++)
	{		
		if ( i != theRoot)
		{
			tree[a[i]].children = (multyNode**)realloc(tree[a[i]].children,sizeof(multyNode*) * s[a[i]]); // memory realloc.
			tree[a[i]].children[ tree[a[i]].nrchildren++] = &tree[i];							//alloc another elem
		}
	}	
}

void afis(multyNode *root, int tab) //  multiway -- pretty
{
	if(root == NULL) return;
	for ( int i = 0; i < tab; i++ ) printf("\t");	

	printf(" %d \n", root->key);
	for (int i = 0; i < root->nrchildren; i++) afis(root->children[i],tab +1);	
}


void afis2( binaryNode *root, int tab)//binar -- pretty
{
	if(root == NULL) return;
	for ( int i = 0; i < tab; i++ ) printf("\t");	
	tab++;
		printf(" %d \n", root->key);
		afis2(root->child,tab);	
	tab--;
		afis2(root->brother,tab);
	
}
void binar( multyNode *root, binaryNode *root2) // build binary tree
{	
	if( root != NULL)
	{

		if ( root->nrchildren > 0)
		{
			root2->child = (binaryNode*)malloc(sizeof(binaryNode));//memory for child
			root2->child->key = root->children[0]->key;
			binar(root->children[0],root2->child); //recursive call for child
			root2 = root2->child;
			for ( int i = 1; i < root->nrchildren; i++)
			{
				root2->brother = (binaryNode*)malloc(sizeof(binaryNode)); // memory for brother
				root2->brother->key = root->children[i]->key;				
				binar(root->children[i],root2->brother); //recursive call for brother
				root2 = root2->brother;
			}
			root2->brother = NULL;
		}
		else root2->child = NULL;
		
	}	
}


int main()
{
	int j=0;
	for(j=0;j<9;j++)
		printf("%d ",a[j]);
	   printf("  \n");
	
	multiway();
    

	binaryNode* r = (binaryNode*) malloc(sizeof(binaryNode)+1);
	r->key = tree[theRoot].key;
	
	binar(&tree[theRoot],r);
  
	afis(&tree[theRoot],0);
	
	    printf(" , \n");
    	printf(" , \n");

	r->brother = NULL;
	afis2(r,0);	
	
	getch();
	return 0;
}
