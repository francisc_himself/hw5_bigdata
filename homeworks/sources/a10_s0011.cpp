#include<iostream>
#include<conio.h>

using namespace std;
int V=6;
int time;

struct nod_lista_adj //structura pentru un nod lista de adiacenta a unui nod
{
	int nr_adj;//nodul adiacent nodului pt care se construieste lista
	nod_lista_adj *urm;//pointer la urmatorul nod din lista
	int tip_muchie;//0=aorbore, 1=inapoi,2=inainte/transversala
};

struct nod //structura pentru un nod din graf
{
	nod_lista_adj *lista;//pointer la primul nod din lista sa de adiacenta
	int d;//timpul de descoperire
	int f;//timpul final;
	int cul;//0=alb, 1=gri, 2=negru
	nod *p;
};
nod *graf[6];

void dfs_visit(int nod)
{
	nod_lista_adj *list;
	time++;
	graf[nod]->d=time;
	graf[nod]->cul=1;
	list=graf[nod]->lista; //descoperirea nodului alb u
	while(list!=NULL) //explorarea muciei (u,v)
	{
		if(graf[list->nr_adj]->cul==0)//alb=tree
		{
			list->tip_muchie=0;//arbore
			graf[list->nr_adj]->p=graf[nod];
			dfs_visit(list->nr_adj);
		}
		else
			if(graf[list->nr_adj]->cul==1)//gri=inapoi
			{
				list->tip_muchie=1;//inapoi
			}
			else
			{
				list->tip_muchie=2;//inainte/transversala
			}
		list=list->urm;
	}
	graf[nod]->cul=2;//am terminat cu nodul u
	time++;
	graf[nod]->f=time;
}

void dfs(nod *graf[], int V)
{
	for(int i=1;i<=V;i++)
	{
		graf[i]->cul=0;
		graf[i]->p=NULL;
	}
	time=0;
	for(int i=1;i<=V;i++)
	{
		if(graf[i]->cul==0)
			dfs_visit(i);
	}
}

int main()
{
	for(int i=1;i<=V;i++)
	{
		graf[i]=(nod*)malloc(sizeof(nod));
		graf[i]->cul=0;
		graf[i]->lista=NULL;
		graf[i]->p=NULL;
		graf[i]->d=0;
		graf[i]->f=0;
	}
	//initializez listele de adiacenta pt fiecare nod din graf=>generez muchiile
	graf[1]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[1]->lista->nr_adj=5;
	graf[1]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[1]->lista->urm->nr_adj=6;
	graf[1]->lista->urm->urm=NULL;

	graf[2]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[2]->lista->nr_adj=1;
	graf[2]->lista->urm=NULL;

	graf[3]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[3]->lista->nr_adj=2;
	graf[3]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[3]->lista->urm->nr_adj=4;
	graf[3]->lista->urm->urm=NULL;

	graf[4]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[4]->lista->nr_adj=5;
	graf[4]->lista->urm=NULL;

	graf[5]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[5]->lista->nr_adj=2;
	graf[5]->lista->urm=NULL;

	graf[6]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[6]->lista->nr_adj=5;
	graf[6]->lista->urm=NULL;

	dfs(graf,3);
	cout<<"Listele de adiacenta "<<endl;
	for(int i=1;i<=V;i++)
	{
		cout<<i<<": ";
		nod_lista_adj *aux=graf[i]->lista;
		while(aux!=NULL)
		{
			cout<<aux->nr_adj<<"("<<aux->tip_muchie<<")    ";
			aux=aux->urm;
		}
		cout<<endl;
	}
	getch();
	return 0;
}




