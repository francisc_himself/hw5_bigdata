// ***GROUP_NUMBER*** ***ANONIM*** ***ANONIM***
#include<time.h>
#include <conio.h>
#include<iostream>
#include<fstream>
using namespace std;

typedef struct nod{                                //Structura folosita la construirea Listelor utilizate in cadrul algoritmului
	int info;                     //camp ce va retine informatia din fiecare nod, in acest caz va fi un numar intreg generat aleator
	struct nod  *urm;              //camp ce va contine referinta la urmatorul nod din lista,  in cazul ultimului nod din lista acest camp va avea valoarea 0
}NOD;

NOD *prim[1000], *ultim[1000];
int M=0;

int c[600000],d[600000],f[600000],p[600000],t[600000];


//nr numarul de muchii
//n nr de noduri
void inserareLista(int nr,int n)  //realizeaza inserarea unui nou element in lista
	//in cadrul acestui algoritm e nevoie doar de cazul inserarii unui element dupa ultimul element al listei
{
	int v,u;
	NOD *p;

	for(u=0;u<nr;u++)
		ultim[u]=prim[u]=NULL;

	for(int i=0;i<nr;i++){

		v=rand()%n;
		u=rand()%n;

		if(prim[u] == NULL){

				p=(NOD*)malloc(sizeof(NOD));                   //creearea unui nou nod
				p->info=v;											//memorarea noii informatii in cadrul acestuia
				p->urm=NULL;                                      //nodul va vi inserat la sfarsitul listei
				prim[u] = p;
				prim[u]->urm = NULL;
				ultim[u]=prim[u];
		}
		else
			{
				p=(NOD*)malloc(sizeof(NOD));                   //creearea unui nou nod
				p->info=v;											//memorarea noii informatii in cadrul acestuia
				p->urm=NULL;                                      //nodul va vi inserat la sfarsitul listei
				ultim[u]->urm=p;
				ultim[u]=p;
		}//noul nod devine ultimul nod al listei
	}
}

void afisare(int n)
{

	for (int i=0;i<n;i++){

		cout<<i;
		NOD *pp=prim[i];
	while(pp!=NULL){

		cout<<pp->info;
		pp=pp->urm;
	}

	cout<<endl;
	}

}


void DFS_visit(int u, int nr)
{
	c[u]=1;
	M=M+1;
	d[u]=t[u];
	t[u]++;
	NOD *pp=prim[u];
	while(pp!=NULL){
		int v=pp->info;
		if(c[v]==0){

			M=M+2;
			p[v]=u;
			DFS_visit(v,nr);

		}


		pp=pp->urm;
	}
	M++;
	c[u]=2;
	f[u]=t[u];
	t[u]++;

}
void DFS(int n, int nr)
{
	for(int u=0;u<n;u++)
	{
		c[u]=0;
		p[u]=NULL;
		t[u]=0;
	}
	for(int u=0;u<n;u++)
	{
		if(c[u]==0)
			DFS_visit(u,nr);
	}

}

void main(void)
{
	int n=5;
	int nr=10;

	ofstream f("file.csv");
	ofstream f2("file2.csv");

	f<<"varfuri"<<","<<"muchii"<<","<<"analiza"<<endl;

	f2<<"varfuri"<<","<<"muchii"<<","<<"analiza"<<endl;


	srand(time(NULL));


	for(int n=100;n<1000;n+=10)
	{
		M=0;
		nr=100;
		inserareLista(nr,n);

		DFS(n,nr);

		f<<n<<","<<nr<<","<<M<<endl;

	}
	cout<<nr<<" done.";
	cout<<endl;
	f.close();
	nr=9000;
	for(int n=110;n<200;n+=10)
	{
		M=0;

		inserareLista(nr,n);

		DFS(n,nr);
		f2<<n<<","<<nr<<","<<M<<endl;
	}
	f2.close();
	cout<<nr<<" done.";
	cout<<endl;



	cout<<"done.";
	_getch();
}
