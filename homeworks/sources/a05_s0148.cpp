#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>

using namespace std; 

#define N 10007
int nrOp = 0;
int a=1;
int b=1000;
long maxs = 0,maxns = 0;

typedef struct NOD{
	int inf;
	struct NOD *urm;
} nod;

nod *thash[N];
int i = 13;


int hf(int a,int i) {
	return (a%N + 3*i + 2*i*i) % N ;
}


int caut( long z)
{
	int i = 0;
	int ok=0;
	int l = 0;
	do
	{
		//nod *p;
		//p=(nod *)malloc(sizeof(nod));
		//p->inf = a->inf;
		l=hf(z , i);
		nrOp++;
		if(thash[l]->inf == z)
		{   
			ok = 1;
			return ok;
		}
		else
			i++;
		//nrOp +=4;
	}while(thash[l] == 0 || i == N);
	return 0;
}

int adauga(int a)
{
	int i=0;
	//printf("adauga %d\n",a);
	while(i != N)
	{
		nod *p;
		unsigned h;
		p=(nod *)malloc(sizeof(nod));
		p->inf = a;
		h=hf(p->inf , i);
		if(thash[h]==0) {
			thash[h] = p;
			p->urm=0;
			return h;
		} else {
			i++;
		}
	}
	return 0;
}

void afisare( float lung,FILE *g)
{

	for (int i=0; i<lung; i++) 
	{
		if (thash[i])
		{
			fprintf(g,"Primul %d \n",thash[i]->inf);	
		}
	}
}

void adaugSiVerific(float n)
{

	for (int i=0; i<n; i++)
		thash[i]=0;
	for (int i=0; i<n; i++) {
		adauga(rand()%(b-a+1) + a);
	}
}

void nrRand(long &s,long &ns,int &nr1,int &nr2)
{ 
	while(nr1 < 1500)
	{
		long p;
		p = rand()%(b-a+1) + a;
		if(caut(p) == 1)
		{
			s = s + nrOp;
			nr1++;
			if(maxs < nrOp)
			{
				maxs = nrOp;
			}
		}
	}

	while( nr2 < 1500)
	{
		long p;
		p = rand()%(b-a+1) + a;
		if(caut(p) == 1)
		{
			ns = ns + nrOp;
			nr2++;
			if(maxns < nrOp)
			{
				maxns = nrOp;
			}
		}
	}


}

void initializare(double n)
{
	for (int i=0; i<n; i++)
		thash[i]=0;
}


int main() {
	nod *p;
	float alfa[5]={0.8, 0.85, 0.9, 0.95, 0.99};

	int n;
	int nr1,nr2;
	long s=0,ns=0;

	srand(time(NULL));

	FILE *f,*op;
	f = fopen("fis.txt", "w");
	op = fopen("operatii.csv","w");
	fprintf(op,"Alfa,Avg. Effort found,Max. Effort found,Avg. Effort not-found,Max. Effort not-found \n");
	initializare(N);
	
	for (int i=0; i<N; i++)
	{
		adauga(rand()%(b-a+1) + a);
	}
	//printf("pasu2\n");
	for (int i=0; i<N; i++) 
	{
		if (thash[i])
		{
			printf("Primul:");
			printf("%d \n",thash[i]->inf);	
		}

	}
	afisare(N,f);


	for(int i=0;i<5;i++)
	{
		n = 0;
		n = alfa[i] * N;
		adaugSiVerific(n);
		nrOp = 0;
		s = 0;
		ns = 0;
		nr1=0;nr2=0;
		nrRand(s,ns,nr1,nr2);
		fprintf(op,"%f,%d,%d,%d,%d \n",alfa[i],s/nr1,maxs,ns/nr2,maxns);
	}

	fclose(f);
	fclose(op);

	getch();
	return 0;
}