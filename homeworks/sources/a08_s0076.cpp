#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
#include<time.h>
#include "Profiler.h"
Profiler profiler ("multimi");
using namespace std;
#define NEG_INF INT_MIN

typedef struct muchie
{
	int a, b; 
}
MUCHIE;

typedef struct nod
{
	int cheie;
	int grad;
	struct nod* parinte;
} NOD;

int nr=0;
MUCHIE edge[60001];
NOD *V[10001];   //pt det comp. conexe


void initializare()
{
	int i;
	for(i=1;i<=60001;i++)
		edge[i]=*(MUCHIE*)malloc(sizeof(MUCHIE));
}


int verificare(int a, int b, int nr_muchii)  //verificam daca muchia nu a mai fost generata o data
{
	int i;
	for(i=1; i<nr_muchii; i++)
		if( ((edge[i].b==a) && (edge[i].a==b)) || ((edge[i].a==a) && (edge[i].b==b)))
			return 0;
	return 1;
}


void generare(int nr_varfuri, int nr_muchii)  //generam un nr de muchii si varfuri
{
	int i,a,b;
	int nr_curent_muchii=1;   //reprezinta numarul de muchii curent
	while(nr_curent_muchii<=nr_muchii)
		for(i = 1; i<=nr_varfuri; i++)
		{
			a = (rand()%(nr_varfuri))+1;
			b = (rand()%(nr_varfuri))+1;
			if((a != b) && (verificare(a,b,nr_muchii)))
			{
				edge[nr_curent_muchii].a = a;
				edge[nr_curent_muchii].b = b;
				nr_curent_muchii++;
			}
		}
}

NOD *grupare(int a)  //initializeaza seturile
{
	NOD *p;
	p=(NOD*)malloc(sizeof(NOD));
	p->cheie=a;
	p->parinte=p;
	p->grad=0;
	profiler.countOperation("multimi",nr,3);
	return p;
}



NOD *cautare_set(NOD *p)
{
	
	if(p!=p->parinte)
	{
		p->parinte=cautare_set(p->parinte);
		profiler.countOperation("multimi",nr);
	}
	return p->parinte;
}


void conectare(NOD *a, NOD *b)  //unim doua componente conexe
{
	if(a->grad > b->grad)
	{
		b->parinte=a;
		profiler.countOperation("multimi",nr);
	}
	else
	{
		a->parinte=b;
		profiler.countOperation("multimi",nr);
		profiler.countOperation("multimi",nr);
		if(a->grad == b->grad)
		{
			a->grad=b->grad+1;
			profiler.countOperation("multimi",nr);
		}
	}
	
}

void conect(NOD *a, NOD *b)
{
	conectare(cautare_set(a),cautare_set(b));
}



void componente_conexe(int nr_varfuri, int nr_muchii)
{
	int i;
	for(i=1; i<=nr_varfuri; i++)
	{
		V[i]=grupare(i);
		profiler.countOperation("multimi",nr);
	}
	for(i=1;i<=nr_muchii;i++)
	{
		profiler.countOperation("multimi",nr);
		if(cautare_set(V[edge[i].a])!=cautare_set(V[edge[i].b]))
		{
			conect(V[edge[i].a], V[edge[i].b]);
			profiler.countOperation("multimi",nr);
		}    
			
	}
}


void afis_muchii (int nr_muchii)
{
	int i;
	for(i=1;i<=nr_muchii; i++)
		cout<<edge[i].a<<edge[i].b;
}



void afis_conexa(int nr_muchii, int nr_varfuri)
{
	int i,j;
	for(i=1;i<=nr_varfuri;i++)
		cout<<"parintele lui "<<V[i]->cheie<<"este :"<<V[i]->parinte->cheie<<endl;
	for(i=1;i<=nr_varfuri;i++)
	{
		cout<<"multimea "<<i<<" este formata din :";
		for(j=1;j<=nr_varfuri;j++)
		{
			if(cautare_set(V[j])->cheie==i)
				cout<< V[j]->cheie<<"  ";
		}
		cout<<endl;
	}
}




void main()
{
	int nr_varfuri=7;
	int nr_muchii=3;

	//FillRandomArray(edge,i,nr_varfuri*nr_varfuri+1,true,0);
	srand(time(NULL));
	generare (nr_varfuri, nr_muchii);
	afis_muchii(nr_muchii);
	componente_conexe(nr_varfuri,nr_muchii);
	afis_conexa(nr_muchii,nr_varfuri);
	/*int i;
	for(i=10000;i<=60000;i=i+1000)
	{
	 FillRandomArray(edge,i,nr_varfuri*nr_varfuri+1,true,0);
	 componente_conexe(nr_varfuri,nr_muchii);
	}
	 profiler.showReport();*/
	getch();
}