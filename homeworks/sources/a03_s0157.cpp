/*
Lorinczi ***ANONIM***
***GROUP_NUMBER***

in the average case both algorithms ha a complexity of nlgn

the qiucksort in the worst case hase a complexity of n^2
and in the best case hes nlgn


*/

#include<stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <time.h>

int b[10005],c[10005],n,assig[2][102], comp[2][102],ac[2][102] ;
FILE *f;

void heapify(int a[], int i,int n, int opr)
{
	int l;
	int r;
	int m;
	int hlp;
	l=2*i;
	r=2*i+1;
	m=i;

	if(l<=n && a[l]>a[i]  ) m=l; comp[0][opr]++;
	if(r<=n && a[r]>a[m]) m=r; comp[0][opr]++;
	if(m!=i)
	{hlp=a[m]; a[m]=a[i]; a[i]=hlp; assig[0][opr]+=3;
	heapify(a,m,n,opr);}
}

void botupheap(int a[], int n, int opr)
{
for(int i=n/2;i>=1;i--)
heapify( a,i,n,opr);
}


void heapsort(int a[], int n,int opr)
{int aux;
	botupheap(a, n,opr);
		for(int i =n; i>=2; i--)
		{	aux=a[i];
			a[i]=a[1];
			a[1]=aux;  assig[0][opr]+=3;
			n=n-1;
			heapify(a,1,n,opr);
		}

}

int partition( int start, int end,int opr)
{
int x,i,j,aux;
x=c[end];assig[1][opr]++;
i=start-1;
for(j=start; j<=end-1; j++)
{if(c[j]<=x )
		{i++;
if(i!=j)
{		aux=c[i];
		c[i]=c[j];
		c[j]=aux; assig[1][opr]+=3;}}
comp[1][opr]++;}
aux=c[i+1];
c[i+1]=c[end];
c[end]=aux; assig[1][opr]+=3;
return i+1;
}




void quicksort( int start, int end,int opr)
{int q;
	if(start<end)
		{q=partition(start,  end, opr);
		quicksort(start, q-1, opr);
		quicksort( q+1,  end, opr);}
}

int partition2( int start, int end,int opr)
{

int x=(start+end)/2;
int aux;
	aux=c[end];
	c[end]=c[x];
	c[x]=aux; assig[1][opr]+=3;

return partition(start,end,opr);
}




void quicksort2( int start, int end,int opr)
{int q;
	if(start<end)
		{q=partition2(start,  end, opr);
		quicksort2(start, q-1, opr);
		quicksort2( q+1,  end, opr);}
}
int main()
{

srand(time(NULL));

b[1]=4; b[3]=2; b[5]=15; b[7]=1; b[9]=10;
b[2]=5; b[4]=6; b[6]=9; b[8]=7; b[10]=14;
b[11]=12; b[12]=13; b[13]=11; b[14]=3; b[15]=8;
for(int i =1; i<=15;i++)
{
printf("%d ",b[i]);
}printf("\n");
heapsort(b,15,0);
for(int i =1; i<=15;i++)
{
printf("%d ",b[i]);
}
printf("\n");


c[1]=4; c[3]=2; c[5]=15; c[7]=1; c[9]=10;
c[2]=5; c[4]=6; c[6]=9; c[8]=7; c[10]=14;
c[11]=12; c[12]=13; c[13]=11; c[14]=3; c[15]=8;
for(int i =1; i<=15;i++)
{
printf("%d ",c[i]);
}
printf("\n");
quicksort( 1, 15,0);
for(int i =1; i<=15;i++)
{
printf("%d ",c[i]);
}
printf("\n");

/*
for(n=100;n<=10000;n+=100)
{
	for(int j=1; j<=5;j++)
{
for(int i=1;i<=n;i++)
{b[i]=rand() % 20000;
c[i]=b[i];}
heapsort(b,n,n/100);
quicksort(c,1,n,n/100);

	}
	printf("%d ", n);
}


for(int i=1;i<=100;i++)
{
	ac[0][i]=assig[0][i]+comp[0][i];
	ac[1][i]=assig[1][i]+comp[1][i];
	ac[0][i]=ac[0][i]/5;
	ac[1][i]=ac[1][i]/5;
	
	assig[0][i]=assig[0][i]/5;
	assig[1][i]=assig[1][i]/5;
	
	comp[0][i]=comp[0][i]/5;
	comp[1][i]=comp[1][i]/5;
}

f=fopen("sorting.csv","w");
	fprintf(f,"n,assig heap, assig quick, comp heap, comp quick, ac heap, ac quick\n");
	for(int i=1;i<=100;i++)
	{	fprintf(f,"%d,",i*100);
		fprintf(f,"%d,",assig[0][i]);
		fprintf(f,"%d,",assig[1][i]);
		fprintf(f,"%d,",comp[0][i]);
		fprintf(f,"%d,",comp[1][i]);
		fprintf(f,"%d,",ac[0][i]);
		fprintf(f,"%d\n",ac[1][i]);
	}

for(int i=1;i<=100;i++)
{assig[1][i]=0;
comp[1][i]=0;
ac[1][i]=0;}
*/

/*
for(n=100;n<=10000;n+=100)

{
	for(int i=1;i<=n;i++)
{c[i]=i;}
	
	quicksort(1,n,n/100);
printf("%d ", n);}


for(int i=1;i<=100;i++)
{	ac[1][i]=assig[1][i]+comp[1][i];}

f=fopen("sorting2.csv","w");
	fprintf(f,"n, assig quick, comp quick, ac quick\n");
	for(int i=1;i<=100;i++)
	{	fprintf(f,"%d,",i*100);
		fprintf(f,"%d,",assig[1][i]);
		fprintf(f,"%d,",comp[1][i]);
		fprintf(f,"%d\n",ac[1][i]);	}
*/


for(int i=1;i<=100;i++)
{assig[1][i]=0;
comp[1][i]=0;
ac[1][i]=0;
}
for(int i=1;i<=10000;i++)
{c[i]=i;}

for(n=100;n<=10000;n+=100)
{quicksort2(1,n,n/100);
printf("%d ", n);}

for(int i=1;i<=100;i++)
{	ac[1][i]=assig[1][i]+comp[1][i];}

f=fopen("sorting3.csv","w");
	fprintf(f,"n, assig quick, comp quick, ac quick\n");
	for(int i=1;i<=100;i++)
	{	fprintf(f,"%d,",i*100);
		fprintf(f,"%d,",assig[1][i]);
		fprintf(f,"%d,",comp[1][i]);
		fprintf(f,"%d\n",ac[1][i]);}

getch();
}