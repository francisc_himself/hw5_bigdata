/*
As we can see from the graphs bubble sort is clearly the most inefficient, not only at the number of comparisons, but also when looking at the number
of assignments made during the sort in average case.

Comparing the average case running time of the selection and insertion sort algorithms, we observe that when comparing assignments, we have noticeably
fewer in the case of selection sort (the graph almost seems linear), while in the case of the number of comparisons made, the insertion sort algorithm
executes 2 times less comparisons than the selection sort.
When comparing the total number of operations in the average case we can see that both Insertion and Selection Sort have approximately the same number
of total operations, therefore both can be used, but Bubble still remains the worst.

Comparing the best case running time of the selection, bubble and insertion sort algorithms, we observe that Bubble Sort is the best algorithm when it
comes to Assignments ( has 0 ), followed by Insertion and Selection, in this case Selection being the worst amongst them 3.
As far as Comparisons go, Insertion is the best algorithm ( seems linear ), followed by Selection and Bubble.
When comparing the total number of operations in the best case we can see that Insertion Sort has the least number of total operations, therefore
it can be considered the best and Bubble by far the worst.

Comparing the worst case running time of the selection, bubble and insertion sorting algorithms, we observe that Selection Sort is the best algorithm
when it comes to Assignments ( seems linear ), followed by Insertion and Bubble.
As far as Comparisons go, Insertion and Selection Sort behave approximately the same, their curves overlap, which indicates that in most of the
cases choosing between the 2 comes down to a matter of preference.
When comparing the total number of operations in  the worst case we can see that Selection Sort has the least number of total operations, therefore 
it can be considered the best and Bubble by far the worst.

Selection sort
Worst case performance	O(n^2)
Best case performance	O(n^2)
Average case performance	O(n^2)

Insertion sort
Worst case performance	O(n^2) comparisons, swaps
Best case performance	O(n) comparisons, O(1) swaps
Average case performance	O(n^2) comparisons, swaps

Bubble sort
Worst case performance	O(n^2)
Best case performance	O(n)
Average case performance	O(n^2)

*/
 
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <math.h>
#include <fstream>
using namespace std;
#define MAX_SIZE 10

void main() {
	int i,j,k,k1,k2,buff,aux,a[10000],b[10000],c[10000];

	int final0=0, comp0=0, assi0=0;
	int final1=0, comp1=0, assi1=0;
	int final2=0, comp2=0, assi2=0;
	int n=20;

	for (i=0;i<n;i++)
	{
		a[i] = rand() %100;
		b[i] = rand() %100;
		c[i] = rand() %100;
	}


	//INSERT SORT
	//cout<<"Insert sort"<<"\n";
	for (j=1;j<n;j++)
	{	
		aux=a[j];
		assi0++;
		k=j-1;
		while ((a[k] > aux) && (k>=0))
		{
			a[k+1]=a[k];
			k--;
			assi0++;
			comp0++;
		}
		comp0++;
		a[k+1]=aux;
		assi0++;
	}
	final0 = comp0+assi0;

	//SELECT SORT
	//cout<<"Selection sort";
	for (k1=0;k1<=n-2;k1++)
	{
		buff=k1;
		for (k2=k1+1;k2<=n-1;k2++)
		{
			comp1++;
			if (b[buff]>b[k2])
			{
				buff=k2;
			}
		}
		if (buff!=k1)
		{
			comp1++;
			aux=b[k1];
			b[k1]=b[buff];
			b[buff]=aux;
			assi1+=3;
		}
	}
	final1 = comp1+assi1;

	//BUBBLE SORT
	//cout<<"Bubble sort";
	for(i=0; i<n; i++)
	{
		for(j=0; j<n-1; j++)
		{
			comp2++;
			if(c[j]>c[j+1])
			{
				aux = c[j+1];
				c[j+1] = c[j];
				c[j] = aux;
				assi2+=3;
			}
		}
	}
	final2 = comp2+assi2;
	cout<<"Insertion Sort:"<<endl;
	for (i=0;i<n;i++)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;
	cout<<"Selection Sort:"<<endl;
		for (i=0;i<n;i++)
	{
		cout<<b[i]<<" ";
	}
	cout<<endl;
	cout<<"Bubble Sort:"<<endl;
			for (i=0;i<n;i++)
	{
		cout<<c[i]<<" ";
	}


	getch();
}