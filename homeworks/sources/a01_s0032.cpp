#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include "Profiler.h"
int n;
/*
------------------------ANALIZA REZULTATELOR-----------------------

---------Cazul favorabil-------------------------------------------

--Asignare---------------------------------------------------------

Din acest punct de vedere bubble-sortul este cel mai eficient,
deoarece nu efectueaza asignare, iar cea mai ineficienta este
sortarea prin inserare.

--Comparare--------------------------------------------------------

Numarul compararilor in cazul bubble-sortului creste liniar,
in timp ce la insert sau select sort creste exponential.

---------Cazul defavorabil------------------------------------------

--Asignare----------------------------------------------------------

In acest caz select-sortul realizeaza cel mai putine asignari,
iar insert si bubble-sortul sunt la aceasi marime, crescand 
exponential.

--Comparare----------------------------------------------------------

Cele trei tipuri de sortari, in cazul defavorabil executa operatia
de comparare aproximativ de acelasi numar de ori, cu o crestere
exponentiala.

---------Cazul mediu-------------------------------------------------

--Asignare-----------------------------------------------------------

Asignarile se efectueaza in mod liniar in cazul sortarii prin
selectie, iar in celelalte doua cazuri in mod exponential.

--Comparare----------------------------------------------------------

In cazul sortarii prin selectie cresterea este cea mai rapida,
iar in cazul bubble-sortului cea mai lenta.

----------------------------------------------------------------------
*/

void bubble_sort(int *v,int n,int &nratr,int &nrcomp)
{
	int ok=0;
	int i,j,aux;
	nratr=0;
	nrcomp=0;
	i=0;
		while(ok==0)
		{
			ok=1;
			i++;
	        for(j=0;j<n-i;j++)
			{nrcomp++;
              if(v[j]>v[j+1])
			    {
					aux=v[j];
		            v[j]=v[j+1];
				    v[j+1]=aux;
					nratr+=3;
					ok=0;
		        }
			}
         }
}
void insert_sort(int *v,int n,int &nratr,int &nrcomp)
{
	int i,j,x,k;
	nrcomp++;
	for(i=1;i<n;i++)
	{
		x=v[i];
		j=0;
		nrcomp++;
		while(v[j]<x)
			{
				j=j+1;
				nrcomp++;
		    }
		for(k=i;k>=j+1;k--)
		{
			v[k]=v[k-1];
			nratr++;
		}
		v[j]=x;
		nratr++;
	}
}
void select_sort(int *v,int n,int &nratr,int &nrcomp)
{
	int i,j,imin,aux;
	nratr = 0;
	nrcomp = 0;
	for(i=0;i<n-1;i++)
	{
		imin=i;
		for(j=i+1;j<n;j++)
		{  nrcomp++;
			if(v[j]<v[imin])
				imin=j;
		}
		aux=v[i];
		v[i]=v[imin];
		v[imin]=aux;
		nratr+=3;
	}
}
int main(void)
{   int i,k,j,atr[3],comp[3];
     FILE *f;
    printf("Working...\n");
	f = fopen("rezultate.csv","w");
	fprintf(f, "n,bubble_fav_asig,bubble_fav_comp,bubble_fav_asig+comp,select_fav_asig,select_fav_comp,select_fav_asig+comp,insert_fav_asig,inserti_fav_comp,insert_fav_asig+comp,bubble_defav_asig,bubble_befav_comp,bubble_defav_asig+comp,select_defav_asig,select_defav_comp,select_defav_asig+comp,insert_defav_asig,inserti_defav_comp,insert_defav_asig+comp,bubble_mediu_asig,bubble_mediu_comp,bubble_mediu_asig+comp,select_mediu_asig,select_mediu_comp,select_mediu_asig+comp,insertion_mediu_asig,insert_mediu_comp,insert_mediu_asig+comp\n");
    for(n=100;n<=10000;n+=100)
	{
		atr[1] = 0;
		atr[2] = 0;
		atr[3] = 0;
		comp[1] = 0;
		comp[2] = 0;
		comp[3] = 0;
		fprintf(f, "%d,", n);
		 int *vect_fav_select=(int *)malloc(n*sizeof(int));
		 int *vect_fav_bubble=(int *)malloc(n*sizeof(int));
		 int *vect_fav_insert=(int *)malloc(n*sizeof(int));
		 int *vect_defav_select=(int *)malloc(n*sizeof(int));
		 int *vect_defav_bubble=(int *)malloc(n*sizeof(int));
		 int *vect_defav_insert=(int *)malloc(n*sizeof(int));
		 int *vect_primar=(int *)malloc(n*sizeof(int));
		 int *vect_bubble=(int *)malloc(n*sizeof(int));
		 int *vect_select=(int *)malloc(n*sizeof(int));
		 int *vect_insert=(int *)malloc(n*sizeof(int));
        for(i=0;i<n;i++)
		{
			vect_fav_select[i]=i;
            vect_fav_bubble[i]=i;
		    vect_fav_insert[i]=i;
		}
        bubble_sort(vect_fav_bubble,n,atr[0],comp[0]);
		fprintf(f,"%d,%d,%d,",atr[0],comp[0],atr[0]+comp[0]);
		select_sort(vect_fav_select,n,atr[0],comp[0]);
		fprintf(f,"%d,%d,%d,",atr[0],comp[0],atr[0]+comp[0]);
		insert_sort(vect_fav_insert,n,atr[0],comp[0]);
	    fprintf(f,"%d,%d,%d,",atr[0],comp[0],atr[0]+comp[0]);

		for(i=n;i>0;i--)
	    {
			vect_defav_select[n-i]=i;
            vect_defav_bubble[n-i]=i;
		    vect_defav_insert[n-i]=i;
		}
        bubble_sort(vect_defav_bubble,n,atr[0],comp[0]);
		fprintf(f, "%d,%d,%d,", atr[0],comp[0],atr[0]+comp[0]);
		select_sort(vect_defav_select,n,atr[0],comp[0]);
		fprintf(f, "%d,%d,%d,", atr[0],comp[0],atr[0]+comp[0]);
		insert_sort(vect_defav_insert,n,atr[0],comp[0]);
		fprintf(f, "%d,%d,%d,", atr[0],comp[0],atr[0]+comp[0]);
	

		

		for(j=0;j<5;j++)
		{FillRandomArray(vect_primar,n,10,50000,true,0);

		 for(k=0;k<n;k++)
		     {
				 vect_bubble[k]=vect_primar[k];
		         vect_select[k]=vect_primar[k];
				 vect_insert[k]=vect_primar[k];
			     
		     }
         bubble_sort(vect_bubble,n,atr[0],comp[0]);
			atr[1] = atr[1] + atr[0];
			comp[1] = comp[1] + comp[0];
		 select_sort(vect_select,n,atr[0],comp[0]);
			atr[2] = atr[2] + atr[0];
			comp[2] = comp[2] + comp[0];
		 insert_sort(vect_insert,n,atr[0],comp[0]);
			atr[3] = atr[3] + atr[0];
			comp[3] = comp[3] + comp[0];
		 
		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,\n", atr[1] / 5, comp[1] / 5, (atr[1] + comp[1]) / 5, atr[2] / 5, comp[2] / 5, (atr[2] + comp[2]) / 5, atr[3] / 5, comp[3] / 5, (atr[3] + comp[3]) / 5);
	}

	getche();
fclose(f);
return 0;


}