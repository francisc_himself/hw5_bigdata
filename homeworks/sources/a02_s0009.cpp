/*
Cazul mediu
Bottom-up:O(n*Log2(n))
Top-down:O(n*Log2(n))



*/
#include <stdio.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <fstream>
#include <iostream>
#include <deque>
#include <iomanip>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;



#define  MINUS_INFINIT -32000000
#define MAX_SIZE 10000

int heapsize;
unsigned long long atr_bottom=0,cmp_bottom=0,atr_top=0,cmp_top=0;




int parinte(int i) {

     return (i-1)/2;
}

int stanga(int i)
{
return 2*i+1;
}

int dreapta(int i)
{
	return 2*i+2;
}






void interschimba(int *a,int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}

//----------------------------------------BOTTOM-UP-------------------------------------------------------------------

void maxHeapify(int a[],int i,int length)
{
	heapsize=length;
	int largest;
	int l=stanga(i);
	int r=dreapta(i);
	cmp_bottom++;
	if (l<heapsize && a[l]>a[i])
			largest=l;
	else largest=i;
	cmp_bottom++;
	if (r<heapsize && a[r]>a[largest])
		largest=r;
	if (largest!=i)
	{
		atr_bottom=atr_bottom+3;
		interschimba(&a[i],&a[largest]);
		//printf("\n%d <=> %d\n",a[i],a[largest]);
		maxHeapify(a,largest,length);
	}
}


void buildMaxHeap(int a[],int length)
{
	int i;
	heapsize=length;
	for (i=length/2-1; i>=0; i--)
	{
		//printf("\n%d \n",i);
		maxHeapify(a,i,length);
	}
}



 //-------------------------------------Top-Down---------------------------------------------





void heapIncreaseKey(int a[],int i,int key)
{
	cmp_top++;
	if (key<a[i])
	{
		perror("New key is smaller than current key");
		//exit(-1);
	}
	atr_top++;
	a[i]=key;
	cmp_top++;
	while (i>0 && a[parinte(i)]<a[i])
	{		
			cmp_top++;
			atr_top=atr_top+3;
			 interschimba(&a[i],&a[parinte(i)]);
			 i=parinte(i);
		
		
	}
	

}

void maxHeapInsert(int a[],int key)
{
	heapsize++;
	atr_top++;
	a[heapsize]=MINUS_INFINIT;
	heapIncreaseKey(a,heapsize,key);
}

void buildMaxHeap2(int a[],int length)
{
	heapsize=0;
	int i;
	for (i=1; i<length; i++)
		maxHeapInsert(a,a[i]);	
}



//-----------------------------------Pretty-Print------------------------------------------

struct BinaryTree {
  BinaryTree *left, *right;
  int data;
  BinaryTree(int val) : left(NULL), right(NULL), data(val) { }
};

// Find the maximum height of the binary tree
int maxHeight(BinaryTree *p) {
  if (!p) return 0;
  int leftHeight = maxHeight(p->left);
  int rightHeight = maxHeight(p->right);
  return (leftHeight > rightHeight) ? leftHeight + 1: rightHeight + 1;
}

// Convert an integer value to string
string intToString(int val) {
  ostringstream ss;
  ss << val;
  return ss.str();
}

// Print the arm branches (eg, /    \ ) on a line
void printBranches(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel, const deque<BinaryTree*>& nodesQueue, ostream& out) {
  deque<BinaryTree*>::const_iterator iter = nodesQueue.begin();
  for (int i = 0; i < nodesInThisLevel / 2; i++) {  
    out << ((i == 0) ? setw(startLen-1) : setw(nodeSpaceLen-2)) << "" << ((*iter++) ? "/" : " ");
    out << setw(2*branchLen+2) << "" << ((*iter++) ? "\\" : " ");
  }
  out << endl;
}

// Print the branches ***ANONIM*** node (eg, ___10___ )
void printNodes(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel, const deque<BinaryTree*>& nodesQueue, ostream& out) {
  deque<BinaryTree*>::const_iterator iter = nodesQueue.begin();
  for (int i = 0; i < nodesInThisLevel; i++, iter++) {
    out << ((i == 0) ? setw(startLen) : setw(nodeSpaceLen)) << "" << ((*iter && (*iter)->left) ? setfill('_') : setfill(' '));
    out << setw(branchLen+2) << ((*iter) ? intToString((*iter)->data) : "");
    out << ((*iter && (*iter)->right) ? setfill('_') : setfill(' ')) << setw(branchLen) << "" << setfill(' ');
  }
  out << endl;
}

// Print the leaves only (just for the bottom row)
void printLeaves(int indentSpace, int level, int nodesInThisLevel, const deque<BinaryTree*>& nodesQueue, ostream& out) {
  deque<BinaryTree*>::const_iterator iter = nodesQueue.begin();
  for (int i = 0; i < nodesInThisLevel; i++, iter++) {
    out << ((i == 0) ? setw(indentSpace+2) : setw(2*level+2)) << ((*iter) ? intToString((*iter)->data) : "");
  }
  out << endl;
}

// Pretty formatting of a binary tree to the output stream
// @ param
// level  Control how wide you want the tree to sparse (eg, level 1 has the minimum space between nodes, while level 2 has a larger space between nodes)
// indentSpace  Change this to add some indent space to the left (eg, indentSpace of 0 means the lowest level of the left node will stick to the left margin)
void printPretty(BinaryTree *root, int level, int indentSpace, ostream& out) {
  int h = maxHeight(root);
  int nodesInThisLevel = 1;

  int branchLen = 2*((int)pow(2.0,h)-1) - (3-level)*(int)pow(2.0,h-1);  // eq of the length of branch for each node of each level
  int nodeSpaceLen = 2 + (level+1)*(int)pow(2.0,h);  // distance between left neighbor node's right arm ***ANONIM*** right neighbor node's left arm
  int startLen = branchLen + (3-level) + indentSpace;  // starting space to the first node to print of each level (for the left most node of each level only)
    
  deque<BinaryTree*> nodesQueue;
  nodesQueue.push_back(root);
  for (int r = 1; r < h; r++) {
    printBranches(branchLen, nodeSpaceLen, startLen, nodesInThisLevel, nodesQueue, out);
    branchLen = branchLen/2 - 1;
    nodeSpaceLen = nodeSpaceLen/2 + 1;
    startLen = branchLen + (3-level) + indentSpace;
    printNodes(branchLen, nodeSpaceLen, startLen, nodesInThisLevel, nodesQueue, out);

    for (int i = 0; i < nodesInThisLevel; i++) {
      BinaryTree *currNode = nodesQueue.front();
      nodesQueue.pop_front();
      if (currNode) {
	      nodesQueue.push_back(currNode->left);
	      nodesQueue.push_back(currNode->right);
      } else {
        nodesQueue.push_back(NULL);
        nodesQueue.push_back(NULL);
      }
    }
    nodesInThisLevel *= 2;
  }
  printBranches(branchLen, nodeSpaceLen, startLen, nodesInThisLevel, nodesQueue, out);
  printLeaves(indentSpace, level, nodesInThisLevel, nodesQueue, out);
}

//--------------------------------------------MAIN-----------------------------------------------------------------------------
int main()
{
	//int a[7]={9,7,1,5,3,20,13};
	//int b[7]={9,7,1,5,3,20,13};

	int a[10]={4,1,3,2,16,9,10,14,8,7};
	int b[10]={4,1,3,2,16,9,10,14,8,7};
	


	int length=sizeof(a)/sizeof(int);
	
	


	buildMaxHeap(a,length);
	buildMaxHeap2(b,length);


	




	 BinaryTree *root2 = new BinaryTree(a[0]);
     root2->left = new BinaryTree(a[1]);
     root2->right = new BinaryTree(a[2]);
     root2->left->left = new BinaryTree(a[3]);
     root2->left->right = new BinaryTree(a[4]);
     root2->right->left = new BinaryTree(a[5]);
     root2->right->right = new BinaryTree(a[6]);
	 root2->left->left->left   = new BinaryTree(a[7]);
	 root2->left->left->right   = new BinaryTree(a[8]);
     root2->left->right->left  = new BinaryTree(a[9]);
   
     printf("\nBottomUp\n\n");
	
	 printPretty(root2, 2, 0, cout);

	 printf("\n\n\n\n");


	BinaryTree *root1 = new BinaryTree(b[0]);
     root1->left = new BinaryTree(b[1]);
     root1->right = new BinaryTree(b[2]);
     root1->left->left = new BinaryTree(b[3]);
     root1->left->right = new BinaryTree(b[4]);
     root1->right->left = new BinaryTree(b[5]);
     root1->right->right = new BinaryTree(b[6]);
	 root1->left->left->left   = new BinaryTree(b[7]);
	 root1->left->left->right   = new BinaryTree(b[8]);
     root1->left->right->left  = new BinaryTree(b[9]);
     printf("\nTopDown\n\n");
	
	 printPretty(root1, 2, 0, cout);

/*	int a[MAX_SIZE];
	int b[MAX_SIZE];
	

	FILE *file1;

	file1 = fopen("Heap.csv", "w");
	
	
	fprintf(file1, "n,Suma_Bottom,Suma_Top\n");

	for(int n=100; n<MAX_SIZE; n += 100)
{

	for (int j=1;j<=5; j++)
	{
		for (int i=0; i<n; i++)
		{	
			
			a[i]=r***ANONIM***();
			b[i]=a[i];
		}
		heapsize=0;
		buildMaxHeap(a,n);
		buildMaxHeap2(b,n);
		
	}
	fprintf(file1, "%d,%llu,%llu\n",n,(atr_bottom+cmp_bottom)/5,(atr_top+cmp_top)/5);

atr_bottom=0;
cmp_bottom=0;
atr_top=0;
cmp_top=0;
}

	fclose(file1);


	FILE *file2;

	file2 = fopen("Worst.csv", "w");
	
	
	fprintf(file2, "n,Suma_Bottom,Suma_Top\n");
atr_bottom=0;
cmp_bottom=0;
atr_top=0;
cmp_top=0;

	for(int n=100; n<MAX_SIZE; n += 100)
{

		a[0]=r***ANONIM***();
		b[0]=a[0];
		for (int i=1; i<n; i++)
		{	
			
			a[i]=a[i-1]+r***ANONIM***();
			b[i]=a[i];
		}
		heapsize=0;
		buildMaxHeap(a,n);
		buildMaxHeap2(b,n);
		
	fprintf(file2, "%d,%llu,%llu\n",n,(atr_bottom+cmp_bottom),(atr_top+cmp_top));

atr_bottom=0;
cmp_bottom=0;
atr_top=0;
cmp_top=0;
}

	fclose(file2);


	*/

	getch();
	return 0;
}


