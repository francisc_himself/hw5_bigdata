

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <time.h>
#include "Profiler.h"
Profiler profiler("Sorting");


int A[10002],B[10001],dimB;



int parent (int i)
{
	return (i/2);
}

void H_PUSH(int B[],int x, int max_elem)
{
	dimB=dimB+1;
	B[dimB]=x;
	int i=dimB;
	while((i>1) && (B[parent(i)]<=B[i]))
	{
		profiler.countOperation("topDown_comp", max_elem, 1);
		profiler.countOperation("topDown_total", max_elem, 1);
		int aux;
		aux=B[parent(i)];
		B[parent(i)]=B[i];
		B[i]=aux;
		profiler.countOperation("topDown_assign", max_elem, 3);
		profiler.countOperation("topDown_total", max_elem, 3);
		i=parent(i);
	}
	profiler.countOperation("topDown_comp", max_elem, 1);
	profiler.countOperation("topDown_total", max_elem, 1);
}


void BuildHeapTD(int A[], int max_elem)
{
	int i;
	dimB=0; 
	for (i=1; i<=max_elem; i++)
		H_PUSH(B,A[i], max_elem);
	for (i=1; i<=max_elem; i++)
		A[i]=B[i];
}
	
int mymax (int x, int y, int z, int j, int max_elem){
if(x>=y)
	{	
		profiler.countOperation("bottomUp_comp", max_elem, 1);
		profiler.countOperation("bottomUp_total", max_elem, 1);
		if(x>=z) 
		{
			profiler.countOperation("bottomUp_comp", max_elem, 1);
			profiler.countOperation("bottomUp_total", max_elem, 1);
			return j;//x
		}
		else 
		{
			profiler.countOperation("bottomUp_comp", max_elem, 1);
			return (2*j);//z
		}
	}
	else 
		if(y>=z) 
		{
			profiler.countOperation("bottomUp_comp", max_elem, 1);
			profiler.countOperation("bottomUp_total", max_elem, 1);
			return (2*j+1);//y
		}
	else 
		{
			profiler.countOperation("bottomUp_comp", max_elem, 1);
			profiler.countOperation("bottomUp_total", max_elem, 1);
			return (2*j);//z
		}
}

void heapify (int A[], int j, int max_elem)
{
	int index;
	if((2*j+1<=max_elem) || (2*j<=max_elem))
	     { 
			index=mymax(A[j], A[2*j+1], A[2*j], j, max_elem);
			if (index!=j) 
			{
					int aux;
					aux=A[j];
					A[j]=A[index];
					A[index]=aux;
					profiler.countOperation("bottomUp_assign", max_elem, 3);
					profiler.countOperation("bottomUp_total", max_elem, 3);
					heapify(A, index, max_elem);
			}
	}
}


void BuildHeapBU (int A[], int max_elem)
{
  int i;
  for(i=max_elem/2; i>=1; i--)
		  heapify (A, i, max_elem);
	  
}


void main(){
	int i,j,k,p;
	int v[10001];   	
	/*for(k=0;k<5;k++){
	for(i=100;i<=10000;i=i+500)
	{
		   
		FillRandomArray(v, i);
		
		for(p=1;p<=i;p++)
			A[p] = v[p];
		BuildHeapTD(A,i);
		for(p=1;p<=i;p++)
			A[p] = v[p];
		BuildHeapBU(A,i);
	
		printf("Done for %d\n", i);
		}
	}*/

	profiler.createGroup("Comparisons", "topDown_comp", "bottomUp_comp");
	profiler.createGroup("Assignements", "topDown_assign", "bottomUp_assign");
	profiler.createGroup("Total operations", "topDown_total", "bottomUp_total");
	profiler.showReport();
	printf("Done all!");
	
//Test pt heeap	
	int c[21] = {20, 14, 12, 11, 17, 21, 71, 9, 4, 12, 31, 40, 21, 22, 15, 1, 19, 14, 51, 10, 0};
	printf("\n Original array: \n");
	for(p=1;p<21;p++)
		printf("%d, ", c[p]);
	for(p=1;p<21;p++)
			A[p] = c[p];
		BuildHeapTD(A,20);
	printf("\n After TD build: \n");
	for(p=1;p<21;p++)
		printf("%d, ", A[p]);
	for(p=1;p<21;p++)
			A[p] = c[p];
		BuildHeapBU(A,20);
	printf("\n After BU build: \n");
	for(p=1;p<21;p++)
		printf("%d, ", A[p]);
	getch();


}
