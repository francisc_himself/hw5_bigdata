// Efectul Josephus
// 348

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

//structura arbore
typedef struct tip_nod {
	int cheie;
	int dim;
	struct tip_nod *stanga;
	struct tip_nod *dreapta;
	struct tip_nod *parinte;
} tip_nod;

int nmax=sizeof(tip_nod), nrA, nrC;
tip_nod *rad, *p;

tip_nod *construireArbore(int stg, int dr) {

	tip_nod *q;
	int m;
	nrC++;
	if (stg<=dr)
	{
		m=(stg+dr)/2;
		q=(tip_nod *)malloc(nmax);
		q->cheie=m;
		q->parinte=NULL;
		nrA+=4;
		q->stanga=construireArbore(stg,m-1);
		q->dreapta=construireArbore(m+1,dr);
		if ((q->stanga==NULL)&&(q->dreapta==NULL)) 
		{
			nrA++;
			q->dim=1;
		}
		else if (q->stanga==NULL) 
		{
			nrA++;
			q->dim=q->dreapta->dim+1;
		}
		else if 
			(q->dreapta==NULL) 
		{
			nrA++;
			q->dim=q->stanga->dim+1;
		}
		else 
		{ 
			nrA++;
			q->dim=q->stanga->dim + q->dreapta->dim + 1; 
		}
		if (q->stanga!=NULL) 
		{ 
			nrA++;
			q->stanga->parinte=q; 
		}
		if (q->dreapta!=NULL) 
		{ 
			nrA++;
			q->dreapta->parinte=q; 
		}
		return q;
	}
	return NULL;
}

tip_nod *so_selectie(tip_nod *q,int i) {// E341 - returneaza nodul cu a i`a cea mai mica cheie din subarborele cu radacina q

	nrC+=3;
	int r;
	if (q==NULL) {
		r=0;
	}
	else if (q->stanga!=NULL) {
		r=q->stanga->dim+1; // caut rangul nodului curent
	}
	else {
		r=1;
	}
	if (i==r)
	{
		nrA++;
		return q;
	}
	else 
	{
		if (i<r)
		{
			return so_selectie(q->stanga,i); // caut in subarborele stang
		}
		else
		{
			if (q->dreapta!=NULL)
				return so_selectie(q->dreapta,i-r); // caut in subarborele drept
		}
	}
}

tip_nod *arboreMinim(tip_nod *x) {//291 - ultimul element extrem stang

	while (x->stanga!=NULL)
	{
		nrA++;
		x=x->stanga;
	}
	return x;
}

tip_nod *succesor(tip_nod *x) {//R212 - succesor in ordinea de sortare determinat prin traversarea in inordine

	//succesor = nodul cu cea mai mica cheia, mai mare decat al nodului x;
	tip_nod *y;
	nrC++;
	if (x->dreapta!=NULL) { //se cauta cel mai din stanga nod al arborelui drept
		return arboreMinim(x->dreapta);
	}
	nrA++;
	y=x->parinte;
	while ((y!=NULL)&&(x==y->dreapta)) { // se cauta stramosul y care are ca si fiu stang tot un stramos a lui x
		nrA+=2;
		x=y;
		y=y->parinte;
	}
	return y;
}


tip_nod *stergeArbore(tip_nod *z)
{

	tip_nod *y,*x,*t;
	nrC+=6;
	if (z==NULL) {
		return NULL;
	}
	if ((z->stanga==NULL)||(z->dreapta==NULL))
	{
		nrA++;
		y=z;
	}
	else
	{
		nrA++;
		y=succesor(z);
	}
	//x se seteaza fie la fiul diferit de NULL a lui y, fie la NULL daca nu are fi
	if (y->stanga!=NULL) 
	{ 
		nrA++;
		x=y->stanga; 
	}
	else 
	{ 
		nrA++;
		x=y->dreapta; 
	}
	if (x!=NULL) 
	{
		nrA++;
		x->parinte=y->parinte; 
	}
	if (y->parinte==NULL) 
	{ 
		nrA++;
		rad=x; 
	}
	else if (y==y->parinte->stanga) 
	{ 
		nrA++;
		y->parinte->stanga=x; 
	}
	else 
	{ 
		nrA++;
		y->parinte->dreapta=x; 
	} 
	if (y!=z)
	{
		nrA++;
		z->cheie=y->cheie;// se copiaza si datele aditionale ale lui x
		z->dim--;
	}
	//intretinere dimensiune
	t=z;
	while (t->parinte!=NULL)
	{
		t->parinte->dim--;
		t=t->parinte;
		nrA+=2;
	}
	return y;
}



//afisare arbore
void afisareArbore(tip_nod *t,int d)
{
	int j;
	if (t==NULL) {return;}
	afisareArbore(t->dreapta,d+1);
	for (j=0;j<=d;j++)
	{
		printf("     ");
	}
	printf("%d %d\n",t->cheie,t->dim);
	afisareArbore(t->stanga,d+1);
}

void Josephus(int n, int m) {

	tip_nod *x, *y;
	int i,dimIntermediara;
	rad=construireArbore(1,n);
	i=m;
	dimIntermediara = n;
	while(rad!=NULL)
	{
		nrA++;
		x=so_selectie(rad,i);
		y=stergeArbore(x);
		free(y);
		dimIntermediara--;
		if (dimIntermediara!=0) {
			i=(i+m-1)%dimIntermediara;
		}
		if (i==0) {
			i=dimIntermediara;
		}
	}
}

void Josephus2(int n, int m) {

	tip_nod *x, *y;
	int i,dimensiune;
	rad=construireArbore(1,n);
	i=m;
	dimensiune = n;
	while(rad!=NULL)
	{
		nrA++;
		x=so_selectie(rad,i);

		printf("%d ",x->cheie);
		y=stergeArbore(x);

		free(y);

		dimensiune--;
		if (dimensiune!=0) {
			i=(i+m-1)%dimensiune;
		}
		if (i==0) {
			i=dimensiune;
		}
	}
}

int main () {	

	FILE *f;
	int i,m,ok,n;
	tip_nod *x,*y;
	f=fopen("date.csv","w");
	printf("Date aleatoare sau date introduse de la tastatura?\n");
	printf("1=date aleatoare\n");
	printf("2=date introduse\n");
	scanf("%d",&ok);
	if (ok==1)
	{
		//printare antet coloane in fisier
		fprintf(f,"%s, %s, %s, %s\n","n","nrA","nrC","nrT");
		fprintf(f,"\n");
		n=100;
		while (n<=10000)
		{
			nrA=0;
			nrC=0;
			m=n/2;

			Josephus(n,m);

			fprintf(f,"%d, %d, %d, %d\n",n,nrA,nrC,nrA+nrC);
			n=n+100;
		}

		//inchidere fisier
		fclose(f);
	}
	else {

		//initializari
		nrA=0;nrC=0;
		printf("dati numarul de elemente n= ");
		scanf("%d",&n);
		printf("\ndati m= ");
		scanf("%d",&m);

		//construire arbore
		rad=construireArbore(1,n);
		//afisare arbore
		printf("\n");
		afisareArbore(rad,1);

		//construire permutare

		printf("\nJ(%d %d)= ",n,m);
		Josephus2(n,m);

	}
	printf("\n \ndone`");
	getch();
	return 0;
}