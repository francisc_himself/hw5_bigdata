//***ANONIM*** ***ANONIM***
//***GROUP_NUMBER***
#include <stdio.h>
#include<stdlib.h>
#include <time.h>

int a[10000],b[10000],c[10000];
int nr_at,nr_c;
FILE *pf;

//functie ce genereaza 3 vectori de cate 10000 de elemente intregi
//vectorul a este aleator pentru cazul mediu statistic
//vectorul b este ordonat crescator pentru cazul cel mai favorabil
//vectorul c este ordonat descrescator ***ANONIM*** cazul cel mai defavorabil
void aleatoare()
{
	int i;
	a[0]=(int)rand()%10000;
	b[0]=(int)rand()%50;
	c[0]=50100-(int)rand()%50;
	for(i=1;i<10000;i++)
	{
		a[i]=(int)rand()%10000;
		b[i]=b[i-1]+(int)rand()%15;
		c[i]=c[i-1]-(int)rand()%5;
		
	}
}
//functia de sortare prin metoda bulelor
//cat timp vectorul nu este sortat se interschimba cate doua elemente consecutive care nu respecta relatia de ordine dorita
void bule(int n,int tmp[])
{
	int aux, s=1,i;
	while(s)
	{
		s=0; 
		for(i=0;i<n-1;i++)
		{	
			nr_c++;
			if (tmp[i]>tmp[i+1])
			{
				aux=tmp[i]; nr_at++;
				tmp[i]=tmp[i+1]; nr_at++;
				tmp[i+1]=aux; nr_at++;
				s=1;
			}
		}
	}

}
//functia de sortare prin selectie
//se selecteaza mereu minimul dintre valorile ramase ne sortate din vector  
void selectie( int n,int vector[])
{
	int i,j,jmin,min,aux;
	for (i=0;i<n;i++)
	{
		min=vector[i]; nr_at++;
		jmin=i;
		for(j=i+1;j<=n;j++)
		{
			nr_c++;
			if(vector[j]<min)
			{
				min=vector[j]; nr_at++;
				jmin=j;
			}

		}
		aux=vector[i]; nr_at++;
		vector[i]=vector[jmin];nr_at++;
		vector[jmin]=aux; nr_at++;
	}
}
//functia de sortare prin insertie
//pentru fiecare valoare din vector se cauta pozitia pe care ar trebui sa apara in vectorul sortat anterior
void insertie(int n,int vector[])
{
	int i,j,v;
    for (i=1; i < n; i++) 
	{
		v=vector[i]; nr_at++;
		j=i-1;
		nr_c++;
		while ( j >= 0 && vector[j] > v) 
		{
			vector[j+1]= vector[j]; nr_at++;
			j--;
			vector[j+1]=v; nr_at++;
			nr_c++;
		}
	}
}
//se iau pe rand vectori temporari pornind de la 100 de elemente apna la 1000 si se sorteaza prin metoda bulelor
//pentru fiecare sortare a vectorului temporar se numara atribuirile si comparatiile intr-un vector care se afiseaza apoi in fisierul .csv
void sort1(int vector[])
{int n,i,j, at[100],cm[100];
int tmp[10000];
for(n=100;n<=10000;n=n+100)
{
	for(i=0;i<n;i++)
		tmp[i]=vector[i];
	nr_at=0; nr_c=0;
	bule(n,tmp);
at[n/100-1]=nr_at;
cm[n/100-1]=nr_c;
}
	for(j=0;j<n/100-1;j++)
		fprintf(pf,"%d,",at[j]);
	fprintf(pf,"\n");
	for(j=0;j<n/100-1;j++)
		fprintf(pf,"%d,",cm[j]);
	fprintf(pf,"\n");

	for(j=0;j<10000;j++)

			printf("%d ",tmp[j]);

	printf("\n\n");
}
//se aplica aceeasi metoda de a crea vectori auziliari si de a numar atribuirile si comparatiile dar de aceasta data pentru metoda sortarii prin selectie
void sort2(int vector[], int tip)
{int n,i,j, at[1000],cm[1000];
int tmp[10000];
int aux;
for(n=100;n<=10000;n=n+100)
	///pentru cazul cel mai defavorabil al sortarii mrin selectie nu este potrivit vectorul sortat descrescator 
	//de aceea el trebuie rotit cu o pozitie spre stanga vectorul ordonat descrescator si apoi schimbate intre ele ultimele doua elemente ale acestuia 
{
	for(i=0;i<n;i++)
		tmp[i]=vector[i];
	if(tip==0)
	{
	aux=tmp[0];
	for(j=0;j<n;j++)
		tmp[j]=tmp[j+1];
	tmp[n-1]=aux;
	aux=tmp[n-2];
	tmp[n-2]=tmp[n-1];
	tmp[n-1]=aux;
	}
	nr_at=0; nr_c=0;
selectie(n,vector);
at[n/100-1]=nr_at;
cm[n/100-1]=nr_c;
}
	for(j=0;j<n/100-1;j++)
		fprintf(pf,"%d,",at[j]);
	fprintf(pf,"\n");
	for(j=0;j<n/100-1;j++)
		fprintf(pf,"%d,",cm[j]);
	fprintf(pf,"\n");

	for(j=0;j<10000;j++)
			printf("%d ",tmp[j]);
			printf("\n\n");


}
//se formeaza si se sorteaza vectori temorari prin metoda sortarii prin insertie
//se observa cazul cel mai defavorabil este cazul mediu statistic nu cel in care vectorul este ordonat 
void sort3(int vector[])
{int n,i,j, at[1000],cm[1000];
int tmp[10000];
for(n=100;n<=10000;n=n+100)
{
	for(i=0;i<n;i++)
		tmp[i]=vector[i];
	nr_at=0; nr_c=0;
	insertie(n,tmp);
at[n/100-1]=nr_at;
cm[n/100-1]=nr_c;
}
	for(j=0;j<n/100-1;j++)
		fprintf(pf,"%d,",at[j]);
	fprintf(pf,"\n");
	for(j=0;j<n/100-1;j++)
		fprintf(pf,"%d,",cm[j]);
	fprintf(pf,"\n");

	for(j=0;j<10000;j++)

			printf("%d ",tmp[j]);

	printf("\n");
	
}
int main()
{
aleatoare();
pf=fopen("fis1.csv","a");
//eficienta:O(n^2) pentru toate cele 3 metode
//metoda bulelor
//////////////////////////////caz mediu statistic
sort1(a);
//////////////////////////////caz favorabil
sort1(b);
/////////////////////////////caz defavorabil
sort1(c);
/////
//metoda selectiei
//////////////////////////////caz mediu statistic
sort2(a,1);
//////////////////////////////caz favorabil
sort2(b,1);
/////////////////////////////caz defavorabil
sort2(c,0);
/////
//metoda sortarii prin insertie
//////////////////////////////caz mediu statistic
sort3(a);
//////////////////////////////caz favorabil
sort3(b);
/////////////////////////////caz defavorabil
sort3(c);
/////




fclose(pf);
getchar();
return 0;
}



