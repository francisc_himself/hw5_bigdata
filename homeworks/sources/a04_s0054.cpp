#include <stdio.h>
#include <stdlib.h>
#include <conio.h> 
#include <time.h>

int atr,comp;	//nr atribuiri, comparatii 


struct heap {
	int v;	 //valoare
	int i;	 //index
};

typedef struct Nod{
	int nr;	
	struct Nod *urm;
}Nod;


int st (int i)
{
return 2*i;
}

int dr (int i)
{
return 2*i+1;
}

int parinte (int i)
{
return i/2;
}

void creare (Nod **prim, Nod **ultim)
	{
	*prim = 0;
	*ultim = 0;
	}

void inserare (Nod **prim, Nod **ultim, int k)
{
	Nod *p;
	p = (Nod *) malloc(sizeof(Nod));
	p->nr = k;

if (*prim == 0)
{
	*prim = p;
	*ultim = p;
}
else
{
	(*ultim)->urm = p;
	*ultim = p;
}
	p->urm = 0;
}

Nod* sterge (Nod **prim, Nod **ultim)
{
Nod *p;

if (*prim == 0)
{
return NULL;
}

p = *prim;
*prim = (*prim)->urm;
return p;
}

void afisare (Nod *prim)
{
Nod *p;
p = (Nod *) malloc(sizeof(Nod));

if (prim==0)
printf ("Lista e vida!");
else
{
p = prim;
while (p!=0)
{
printf ("%d ",p->nr);
p = p->urm;
}
}
printf ("\n");
}


void initializare (heap H[10001],int *dim_heap)
{
*dim_heap = 0;
}

void Push (heap a[10001], int *dimens, int valoare, int index)
{
int i;
heap aux;

*dimens = *dimens + 1;
atr+=1;
a[*dimens].i = index;
a[*dimens].v = valoare;
i = *dimens;
while (i>1 && a[i].v < a[parinte(i)].v)
{
comp++;
atr+=3;
aux = a[i];
a[i] = a[parinte(i)];
a[parinte(i)] = aux;

i = parinte (i);
}
comp++;
}

void reconstruct (heap a[10001],int dim_heap, int i)
{
int s,d,ind;
heap aux;

s = st (i);
d = dr (i);

ind = i;

comp++;
if (s<=dim_heap && a[s].v<a[ind].v)
ind = s;

comp++;
if (d<=dim_heap && a[d].v<a[ind].v)
ind = d;

if (ind != i)
{
atr+=3;
aux = a[i];
a[i] = a[ind];
a[ind] = aux;

reconstruct (a,dim_heap,ind);
}	
}


heap Pop (heap a[10001], int *dimens)
{
heap e;
if (*dimens > 0)
{
atr+=2;
e = a[1];
a[1] = a[*dimens];
*dimens = *dimens -1;
reconstruct (a,*dimens,1);
}
return e;
}


void interclasare (Nod* pcap[10001],Nod* ucap[10001], int k,Nod **pout,Nod **uout,heap H[10001], int *dim_heap)
{
initializare (H,dim_heap);
creare (pout,uout);
Nod* p;
p=(Nod *) malloc(sizeof(Nod));

for (int i=1; i<=k; i++)
{
comp++;
if (pcap[i]!=NULL)
{
atr++;
p = sterge (&(pcap[i]),&(ucap[i]));
}
else
p = NULL;
comp++;
if (p != NULL)
Push (H,dim_heap,p->nr,i);
}

while (*dim_heap > 0)
{
atr++;
heap e = Pop (H,dim_heap);
inserare (pout,uout,e.v);
Nod *q;
atr++;
q = sterge (&(pcap[e.i]),&(ucap[e.i]));
comp++;
if (q != NULL)
Push (H,dim_heap,q->nr,e.i);
}
}

void rezolva_a ()
{
int n,k;
int j,l,val,c;	// l = lungimea unei liste; lultima = lungimea ultimei liste
Nod *prim[10001],*ultim[10001];	 //listele cu elementele prim su ultim
Nod *pout,*uout;
heap H[10001];
int dim_heap;
srand(time(NULL));

FILE *pf5,*pf10,*pf100;

pf5 = fopen("k5.csv","w");
pf10 = fopen("k10.csv","w");
pf100 = fopen("k100.csv","w");


k = 5;

while (k<=100)
{
//generam k liste ca caror dimensiuni insumate n
for (n=100; n<=10000; n+=100)
{
atr = comp = 0;
//generam lungimea listelor
l = n/k;
if (l * k == n)
{
//avem k liste de lungime egala
for (j=1;j<=k;j++)
{	
creare (&prim[j],&ultim[j]);

//generam primul elem
val = rand () %100 +1;

inserare (&prim[j],&ultim[j],val);

//generam celelalte elem
for (c=2;c<=l;c++)
{
val = val + rand()%10 + 1;
inserare (&prim[j],&ultim[j],val);
}
}
}
else
{
if (l != 0)
{
//generam k-1 liste de lungime egala l si o lista de lungime mai mare
for (j=1;j<k;j++)
{
creare (&prim[j],&ultim[j]);

//generam primul elem
val = rand () %100 +1;
inserare (&prim[j],&ultim[j],val);

//generam celelalte elemente
for (c=2;c<=l;c++)
{
val = val + rand()%10 + 1;
inserare (&prim[j],&ultim[j],val);
}
}
//lungimea ultimei liste
l = n- (k-1) * l;

creare (&prim[k],&ultim[k]);

//generam primul elem
val = rand () %100 +1;
inserare (&prim[k],&ultim[k],val);

//generam celelalte elementee
for (c=2;c<=l;c++)
{
val = val + rand()%10 + 1;
inserare (&prim[j],&ultim[j],val);
}
}
else
{
for (j=1;j<=k;j++)
{
creare (&prim[j],&ultim[j]);
}

for (c=1; c<=n; c++)	 
{
val = rand () %100 +1;
inserare (&prim[c],&ultim[c],val);
}
}
}


interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
switch (k)
{
case 5:
fprintf (pf5,";%d ;;;;%d ;;;;%d\n",k,n,atr+comp);
break;
case 10:
fprintf (pf10,";%d ;;;;%d ;;;;%d\n",k,n,atr+comp);
break;
case 100:
fprintf (pf100,";%d ;;;;%d ;;;;%d\n",k,n,atr+comp);
break;
}
}
if (k == 5)
k = 10;
else 
{
if (k == 10)
k = 100;
else
k = 101;
}	
}

}

void rezolva_b ()
{
int n,k;
int j,l,val,c;	//l = lungimea unei liste; lultima = lungimea ultimei liste
Nod *prim[10001],*ultim[10001];	 //listele cu elementele prim su ultim
Nod *pout,*uout;
heap H[10001];
int dim_heap;
srand(time(NULL));

FILE *pf;

pf = fopen("n10000.csv","w");

n = 10000;

for (k=10; k<=500; k+=10)
{
//generam listele
atr = comp = 0;
//generam lungimea listelor
l = n/k;
if (l * k == n)
{
//avem k liste de lungime egala
for (j=1;j<=k;j++)
{	
creare (&prim[j],&ultim[j]);

//generam primul element
val = rand () %100 +1;

inserare (&prim[j],&ultim[j],val);

//generam celelalte elemente
for (c=2;c<=l;c++)
{
val = val + rand()%10 + 1;
inserare (&prim[j],&ultim[j],val);
}
}
}
else
{
if (l != 0)
{
//generam k-1 liste de lungime egala l si o lista de lungime mai mare
for (j=1;j<k;j++)
{
creare (&prim[j],&ultim[j]);

//generam primul element
val = rand () %100 +1;
inserare (&prim[j],&ultim[j],val);

//generam celelalte elemente
for (c=2;c<=l;c++)
{
val = val + rand()%10 + 1;
inserare (&prim[j],&ultim[j],val);
}
}
//lungimea ultimei liste
l = n- (k-1) * l;

creare (&prim[k],&ultim[k]);

//generam primul element
val = rand () %100 +1;
inserare (&prim[k],&ultim[k],val);

//generam celelalte elemente
for (c=2;c<=l;c++)
{
val = val + rand()%10 + 1;
inserare (&prim[j],&ultim[j],val);
}
}
else
{
for (j=1;j<=k;j++)
{
creare (&prim[j],&ultim[j]);
}

for (c=1; c<=n; c++)	 //cat mai avem elemente punem cate unul in fiecare lista
{
val = rand () %100 +1;
inserare (&prim[c],&ultim[c],val);
}
}
}


interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);

fprintf (pf,";%d;;;%d;;;%d\n",k,n,atr+comp);
}
}


void test ()
{
int n,k;
k=4;
n=8;
Nod *prim[4],*ultim[4];
Nod *pout,*uout;
heap H[10001];
int dim_heap;

creare (&prim[1],&ultim[1]);
inserare (&prim[1],&ultim[1],2);
inserare (&prim[1],&ultim[1],7);
inserare (&prim[1],&ultim[1],10);

afisare (prim[1]);


creare (&prim[2],&ultim[2]);
inserare (&prim[2],&ultim[2],1);
inserare (&prim[2],&ultim[2],5);
inserare (&prim[2],&ultim[2],12);

afisare (prim[2]);

creare (&prim[3],&ultim[3]);
inserare (&prim[3],&ultim[2],11);
inserare (&prim[3],&ultim[2],35);

afisare (prim[3]);


creare (&prim[4],&ultim[4]);
inserare (&prim[4],&ultim[3],13);
inserare (&prim[4],&ultim[3],25);
inserare (&prim[4],&ultim[3],30);

afisare (prim[4]);
interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
printf ("Interclasare:\n");
afisare (pout);
}

int main ()
{
test ();
rezolva_a ();
rezolva_b();
getche ();
return 0;
}