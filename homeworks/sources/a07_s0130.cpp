#include <stdio.h>
//#include "Profiler.h"
#include <iostream>
#include <conio.h>
#include <fstream>
using namespace std;
#define maxnb 9

struct MultiWay
{
	int value;
	bool root;
	int nbOfChilds;
	MultiWay **childs;
}**T2;

struct BinaryTree
{
	int val;
	BinaryTree *child, *brother;
}**T3;

void T1ToT2(int t1[])
{
	T2=(MultiWay**)(malloc(maxnb*sizeof(MultiWay)));

	for(int i=0; i<maxnb; i++)
	{
		T2[i]=(MultiWay*)(malloc(sizeof(MultiWay)));
		T2[i]->childs=(MultiWay**)(malloc(maxnb*sizeof(MultiWay)));
		T2[i]->nbOfChilds=0;
		T2[i]->value=i+1;
		T2[i]->root=false;
	}

	for(int i=0; i<maxnb; i++)

	{
		if(t1[i]==-1)
		{
			T2[i]->root=true;
		}
		else
		{

			T2[t1[i]-1]->childs[T2[t1[i]-1]->nbOfChilds]=T2[i];
			T2[t1[i]-1]->nbOfChilds++;
		}
	}
}

void printT2(MultiWay **t, int pos)
{
	for(int i=0; i<maxnb; i++)
	{
		printf("%d:", t[i]->value);
		for(int k=0; k<t[i]->nbOfChilds; k++)
			printf(" %d,", t[i]->childs[k]->value);
		printf("\n");
	}
}

void T2ToT3(BinaryTree *T3, MultiWay *t2, int pos, int k)
{
	T3->val=t2->value;
	for(int i=0; i<pos; i++)
		printf("  ");
	printf("%d \n", T3->val);
	if(t2->nbOfChilds>0){
	T3->child=T3+(t2->childs[0]->value-1);
	T2ToT3(T3->child, t2->childs[0], pos+1, (k+1)%maxnb);
	}
	BinaryTree *aux=T3->child;
	for(int i=1;i< t2->nbOfChilds; i++ )
		{
			aux->brother=T3+t2->childs[i]->value-1;
			T2ToT3(aux->brother, t2->childs[i], pos+1, (k+1)%maxnb);
			aux=aux->brother;
			
		}
}

int main ()
{
	int T1[]={2,7,5,2,7,7,-1,5,2};
	T1ToT2(T1);
	int k=0;
	while(!T2[k]->root)
		k++;
	printf("Multi-Way Tree:\n");
	printT2(T2,0);
	
	T3=(BinaryTree**)malloc(maxnb*sizeof(BinaryTree));
	for(int i=0; i<maxnb; i++)
	{
		T3[i]=(BinaryTree*)malloc(sizeof(BinaryTree));
		T3[i]->brother=NULL;
		T3[i]->child=NULL;
		T3[i]->val=i;
	}

	printf("\nBinary Tree: \n");
	T2ToT3(T3[0], T2[k], 0, k);
	
	getch();
}