#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include "Profiler.h"

#define MAX 10000
/****ANONIM*** ***ANONIM*** ***ANONIM*** gr.***GROUP_NUMBER***
	Algoritmul urmator este un algoritm de interclasare pentru k liste ordonate, avand
n elemente. Algoritmul de interclasare utilizeaza intr-un ciclu for de k ori: un algoritm 
de h_push cu eficienta O(log2k) si unul de stergere din lista cu O(1)=> eficienta pentru for = O(klog2k);
si un ciclu while : h_pop eficienta O(log2k),inserare si stergere lista O(1),  h_push cu eficienta O(log2k)
=>eficienta pentru while O(nlog2k). Astfel eficienta algoritmului este O(nlog2k).

*/
int nrA,nrC,lung;
typedef struct _NOD{
	int valoare;
	struct _NOD *next,*prev;
}NOD;
typedef struct _Heap_ELT
{
	int idList;
	int val;
}HEAP_ELT;

_Heap_ELT H[MAX];
NOD *head[MAX];
NOD *tail[MAX];
NOD *joinedHead, *joinedTail;
NOD *prim,*ultim;

void creare(NOD **prim,NOD **ultim)
{
	*prim=0;
	*ultim=0;
}


int parinte(int i){
	return i/2;
}
int left(int i){
	return 2*i;
}
int right(int i){
	return 2*i+1;
}

void h_init(int *n){
	*n=0;
}

void h_push(int x,int index,int *n){
	*n=*n+1;
	H[*n].val=x; 
	H[*n].idList=index;
	nrA++;
	int i=*n;
	while(i>=1 && H[i].val<H[parinte(i)].val){
		HEAP_ELT aux=H[i];
		H[i]=H[parinte(i)];
		H[parinte(i)]=aux;
		nrA+=3;
		nrC++;
		i=parinte(i);
	}
	nrC++;
}
void  reconstructie(int i, int dim)
{
	int st=left(i);
	int dr=right(i);
	int max;
	if (st<=dim && H[st].val<H[i].val){max=st;}
	else max=i;
	if(dr<=dim && H[dr].val<H[max].val){max=dr;}

	nrC+=2;
	if (max!=i){
		HEAP_ELT aux=H[i]; 
		H[i]=H[max];
		H[max]=aux;
		nrA+=3;

		reconstructie(max,dim);
	}

}

HEAP_ELT h_pop(int *n){
	HEAP_ELT x;

	if(*n>0){
		x=H[1];
		H[1]=H[*n];
		H[*n]=x; 
		nrA+=3;
		*n=*n-1;
		reconstructie(1,*n);
	}
	return x;
}

void elib_nod(NOD *p){
	free(p);
}
void afisare(NOD *h,NOD *t)
{
	NOD *p;
	p=h;
	do
	{
		printf("%d ",p->valoare);
		p=p->next;
	}while (p!=NULL);
	printf("\n");

}
void adaugare(NOD **h,NOD **t,int val)
{
	NOD *p;
	p=(NOD *)malloc(sizeof(NOD)); 
	if (*h==NULL)
	{
		*h=p;
		*t=p;

		(*h)->valoare=val;
	}
	else
	{
		p->prev=*t;
		(*t)->next=p;
		*t=p;

		(*t)-> valoare=val;
	}
	p->next=NULL;
}

NOD* sterge(NOD **h,NOD **t) 
{
	NOD *p;
	if(*h==NULL)
	{
		return NULL;
	}
	p=*h;
	*h=p->next;

	return p;
}

void interclasare(int k,NOD **cap,NOD **sf,int *n){
	h_init(n);
	creare(&joinedHead,&joinedTail);
	NOD *p,*q;
	p=(NOD *)malloc(sizeof(NOD));
	int i;
	for(i=0;i<k;i++){
		p=sterge(&(cap[i]),&(sf[i]));
		nrA++;
		h_push(p->valoare,i,n);
	}
	while(*n>0){
		HEAP_ELT h=h_pop(n);
		adaugare(&joinedHead,&joinedTail,h.val);
		q=(NOD *)malloc(sizeof(NOD));
		q=sterge(&(cap[h.idList]),&(sf[h.idList]));
		nrA+=2;
		if(q!=NULL)
			h_push(q->valoare,h.idList,n);
		nrC++;
	}

}

void main()
{
	srand(time(NULL));
	int v[MAX],vh[MAX];
	int v1[11]={0,1,2,3,4,5,6,7,8,9};
	int v2[11]={0,10,20,30,40,50,60,70,80,90};
	int v3[11]={0,15,25,35,45,55,65,75,85,95};


	HEAP_ELT heap;//[1000];
	for(int j=0;j<3;j++){
		int v[11];
		if(j==0) memcpy(v, v1, (10) * sizeof(int)); 
		if(j==1) memcpy(v, v2, (10) * sizeof(int)); 
		if(j==2) memcpy(v, v3, (10) * sizeof(int)); 
		for(int i=0;i<10;i++) {
			adaugare(&(head[j]),&(tail[j]),v[i]);}
		afisare(head[j],tail[j]); }
	int n=10;
	interclasare(3,head,tail,&n);
	afisare(joinedHead,joinedTail); 

	FILE  *f;
	f=fopen("graf.csv","w");
	fprintf(f,"n ; k=5 ; k=10 ; k=100\n");

	/*int k;
	for(int n=100;n<5000;n+=200){
		for(int i=0;i<3;i++){
			if(i==0) k=5;
			if(i==1) k=10;
			if(i==2) k=100;

			lung=n;
			printf("n=%d  k=%d\n",n,k);
			for(int j=0;j<k;j++){
				int v[MAX];

				FillRandomArray(v,n,1,50000,false,1);
				for(int i=0;i<n;i++){
					adaugare(&head[j],&tail[j],v[i]);
				}			
			}
			nrA=0;
			nrC=0;
			interclasare(k,head,tail,&n);
			switch (k)
			{
			case 5:fprintf(f,"%d ; %d ;",lung,nrC+nrA);
				break;
			case 10:fprintf(f,"%d ; ",nrC+nrA);
				break;
			case 100:fprintf(f,"%d \n",nrC+nrA);
				break;

			}
			n=lung;
		}
	}*/


	fclose(f);
	//k=5;
	FILE *f1;
	f1=fopen("grafCT.csv","w");
	fprintf(f1,"k ; n ; nrA+nrC\n");
	n=5000;
	//for(int n=100;n<5000;n+=200){
		for(int k=10;k<500;k+=10){
			lung=n;	
			printf("n=%d  k=%d\n",n,k);
			for(int j=0;j<k;j++){
				int v[MAX];

				FillRandomArray(v,n/k,1,50000,false,1);
				for(int i=0;i<n/k;i++){
					adaugare(&head[j],&tail[j],v[i]);
				}			
			}
			nrA=0;
			nrC=0;
			interclasare(k,head,tail,&n);
			n=lung;
			fprintf(f1,"%d;%d;%d\n",k,lung,nrA+nrC);
		}
		
		fclose(f1);
	}

