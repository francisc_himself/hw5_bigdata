#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <iostream>
using namespace std;

const int TABLE_SIZE = 10007;

int table[TABLE_SIZE];
const int AVAILABLE_SLOT = -1;

//fill factor: NrElements = fillFactor * TABLE_SIZE  fillfactor e [0,1]


void insert(int *hashF(int,int),int key) {
	
	int i = 0;
	int *tryPlace = hashF(key,i);

	while((table[*tryPlace] != AVAILABLE_SLOT) && (i < TABLE_SIZE)) { 
		i++;
		tryPlace = hashF(key,i);
	}

	if (i >= TABLE_SIZE) 
		exit(1);

	table[*tryPlace] = key;

}

int *hashF1(int key,int i) {
	int place=(key+2*i*i+3*i) % TABLE_SIZE;
	return &place;
}

//return the number of elements that are put in the table
//the numbers are generated in the interval [0,range-1]
int fillTable(int range, double fillFactor) {
	int nbElements = floor(fillFactor*TABLE_SIZE);

	//init all table
	for(int i = 0; i < TABLE_SIZE;i++)
		table[i] = AVAILABLE_SLOT;

	for(int i = 0; i < nbElements;i++) 
		insert(hashF1,rand() % range);		
	

	return nbElements;
}

int search(int *hashF(int,int),int key,int &accesses) {
	int i = 0;
	int *tryPlace = hashF(key,i);

	accesses++;
	while((table[*tryPlace] != key) && (table[*tryPlace] != AVAILABLE_SLOT) && (i < TABLE_SIZE)) {
		i++;
		accesses++;
		tryPlace = hashF(key,i);
	}
	
	if ( (i >= TABLE_SIZE) || (table[*tryPlace] == AVAILABLE_SLOT))
		return -1;
	else return key;

}

void searchGenerator(int range,int nbElements,int &foundAverage, int &foundMax,int &notFoundAverage,int &notFoundMax) {
	int accesses = 0;
	int nbFoundElements = 0;

	for(int i = 0;i <1500;) {
		accesses = 0;
		if (search(hashF1,rand() % range, accesses) > 0) {
			foundAverage += accesses;
			nbFoundElements++;
			i++;
			if(accesses > foundMax) foundMax = accesses;
		}
	}

	for(int i = 0;i <1500;i++) {
		accesses = 0;
		if (search(hashF1,range+rand() % range, accesses) > 0) {
			foundAverage += accesses;
			nbFoundElements++;			
			if(accesses > foundMax) foundMax = accesses;
		}else { 
			if(accesses > notFoundMax) notFoundMax = accesses;
			notFoundAverage += accesses;
		}
	}

	foundAverage /= 1500;
	notFoundAverage /= 1500;
}
void printResult(double fillFactor,int foundAverage, int foundMax,int notFoundAverage,int notFoundMax) {
	printf("%lf	%d         	%d		     %d   	     %d\n",fillFactor,foundAverage,foundMax,notFoundAverage,notFoundMax);
}

void each(double fillFactor,int range) {
	int nbElements = 0;
	int foundAverage = 0;
	int foundAverage5 = 0;
	int foundMax = 0;
	int notFoundAverage = 0;
	int notFoundAverage5 = 0;
	int notFoundMax = 0;

	nbElements = fillTable(range,fillFactor);
	for(int i = 0;i < 5;i++) {
		searchGenerator(range,nbElements,foundAverage,foundMax,notFoundAverage,notFoundMax);
		foundAverage5 += foundAverage;
		foundAverage = 0;
		notFoundAverage5 += notFoundAverage;
		notFoundAverage = 0;
	}

	foundAverage5 /=5;
	notFoundAverage5 /= 5;

	printResult(fillFactor,foundAverage5,foundMax,notFoundAverage5,notFoundMax);
}

void testing(){
	int nbElements = 0;
	int accesses = 0;
	nbElements = fillTable(7,1);
	if(search(hashF1, 2, accesses)>0){
		printf("s-a gasit");
	}
	else{
		printf("Nu s-a gasit");
	}
}

void main()
{
	int range = 30000;
	puts("fillFactor   foundAverage     foundMax       notFoundAverage     notFoundMax");

	each(0.8,range);
	each(0.85,range);
	each(0.90,range);
	each(0.95,range);
	each(0.99,range);

	//testing();

	getch();
}