/*
***ANONIM*** ***ANONIM*** GR ***GROUP_NUMBER*** sg 2
data: 2013.03.21

Tema 2: Heap
Avem un vector si il organizam intr-un heap.
Exista doua modalitati pentru a face acest lucru:
a) tehnica buttom-up
	luam elementele vectorului si il aranjam in ordine
	fii nodului tata sa fie mai mic decat tatal
b) tehnica top-down
	facem invers aranjarea in heap,,, de sus in jos aranjam elementele vectorului
	creand un alt vector numit heap





*/




#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>

int marime=0;
int b_as,b_co,t_as,t_co ;
int n;


int parinte(int i)
{
	return i/2;
}

int stanga(int i)
{
	return 2*i;
}

int dreapta(int i)
{
	return (2*i)+1;
}

void ReconstructieHeap(int a[],int i) 
{
	int st,dr,index,aux;
	st=stanga(i);
	dr=dreapta(i);
	b_co++;
	if(st<=marime && a[st]<a[i])
		index=st;
	else
		index=i;
	b_co++;
	if(dr<=marime && a[dr]<a[index])
		index=dr;
	if(index!=i)
	{
		aux=a[i];
		a[i]=a[index];
		a[index]=aux;
		b_as+=3;
		ReconstructieHeap(a,index);
	}
}

void ConstruiesteHeapBU(int a[]) 
{
	marime=n;
	for(int i=n/2;i>=1;i--)
		ReconstructieHeap(a,i);
}

void H_Push(int b[],int x)
{
	int i,aux;
	marime++;
	i=marime;
	t_as++;
	b[marime]=x;
	t_co++;
	while(i>1 && b[parinte(i)]>b[i])
	{
		t_co++;
		t_as+=3;
		aux=b[parinte(i)];
		b[parinte(i)]=b[i];
		b[i]=aux;
		i=parinte(i);
	}

}

void ConstruiesteHeapTD(int a[],int b[])
{
	marime=0;
	for(int i=1;i<=n;i++)
		H_Push(b,a[i]);

}

int main()
{
	/* 
	int w[10],q[10];
	w[1]=11;
	w[2]=8;
	w[3]=5;
	w[4]=1;
	w[5]=3;
	w[6]=4;
	 n=6;
	

	ConstruiesteHeapTD(w,q);
	for (int i=1; i<=n; i++)
		printf("%d ",q[i]);
	printf("\n");

	ConstruiesteHeapBU(w);
	for (int i=1; i<=n; i++)
		printf("%d ",w[i]);
	printf("\n");*/
//----------------------------------------------------------------------------------
	int k;
	int a[10001],b[10001];

	
	FILE *f=fopen("average.csv","w");
	fprintf(f, "n, asignaritd, compararitd, sumatd, atribuiribu, compararibu, sumabu\n");
	
	for(n=100;n<=10000;n+=100)
	{
		t_as=0;
		t_co=0;
		b_as=0;
		b_co=0;
		for(int k=1;k<=3;k++)
		{
			
			for(int i=0;i<n;i++)
			{
				srand(time(NULL));
				a[i]=rand();
			}
			ConstruiesteHeapTD(a,b);
			ConstruiesteHeapBU(a);
		}

		t_as=t_as/3;
		t_co=t_co/3;
		b_as=b_as/3;
		b_co=b_co/3;

		
		fprintf(f,"%d, %d, %d, %d, %d, %d, %d \n", n , t_as, t_co, t_as+t_co, b_as, b_co, b_as+b_co);
		
	}

	fclose(f);


	return 0;
}