/* 
When comparing the total operations in the average case, we can see that quicksort does far less operations than the bottom-up implementation of heapsort.
In this implementation of quicksort, the pivot is chosen as the middle element of the array.

When looking at the total number of operations executed in quicksort in all three (average, best and worst) cases, we notice the similarity between the
best and average case: both have O(nlgn). In the worst case on the other hand, the time complexity is of O(n^2), because at each split, the array is split
into to arrays of size 1 and n-1 and so on.

*/
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include<stdio.h>
#include<conio.h>

using namespace std;

fstream f1("case1.csv",ios::out);
fstream f2("case2.csv",ios::out);
fstream f3("case3.csv",ios::out);
fstream f4("case4.csv",ios::out);
fstream f5("case5.csv",ios::out);
fstream f6("worst.csv",ios::out);
fstream f7("best.csv",ios::out);
long cmp,atr;
long qC,qA;

//Random number generator for filling the array
void randomGenerator(int a[],int n)
{
    for (int i = 1 ; i <=n ; i++)
        a[i] = rand();
}

//Heap reconstructionBU
// heap[] : vector ; i : index , n: vector lengthq
void reconstructHeap(int heap[],int i,int n)
{
    int left = 2*i;
    int right = 2*i +1;
    int max;

        cmp++;
    if ( (left <= n) && ( heap[left] > heap[i] ) )
        max = left;
    else
        max = i;

        cmp++;
    if ( (right <= n ) && (heap[right] > heap[max]) )
        max = right;
		cmp++;
    if ( max != i )
    {
        int aux = heap[i];
        heap[i] = heap[max];
        heap[max] = aux;
        atr+=3;
        reconstructHeap(heap,max,n);
    }
}

//Constructing the Heap Bottom-Up
// heap[]:vector ; n: vector length
void constructHeapBU(int heap[],int n)
{
    for (int i = n/2 ; i>0 ; i--)
        reconstructHeap(heap,i,n);
}

//Sorting the Heap Bottom - Up
//heap[]:vector ; n: vector length; a[]:the vector that needs sorting
void heapSortBU(int a[],int heap[],int n)
{
    constructHeapBU(heap,n);
    while (n!=0)
    {
        a[n] = heap[1];
        heap[1]=heap[n];
        n--;
        atr+=2;
        reconstructHeap(heap,1,n);
    }
}
//Quicksort
void quickSort(int arr[], int left, int right)
{
	int i = left, j = right;
	int tmp;
	int pivot = arr[(left+right)/2];
	qA++;
	/* partition */
	while (i <= j) {
		qC++;
		while (arr[i] < pivot)
		{
			i++;
			qC++;
		}
		qC++;
		while (arr[j] > pivot)
		{
			qC++;
			j--;
		}

		if (i <= j) 
		{
			tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
			qA+=3;
			i++;
			j--;
		}
	}

	/* recursion */
	if (left < j)
		quickSort(arr, left, j);
	if (i < right)
		quickSort(arr, i, right);

}

int main()
{
	int a[10]={0,1,2,3,4,5,6,7,8,9},heap[13],a1[10001],b1[10000],heap1[10001];
    int n, whichFile;
	int dim;

	
    /*for ( whichFile = 1 ; whichFile <=5 ; whichFile++ )
    {

        for ( n = 100 ; n <= 10000; n+=300)
        {
            cmp = 0;
			atr = 0;
			qC = 0;
			qA = 0;
			// generates the random numbers from 1 to n
           //randomGenerator(a1,n); 

			// the heap gets populated
            for (int i = 0 ; i < n ; i++)
			{
				b1[i] = i;
				//cout<<b1[i]<<" ";
			}
			quickSort(b1,0,n-1);


			f6<<qC+qA<<"\n";
            //heapSortBU(a1,heap1,n);


            switch(whichFile)
            {
                case(1):f1<<qC+qA<<","<<cmp+atr<<"\n";break;
                case(2):f2<<qC+qA<<","<<cmp+atr<<"\n";break;
                case(3):f3<<qC+qA<<","<<cmp+atr<<"\n";break;
                case(4):f4<<qC+qA<<","<<cmp+atr<<"\n";break;
                case(5):f5<<qC+qA<<","<<cmp+atr<<"\n";break;
                default:break;
            }

        }

    }*/

	n = 12;
	int d=0;
	int b[12] = {5, 3, 65, 1, 54, 102, 43, 23, 10, 14, 32, 90};
	randomGenerator(a,n); //we generate the vector for which we do the simulation
	for (int i = 0 ; i < n ; i++)  //we fill the heap
    heap[i+1] = a1[i] = b[i];
	
	cout<<"Heap"<<endl<<endl;
	constructHeapBU(heap,n);

	for (int i=1; i<=n; i++)
		cout<<heap[i]<<" ";

	cout<<endl<<endl;
	cout<<"Heapsort"<<endl<<endl;
	heapSortBU(b,heap,n);
	for (int i=1; i<=n; i++)
		cout<<b[i]<<" ";
	cout<<endl<<endl<<"Quicksort"<<endl<<endl;

	quickSort(a1,0,n-1);
	for (int i=0; i<n; i++)
		cout<<a1[i]<<" ";
	getch();

    return 0;
}
