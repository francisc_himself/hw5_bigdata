//***ANONIM*** ***ANONIM*** Melania , grupa ***GROUP_NUMBER***
//Tema 8: Multimi disjuncte
/*
	Am testat programul si pe un numar mic de varfuri si arce: nr varfuri n=6 si nr arce m=7.
	Testam programul pe un numar fix de varfuri V_MAX=10 000 si un numar variabil de arce 
	pornind de la 10 000 si pana la E_MAX=60 000 din 1000 in 1000. Functia rezultata este liniara 
	conform asteptarilor.
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler profiler ("tema8");

#define V_MAX 10000
#define E_MAX 60000

typedef struct NOD{
	struct NOD *p;
	int rank;
	int key;
}NOD;

typedef struct _NODE{
	int key;
	struct _NODE *next;
}NODE;

NODE * adj[V_MAX];
int edges[E_MAX],x1;
NOD *prim[E_MAX];
int op;

//generare graf aleator: (n=varfuri;m=muchii)
void generate(int n,int m)
{
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE *nod=new NODE;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;
	}
}

//disj:
void MAKE_SET(int x)
{
	prim[x]=new NOD;//(NOD*)malloc(sizeof(NOD));;
	prim[x]->key=x;
	prim[x]->p=prim[x];
	prim[x]->rank=0;
	profiler.countOperation("Multimi_disjuncte",op,1);
}

int FIND_SET (int x)
{
	profiler.countOperation("Multimi_disjuncte",op,1);
	if(prim[x]!=prim[x]->p)
		prim[x]->p->key=FIND_SET(prim[x]->p->key);
	return prim[x]->p->key;
}

void LINK(int x,int y)
{
	if (prim[x]->rank>prim[y]->rank)
		prim[y]->p=prim[x];
	else 
	{
		prim[x]->p=prim[y];
		if(prim[x]->rank==prim[y]->rank)
			prim[y]->rank=prim[y]->rank+1;
	}
}

void UNION(int x,int y)
{
	profiler.countOperation("Multimi_disjuncte",op,1);
	LINK(FIND_SET(x),FIND_SET(y));
}

void CONNECTED_COMPONENTS(int n,int m)
{
	for (int i=0;i<n;i++)//each vertex v
		MAKE_SET(i);
	for (int i=0;i<m;i++)//each edge (u,v)
		if (FIND_SET(edges[i]/n)!=FIND_SET(edges[i]%n))
			UNION(edges[i]/n,edges[i]%n);
}

void main()
{
	int n,m;
	n=6;m=7;
	generate(n,m);
	for(int i=0;i<n;i++)
	{
		while(adj[i]!=NULL)
		{
			printf("%d",adj[i]->key);
			adj[i]=adj[i]->next;
		}
		printf("\n");
	}

	//pentru nr mic de muchii si arce
	//CONNECTED_COMPONENTS(n,m);
	op=0;
	for(int e=10000;e<=E_MAX;e+=1000)
	{
		op=e;
		CONNECTED_COMPONENTS(V_MAX,e);
	}
	profiler.showReport();

}