#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>

//Declaram structura unui nod din lista
typedef struct lista
{
    long info;
    struct lista *urm;
}lista;


lista *point[10001];   //Vectorul de liste
lista *prim,*ultim,*nod;   
long nrOperatii;

//Functie de adaugare in Lista Finala Sortata
void adaugareListaFinala(int val)
{
    if (prim==NULL)
    {
        prim=(lista*)malloc(sizeof(lista*));
        prim->info=val;
        prim->urm=NULL;
        ultim=prim;
    }
        else
    {
        nod=(lista*)malloc(sizeof(lista*));
        nod->info=val;
        nod->urm=NULL;
        ultim->urm=nod;
        ultim=nod;
    }
}

//Functie pentru afisarea Listei Finale Sortate
void afisare()
{
    lista *nod;
    nod=prim;
    while(nod!=NULL)
    {
        printf("%ld ",nod->info);
        nod=nod->urm;
    }
}

int st(int i)
{
    return 2*i;
}

int dr(int i)
{
    return 2*i+1;
}

int parinte(int i)
{
    return i/2;
}

/**
Verifica daca Heap-ul e gol
Returneaza 1 daca e gol, altfel 0
*/
int heapIsEmpty(lista *a[],int n)
{
    int isEmpty=1;
    for (int i=1;i<=n;i++)
    {
        if (a[i]!=NULL)
        {
            isEmpty=0;
        }
    }
    return isEmpty;
}

/**
A[] ->Vectorul asupra caruia aplicam Reconstructie_Heap
i -> Elementul de la care incepem
n -> Lungimea vectorului asupra caruia aplicam Reconstructie_Heap
*/
void Reconstructie_Heap(lista *A[],int i,int n)
{
	int stanga=st(i);
	int dreapta=dr(i);
	int maxim;
	lista *aux;
	nrOperatii++;
	if(stanga<=n && A[stanga]->info<A[i]->info)
	{
		maxim=stanga;
	}
	else
	{
		maxim=i;
	}
	nrOperatii++;
	if(dreapta<=n && A[dreapta]->info<A[maxim]->info)
	{
		maxim=dreapta;
	}
	if(maxim !=i)
	{
		nrOperatii+=3;
		aux=A[i];
		A[i]=A[maxim];
		A[maxim]=aux;
		Reconstructie_Heap(A,maxim,n);
	}
}
/**
Facem vectorul de liste A un heap cu metoda Bottom Up
A -> vectorul de liste
n -> Lungimea vectorului
*/
void Construieste_Heap(lista *A[],int n)
{
	for(int i=n/2;i>=1;i--) 
	 Reconstructie_Heap(A,i,n);
}
/**
Creaza Lista Finala Sortata
*/
void creareListaFinala(lista *a[],int n)
{
    int k;
    int element_scos;
    lista *aux;
    prim=NULL;
    nod=NULL;
    ultim=NULL;
    Construieste_Heap(a,n);
    k=n;
    while (heapIsEmpty(a,k)==0)
    {
        element_scos=a[1]->info;
        adaugareListaFinala(element_scos);
        if (a[1]->urm!=NULL)
        {
            nrOperatii++;
            a[1]=a[1]->urm;
        }
            else
        {
            nrOperatii+=3;
            aux=a[1];
            a[1]=a[k];
            a[k]=aux;
            k--;
        }
        Reconstructie_Heap(a,1,k);
    }

}
/**
 k=Numarul de Liste
 n=Numarul Total de Elemente
*/
void creareVectorDeListe(int k,int n)
{
    lista *prim,*nod,*ultim;
    int i,j;
    prim=NULL;
    nod=NULL;
    ultim=NULL;
    srand(time(NULL));
    for (i=0;i<n/k+n%k;i++) //Construim prima lista. Ea va fi mai lunga decat restul in cazul in care
                            //Numarul de elemente n nu e divizibil cu numarul de liste k
    {
        if (prim==NULL)
        {
            prim=(lista*)malloc(sizeof(lista*));
            prim->info=rand() % 100;
            prim->urm=NULL;
            ultim=prim;
        }
            else
        {
            nod=(lista*)malloc(sizeof(lista*));
            nod->info=ultim->info+i;
            nod->urm=NULL;
            ultim->urm=nod;
            ultim=nod;
        }
    }
    point[1]=prim;    //Punem prima lista in vectorul de liste
    for (j=2;j<=k;j++) // Continuam crearea vectorului de liste
    {
        prim=NULL;
        nod=NULL;
        ultim=NULL;
        for (i=0;i<n/k;i++) //Cream lista care urmeaza sa fie inserat in vectorul de liste. Va avea lungimea n/k
        {
            if (prim==NULL)
            {

                prim=(lista*)malloc(sizeof(lista*));
                prim->info=rand() % 100;
                prim->urm=NULL;
                ultim=prim;
            }
                else
            {
                nod=(lista*)malloc(sizeof(lista*));
                nod->info=ultim->info+i;
                nod->urm=NULL;
                ultim->urm=nod;
                ultim=nod;
            }
        }
        point[j]=prim;
    }
}
/**
Functie pentru afisarea Vectorului de liste a
a -> Vectorul de liste
n-> lungimea vectorului de liste
*/
void afisareVectordeListe(lista *a[],int n)
{
    int j;
    lista *nod;
    nod=NULL;
    for (j=1;j<=n;j++)
    {
        nod=a[j];
        while(nod!=NULL)
        {
            printf("%ld ",nod->info);
            nod=nod->urm;
        }
        printf("\n");
    }

}

void ecran()
{
    int n,k;
    n=20;
    k=6;
    creareVectorDeListe(k,n);
    afisareVectordeListe(point,k);
    printf("\n");
    creareListaFinala(point,k);
    afisare();
    printf("\n");
}
void main()
{
	ecran();
    int n;
    FILE *f;
    f=fopen("1.csv","w");
    fprintf(f,"n,k1=5,k2=10,k3=100\n");
    int k1=5,k2=10,k3=100;
    for (n=100;n<=10000;n=n+100)
    {
        fprintf(f,"%d,",n);

        nrOperatii=0;
        creareVectorDeListe(k1,n);
        creareListaFinala(point,k1);
        fprintf(f,"%ld,",nrOperatii);

        nrOperatii=0;
        creareVectorDeListe(k2,n);
        creareListaFinala(point,k2);
        fprintf(f,"%ld,",nrOperatii);

        nrOperatii=0;
        creareVectorDeListe(k3,n);
        creareListaFinala(point,k3);
        fprintf(f,"%ld\n",nrOperatii);
    }
	fclose(f);
    
	nrOperatii=0;
    n=10000;
    int k;
    FILE *g;
    g=fopen("2.csv","w");
    fprintf(g,"k,nr_operatii\n");
    for (k=10;k<=500;k=k+10)
    {
        nrOperatii=0;
        creareVectorDeListe(k,n);
        creareListaFinala(point,k);
        fprintf(g,"%d,%ld\n",k,nrOperatii);
    }
    fclose(g);
    //nrOperatii=0;
	printf("datele au fost scrise");
	getch();
}
