#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>

using namespace std;

enum colors {BLACK, WHITE, GRAY};
int nrAtr = 0 ,nrComp = 0;

typedef struct nodCoada{
	int inf;
	nodCoada *urm;
};

nodCoada *prim,*ultim;

typedef struct nod{
	int inf;
	int pred;
	int dist;
	int culoare;
	int n_muchi;
	int adj[100];
};

void enqueue(int a){
	nodCoada *p;
	p = (nodCoada*) malloc(sizeof(nodCoada));
	p->inf = a;
	p->urm = NULL;

	if(ultim == NULL){
		prim = ultim = p ;
	}
	else{
		ultim->urm = p;
		ultim = p;
	}
}

int dequeue(){
	nodCoada *p;
	int a;
	if(prim == NULL){
		return 0;
	}
	else{
		if(prim->urm == NULL){
			a = prim->inf;
			p = prim;
			prim = ultim = NULL;
			delete p;
		}
		else{
			a = prim->inf;
			p = prim;
			prim = p->urm;
			delete p;
		}
	}
	
	return a;
}

void BFS(nod *s , int n,nod *vect[]){
	for(int i = 0; i < n ;i++){
		vect[i]->culoare = WHITE;
		vect[i]->pred = 0;
		vect[i]->dist = 100;

		nrAtr += 3;
	}

	vect[s->inf]->culoare = GRAY;
	vect[s->inf]->pred = 0;
	vect[s->inf]->dist = 0;

	nrAtr += 3;

	enqueue(s->inf);

	nrComp++;
	int u ,v ,k;

	while(prim){
		u = dequeue();
		v = vect[u]->n_muchi;
		printf("%d \n",vect[u]->inf);
		for(int j =0;j<v;j++){
			k = vect[u]->adj[j];
			if(vect[k]->culoare == WHITE){

				nrComp++;
				nrAtr += 3;

				vect[k]->culoare = GRAY;
				vect[k]->pred = u;
				vect[k]->dist = vect[u]->dist+1;
				enqueue(k);
			}
			vect[u]->culoare = BLACK;

			nrAtr++;
		}
	}
}


int main(){
	
	nod *vect[10000],*nNou;
	prim = (nodCoada*) malloc(sizeof(nodCoada));
	ultim = (nodCoada*) malloc(sizeof(nodCoada));

	prim = NULL;
	ultim =NULL;

	int nrNoduri = 14;

	for(int i = 0;i < nrNoduri;i++){
		nNou = (nod*) malloc(sizeof(nod));
		nNou->inf = i;

		nNou->n_muchi = rand() % nrNoduri;
		for(int j=0;j< nNou->n_muchi;j++){
			nNou->adj[j] = rand() % nrNoduri ;
		}
		vect[i] = nNou;
	}

	BFS(vect[0],nrNoduri,vect);

	//Caz I
	/*FILE *fis;
	fis = fopen("fisier.csv","w");
	fprintf(fis,"atrib,comp,sum\n");

	int nrVf = 100,nrMchi = 1000;
	for(int i = 1000;i<=5000;i=i+100){
	    nrAtr = 0;
		nrComp = 0;

		//nr mediu de muchii pt fiecare nod
		int nrMed = nrMchi/nrVf;
		for(int j=0;j<nrVf;j++){
           nNou = (nod*) malloc(sizeof(nod));
		   nNou->inf = i;
		   for(int l=0;l<nrMed;l++){
			   nNou->adj[l] = rand() % nrVf;
			   //printf("%d ",nNou->adj[l]);
			   //verificam daca exista deja
			   int ok = 0;
			   for(int k=0;k<nNou->n_muchi;k++){
				   if(vect[nNou->adj[l]]->adj[k] == nNou->adj[j]){
					   ok++;
				   }
			   }
			   if(ok>0){
				   l--;
			   }
		   }

		   nNou->n_muchi = nrMchi;
		   vect[j] = nNou;

		}

		BFS(vect[0],nrVf,vect);
		fprintf(fis,"%d,%d,%d\n",nrAtr,nrComp,nrAtr+nrComp);
	}

	fclose(fis);*/

	//Caz II

	/*FILE *fis2;
	fis2 = fopen("fisier2.csv","w");

	int nrVf = 100,nrMchi = 9000;
	fprintf(fis2,"atrib,comp,sum\n");
		
	for(int i = 0;i<=200;i=i+10){
	    nrAtr = 0;
		nrComp = 0;

		//nr mediu de muchii pt fiecare nod
		int nrMed = nrMchi/nrVf;
		for(int j=0;j<nrVf;j++){
           nNou = (nod*) malloc(sizeof(nod));
		   nNou->inf = i;
		   for(int l=0;l<nrMed;l++){
			   nNou->adj[l] = rand() % nrVf;
			   //printf("%d ",nNou->adj[l]);
			   //verificam daca exista deja
			   int ok = 0;
			   for(int k=0;k<nNou->n_muchi;k++){
				   if(vect[nNou->adj[l]]->adj[k] == nNou->adj[j]){
					   ok++;
				   }
			   }
			   if(ok>0){
				   l--;
			   }
		   }

		   nNou->n_muchi = nrMchi;
		   vect[j] = nNou;

		}

		BFS(vect[1],nrVf,vect);
		fprintf(fis2,"%d,%d,%d\n",nrAtr,nrComp,nrAtr+nrComp);
	}
	fclose(fis2);*/
	getch();
}

