/*
***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***2
Heep sort:La fiecare pas, cel mai mic element din tablou este gasit si mutat in spatele tabloului,
	fiind ignorat de pasii urmatori, care vor continua pe restul tabloului. Arborele binar obtinut indeplineste urmatoarea conditie:
	�fiecare nod are cheia mai mare sau egala cu a tatalui sau�.
Quick Sortul : e o sortare de modul 'divire et impera'. imparte stringul in 2 dupa un anumit pivot.
- quick sortul are eficienta: O(n^2) in cazul defavorabil, O(nlgn) in cazul mediu statistic
				-heap sortul are eficienta O(nlgn) */
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;




int const minim=100, maxim=10000;
int nr_atB=0, nr_cmpB=0, nr_totB=0;
int nr_atT=0, nr_cmpT=0, nr_totT=0;
int nr_atQ=0, nr_cmpQ=0, nr_totQ=0;

ofstream f("test.csv");
int Left(int i) {
	return 2*i;
}
int Right(int i) {
	return 2*i + 1;
}
void swap(int arr[],int x, int y){
	int temp = arr[x];
	arr[x] = arr[y];
	arr[y] = temp;
}



void HeapifyTD(int heap_size,int arr[],int i){
	int l = Left(i); //left child
	int r = Right(i); //right child
	int largest;
	nr_cmpT+=2;
	if(l <= heap_size && arr[l] > arr[i])
		largest = l;
	else
		largest = i;
	nr_cmpT+=2;
	if(r <= heap_size && arr[r] > arr[largest])
		largest = r;
	nr_cmpT++;
	if(largest != i){//unul din cei 2 fii este mai mare decat arr[i]
		swap(arr,i,largest);//swap parinte cu child
		nr_atT+=3;
		HeapifyTD(heap_size,arr,largest);
	}
}
void HeapifyBU(int heap_size,int arr[],int i){
	int l = Left(i); //left child
	int r = Right(i); //right child
	int smallest;
	nr_cmpB+=2;
	if(l <= heap_size && arr[l] < arr[i])
		smallest = l;
	else
		smallest = i;
	nr_cmpB+=2;
	if(r <= heap_size && arr[r] < arr[smallest])
		smallest = r;
	nr_cmpB++;
	if(smallest != i){//unul din cei 2 fii este mai mic decat arr[i]
		swap(arr,i,smallest);//swap parinte cu child
		nr_atB+=3;
		HeapifyBU(heap_size,arr,smallest);
	}
}
void build_heapTD(int arr[],int heap_size){//Top-down
	for(int i=heap_size/2;i>=0;i--)
		HeapifyTD(heap_size,arr,i);
}
void build_heapBU(int arr[],int heap_size){//Bottom-up
	for(int i=heap_size/2;i>=0;i--)
		HeapifyBU(heap_size,arr,i);
}

void OutputHeap(int* ipHeap, int iSize) {
	using namespace std;

	 // Find the largest power of two, That is the depth
	int iDepth = 0;
	int iCopy = iSize;
	while (iCopy > 0) {
		iCopy >>= 1;
		++iDepth;
	}

	int iMaxWidth = (1 << iDepth);
	int iCharWidth = 4*iMaxWidth;
	int iEntry = 0;

	for (int i = 0; i < iDepth; ++i) {
		int iPowerOf2 = (1 << i);
		for (int j = 0; j < iPowerOf2; ++j) {
			int iSpacesBefore = ((iCharWidth/(1 << (i + 1))) - 1);
			 // Spaces before number
			for (int k = 0; k < iSpacesBefore; ++k) {
				cout << " ";
			}
			 // Output an extra space if the number is less than 10
			if (ipHeap[iEntry] < 10) {
				cout << " ";
			}
			 // Output the entry and the spaces after it
			cout << ipHeap[iEntry];
			++iEntry;
			if (iEntry >= iSize) {
				cout << endl;
				return;
			}
			for (int k = 0; k < iSpacesBefore; ++k) {
				cout << " ";
			}
		}
		cout << endl << endl;
	}
}

void OutputArray(int* ipArray, int iSize) {
	
	for (int i = 0 ; i < iSize; ++i) {
		cout << ipArray[i] << "  ";
	}
	cout << endl;
}
void HeapsortBU(int arr[],int heap_size){//Heapsort
	build_heapBU(arr,heap_size);//Bottom-up
	for(int i=heap_size;i>=1;i--){
		nr_atB+=3;// AVG Swaps
		swap(arr,0,i);
		HeapifyBU(i-1,arr,0);
	}
	nr_totB = nr_cmpB+nr_atB;
}
void HeapsortTD(int arr[],int heap_size){//Heapsort
	build_heapTD(arr,heap_size);//Top-down
	for(int i=heap_size-1;i>=1;i--){
		nr_atT+=3;// AVG Swaps
		swap(arr,0,i);
		HeapifyTD(i-1,arr,0);
	}
	nr_totT = nr_cmpT+nr_atT;
}

void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
	  nr_atQ++;
      /* partition */
	  nr_cmpQ++;
      while (i <= j) {
		  nr_cmpQ++;
            while (arr[i] < pivot)
                  i++;
			nr_cmpQ++;
            while (arr[j] > pivot)
                  j--;
			nr_cmpQ++;
            if (i <= j) {
				nr_atQ+=3;
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      };
  
      /* recursion */
	  nr_cmpQ++;
      if (left < j)
            quickSort(arr, left, j);
	  nr_cmpQ++;
      if (i < right)
            quickSort(arr, i, right);
}
int main()
{
    int a[7]={1,5,3,6,7,2,5};
    int b[7]={1,5,3,6,7,2,5};
    int n=7;
	cout << "---------------------------------------------" << endl;
	cout << "                initial                      " << endl;
	cout << "---------------------------------------------" << endl;
	cout<<"vectorul : ";
	OutputArray(a, n);
	cout << endl;
	OutputHeap(a, n);
	cout << "---------------------------------------------" << endl;
	cout << "                top-down                     " << endl;
	cout << "---------------------------------------------" << endl;
    HeapsortTD(a,n-1);
    cout<<"vectorul : ";
	OutputArray(a, n);
	cout << endl;
	OutputHeap(a, n);
	cout << "---------------------------------------------" << endl;
	cout << "                bottom-up                    " << endl;
	cout << "---------------------------------------------" << endl;
    HeapsortBU(a,n);
    cout<<"vectorul : ";
	OutputArray(a, n);
	cout << endl;
	OutputHeap(a, n);
	cout << "---------------------------------------------" << endl;
	cout << "                quicksort                    " << endl;
	cout << "---------------------------------------------" << endl;
	quickSort(a, 0, 7);
	cout<<"vectorul : ";
	for (int i =1 ; i <= 7; ++i) {
		cout << a[i] << "  ";
	}
	cout << endl;
	
	//Testare average
	cout<<"\n\nProcessing...";
	srand (time(NULL));


	f << "Heapsort\n" << "Elemente" << "," << "BU"  << ","<< "TD"<< "," <<"quickSort\n";
	int test[10000],test2[10000],test3[10000],test4[10000],test5[10000];
	for(int i=0;i<maxim;i++)
	{
		test[i]=rand();
	}
	int aux[10000];
	for(int i=0;i<maxim;i++)
	{
		test2[i]=rand();
	}
	for(int i=0;i<maxim;i++)
	{
		test3[i]=rand();
	}
	for(int i=0;i<maxim;i++)
	{
		test4[i]=rand();
	}
	for(int i=0;i<maxim;i++)
	{
		test5[i]=rand();
	}
	

	for(int dimensiune = minim;dimensiune<=maxim;dimensiune += 100){
		//1
		for(int i=0;i<maxim;i++)
			aux[i]=test[i];
		HeapsortBU(aux,dimensiune);
		
		
		for(int i=0;i<maxim;i++)
			aux[i]=test[i];
		HeapsortTD(aux,dimensiune);
		
		for(int i=0;i<maxim;i++)
			aux[i]=test[i];
		quickSort(aux,0,dimensiune-1);
		nr_totQ=nr_totQ+nr_atQ+nr_cmpQ;
		//2
		for(int i=0;i<maxim;i++)
			aux[i]=test2[i];
		HeapsortBU(aux,dimensiune);
		
		
		for(int i=0;i<maxim;i++)
			aux[i]=test2[i];
		HeapsortTD(aux,dimensiune);
		
		for(int i=0;i<maxim;i++)
			aux[i]=test2[i];
		quickSort(aux,0,dimensiune-1);
		nr_totQ=nr_totQ+nr_atQ+nr_cmpQ;
		//3
		for(int i=0;i<maxim;i++)
			aux[i]=test3[i];
		HeapsortBU(aux,dimensiune);
		
		
		for(int i=0;i<maxim;i++)
			aux[i]=test3[i];
		HeapsortTD(aux,dimensiune);
		
		for(int i=0;i<maxim;i++)
			aux[i]=test3[i];
		quickSort(aux,0,dimensiune-1);
		nr_totQ=nr_totQ+nr_atQ+nr_cmpQ;
		//4

		for(int i=0;i<maxim;i++)
			aux[i]=test4[i];
		HeapsortBU(aux,dimensiune);
		
		
		for(int i=0;i<maxim;i++)
			aux[i]=test4[i];
		HeapsortTD(aux,dimensiune);
		
		for(int i=0;i<maxim;i++)
			aux[i]=test4[i];
		quickSort(aux,0,dimensiune-1);
		nr_totQ=nr_totQ+nr_atQ+nr_cmpQ;
		//5
		for(int i=0;i<maxim;i++)
			aux[i]=test5[i];
		HeapsortBU(aux,dimensiune);
		
		
		for(int i=0;i<maxim;i++)
			aux[i]=test5[i];
		HeapsortTD(aux,dimensiune);
		
		for(int i=0;i<maxim;i++)
			aux[i]=test5[i];
		quickSort(aux,0,dimensiune-1);
		nr_totQ=nr_totQ+nr_atQ+nr_cmpQ;

		f << dimensiune << ","<<nr_totB/5<< ","<<nr_totT/5<<","<<nr_totQ/5<<"\n";
		nr_atT=0; 
		nr_cmpT=0; 
		nr_totT=0;
		nr_atB=0; 
		nr_cmpB=0; 
		nr_totB=0;
		nr_atQ=0; 
		nr_cmpQ=0; 
		nr_totQ=0;
		

	}
	f.close();
	cout<<endl;
	cout<<"done.";
	getch();
    return 0;

}



