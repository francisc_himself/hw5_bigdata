//***ANONIM*** ***ANONIM***-Wilhelm
//***GROUP_NUMBER***

//OBS : Eficienta permutarii Josephus este de O(n log n) dupa cum se vede si pe grafic.

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

typedef struct node{
	struct node *parent;    //parinte
	struct node *left;      //nod stang
	struct node *right;     //nod drept
	int value;
	int size;
}NODE;

int ordine[10000];  //rezultatul permutarii Josephson

int nr_operatii;    //numarul de atribuiri si comparatii

//functia de construire a unui arbore binar de cautare echilibrat
node *build_balanced_BST (int left, int right, int *dim){
	node *p;

   // int dim1 = 0;
    
	nr_operatii++;
	if (left>right){
        *dim = 0;
   	    return NULL;
    }
	else{
		p = (node*)malloc(sizeof(node));
	    p->value = (left+right)/2;   //luam mediana intervalului (left,right)        
		p->left = NULL;
		p->right = NULL;
		p->size = right-left+1;              

		nr_operatii += 5;

		if(left<right){
			p->left = build_balanced_BST(left,(left+right)/2-1,dim);

			if(p->left != NULL){
			    p->left->parent = p;
			    nr_operatii++;
            }

            p->right = build_balanced_BST((left+right)/2+1,right,dim);

            if(p->right != NULL){
                p->right->parent = p;
                nr_operatii++;
            }

        }
    }

    nr_operatii++;
    *dim = p->size;

    return p;
}

NODE *BST_min(NODE *p){
	while (p->left != NULL){
		nr_operatii++;
		p = p->left;
	}

	return p;
}

//functia ne returneaza succesorul lui p
NODE *BST_succesor(NODE *p){
	NODE *s;

	nr_operatii++;
	if (p->right != NULL){
		return BST_min (p->right);
	}

	nr_operatii++;
	s = p->parent;
	
	while ((s != NULL) && (p == s->right)){
		p = s;
		s = p->parent;
		nr_operatii+=2;
	}
	
	return s;
}

void BST_substract_size(node *p){ 
	while (p != NULL){
		  nr_operatii+=2;
		  p->size--;
		  p = p->parent;
	}
}

//functie de stergere a unui node p din arbore
void BST_delete(node **root, node *p){
	NODE *s,*x;

	nr_operatii++;
	if (p->left == NULL || p->right==NULL){
		nr_operatii++;
		s = p;
	}
	else{
		nr_operatii++;
		s = BST_succesor(p);
	}
	
	nr_operatii++;
	if (s->left != NULL){
		nr_operatii++;
		x = s->left;
	}
	else{
		nr_operatii++;
		x = s->right;
	}

	nr_operatii++;
	if (x != NULL){
		nr_operatii++;
		x->parent = s->parent;
	}

	BST_substract_size(s->parent);

	nr_operatii++;
	if (s->parent == NULL)  
		*root = x;
	else{
		nr_operatii++;
		if (s == (s->parent)->left){
			nr_operatii++;
			(s->parent)->left = x;
		}
		else{
			nr_operatii++;
			(s->parent)->right = x;
		}
	}

	nr_operatii++;
	if (s != p){
		nr_operatii++;
		p->value = s->value;
	}

	free(s);
}

//functia returneaza al i-lea nod cu cea mai mica valoare
NODE *OS_Select(NODE *p, int i){
	int r;
	
	nr_operatii++;
	if (p->left != NULL){
		r = ((p->left)->size)+1;
		nr_operatii++;
	}
	else{
		r = 1;
		nr_operatii++;
	}

	nr_operatii++;
	if (r == i){
		return p;
	}
	else{
		nr_operatii++;
		if (i < r){
			return OS_Select(p->left,i);
		}
		else{
			return OS_Select(p->right,i-r);
		}
	}
}

void pretty_print_initial(NODE *rad)
{
	if(rad != NULL){
		if(rad->right->right->value != NULL){
			printf("                        %d\n\n",rad->value);
			printf("        %d                              %d\n\n",rad->left->value,rad->right->value);
			printf("%d                %d          %d                  %d\n\n",rad->left->left->value,rad->left->right->value,
																			  rad->right->left->value,rad->right->right->value);
		}
	}
}

void josephus (int n, int m){
	node *rad,*p;
	int poz = m;
    int h;
    int ind = 0;
    
    rad = build_balanced_BST(1,n,&h);
    rad->parent = NULL;
	
	//pretty_print_initial(rad);

    while (n > 0){
        p = OS_Select(rad,poz);   //cautam urmatorul node care va fi scos
        ordine[ind] = p->value;   //adaugam valoarea nodului scos in sirul de ordine a scoaterii din arbore
        ind++;

        BST_delete(&rad,p);       //stergem nodul scos din arbore
        n--;                      //numarul elementelor se decrementeaza

		if (n != 0){
            poz = (poz + m - 1) % n;      //pozitia nodului care se va selecta se va schimba cat timp numarul de elemente este dif de 0
            
			if (poz == 0){
				poz = n;
			}
        }
	}
}

void evaluate(){
	int m;
	FILE *f;

	f = fopen ("josephus_permutation.txt","w");
	
	for (int n=100; n<=10000; n=n+100){
		printf("%d\n",n);

		m = n/2;
	
		nr_operatii = 0;

		josephus(n,m);
		
		fprintf(f,"%d %d\n",n, nr_operatii);
	}

	fclose(f);
}

void test_josephus(){
	josephus(7,3);

	printf("Ordinea de iesire :\n");

	for(int i=0;i<7;i++){
		printf("%d ",ordine[i]);
	}
}

void pretty_print_arbore(NODE *rad)
{
	if(rad != NULL){
		printf("                                         %d\n\n",rad->value);
		printf("                 %d                                           %d\n\n",rad->left->value,rad->right->value);
		printf("        %d                 %d                       %d                  %d\n\n",rad->left->left->value,rad->left->right->value,
																			  rad->right->left->value,rad->right->right->value);
		printf("                %d                 %d                         %d\n\n",rad->left->left->right->value,
																				rad->left->right->right->value,
																			  rad->right->left->right->value);
	}
}

void test_BST(){
	node *rad;
	int h;

	rad = build_balanced_BST (1, 11, &h);

	pretty_print_arbore(rad);
}

int main(){
	//test_josephus();
	//test_BST();
	evaluate();
	getche();
	return 0;
}