#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
#include "Profiler.h"
#define  N  9973
#define SEARCH_SIZE 3000
/* LABORATOR 6 -> ASSIGN 5
***ANONIM*** ***ANONIM*** 
Grupa ***GROUP_NUMBER***
`````````Tabele de dispersie````````````````
Cautarea unui element intr-o tabela de dispersiev este O(1) => operatii rapide
Coliziunea apare atunci cand doua intrari diferite au aceeasi cheie hash.
Aces fenomen se poate evita astfel: daca pozitia cheii  pe care se doreste sa se puna elementul, este ocupat, 
atunci se va insera pe urmatoarea pozitie libera de dupa acesta. Astfel coliziunile sunt rezolvate prin inlantuire.
*/
int nr_op=0;

//definim functia hash
int h(int x,int i)
{
	return (x+1*i+2*i*i) % N;
}

//inserare in hash table
int hash_insert(int T[N], int k)
{ 	
	int i=0;
	int j;
	do {
		j=h(k,i);
		if (T[j]==-1)
		{
			T[j]=k;
			return j;
		}
		else 
			i=i+1;
	}
	//while(i==N);
	while(i!=N);
	//printf("eroare: Tabelul este plin\n");
	return -1;
}


//cautare in hash table
int hash_search(int T[N], int k)
{
	int i=0;
	int j;
	do{
		//nr_op++;
		j=h(k,i);
		nr_op++;
		if(T[j]==k)
		{
			return j;
			//printf("elementul a fost gasit\n");
		}
		
		i=i+1;
	}
	//	while((T[j]==-1) || (i==N));
	while((T[j]!=-1) && (i!=N));


	//printf("eroare: Elementul nu a fost gasit in lista\n");
	return -1;
}
void initializare_hash(int T[])
{ 
	int i;
	for(i=0;i<N;i++)
		T[i]=-1;

}
void afisare(int T[])
{
	for(int i=0;i<N;i++)
	{
		printf("%d ", T[i]);
	}
	printf("\n");
}
void main()
{
	/*
	//Testare functie "insert' si "search"pt.HashTable pentru un caz concret
	printf("Start\n");
	int T[N];
	initializare_hash(T);
	int r,p;
	hash_insert(T, 3);
	hash_insert(T, 5);
	hash_insert(T, 7);
	hash_insert(T, 9);
	hash_insert(T, 1);
	hash_insert(T, 8);
	printf("Afisare hashtable");
	afisare(T);
	r=hash_search(T,4);
	printf("rez cautari nr 4 in T este: %d\n",r);
	printf("%d\n",	nr_op);
	p=hash_search(T,9);
	printf("rez cautari nr 9 in T este: %d\n",p);
	*/
	FILE*f;
	f=fopen("HashTable.csv","w");
	int samples[N+(SEARCH_SIZE/2)];
	int indices[SEARCH_SIZE/2];
	int i,j;
	int n=0;
	int max_nr_notf, max_nr_f,cautari;
	double med_f,med_notf;
	max_nr_notf=0;
	max_nr_f=0;
	double a[]={0.8,0.85,0.90,0.95,0.99};
	for(j=0;j<5;j++)
	{ 
		printf("este cazul: %.2f \n",a[j]);
		n=N*a[j];
		int T[N];
		FillRandomArray(samples, n+(SEARCH_SIZE/2), 1, 1000000,  true, 0);
		FillRandomArray(indices, SEARCH_SIZE/2,0,n-1, true, 0);
		initializare_hash(T);

		for( i=0;i<n;i++)
		{
			hash_insert(T,samples[i]);
		}

		//Caz NOT FOUND
		cautari=0;
		printf("Cazul NOT FOUND \n");
		for( i=n;i<n+(SEARCH_SIZE/2);i++)
		{
			nr_op=0;
			hash_search(T, samples[i]);

			if(max_nr_notf<nr_op)
				max_nr_notf=nr_op;
			//suma lor
			cautari=cautari+nr_op;
		}
		med_notf=(double)cautari/(SEARCH_SIZE/2);
		printf("nr_op= %d\n",max_nr_notf);
		printf("med_nr_op = %1.2lf \n",med_notf);
		//========================================================
		//Caz FOUND
		cautari=0;
		printf("Cazul FOUND \n");
		for(i=0;i<( SEARCH_SIZE/2);i++)
		{
			nr_op=0;
			hash_search(T, samples[indices[i]]);
			if(max_nr_f < nr_op)
				max_nr_f=nr_op;
			//suma lor
			cautari=cautari+nr_op;
		}
		//max_nr_f=nr_op;
		//cautari=cautari+max_nr_f;
		med_f=(double)cautari/( SEARCH_SIZE/2);
		printf("nr_max_op = %d \n",max_nr_f);
		printf("med_nr_op = %1.2lf \n",med_f);	
		fprintf(f,"%.2f, %d, %1.2lf, %d,  %1.2lf \n ",a[j],max_nr_notf,med_notf, max_nr_f,med_f);
		printf("//========================================================//\n");
	}

	getch();
}
