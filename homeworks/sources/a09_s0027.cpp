// BFS.cpp : Defines the entry point for the console application.
// Student: ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***
// Breast First Search - cautarea in latime
//Algoritmul calculeaza distanta(cel mai mic numar de muchii) de la radacina la toate varfurile accesibile

#include"stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define ALB 0 
#define GRI 1
#define NEGRU 2
#define INF 10000

	//Structura care defineste proprietatile unui varf a grafului
	typedef struct Proprietati_nod
		{
			int color;
			int  d; /*distanta de  la radacina la varf*/
			int parinte;
		} PROP;

	//Structura care reprezinta lista de adiacenta a unui nod
	typedef struct TIP_NOD
		{	
			int valoare;
			struct TIP_NOD* next;
		}TIP_NOD;


	TIP_NOD *prim[60001], *ultim[60001];
	PROP prop[60001];
	int timp;
	int k, nr_operatii;
	int Q[60001];  //coada

	void Cautare_latime (int s, int nr_varfuri)
		{
			//varf ALB - la inceput toate varfurile se initializeaza cu culoarea ALB - niciun varf nu a fost inca parcurs
			//varf GRI - varful a fost descoperit, dar mai are noduri adiacente albe(nu toate varfurile au fost descoperite)
			//varf NEGRU - varful a fost descoperit si toate nodurile adiacente au fost descoperite
			int u;
			for (u=1; u<=nr_varfuri; u++)
				{
					nr_operatii++;
			if (u!=s)
				{
					nr_operatii = nr_operatii+3;
					prop[u].color=ALB; //coloreaza fiecare varf in ALB
					prop[u].d=INF; //distanta de la sursa s la varful u
					prop[u].parinte=0;//seteaza parintele fiecarui varf pe NIL
				}
			}
			prop[s].color=GRI; //coloreaza varful sursa cu GRI deoarece el este considerat descoperit atunci cand procedura incepe
			prop[s].d=0;	
			prop[s].parinte=0; //initializeaza predecesorul sursei cu NIL
			nr_operatii++;
			k++;
			Q[k]=s; //determina varful GRI din capul cozii
			while(k!=0)
				{
					u=Q[1];
					for (int i=1; i<k; i++)
					Q[i]=Q[i+1];
					k--;
					TIP_NOD *temp;
					temp=prim[u];
					printf("%d ",u);
			while (temp!=NULL)
				{
				  int v=temp->valoare;
				  temp=temp->next;
				  nr_operatii++;
				  if (prop[v].color==ALB) //daca v este alb,atunci el inca nu a fost descoperit
					{
						nr_operatii++;
				    	prop[v].color=GRI;
						prop[v].d=prop[u].d+1;
						prop[v].parinte=u;
						k++;
						Q[k]=v; //varful v este plasat la sfarsitul cozii
					}
				}
			prop[u].color=NEGRU;
	}
}

	bool verifica (int x, int y) //metoda care verica daca sunt doua noduri identice in lista
		{
			TIP_NOD *p;
			p=prim[x];
			while(p!=NULL)
				{
					if(y==p->valoare)
					return false; 
					p=p->next;
				}
			return true;
		}

	void lista_adiacenta(int nr_varfuri,int nr_muchii)  //nr_varfuri = numarul de varfuri, nr_muchii = numarul de muchii
		{	/*lista de adiacenta pentru fiecare varf*/
			/*lista de adiacenta contine pointeri pentru toate varfurile pentru care exista muchie,totalitatea varfurilor adiacente lui*/
			int i,u,v;
			for(i=0;i<=nr_varfuri;i++)
			prim[i]=ultim[i]=NULL;
			i=1;
			while (i<=nr_muchii)
				{
					u=rand()%nr_varfuri+1;
					v=rand()%nr_varfuri+1;
					if (u!=v)
						{
						if(prim[u]==NULL) //daca lista nu mai are elemente
					       {
							prim[u]=(TIP_NOD *)malloc(sizeof(TIP_NOD *));
							prim[u]->valoare=v;
							prim[u]->next=NULL;
							ultim[u]=prim[u];
							i++;
							}
						else //daca lista are elemente atunci adauga dupa ultimul element
							{
								if (verifica (u, v))
									{
										TIP_NOD *p;
										p=(TIP_NOD *)malloc(sizeof(TIP_NOD *));
										p->valoare=v;
										p->next=NULL;
										ultim[u]->next=p;
										ultim[u]=p;
										i++;
									}
							}
						}
				}
		}

//print lista de adiacenta
	void print(int nrv)
		{
			int i;
			TIP_NOD *p;
			for(i=1;i<=nrv;i++)
				{
					printf("%d : ",i);
					p=prim[i];
			while(p!=NULL)
				{
					printf("%d,",p->valoare);
					p=p->next;
				}
		   printf("\n");
				}
		}

	int main()
	{
			int nre, nrv;
			int v,e;
			nrv=5;
			nre=10;
			lista_adiacenta(nrv, nre);
			print(nrv);
			printf("\n\n\n");
			Cautare_latime(1, nrv);
			/*FILE *f;
			f=fopen("Rezultate1.csv","w");
			fprintf(f,"nrv,nre,nr_operatii\n");
			nrv=100;
			for (nre=1000; nre<=5000; nre=nre+100)
				 {
				  srand(time(NULL));
				  lista_adiacenta(nrv, nre);
				  Cautare_latime (1, nrv);
				  fprintf(f,"%d,%d%d\n",nre,nrv,nr_operatii);
				 }
    fclose(f);*/

	/*FILE *f1;
    f1=fopen("Rezultate2.csv","w");
    fprintf(f1,"nrv,nre,nr_operatii\n");
	e=9000;
    for (v=100;v<=200;v=v+10)
    {
        lista_adiacenta(v, e);
        Cautare_latime (1, v);
        fprintf(f1,"%d,%d%d\n",e,v,nr_operatii);
    }
    fclose(f1);*/
	getch(); 
    return 0;
}