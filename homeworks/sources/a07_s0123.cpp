#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int a[] = {2,5,6,2,5,6,-1,5,2};
int n = 9;

struct mwNode{
	int info;
	int nbSons;
	mwNode **children;
} mwTree[9];

struct bNode{
	int info;
	bNode * child;
	bNode * brother;
};

//vector to multiway
int transform1(int a[]){
	int rootMW = -1;
	for( int i = 0; i < n; i++){
		mwTree[i].info = i;
		if( a[i] == -1 )
			rootMW = i;
		
		if ( i != rootMW){
			mwTree[a[i]].children = (mwNode**)realloc(mwTree[a[i]].children,sizeof(mwNode*) * 8); 
			mwTree[a[i]].children[ mwTree[a[i]].nbSons++] = &mwTree[i];	
		}
	}
	return rootMW;
}
void printTreeMW(mwNode *rootMW, int level){ 
	if(rootMW == NULL) return;
	for (int i = level-1; i > 0; i--)
		printf("  ");
	printf("  %d\n",rootMW->info);
	for (int i = 0; i < rootMW->nbSons; i++)
		printTreeMW(rootMW->children[i],level -1);
}

//multiway to binary
void transform2( mwNode *rootMW, bNode *rootB) { // constructing the binary mwTree 
	if( rootMW != NULL ) {
		if ( rootMW->nbSons > 0) {
			rootB->child = new bNode;
			rootB->child->info = rootMW->children[0]->info;
			transform2(rootMW->children[0],rootB->child);
			rootB = rootB->child;

			for ( int i = 1; i < rootMW->nbSons; i++) {
				rootB->brother = new bNode;
				rootB->brother->info = rootMW->children[i]->info;
				transform2(rootMW->children[i],rootB->brother); 
				rootB = rootB->brother;
			}
			rootB->brother = NULL;
		}
		else rootB->child = NULL;
	}
}

void printTreeB( bNode *rootMW, int level) {
	if(rootMW == NULL) return;
	for ( int i = 0; i < level; i++ )
		printf("  ");
	printf("  %d\n",rootMW->info);
	printTreeB(rootMW->child,level+1);
	printTreeB(rootMW->brother,level);
}

int main() {
	printf("Vector:\n");
	for(int i=0;i<9;i++)
		printf("%d ",a[i]);

	printf("\n\nTransform 1:\n");
	int rootMW = transform1(a);
	printTreeMW(&mwTree[rootMW],4);

	printf("\n\nTransform 2:\n");
	bNode* rootB = new bNode;
	rootB->info = mwTree[rootMW].info;
	rootB->brother = NULL;
	transform2(&mwTree[rootMW],rootB);
	printTreeB(rootB,0);

	getche();
	return 0;
}