#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;

struct list{
	int key;
	int index;
	list *next;
};

int a[10001], length, heapsize, n;
//int k[3] = {5, 10, 100};
list *v[501],*head[501],*arr[], *min[1];

int left(int );
int right(int);
int parent(int);
//void MINHEAPIFY(int[], int);
void copy(int[], int[]);
void BUILDMAXHEAPBU(int []);

void display();
void generate();

//left leaf
int left(int i){
	return 2*i;
}

//right leaf
int right(int i){
	return 2*i+1;
}

//parent of leaf
int parent(int i){
	return i/2;
}

//BOTTOM UP method
void MINHEAPIFY(list *l, int i){
	int r, smallest, help;
	l->key = left(i);
	r = right(i);
	profiler.countOperation("assCompBU",length);
	if(l->key <= heapsize && a[l->key] < a[i])
		smallest = l->key;
	else 
		smallest = i;
	profiler.countOperation("assCompBU",length);
	if(r <= heapsize && a[r] < a[smallest])
		smallest = r;
	if(smallest != i)
	{
		profiler.countOperation("assCompBU",length,3);
		help = a[i];
		a[i] = a[smallest];
		a[smallest] = help;
		MINHEAPIFY(l, smallest);
	}
}

void BUILDMAXHEAPBU(list *l)
{
	heapsize = length;
	for(int i = length/2; i >= 1; i--)
		MINHEAPIFY(l, i);
}


void copy(int a[], int v[])
{
	for(int i=1; i <= length; i++)
		v[i] = a[i];
}
/*
void generate(){

	for( int i=0; i < 3; i++)
		for( int j = 0; j < k[i]; j++)
		    //for( int l = 100; l <=10000; l+=100){
		{
			FillRandomArray(a,length,1,10000,false,1);
		}
}

void generateRandom(){
	int n = 10000;
	for (int m = 10; m<=500; m+=10)
	{
		FillRandomArray(a,length,1,10000,false,1);

	}
	*/

void generate(int k){
	int arrays[10000];
	list *help;
	for(int i=1; i<=k; i++)
	{
		FillRandomArray(arrays,n,1,10000,false,1);
		help = new list;
		help->key = arrays[0];
		help->index = i;
		v[i] = help;
		head[i] = help;
		for(int j=1; j<=n; j++)
		{
			help = new list;
			help->key = arrays[j];
			help->index = i;
			v[i]->next = help;
			v[i] = v[i]->next;
		}
		v[i]->next = NULL;
	}
}
void display(int k)
{
	for(int i=1; i<=k; i++)
	{
		printf("List %d: ",i);
		v[i] = head[i];
		while(v[i]->next != NULL)
		{
			printf("%d ",v[i]->key);
			v[i] = v[i]->next;
		}
		printf("\n");
	}
}
/*
list selectMin(int k1){
	list *min2;
	boolean firstStep = true;
	for(int i=1; i<=k1; i++){
		v[i]=head[i];
		if (v[i]->key == 0)
			firstStep = false;
	}
	if (firstStep){
		min->key = head[0];
		min->index = head[0]->index;
		min->next = NULL;
		for (int i=1; i<=k1; i++){
			v[i] = head[i];
			arr[i]->key = v[i]->key;
			arr[i]->index = v[i]->index;
			arr[i]->next = NULL;
			if(arr[i]->key < min->key)
			{
				min->key = arr[i]->key;
				min->index = arr[i]->index;
			}
		}
	}
	else{
		min2=arr[1];
		for (int i=2; i<=n; i++){
			if(min2->key > arr[i]->key)
				min2=arr[i];
		}
	}
	for(int i=0; i<=n; i++)
		if (min2==arr[i])
			arr[i]=0;
	return min;
}

*/
int main()
{
	int k[3] = {5, 10, 100};
	n=10; 
	
	generate(k[1]);
	display(k[1]);
	getch();
	return 0;
}