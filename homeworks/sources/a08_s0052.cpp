#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h" 
/****ANONIM*** ***ANONIM***-***ANONIM*** gr.***GROUP_NUMBER***
	
*/
Profiler profiler("demo"); 

#define V_max 10000
#define E_max 60000
typedef struct _NODE
{
	int val,rang;
	struct _NODE *p;
}NODE;

NODE nod[V_max];
int muchii[E_max];

typedef struct _GRAPH{
	int nrN,nrM;
}GRAPH;

int size;

void makeSet(NODE *x,int val){
	profiler.countOperation("intrari",size,1);
	x->p=x;
	x->rang=0;
	x->val=val;
}

NODE* findSet(NODE *x){
	profiler.countOperation("intrari",size,1);
	if (x->p!=x){
		x->p=findSet(x->p);	
	}
	return x->p;
}

void link(NODE *x,NODE *y){
	profiler.countOperation("intrari",size,1);
	if(x->rang>y->rang){
		y->p=x;
	}else {
			x->p=y;
			if(x->rang==y->rang)
				y->rang=y->rang+1;
		  }
}

void unions(NODE *x,NODE *y){
	profiler.countOperation("intrari",size,1);
	link(findSet(x),findSet(y));
}
void genereaza(GRAPH *G){
	
	FillRandomArray(muchii,G->nrM,0,G->nrN*G->nrN-1,true);
}
void compConex(GRAPH *G){
	int nrN=G->nrN;
	int nrM=G->nrM;
	//printf("%d %d",nrM,nrN);
	for(int i=0;i<nrN;i++){
		makeSet(&nod[i],i);
	}
	genereaza(G);
	for(int i=0;i<nrM;i++){
		int a=muchii[i]/nrN;
		int b=muchii[i]%nrN;
		if(nrM==9)
			printf("%d %d %d\n",i,a,b);
		if(findSet(&nod[a])!= findSet(&nod[b])){
			unions(&nod[a],&nod[b]);
		}
	}
}

void main(){
	int n=V_max;
	GRAPH *g=(GRAPH*)malloc(sizeof(GRAPH));
	g->nrN=5;
	g->nrM=9;
	compConex(g);
	/*for(int muchii=V_max;muchii<=E_max;muchii+=5000){
		g->nrN=n;
		g->nrM=muchii;
		size=muchii;
		compConex(g);
	}
	profiler.showReport();
*/
}