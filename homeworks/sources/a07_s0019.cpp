#include<conio.h>
#include<stdio.h>
#include<stdlib.h>

/**Programul trebuie sa transforme dintrun vector de parinti mai intai intrun arbore multi-cai 
(un parinte cu toti copii lui). Acest lucru se realizeaza in functia T1() unde se intializeaza 
si se construieste structura multi-tree(un sir de noduri care au fiecare pointer la copii) incepand
de la primul element din sirul de parinti. Acest lucru face n calcule.
Functia T2() realizeaza transformarea din multi_tree in binary_tree, adica reprezinta legatura
parinte-copil ca o legatura de copil-stang si legatura dintre copii(frati) ca o legatura copil-drept.
La fel ca in cazul primei trasformari, parcurgem fiecare lista de copii corespunzatoare unui nod
din multi_tree si ne rezulta O(n) calcule.
Se mai realizeaza doua functii pentru afisarea rezultatelor dupa fiecare transformare.*/

typedef struct multi_tree
{
	int key;
	int nr_child;
	int parent;
	struct multi_tree *child[10];	
} MULTI_TREE;					

typedef struct binary_tree
{
	int key;
	struct binary_tree *stg_child, *dr_brother;		

}BINARY_TREE;

MULTI_TREE *root1;
MULTI_TREE *a[100];
BINARY_TREE *b[100];

int P[100], n; 

void T1()
{
   int i,aux;
   for(i=0;i<n;i++)
	{
	   a[i]=(MULTI_TREE*)malloc(sizeof(MULTI_TREE)); 
	   a[i]->key=i;
	   a[i]->nr_child=0;
	   a[i]->child[1]=NULL;
   }
   for(i=0;i<n;i++)
   {
	   if(P[i]==-1)
	      root1=a[i];
	   else
	   {
			   a[P[i]]->nr_child++;
			   aux=a[P[i]]->nr_child;
			   a[P[i]]->child[aux]=a[i];
	   }
   }
}


 void T2(MULTI_TREE *p)
{
	int i;
	
	if(p!=NULL)
	{
		for(i=1;i<=p->nr_child;i++)
		{
			if(i==1)
			{
				int j=p->key;
				b[j]->stg_child=b[p->child[i]->key];
			}
			else
			{
				b[p->child[i-1]->key]->dr_brother=b[p->child[i]->key];
			}
			T2(p->child[i]);
		}
	}
}

void printfMultiTree(MULTI_TREE *p)
{
	int i;
	if(p!=NULL)
	 {
		  if(p->nr_child==0)
			    printf("\n%d nu are copii\n",p->key);
		  else
		  {
				printf("\ncopii lui %d sunt: ",p->key);
				for(i=1;i<=p->nr_child;i++)
				{
					 //printf(" %d ",p->child[i]->key);
					 printfMultiTree(p->child[i]);
				}
		  }
	 }
}


void printBinaryTree(int ind,int h)
{
   int i;
   printf("\n");
   for(i=0;i<=3*h;i++)
	   printf(" ");
   printf("%d",ind);

   if(b[ind]->stg_child!=NULL)
	     printBinaryTree(b[ind]->stg_child->key,h+1);
   if(b[ind]->dr_brother!=NULL)
	     printBinaryTree(b[ind]->dr_brother->key,h);
}

void main()
{
	int i;

	n=9;
	P[0]=1;
	P[1]=6;
	P[2]=4;
	P[3]=1;
	P[4]=6;
	P[5]=6;
	P[6]=-1;
	P[7]=4;
	P[8]=1;

	T1();
	printf("\nT1: \n");
	printfMultiTree(root1);
	printf("\n");

	for(i=0;i<n;i++)
	{
		b[i]=(BINARY_TREE*)malloc(sizeof(BINARY_TREE));  
		b[i]->key=i;
		b[i]->stg_child=b[i]->dr_brother=NULL;
	}
	T2(root1);
	
	printf("\nT2: \n");
	printf("%d", root1->key);
	printBinaryTree(b[1]->key,0);
	getch();

}