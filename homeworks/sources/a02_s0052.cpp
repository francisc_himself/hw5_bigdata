/*
***ANONIM*** ***ANONIM***-***ANONIM***  gr.***GROUP_NUMBER***
Observand graficele rezultate in urma implementarii algoritmilor se poate deduce ca 
algoritmul de constructie heap BOTTOM-UP este mult mai eficient decat algortimul TOP-DOWN.
Numarul de de asignari este aproximativ egal cu cu numarul de comparatii la bottom-up,
numarul de asignari la top-down este mai mic decat numarul de comparatii, astfel suma dintre 
asignari si comparatii pentru fiecare algoritm este mai muca la bottom-up decat la top-down.

*/

#include <stdio.h>
#include <stdlib.h>

#include "Profiler.h"
Profiler profiler("demo");

#define MAX 10000
int lung,size;

////--------------------------bottom-up--------------------------////
void afisare(int *v,int n,int k,int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
	for(int i=1;i<=nivel;i++)
	{
		printf("   ");
	}
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);
}

int parinte(int i)
{return i/2;}
int left(int i)
{return 2*i;}
int right(int i)
{return 2*i+1;}

int* reconstructie(int *v,int i, int dim)
{
	int st=left(i);
	int dr=right(i);
	int max;
	if (st<=dim && v[st]>v[i]){max=st;}
	else max=i;
	if(dr<=dim && v[dr]>v[max]){max=dr;}
	profiler.countOperation("bucomp",lung,2);
	if (max!=i){
		int aux=v[i];
		v[i]=v[max];
		v[max]=aux;
		profiler.countOperation("buassign",lung,3);
		reconstructie(v,max,dim);
	}
	return v;
}

void constructie(int *v, int n)
{
	int m=n;
	for(int i=m/2;i>0;i--)
		reconstructie(v,i,n);
}


	
////---------------top-dowm---------------------////

void increaseKey(int *v, int i,int key,int n)
{
	if(key<v[i]){printf("cheie mica");}
	v[i]=key;
	profiler.countOperation("tdassign",lung,1);
	profiler.countOperation("tdcomp",lung,1);
	while((i>1) && (v[parinte(i)]<v[i]))
	{
		int aux=v[i];
		v[i]=v[parinte(i)];
		v[parinte(i)]=aux;
		profiler.countOperation("tdassign",lung,3);
		i=parinte(i);
		profiler.countOperation("tdcomp",lung,1);
	}

}

void heapInsert(int *v, int key,int n )
{
	size++;
	v[size]=-MAX;
	profiler.countOperation("tdassign",lung,1);
	increaseKey(v,size,key,n);

}

void construieste2(int *v,int n)
{
	size=1;
	for(int i=2;i<=n;i++)
		heapInsert(v,v[i],n);
}
	
void main()
{
	int v[MAX],v1[MAX];

	srand(time(NULL));
	for(int t=0;t<5;t++){
		for(int n=100;n<2000;n+=100){
			printf("n=%d\n",n);
			FillRandomArray(v,n);
			 lung=n;
			memcpy(v1,v,n*sizeof(int));
			constructie(v1,lung+1);
			memcpy(v1,v,n*sizeof(int));
			construieste2(v1,lung+1);
		}
	}

	profiler.createGroup("bottom-up","buassign","bucomp");
	profiler.addSeries("buSeria","buassign","bucomp");
	profiler.createGroup("top-down","tdassign","tdcomp");
	profiler.addSeries("tdSeria","tdassign","tdcomp");
	profiler.createGroup("TOATE","buassign","bucomp","tdassign","tdcomp");
	profiler.createGroup("SERIA","buSeria","tdSeria");
	
	profiler.showReport();


	//int v[]={0,4,1,3,2,16,9,10,14,8,7};
	//int v1[]={0,4,1,3,2,16,9,10,14,8,7};
	////afisare(v,10,1,0);
	////maxHeap(v,10);
	////heapsort(v,10);
	////for(int i=0;i<11;i++)
	////construieste2(v1,10);
	///*tdheapsort(v1,10);
	//afisare(v1,10,1,0);*/
	//afisare(v,10,1,0);
	////constructie(v,10);
	//construieste2(v,10);
	//afisare(v,10,1,0);
}
