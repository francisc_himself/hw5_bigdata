
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <list>

using namespace std;

struct heapNode
{
    int value;
    int source;
};

int heapsize=0;
unsigned long att=0;
unsigned long comp=0;
int arrSize = 10;
heapNode a[10000];
int x[10000];
int finalMergedListSize;

int listNumbers;
list<int> l[1000];

const int MIN = 10;
const int MAX = 100;

//ofstream g("listMerge.csv");
//ofstream h("listMergeKVar.csv");

int left(int k) // bun
{
	return 2*k+1;
}
int right(int k) // bun
{
	return 2*k+2;
}
int parent(int k) // bun
{
	return (k-1)/2;
}

void swap(heapNode &a, heapNode &b)
{
    int bValue, bSource;
    int aSource, aValue;

    bValue = (int)b.value;
    bSource = (int)b.source;
    aValue = (int)a.value;
    aSource = (int)a.source;

    b.value = aValue;
    b.source = aSource;
    a.value = bValue;
    a.source = bSource;
}

void generateLists(int total)
{
    int value, chosenList;
    value = rand()%(MAX-MIN) + MIN;
    for (int i=0;i<listNumbers;i++) l[i].clear();
    for (int i=0;i<total;i++)
    {
        value = value + rand()%(MAX-MIN) + MIN;
        chosenList = rand()%(listNumbers);
        l[chosenList].push_back(value);
    }
}

void printLists()
{
    unsigned int size;
    for (int i=0;i<listNumbers;i++)
    {
        size = l[i].size();
        for (unsigned int j=0;j<size;j++)
        {
            printf("%d ",l[i].front());
            l[i].push_back(l[i].front());
            l[i].pop_front();
        }
        printf("\n");
    }
}

void heapify(heapNode *a, int i)
{
    int largest=i;
    int l=left(i);
    int r=right(i);
    comp++;
    if((l<heapsize)&&(a[l].value<a[i].value)) largest=l;
    else largest=i;
    comp++;
    if((r<heapsize)&&(a[r].value<a[largest].value)) largest=r;
    if(largest!=i)
    {
        att=att+3;
        swap(a[largest],a[i]);
        heapify(a,largest);
    }
}

void build_heap_top_down(heapNode *a, int n)
{
    int i;
    int k;
    for (i=1;i<n;i++)
    {
        heapsize = i;
        k=i;
        while (k>0 && a[k].value < a[parent(k)].value)
        {
            comp++;
            att=att+3;
            swap(a[k],a[parent(k)]);
            k = parent(k);
        }
    }
}

void heap_sort_top_down(heapNode *a, int n)
{
    heapsize=n;
    build_heap_top_down(a,n);
    for(int i=n-1;i>=1;i--)
    {
        att=att+3;
        swap(a[0],a[i]);
        heapify(a,0);
        heapsize--;
    }
}

void printMergedList()
{
    for (int i=0;i<finalMergedListSize;i++)
    {
        printf("%d ",x[i]);
    }
}

void printHeapContent(heapNode *a, int n)
{
    for (int i=0;i<n;i++)
    {
        printf("[Node %d][Source: %d][Value: %d]\n",i,a[i].source,a[i].value);
    }
    printf("\n");
}

void mergeLists()
{
    int k=0;
    int activeLists;
    activeLists = listNumbers;
    for (int i=0;i<listNumbers;i++)
    {
        a[i].value=l[i].front();
        a[i].source = i;
        l[i].pop_front();
    }
    build_heap_top_down(a,listNumbers);
}

int main()
{
    listNumbers = 3;

    FILE *f;
    f=fopen("listMerge.csv","w");

    FILE *g;
    g=fopen("listMergeKVar.csv","w");
    //ofstream g("listMerge.csv");
//ofstream h("listMergeKVar.csv");

    for (int i=100;i<=10000;i=i+100)
    {
        att=0;
        comp=0;
        generateLists(i);
        mergeLists();

        fprintf(f,"%d %d %d %d\n",i,att,comp,att+comp);

      //  g<<i<<","<<att<<","<<comp<<","<<att+comp<<endl;
    }
    int i = 10000;
    for (listNumbers = 3;listNumbers <=100;listNumbers++)
    {
        att=0;
        comp=0;
        generateLists(i);
        mergeLists();

        fprintf(g,"%d %d %d %d\n",listNumbers,att,comp,att+comp);
        //h<<listNumbers<<","<<att<<","<<comp<<","<<att+comp<<endl;
    }
    return 0;
}
