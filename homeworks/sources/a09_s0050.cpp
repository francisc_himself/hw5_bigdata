#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define white 0
#define gray 1
#define black 2
#define V_MAX 10000
#define E_Max 60000
#define inf 32767

int count;
int tail;
int head;
int lungimeQ;

typedef struct{
int key;
int parent;
int distance;
int color;

}GRAPH_NODE;
typedef struct nod{
int key;
struct nod *next;
}NODE;
GRAPH_NODE Q[E_Max];
NODE  *adj[V_MAX];
GRAPH_NODE graph[V_MAX];
int edges[E_Max];

void generate(int n,int m)
{
memset(adj,0,n*sizeof(NODE*));
FillRandomArray(edges,m,0,n*n-1,true);
for(int i=0;i<m;i++)
{
int a=edges[i]/n;
int b=edges[i]%n;
NODE *nod=(NODE*)malloc(sizeof(NODE));
nod->key=b;
nod->next=adj[a];
adj[a]=nod;
}
}
void initQ()
{
tail=0;
head=0;
lungimeQ=0;
}
void ENQUEUE( GRAPH_NODE x)
{
	//Q[lungimeQ]=x;
	
	count++;
	Q[tail]=x;
	//if(tail==lungimeQ) tail=1;
	//else
		tail++;
	lungimeQ++;
}

GRAPH_NODE DEQUEUE()
{
	count++;
	lungimeQ--;
	//GRAPH_NODE x=Q[0];
	
	GRAPH_NODE x=Q[head];
	//if(head==lungimeQ)head=1;
	//else
		head++;
	return x;
}
void initWhite(int n,int j)
{
for(int i=0;i<n;i++)
{ if(i!=j){
	graph[i].color=white;
	graph[i].distance=inf;
	graph[i].parent=0;
}
}
}
void BFS(int j)
{
	//GRAPH_NODE s=graph[j];
count++;
graph[j].color=gray;
graph[j].distance=0;
graph[j].parent=0;
initQ();
ENQUEUE(graph[j]);
while(lungimeQ!=0)
{count++;
	GRAPH_NODE u=DEQUEUE();
	NODE *v=adj[u.key];
	
	while(v!=NULL)
	{	count++;
		if (graph[v->key].color==white){
			graph[v->key].color=gray;
			graph[v->key].distance=u.distance+1;
			graph[v->key].parent=u.key;
			ENQUEUE(graph[v->key]);
			count++;
		}
		if(v!=NULL)v=v->next;

	}
	u.color=black;
	graph[u.key]=u;
	count++;
}

}
void initG()
{
for(int i=0;i<V_MAX;i++)
	graph[i].key=i;
}

void afisareAdj(int n)
{ 

for(int i=0;i<n;i++)
{	printf("\n%d",i);
NODE *v=adj[i];
while(v!=NULL){
	printf("->%d ",v->key);
	v=v->next;
}
}
}

void afisareGraf(int n)
{
	printf("\nAfisare graf BFS\n");
for (int i=0;i<n;i++)
{
	printf("%d -> %d culoare: %d d=%d\n",graph[i].parent,graph[i].key,graph[i].color,graph[i].distance);
}

}
int main()
{	

	FillRandomArray(edges,10000,0,10000,true,0);
	int n=5;
	int m=7;
	generate(n,m);
	afisareAdj(n);
	initG();
	initWhite(n,0);
for(int i=0;i<n;i++)
	if(graph[i].color==white)
	BFS(i);

afisareGraf(n);


FILE *f =fopen("rezM.csv","w");
fprintf(f,"numarMuchii \t count\n");
	for (int numarMuchii=1000;numarMuchii+1 <6000;numarMuchii+=100){
		FillRandomArray(edges,numarMuchii,0,100*100-1,true,0);
		generate(100,numarMuchii);
		initG();
		initWhite(100,0);
		count=0;
		for(int i=0;i<100;i++)
			if(graph[i].color==white) BFS(i);	
	 	fprintf(f,"%d,%d\n",numarMuchii,count);
	}

FILE *l =fopen("rezN.csv","w");
fprintf(l,"numarN \t count\n");
	for (int numarN=100;numarN <200;numarN+=10){
		FillRandomArray(edges,9000,0,numarN*numarN-1,true,0);
		generate(numarN,9000);
		initG();
		initWhite(numarN,0);
		count=0;
		for(int i=0;i<numarN;i++)
			if(graph[i].color==white) BFS(i);	
	 	fprintf(l,"%d,%d\n",numarN,count);
	}
return 10;
}


