#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
/* Tema 5 Hash Tables 
Nume : ***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa : ***GROUP_NUMBER*** CTI
In aceasta tema am avut de studiat efortul mediu si maxim pentru un element gasit si unul negasit 
*/
#include <time.h>
#include <math.h>
#define empty_hash -1
int N = 9973;	 //numarul de elemente
int n;
int c1 = 1, c2=2;
int m = 17;
int sn = 3000;
float na;	//nr de accese

int h (int T[9973], int k, int i)
{
	return ((k % m) + c1*i + c2*i*i)%N; // returneaza index la care sa fie pus elementul , c1 , c2 le asignam noi : sursa -> wikipedia 
}

int hash_insereaza (int T[9973],int k)
{
	int i = 0, j;
	do 
	{
		j = h (T,k,i);
		if (T[j] == empty_hash)
			return j;
		else
			i ++;
	}
	while (i != N-1);
	printf ("Depasire capacitate hash table!\n");
	return empty_hash;
}

int hash_cauta (int T[9973],int k)
{
	int i = 0, j;
	do 
	{
		j = h (T,k,i);
		na ++;
		if (T[j] == k)
			return j;
		i ++;
	}
	while (T[j] != -1 && i!=N-1);
	printf ("Nu am gasit elementul %d!\n",k);
	return empty_hash;
}

void rezolva()
{
	int T[9973], v[10000],nv,index;
	int i,j,value;		//j indexul in tabela de dispersie
	srand(time(NULL));
	float fact_umpl;		//factor de umplere
	float efmaxgasite,efmediugasite,efmaxnegasite,efmediunegasite;;

	FILE *pf;
	pf = fopen ("efort_cautare.txt","w");

	fact_umpl = 0.8;
	while (fact_umpl != 1)
	{
		nv = 0;

		//trebuie sa inseram n = fact_umpl * 9977 elemente
		n = fact_umpl * N;
	
		//initializam tabela de dispersie cu null = -1
		for (i=0; i<N; i++)
			T[i] = empty_hash;

		//inseram elemente
		for (i=0; i<n; i++)
		{
			value = rand()%10000 + 1;	//intervalul 1 10000
			v[++nv] = value;
			j = hash_insereaza (T,value);
			if (j != -1)
				T[j] = value;
		}
		


		//aici caut in prima jumatate
		efmaxgasite = 0;
		efmediugasite = 0;
		for (i=1; i<=sn/2; i++)
		{
			index = rand()%n +1;
			na = 0; 
			hash_cauta (T,v[index]);
			efmediugasite += na;
			if (na > efmaxgasite)
				efmaxgasite = na;
		}
		efmediugasite = efmediugasite / n;

		//cautam in a doua jumatate
		efmaxnegasite = 0;
		efmediunegasite = 0;
		int cate=0,rez;
		for (j=1; j<=sn-i; j++)
		{
			value = rand()%10000 +10001;
			na = 0; 
			rez=hash_cauta (T,value);
			efmediunegasite += na;
			if (na > efmaxnegasite)
				efmaxnegasite = na;
		}
		efmediunegasite = efmediunegasite / n;
		fprintf (pf,"%0.3f %0.3f %0.3f %0.3f %0.3f\n",fact_umpl,efmediugasite,efmaxgasite,efmediunegasite,efmaxnegasite);

		if (fact_umpl == (float)0.8)
		{
			fact_umpl = 0.85;
		}
		else
		{
			if (fact_umpl == (float)0.85)
				fact_umpl = 0.9;
			else
			{
				if (fact_umpl == (float)0.9)
					fact_umpl = 0.95;
				else
				{
					if (fact_umpl == (float)0.95)
						fact_umpl = 0.99;
					else
						fact_umpl = 1;
				}
			}
		}
	}
	fclose(pf);
}

void testeaza ()
{
	int T[9973],n;
	int i,value,j;
	float fact_umpl;

	fact_umpl = 0.8;
	N = 20;
	c1 = 1;
	c2 = 2;
	m = 3;

	//inseram N* fact_umpl
	n = fact_umpl * N;
	
	//initializam tabela de dispersie cu null = -1
		for (i=0; i<N; i++)
			T[i] = -1;

		//inseram elemente 
		for (i=0; i<n; i++)
		{
			value = rand()%100 + 1;	//valori de la 1 lal 100
			j = hash_insereaza (T,value);
			if (j != -1)
				T[j] = value;
		}
		
		printf ("fact_umpl=%f\n",fact_umpl);
		for (i=0; i<N; i++)
			printf ("elementul[%d]=%d ",i,T[i]);
		printf ("\n");

		printf ("%d\n",value);
		printf ("%d\n",hash_cauta(T,value)); 

		value = 24;

		printf ("fact_umplere=%f\n",fact_umpl);
		for (i=0; i<N; i++)
			printf ("elementul[%d]=%d",i,T[i]);
		printf ("\n");

		printf ("%d\n",value);
		printf ("%d\n",hash_cauta(T,value)); 

		value = 299;

		printf ("fact_umplere=%f\n",fact_umpl);
		for (i=0; i<N; i++)
			printf ("T[%d]=%d ",i,T[i]);
		printf ("\n");

		printf ("%d\n",value);
		printf ("%d\n",hash_cauta(T,value)); 
}

int main ()
{
	rezolva();
	testeaza ();
	getche ();
	return 0;
}