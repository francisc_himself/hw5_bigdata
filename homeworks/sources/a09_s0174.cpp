#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <stdlib.h>

/**
*   @autor ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
*   Din graficele obtinute, am observat o crestere liniara a numarului de operatii
*   in raport cu modificarea numarului de arce sau de noduri. O(n)
*
*/


#define max 9001
#define maxv 201
#define white 0
#define gray 1
#define black 2

double opse[40], opsv[10];
int vertexes[200][200];

struct node{
	int vertex;
	struct node *next;
};
typedef struct node* nodeptr;
typedef struct queue
{
	int front,rear;
	int arr[max];
} queue;

void scrie_rez(FILE *f, double *a, int length){
    int i;
	fprintf(f,"\n");
	for(i = 1; i <= length; i++)
		fprintf(f, "%.0f,", a[i]);
}

nodeptr getnode(){
	nodeptr p;
	p=(nodeptr)malloc(sizeof(struct node));
	p->next=NULL;
	return p;
}

int qempty(struct queue *q){
	if(q->front > q->rear)
		return 1;
	else
		return 0;
}

void insertq(struct queue *q,int x){
	q->rear++;
	q->arr[q->rear]=x;
}

int removeq(struct queue *q){
	int x;
	x=q->arr[q->front];
	q->front++;

	return x;
}

void init(nodeptr head[],int n){
	int v;
	for(v=1;v<=n;v++)
		head[v]=NULL;
}

void initialise_visit(int visited[],int n){
	int i;
	for(i=1;i<=n;i++)
		visited[i]=white;
}

void createEdge(nodeptr head[], int v1, int v2){
	nodeptr new1,p;

	new1=getnode();
	new1->vertex=v2;
	p=head[v1];

	if(p==NULL)
		head[v1]=new1;
	else{
		while(p->next!=NULL)
			p=p->next;
		p->next=new1;
	}

	new1=getnode();
	new1->vertex=v1;
	p=head[v2];
	if(p==NULL)
		head[v2]=new1;
	else{
		while(p->next!=NULL)
			p=p->next;
		p->next=new1;
	}

}

void display(nodeptr head[],int n){
	int v;
	nodeptr adj;
	printf("\n Adjancency List Is:\n");
	for(v=1;v<=n;v++){
		printf("\n Head[%d]-> ",v);
		adj=head[v];
		while(adj!=NULL){
			printf("%d  ",adj->vertex);
			adj=adj->next;
		}
		printf("\n");
	}
}

void BFS(nodeptr head[],int start,int visited[], int print, int numberOfVertexes, int i){
	nodeptr adj;
	struct queue q;
	int v, operations = 0;
	q.front = 0;
	q.rear = -1;
	visited[start] = 1;
	if(print)
		printf("\nArbore BFS, incepind cu: %d\n\n%d", start, start);
	insertq(&q, start);
	operations++;
	while(!qempty(&q)){
		v = removeq(&q);
		operations++;
		adj = head[v];
		while(adj != NULL){
			if(visited[adj->vertex] == white)
			{
				visited[adj->vertex] = gray;
                operations++;
				if(print)
					printf("\t %d", adj->vertex);
				insertq(&q, adj->vertex);
			}
			adj = adj->next;
            operations++;
		}
	}
	if(i == -1) return;
	if(numberOfVertexes == 9000){
        opsv[i] = operations;
	}
	else{
        opse[i] = operations;
	}
}

void initops(){
    int i;
	for(i = 0; i < 11 ;i++){
		opsv[i] = 0;
	}

	for(i = 0; i < 41 ;i++){
		opse[i] = 0;
	}
}

int generateRandom(int i){
    return rand() % i;
}

void initialiseVertexArray(){
    int i, j;
    for(i = 0; i < maxv; i++)
        for(j = 0; j < maxv; j++)
            vertexes[i][j] = 0;
}

void generateEdges(nodeptr head[], int n, int number){
    int i, aux1, aux2, waiter = 0;
    for(i = 0; i < n; i++){
        aux1 = generateRandom(number);
        waiter++;
        aux2 = generateRandom(number);
        if(vertexes[aux1][aux2] == 0){
            vertexes[aux1][aux2] = 1;
            createEdge(head, aux1, aux2);
        }
        else i--;
    }

}

int main()
{
	FILE *f = fopen("BFS.csv","w");
	int i, n = 5;
	int visited[maxv];
	nodeptr head[maxv];

	init(head, n);
	createEdge(head, 1, 2);
	createEdge(head, 2, 3);
	createEdge(head, 4, 5);
	createEdge(head, 5, 1);
	display(head, n);
	initialise_visit(visited, n);
	BFS(head, 1, visited, 1, n, -1);

	n = 100;
	init(head, n);
	initops();
    for(i = 1000; i <= 5000; i += 100){
        init(head, n);
        initialiseVertexArray();
        generateEdges(head, i, n);
        initialise_visit(visited, n);
        BFS(head, 1, visited, 0, i, (i / 100) - 10);
    }
    n = 9000;
    for(i = 100; i <= 200; i += 10){
        init(head, i);
        initialiseVertexArray();
        generateEdges(head, n, i);
        initialise_visit(visited, i);
        BFS(head, 1, visited, 0, n, (i / 10) - 10);
    }

	scrie_rez(f, opse, 40);
	fprintf(f, "\n");
	scrie_rez(f, opsv, 10);
	fclose(f);
	return 0;
}
