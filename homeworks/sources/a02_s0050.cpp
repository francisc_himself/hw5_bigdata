#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* ***ANONIM*** ***ANONIM***  grupa ***GROUP_NUMBER***
	Conform graficului putem trage concluzia ca algortmul de constructie HEAP(bottom-up) este mai eficient decat Heap(tod-down)
deoarece numarul de asignari al metodei top-down  este mai mare decat cel de la bottom-up, iar numarul de comparatii 
de la top-down este aproximativ egal cu bottom-up.
*/
Profiler profiler("demo");
#define Iinfinit 10000

int lungime;
int size=0;
void afisare(int* v, int n,int k, int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
	for(int i=0;i<nivel;i++) printf("  ");
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);
}
int pr(int x){return x/2;}
int dr(int x){return 2*x+1;}
int st(int x){return 2*x;}

void reconstructieHeap(int* A,int i, int n)
{
	int max;
	int s=st(i);
	int d=dr(i);
	if((s<=n)&&A[s]>A[i]) max=s;
	else max=i;
	if((d<=n)&&(A[d]>A[max])) max=d;
	profiler.countOperation("buComp",lungime,2);
	if (max != i) {int aux=A[i];
	A[i]=A[max];
	A[max]=aux;
	reconstructieHeap(A,max,n);
	profiler.countOperation("buAssign",lungime,3);
	}

}
void constructieHeap(int* A,int n)
{
	for(int i=n/2;i>=1;i--)
		reconstructieHeap(A,i,n);

}

//void heapsortBU(int* A,int n)
//{
//	constructieHeap(A,n);
//	for (int i=n;i>=2;i--)
//	{
//		int aux=A[1];
//		A[1]=A[i];
//		A[i]=aux;
//		n--;
//		profiler.countOperation("buAssign",lungime,3);
//
//		reconstructieHeap(A,1,n);
//	}
//}

void increaseTD(int* A,int i,int key,int n)
{
	if(key<A[i]) perror("new key is smaller than current key");
	A[i]=key;
	profiler.countOperation("tdComp",lungime,1);
	profiler.countOperation("tdAssign",lungime,1);
	while((i>1)&&(A[pr(i)]<A[i]))
	{
		int aux=A[i];
		A[i]=A[pr(i)];
		A[pr(i)]=aux;
		i=pr(i);
		profiler.countOperation("tdComp",lungime,1);
		profiler.countOperation("tdAssign",lungime,3);
	}

}

void insertTD(int* A,int key,int n)
{
	size+=1;
	A[size]=-Iinfinit;
	profiler.countOperation("tdAssign",lungime,1);
	increaseTD(A,size,key,n);
}

void constructieTD(int* A, int n)
{
	size=1;
	for(int i=2;i<=n;i++)
		insertTD(A,A[i],n);
}
//void heapsortTD(int* A,int n)
//{
//
//	constructieTD(A,n);
//	for (int i=n;i>=2;i--)
//	{
//		int aux=A[1];
//		A[1]=A[i];
//		A[i]=aux;
//		n--;
//		profiler.countOperation("tdAssign",lungime,3);
//
//		reconstructieHeap(A,1,n);
//	}
//}
void main()
{

	int vect[]={0,4,1,3,2,16,9,10,14,8,7};
	int vect2[]={0,6,5,3,1,8,7,2,4};
	//vect2=vect;
	//n din 100 + > 500;
	afisare(vect,10,1,0);
	//heapsortBU(vect,4);
	constructieHeap(vect,10);
	afisare(vect,10,1,0);
	//heapsortBU(vect,10);
	//afisare(vect,10,1,0);
	///top-down
	printf("top-down/n");
	//constructieHeap(vect2,12);
	constructieTD(vect2,8);
	afisare(vect2,8,1,0);
	//heapsortTD(vect2,8);
	afisare(vect2,8,1,0);
	int n;
		int v[10000], v1[10000];
	for(int testt=0;testt<5;testt++)
		{
			for(n=100; n<2000; n+=100)
			{
			lungime=n;
			printf("n=%d\n", n);
			FillRandomArray(v,n);//fillrandomarray(v,n);
			memcpy(v1, v, n * sizeof(int));
			constructieHeap(v1, n);
			memcpy(v1, v, n * sizeof(int));
		constructieTD(v1,n);
			}
		}
	profiler.addSeries("buSerie","buComp","buAssign");
	profiler.addSeries("tdSerie","tdComp","tdAssign");
	profiler.createGroup("DiferentaSerii","buSerie","tdSerie");
	profiler.createGroup("Bottom-Up","buAssign","buComp");
	profiler.createGroup("Top-Down","tdAssign","tdComp");
	profiler.createGroup("ALL","tdAssign","tdComp","buAssign","buComp");	
	profiler.showReport();

}