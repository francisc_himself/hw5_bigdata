#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>
#include <time.h>
#include<stdlib.h>
#include<string.h>
long timee=0,op=0;
bool ok=true;
/*
The running time of the algorithm is O(V+E),increases liniary
1. WHITE indicates a tree edge,
2. GRAY indicates a back edge, and
3. BLACK indicates a forward or cross edge.
TOPOLOGICAL-SORT.G/
1 call DFS.G/ to compute finishing times v.f for each vertex v
2 as each vertex is finished, insert it onto the front of a linked list
3 return the linked list of vertices
*/
/**************************************DFS***********************************************/
//node need for the graph
		typedef struct nod {
			int key;//the key
			int d;//discovery time
			int f;//finish time
			int color; //0-white,1-gray,2-black

		} vert;

		//node for the adjecy list
		typedef struct node {
			vert *v;
			struct node *next;
		} List;

		//queue node used in topological sort
		typedef struct queue {
			vert *q[200];
			int tail;
			int head;
		} Queue;

		typedef struct edge
		{
			int n1;
			int n2;
			char type[10];//the type of edge
		}
		Edge;

	Queue *que=new Queue;
	Edge *E = new Edge[10000];
	int edgenr=-1;
		//operation on queue,enqueue
		void enqueue(vert *s) {
			(*que).q[(*que).tail]=s;
			(*que).tail++;
		}
		//operation on queue,dequeue
		vert *dequeue() {
			(*que).tail--;
			return (*que).q[(*que).tail];
			
		}

	void DFS_VISIT(List *adj[],vert *u)
	{
	List *p;
	timee=timee+1;
	u->d=timee;
	u->color=1;//grey
	p=adj[u->key];
	op+=3;
	while(p!=NULL)
	{
		op+=1;
		if(p->v->color==0)//white
			{edgenr++;
			E[edgenr].n1=u->key;
			E[edgenr].n2=p->v->key;
			strcpy(E[edgenr].type,"Tree");//Tree Edge,is a tree edge if v was first discovered by exploring edge
			op+=2;
			DFS_VISIT(adj,p->v);
			}
		else 
			if(p->v->color==1)        
			{edgenr++;
			E[edgenr].n1=u->key;
			E[edgenr].n2=p->v->key;
			strcpy(E[edgenr].type,"Back");//Back Edge,We consider self-loops, which may occur in directed graphs, to be back edges.
			ok=false;
			}

		else 
			if(p->v->color==2 && u->d<p->v->d)
				{edgenr++;
				E[edgenr].n1=u->key;
				E[edgenr].n2=p->v->key;
				strcpy(E[edgenr].type,"Forward");//Forward edge,nontree edges,connecting a vertex u to a descendant v in a depth-first tree.
			}

		else if(u->d > p->v->d)
		{
			edgenr++;
			E[edgenr].n1=u->key;
			E[edgenr].n2=p->v->key;
			strcpy(E[edgenr].type,"Cross");//Cross Edge,are all other edges
			}
	p=p->next;

	}
		u->color=2;//black
		timee=timee+1;
		u->f=timee;
		op+=2;
		enqueue(u);
	}
	void DFS(List *adj[],int n)
	{	//initliazile the topological queue end & begin with 0
		(*que).head=0;
		(*que).tail=0;
		for(int i=0;i<n;i++)
		{
			adj[i]->v->color=0;
			op++;
		}
		timee=0;
		edgenr=0;
		//perform the dfs
		for(int i=0;i<n;i++)
			{op++;
			if(adj[i]->v->color==0)
				DFS_VISIT(adj,adj[i]->v);
		}
	}

	//program to create de adjency lists
		void create_adjlist(List *adj[],int n,int m)
				{		List *p;
						vert *v1,*v2;
						int noE=0;
						int ok=1;
						srand(time(NULL));
						//allocate all the vertices
						for (int i=0;i<n;i++) {
							adj[i]=(List *)malloc(sizeof(List));
							adj[i]->v=(vert*)malloc(sizeof(vert));
							adj[i]->v->key=i;
							adj[i]->next=NULL;
												}
						//while there are nodes to be added
						while (noE <= m) 
							{
								v1=adj[rand()%n]->v;
								v2=adj[rand()%n]->v;
								p=adj[v1->key];
								ok=1;
						while (p->next!=NULL)//verify if the edge has be already added 
						{
								if (p->v->key==v2->key)
								{
										ok=0;
									break;
								}
							p=p->next;
						}
						if (p->v->key==v2->key) 
								ok=0;
						//add the new egde
						if (ok) 
						{
							p->next=(List *)malloc(sizeof(List));
							p->next->v=v2;
							p->next->next=NULL;
							noE++;
							}
						}
					}

		

int main() 
{
int k[11];
int r;
List *p;
vert *u;
List *adj[200];

FILE *g=fopen("Case1.txt","w");
FILE *f = fopen("Case2.txt","w");
srand(time(NULL));
create_adjlist(adj,10,20);

printf("\nAdjacency list: \n");
for (int i=0;i<10;i++) 
{
p=adj[i];
while (p!=NULL) {
printf("%d ",p->v->key);
p=p->next;
}
printf("\n");
}
DFS(adj,10);
if(ok)
{
printf("Topological Sort \n");
while ((*que).head != (*que).tail)
{	u=dequeue();
	printf(" %d ",u->key);
}
}
else
	printf("No topological sort,cycle detected!");
printf("\n\n");
for(int i=1;i<=edgenr;i++)
	{
		printf("The edge (%d,%d) is %s \n",E[i].n1,E[i].n2,E[i].type);
	}
/*
for (int n=1000;n<=5000;n+=100)
	{op=0;
	create_adjlist(adj,100,n);
	DFS(adj,100);
	fprintf(g,"%ld %ld\n",n,op);
	}

for (int n=100;n<=200;n+=10) 
		{
		op=0;
		create_adjlist(adj,n,9000);
		DFS(adj,n);
		fprintf(f,"%ld %ld\n",n,op);

		}*/
	getch();
	return 0;
}