//***ANONIM*** ***ANONIM***
//Grupa ***GROUP_NUMBER***
//Assignment 7- Multi-way Trees

#include <stdio.h>
#include <stdlib.h>
#define N_MAX 20
#define MAX_FII 10

//R1
typedef struct parinte_fii
{
    int nr_fii;
    int fii[MAX_FII];
}parinte_fii;

//R2
typedef struct multi_way
{
    int key;
    int nr_fii;
    struct multi_way *fii[MAX_FII];
} multi_cai;

//R3
typedef struct binary
{
    int key;
    struct binary *stg;
    struct binary *dr;
} binar;

//O(n) - adaug n-1 fii radacina fiind deja inserata
void creare_arbore_multicai(multi_way **r, parinte_fii fii[])
{
    int i, parinte = (*r)->key;
    for(i = 0; i < fii[parinte].nr_fii; i++)
    {
        //adaugare fiu
        (*r)->fii[i] = (multi_cai*)malloc(sizeof(multi_cai));
        (*r)->fii[i]->key = fii[parinte].fii[i];
        (*r)->fii[i]->nr_fii = 0;
        (*r)->nr_fii++;
        creare_arbore_multicai(&((*r)->fii[i]), fii);
    }
}




//O(n) - adaug n-1 fii radacina fiind deja inserata
void creare_arbore_binar(binary **b, multi_way *m)
{
    binar *aux;
    int i;
    if(m->nr_fii > 0)
    {
        (*b)->stg = (binary*)malloc(sizeof(binary));
        (*b)->stg->key = m->fii[0]->key;
        (*b)->stg->stg = NULL;
        (*b)->stg->dr = NULL;
        aux = (*b)->stg;
        for(i = 1; i < m->nr_fii; i++)
        {
            aux->dr = (binary*)malloc(sizeof(binary));
            aux->dr->key = m->fii[i]->key;
            aux->dr->stg = NULL;
            aux->dr->dr = NULL;
            creare_arbore_binar(&aux,m->fii[i-1]);
            aux = aux->dr;
        }

    }
}

void pretty_print(binary *r, int nr_spatii)
{
    int i;
    for(i = 0; i <= nr_spatii; i++)
        printf(" ");
    printf("%d\n", r->key);
    if(r->stg != NULL)
        pretty_print(r->stg, nr_spatii+1);
    if(r->dr != NULL)
        pretty_print(r->dr, nr_spatii);
}

int main()
{
    //variabile
    int a[N_MAX], i, n,m;
	int exista[N_MAX];
    parinte_fii aux[MAX_FII];
    multi_way *r2 = NULL; //radacina pentru aroborele multi-cai
    binary *r3 = NULL; //radacina pentru arborele binar

	
	//intrarea este un vector de tati
	printf("Dati numarul de elemente ale vectorului de tati: ");
	scanf("%d",&n);
	printf("\nDati elementele vectorului de tati");
	  for (int j=1; j<=n; j++)
	  {
		  scanf("%d",&m);
		  a[j]=m;
		  
	  }
   

    
    printf("R1:\n");
    for(i = 1; i <= n; i++)
        printf("%d ", a[i]);
    printf("\n");
	
     //initializez vectorul exista
	for(int j=1; j<=n; j++)
		exista[j]=0;

    //T1
    //O(n)
        //initializez pentru fiecare nod numarul de fii
    for(i = 1; i <= n; i++)
        aux[i].nr_fii = 0;
    //O(n)
    //creez vectori cu fiii fiecarui nod, cu exceptia radacinii
	
    for(i = 1; i <= n; i++)
    {
		     
		if(a[i] != -1)
        {
            aux[a[i]].fii[aux[a[i]].nr_fii] = i;//inserez fiul
            aux[a[i]].nr_fii++;
        }
		
    }

	//afisarea R2
	printf("\n\n");
	printf("Afisare R2:\n");
	for (i=1; i<=n; i++)
	{
      if ((a[i]!=-1)&&(exista[i]==0))
	  {
		  printf("%d: ",a[i]);
		  for (int j=0; j<aux[a[i]].nr_fii; j++)
			  printf("%d ",aux[a[i]].fii[j]);
	  }   
	  printf("\n");
	  int key=a[i];
	  for (int k=1; k<=n; k++)
          if (key==a[k])
			  exista[k]=1;
	}
	
	

   
    //caut radacina
    i = 1;
    while(a[i] != -1)
        i = a[i];
    //inserez radacina
    r2 = (multi_cai*)malloc(sizeof(multi_cai));
    r2->key = i;
    r2->nr_fii = 0;
    //O(n)
    creare_arbore_multicai(&r2, aux);
	
	


    //T2
    r3 = (binar*)malloc(sizeof(binar));
    r3->key = r2->key;
    r3->stg = NULL;
    r3->dr = NULL;
    creare_arbore_binar(&r3, r2);
    

    //Pretty print R3
    printf("R3:\n");
    pretty_print(r3, 0);
	getchar();
   



	getchar();
    return 0;
}
