
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include<iostream>
using namespace std;

int a[] = {1, 6, 4, 1, 6, 6, -1, 4, 1};
int n = 9;

int f[9];
struct node
{
    int info;
    int nrSons;
    node ** Sons;
} arb[9];

struct node2
{
    int info;
    node2 * Son;
    node2 * Brother;
};

//Transforming the vector into a multiway tree

int multiway(int a[])
{
    int root_multiway = -1;
    for ( int i = 0; i < n; i++)
    {
        arb[i].info = i; 
        if( a[i] == -1 )
        {
            root_multiway = i;
        }
        else f[a[i]]++;  // nr of sons that need to be allocated
    }

    for( int i = 0; i <n; i++)
    {
        if ( i != root_multiway)
        {
            arb[a[i]].Sons = (node**)realloc(arb[a[i]].Sons,sizeof(node*) * f[a[i]]); // realloc the memory
            arb[a[i]].Sons[ arb[a[i]].nrSons++] = &arb[i];							//another element will be reallocated to the ones that we already have
        }
    }
    return root_multiway;
}

void multiway_pretty(node *root, int level) //print for the first case, multiway - pretty print
{
    if(root == NULL) return;
    for ( int i = 0; i < level; i++ )
        cout<<"\t";
    cout<<"--->"<<root->info<<endl;
    for (int i = 0; i < root->nrSons; i++)
        multiway_pretty(root->Sons[i],level+1);
}


//Transforming from a multiway tree into a binary tree


void binary( node *root, node2 *root2) // building the binary tree
{
    if( root != NULL)
    {

        if ( root->nrSons > 0)
        {
            root2->Son = new node2; // allocating memory for the son
            root2->Son->info = root->Sons[0]->info;
            binary(root->Sons[0],root2->Son); //recursive call for the son
            root2 = root2->Son;
            for ( int i = 1; i < root->nrSons; i++)
            {
                root2->Brother = new node2; // allocating memory for the brother
                root2->Brother->info = root->Sons[i]->info;
                binary(root->Sons[i],root2->Brother); //recursive call for the brother
                root2 = root2->Brother;
            }
            root2->Brother = NULL;
        }
        else root2->Son = NULL;

    }
}


void pretty_print( node2 *root, int level)//printing for the second case, binary - pretty print
{
	if (root!=NULL){

		 pretty_print(root->Brother,level+1);
		 for( int i=0;i<=level;i++)
			 cout<<"\t";
		 cout<<root->info<<endl;
		 pretty_print(root->Son,level+1);
	       }
}

int main()
{

    cout<<"Transformation from vector into a multiway tree:"<<endl;
    int root1 = multiway(a);
    multiway_pretty(&arb[root1],0);
    cout<<endl<<endl<<"Transformation from multiway tree into a binary tree:"<<endl;
    node2* root2 = new node2;
    root2->info = arb[root1].info;
    root2->Brother = NULL;
    binary(&arb[root1],root2);
    pretty_print(root2,0);
	getche();
    return 0;
	
}



