/*Student:						***ANONIM*** ***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Cerinta:				
	Se cere implementarea corecta si eficienta a operatiilor de adaugare si cautare in tabela de dispersie cu adresare deschisa si 
	verificare patratica.

	-folosirea unei tabele de dispersie cu adreasare deschisa

	-se va folosi o functie de HASH-ing de forma h(k,i)=(h'(k)+c1*i+c2*i*i) mod N unde c1,c2 sunt constante auxiliare

	-programul va fi testat pentru factori de umplere 80%,85%,90%,95%,99%

	-se vor cauta 1500 elemente exitente in tabela si 1500 elemente ce nu exista in tabela; si se va retine numarul de operatii 
	pentru fiecare caz

    -datele rezultate se vor afisa intr-un tabel cu urmatoarele coloane 
	factorului de umplere|Efort mediu cautare elemente gasite|Efort maxim gasite|Efort mediu cautare negasite|Efort maxim negasite|

    

	Concluzii:
	
	-pentru gasirea elementelor existente in tabela numarul de operatii este mic si aproape constant indiferent 
	de dimensiunea tabelei
    
	-in cazul cautarii unui elemnt inexistent in tabela numarul de operatii este relativ mic in comparatie cu dimensiunea tabelei
	
	-numarul de operatii creste in functie de factorul de umplere(lucru care se vede clar la cautarea elementelor ce nu exista in
	tabela si mai putin vizibil la cautarea elemntelor existente in tabela)
    
	-valorile relativ mici se datoreaza constructiei functiei de HASH-ing



	Solutie pentru stergerea transparenta a elementelor
	-double buffering
	-crearea unei noi tablele de hash, se complica cautarea
	-copierea tabelei intr-un alt tabel mai mare
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>
#define HASH_SIZE 9973
#define SEARCH_SIZE 3000
#include"profiler.h"

int HashTable[HASH_SIZE],samples[HASH_SIZE+SEARCH_SIZE/2],indices[SEARCH_SIZE/2],ef,ef_g,ef_max_g,ef_neg ,ef_max_neg;
FILE *f;


int h(int value,int index)   //functia de hash principala
{
  int x;

  x=(value+4*index+6*index*index)%HASH_SIZE;
  return x;
}
int Hash_Insert(int k, int *HashTable)   ///functia de inserare 
{
   int i=0,j;
   do{
       j=h(k,i);
       if(HashTable[j]==-1)
        {
           HashTable[j]=k;
	       return j;
        }
       else i++;

   }while(i<HASH_SIZE);

return -1;
}

int Hash_Search(int k,int *HashTable)// functia de cautare 
{
  int i=0,j;

   do{
      j=h(k,i);
 
       if(HashTable[j]==k)
        {
	     return j;
        }
        else i++;
		ef++;
    
   }while((HashTable[j]!=-1) && (i<HASH_SIZE));

return -1;
 }

void corect()// testarea pentru valori mici
{
	int hi;//index din tabela de hash

	for(int i=0;i<HASH_SIZE;i++)//initializarea tabelei de hashing
	{
		HashTable[i] = -1;
	}


	for(int i=1;i<=30;i++)
		Hash_Insert(i,HashTable);

	
	for(int i = 1; i <= 50;i+=5)
	{
		if((hi=Hash_Search(i,HashTable))!=-1)
		{
			printf("\nla pozitia %d am gasit: %d\n", i, hi);
		}
		else 
		{
			printf("\n %d nu a fost gasita in tabela\n", i);
		}


	}
}



void tabel(float fu)// fu - transmis ca si parametru este factorul de umplere al tablei
{

	ef_g=ef_max_g=ef_neg=ef_max_neg=0;
	
	for(int i=0;i<HASH_SIZE;i++)//initializarea tabelei de hashing
	{
		HashTable[i] = -1;
	}

	int n = fu*HASH_SIZE; 
	f = fopen("tabel.csv","a");
	
	fprintf(f,"\nF.umpl, ef.med.elem.gasite, ef.max.elem.gasite, ef.med.elem.negasite, ef.max.elem.negasite\n");//creare cap de tabel
	
	
		FillRandomArray(samples,n+SEARCH_SIZE/2,1,1000000,true,0);
		FillRandomArray(indices,SEARCH_SIZE/2,0,n-1,true,0);
			
			for(int i = 0;i<n;i++)
			{
				Hash_Insert(samples[i],HashTable);
			}


			//for pentru elemente gasite
	
			for(int i= 0;i<SEARCH_SIZE/2;i++)
			{
				
				ef=0;
				Hash_Search(samples[indices[i]],HashTable);
				if(ef_max_g< ef)
				{
					ef_max_g= ef;
				}
				ef_g+=ef;
			}
	
	
			//for pentru el negasite
			for(int i= 0;i<SEARCH_SIZE/2;i++)
			{
				ef=0;
				Hash_Search(samples[n+i],HashTable);
				if(ef_max_neg< ef)
				{
					ef_max_neg = ef;
				}
				ef_neg+=ef;
			}
	
			fprintf(f,"%f, %d, %d, %d, %d\n",fu,2*ef_g/SEARCH_SIZE,ef_max_g,2*ef_neg/SEARCH_SIZE,ef_max_neg);
	
	fclose(f);
}

void main()
{
//corect();


tabel(0.80);
tabel(0.85);
tabel(0.90);
tabel(0.95);
tabel(0.99);
getch();

}