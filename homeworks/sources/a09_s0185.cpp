#include<iostream>
#include<fstream>
#include<time.h>

using namespace std;

ofstream g("data1.csv");
ofstream f("data2.csv");


typedef struct NOD
{
	int value;
	struct NOD * next;
}nod;

NOD* L[10001];
int viz[10001]; 
int n;
int prim,ultim,C[10001];
int op = 0;

void addNode(int i,int j)
{
	nod *p,*q;
	p=new nod;
	op=op+7;
	p->value=j;
	p->next=L[i];
	L[i]=p;
	q=new nod;
	q->value=i;
	q->next=L[j];
	L[j]=q;
}

void bfs()
{
	int varf,nr;
	nod *p;
	while(prim<=ultim)
	{
		varf=C[prim];
		p=L[varf]; 
		op+=2;
		while(p)
		{
			nr=p->value;
			op++;
			if(viz[nr]==0)
			{
				ultim++; 
				C[ultim]=nr; 
				op++;
				op++;
				viz[nr]=1;
			}  
			p=p->next;
			op++;
		}
		prim++; 
	}
}

void afisare(int nr_nod)
{
	nod *p=L[nr_nod];
	if(p==0)
		cout<<nr_nod<<" este izolat \n";
	else
	{
		cout<<nr_nod<<" : ";
		nod *c=p;
		while(c)
		{
			cout<<c->value<<" ";
			c=c->next;
		}
		cout<<"\n";
	}
}


void test(){
	int n = 7;//noduri
	int start = 3;
	addNode(1,4);
	addNode(1,2);
	addNode(2,3);
	addNode(3,4);
	addNode(3,5);
	addNode(6,7);
	for(int i=1;i<=n;i++)
		afisare(i);
	viz[start]=1;
	prim=ultim=1;
	C[prim]=start;
	bfs();
	for(int i=1;i<=ultim;i++)
		cout<<C[i]<<" ";

}

void problema1(){
	n = 100;//noduri
	int nrArc;
	int nod1,nod2;
	int start = 3;
	for(nrArc=1000;nrArc<=5000;nrArc=nrArc+100)
	{
	nod1 = rand()%100 + 1;
	nod2 = rand()%100 + 1;
	addNode(nod1,nod2);
	g<<nrArc<<","<<op<<endl;
	viz[start]=1;
	prim=ultim=1;
	C[prim]=start;
	bfs();
	}
	

}

void problema2(){

	int nrArc;
	int nod1, nod2;
	int start = 3;
	for(n=100;n<=200;n=n+10){
	f<<n<<","<<op<<endl;
	op = 0;
	for(nrArc = 1; nrArc<=9000;nrArc++){
	nod1 = rand()&n + 1;
	nod2 = rand()&n + 1;
	addNode(nod1,nod2);
	viz[start]=1;
	prim=ultim=1;
	C[prim]=start;
	
	}
	bfs();
	}

}
void main()
{
	
	test();
	problema1();
	problema2();

}