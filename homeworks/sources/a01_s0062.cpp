/* Nume: ***ANONIM*** ***ANONIM*** ***ANONIM***
* Grupa : ***GROUP_NUMBER*** 
* Data : 26.II.2013

*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

int a[10000],c[10000];
int bubAt=0,bubComp=0;
int selAt=0,selComp=0;
int inAt=0,inComp=0;

void Bubble_Sort(int n)
{
	int ok=0,aux;
	bubComp=0;
	bubAt=0;
	
	while(ok==0)
	{
	  ok=1;
	  for(int j=1;j<=n-1;j++)
		{
		  bubComp++;
		   if(a[j]>a[j+1])
			{
				bubAt++;
				aux=a[j];
				a[j]=a[j+1];
				a[j+1]=aux;	
				ok=0;
			}
		}
	}
}


void Select_Sort(int n)
{
	int aux,imin;
	selAt=0;
	selComp=0;

	for(int i=1;i<=n-1;i++)
	{
		
		imin=i;
		for(int j=i+1;j<=n;j++)
		 {
			selComp++;
			if(a[j]<a[imin])
			 {
				imin=j;	
			 }			
		 }
		selAt++;
		aux=a[i];
		a[i]=a[imin];
		a[imin]=aux;		
	}
}


void Insert_Sort(int n)
{
	int j,x;	
	inAt=0;
	inComp=0;

	for(int i=2;i<=n;i++)
	{
		x=a[i];
		j=1;
		inComp++;
		while(a[j]<x)
		 {
		   j=j+1;
		   inComp++;
		 }
		for(int k=i;k>=j+1;k--)
			{
			a[k]=a[k-1];
			inAt++;
			}
		a[j]=x;
		inAt++;
	}
}

int main()
{
	int n,m,i;
	int auxAt[2],auxComp[2];
	FILE *f;

	f=fopen("fisier.csv","w");		
	fprintf(f,"n, b_at_fav, b_comp_fav,b_at+comp_fav,s_at_fav,s_comp_fav,s_at+comp_fav,i_at_fav,i_comp_fav,i_at+comp_fav,");  
	fprintf(f,"b_at_med,b_comp_med,b_at+comp_med,s_at_med,s_comp_med,s_at+comp_med,i_at_med,i_comp_med,i_at+comp_med,");    
	fprintf(f,"b_at_def,b_comp_def,b_at+comp_def,s_at_def,s_comp_def,s_at+comp_def,i_at_def,i_comp_def,i_at+comp_def \n");   
	
	for(n=100;n<=10000;n=n+100)
	{
		printf("%d, au mai ramas %d pasi. \n ", n, ((10000-n)/100));
		fprintf(f, "%d,", n);
		for (i=1;i<=n;i++)
			a[i] = i;

		Bubble_Sort(n);
		fprintf(f,"%d,%d,%d,",bubAt,bubComp,bubAt+bubComp);

		Select_Sort(n);
		Select_Sort(n);
		fprintf(f,"%d,%d,%d,",selAt,selComp,selAt+selComp);
		Insert_Sort(n);
		fprintf(f,"%d,%d,%d,",inAt,inComp,inAt+inComp);
		auxAt[0]=0; auxAt[1]=0; auxAt[2]=0;
		auxComp[0]=0; auxComp[1]=0; auxComp[2]=0;
		for(m=1;m<=5;m++)
		{
		  srand (time(NULL));
	      for(int b=1;i<=n;i++)
	       {				
		    a[i]=rand()%1000;
		    c[i]=a[i];
	       }	
			
			Bubble_Sort(n);
			auxAt[0]=auxAt[0]+bubAt;
			auxComp[0]=auxComp[0]+bubComp;

			for(i=1;i<=n;i++)
			a[i]=c[i];
			Select_Sort(n);
			auxAt[1]=auxAt[1]+selAt;
			auxComp[1]=auxComp[1]+selComp;

			for(i=1;i<=n;i++)
			a[i]=c[i];
			Insert_Sort(n);
			auxAt[2]=auxAt[2]+inAt;
			auxComp[2]=auxComp[2]+inComp;
		}

		fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,",auxAt[0]/5,auxComp[0]/5,(auxAt[0]+auxComp[0])/5 , auxAt[1]/5,auxComp[1]/5,(auxAt[1]+auxComp[1])/5 , auxAt[2]/5,auxComp[2]/5,(auxAt[2]+auxComp[2])/5 );
		for(i=1;i<=n;i++)
			a[i]=n-i;
		
		Bubble_Sort(n);
		fprintf(f,"%d,%d,%d,",bubAt,bubComp,bubAt+bubComp);

		for(i=1;i<=n;i++)
			a[i]=n-i;
		Select_Sort(n);
		fprintf(f,"%d,%d,%d,",selAt,selComp,selAt+selComp);

		for(i=1;i<=n;i++)
			a[i]=n-i;
		Insert_Sort(n);
		fprintf(f,"%d,%d,%d \n",inAt,inComp,inAt+inComp);
	}
	fclose(f);
	getch();	
}