// DFS_Sortare-topologica.cpp : Defines the entry point for the console application.
// ***ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER***
// Tema nr 10

#include "stdafx.h"



#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>

#define ALB 0
#define GRI 1
#define NEGRU 2

int e_aciclic=1; //presupunem ca graful e aciclic si orientat, deci poate fi sortat topologic

struct TNOD{
	int val;
	TNOD *urm;
};

typedef struct Prop
{
	int culoare, parinte, d, f; //unde d si f sunt marcaje de timp 
} PROP;

TNOD *prim[60001], *ultim[60001];
PROP prop[60001];
int t;
int q;


//pentru reprezentarea grafului sub forma de liste de adiacenta
bool verif (int x, int y)
{
	TNOD *p;
	p=prim[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->urm;
	}
	return true;

}

void genereaza(int nrv,int nre)  //nrv= numarul de varfuri, nre = numarul de edges
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		prim[i]=ultim[i]=NULL;
	i=1;
	while (i<=2*nre)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
				if(prim[u]==NULL)
				{
					prim[u]=(TNOD *)malloc(sizeof(TNOD *));
					prim[u]->val=v;
					prim[u]->urm=NULL;
					ultim[u]=prim[u];
					i++;
				}
				else {
						if (verif (u, v))
						{
							TNOD *p;
			            	p=(TNOD *)malloc(sizeof(TNOD *));
							p->val=v;
							p->urm=NULL;
							ultim[u]->urm=p;
							ultim[u]=p;
							i++;
						}
					}
		}
	}
}


void print(int nrv)
{
	int i;
	TNOD *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=prim[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->urm;
		}
		printf("\n");
	}
}

void DFS_VISIT(int u)
{
	printf("%d ",u);
	prop[u].culoare = GRI;
	prop[u].d = t;
	t++;
	TNOD *x;
	int v;
	x = prim[u];
	q++; //operatii
	while(x!=NULL)
	{
		v = x->val;
		x = x->urm;
		q++;    //operatii
		if(prop[v].culoare==ALB)  
		{
			prop[v].parinte = u;
			q++;  //operatii
			//printf("[%d %d] -> apartine arbore\n",u,v);
			//printf("%d\n",v);
			DFS_VISIT(v);
		}
		else 
		{
			//daca e gri e muchie inapoi
			q++;
			if (prop[v].culoare==GRI)
				e_aciclic=0;
		}
		
	}
	prop[u].culoare = NEGRU;  
	prop[u].f = t;
	t = t+1;
}

void Init(int n)
{
	t = 0; //timp
	q = 0; //operatii
	int i;
	for(i = 1 ; i <= n ; i++)
	{
		prop[i].f = 0;
		prop[i].d = 0;
		prop[i].parinte = 0;
		prop[i].culoare = ALB;
	}
}

void DFS(int n)
{
	int u;
	for(u = 1 ; u <= n ; u++)
	{
		prop[u].culoare = ALB;
		prop[u].parinte = 0;
	}
	for( u = 1 ; u <= n ; u++)
	{
		q++;    //operatii
		if(prop[u].culoare==ALB)
		{
			//printf("%d\n",u);
			DFS_VISIT(u);
		}
	}
}

int main()
{

//n reprezinta numarul de varfuri ale grafului, iar nr numarul de muchii
int n,i,nr;
int nrv,nre;
n=5;
nr=9;

srand(time(NULL));
genereaza(n,nr);
print(n);
Init(n);
printf("\nalgoritmul DFS:\n");
DFS(n);

if (e_aciclic==0)
 printf("\nGraful nu poate fi sortat topologic.");
else
	printf("\nGraful nu poate fi sortat topologic.");

FILE *f;

/*f = fopen("fct1.csv","w");
fprintf(f,"nrv=100,nre,q\n");
nrv=100;
for(nre = 1000 ; nre <= 5000 ; nre = nre+100)
{
	srand(time(NULL));
	genereaza(nrv,nre);
	Init(nrv);
	DFS(nrv);
	fprintf(f," %d , %d , %d\n",nrv,nre,q);
}
fclose(f);*/

/*f1 = fopen("fct2.csv","w");
fprintf(f1,"nrv,nre=9000,q\n");
nre=9000;
for(nrv = 110 ; nrv <= 200 ; nrv = nrv+10)
{

	genereaza(nrv,nre);
	Init(nrv);
	DFS(nrv);
	fprintf(f1," %d , %d , %d\n",nrv,nre,q);
}
fclose(f1);*/




getch();
return 0;
}


