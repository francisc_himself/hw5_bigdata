#include <stdio.h>
#include <stdlib.h>

int nr_op = 0;

struct arbore_binar
{
    int numar;
    int grad;
    struct arbore_binar *st, *dr, *p;
};

void prety_print(struct arbore_binar *rad, int i)
{
    int j;
    for(j = i; j > 0; j--)
        printf(" ");
    printf("%d\n", rad->numar);
    if(rad->st != NULL)
        prety_print(rad->st, i + 1);
    if(rad->dr != NULL)
        prety_print(rad->dr, i + 1);
}

void inordine(struct arbore_binar *rad)
{
    if(rad != NULL)
    {
        if(rad->st != NULL)
            inordine(rad->st);
        printf("%d ", rad->numar);
        if(rad->dr != NULL)
            inordine(rad->dr);
    }
}

void inordine_g(struct arbore_binar *rad)
{
    if(rad != NULL)
    {
        if(rad->st != NULL)
            inordine_g(rad->st);
        printf("%d ", rad->grad);
        if(rad->dr != NULL)
            inordine_g(rad->dr);
    }
}

void preordine(struct arbore_binar *rad)
{
    printf("%d ", rad->numar);
    if(rad->st != NULL)
        inordine(rad->st);
    if(rad->dr != NULL)
        inordine(rad->dr);
}

void inserare(struct arbore_binar **radacina, int nr, struct arbore_binar *parinte)
{
    if (*radacina == NULL)
    {
        *radacina = (struct arbore_binar*) malloc(sizeof(struct arbore_binar));
        (*radacina)->numar = nr;
        (*radacina)->grad = 1;
        (*radacina)->st = NULL;
        (*radacina)->dr = NULL;
        (*radacina)->p = parinte;
    }
    else if (nr < (*radacina)->numar)
    {
        (*radacina)->grad++;
        inserare (&(*radacina)->st, nr, *radacina);
    }
    else if (nr > (*radacina)->numar)
    {
        (*radacina)->grad++;
        inserare (&(*radacina)->dr, nr, *radacina);
    }
}

void creare_arbore(struct arbore_binar **radacina, int a, int b)
{
    inserare(radacina, (a+b)/2, NULL);
    //printf("inserare %d\n", (a+b)/2);
    if(a <= (a+b)/2-1)
        creare_arbore (radacina, a, (a+b)/2-1);
    if((a+b)/2+1 <= b)
        creare_arbore (radacina, (a+b)/2+1, b);
}

void stergere(struct arbore_binar **radacina, int nr)
{
    nr_op++;
    if ((*radacina)->numar == nr)
    {
        if ((*radacina)->grad == 1)
        {
            if((*radacina)->p == NULL)
            {
                free(*radacina);
                *radacina = NULL;
            }
            else
            {
                //printf("nr de sters: %d\nnr parinte %d", nr, (*radacina)->p->numar);
                nr_op++;
                if(nr < ((*radacina)->p->numar))
                    (*radacina)->p->st = NULL;
                else //(nr > (*radacina)->p->numar)
                    (*radacina)->p->dr = NULL;
                free(*radacina);
            }
        }
        else
        {
            struct arbore_binar *r = NULL, *aux = NULL;
            int ok = 0;//daca intra in while ok = 1
            (*radacina)->grad--;
            if((*radacina)->st != NULL)
            {
                r = (*radacina)->st;
                r->grad--;
                while(r->dr != NULL)
                {
                    ok = 1;
                    r = r->dr;
                    r->grad--;
                }
                if (ok == 1)
                    r->p->dr = NULL;
                else
                    r->p->st = NULL;
            }
            else
            {
                r = (*radacina)->dr;
                r->grad--;
                while(r->st != NULL)
                {
                    ok = 1;
                    r = r->st;
                    r->grad--;
                }
                if (ok == 1)
                    r->p->st = NULL;
                else
                    r->p->dr = NULL;
            }
            r->grad = (*radacina)->grad;
            aux = (*radacina);
            r->st = (*radacina)->st;
            r->dr = (*radacina)->dr;
            r->p = (*radacina)->p;
            if (r->dr != NULL)
                r->dr->p = r;
            if (r->st != NULL)
                r->st->p = r;
            *radacina = r;
            free(aux);
        }
    }
    else
    {
        nr_op++;
        (*radacina)->grad--;
        nr_op++;
        if (nr < (*radacina)->numar)
            stergere (&(*radacina)->st, nr);
        else
            stergere (&(*radacina)->dr, nr);
    }
}

int select(struct arbore_binar *r, int grad)
{
    int gr_nod_st;
    nr_op++;
    if (r->st != NULL)
        gr_nod_st = r->st->grad + 1;
    else
        gr_nod_st = 1;
    //printf("nod: %d\ngrad nodul fiu stg: %d\n", r->numar, gr_nod_st);
    nr_op++;
    if (grad < gr_nod_st)
    {
        //printf("\tstanga\n");
        select (r->st, grad);
    }
    else if (grad > gr_nod_st)
    {
        //printf("\tdreapta\n");
        select (r->dr, grad - gr_nod_st);
    }
    else //if grad == gr_nod_st
        return r->numar;
}

int main()
{
    struct arbore_binar *r = NULL;
    int j = 1, n = 20, m = 3, k, x;
    FILE *f;
    f = fopen("arbori.csv","w");
    fprintf(f,"n,op\n");

    /************************EXEMPLU************************/
    creare_arbore(&r,1,n);
    //prety_print(r,1);
    inordine(r);
    printf("\n");
    inordine_g(r);
    printf("\n");
    prety_print(r,0);
    for (k = n; k >= 1; k--)
    {
        // gradul elementului
        j = (( j + m - 2) % k) + 1;
        printf("j[%d] = %d\n", n - k + 1, j);
        x = select(r,j);
        //printf("nod = %d\n", x);
        stergere(&r, x);
        printf("nodul %d a fost sters\n", x);
        printf("arborele in inordine:\n");
        inordine(r);
        printf("\ngradele arborelui in inordine\n");
        inordine_g(r);
        printf("\n");
        prety_print(r,0);
    }
    /************************EXEMPLU************************/
    /*for(n = 100; n <= 10000; n = n + 100)
    {
        m = n/2;
        j = 1;
        creare_arbore(&r,1,n);
        prety_print(r, 1);
        //inordine(r);
        for(k = n; k >=1; k--)
        {
            j = (( j + m - 2) % k) + 1;
            x = select(r,j);
            printf("nod: %d\n", x);
            stergere(&r, x);
            printf("%d\n", k);
        }
        fprintf(f, "%d,%d\n", n, nr_op);
        nr_op = 0;
        printf("%d\n", n);
    }*/

    return 0;
}
