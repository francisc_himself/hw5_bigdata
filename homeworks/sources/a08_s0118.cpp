/*
* Assignment 8: Disjoint Sets
* ***ANONIM*** ***ANONIM***
*
* Implement correctly and efficiently Make-Set(x), Union(x,y), Find-Set(x)
* using path compression and union by rank to improve the running time.
*
*/
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct nod {
	int cheie;		//cheia 
	int dim;		//dimensiunea componentei conexe
	struct nod *reprezentant;	//referinta la reprezentant
	struct nod *urm;			//referinta la urmatorul nod
} NOD;

//structura care retine informatie despre o muchie
typedef struct muchie {
	int nod1;		//nodul de pornire
	int nod2;		//nodul de terminare
} muchie;

NOD *prim[10000], *ultim[10000];
int count;
muchie muchii[60000];

//genereaza muchiile si varfurile unui graf cu numarul de muchii dat ca parametru
void generare_graf(int m) {
	int i = 0;
	int nod1, nod2;

	//numarare muchii
	while(i < m) {  
		//nod1, nod2 = capetele unei muchii, generate aleator
		nod1 = rand() % 10000;
		nod2 = rand() % 10000;

		//se verifica sa nu fie pe diagonala principala
		if (nod1 != nod2) {
			//creare muchii, (nod1,nod2) si (nod2,nod1)
			muchii[i].nod1 = nod1;		
			muchii[i].nod2 = nod2;
			i++;
		}
	}
}

//formeaza o multime cu un reprezentat dat ca parametru
void makeSet(int x) {
	//se aloca memorie pentru pointerul de inceput al listei cu reprezentatul x
	prim[x] = (NOD*)malloc(sizeof(NOD));
	
	//dimensiunea listei
	prim[x]->dim = 1;		
	
	//cheia listei
	prim[x]->cheie = x;		
	
	//reprezentatul multimii va fi nodul respectiv
	prim[x]->reprezentant = prim[x];	
	
	//initial lista va contine un singur element
	prim[x]->urm = NULL;				
	ultim[x] = prim[x]; 
}

//gaseste o multime in functie de reprezentatul ei dat ca parametru
int findSet(int x) {
	//gaseste reprezentantul si returneaza cheia
	return prim[x]->reprezentant->cheie;  
}

//reuneste doua multimi
void Union(int x, int y) {
	NOD *p;

	//verifica dimensiunea multimilor
	if (prim[x]->dim >= prim[y]->dim) {
		//seteaza reprezentantul multimii mai mici cu reprezentantul multimii mai mari
		prim[y]->reprezentant = prim[x];	

		//uneste multimea mai mica la multimea mai mare si seteaza ultimul nod din lista x sa pointeze spre primul din lista y
		ultim[x]->urm = prim[y];	

		//ultimul nod al listei x va fi ultimul nod a listei y
		ultim[x] = ultim[y];

		//la dimensiunea listei x se adauga dimensiunea listei y
		prim[x]->dim += prim[y]->dim;	

		//se parcurge lista y
		p = prim[y]; 
		while (p != NULL) {
			//nodurile din lista y vor pointa spre reprezentantul listei x
			p->reprezentant = prim[x]; 
			p = p->urm;
		}
	}
	//se adauga lista x la lista y utilizandu-se aceleasi operatii, complementare
	else {
		prim[x]->reprezentant = prim[y];
		ultim[y]->urm = prim[x];
		ultim[y] = ultim[x];
		prim[y]->dim += prim[x]->dim;
		p = prim[x];
		while (p != NULL) {
			p->reprezentant = prim[y];
			p = p->urm;
		}
	}
}

void componenta(int m) {   
	int i;
	int rez1, rez2;

	//se genereaza listele cu cate un reprezentant
	for (i=0; i<10000; i++) {
		count++;
		makeSet(i); 
	}

	for (i=0; i<m; i++) {	
		count = count+2;
		rez1 = findSet(muchii[i].nod1);
		rez2 = findSet(muchii[i].nod2);
		//verifica daca listele care urmeaza sa fie reunite au acelasi reprezentanti diferiti
		if (rez1 != rez2) {
			count = count+3;
			Union(rez1, rez2);
		}
	}
}

void main() {
	FILE *pf;
	int i, j;

	pf = fopen("rezultate.txt","w");
	for (i=10000; i<=60000; i=i+1000) {
		count = 0;
		generare_graf(i);
		printf("%d\n",i);
		componenta(i);
		fprintf(pf,"%d	%d\n",i,count);
		for (j=0; j<10000; j++) {	
			free(prim[j]);
		}
	}
	fclose(pf);	
}