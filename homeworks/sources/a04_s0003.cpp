#include <iostream>
#include <fstream>

using namespace std;

int n,k, v[1000], dimh;
ifstream f("liste.in");

typedef struct NOD
{
    int value;
    struct NOD *urm;
}NOD;

typedef struct _HEAP
{
	int index;
	NOD *node;
} _HEAP;

_HEAP heap[1000];
NOD *prim [1003];
NOD *ultim [1003];
NOD *primfinal, * ultimfinal;

void initList(NOD *&prim , NOD *&ultim)
{
    prim=NULL;
    ultim=NULL;
}

void adauga_prim ( NOD *&prim, int i)
{
    prim= new NOD;
    prim->value = i;
    prim->urm=NULL;
    ultim[k]=prim;
}

void adauga_nod ( NOD *&ultim, int i)
{
    NOD *p = new NOD;
	p->value=i;
    ultim->urm=p;
    ultim=p;
    ultim->urm=NULL;
}

void afisare ( NOD *prim)
{
    for(NOD *p=prim; p!=NULL; p=p->urm)
        cout<<p->value<<" ";
}
void sterge_prim ( NOD *prim)
{
  NOD *p = new NOD;
  p=prim->urm;
  cout<<endl<<p->value;
 // p->value=prim->urm->value;
  free(prim);
  prim=p;
 // cout<<endl<<prim->value;
}
int parinte (int i )
{
	return i/2;
}
int stanga ( int i )
{
	return 2*i;
}
int dreapta ( int i )
{
	return 2*i+1;
}
void reconstructie_heap ( int v[], int i)
{
	int st,dr,min,aux;
	st=stanga(i);
	dr=dreapta(i);
	if ( st<=dimh && v[st]<v[i])
		min=st;
	else
		min=i;
	if ( dr<=dimh && v[dr]<v[min])
		min=dr;
	if (min!=i)
	{
		aux=v[i];
		v[i]=v[min];
		v[min]=aux;
		//cout<<min<<endl;
		reconstructie_heap(v,min);
	}
}

void construire_heap (int v[])
{
	int i;
	dimh=k;
	cout<<"dim heap-ului: "<<dimh<<endl;
	for(i=k/2;i>=1;i--)
		reconstructie_heap(v,i);
}
int main()
{
    int i,nr,x;   
    
	for(i=1;i<=1000;i++)
	{
		prim[i]=NULL;
		ultim[i]=NULL;
	}
	
	while(!f.eof())
    {
        f>>n;
        k++;
        for(i=1;i<=n;i++)
        {
			f>>x;
			if(i==1)
               adauga_prim(prim[k],x);
            else
			   adauga_nod(ultim[k],x);
		}
	}

	for(i=1;i<=k;i++)
	{	
		afisare(prim[i]);
		cout<<endl;
    }
	
	sterge_prim(prim[2]);
	
	cout<<"prim: "<<prim[2]->value;
	//for(i=1;i<=k;i++)
		//v[i]=prim[i]->value;
	
	//construire_heap(v);
	
	cout<<endl;
	
	for(i=1;i<=k;i++)
		cout<<v[i]<<" ";
	
	return 0;
}
