/*

	Student:					***ANONIM*** ***ANONIM***-***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Cerinte:					Sa se implementeze un program care pune in evidenta eficienta algoritmilor HEAP-SORT si QUICKSORT
	                             in cazul mediu statistic.
	                            
	Cerinte speciale:			Pentru fiecare algoritm se vor calcula numarul de comparatii si 
	                            numarul de atribuiri efectuate.Aceste valori vor fi memorate intr-un fisier
								iar datele din fisier vor fi folosite pentru trasarea unor grafice
								care vor pune in evidenta eficienta fiecarui algoritm fata de ceilalti algoritmi.
							    Algoritmii vor fi testati in cazul mediu.
	Concluzii, interpretari:	
	                            Atat HEAP-SORT cat si QUICK-SORT sunt de complexitate n*log n insa din graficele realizate pe baza datelor 
								returnate de program reiese ca algoritmul Quick-Sort este mai bun decat Heap-Sort

								Algoritmul Heap-Sort are la baza un MAX-HEAP si foloseste constructia Buttom-Up pentru a obtine maximum de eficienta

*/

#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
int atribuiri,comparatii;         //reprezinta numarul de atribuiri respectiv comparatii
                      //acestea 2 sunt declarate ca variabile globale pentru a avea vizibilitate intoate structurile programului
                     // fiind astfel mai usor efectuarea de modificari asupra acestora
#define NMAX 10000   //reprezinta dimensiunea maxima a vectorilor folositi





void copiere(int n,int *sir1,int *sir2) //functia ce realizeaza dublicarea sirului 1 copiind valorile sale in sirul 2
{                                       //functie folosita pentru pastrarea ordinii elemetelor sirului 1 deoarece in urma aplicarii  
	                                    //directe a algoritmilor de sortare asupra sa, structura sirului s-ar modifica 
	                                    //e nevoie ca sirul 1 care va fi dat ca parametru functiilor sa fie acelasi pentru 
	                                    //ca rezultatele obtinute legate de eficienta sa fie relevante
	                                    //din aceasta cauza se va folosi un sir auxiliar sir2 care va fi o copie a lui sir 1
	int i;  
	for (i=0;i<n;i++)
		sir2[i]=sir1[i];
}

void creare_sir_m(int n,int *v)      //functie ce creeza un sir de lungime "n" cu elemente aleatoare
{
	int i;
	
	for(i=0;i<n;i++)
		v[i]=rand();

	                                   //sirul obtinut va fi folosit la testatea cazului mediu
}



void interschimba(int *a,int *b)              //functie ce realizeaza inteschimbarea valorilor a 2 variabile
{ int aux;                           //functia este utilizata pentru o mai buna lizibilitate a codului si a vedea exact unde se mareste numarul de atribuiri

 aux=(*a);
 (*a)=(*b);
 (*b)=aux;
}

//---------------------------Functii necesare realizarea HEAP-SORT-ului

int STANGA(int i)                      //Functie ce returneaza indicele fiului stang nodului i
{
  return 2*i+1;
}

int DREAPTA(int i)                        //Functie ce returneaza indicele fiului drept a nodului i
{
  return 2*i+2;
}

int PARINTE(int i)                       //Functie ce returneaza indicele parintelui nodului  i
{
  return i/2;
}
//creearea si utilizarea acestor functii permite o mai buna lizibilitate a codului
//cele 3 functii sunt folosite in cadrul algoritmilor de generare a HEAP-urilor

void MH(int *s,int i,int hsize)       // Functie care verifica daca un element din arbore respecta condtiile unui MAX HEAP 
{  int l=STANGA(i);                     //Functia contribuie de asemenea si la constructia BOTTOM-UP
   int r=DREAPTA(i);
   int large;
   
   
   comparatii++;
   if((l<hsize)&&(s[l]>s[i]))
       large=l;
   else large=i;
   comparatii++;
   if((r<hsize)&&(s[r]>s[large]))
	   large=r;
  
   if(large!=i)
   {    atribuiri++;
      interschimba(&s[i],&s[large]);
	  MH(s,large,hsize);
   }


}



void buildH(int *a,int n,int *h_size) //constructie heap Bottom-Down
{
  (*h_size)=n;
  int i;


  for(i=(n/2);i>=0;i--)
       MH(a,i,*h_size);
}

void HeapSort(int *a,int n)
{
  int i,k;
  int size=0;
  buildH(a,n,&size);

  /*for(i=0;i<(n/2);i++)
  {
    interschimba(&a[i],&a[n-i]);
	atribuiri++;
  
  }
  */
 
  for(i=n-1;i>=1;i--)
  {
	  
     interschimba(&a[0],&a[i]);
	   atribuiri++;
	 
	 size--;
	 
     MH(a,0,size);

	

  }

}

//---------------------------Functii necesare realizarea QUICK-SORTULUI-ului

void QS(int prim,int ultim,int *a)
{
	int i,j;
	int x,pivot;
	i=prim;
	j=ultim;

	pivot=a[(prim+ultim)/2];atribuiri++;

	do{

		while(a[i]<pivot)
		{i++;
		comparatii++;
		}
		while(a[j]>pivot)
		{
		    comparatii++;
		    j--;}
if(i<=j){
    x=a[i];
    a[i]=a[j];
    a[j]=x;
    i++;
    j--;
    atribuiri++;
    };

	}while(i<=j);
if(prim<j) QS(prim,j,a);
if(i<ultim) QS(i,ultim,a);

}
//Metoda main
int main()
{
   int n,i;
  FILE* pf;
  pf=fopen("mediu.txt","w");
 int sir1[NMAX],sir2[NMAX];
 int a2,c2,a3,c3;


  
 


for(n=100;n<NMAX;n+=100)    //n= dimensiunea unui sir
  {                           //programul returneaza rezultate legate de eficienta algoritmilor de sorta
      a2=0;
	  c2=0;
	  a3=0;
	  c3=0;
	  for(int k=0;k<5;k++)  //contribuie la realizarea unei medieri cu 5
	  {

         creare_sir_m(n,sir1);//Creare Sir
	      copiere(n,sir1,sir2);
      
	      comparatii=0;
	      atribuiri=0;
          HeapSort(sir2,n);
         
          a2=a2+atribuiri;
		  c2=c2+comparatii;

	      comparatii=0;
	      atribuiri=0;
	      copiere(n,sir1,sir2);
          QS(0,n-1,sir2);
	     
          a3=a3+atribuiri;
		  c3=c3+comparatii;
	  }
	  //contribuie la realizarea unei medieri cu 5
	  a2=a2/5;
	  c2=c2/5;
	  a3=a3/5;
	  c3=c3/5;     
       fprintf(pf,"\n%d  \t%d   \t%d   \t%d  \t%d \t%d  \t%d ",n,a2,c2,a2+c2,a3,c3,a3+c3);
       
     }
fclose(pf);

}




   





