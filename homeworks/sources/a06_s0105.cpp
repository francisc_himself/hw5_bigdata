﻿#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<fstream>
#include<conio.h>

#define MAX(x,y) ((x>y)?x:y)   //defines a macro for the max function:  if(x>y) return x; else return y
/*
	-O(nlgn)-time algorithm which outputs the (n,m)-Josephus permutation, for when m is not constant
	-Evaluate the computational effort as the sum of the comparisons and assignments
	-use a balanced, augmented Binary Search Tree, where each node in the tree holds, besides the necessary information, also the size field (i.e. the size of the sub-tree rooted at the node
	-at each step you have to select and delete the k-th element from the tree
	-For a range between 100-10000 the effort(sum of the comparisons and assignments) was evaluated to 1746-305423
*/

long effort; //will hold the number of operations

typedef struct node
{
	int info;	//the key of the node
	int size;   //the size of the tree rooted at the node
	node *parent, *left, *right;
}nodeBST;

//long A[10001];
nodeBST* Tree;

//inorder traversal
void tree_walk(nodeBST *x)
{
	//effort++;
	if(x!=NULL){
		tree_walk(x->left);
		printf("%d(size:%d) ",x->info,x->size);
		tree_walk(x->right);
		}
}

/*
void preorder(nodeBST *x)
{

	if(x!=NULL){
		printf("%d ",x->info);
		preorder(x->left);
		preorder(x->right);
			}
}
*/

//given a BT, return the height of the tree
int height(nodeBST* node)
{
   if (node==NULL)
       return 0;
   else
		return MAX(height(node->left),height(node->right))+1;
} 

//prints a given number of spaces
void print_spaces(int nr)
{
	for(int i=0;i<nr;i++)
		printf("  ");
}


// Print nodes at a given level 
void print_given_level(nodeBST* root, int level)
{
  if(root==NULL)
	 return;
  if(level==1)
		printf("%d(%d)  ",root->info,root->size);
  else if (level>1)
  {
    print_given_level(root->left,level-1);
    print_given_level(root->right,level-1);
  }
}
 
//print tree level by level
void pretty_print(nodeBST* root)
{
  int i,h,nr;
  h=height(root);
  nr=h;
  for(i=1;i<=h; i++){
	printf("\n");
	print_spaces(nr);
	nr=nr-i*i;
	print_given_level(root, i);
	printf("\n");
	}
} 


//start and end : beginning and ending indexes
nodeBST* build_tree(nodeBST* Tree, nodeBST *p, int start, int end)
{	
	//int i=0;
	effort++;
	if(start>end)
		return NULL;
	else 
	{
		nodeBST *T=new nodeBST;
		T->info=(start+end)/2;
		T->parent=p;
		T->left=build_tree(Tree, T,start,(start+end)/2-1);
		T->right=build_tree(Tree, T,(start+end)/2+1,end);
		T->size=end-start+1;
		return T;
	}
}


void decrease_size(nodeBST* node)
{
	while(node!=NULL)
	{
		node->size=node->size-1;
		node=node->parent;			
	}
}

/*
//se poate si asa, si cu celalalt decrease_size
void decreaseSize(nodeBST* node) //mai eficient daca fac decrease size in tree_min si os_select
{
	node->size=node->size-1;
		
}
*/

//Root’s leftmost leaf; O(h)
nodeBST* find_tree_min(nodeBST *x)  //x=root
{
	while(x->left!=NULL) {
		//decreaseSize(x);
		x=x->left;	
		effort=effort+3;
	}
	return x;
}

//succ=min in the right subtree
//succ=lowest level ancestor whose left child is an ancestor as well
nodeBST* find_tree_successor(nodeBST *x)
{
	nodeBST *y=NULL;
	effort++;
	if(x->right!=NULL)
		return find_tree_min(x->right);
	y=x->parent;  //keeps a pointer 1 level above x
	while(y!=NULL && x==y->right)      //as long as we haven’t reached the root and we haven’t changed the direction 
	{
		x=y;
		y=y->parent;
		effort=effort+3;
	}
	return y;
}

//delete a node from the tree
nodeBST* tree_delete(nodeBST* T,nodeBST *z)  //z=new to delete; y is physically deleted
{
	nodeBST *x,*y;
	effort++;
	if(z->left==NULL || z->right==NULL)  //we have already reached z, with a search procedure
		y=z;  //at most 1 child; delete z
	else y=find_tree_successor(z);  //find replacement=max(right)
	
	effort++;
	if(y->left!=NULL){
		//printf("corect");
		x=y->left;    //y=z has no child to the right; x=y’s only child
	}
	else x=y->right;   // y=z’s successor => has no child to the 
						// left; x=y’s only child

	effort++;
	if(x!=NULL) //y is not a leaf; its child redirected to y’s parent
		x->parent=y->parent;

	effort++;
	if(y->parent==NULL)
			T=x;  //y’s child becomes the new root
	else
	{
		if(y==y->parent->left)
			{
				effort++;
				y->parent->left=x;
			}
		else 
			{ 
				effort++;
				y->parent->right=x;
			}
	}

	z->info=y->info; //copy the info from z to y
	//free(z);
	//z=NULL;
	return y;	 //return y
}


//returns a pointer to the node containing the ith smallest key in the subtree parented at x.
nodeBST* OS_SELECT(nodeBST *x, int i)
 {
	int r;
	effort++;
	if(x->left==NULL)
		r=1;
	else 
		r=x->left->size+1;   //x->left->size is the number of nodes that come before x in an inorder tree walk of the subtree rooted at x.
	if(i==r)
		return x;
	else 
			if(i<r)   //the i th smallest element resides in x’s left subtree
			{ 
				effort++;
				//decreaseSize(x);
				return OS_SELECT(x->left,i);
			}
			else 
			{
				effort++;
				//decreaseSize(x);
				return OS_SELECT(x->right,i-r);     //the subtree rooted at x
													//contains r elements that come before x’s right subtree in an inorder tree walk, the
													//i th smallest element in the subtree rooted at x is the i-r th smallest element in
													//the subtree rooted at x->right
			}	 
}


//J(n,m)
void Josephus(int n, int m)
{
	Tree=build_tree(Tree,NULL,1,n);
	printf("Initial tree: ");
	pretty_print(Tree);
	nodeBST* x=new nodeBST;
	int j,k;
	j=1; 
	for(k=n;k>=1;k--){
		j=((j+m-2)%k)+1;
		x=OS_SELECT(Tree,j);
		//printf("\nKey: %d ",j);
		printf("\nDelete: %d ", x->info);
		//printf("\nSuccessor: %d",find_tree_successor(x)->info);
		nodeBST* deleted=tree_delete(Tree,x);
		decrease_size(deleted);
		//free(deleted);
		pretty_print(Tree);
	}	
	free(x);  //deallocate x
	x=NULL;	
}


int main()
{
	int i,m;
	FILE *w;
	w=fopen("josephus_permutation.txt","w");
	effort=0;
/*
	//check if the functions work properly
	nodeBST *tree=NULL;
	nodeBST=build_tree(NULL,1,7);
	ntree_walk(tree);
	printf("\n");
	find_tree_min(tree);
	printf("\nLeftmost leaf: %d",find_tree_min(tree)->info);
	printf("\nSuccessor: %d",find_tree_successor(tree)->info);
	printf("\n");
	nodeBST_delete(tree,tree->right);
	printf("\nOS_SELECT: %d",OS_SELECT(tree,1)->info);
	*/
	
	//check the Josephus function on a small input
	Josephus(7,3);
	printf("\nTotal number of operations: %d",effort);
	/*
	//vary n from 100 to 10000 with step 100; for each n, choose the value of m as n/2
	for(i=100;i<=10000;i=i+100)
	{
		effort=0;
		m=i/2;
		Josephus(i,m);
		fprintf(w,"%d %d\n",i,effort);
	}
	*/

	fclose(w);
	getch();
	return 0;
}


