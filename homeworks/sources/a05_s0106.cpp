#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

#define N 9973

long Table[N];
long verify[3000];
int count=0;
/*
We analize the search procedure for a number of 3000 elements from which 1500 are elemnts from the table and the rest elements that are not mandatory from 
the table.We analize this for different filling factor {0.8,0.85,0.9,0.95,0.99}
As we can see from the results as the filling factor increases the number of searches increase too.
In average case we can see that the non found elements have a bigger overall number of searches than for found elements as it should be.
In max case the priciple is kept and the values also difer for found elements and not found ones.

*/
void initialize ()
{
    int i;
    for (i=0;i<N;i++)
    {

        Table[i]=-1;
    }
}

int h_funct (long k,int i)
{
    return (k%N + i + i*i)%N;
}

int hash_insert (long k)
{
	int i=0;
	int j;
	do
	{j=h_funct(k,i);

		if(Table[j]==-1)
		{
			//count++;
			Table[j]=k;
		return j;
		}
		else
			i++;
	}
	while (i<N);
	return -1;
}
int hash_search(long k)
{int i=0,j;
do{
	j=h_funct(k,i);
	if(Table[j]==k)
		{	
			count++;
			//printf("element found at poz %d\n",j);
			return j;
	
		}
	count++;
	i=i+1;

}
while(Table[j]!=-1 && i<=N);
return -1;
}


int main()
{
	int i,j,nr=0,max,countint,x,countavgf,maxavgf,countavgnf,maxavgnf;
	long key;
	float fill[]={0.8, 0.85 , 0.9, 0.95, 0.99};

	srand(time(NULL));

	FILE *f,*t;

	f=fopen("hash.txt","w");
	t=fopen("hashtest.txt","w");

	/*
	initialize();
	for(int i=1; i<10; i++)
		  hash_insert(rand()%9);
	

	for(int i=0;i<10;i++)
			printf("%d\n",Table[i]);

	for(int i=0;i<10 ;i++)
			hash_search(i);
			*/

for(int k=0;k<5;k++)
{	
	countavgf=0;
	maxavgf=0;
	countavgnf=0;
	maxavgnf=0;
	fprintf(f,"Values for filling factor %f: \n",fill[k]);
	for(j=0;j<5;j++)
	{
	initialize();
	for (i=1; i<= (fill[k] * N) ; i++)
	{

		//generate key
		key = rand()*10000+rand();
		hash_insert(key);

		//generate verify array for search
	}

    nr=1;

    while (nr<=1500)
    {
        x=rand()%N;
        if (Table[x]!=-1)
            {
                verify[nr]=Table[x];
                nr++;
            }
    }

    for (i=1501;i<=3000;i++)
       verify[i]=rand()*10000+rand();

	countint=0;max=0;

	for (i=1;i<=1500;i++)
	{
		count=0;
		//hash_search(verify[i]);
		fprintf(t,"element found at poz %d\n",hash_search(verify[i]));
		countint+=count;
		if (count>max)
			max=count;
	}

	countavgf+=countint/1500;
	maxavgf+=max;
	countint=0;max=0;
	fprintf(t,"----------\n\n\n\n\n\n");
	for (i=1501;i<=3000;i++)
	{	count=0;
		//hash_search(verify[i]);
		fprintf(t,"element found at poz %d\n",hash_search(verify[i]));
		countint+=count;
		if (count>max)
			max=count;
	}
	countavgnf+=countint/1500;
	maxavgnf+=max;
	}
	
	fprintf(f,"avg found: %d\nmax found: %d\n", countavgf/5,maxavgf/5);
	fprintf(f,"avg  not found: %d\nmax not found: %d\n\n\n\n", countavgnf/5,maxavgnf/5);
}
fclose(f);
//getch();
return 0;
}
