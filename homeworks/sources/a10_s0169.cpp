#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <queue>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;
#define v_max 201
#define e_max 9001

typedef struct node{
	int key;
	node *next;
}node;

typedef struct{
	int color;
	int parent;
	int d;
	int f;
}tnode;
tnode *g[v_max];
node *adj[v_max];
int edges[e_max], profilervar;
int n,m,timp,c,t;   

void generate(int n,int m){
	memset(adj,0,n*sizeof(node*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		node *nod=new node;
		nod->key=b;
		nod->next=adj[a];
		while(a==nod->key)
			a=rand()%n;
		adj[a]=nod;
	}
}
void print_adj(node * a[])
{
	for(int i=0;i<n;i++)
	{
		node*aux=a[i];
		printf("%d",i);
		while(aux!=NULL)
		{
			printf("->%d",aux->key);
			aux=aux->next;
		}
		printf("\n");
	}
}
void visit(int u)
{
	timp++;
	g[u]->d=timp;
	g[u]->color=1;
	profiler.countOperation("operations",c,2);
	node* aux;
	aux=adj[u];
	while(aux!=NULL){
		profiler.countOperation("operations",c,2);
		int v=aux->key;
		if(g[v]->color==0){  
			g[v]->parent=u;
			profiler.countOperation("operations",c);
			if(t==1)
				//muchie de arbore deoarece varful q a fost descoperit explorand muchia (v,q); culoare varf = alb
				printf("%d	%d	%s\n",aux->key,u,"muchie arbore");
			//printf("vizitez  %d,%d,%d\n",v,g[v]->parent,g[u]->color);
			visit(v);
		}
		else if(t==1){
			profiler.countOperation("operations",c);
			if(g[v]->color==1)
				//muchie inapoi deoarece varful q este un "stramos" a lui v; culoare varf = gri
				printf("%d	%d	%s\n",u,aux->key,"muchie inapoi");

			else{
				profiler.countOperation("operations",c);
				if(g[v]->d<g[aux->key]->d)
					//muchie inainte conecteaza varful v cu un descendent q, dar nu este muchie arbore; culoare varf = negru
					printf("%d	%d	%s\n",u,aux->key,"muchie inainte");
				else
					//unesc nodurile v si q,dar cu conditia ca unul sa nu fie stramosul celuilat; culoare varf = negru
					printf("%d  %d	%s\n",u,aux->key,"muchie transversala");
			}
		}
//}
	timp++;
	profiler.countOperation("operations",c);
	g[u]->color=2;
	g[u]->f=timp;
	aux=aux->next;
	}
}
void dfs()
{
	for(int u=0;u<n;u++){
		g[u]=new tnode;
		g[u]->color=0;
		g[u]->parent=NULL;
		profiler.countOperation("operations",c,2);
	}
	timp=0;
	for(int u=0;u<n;u++){
		profiler.countOperation("operations",c);
		if(g[u]->color==0)
			//printf("vizitez  %d,%d,%d\n",u,g[u]->parent,g[u]->color);
			visit(u);
		printf("nod %d, disc_t %d,finish %d,parent %d,color %d\n",u,g[u]->d,g[u]->f,g[u]->parent,g[u]->color);

	}
}
void test()
{
	n=5;m=10;
	t=1;
	generate(n,m);
	print_adj(adj);
	dfs();
}
void varyv()
{
	c=m=9000;t=0;
	for(n=100;n<=200;n+=10)
	{
		c=n;
		printf("n = %d\n",n);
		generate(n,m);
		dfs();
	}
	profiler.createGroup("vary the number of edges","operations");
	profiler.showReport();
}
void varye()
{
	n=100;t=0;
	for(m=1000;m<=5000; m+=100)
	{	c=m;
		printf("m= %d\n",m);
		generate(n,m);
		dfs();
	}
	profiler.createGroup("vary the number of vertices","operations");
	profiler.showReport();
}
void main()
{
	int op;
	do
	{
		printf("\n\n1)test\n2)vary the number of nodes\n3)vary the number of edges\n4)quit");
		printf("\nenter your choice: ");
		scanf_s("%d",&op);
		switch(op)
		{ 
		case 1 :test();break;
		case 2: varye();break;
		case 3:varyv();break;
		default:break;
		}
	}while(op!=4);
}
