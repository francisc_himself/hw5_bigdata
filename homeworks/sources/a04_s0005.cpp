#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<iostream>
/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Interclasarea a k liste
*/

typedef struct nod {
	int info;
	nod* next;
}NOD;

typedef struct element {
	int poz;
	int elem;
}ELEMENT;

struct liste {
	NOD*prim;
	NOD*ultim;
};
struct liste CAP[501];

ELEMENT Heap[10001];
NOD *interprim, *interultim;
long asignari=0, comparari=0;


void adauga(NOD **prim, NOD **ultim, int val)	//Functia de adaugare a unui nod intr-o lista
{
	if((*prim)==NULL)
	{
		(*prim)= new NOD;
		(*prim)->info=val;
		(*prim)->next=NULL;
		*ultim=(*prim);
	}
	else
	{
		NOD *elem=new NOD;
		elem->info=val;
		elem->next=NULL;
		(*ultim)->next=elem;
		(*ultim)=elem;
	}
}

int p(int i) {				
	return i/2;
}

int st(int i) {				
	return 2*i;
}

int dr(int i) {				
	return 2*i+1;
}


void reconstructie_heap(ELEMENT Heap[], int i, int size) {	//Functia de verificare a heap-ului
	int minim;									
	ELEMENT aux;
	minim=i;
	if(st(i)<=size && Heap[st(i)].elem  <Heap[i].elem)
		minim=st(i);
	else
		minim=i;
	if(dr(i)<=size && Heap[dr(i)].elem < Heap[minim].elem)
		minim=dr(i);
	comparari+=2;
	if(minim!=i) {
		aux=Heap[i];
		Heap[i]=Heap[minim];
		Heap[minim]=aux;
		asignari+=3;
		reconstructie_heap(Heap, minim, size);
	}
}

ELEMENT extragere_heap(ELEMENT Heap[], int *size) {		//Functia imi scoate primul element din heap(minimul), iar apoi apeleaza reconstructie_heap pentru a reface structura
	ELEMENT x;
	if((*size)>0) {
		x=Heap[1];
		Heap[1]=Heap[(*size)];
		asignari+=2;
		(*size)--;
		reconstructie_heap(Heap,1,(*size));
	}
	return x;
}

void inserare_heap(ELEMENT Heap[], ELEMENT x, int *size) {		//Functia imi adauga un element in heap(pe ultima pozitie) si il muta in sus pe ramura pana cand este refacuta structura de heap
	(*size)++;
	Heap[(*size)]=x;
	int i=(*size);
	ELEMENT aux;
	comparari++;
	while(i>1 && (Heap[i].elem)<(Heap[p(i)].elem))
	{
		comparari++;
		aux=Heap[i];
		Heap[i]=Heap[p(i)];
		Heap[p(i)]=aux;
		asignari+=3;
		i=p(i);
	}
}

void creare_k_liste(struct liste CAP[], int k, int n) {		//Functia care imi genereaza cele k liste ordonate.
	int nr=0, i, j, l;
	for(l=1; l<=k;l++)
		CAP[l].prim=CAP[l].ultim=NULL;
	
	for(i=1;i<=k;i++) {
		for(j=1;j<=n/k;j++)
		{
			nr=nr+rand()%100;
			adauga(&CAP[i].prim, &CAP[i].ultim, nr);
		}
		nr=0;
	}
	
}

void listare(NOD *prim, NOD *ultim) {			//Functia imi afiseaza continutul unei liste.
	NOD *c;
	c=prim;
	while(c!=0) {
		printf("%d ", c->info);
		c=c->next;
	}
	printf("\n");
}

void pretty_print(ELEMENT Heap[], int i,int k, int nivel)			//Functie de pretty_print pentru Heap
{
    if(i<=k)
    {

        pretty_print(Heap, 2*i+1,k, nivel+1);
        for(int j=1; j<=nivel; j++)
            printf("\t");
        printf("%d\n", Heap[i].elem);
        pretty_print(Heap,2*i,k,nivel+1);
    }

}

void interclasare(struct liste CAP[], int k) {				//Functia de interclasare a listelor.
	int size=0, i, poz;
	ELEMENT v;
	for(i=1;i<=k;i++) {					//se pune in heap primul element din fiecare cele k liste.
		v.poz=i;			
		v.elem=(CAP[i].prim)->info;
		inserare_heap(Heap, v, &size);
	}
	//pretty_print(Heap, 1, size, 0);
	//printf("\n\n");
	ELEMENT ***ANONIM***;
	while(size>0) {								//se extrage minimul din heap si il adauga in lista mare, dupa care urmatorul element din lista din care a fost extras minimul
		***ANONIM***=extragere_heap(Heap, &size);		//este introdus in heap. 
		//pretty_print(Heap, 1, size, 0);
		//printf("\n\n");
		poz=***ANONIM***.poz;
		adauga(&interprim, &interultim, ***ANONIM***.elem);
		CAP[poz].prim=CAP[poz].prim->next;
		if(CAP[poz].prim!=NULL) {
			v.poz=***ANONIM***.poz;
			v.elem=CAP[***ANONIM***.poz].prim->info;
			inserare_heap(Heap, v, &size);
		}
	}
}

void drop_all(int k)			//Functia care imi elibereaza memoria.
{
    NOD *p;
    for(int i=1; i<=k; i++)
    {
        while ( CAP[i].prim!=0)
        {
            p=CAP[i].prim;
            CAP[i].prim=CAP[i].prim->next;
            free(p);
        }
        free(CAP[i].prim);
        free(CAP[i].ultim);
    }

    while (interprim!=NULL)
    {
        p = interprim;
        interprim = interprim->next;
        free(p);
    }
    free(interprim);
}


void main () {
	int i, k, n;
	srand(time(NULL));
	/*
	printf("dati numarul de liste: ");
	scanf("%d", &k);
	printf("dati n: ");
	scanf("%d", &n);
	for(i=1;i<=k;i++)
		drop_all(k);
	creare_k_liste(CAP, k, n);
	for(i=1;i<=k;i++) {
		printf("Lista %d\n", i);
		listare(CAP[i].prim, CAP[i].ultim);
	}

	interclasare(CAP, k);
	listare(interprim, interultim);
	

	*/
	
	FILE *f1, *f2, *f3, *f4;
	f1=fopen("simulare_k=5.csv", "w");
	f2=fopen("simulare_k=10.csv", "w");
	f3=fopen("simulare_k=100.csv", "w");
	f4=fopen("simulare_k_variabil.csv", "w");

	k=5;
	drop_all(k);
	fprintf(f1, "n,A,C,A+C\n");
	for(i=100;i<=10000;i+=100) {
		creare_k_liste(CAP, k, i);
		asignari=0;
		comparari=0;
		interclasare(CAP, k);
		fprintf(f1, "%d,%d,%d,%d\n", i, asignari, comparari, asignari+comparari);
	}


	k=10;
	drop_all(k);
	fprintf(f2, "n,A,C,A+C\n");
	for(i=100;i<=10000;i+=100) {
		creare_k_liste(CAP, k, i);
		asignari=0;
		comparari=0;
		interclasare(CAP, k);
		fprintf(f2, "%d,%d,%d,%d\n", i, asignari, comparari, asignari+comparari);
	}

	k=100;
	drop_all(k);
	fprintf(f3, "n,A,C,A+C\n");
	for(i=100;i<=10000;i+=100) {
		creare_k_liste(CAP, k, i);
		asignari=0;
		comparari=0;
		interclasare(CAP, k);
		fprintf(f3, "%d,%d,%d,%d\n", i, asignari, comparari, asignari+comparari);
	}

	n=10000;
	fprintf(f4, "k,A,C,A+C\n");
	for(i=10;i<=500;i+=10) {
		drop_all(i);
		creare_k_liste(CAP, i, n);
		asignari=0;
		comparari=0;
		interclasare(CAP,i);
		fprintf(f4, "%d,%d,%d,%d\n", i, asignari, comparari, asignari+comparari);
	}

	fclose(f1);
	fclose(f2);
	fclose(f3);
	fclose(f4);
	
}
