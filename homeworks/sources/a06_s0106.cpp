#include <stdio.h>
#include <stdlib.h>
#include <conio.h>


/*

	-O(nlgn)-time algorithm which outputs the (n,m)-Josephus permutation, for when m is not constant
	-Evaluate the computational effort as the sum of the comparisons and assignments
	-use a balanced, augmented Binary Search Tree
	-Each node in the tree holds, besides the necessary information, also the size field (i.e. the size of the sub-tree rooted at the node)
	-After we completed the test we can see that the graph has a grouth of nlogn
*/
typedef struct NodeTree
{
    int value;
    int size;
    struct NodeTree *left;
    struct NodeTree *right;
    struct NodeTree *parent;
}NodeT;

NodeT *Tree;
int operations = 0;

//create a PBT
NodeT* Built_PBT (int start, int end, NodeT *parent)
{
	if (start <= end)
	{
		NodeT *p = new NodeT;

		operations++;	
		p->value = (start + end) / 2;
		p->size = end - start + 1;//put the size of the node wich is the number of nodes in the subtree including the current node
		p->left = Built_PBT(start, (start + end) / 2 - 1, p);
		p->parent = parent;
		p->right = Built_PBT((start + end) / 2 + 1, end, p);
		return p;
	}
	else
		return NULL;
}

//ith smallest value in the subtree rooted at p
NodeT* OS_SELECT (NodeT *x, int i)
{
	int r;
	
	operations++;
	if (x->left != NULL)//if it is not a root node
		r = x->left->size + 1;
	else
		r = 1;

	if (i == r)//if the node is the ith serched
		return x;
	else
	{
		operations++;
		x->size = x->size - 1;//decrement size
		if (i < r)
		{
			return OS_SELECT(x->left, i);//the node is in the left
		}
		else
		{
			return OS_SELECT(x->right, i - r);//node is in the right

		}
	}
}

//needed for deletion
NodeT* find_tree_min (NodeT* x)
{
	while (x->left != NULL)//finds the min value along the branch
	{
		operations+=3;
		x->size = x->size - 1;
		x = x->left;
	}
	return x;
}

//needed for deletion
NodeT* find_tree_succesor (NodeT* x)
{
	operations++;
	if (x->right != NULL)
		return find_tree_min(x);//succesor for a non leaf node

	NodeT *y = x->parent;
	while (y != NULL && x == y->right)//succesor for a leaf node
	{
		operations+=3;
		x = y;
		y = x->parent;
	}
	return x;
}

//delete a node from the tree = algorithm for Red-Black Trees
NodeT* OS_DELETE (NodeT *p, NodeT *c)
{
	NodeT *y,*x;

	operations++;
	if (c->left == NULL || c->right == NULL)
		y = c;
	else
		y = find_tree_succesor(c);

	operations++;
	if (y->left != NULL)
	{
		x = y->left;
	}
	else
	{
		x = y->right;
	}


	operations++;
	if (x != NULL)
		x->parent = y->parent;
	
	operations++;
	if (y->parent == NULL)
	{
		Tree = x;
	}
	else
	{
		operations++;
		if (y == y->parent->left)
			y->parent->left = x;
		else
			y->parent->right = x;
	}

	operations++;
	if (y != c)
	{
		c->value = y->value;
	}
	return y;
}

   void pretty_print (NodeT* p, int start)
    {
		if(p!=NULL)
		{
        start++;
        if (p->right != NULL)
        {
            pretty_print(p->right,start);
        }
        for (int i = 1; i < start; i++)
        {
            printf("     ");
        } 
         printf("%d \n",p->value);
        if (p->left != NULL)
        {
            pretty_print(p->left, start);
        }
    }
   }
//Josephus algorithm
void josephus (int n, int m)
{
	int c = 1;
	int i;
	NodeT *node;
	//pretty_print (Tree, 0);
	for(i=n;i>=1;i--){
		c=((c+m-2)%i)+1;

		node=OS_SELECT(Tree,c);
		printf("Deleted node: %d \n \n", node->value);
		OS_DELETE(Tree, node);
		//pretty_print (Tree, 0);
		printf("\n \n");
		node=NULL;
	}

}

int main()
{
	FILE *f;
	f=fopen("josephus.txt","w");
	Tree = Built_PBT(1, 7, NULL);
	//josephus(7, 3);
	for (int i=100;i<=10000;i=i+100)
	{
		operations=0;
		Tree=Built_PBT(1,i,NULL);
		josephus(i,i/3);
		fprintf(f,"%d %d\n",i,operations);
	}
	fclose(f);
	getch();
	return 0;
}
