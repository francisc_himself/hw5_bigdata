//***ANONIM*** ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"

#define HASH_SIZE 9973
#define SEARCH_SIZE 3000

Profiler profiler("Lab6");

int samples[HASH_SIZE + SEARCH_SIZE /2 ];
int indices [SEARCH_SIZE /2 +1];
int i,j,k,T[HASH_SIZE];
float alfa[6]={0, 0.8, 0.85, 0.9, 0.95, 0.99};
int nr_cautari,i2,amGasit=0;
double gasit,nu_gasit,gasitMax=0,nu_gasitMax=0;


int h(int k, int i)
{
	int x;
	x=(k+i+2*i*i)% HASH_SIZE;
	
	return x;
}

int HASH_INSERT(int T[],int k)
{
	i=0;
	do
	{
		j=h(k,i);
		if(T[j]==0)
		{
			T[j]=k;
			return j;
		}
		else i=i+1;
	}while (i==HASH_SIZE);
}

int HASH_SEARCH(int T[],int k)
{
	i=0;
	do
	{
		j=h(k,i);
		nr_cautari++;
		if (T[j]==k)
		{
			amGasit=1;
			return j;
		}
		i=i+1;
	}while((T[j]!=0)&&(i<HASH_SIZE));
	return -1;
}
void main()
{
	float n;
	memset(T,256,HASH_SIZE*sizeof(int));//punem toate elementele din vector= 0
		
		//pentru un numar mic de elemente:

		
		HASH_INSERT(T,169456);
		HASH_INSERT(T,162346);
		HASH_INSERT(T,7437);
		HASH_INSERT(T,743722);

		//pentru elementele care nu se gasesc:
			amGasit=0;
			HASH_SEARCH(T,568);
	
			if(amGasit==1) printf("am gasit numarul \n ");//sa afisez j si daca e pozitiv sau nu
			else  printf("nu am gasit numarul \n");

		//pentru elementele care se gasesc:		
			amGasit=0;
			HASH_SEARCH(T,7437);
			
			if(amGasit==1) printf("am gasit numarul \n");
			else  printf("nu am gasit numarul \n");
				
	
		
		FILE * f=fopen("fisier.csv","w");
		fprintf (f,"Factor umplere,Numar mediu (gasite),Numar maxim (gasite),Numar mediu (nu se gasesc),Numar maxim (nu se gasesc) \n");

		for (i2=1;i2<=5;i2++)
		{
			n=alfa[i2]*HASH_SIZE;
			FillRandomArray(samples,n+(SEARCH_SIZE/2),1,1000000,true,0);//generare elem unice
			for(k=0;k<n;k++)
			{
				HASH_INSERT(T,samples[k]);
			}

		//pentru elementele care nu se gasesc:
		nu_gasit=0;
		for(k=n;k<=(n+(SEARCH_SIZE/2)-1);k++)
		{
			nr_cautari=0;
			HASH_SEARCH(T,samples[k]);
			nu_gasit=nu_gasit+nr_cautari;
			if(nr_cautari>nu_gasitMax)
			{
				nu_gasitMax=nr_cautari;
			}
		}

		//pentru elementele care se gasesc:
		FillRandomArray(indices,(SEARCH_SIZE/2)-1,0,(int)n-1,true,0);
		for(k=0;k<=((SEARCH_SIZE/2)-1);k++)
		{
			nr_cautari=0;
			HASH_SEARCH(T,samples[indices[k]]);
			gasit=gasit+nr_cautari;
			if(nr_cautari>gasitMax)
			{
				gasitMax=nr_cautari;
			}
		}

		gasit=gasit/(SEARCH_SIZE/2);
		nu_gasit=nu_gasit/(SEARCH_SIZE/2);
		fprintf(f,"%f, %f, %f, %f, %f, \n",alfa[i2],gasit,gasitMax,nu_gasit,nu_gasitMax);
	}
	
	fclose(f);

	
}