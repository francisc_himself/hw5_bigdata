/*
***ANONIM*** ***ANONIM***
cele 2 metode de constructie au complexitatea O(n log n), in cazut favorabil bottom-up are complexitatea O(n)
*/
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include<cstdlib>
#include<math.h>


int n,lung=0;
int a[10000];//valorile aleatoare
int b[10000]; // Td
int c[10000]; //Bu
int nrcompbu=0, nrcomptd=0, nratrbu=0, nratrtd=0;

void citire(int v[])
{
	n=10;
	for(int i=1;i<=10;i++)
	{
		printf("dati v[%d] ",i);
		scanf("%d",& v[i]);
	}
	
}
void afisare_heap(int v[])
{
	printf("\t\t\t%d\n\t\t%d\t\t%d\n",v[1],v[2],v[3]);
	printf("\t%d\t%d\t\t%d\t%d\n",v[4],v[5],v[6],v[7]);
	printf("%d\t%d\t%d\n",v[8],v[9],v[10]);
}
void afisare(int v[]) {
  for (int i = 1; i <= n; i++) 
  {
    printf("%d - ",v[i]);
  }
  printf("\n");
}

//constructie heap bottom up
int left(int i) {
  return (2 * i) ;//constructie fiu stanga
}

int right(int i) {
  return (2 * i) + 1;//constructie fiu dreapta
}

void reconstructie(int a[], int i) {
  int ok;
  int l = left(i);
  int r = right(i);
  nrcompbu++;
  if (  (l <= lung) && (a[l] < a[i])) {
    ok = l;// este maxim
  }
  else {
    ok = i;
  }
  nrcompbu++;
  if ( (r <= lung) && (a[r] < a[ok])) {
    ok = r;
  }
  if (ok != i) {
	  nratrbu+=3;
    int aux = a[i];
    a[i] = a[ok];
    a[ok] = aux;
	reconstructie(a, ok);
  }
}

void HeapBU(int a[]){
	int i;
    lung=n;
	//nratrbu=0;nrcompbu=0;
	for (i=n/2; i > 0;i--) 
		reconstructie(a, i); 
	
	
}

//build heap top down

int parent(int i) {
	return (i / 2);//constructie parinte
}


void H_Push(int a[],int x){ //o(log n)
	lung++;
	nratrtd++;
	a[lung]=x;
	int i=lung;
	nrcomptd++;
	while(i>1 && a[parent(i)]>a[i])
	{	nrcomptd++;
		nratrtd+=3;
		int aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}

}

void H_Init(int a[]){
	n=0;
}

void HeapTD(int b[] ,int a[]){
	lung=0;
	//nrcomptd=0;nratrtd=0;
	for(int i=1;i<=n;i++){ 
		H_Push(b,a[i]);
		//print(b);
	}
	
	
}
void generare_aleator(){
	 FILE *f=fopen("fisier.csv","w");
	for(n=100;n<=10000;n=n+200){
		nrcompbu=0;
		nratrbu=0;
	    nrcomptd=0;
		nratrtd=0;
		for(int j=1;j<=5;j++)
		{	
			srand (time(NULL));
			for(int i=1;i<=n;i++){				
				a[i]=rand();
				c[i]=a[i];
			}
			srand(1);
			HeapBU(c);
			HeapTD(b,a);
		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d \n",n,nratrbu/5,nrcompbu/5,(nratrbu+nrcompbu)/5 , nratrtd/5,nrcomptd/5,(nratrtd+nrcomptd)/5 );
	}

}

void main(){
	citire(a);
	afisare_heap(a);
	HeapTD(b,a);
	afisare_heap(b);
	HeapBU(a);
	afisare_heap(a);
	//generare_aleator();
	getch();

	
	
}
