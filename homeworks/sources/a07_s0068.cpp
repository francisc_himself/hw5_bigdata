/***************************
Assign7 ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Transformarea T1 contine radacina arborelui multiway si restul nodurilor contin copii arborelui
Transformarea T2 pentru constructia structurii de date cu child si brother este facuta concret pe exemplul dat in lucrare.s
****************************/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>


typedef struct tip_nod{
			       int key;
				   int nrChild;
		   	       struct tip_nod  *children[10];
			     } NOD;
typedef struct tip_nod2{
					int key;
					struct tip_nod2 *child,*brother;
				} NOD2;
NOD *rad,*nod2[10],*rad2;
int n=10;
NOD2 *rad3,*rad31,*rad32,*rad33;							

void preordine(NOD *p,int nivel)
{
    int i;

    if (p!=0)
    {
        for(i=0;i<nivel;i++)
			printf(" ");
        printf("%d\n",p->key);
	//	preordine(p->stg,nivel+1);
      //  preordine(p->dr,nivel+1);
    }
}
void init_t2()
{
	for (int i=0;i<n;i++)
	{
		nod2[i]=(NOD*)malloc(sizeof(NOD));
		nod2[i]->nrChild=0;
		nod2[i]->key=0;
	}
}
void t2(int a[10])
{
	for(int i=1;i<n;i++)
	{
		if(a[i]==-1)
		{
			nod2[i]->key=i;
			rad=(NOD*)malloc(sizeof(NOD));
			rad=nod2[i];
		}
		else
		{
		nod2[a[i]]->key=a[i];
		nod2[a[i]]->nrChild++;
		nod2[a[i]]->children[nod2[a[i]]->nrChild]=(NOD*)malloc(sizeof(NOD));
		nod2[a[i]]->children[nod2[a[i]]->nrChild]->key=i;
		printf("Nodul %d are %d copii si are copilul %d\n",a[i],nod2[a[i]]->nrChild,nod2[a[i]]->children[nod2[a[i]]->nrChild]->key);
		}
	}
}

void t3_version1_2()
{
	rad3=(NOD2*)malloc(sizeof(NOD2));
	rad2=(NOD*)malloc(sizeof(NOD));
	rad2=rad;
	rad3->key=rad->key;
	rad3->brother=NULL;
	rad3->child=NULL;
	
	rad32=(NOD2*)malloc(sizeof(NOD2));
	//rad3->child=(NOD2*)malloc(sizeof(NOD2));
	rad32=rad3;
	for(int i=1;i<4;i++)
	{
		
		//rad32->brother=(NOD2*)malloc(sizeof(NOD2));
		if(rad2->nrChild!=0)
		{
		
			
			rad32->child=(NOD2*)malloc(sizeof(NOD2));
			
			for(int j=1;j<=rad2->nrChild;j++)
			{
				
				if(j==1)
				{
					rad31=rad32->child;
					rad31->key=rad2->children[j]->key;
					rad32=rad32->child;
					
				}
				else
				{
					rad31->brother=(NOD2*)malloc(sizeof(NOD2));
					rad31->brother->key=rad2->children[j]->key;
					if((i==1|| i==3)&&j==3)
					{
						rad33=rad31;
					}
					rad31=rad31->brother;
				}
			}
		}
		rad2=nod2[rad->children[i]->key];
		if(i==2)
			rad32=rad33;
		//rad32=rad32->brother;
	}
	
}
		
void prettyPrintT2()
{
	printf("%d\n",rad3->key);
	printf("\t%d\n",rad3->child->key);
	printf("\t\t%d\n",rad3->child->child->key);
	printf("\t\t%d\n",rad3->child->child->brother->key);
	printf("\t\t%d\n",rad3->child->child->brother->brother->key);
	printf("\t%d\n",rad3->child->brother->key);
	printf("\t\t%d\n",rad3->child->brother->child->key);
	printf("\t\t%d\n",rad3->child->brother->child->brother->key);
	printf("\t%d %p\n",rad3->child->brother->brother->key,rad3->child->brother->brother->brother);

}
void prettyPrintT3(NOD2 *p,int nivel)
{
    

    if (p!=NULL)
    {
        for(int i=0;i<nivel;i++)
			printf(" ");
        printf("%d\n",p->key);
		prettyPrintT3(p->child,nivel+1);
        prettyPrintT3(p->brother,nivel+1);
    }
}
void main()
{
	
	int a[10];
	a[1]=2;
	a[2]=7;
	a[3]=5;
	a[4]=2;
	a[5]=7;
	a[6]=7;
	a[7]=-1;
	a[8]=5;
	a[9]=2;
	init_t2();
	t2(a);
	t3_version1_2();
	prettyPrintT2();
	prettyPrintT3(rad3,0);


}