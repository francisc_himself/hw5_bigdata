
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>
/*HeapSort este mai slab (dar nu cu mult) decat QuickSort, dar are marele avantaj fata de acesta ca nu este recursiv. 
Algoritmii recursivi consuma o mare cantitate de memorie.
HeapSort are eficienta O(nlog n)
QuickSort are tot O(n log n) dar pe cazul worst are O(n^2)*/

//---------------------------declaratiiglobale
long heapComp;
long heapAtrib;
long quickComp;
long quickAtrib;
long dimHeap;
//---------------------------fin declaratiiglobale

//---------------------------creare de siruri
//generare de numere random
void genRandom(long a[], int inc)
{
	for(int i=0;i<inc;i++)
		a[i]=rand();
}

//generare numere random crescator
void genRandomCresc(long a[], int inc)
{
	a[0]=1;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]+rand();
}

//generare numere random descrescator
void genRandomDescresc(long a[], int inc)
{
	a[0]=LONG_MAX;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]-rand();
}//---------------------------------------------------------------------------


//--------------------------copiere sir
void copiereSir(long b[], long a[], int n)
{
	for(int i=0;i<n;i++)
		b[i]=a[i];
}
//----------------------------

//---------------------------heapify-ul
void heapify(long a[], int i)  
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    heapComp=heapComp+1;
    if (left<=dimHeap && a[left]>a[i]) 
        largest=left;	
    else
        largest=i;

    heapComp=heapComp+1;
    if (right<=dimHeap && a[right]>a[largest])
        largest=right;
    if (largest!=i) 
        {
            heapAtrib+=3;
            aux=a[i];
            a[i]=a[largest];
            a[largest]=aux;
            heapify(a,largest);  
        }
}

//----------------------------cpnstructie bottomup la heam
void bh_bottomup(long A[],int n)
{
    int i;
	dimHeap=n;
    for (i=dimHeap/2;i>=1;i--) 
        heapify(A,i); 
}

//----------------------------------heapsort
void heapSort(long a[],int n)
{
    int k;
    long aux;
    bh_bottomup(a,n);
    for (k=n;k>=2;k--)
    {
        heapAtrib+=3;
        aux=a[1];
        a[1]=a[k];
        a[k]=aux;
        dimHeap=dimHeap-1;
        heapify(a,1);
    }
}


//----------------------------------------partitionarea
int partition(long a[],int p, int r,int pivot){

	long x,aux;
	int i,j;
	if(pivot==1)
		x=a[r];
	else if(pivot==2)
		x=a[(p+r)/2];
	else if(pivot==3)
		x=a[p+1];
	quickAtrib++;
	i=p-1;
	for(j=p;j<=r-1;j++){
		quickComp++;
		if(a[j]<=x)
			{i++;
			quickAtrib+=3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	quickAtrib+=3;
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;

	return i+1;
}


//--------------------------------------------quicksort-ul
void quickSort(long a[],int p, int r,int pivot)
{	if ( p < r )
    {
        int q = partition(a, p, r,pivot); 
			quickSort(a, p, q-1,pivot);
			quickSort(a, q+1, r,pivot);
    }
}


//---------------------------------------------main-ul
int main()
{
	srand(time(NULL));
    long heap[10001];
    long quick[10001];
    int j,k;
    long heapCompeap=0,quickCompuick=0,heapAtribeap=0,quickAtribuick=0;

	//------------declaram sideschidem fisierele
    FILE *fis,*average,*best,*worst;

    fis=fopen("average.csv","w");
	worst=fopen("quicksort_w.csv","w");
	average=fopen("quicksort_a.csv","w");
	best=fopen("quicksort_b.csv","w");
	//------------final deschidere fisier

	
	//cazul mediu statistic
	//luam lungimea sirului cu increment de 100
    for (int i=100;i<=10000;i=i+100)
	{

		//initializari
		heapComp=0;
		quickComp=0;
		heapAtrib=0;
		quickAtrib=0;

		//luam cate 5 cazuri
		for (j=1;j<=5;j++)
		{

			heapCompeap=0;
			quickCompuick=0;
			heapAtribeap=0;
			quickAtribuick=0;

			//copiem sirul ca sa fie identice pentru ambele metode de sortare
			genRandom(heap,i);
			copiereSir(quick, heap, i);


			//apelam  sortarile
			heapSort(heap,i);
			quickSort(quick,1,i,1);

			heapCompeap+=heapComp;
			quickCompuick+=quickComp;
			heapAtribeap+=heapAtrib;
			quickAtribuick+=quickAtrib;
		}
			quickCompuick = quickCompuick/5;
			quickAtribuick = quickAtribuick/5;
			heapCompeap = heapCompeap/5;
			heapAtribeap = heapAtribeap/5;

			//scriem in fisier cazul favorabil
			fprintf(average,"%d,%ld,%ld,%ld\n",i,quickCompuick,quickAtribuick,quickCompuick+quickAtribuick);
			fprintf(fis,"%d,%ld,%ld,%ld,%ld,%ld,%ld\n",i,heapCompeap,heapAtribeap,heapCompeap+heapAtribeap,quickCompuick,quickAtribuick,quickCompuick+quickAtribuick);
	}

	//cazul favorabil
	for (int i=100;i<=10000;i=i+100)
	{
		quickComp=0;
		quickAtrib=0;

		//generam sirul crescator
		 genRandomCresc(quick,i);
		
		 //sortam
		quickSort(quick,1,i,2);
		fprintf(best,"%d,%ld,%ld,%ld\n",i,quickComp,quickAtrib,quickComp+quickAtrib);
		
	}

	
	//caz defavorabil
	for (int i=100;i<=5000;i=i+100)
	{
		quickComp=0;
		quickAtrib=0;

		//generam sirul
		genRandomDescresc(quick, i);

		//sortam
		quickSort(quick,1,i,3);
		fprintf(worst,"%d,%ld,%ld,%ld\n",i,quickComp,quickAtrib,quickComp+quickAtrib);
		
	}
	
	
	fclose(fis);
	fclose(average);
	fclose(best);
	fclose(worst);
	//getch();
	return 0;

	
}
