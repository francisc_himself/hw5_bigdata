/*
* Student: ***ANONIM*** ***ANONIM*** ***ANONIM***
* Grupa: ***GROUP_NUMBER***
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>  

////variabile globale///
int n,k;
long At=0,Comp=0,dimHeap;
////////////////////////

//structura pentru pointer_lis//
typedef struct nod
 {
   int nr;
   struct nod *urm;
 }NOD;
/////////////////////////

//structura pentru index
typedef struct xi
 {
    int x;
    int index;
 }X_I;
////////////////////////

/////pointerii pentru lista//////
int Lista_finala[100001],count=0;
NOD *pointer_lis[1001];
NOD *prim[1001],*ultim[1001];
/////////////////////////////////

////////////Afisare//////////////
void afisare(int i)
 {
    NOD *p;
    if(prim==NULL)
	 {
		printf("lista e vida");
	 }   
    else
	 {
		p=prim[i];
        while(p)
        {
		  printf("%d ",p->nr);
          p=p->urm;
		}
	 }
	printf("\n");
}
////////////////////////////////

//////////creeare lista///////////
void inserare_dupa_ultim(int i,int n)
{
 int temp=0;
 for(int j=1; j<=n/k; j++)
 {
   NOD *p;
   p = (NOD*)malloc(sizeof(NOD));
   temp = temp + rand()%25;;
   p->nr = temp;
   p->urm = 0;
    if(prim[i]==NULL)
	 {
		pointer_lis[i] = p;
		prim[i]  = p;
		ultim[i] = p;
	 }
    else 
	 {
	    ultim[i]=p;
		pointer_lis[i]->urm=p;  
		pointer_lis[i]=p;
	 }
 }
}
//////////////////////////////////

//returneaza indicele parintelui//
int parent(int i) 
{
	return (i / 2);
}
//////////////////////////////////

//returneaza indicele elementului din stanga//
int stanga(int j)
{
  return (2*j);
}
/////////////////////////////////////////////

//returneaza indicele elementului din dreapta//
int dreapta(int j)
{
  return (2*j)+1;
}
//////////////////////////////////////////////

//////////////Refacere Heap-BU////////////////
void heap_BU(X_I H[], int i)
{ 
  int l = stanga(i);
  int r = dreapta(i);
  int t;
  if ((l <= dimHeap) && (H[l].x < H[i].x)) 
   {
      t = l;
   }
  else
   {
      t = i;
   }

  if ((r <= dimHeap) && (H[r].x < H[t].x)) 
   {
     t = r;
   }

  if (t!= i) 
   {
	  int auxt = H[i].x;      
	  int auxi = H[i].index;
	  H[i].x = H[t].x;	   
	  H[i].index = H[t].index;
	  H[t].x = auxt;	   
	  H[t].index = auxi;
      heap_BU(H, t);
  }
}
/////////////////////////////////////////////////

///////Inserare de elemente in Heap///////
void heapPush(X_I H[],X_I xi)
 { 
    dimHeap++;
	H[dimHeap].x = xi.x;
	H[dimHeap].index = xi.index;
	At++;
	int i = dimHeap;
	Comp++;
	while(i>1 && H[parent(i)].x > H[i].x)
	 {	
		Comp++;
		At = At+3;
		int aux1 = H[parent(i)].x;		
		int aux2 = H[parent(i)].index;	
		H[parent(i)].x = H[i].x;		
		H[parent(i)].index = H[i].index;
		H[i].x = aux1;					
		H[i].index = aux2;
		i = parent(i);
	}
 }
//////////////////////////////////////////

//Scoaterea elementelor din HEAP//
X_I heapPop(X_I H[])
 { 
	X_I xi;
	xi.x = H[1].x;
	xi.index = H[1].index;

	//pe elementul 1 pun ultimul elem
	H[1].x = H[dimHeap].x;
	H[1].index = H[dimHeap].index;
	At++;
	dimHeap--;
	heap_BU(H,1);
	return xi;
 }
//////////////////////////////////

///////////////////Interclasare////////////////////
void interclasare(int k)
{
	  X_I xi, H[100001];	
	  dimHeap=0;
	  for(int i=1; i<=k; i++)
	   {
		xi.x = prim[i]->nr;
		xi.index = i;	
		heapPush(H,xi);		
       }

	while(dimHeap>0)
	 {
		xi = heapPop(H);
		count++;         
		Lista_finala[count]=xi.x; 	
		if(prim[xi.index]->urm!=NULL)
		{
			prim[xi.index] = prim[xi.index]->urm;
			xi.x = prim[xi.index]->nr;
			heapPush(H,xi);
		}	
  }
}
////////////////////////////////////////////////////////


int main()
{
   int L1,L2,L3;
   srand(time(NULL));
   FILE *f,*ff;
   int aux1[4],aux2[4];

   //>>>>>>>>>>>>>>>>> PUNCTUL 1 <<<<<<<<<<<<<
   printf("\nSe executa punctul 1");
   f=fopen("tabel1.csv","w");
   fprintf(f,"n,k1_at,k1_comp,k1_at+comp,k2_at,k2_comp,k2_at+comp,k3_at,k3_comp,k3_at+comp \n"); 

	
	for(n=100; n<=10000; n=n+100)
	 { 
		//////////////// k1 //////////////////
		k=5;
		aux1[0]=0; aux1[1]=0; aux1[2]=0;
	    aux2[0]=0; aux2[1]=0; aux2[2]=0;

		 for(int i=1; i<=k; i++)
		  {	
			 inserare_dupa_ultim(i,n);	   
		  }
	    At=0;
	    Comp=0;
	    interclasare(k);
	    aux1[0]=aux1[0]+At;
	    aux2[0]=aux2[0]+Comp;
	    //////////////////////////////////////

	    //////////////// k2 //////////////////
	    k=10;
	     for(int i=1;i<=k;i++)
	      {	
			    inserare_dupa_ultim(i,n);   
	      }
	    At=0;Comp=0;
	    interclasare(k);
	    aux1[1]=aux1[1]+At;
	    aux2[1]=aux2[1]+Comp;
	    //////////////////////////////////////

	    //////////////// k3 //////////////////
	    k=100;
		 for(int i=1;i<=k;i++)
		  {	
			inserare_dupa_ultim(i,n);			
		  }
	    At=0;Comp=0;
	    interclasare(k);
	    aux1[2]=aux1[2]+At;
	    aux2[2]=aux2[2]+Comp;
	    //////////////////////////////////////

	    fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d \n",n,aux1[0],aux2[0],(aux1[0]+aux2[0]) ,  aux1[1],aux2[1],(aux1[1]+aux2[1]),  aux1[2],aux2[2],(aux1[2]+aux2[2]) );
	  }
	fclose(f);

	printf("\nSe executa punctul 2\n");
	n=10000;
	ff=fopen("tabel2.csv","w");
	fprintf(ff,"k,at,comp,at+comp \n"); 

	 for(k=10; k<=500; k=k+10)
	  {	
	     for(int i=1; i<=k; i++)
	      {	
			   inserare_dupa_ultim(i,n);
	      }
	    At=0; Comp=0;
	    interclasare(k);
	    fprintf(ff, "%d,%d,%d,%d \n",k,At,Comp,At+Comp );
	  }
   fclose(ff);
   printf("\nDone!");
   getch();
   return 0;
}