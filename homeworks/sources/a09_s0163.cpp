/****ANONIM*** ***ANONIM*** Assign 9 2013. BFS
    indiferent de numarul de arce sau de noduri, BFS-ul are aceeasi complexitate: |V|+|E| deoarece operatiile de
    adaugare/scoatere din coada sunt liniare, dominant devine numarul de comparatii care au loc pentru adaugarea
    respectiv scoaterea din coada a varfurilor. Din grafice se observa ca, ecuatia complexitatii are reprezentarea
    unei drepte care variaza dependent doar de numarul de varfuri si de arce, deci BFS are complexitate liniara.
*/

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>

typedef struct Queue
{
        int capacity;
        int size;
        int front;
        int rear;
        int *elements;
}Queue;

int no_op;
int graph[10000][10000];

Queue * CreateQueue(int maxElements)
{

        Queue *Q;
        Q = (Queue *)malloc(sizeof(Queue));

        Q->elements = (int *)malloc(sizeof(int)*maxElements);
        Q->size = 0;
        Q->capacity = maxElements;
        Q->front = 0;
        Q->rear = -1;
        return Q;
}
void Dequeue(Queue *Q)
{

        if(Q->size==0)
        {
                printf("Queue is Empty\n");
                return;
        }

        else
        {
                Q->size--;
                Q->front++;

                if(Q->front==Q->capacity)
                {
                        Q->front=0;
                }
        }
        return;
}

int Front(Queue *Q)
{
        if(Q->size==0)
        {
                printf("Queue is Empty\n");
                exit(0);
        }

        return Q->elements[Q->front];
}

void Enqueue(Queue *Q,int element)
{
	Q->size++;
	Q->rear = Q->rear + 1;
    if(Q->rear == Q->capacity)
	{
        Q->rear = 0;
    }
    Q->elements[Q->rear] = element;
    return;
}



void Bfs(int size[], int presentVertex,int *visited)
{
        visited[presentVertex] = 1;
		no_op++;
        Queue *Q = CreateQueue(201);
        Enqueue(Q,presentVertex);
        while(Q->size)
        {
                presentVertex = Front(Q);

                Dequeue(Q);
                int iter;
                for(iter=0;iter<size[presentVertex];iter++)
                {
					no_op++;
                        if(!visited[graph[presentVertex][iter]])
                        {
                                visited[graph[presentVertex][iter]] = 1;
								no_op++;
                                Enqueue(Q,graph[presentVertex][iter]);
                        }
                }
        }
        return;


}

int contains(int vertex1,int vertex2,int size[])
{
	if(vertex1==vertex2)
		return 1;
	for(int i=0;i<size[vertex1];i++)
		if(graph[vertex1][i]==vertex2)
			return 1;
	return 0;
}

int main()
{
	int size[5000]={0},visited[5000]={0};
        int vertices,edges,iter;
		FILE *f=fopen("bfs.csv","w");
       srand(NULL);
        vertices=100;
		for (edges=1000;edges<5000;edges+=100)
		{
        int vertex1,vertex2;
		for (int k=0;k<5000;k++)
			{
				size[k]=0;
				visited[k]=0;
			}

        for(iter=0;iter<edges;iter++)
        {

                while(1)
				{
					vertex1=rand()%100;
					vertex2=rand()%100;

					if(contains(vertex1,vertex2,size)==0)
					{
						size[vertex1]++;
						size[vertex2]++;
						//printf("%d , %d \n",size[vertex1],size[vertex2]);
						graph[vertex1][size[vertex1]] = vertex2;
						graph[vertex2][size[vertex2]] = vertex1;
						break;
					}

				}

        }
		printf("%d\n",edges);
        int presentVertex;

        for(presentVertex=0;presentVertex<vertices;presentVertex++)
        {
			no_op++;
                if(!visited[presentVertex])
                {
                        Bfs(size,presentVertex,visited);
                }
        }
		fprintf(f,"%d, %d,\n",edges,no_op);
		no_op=0;
		}




		edges=4500;
		for (vertices=100;vertices<200;vertices+=10)
		{
        int vertex1,vertex2;
		for (int k=0;k<5000;k++)
			{
				size[k]=0;
				visited[k]=0;
			}

        for(iter=0;iter<edges;iter++)
        {

                while(1)
				{
					vertex1=rand()%100;
					vertex2=rand()%100;

					if(contains(vertex1,vertex2,size)==0)
					{
						size[vertex1]++;
						size[vertex2]++;
						//printf("%d , %d \n",size[vertex1],size[vertex2]);
						graph[vertex1][size[vertex1]] = vertex2;
						graph[vertex2][size[vertex2]] = vertex1;
						break;
					}

				}

        }
		printf("%d\n",edges);
        int presentVertex;

        for(presentVertex=0;presentVertex<vertices;presentVertex++)
        {
			no_op++;
                if(!visited[presentVertex])
                {
                        Bfs(size,presentVertex,visited);
                }
        }
		fprintf(f,"%d, %d,\n",vertices,no_op);
		no_op=0;
		}
		fclose(f);
        return 0;



}
