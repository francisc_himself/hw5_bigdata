
// ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER*** Multi-way Three
#include<iostream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#define N_MAX 20
#define MAX_FII 10
using namespace std;
//R1: parent representation: pt fiecare cheie se pastreaza intr-un vector cheia parintelui
typedef struct parinte_fii
{
    int nr_fii;
    int fii[MAX_FII];
}parinte_fii;

//R2: multi-way representation: pt fiecare nod avem cheia si toti fii pastrati intr-un vector
typedef struct multi_cai
{
    int key;
    int nr_fii;
    struct multi_cai *fii[MAX_FII];
} multi_cai;

//R3: binary tree representation: pt fiecare nod avem salvate cheia si 2 pointeri: 
//catre primul fiu si catre fratele drept
typedef struct binar
{
    int key;
    struct binar *stg;
    struct binar *dr;
} binar;

//O(n) - adaug n-1 fii radacina fiind deja inserata
void creare_arbore_multicai(multi_cai **r, parinte_fii fii[])
{
    int i, parinte = (*r)->key;

    for(i = 0; i < fii[parinte].nr_fii; i++)
    {
       
        (*r)->fii[i] = (multi_cai*)malloc(sizeof(multi_cai));
        (*r)->fii[i]->key = fii[parinte].fii[i];
        (*r)->fii[i]->nr_fii = 0;
        (*r)->nr_fii++;
        creare_arbore_multicai(&((*r)->fii[i]), fii);
    }
}


//traversare in preordine a arborelui multicai
void print_preordine_multicai(multi_cai *r, int nr_spatii)
{
    int i;
    for(i = 0; i <= nr_spatii; i++)
		cout<<" ";
    
	cout<<r->key<<endl;
    for(i = 0; i < r->nr_fii; i++)
        print_preordine_multicai(r->fii[i], nr_spatii+1);
}


//O(n) - adaug n-1 fii radacina fiind deja inserata
void creare_arbore_binar(binar **b, multi_cai *m)
{
    binar *aux;
    int i;
    if(m->nr_fii > 0)
    {
        (*b)->stg = (binar*)malloc(sizeof(binar));
        (*b)->stg->key = m->fii[0]->key;
        (*b)->stg->stg = NULL;
        (*b)->stg->dr = NULL;
        aux = (*b)->stg;
        for(i = 1; i < m->nr_fii; i++)
        {
            aux->dr = (binar*)malloc(sizeof(binar));
            aux->dr->key = m->fii[i]->key;
            aux->dr->stg = NULL;
            aux->dr->dr = NULL;
            creare_arbore_binar(&aux,m->fii[i-1]);
            aux = aux->dr;
        }

    }
}


//traversare in preordine a arborelui binar
void print_preordine_binar(binar *r, int nr_spatii)
{
    int i;
    for(i = 0; i <= nr_spatii; i++)
       cout<<" ";
    
	cout<<r->key<<endl;
    if(r->stg != NULL)
        print_preordine_binar(r->stg, nr_spatii+1);
    if(r->dr != NULL)
        print_preordine_binar(r->dr, nr_spatii);
}

int main()
{
	cout<<"Multi-way Three"<<endl;
	cout<<"###############"<<endl;
	cout<<endl;
    int a[N_MAX], i, n = 9;
    parinte_fii aux[MAX_FII];
    multi_cai *r2 = NULL;
    binar *r3 = NULL;
    a[1] = 2;
    a[2] = 7;
    a[3] = 5;
    a[4] = 2;
    a[5] = 7;
    a[6] = 7;
    a[7] = -1;
    a[8] = 5;
    a[9] = 2;

    //afisare parent representation , cu vectorul de tati
	cout<<"Afisare vectori tati"<<endl;
	cout<<"---------------"<<endl;
	cout<<"R1: "<<endl;
    for(i = 1; i <= n; i++)
       		cout<<a[i]<<" ";
		cout<<endl;
  
   
	


    //T1- transforma arborele intr-o reprezentare multicai
   
    for(i = 1; i <= n; i++)
        aux[i].nr_fii = 0;            //initializez aux, eficienta O(n)
    
       
    for(i = 1; i <= n; i++)            //creez vectori cu fii fiecarui nod, O(n)
    {
        if(a[i] != -1)
        {
            aux[a[i]].fii[aux[a[i]].nr_fii] = i;//inserez fiul
            aux[a[i]].nr_fii++;//cresc numarul de fii
        }
    }
    
      
    i = 1;
    while(a[i] != -1)                //caut radacina, eficienta este in cel mai rau caz O(n)
        i = a[i];
    
    r2 = (multi_cai*)malloc(sizeof(multi_cai));//inserez radacina
    r2->key = i;
    r2->nr_fii = 0;
    
    creare_arbore_multicai(&r2, aux);  //O(n)
   

   //afisare arbore multicai
  cout<<"Afisare arbore multicai"<<endl;
	cout<<"---------------"<<endl;
	cout<<"R2: "<<endl;
    print_preordine_multicai(r2, 0);
   

    //T2: transformare din reprezentare multicai in reprezentare binara
    r3 = (binar*)malloc(sizeof(binar));
    r3->key = r2->key;
    r3->stg = NULL;
    r3->dr = NULL;
    creare_arbore_binar(&r3, r2);
   

    //afisare arbore binar
	cout<<"Afisare arebore binar"<<endl;
	cout<<"---------------"<<endl;
    cout<<"R3: "<<endl;
    print_preordine_binar(r3, 0);
    

	_getch();
    return 0;
}
