/****ANONIM*** Mariana ***ANONIM***, gr ***GROUP_NUMBER***.
Implementarea algoritmului de cautare in latime. La un numar variabil de noduri si un numar fix de varfuri(bfs1.csv), cat si la un numar
variabil de muchii si un numar fix de noduri(bfs.csv) algoritmul ruleaza intr-un timp liniar, ce depinde de marimea reprezentarii prin lista de adiacenta
a grefului. Deasemenea complexitatea este O(nrnod+nrm), suma numarului de muchii si a celui de varfuri.
Operatiile de inserare_coada si stergere_coada a unui element se fac intr-un timp O(1), deci operatiile efectuate pe coada
sunt O(nr_muchii). Pentru parcurgerea listelor de adiacenta complexitatea este O(nr_noduri).
*/
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ALB 0
#define GRI -1
#define NEGRU 1
#define INF 32765 //pentru valoarea infinit am selectat o valoare oarecare
#define max 1000

int T[100];//vectorul parinte
int nrnod;//numar noduri
int nrm;//numar muchii

int contor=0;
typedef struct _MULTI_CAI_NODE
{
	int key;
	int count;
	struct _MULTI_CAI_NODE *child[50];
}MULTI_NODE;//pentru prrety print

typedef struct nod {
	int n;
	nod* urm;
} coada;
typedef struct varf{
	int key;
	int culoare;
    int parinte;
	int d;
	coada *prim;
	coada *ultim;
} lista;


lista *G[max];

coada *pr,*ul;

void G_init()
{
	int i;
//pentru fiecare nod, initializam parintele si distanta la o valoare neutra si culoarea initiala e alb.
	for(i=1; i<=nrnod; i++)
	{
		contor++;
		G[i] = (lista *)malloc(sizeof(lista));
		G[i] -> key = i;
		G[i] -> culoare = 0;
		G[i] -> d = INF;
		G[i] -> parinte = 0;
		G[i] -> prim = NULL;
		G[i]->ultim = NULL;
	}
}
//adaugam in graf o muchie
void adauga(int x, int y)
{
	coada *p;
	p = (coada *)malloc(sizeof(coada));
	p ->n = y;
	p -> urm = NULL;

	//adaug primul vecin
	if(G[x]->prim == NULL)
	{
		G[x] -> prim = p;
		G[x] -> ultim = p;
	}
	//adaug la sfarsitul listei, dupa alti vecini
	else
	{
		G[x] -> ultim->urm = p;
		G[x] -> ultim = p;
	}

}


void inserare_coada(int nr)
{
	contor++;
	coada *p;
	p = (coada *)malloc(sizeof(coada));
	p->n=nr;
	p->urm = NULL;
   	if(pr == NULL)
	{
		pr=p;
		ul = p;
	}
	else
	{
		ul->urm = p;
		ul = p;

	}
}
coada* stergere_coada()
{
    //stergem primul element din coada si in acelsi timp il returnam
	contor++;
	coada *p, *r;
	p = (coada *)malloc(sizeof(coada));

	if(pr!=NULL)
	{
		p ->n = pr->n;
		p->urm = NULL;
		r =pr;
		pr = pr->urm;
		free(r);
	}
	else p = NULL;

	return p;
}

void BFS(int s)
{
	coada *u,*v;
	for (int i=1; i<=nrnod; i++)
	{
		G[i]->culoare = ALB;
		G[i]->d = INF;
		G[i]->parinte = 0;
		contor++;
	}

	G[s]->culoare= GRI;
	G[s]->d = 0;
	G[s]->parinte = 0;
	contor++;
	pr=NULL;
	inserare_coada(s);
	while (pr != NULL)//cat timp coada nu e vida
	{
		u = stergere_coada();
		for (v=G[u->n]->prim; v!= NULL; v=v->urm)
		{
			contor++;
			if (G[v->n]->culoare == ALB)
			{
				G[v->n]->culoare = GRI;
				G[v->n]->d= G[u->n]->d+1;
				G[v->n]->parinte = u->n;
				inserare_coada(v->n);
				contor++;
			}
		}
        contor++;
		G[u->n] ->culoare = NEGRU;
	}
}

void afiseaza_lista()
{
	int i;
	coada *p;

	printf("lista de adiacenta: ");
	for(i=1; i<=nrnod; i++)
	   {
		    printf("\n%d : ", i);
			p = G[i]->prim;
			while(p != NULL)
			{
				printf("%d ",p->n);
				p = p->urm;
			}
	   }
}

void citesc()
{
	int i, x, y;


	printf("bfs\n");
	printf("nr noduri = ");
	scanf("%d", &nrnod);
	printf("nr muchii = ");
	scanf("%d", &nrm);
	G_init();
	printf("introduceti muchiile:\n");
	for(i=1;i<=nrm;i++)
	{
		scanf("%d %d", &x, &y);
		adauga(x, y);
	}

	afiseaza_lista();
}


void prettyprint(MULTI_NODE *no, int nivel=0)
{
	for(int i=0;i<nivel;i++)
		printf("  ");
	printf("%d \n", no->key);
	for(int i=0; i<no->count;i++)
		 prettyprint(no->child[i], nivel+1);
}

void insertMN(MULTI_NODE *parent, MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
}

MULTI_NODE *createMN(int key)
{
	MULTI_NODE *x=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	x->key=key;
	x->count=0;
    return x;
}

MULTI_NODE *transform1(int t[],int size)
{
	int i;
	MULTI_NODE *root=NULL;
	MULTI_NODE *nodes[50];
	for (i=0;i<size;i++) //create nodes
       nodes[i]=createMN(i);
    for (i=0;i<size;i++) //legaturi
	{
		if (t[i]!=-1){
	 insertMN(nodes[t[i]],nodes[i]);
		}
		else{
			root=nodes[i];
		}
     }
	return root;
}

void verificare()
{
	int i;
	citesc();
	BFS(1);
	for(i=1;i<=nrnod;i++)
	{
		printf("\ndistanta %d ,nod %d ,parinte nod:%d ",G[i]->d, G[i]->key,G[i]->parinte);
	}
	for(i=1; i<=nrnod ;i++)
		if(G[i]->parinte == 0)
		{
			T[i] = -1;
     	}
		else
			T[i] = G[i]->parinte;//vectorul de tati
   printf("\n vectorul parinti: ");
	for(i=1;i<=nrnod;i++)
	{

		printf("%d ,",T[i]);
	}
	/*int size=sizeof(T)/sizeof(T[0]);
   MULTI_NODE *root=transform1(T,size);
	prettyprint(root);  */
	//ar trebui sa afisez arborele dupa vectorul de tati
}

void aleator()
{

	int i, x, y;
	FILE *f;

	srand(time(NULL));
	nrnod=100;
	G_init();
	//pentru numar variabil de muchii
	/*f = fopen("bfs.csv", "w");
	fprintf(f, "muchii, operatii\n");

	for(nrm=1000; nrm<=5000; nrm+=100)
	{
		contor=0;
		G_init();
		for(i=1; i<=nrm; i++)
		{
			x = rand()%100+1;
			do{
				y = rand()%100+1;
			   }
			while(x==y);//sa nu avem muchie de la un nod la el insusi
			adauga(x,y);
		}
		BFS(1);
		fprintf(f, "%d, %d\n", nrm, contor);
	}

	fclose(f);*/

	//pentru numar variabil de noduri
	f = fopen("bfs1.csv", "w");
	nrm=9000;
	fprintf(f, "nrnod, operatii\n");
	for(nrnod=150; nrnod<=200; nrnod+=10)
	{

		G_init();
		for(i=1; i<=nrm; i++)
		{
			x = rand()%nrnod+1;
			do{
				y = rand()%nrnod+1;
			}
			while(x==y);
			adauga(x,y);
		}
		BFS(1);
		fprintf(f, "%d, %d\n", nrnod, contor);
	}

	fclose(f);

}

void main()
{
	//verificare();
	aleator();
	getch();

}
