#include<conio.h>
#include<stdio.h>
#include<time.h>
#include <stdlib.h>
#include <fstream>
#define MAX_SIZE 10001




int dimB;
int contor1_comp;
int contor1_atri;
int contor2_comp;
int contor2_atri;


int Partition (int *A, int p, int r);

void Quicksort (int *A, int p, int r)
{
	int q;
	if(p < r)
	{
		q = Partition(A,p,r);
		Quicksort(A,p,q-1);
		Quicksort(A,q+1,r);

	}
}

int Partition (int *A, int p, int r)
{
	int x,i,temp,j;
	x=A[r];
	i=p-1;
	for(j=p;j<=r-1;j++)
	{
		contor2_comp++;
		if(A[j]<=x)
		{
			
			i=i+1;
			temp=A[i];
			A[i]=A[j];
			A[j]=temp;
			contor2_atri=contor2_atri+3;
		}
	}
	temp=A[i+1];
	A[i+1]=A[r];
	A[r]=temp;
	contor2_atri=contor2_atri+3;
	return i+1;
}

int P (int i)
{
	return i/2;
}

void heapify(int *v, int i)
{
	int left,right,min,temp;
	left=2*i;
	right=2*i+1;

	if(left<=dimB && v[left]<v[i])
			min=left;
	
	else 
		min=i;

	
	if (right<=dimB && v[right]<v[min])
		
		min=right;
		
	if (min != i)
	{
		temp=v[i];
		v[i]=v[min];
		v[min]=temp;
		
		contor1_atri=contor1_atri+3;
		
		heapify(v,min);
	}
	contor1_comp=contor1_comp+3;
}

void creare(int *v,int n,int low,int high)
{
	int i,aux;
	
	for(i=0;i<n;i++)
	{
		aux=rand()%(high-low+1);
		aux=aux+low;
		v[i]=aux;
	}
}


void copy(int *v1, int *v2,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v2[i]=v1[i];
	}
	
}


int main ()
{
	int n,i,j,q=0,m=5;
	FILE *fp;
	srand(time(NULL));
	fp = fopen("results.csv", "w"); 

	int a[]={2,1,3,4,0,6};
	int test[6];
	Quicksort(a,1,5);
	

	for(i=0;i<6;i++)
	{
		printf("%d ",test[i]);
	}
	printf("\n");

	for(n=100;n<MAX_SIZE;n=n+100)
	{
			int* v1=(int*)malloc((n+1)*sizeof(int));
			int* v2=(int*)malloc((n+1)*sizeof(int));
			
			printf(" %d",n);

			contor1_comp=0;
			contor2_comp=0;
			contor1_atri=0;
			contor2_atri=0;
			dimB=n;
			for(int i=0;i<m;i++)
			{
				creare(v1,n,1,100);
				copy(v1,v2,n);
				dimB=n;
				
				for(j=dimB/2;j>0;j--)  //creez heap
					heapify(v1,j);

				while(dimB != 1)     //scoatem elem in ordine descrescatoare
				{
					v1[1]=v1[dimB];
					dimB--;
					heapify(v1,1);
				}
				
				Quicksort(v2,1,n);
										
				
			}
			fprintf(fp,"%d,%d,%d,%d,%d\n",n,(contor1_comp/m),(contor1_atri/m),(contor2_comp/m),(contor2_atri/m));
			free(v1);
			free(v2);
			
	 }


	
	fclose(fp);
	
	
	
	getch();
	return 0;
}
