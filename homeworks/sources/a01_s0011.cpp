/****ANONIM*** ***ANONIM***
Grupa: ***GROUP_NUMBER***
Tema: Compararea algoritmilor de sortare prin interschimbare, prin insertie si prin selectie
In cazul favorabil (cand vectorul este sortat crescator), eficienta algoritmilor este O(n) pt interschimbare si insertie
si O(n^2) pentru selectie.
In cazul defavorabil (cand vectorul este sortat descrescator), eficienta algoritmilor este O(n^2).
In cazul mediu (cand elementele sunt generate aleator), eficienta este O(n^2).
In general, ca si numar de operatii, metoda de sortare prin insertie efectueaza cele mai putin.
*/


#include <iostream>
#include <conio.h>
#include <fstream>  


#define MAX_SIZE 10000

using namespace std;
ofstream fout("date.csv");
int comp_interschimb;
int atr_interschimb;
int comp_sel;
int atr_sel;
int comp_ins;
int atr_ins;
int n;

void interschimbare(int a[], int n)
{
	//comp_interschimb=0;  //pentru caziul mediu fav nu avem nevoie de initializare cu 0
	//atr_interschimb=0;	
	bool sortat=false;
	int i=1;
	while (!sortat)
	{
		sortat=true;
		for(int j=0;j<n-i;j++)
		{
			comp_interschimb++;
			if(a[j]>a[j+1])
			{
				atr_interschimb=atr_interschimb+3;
				int aux=a[j];
				a[j]=a[j+1];
				a[j+1]=aux;
				sortat=false;
			}
		}
		i++;
	}
}

void selectie(int a[], int n)
{
	//atr_sel=0;        //pt cazul mediu fav nu avem nevoie de init cu 0
	//comp_sel=0;
	for(int i=0;i<n-1;i++)
	{
		atr_sel++;
		int min=a[i];
		int i_min=i;
		for(int j=i+1;j<n;j++)
		{
			comp_sel++;
			if(a[j]<min)
			{
				atr_sel++;
				min=a[j];
				i_min=j;
			}
		}
		atr_sel=atr_sel+3;
		int aux=a[i];
		a[i]=a[i_min];
		a[i_min]=aux;
	}
}

void insertie(int a[], int n)
{
	//atr_ins=0;		//pt cazul mediu fav  nu avem nevoie de init cu 0
	//comp_ins=0;
	for(int i=1;i<n;i++)
	{
		atr_ins++;
		int x=a[i];
		int j=1;
		comp_ins++;
		while(a[j]<x)
		{
			comp_ins++;
			j++;
		}
		for(int k=i;k>=j+1;k--)
		{
			atr_ins++;
			a[k]=a[k-1];
		}
		atr_ins++;
		a[j]=x;
	}
}

int main()
{
	/*int a[6]={1,7,10,3,2,8};
	int b[6]={1,7,10,3,2,8};
	int c[6]={1,7,10,3,2,8};
	interschimbare(a,6);
	cout<<"Vectorul sortat prin interschimbare: ";
	for(int i=0;i<6;i++)
		cout<<a[i]<<" ";
	selectie(b,6);
	cout<<endl<<"Vectorul sortat prin selectie: ";
	for(int i=0;i<6;i++)
		cout<<b[i]<<" ";
	insertie(c,6);
	cout<<endl<<"Vectorul sortat prin insertie: ";
	for(int i=0;i<6;i++)
		cout<<c[i]<<" ";*/
	int a[MAX_SIZE];
	int b[MAX_SIZE];
	int c[MAX_SIZE];
	fout<<"NR,"<<"Comp_interschimb,"<<"Comp_sel,"<<"Comp_insert,"<<"Atr_interschimb,"<<"Atr_sel,"<<"Atr_insert,"<<"Total_inetrschimb,"<<"Total_sel,"<<"Total_insert\n ";
	
	for(n=100; n<=MAX_SIZE; n += 100)
	{
		comp_interschimb=0;
		atr_interschimb=0;
		comp_sel=0;
		atr_sel=0;
		comp_ins=0;
		atr_ins=0;
		for(int nr=0;nr<5;nr++)
		{
			for(int i=0;i<n;i++)
			{	a[i]=rand()%RAND_MAX; //pt cazul mediu
				//a[i]=i; //pt cazul favorabil
				//a[i]=n-i; //pt cazul defavorabil
			}
			for(int i=0;i<n;i++)
			{
				b[i]=a[i];
				c[i]=a[i];
			}
			interschimbare(a,n);
			selectie(b, n);
			insertie(c, n);
		}
		comp_interschimb=comp_interschimb/5;
		atr_interschimb=atr_interschimb/5;
		comp_sel=comp_sel/5;
		atr_sel=atr_sel/5;
		comp_ins=comp_ins/5;
		atr_ins=atr_ins/5;
		fout<<n<<","<<comp_interschimb<<","<<comp_sel<<","<<comp_ins<<","<<atr_interschimb<<","<<atr_sel<<","<<atr_ins<<","<<comp_interschimb+atr_interschimb<<","<<comp_sel+atr_sel<<","<<comp_ins+atr_ins<<endl;

	}
	
	fout.close();
	return 1;

}