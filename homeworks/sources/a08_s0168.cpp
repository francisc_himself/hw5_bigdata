﻿/* ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Structuri de date pentru multimi disjuncte
Să presupunem că aveți o colecție de liste și fiecare nod din fiecare listă conține un obiect, numele listei din care face parte, și numărul de elemente din acea listă.
De asemenea, presupunem că suma numărul de elemente din toate listele este n (adică există n elemente de ansamblu).
Ne dorim să fie în măsură să fuzioneze oricare două dintre aceste liste, și să actualizeze toate nodurile lor, astfel încât acestea mai conțin numele listei de care aparțin. 
Regula de fuziune listele A și B, este că, dacă A este mai mare decât B, apoi îmbinați elementele de B în A și actualizati elementele care au aparținut lui B, și vice-versa.

Eficienta O(m Log n)
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

//structura pentru nodurile disjuncte 
typedef struct ds_node{
	int key;
	struct ds_node * rep;	//cel mai reprezentativ element( primul din fiecare lista)
	struct ds_node * next;
}DS_NODE;

DS_NODE *head[10001], *tail[10001];
int a[10001][10001], v, null_list[10001];

int M;

//printeaza o matrice data ca parametru
void print_matrix (int b[10001][10001],int n)
{
	int i, j;

	for (i=1; i<=n; i++)
	{
		for (j=1; j<=n; j++)
			printf ("%d ",a[i][j]);
		printf ("\n");
	}
}

//creaza un nod de cheie k
DS_NODE * create_vertex (int k)
{
	DS_NODE * p;

	p=(DS_NODE *) malloc(sizeof(DS_NODE));
	p->next = NULL;
	p->key = k;

	return p;
}

//initializam cu 1 vectorul de liste nule(null_list)
void initializare (int index)
{
    
	null_list[index] = 1;
}

//Metoda care creaze o lista a unui element
void make_set (int v)
{
	head[v] = tail[v] = create_vertex (v);
	head[v]->rep = tail[v]->rep = head[v];
}

DS_NODE * find_set (int u, int *index)
{
	int i;
	DS_NODE *p;

	for (i=1; i<=v; i++)
	{
	
		if (null_list[i] != 1)
		{
			p = head[i];
			
			do
			{
				if (p->key == u)
				{
					*index = i;
					return head[i];
				}
			
				p = p->next;
			}
			while (p!=NULL);
		}
	}
}
// Metoda union uneste cele 2 liste ( cu eficienta O(N))
void union_sets (int u, int v)	//returneaza reprezentantul grupului
{
	DS_NODE *head1, *head2, *tail1, *tail2, *p;
	int index1, index2;

	head1 = find_set (u, &index1);
	head2 = find_set (v, &index2);
	tail1 = tail[index1];
	tail2 = tail[index2];

	//il consideram ca reprezentant pe cel mai mic reprezentant
	if (head1->key < head2->key)
	{
		//punem lista 2 la sfarsitul listei 1
		tail[index1]->next = head[index2];
	
		tail[index1] = tail[index2];
		//il punem ca repezentant pe head1
		p = head[index2];
		while (p != NULL)
		{
			p->rep = head1;
			p = p->next;
		}

		//initializam lista 2
		initializare (index2);
	}
	else
	{
		//punem lista 1 la sfarsitul listei 2
		tail2->next = head1;
	
		tail[index2] = tail[index1];
		//il punem ca repezentant pe head2
		p = head1;
		while (p != NULL)
		{
			p->rep = head2;
			p = p->next;
		}

		//initializam lista 1
		initializare (index1);
	}
}
// Metoda pentru componente conexe
void connected_components ()
{
	int i,j, index;

	for (i=1; i<=v; i++)
	{
		M++;
		make_set (i);
	}

	for (i=1; i<=v; i++)
		for (j=1; j<=v; j++)
			if (a[i][j] == 1)
			{
				M = M+2;
				if (find_set (i, &index) != find_set(j, &index))
				{
					M++;
					union_sets (i,j);
				}
			}
}

void resolve ()
{
	FILE *pf;
	int i, j;
	int e, v1, v2;

	pf = fopen ("multimi_disjuncte.txt","w");
	srand ( time(NULL) );

	v = 10000;

	//numarul de muchii E variind de la 10000 la 60000, cu pasul 1000;
	for (e=10000; e<=60000; e=e+1000)
	{
		//initializam matricea de muchii
		for (i=1; i<=v; i++)
			for (j=1; j<=v; j++)
				a[i][j] = 0;

		//generam muchiile
		for (i=1; i<=e; i++)
		{
			v1 = rand () % v + 1;
			v2 = rand () % v + 1;

			while (v1==v2 || a[v2][v1] == 1 || a[v1][v2]==1)
			{
				v1 = rand () % v + 1;
				v2 = rand () % v + 1;
			}

			a[v1][v2] = 1;
		}

		//initializam matricele
		for (i=1; i<=v; i++)
		{
			head[i] = tail[i] = NULL;	//initializam listele inlantuite
			null_list[i] = 0;			//initializam vectorul de liste nule
		}
		
		M = 0;
		connected_components ();

		fprintf (pf,"%d %d\n",e,M);
	}
}

void test ()
{
	DS_NODE *p;
	int i, j;
	int e, v1, v2;

	srand ( time(NULL) );
	
	v = 10; 

	//initializam matricea de muchii
	for (i=1; i<=v; i++)
		for (j=1; j<=v; j++)
			a[i][j] = 0;

	e = 5;

	//generam muchiile
	printf ("Muchiile sunt:\n");
	for (i=1; i<=e; i++)
	{
		//generam aleator perechi de numere cuprinse intre 1 si 10000 (perechi de varfuri) => muchii
		v1 = rand () % v + 1;
		v2 = rand () % v + 1;
		
		//printf ("\nv1=%d v2=%d\n", v1, v2);

		while (v1==v2 || a[v2][v1] == 1 || a[v1][v2]==1)
		{
			v1 = rand () % v + 1;
			v2 = rand () % v + 1;

			//printf ("v1=%d v2=%d\n", v1, v2);
		}

		printf ("v1=%d v2=%d\n", v1, v2);
		a[v1][v2] = 1;
	}

	printf ("\nMatricea muchiilor:\n");
	print_matrix(a,v);

	//initializam listele inlantuite
	for (i=1; i<=v; i++)
		head[i] = tail[i] = NULL;
	
	

	connected_components ();
	printf ("\nComponentele conexe:\n");
	for (i=1; i<=v; i++)
		if (null_list[i] != 1)
		{
			p = head[i];
			printf ("L[%d]: ",i);
			while (p != NULL)
			{
				printf ("%d ",p->key);
				p = p->next;
			}
			printf ("\n");
		}
}

void main ()
{
	test ();
	
	getche ();
}