#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
int n, aux, atr=0, cmp=0;
int atr2=0, cmp2=0;
int atr3=0, cmp3=0;
//selection
void sort1 (int a[], int n) {
    for (int i=0; i<n; i++) {
            int min = i;
            for (int j=i+1; j<n; j++) {
                if (a[j] < a[min])
                    min = j;
            }
            if (min != i) {
                int aux = a[i];
                a[i] = a[min];
                a[min] = aux;
                atr+=3;
            }
            cmp++;
        }
}
void sort2 (int  a[], int n) {
    for (int i=0; i<n; i++){
            int j = i;

            while (j>0 && a[j-1] > a[j]) {
                aux = a[j];
                a[j] = a[j-1];
                a[j-1] = aux;
                j--;
                atr2+=3;
                cmp2++;
            }
        }
}
void sort3 (int a[], int n) {
    for (int i=0; i<n; i++) {
            for(int j=i+1; j<n; j++) {
                if (a[i] > a[j]) {
                    aux = a[i];
                    a[i] = a[j];
                    a[j] = aux;
                     atr3+=3;
                }
                cmp3++;
            }
        }
}
int main() {

    int i;

    FILE *fp1, *fp2, *fp3;
    int *a, *b, *c;
    fp1 = fopen("data1.txt", "w");
    fp2 = fopen("data2.txt", "w");
    fp3 = fopen("data3.txt", "w");

    for (n=100; n<=10000; n+=100) {
        atr=0; cmp=0;
        a = (int *)malloc(n * sizeof(int));
        b = (int *)malloc(n * sizeof(int));
        c = (int *)malloc(n * sizeof(int));
        for (i=0; i<n; i++)
            a[i] = rand();
        memcpy(b,a,n);
        memcpy(c,a,n);
        sort1(a,n);
        sort2(b,n);
        fprintf(fp1, "%d\t%d\t%d\n", n,atr, cmp);
        fprintf(fp2, "%d\t%d\t%d\n", n,atr2, cmp2);
        fprintf(fp3, "%d\t%d\t%d\n", n,atr3, cmp3);
        free(a);
    }
    return 0;
}