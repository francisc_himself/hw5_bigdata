
/*
	Prof. Vlad Baja
	***ANONIM*** Mariana ***ANONIM***, gr ***GROUP_NUMBER***
	      Dupa cum reiese si din tabel, sortarea cu ajutorul Heap-ului este una foarte eficienta, in comparatie cu QuickSort.
	Asadar numarul de comparatii facute pe parcursul sortarii QuickSort este vizibil mult mai mic decat cel calculat din rularea
	sortarii Heap-ului. Eficienta in cazul mediu statistic pentru ambele sortari este O(n*log2n).
	*/


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
#define max 10000

FILE * fout=fopen("quick.csv","w");

int a[max],b[max],n;
int nrcomp,nrasig; // contorul operatiilor pentru quickSORT

int asig_bu, comp_bu;//contor pentru heap sort
int dimA;


void quickSort ( int A[], int lo, int hi)
{

    int i=lo, j=hi, h;
    int x=A[(lo+hi)/2]; 
	nrasig++;

    
    do
    {   nrcomp+=2;
		while (A[i]<x)
		{
			i++;
		}
		while (A[j]>x)
		{
			j--;
		}
        if (i<=j)
        {
            h=A[i]; A[i]=A[j]; A[j]=h;
            i++; j--;
			nrasig+=3;
        }
    } while (i<=j);

   
    if (lo<j) quickSort(A, lo, j);
    if (i<hi) quickSort(A, i, hi);
}





int parinte(int i)
{
	return i/2;
}

int stanga(int i)
{
	return 2*i;
}

int dreapta(int i)
{
	return 2*i+1;
}

void initializare_heap()
{
  int dim_heap=0;
}

void recon_heap(int A[],int i)
{
	int l,r,min,temp;
	l=stanga(i);
	r=dreapta(i);
	
	if( (l<=dimA) && (A[l]<=A[i]) )
	{
		min=l;
	   
       }
	else
	{
		min=i;
		
	}
	
	if( (r<=dimA) && (A[r]<=A[min]) )
	{
		min=r;
		
	}
	comp_bu++;
	if(min != i)
	{
		temp=A[i];
		A[i]=A[min];
		A[min]=temp;
		asig_bu=asig_bu+3;
		recon_heap(A,min);
	}
}
  


void constructie_bu(int A[],int dim_a)
{
	
    int i,n,dim_heap;
	
	n=dim_a/2;
	asig_bu=asig_bu+2;
	for(i=n;i>=1;i--)
	   recon_heap(A,i);
}

void heapSort(int A[],int dimA)
{
	constructie_bu( A, dimA);
	for (int i=dimA;i>=2;i--)
	{
		int aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		nrasig+=3;
        dimA=dimA-1;
		recon_heap(A,1);
	}
}

void generare_sir(int *A,int m)
{
	int i;
	
	for(i=1;i<=m;i++)
		A[i]=rand()% 10; 
}


int main()
{	
	 int  comp,co;
int A[10000],CO[10000];
	FILE *f;
	int i,n,dim;
		for(int x=100;x<=10000;x=x+200)
		{	
			comp=0;
			co=0;

			for(int i=1;i<=5;i++)
			{
				n=x;
				srand(100*x);
				for(i=1;i<=n;i++)
				{ 
					a[i]=rand();
					b[i]=a[i]; 
				}

				
				quickSort (b,0,n-1);
	             initializare_heap();
	            heapSort(b,n);	             
				
				comp+=nrcomp+nrasig;//comparatiile la sortare quick
				co+=comp_bu+asig_bu;//comparatiile la sortare heap
		
			}

			
			fprintf(fout," %d, %d, %d \n",n,comp/5,co/5);
			
		}
		fclose(fout);

	 printf("Lungimea heap-ului : ");
scanf("%d",&dimA);
generare_sir(A,dimA);
printf("sirul generat: \n");
for (i=1;i<=dimA;i++)
		printf("%d ",A[i]);
printf("heap-ul: \n");
initializare_heap();
  constructie_bu(A,dimA);
	for (i=1;i<=dimA;i++)
		printf("%d ",A[i]);

	quickSort(A,1,dimA);
	printf("quicksort: ");
for (i=1;i<=dimA;i++)
		printf("%d ",A[i]);

initializare_heap();
  heapSort(A,dimA);
	for (i=1;i<=dimA;i++)
		printf("\n%d ",A[i]);
getch();
		return 0;
}
