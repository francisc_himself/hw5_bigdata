#include<stdlib.h>
#include<conio.h>
#include<stdio.h>
#include "Profiler.h"

/*--------------Evaluare----------------------------

Pe baza graficului putem observa ca intr-adevar 
quick sort este o metoda mai ineficienta fata de
heap-sort, care are comlexitatea n*log(n), fata de 
n^2 al quick_sortului.  
----------------------------------------------------
*/
int a[1000],BUcount,QScount,dim_h;


int partitionare(int a[],int st,int dr)
{
	int x,i,j,aux;
	x=a[dr];
	i=st-1;
	for(j=st;j<dr;j++)
	{	
			if(a[j]<=x)
	     {   QScount++;
			 i=i+1;
	      aux=a[i];
		  a[i]=a[j];
		  a[j]=aux;
		  QScount+=3;
			}
	     }
	 aux=a[i+1];
	 a[i+1]=a[dr];
     a[dr]=aux;
	 
	 return i+1;
	
}
void quick_sort(int a[],int st,int dr)
{
	int q;
	if(st<dr)
	{
		q=partitionare(a,st,dr);
		quick_sort(a,st,q-1);
		quick_sort(a,q+1,dr);
	} 
}
int left(int i) {
  return (2 * i) ;
}

int right(int i) {
  return (2 * i) + 1;
}
int parent(int i) {
	return (i / 2);
}
void heapify(int a[],int i) 
{
  int smallest;
  int l=left(i);
  BUcount++;
  int r=right(i);
  if ((l<=dim_h)&&(a[l]>a[i])) 
  {
    smallest=l;
  }
  else 
  {
    smallest=i;
  }
  BUcount++;
  if ((r<=dim_h)&&(a[r]>a[smallest])) 
  {
    smallest = r;
  }
  if (smallest!=i) 
  {
	  BUcount+=3;
    int aux=a[i];
    a[i]=a[smallest];
    a[smallest]=aux;
    heapify(a,smallest);
  }
}
void constr_heap_bu(int a[],int n)
{
	int i;
	dim_h=n;
	for (i=n/2;i>0;i--) 
	{
		heapify(a,i); 
		
	}
}
void heap_sort(int a[],int n)
{   int aux;
	constr_heap_bu(a,n);
  for(int i=n;i>=2;i--)
 { aux=a[i];
  a[i]=a[1];
  a[1]=aux;
  BUcount+=3;
  
dim_h--;
heapify(a,1);
  }
}


void main()
{
	 FILE *f;
	int a[30],b[30],quick_medie=0,heap_medie=0,k;
	int i,j;
	int n=10;
	for(i=1;i<=n;i++)
		{printf("a[%d]=",i);
	scanf("%d",&a[i]);
	b[i]=a[i];
	}
	quick_sort(a,1,10);
	heap_sort(b,10);
	for(i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n");
	for(i=1;i<=10;i++)
		printf("%d ",b[i]);
	
	
    printf("Working...\n");
	f = fopen("rezultate.csv","w");
	fprintf(f, "n,mediu_quick_sort,mediu_heap_sort\n");
    for(n=100;n<=10000;n+=100)
	{fprintf(f, "%d,", n);
		printf("%d\n", n);
		 int *vect_primar=(int *)malloc(n*sizeof(int));
		 int *vect_quick=(int *)malloc(n*sizeof(int));
		 int *vect_heap=(int *)malloc(n*sizeof(int));
	
		for(j=0;j<5;j++)
		{ BUcount=0;
		 QScount=0;
			FillRandomArray(vect_primar,n,10,50000,true,0);

		 for(k=1;k<=n;k++)
		     {
				 vect_quick[k]=vect_primar[k];
		         vect_heap[k]=vect_primar[k];
		     }
		 
		
		 quick_sort(vect_quick,1,n);
		 quick_medie+=QScount;
		
		 heap_sort(vect_heap,n);
		 heap_medie+=BUcount;
		 
		}
    fprintf(f, "%d,%d\n",quick_medie/5,heap_medie/5);
	}  
	getche();

}


