
// laborator 10.cpp : Defines the entry point for the console application.
// am fost ajutat de Bonczidai Levente,gr ***GROUP_NUMBER***


#include "stdafx.h"
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Profiler.h"

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define V_MAX 10000
#define E_MAX 60000

#define inainte 0
#define inapoi 1
#define transv 2
#define arbore 3

typedef struct
{
	int parrent;
	int distance;
	int color;
	int f;
	int d;

}GRAPH_NODE;

typedef struct _NODE
{
	int key;
	struct _NODE *next;
	int tip;
}NODE;

NODE *adj[V_MAX];
GRAPH_NODE graph[V_MAX];

int timp=0;

int nr_v;
int nr_e;

int edges[E_MAX];

void adaugamuchie(int a,int b)
{
	NODE *nod= new NODE;
	nod->key=b;
	nod->next=adj[a];
	adj[a]=nod;
}
//n=noduri m=muchii
void generate(int n,int m)
{
	
	FillRandomArray(edges,m,10,n*n-1,true);

	for(int i=0;i<m;i++)
	{
		int a=edges[i]%n;
		int b=edges[i]/n;
		adaugamuchie(a,b);
	}
}

void print_adi()
{
	printf("\n\n adiacenta list \n");
	for (int i=0;i<nr_v;i++)
	{
		printf("\n %d ",i);
		NODE *p=adj[i];
		while((p) !=0)
		{
			printf("%d ",p->key);
			p=p->next;
		}
	}
}
//sterge lista de adiacenta
void clear_edge()
{
	for (int i=0;i<nr_v;i++)
	{
	NODE *p=adj[i];
		while(p !=0)
		{
			NODE *aux=p;
			p=p->next;
			delete aux;
		}
	adj[i]=0;
	}
}



void DFSvisit(int u)
{
	
	timp=timp+1;
	graph[u].d=timp;
	graph[u].color=GRAY;
	
	NODE *p=adj[u];
	while(p!=0)
	{
		if (graph[p->key].color==WHITE)
		{
			graph[p->key].parrent=u;
			DFSvisit(p->key);
		}
		
		p=p->next;
	}
	graph[u].color=BLACK;
	timp++;
	graph[u].f=timp;

}

void DFS()
{
	for(int i=0;i<nr_v;i++)
	{
		graph[i].color=WHITE;
		graph[i].parrent=-1;
	}
	
	timp=0;
	for(int i=0;i<nr_v;i++)
	{
		if (graph[i].color==WHITE)
		{
		DFSvisit(i);
		}
	}
}

void print_parrent(int s)
{
	printf("\n print parrent");
	for (int i=0;i<nr_v;i++)
	{
		printf("%d ",graph[i].parrent);
	}

}

void settip()
{
	for (int i=0;i<nr_v;i++)
	{
		NODE *p=adj[i];
		while (p !=0)
		{
			if (graph[p->key].parrent==i)
			{
				p->tip=arbore;
				printf("intre %d si %d este arc de tip arbore\n",i,p->key);
			}
			else if (graph[p->key].d>graph[i].d && graph[p->key].f<graph[i].f)
			{
				p->tip=inapoi;
				printf("intre %d si %d este arc de tip inapoi\n",i,p->key);
			}
			else if(graph[p->key].d>graph[i].f)
			{
				p->tip=transv;
				printf("intre %d si %d este arc de tip transversal\n",i,p->key);
			}
			else
			{
				p->tip=inainte;
				printf("intre %d si %d este arc de tip inainte\n",i,p->key);
			}
			p=p->next;
		}
	}
	
}

int main ()
{
	memset(adj,0, V_MAX*sizeof(NODE*));
	
	// test generate
	nr_e=7;
	nr_v=10;
	generate(nr_v,nr_e);
	print_adi();
	
	clear_edge();

	//-------------------------

	nr_e=9;
	nr_v=8;

	adaugamuchie(0,1);
	adaugamuchie(0,2);
	adaugamuchie(2,3);
	adaugamuchie(2,4);
	adaugamuchie(2,5);
	adaugamuchie(4,6);
	adaugamuchie(5,6);
	adaugamuchie(2,7);
	adaugamuchie(7,6);

	print_adi();
	//test BFS


	DFS();

	print_parrent(0);

	settip();
	clear_edge();
	//-------------------------
}




