#include <iostream>
#include<stdlib.h>
using namespace std;

typedef struct Nod
{
    int inf;
    int size;
    Nod *st, *dr;
};

Nod *rad,*tata;

int n,m,comp,as;

void inserare ( Nod *&rad, int nr)
{
    if(rad==NULL)
    {
        rad= new Nod;
        rad->inf=nr;
        rad->size=1;
        rad->st=NULL;
        rad->dr=NULL;
    }
    else
        if(nr<rad->inf)
        {
            rad->size++;
            inserare(rad->st,nr);
        }
        else
        {
            rad->size++;
            inserare(rad->dr,nr);
        }
}

void divide ( int st, int dr)
{
    int mijl;
    if(st<=dr)
    {
        mijl=(st+dr)/2;
        inserare(rad,mijl);
        divide(st,mijl-1);
        divide(mijl+1,dr);
    }

}

void stergere ( Nod *p, int val)
{
    if(p==NULL) cout<<"arbore vid";
    else
        if(p->inf>val)
        {
           tata=p;
           tata->size--;
           stergere(p->st,val);
        }
        else
            if(p->inf<val)
            {
                tata=p;
                tata->size--;
                stergere(p->dr,val);
            }
            else
            {
                if(p->st==NULL && p->dr==NULL)
                {

                    if(tata->st==p)
                    {
                        tata->st=NULL;
                    }
                    if(tata->dr==p)
                    {
                        tata->dr=NULL;
                    }
                }

                else if(p->st==NULL || p->dr==NULL)
                {
                    if(tata->dr==p)
                        if(p->st==NULL)
                            tata->dr=p->dr;
                        else
                            tata->dr=p->st;
                    else
                        if(p->st==NULL)
                                tata->st=p->dr;
                            else
                                tata->st=p->st;

                }
                else if(p->st!=NULL && p->dr!=NULL)
                {
                    Nod *tata2=new Nod;
                    Nod *aux = new Nod;
                    p->size--;
                    aux=p;
                    p=p->st;
                    p->size--;
                    if(p->dr==NULL)
                        if(p->st==NULL)
                        {
                            aux->inf=p->inf;
                            aux->st=NULL;
                        }
                        else
                        {
                            aux->inf=p->inf;
                            aux->st=p->st;
                        }
                    while(p->dr!=NULL)
                    {
                        tata2=p;
                        p=p->dr;
                        p->size--;
                    }

                    aux->inf=p->inf;
                    if(p->st!=NULL)
                        tata2->dr=p->st;
                    else
                        tata2->dr=NULL;

                   }
            }
}

int pozitie(int p, int n, int m)
{
    int poz;
    poz=(p-1+m)%n;
    if(poz!=0)
        return poz;
    else
        return n;
}

int valoare (Nod *rad, int k, int n)
{
    int c;
    if(rad->st==NULL)
        c=rad->size;
    else
        c=rad->st->size+1;
    if(k==c)
        return rad->inf;
    else
        if(k<c)
        {
            return valoare(rad->st,k,n);
        }
        else
        {
            return valoare(rad->dr,k-c,n);
        }

}


void afisare ( Nod *rad , int nivel)
{
    if(rad!=NULL)
    {
        afisare(rad->dr,nivel+1);
        for(int i=0;i<=nivel;i++)
            cout<<"  ";
        cout<<rad->inf;
        cout<<endl;
        afisare(rad->st,nivel+1);
    }
}

void josephus ( int n, int m)
{
    divide(1,n);
    afisare(rad,0);
    int poz=m,val;
    val=valoare(rad,poz,n);
    cout<<"valoare de sters: "<<val;
    cout<<endl<<endl<<endl;
    stergere(rad,val);
    afisare(rad,0);
    n--;
    while(n>0)
    {
        poz=pozitie(poz,n,m);
        val=valoare(rad,poz,n);
        cout<<"valoare de sters: "<<val;
        cout<<endl<<endl<<endl;
        stergere(rad,val);
        afisare(rad,0);
        n--;
    }
}

int main()
{
    tata = new Nod;
    josephus(7,3);
    return 0;
}
