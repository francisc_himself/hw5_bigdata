
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct node{
int val;
node **children; //array of children of this node
int nrch;
}node;

typedef struct BSTNode
{
    BSTNode *left ;
    BSTNode *right;
    BSTNode *parent;
    int size;
    int val;
}BSTNode;

void pretty_print(BSTNode *x, int d)
{
    if (x!=NULL)
    {
		 for (int i=0;i<d;i++)
        {
            printf("     ");
        }
        printf("%d \n",x->val);
        //cand meri la stanga cobori un nivel d+1
        pretty_print(x->left,d+1);

		//printf("%d \n",x->val);
        pretty_print(x->right, d);
        //cand nu ramai
    }
}

void print_multi(node *x){
	if(x!=NULL){
		printf("%d ",x->val);
		if(x->nrch!=0)
		for(int i=0;i<x->nrch;i++){
			print_multi(x->children[i]);
		}
	}
}

/*
transform from R1 to R2(parent repr-->multiway representation)
*/
void transform(node *x, BSTNode * root,node* parent,int index){
	if(x!=NULL){
		if (x->nrch>0){
			root->left=new BSTNode;
			root->left->val=x->children[0]->val;/////////////////////////////1 in our example--ia prima val din sirul de copii ai nodului x(= fiul stang)
			transform(x->children[0],root->left,x,0);// apoi merge pe cate un nivel tot pe fiul stang
		}
	for(int i=0;i<x->nrch-1; i++){

		root->right=new BSTNode;
		root->right->val=x->children[i+1]->val;//// apoi iau de la children[1] toate valorile, ca fiind right child pt root(ex: 4, 5)
	}

	if ((parent!=NULL)&&(index<parent->nrch-1))
		transform(parent->children[index+1],root->right,parent,index+1);
	}
}


/*
transform from R2 to R3(multiway representation-->binary representation)
*/
BSTNode* transform2(node *T, node *parent, int ch){
BSTNode *p;
p = (BSTNode *)(malloc(sizeof(BSTNode)));// p - nodul din arborele binar care ii construit in paralel
p->val = T->val;//copy the value from multiway tree in BST

//if is not a leaf
if(T->nrch > 0){
    //fiul stang al nodului din bst ii radacina arborelui dat de primul fiu stang al nodului curent
	p->left = transform2(T->children[0], T, 0);// memoreaza left child pt fiecare nod
}
	else p->left = NULL;//daca e frunza
//if the parent is not null and there are still children to verify==> the right child becomes the brother of this node.
if(parent && ch < parent->nrch - 1)
    //fiul drept devine fratele
	p->right = transform2(parent->children[ch + 1], parent, ch + 1);//the right child begins from position 1 in the children array
	else p->right = NULL;
return p;
}


BSTNode *root= new BSTNode;


int main(){
	int N=9;
	int root_poz=0;
	
	int a[]={1, 6, 4, 1, 6, 6, -1, 4, 1};// 
//	int a[]={2,7,5,2,7,7,-1,5,2};
	
	printf("PARENT VECTOR R1: ");
	for(int i=0;i<N;i++)
		printf("%d ",a[i]);
		printf("\n");
	
	node *tree=new node[N]; // array of nodes
	int *nrch =new int[N];
	
	//initialize the root and the nodes
	for(int i=0;i<N;i++){
		if(a[i]==-1) root_poz=i; // save the position of the root
		tree[i].nrch=0;
		tree[i].val=i; //the nodes in ascending order: 0,1,2,..,9
	}

	//count number of children for each node
	//i-child , a[i] parent
	for(int i=0; i<N; i++){
		int aux=a[i];
		tree[aux].nrch++;// i is a child of a[i]----ex: 6 has 3 children
	}

	//initialize arrray of children for each node;
	//aloca memorie pentru sirul de copii
	for(int i=0; i<N;i++){
		if(tree[i].nrch>0)
				tree[i].children=new node*[tree[i].nrch]; // initialize the array of children for each node
		
		else tree[i].children=NULL;
		//initialize number of children found foreach node
		nrch[i]=0;
		// contor pentru pozitia in cadrul sirului de copii
	}

	/*
		the node a[i] is a parent
		its child with number nrch[i] (nrch is the number of children
		already found), becomes the element from the initial
		array of nodes from position i
		--a[i] is the parent of i--
		nrch[a[i]] = the number of children of node a[i]
		*/

	for(int i=0; i<N; i++){
		if(a[i]>=0) 
			tree[a[i]].children[nrch[a[i]]]=&tree[i];// aici asignez fiecare copil la parintele sau
		//increase the number of children
		nrch[a[i]]++;
			}

//transform the parent vector in a multiway tree
transform(&tree[0],root,NULL,0);
printf("\nmultiway tree representation:\n");
print_multi(&tree[root_poz]);
printf("\n");

//transform the multiway tree in a binary tree
root=transform2(&tree[root_poz], NULL, 0);
printf("\n(Pretty print) Binary tree representation:\n");
pretty_print(root,0);

getch();
return 0;
}
