#include <iostream>
#include <conio.h>
#include "Profiler.h"

#define MAX_SIZE 10000

using namespace std;

/**
student: ***ANONIM*** ***ANONIM***, group ***GROUP_NUMBER***

For best case, assignments for the Bubble method and comparisons for the Insertion method are a constant function, of value 0.
In terms of overall operations in best case, Bubble behaves the best and Selection the worst.

For worst case, assignments for the Selection method are the least, this method being the most efficient, followed by Insertion. Bubble is the worst 
performing method. For overall operations, Selection is still the best.

For average case, Selection and Insertion are close as best, Bubble being the worst.

**/
Profiler profiler("lab2");

void printArray(int A[],int n){
	int k;
	for(k=0;k<n;k++)
		cout<<A[k]<<" ";
	cout<<endl;
}

void LinearInsertionSort(int A[], int n){
	int i, j, buff;

	profiler.countOperation("compL", n, 0);
	for(i=1; i<n; i++){
		buff = A[i];
		profiler.countOperation("assignL", n, 1);
		j=i-1;
		while(A[j]>buff && j>=0){
			A[j+1]=A[j];
			profiler.countOperation("assignL", n, 1);
			profiler.countOperation("compL", n, 1);
			j=j-1;
		}
		A[j+1] = buff;
		profiler.countOperation("assignL", n, 1);
	}
}
void SelectionSort(int A[], int n){
	int j, pos, i, aux;
	profiler.countOperation("compS", n, 0);
	for(j=0; j<n-1; j++)
	{
		pos = j;
		for(i=j+1;i<=n;i++){
			profiler.countOperation("compS", n, 1);
			if(A[i]<A[pos])
				pos = i;
		}
		if(j!=pos){
			aux = A[j];
			A[j]=A[pos];
			A[pos]=aux;
			profiler.countOperation("assignS", n, 3);
		}
	}
}

void BubbleSort(int A[], int n){
	int swapped = 1, aux;

	do {
		swapped = 0;
		for(int i=0;i<n-1;i++){
			profiler.countOperation("compB", n, 1);
			profiler.countOperation("assignB", n, 0);
			if(A[i]>A[i+1])
			{
				aux = A[i];
				A[i]=A[i+1];
				A[i+1]=aux;
				profiler.countOperation("assignB", n, 3);
				swapped = 1;
			}
		}
	} while(swapped == 1);
}

void generateBestCase(int A[], int n){
	int i;
	for(i=0;i<n;i++)
		A[i] = i;
}

void generateWorstCase(int A[], int n){
	int i;
	for(i=0;i<n;i++){
		A[i] = n-i;
	}
}

void SelectionWorstCase(int A[], int n){
	int i;
	for(i=1;i<(n-1);i++)
		A[i] = i;
	A[n-1] = 0;
}

int main(){

	int A[MAX_SIZE], i=1, j=15, n;
	
	/*for(n=100; n < MAX_SIZE; n+=100){
		generateBestCase(A, n);
		LinearInsertionSort(A, n);
		generateBestCase(A, n);
		SelectionSort(A,n);
		generateBestCase(A, n);
		BubbleSort(A,n);
	}

	profiler.createGroup("BestCaseComparisons", "compL", "compS", "compB");
	profiler.createGroup("BestCaseComparisons-Insertion", "compL");
	profiler.createGroup("BestCaseComparisons-Selection", "compS");
	profiler.createGroup("BestCaseComparisons-Bubble", "compB");
	profiler.createGroup("BestCaseAssignments", "assignL", "assignS", "assignB");
	profiler.createGroup("BestCaseAssignments-Selection", "assignS");
	profiler.createGroup("BestCaseAssignments-Insertion", "assignL");
	profiler.createGroup("BestCaseAssignments-Bubble", "assignB");
	profiler.addSeries("BestCaseOperations-Insertion","compL", "assignL");
	profiler.addSeries("BestCaseOperations-Selection","compS", "assignS");
	profiler.addSeries("BestCaseOperations-Bubble","compB", "assignB");
	profiler.createGroup("BestCaseOperations", "BestCaseOperations-Insertion", "BestCaseOperations-Selection", "BestCaseOperations-Bubble");
	profiler.showReport();
	*/
	
	/*for(n=100; n < MAX_SIZE; n+=100){
		generateWorstCase(A,n);
		LinearInsertionSort(A, n);
		selectionWorstCase(A, n);
		SelectionSort(A,n);
		generateWorstCase(A,n);
		BubbleSort(A,n);
	}

	profiler.createGroup("WorstCaseComparisons", "compS", "compB", "compL");
	profiler.createGroup("WorstCaseComparisons-Insertion", "compL");
	profiler.createGroup("WorstCaseAssignments", "assignL", "assignS", "assignB");
	profiler.createGroup("WorstCaseAssignments-Bubble", "assignB");
	profiler.addSeries("WorstCaseOperationsOnInsertion","compL", "assignL");
	profiler.addSeries("WorstCaseOperationsOnSelection","compS", "assignS");
	profiler.addSeries("WorstCaseOperationsOnBubble","compB", "assignB");
	profiler.createGroup("WorstCaseOperations-Insertion", "WorstCaseOperationsOnInsertion");
	profiler.createGroup("WorstCaseOperations-Bubble", "WorstCaseOperationsOnBubble");
	profiler.createGroup("WorstCaseOperations-Selection","WorstCaseOperationsOnSelection");
	profiler.createGroup("WorstCaseOperations", "WorstCaseOperationsOnInsertion", "WorstCaseOperationsOnSelection", "WorstCaseOperationsOnBubble");
	profiler.showReport();*/

	while(i<=5){
		for(n=100; n < 10000; n+=100){
			FillRandomArray(A, n);
			LinearInsertionSort(A, n);
			FillRandomArray(A, n);
			SelectionSort(A,n);
			FillRandomArray(A, n);
			BubbleSort(A,n);
		}
		i++;
	}

	profiler.createGroup("AverageCaseComparisons", "compB", "compS", "compL");
	profiler.createGroup("AverageCaseComparisons-Insertion", "compL");
	profiler.createGroup("AverageCaseAssignments", "assignL", "assignS", "assignB");
	profiler.createGroup("AverageCaseAssignments-Insertion", "assignL");
	profiler.createGroup("AverageCaseAssignments-Bubble", "assignB");
	profiler.createGroup("AverageCaseAssignments-Selection", "assignS");

	profiler.addSeries("AverageCaseOperations-Insertion","compL", "assignL");
	profiler.addSeries("AverageCaseOperationsOnSelection","compS", "assignS");
	profiler.addSeries("AverageCaseOperationsOnBubble","compB", "assignB");
	profiler.createGroup("AverageCaseOperations", "AverageCaseOperationsOnSelection", "AverageCaseOperationsOnBubble", "AverageCaseOperations-Insertion");
	profiler.showReport();
	
	_getch();
	return 0;
}