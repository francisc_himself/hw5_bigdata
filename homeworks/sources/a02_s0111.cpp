#include <stdio.h>
#include <conio.h>
#include "Profiler.h"
#define LEFT(i) 2*i+1;
#define RIGHT(i) 2*i+2;
/*
Heap sort whether implemented bottom-up or top-down is an optimal algorithm.
1) In the bottom-up (leafs -> root) implementation,the array size is known and the algorithm starts with the last node that has at leat one leaf (n/2 -1 if array starts with 0 index) and decreases to the root;
for each node it sinks the element it contain if the heap condition is not satisfied
2) In the top-down (root -> leafs) implementation the array is initially empty and filled with values using a push method;each value is bubbled upwards to the root untill it satisfy the heap condition
Advantages:Bottom-up method is faster than top-down approach; top-down deals with variable dimension arrays
Disadvantages:Bottom-up approach works with fixed dimension arrays; top-down is slower
In average case both build heap methods have logarithmic time complexity, O(nlgn); bottom-up is faster than top-down
*/

Profiler profiler("HW2");

void heapify(int a[],int i,int size,int *op) {
   int l,r,largest,temp;
   l=LEFT(i);
   r=RIGHT(i);
  
  (*op)++;
  if (l<=size && a[l]>a[i] ) 
	largest=l;
  else 
	largest=i;
 
  (*op)++;
  if ( r<=size && a[r]>a[largest] )  
	largest=r;
 
  if ( largest != i ) {
	  temp=a[i];
	  a[i]=a[largest];
	  a[largest]=temp;
	  *op+=3;
	  heapify(a,largest,size,op);
  }
}

void buildHeap1(int a[],int size,int *op) {
	 for (int j=(size/2)-1;j>=0;j--) {
		heapify(a,j,size,op);	
	 }
}

int getParent(int pos) {
	float i,j;
	i=(pos-1)/2;
	j=(pos-2)/2;
	if (pos>0) {
		if ( i==(int)i ) 
			return (int)i;
		else if ( j==(int)j )
			return (int)j;
	}
	return 0;
}


void pushHeap(int b[],int el,int pos,int *op) {
	int p=getParent(pos),aux;
	b[pos]=el;
	*op+=2;
	while (b[p] < b[pos]) {
		aux=b[p];
		b[p]=b[pos];
		b[pos]=aux;
		pos=p;
		p=getParent(pos);
		*op+=4;
	}
}

void buildHeap2(int a[],int b[],int size,int *op) {
	for (int i=0;i<size;i++) 
		pushHeap(b,a[i],i,op);
}

void prettyPrint(int a[], int pos, int level,int size)
{
 if(pos < size)
 {
  prettyPrint(a, 2*pos+1, level+1,size);
  for(int i=1;i<=7*level;i++)  
	  printf(" ");
  printf("%d\n", a[pos]);
  prettyPrint(a, 2*pos, level+1,size);
 }
}
int main() {
	int op1,op2,sum1,sum2;
	//int a[],b[10000];
	/*for (int n=100;n<=10000;n+=100) {
		sum1=sum2=0;
		for(int j=1;j<=5;j++) {
			FillRandomArray(a,n,10,10000,false,0);
			op1=op2=0;
			buildHeap2(a,b,n,&op2);
			buildHeap1(a,n,&op1);
			sum1+=op1;
			sum2+=op2;
		}
		profiler.countOperation("bottom_op",n,sum1/5);
		profiler.countOperation("top_op",n,sum2/5);
	}	
	profiler.createGroup("average_case","bottom_op","top_op");
	profiler.showReport();*/
	int a[]={3,7,2,4,8,0,5,1,3,2};
	int b[10];
	buildHeap2(a,b,10,&op2);
	buildHeap1(a,10,&op1);

	for(int i=0;i<10;i++) {
		//printf("%d ",a[i]);
	}
	for(int i=0;i<10;i++) {
		//printf("%d ",b[i]);
	}
	printf("bottom-up\n");
	prettyPrint(a,1,0,10);
	printf("top-down\n");
	prettyPrint(b,1,0,10);
getch();
return 0;
}


