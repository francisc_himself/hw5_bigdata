

#include <conio.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <limits>

#define MAX 101

using namespace std;

 
enum colors {BLACK, WHITE, GRAY};

int color[MAX], d[MAX], p[MAX], f[MAX], t, vertex, edge;


void DFS_VISIT(vector<int> G[], int u)
{
	t = t + 1;
    d[u] = t;
    color[u] = GRAY;
   
	for(int v=0; v<G[u].size(); v++) 
	{
        if(color[G[u][v]] == WHITE)
		{
            p[G[u][v]] = u;
            DFS_VISIT(G,G[u][v]);
        }
    }

    color[u] = BLACK;

    t = t + 1;

    f[u] = t;

}


void DFS(vector<int> G[])
{

    for(int u=0; u<=vertex; u++)
	{
        color[u] = WHITE;
        p[u] = NULL;
    }

    t = 0;

    for(int u=1; u<=vertex; u++) 
	{
        if(color[u] == WHITE) 
		{
            DFS_VISIT(G,u);
        }
    }
}

 

 

int main()

{
    vector<int> adjList[MAX];

    int u, v;
    cin >> vertex >> edge;

    for(int e=1; e<=edge; e++)
	{
        cin >> u >> v;
        adjList[u].push_back(v);
    }

    DFS(adjList);

    for(int v=1; v<=vertex; v++) 
	{
        cout << v <<"   "<< d[v] <<" / " << f[v] << endl;
    }

    getch();

}
