/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
  FA - Assignment 1 - Direct Sorting Methods*/


#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "math.h"
#include "time.h"



/**
*In the worst case the 3 methods have almost the same evolution. From the values of the charts it is clear that the order of methods is: 
bubble, selection, insertion (descending order of the number of assignments and comparisons). This means that insertions sort acts better
than the others in the worst case. 

In the best case, the selection and insertion methods are quite similar, but bubble sort does all the possible combination when comparing
two numbers from the array. That makes bubble sort slower than the others even in the best case. 

For average cases bubble sort has the greatest values for done comparisons and assignments. The selection and insertion methods have 
close values for comparisons, but selection method does too many assignments for the given arrays.

In conclusion, the insertion method seems to act very well in all 3 cases, compared to the other two methods.

*/

int n, a[100001], b[100001];
int as_s, comp_s; // number of assgments and comparisons done by the selection method
int as_i, comp_i; //number of assgments and comparisons done by the insertion method
int as_b, comp_b;  //number of assgments and comparisons done by the bubble method
int comp_total_i, as_total_i, comp_total_s, as_total_s,  comp_total_b, as_total_b;
int i, j;
FILE *f;

void insertion (int n)
{
  int i,j,aux;
  comp_i=0;
  as_i=0;
  a[0]=-10000;
  for(i=1;i<=n;i++)
  {
	aux=a[i];
    as_i++;
	j=i-1;
	while((aux<a[j]) && (j>=0)) 
	{ 
		a[j+1]=a[j];
	    j--;    
	    as_i++;
	    comp_i++;
	}
	a[j+1]=aux;
	as_i++;
	comp_i++;
  }
  	
}

void selection (int n)
{
  int i, j, aux, min;
  comp_s=0;
  as_s=0;
  for(i=1;i<=n-1;i++)
  {
	  min=i;
      for(j=i+1;j<=n;j++)
	  {
		  if(a[min]>a[j]) { min=j;  comp_s++;   }
	       if(min!=i) 
		   {   aux=a[i];
		       a[i]=a[min];
		       a[min]=aux; 
		       as_s=as_s+3;  }
	        }
	    comp_s++;
  }

}

void bubble (int n)
{

int i,j, aux;
comp_b=0;
as_b=0;
for(i=1;i<n;i++)
	for(j=i+1;j<=n;j++)
	{ 
		 if(a[i]>a[j])
			 {  
				 aux=a[i];
				 a[i]=a[j];
				 a[j]=aux;
				 as_b=as_b+3;
				 //comp_b++; nu treb
			}
		comp_b++; 
	} 
}



void generate_average (int n)
{
    for(int i=1;i<=n;i++)
	     b[i]=rand()%9999;
}

void generate_best (int n)
{
    for(int i=1;i<=n;i++)
	     a[i]=i;
}

void generate_worst1 (int n)
{
    for(int i=1;i<=n;i++)
	     a[i]=n-i;
}

void generate_worst2 (int n)
{
	int j, i;
    for(i=2, j=1;j<=n/2;i=i+2, j++)
		 a[j]=i;
	     
	for(i=n/2+2; j<=n; j++, i=i-2)
		a[j]=i;
}


int main ()
{
     srand(time(NULL));
	 printf("\nBest casses:\n");
	 f=fopen("best.txt","w");
	 fprintf(f,"n as_i comp_i tot_i as_s comp_s tot_s as_b comp_b tot_b\n");
	 for(n=100; n<=10000; n=n+100) //n=100; n<=10.000; n=n+100;
	 {  
		       generate_best(n);
			   insertion(n);
			   generate_best(n);
			   selection(n);
			   //comp_total_s += comp_s ;
			   //as_total_s+=as_s;
			   generate_best(n);
			   bubble(n);

		       printf("\nBest results for an array of %d numbers insertion method: ", n);
		       printf("\ncomparations: %d ", comp_i);
	           printf("\nassignments: %d ", as_i);
			   printf("\nBest results for an array of %d numbers selection method: ", n);
			   printf("\ncomparations: %d ", comp_s);
	           printf("\nassignments: %d ", as_s);
			   printf("\nBest results for an array of %d numbers bubble method: ", n);
			   printf("\ncomparations: %d ", comp_b);
	           printf("\nassignments: %d ", as_b);

			   fprintf(f,"%d %d %d %d %d %d %d %d %d %d \n",
				   n, as_i, comp_i, as_i+comp_i, as_s, comp_s, as_s+comp_s, as_b, comp_b, as_b+comp_b);
	 }
	 fclose(f);

	 printf("\nWorst casses:\n");
	 as_i=comp_i=as_s=comp_s=as_b=comp_b=0;
	 f=fopen("worst.txt","w");
	 fprintf(f,"n as_i comp_i tot_i as_s comp_s tot_s as_b comp_b tot_b\n");
	 for(n=100; n<=10000; n=n+100) //n=100; n<=10.000; n=n+100;
	 {  
		       generate_worst1(n);
			   insertion(n);
			   generate_worst1(n);
			   bubble(n);
			   generate_worst2(n);
			   selection(n);
			  
		       printf("\nWorst results for an array of %d numbers insertion method: ", n);
		       printf("\ncomparations: %d ", comp_i);
	           printf("\nassignments: %d ", as_i);
			   printf("\nWorst results for an array of %d numbers selection method: ", n);
			   printf("\ncomparations: %d ", comp_s);
	           printf("\nassignments: %d ", as_s);
			   printf("\nWorst results for an array of %d numbers bubble method: ", n);
			   printf("\ncomparations: %d ", comp_b);
	           printf("\nassignments: %d ", as_b);

			   fprintf(f,"%d %d %d %d %d %d %d %d %d %d \n",
				   n, as_i, comp_i, as_i+comp_i, as_s, comp_s, as_s+comp_s, as_b, comp_b, as_b+comp_b);
	 }
	 fclose(f);

	printf("\nAverage casses:\n");
	as_i=comp_i=as_s=comp_s=as_b=comp_b=0;
	f=fopen("avg.txt","w");
	fprintf(f,"n as_i comp_i tot_i as_s comp_s tot_s as_b comp_b tot_b\n");
	for(n=100; n<=10000; n=n+100) //n=100; n<=10.000; n=n+100;
	{
		///lipseste total=0
		as_total_i=comp_total_i=as_total_s=comp_total_s=as_total_b=comp_total_b=0;
	     for(int i=1;i<=5; i++)
				{   
					     generate_average(n);
		                 for(j=1;j<=n;j++)
							      a[j]=b[j];
						 insertion(n);
						 comp_total_i += comp_i ;
						 as_total_i+=as_i;
						 
						 for(j=1;j<=n;j++)
							      a[j]=b[j];
						 selection(n);
						 comp_total_s += comp_s ;
						 as_total_s+=as_s;	

						 for(j=1;j<=n;j++)
							      a[i]=b[i];
						 bubble(n);
						 comp_total_b += comp_b ;
						 as_total_b+=as_b;
	                }

		  printf("\nAverage results for an array of %d numbers for insertion method: ", n);
		  printf("\ncomparations: %d ", comp_total_i/5);
	      printf("\nassignments: %d ", as_total_i/5);
		  printf("\nAverage results for an array of %d numbers for selection method: ", n);
		  printf("\ncomparations: %d ", comp_total_s/5);
	      printf("\nassignments: %d ", as_total_s/5);
		  printf("\nAverage results for an array of %d numbers for bubble method: ", n);
		  printf("\ncomparations: %d ", comp_total_b/5);
	      printf("\nassignments: %d ", as_total_b/5);

		  fprintf(f,"%d %d %d %d %d %d %d %d %d %d \n",
			  n, as_total_i/5, comp_total_i/5, as_total_i/5+comp_total_i/5, 
			  as_total_s/5, comp_total_s/5, as_total_s/5+comp_total_s/5, 
			  as_total_b/5, comp_total_b/5, as_total_b/5+comp_total_b/5);
	}	
	
	fclose(f);

	printf("\n\n The Process Ended Successfully");
	getch();
    return 0;
}


