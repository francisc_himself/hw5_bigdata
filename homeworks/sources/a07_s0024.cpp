#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct tip_nod1 {
	int cheie;
	int nr_fii;
	struct tip_nod1 *fii[10];
} tip_nod1;

typedef struct tip_nod2 {
	int cheie;
	struct tip_nod2 *stanga;
	struct tip_nod2 *dreapta;
} tip_nod2;

int rad;
tip_nod1 *radacina1[100];
tip_nod2 *radacina2;

//transformarea T1
void Transformare1(int parinte[100],int dim)
{
	int i;

	for (i=1;i<=dim;i++)
	{
		radacina1[i]=(tip_nod1*)malloc(sizeof(tip_nod1));//alocarea memoriei pt radacina
		radacina1[i]->cheie=i; //se retine indicele nodului
		radacina1[i]->nr_fii=-1; //se initializeaza la -1 numarul fiilor
	}

	for (i=1;i<=dim;i++) // caut radacina
	{
		if (parinte[i]==-1) rad=i; //rad=cheia radacinii
	}

	for (i=1;i<=dim;i++) // transformare din parinte in copii
	{
		if (parinte[i]!=-1)
		{
			radacina1[parinte[i]]->nr_fii++;//creste numarul fiilor pentru nodul specificat in parinte
			radacina1[parinte[i]]->fii[radacina1[parinte[i]]->nr_fii]=(tip_nod1*)malloc(sizeof(tip_nod1*));
			radacina1[parinte[i]]->fii[radacina1[parinte[i]]->nr_fii]=radacina1[i];//se retin fiii
		}
	}
}
//transformarea T2
tip_nod2* Transformare2(tip_nod1 *par,tip_nod1 *r,int k)
{
	tip_nod2* p=(tip_nod2*)malloc(sizeof(tip_nod2));//se aloca memoria
	p->cheie=r->cheie;
	p->dreapta=0;
	p->stanga=0;
	printf("Nodul:%d are parintele:%d si are %d copii\n",p->cheie,par->cheie,r->nr_fii+1);
	if (r->nr_fii!=-1){
		p->stanga=Transformare2(r,r->fii[0],0);
	}
	else p->stanga=0;
	if (par->nr_fii>k){
		p->dreapta=Transformare2(par,par->fii[k+1],k+1);
	}
	else p->dreapta=0;
	return p;
}


void afisare(tip_nod2 *p,int dim)
{
	printf("%d	",p->cheie);
	if (p->stanga!=0) 
		afisare(p->stanga,dim+1);
	printf("\n");
	for (int i=0;i<dim;i++)
		printf("	");
	if (p->dreapta!=0) 
		afisare(p->dreapta,dim);	
}

int main()
{
	int n,parinte[100];//vectorul p e vectorul de parinti al arborelui, numerotarea incepe de la 0
	int i,j;
	/*printf("dati numarul de noduri n=\n");
	scanf("%d",&n);
	for (i=1;i<=n;i++)
	{
	printf("parinte[%d]=\n",i);
	scanf("%d",&parinte[i]);
	}*/
	n=9;
	parinte[1] = 2;
	parinte[2] = 7;
	parinte[3] = 5;
	parinte[4] = 2;
	parinte[5] = 7;
	parinte[6] = 7;
	parinte[7] = -1;
	parinte[8] = 5;
	parinte[9] = 2;
	//104
	Transformare1(parinte,n);
	printf("		DUPA TRANSFORMAREA 1		\n\n");
	printf("Radacina: %d\n",rad);
	for (i=1;i<=n;i++){
		printf("Nodul %d:",radacina1[i]->cheie);
		for (j=0;j<=radacina1[i]->nr_fii;j++){
			printf("%d ", radacina1[i]->fii[j]->cheie);
		}
		printf("\n");
	}
	printf("\n		IN TIMPUL TRANSFORMARII 2		\n\n");
	tip_nod1* p=(tip_nod1*)malloc(sizeof (tip_nod1));
	p->cheie=-1;
	p->nr_fii=0;
	p->fii[0]=(tip_nod1*)malloc(sizeof(tip_nod1));
	p->fii[0]=radacina1[rad];
	radacina2=Transformare2(p,radacina1[rad],0);
	printf("\n		DUPA TRANSFORMAREA 2		\n\n") ;
	afisare(radacina2,0);

	getch();
	return 0;
}