#include <fstream>
#include <time.h>
#include<conio.h>



#define MAX_SIZE 10001

int dimA;
int dimB;
int contor1_comp; //bu
int contor1_atri;
int contor2_comp;  //td
int contor2_atri;



int P (int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right (int i)
{
	return 2*i+1;
}

void heapify(int *v, int i)   //bu
{
	int l,r,min,temp;
	l=left(i);
	r=right(i);
	
	if(l<=dimB && v[l]<v[i])
		{
			min=l;
			
		}
	else 
	{
		min=i;
		
	}
	
	if (r<=dimB && v[r]<v[min])
	{
		min=r;
	
	}
	
	if (min != i)
	{
		temp=v[i];
		v[i]=v[min];
		v[min]=temp;
		contor1_atri=contor1_atri+3;
		heapify(v,min);
	}
	contor1_comp=contor1_comp+3;
}



void H_Push (int *A, int x)  //td
{
	int i,temp;
	dimA = dimA+1;
	A[dimA]=x;
	i=dimA;
	while(i>1 && A[i]<A[P(i)])
	{
		contor2_comp++;
		contor2_atri=contor2_atri+3;
		temp=A[i];
		A[i]=A[P(i)];
		A[P(i)]=temp;
		i=P(i);
	}
}


void creare(int *v,int n,int low,int high)
{
	int i,aux;
	
	for(i=0;i<n;i++)
	{
		aux=rand()%(high-low+1);
		aux=aux+low;
		v[i]=aux;
	}
}


void copy(int *v1, int *v2,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v2[i]=v1[i];
	}
	
}

void afisare(int A[],int n)
{
	int i, j, pow=2, level=1, nr=5; 
	
	printf("        %d\n\n ",A[1]); 
	
	for(i=2;i<n;)
	{
		level=pow; 
		for(j=0;j<nr;j++) 
		printf(" "); 
		
		while(i<n && level>0)
		{ 
			printf(" %d ",A[i]);
			i++; 
			level--; 
		}
	
	printf("\n\n");	
	pow = pow * 2; 
	nr--; 
	}
}

void printArr(int a[], int n)
{
	for(int i=1;i<n;i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n");
}


int main ()
{
	int n,i,j,m=5;
	FILE *fp;
	srand(time(NULL));
	
	int test1[]={3,6,12,8,5,21,7,3,32,15};// cazuri particulare pt demonstrarea
	int test2[]={3,6,12,8,5,21,7,3,32,15};// corectitudinii algoritmului
	
	int test2_rez[10];
	
	dimB=9;
	for(i=9/2;i>0;i--)  
	{
		heapify(test1,i);
	}

	afisare(test1,10);
	
	printf("\n");

	for(i=1;i<10;i++)
		H_Push(test2_rez,test2[i]);
	afisare(test2_rez,10);
	
	fp = fopen("results.csv", "w"); // scriem in fisier rezultatul
	
	for(n=100;n<MAX_SIZE;n=n+100)
	{
			int* v1=(int*)malloc((n+1)*sizeof(int));
			int* v2=(int*)malloc((n+1)*sizeof(int));
			int* v3=(int*)malloc((n+1)*sizeof(int));
			printf(" %d",n);
			contor1_comp=0;
			contor2_comp=0;
			contor1_atri=0;
			contor2_atri=0;
			for(int i=0;i<m;i++)
			{
				creare(v1,n,1,1000);
				
				copy(v1,v2,n);
				dimB=n;
				for(i=dimB/2;i>0;i--)
					heapify(v1,i);

				dimA=0;
				for(i=1;i<n;i++)
					H_Push(v3,v2[i]);

							
				
			}
			fprintf(fp,"%d,%d,%d,%d,%d\n",n,(contor1_comp/m),(contor1_atri/m),(contor2_comp/m),(contor2_atri/m));
			free(v1);
			free(v2);
			free(v3);
	 }


	fclose(fp);
	getch();

	return 0;
}