// Tema_4.2.cpp : Defines the entry point for the console application.
//
//
// Student : ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***
// Cerinte :					Se cere implementarea corecta si eficienta a unei metode de timp O(n logk) pentru a
//								interclasa k liste ordonate, unde n este numarul total de elemente din listele de intrare.
//
//	Cerinte speci***ANONIM***:			*se vor folosi liste dinamice simplu inlantuite
//	                            *se va folosi o structura de tip HEAP pentru realizarea eficienta a algorimtului
//	                            *se va retine numarul de atribuiri si comparatii realizarea atat in cadrul operatiilor 
//								realizate asupra heapului precum si asupra listelor
//	                            *pentru liste se considera atribuire atat inserarea unui element precum si stergerea unui element
//
//	Concluzii :                  Rezultatul este o lista sortat obtinuta in urma unui algoritm asemenator cu cel al sortarii prin interclasare
//								 Lista rezultat are lungimea "n"  iar listele folosite pentru interclasarea au dimensiunea n/k
#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

//Declararea structurii unui nod din lista  
//Structura folosita la construirea listelor utilizate in cadrul algoritmului
	typedef struct tip_nod
		{
			int info;		//camp ce va retine informatia din fiecare nod
			struct tip_nod *urm;  //camp ce va contine referinta la urmatorul nod din lista,  in cazul ultimului nod din lista acest camp va avea valoarea 0
		} TIP_NOD;

	TIP_NOD *prim,*ultim,*prim1,*ultim1;
	TIP_NOD *a[5001];                             // sir de noduri in care se pastreaza primul nod dintr-o lista

	int nr_op;

	//LISTE
	void inserareLista(int a)  //realizeaza inserarea unui nou element in lista 
                                                  
	{ 
			 TIP_NOD *p;
			 p=(TIP_NOD*)malloc(sizeof(TIP_NOD));                   //creearea unui nou nod
		     p->info=a;										  //memorarea noii informatii in cadrul acestuia
		     p->urm=0;											  //nodul va vi inserat la sfarsitul listei

	   if(prim==0)										//cazul in care lista e vida
		 { 
			  prim=p;
		      ultim=p;
        
		 }
	 else 
			 ultim->urm=p;                      //inserarea dupa ultimul nod al listei
			 ultim=p;                                    
   
	}

	
	//afisarea unei liste
	void afisare()
		{
			TIP_NOD *p=prim;
			while (p!=NULL)
				{
					printf("%d ",p->info);
					p=p->urm;
				}
		}

	// Heap
	int Stanga(int i)                     
		{
			 return 2*i;
		}

	int Dreapta(int i)                    
		{
			return 2*i+1;
		}

	int Parinte(int i)                  
		{
			return i/2;
		}

	void MIN_HEAPIFY(TIP_NOD *b[], int i, int n) 
		//functie care porneste de la un anumit element i din cadrul arboerelui si verifica daca elemetele respecta strucutura de HEAP
		//intretinerea heap-ului
		{
			TIP_NOD *aux;
			int st = Stanga(i);
			int dr = Dreapta(i);
			int min;
			nr_op++;
			if ((st<=n) && (b[st]->info < b[i]->info))
				min=st;
			else min=i;
				nr_op++;
			if ((dr<=n) && (b[dr]->info < b[min]->info))
				min=dr;
			if (min!=i)
				{
					nr_op=nr_op+3;
					aux=b[i];
					b[i]=b[min];
					b[min]=aux;
					MIN_HEAPIFY(b,min,n); //verifica daca elementul respecta structura de HEAP in subarborele in care a fost trimis
				}
		}

	//cream vectorul de liste prin Bottom-up
	void BUILD_MIN_HEAP_BU(TIP_NOD *b[], int n)
		{
			int i;
			for (i=(n/2); i>=1; i--)
			MIN_HEAPIFY(b,i,n);
		}

	//Functie de verificare daca MIN HEAP-UL este gol, pentru a stii daca mai avem elemnte de inserat in lista finala sau nu
	int verificare_heap(TIP_NOD *b[], int n)
		{
			int i;
			int free=1;
			for (i=1; i<=n; i++)
				{
					if (b[i]!=NULL)
					free=0;
				}
			 return free;
		}

	//Functia de creare a vectorului de liste, unde k este numarul de liste, iar n este numarul de elemente ***ANONIM*** vectorului. 
	//Fiecare vector va avea n/k elemente
	void creare_lista(int k, int n)
		{
			TIP_NOD *q;
			prim1=0;
			q=0;
			ultim1=0;
			int i,j;
			srand(time(NULL));
			for (i=1; i<=k; i++)
				{
					prim1=0;
					q=0;
					ultim1=0;
			for (j=0; j<n/k; j++)
				{
			if (prim1==0)
				{
					prim1=(TIP_NOD*)malloc(sizeof(TIP_NOD*));
					prim1->info=rand()%100;
					prim1->urm=0;
					ultim1=prim1;
				}
			else 
				{
					q=(TIP_NOD*) malloc(sizeof(TIP_NOD*));
					q->info=ultim1->info+j;
					q->urm=0;
					ultim1->urm=q;
					ultim1=q;
			   }
		}
		  a[i]=prim1;
			}
	}

	//Functie pentru afisarea vectorului de liste

	void afisare2(TIP_NOD *b[], int n)
		{
		 int i;
		 TIP_NOD *q;
		 q=0;
		 for (i=1;i<=n;i++)
		{
			   q=b[i];
			  while(q!=NULL)
			 {
				 printf("%ld ",q->info);
				 q=q->urm;
			 }
			printf("\n");
		 }

		}

	//Functia care construieste lista finala, prin extragerea elementelor din heap min si inserarea lor in lista finala. 
	//Lista finala va fi sortata

	void construire_lista(TIP_NOD *b[], int n)
	{
		TIP_NOD *p;
		int k;
		int elem_min;
		TIP_NOD *aux;
		prim=0;
		p=0;
		ultim=0;
		BUILD_MIN_HEAP_BU(b,n);
		k=n;
		while (verificare_heap(a,k)==0)
			 {
			   elem_min=b[1]->info;
			   inserareLista(elem_min);
		  if (b[1]->urm!=0)
			 {
				nr_op++;
				b[1]=b[1]->urm;
			}
          else
		   {
			  nr_op=nr_op+3;
			  aux=b[1];
              b[1]=b[k];
              b[k]=aux;
              k--;
          }
             MIN_HEAPIFY(b,1,k);					//refacerea heap-ului
    }

   }

void interpretare_rezultate()
{
    int k,n;
    FILE *fp;
    fp=fopen("Interpretare rezultate.csv","w");
    fprintf(fp,"n,k1=5,k2=10,k3=100\n");
    int k1=5,k2=10,k3=100;
    for (n=100;n<=10000;n=n+100)
    {
        fprintf(fp,"%d,",n);

        nr_op=0;
        creare_lista(k1,n);
        construire_lista(a,k1);
        fprintf(fp,"%d,",nr_op);

        nr_op=0;
        creare_lista(k2,n);
        construire_lista(a,k2);
        fprintf(fp,"%d,",nr_op);

        nr_op=0;
        creare_lista(k3,n);
        construire_lista(a,k3);
        fprintf(fp,"%d\n",nr_op);
    }
    fclose(fp);
    nr_op=0;
}

int main()
{
	//Verificarea corectitudinii algoritmului
	int x=20,y=4; 
	printf("Afisarea vectorului de liste \n \n");
    creare_lista(y,x);
    afisare2(a,y);
    printf("\n");
	printf("Afisare lista final ordonata \n \n");
    construire_lista(a,y);
    afisare();
    printf("\n");

	int n=10000,k;
    FILE *fp;
    fp=fopen("Tabel2.csv","w");
    fprintf(fp,"k,nr_op \n");
    for (k=10;k<=500;k=k+10)
	 {
		 nr_op=0;
		 creare_lista(k,n);
		 construire_lista(a,k);
		 fprintf(fp,"%d,%d\n",k,nr_op);
	}
    fclose(fp);
    nr_op=0;
	interpretare_rezultate();
	getch();
	return 0;
}







