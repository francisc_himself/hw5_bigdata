
#include <stdio.h>
// ***ANONIM*** ***ANONIM***- ***ANONIM*** grupa ***GROUP_NUMBER***
//comparand graficele metoda selectiei are cele mai mici valori

#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
#include "Profiler.h"
Profiler profiler ("sortare");

using namespace std;

void bubble_sort(int *A, int n)
{
	int i=0,aux,ok=0,j,asig=0,comp=0;
	profiler.countOperation("comp_bubble_sort", n,0);
	profiler.countOperation("asig_bubble_sort", n,0);
	while(ok==0)
	{
		ok=1;
		for(j=0;j<n-i-1;j++)
		{

			if (A[j]>A[j+1])   {
				aux=A[j];
				A[j]=A[j+1];
				A[j+1]=aux;
				ok=0;
				profiler.countOperation("asig_bubble_sort", n,3);
			}
			profiler.countOperation("comp_bubble_sort", n);
		}
		i++;
	}
	profiler.addSeries("operatii_bubble","asig_bubble_sort","comp_bubble_sort");
}

void selectie(int *A,int n)
{
	int i,i_min,j,aux,asig=0,comp=0;
	for(i=0;i<n-1;i++)
	{
		i_min=i;
		for(j=i+1; j<n; j++)
		{
			profiler.countOperation("comp_selectie", n);
			if(A[j]<A[i_min])  {
				i_min=j;
			}
		}
		aux=A[i_min];
		A[i_min]=A[i];
		A[i]=aux;
		profiler.countOperation("asig_selectie", n,3);
	}
	profiler.addSeries("operatii_selectie","comp_selectie","asig_selectie");
}

void insertie(int *A,int n)
{
	int i,j,x,k,asig=0,comp=0;

	for(i=1;i<n;i++)
	{
		x=A[i];
		j=0;
		profiler.countOperation("asig_insertie", n);
		profiler.countOperation("comp_insertie", n);
		while(A[j]<x)
		{  
			j=j+1;
			profiler.countOperation("comp_insertie", n);

		}
		for(k=i;k>=j+1;k--)
		{  
			A[k]=A[k-1];
			profiler.countOperation("asig_insertie", n);
		}
		A[j]=x;
		profiler.countOperation("asig_insertie", n);
	}
	profiler.addSeries("operatii_insertie","comp_insertie","asig_insertie");
}
void rau()
{
	int A[10000],n,A1[10000],A2[10000],A3[10000];

	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(A,n,2);
		memcpy(A1,A,n*sizeof(int));
		bubble_sort(A1,n);

		memcpy(A2,A,n*sizeof(int));
		selectie(A2,n);

		memcpy(A3,A,n*sizeof(int));
		insertie(A3,n);
	}

	bubble_sort(A1,n);
	selectie(A2,n);
	insertie(A3,n);
	profiler.createGroup("Asignari caz defavorabil","asig_bubble_sort","asig_selectie","asig_insertie");
	profiler.createGroup("Comparatii caz defavorabil","comp_bubble_sort","comp_selectie","comp_insertie");
	profiler.createGroup("Total operatii caz defavorabil","operatii_selectie","operatii_bubble","operatii_insertie");

}
void bun(){
	int A[10000],n,A1[10000],A2[10000],A3[10000];
	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(A,n,1); //vector sortat
		memcpy(A1,A,n*sizeof(int));
		bubble_sort(A1,n);

		memcpy(A2,A,n*sizeof(int));
		selectie(A2,n);

		memcpy(A3,A,n*sizeof(int));
		insertie(A3,n);
	}

	bubble_sort(A1,n);
	selectie(A2,n);
	insertie(A3,n);
	profiler.createGroup("Asignari caz favorabil","asig_bubble_sort","asig_selectie","asig_insertie");
	profiler.createGroup("Comparatii caz favorabil","comp_bubble_sort","comp_selectie","comp_insertie");
	profiler.createGroup("Total operatii caz favorabil","operatii_selectie","operatii_bubble","operatii_insertie");

}
void mediu ()
{
	int A[10000],n,A1[10000],A2[10000],A3[10000];

	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(A,n);
		memcpy(A1,A,n*sizeof(int));
		bubble_sort(A1,n);

		memcpy(A2,A,n*sizeof(int));
		selectie(A2,n);

		memcpy(A3,A,n*sizeof(int));
		insertie(A3,n);
	}

	bubble_sort(A,n);
	selectie(A,n);
	insertie(A,n);
	profiler.createGroup("Asignari caz mediu ","asig_bubble_sort","asig_selectie","asig_insertie");
	profiler.createGroup("Comparatii caz mediu","comp_bubble_sort","comp_selectie","comp_insertie");
	profiler.createGroup("Total operatii caz mediu","operatii_selectie","operatii_bubble","operatii_insertie");
	
}
void test()
{
	int i,n=5;
	int A[]={4,9,1,8,3};
	int A1[]={4,9,1,8,3};
	int A2[]={4,9,1,8,3};
	int A3[]={4,9,1,8,3};
	bubble_sort(A1,5);

	
	selectie(A2,5);

	insertie(A3,5);


	profiler.createGroup("Asignari test","asig_bubble_sort","asig_selectie","asig_insertie");
	profiler.createGroup("Comparatii test","comp_bubble_sort","comp_selectie","comp_insertie");
	profiler.createGroup("Total operatii test ","operatii_bubble","operatii_selectie","operatii_insertie");
	 
	cout<<"vectorul sortat cu bubble sort este:";

	for(i=0;i<n;i++)
		cout<<A1[i]<<"  ";
	cout<<endl;
	cout<<"vectorul sortat cu metoda selectiei este:";
	for(i=0;i<n;i++)
		cout<<A2[i]<<"  ";
	cout<<endl;
	cout<<"vectorul sortat cu metoda insertiei este:";
	for(i=0;i<n;i++)
		cout<<A3[i]<<"  ";
}
void main()
{
	test();
	//rau();
	//bun();
	//mediu();
	profiler.showReport();
	_getch();
}