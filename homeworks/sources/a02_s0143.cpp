#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

int heap_size, dim;
int atr, comp;

//build heap bottom up
int LEFT (int i){
	return 2 * i;
}

int RIGHT (int i){
	return 2 * i + 1;
}

void MIN_HEAPIFY (int A[], int i) {
	int l = LEFT (i), lowest;
	int r = RIGHT (i);
	comp++;
	if (  (l <= heap_size) && (A[l] < A[i])) {
		lowest = l;
	}
	else
		lowest = i;
	comp++;
	if ( (r <= heap_size) && (A[r] < A[lowest])) {
		lowest = r;
	}
	if (lowest != i) {
		atr = atr + 3;
		int aux = A[i];
		A[i] = A[lowest];
		A[lowest] = aux;
		MIN_HEAPIFY (A, lowest);
	}
}

void BUILD_MIN_HEAP_BOTTOM_UP (int a[]){
	int i;
	atr = 0; comp = 0;
	for (i = heap_size / 2; i > 0; i--) 
		MIN_HEAPIFY (a, i);
}

//build heap top down

int parent (int i) {
	return i / 2;
}


void heapInsert (int a[],int key){
	dim++;
	atr++;
	a[dim] = key;
	int i = dim;
	comp++;
	while(i > 1 && a[parent(i)] > a[i]){
		comp++;
		atr = atr + 3;
		int aux = a[parent(i)];
		a[parent(i)] = a[i];
		a[i] = aux;
		i = parent(i);
	}

}

void initHeap(int a[]){
	dim = 0;
}

void BuildHeapTD(int b[] ,int a[]){
	comp = 0,atr = 0;
	initHeap(b);
	for(int i = 1; i <= heap_size; i++){ 
		heapInsert (b, a[i]);
	}
}


void copiere_sir(int sursa[], int destinatie[], int n){
	int i;
	for (i = 1; i <= heap_size; i++)
		destinatie[i] = sursa[i];
}

int main(){
	int a[10001],b[10001], i, j, atribuiri[2], comparatii[2];

//EXEMPLU PENTRU VERIFICAREA ALGORITMULUI
/**********************************************/
	heap_size = 10;
	a[1] = 2; a[2] = 13; a[3] = 9; a[4] = 7; a[5] = 1; a[6] = 3; a[7] = 5; a[8] = 15; a[9] = 3; a[10] = 4;
	printf("vector nesortat: \n");
	for(int i = 1; i <= heap_size; i++)
		printf("%d ", a[i]);
	
	int adancime = 1;
	BUILD_MIN_HEAP_BOTTOM_UP(a);
	printf("\nvector sortat top down: \n");
	for(int i = 1; i <= heap_size; i++){
		for (j = 1; j <= heap_size / 2 - adancime; j++)
			printf("\t");
		printf("%d ", a[i]);
		if ((i == 1) || (i == 3) || (i == 7)){
			adancime++;
			printf("\n");
		}
	}
	a[1] = 2; a[2] = 13; a[3] = 9; a[4] = 7; a[5] = 1; a[6] = 3; a[7] = 5; a[8] = 15; a[9] = 3; a[10] = 4;
	BuildHeapTD(b,a);
	printf("\nvector sortat top down: \n");
	for(int i = 1; i <= heap_size; i++){
		for (j = 1; j <= heap_size / 2 - adancime; j++)
			printf("\t");
		printf("%d ", b[i]);
		if ((i == 1) || (i == 3) || (i == 7)){
			adancime++;
			printf("\n");
		}
	}
/**********************************************/

	FILE *f;
	f = fopen("bottom_up vs top_down.csv","w");

	//initializare pas pt functia rand
	srand (time(NULL));
	//cap tabel
	fprintf(f,"n,atr_bottom_up,comp_bottom_up,total_bottom_up,atr_top_down,comp_top_down,total_top_down\n");

	for (heap_size = 100; heap_size <= 10000; heap_size = heap_size + 100)
	{
		atribuiri[0] = 0;
		atribuiri[1] = 0;
		comparatii[0] = 0;
		comparatii[1] = 0;

		for (j = 1; j <= 5; j++){
			for (i = 1; i <= heap_size; i++){
				a[i] = rand() % 10000;
				b[i] = a[i];
			}
			BUILD_MIN_HEAP_BOTTOM_UP(a);
			atribuiri[0] = atribuiri[0] + atr;
			comparatii[0] = comparatii[0] + comp;

			copiere_sir(b, a, heap_size);

			BuildHeapTD(b,a);
			atribuiri[1] = atribuiri[1] + atr;
			comparatii[1] = comparatii[1] + comp;
		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d\n", heap_size, atribuiri[0]/5, comparatii[0]/5, (atribuiri[0]+comparatii[0])/5, atribuiri[1]/5, comparatii[1]/5, (atribuiri[1]+comparatii[1])/5);
	}

	fclose(f);

	printf("\ngata!!!");

	getch();
	
	return 0;
}