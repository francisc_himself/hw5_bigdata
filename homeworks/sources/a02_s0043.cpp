//***ANONIM*** ***ANONIM*** 
//grupa ***GROUP_NUMBER***

//Cazul mediu - dupa cum putem vedea din grafice, este evident ca algoritmul Bottom-Up este mai eficient, 
//numarul de comparatii executate de algoritmul BU este cu mult mai mic decat numarul comparatiilor executate de algoritmul TD 
//complexitatea algoritmilor: O(n) pt BU si O(n lg n) pt TD
//Se mai poate observa ca acesti algoritmi sunt foarte stabili adica au o crestere liniara.
//pseudocodul a fost luat din "Introduction to Algorithms, Second Edition"-T.Cormen.

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;
#define NEG_INF INT_MIN

int heap[10001],lung, dim_heap, x[10001],prettyBU[13],prettyTD[13];
int stanga(int i)
{
	return 2*i;
}
int dreapta(int i)
{
	return 2*i+1;
}
int parinte(int i)
{
	return i/2;
}
//BU
void maxHeapify(int heap[], int i)
{
	int st, dr, l, aux;
	st = stanga(i);
	dr = dreapta(i);
	profiler.countOperation("OperatiiBU",lung);
	if(st <= dim_heap && heap[st] > heap[i])
		l = st;
	else 
		l= i;
	profiler.countOperation("OperatiiBU",lung);
	if(dr <= dim_heap && heap[dr] > heap[l])
		l = dr;
	if(l != i)
	{
		profiler.countOperation("OperatiiBU",lung,3);
		aux = heap[i];
		heap[i] = heap [l];
		heap[l] = aux;
		maxHeapify(heap, l);
	}
}
void buildMaxHeapBU(int heap[])
{
	dim_heap = lung;
	for(int i = lung/2; i >= 1; i--)
		maxHeapify(heap, i);
}

//in TD
int heapMax(int heap[])
{
	return heap[1];
}
int heapExtractMax(int heap[])
{
	int max;
	profiler.countOperation("OperatiiTD",lung);
	if(dim_heap< 1)
		printf("\nError!");
	profiler.countOperation("OperatiiTD",lung,2);
	max = heap[1];
	heap[1] = heap[dim_heap];
	--dim_heap;
	maxHeapify(heap, 1);
	return max;
}
void heapIncreaseKey(int heap[], int i, int key)
{
	int aux;
	profiler.countOperation("OperatiiTD",lung);
	if(key < heap[i])
		printf("\nNoua cheie este mai mica decat cheia curenta");
	profiler.countOperation("OperatiiTD",lung,2);
	heap[i] = key;
	while(i > 1 && heap[parinte(i)] < heap[i])
	{
		profiler.countOperation("OperatiiTD",lung,3);
		aux = heap[i];
		heap[i] = heap[parinte(i)];
		heap[parinte(i)] = aux;
		i = parinte(i);
	}
}
void maxHeapInsert(int heap[], int key)
{
	++dim_heap;
	profiler.countOperation("OperatiiTD",lung);
	heap[dim_heap] = NEG_INF;
	heapIncreaseKey(heap, dim_heap, key);
}
void buildMaxHeapTD(int heap[])
{
	dim_heap = 1;
	for(int i = 2; i <= lung; i++)
		maxHeapInsert(heap, heap[i]);
}
void copy(int heap[], int x[])
{
	for(int i=1; i <= lung; i++)
		x[i] = heap[i];
}
void averageCase()
{
	for(int i=1; i <= 5; i++)
		for(lung = 100; lung <= 10000; lung += 100)
		{
			FillRandomArray(heap,lung);
			copy(heap,x);
		//	printf("\n Nesortat: \n");
			//afisare(heap);
		//	printf("\nx=\n");
		//	afisare(x);
			buildMaxHeapBU(x);
			buildMaxHeapTD(heap);
			printf("\n mas : %d, lungime = %d\n",i, lung);
		//	printf("\nBottom-Up:\n");
			//afisare(x);
		//	printf("\nTop-Down: \n");
			//afisare(heap);
		//	getchar();
		}
	profiler.createGroup("Heap     Ionut19�","OperatiiTD","OperatiiBU");
	profiler.showReport();
}
void afisare(int xy[])
{
	for(int i =1; i <= lung; i++)
		printf("%d ",xy[i]);
}
void prettyPrint(int he[], int i, int niv)
{
	if(i > lung)
		return;
	prettyPrint(he,stanga(i), niv+1);
	for(int j=0;j<niv;j++)
		printf("         ",niv);
	printf("%d\n",he[i]);
	prettyPrint(he,dreapta(i), niv+1);
		
}
int main()
{
	averageCase();
	
	lung = 7;
	FillRandomArray(prettyBU, lung+1,1, 100, true);
	/*for(int i=1 ; i <= lung; i++)
		scanf("%d", prettyBU+i);*/
	copy(prettyBU,prettyTD);
	afisare(prettyTD);
	buildMaxHeapBU(prettyBU);
	buildMaxHeapTD(prettyTD);
	printf("\n Afisare pretty print pentru Bottom-Up: \n");
	//afisare(prettyBU);
	prettyPrint(prettyBU, 1, 0);
	printf("\n Afisare pretty print pentru Top-Down: \n");
	prettyPrint(prettyTD, 1, 0);
	return 0;
}