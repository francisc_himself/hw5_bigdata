#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>

int n,m;
int nr_atributii=0;
int nr_comparatii=0;

typedef struct nod
{
	int key;
	int dimensiune;
	nod *left, *right, *parent;
}NOD;

NOD* rad;

NOD* OS_SELECT(NOD *x, int i)
{
	int r=1;
	if (x->left != NULL)							
		r = x->left->dimensiune+1;					
	else
		r = 1;
	if (i == r)
		return x;
    else
	{
		if (i < r)
		{
			return OS_SELECT(x->left,i);
		}
        else
		{
			return OS_SELECT(x->right,i-r);
		}
    }
}

NOD* create(int x, int dim, NOD *stg, NOD *dr)
{
	NOD *q;
	q= new NOD;
	q->key=x;					
	q->left=stg;				
	q->right=dr;				
	q->dimensiune=dim;			
	nr_atributii+=4;
	if (stg!=NULL)
	{
		stg->parent = q;		
	}
	if (dr!=NULL)
	{
		dr->parent=q;
	}
	q->parent=NULL;			
	nr_atributii++;
	return q;
}

NOD* build(int start, int end)
{
	NOD *s,*d;
	if (start > end)
		return NULL;
	if (start == end)
		return create(start, 1, NULL, NULL);		
	else
	{
		int mijloc =(start + end)/2;
		s = build(start, mijloc-1);
		d  = build(mijloc+1, end);
		return create(mijloc, end-start+1,s, d);
	}
}

NOD* min(NOD* x)
{
	while(x->left!=NULL)
	{
		x = x->left;
		nr_atributii++;
		nr_comparatii++;
	}
	return x;						
}

NOD* succesor(NOD* x)
{
	NOD *y;
	if(x->right!=NULL)						
	{
		nr_comparatii++;
		return min(x->right);				
	}
	nr_atributii++;
	y = x->parent;							
	while ((y != NULL) && (x == y->right))		
	{
		nr_comparatii+=2;								
		x = y;								
		y = y->parent;
		nr_atributii+=2;
	}
	return y;								
}


NOD* sterge(NOD* z)
{
	NOD *y;
	NOD *x;

	if (z->left == NULL || z->right == NULL)		
		{
			nr_atributii++;
			y=z;
		}
	else
		{
			y = succesor(z);	
			nr_atributii++;
		}
	nr_comparatii++;
	if (y->left != NULL)							
		{
			nr_atributii++;
			x = y->left;
		}
	else
		{
			nr_atributii++;									
			x = y->right;
		}
	
	if (x != NULL)
		{
				nr_comparatii++;
				nr_atributii++;
				x->parent = y->parent;
		}
	nr_comparatii++;
	if (y->parent == NULL)
		{
			rad = x;
			nr_atributii++;
		}
	else
		if (y == y->parent->left)
			{
				nr_atributii++;
				y->parent->left = x;
			}
		else
			{
				nr_atributii++;
				y->parent->right = x;
			}
	nr_comparatii++;
	if (y != z)
	{
		nr_atributii++;
		z->key = y->key;
	}
	return y;
}


void reset_dim(NOD *p)
{
	while (p != NULL)
	{
		p->dimensiune--;			                                   
		p=p->parent;
		nr_atributii+=2;
	}
}

void preety_print(NOD *rad, int depth)
{
	if (rad!=NULL){
		preety_print(rad->left, depth+1);
	for(int j=0;j<depth;j++)
		printf("\t");
		printf("%d %d",rad->key,rad->dimensiune);
		printf("\n");
	
		preety_print(rad->right, depth+1);
	}
}

void josephus(int n, int m)
{
	NOD *x,*z;
	int k;
	rad = build(1, n);		
	int j = 1;
	for(k=n;k>1;k--)
	{
		j = ((j+m-2)%k) + 1;
		x = OS_SELECT(rad, j);	
		//printf("\nArbore:\n");
		//preety_print(rad,0); 
		//printf("Nod eliminat: %d\n",x->key);
		z = sterge(x);			
		reset_dim(z);			                                  
		free(z);					
		n--;
	}
}

int main()
{
	FILE *f;
	f = fopen("D:\\tema6.xls","w");
	fprintf(f,"i\t nr_a+nr_c\t\n");
	int i;
	for(i=0;i<10000;i+=100)
	{
		nr_atributii = 0;
		nr_comparatii = 0;
		josephus(i,i/2);
		fprintf(f,"%d\t %d\t\n",i,nr_atributii + nr_comparatii);
	}

	//josephus(7,3);
	fclose(f);
	getch();
}
