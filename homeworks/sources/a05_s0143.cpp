#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#define N 10007//1009

int comp;

int hash_function(int k, int i){
    return (k +i+i*i) % N;
    //c1 = 0
    //c2 = 1
}

int insert_element(int k, int *hash[]){
    int aux, i = 0;
    do{
        aux = hash_function(k, i);
        if(hash[aux] == NULL){
            hash[aux] = (int*) malloc(sizeof(int));
            *hash[aux] = k;
            return aux;
        }
        else
            i++;
    }while(i < N);
    return -1;
}

int search_element(int k, int *hash[]){
    int aux, i = 0;
    do{
        aux = hash_function(k, i);
        comp++;
        if(hash[aux] != NULL){
            if(*hash[aux] == k){
                return aux;
            }
            else
                i++;
        }
    }while(hash[aux] != NULL && i < N);
    return -1;
}

void erase_hash(int *hash[]){
    int i, *aux;
    for(i = 0; i < N; i++)
        if(hash[i] != NULL){
            aux = hash[i];
            hash[i] = NULL;
            free(aux);
        }
}

int main()
{
    int n, *hash[N] = {NULL}, i, j, k, m, max = 0, comp_total = 0;
    int aux[N]; //aux reprezinta un sir unde imi pastrez elementele introduse. ca atunci cand caut,
                //sa caut elemente care sigur au fost introduse
    float a = 0.8;
    FILE *f;
    f = fopen("hash.csv","w");
    //initializare pas pt functia rand
	srand (time(NULL));

    /***************EXEMPLU******************/
    /*n = (int)(N * a);
    printf("start %d\n", n);
    for(i = 1; i <= n; i++){
        k = rand() % 32000;
        aux[i-1] = k;
        j = insert_element(k, hash);
        if (j == -1)
            i--;
    }

    for(i = 0; i < N; i++){
        if(hash[i] != NULL)
            printf("(%d,%d,%d)\n", i, *hash[i], aux[i]);
    }
    printf("\n%d", n);*/
    /***************EXEMPLU******************/

    fprintf(f,"alfa,avg effort found,max effort found,avg effort not-found,max effort not-found\n");
    //initializare hash pentru factorul 0.8
    for (a = 0.8; a < 1; a = a + 0.05){
        printf("%f ",a);
        n = (int) (a * N);
        for(i = 0; i < n; i++){
            k = rand();
            aux[i] = k;
            j = insert_element(k, hash);
            if (j == -1)
                i--;
        }

        for(i = 0; i < 5; i++){
            for(m = 0; m < 1500; m++){
                comp = 0;
                //generam o valoare random intre 0 si numarul de elemente generate
                k = rand() % n; //valoarea reprezinta indicele din vectorul in care am salvat elementele
                k = aux[k];     //vom cauta numarul care este in aux[val generata];
                search_element(k, hash);
                comp_total += comp;
                if (max < comp)
                    max = comp;
            }
        }
        fprintf(f, "%f, %d, %d,", a, comp_total/7500, max);
        comp_total = 0;
        max = 0;

        for(i = 0; i < 5; i++){
            for(m = 0; m < 1500; m++){
                comp = 0;
                //generam valori peste cele pe care le-am generat in tabela de hash
                k = rand() + 33000;
                search_element(k, hash);
                comp_total += comp;
                if (max < comp)
                    max = comp;
            }
        }
        fprintf(f, "%d,%d\n", comp_total/7500, max);
        comp_total = 0;
        max = 0;

        erase_hash(hash);
    }

    a = 0.99;
    printf("%f ",a);
    n = (int) (a * N);
    for(i = 0; i < n; i++){
        k = rand();
        aux[i] = k;
        j = insert_element(k, hash);
        if (j == -1)
            i--;
    }

    for(i = 0; i < 5; i++){
        for(m = 0; m < 1500; m++){
            comp = 0;
            //generam o valoare random intre 0 si numarul de elemente generate
            k = rand() % n; //valoarea reprezinta indicele din vectorul in care am salvat elementele
            k = aux[k];     //vom cauta numarul care este in aux[val generata];
            search_element(k, hash);
            comp_total += comp;
            if (max < comp)
                max = comp;
        }
    }
    fprintf(f, "%f, %d, %d,", a, comp_total/7500, max);
    comp_total = 0;
    max = 0;

    for(i = 0; i < 5; i++){
        for(m = 0; m < 1500; m++){
            comp = 0;
            //generam valori peste cele pe care le-am generat in tabela de hash
            k = rand() + 33000;
            search_element(k, hash);
            comp_total += comp;
            if (max < comp)
                max = comp;
        }
    }

    fprintf(f, "%d,%d\n", comp_total/7500, max);

    erase_hash(hash);
    fclose(f);
    return 0;
}
