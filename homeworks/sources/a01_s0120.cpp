/*Fundamental algorithms Assignment 1
  Student: ***ANONIM*** Mihai-***ANONIM***
  Group: ***GROUP_NUMBER***

  Discussion:

  Average case: the algorithm with the best behaviour is the insertion algorithm, followed by bubble sort and last by selection sort
  Worst case: the same order as for average case
  Best case: the same order as the average case

  Conclusion: The best algorithm by far is the insertion sorting algorithm while the other two have similar but worse performances*/

#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#define ARRLEN 25
#define MAX_SIZE 10000
int n = ARRLEN;
int demoArray[20];
int iComp,iAssig,sComp,sAssig,bComp,bAssig;
ofstream myfile;

/*reset_arr function repopulates the array with the hardcoded values*/
void reset_arr(int array[]){
	demoArray[1]=5;
	demoArray[2]=8;
	demoArray[3]=4;
	demoArray[4]=6;
	demoArray[5]=10;
	demoArray[6]=4;
	demoArray[7]=22;
	demoArray[8]=2;
	demoArray[9]=1;
}

/*print method outputs the array*/
void print(int arrSize, int array[]){
int i;
	for (i=1;i<=arrSize;i++){
		printf("%d ",array[i]);
	}
	printf("\n");
}

/*selection function contains the selection sort algorithm and it also prints the number of assignments and comparations*/
void selection(int arrSize, int array[]){
int i,i_min,min,j;
	min=0;
	sAssig=0;
	sComp=0;
	myfile.open ("sSort.txt",ios::app);
	for (i=1;i<arrSize;i++)
	{
		i_min=i;
		for (j=i+1;j<=arrSize;j++){
		    sComp++;
			if (array[j]<array[i_min]){
				i_min=j;
			}
			sComp++;
			if (min!=array[i_min]){
				min=array[i_min];
				sAssig++;
				array[i_min]=array[i];
				sAssig++;
				array[i]=min;
				sAssig++;
			}
		}
	}
    printf("Selection: A: %d C: %d T: %d \n",sAssig, sComp, sAssig+sComp);
	myfile << n << "," << sAssig << "," << sComp<< "," << sAssig+sComp << endl ;
	myfile.close();
}

/*insertion function contains the insertion sort algorithm and it also prints the number of assignments and comparations*/
void insertion(int arrSize, int array[]){
int i,k,buff,j;
    iAssig=0;
    iComp=0;
    myfile.open ("iSort.txt",ios::app);
	for (i=2;i<=arrSize;i++){
		k=i-1;
		buff=array[i];
		iAssig++;
		while ((array[k]>buff)&&(k>0)){
		    iComp++;
			array[j=k+1]=array[k];
			iAssig++;
			k--;
		}
		iComp++;
		array[j=k+1]=buff;
		iAssig++;
	}
    printf("Insertion: A: %d C: %d T: %d \n",iAssig, iComp, iAssig+iComp);
	myfile << n << "," << iAssig << "," << iComp<< "," << iAssig+iComp << endl ;
	myfile.close();
}

/*bubbleSort function contains the bubble sort algorithm and it also prints the number of assignments and comparations*/
void bubbleSort(int arrSize, int array[]){
int i, j, temp;
    bComp=0;
    bAssig=0;
    myfile.open ("bSort.txt",ios::app);
	for (i = arrSize; i > 1; i--){
		for (j = 1; j <= i; j++){
		    bComp++;
			if (array[j-1] > array[j]){
				temp = array[j-1];
				bAssig++;
				array[j-1] = array[j];
				bAssig++;
				array[j] = temp;
				bAssig++;
			}
		}
	}
	printf("Bubble: A: %d C: %d T: %d \n",bAssig, bComp, bAssig+bComp);
    myfile << n << "," << bAssig << "," << bComp<< "," << bAssig+bComp << endl ;
    myfile.close();
}

/*the demo function calls each sorting algorithm, then prints and resets the array before calling the next algorithm*/
void demo(){
    printf("Original array:\n");
	reset_arr(demoArray);
	print(9,demoArray);

    printf("Selection sort:\n");
	selection (9,demoArray);
	print(9,demoArray);
	reset_arr(demoArray);

    printf("Insertion sort:\n");
	insertion (9,demoArray);
	print(9,demoArray);
    reset_arr(demoArray);

    printf("Bubble sort:\n");
	bubbleSort (9,demoArray);
    print(9,demoArray);
}

/*the test function populates the array with random variables and computes the average,best and worst cases for the running of the algorithms*/
void test(){
    int v[MAX_SIZE];
	int x[MAX_SIZE];
	int y[MAX_SIZE];
	int z[MAX_SIZE];

	//average case
	printf("\n Average case evaluation: \n\n");

	for (int j=0; j<5; j++){
        myfile.open ("iSort.txt",ios::app);
        myfile <<"Average case " << j << endl << endl;
        myfile.close();
        myfile.open ("bSort.txt",ios::app);
		myfile <<"Average case " << j << endl << endl;
        myfile.close();
		myfile.open ("sSort.txt",ios::app);
		myfile <<"Average case " << j << endl << endl;
        myfile.close();
		for(n=100; n<10000; n += 500){
			printf ("%d \n\n ", n);
			for (int j=0; j<n; j++){
                v[j] = rand() % 1010;
                x[j] = v[j];
                y[j] = v[j];
                z[j] = v[j];
			}
			bubbleSort(n,x);
			selection(n,y);
			insertion(n,z);
        }
	}
    myfile.open ("iSort.txt",ios::app);
	myfile <<"Worst case" << endl << endl;
	myfile.close();
    myfile.open ("bSort.txt",ios::app);
	myfile <<"Worst case " << endl << endl;
	myfile.close();
		//worst case
	printf("\n Worst case evaluation: \n\n");
	for(n=100; n<10000; n += 500){
        printf ("%d \n\n", n);
		for (int p=n; p>0; p--){
			x[p] = n-p;
			y[p] = n-p;
			z[p] = n-p;
		}
		bubbleSort(n,x);
		insertion(n,z);
	}
	myfile.open ("sSort.txt",ios::app);
	myfile <<"Worst case " << endl << endl ;
	myfile.close();

	printf("\n Worst case evaluation (SS): \n\n");
	for(n=100; n<10000; n += 500){
		printf ("%d \n\n", n);
		for (int p=n-1; p>0; p--){
			y[p] = n-p+1;
			y[n] = 0;
		}
		selection(n,y);
	}
	//best case
    myfile.open ("iSort.txt",ios::app);
	myfile <<"Best case" << endl << endl ;
    myfile.open ("bSort.txt",ios::app);
	myfile <<"Best case " << endl << endl ;
	myfile.close();
    myfile.open ("sSort.txt",ios::app);
	myfile <<"Best case " << endl << endl ;
	myfile.close();
	myfile.close();
	printf("\n Best case evaluation: \n\n");
	for(n=100; n<10000; n += 500){
		printf ("%d \n\n", n);
		for (int j=0; j<n; j++){
			x[j] = j;
			y[j] = j;
			z[j] = j;
		}
		bubbleSort(n,x); selection(n,y); insertion(n,z);
	}
	_getch();
}

/*The main function calls the demo and test functions*/
int main(){
    demo();
    //test();
    return 0;
}
