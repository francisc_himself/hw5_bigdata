/*
Dupa rularea algoritmilor de heapsort si quicksort am facut
grafice pe average, worst si best case pentru heapsort si
average si worst case pentru quicksort. Toate au fost puse
pe aceleasi grafic. Rezultatul este ca heapsortul are aceleasi
performanta pentru orice fel de caz. Este intotdeauna intre 
quicsort best si quicksort worst.
Quicksortul in cazul defavorabil are complexitatea n^2, pentru
evitarea acestui caz este foarte important sa fie ales un
pivot bun. Daca pivotul este bine ales, quicksortul este
mult mai rapid decat heapsortul.
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int counter = 0;

//#define movs(a,b) memcpy(a,b,sizeof(a));

inline void swap(int &a, int &b)
{
	int x = a;
	a = b;
	b = x;
}

class dvector
{
public:
	int* data;
	int size;

	dvector(dvector& other)
	{
		size = other.size;
		data = new int[size];
		memcpy(data,other.data,size*sizeof(int));
	}

	dvector()
	{
		size=0;
		data=0;
	}

	~dvector()
	{
		if (data!=0)
		{
			delete[] data;
		}
	}

	int& operator [] (int i)
	{
		if (i<size)
		{
			return data[i];
		}
		else
		{
			int *ndata = new int[i+1];

			if (data != 0)
			{
				memcpy(ndata,data,size*sizeof(int));
				delete[] data;
			}
			for (int j = size;j<i+1; j++)
			{
				ndata[j]=0;
			}
			data=ndata;
			size=i+1;
			return data[i];
		}
	}

	void print()
	{
		for (int i=0; i<this->size; i++)
		{
			printf("%d ",data[i]);
		}
		printf("\n");
	}

	void heapsort();

	void prealloc(int size)
	{
		if (size<this->size)
		{
			size--;
			(*this)[size]=0;
		}
	}

	void quicksort()
	{
		quicksort(0,size-1);
	}

	int partition(int left, int right)
	{
		int x = data[right];
		int i = left-1;
		int j;
		for (j=left; j<right; j++)
		{
			counter++;
			if (data[j]<=x)
			{
				i++;
				counter+=3;
				swap(data[i],data[j]);
			}
		}
		counter+=3;
		swap(data[i+1],data[j]);
		return i+1;
	}

	void quicksort(int left, int right)
	{
		if (left<right)
		{
			int mid = partition(left,right);
			quicksort(left,mid-1);
			quicksort(mid+1,right);
		}
	}

	void quicksort_better()
	{
		quicksort_better(0,size-1);
	}

	int partition_better(int left, int right)
	{
		int x = 0;
		int c = 0;
		for (int i=0; i<4 && left+i<right; i++)
		{
			x+=data[left+i];
			c++;
		}
		for (int i=0; i<4 && left<right-i; i++)
		{
			x+=data[right-i];
			c++;
		}
		x = x / c;

		int i = left-1;
		int j;
		for (j=left; j<right; j++)
		{
			counter++;
			if (data[j]<=x)
			{
				i++;
				counter+=3;
				swap(data[i],data[j]);
			}
		}
		counter+=3;
		swap(data[i+1],data[j]);
		return i+1;
	}

	void quicksort_better(int left, int right)
	{
		if (left<right)
		{
			int mid = partition_better(left,right);
			quicksort_better(left,mid-1);
			quicksort_better(mid+1,right);
		}
	}

};

class heap
{
public:
	dvector* vec;
	int heap_size;
	int direction;
	
	heap()
	{
		vec = new dvector;
		heap_size=0;
		direction=0;
	}
	~heap()
	{
		delete vec;
	}
	int& operator [] (int i)
	{
		int &d = (*vec)[i];
		
		if (i>heap_size)
		{
			heap_size=i;
		}
		
		return d;
	}

	void max_up_heapify(int i)
	{
		if (i>1) //boundary check
		{
			int &a = (*vec)[i]; //current key
			int &b = (*vec)[i>>1]; //parent key
			counter ++;
			if (b<a)
			{
				counter +=3;
				swap(a,b);
				//print(); //step by step print;
				max_up_heapify(i>>1);
			}
		}
	}

	void max_down_heapify(int i)
	{
		int ch[2];
		ch[0] = i<<1;
		ch[1] = ch[0]+1;
		for (int k=0; k<2; k++) //for each immediate child
		{
			int &c = ch[k];
			if (c<=heap_size) //boundary check
			{
				int& iv = (*this)[i];
				int& cv = (*this)[c];
				counter++;
				if (iv<cv)
				{
					counter+=3;
					swap(iv,cv);
					//print();//step by step print
					max_down_heapify(c);
				}
			}
		}
	}

	void min_up_heapify(int i)
	{
		if (i>1) //boundary check
		{
			int &a = (*vec)[i]; //current key
			int &b = (*vec)[i>>1]; //parent key
			counter ++;
			if (b>a)
			{
				counter +=3;
				swap(a,b);
				//print(); //step by step print;
				min_up_heapify(i>>1);
			}
		}
	}

	void min_down_heapify(int i)
	{
		int ch[2];
		ch[0] = i<<1;
		ch[1] = ch[0]+1;
		for (int k=0; k<2; k++) //for each immediate child
		{
			int &c = ch[k];
			if (c<=heap_size) //boundary check
			{
				int& iv = (*this)[i];
				int& cv = (*this)[c];
				counter++;
				if (iv>cv)
				{
					counter+=3;
					swap(iv,cv);
					//print();//step by step print
					max_down_heapify(c);
				}
			}
		}
	}

	void bottom_up()
	{
		direction = 0;
		for (int i=this->heap_size>>1; i>0; i--)
		{
			max_down_heapify(i);	
		}
	}

	void top_down()
	{
		direction = 0;
		for (int i=1; i<=this->heap_size; i++)
		{
			max_up_heapify(i);
		}
	}

	void max_bottom_up()
	{
		direction = 0;
		bottom_up();
	}

	void max_top_down()
	{
		direction = 0;
		top_down();
	}

	void min_bottom_up()
	{
		direction = 1;
		for (int i=this->heap_size>>1; i>0; i--)
		{
			min_down_heapify(i);	
		}
	}

	void min_top_down()
	{
		direction = 1;
		for (int i=1; i<=this->heap_size; i++)
		{
			min_up_heapify(i);
		}
	}

	void set_data(dvector *vec2)
	{
		delete this->vec;
		vec=new dvector(*vec2); //copy
		heap_size=vec->size;

		(*vec)[vec->size]=0;

		for (int i=heap_size; i>0; i--)//align
		{
			vec->data[i] = vec->data[i-1];
		}
		(*vec)[0]=-1;
	}

	void bottom_up(dvector *vec2)
	{
		set_data(vec2);
		bottom_up();
	}

	void top_down(dvector *vec2)
	{
		set_data(vec2);
		top_down();
	}

	void max_bottom_up(dvector *vec2)
	{
		set_data(vec2);
		bottom_up();
	}

	void max_top_down(dvector *vec2)
	{
		set_data(vec2);
		top_down();
	}

	void min_bottom_up(dvector *vec2)
	{
		set_data(vec2);
		min_bottom_up();
	}

	void min_top_down(dvector *vec2)
	{
		set_data(vec2);
		min_top_down();
	}

	void insert(int value)
	{
		(*this)[heap_size+1]=value;		
		max_up_heapify(heap_size);
	}

	void print(int i,int depth=0)
	{
		int c1 = i<<1;
		int c2 = c1+1;

		if (c1<=heap_size)
			print(c1,depth+1);
	
		for(int k=0; k<depth; k++)
			printf("\t");

		printf("%d\n",(*vec)[i]);

		if (c2<=heap_size)
			print(c2,depth+1);
	}

	void print()
	{
		print(1,0);
	}

	int pop()
	{
		counter++;

		int x = (*vec)[1];
		(*vec)[1] = (*vec)[heap_size];
		heap_size--;
		if (direction == 0)
			this->max_down_heapify(1);
		else
			this->min_down_heapify(1);
		return x;
	}

	void push(int x)
	{
		heap_size++;
		(*vec)[heap_size]=x;
		this->max_up_heapify(heap_size);
	}
};

void dvector::heapsort()
{
	heap h;
	h.max_bottom_up(this);

	//h.print();

	for (int i=0; i<size; i++)
	{
		int n = h.pop();
		(*this)[size-i-1]=n;
	}
	
}



int main()
{
	dvector q;
	

	q.prealloc(10);

	for (int i=0; i<10; i++)
	{
		q[i]=rand()&0xfff;
	}

	dvector p(q);

	printf("original\n");
	q.print();

	printf("heap\n");
	q.heapsort();
	q.print();

	printf("quick\n");
	p.quicksort();
	p.print();


	//testing time

		FILE *output = fopen("out.txt","w");
	fprintf(output,"Test Size, Heapsort, Quicksort, Quicksort Better, Heapsort Asc, Quicksort Asc, Quicksort Better Asc, Heapsort Desc, Quicksort Desc, Quicksort Better Desc\n");

	//for (int i=0; i<sizeof(test_table)/sizeof(int); i++)
	for (int i=0; i<=240; i++)
	{
		int test_size = i*10;

		//random vector

		dvector test_case[3];
		int outv[9];


		for (int i=test_size-1; i>=0; i--)
		{
			test_case[0][i]=rand()&0xfff; //random
			test_case[1][i]=i; //worst case (asc sorted)
			test_case[2][i]=test_size-i; //best case (desc sorted)
		}

		for (int test = 0; test<3; test++)
		{
			dvector vec_sort_heap(test_case[test]);
			dvector vec_sort_quick(test_case[test]);
			dvector vec_sort_quick_better(test_case[test]);
			
			counter = 0;
			vec_sort_heap.heapsort();
			outv[(test*3)]=counter;

			counter = 0;
			vec_sort_quick.quicksort();
			outv[(test*3)+1]=counter;

			counter = 0;
			vec_sort_quick.quicksort_better();
			outv[(test*3)+2]=counter;
		}

		fprintf(output,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",test_size,outv[0],outv[1],outv[2],outv[3],outv[4],outv[5],outv[6],outv[7],outv[8]);

	}

	fclose(output);
	

	return 0;
}