#include <stdio.h>
#include <conio.h>
#include "Profiler.h"

Profiler profiler("disjoint_sets");
typedef struct node Forrest;

typedef struct vert {
	int key;
	Forrest *f;
} Vertex;

typedef struct edge {
	Vertex *v1;
	Vertex *v2;
} Edge;

typedef struct node {
	int key;
	int rank;
	int visited;
	Vertex *v;
	struct node *p;
} Forrest;



void makeSet(Vertex *vert) {
	Forrest *y=(Forrest *)malloc(sizeof(Forrest));
	vert->f=y;
	y->v=vert;
	y->p=y;
	y->rank=0;
	y->key=vert->key;
}

Forrest *findSet(Forrest *x,int *op) {
	if (x != x->p) {
		x->p=findSet(x->p,op);
		(*op)++;
	}
	return x->p;
}

void link(Forrest *x,Forrest *y) {
	if (x->rank > y->rank) {
		y->p=x;
	} else {
		x->p=y;
		if (x->rank==y->rank) 
			y->rank+=1;
	}
}

void union_(Forrest *x,Forrest *y,int *op) {
	*op=*op+2;
	link(findSet(x,op),findSet(y,op));
}

int main() {
	Vertex *vertex[10000];
	Edge *edges[60000];
	int op=0;

	srand(time(NULL));

	for (int i=0;i<10;i++) {
		vertex[i]=(Vertex*)malloc(sizeof(Vertex));
		vertex[i]->key=i;
		makeSet(vertex[i]);
	}

	for (int i=0;i<5;i++) {
		edges[i]=(Edge*)malloc(sizeof(Edge));
		edges[i]->v1=vertex[rand()%10];
		edges[i]->v2=vertex[rand()%10];
		if (findSet(edges[i]->v1->f,&op) != findSet(edges[i]->v2->f,&op))
			union_(edges[i]->v1->f,edges[i]->v2->f,&op);
	}

	printf("Edges:\n");
	for (int i=0;i<5;i++) {
		printf("( %d, %d ) ",edges[i]->v1->key,edges[i]->v2->key);
	}
	printf("\nDistinct roots:\n");
	int cnt=0;
	for (int i=0;i<10;i++) {
		Forrest *v=findSet(vertex[i]->f,&op);	
		if (v->visited!=1) {
			printf("%d ",v->key);
			cnt++;
		}
		v->visited=1;	
	}
	printf("\nThere are %d connected components.",cnt);
	
/*	for (int i=0;i<10000;i++) {
		vertex[i]=(Vertex*)malloc(sizeof(Vertex));
		vertex[i]->key=i;
		makeSet(vertex[i]);
	}

	for (int n=10000;n<=60000;n+=500) {
		op=0;
		for (int i=0;i<n;i++) {
			edges[i]=(Edge*)malloc(sizeof(Edge));
			edges[i]->v1=vertex[rand()%10000];
			edges[i]->v2=vertex[rand()%10000];
			op+=3;
			if (findSet(edges[i]->v1->f,&op) != findSet(edges[i]->v2->f,&op))
				union_(edges[i]->v1->f,edges[i]->v2->f,&op);
		}
		profiler.countOperation("operations_disjoint_sets",n,10000+op);
	}

	profiler.showReport();*/

	getch();
	return 0;
}