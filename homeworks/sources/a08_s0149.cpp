#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <time.h>
/* ***ANONIM*** ***ANONIM***
** gr***GROUP_NUMBER***
**CERINTA:Structuri de date pentru multimi disjuncte 
*/

typedef struct nod{
	int key;
	struct nod *p;
	struct nod *next;
}NOD;

//structura pentru lista care reprezinta multimea
typedef struct lista{
	int key;
	NOD *prim;
	NOD *ultim;
	int size;
}MUL;

#define N 10000

MUL *mul = 0;
NOD *nod = 0;
FILE *f = 0;
int m = 0, nrMul = N;

//crearea multimii cu un singur element
void createMul(NOD *p)
{
	m++;//incrementarea nr de operatii
	mul[p->key].key = p->key;
	mul[p->key].size = 1;
	mul[p->key].prim = p;
	mul[p->key].ultim = p;
}

//stergerea unei multimi
void destroy(int key)
{
	mul[key].key = -1;
	mul[key].prim = 0;
	mul[key].size = 0;
	mul[key].ultim = 0;
}

//initializarea 
void init()
{
	int i;
	m = 0;
	nod = (NOD*) malloc(N*sizeof(NOD));//pt nod
	if (!nod)
	{
		printf("Eroare la alocare memorie nod");
		exit(1);
	}
	mul = (MUL*) malloc(N*sizeof(MUL));//pt multime
	if (!mul)
	{
		printf("Eroare la alocare memorie multime");
		exit(1);
	}
	//crearea multimilor cu un singur element
	for (i=0; i<N; i++)
	{
		nod[i].key = i;
		nod[i].next = 0;
		nod[i].p = &nod[i];
		createMul(&nod[i]);
	}
}

//gasirea multimii cu elementul dat
MUL* gasesteMul(NOD *p)
{
	m++;//incrementeaza numarul de operatii
	//returneaza o referinta catre reprezentatntul unic al multimii care il contine pe p
	return &mul[p->p->key];
}

//reuniunea multimilor
void reuneste(NOD *p, NOD *q)
{
	MUL *aux, *a, *b;
	m++;
	a = gasesteMul(p);
	b = gasesteMul(q);
	//cheile nu pot fi egale
	if (a->key == b->key) return;
	//daca dimensiunea lui a este mai mare decat dimensiunea lui b atunci interschimbam a si b
	if ( a->size > b->size ) 
	{
		aux = a;
		a = b;
		b = aux; 
	}
	//p va fi primul elem din a
	p = a->prim;
	//p se va pune la sfarsit in b
	b->ultim->next = p;
	//legatura propriu zisa
	while ( p!=NULL )
	{
		p->p = b->prim;//reprezentantul nodului p va fi primul b
		b->ultim = p;//ultimul b va fi p
		p = p->next;//se trece mai departe
	}
	//creste dimensiunea lui b
	b->size = b->size + a->size;
	//stergerea cheii a
	destroy(a->key);
	//nr de multimi scade
	nrMul--;
}

//trasarea muchiilor
void addEdges(int e)
{
	int i, k1, k2;
	for (i=0; i<e; i++)
	{
		//2 numere luate aleator 
		k1 = rand()%N; 
		k2 = rand()%N;
		reuneste(&nod[k1], &nod[k2]);//reunirea multimilor
	}
}

//eliberarea nodului si a multimii
void freeAll()
{
	free(nod);
	free(mul);
}

int main()
{
	int e = 0;
	srand(time(NULL));	
	fopen_s(&f,"rezultat_pt_grafic.txt","w");
	for (e=N; e<=60000; e=e+1000)
	{
		init();//crearea nodurilor si a multimilor
		addEdges(e);//trasarea muchiilor
		printf("e = %d; \n", e);
		fprintf(f, "%d\t%d\n", e, m);
		freeAll();
	}
	fclose(f);
	return 0;
}