#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
/*
Average case:
We compared the two strategies in the average case,making 5 random generations for each input in the interval [100 10 000];
Assigments:Lets now analize the first graph regardin the number of assigment done by the methods for bottom-up is goes from 201-22228, 
while the top-down procedure, behaves worst:426-48283
Comparisons:Regarding the comparisons for the bottom up procedure is between 234 and 24844, while for top-down is almost half from
109-12776, for a random input data.
Overall: The top-down approach is more efficient than the bottom up approach as total number of comparisons and assigments
In worst case we can see that by far the most efficients overall is the buttom-up procedure.
As a conclusion ,we can say that bottom-up procedure is faster than the top down aproach overall
Regarding their usage: Bottom-up is used for sorting, and top-down as priority queues.Tlking about running time both methods are linear.

*/
long bupc=0;
long bupa=0;
long tdwc=0;
long tdwa=0;
/*
Bottom-up 
-1/2 out of all nodes are leafs=> heaps 
-apply heapify to the first non-leaf node
-go to the next indexed node sibling to the left of the first processed element
-continue the procedure until reaching the root
Running time for n elements: O(n)

*/
void heapify(long a[], int i, int n)
{
    int l,r,max;
    long aux;

    l=2*i;//left child
    r=2*i+1;//right child

    bupc++;
	//determine the max element,the parent
    if (l<=n && a[l]>a[i])
        max=l;
    else
        max=i;

	 bupc++;
    if (r<=n && a[r]>a[max])
        max=r;
	//switch themax element with the parent
    if (max!=i)
        {
            bupa+=3;
            aux=a[i];
            a[i]=a[max];
            a[max]=aux;
            heapify(a,max,n);
        }
}

void bottomup(long a[],int n)
{
    int i;
    for (i=n/2;i>=1;i--)
        heapify(a,i,n);
}
/*
Top-down 
-new element on the the bottom
-rebuild the heap, moveing  the bottom element upper in the heap until it finds a larger value parent
Running time for n elements: O(nlgn)
*/
void insert_in_heap(long a[],long key,int *dim)
{
    (*dim)=(*dim)+1;
    int i;
	long aux;
    i=(*dim);
	//while the element is grater than node
    while (i>1 && a[i/2]<key)
    {
        tdwc++;
        tdwa+=3;
		aux=a[i];
        a[i]=a[i/2];
		a[i/2]=aux;
        i=i/2;
    }

}

void topdown(long a[],int n)
{
    int dimension=1,i;
    for (i=2;i<=n;i++)
        insert_in_heap(a,a[i],&dimension);
}
//preety print
void pp(long a[],int n){
	int i,j,p=2,level=1,space=5; 
	printf("             %d\n\n ",a[1]); 
	for(i=2;i<n;){
		level=p;
		for(j=0;j<space;j++) //print nr of spaces
			printf("  "); 
		while(i<n && level>0){  
			printf(" %d ",a[i]);
			i++;  
			level--;  
		}
			printf("\n\n");	
			p=p*2;  
			space--;  
	}
	
}
int main()
{
    srand(time(NULL));
    long bu[10001];
    long td[10001];
	long wrsta[10001];
	long wrstb[10001];
    int i,j,k,nr;
    long compup,compdown,assignup,assigndown;
    FILE *f,*e;
    f=fopen("buildheap.csv","w");
	e=fopen("buildheapwost.csv","w");

//Verify the two approaches for a length of 10
	printf("Bottomup: \n ");
	long a[]={0,4,16,14,8,7,9,10,2,3,5};
	bottomup(a,10);
	pp(a,10);


	printf("\n");
	printf("Topdown: \n ");
	long a1[]={0,4,16,14,8,7,9,10,2,3,5};
    topdown(a1,10);
	pp(a1,10);

		

	//worst case
			 for (i=100;i<=10000;i=i+100)
		 {
			 bupc=tdwc=bupa=tdwa=0;
			 for(j=1;j<=i;j++)
			 {wrsta[j]=j;
			 wrstb[j]=j;}
			 bottomup(wrsta,i);
			topdown(wrstb,i);

			fprintf(e,"%d %ld %ld %ld %ld %ld %ld\n",i,bupc,bupa,bupc+bupa,tdwc,tdwa,tdwc+tdwa);
		 }

//the avage case for the buttom up and top down build heap methods

    for (i=100;i<=10000;i=i+100)
	{
		bupc=tdwc=bupa=tdwa=0;


	
		for (j=1;j<=5;j++)
		{
			nr=0;
			compup=0;compdown=0;
			assignup=0;assigndown=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				bu[nr]=td[nr]=rand();
			}
			bottomup(bu,i);
			topdown(td,i);
			compup+=bupc;
			compdown+=tdwc;
			assignup+=bupa;
			assigndown+=tdwa;
		}
			fprintf(f,"%d %ld %ld %ld %ld %ld %ld\n",i,compup/5,assignup/5,compup/5+assignup/5,compdown/5,assigndown/5,compdown/5+assigndown/5);
	}
	getch();
    return 0;
}
