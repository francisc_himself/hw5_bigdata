#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <time.h> 
#include<conio.h>

#define N 10007

int T[N];
int op_found,op_nfound;

int hash(int k, int i)
{
	int a,c1=7,c2=17;
	a=(k%N +c1*i + c2*i*i) %N;
	return a;
}

 ///functia de inserare 
int Hash_Insert(int *T, int k)   
{
   int i=0,temp;
   do
    {
       temp=hash(k,i);
       if(T[temp]==NULL)
        {
           T[temp]=k;
	       return temp;
        }
       else i++;
    }
   while(i!=N);
return -1;
}

// functia de cautare 
int Hash_Search(int *T,int k)
{
  int i=0,temp,nr=0;
  do
   {
      temp=hash(k,i);
      nr++;
      if(T[temp]==k)
       {
		   op_found=nr; //daca numarul a fost gasit atunci va retuna numarul de operatii efectuate pana la gasirea acestuia
		   return temp;
       }
      else i++;
    } 
   while((T[temp]!=NULL)&&(i!=N));

op_nfound=nr;// daca numarul nu a fost gasit atunci va returna numarul total de opartii efectuate
return -1;
  }

void main()
{
    FILE *pf;
	srand(time(NULL));
	int n,k,l,m,maxim1,maxim2;
	int elem_max=0,mediu2,mediu1;
	int medie_gas,medie_neg;
	double fump[5]={0.8,0.85,0.9,0.95,0.99};
	int a[10010];

    pf=fopen("hash.csv","w");
    fprintf(pf,"Factor umplere ,Avg. Effort(f) ,Max. Effort(f) ,Avg. Effort(nf) ,Max. Effort(nf)\n"); 
	for(int i=0; i<5; i++)
	 {
		 n = fump[i]*N;  
		 medie_gas=0;  medie_neg=0;  
		 maxim1=0;     
		 maxim2=0;    
		 
		 for(int j=0;j<5;j++)//pentru fiecare valoarea a factorului de umplere se testeaza 5 cazuri diferite (mediere)
		  {
			  for(m=0;m<N;m++)      T[m]=NULL;  //initializare
			  for(l=0;l<n;l++)
			   {
				   k=rand(); 
				   a[l]=k;
				   if(k>elem_max) //elem max pt cautare numerele care nu se gasesc 
					elem_max=k;
				   Hash_Insert(T,k);
			   }

			  mediu1=0;
			  for(l=0;l<1500;l++)  
			   {
				   op_found=0;
				   k=a[rand()%n];
				   Hash_Search(T,k);
				   if(op_found>maxim1)
					   maxim1=op_found; 
				   mediu1=mediu1+op_found;	 
			   }
			  medie_gas=medie_gas + mediu1/1500;
			  
			  mediu2=0;
			  for(l=0;l<1500;l++)  
			   {    
				   op_nfound = 0;   
				   k=rand()+ elem_max;
				   Hash_Search(T,k);
				   mediu2 = mediu2+op_nfound;
				   if(op_nfound>maxim2)
					  maxim2 = op_nfound; 						
			   }
			  medie_neg=medie_neg+mediu2/1500;	
		 }	
		if(medie_gas>=5) 
			medie_gas=medie_gas/5;
		fprintf(pf,"%1.2f , %d , %d ,%d ,%d \n",fump[i],medie_gas,maxim1,medie_neg/5,maxim2); 
	}

 fclose(pf);
 printf("Programul s-a executat cu succes!");
 getch();
}