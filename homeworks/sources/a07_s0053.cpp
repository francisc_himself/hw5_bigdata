// Arbore multicai -> binari

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>

typedef struct node{
	struct node *next;
	int cheie;
}Node;

typedef struct arb{
	struct arb *right;
	struct arb *left;
	int cheie;
}Arb;

int a[]={2,7,5,2,7,7,-1,5,2};
Node a2[100];
Arb* a3;
int n,top;

Arb* creare(int x){
	Arb *p;
	p=(Arb* )malloc(sizeof(Arb));
	p->cheie=x;
	p->right=NULL;
	p->left=NULL;
	return p;}

Node* nodNou(int x){
	Node *p;
	p=(Node* )malloc(sizeof(Node));
	p->cheie=x;
	p->next=NULL;
	return p;}

void init(){
	for (int i=0;i<n;i++)
		a2[i].next=NULL;}

void transform(){
	init();
	for (int i=n-1;i>=0;i--){
		Node *p=nodNou(i);
		if (a[i]!=-1)		{
			p->next=a2[a[i]-1].next;
			a2[a[i]-1].next=p;	}
		else{
			top=i;		}}}

void transform2(Arb* T){
	Node* p=a2[T->cheie].next;
	if (p!=NULL)	{
		Arb* t=creare(p->cheie);
		T->left=t;
		transform2(t);
		p=p->next;
		while(p!=NULL && p->cheie!=-1)		{
			Arb* nw=creare(p->cheie);
			t->right=nw;
			t=t->right;
			transform2(t);
			p=p->next;}}
	else{
		T->left=NULL;}}

void prettyprint(int x,Arb *t){
	for (int i=0;i<x;i++)
		printf(" ");
	printf("%d\n",t->cheie+1);
	if (t->left!=NULL)
		prettyprint(x+5,t->left);
	if (t->right!=NULL)	{
		prettyprint(x,t->right);}}

int main(){
	n=9;
	
	transform();
	a3=creare(top);
	transform2(a3);
	prettyprint(0,a3);
	getch();
	return 0;}
