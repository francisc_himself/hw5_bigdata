/*

***ANONIM*** ***ANONIM*** Daniela
Operatiile de punere si extragere a unui element din coada se fac intr-un timp O(1), deci timpul total alocat 
operatiilor efectuate cu coada este O(V).Pentru ca lista de adiacenta a fiecarui varf este examinata doar atunci 
cand varful este scos din coada , lista de adiacenta a fiecarui varf este examinata cel mult odata.
Deoarece suma lungimilor tuturor listelor de adiacenta este O(E), timpul cel mai defavorabil pentru parcurgerea
tuturor listelor de adiacenta este O(E).Timpul pentru initializare este O(V) , si astfel timpul total de executie 
a algoritmului DFS este O(V+E). Deci cautarea in latime ruleaza intr-un timp liniar, ce depinde de marimea 
reprezentarii prin liste de adiacenta a grafului.
*/

#include<conio.h>
#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#define ALB 0
#define GRI 1
#define NEGRU 2
#define INF 100000


typedef struct Node
{
    int val;
    struct Node* urm;
}NODE;
typedef struct p_nod
{
	int col;
	int dist;
	int parent;
} p_nod;


int ind=0,	nr_op, op=0;
int coada[60000];
p_nod p[60000];
NODE *first[60000];
NODE *last[60000];



void BFS (int v, int n)
{
	int i;
	ind=0;
	op=0;
	for ( i=1; i<=n; i++)
		if (i!=v)
		{
			p[i].dist=INF;
			p[i].parent=0;
			p[i].col=ALB;

			op++;
		}
	p[v].dist=0;
	p[v].parent=0;
	p[v].col=GRI;
	
	ind++;
	op=op+2;
	coada[ind]=v;
	while(ind!=0)
	{
		i=coada[1];
		printf("%d ",i);
			op++;
		for (int l=1; l<ind; l++)
		 coada[l]=coada[l+1];
		Node *nod;
		nod=first[i];
		op++;
		ind--;
		while (nod!=NULL)
		{
			
			int u=nod->val;
			nod=nod->urm;
			op++;
			if (p[u].col==ALB)
			{
				p[u].dist=p[i].dist+1;
				p[u].parent=i;
				p[u].col=GRI;
				coada[ind]=u;
					ind++;
			}
		}
		p[i].col=NEGRU;
		ind++;
	}
}

bool testare (int s, int v)
{
	NODE *nod;
	nod=first[s];
	op++;
	while(nod!=NULL)
	{
		op++;
		if(s==nod->val)
			return false; 
		nod=nod->urm;
	}
	return true;

}

void Liste(int n,int nr) 
{
	int i,u,v;
	for(v=0;v<=n;v++)
		first[v]=NULL;
	     last[v]=NULL;

	for(v=1;v<=nr;v++)


	{
		i=rand()%n+1;
		u=rand()%n+1;
		if (i!=u)
		{

				if(first[i]==NULL)
				{
					first[i]=(NODE *)malloc(sizeof(NODE *));
					first[i]->val=u;
					first[i]->urm=NULL;
					last[i]=first[i];
					v++;
				}
				else {
						if (testare (i, u) && testare(u,i))
						{
							NODE *q;
							q=(NODE *)malloc(sizeof(NODE *));
							q->val=u;
							q->urm=NULL;
							last[u]->urm=q;
							last[u]=q;
							
						}
					}
		}
	}
}



void fisier()
{

	FILE *f1,*f2;
	f1 = fopen("F1.csv","w");
for(int nr = 1000 ; nr < 5000 ; nr = nr+100)
{
    op=0;
	int n = 100;
	Liste(n,nr);
	BFS(1,n);
	fprintf(f1," %d , %d , %d\n",n,nr,op);
}
fclose(f1);

f2 = fopen("F2.csv","w");
for(int n = 100 ; n <= 200 ; n = n+10)
{
    op=0;
	int nr = 9000;
    Liste(n,nr);
	BFS(1,n);
	fprintf(f2," %d , %d , %d\n",n,nr,op);
}
fclose(f2);


}



int main()
{
	
	NODE *p;
	int n=5;
	int nr=5;
	Liste(n, nr);
for(int i=1;i<=n;i++)
	{
		printf("%d : ",i);
		p=first[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->urm;
		}
		printf("\n");
	}

BFS(3, n);
	srand(time(NULL));
	//fisier();


	getch(); 
    return 0;
}