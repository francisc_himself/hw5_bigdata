﻿/* 
* ***ANONIM*** ***ANONIM***, group ***GROUP_NUMBER***
* Search Operation in Hash Tables 
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fstream>

#define N 10007

using namespace std;
ofstream fout;

int h (int k, int i);
void insert (int k);
int search (int k);
void writeToFile (float fillingFactor, double avgEffortFound, int maxEffortFound, double avgEffortNotFound, int maxEffortNotFound, char * fileName);

int t[N];
int noAccessesSuccessfulSearch, noOpSuccessfulSearch, maxSuccessfulSearch, noAccessesUnsuccessfulSearch, noOpUnsuccessfulSearch, maxUnsuccessfulSearch;
double avgSuccessfulSearch, avgUnsuccessfulSearch;

void main() {
	double fillFactor[5] = {0.8,0.85,0.9,0.95,0.99};
	int n;



	/*for (int i=0;i<20;i++)
		t[i]=0;

	for (int i=0;i<20;i++) {
		int random = rand()%20;
		printf ("%d ", random);
		insert (random);
	}
	printf("\n");

	for (int i=0;i<20;i++)
		printf ("%d ", t[i]);

	getch();*/

	fout.open ("hash.csv", ios::app );
	fout<<"Load factor, Avg. Effort Found, Max. Effort Found, Avg. Effort Not Found, Max. Effort Not Found\n";
	fout.close();
	
	// for each fillFactor value
	for (int k = 0; k< 5; k++) {

		//repeat the operation 5 times
		for (int j=0; j<5; j++ ) {
			int array[N];
			n = fillFactor[k]*N;
			for(int i=0; i<N; i++)
				t[i]=-1;

			// generates the elements and adds them to the hash table
			for(int i=0; i<n; i++) {
				int random = rand();
				insert (random);
				array[i]=random;
			}

			// Search, in each case, m random elements (m ~ 3000), such that approximately half 
			//of the searched elements will be found in the table, and the rest will not be found
			for (int i=0; i < 1500; i++) {
				if (search(array[rand()%N])<0) {
					noOpUnsuccessfulSearch ++; 	
					avgUnsuccessfulSearch += noAccessesUnsuccessfulSearch;
					noAccessesUnsuccessfulSearch = 0;
				}
				else {
					noOpSuccessfulSearch ++;
					avgSuccessfulSearch += noAccessesSuccessfulSearch;
					noAccessesSuccessfulSearch = 0;
				}
			}

			for (int i=0; i < 1500; i++) {
				int randomValue = rand() + 100000;
				if (search(randomValue)<0) {
					noOpUnsuccessfulSearch ++; 	
					avgUnsuccessfulSearch += noAccessesUnsuccessfulSearch;
					noAccessesUnsuccessfulSearch = 0;
				}
				else {
					noOpSuccessfulSearch ++;
					avgSuccessfulSearch += noAccessesSuccessfulSearch;
					noAccessesSuccessfulSearch = 0;
				}
			}
			

		}

		if ( noOpSuccessfulSearch == 0) 
			noOpSuccessfulSearch ++;
		if ( noOpUnsuccessfulSearch == 0) 
			noOpUnsuccessfulSearch ++;

		avgSuccessfulSearch = (double)avgSuccessfulSearch/noOpSuccessfulSearch;
		avgUnsuccessfulSearch = (double)avgUnsuccessfulSearch/noOpUnsuccessfulSearch;

		writeToFile (fillFactor[k], avgSuccessfulSearch, maxSuccessfulSearch, avgUnsuccessfulSearch, maxUnsuccessfulSearch, "hash.csv");

		noAccessesSuccessfulSearch = noOpSuccessfulSearch = avgSuccessfulSearch = maxSuccessfulSearch = 0;
		noAccessesUnsuccessfulSearch = noOpUnsuccessfulSearch = avgUnsuccessfulSearch = maxUnsuccessfulSearch = 0;
	}
} 

int h(int k, int i) {
	return (k + i + 2*i*i) % N;
}

void insert (int k) {
	int i=0;
	while (t[h(k,i)] != -1)
		i++;							
	t[h(k,i)] = k;
}

int search (int k) {
	int i=0;
	while (t[h(k,i)] != k && t[h(k,i)] != -1 && i < 10007)   
		i++;     
	if (t[h(k,i)] == -1) {
		noAccessesUnsuccessfulSearch += i;
		if (noAccessesUnsuccessfulSearch > maxUnsuccessfulSearch)
			maxUnsuccessfulSearch = noAccessesUnsuccessfulSearch;
		return -1;
	}

	noAccessesSuccessfulSearch = i+1;
	if (noAccessesSuccessfulSearch > maxSuccessfulSearch)
		maxSuccessfulSearch = noAccessesSuccessfulSearch;
	return h(k,i);	
}

void writeToFile (float fillingFactor, double avgEffortFound, int maxEffortFound, double avgEffortNotFound, int maxEffortNotFound, char * fileName) {
	fout.open (fileName, ios::app );
	fout<<fillingFactor<<","<<avgEffortFound<<","<<maxEffortFound<<","<<avgEffortNotFound<<","<<maxEffortNotFound<<"\n";
	fout.close();
}

