/*
author:***ANONIM*** ***ANONIM***
  group:***GROUP_NUMBER***
  date:13.03.2013
  program description: implements three direct sorting methods, namely the insertion sort, selection sort and bubblesort. 
					   All thre sorting algorithms are run for the best, worst and average cases for input data in the range [100,10000] with step of 100.
					   The number of comparisons and assignments are counted and using the Profiler tool the results are displayed on charts. 
				   	   For the average case 5 sets of random data are given as input.

Insertion sort:      at every step in the algorithm we have two sets, one is sorted, the other is unsorted. Starting with the initial 
					 state, that the sorted set consists of the first element from the array we have to sort (one element is already sorted), the 
					 unsorted the rest of the elements.At every step we choose the first element from the unsorted set and put it in the right place in the 
					 sorted one, thus the dimension of the sorted set grows and the one of the unsorted shrinks. We repeat until all elements are in the
					 sorted set.

Selection sort:      at every step the minimum is selected from the unsorted part of the array and put in the right place in the sorted one. Start with the initial 
					 state, that the ordered set is empty and the unordered set has all the elements of the array. Repeat the step until the next element which 
					 is the maximum from the array, so the algorithm stops.

Bubble sort:		 at every step two consecutive elements from the array are compared and if the element from the lower index is grater than the element 
					 from the higher index they are swapped. The algorithm stops when there are no more swaps in a step.

Best case : -for all thre algorithms the array is already sorted
			-insertion and bubble sort n-1 steps for comparisons , the selection sort has way more because of selecting the minimum
			-insertion sort has 2*n-2 assignments, the bubble and selection sort doesn't do any assignment

Worst case: -for insertion sort and bubble sort the worst case is having the array inversly sorted
			-for selection sort the worst case is having the array in form 2,3,4,...,n,1
			-all thre algorithms perform almost the same number of comparisons
			-assignments : bubble sort ~ 150.000.000 for input data of dimension 10.000
						   insertion sort ~ 50.000.000 for input data of dimension 10.000
						   selection sort ~ 30.000 for input data of dimension 10.000

Average case: -for every dimension 5 arrays with elements computed randomly
			  -all three algorithms perform a great number of comparisons (120-240 million for input data of dimension 10.000)
			  -assignments : bubble sort ~ 370.000.000 for input data of dimension 10.000
						     insertion sort ~ 125.000.000 for input data of dimension 10.000
						     selection sort ~ 150.000 for input data of dimension 10.000

One can see that for large input data the bubble and insertion sort performs a huge number of assignments, but the number of comparisons doesn`t vary
on such a great scale.

All three algorithms are stable, because they keep the relative position of same elements.
None of these algorithms is optimal, their running time being O(n^2). 
*/
#include <stdio.h>
#include "Profiler.h"

Profiler p("lab2_2");

void insertion(int A[],int n)
{
		p.countOperation("insertion_assign",n,0);
	    p.countOperation("insertion_comp",n,0);
	for(int i=2;i<=n;i++)
					{
		
						A[0]=A[i];
						p.countOperation("insertion_assign",n);
						int j=i-1;
						while (A[j]>A[0] && j>0)
						{
							p.countOperation("insertion_comp",n);
							A[j+1]=A[j];
							p.countOperation("insertion_assign",n);
							j=j-1;
			
						}
						p.countOperation("insertion_comp",n);
						A[j+1]=A[0];
						p.countOperation("insertion_assign",n);

					}

}

void selection(int A[],int n)
{
int pos,i,j;
    p.countOperation("selection_comp",n,0);
	p.countOperation("selection_assign",n,0);
		for (j=1;j<n;j++)
		{
		pos=j;
		for (i=j+1;i<=n;i++)
			{
			p.countOperation("selection_comp",n);
			if (A[i]<A[pos]) 
				pos=i;
			}
		if (j!=pos) 
			{
			int aux=A[j];
			A[j]=A[pos];
			A[pos]=aux;
			p.countOperation("selection_assign",n,3);
			}
		}

}

void bubble(int A[],int n)
{
	 int i=1,j;
     p.countOperation("bubble_comp",n,0);
	 p.countOperation("bubble_assign",n,0);
		bool sorted=false;

		while(!sorted)
		{
			sorted=true;
			for(j=1;j<=n-i;j++)
			{
				p.countOperation("bubble_comp",n);
				if(A[j]>A[j+1])
				{
					p.countOperation("bubble_assign",n,3);
					int aux=A[j];
					A[j]=A[j+1];
					A[j+1]=aux;
					sorted=false;
				}
			}
			i++;
		}
}

void printArray(int A[],int n)
{
	for (int i=1;i<=n;i++)
		printf("%d ",A[i]);
	printf("\n");
}

int main()
{
	int n,i;
	int B[10001];
	/* best case 
	for(n=100;n<=10000;n+=100)
	{
		for(i=1;i<=n;i++)
			B[i]=i;

	insertion(B,n);
	selection(B,n);
	bubble(B,n);
	printf("%d  ",n);
	}
	p.createGroup("Best case comparison all","bubble_comp","insertion_comp","selection_comp");
	p.createGroup("Best case assignment all","bubble_assign","insertion_assign","selection_assign");
	p.addSeries("bubble_comp_assign", "bubble_assign","bubble_comp");
	p.addSeries("insertion_comp_assign", "insertion_assign","insertion_comp");
	p.addSeries("selection_comp_assign", "selection_assign","selection_comp");
	p.createGroup("Best case assignment+comparison all","bubble_comp_assign","insertion_comp_assign","selection_comp_assign");
	p.showReport(); */

	/* worst case 
		for(n=100;n<=10000;n+=100)
	{
		for(i=1;i<=n;i++)
			B[i]=n-i+1;

		insertion(B,n);

		for(i=1;i<n;i++)
			B[i]=i+1;
		B[n]=1;

		selection(B,n);

	    for(i=1;i<=n;i++)
			B[i]=n-i+1;
	    
		bubble(B,n);

	printf("%d  ",n);
	}
	p.createGroup("Worst case comparison all","bubble_comp","insertion_comp","selection_comp");
	p.createGroup("Worst case assignment all","bubble_assign","insertion_assign","selection_assign");
	p.addSeries("bubble_comp_assign", "bubble_assign","bubble_comp");
	p.addSeries("insertion_comp_assign", "insertion_assign","insertion_comp");
	p.addSeries("selection_comp_assign", "selection_assign","selection_comp");
	p.createGroup("Worst case assignment+comparison all","bubble_comp_assign","insertion_comp_assign","selection_comp_assign");
	p.showReport(); */

	/* avg case 
	for(n=100;n<=10000;n+=100)
	{
	for(int k=0;k<5;k++)
	{
		FillRandomArray(B,n);
		insertion(B,n);
		FillRandomArray(B,n);
		selection(B,n);
		FillRandomArray(B,n);
		bubble(B,n);
	}
		printf("%d  ",n);
	}
	p.createGroup("Avg case comparison all","bubble_comp","insertion_comp","selection_comp");
	p.createGroup("Avg case assignment all","bubble_assign","insertion_assign","selection_assign");
	p.addSeries("bubble_comp_assign", "bubble_assign","bubble_comp");
	p.addSeries("insertion_comp_assign", "insertion_assign","insertion_comp");
	p.addSeries("selection_comp_assign", "selection_assign","selection_comp");
	p.createGroup("Avg case assignment+comparison all","bubble_comp_assign","insertion_comp_assign","selection_comp_assign");
	p.showReport();*/

	int C[11] = {0,2,6,9,8,7,3,4,1,0,8};

	insertion(C,10);
	printArray(C,10);
	int D[11] = {0,2,6,9,8,7,3,4,1,0,8};
	selection(D,10);
	printArray(D,10);
	int E[11] = {0,2,6,9,8,7,3,4,1,0,8};
	bubble(E,10);
	printArray(E,10);

return 0;
}