/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
17 Mai 2013
Arbori multicai

Am avut de implementat o structura de tip arbore multicai. Arborele multicai este un arbore cu mai multi fii. Acestia sunt memorati ca o lista 
de fii. Am implementat lista de fii ca o lista dinamica, nu statica, deoarece daca ar fi statica am fi obligati pentru fiecare nod sa asignam
un vector de dimensiune = nr de noduri, ceea ce ar dauna mult memoriei. Fii memorati ca o lista dinamica nu sunt mai usor de gestionat dar dau 
performanta crescuta algoritmului

In cadrul implementarii am avut de transformat dintr-un sir de parinti intr-un arbore multicai, si dintr-un arbore multica intr-un arbore binar,
cu stanga = fiu si dreapta = frate

*/

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

typedef struct _mult_node{
    int key;
    int count;
    int quantity;
    struct _mult_node **child;
}mult_node;

typedef struct _binary_node{
	int key;
	struct _binary_node *left,*right;
}binary_node;

mult_node* createMN(int key1){
    mult_node *newn;
    newn = (mult_node*)malloc(sizeof(mult_node));
    newn->key = key1;
    newn->count = -1;
    newn->quantity = 0;
    newn->child = NULL;
	return newn;
}

void insertMN(mult_node *parent, mult_node *childadd){

      parent->count += 1;
    if(parent->quantity == 0){
        parent->child = (mult_node**)malloc(5*sizeof(mult_node*));
        parent->quantity = 5;
    }
    if (parent->count >= parent->quantity) {
        parent->quantity *= 2;
		parent->child =  (mult_node**)realloc(parent->child, parent->quantity);       
    }
  
	parent->child[parent->count] = childadd;
}

mult_node* transform(int t[],int size){
	mult_node *root;
	mult_node **nodes;
	int i;
	nodes = (mult_node**)malloc(size*sizeof(mult_node*));
	for( i = 0; i < size; i++){
		nodes[i] = createMN(i);
	}
	for( i = 0; i < size; i++){
		if(t[i]!=-1){
			insertMN(nodes[t[i]],nodes[i]);
		}
		else{
			root = nodes[i];
		}		
	}
	return root;
}


void afisareArbore(mult_node *nod, int depth){
	int level;
	if(nod == NULL){
	return;
	}
	for(level=0; level<depth; level++)
		printf("       ");
	printf("%d\n",nod->key);
	for(int i = 0; i<= nod->count; i++){
		afisareArbore(nod->child[i],depth+1);
	}
}

void afisareArboreBinar(binary_node *nod, int depth){
	int level;
	if(nod == NULL){
	return;
	}
	
	afisareArboreBinar(nod->left,depth+1);

	for(level=0; level<depth; level++)
		printf("       ");
	printf("%d\n",nod->key);


	afisareArboreBinar(nod->right,depth);
	
	
}

binary_node* CreareArboreBinar(mult_node* nod, struct _mult_node **child,int dim){
	int i;
	binary_node *x;
	if(nod == NULL){
		return NULL;
	}
	x= (binary_node*)malloc(sizeof(binary_node));
	x->key = nod->key;
	struct _mult_node **child1;
	child1 = (_mult_node **)malloc(sizeof(_mult_node*));
	for(i=0;i<dim;i++){
		child1[i]=child[i+1];
	}
	if(nod->count == -1){
		x->left = NULL;
	}
	else{
		x->left = CreareArboreBinar(nod->child[0],nod->child,nod->count);
	}
	if(dim <= 0){
		x->right = NULL;
	}
	else{
		x->right = CreareArboreBinar(child[1],child1,dim-1);
	}
	return x;
}

int main(){
    int t[]={9,5,5,9,-1,4,4,5,5,4,2};
    int size = sizeof(t)/sizeof(t[0]);
	mult_node *root = transform(t,size);
	afisareArbore(root,1);	
	printf("==============================================\n");
	binary_node *rootB = CreareArboreBinar(root,NULL,-1);
	afisareArboreBinar(rootB,1);

	getch();
}
