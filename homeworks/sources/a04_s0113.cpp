/*
	Name: ***ANONIM*** ***ANONIM***�n
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently an O(nlogk) method for merging k 
				  sorted sequences, where n is the total number of elements.

	Analysis: -in the first case, where k is fixed at the asked values, we can observe that the results
				represent a linear growth with respect to n
			  -in the second case, where n is fixed at 10000 and k is varied, we can observe that the
			   result is growing logarithmically with respect to k
			  -these prove the complexity of O(nlogk)
*/
#include<stdio.h>
#include<stdlib.h>
#define KMAX 10010

int i,j,k,n,m,x, heapSize, count;
int I,T;

struct HEAP
{
	int value, index;
}heap[KMAX];

struct LLST
{
	int key;
	LLST *next;
}*kList[KMAX], *result;

void insert_into_list(int listnr, int key)
{
	LLST *node=new LLST;
	node->key=key;
	node->next=kList[listnr];
	kList[listnr]=node;
}

void insert_into_result(int key)
{
	LLST *node=new LLST;
	node->key=key;
	node->next=result;
	count++;
	result=node;
}


int get_key(int listnr)
{
	if(kList[listnr]==NULL)
	{
		heapSize--;
		return -1;
	}
	LLST *aux=kList[listnr];
	int x=aux->key;
	kList[listnr]=kList[listnr]->next;
	free(aux);
	return x;
}

void build_initial_heap()
{
	for(i=1;i<=k;i++)
	{
		heap[i].value=get_key(i);
		heap[i].index=i;
		count++;
	}
	heapSize=k;
}

void max_heapify(int pos)
{
	int left=2*pos;
	int right=left+1;
	int largest;
	if(left <= heapSize && heap[left].value > heap[pos].value)
		largest=left;
	else largest=pos;
	count++;
	if(right <= heapSize && heap[right].value > heap[largest].value)
		largest=right;
	count++;
	if(largest!=pos)
	{
		HEAP aux=heap[pos];
		heap[pos]=heap[largest];
		heap[largest]=aux;
		count+=3;
		max_heapify(largest);
	}
}

void build_heap()
{
	for(i=heapSize/2;i>=1;i--)
		max_heapify(i);
}

void print_result()
{
	while(result!=NULL)
	{
		printf("%d, ", result->key);
		result=result->next;
	}
	printf("\n");

}

int val;

void merge()
{	
	for(i=1;i<=n-k;i++)
	{
		insert_into_result(heap[1].value);
		val=get_key(heap[1].index);
		if(val>=0)
			heap[1].value=val;
		else heap[1]=heap[heapSize+1];
		count++;
		max_heapify(1);
	}
	for(i=1;i<=k;i++)
	{
		insert_into_result(heap[1].value);
		heapSize--;
		heap[1]=heap[heapSize+1];
		count++;
		max_heapify(1);
	}
}

void free_result()
{
	while(result!=NULL)
	{
		LLST *aux=result;
		result=result->next;
		free(aux);
	}
}

void test()
{
	k=4;
	n=20;
	for(int i=1;i<=20;i++)
		insert_into_list(i%4+1, i);
	build_initial_heap();
	build_heap();
	merge();
	print_result();
	free_result();
}

int main()
{
	test();
	count=0;
	
	freopen("test.txt", "r",stdin);
	freopen("average1.csv", "w", stdout);
	printf("n,k=5,k=10,k=100");
	scanf("%d", &T);
	int i,j,I;
	for(I=1;I<=T;I++)
	{
		//printf("%d\n", I);
		count=0;
		free_result();
		scanf("%d%d", &n, &k);
		if(I%3==1)
			printf("\n%d", n);
		for(i=1;i<=k;i++)
		{
			scanf("%d", &m);
			for(j=1;j<=m;j++)
			{
				scanf("%d", &x);
				insert_into_list(i, x);
			}
		}
		build_initial_heap();
		build_heap();
		merge();
		printf(",%d",count);
		//print_result();
	}
	
	/*
	freopen("test2.txt", "r",stdin);
	freopen("average2.csv", "w", stdout);
	printf("k,count\n");
	scanf("%d", &T);
	int i,j,I;
	for(I=1;I<=T;I++)
	{
		//printf("%d\n", I);
		count=0;
		free_result();
		scanf("%d%d", &n, &k);
		for(i=1;i<=k;i++)
		{
			scanf("%d", &m);
			for(j=1;j<=m;j++)
			{
				scanf("%d", &x);
				insert_into_list(i, x);
			}
		}
		build_initial_heap();
		build_heap();
		merge();
		printf("%d,%d\n",k,count);
		//print_result();
	}
	*/

	return 0;
}