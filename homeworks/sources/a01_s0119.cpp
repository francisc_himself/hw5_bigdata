//AVERAGE CASE 
//Comparisons               - from the point of view of comparisons, the best method to use is the Insetion Sort
//Assignments               - the best method to be used in the average case is the Selection Sort
//Comparisons + Assignments - the Insertion Sort seems to be the most efficient method to be used in the average cases.

//BEST CASE
//Comparisons               - the best choice is the Insertion Sort. It makes a number of comparisons equal to the length if the string
//Assignments               - we can choose either Bubble Sort or Insertion Sort. They are doing the same number of assignments. The 
				            //Selection sort is not a good choice, because it takes approximately duble the number of assignments.
//Comparisons + Assignments - The minimum number of comparisons and assignments is done by the Insertion Sort algorithm.

//WORST CASE
//Comparisons               - it's best to use Insertion Sort
//Assignments               - It's better to use Selection Sort
//Comparisons + Assignments - It's better to use selection

//All the functions in all three cases are parabola.
//All three algorithms are stable.

#include<iostream>
#include<conio.h>
#include<stdio.h>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;

void read();
void copy();
void bubbleSort();
void insertionSort();
void selectionSort();
void display();
void averageCase();
void bestCase();
void worstCase();

int n, a[10001], x[10001], nr =0;

void read()
{
    printf("\nInput a value for n: ");
    scanf_s("%d",&n);
    for(int i=1; i<=n; i++)
    {
        printf("\nvalue :");
        scanf_s("%d",a+i);
    }
}
void copy()
{
    for(int q=1;q<=n;q++)
        x[q] = a[q];
}
void display()
{
	printf("\n");
    for(int i=1; i<=n; i++)
        printf("%d ",x[i]);
	printf("\n");
}
void averageCase()
{
	for(int i=1; i<=5; i++)
	{
		n=100;
		while(n<=10000)
		{	
			nr++;
			printf("\nExecution nr: %d     n = %d", nr, n);
			FillRandomArray(a, n);
			copy();
			bubbleSort();
			copy();
			insertionSort();
			copy();
			selectionSort();
			n+=100;
		}
	profiler.createGroup("Comparisons","comparisons","comparisonsIns","comparisonsSel");
	profiler.createGroup("Assignments","assignments","assignmentsIns","assignmentsSel");
	profiler.createGroup("Selection Sort assignments", "assignmentsSel");
	profiler.addSeries("Bubble", "assignments", "comparisons");
	profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
	profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
	profiler.createGroup("All","Bubble","Insertion","Selection");
	profiler.showReport();
	}
}
void bestCase()
{
	//for Bubble Sort the best case is when the string is already sorted
	
	n=100;
	while(n <= 10000)
	{
		nr++;
		printf("\nExecution nr: %d     n = %d", nr, n);
		FillRandomArray(a,n,1,10000,false,1);
		copy();
		bubbleSort();
	
		// for Insertion Sort, also, the best case is when the string is already sorted
		copy();
		insertionSort();
	
		// for Selection Sort the best case is when the string is already sorted
		copy();
		selectionSort();

		n+=100;
	}
	profiler.createGroup("BestCaseComparisons","comparisons","comparisonsIns","comparisonsSel");
	profiler.createGroup("BestCaseComparisonsInsertion","comparisonsIns");
	profiler.createGroup("BestCaseAssignments","assignments","assignmentsIns","assignmentsSel");
	profiler.addSeries("Bubble", "assignments", "comparisons");
	profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
	profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
	profiler.createGroup("BestCaseAll","Bubble","Insertion","Selection");
	profiler.createGroup("BestCaseAllInsertion","Insertion");
	profiler.showReport();
}
void worstCase()
{
	n=100;
	while(n <= 10000)
	{
		nr++;
		printf("\nStep : %d, n = %d", nr, n);
		FillRandomArray(a,n,1,10000,false,2);
		//for Bubble Sort, the worst case is when the array is reversed
		copy();
		bubbleSort();
		//for insertion sort also the worst case is when the string is reversed
		copy();
		insertionSort();
		//selection sort is appropriate only for small n
		FillRandomArray(x,n/2,1,10000,false,2);
		FillRandomArray(x+(n+1)/2+1,n/2,1,10000,false,1);
		//display();
		selectionSort();	
		n+=100;
	}
	profiler.createGroup("WorstCaseComparisons","comparisons","comparisonsIns","comparisonsSel");
	profiler.createGroup("WorstCaseAssignments","assignments","assignmentsIns","assignmentsSel");
	profiler.addSeries("Bubble", "assignments", "comparisons");
	profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
	profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
	profiler.createGroup("WorstCaseAll","Bubble","Insertion","Selection");
	profiler.showReport();
}
void bubbleSort()
{
 //   printf("\nBubble Sort of the array: ");
    int inter=0, aux, ass = 0, comp = 0;
    do{
        inter = 0;
        for(int i=1; i<n-1; i++)
        {
			profiler.countOperation("comparisons",n);
		//	comp++;
            if(x[i]>x[i+1])
            {
                aux = x[i];
                x[i] = x[i+1];
                x[i+1] = aux;
				profiler.countOperation("assignments",n,3);
                ass +=3;
                inter = 1;
            }
        }
    }while(inter);
	if(ass == 0) profiler.countOperation("assignments",n,0);
}
void insertionSort()
{
//    printf("\nInsertion sort of the array: ");
    int i, buff, k, ass = 0, comp = 0;
    for(i=2; i<=n; i++)
    {
        k = i-1;
        buff = x[i];
		profiler.countOperation("assignmentsIns",n);
 //      ass++;
        while(x[k] > buff && k>0)
        {    
			profiler.countOperation("comparisonsIns",n);
   //         comp ++;
            x[k+1] = x[k];
			profiler.countOperation("assignmentsIns",n);
  //         ass++;
            k--;
		}
       x[k+1] = buff;
	   profiler.countOperation("assignmentsIns",n);
  //          ass++;
	}  
		profiler.countOperation("comparisonsIns",n);
    //   comp++;
  //  printf("\n	Assignments : %d \n	Comparisons: %d",ass, comp);
}	
void selectionSort()
{
  // printf("\nSelection Sort of the array");
    int imin, min, comp = 0, ass = 0;
    for(int i=1; i<n; i++)
    {
        min = x[i];
		profiler.countOperation("assignmentsSel",n);
//        ass ++ ;
		 imin = i;
        for(int j = i+1; j <= n; j++)
        {
			profiler.countOperation("comparisonsSel",n);
//            comp ++;
            if(x[j] > min);
            else
            {
                min = x[j];
				profiler.countOperation("assignmentsSel",n);
//                ass++;
                imin = j;
            }
        }
        if(i != imin)
        {
            min = x[imin];
            x[imin] = x[i];
            x[i] = min;
			profiler.countOperation("assignmentsSel",n,3);
//            ass+=3;
        }
    }
   // printf("\n	Assignments : %d \n	Comparisons: %d",ass, comp);
}

int main()
{
   /* read();
    copy();
	printf("\n	The unordered sequence: ");
    display();
    bubbleSort();
	printf("\n	The output sequence after bubble sort: ");
    display();
    copy();
    insertionSort();
	printf("\n	The output sequence after insertion sort: ");
    display();
    copy();
    selectionSort();
	printf("\n	The output sequence after selection sort: ");
    display();*/
	averageCase();
	//bestCase();
	//worstCase();
    return 0;
}
