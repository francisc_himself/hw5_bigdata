#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>
#define max 10000
/*
Se observa in cazul cel mai defavorabil :
        Comparatii cele mai putine: Insert
        Asignari cele mai putine: Bubble

Se observa in cazul cel mai favorabil :
        Comparatii cele mai putine: Bubble
        Asignari cele mai putine: Bubble

Se observa in cazul mediu :
        Comparatii cele mai putine: Insert
        Asignari cele mai putine: Select

In concluzie cea mai putin eficienta metoda este sortarea Bubble (e eficienta doar
in cazul cel mai favorabil), iar intre Selectie si Insertie depinde de cazuri.

*/
int v[10000];
int cop1[10000],cop2[10000];
int atrbubble, atrins, atrsel;
int compbubble,compins, compsel;

void insertieInv(int v[],int n)
{
int k,x;
    int I;
    int J;
	for(I=1;I<n;I++)
	{
		x=v[I];
		J=0;
		while(v[J]>x)
			{
			    compins++;
			    J=J+1;
			}
		for(k=I;k>=J+1;k--)
			{
			    atrins++;
			    v[k]=v[k-1];
			}

		v[J]=x;
		atrins++;
	}



}

void bubble(int v[],int n) // verificat ok
{
    atrbubble=0;
	compbubble=0;
        int J;
	int ok=0;
	while (ok==0)
	{ ok=1;
	for (J=0; J<=n-1;J++){
			compbubble++;
			if (v[J]>v[J+1])
				{ int aux=0;
					atrbubble++;
					aux=v[J];
					v[J]=v[J+1];
					v[J+1]=aux;
					ok=0;
                }
		}
	}
}

void selectie(int v[],int n) // verificat ok
{
    atrsel=0;
    compsel=0;
    int I;
    int J;
	for (I=0; I<n;I++){
	int Imin=I;
	for (J=I+1; J<n; J++){
		compsel++;
		if (v[J]<v[Imin])
                Imin=J;
	}
        int aux=v[I];
        v[I]=v[Imin];
        v[Imin]=aux;
        atrsel++;
	}


}

void insertie(int v[],int n) //facut ok
{
    compins=0;
    atrins=0;
int k,x;
    int I;
    int J;
	for(I=1;I<n;I++)
	{
		x=v[I];
		J=0;
		compins++;
		while(v[J]<x)
			{
			    compins++;
			    J=J+1;
			}
		for(k=I;k>=J+1;k--)
			{
			    atrins++;
			    v[k]=v[k-1];
			}

		v[J]=x;
		atrins++;
	}



}
int main()
{
	int proba[6];
	int p;
	for (p=0;p<4;p++)
	{
		printf("Elem %d: ",p+1);
		scanf("%d", &proba[p]);
	}
	insertie(proba,4);
	printf("Sirul ordonat: ");
	for (p=0;p<4;p++)
	{
		
		printf("%d ",proba[p]);
		
	}

    FILE *f,*fav1,*defav1;
    f=fopen("SortariMediuToate.csv","w");

    fprintf(f,"n,Ass Bubble,Comp Bubble,Ass+Comp Bubble, \t, \t, ");
    fprintf(f,"n,Ass Select,Comp Select,Ass+Comp Select, \t, \t, ");
    fprintf(f,"n,Ass Insert,Comp Insert,Ass+Comp Insert \n");

    fav1=fopen("SortariFavToate.csv","w");
    fprintf(fav1,"n,Ass Bubble,Comp Bubble,Ass+Comp Bubble, \t, \t, ");
    fprintf(fav1,"n,Ass Select,Comp Select,Ass+Comp Select, \t, \t, ");
    fprintf(fav1,"n,Ass Insert,Comp Insert,Ass+Comp Insert \n");

    defav1=fopen("SortariDefavToate.csv","w");
    fprintf(defav1,"n,Ass Bubble,Comp Bubble,Ass+Comp Bubble, \t, \t, ");
    fprintf(defav1,"n,Ass Select,Comp Select,Ass+Comp Select, \t, \t, ");
    fprintf(defav1,"n,Ass Insert,Comp Insert,Ass+Comp Insert \n");

	for (int n=100; n<max; n+=500){

//printf("sirul");
	for (int k=0;k<5;k++)
		{

			srand(time(NULL));
			for (int a=0;a<n;a++){
				v[a]=rand();
			//	printf("%d ",v[a]);
			}
			//printf("\n");
			for (int a=0;a<n;a++){
				cop1[a]=v[a];
				cop2[a]=v[a];
			//	printf("%d ",v[a]);
			}
			bubble(v,n);
            insertie(cop1,n);
			selectie(cop2,n);
			}
    fprintf(f,"%d,%d,%d,%d, \t, \t, ",n,atrbubble/5,compbubble/5, (atrbubble+compbubble)/5);
    fprintf(f,"%d,%d,%d,%d, \t, \t, ",n,atrsel/5,compsel/5, (atrsel+compsel)/5);
    fprintf(f,"%d,%d,%d,%d \n",n,atrins/5,compins/5, (atrins+compins)/5);
	//acum vectorul e aranjat => caz favorabil
    bubble(v,n);
    insertie(cop1,n);
    selectie(cop2,n);
    fprintf(fav1,"%d,%d,%d,%d, \t, \t, ",n,atrbubble,compbubble, atrbubble+compbubble);
    fprintf(fav1,"%d,%d,%d,%d, \t, \t, ",n,atrsel,compsel, atrsel+compsel);
    fprintf(fav1,"%d,%d,%d,%d \n",n,atrins,compins, atrins+compins);

    insertieInv(v,n);
    insertieInv(cop1,n);
    insertieInv(cop2,n);
    //acum vectorul e aranjat invers => caz nefavorabil

    bubble(v,n);
    insertie(cop1,n);
    selectie(cop2,n);
    fprintf(defav1,"%d,%d,%d,%d, \t, \t, ",n,atrbubble,compbubble, atrbubble+compbubble);
    fprintf(defav1,"%d,%d,%d,%d, \t, \t, ",n,atrsel,compsel, atrsel+compsel);
    fprintf(defav1,"%d,%d,%d,%d \n",n,atrins,compins, atrins+compins);

//	printf("%d Bubble atr \n",atrbubble/5);
//	printf("%d Bubble comp \n",compbubble/5);
//	printf("%d Inserare atr \n",atrins/5);
//	printf("%d Inserare comp \n",compins/5);
//	printf("%d Seletie atr \n",atrsel/5);
//	printf("%d Selectie comp \n",compsel/5);

	//getch();
	}
  fclose(f);
	fclose(fav1);
	fclose(defav1);
	getch();
return 0;

}
