// dfs.cpp : Defines the entry point for the console application.
//Depth-First Search - cautarea in adancime

#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>

#define ALB 0
#define GRI 1
#define NEGRU 2

struct TIP_NOD
	{
		int valoare;
		TIP_NOD *urm;
	};

typedef struct Prop
{
	int culoare;
	int parinte;
	//un varf are doua marcaje de timp
	int d; //momentul cand un varf este descoperit prima oara(si colorat gri)
	int f; //momentul cand cautarea termina de examinat lista de adiacenta a unui varf
} PROP;


TIP_NOD *prim[60001], *ultim[60001];
PROP prop[60001];
int timp;
int nr_operatii;
//graf aciclic - nu are cicluri
int graf_aciclic=1; //presupunem ca graful e aciclic si orientat, deci poate fi sortat topologic


//pentru reprezentarea grafului sub forma de liste de adiacenta
	bool verif (int x, int y)
		{
			TIP_NOD *p;
			p=prim[x];
			while(p!=NULL)
				{
					if(y==p->valoare)
					return false; 
					p=p->urm;
				}
			return true;
		}

	void genereaza(int nr_varfuri,int nr_muchi)  //nr_varfuri = numarul de varfuri, nr_muchii = numarul de muchii
		{
			int i,u,v;
			for(i=0;i<=nr_varfuri;i++)
			prim[i]=ultim[i]=NULL;
			i=1;
			while (i<=2*nr_muchi)
				{
					u=rand()%nr_varfuri+1;
					v=rand()%nr_varfuri+1;
			if (u!=v)
				{
					if(prim[u]==NULL)
						{
							prim[u]=(TIP_NOD *)malloc(sizeof(TIP_NOD *));
							prim[u]->valoare=v;
							prim[u]->urm=NULL;
							ultim[u]=prim[u];
							i++;
						}
				   else 
						{
						if (verif (u, v))
				    	 {
							TIP_NOD *p;
			            	p=(TIP_NOD *)malloc(sizeof(TIP_NOD *));
							p->valoare=v;
							p->urm=NULL;
							ultim[u]->urm=p;
							ultim[u]=p;
							i++;
						}
					  }
		   }
	   }
   }

	void initializare(int n)
		{
			timp = 0; //contorul de timp 
			nr_operatii = 0; 
			for(int u = 1 ; u <= n ; u++)
				{
					prop[u].f = 0;
					prop[u].d = 0;
					prop[u].parinte = 0;
					prop[u].culoare = ALB;  //la inceput toate varfurile se coloreaza in alb
				}
		}


	void CautareAdancime_Vizita(int u) //de fiecare data cand procedura CautareAdancime_Vizita este apelata, varful u devine radacina unui nou arbore fin padurea de adancime
		{							   //cand CautareAdancime_Vizita se termina, fiecarui varf u i s-a atribuit un timp de descoperire si unu de terminare
			printf("%d ",u);
			prop[u].culoare = GRI; //varful u este colorat in gri
			prop[u].d = timp;	   //se memoreaza timpul de descoperire salvand variabila timp	
			timp++;	
			TIP_NOD *x;
			int v;
			x = prim[u];
			nr_operatii++; 
			while(x!=NULL)
				{
					v = x->valoare;
					x = x->urm;
					nr_operatii++;    
					if(prop[v].culoare==ALB)  //daca s-a gasit un nod ALB,varful u devine radacina
						{
							prop[v].parinte = u;
							nr_operatii++;  
							//printf("[%d %d] -> apartine arbore\n",u,v);
							//printf("%d\n",v);
							 CautareAdancime_Vizita(v);
						}
					else 
						{
							//daca e gri e muchie inapoi 
							/**muchie inapoi - sunt acele muchii (u,v) care unesc un varf u cu un stramos v*/
							/*buclele care pot aparea intr-un graf orientat sunt considerate muchii inapoi*/
							if (prop[v].culoare==GRI)
							graf_aciclic=0;
						}
		
				}
			prop[u].culoare = NEGRU;  
			prop[u].f = timp;
			timp = timp+1;
		}



	void Cautare_adancime(int n)
		{
			for(int u = 1 ; u <= n ; u++) //se parcurg toate varfurile si se initializeaza cu alb
				{
					prop[u].culoare = ALB;
					prop[u].parinte = 0;
				}
			for(int u = 1 ; u <= n ; u++) //se verifica fiecare varf caruia ii vine randul si, cand este gasit un varf alb, el este vizitat 
				{
					nr_operatii++;   
					if(prop[u].culoare==ALB)
						{
							CautareAdancime_Vizita(u);
						}
				}
		}

	void print(int nr_varfuri)
		{
			int i;
			TIP_NOD *p;
			for(i=1;i<=nr_varfuri;i++)
				{
					printf("%d : ",i);
					p=prim[i];
					while(p!=NULL)
						{
							printf("%d,",p->valoare);
							p=p->urm;
						}
			printf("\n");
				}
		}


	int main()
		{
			//n reprezinta numarul de varfuri ***ANONIM*** grafului, iar nr numarul de muchii
			int n,i,nr;
			int nrv,nre;
			n=5; nr=9;
			srand(time(NULL));
			genereaza(n,nr);
			print(n);
			initializare(n);
			printf("\nalgoritmul DFS:\n");
			Cautare_adancime(n);
			if (graf_aciclic==0)
				printf("\nGraful nu poate fi sortat topologic.");
			else
			printf("\nGraful nu poate fi sortat topologic.");
			FILE *f,*f1;
			/*f = fopen("fct1.csv","w");
			fprintf(f,"nrv=100,nre,nr_operatii\n");
			nrv=100;
			for(nre = 1000 ; nre <= 5000 ; nre = nre+100)
				{
					srand(time(NULL));
					genereaza(nrv,nre);
					initializare(nrv);
					Cautare_adancime(nrv);
					fprintf(f," %d , %d , %d\n",nrv,nre,nr_operatii);
				}
			fclose(f);*/

			/*f1 = fopen("fct2.csv","w");
			fprintf(f1,"nre=9000,nrv,nr_operatii\n");
			nrv=9000;
			for(nre = 110 ; nre <= 200 ; nre = nre+10)
				{
					
					srand(time(NULL));
					genereaza(nrv,nre);
					initializare(nrv);
					Cautare_adancime(nrv);
					fprintf(f1," %d , %d , %d\n",nrv,nre,nr_operatii);
				}
			fclose(f1);*/




getch();
return 0;
}


