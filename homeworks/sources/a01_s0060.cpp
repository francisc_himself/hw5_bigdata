// ***ANONIM*** ***ANONIM*** ***ANONIM***
// Grupa : ***GROUP_NUMBER***


//In cazul mediu statistic nr de atribuiri la metoda de sortare Bubble Sort este mai mare decat in cazul celorlalte metode, 
//rezultand ineficienta.
//La nr de atribuiri metoda de sortare prin selectie este mai eficienta.
//La nr de comparatii Bubble Sort este din nou cea mai ineficienta, metoda prin insertie fiind cea mai eficienta. 
#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <stdlib.h>

void randomMare(long int v[],long int n)
{
	for(int i=1;i<=n;i++)
	{
		v[i] = rand()%1000;
	}
}

void randomCresc(long int v[],long int n)
{
	v[0] = rand() % 10;
	for(int i=1;i<=n;i++)
	{
		v[i] = v[i-1] + rand() % 10;
	}
}

void randomDescr(long int v[],long int n)
{
	v[0] = rand() % 100 + n;
	for(int i=1;i<=n;i++)
	{
		v[i] = v[i-1] - rand() % 10;
	}
}
//se compara cate 2 elemente consecutive; 
//la o prcurgere se ajunge ca ultimul element este maxim; elementul maxim se pune in dreapta
//eficienta: def: O(n^2),mediu st O(n^2), fav O(n)
void bubble_sort(long int A[],long int n, long &atribuiri, long &comparatii)
{
	int aux;

	for (int i=1; i<=n-1; i++)
		for (int j=1; j<=n-1; j++)
		{
			comparatii++;
			if (A[j] > A[j+1])
			{
				atribuiri +=3;
				aux = A[j];
				A[j] = A[j+1];
				A[j+1] = aux;
			}
		}
}
//se trece elementul minim din sursa pe prima pozitie din sura
//fav: O(n^2); def: O(n^2),mediu st: O()
void selectie(long int A[],long int n,long &atribuiri, long &comparatii)
{
	int imin,aux,i;

	for(i=1;i<=n;i++)
	{
		imin = i;
		for(int j=i+1;j<=n;j++)
		{
			comparatii++;
			if(A[j]<A[imin])
			{
				imin = j;
			}
		}	
		atribuiri += 3;
		aux=A[i];
		A[i]=A[imin];
		A[imin]=aux;
	}
}
//eficienta O(n^2); fav O(n)
void insertie(long int A[],long int n, long &atribuiri, long &comparatii)
{
	int x,j;

	for(int i=2;i<=n;i++)
	{
		atribuiri++;
		x=A[i];
		j=1;
		comparatii++;
		while(j<i && A[j]<x)
		{
			comparatii++;
			j++;
		}
		for(int k=i-1;k>=j;k--)
		{
			atribuiri++;
			A[k+1]=A[k];
		}
		atribuiri++;
		A[j]=x;

	}
}

void copiere(long int A[], long int B[],long int n)
{
	for (int i=1; i<=n; i++)
		B[i] = A[i];
}

void favorabil()
{
	long int A[10001],B[10001];
	long int n;
	long atribuiri = 0;
	long comparatii = 0;

	FILE *f1;
	f1 = fopen ("bubble_favorabil.txt","w");
	
	FILE *f2;
	f2 = fopen ("selectie_favorabil.txt","w");
	
	FILE *f3;
	f3 = fopen ("insertie_favorabil.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		
		randomCresc(A,n);
		printf("%d  ",n);
		//bubble_sort
		atribuiri = 0;
		comparatii = 0;
		copiere(A,B,n);
		bubble_sort(B,n,atribuiri,comparatii);
		fprintf(f1,"%d \t %d \t %d \t %d\n",n,atribuiri,comparatii,atribuiri+comparatii);

		//selectie
		atribuiri = 0;
		comparatii = 0;
		copiere(A,B,n);
		selectie(B,n,atribuiri,comparatii);
		fprintf(f2,"%d \t %d \t %d \t %d\n",n,atribuiri,comparatii,atribuiri+comparatii);

		//insertie
		atribuiri = 0;
		comparatii = 0;
		copiere(A,B,n);
		insertie(B,n,atribuiri,comparatii);
		fprintf(f3,"%d \t %d \t %d \t %d\n",n,atribuiri,comparatii,atribuiri+comparatii);

		
	}
	
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

void defavorabil ()
{
	long int A[10001], B[10001];
	long int n;
	long atribuiri = 0;
	long comparatii = 0;

	FILE *f1;
	f1 = fopen ("bubble_dfav.txt","w");
	FILE *f2;
	f2 = fopen ("selectie_dfav.txt","w");
	FILE *f3;
	f3 = fopen ("insertie_dfav.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		randomDescr(A,n);
		printf("%d  ",n);
		//bubble_sort
		atribuiri = comparatii = 0;
		copiere(A,B,n);
		bubble_sort(B,n,atribuiri,comparatii);
		fprintf(f1,"%d \t %d \t %d \t %d\n",n,atribuiri,comparatii,atribuiri+comparatii);

		//sortare selectie
		atribuiri = comparatii = 0;
		copiere(A,B,n);
		selectie(B,n,atribuiri,comparatii);
		fprintf(f2,"%d \t %d \t %d \t %d\n",n,atribuiri,comparatii,atribuiri+comparatii);

		//sortare_insertie
		atribuiri = comparatii = 0;
		copiere(A,B,n);
		insertie(B,n,atribuiri,comparatii);
		fprintf(f3,"%d \t %d \t %d \t %d\n",n,atribuiri,comparatii,atribuiri+comparatii);
	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

void mediu ()
{
	long int A[10001],B[10001];
	long int n;
	long a1,a2,a3,c1,c2,c3;
	long atribuiri,comparatii;

	FILE *f1;
	f1 = fopen ("bubble_med.txt","w");
	FILE *f2;
	f2 = fopen ("selectie_med.txt","w");
	FILE *f3;
	f3 = fopen ("insertie_med.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		a1 = 0;
		a2 = 0;
		a3 = 0;
		c1 = 0;
		c2 = 0;
		c3 = 0;

		for (int k=1; k<=5; k++)
		{
			randomMare(A,n);
			printf("%d  ",n);
			//bubble_sort
			atribuiri=0;
			comparatii=0;
			copiere(A,B,n);
			bubble_sort(B,n,atribuiri,comparatii);
			a1+=atribuiri;
			c1+=comparatii;

			//sortare selectie
			atribuiri=0;
			comparatii=0;
			copiere(A,B,n);
			selectie(B,n,atribuiri,comparatii);
			a2+=atribuiri;
			c2+=comparatii;

			//sortare_insertie
			atribuiri=comparatii=0;
			copiere(A,B,n);
			insertie(B,n,atribuiri,comparatii);
			a3+=atribuiri;
			c3+=comparatii;
		}

		a1/=4;c1/=4;
		a2/=4;c2/=4;
		a3/=4;c3/=4;

		fprintf (f1,"%d \t %d \t %d \t %d\n",n,a1,c1,a1+c1);
		fprintf (f2,"%d \t %d \t %d \t %d\n",n,a2,c2,a2+c2);
		fprintf (f3,"%d \t %d \t %d \t %d\n",n,a3,c3,a3+c3);
	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

int main()
{
	//favorabil();
	//defavorabil();
	//mediu();
	long atribuiri, comparatii,A[10000],n;
	scanf("%d", &n);
	
	      randomMare(A, n);
			bubble_sort(A,n,atribuiri,comparatii);
           for(long i=1; i<= n; i++)
		   {
			printf("%d ",A[i]);
		   }
		   printf("\n");
		     randomMare(A, n);
			selectie(A,n,atribuiri,comparatii);
			for(long i=1; i<= n; i++)
		   {
			printf("%d ",A[i]);
		   }
			printf("\n");
			
		    randomMare(A, n);
			insertie(A,n,atribuiri,comparatii);
			for(long i=1; i<= n; i++)
		   {
			printf("%d ",A[i]);
		   }
			printf("\n");
			

	
	
	return 0;
}