/******************************************
*	***ANONIM*** ***ANONIM***						  *
*	Group: ***GROUP_NUMBER***						  *
*	Assignment 6						  *
*   Josephus Permutation				  *
*******************************************/


#include<iostream>
#include<conio.h>
#include<fstream>
#include<stdlib.h>
#include<stdio.h>

#include"Profiler.h"

using namespace std;

Profiler profiler("demo");

static int ASS,CMP;
long a[]={1,2,3,4,5,6,7,8,9};
int n, m;

struct BST
{
	int data, size;
	BST *left, *right, *parent;
};
typedef struct BST BST;
BST* root;

BST* osSelect (BST* BST, int sizeKey)
{
	int size;
	
	if(BST->left != NULL)
		size = BST->left->size + 1;
	else
		size = 1;
	CMP++;

	if(sizeKey == size)
		return BST;
	else if(sizeKey < size)
		return osSelect(BST->left, sizeKey);
	else
		return osSelect(BST->right, sizeKey - size);

}


BST* BST_min(BST *node)
{
	while(node->left != NULL)
	{
		node = node->left;
		CMP++;
		ASS++;
	}
	CMP++;
	return node;
}

BST * BST_successor(BST * node)
{
	BST *newNode;
	if(node->right != NULL)
		return BST_min(node->right);
	newNode = node->parent;
	CMP++;
	ASS++;

	while( newNode!=NULL && node == newNode->right)
	{
		ASS+=2;
		CMP+=2;
		node = newNode;
		newNode = newNode->parent;
	}
	CMP+=2;

	return newNode;
}


BST* os_delete(BST *node)
{
	BST *y;
	BST *x;
	if(node->left == NULL)
		y = node;
	else
	{
		CMP++;
		if(node->right == NULL)
			y= node;
		else
			y =	BST_successor(node);
	}

	CMP++;
	ASS++;

	if (y->left != NULL)
		x = y->left;
	else 
		x = y->right;

	CMP++;
	ASS++;

	if (x != NULL)
	{
		x->parent = y->parent;
		ASS++;
	}
	CMP++;
	
	if(y->parent == NULL)
		root = x;
	else 
	{
		CMP++;
		if (y == y->parent->left)
			y->parent->left = x;
		else 
			y->parent->right = x;
	}
	
	CMP++;
	ASS++;

	if( y != node )
	{
		node->data = y->data;
		ASS++;
	}
	CMP++;
	
	return y;
}

void BST_insert(BST* &root,int key)
{
	BST *x,*y,*newNode;
	y = NULL;
	x = root;
	newNode = (BST*)(malloc(sizeof(BST)));
	newNode->data = key;
	newNode->size = 1;
	newNode->left = NULL;
	newNode->right = NULL;
	newNode->parent = NULL;
	
	while(x != NULL)
	{
		y = x;
		y->size++;
		
		if(newNode->data<x->data)
			x = x->left;
		else
			x = x->right;
	}
	
	newNode->parent = y;
	ASS++;
	if(y==0)
		root = newNode;
	else 
		if(newNode->data < y->data)
			y->left = newNode;
	else  y->right = newNode;
	
}

void decreaseDimension(BST* node)
{
	while(node != NULL)
	{
		node->size--;
		node = node->parent;
		ASS+=2;
		CMP++;
	}
	CMP++;
}

void build_BST(BST * &root, int start, int end)
{
	if(start < end)
	{
		BST_insert(root, (start+end)/2);
		build_BST(root, start,(start+end)/2);
		build_BST(root, (start+end)/2+1, end);
	}
	else return;
}

void Josephus(int n, int m)
{
	BST *node;
	build_BST(root,1,n+1);

	int j=1;
	for( int k=n; k>=1; k--)
	{
		j = (j+m-2)%k + 1;
		node = osSelect(root,j);
	//	cout<<"Removed: "<<node->data<<endl;
		decreaseDimension(os_delete(node));
	}
	
}

void main()
{

	//Josephus(10,2); 
	
	for(int n=100; n<=10000; n+=100)
	{
		cout<<n<<endl;
		m = n / 2;
		ASS=0;
		CMP=0;
		Josephus(n,m);
		profiler.countOperation("ASS",n,ASS);
		profiler.countOperation("CMP",n,CMP);
		profiler.countOperation("CMP+ASS",n,ASS+CMP);
	}
	profiler.createGroup("Total","CMP","ASS");
	profiler.showReport();
	
	
	getch();
}