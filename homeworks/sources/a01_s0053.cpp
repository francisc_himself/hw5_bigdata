// ***ANONIM*** ***ANONIM***-***ANONIM***

#include <conio.h>
#include <stdio.h>
#include "Profiler.h"
Profiler profiler("lab2");

void bubble(int *v, int n)
{
	int i,j,x;
	for (i=0;i<n-2;i++)
	{
		for (j=0;j<n-i-1;j++)
		{
			profiler.countOperation("bubble_comp",n);
			if (v[j]>v[j+1])
			{
				profiler.countOperation("bubble_assign",n,3);
				x=v[j];
				v[j]=v[j+1];
				v[j+1]=x;
			}
		}
	}
}

void selection(int *v, int n)
{
	int x,i,j,imin;
	for (i=0;i<n-1;i++)
	{
		imin=i;
		for (j=i+1;j<n;j++)
		{
			
			if (v[j]<v[imin])
			{
				profiler.countOperation("selection_comp",n);
				imin=j;
			}
			profiler.countOperation("selection_assign",n,3);
			x=v[imin];
			v[imin]=v[i];
			v[i]=x;
		}
		
	}
}

void insertion(int *v, int n)
{

	int i,j,k,x;
	for (i=1;i<n;i++)
	{
		profiler.countOperation("insertion_assign",n);
		x=v[i];
		j=0;
		while (v[j]<x)
		{
			profiler.countOperation("insertion_comp",n);
			j=j+1;
		}
		for (k=i;k>=j+1;k--)
		{
			profiler.countOperation("insertion_assign",n);
			v[k]=v[k-1];
		}
		profiler.countOperation("insertion_assign",n);
		v[j]=x;
	}
}

void main()
{
	//int i;
	//int v[]={5,45,23,81,6};
	//bubble(v,5);
	//int v1[]={5,45,23,81,6};
	//selection(v1,5);
	//int v2[]={5,45,23,81,6};
	//insertion(v2,5);
	//for (i=0;i<5;i++)
	//{
	//	printf("%d ",v[i]);
	//}
	//for (i=0;i<5;i++)
	//{
	//	printf("%d ",v1[i]);
	//}
	//for (i=0;i<5;i++)
	//{
	//	printf("%d ",v2[i]);
	//}
	int n;
	int v[10000],v1[10000];
	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);
		memcpy(v1,v,n*sizeof(int));
		selection(v1,n);
		memcpy(v1,v,n*sizeof(int));
		insertion(v1,n);
	}
	profiler.addSeries("bubble","bubble_comp","bubble_assign");
	profiler.addSeries("selection","selection_comp","selection_assign");
	profiler.addSeries("insertion","insertion_comp","insertion_assign");
	profiler.createGroup("comp","bubble_comp","selection_comp","insertion_comp");
	profiler.createGroup("assign","bubble_assign","selection_assign","insertion_assign");
	profiler.showReport();
	
}