/*
	Din punct de vedere al implementari programul a fost chiar banal , dar partea grea a fost aceea de intelegere
	a tabeli hash. Am avut nevoie de mult timp pana am inteles exact ceea ce face aceasta tabela.
	Program interesant si surprinzator de rapid din punct de vedere al timpului de executie si foarte eficient in cautarea
	elementelor , avand nevoie de foarte putine operatii pentru a face asta.
	Cel mai important lucru , dupa rezolvarea codului , a fost acela de intelegere a tebelei Hash

	Student : ***ANONIM*** ***ANONIM*** - ***ANONIM*** 
*/


#include<stdio.h>
#include<stdlib.h>
#include<conio.h>

#define N 10007
int T[N];
int find,not;

int hash(int k, int i)
{
	int a,c1=3,c2=5;
	a=(k%N +c1*i + c2*i*i)%N;
	return a;
}

 ///functia de inserare - Carte Corman 
int Hash_Insert(int *T, int k)   
{
   int i=0,j;
   do{
       j=hash(k,i);
       if(T[j]==NULL)
        {
           T[j]=k;
		   //folosit pentru testarea partiala a alg
		   //printf("el %d a fost inserat pe pozitia %d\n",k,j);
	       return j;
        }
       else i++;

   }while(i!=N);

return -1;
}
//functia de cautare - carte Corman
int Hash_Search(int *T,int k)
{
  int i=0,j,nr=0;

   do{
	  nr++;
      j=hash(k,i);
      nr++;
       if(T[j]==k)
        {
			nr++;
			 find=nr;
			 //folosit pentru testarea partiala a alg
			 //printf("nr %d a fost gasit la pozitia %d\n",k,j);
			 return j;
        }
        else i++;
		nr++;
   }while((T[j]!=NULL)&&(i!=N));
   //folosit pentru testarea partiala a alg
   //printf("nr %d nu a fost gasit\n",k);
   not=nr;// in cazul in acre nu se gaseste numarul
return -1;
}


void initializare(int *T)
{
	for(int i=0;i<N;i++)
	{
		T[i]=NULL;
	}
}

int main()
{
		double alfa[5]={0.8,0.85,0.9,0.95,0.99};// factorul de umplere al tabelei
	
		FILE *f;
		f=fopen("hash_table.csv","w");

		int i,nrele,j,k,ii,medienot=0,mediefind=0,medienotmax=0,mediefindmax=0;
		int eleex[1500];


		//test verificare algoritm
		/*
		int t1,t2;
		nrele=alfa[0]*N;
		t1=rand();
		Hash_Insert(T,t1);
		for(i=1;i<nrele-1;i++)
		{
			Hash_Insert(T,rand());
		}
		t2=rand();
		Hash_Insert(T,t2);
		    //pentru elemente existente
			Hash_Search(T,t1);
			Hash_Search(T,t2);

			//pentru elemente neexistente (speram)
			Hash_Search(T,1);
			Hash_Search(T,2);
		*/
		fprintf(f,"alfa,Mexistente,Minexistente\n");
		for(i=0;i<5;i++)
		{
			initializare(T);
			nrele=alfa[i]*N;
			//inserarea numere pare
			for(j=0;j<nrele;j++)
			{
				k=2*rand();
				if(j<1500)
				{
					eleex[j]=k;
				}
				Hash_Insert(T,k);
			}

			//pentru elemente neexistente 
			for(ii=0;ii<1500;ii++)
			{
				not=0;
				k=2*rand()+1;
				Hash_Search(T,k);
				medienot+=not;
			}

			if(medienot>medienotmax) medienotmax=medienot;

			//pentru elemente existente , retinute in eleex
			for(ii=0;ii<1500;ii++)
			{
				find=0;
				Hash_Search(T,eleex[ii]);
				mediefind+=find;
			}

			if(mediefind>mediefindmax) mediefindmax=mediefind;

			fprintf(f,"%1.2f,%d,%d\n",alfa[i],mediefind/1500,medienot/1500);
		}

		fprintf(f," opmaxFind,%d\n",mediefindmax);
		fprintf(f," opmaxNot,%d\n",medienotmax);
		fclose(f);
}