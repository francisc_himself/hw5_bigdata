	/* Average case: Bubblesort has the most assignments ***ANONIM*** comparisons, then selection ***ANONIM*** lastly insertion. 
		Best case: Insertion sort is the only one that does assignments.Selection sort ***ANONIM*** bubble sort do an equal amount of comparisons, insertion sort does none.
		Worst case:An exponentially larger amount of assignments is done by bubblesort, while insertion ***ANONIM*** selection have equal values.Bubblesort does the most comparisons, second place being insertion
		***ANONIM*** lastly selection
	*/
#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <conio.h>
#include<fstream>
#include<time.h>
using namespace std;
int a,c,ac;
void afisare(int A[],int n)
{int i;
for(i=0;i<n;i++)
{cout<<A[i];}
}


void bubble(int A[],int n)
{int ok,aux,i;
a=c=ac=0;
do	{
	c++;
	ok=1;
	for(i=1;i<n;i++){
		c=c+2;
	if(A[i]<A[i-1])
			{
			aux=A[i];
			A[i]=A[i-1];
			A[i-1]=aux;
			a=a+3;
			ok=0;
			}
	}
	}while(ok==0); c++;
	ac=a+c;
}
void selection(int A[],int n)
{int j,aux,i;
a=c=ac=0;
for(i=0;i<n-1;i++)
	for(j=i+1;j<n;j++)
	{c=c+1;
		if(A[i]>A[j])
		{	a=a+3;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;}
	}
ac=a+c;
}
void insertion(int A[],int n)
{int j,aux,i,k;
a=c=ac=0;
for(j=1;j<n;j++)
	{aux=A[j];
	k=j-1;
	a++;
		while(k>0&&A[k]>aux)
		{	c++;
			a=a++;
			A[k+1]=A[k];
			k--;
		}
		A[k+1]=aux;
		c++;a++;
	}
ac=a+c;
}

void bestCase()
{int n=100,A[10000],b[10];
FILE *f;
f=fopen("best.csv","w");
fprintf(f,"n,AB,CB,ACB,AI,CI,ACI,AS,CS,ACS\n");
	for (int n=100;n<=10000;n=n+100)
	 {
			for (int i=0;i<n;i++)
				A[i]=i;
			bubble(A,n);
			fprintf(f,"%d,%d,%d,%d,",n,a,c,ac);
			insertion(A,n);
			fprintf(f,"%d,%d,%d,",a,c,ac);
			selection(A,n);
			fprintf(f,"%d,%d,%d\n",a,c,ac);
	}
	 
fclose(f);
}
void worstCase()
{int n=100,i,A[20000],b[10];

	FILE *h;
	h=fopen("worst.csv","w");
	fprintf(h,"n,AB,CB,ACB,AI,CI,ACI,AS,CS,ACS\n");
		for (int n=100;n<=10000;n=n+100)
		{
			for (int i=0;i<n;i++)
				A[i]=n-1-i;
			bubble(A,n);
			fprintf(h,"%d,%d,%d,%d,",n,a,c,ac);

			for (int i=0;i<n;i++)
				A[i]=n-1-i;
			insertion(A,n);
			fprintf(h,"%d,%d,%d,",n,a,c,ac);

	
			for (int i=1;i<n;i++)
				A[i-1]=i;
			A[n-1]=0;
			selection(A,n);
			fprintf(h,"%d,%d,%d\n",n,a,c,ac);
		}

fclose(h);
}

void averageCase()
{int n=100,j,i,A[20000],sumc[3],suma[3],z[10000],x[10000];
	FILE *g;
	g=fopen("avg.csv","w");
	fprintf(g,"n,AB,CB,ACB,AI,CI,ACI,AS,CS,ACS\n");
			sumc[0]=0;sumc[1]=0;sumc[2]=0;
			suma[0]=0;suma[1]=0;suma[2]=0;
			for (int n=100;n<=10000;n=n+100)
			{
			for(int j=0;j<5;j++) 
			{
			    sr***ANONIM***(time(NULL));
				for (int i=0;i<n;i++)
				{
						A[i]=r***ANONIM***()%1000;
						z[i]=A[i];
						x[i]=A[i];
				}
				bubble(A,n);
				sumc[0]=sumc[0]+c;
				suma[0]=suma[0]+a;
				insertion(z,n);
				sumc[1]=sumc[1]+c;
				suma[1]=suma[1]+a;
				selection(x,n);
				sumc[2]=sumc[2]+c;
				suma[2]=suma[2]+a;
			}
			suma[0]=suma[0]/5;sumc[0]=sumc[0]/5;
			suma[1]=suma[1]/5;sumc[1]=sumc[1]/5;
			suma[2]=suma[2]/5;sumc[2]=sumc[2]/5;
			fprintf(g,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,suma[0],sumc[0],suma[0]+sumc[0],suma[1],sumc[1],suma[1]+sumc[1],suma[2],sumc[2],suma[2]+sumc[2]);
			}
fclose(g);
	
}
int main()
{
bestCase();
worstCase();
averageCase();

return 0;
}

