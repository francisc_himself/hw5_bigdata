
/*
Student:						***ANONIM*** ***ANONIM***
Grupa:						    ***GROUP_NUMBER***
Cerinta:				

-Implementati corect si efiient un algoritm pentru permutarea lui Josephus pentru cazul in care m este constant - O(n lgn)

-Josephus(7, 3) este {3, 6, 2, 7, 5, 1, 4}.

-Folositi un arbore binar de cautare echilibrat  

-Creati un arbore binar de cautare echilibrat continand cheile 1, 2,..., n

-procedura de stergere a unui nod dintr-un arbore binar de cautare, procedura SO-select.

-Pentru a dovedii functionalitatea algoritmului, se face un apel Josephus(7,3), iar in consola se poate observa foarte usor rezultatul asteptat

-pentru a dovedii functionalitatea metodei de stergere a unui nod este efectuata prin afisarea frumoasa inainte si dupa stergere prin apelul functiei corect()

-Pentru generarea graficului cu referire la numarul de operatii necesare pentru a duce la bun sfarsit algoritmul pentru n variabil intre 
100 si 10000 cu un increment de 100, se apeleaza functioa grafic(), iar rezultatul asteptat se gaseste in grafic.xlsx, unde se poate observa ca intradevar,
complexitatea este O(nlgn)

*/

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>


int operatii;

typedef struct nod{int cheie;int dimensiune;struct nod *stanga;struct nod *dreapta;} NOD;

NOD *RADACINA;

//gasirea elementului care in secventa sortata este al i-lea
//-----------------------------------------------------------------
NOD** osSelect(NOD **x, int i)
{
	 int r;
	 operatii+=2;
	 if ((*x)->stanga!=0)
	 {
		 operatii++;
		 r=(*x)->stanga->dimensiune+1; 
	 }
  
	  else r=1;
	  operatii++;
	  if(i==r) return x;
	  else 
	  {   operatii++;
		  if(i<r) 
			  return osSelect(&(*x)->stanga,i);
		  else 
			  return osSelect(&(*x)->dreapta,i-r);
	  }

}
//crearea arborelui echilibrat
//-----------------------------------------------------------------
NOD* creareArbore(int stanga,int dreapta)
{
	NOD *nod;
	int mijloc=0;

	operatii++;
	if(stanga <= dreapta)
	{
		
		operatii++;
		mijloc = (stanga + dreapta)/2;

		operatii+=5;
		nod = (NOD*)malloc(sizeof(NOD));;
		nod->cheie = mijloc;
		nod->dimensiune = 1;
		nod->stanga = creareArbore(stanga, mijloc-1);
		nod->dreapta = creareArbore(mijloc+1,dreapta);


		operatii++;
		if(nod->stanga != 0)
		{
			operatii++;
			nod->dimensiune += nod->stanga->dimensiune;
		}

		operatii++;
		if(nod->dreapta != 0)
		{
			operatii++;
			nod->dimensiune += nod->dreapta->dimensiune;
		}

		return nod;
	}
	
	return 0;
}

//actualizarea dimensiunii
//-----------------------------------------------------------------
void actualizareDimensiune(NOD *x,int cheie)
{
	operatii+=2;
	if(x != 0 && x->cheie != cheie)
	{
		operatii++;
		x ->dimensiune --;
		operatii++;
		if(x->cheie > cheie)
		{
			actualizareDimensiune(x->stanga,cheie);
		}
		else
		{
			actualizareDimensiune(x->dreapta,cheie);
		}
	}
	else if(x != 0 && x->cheie == cheie)
	{
		x->dimensiune --;
	}
}
//se alege minimul din dreapta sau maximul din stanga, eu am ales maximul din stanga
//z este nodul care se sterge efectiv
//-----------------------------------------------------------------
void stergeNod(NOD** x)
{
	NOD **y,*z;

	y = x;
	operatii++;
	if((*x)->dreapta == 0)
	{
		operatii++;
		z = (*x)->dreapta;
		*x = (*x)->stanga;
	}

	else 
	{
		if((*x)->stanga == 0)
			{
				operatii++;
				z = (*x)->stanga;
				*x = (*x)->dreapta;
				
			}
		else 
		{
			operatii++;
			y = &(*y)->stanga;
			operatii++;
			while((*y)->dreapta!= 0)
			{
				operatii++;
				operatii++;
				(*y)->dimensiune--;
				operatii++;
				y = &((*y)-> dreapta);
			}
			operatii++;
			(*x)->cheie = (*y)->cheie;
			actualizareDimensiune((*y),(*y)->cheie);
			stergeNod(y);
		}

	}
	
}



//-----------------------------------------------------------------
void Josephus(int n,int m)
{
	int i=1,k;
	NOD **x;
	RADACINA = creareArbore(1,n);
	printf("permutarea Josephus\n");
	for (k = n; k >= 1; k--)
	{
		i= (i + m - 2) % k +1;
		x = osSelect(&RADACINA,i);
		if((*x)!=0)
			printf(" %d  ",(*x)->cheie);
		actualizareDimensiune(RADACINA,(*x)->cheie);
		stergeNod(x);
	}
printf("\n\n");
}

//-----------------------------------------------------------------
void afiseazaFrumos(NOD *x,int nivel)
{

	int i;
	if(x!= 0)
	{
		afiseazaFrumos(x->dreapta,nivel+1);
		for(i=0;i<nivel;i++) printf("   ");
		 printf("%d,%d\n",x->cheie,x->dimensiune);
				
		afiseazaFrumos(x->stanga,nivel+1);

	}
}

//-----------------------------------------------------------------
void grafic()
{
	FILE* f;
	f=fopen("graficJose.csv","w");
	fprintf(f,"n, operatii\n");//creeare cap de tabel
	for(int n = 100;n <= 10000;n+=100)
	{
		operatii=0;
		Josephus(n,n/2);
		fprintf(f,"%d,%d\n",n,operatii);
	}

	fclose(f);
}



//-----------------------------------------------------------------
void corect()
{
	printf("arborele\n");
	RADACINA = creareArbore(1,15);
	afiseazaFrumos(RADACINA,0);
	printf("\ndupa stergerea radacinii \n");
	stergeNod(&RADACINA);
	actualizareDimensiune(RADACINA,RADACINA->cheie);
	afiseazaFrumos(RADACINA,0);

}




//-----------------------------------------------------------------
void main()
{
	Josephus(7,3);
	corect();
	
	//grafic();
	getch();
}