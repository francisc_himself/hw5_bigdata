#include<conio.h>
#include<stdio.h>
#include<stdlib.h>



#define white 0
#define grey 1
#define black 2

#define MAXV  210

//***ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***
//complexitatea creste liniar la cresterea numarului de legaturi
//complexitatea creste liniar la cresterea numarului de noduri

typedef struct node
{
    int key;
    struct node *next;
} NODE;

struct STACK{
    int poz;
    int tab[10];
};

NODE *Adj[MAXV];

int pi[MAXV];
int d[MAXV];
int f[MAXV];
int color[MAXV];
int time;
struct STACK mystack;
double op,opsv[11],opse[50];

void scrie_rez(FILE *f,double *a,int length)
{
    int i;
	fprintf(f,"\n");
	for(i=1;i<=length;i++)
		fprintf(f,"%.0f,",a[i]);
}

void initstack()
{
    mystack.poz=-1;
    int i;
    for(i=0;i<10;i++)
    {
        mystack.tab[i]=0;
    }

}

void push_stack(int u)
{
    mystack.poz++;
    mystack.tab[mystack.poz]=u;
}

int pop_stack()
{
    mystack.poz--;
    return mystack.tab[mystack.poz+1];
}

int stackEmpty()
{
    if(mystack.poz==0)
        return 1;
    else
        return 0;
}

void printStack()
{
    while(!stackEmpty())
    {
        printf("%d ",pop_stack());
    }
}


void DFS_VISIT(int u,int topsort)
{
    op += 4;
    time++;
    d[u]=time;
    color[u]=grey;
    NODE *v = Adj[u];

    op+=1;
    while(v!=0)
    {
        op+=1;
        if(color[v->key]==white)
        {
            op+=1;
            pi[v->key]=u;
            DFS_VISIT(v->key,topsort);
        }
        v = v->next;
    }

    op += 3;
    color[u]=black;
    if(topsort)
        push_stack(u);
    time++;
    f[u]=time;
}

void DFS(int n, int topsort)
{
    int u;

    for(u=1; u<n+1; u++)
    {
        op+=1;
        color[u]=white;
    }
    time=0;
    for(u=1; u<n+1; u++)
    {
        op+=1;
        if(color[u]==white)
        {
                DFS_VISIT(u,topsort);
        }
    }
}

void generateGraph(int n, int edges)
{
    int i;
    int found;
    int node1, node2;
    NODE *urm,*aux1,*aux2;

    for(i = 0; i < MAXV; i++)
        Adj[i]=0;

    for(i = 0; i < edges; i++)
    {
        found=1;
        while(found == 1)
        {
            found=0;
            node1=rand()%(n+1);
            node2=rand()%(n+1);

            if((node2==0)||(node1==0))
            {
                found=1;
                continue;
            }
            else
            {
                if(Adj[node1]==0)
                {
                    aux1=(NODE*)malloc(sizeof(NODE));
                    aux1->key=node2;
                    aux1->next=0;
                    Adj[node1]=aux1;
                }
                else
                {
                    urm=Adj[node1];
                    while(urm!=0)
                    {
                        if(urm->key==node2)
                        {
                            found=1;
                            break;
                        }
                        aux2=urm;
                        urm=urm->next;
                    }
                    if(found==0)
                    {
                        aux1=(NODE*)malloc(sizeof(NODE));
                        aux1->key=node2;
                        aux1->next=0;
                        aux2->next=aux1;
                    }
                }
            }
        }
    }
}

void init(int n)
{
    op = 0;
    int i;
    for( i= 0; i < n; i++);
    {
        f[i]=0;
        d[i]=0;
        Adj[i]=0;
        pi[i]=0;
        color[i]=0;
    }
}

void createVertice(int node1, int node2)
{
    NODE * aux1=(NODE*)malloc(sizeof(NODE));
    NODE *aux2;
    aux1->key=node2;
    aux1->next=0;
    if(Adj[node1]==0)
    {
        Adj[node1]=aux1;
    }else
    {
        aux2=Adj[node1];
        while(aux2->next)
        {
            aux2=aux2->next;
        }
        aux2->next=aux1;
    }
}


int main()
{
    FILE *fis = fopen("DFS.csv", "w");

    int i;
    init(10);
    createVertice(4,0);
    createVertice(4,3);
    createVertice(4,6);
    createVertice(4,5);
    createVertice(4,7);

    createVertice(7,5);
    createVertice(7,1);

    createVertice(1,3);
    createVertice(1,6);
    createVertice(1,2);

    createVertice(2,5);
    DFS(7,1);
    //afisam sortarea topologica
    printStack();

    for (i = 10; i <= 50; i += 1)
    {
        init(100);
        generateGraph(100, i*100);
        DFS(100,0);
        opse[i-9]=op;
    }

    for (i = 11; i <= 20; i += 1)
    {
        init(i*10+1);
        generateGraph(i*10, 9000);
        DFS(i*10,0);
        opsv[i-10]=op;
    }

    scrie_rez(fis,opse,40);
    scrie_rez(fis,opsv,10);
    fclose(fis);
    return 0;
}
