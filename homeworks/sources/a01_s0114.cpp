#include <stdio.h>
#include <conio.h>
#include "Profiler.h"

#define MAX_SIZE 8
#define LOW 0
#define HIGH 2531
Profiler profiler("demo");

int toFile(char * fileName,int * vector, int n){
	FILE * f = fopen(fileName,"w");//open file for writing
	
	int i;
	if (f == NULL)
	{
		return 0;
	}
	for (i = 0;i < n;i++){
		fprintf(f,"%d ",vector[i]);//write vector elements in the file
	}
	fclose(f);//close file
	return 1;

}
int fromFile(char * fileName){
	FILE * f = fopen(fileName,"r");//open file for reading
	if (f == NULL)
	{
		return 0;
	}
	int i = 0,number;
	printf("\nNUMBERS IN FILE: \n");
	while(fscanf(f,"%d",&number) != EOF){
		printf("Element[%d] = %d\n",i,number);//print numers in file on the screen
		i++;
	}
	return 1;
}


void printV(int * vector,int n){
 int i;
 for (i = 0;i<n;i++){
	printf("Element[%d] = %d\n",i,*(vector+i));
 }
}

int main(){

	int vector[MAX_SIZE];
	int sortedV[MAX_SIZE];
	int n,i,number;
	printf("number of elements = ");//read the number of elements
	scanf("%d",&n);//read n
	
	FillRandomArray(vector,n,LOW,HIGH);//fill array with random numbers
	printV(vector,n);//print the array on the screen
	toFile("file.txt",vector,n);//print numbers to file
	fromFile("file.txt");

	FillRandomArray(sortedV,n,LOW,HIGH,false,1);//fill array with sorted random numbers
	printf("\nRandom numbers:\n");
	printV(sortedV,n);//print the array on the screen
	toFile("sortFile.txt",sortedV,n);//print numbers to file
	fromFile("sortFile.txt");//print numbers to screen from file


	getch();
	return 0;
}