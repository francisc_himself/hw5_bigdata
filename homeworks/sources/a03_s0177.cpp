// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
//Both alg: O(nlgn)
//Quicksort, worst case: O(n^2)
//Both alg. are linear
#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include "Profiler.h"
#define nr 10000
using namespace std;
Profiler profiler("Heapsort - Quicksort");
int size;
int n;
void heapify(int a[],int i,int n)
{
	int left,right,largest,aux;
	left=2*i;
	right=2*i+1;
	
	profiler.countOperation("Heapsort",n);
	if(left<=size && a[left]>a[i])
	{
		largest=left;
	}
	else 
	{
		largest=i;
	}

	profiler.countOperation("Heapsort",n);
	if(right<=size && a[right]>a[largest])
	{
		largest=right;
	}


	if(largest!=i)
	{
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		profiler.countOperation("Heapsort",n,3);
		heapify(a,largest,n);
	}
	

}


void BuildMaxHeap(int a[],int n){
	size=n;
	for(int i=n/2;i>=1;i--){
		heapify(a,i,n);
	}
}

void HeapSort(int a[]){
	int i;
	int aux;
	BuildMaxHeap(a,n);
	for(i=n;i>=2;i--){
		aux=a[i];
		a[i]=a[1];
		a[1]=aux;
		profiler.countOperation("Heapsort",n,3);
		size--;
		heapify(a,1,n);
	}
}


void quickSort(int a[], int l, int r) {
    int i = l, j = r;
    int aux;
    int pivot = a[(l + r) / 2];
	profiler.countOperation("Quicksort",n);
    while (i <= j) 
	{
		profiler.countOperation("Quicksort",n);
        while (a[i] < pivot)
		{ 
			i++;
			profiler.countOperation("Quicksort",n);
		}
		profiler.countOperation("Quicksort",n);
        while (a[j] > pivot)
		{
			j--;
			profiler.countOperation("Quicksort",n);
		}
        if (i <= j) 
		{
			profiler.countOperation("Quicksort",n,3);
            aux = a[i];
            a[i] = a[j];
            a[j] = aux;
            i++;
            j--;
        }
	}
	if (l < j)
		quickSort(a, l, j);
	if (i < r)
		quickSort(a, i, r);
}

void quickbest(int a[], int l, int r) {
    int i = l, j = r;
    int aux;
    int pivot = a[(l + r) / 2];
	profiler.countOperation("QuickBest",n);
    while (i <= j) 
	{
		profiler.countOperation("QuickBest",n);
        while (a[i] < pivot)
		{ 
			i++;
			profiler.countOperation("QuickBest",n);
		}
		profiler.countOperation("QuickBest",n);
        while (a[j] > pivot)
		{
			j--;
			profiler.countOperation("QuickBest",n);
		}
        if (i <= j) 
		{
			profiler.countOperation("QuickBest",n,3);
            aux = a[i];
            a[i] = a[j];
            a[j] = aux;
            i++;
            j--;
        }
	}
	if (l < j)
		quickbest(a, l, j);
	if (i < r)
		quickbest(a, i, r);
}

void quickworst(int a[], int l, int r) {
    int i = l, j = r;
    int aux;
    int pivot = a[r];
	profiler.countOperation("QuickWorst",n);
    while (i <= j) 
	{
		profiler.countOperation("QuickWorst",n);
        while (a[i] < pivot)
		{ 
			i++;
			profiler.countOperation("QuickWorst",n);
		}
		profiler.countOperation("QuickWorst",n);
        while (a[j] > pivot)
		{
			j--;
			profiler.countOperation("QuickWorst",n);
		}
        if (i <= j) 
		{
			profiler.countOperation("QuickWorst",n,3);
            aux = a[i];
            a[i] = a[j];
            a[j] = aux;
            i++;
            j--;
        }
	}
	if (l < j)
		quickworst(a, l, j);
	if (i < r)
		quickworst(a, i, r);
}

void print(int a[], int n)
{	
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
}

void main()
{
	int a[nr],b[nr],c[nr];
	int i,j;
	char x;

 	/*for(i=100;i<10000;i+=100)
	{
		FillRandomArray(a,i,0,10000);
		for(int k=1;k<=5;k++)
		{	
			n=i;
			for(j=1;j<=n;j++)
			{
				b[j]=a[j];
				c[j]=a[j];
			}
			quickSort(b,1,n);
			HeapSort(c);
		}
	}
	profiler.createGroup("AverageOperation","Heapsort","Quicksort");
	profiler.showReport();*/

	/*for(i=100;i<10000;i+=100)
	{
		FillRandomArray(a,i,1,10000,false,1);
		n=i;
		for(j=1;j<=i;j++)
			{
				b[j]=a[j];
				c[j]=a[j];
			}
		quickbest(b,1,n);
		quickworst(c,1,n);
	}
	
	profiler.createGroup("Best Case","QuickBest");
	profiler.createGroup("Worst Case","QuickWorst");
	profiler.showReport();*/
	
	
	/*printf("n=");
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		printf("a[%d]=",i);
		scanf("%d",&a[i]);
	}	
	for(j=1;j<=n;j++)
	{
		b[j]=a[j];
		c[j]=a[j];
	}
	printf("\nUnsorted: \n\n");
	print(a,n);
	quickbest(b,1,n);
	quickworst(c,1,n);
	cout<<"\nSorted with best: \n\n";
	print(b,n);
	cout<<"\nSorted with worst: \n\n";
	print(c,n);
	char y;
	scanf("%c",&y);
	scanf("%c",&y);*/
	
	printf("n=");
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		printf("a[%d]=",i);
		scanf("%d",&a[i]);
	}	
	for(j=1;j<=n;j++)
	{
		b[j]=a[j];
		c[j]=a[j];
	}
	printf("\nUnsorted:");
	print(a,n);
	quickSort(b,1,n);
	HeapSort(c);
	cout<<"\nQuicksort:";
	print(b,n);
	cout<<"\nHeapsort:";
	print(c,n);
	scanf("%x",&x);
}