#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//Declaram structura unui nod din lista
typedef struct ListItem
{
    long valoare;
    struct ListItem *next;
}ListItem;

//Variabile Globale:
ListItem *v[10001];   //Vectorul de liste
ListItem *l_priml,*l_ultimul,*l_current;   //Pointeri Globali pentru crearea listei finale ordonate
long nr_operatii;
//Declareare de functii
/**
Functie de adaugare in Lista Finala Sortata
*/
void addToFinalList(int add_val)
{
    if (l_priml==NULL)
    {
        l_priml=(ListItem*)malloc(sizeof(ListItem*));
        l_priml->valoare=add_val;
        l_priml->next=NULL;
        l_ultimul=l_priml;
    }
        else
    {
        l_current=(ListItem*)malloc(sizeof(ListItem*));
        l_current->valoare=add_val;
        l_current->next=NULL;
        l_ultimul->next=l_current;
        l_ultimul=l_current;
    }
}
/**
Functie pentru afisarea Listei Finale Sortate
*/
void printFinalList()
{
    ListItem *current;
    current=l_priml;
    while(current!=NULL)
    {
        printf("%ld ",current->valoare);
        current=current->next;
    }
}
/**
Returneaza elementul stang
*/
int elementul_stang(int i)
{
	return 2*i;
}
/**
Returneaza elementul drept
*/
int elementul_drept(int i)
{
	return (2*i)+1;
}
/**
Verifica daca Heap-ul e gol
Returneaza 1 daca e gol, else 0
*/
int heapIsEmpty(ListItem *a[],int n)
{
    int i;
    int _empty=1;
    for (i=1;i<=n;i++)
    {
        if (a[i]!=NULL)
        {
            _empty=0;
        }
    }
    return _empty;
}
/**
Reconstructie Heap sau Heapify
A[] ->Vectorul asupra caruia aplicam Heapify
i -> Elementul de la care incepem
n -> Lungimea vectorului asupra caruia aplicam Heapify
 ! Conditia de heap : Parinte mai mic decat fii !
*/
void Reconstituire_Heap(ListItem *A[],int i,int n)
{
	int stanga=elementul_stang(i);
	int dreapta=elementul_drept(i);
	int poz_max;
	ListItem *aux;
	nr_operatii++;
	if(stanga<=n && A[stanga]->valoare < A[i]->valoare)
	{
		poz_max=stanga;
	}
	else
	{
		poz_max=i;
	}
	nr_operatii++;
	if(dreapta<=n && A[dreapta]->valoare < A[poz_max]->valoare)
	{
		poz_max=dreapta;
	}
	if(poz_max !=i)
	{
		nr_operatii+=3;
		aux=A[i];
		A[i]=A[poz_max];
		A[poz_max]=aux;
		Reconstituire_Heap(A,poz_max,n);
	}
}
/**
Facem vectorul de liste A un heap cu metoda Bottom Up
A -> vectorul de liste
n -> Lungimea vectorului
*/
void Buttom_Up(ListItem *A[],int n)
{
	int i;
	for(i=n/2;i>=1;i--) Reconstituire_Heap(A,i,n);
}
/**
Creaza Lista Finala Sortata
*/
void creareListaFinalaSortata(ListItem *a[],int n)
{
    int k;
    int element_scos;
    ListItem *aux;
    l_priml=NULL;
    l_current=NULL;
    l_ultimul=NULL;
    Buttom_Up(a,n);
    k=n;
    while (heapIsEmpty(a,k)==0) //Pana cand Heapul nu e gol
    {
        element_scos=a[1]->valoare;//scoatem primul element din heap
        addToFinalList(element_scos); //adaugam numarul la lista finala
        if (a[1]->next!=NULL) //Daca din lista din care am scos elementul mai are elemente
                              //Setam pointerul catre urmatorul element, sa fie in capul heapului
        {
            nr_operatii++;
            a[1]=a[1]->next;
        }
            else              //Daca nu mai avem elemente atunci punem in capul heapului ultimul element
        {
            nr_operatii+=3;
            aux=a[1];
            a[1]=a[k];
            a[k]=aux;
            k--;
        }
        Reconstituire_Heap(a,1,k);
    }

}
/**
 k=Numarul de Liste
 n=Numarul Total de Elemente
*/
void creareVectorDeListe(int k,int n)
{
    ListItem *primul,*current,*ultimul;
    int i,j;
    primul=NULL;
    current=NULL;
    ultimul=NULL;
    srand(time(NULL));
    for (i=0;i<n/k+n%k;i++) //Construim prima lista. Ea va fi mai lunga decat restul in cazul in care
                            //Numarul de elemente n nu e divizibil cu numarul de liste k
    {
        if (primul==NULL)
        {
            primul=(ListItem*)malloc(sizeof(ListItem*));
            primul->valoare=rand() % 100;
            primul->next=NULL;
            ultimul=primul;
        }
            else
        {
            current=(ListItem*)malloc(sizeof(ListItem*));
            current->valoare=ultimul->valoare+i;
            current->next=NULL;
            ultimul->next=current;
            ultimul=current;
        }
    }
    v[1]=primul;    //Punem prima lista in vectorul de liste
    for (j=2;j<=k;j++) // Continuam crearea vectorului de liste
    {
        primul=NULL;
        current=NULL;
        ultimul=NULL;
        for (i=0;i<n/k;i++) //Cream lista care urmeaza sa fie inserat in vectorul de liste. Va avea lungimea n/k
        {
            if (primul==NULL)
            {

                primul=(ListItem*)malloc(sizeof(ListItem*));
                primul->valoare=rand() % 100;
                primul->next=NULL;
                ultimul=primul;
            }
                else
            {
                current=(ListItem*)malloc(sizeof(ListItem*));
                current->valoare=ultimul->valoare+i;
                current->next=NULL;
                ultimul->next=current;
                ultimul=current;
            }
        }
        v[j]=primul;
    }
}
/**
Functie pentru afisarea Vectorului de liste a
a -> Vectorul de liste
n-> lungimea vectorului de liste
*/
void afisareVectordeListe(ListItem *a[],int n)//Folosit la testing
{
    int j;
    ListItem *current;
    current=NULL;
    for (j=1;j<=n;j++)
    {
        current=a[j];
        while(current!=NULL)
        {
            printf("%ld ",current->valoare);
            current=current->next;
        }
        printf("\n");
    }

}

void test()
{
    int n,k;
    n=20;
    k=4;
    creareVectorDeListe(k,n);
    afisareVectordeListe(v,k);
    printf("\n");
    creareListaFinalaSortata(v,k);
    printFinalList();
    printf("\n");
}
void run_avarage_1()
{
    int n;
    FILE *f;
    f=fopen("Rezultate_1.csv","w");
    fprintf(f,"n,k1=5,k2=10,k3=100\n");
    int k1=5,k2=10,k3=100;
    for (n=100;n<=10000;n=n+100)
    {
        fprintf(f,"%d,",n);

        nr_operatii=0;
        creareVectorDeListe(k1,n);
        creareListaFinalaSortata(v,k1);
        fprintf(f,"%ld,",nr_operatii);

        nr_operatii=0;
        creareVectorDeListe(k2,n);
        creareListaFinalaSortata(v,k2);
        fprintf(f,"%ld,",nr_operatii);

        nr_operatii=0;
        creareVectorDeListe(k3,n);
        creareListaFinalaSortata(v,k3);
        fprintf(f,"%ld\n",nr_operatii);
    }
    fclose(f);
    nr_operatii=0;
}
void run_avarage_2()
{
    int n=10000;
    int k;
    FILE *f;
    f=fopen("Rezultate_2.csv","w");
    fprintf(f,"k,nr_operatii\n");
    for (k=10;k<=500;k=k+10)
    {
        nr_operatii=0;
        creareVectorDeListe(k,n);
        creareListaFinalaSortata(v,k);
        fprintf(f,"%d,%ld\n",k,nr_operatii);
    }
    fclose(f);
    nr_operatii=0;
}
int main()
{
  test();
 //   run_avarage_1();
  //  run_avarage_2();
    return 0;
}
