#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

int M=0;

typedef struct node{
int val;
int rank;
node *parent;
}node;

typedef struct edge{
	int a1;
	int b1;
}edge;

node* make_set(int n){
	
	node *ret = (node*)malloc(sizeof(node));
	ret->val = n;
	ret->rank=0;
	ret ->parent = ret;
	M++;
	return ret;
}

void union_set(node *x, node *y){

	if(x->rank > y-> rank){
		y->parent = x;
	}
	if(x->rank < y->rank){
		x->parent = y;
	}
	if(x->rank == y->rank){
		x->parent = y;
		y->rank = y->rank + 1;
	}
	M++;
}


node* find_set(node *z){

	if(z != z->parent){
		z->parent = find_set(z->parent);
	}
	M++;
	return z->parent;
}



int  main(){


	int N=10000;
	node **a = new node*[N];
	FILE *f = fopen("union.csv","w");
	for(int nr_edge=10000;nr_edge<60000; nr_edge+=500){
		M=0;
		printf("%d\n",nr_edge);
		edge *EE = new edge[nr_edge];
		//genereaza varfi
		for(int i=0;i<N;i++){
			a[i] = make_set(i);
		}
		//genereaza muchii
		for(int i=0;i<nr_edge;i++){
			EE[i].a1 = rand()%10000;
			EE[i].b1 = rand()%10000;
		}
		for(int i=0; i<nr_edge; i++){
			int aux= EE[i].a1;
			int bux= EE[i].b1;
			//printf("%d %d", aux, bux);
			node *x = find_set(a[aux]);
			node *y = find_set(a[bux]);
			if(x!=y)union_set(x,y);
		}
		fprintf(f,"%d,%d\n",nr_edge,M);
	}
	fclose(f);
	//for(int i=0; i<N; i++){
	//	printf("%d ",find_set(a[i])->val);
	//}
    getch();
	
return 0;
}
