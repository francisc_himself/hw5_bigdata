/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

codul de fata are rolul de a compara metode de constructie a heap-urilor. Voi compara metodele top-down si bottom-up

O. Rezultate asteptate

Complexitati: 
	-constructia buttom-up  : O(n)
	-constructia top-down	: O(n*log(n))

I. Compararea celor 2 metode in cazul mediu statistic

La o testare a metodelor de consturctie a heap-urilor pentru dimensiunea 50000 am constatat urmatoarele :
	-algoritmul de consturctie buttom-up efectueaza in medie 407023 operatii
	-algoritmil de constructie top-down efectueaza in medie 508248 operaetii

Concluzii:
	-algoritmul buttom-up este mai eficient din punctul de vedere al numarului mediu de operatii in cazul mediu statistic

II. Generarea graficelor

	Am realizat graficele de comparatie utilizand profiler-ul. 
	Graficul pentru cazul mediu se gaseste in fisierul: grafic.html

III. Interpretarea graficelor si observatii personale

Desi aparent BU ar avea tot o complexitate O(n*log(n)), deoarece intr-un arbore binar avem urmatoarea distributie a nodurilor 

			 1 nod				- 1 nod
		1nod      1nod			- 2 noduri
	1nod  1nod	1nod  1nod		- 4 noduri
	.......................

se poate demonstra ca BU are o complexitate O(n). Acest fapt este confirmat de grafice. 
Constructia TD are compexitatea O(n*log(n));


IV. Compararea metodelor in cazul defavorabil
Graficul pentru cazul nefavorabil se gaseste in fisierul : caz_nefavorabil.html
Se observa ca in cazul defavorabil diferenta dintre cele doua metode de constructie este foarte mare. In cazul metodei TD se facu un 
numar foarte mare de interschimbari comparativ cu metoda metoda BU.

*/

#include<stdio.h>
#include"Profiler.h"

Profiler profiler("Compararea metodelor de constructie a heapurilor");
int dimensiune_curenta; // variabila folosita pentru numararea operatiilor

unsigned long long nrOpBU, nrOpTD,mediaBU,mediaTD; // variabile pentru calcularea numarului mediu de operatii in caz mediu

// in cele ce urmeaza presupun ca dimensiunea unui heap H este stocata in H[0];

//functii necesare:

/*
functie de initializare a heapului

@param: H = pointer la sirul in care este stocat heap-ul
*/

void _hInit (int H[]){
	H[0]=0;
}


/*
parintele nodului i
@param: i = elementul al carui parinte il cautam
*/
int parinte(int i){
	return i/2;
}


/*
dscendentul stang al lui i
@param i
*/
int stanga(int i){
	return 2*i;
}

/*
decendentul drept al lui i
@param: i
*/
int dreapta(int i){
	return 2*i+1;
}

/*
Returneaza indexul elementului minim dintre H[i],H[a] si H[b]
la apelare functiei a si b vor fi stanga lui i respectiv dreapta lui i, deci tratez aici si cazurile de depasiri de dimensiune
*/




int index_min(int H[], int i, int a, int b)
{
	nrOpBU++;
	profiler.countOperation("Comparatii_BU",dimensiune_curenta,1);	
	// numara comparatia de mai jos
	if(a>H[0]){
		return i;
	}

	int min;

	profiler.countOperation("Comparatii_BU",dimensiune_curenta,1);	
	profiler.countOperation("Atribuiri_BU",dimensiune_curenta,1);
	nrOpBU+=2;
	//numara comparatia si atribuirea
	min = H[i]<H[a]? i: a;

	nrOpBU+=1;
	profiler.countOperation("Comparatii_BU",dimensiune_curenta,1);
	//numara comparatia de mai jos
	
	if(b>H[0]){
		return min;
	}

	nrOpBU+=2;
	profiler.countOperation("Comparatii_BU",dimensiune_curenta,1);
	profiler.countOperation("Atribuiri_BU",dimensiune_curenta,1);
	// numara comparatia si atribuirea

	min = H[min]<H[b]? min:b;
	return min;
}


/*
reaconsructia unui heap
*/

void hReconstituie (int H[],int i){
	int aux;
	/*
	 // cod de debug
	printf("hReconstituie....  %d\n",i);
				
	for(int j=0;j<=H[0];j++)
		printf("%d ",H[j]);
	printf("\n");
	*/

	profiler.countOperation("Atribuiri_BU",dimensiune_curenta,1);
	// numara atribuire de mai jos
	int index = index_min(H,i,stanga(i),dreapta(i));
	
	profiler.countOperation("Comparatii_BU",dimensiune_curenta,1);
	nrOpBU++;
	//numara comparatia de mai jos
	
	if(index !=i){
		nrOpBU+=4;							 // 4 atribuiri
		profiler.countOperation("Atribuiri_BU",dimensiune_curenta,4);
		//numara atribuirile 
		aux=H[i];
		H[i]=H[index];
		H[index]=aux;
		hReconstituie(H,index);
	}
}

/*
constructie bottom up
*/

void hBUBuild(int H[]){
	int i; //iterator
	for(i=H[0]/2;i>0;i--)
		hReconstituie(H,i);
}


/*
punerea unui element in heap
*/

void hPush(int H[],int elem){
	int i;
	profiler.countOperation("Atribuiri_TD",dimensiune_curenta,3);
	nrOpTD+=3;						//3 atribuiri 
	//numara atribuirile de mai jos
	H[0]++;							// H[0] = dim(h)
	H[H[0]] = elem;
	i = H[0];						

	profiler.countOperation("Comparatii_TD",dimensiune_curenta,2);
	nrOpTD+=2;						//2 comaratii 
	//numara comparatiile de la intrarea din while
	
	while(i>1 && H[i]< H[parinte(i)]){
		//printf("\ninterschimb %d %d\n",H[i],H[parinte(i)]);
		nrOpTD+=6;					// 4 atribuiri si 2 comparatii
		profiler.countOperation("Comparatii_TD",dimensiune_curenta,2);
		profiler.countOperation("Atribuiri_TD",dimensiune_curenta,4);
		//numara operatiile din while
		H[i]=			H[i]^H[parinte(i)];
		H[parinte(i)]=	H[i]^H[parinte(i)];
		H[i]=			H[i]^H[parinte(i)];
		i = parinte(i);
	}

	/*
	 //cod de debug
	printf("\nheapul a devenit\n");
	for(int j=0;j<=H[0];j++)
		printf("%d ",H[j]);
	printf("\n");
	*/

}


/*
scoatere din heap
*/
int hPop(int H[]){
	int elem;
	if (H[0]<=0)
		return 0x80000001;
	
	elem=H[1];
	H[1]=H[H[0]];
	H[0]--;
	
	hReconstituie(H,1);
	return elem;
}

/*
consturctie  top - down
*/

void hTDBuild(int Sir[], int H[]){
	for(int i = 1;i<=Sir[0];i++){
		//printf("\npunem in heap %d\n",Sir[i]);
		hPush(H,Sir[i]);
	}
}

/*
cod pentru afisare formatata
*/
void afiseaza_frumos(int H[],int i)
{
	if(i>H[0]) 
		return;
	afiseaza_frumos(H,dreapta(i));
	int tab=i/2;
	while (tab>0){
		printf("   ");
		tab=tab>>1;
	}
	printf(" --");
	printf("%d\n",H[i]);
	afiseaza_frumos(H,stanga(i));
}

void verifica_corectitudine()
{
	//cod pentru testarea corectitudinii
	int A[10]={9,8,3,2,41,5,23,4,85,1};
	int B[10]={9,8,3,2,41,5,23,4,85,1};
	int C[10];

	hBUBuild(A);
	
	printf("Dupa reconstructie de tip B-U\n");
	afiseaza_frumos(A,1);
	
	printf("\n=========================================================\n");
	
	printf("verificare: \n");
	while(A[0]>0)
		printf(" %d ",hPop(A));
	printf("\n=========================================================\n");


	_hInit (C);

	hTDBuild(B,C);
	printf("Dupa reconstructie de tip T-D\n");
	
	afiseaza_frumos(C,1);

	printf("\n=========================================================\n");
	
	printf("verificare: \n");
	while(C[0]>0)
		printf(" %d ",hPop(C));
	
}


void genereaza_caz_mediu(){
	
	//Cod pentru calcualrea mediilor in cazul mediu statistic

	int sir_random_medii[50001];
	int heap_pt_BUm[50001];
	int heap_pt_TDm[50001];

	for (int i= 0; i<4;i++){
		nrOpBU = nrOpTD = 0;
		FillRandomArray(sir_random_medii,50001);
		sir_random_medii[0]=50000;
		for(int j = 0;j<50001;j++)
			heap_pt_BUm[j] = sir_random_medii[j];
		
		hBUBuild(heap_pt_BUm);
		_hInit(heap_pt_TDm);
		hTDBuild(sir_random_medii,heap_pt_TDm);

		mediaBU+=nrOpBU;
		mediaTD+=nrOpTD;
	}

	printf ("Pentru 50000 de elemente metoda BU efectueaza %llu operatii iar metoda TD %llu operatii",mediaBU/5,mediaTD/5);

}

void genereaza_grafice(){
 // cod pentru generarea graficelor
	int sir_random[50001];
	int heap_pt_BU[50001];
	int heap_pt_TD[50001];

	for(dimensiune_curenta = 100; dimensiune_curenta<=50000; dimensiune_curenta+=100){
		printf("acum calculam pentru %d...\n",dimensiune_curenta);

		FillRandomArray(sir_random, dimensiune_curenta+1);

		sir_random[0]=dimensiune_curenta;
		
		for(int i = 0;i<dimensiune_curenta;i++)
			heap_pt_BU[i]=sir_random[i];
		
		hBUBuild(heap_pt_BU);
		
		_hInit(heap_pt_TD);
		hTDBuild(sir_random,heap_pt_TD);
	}


	profiler.addSeries("Operatii_BU","Comparatii_BU","Atribuiri_BU");
	profiler.addSeries("Operatii_TD","Comparatii_TD","Atribuiri_TD");

	profiler.createGroup("Atribuiri","Atribuiri_BU","Atribuiri_TD");
	profiler.createGroup("Comparatii","Comparatii_BU","Comparatii_TD");
	profiler.createGroup("Operatii","Operatii_BU","Operatii_TD");

	profiler.showReport();
	
}

void genereaza_caz_defavorabil(){
	// cod pentru generarea graficelor
	int sir_random[10001];
	int heap_pt_BU[10001];
	int heap_pt_TD[10001];

	for(dimensiune_curenta = 100; dimensiune_curenta<=10000; dimensiune_curenta+=100){
		printf("acum calculam pentru %d...\n",dimensiune_curenta);

		FillRandomArray(sir_random, dimensiune_curenta,10,50000,false,2);

		sir_random[0]=dimensiune_curenta-1;
		
		for(int i = 0;i<dimensiune_curenta;i++)
			heap_pt_BU[i]=sir_random[i];
		
		hBUBuild(heap_pt_BU);
		
		_hInit(heap_pt_TD);
		hTDBuild(sir_random,heap_pt_TD);
	}


	profiler.addSeries("Operatii_BU","Comparatii_BU","Atribuiri_BU");
	profiler.addSeries("Operatii_TD","Comparatii_TD","Atribuiri_TD");

	profiler.createGroup("Atribuiri","Atribuiri_BU","Atribuiri_TD");
	profiler.createGroup("Comparatii","Comparatii_BU","Comparatii_TD");
	profiler.createGroup("Operatii","Operatii_BU","Operatii_TD");

	profiler.showReport();
}

int main(){
	
	//verifica_corectitudine();
	
	//genereaza_grafice();
	//genereaza_caz_mediu();
	genereaza_caz_defavorabil();
	
	system("pause");
	return 0;
}