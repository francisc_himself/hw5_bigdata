/*Student: ***ANONIM*** ***ANONIM*** Szonja
Group: ***GROUP_NUMBER***

Interpretation of results:

The measurements were conducted for 3 different cases:

**************** BEST CASE ***************
	
FOR BEST CASE WE HAVE AS INPUT DATA: 
	- A NONDECREASING ARRAY ON WHICH WE APPLIED BUBBLE, SELECTION AND INSERTION SORT

GRAPH INTERPRETATION:
	- ASSIGNMENTS: In case of Bubble and Selection sort we can notice a constant value for the number of assignments, namely 0.
				   For Insertion sort there is a linear increase of the number of assignments.
	- COMPARISONS: For Bubble and Insertion sort we have a linear increase.
				   For Selection sort, the number of comparisons increases quadratically.
			
	
**************** AVERAGE CASE ***************
	
FOR AVERAGE CASE WE HAVE AS INPUT DATA: 
	- AN ARRAY WITH RANDOM UNSORTED ELEMENTS ON WHICH WE APPLY BUBBLE, SELECTION AND INSERTION SORT
	- THE MEASUREMENTS WERE TAKEN 5 TIMES AND THE GRAPH PRESENTS THE SUM OF THE RESULTS OF THESE 5 MEASUREMENTS

GRAPH INTERPRETATION:
	- ASSIGNMENTS: In the case of Bubble and Insertion the number of assignments increases quadratically.
				   For Selection sort, the number of assignments increases slowly; there is a linear increase.
	- COMPARISONS: In the case of all 3 sorting algorithms, the number of comparisons increases quadratically.
				   Bubble sort has the fastest increase, followed by Selection and Insertion.


**************** WORST CASE ***************
	
FOR WORST CASE WE HAVE:
	- NONINCREASING ARRAY ON WHICH WE APPLY BUBBLE AND INSERTION SORT
	- NONDECREASING ARRAY, EXCEPT THE SMALLEST ELEMENT WHICH IS PLACED AT THE END OF ARRAY ON WHICH WE APPLY SELECTION SORT

GRAPH INTERPRETATION:
	- ASSIGNMENTS: In the case of Bubble and Insertion the number of assignments increases quadratically.Bubble has a faster increase, compared to Insertion.
				   For Selection sort, the number of assignments increases slowly; there is a linear increase.
	- COMPARISONS: In the case of all 3 sorting algorithms, the number of comparisons increases quadratically.
				   Bubble sort has the fastest increase, followed by Selection and Insertion which increase at approximately the same rate.


OTHER ASPECTS:
	- EFFICIENCY:
		- For all three algorithms
			Best: O(n)       Worst: O(n^2)
		

	-STABILITY (refers to the relative position of the elements having the same value in the array after sorting):
		- As far as stability is concerned, we can conclude that Insertion is a stable sorting algorithm.
		- In the case of Selection and Bubble, the algorithms given below are stable due to the comparison
		statements inculded in the "if" conditions. For these algorithms we used ">" instead of ">=", which 
		would have made the algorithms become unstable.
		
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>  
//#include <time.h> 
#include "Profiler.h"
#define MAX_SIZE 10000
Profiler profiler("demo");

void BubbleSort(int a[], int array_size)
{
	int i,aux;
	boolean ordered=true;
	static int c[MAX_SIZE];
	profiler.countOperation("comparisonsb",array_size,0);
	profiler.countOperation("assignmentsb",array_size,0);
	for (int l=0;l<array_size;l++)
		c[l]=a[l];
	do
	{
		ordered=true;
		for (i=0;i<array_size-1;i++)
			{profiler.countOperation("comparisonsb",array_size);
			if (c[i]>c[i+1])
			{
				aux=c[i];
				c[i]=c[i+1];
				c[i+1]=aux;
				ordered=false;
				profiler.countOperation("assignmentsb",array_size,3);
			}}
	}while(!ordered);

	/*for (int l=0;l<array_size;l++)
		printf("%d ",c[l]);
	printf("\n");*/
}

void SelectionSort(int a[],int array_size)
{
	int min,i,j,aux;
	static int c[MAX_SIZE];
	for (int l=0;l<array_size;l++)
		c[l]=a[l];
	profiler.countOperation("comparisonss",array_size,0);
	profiler.countOperation("assignmentss",array_size,0);

	for (i=0;i<array_size-1;i++)
	{
		min=i;
		
		for (j=i+1;j<array_size;j++)
			{if (c[min]>c[j])
				{
					min=j;
					
				}
			profiler.countOperation("comparisonss",array_size);
		}
		if (min!=i)
		{
            aux=c[min];
			c[min]=c[i];
			c[i]=aux;
			profiler.countOperation("assignmentss",array_size,3);
		}
		
	}
	/*for (int l=0;l<array_size;l++)
		printf("%d ",c[l]);
	printf("\n");*/
	
}

void InsertionSort(int a[],int array_size)
{
	int i,k,aux;
	static int c[MAX_SIZE];
	for (int l=0;l<array_size;l++)
		c[l]=a[l];
	profiler.countOperation("assignmentsi",array_size,0);
	profiler.countOperation("comparisonsi",array_size,0);

	for (i=1;i<array_size;i++)
	{
		aux=c[i];
		profiler.countOperation("assignmentsi",array_size);
		k=i-1;

		while(k>=0 && aux<c[k])
		{
			c[k+1]=c[k];
			profiler.countOperation("assignmentsi",array_size);
			k--;
			profiler.countOperation("comparisonsi",array_size);
		} 
		profiler.countOperation("comparisonsi",array_size);
		
		c[k+1]=aux; profiler.countOperation("assignmentsi",array_size);
	}
	/*for (int l=0;l<array_size;l++)
		printf("%d ",c[l]);
	printf("\n");*/
}

void PrintArray(int a[],int array_size)
{
	printf("\nThe array is:\n");
	for (int i=0;i<array_size;i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n");
}

int main()
{
	
	int v[MAX_SIZE];
	int n=5;
	int y;





	/****************BEST CASE ***************/
	
	//FOR BEST CASE WE HAVE:
	//NONDECREASING ARRAY FOR BUBBLE, SELECTION AND INSERTION SORT
		
	/*for(n=100; n<10000; n=n+100){
		FillRandomArray(v, n, 0, 32000, true, 1);
		printf("\n n=%d\n", n);
		
		BubbleSort(v,n);
		SelectionSort(v,n);
		InsertionSort(v,n);
		
	}

	profiler.addSeries("compb_plus_assb","comparisonsb","assignmentsb");
	profiler.addSeries("comps_plus_asss","comparisonss","assignmentss");
	profiler.addSeries("compi_plus_assi","comparisonsi","assignmentsi");
	
	profiler.createGroup("best_case_assign","assignmentsb","assignmentss","assignmentsi");
	profiler.createGroup("best_case_comp","comparisonsb","comparisonss","comparisonsi");
	profiler.createGroup("best_case_ass_plus_comp","compb_plus_assb","comps_plus_asss","compi_plus_assi");
	profiler.showReport();
	

	*/



    /****************WORST CASE ***************/
	
	//FOR WORST CASE WE HAVE:
	//NONINCREASING ARRAY FOR BUBBLE AND INSERTION SORT
	//NONDECREASING ARRAY, EXCEPT THE SMALLEST ELEMENT WHICH IS PLACED AT THE END OF ARRAY FOR SELECTION SORT
	
	/*for(n=100; n<10000; n=n+100){
		FillRandomArray(v, n, 0, 32000, true, 2);
		printf("\n n=%d\n", n);
		
		BubbleSort(v,n);
		InsertionSort(v,n);

		FillRandomArray(v, n, 0, 32000, true, 1);
		y=v[0];
		for (int j=1;j<n;j++)
			v[j-1]=v[j];
		v[n-1]=y;
		SelectionSort(v,n);
		
		
	}

	profiler.addSeries("compb_plus_assb","comparisonsb","assignmentsb");
	profiler.addSeries("comps_plus_asss","comparisonss","assignmentss");
	profiler.addSeries("compi_plus_assi","comparisonsi","assignmentsi");
	
	profiler.createGroup("worst_case_assign","assignmentsb","assignmentss","assignmentsi");
	profiler.createGroup("worst_case_comp","comparisonsb","comparisonss","comparisonsi");
	profiler.createGroup("worst_case_ass_plus_comp","compb_plus_assb","comps_plus_asss","compi_plus_assi");
	profiler.showReport();
	
	*/


	
	/****************AVERAGE CASE ***************/
	
	//FOR AVERAGE CASE WE HAVE:
	//ARRAY WITH RANDOM UNSORTED ELEMENTS FOR BUBBLE, SELECTION AND INSERTION SORT
	
	for (int k=0;k<5;k++)
	{
	for(n=100; n<10000; n=n+100){
		FillRandomArray(v, n, 0, 32000, true, 0);
		printf("\n n=%d\n", n);
		
		BubbleSort(v,n);
		SelectionSort(v,n);
		InsertionSort(v,n);
		
	}	
	}

	profiler.addSeries("compb_plus_assb","comparisonsb","assignmentsb");
	profiler.addSeries("comps_plus_asss","comparisonss","assignmentss");
	profiler.addSeries("compi_plus_assi","comparisonsi","assignmentsi");
	
	profiler.createGroup("sum_of_avg_case_assign","assignmentsb","assignmentss","assignmentsi");
	profiler.createGroup("sum_of_avg_case_comp","comparisonsb","comparisonss","comparisonsi");
	profiler.createGroup("sum_of_avg_case_ass_plus_comp","compb_plus_assb","comps_plus_asss","compi_plus_assi");
	profiler.showReport();

   getch();
	return 0;

	
}