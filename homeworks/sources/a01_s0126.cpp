#include "Profiler.h"
#include <stdio.h>
#include <conio.h>
Profiler profiler("demo");
const int VMAX = 5000;
const int VMIN = 0;
void insertionsort(int a[], int n)
{
	profiler.countOperation("assig_ins",n,0);
	profiler.countOperation("count_ins",n,0);

	for (int i = 1; i<n; i++)
	{
		int k = i-1;
		int buff =a[i];
		while(a[k]>buff&&k>=0)
		{
			a[k+1]=a[k];profiler.countOperation("assig_ins",n);
			k--;profiler.countOperation("count_ins",n);
		}
		a[k+1] = buff;profiler.countOperation("assig_ins",n);
	}
}

void selectionsort(int *a, int n)
{
	int aux,min,i,j;
	profiler.countOperation("assig_sel",n,0);
	profiler.countOperation("count_sel",n,0);
	profiler.countOperation("total_sel",n,0);
	for(i=0;i<n-1;i++)
	{
		min = i;
		for (j=i+1;j<n;j++)
		{
			profiler.countOperation("count_sel",n);
			if (a[j]<a[min]) min = j;
		}
			aux = a[min];
			a[min] = a[i]; profiler.countOperation("assig_sel",n,3);
			a[i] = aux;
	}
}

void bubblesort(int *a, int n)
{
	profiler.countOperation("assig_bubble",n,0);
	profiler.countOperation("count_bubble",n,0);
	int swap,aux;
	do
	{
		swap = 0;
		for (int i = 0; i<n-1; i++)
		{
			if (a[i] > a[i+1]) 
			{
				aux = a[i];
				a[i] = a[i+1];
				a[i+1] = aux;
				swap = 1;
				profiler.countOperation("assig_bubble",n,3);
			}
			profiler.countOperation("count_bubble",n);
		}
	}
	while(swap);
}

int* generateRandom(int n)
{
  int *a = new int[n];
  srand(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=rand()%(VMAX-VMIN) +VMIN;
  return a;
}

int* generateOrdered(int n)
{
 int* a=new int[n]; 
 int maxdiff=5;
 a[0]=rand()%maxdiff;
  for (int i=1;i<n;i++)
    a[i]=a[i-1]+rand()%maxdiff;
  return a;
}

int* generateDecreasing(int n)
{
 int* a=new int[n]; 
 int maxdiff=5;
 a[n-1]=rand()%maxdiff;
  for (int i=n-2;i>0;i--)
    a[i]=a[i+1]+rand()%maxdiff;
  return a;
}

int * deepCopy(int *a, int n)
{
	int *b = new int[n];
	for(int i = 0; i<n;i++)
	{
		b[i] = a[i];
	}
	return b;
}

void main()
{
	//test if it works
	/*
	int *a = generateRandom(500);
	int *b = deepCopy(a,500);
	int *c = deepCopy(a,500);
	bubblesort(c,500);
	insertionsort(a,500);
	selectionsort(b,500);
	for (int i = 0;i<500;i++)
		printf("%d ",a[i]);
	printf("\n \n");
	for(int i = 0;i<500;i++)
		printf("%d ",b[i]);
	printf("\n \n");
	for(int i = 0;i<500;i++)
		printf("%d ",c[i]);
	*/

	//test for average case
	/*
	printf("average case testing... \n");
	for (int j = 0;j<5;j++)
	{
	printf("step %d/5 \n",j+1);
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *f = generateRandom(n);
		int *d = deepCopy(f,n);
		int *e = deepCopy(f,n);
		printf("insertionsort...\n");
		insertionsort(f,n);
		printf("selectionsort...\n");
		selectionsort(d,n);
		printf("bubblesort...\n");
		bubblesort(e,n);
	}
	}
	*/
	profiler.createGroup("count","count_ins","count_sel","count_bubble");
	profiler.createGroup("assig","assig_ins","assig_sel","assig_bubble");
	profiler.addSeries("total_ins","assig_ins","count_ins");
	profiler.addSeries("total_sel","assig_sel","count_sel");
	profiler.addSeries("total_bubble","assig_bubble","count_bubble");
	profiler.createGroup("total","total_ins","total_sel","total_bubble");
	profiler.showReport();

	//bubblesort  best case test
	/*
	for (int n=100;n<10000;n = n + 100)
	{
		int *a = generateOrdered(n);
		bubblesort(a,n);
	}
	profiler.addSeries("total_bubble","assig_bubble","count_bubble");
	profiler.showReport();

	//bubblesort worst case test
	*/
	printf("Bubblesort worst case\n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateDecreasing(n);
		bubblesort(a,n);
	}
	profiler.addSeries("total_bubble","assig_bubble","count_bubble");
	profiler.showReport();
	/*
	//insertionsort best case test
	
	for (int n=100;n<10000;n = n + 100)
	{
		int *a = generateOrdered(n);
		insertionsort(a,n);
	}
	profiler.addSeries("total_ins","assig_ins","count_ins");
	profiler.showReport();
	
	//insertionsort worst case test
	
	printf("Insertionsort worst case\n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateDecreasing(n);
		insertionsort(a,n);
	}
	profiler.addSeries("total_ins","assig_ins","count_ins");
	profiler.showReport();
	
	//selectionsort best case test
	printf("Selectionsort best case\n");
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateOrdered(n);
		selectionsort(a,n);
	}
	profiler.addSeries("total_sel","assig_sel","count_sel");
	profiler.showReport();
	
	//selectionsort worst case test
	printf("Selectionsort worst case\n");
	int min;
	for (int n=100;n<10000;n = n + 100)
	{
		printf("n=%d\n",n);
		int *a = generateOrdered(n);
		min = a[0];
		for(int i=0;i<n-1;i++)
			a[i] = a[i+1];
		a[n-1] = min;
		selectionsort(a,n);
	}
	profiler.addSeries("total_sel","assig_sel","count_sel");
	profiler.showReport();*/
}