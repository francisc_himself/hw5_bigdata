#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler;


typedef struct node // single linked list
{
    int info;
    node *next;
} NODE;


typedef struct element
{
    int position; // index of lists
    int elem;
} ELEMENT;


struct list
{
    NODE *first;
    NODE *last;
};


struct list LIST[501];
NODE *interfirst ;
NODE *interlast;
ELEMENT Heap[10001];
int a[10001];


void add_element(NODE **first, NODE **last, int value)		//adds an elem to the list;
{
    if((*first)==NULL)		//In the case that the list is empty;
    {
        (*first) = new NODE;
        (*first) -> info = value;
        (*first) -> next = NULL;
        *last = (*first);
    }
    else
    {
        NODE *element = new NODE;
        element -> next = NULL;
        element -> info = value;
        (*last) -> next = element;
        (*last) = element;
    }
}


void reconstruct_heap_TD(ELEMENT Heap[],int i,int size,int n)	//Reconstructs the heap, top down;
{
    int left,right,minimum;
    ELEMENT aux;
    left=2*i;
    right=2*i+1;
	profiler.countOperation("Assig and Comp",n);
    if ((left<=size)&&(Heap[left].elem < Heap[i].elem))		// which of the son is smaller than its parrent;
        minimum=left;
    else
        minimum=i;
	profiler.countOperation("Assig and Comp",n);
    if ((right<=size)&&(Heap[right].elem < Heap[minimum].elem))
        minimum=right;
	profiler.countOperation("Assig and Comp",n);
    if (minimum!=i)		//swap if a son is smaller than the parrent;
	{
        aux=Heap[i];
        Heap[i] = Heap[minimum];
        Heap[minimum]=aux;
		profiler.countOperation("Assig and Comp",n,3);
        reconstruct_heap_TD(Heap,minimum,size,n);
    }
}


void insert_heap(ELEMENT Heap[],ELEMENT x,int *size, int n)		//adds an elem to the heap;
{
    (*size)++;
    Heap[(*size)]=x;
    int i=(*size);
    ELEMENT aux;
	profiler.countOperation("Assig and Comp",n);
    while (i>1 && (Heap[i].elem)<(Heap[i/2].elem))
    {
        profiler.countOperation("Assig and Comp",n);
		aux=Heap[i];
        Heap[i]=Heap[i/2];
        Heap[i/2]=aux;
		profiler.countOperation("Assig and Comp",n,3);
        i=i/2;
    }
}


element extract_heap(ELEMENT Heap[],int *size,int n)		//returns the first elem from the heap;
{
    ELEMENT x;
    if((*size)>0)
    {
        x=Heap[1]; 
        Heap[1]=Heap[(*size)];
		profiler.countOperation("Assig and Comp",n);
        (*size)--;
        reconstruct_heap_TD(Heap,1,(*size),n);		//reconstructs the heap;
    }
    return x;
}


void interclass(list LIST[],int k,int n)
{
    int size = 0;
    ELEMENT v;
    for(int i=1; i<=k; i++)		//places in the heap array the smallest k elements;
    {
        v.position = i;
        v.elem = LIST[i].first -> info;
		profiler.countOperation("Assig and Comp",n);
        insert_heap(Heap,v,&size,n);
    }
    ELEMENT pop;
    while (size > 0)
    {
        pop = extract_heap(Heap,&size,n);
        int position = pop.position;
        add_element(&interfirst,&interlast,pop.elem);
        LIST[position].first = LIST[position].first->next;
		profiler.countOperation("Assig and Comp",n);
		profiler.countOperation("Assig and Comp",n);
        if (LIST[position].first!=NULL)
        {
            v.position = pop.position;
            v.elem = LIST[pop.position].first ->info;
			profiler.countOperation("Assig and Comp",n);
            insert_heap(Heap,v,&size,n);		//we insert to the heap the smallest element from the list;
        }
    }
}


void create_list(list LIST[],int k,int n)
{
    int nr = 0;
    for (int i=1; i<=k; i++)
        LIST[i].first = LIST[i].last = NULL;
    for (int i=1; i<=k; i++)
    {
		nr = rand()%100;
        for (int j=1; j<=n/k; j++)
        {
            nr=nr+rand()%100;
            add_element(&LIST[i].first,&LIST[i].last,nr);
        }
    }

}


void listing(NODE *first,NODE *last)
{
    NODE *c;
    c = first;
    while(c!=0)		//while they are still in the list;
    {
        printf("%d    ",c->info);
        c = c->next;		//advance in the list to the next address;
    }
	printf("\n");
}




int main()
{	

	//Test to see if it works;

	printf("\n\nLists:\n\n");
	create_list(LIST,5,25);
	for(int i=0;i<=5;i++)
		listing(LIST[i].first,LIST[i].last);
	interclass(LIST,5,25);
	printf("\n\nAfter the interclass:\n\n");
	listing(interfirst,interlast);
	_getch();
	
	

	//Test for k=5;
	
/*	
	int k;
	for (int n = 100 ; n <= 10000; n+=100)
    {
		printf("n= %d\n",n);
		k=5;
        create_list(LIST,k,n);
        interclass(LIST,k,n);
	}
	profiler.showReport();
	*/
	//Test for k=10
	/*
	int k;
	for (int n = 100 ; n <= 10000; n+=100)
    {
		printf("n= %d\n",n);
		k=10;
        create_list(LIST,k,n);
        interclass(LIST,k,n);
	}
	profiler.showReport();
	*/

	//Test for k=100
	
	/*
	int k;
	for (int n = 100 ; n <= 10000; n+=100)
    {
		printf("n= %d\n",n);
		k=100;
        create_list(LIST,k,n);
        interclass(LIST,k,n);
	}
	profiler.showReport();
	*/
	

	//Test 2;
	/*
	int   n=10000;
	for (int k = 10 ; k<=500 ; k+=10)
	{
		printf("k= %d\n",k);
		create_list(LIST,k,n);
		interclass(LIST,k,k);
	}
	profiler.showReport();
	*/
	return 0;
}


	