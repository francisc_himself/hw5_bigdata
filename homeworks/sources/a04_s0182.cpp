#include "Profiler.h"
#include <time.h> 
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
Profiler profiler("demo");

int assign;
int n;
int k;

typedef struct pair
{
	int value, setNr;
} HeapElementT;

void heapify( HeapElementT heap[], int size, int k , int dim)
{
	int smallest;
 
	
	assign++;
	if ( 2*k <= size && heap[2*k].value < heap[k].value ) //leftchild smaller than parent
	{
		smallest = 2*k;
		profiler.countOperation("merge", dim,1);
	}
	else smallest = k;
	assign++;
	if ( 2*k + 1 <= size && heap[2*k + 1].value < heap[smallest].value ) //rightchild is the smallest
	{
		smallest = 2*k + 1;
		profiler.countOperation("merge", dim,2);
	}
	if ( smallest != k )
	{
		HeapElementT temp = heap[k];
		heap[k] = heap[smallest];
		heap[smallest] = temp;
		profiler.countOperation("merge", dim,3);
		assign += 3;
		heapify( heap, size, smallest, dim);
	}
}

void buildHeap( HeapElementT heap[], int size )
{
	
	int i;
	 
	for ( i = size/2 ; i>= 1; i-- )
		heapify( heap, size, i, size );
}

void merge ( HeapElementT heap[], int p, int final[], int **sets, int current[], int n  )
{
	int fn, index, i;
	char c;
	k = p;

	fn = 0;
	while ( p != 0 )
	{
		fn++;
		final[fn] = heap[1].value; //puts the root of the heap (the smallest element) in the final set
		profiler.countOperation("merge", k, 1);
		assign++;
   
		assign++;
		if ( current[heap[1].setNr] != n/p )
		{
			current[heap[1].setNr]++;
			assign++;
			heap[1].value = sets[heap[1].setNr][current[heap[1].setNr]]; //puts new element in the root
			profiler.countOperation("merge", k,2);
			assign++;
		}
		else 
		{
			heap[1].value = heap[p].value;
			heap[1].setNr = heap[p].setNr;
			profiler.countOperation("merge", k,2);
			p--;
		}
		
		//resets the heap property
		heapify( heap, p, 1,k);

	}
}

int main()
{
	int  p, i, j, m;
	int** sets = new int*[501];//[10001];
	int *finalSet = new int[10001];
	HeapElementT heap[501];
	int current[501];
	char c;


	for ( i = 1; i <= 501; i++)
		sets[i] = new int[2001];
	
  srand( time( NULL));


  n = 10000;
  //p=100;
  //for (n=100; n<10000; n+=100){
 for ( p = 40; p <= 500; p = p+=10 )
 {
	  printf("%d \n", p);

	  assign = 0;
  for ( i = 1; i <= p; i++)
	  for ( j = 1; j <= n/p; j++ )
		sets[i][j] = 0;

  for ( i = 1; i <= p; i++ ) //generate the random elements of each set which is ordered ascendingly
	for ( j = 1; j <= n/p; j++ )
		sets[i][j] = sets[i][j-1] + rand() % 20;

   // the array representing the heap will initially hold the first elements
   for ( i = 1; i <= p; i++ )
   {
	   heap[i].value = sets[i][1];
	   heap[i].setNr = i;
   }
   
  //sets for each set the current element to be the second
   for ( i = 1; i <= p; i++ )
	   current[i] = 1;

   //build heap 
   buildHeap( heap, p );

   merge( heap, p, finalSet, sets, current, n );

   
	
  
  }
  
  profiler.createGroup("Merge", "merge");
	profiler.showReport();
}