//parent to normal
//normal to multiway
//print multiway

#include "stdio.h" 
#include "stdlib.h" 
#include "conio.h" 
 




typedef struct BinaryNode{
	BinaryNode*  Children;
	BinaryNode* Next;
	int key; 
}BinaryNode;


typedef struct MultiNode{
	int key;
	MultiNode** children;
	int nrofchildren;
}MultiNode;


MultiNode* R2(int *a ,int size){
 int* count=(int*)malloc(sizeof(int)*size); //array for storing the number of child nodes

 for(int i=0;i<size;i++){//Initializing the count array
  count[i]=0;
 }

 for(int i=0;i<size;i++){
  count[a[i]]++;
 }

MultiNode** Nodes=(MultiNode**)malloc(sizeof(MultiNode*)*size);//creating an array of nodes

 for (int i=0;i<size;i++){
	Nodes[i]=(MultiNode*)malloc(sizeof(MultiNode));
	Nodes[i]->key=i;
	Nodes[i]->children=(MultiNode**)malloc(sizeof(MultiNode*)*count[i]);//allocating momery for vector of children nodes
	Nodes[i]->nrofchildren=0;
 }

for (int i=0;i<size;i++){//allocating memory for chilodren nodes
	for (int j=0;j<count[i];j++){
	(Nodes[i]->children[j])=(MultiNode*)malloc(sizeof(MultiNode));

	}
 }


for (int i=0;i<size;i++){
	//add i as child to a[i]
	//(Nodes[a[i]]->children[Nodes[a[i]]->nrofchildren])=(MultiNode*)malloc(sizeof(MultiNode));

	if(count[a[i]]!=0 && a[i]!=-1 ) {
	(Nodes[a[i]]->children[Nodes[a[i]]->nrofchildren]  )=Nodes[i];
	Nodes[a[i]]->nrofchildren++;	
	}
 }


for (int i=0;i<size;i++){
	//finding root
	if (a[i]==-1)
		return Nodes[i];
 }
return NULL;

}

/*
BinaryNode* children(MultiNode* Multi)
{//function returns a binary node that has the same children as the multiway node 
BinaryNode* Binary=(BinaryNode*)malloc(sizeof(BinaryNode));
BinaryNode** Aux=(BinaryNode**)malloc(sizeof(BinaryNode*));
Binary->key=Multi->key;
//Aux=&Binary->Children;
*Aux=Binary->Children;

for(int i=0;i<Multi->nrofchildren;i++){
	*Aux=children(Multi->children[i]);

	//Aux->key=Multi->children[i]->key;
	(*Aux)->Next=(BinaryNode*)malloc(sizeof(BinaryNode));
	*Aux=(*Aux)->Next;
}

return Binary;
}
BinaryNode* R3(MultiNode* root){


BinaryNode* Root=(BinaryNode*)malloc(sizeof(BinaryNode));
Root=children(root);
/*Root->key=root->key;
Root->Children=(BinaryNode*)malloc(sizeof(BinaryNode));
Root->Next=NULL;
Root->Children->key=root->children[0]->key;

*/

//BinaryNode* Aux=(BinaryNode*)malloc(sizeof(BinaryNode));
//Aux=Root->Children;
//for(int i=1;i<root->nrofchildren;i++){
//	Aux->Next=(BinaryNode*)malloc(sizeof(BinaryNode));
//	Aux->Next->key=root->children[i]->key;
//	Aux=Aux->Next;

//}

//return Root;
//}


/*
MultiwayNode* T(int* a,int size){
	MultiwayNode** Nodes=(MultiwayNode**)malloc(sizeof(MultiwayNode*)*size);
	for (int i=0;i<size;i++){
	Nodes[i]=(MultiwayNode*)malloc(sizeof(MultiwayNode));
	Nodes[i]->key=i;
	Nodes[i]->Children=NULL;
	Nodes[i]->Next=NULL;
	printf(" Node %d s key is %d\n",i,Nodes[i]->key);
	}
	
	for (int i=0;i<size;i++)
	{
		if (a[i]==-1) continue;
		MultiwayNode* Aux=(MultiwayNode*)malloc(sizeof(MultiwayNode));	
		Aux=Nodes[a[i]]->Children;
		if (Nodes[a[i]]->Children==NULL )  Nodes[a[i]]->Children=Nodes[i];
		else
		{
		while ( Aux->Next!=NULL )
			{
				 Aux=Aux->Next;
			}
			Aux->Next=Nodes[i];//adding child at the end of the list containing its children
		}


	}//endfor 
	
	for (int i=0;i<size;i++){
	if (a[i]==-1) return Nodes[i];
	}

	return NULL;
}

*/
BinaryNode* R3(BinaryNode* binary,MultiNode* multi){
if(multi == NULL){
return NULL;
}
binary = (BinaryNode*)malloc(sizeof(BinaryNode));
binary->key=multi->key;
binary->Children=NULL;
	binary->Next=NULL;
BinaryNode* current = NULL;
BinaryNode* prev =NULL;
for(int i = 0;i < multi->nrofchildren;i++){
if(i == 0){
	current = R3(binary->Children,(multi->children[i]));
	binary->Children = current;
}else{
prev = current;
current = R3(current->Next,(multi->children[i]));
prev->Next = current;
}
}
return binary;

}



void prettyPrint(BinaryNode* binary,int d){
if(binary == NULL){
return;
}
for(int i = 0 ; i < d;i++){
printf("       ");
}
printf("%d\n",binary->key+1);
prettyPrint(binary->Children,d+1);
prettyPrint(binary->Next,d);
return;
}

int main(void){
	int a[9]={1,6,4,1,6,6,-1,4,1};


//Parent representation
	printf("First representation: \n");
	for(int i=0;i<9;i++){
		if (a[i]==-1) printf(" %d ",-1); else
				printf(" %d ",a[i]+1);
	}
	printf("\n");

//Multiway representation
	printf("Second representation: \n");
MultiNode* Multi=(MultiNode*)malloc(sizeof(MultiNode));
Multi= R2(a ,9);

	printf("%d has %d nrofclihdren\n",Multi->key+1,Multi->nrofchildren);
for(int i=0;i<Multi->nrofchildren;i++){
	printf("%d has %d nrofclihdren \n",Multi->children[i]->key+1,Multi->children[i]->nrofchildren);
	for(int j=0;j<Multi->children[i]->nrofchildren;j++){
		printf("%d has %d nrofclihdren \n",Multi->children[i]->children[j]->key+1,Multi->children[i]->children[j]->nrofchildren);
	}
}


//Third representation
/*
printf("Third representation: \n");
BinaryNode* Binary=(BinaryNode*)malloc(sizeof(BinaryNode));
Binary=R3(Multi);
printf("%d ",Binary->key+1);

BinaryNode** Aux=(BinaryNode**)malloc(sizeof(BinaryNode));
BinaryNode* Aux2=(BinaryNode*)malloc(sizeof(BinaryNode));
*Aux=Binary->Children;
while(Aux!=NULL){
printf("  %d\n",(*Aux)->key+1);

Aux2=(*Aux)->Children;
while(Aux2!=NULL){
printf("    %d\n",Aux2->key);
Aux2=Aux2->Next;
}
(*Aux)=(*Aux)->Next;
}

*/
printf("Third representation: \n");
BinaryNode* Binary=(BinaryNode*)malloc(sizeof(BinaryNode));
Binary=R3(Binary,Multi);
prettyPrint(Binary,1);

	getch();
	return 0;
}