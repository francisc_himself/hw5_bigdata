﻿
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10007
/****ANONIM*** ***ANONIM***,gr ***GROUP_NUMBER***
Se cere implementarea corecta si eficienta ,,,insert si cautare in operațiuni într-un
tabelul hash folosind abordarea deschisă...
*/

int t[N];
//Quadratic hashing
int hash(int x, int i)
{ 

	//Cormen,formula
	return (x%N + 37*i+ 41*i*i)%N;
}

int insert(int x)
{
	int j, pos;
	for (j=0; j<N; j++)
	{
		pos = hash(x, j); //J- numarul de incercari de a construi hashul pentru un element
		if (t[pos]==-1) //pozitie libera
		{
			t[pos] = x; //se introduce elementul pe pozitia calculata 
			return pos; //returneaza pozitia elementului introdus
		}
	}
	return -1;
}

//se cauta elementul x in hash table,se returneaza -n in caz cind elementul nu e gasit
int searchEff(int x)
{
	int i, p;
	for (i=0; i<N; i++)
	{
		p = hash(x,i); 
		if (t[p]==-1)
			return -(i+1); 
		if (t[p]==x)
			return i+1;	//pozitia elementului gasit	
	}
	return -N;
}

void removeAll()
{
	int i;
	for (i=N-1; i>=0; i--)
		t[i] = -1;
}

int main()
{
	int i, j, k, x, s, u, e, EMedG, EMaxG, EmedNEG, EMaxNEG;
	for (i=N-1;i>=0;i--)
		t[i] = -1; //pozitie libera, nu se contina nici un element
	FILE *g = fopen("hash.csv", "w");
	fprintf(g,"k;EMedG;EMaxG;EmedNEG;EMaxNEG\n");

	int l, f[]={8000,8500,9000,9500,9900};
	srand((unsigned int)time(NULL));
	for (l=0; l<5; l++)
	{
		k = f[l];
		EMedG = EMaxG = EmedNEG = EMaxNEG = 0;
		for (j=0; j<5; j++)
		{
			for (i=k-1; i>=0; i--) 
			{
				x = rand();
				if (x<0)//se pot introduce nr. pozitive 
					insert(-x);
				else
					insert(x);
			}
			//search
			u = s = 0;
			while(u<3000 || s<3000)
			{
				x = rand(); //se genereaza nr random
				e = searchEff(x); //se cauta elementul in tabel
				if (e>0 && s<3000) //daca codul returnat e mai mare ca 0 - successuful
				{
					s++; //incrementeaza nr de successful
					EMedG += e;
					if (e>EMaxG)
						EMaxG = e;
				} //acelasi lucru pentru unsuccesuful, e <0
				if (e<0 && u<3000)
				{
					u++;//incrementeaza unsuccesful
					EmedNEG -= e;
					if (-e>EMaxNEG)
						EMaxNEG = -e;
				}
			}			
			removeAll();
		}
		fprintf(g, "%f;%f;%d;%f;%d\n", (float)k/N, EMedG/3000.0/5, EMaxG, EmedNEG/3000.0/5, EMaxNEG);
		/*
		EMedG - Efort mediu cautare elemente gasite

		EMaxG - Efort maxim gasite

		EmedNEG - Efort mediu cautare negasite

		EMaxNEG - Efort maxim negasite
		*/

	}

	fclose(g);
	return 0;
}