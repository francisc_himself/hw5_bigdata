/*
	
	Nume  : ***ANONIM***-Cornel ***ANONIM***
	Grupa : ***GROUP_NUMBER***

	Hash Tables

*/


#include <stdio.h>     
#include <stdlib.h>     
#include <time.h>
#include <conio.h>


#define N 9973

FILE *fout = fopen("hash.csv", "w");

int t[N];

int maxEffortFound = 0;
int maxEffortNotFound = 0;

int effortFound = 0;
int effortNotFound = 0;

int random1[N];
int random2[N];

void genRandom()
{
	srand (time(NULL));
	for(int i=0;i<N;i++)
	{
		random1[i] = 1 + rand() % 10000;
		random2[i] = 10000 + rand() % 5000;
	}
}

//Hash Function
int h(int x, int i)
{
	return (x + i + 3*i*i)%N;
}

//Add Function
void add(int x)
{
	int i = 0;
	int j = h(x,i);

	while(i<N)
	{
		if(t[j] == -1)
		{
			t[j] = x;
		}
		i++;
	}
}

//Search Function for numbers in the hash map
int searchFound(int x)
{
	int i = 0;
	do{
		int j = h(x,i);
		effortFound++;
		if(t[j] == x)
		{
			if(i+1 > maxEffortFound)
				maxEffortFound = i+1;
			return x;
		};

		if(t[j]==-1)
		{
			if(i+1 > maxEffortFound) 
				maxEffortFound = i+1;
			return -1;
		};

		i++;
		}
	while(i<N);

	if(i+1 > maxEffortFound) maxEffortFound = i+1;
}

//Search Function for numbers NOT in the hash map
int searchNotFound(int x)
{
	int i = 0;
	do{
		int j = h(x,i);
		effortNotFound++;
		if(t[j]==x)
		{
			if(i+1 > maxEffortNotFound) 
				maxEffortNotFound = i+1;
			return x;
		}

		if(t[j]==-1)
		{
			if(i+1 > maxEffortNotFound)
				maxEffortNotFound = i+1;
			return -1;
		}

		i++;
		
	}	while(i<N);

	if(i+1 > maxEffortNotFound) maxEffortNotFound = i+1;
	
}

//main method
int main()
{
	//initialize hash table
	for(int i =0;i<N;i++)
	{
		t[i] = -1;
	}

	//generate random
	genRandom();

	fprintf(fout, "Fill Value,Avg Effort Found,Max Effort Found,Avg Effort Not Found,Max Effort Not Found\n");

	//testing cases 0.8, 0.85, 0.9, 0.95
	for(float j=0.8;j<0.99;j+=0.05){
		maxEffortFound = 0;
		maxEffortNotFound = 0;
		//add N*j random numbers to the hash table
		for(int i=0;i<N*j;i++)
		{
			add(random1[i]);
		}

		//search for the added numbers
		for(int i=0;i<1500;i++)
		{
			searchFound(random1[i]);
		}

		//search for numbers not in hash table
		for(int i=0;i<1500;i++)
		{
			searchNotFound(random2[i]);
		}

		fprintf(fout, "%.2f,%.2f,%d,%.2f,%d\n", j, effortFound/1500.0, maxEffortFound, effortNotFound/1500.0, maxEffortNotFound);
	}


	//testing 0.99 case
	float j=0.99;
		//add N*j random numbers to the hash table
		for(int i=0;i<N*j;i++)
		{
			add(random1[i]);
		}

		//search for the added numbers
		for(int i=0;i<1500;i++)
		{
			searchFound(random1[i]);
		}

		//search for numbers not in hash table
		for(int i=0;i<1500;i++)
		{
			searchNotFound(random2[i]);
		}

		fprintf(fout, "%.2f,%.2f,%d,%.2f,%d\n", j, effortFound/1500.0, maxEffortFound, effortNotFound/1500.0, maxEffortNotFound);

	return 0;
}