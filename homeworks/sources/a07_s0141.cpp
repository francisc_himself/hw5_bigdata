#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct struct1
{
    int nrFii;
    int fii[10];
}struct1;


typedef struct struct_arbore_multicai
{
    int key;
    int nrFii;
    struct struct_arbore_multicai *fii[10];
} struct_arbore_multicai;


typedef struct struct_arbore_binar
{
    int key;
    struct struct_arbore_binar *st;
    struct struct_arbore_binar *dr;
} struct_arbore_binar;


void vectorInMulticai(struct_arbore_multicai **r, struct1 v[])
{
    int i;
    for(i = 0; i < v[(*r)->key].nrFii; i++)
    {
        (*r)->fii[i] = (struct_arbore_multicai*)malloc(sizeof(struct_arbore_multicai));
        (*r)->fii[i]->key = v[(*r)->key].fii[i];
        (*r)->fii[i]->nrFii = 0;
        (*r)->nrFii++;
        vectorInMulticai(&((*r)->fii[i]), v);
    }
}


void multicaiInBinar(struct_arbore_binar **b, struct_arbore_multicai *m)
{
    struct_arbore_binar *aux;
    int i;
    if(m->nrFii > 0)
    {
        (*b)->st = (struct_arbore_binar*)malloc(sizeof(struct_arbore_binar));
        (*b)->st->key = m->fii[0]->key;
        (*b)->st->st = NULL;
        (*b)->st->dr = NULL;
        aux = (*b)->st;
        for(i = 1; i < m->nrFii; i++)
        {
            aux->dr = (struct_arbore_binar*)malloc(sizeof(struct_arbore_binar));
            aux->dr->key = m->fii[i]->key;
            aux->dr->st = NULL;
            aux->dr->dr = NULL;
            multicaiInBinar(&aux,m->fii[i-1]);
            aux = aux->dr;
        }

    }
}

void afisareArboreBinar(struct_arbore_binar *r, int n)
{
    int i;
    for(i = 0; i <= n; i++)
        printf(" ");
    printf("%d\n", r->key);
    if(r->st != NULL)
        afisareArboreBinar(r->st, n+1);
    if(r->dr != NULL)
        afisareArboreBinar(r->dr, n);
}

void main()
{
    int  i, n;
	n = 9;
    struct1 v[10];
    struct_arbore_multicai *radacinaArboreMulticai = NULL;
    struct_arbore_binar *radacinaArboreBinar = NULL;
	int a[9];
	a[1] = 2;
	a[2] = 7;
    a[3] = 5;
    a[4] = 2;
    a[5] = 7;
    a[6] = 7;
    a[7] = -1;
    a[8] = 5;
    a[9] = 2;

    for(i = 1; i <= n; i++)
        printf("%d ", a[i]);
    printf("\n");

    for(i = 1; i <= n; i++)
        v[i].nrFii = 0;

    for(i = 1; i <= n; i++)
    {
        if(a[i] != -1)
        {
            v[a[i]].fii[v[a[i]].nrFii] = i;
            v[a[i]].nrFii++;
        }
    }

    i = 1;
    while(a[i] != -1)
        i = a[i];

    radacinaArboreMulticai = (struct_arbore_multicai*)malloc(sizeof(struct_arbore_multicai));
    radacinaArboreMulticai->key = i;
    radacinaArboreMulticai->nrFii = 0;
    vectorInMulticai(&radacinaArboreMulticai, v);

	printf("\n");


    radacinaArboreBinar = (struct_arbore_binar*)malloc(sizeof(struct_arbore_binar));
    radacinaArboreBinar->key = radacinaArboreMulticai->key;
    radacinaArboreBinar->st = NULL;
    radacinaArboreBinar->dr = NULL;
    multicaiInBinar(&radacinaArboreBinar, radacinaArboreMulticai);

    afisareArboreBinar(radacinaArboreBinar, 0);

    getch();
}
