#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>

struct node{
	int val;
	node *copil;
	int nr_ch;
};

struct binar{
	int val;
	binar *copii,*frati;
};

node *noduri;
int root;

void T1(int *t,int n)
{
	noduri=new node[n];
	int pos[100],i;

	memset(pos,0,sizeof(pos));

	for(i=0;i<n;i++)
	{
		noduri[i].nr_ch=0;
		noduri[i].copil=NULL;
	}

	
	for(i=0;i<n;i++)
		if(t[i]>=0)
			noduri[t[i]].nr_ch++; 
		else
			root=i;

	
	for(i=0;i<n;i++)
		if(noduri[i].nr_ch>0)
			noduri[i].copil=new node[noduri[i].nr_ch];

	for(i=0;i<n;i++)
	{
		noduri[i].val=i;
		if(t[i]>=0)
		{
			noduri[t[i]].copil[pos[t[i]]]=noduri[i];   
			pos[t[i]]++;
		}
	}

}

void T2(binar *current, int Mroot)
{
	if(noduri[Mroot].nr_ch>0)
	{
		
		current->copii=new binar;
		current->copii->copii=NULL;
		current->copii->frati=NULL;
		current->copii->val=noduri[Mroot].copil[0].val;

		int i;
		binar *fiu=current->copii;  

		for(i=1;i<noduri[Mroot].nr_ch;i++)
		{
			fiu->frati=new binar;
			fiu->frati->frati=NULL;
			fiu->frati->copii=NULL;
			fiu->frati->val=noduri[Mroot].copil[i].val;
			fiu=fiu->frati; 
		}
	}

	
	if(current->copii!=NULL)
		T2(current->copii,current->copii->val);

	if(current->frati!=NULL)
		T2(current->frati,current->frati->val);
}

void PFP(binar *r,int spaces)
{
	int i;

	for(i=1;i<=spaces;i++)
		printf(" ");

	printf("%d\n",r->val);

	if(r->copii!=NULL)
		PFP(r->copii,spaces+1);

	if(r->frati!=NULL)
		PFP(r->frati,spaces);
}

int main()
{
	int n=9,t[9]={1, 6, 4, 1, 6, 6, -1, 4, 1};
	binar *rootB;
	
	T1(t,n);

	rootB=new binar;
	rootB->frati=NULL;
	rootB->copii=NULL;
	rootB->val=root;
	T2(rootB,root);

	PFP(rootB,0);
	getch();
	return 0;
}