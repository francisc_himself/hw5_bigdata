/*

Tema1 de ***ANONIM*** ***ANONIM***
grupa ***GROUP_NUMBER*** semigrupa 2
Data: 2013.03.14

Titlu: Sortari: Bubble, Insertie, Selectie



1) cazul cel mai favorabil la cele 3 sortari
	elementele vectorului sunt in ordine

2) cazul cel mai defavorabil la cele 3 sortari
	elementele vectorului sunt in ordie inversa

3) cazul mediu statistic
	elementele vectorului sunt aleatoare
	se ordoneaza de 5 ori si se face media 
	atribuirilor comparatiilor si sumelor acestora

Graficele se gasesc in best-final
					   worst-final
					   average-final
.csv




*/


#include <conio.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int i,n,sortat,k;
int v[10000];



void bubble(int v[], int n, int* a, int* c)
{
	int sortat=0;
	while (sortat==0)
	{
		sortat=1;
		for (i=0; i<n-1; i++)
		{
			(*c)++;
			if (v[i]>v[i+1])
			{
				k=v[i];
				v[i]=v[i+1];
				v[i+1]=k;
				(*a)+=3;
				sortat=0;
			}
		}
	}

}

void selectie(int v[], int n, int* a, int* c)
{
	int i,min,imin,j;
	for (i=0; i<n-1; i++)
	{
		(*a)++;
		min=v[i];
		imin=i;
		for (j=i+1; j<n; j++)
		{
			(*c)++;
			if (v[j]<min)
			{
				min=v[j];
				imin=j;
				(*a)=(*a)+1;
			}
		}
		v[i]=v[imin];
		(*a)++;
	}

}

void insertie(int v[], int n, int* a, int* c)
{
	int x,j;
	for(i=2;i<=n;i++)
	{
		x = v[i];
		(*a)++;
		j = 0;
		while(v[j] < x)
		{
			(*c)++;
			j++;
		}
		for(k=i-1; k > j+1; k--)
		{
			v[k] = v[k-1];
			v[j] = x;
			(*a)=(*a)+2;
		}
	}
}

void main()
{
	FILE *best = fopen("best.csv", "w");
	FILE *worst= fopen("worst.csv", "w");
	FILE *average = fopen("average.csv", "w");

	int a=0, c=0, n,nr;
	fprintf(best, "n, AtrBubble, CompBubble, SumaBubble, AtrIns, CompIns, SumaIns, AtrSel, CompSel, SumaSel \n");
	fprintf(worst, "n, AtrBubble, CompBubble, SumaBubble, AtrIns, CompIns, SumaIns, AtrSel, CompSel, SumaSel \n");
	fprintf(average, "n, AtrBubble, CompBubble, SumaBubble, AtrIns, CompIns, SumaIns, AtrSel, CompSel, SumaSel \n");
	
	for(n=100; n<10000; n+=100)	{
		for(int k=0; k<n; k++)
			v[k] = k;
        //cazul mediu statistic
		bubble(v,n,&a,&c);
		fprintf(best, "%d, %d, %d, %d, ", n, a, c, a+c);
		a=c=0;

		insertie(v,n,&a,&c);
		fprintf(best, "%d, %d, %d, ", a, c, a+c);
		a=c=0;

		selectie(v,n,&a,&c);
		fprintf(best, "%d, %d, %d \n", a, c, a+c);
		a=c=0;
	}

		//...........................................................
		
	for(n=100; n<10000; n+=100)	{
		int j;		
		j=n;
		
		for (int k=0; k<n; k++)
		{
			v[k]= j;
			j--;
		}//cazul defavorabil

		bubble(v,n,&a,&c);
		fprintf(worst, "%d, %d, %d, %d", n, a, c, a+c);
		a=c=0;

		insertie(v,n,&a,&c);
		fprintf(worst, ",%d, %d, %d", a, c, a+c);
		a=c=0;

		selectie(v,n,&a,&c);
		fprintf(worst, ",%d, %d, %d \n", a, c, a+c);
		a=c=0;
	}
		//............................................................
	for(n=100; n<10000; n+=100)	
	{
		unsigned int iseed = (unsigned int)time(NULL);
		srand (iseed); //pentru nr aleatoare
		int suma,sumc;

		nr=1;
		suma=sumc=0;
		while (nr <= 5)
		{
			a=c=0;
			for (int j=0; j<n; j++)
				v[j]=rand();
			bubble(v,n,&a,&c);

			suma=suma+a;
			sumc=sumc+c;

			nr++;
		} //cele 5 sortari la numere aleatoare

		//la fel  la urma
		a=suma/nr;
		c=sumc/nr;
		fprintf(average, "%d, %d, %d, %d", n, a, c, a+c);
		a=c=0;

		nr=1;
		suma=sumc=0;
		while (nr <= 5)
		{
			a=c=0;
			for (int j=0; j<n; j++)
				v[j]=rand();
			insertie(v,n,&a,&c);

			suma=suma+a;
			sumc=sumc+c;

			nr++;
		}
		a=suma/nr;
		c=sumc/nr;
		fprintf(average, ",%d, %d, %d", a, c, a+c);
		
		a=c=0;

		nr=1;
		suma=sumc=0;
		while (nr <= 5)
		{
			a=c=0;
			for (int j=0; j<n; j++)
				v[j]=rand();
			selectie(v,n,&a,&c);

			suma=suma+a;
			sumc=sumc+c;

			nr++;
		}
		a=suma/nr;
		c=sumc/nr;
		fprintf(average, ",%d, %d, %d \n", a, c, a+c);
		a=c=0;



	}
	fclose(best);
	fclose(worst);
	fclose(average);
	getch();

	
}