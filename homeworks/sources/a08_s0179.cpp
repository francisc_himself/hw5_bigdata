#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"
#include "stddef.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 8 - Disjoint Sets
/***************************************************/

Profiler profiler("DisjointTest");

typedef struct graph
{
	struct graph *next;
	int value;
}graphT;

int e;

typedef graphT* graphNode;

graphNode g[10001];

typedef struct setType
{
	struct setType *parent;
	int value;
	int rank;
}setT;

typedef setT* setElement;

setElement s[10001];

//Set operations

void makeSet(setElement x);
void unite(setElement x, setElement y);
void link(setElement x,setElement y);
setElement findSet(setElement x);

void initializeSet(int size)
{
	for(int i = 1; i<= size; i++)
	{
		s[i] = (setElement)malloc(sizeof(setT));
		s[i]->value = g[i]->value;
		s[i]->parent = NULL;
		s[i]->rank = 0;
	}
}

void makeSet(setElement x)
{
	profiler.countOperation("Total",e,1);
	x->parent = x;
	x->rank = 0;
}

void unite(setElement x, setElement y)
{
	profiler.countOperation("Total",e,1);
	link(findSet(x),findSet(y));
}

void link(setElement x, setElement y)
{
	if(x->rank > y->rank)
	{
		y->parent = x;
	}
	else
	{
		x->parent = y;
		if(x->rank == y->rank)
		{
			y->rank = y->rank + 1;
		}
	}
}

setElement findSet(setElement x)
{
	profiler.countOperation("Total",e,1);
	if(x != x->parent)
	{
		x->parent = findSet(x->parent);
	}
	return x->parent;
}

void connectedComponents(int size)
{
	for(int i = 1; i <= size; i++)
	{
		makeSet(s[i]);
	}
	for(int i = 1; i <= size; i++)
	{
		setElement u = s[i];
		graphNode aux = g[i]->next;
		while(aux != NULL)
		{
			setElement v = s[aux->value];
			if(findSet(u) != findSet(v))
			{
				unite(u,v);
			}
			aux = aux->next;
		}
	}
}

bool sameComponent(setElement u, setElement v)
{
	if(findSet(u) == findSet(v))
	{
		return true;
	}
	else
		return false;
}

void generateGraph(int edgeNumber, int vertexNumber)
{
	int i = 1;
	while(i <= edgeNumber)
	{
		bool found = false;
		int start = rand() % vertexNumber +1;
		int end = rand() % vertexNumber +1;
		if(start!=end)
		{
		graphNode aux = g[start];
		while(aux->next != NULL)
		{
			if(aux->next->value == end)
			{
				found = true;
			}
			aux = aux->next;
		}
		if(!found)
		{
			aux->next = (graphNode)malloc(sizeof(graphT));
			aux->next->value = end;
			aux->next->next = NULL;
			i++;
		}
		}
	}
}

void initializeGraph(int vertexNumber)
{
	for(int i = 1; i <= vertexNumber; i++)
	{
		g[i]->next = NULL;
	}
}

void main()
{
	/*
	//PROVING CORRECTNESS

	int size = 10;
	for(int i = 1; i<= size; i++)
	{
		g[i] = (graphNode)malloc(sizeof(graphT));
		g[i]->value = i;
		g[i]->next = NULL;
	}
	initializeSet(10);
	initializeGraph(10);
	generateGraph(5,10);
	connectedComponents(size);
	for(int i = 1; i<= size; i++)
	{
		graphNode aux = g[i];
		while(aux != NULL)
		{
			printf("%d ->",aux->value);
			aux = aux->next;
		}
		printf("\n");
	}
	int node1,node2;
	printf("\n\nfive tests follow\n");
	for(int i = 1; i <= 5; i++)
	{
	printf("Input two nodes: ");
	scanf("%d %d",&node1,&node2);
	if(sameComponent(s[node1],s[node2]))
	{
		printf("Elements are in same component\n");
	}
	else
	{
		printf("Elements are not in same component\n");
	}
	}
	getch();
	*/

	
	//TESTING

	int size = 10000;

	for(int i = 1; i<= size; i++)
	{
		g[i] = (graphNode)malloc(sizeof(graphT));
		g[i]->value = i;
		g[i]->next = NULL;
	}
	for(e = 10000; e <= 60000; e+= 1000)
	{
		initializeSet(size);
		initializeGraph(size);
		generateGraph(e,size);
		connectedComponents(size);
		printf("%d\n",e);
	}
	profiler.showReport();
}