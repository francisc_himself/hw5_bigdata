
    /*
    Nume Prenume : ***ANONIM*** ***ANONIM*** Florin
    ***ANONIM*** : ***GROUP_NUMBER*** CTI 
    OBS : Algoritmul are o complexitate de : O(nlogk)
    */

    #include <stdio.h>
    #include <stdlib.h>
    #include <conio.h> 
    #include <time.h>





    int nra,nrc;	//nr atribuiri, comparatii 

    
    struct element {
        int v;		//valoare
        int i;		//index
    };

    typedef struct nod{
        int cheie;	
        struct nod *urm;
    }nodul;

    //lista
    void create_free_list (nodul **prim, nodul **ultim)
    {
        *prim = 0;
        *ultim = 0;
    }

    void inserare_sfarsit (nodul **prim, nodul **ultim, int k)
    {
        nodul *p;
        p = (nodul *) malloc(sizeof(nodul));
        p->cheie = k;

        if (*prim == 0)
        {
            *prim = p;
            *ultim = p;
        }
        else
        {
            (*ultim)->urm = p;
            *ultim = p;
        }
        p->urm = 0;
    }

    nodul* sterge_primul_nod (nodul **prim, nodul **ultim)
    {
        nodul *p;
        
        if (*prim == 0)
        {
            return NULL;
        }

        p = *prim;
        *prim = (*prim)->urm;
        return p;
    }

    void print_list (nodul *prim)
    {
        nodul *p;
        p = (nodul *) malloc(sizeof(nodul));
        
        if (prim==0)
            printf ("Lista e vida!");
        else
        {
            p = prim;
            while (p!=0)
            {
                printf ("%d ",p->cheie);
                p = p->urm;
            }
        }
        printf ("\n");
    }

    //heap
    int st (int i)
    {
        return 2*i;
    }

    int dr (int i)
    {
        return 2*i+1;
    }

    int parinte (int i)
    {
        return i/2;
    }

    void initHeap (element H[10001],int *dim_heap)
    {
        *dim_heap = 0;
    }

    void Hpush (element a[10001], int *dim_a, int valoare, int index)
    {
        int i;
        element aux;

        *dim_a = *dim_a + 1;
        nra+=1;
        a[*dim_a].i = index;
        a[*dim_a].v = valoare;
        i = *dim_a;
        while (i>1 && a[i].v < a[parinte(i)].v)
        {
            nrc++;
            nra+=3;
            aux = a[i];
            a[i] = a[parinte(i)];
            a[parinte(i)] = aux;

            i = parinte (i);
        }
        nrc++;
    }

    void reconstrHeap (element a[10001],int dim_heap, int i)
    {
        int s,d,ind;
        element aux;

        s = st (i);
        d = dr (i);

        ind = i;

        nrc++;
        if (s<=dim_heap && a[s].v<a[ind].v)
            ind = s;

        nrc++;
        if (d<=dim_heap && a[d].v<a[ind].v)
                ind = d;

        if (ind != i)
        {
            nra+=3;
            aux = a[i];
            a[i] = a[ind];
            a[ind] = aux;

            reconstrHeap (a,dim_heap,ind);
        }	
    }


    element Hpop (element a[10001], int *dim_a)
    {
        element e;
        if (*dim_a > 0)
        {
            nra+=2;
            e = a[1];
            a[1] = a[*dim_a];
            *dim_a = *dim_a -1;
            reconstrHeap (a,*dim_a,1);
        }
        return e;
    }

    //interclasare
    void interclasare (nodul* pcap[10001],nodul* ucap[10001], int k,nodul **pout,nodul **uout,element H[10001], int *dim_heap)
    {
        initHeap (H,dim_heap);
        create_free_list (pout,uout);
        nodul* p;
        p=(nodul *) malloc(sizeof(nodul));
        
        for (int i=1; i<=k; i++)
        {
            nrc++;
            if (pcap[i]!=NULL)
            {
                nra++;
                p = sterge_primul_nod (&(pcap[i]),&(ucap[i]));
            }
            else
                p = NULL;
            nrc++;
            if (p != NULL)
                Hpush (H,dim_heap,p->cheie,i);
        }

        while (*dim_heap > 0)
        {
            nra++;
            element e = Hpop (H,dim_heap);
            inserare_sfarsit (pout,uout,e.v);
            nodul *q;
            nra++;
            q = sterge_primul_nod (&(pcap[e.i]),&(ucap[e.i]));
            nrc++;
            if (q != NULL)
                Hpush (H,dim_heap,q->cheie,e.i);
        }
    }

    void FirstEx ()
    {
        int n,k;
        int j,l,val,c;	// 
        nodul *prim[10001],*ultim[10001];		//listele cu elementele prim si ultim
        nodul *pout,*uout;
        element H[10001];
        int dim_heap;
        srand(time(NULL));

        FILE *pf5,*pf10,*pf100;

        pf5 = fopen("cazul_k5.txt","w");
        pf10 = fopen("cazul_k10.txt","w");
        pf100 = fopen("cazul_k100.txt","w");

        // k1 = 5
        k = 5;
        
        while (k<=100)
        {
            //generam k liste ca caror dimensiuni insumate e n
            for (n=100; n<=10000; n+=100)
            {
                nra = nrc = 0;
                //generam lungimea listelor
                l = n/k;
                if (l * k == n)
                {
                    //avem k liste de lungime egala
                    for (j=1;j<=k;j++)
                    {		
                        create_free_list (&prim[j],&ultim[j]);
                        
                        //generam primul element
                        val = rand () %100 +1;

                        inserare_sfarsit (&prim[j],&ultim[j],val);

                        //generam celelalte elemente
                        for (c=2;c<=l;c++)
                        {
                            val = val + rand()%10 + 1;
                            inserare_sfarsit (&prim[j],&ultim[j],val);
                        }
                    }
                }
                else
                {
                    if (l != 0)
                    {
                        //generam k-1 liste de lungime egala l si o lista de lungime mai mare
                        for (j=1;j<k;j++)
                        {
                            create_free_list (&prim[j],&ultim[j]);
                        
                            
                            val = rand () %100 +1;
                            inserare_sfarsit (&prim[j],&ultim[j],val);

                            for (c=2;c<=l;c++)
                            {
                                val = val + rand()%10 + 1;
                                inserare_sfarsit (&prim[j],&ultim[j],val);
                            }
                        }
                        //lungimea ultimei liste
                        l = n- (k-1) * l;

                        create_free_list (&prim[k],&ultim[k]);
                        
                  
                        val = rand () %100 +1;
                        inserare_sfarsit (&prim[k],&ultim[k],val);

                    
                        for (c=2;c<=l;c++)
                        {
                            val = val + rand()%10 + 1;
                            inserare_sfarsit (&prim[j],&ultim[j],val);
                        }
                    }
                    else
                    {
                        for (j=1;j<=k;j++)
                        {
                            create_free_list (&prim[j],&ultim[j]);
                        }

                        for (c=1; c<=n; c++)		//cat mai avem elemente punem cate unul in fiecare lista
                        {
                            val = rand () %100 +1;
                            inserare_sfarsit (&prim[c],&ultim[c],val);
                        }
                    }
                }
                
                
                interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
                switch (k)
                {
                    ***ANONIM*** 5:
                            fprintf (pf5,"%d %d %d\n",k,n,nra+nrc);
                            break;
                    ***ANONIM*** 10:
                            fprintf (pf10,"%d %d %d\n",k,n,nra+nrc);
                            break;
                    ***ANONIM*** 100:
                            fprintf (pf100,"%d %d %d\n",k,n,nra+nrc);
                            break;
                }
            }
            if (k == 5)
                k = 10;
            else 
            {
                if (k == 10)
                    k = 100;
                else
                    k = 101;
            }	
        }
			
    }

    void SecondEx ()
    {
        int n,k;
        int j,l,val,c;	//l = lungimea unei liste; lultima = lungimea ultimei liste
        nodul *prim[10001],*ultim[10001];		//listele cu elementele prim su ultim
        nodul *pout,*uout;
        element H[10001];
        int dim_heap;
        srand(time(NULL));

        FILE *pf;

        pf = fopen("cazul_n_10000.txt","w");
        
        n = 10000;

        for (k=10; k<=500; k+=10)
        {
            //generam listele
            nra = nrc = 0;
            //generam lungimea listelor
            l = n/k;
            if (l * k == n)
            {
                //avem k liste de lungime egala
                for (j=1;j<=k;j++)
                {		
                    create_free_list (&prim[j],&ultim[j]);
                        
                    //generam primul element
                    val = rand () %100 +1;

                    inserare_sfarsit (&prim[j],&ultim[j],val);

                    //generam celelalte elemente
                    for (c=2;c<=l;c++)
                    {
                        val = val + rand()%10 + 1;
                        inserare_sfarsit (&prim[j],&ultim[j],val);
                    }
                }
            }
            else
            {
                if (l != 0)
                {
                    //generam k-1 liste de lungime egala l si o lista de lungime mai mare
                    for (j=1;j<k;j++)
                    {
                        create_free_list (&prim[j],&ultim[j]);
                        
                        //generam primul element
                        val = rand () %100 +1;
                        inserare_sfarsit (&prim[j],&ultim[j],val);

                        //generam celelalte elemente
                        for (c=2;c<=l;c++)
                        {
                            val = val + rand()%10 + 1;
                            inserare_sfarsit (&prim[j],&ultim[j],val);
                        }
                    }
                    //lungimea ultimei liste
                    l = n- (k-1) * l;

                    create_free_list (&prim[k],&ultim[k]);
                        
                    //generam primul element
                    val = rand () %100 +1;
                    inserare_sfarsit (&prim[k],&ultim[k],val);

                    //generam celelalte elemente
                    for (c=2;c<=l;c++)
                    {
                        val = val + rand()%10 + 1;
                        inserare_sfarsit (&prim[j],&ultim[j],val);
                    }
                }
                else
                {
                    for (j=1;j<=k;j++)
                    {
                        create_free_list (&prim[j],&ultim[j]);
                    }

                    for (c=1; c<=n; c++)		//cat mai avem elemente punem cate unul in fiecare lista
                    {
                        val = rand () %100 +1;
                        inserare_sfarsit (&prim[c],&ultim[c],val);
                    }
                }
            }
                
            
            interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
            
            fprintf (pf,"%d %d %d\n",k,n,nra+nrc);
        }
    }


    int main ()
    {
        //FirstEx ();
        //SecondEx();
		int n,k;
        k=4;
        n=20;
        nodul *prim[5],*ultim[5];
        nodul *pout,*uout;
        element H[10001];
        int dim_heap;

        create_free_list (&prim[1],&ultim[1]);
        inserare_sfarsit (&prim[1],&ultim[1],2);
        inserare_sfarsit (&prim[1],&ultim[1],7);
        inserare_sfarsit (&prim[1],&ultim[1],14);
		inserare_sfarsit (&prim[1],&ultim[1],21);
		inserare_sfarsit (&prim[1],&ultim[1],25);
        
        print_list (prim[1]);


        create_free_list (&prim[2],&ultim[2]);
        inserare_sfarsit (&prim[2],&ultim[2],3);
        inserare_sfarsit (&prim[2],&ultim[2],4);
        inserare_sfarsit (&prim[2],&ultim[2],6);
		inserare_sfarsit (&prim[2],&ultim[2],9);
        inserare_sfarsit (&prim[2],&ultim[2],12);
        inserare_sfarsit (&prim[2],&ultim[2],22);


        print_list (prim[2]);

        create_free_list (&prim[3],&ultim[3]);
        inserare_sfarsit (&prim[3],&ultim[3],1);
        inserare_sfarsit (&prim[3],&ultim[3],8);
		inserare_sfarsit (&prim[3],&ultim[3],12);
        inserare_sfarsit (&prim[3],&ultim[3],24);

        print_list (prim[3]);

		create_free_list (&prim[4],&ultim[4]);
        inserare_sfarsit (&prim[4],&ultim[4],2);
        inserare_sfarsit (&prim[4],&ultim[4],16);
        inserare_sfarsit (&prim[4],&ultim[4],23);
		inserare_sfarsit (&prim[4],&ultim[4],27);
		inserare_sfarsit (&prim[4],&ultim[4],29);
        
        print_list (prim[4]);



        interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
        printf ("Lista rezultata:\n");
        print_list (pout);
        getche ();
        return 0;
    }