/*
***ANONIM*** Vasile-***ANONIM***, grupa ***GROUP_NUMBER***
Functia make_set() are eficienta O(1); functia union() are eficienta O(n).
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>

typedef struct nod{
	int k,rank;
	struct nod *urm,*ref;
}NOD;

NOD *sir[60000];
int nr_union=0,nr_find=0,nr_make=0;

NOD *make_set(int x)
{
	NOD *p=(NOD*)malloc(sizeof(NOD));
	p->k=x;
	p->rank=0;
	p->ref=p;
	p->urm=0;
	return p;
}

void link(NOD *x,NOD *y)
{
	if(x->rank>y->rank)
		y->ref=x;
	else
	{
		x->ref=y;
		if(x->rank==y->rank)
			y->rank+=1;
	}
}

NOD *find_set(NOD *x)
{
	nr_find++;
	if(x!=x->ref)
		x->ref=find_set(x->ref);
	return x->ref;
}

void Union(NOD *x,NOD *y)
{
	if(find_set(x)!=find_set(y))
	{
		link(find_set(x),find_set(y));
		nr_union++;
	}
}


void con_comp()
{

}


int a[10000][10000];

int main()
{
	int vf=10000,m;
	int i,j;
	FILE *f;

	f=fopen("disj.csv","w");
	fprintf(f,"muchii,nr_make,nr_union,nr_find\n");

	for(m=10000;m<=60000;m+=10000)
	{	
		nr_make=0;
		nr_union=0;
		nr_find=0;
		for(i=0;i<vf;i++)
		{
			sir[i]=make_set(i);
			nr_make++;
		}

		for(i=0;i<vf;i++)
		{
			for(j=0;j<vf;j++)
				a[i][j]=0;
		}

		for(int k=0;k<m;k++)
		{
			i=rand()%vf;
			j=rand()%vf;
			if((i<j)&&(a[i][j]==0))
			{		
				a[i][j]=1;
				a[j][i]=1;
				Union(sir[i],sir[j]);
			}
			else k--;
		}
		fprintf(f,"%d,%d,%d,%d\n",m,nr_make,nr_union,nr_find);
	}
	/*	for(i=0;i<vf;i++)
	{
	printf(" %d->%d ",i,find_set(sir[i])->k);
	printf("\n");
	}
	system("pause");*/

}