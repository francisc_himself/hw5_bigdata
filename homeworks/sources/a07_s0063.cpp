#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int *b;
typedef struct nodeM{
	int val;
	nodeM* children;
	int nChild;
}NodeMT;
typedef struct nodeB{
	int val;
	nodeB* child;
	nodeB* brother;
}NodeBT;
void MultiToBinary(NodeMT* nodMParent,NodeMT* nodM, NodeBT* nodB ){
	NodeMT*aux;
	nodB -> val = nodM -> val;
	nodB ->brother = NULL;
	nodB ->child = NULL;
	if (nodM->children!=NULL){
		nodB->child = (NodeBT*) malloc(sizeof(NodeBT));
		nodM->nChild++;
		MultiToBinary(nodM,&nodM->children[0], nodB->child);
	}

	if (nodMParent->children !=NULL){
	aux = &nodMParent->children[nodMParent->nChild];

		if (aux->val!=-1){
			nodMParent->nChild++;
			nodB->brother = (NodeBT*) malloc(sizeof(NodeBT));
			MultiToBinary(nodMParent,aux,nodB->brother);
			}	
	}
}
void preetyPrint(NodeBT*  nod, int h){
	if (nod!=NULL){
		for(int i=0;i<h;i++)
			printf("   ");
		printf("%d\n",nod->val);
		preetyPrint(nod->child,h+1);
		preetyPrint(nod->brother,h);
	}
}
void main(){
	int a[] = {1, 6, 4, 1, 6, 6, -1, 4, 1};
	int size = sizeof(a)/sizeof(int);
	b = (int*) malloc((size+1) * sizeof(int));
	for (int i=-1;i<size;i++)
		b[i] = 0;
	for (int i=0;i<size;i++){
		b[a[i]]++;
	}
	NodeMT* n = (NodeMT*) malloc (size*sizeof(NodeMT));
	NodeMT* rootM;
	for (int i = 0;i<size;i++){
		n[i].val = i;
		n[i].nChild = 0;
		if (b[i] != 0){
		n[i].children = (NodeMT*) malloc((b[i]+1)*sizeof(NodeMT));
		n[i].children[b[i]].val = -1;
		b[i] = 0;
		}
		else
			n[i].children = NULL;
	}
	for (int i = 0;i<size;i++){
		if (a[i] == -1)
			rootM = &n[i];
		else
		{
			n[a[i]].children[b[a[i]]] = n[i];
			(b[a[i]])++;
		}
	}
	printf("\n");
	NodeBT* rootB =(NodeBT*) malloc(sizeof(NodeBT));
	rootB->val = rootM->val;
	rootB->child = (NodeBT*) malloc(sizeof(NodeBT));
	rootB->brother = NULL;
	rootM->nChild++;
	MultiToBinary(rootM,&rootM->children[0],rootB->child);
	preetyPrint(rootB,0);
	getch();
}