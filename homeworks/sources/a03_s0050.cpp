
#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* ***ANONIM*** ***ANONIM***  grupa ***GROUP_NUMBER***
		Algoritmul de sortare quick-sort, intr-un caz mediu statistic, este cel mai efiecient dintre toate celelalte metode de sortare avand efiecienta egala cu O(n*logn),
	acest lucru se observa si din graficul in care se compara cu heap-sort-ul.
		Quick-sort-ul in cazul favorabil se apropie de eficienta  O(n) atunci cand alegem ca pivot mijlocul intervalului in care sunt cuprinse elementele tabloului sortat anterior descrescator.
		Cazul cel mai defavorabil pentru quick-sort este atunci cand alegem ca pivot unul din capetele unui tablou sortat anterior, avand eficienta aproximativ egala cu O(n^2);
*/
Profiler profiler("demo");
#define Iinfinit 10000

int lungime; 
int size=0;
void afisare(int* v, int n,int k, int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
	for(int i=0;i<nivel;i++) printf("  ");
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);
}
int pr(int x){return x/2;}
int dr(int x){return 2*x+1;}
int st(int x){return 2*x;}

void reconstructieHeap(int* A,int i, int n)
{
	int max;
	int s=st(i);
	int d=dr(i);
	if((s<=n)&&A[s]>A[i]) max=s;
	else max=i;
	if((d<=n)&&(A[d]>A[max])) max=d;
	profiler.countOperation("buComp",lungime,2);
	if (max != i) {int aux=A[i];
	A[i]=A[max];
	A[max]=aux;
	reconstructieHeap(A,max,n);
	profiler.countOperation("buAssign",lungime,3);
	}

}
void constructieHeap(int* A,int n)
{
	for(int i=n/2;i>=1;i--)
		reconstructieHeap(A,i,n);

}

void heapsortBU(int* A,int n)
{
	constructieHeap(A,n);
	for (int i=n;i>=2;i--)
	{
		int aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		n--;
		profiler.countOperation("buAssign",lungime,3);

		reconstructieHeap(A,1,n);
	}
}
int partitionare(int* A,int st,int dr,int n)
{
	//int x=A[n/2];
	int x=A[dr];
	profiler.countOperation("quickAssign",lungime,1);
	int i=st-1;
	//int j=dr+1;
	for(int j=st;j<=dr-1;j++)
	{	profiler.countOperation("quickComp",lungime,1);
	if(A[j]<=x){
		i++;

		int aux=A[i];
		A[i]=A[j];
		A[j]=aux;
		profiler.countOperation("quickAssign",lungime,3);
	}
	}
	int aux=A[i+1];
	A[i+1]=A[dr];
	A[dr]=aux;
	profiler.countOperation("quickAssign",lungime,3);
	return i+1;
}





void QSort(int* A,int st,int dr,int n)
{	int m;
if(st<dr)
{
	m=partitionare(A,st,dr,n);
	QSort(A,st,m-1,n);
	QSort(A,m+1,dr,n);
}
}
//mijlocul intervalului cu apelare recursiva..
void qsort2(int* A,int st,int dr)
{
   int aux,i,j,pivot;

   pivot = A[(st+dr)/2];     
   profiler.countOperation("quickAssign",lungime,1);
   i = st; j = dr;
   while (i<=j)
   {
	    profiler.countOperation("quickComp",lungime,2);
	while(A[i] < pivot) {i++; 
						profiler.countOperation("quickComp",lungime,1);
						}
		while(A[j] > pivot) {j--;
		profiler.countOperation("quickComp",lungime,1);
				}
        if(i <= j)                     
        {
            aux = A[i];
            A[i] = A[j];
            A[j] = aux;
		profiler.countOperation("quickAssign",lungime,3);
		i++; j--;
        }
    }
    
  if(st < j) qsort2(A,st,j);  
  if(i < dr) qsort2(A,i,dr);  
}

void main()
{

	int vect[]={0,4,1,3,2,16,9,10,14,8,7};
	int vect2[]={0,6,5,3,1,8,7,2,4};
	//vect2=vect;
	//n din 100 + > 500;
	afisare(vect,10,1,0);
	//heapsortBU(vect,4);
	//constructieHeap(vect,10);
	//afisare(vect,10,1,0);
	//heapsortBU(vect,10);
	//afisare(vect,10,1,0);

	printf("quick\n\n");
	qsort2(vect,1,10);
	afisare(vect,10,1,0);
	//QSort(vect,1,11);
	///top-down

	int n;
	int v[10000], v1[10000];

	int choose;
	printf("Alege: 0-Random; 1-Worst; 2-Best 3-Exit\n");
	scanf("%d",&choose);
	
	while(choose!=3)
	{
		
	switch(choose)
	{case 0:
	for(int testt=0;testt<5;testt++)
		{
			for(n=100; n<2000; n+=100)
			{
			lungime=n;
			printf("n=%d\n", n);
			FillRandomArray(v,n);//fillrandomarray(v,n);
			memcpy(v1, v, n * sizeof(int));
			heapsortBU(v1,n);
			memcpy(v1, v, n * sizeof(int));
			QSort(v1,1,n-1,n-1);
			
			}
	}
	profiler.addSeries("buSerie","buComp","buAssign");
	profiler.addSeries("quickSerie","quickComp","quickAssign");
	profiler.createGroup("DiferentaSerii","buSerie","quickSerie");
	/*profiler.createGroup("Bottom-Up","buAssign","buComp");
	profiler.createGroup("Top-Down","tdAssign","tdComp");*/
	profiler.createGroup("ALL","quickAssign","quickComp","buAssign","buComp");	
	profiler.showReport();
	break;
	
	////worst case
	case 1:
		
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
			lungime=n;
			printf("n=%d\n", n);
			FillRandomArray(v,n,1,50000,false,2);//fillrandomarray(v,n);
			/*memcpy(v1, v, (n) * sizeof(int));
			heapsortBU(v1,n);*/
			memcpy(v1, v, (n) * sizeof(int));
			QSort(v1,0,n-1,n);

		}
	}
	profiler.addSeries("buSerie","buComp","buAssign");
	profiler.addSeries("quickSerie","quickComp","quickAssign");
	profiler.createGroup("DiferentaSerii","buSerie","quickSerie");
	/*profiler.createGroup("Bottom-Up","buAssign","buComp");
	profiler.createGroup("Top-Down","tdAssign","tdComp");*/
	profiler.createGroup("ALL","quickAssign","quickComp","buAssign","buComp");	
	profiler.showReport();
	break;
		////best
	case 2:
		
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
			lungime=n;
			printf("n=%d\n", n);
			FillRandomArray(v,n,1,50000,false,2);//fillrandomarray(v,n);
		/*	memcpy(v1, v, (n) * sizeof(int));
			heapsortBU(v1,n);*/
			memcpy(v1, v, (n) * sizeof(int));
			qsort2(v1,0,n-1);

		}
	}
	profiler.addSeries("buSerie","buComp","buAssign");
	profiler.addSeries("quickSerie","quickComp","quickAssign");
	profiler.createGroup("DiferentaSerii","buSerie","quickSerie");
	/*profiler.createGroup("Bottom-Up","buAssign","buComp");
	profiler.createGroup("Top-Down","tdAssign","tdComp");*/
	profiler.createGroup("ALL","quickAssign","quickComp","buAssign","buComp");	
	profiler.showReport();
	break;
	
	}
	printf("Alege: 0-Random; 1-Worst; 2-Best; 3-Exit\n ");
	scanf("%d",&choose);
	
	
	}
}