/****ANONIM*** grupa ***GROUP_NUMBER***
	Multimea disjuncta este un set de structuri de date care tine evidenta unui set de elemente partitionate intr-un numar de submultimi disjuncte.
	Am aplicat trei functii asupra nodurilor:Make_Set,Find_Set si Union,din care Make_Set si Union,in care Union si Make_Set au o complexitate de 
	O(1),deoarece nu au o functie repetitiva pentru a le creste timpul,iar Find_Set are o complexitate O(logn),deoarece gasim reprezentatul
	fiecarui nod(radacina) si se apeleaza recursiv.Astfel complexitatea intergului algoritm este de O(nlogn),prin generarea de componente conexe.
	Consider ca am intampinat anumite greutati in pargurgerea intregului program,avand mici greseli care impiedica programul sa functioneze corect.
*/#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"
Profiler profiler("demo");
#define V_MAX 10000
#define E_MAX 60000
typedef struct nod{
	int rank;
	int val;
	struct nod*p;
}nod;
typedef struct gr{
	int nrMuchii;
	int nrNoduri;
}gr;
int lung;
nod nodes[V_MAX];
int edges[E_MAX];
void Make_Set(nod*x,int i){
	//profiler.countOperation("Op",lung);
	lung++;
	nod*aux=(nod*)malloc(sizeof(nod));
	aux=x;
	x->p=aux;;
	x->rank=0;
	x->val=i;
}
void Link(nod*x,nod*y){
	if (x->rank>y->rank) y->p=x;
	else{ x->p=y;
		  if(x->rank==y->rank)
				y->rank=y->rank+1;
	}
}
nod*Find_Set(nod*x){
	//printf("%d\n",x->val);
	//profiler.countOperation("Op",lung);
	lung++;
	if(x!=x->p)
		x->p=Find_Set(x->p);
	return x->p;
}
void Union(nod*x,nod*y){
	//profiler.countOperation("Op",lung);
	lung++;
	Link(Find_Set(x),Find_Set(y));
}
void generate(int n,int m){
	//printf("%d",n);

	FillRandomArray(edges,m,0,n*n-1,true);
}
void Comp_conec(gr*gr){
	for(int i=0;i<=gr->nrNoduri;i++)
		Make_Set(&nodes[i],i);
	generate(gr->nrNoduri,gr->nrMuchii);
	for(int i=0;i<=gr->nrMuchii;i++)
	{
			int a=edges[i]/gr->nrNoduri;
			int b=edges[i]%gr->nrNoduri;
			printf("%d %d\n",a,b);
			if(Find_Set(&nodes[a])!=Find_Set(&nodes[b]))
				Union(&nodes[a],&nodes[b]);
	}
}
void main(){
	gr* g=(gr*)malloc(sizeof(gr));
	//g->nrNoduri=3;
	//g->nrMuchii=5;
	////Comp_conec(g);
	FILE *fdi;
	fdi=fopen("afis.csv","w");
	fprintf(fdi,"NrMuchii ; Operatii\n");
	int n=V_MAX;
	for(int i=n;i<=E_MAX;i+=1000){

		printf("i=%d\n",i);
		g->nrMuchii=i;
		g->nrNoduri=V_MAX;
	//	printf("m=%d  n=%d\n",g->nrMuchii,g->nrNoduri);
		lung=i;
		//printf("lung=%d\n",lung);
		Comp_conec(g);
	//	printf("lung=%d\n",lung);
		fprintf(fdi,"%d ; %d\n",i,lung);
	}
	//profiler.showReport();
}