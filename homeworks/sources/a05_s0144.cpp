#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define hash_size 9973


/*Interpretari: 
In ipoteza dispersiei uniforme, o tabela de dispersie cu adresare directa si factorul de umplere alfa=n/m <1,
 nr. mediu de modificari intr-o cautare fara succes este e<1/(1-alfa), iar pt o cautare cu succes e<1/alfa ln 1/(1-alfa)
  Consideram alfa={0.8,0.85,0.90,0.95,0.99}
  Pe masura ce alfa creste, efortul depus e mult mai mare
  De asemenea, in urma rularilor am observat: (timpul de cautare constant)
  -pt un factor de umplere=90% -> nr. de cautari: 2%3
  -pt un factor de umplere=99% -> nr de cautari creste si poate ajunge pana la 5 
  */

long hash[hash_size];
long test[3000];
int total=0;


/*initializam toate elementele cu null (-1) **/

void init ()
{
    int i;
    for (i=0;i<hash_size;i++)
    {
        hash[i]=-1;
    }
}


/* cream functia de hash*/
int hashFunction (long number,int n)
{
    return (number%hash_size + n + n*n)%hash_size;
}

/* inserarea in tabel: aplicam functia de hash si apoi inseram elementul in tabel*/
void insertTable (long key)
{
    int i=0,j,ok=0;
    do
    {
        j=hashFunction(key,i);
        if (hash[j]==-1)
        {
            total++;
			hash[j]=key;
            ok=1;
        }else i++;
    }
	while (i<hash_size && ok==0);
    if (j>hash_size) 
		printf ("Error: hash table overflow");
}


/* cautam in tabel si afisam un raspuns in consola*/ 
void searchTable (long key)
{
    int j;
    int i=0,ok=0;
    do
    {
        j=hashFunction(key,i);
        if (hash[j]==key)
        {
            total++;
            printf("yes");
			return;
        }
        i++;
        total++;
    }
   /* continuam sa cautam pana cand gasim un element sau i>hash_size */
    while (hash[j]!=-1 && i<=hash_size);
    if (ok==0) 
		printf("no");
}

int main()
{
	int i,nr=0,max,elem,x;
	long key;
	init();
	srand(time(NULL));

	FILE *f;

	f=fopen("hash.txt","w");

    //aici se pune alfa 0.8...pana la 0.99
	for (i=1; i<= (0.80 * hash_size) ; i++)
	{
		//generam cheia random si inseram in tabel
		key = rand()*RAND_MAX+rand();
		insertTable(key);
	}

    nr=1;
	/* mergem pana la jumatatea tabelului (1500), cautam pozitia x din tabel,random si daca nu este libera,
	in test[nr] punem elementul din hash, unde nr creste succesiv; pt restul pozitilor de la 1501 pana la 3000 punem valori random*/

    while (nr<=1500)
    {
        x=rand()%hash_size;
        if (hash[x]!=-1)
            {
                test[nr]=hash[x];
                nr++;
            }
    }

    for (i=1501;i<=3000;i++)
       test[i]=rand()*RAND_MAX+rand();

	total=0;max=0;

	for (i=1;i<=1500;i++)
	{
		elem=total;
		searchTable(test[i]);
		elem=total-elem;
		if (elem>max)
			max=elem;
	}

	fprintf(f,"avg.found: %d\n max.found: %d\n", total/1500,max);

	total=0;max=0;

	for (i=1501;i<=3000;i++)
	{
		elem=total;
		searchTable(test[i]);
		elem=total-elem;
		if (elem>max)
			max=elem;
	}

	fprintf(f,"avg. not found: %d\n max.not found: %d\n", total/1500,max);

fclose(f);
return 0;
}
