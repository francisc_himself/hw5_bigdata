/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

Acest cod are rolul de a implementa algoritmii eficienti pentru multimi si de a verifica eficienta lagoritmului de cautare a componentelor conexe 
ale unui graf cu ajutorul structurilor si algoritmilor pentru multimi.


*/
#include<iostream>
#include<stdlib.h>
#include<time.h>
#include "Profiler.h"
using namespace std;

#define NUMAR_VARFURI 10000

Profiler profiler("Apeluri pentru functiile pe multimi");
int nrMuchii;

//==============================================================
//types
typedef struct _SET_ELEMENT
{
	int Data;
	struct _SET_ELEMENT *Parent;
	int	   Rang;
}SET_ELEMENT;

typedef SET_ELEMENT *VARF;

typedef struct _MUCHIE
{
	int V1;
	int V2;
}MUCHIE;

typedef struct _GRAF
{
	int NrVarfuri;
	int NrMuchii;
	VARF *Varfuri;
	MUCHIE *Muchii;
}GRAF;
//=========================================================================

//=========================================================================
//set functions
void MakeSet(SET_ELEMENT *x)
{
	x->Parent = x;
	x->Rang = 0;
}

void Link(SET_ELEMENT *x, SET_ELEMENT *y)
{
	if(x->Rang > y->Rang)
	{
		y->Parent = x;
	}
	else
	{
		x->Parent = y;
		if(x->Rang = y->Rang)
		{
			y->Rang++;
		}
	}
}

SET_ELEMENT* FindSet(SET_ELEMENT *x)
{
	if(x != x->Parent)
	{
		profiler.countOperation("Nr_Operatii",nrMuchii,1);
		profiler.countOperation("Calls_FindSet",nrMuchii,1);
		x->Parent = FindSet(x->Parent);
	}
	return x->Parent;
}

void Union(SET_ELEMENT *x, SET_ELEMENT *y)
{
	profiler.countOperation("Nr_Operatii",nrMuchii,2);

	profiler.countOperation("Calls_FindSet",nrMuchii,2);
	Link(FindSet(x),FindSet(y));
}
//=========================================================================

//=========================================================================
//graf functions
void MakeGraph(GRAF **G, int nrVarfuri, int nrMuchii, MUCHIE *Muchii)
{
	*G  = new GRAF;
	(*G )-> NrVarfuri = nrVarfuri;
	(*G )-> NrMuchii = nrMuchii;
	(*G )-> Varfuri = (VARF *) malloc(nrVarfuri*sizeof(VARF));
	(*G )-> Muchii = (MUCHIE *)malloc(nrMuchii*sizeof(MUCHIE));
	
	for(int i =0 ;i<nrVarfuri;i++)
	{
		(*G )->Varfuri[i] = (VARF)malloc(sizeof(SET_ELEMENT));
		(*G )->Varfuri[i]->Data = i;
		(*G )->Varfuri[i]->Rang = 0;
	}

	for(int i = 0;i< nrMuchii;i++)
	{
			(*G )-> Muchii[i] = Muchii[i]; 
	}
	
}

void ConnectedComponents(GRAF G)
{
	int i;
	for (i = 0;i< G.NrVarfuri;i++)
	{
		profiler.countOperation("Nr_Operatii",nrMuchii,1);
		profiler.countOperation("Calls_Make-set",nrMuchii,1);
		MakeSet(G.Varfuri[i]);	
	}
	for(i =0;i < G.NrMuchii;i++)
	{
		profiler.countOperation("Nr_Operatii",nrMuchii,2);
		profiler.countOperation("Calls_FindSet",nrMuchii,2);
		if(FindSet(G.Varfuri[G.Muchii[i].V1]) != FindSet(G.Varfuri[G.Muchii[i].V2]))
		{
			profiler.countOperation("Calls_Union",nrMuchii,1);
			Union(G.Varfuri[G.Muchii[i].V1],G.Varfuri[G.Muchii[i].V2]);
		}
	}
}

bool SameComponent(VARF u, VARF v)
{
	if(FindSet(u)== FindSet(v))
	{
		return true;
	}
	return false;
}

//============================================================================================
//functii si variabile folosite pentru verificarea corectitudinii si construirea graficelor
void TesteazaCorectitidine()
{
	GRAF *G = new GRAF;
	MUCHIE M[] = {{0,1},{0,2},{1,2},{1,3},{3,4},{5,6}};
	
	/*
	Graful este:
			1	-	3			5
		/
	0		|		|			|			7
		\	
			2		4			6
	
	*/

	MakeGraph(&G,8,6,M);
	ConnectedComponents(*G);
	int componente[10];
	int numarComponente = 0;
	componente[0] = 0;

	for(int i=0;i<8-1;i++)
		for(int j=i+1;j<8;j++)
		{
			if(SameComponent(G->Varfuri[i],G->Varfuri[j]))
			{
				componente[j] = componente[i];
			}
			else 
			{
				numarComponente++;
				componente[j] = numarComponente;
			}
		}
	for(int i = 0; i<8;i++)
	{
		printf("varful %d se afla in componenta marcata cu %d\n", i, componente[i]);
	}
}

void ConstruiesteGrafice()
{
	MUCHIE muchii[60001];
	srand(time(NULL));
	bool gasit;
	for(nrMuchii = 10000;nrMuchii<=60000;nrMuchii+=100)
	{
		GRAF *G = new GRAF;
		cout<<"nrMuchii = "<<nrMuchii<<endl;
		for(int i = 0;i<nrMuchii;i++)
		{
			do
			{
				gasit = false;
				muchii[i].V1 = rand()%NUMAR_VARFURI;
				muchii[i].V2 = rand()%NUMAR_VARFURI;
				for(int j=0;j<i;j++)
				{
					if((muchii[j].V1==muchii[i].V1 && muchii[j].V2 == muchii[i].V2) || (muchii[j].V2==muchii[i].V1 && muchii[j].V1 == muchii[i].V2) || muchii[i].V1 == muchii[i].V2)
					{
						gasit = true;
					}
				}
			}
			while (gasit);
		}

		MakeGraph(&G,NUMAR_VARFURI,nrMuchii,muchii);
		ConnectedComponents(*G);
		delete G;
	}

	profiler.showReport();
}

int main()
{
	//TesteazaCorectitidine();
	ConstruiesteGrafice();
}