
/*
	Nume : ***ANONIM***-Cornel ***ANONIM***
	Grupa : ***GROUP_NUMBER***
	
	Concluzii : 
	
	Quicksort - caz mediu : O(N log N)
			  - caz devaforabil : O( N * N)
			  
	Heapsort - caz mediu : O(N log N)
			 - caz defavorabil : O(N log N)
	

*/



#include<iostream>
#include<conio.h>
#include  "Profiler.h"
#define MAX 10000
using namespace std;
Profiler profiler("Heapsort vs Quicksort");
Profiler profiler2("Quicksort Best-Worst");
int size,n;

//setez proprietatile de heap pentru setul de elemente
// i, 2i, 2i+1
void heapify(int A[],int i,int n){
	int left,right,largest,aux;
	int comp = 0;
	int assign = 0;
	left=2*i;
	right=2*i+1;
	comp++;
	if(left<=size && A[left]>A[i])
		{
			largest=left;
	}
	else{
		largest=i;
	}
	
	comp++;
	
	if(right<=size && A[right]>A[largest]){
		largest=right;
	}
	if(largest!=i){
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		assign+=3;
		heapify(A,largest,n);
	}
	profiler.countOperation("Atribuiri_Heapsort",n,assign);
	profiler.countOperation("Comparari_Heapsort",n, comp);
}

//Construieste heap maxim bottom-up
void BuildMaxHeap(int A[],int n){
	size=n;
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}

//Sortez cu HeapSort
void HeapSort(int A[], int n){
	int aux;
	int assign = 0;
	BuildMaxHeap(A,n);
	for(int i=n;i>=2;i--){
		aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		assign+=3;
		size--;
		heapify(A,1,n);
	}
	profiler.countOperation("Atribuiri_Heapsort",n,assign);
}


//Sortez cu QuickSort
void quickSort(int arr[], int left, int right) {
    int i = left, j = right;
	int assign = 0;
	int comp = 0;
    int tmp;
    int pivot = arr[(left + right) / 2];
	assign++;
	while (i <= j) 
	{	comp++;
		while (arr[i] < pivot)
		{	comp++;
			i++;
		}
		comp++;
		while (arr[j] > pivot)
		{	comp++;
			j--;
		}
        if (i <= j) 
		{	assign++;
			
            tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
	}
	if (left < j)
		quickSort(arr, left, j);
	if (i < right)
		quickSort(arr, i, right);
	profiler.countOperation("Atribuiri_Quicksort",n,assign);
	profiler.countOperation("Comparari_Quicksort",n,comp);
}

//Construiesc caz mediu
void averageCase()
{
	int a[MAX],b[MAX],c[MAX];
	int i,j;
	for(i=100;i<10000;i+=100)
	{
		FillRandomArray(a,i,0,10000);
		for(int k=1;k<=5;k++)
		{	
			n=i;
			for(j=1;j<=n;j++)
			{
				b[j]=a[j];
				c[j]=a[j];
			}
			quickSort(b,1,n);
			HeapSort(c,n);
		}
	}
	profiler.addSeries("Heapsort","Comparari_Heapsort","Atribuiri_Heapsort");
	profiler.addSeries("Quicksort","Comparari_Quicksort","Atribuiri_Quicksort");
	profiler.createGroup("Comparari","Comparari_Heapsort","Comparari_Quicksort");
	profiler.createGroup("Atribuiri","Atribuiri_Heapsort","Atribuiri_Quicksort");
	profiler.createGroup("Total","Heapsort","Quicksort");
	profiler.showReport();
}

//Construiesc caz favorabil pentru quicksort
void quickbest(int arr[], int left, int right) {
    int i = left, j = right;
    int tmp;
    int pivot = arr[(left + right) / 2];
	profiler2.countOperation("Atribuiri_Best",n);
    while (i <= j) 
	{
		profiler2.countOperation("Comparari_Best",n);
        while (arr[i] < pivot)
		{ 
			i++;
			profiler2.countOperation("Comparari_Best",n);
		}
		profiler2.countOperation("Comparari_Best",n);
        while (arr[j] > pivot)
		{
			j--;
			profiler2.countOperation("Comparari_Best",n);
		}
        if (i <= j) 
		{
			profiler2.countOperation("Atribuiri_Best",n,3);
            tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
	}
	if (left < j)
		quickbest(arr, left, j);
	if (i < right)
		quickbest(arr, i, right);
}

//Construiesc caz defavorabil pentru quicksort
void quickworst(int arr[], int left, int right) {
    int i = left, j = right;
    int tmp;
	int assign = 0;
	int comp = 0;
    int pivot = arr[left];
	assign++;
    while (i <= j) 
	{	comp++;
        while (arr[i] < pivot)
		{	comp++;
			i++;
		}
		comp++;
        while (arr[j] > pivot)
		{	comp++;
			j--;
		}
        if (i <= j) 
		{	assign+=3;
			tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
	}
	if (left < j)
		quickworst(arr, left, j);
	if (i < right)
		quickworst(arr, i, right);
	profiler2.countOperation("Atribuiri_Worst",n,assign);
	profiler2.countOperation("Comparari_Worst",n, comp);
}

//Comparatie intre Best si Worst
void best_and_worst(){
int a[MAX],b[MAX],c[MAX];
	int i,j;
	for(i=100;i<10000;i+=100)
	{
		FillRandomArray(a,i,0,10000,false,1);
		n=i;
		for(j=1;j<=i;j++)
			{
				b[j]=a[j];
				c[j]=a[j];
			}
		quickbest(b,1,n);
		quickworst(c,1,n);
	}
	
	profiler2.addSeries("QuickBest","Comparari_Best","Atribuiri_Best");
	profiler2.addSeries("QuickWorst","Comparari_Worst","Atribuiri_Worst");
	profiler2.createGroup("Comparison","Comparari_Best","Comparari_Worst");
	profiler2.createGroup("Assignments","Atribuiri_Best","Atribuirit_Worst");
	profiler2.createGroup("Best-vs-Worst","QuickBest","QuickWorst");
	profiler2.showReport();
}


void main(){

	averageCase();
	best_and_worst();
	
}