//***ANONIM*** Mariana ***ANONIM***, grupa ***GROUP_NUMBER***
//Prof. Vlad Baja
//Multi-way trees
//Implementarea algoritmului de reprezentare a unui arbore multi-cai si a unui arbore binar.
//Pentru prima transformare am folosit o structura care contine cheia nodului, numarul de descendenti si legaturi spre copii acestuia. Cea de a doua structura
//contine cheia nodului si legaturi spre copil, respectiv fratele nodului. Cele doua functii de transformare a arborelui, transform1 si second, pe baza
//vectorului de tati, construiesc cele doua structuri arborelui si le afisam intr-o maniera prietenoasa cu ajutorul functiilor prettyprint si afisarebinar.
//In functia main am inititalizat un vector de tati t si, aplicand functiile declarate mai jos, am obtinut o reprezentare binare si una multi-cai.
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

typedef struct _MULTI_CAI_NODE
{
	int key;
	int count;
	struct _MULTI_CAI_NODE *child[50];
}MULTI_NODE;
MULTI_NODE *x[10];
typedef struct Multi  // folosit pentru a doua structura
{
    int key;
    struct Multi *ch;
    struct Multi * br;
}MultiT;


MultiT *second(MULTI_NODE *nod)
{
    MultiT *q;
	MultiT *final = (MultiT*)malloc(sizeof(MultiT));
	int i;
       final->key = nod->key;
	   final-> ch = NULL;
	   final->br = NULL;

  if(nod->count >0)
  {
	  final->ch=second(nod->child[0]);
	  q=final->ch;
		for(i=1;i<nod->count;i++)
		{
			q->br = second(nod->child[i]);
			q=q->br;
        }
    }
  return final;
 }



MULTI_NODE *createMN(int key)
{
	MULTI_NODE *x=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	x->key=key;
	x->count=0;
    return x;
}

void insertMN(MULTI_NODE *parent, MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
}

MULTI_NODE *transform1(int t[],int size)
{
	int i;
	MULTI_NODE *root=NULL;
	MULTI_NODE *nodes[50];
	for (i=0;i<size;i++) //create nodes
       nodes[i]=createMN(i);
    for (i=0;i<size;i++) //legaturi
	{
		if (t[i]!=-1){
	 insertMN(nodes[t[i]],nodes[i]);
		}
		else{
			root=nodes[i];
		}
     }
	return root;
}

void prettyprint(MULTI_NODE *nod, int nivel=0)
{
	for(int i=0;i<nivel;i++)
		printf("  ");
	printf("%d \n", nod->key);
	for(int i=0; i<nod->count;i++)
		 prettyprint(nod->child[i], nivel+1);
}

void afisareBinar(MultiT *rad, int nivel)
{
    int k;

	if(rad!=NULL)
	{
		afisareBinar(rad->ch,nivel+1);
		for(k=0;k<nivel;k++)
			printf(" ");
		printf("%d \n", rad->key);
		afisareBinar(rad->br,nivel+1);
	}
}

int main()
{
	int t[]={9,5,5,9,-1,4,4,5,5,4,2};
    int size=sizeof(t)/sizeof(t[0]);
	printf("transformare 1:\n");
	MULTI_NODE *root=transform1(t,size);
	prettyprint(root);
	printf("transformare 2:\n");
	MultiT *root1=second(root);
	afisareBinar(root1,0);


	getch();
    return 0;
}
