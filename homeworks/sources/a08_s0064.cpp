#include <stdlib.h>
#include <stdio.h>


typedef struct NOD
{
	int key;
	int rank;
	struct NOD *parent;
}Nod;

Nod *nodes[10001];
int nrOp;

 

Nod * MakeSet(int x)
{
	Nod *nod;
	nod = (Nod *)malloc(sizeof(Nod));
	nodes[x] = nod;
	nod->key = x;
	nod->parent = nod;
	nod->rank = 0;
	nrOp+=4;
	return nod;
}

 

Nod *Find(int x)
{
	if(nodes[x]->parent != nodes[x])
	{
		nrOp++;
		nodes[x]->parent = Find(nodes[x]->parent->key);
	}
	nrOp++;
	return nodes[x]->parent;
}
 

void Union(int x, int y)
{
	Nod *xRoot, *yRoot;
	xRoot = Find(x);
	yRoot = Find(y);
	nrOp+=3;
	if(xRoot->key == yRoot->key)
	{
		return;
	}
	nrOp++;
	if(xRoot->rank < yRoot->rank)
	{
		nrOp++;
		xRoot->parent = yRoot;
	}
	else if(xRoot->rank > yRoot->rank)
	{
		nrOp+=2;
		yRoot->parent = xRoot;
	}
	else
	{
		nrOp+=3;
		yRoot->parent = xRoot;
		xRoot->rank = xRoot->rank + 1;
	}
}

void InitializeNodes()
{
	for(int i = 0; i<=10000;i++)
		nodes[i] = NULL;
}

void GenerateAndAddEdges(int maxNr, int m)
{
	int x,y,i;
	for(i = 1; i<=m; i++)
	{
		
		x = rand()%maxNr;
		y = rand()%maxNr;
		while(x==y)
		{
			y = rand()%maxNr;
		}
		Union(x,y);
	}

}

int main()
{
	int n, m, i, x, y;
	/*scanf("%d",&n);
	scanf("%d", &m);
	for(i = 1; i<=n;i++)
		MakeSet(i);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		if(Find(x) != Find(y))
			Union(x,y);
	}

	while(1)
	{
		scanf("%d%d",&x,&y);
		if(Find(x) == Find(y))
			printf("Same component\n");
		else printf("Different components\n");
	}*/

	FILE *f;
	f=fopen("rezultat.csv","w");
	n = 9999;

	for(m=10000; m<=60000;m+=100)
	{
		nrOp = 0;
		InitializeNodes();
		for(i = 0; i<=n;i++)
			MakeSet(i);

		GenerateAndAddEdges(10000,m);

		fprintf(f,"%d,%d\n",m,nrOp);
	}
	fclose(f);

	return 0;
}