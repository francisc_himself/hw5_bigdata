/* Assign 8

***ANONIM*** ***ANONIM*** Razvan 
Grupa ***GROUP_NUMBER***

Cerinta : implementati corect si eficient operatiile de baza pe multimi disjuncte : 
make_set, union, find_set. Utilizati "comprimarea drumului" si reuniunea dupa rang.
Mai mult, implementati aceste structuri intr-un algoritm de gasire a componentelor conexe ale unui graf.

Vom folosi o structura numita Nod pentru a reprezenta nodurile din multimi, si apoi din grafuri
si o structura Muchie pentru a reprezenta muchiile in graf. Apoi vom implementa functiile 
cerute , pe baza algoritmilor din Cormen, inclusiv pe cele de verificare daca doua varfuri
sunt in aceeasi componente conexa si de construire a grafului , returnand numarul componentelor 
conexe. 

In main, generam muchii de la 10.000 la 60.000 , pentru un numar fix de 10.000 de varfuri,
iar apoi construim graful.
Dupa scrierea datelor in fisierul csv si construirea graficului,
se observa ca acesta este aproximativ liniar.

*/


#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/*Nodul unui Nod, in care se retine un varf al grafului*/
typedef struct node{
	int key,rang;
	node *p;
}Nod;

typedef struct m{
	int x;
	int y;
}muchie;

const int MIN=1;
const int MAX=10000;

int op;//variabila globala ce retine numarul de apeluri la procedurile: findSet, makeSet, reuneste
muchie edge[60000];
Nod* nod[10001];

/*Procedua makeSet creeaza o multime noua, continand un singur element
care este totodata si reprezentatul multimii*/
Nod * makeSet(int x){
	Nod *element=new Nod();
	element->key=x;
	element->rang=0;
	element->p=element;
	return element;
}

/*Procedura findSet  */
Nod * findSet(Nod* x){
	if (x->key!=x->p->key)
	{
		/*Apelarea recursiva duce la comprimarea drumului*/
		x->p=findSet(x->p);
		op++;
	}

	return x->p;
}

/*Procedura link leaga doua noduri date ca si argumente astfel:
daca rangul primului nod este mai mare decat rangul celui de-al
doilea nod, atunci primul nod devine parintele celui de-al doilea,
altfel, asocierea se face invers*/
void link(Nod* x, Nod* y){
	if (x->rang>y->rang)
		y->p=x;
	else {
			x->p=y;
			if (x->rang==y->rang)
				y->rang++;
		}
}

/*Procedura reuneste cei doi arbori care contin nodurile x si y,
folosindu-se de procedurile link, care leaga doi arbori de radacini
date si findSet care returneaza reprezentatnul multimii disjuncte din
care face parte nodul transmis ca si parametru*/
void reuneste(Nod *x, Nod* y){
	op+=2;
	link(findSet(x),findSet(y));
}

/*Procedura returneaza pointer-ul catre nodul de cheie data k*/
Nod* gasesteNod(int k, int nr){
	for (int i=0;i<nr;i++)
		if (nod[i]->key==k)
			return nod[i];

    //return NULL;
}

/*Procedura primeste ca si parametrii nr de noduri si de muchii ale grafului.
Pentru inceput creeaza o componenta conexa(o multime disjuncta) pentru fiecare
nod in parte.
Al doilea pas il constituie parcurgerea vectorului in care sunt retinue muchiile,
si in momentul in care se gaseste o muchie care are extremitatile in doua multimi disjuncte
(2 componente conexe diferite), cele doua multimi sunt reunite*/
void componenteConexe(int nrNoduri, int nrMuchii){
	int i;
	int comp=nrNoduri;
	for (i=0;i<nrNoduri;i++){
			nod[i]=makeSet(i);
			op++;
		}

	for (i=0;i<nrMuchii;i++){
		if (findSet(gasesteNod(edge[i].x, nrNoduri))!=findSet(gasesteNod(edge[i].y, nrNoduri)))
		{
			reuneste(gasesteNod(edge[i].x, nrNoduri),gasesteNod(edge[i].y, nrNoduri));
			op++;
			comp--;
		}
	}

	printf("componente conexte = %d " , comp);

}

/*Procedura sameComponent verifica daca doua noduri transmise ca si parametru
fac parte din aceeasi componenta conexa*/
void sameComponent(Nod* x, Nod* y){
	if (findSet(x)==findSet(y))
		printf ("Nodurile fac parte din aceeasi componenta conexa");
	else printf ("Nodurile NU fac parte din aceeasi componenta conexa");
}

int main(){

	srand (time(NULL));
	int i;
	int nrMuchii = 10000;
	op=0;
	bool g,h;
	FILE *f;

	f=fopen("multimi.csv","w");
	fprintf(f, "nr_muchii, operatii\n");

	while(nrMuchii<=60000)
	{
		op=0;
		for(i=0;i<nrMuchii;i++)
		{
			edge[i].x = rand()%(10000-1);
			edge[i].y = rand()%(10000-edge[i].x)+edge[i].x+1;

		}

		g=true;

		for(i=0;i<1000-1;i++)
			for(int j=i+1;j<1000;j++)
				if ((edge[i].x==edge[j].x) && (edge[i].y==edge[j].y))
					g=false;

		if (g!=true)
			for(i=0;i<1000-1;i++)
				for(int j=i+1;j<1000;j++)
					if ((edge[i].x==edge[j].x) && (edge[i].y==edge[j].y))
						edge[i].y--;


		if (g==true)
			printf("\nOK\n");
		else
			printf("\nNOT OK\n");

		printf("\nnrMuchii=%d\n",nrMuchii);
		componenteConexe(10001,nrMuchii);
		fprintf(f,"%d,%d\n",nrMuchii,op);
		nrMuchii+=1000;
	}
	fclose(f);

	getch();
}
