#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#define MAX_SIZE 10000

typedef struct NOD {
	int val;
	NOD * urm;
} nod;

typedef struct NOD_heap {
	nod *elem;
	int idList;
} nod_heap;

nod_heap v[MAX_SIZE];
int n=0; 
int k=0; 
int heap_size=0;
nod *prim[1000];
nod  *ultim[1000];
nod *primJ;
nod *ultimJ;

void inserare(nod **prim, nod **ultim, int val)
{
	nod *p=(nod *)malloc(sizeof(nod));
	p->val=val;
	if (*prim == 0)
	{
		*prim=*ultim=p;
	}
	else
	{
		(*ultim)->urm=p;
		*ultim=p;
	}

	p->urm=0;
}

void afisare(nod *prim)
{
	nod *p;

	if(prim == 0)
	{
		printf("Lista vida\n");
	}
	else
	{
		p=prim;
		while(p != 0)
		{
			printf("%d ", p->val);
			p=p->urm;
		}
	}
	printf("\n");
}

void deletePrim(nod **prim, nod **ultim)
{
	nod *p;
	if (*prim == 0)
	{
		return;
	}
	p=*prim;
	*prim=(*prim)->urm;
	free(p);
	if (*prim == 0)
	{
		*ultim=0;
	}
}

void deleteList(nod **prim, nod **ultim)
{
	nod *p, *pp;
	p=*prim;

	if (p == 0)
	{
		return;
	}
	do
	{
		pp=p;
		p=p->urm;
		free(pp);
	} while (p != 0);
	*prim=*ultim=0;
}


void prtyprnt(int *v, int n, int k, int nivel)
{
	if (k>n)
	{
		return;
	}
	prtyprnt(v,n,2*k+1,nivel+1);
	for (int i=1;i<=nivel;i++)
	{
		printf("   ");
	}
	printf("%d\n",v[k]);
	prtyprnt(v,n,2*k,nivel+1);

}

void main()
{
	nod *head=0, *tail=0;
	int sortednr,srt;
	sortednr=rand()%1000;
	srt=sortednr+rand()%1000;
	inserare(&head, &tail, sortednr);
	for(int i=1; i<10; i++)
	{
		inserare(&head, &tail, srt);
		srt=srt+rand()%1000;
	}
	afisare(head);
	deletePrim(&head, &tail);
	deletePrim(&head, &tail);
	afisare(head);
	deleteList(&head, &tail);
	afisare(head);
	int b=3;
	inserare(&prim[b],&ultim[b], rand());
	afisare(prim[b]);
	deleteList(&prim[b],&ultim[b]);
	afisare(prim[b]);
}