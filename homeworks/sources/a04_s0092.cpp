/**
Student: ***ANONIM*** Diana ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: Merge k Ordered Lists Efficiently
Comments:
	

**/
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
struct node {
   int val;
   struct node * next;
};
struct bob {
	int val;
	int index;
};
long nr=0;
long heapsize=0,sizeOL=0;
node* lists[10000];
node* f;
node* l;
node* c;
int b[10001];
bob a[10001];

int right(int i)
{
	return 2*i+1;
}
int left(int i)
{
	return 2*i;
}

void min_heapify(int i)
{
	int lowest,l=left(i),r=right(i);
	if ((l<=heapsize)&&(a[l].val<a[i].val))
		lowest=l;
	else lowest=i;
	nr++;
	if ((r<=heapsize)&&(a[r].val<a[lowest].val))
		lowest=r;
	nr++;
	if (lowest!=i)
	{
		bob aux=a[i];
		a[i]=a[lowest];
		a[lowest]=aux;
		min_heapify(lowest);
		nr+=3;
	}
}
void pushHeap(bob m)
{
	heapsize++;
	a[heapsize]=m;
	nr++;
}

bob popList(node* nod,int i) // scoate din lista primul nod pt al baga in heap
{

	bob termen;
	termen.val=0;
	termen.index=0;
	
		termen.index=i;
		termen.val=nod->val;
		lists[i]=nod->next;
		nr++;
		free(nod);
	return termen;
}
bob popHeap()
{
	bob aux=a[1];
	a[1]=a[heapsize];
	nr=nr+2;
	
	heapsize--;
	min_heapify(1);
	return aux;
}
void pushList(bob pune)
{
	node* aux;
	aux=(node*)malloc(sizeof(node));
	aux->val=pune.val;
	nr++;
	aux->next=NULL;
	if (sizeOL==0)
	{
		f=aux;
		l=f;
	}
	else 
	{
		l->next=aux;
		l=aux;
	}
	sizeOL++;
}
void n_merge(int k)
{
	heapsize=0;
	for(int i=0;i<k;i++)
	{
		bob val=popList(lists[i],i);
		nr++;
		pushHeap(val);
	}
	for (int i=heapsize/2;i>0;i--)
		min_heapify(i);
	while(heapsize>0)
	{
		bob aux=a[1];
		nr++;
		pushList(aux);
		int i=0;bob min;
		if (lists[aux.index]!=NULL)
		{
			min=popList(lists[aux.index],aux.index);
			a[1]=min;
			nr++;
			min_heapify(1);
		}
		else
		{
			popHeap();
		}
	}
}
void random(int k,int n)
{
	int x=0;
	for (int j=0;j<n/k;j++)
		{
			x=x+rand()%30;
			b[j]=x;
		}
	
}

int main()
{
	srand(time(NULL));
	FILE *f1,*f2;
	f1=fopen("case1.csv","w");
	f2=fopen("case2.csv","w");
	fprintf(f1,"n,nr_k5,nr_k10,nr_k100\n");
	fprintf(f2,"k,nr\n");
	
	node *first,*last,*curr;

	//k constant, n vary
	// k1=5

	for (int n=100;n<=10000;n=n+100)
	{
		int k=5;nr=0;
		for(int i=0;i<k;i++)
		{
			//generez random lista
			for (int i=0;i<n/k;i++)
				b[i]=0;
			random(k,n);
			//creare lista
			lists[i]=(node*)malloc(sizeof(node));
			lists[i]->val=b[0];
			lists[i]->next=NULL;
			last=lists[i];
			for (int j=1;j<n/k;j++)
			{
				curr=(node*)malloc(sizeof(node));
				curr->val=b[j];
				last->next=curr;
				curr->next=NULL;
				last=curr;
			}
		}
		n_merge(k);

		fprintf(f1,"%d,%d,",n,nr);
		
		//k2=10
		k=10;nr=0;
		for(int i=0;i<k;i++)
		{
			//generez random lista
			for (int i=0;i<n/k;i++)
				b[i]=0;
			random(k,n);
			//creare lista
			lists[i]=(node*)malloc(sizeof(node));
			lists[i]->val=b[0];
			lists[i]->next=NULL;
			last=lists[i];
			for (int j=1;j<n/k;j++)
			{
				curr=(node*)malloc(sizeof(node));
				curr->val=b[j];
				last->next=curr;
				curr->next=NULL;
				last=curr;
			}
		}
		n_merge(k);
		fprintf(f1,"%d,",nr);
		
		//k3=100
		k=100;nr=0;
		for(int i=0;i<k;i++)
		{
			for (int i=0;i<n/k;i++)
				b[i]=0;
			//generez random lista
			random(k,n);
			//creare lista
			lists[i]=(node*)malloc(sizeof(node));
			lists[i]->val=b[0];
			lists[i]->next=NULL;
			last=lists[i];
			for (int j=1;j<n/k;j++)
			{
				curr=(node*)malloc(sizeof(node));
				curr->val=b[j];
				last->next=curr;
				curr->next=NULL;
				last=curr;
			}
		}
		n_merge(k);
		fprintf(f1,"%d\n",nr);
	}

	/**
	//afisare lista sortata
	c=f;
	printf("OL:\n");
	while(c!=NULL)
	{
			printf("%d - ",c->val);
			c=c->next;
	}
	printf("\n SizeOL:%d",sizeOL);
	getch();
	
	//}
	**/
	//k vary, n constant
	int n=10000;
	
	for (int k=10;k<=500;k=k+10)
	{
		nr=0;
		for(int i=0;i<k;i++)
		{
			//generez random lista
			for (int i=0;i<n/k;i++)
				b[i]=0;
			random(k,n);
			//creare lista
			lists[i]=(node*)malloc(sizeof(node));
			lists[i]->val=b[0];
			lists[i]->next=NULL;
			last=lists[i];
			for (int j=1;j<n/k;j++)
			{
				curr=(node*)malloc(sizeof(node));
				curr->val=b[j];
				last->next=curr;
				curr->next=NULL;
				last=curr;
			}
		}
		n_merge(k);
		fprintf(f2,"%d,%d\n",k,nr);
	}
	
	fclose(f1);
	fclose(f2);

	
}

//afisare lista
		/**
			printf("Lista %d \n",i);
		curr=lists[i];
		while(curr!=NULL)
		{
			printf("%d, ",curr->val);
			curr=curr->next;
		}
		printf("\n");
		**/
//VERIFICARE BUILD MIN-HEAP
	/**
	srand(time(NULL));
	for (int i=1;i<=n;i++)
		a[i]=rand()%15;
	
	for (int i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
	build_heap(n);
	for (int i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
	printf("\n");
	//pretty_print(a,1,0);
	getch();
	**/


	