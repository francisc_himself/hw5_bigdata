#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
/* ***ANONIM*** ***ANONIM***-***ANONIM*** gr.***GROUP_NUMBER***
	In implementarea algoritmului urmator am utilizat doua structuri diferite pentru nod in functie de arborele la care 
	este utilizat(binar sau multi cai). Astfel un nod poate sa aiba 2 fii(binar) sau mai multi(multicai).
	Operatia de transformare a unui vector in oricare tip are eficienta O(n), deoarece trebuie parcurs vectorul si apoi pentru 
	fiecare element trebuie creat un nod. 

*/

typedef struct _MULTI_NODE{
	int key;
	int count;
	struct _MULTI_NODE *child[50];
}MULTI_NODE;

typedef struct _binar{
	int key;
	struct _binar *fchild,*fdr; 
}Binar;

MULTI_NODE* createMN(int key)
{
	MULTI_NODE *p;
	p=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	//if()
	p->key=key;
	p->count=0;
	return p;
}
	
void insertMn(MULTI_NODE *pr,MULTI_NODE *child)
{ 
	pr->child[pr->count++]=child;	
}
void insertMN2(MULTI_NODE *pr,MULTI_NODE *child1,MULTI_NODE *child2)
{
	pr->child[0]=child1;
	pr->child[1]=child2;
	for(int i=2;i<pr->count;i++){
		free(pr->child[i]);
		pr->count--;
	}
}

MULTI_NODE* transform1(int t[],int size)
{
	MULTI_NODE *node[50];
	MULTI_NODE *root=NULL;
	for(int i=0;i<size;i++)
	{
		node[i]=createMN(i);
	}
	for(int i=0;i<size;i++)
	{
 		if(t[i]==-1)
		{
			root=node[i];
		}else insertMn(node[t[i]],node[i]);
	}
	return root;
}

void transform2(MULTI_NODE *root ,Binar *rad)
{
	if(root->count>0){
		Binar *nod=(Binar*)malloc(sizeof(Binar));
		nod->key=root->child[0]->key;
		rad->fchild=nod;
		transform2(root->child[0],nod);
		if(root->count>1){
			int i=1;
			while(i<root->count){
				nod->fdr=(Binar*)malloc(sizeof(Binar));
				nod->fdr->key=root->child[i]->key;
				nod=nod->fdr;
				transform2(root->child[i],nod);
				i++;
			}
			nod->fdr=NULL;
		}
		else rad->fchild->fdr=NULL;
		rad=nod;
	}
	else rad->fchild=NULL;
}


void afisMN(MULTI_NODE *nod,int nivel=0)
{
	for(int i=0;i<nivel;i++)
		printf("   ");
	printf("%d\n",nod->key);
	for(int i=0;i<nod->count;i++)
		afisMN(nod->child[i],nivel+1);
}
void afisBN(Binar *nod,int nivel=0)
{
	if(nod==NULL) return;
	afisBN(nod->fdr,nivel+1);
	
	for(int i=0;i<nivel;i++)
		printf("   ");
	
	printf("%d\n",nod->key);
	afisBN(nod->fchild,nivel+1);
	
	}

int main()
{
	int t[]={0,2,7,5,2,7,7,-1,5,2};
	int size=sizeof(t)/sizeof(t[0]);
	MULTI_NODE *root=transform1(t,size);
	afisMN(root);
	Binar *rad =(Binar*)malloc(sizeof(Binar));
	rad->key=root->key;
	transform2(root,rad);
	rad->fdr=NULL;
	printf("--------------------------------------\n");
	afisBN(rad,0);
	return  0;	
}