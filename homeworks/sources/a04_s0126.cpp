#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler;
typedef struct node
{
    int info;
    node *next;
} NODE;

typedef struct elem
{
    int poz;
    int element;
} ELEM;

struct lists
{
    NODE *first;
    NODE *last;
};

struct lists LISTS[501];
NODE *first_inter ;
NODE *last_inter;
ELEM heap[10001];
int a[10001];

void addElem(NODE **first,NODE **last,int val)
{
    if((*first)==NULL)
    {
        (*first) = new NODE;
        (*first)->info=val;
        (*first)->next=NULL;
        *last=(*first);
    }
    else
    {
        NODE *elem=new NODE;
        elem->next=NULL;
        elem->info=val;
        (*last)->next=elem;
        (*last)=elem;
    }
}

void heapify(ELEM heap[],int i,int size,int n)
{
    int l,r,min;
    ELEM aux;
    l=2*i;
    r=2*i+1;
	profiler.countOperation("total",n);
    if ((l<=size)&&(heap[l].element < heap[i].element))
        min=l;
    else
        min=i;
	profiler.countOperation("total",n);
    if ((r<=size)&&(heap[r].element < heap[min].element))
        min=r;
	profiler.countOperation("total",n);
    if (min!=i)
    {
        aux=heap[i];
        heap[i] = heap[min];
        heap[min]=aux;
		profiler.countOperation("total",n,3);
        heapify(heap,min,size,n);
    }
}

void insertHeap(ELEM heap[],ELEM x,int *size, int n)
{
    (*size)++;
    heap[(*size)]=x;
    int i=(*size);
    ELEM aux;
	profiler.countOperation("total",n);
    while (i>1 && (heap[i].element)<(heap[i/2].element))
    {
        profiler.countOperation("total",n);
        aux=heap[i];
        heap[i]=heap[i/2];
        heap[i/2]=aux;
        profiler.countOperation("total",n,3);
        i=i/2;
    }
}

ELEM extractHeap(ELEM heap[],int *size,int n)
{
    ELEM x;
    if((*size)>0)
    {
        x=heap[1];
        heap[1]=heap[(*size)];
        profiler.countOperation("total",n);
        (*size)--;
        heapify(heap,1,(*size),n);
    }
    return x;
}

void interclass(lists LISTS[],int k,int n)
{
    int size = 0;
    ELEM v;
    for(int i=1; i<=k; i++)
    {
        v.poz=i;
        v.element=LISTS[i].first->info;
        profiler.countOperation("total",n);
        insertHeap(heap,v,&size,n);
    }
    ELEM pop;
    while (size>0)
    {
        pop = extractHeap(heap,&size,n);
        int poz =pop.poz;
        addElem(&first_inter,&last_inter,pop.element);
        LISTS[poz].first=LISTS[poz].first->next;
        profiler.countOperation("total",n);
        profiler.countOperation("total",n);
        if (LISTS[poz].first!=NULL)
        {
            v.poz=pop.poz;
            v.element=LISTS[pop.poz].first->info;
            profiler.countOperation("total",n);
            insertHeap(heap,v,&size,n);
        }
    }
}


void createLists(lists LISTS[],int k,int n)
{
    int nr = 0;
    for (int i=1; i<=k; i++)
        LISTS[i].first = LISTS[i].last = NULL;
	for (int i=1; i<=k; i++)
     {
		 nr = rand()%100;
		for (int j=1; j<=n/k; j++)
		 {
            nr=nr+rand()%100;
            addElem(&LISTS[i].first ,&LISTS[i].last,nr);
        }
    }
}

void printList(NODE *first,NODE *last)
{
    NODE *c;
    c=first;
    while(c!=0)
    {
        printf("%d    ",c->info);
        c=c->next;
    }
	printf("\n");
}

int main()
{
	//test if it works
	createLists(LISTS,10,100);
	for(int i=0;i<=10;i++)
		printList(LISTS[i].first,LISTS[i].last);
	interclass(LISTS,10,100);
	printf("\n\nafter interclass:\n\n");
	printList(first_inter,last_inter);
	_getch();
	//first test
	 /*int k=0;
	    for (int n = 100 ; n <= 10000; n+=100)
        {
            for (int l=1; l<=3; l++)
            {
                if (l==1)
                    k=5;
                else if (l==2)
                    k=10;
                else
                    k=100;
                createLists(LISTS,k,n);
                interclass(LISTS,k,n);
			}
		}*/
		//second test
	   /*int   n=10000;
       for (int k = 10 ; k<=500 ; k+=10)
          {
                createLists(LISTS,k,n);
                interclass(LISTS,k,k);
		  }	
		profiler.showReport();*/
	return 0;
}
