/*
	in urma evaluarii constructia top-down are eficienta O(n*log2(n)); aici majoritatea nodurilor, cele mai multe, de pe ultimele nivele 
	au deplasarile cele mai scurte(de sus in jos); iar in cazul constructiei bottom-up, eficienta este O(n).Aici majoritatea nodurilor
	(cele mai multe), de pe ultimele nivele au deplasari cele mai lungi (de jos in sus)
*/
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <time.h>

#define MAX 10001

int heap_size, dim;
int atrt, cmpt, atrb, cmpb;


////////////////Bottom-um////////////////

int stanga(int i)
{
	return 2*i;
}

int dreapta(int i)
{
	return i*2+1;
}
 
void reconstituie_heap(int A[], int i)
{
	int l, r, min, aux;

	l=stanga(i);
	r=dreapta(i);

	cmpb++;
	if((l<=heap_size) && (A[l]<A[i]))
		{
			min=l;	
		}
	else {min=i;}

	cmpb++;
	if((r<=heap_size) && (A[r]<A[min]))
		{min=r;}
	if(min!=i)
		{	atrb= atrb+3;

			aux=A[i];
			A[i]=A[min];
			A[min]=aux;
			reconstituie_heap(A, min);
		}
}

void bottom_up(int A[])
{
	int i;
	atrb=0; cmpb=0;
	
	for(i=heap_size/2; i>0; i--)
		reconstituie_heap(A, i);
}
///////////////////Top-down//////////////////
int p(int i)
{	
	return i/2;
}


void push(int A[], int cheie)
{
	dim++;

	atrt++;

	A[dim]=cheie;
	int i=dim;
	int aux;

	cmpt++;
	while(i>0 && A[p(i)]>A[i])
		{	
			cmpt++;
			atrt=atrt+3;

			aux=A[p(i)];
			A[p(i)]=A[i];
			A[i]=aux;
			i=p(i);
		}
}

void initializeaza_heap(int A[])
{
	dim=0;
}

void top_down(int B[], int A[])
{	cmpt=0; atrt=0;

	initializeaza_heap(B);
	for(int i=1; i<=heap_size; i++)
		push(B, A[i]);
}


void copie_sir(int sursa[], int dest[], int n)
{
	int i;
	for(i=1; i<=heap_size; i++)
		dest[i]=sursa[i];
}

 ////////////////Testarea algoritmului////////////////////
void main()
{	heap_size=6;
	int A[7];
	int B[7];
	int  i,j;
	A[1]=5, A[2]=1, A[3]=4, A[4]=10, A[5]=2, A[6]=8;
	printf("Vector nesortat: \n");
	for( i=1; i<=heap_size; i++)
		printf("%d ", A[i]);

	int adancime = 1;
	bottom_up(A);
	printf("Vector sortat: \n");
	for( i=1; i<=heap_size; i++)
		{
			for(j=1; j<=heap_size; j++)
				printf("\t");
			printf("%d ", A[i]);
			if((i==1)||(i==3)||(i==7))
				{
					adancime++;
					printf("\n");
				}
		}

	A[1]=5, A[2]=1, A[3]=4, A[4]=10, A[5]=2, A[6]=8;
	
	printf("\n");
	printf("Vector nesortat: \n");
	for( i=1; i<=heap_size; i++)
		printf("%d ", A[i]);
	top_down(B, A);
	printf("\n");
	printf("Vector sortat: \n");
	for( i=1; i<=heap_size; i++)
		{
			for(j=1; j<=heap_size/2-adancime; j++)
				printf("\t");
			printf("%d ", B[i]);
			if((i==1)||(i==3)||(i==7))
				{
					adancime++;
					printf("\n");
				}

		}
}



/*
/////////////////Rapoarte///////////////
void main()
{  FILE *f;
   int A[MAX];
   int B[MAX];
   int atribuiri_t=0;
   int comparatii_t=0;
   int atribuiri_b=0;
   int comparatii_b=0;

   srand(time(NULL)); //initializeaza pasul pentru functia rand


   f=fopen("mediu_statistic.csv", "w");
   //f=fopen("defavorabil.csv", "w");

   //scrierea capului de tabel
   fprintf(f, "n, bottom_atr, bottom_cmp, bottom_atr+cmp, top_atr, top_cmp, top_atr+cmp\n");

   for(heap_size=100; heap_size<=10000; heap_size=heap_size+100)
	{
		atrt=0; 
		cmpt=0;
		atrb=0;
		cmpb=0;
		atribuiri_t=0;
		comparatii_t=0;
		atribuiri_b=0;
		comparatii_b=0;


		for(int j=1; j<=5; j++)
		{
			for(int i=1; i<=heap_size; i++)
			{	A[i]=rand();
			    B[i]=A[i];}
		

		
		bottom_up(A);
		atribuiri_b=atribuiri_b+atrb;
		comparatii_b=comparatii_b+cmpb;

		copie_sir(B, A, heap_size);

		top_down(B, A);
		atribuiri_t=atribuiri_t+atrt;
		comparatii_t=comparatii_t+cmpt;
		}
   fprintf(f, "%d,%d,%d,%d,%d,%d,%d\n", heap_size, atribuiri_b/5, comparatii_b/5, (atribuiri_b+comparatii_b)/5, atribuiri_t/5, comparatii_t/5,( atribuiri_t+comparatii_t)/5);

}
   
}


  */