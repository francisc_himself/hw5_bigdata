/*
* ***ANONIM*** ***ANONIM*** ***ANONIM*** Gr : ***GROUP_NUMBER*** CTI
* Structuri de date pentru multimi disjuncte
*/



#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>



typedef struct ds_node{
	int key;
	struct ds_node * reprezentant;	
	struct ds_node * urmator;
}DS_NODE;

DS_NODE *cap_lista[10001], *coada_lista[10001];
int a[10001][10001], v, null_list[10001];

int NR_Operatii;

//aceasta functie va printa o matrice data ca parametru
void printeaza_matrice (int b[10001][10001],int n)
{
	int i, j;

	for (i=1; i<=n; i++)
	{
		for (j=1; j<=n; j++)
			printf ("%d ",b[i][j]);
		printf ("\n");
	}
}

//creaza un nod de cheie k
DS_NODE * creeaza_nodcheie (int k)
{
	DS_NODE * p;

	p=(DS_NODE *) malloc(sizeof(DS_NODE));
	p->urmator = NULL;
	p->key = k;

	return p;
}

//sterge o lista
void delete_list (int index)
{
    
	null_list[index] = 1;
}

void creeaza_set (int v)
{
	cap_lista[v] = coada_lista[v] = creeaza_nodcheie (v);
	cap_lista[v]->reprezentant = coada_lista[v]->reprezentant = cap_lista[v];
}

DS_NODE * gaseste_set (int u, int *index)
{
	int i;
	DS_NODE *p;

	for (i=1; i<=v; i++)
	{
		
		if (null_list[i] != 1)
		{
			p = cap_lista[i];
			
			do
			{
				if (p->key == u)
				{
					*index = i;
					return cap_lista[i];
				}
			
				p = p->urmator;
			}
			while (p!=NULL);
		}
	}
}

void repr_Grup (int u, int v)	//returneaza reprezentantul grupului
{
	DS_NODE *cap_lista1, *cap_lista2, *coada_lista1, *coada_lista2, *p;
	int index1, index2;

	cap_lista1 = gaseste_set (u, &index1);
	cap_lista2 = gaseste_set (v, &index2);
	coada_lista1 = coada_lista[index1];
	coada_lista2 = coada_lista[index2];

	//il consideram ca reprezentant pe cel mai mic reprezentant
	if (cap_lista1->key < cap_lista2->key)
	{
		//punem lista 2 la sfarsitul listei 1
		coada_lista[index1]->urmator = cap_lista[index2];
	
		coada_lista[index1] = coada_lista[index2];
		//il punem ca repezentant pe cap_lista1
		p = cap_lista[index2];
		while (p != NULL)
		{
			p->reprezentant = cap_lista1;
			p = p->urmator;
		}

		//stergem lista 2
		delete_list (index2);
	}
	else
	{
		//punem lista 1 la sfarsitul listei 2
		coada_lista2->urmator = cap_lista1;
	
		coada_lista[index2] = coada_lista[index1];
		// repezentant cap_lista2
		p = cap_lista1;
		while (p != NULL)
		{
			p->reprezentant = cap_lista2;
			p = p->urmator;
		}

		//stergere lista 1
		delete_list (index1);
	}
}

void componente_conexe ()
{
	int i,j, index;

	for (i=1; i<=v; i++)
	{
		NR_Operatii++;
		creeaza_set (i);
	}

	for (i=1; i<=v; i++)
		for (j=1; j<=v; j++)
			if (a[i][j] == 1)
			{
				NR_Operatii = NR_Operatii+2;
				if (gaseste_set (i, &index) != gaseste_set(j, &index))
				{
					NR_Operatii++;
					repr_Grup (i,j);
				}
			}
}


void testare_afisare ()
{
	DS_NODE *p;
	int i, j;
	int e, v1, v2;

	srand ( time(NULL) );
	
	v = 15; 

	//initializam matricea de muchii
	for (i=1; i<=v; i++)
		for (j=1; j<=v; j++)
			a[i][j] = 0;

	e = 10;

	//generam muchiile
	printf ("Muchiile sunt:\n");
	for (i=1; i<=e; i++)
	{
		//generam aleator perechi de numere cuprinse intre 1 si 10000 muchii
		v1 = rand () % v + 1;
		v2 = rand () % v + 1;
		
	

		while (v1==v2 || a[v2][v1] == 1 || a[v1][v2]==1)
		{
			v1 = rand () % v + 1;
			v2 = rand () % v + 1;

			//printf ("v1=%d v2=%d\n", v1, v2);
		}

		printf ("v1=%d v2=%d\n", v1, v2);
		a[v1][v2] = 1;
	}

	printf ("\nMatricea muchiilor:\n");
	printeaza_matrice(a,v);

	//initlistele inlantuite
	for (i=1; i<=v; i++)
		cap_lista[i] = coada_lista[i] = NULL;

	componente_conexe ();
	printf ("\nComponentele conexe:\n");
	for (i=1; i<=v; i++)
		if (null_list[i] != 1)
		{
			p = cap_lista[i];
			printf ("L[%d]: ",i);
			while (p != NULL)
			{
				printf ("%d ",p->key);
				p = p->urmator;
			}
			printf ("\n");
		}
}

int main ()
{
	testare_afisare ();
	
	
	FILE *pf;
	int i, j;
	int e, v1, v2;

	pf = fopen ("export_mult_disjuncte.txt","w");
	srand ( time(NULL) );

	v = 10000;

	//numarul de muchii E variind de la 10000 la 60000, cu incrementare 1000;
	for (e=10000; e<=60000; e=e+1000)
	{
		//initializam matricea de muchii
		for (i=1; i<=v; i++)
			for (j=1; j<=v; j++)
				a[i][j] = 0;

		//generare muchii
		for (i=1; i<=e; i++)
		{
			v1 = rand () % v + 1;
			v2 = rand () % v + 1;

			while (v1==v2 || a[v2][v1] == 1 || a[v1][v2]==1)
			{
				v1 = rand () % v + 1;
				v2 = rand () % v + 1;
			}

			a[v1][v2] = 1;
		}

		//initializam matricele
		for (i=1; i<=v; i++)
		{
			cap_lista[i] = coada_lista[i] = NULL;	//initializam listele inlantuite
			null_list[i] = 0;			//initializam vectorul de liste nule
		}
		
		NR_Operatii = 0;
		componente_conexe ();

		fprintf (pf,"%d %d\n",e,NR_Operatii);
	}



	getch();
	return 0 ;
}