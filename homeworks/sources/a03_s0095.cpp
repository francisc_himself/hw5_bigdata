/*
Both algorithms has the complexity O( n log n ) and quicksort in worst case has the complexity O( n*n ).
Both algorithms ***ANONIM*** liner.
*/
#include "stdafx.h"
#include<iostream>
#include<math.h>
#include<conio.h>
#include "Profiler.h"
#define MAX 10000
using namespace std;
Profiler profiler("Heapsort vs Quicksort");
int size,n;

void PrettyPrint(int A[],int n){
	double  height,i,j,k,nr=0,ok=1,level;
	do{
		if(n>pow(2,nr) && n<=pow(2,nr+1))
			ok=0;
		nr++;
	}while(ok!=0);
	cout<<"                 ";
	height=nr;nr=0;
	level=height;
	for(i=1;i<=height;i++){
		for(j=pow(2,nr);j<pow(2,nr+1);j++){
			if(j==pow(2,nr)){
				for(k=1;k<=pow(2,level-1)-1;k++) cout<<" ";
			}
			if(j<=n){
			cout<<A[(int)j];
			for(k=1;k<=pow(2,level)-1;k++) cout<<" ";
			}
		}cout<<"\n\n                 ";
	nr++;level--;
	}
}
void heapify(int A[],int i,int n){
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
	
	profiler.countOperation("Heapsort",n);
	if(l<=size && A[l]>A[i])
	{largest=l;}
	else 
	{largest=i;}

	profiler.countOperation("Heapsort",n);
	if(r<=size && A[r]>A[largest])
	{largest=r;}


	if(largest!=i)
	{
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		profiler.countOperation("Heapsort",n,3);
		heapify(A,largest,n);
	}
	

}


void BuildMaxHeap(int A[],int n){
	size=n;
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}

void HeapSort(int A[]){
	int i,aux;
	BuildMaxHeap(A,n);
	for(i=n;i>=2;i--){
		aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		profiler.countOperation("Heapsort",n,3);
		size--;
		heapify(A,1,n);
	}
}


void quickSort(int A[], int l, int r) {
    int i = l, j = r;
    int aux;
    int pivot = A[(l + r) / 2];
	profiler.countOperation("Quicksort",n);
    while (i <= j) 
	{
		profiler.countOperation("Quicksort",n);
        while (A[i] < pivot)
		{ 
			i++;
			profiler.countOperation("Quicksort",n);
		}
		profiler.countOperation("Quicksort",n);
        while (A[j] > pivot)
		{
			j--;
			profiler.countOperation("Quicksort",n);
		}
        if (i <= j) 
		{
			profiler.countOperation("Quicksort",n,3);
            aux = A[i];
            A[i] = A[j];
            A[j] = aux;
            i++;
            j--;
        }
	}
	if (l < j)
		quickSort(A, l, j);
	if (i < r)
		quickSort(A, i, r);
}

void quickbest(int A[], int l, int r) {
    int i = l, j = r;
    int aux;
    int pivot = A[(l + r) / 2];
	profiler.countOperation("QuickBest",n);
    while (i <= j) 
	{
		profiler.countOperation("QuickBest",n);
        while (A[i] < pivot)
		{ 
			i++;
			profiler.countOperation("QuickBest",n);
		}
		profiler.countOperation("QuickBest",n);
        while (A[j] > pivot)
		{
			j--;
			profiler.countOperation("QuickBest",n);
		}
        if (i <= j) 
		{
			profiler.countOperation("QuickBest",n,3);
            aux = A[i];
            A[i] = A[j];
            A[j] = aux;
            i++;
            j--;
        }
	}
	if (l < j)
		quickbest(A, l, j);
	if (i < r)
		quickbest(A, i, r);
}

void quickworst(int A[], int l, int r) {
    int i = l, j = r;
    int aux;
    int pivot = A[r];
	profiler.countOperation("QuickWorst",n);
    while (i <= j) 
	{
		profiler.countOperation("QuickWorst",n);
        while (A[i] < pivot)
		{ 
			i++;
			profiler.countOperation("QuickWorst",n);
		}
		profiler.countOperation("QuickWorst",n);
        while (A[j] > pivot)
		{
			j--;
			profiler.countOperation("QuickWorst",n);
		}
        if (i <= j) 
		{
			profiler.countOperation("QuickWorst",n,3);
            aux = A[i];
            A[i] = A[j];
            A[j] = aux;
            i++;
            j--;
        }
	}
	if (l < j)
		quickworst(A, l, j);
	if (i < r)
		quickworst(A, i, r);
}

void print(int a[], int n)
{	
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
}

void main()
{
	int a[MAX],b[MAX],c[MAX];
	int i,j;

 	/*for(i=100;i<10000;i+=100)
	{
		FillRandomArray(a,i,0,10000);
		for(int k=1;k<=5;k++)
		{	
			n=i;
			for(j=1;j<=n;j++)
			{
				b[j]=a[j];
				c[j]=a[j];
			}
			quickSort(b,1,n);
			HeapSort(c);
		}
	}
	profiler.createGroup("AverageOperation","Heapsort","Quicksort");
	profiler.showReport();*/

	/*for(i=100;i<10000;i+=100)
	{
		FillRandomArray(a,i,0,10000,false,1);
		n=i;
		for(j=1;j<=i;j++)
			{
				b[j]=a[j];
				c[j]=a[j];
			}
		quickbest(b,1,n);
		quickworst(c,1,n);
	}
	
	profiler.createGroup("Best Case","QuickBest");
	profiler.createGroup("Worst Case","QuickWorst");
	profiler.showReport();*/
	
	
	/*printf("n=");
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		printf("a[%d]=",i);
		scanf("%d",&a[i]);
	}	
	for(j=1;j<=n;j++)
	{
		b[j]=a[j];
		c[j]=a[j];
	}
	printf("\nUnsorted: \n\n");
	print(a,n);
	quickbest(b,1,n);
	quickworst(c,1,n);
	cout<<"\nSorted with best: \n\n";
	print(b,n);
	cout<<"\nSorted with worst: \n\n";
	print(c,n);
	getch();*/
	
	printf("n=");scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		printf("a[%d]=",i);
		scanf("%d",&a[i]);
	}	
	for(j=1;j<=n;j++)
	{
		b[j]=a[j];
		c[j]=a[j];
	}
	printf("\nUnsorted:");
	print(a,n);
	quickSort(b,1,n);
	HeapSort(c);
	cout<<"\nQuicksort:";
	print(b,n);
	cout<<"\nHeapsort:";
	print(c,n);
	getch();
}