
/*

	Student:					***ANONIM*** ***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Requirements:				You are required to implement correctly and efficiently the Heapsort and Quicksort advanced sorting methods.                                
                                1. You are required to compare the two sorting procedures in the average case.
                                   Remember that for the average case you have to repeat the measurements m times 
                                   (m=5) and report their average; also for the average case, make sure you always
                                    use the same input sequence for the two methods  to make the comparison fair.                               2.The analysis should be performed:
                               2. This is how the analysis should be performed:
                                - vary the dimension of the input array (n) between [10010000], with an increment 
                                 of maximum 500 (we suggest 100).
                                - for each dimension, generate the appropriate input sequence for the method; 
                                 run the method, counting the operations (assignments and comparisons, may be counted together).
                                 Only the assignments and comparisons performed on the input structure and its 
                                 corresponding auxiliary variables matter.
                               3. Generate a chart which compares the two methods under the total number of operations,
                                 in the average case. If one of the curves cannot be visualized correctly because the 
                                 other has a larger growth rate, place that curve on a separate chart as well.
                                  Name your chart and the curves on it appropriately.
                               4.Evaluate Quicksort in the best and worst cases also  total number of operations. 
                                 Compare the performance of Quicksort in the three analysis cases. Interpret the results.   
            
 Conclusions, Interpretations:  I have learned about how to perform tow different sorting methods and compare them:
                                 quicksort compared with heapsort from the previous laboratory.For average case one 
                                 can notice that quicksort performs better than heapsort , even if they  have the same 
                                 order of growth O(n*lgn). So even if heapsort is an optimal algorithm , in practice 
                                 quick sort performs better.
                                 Also when I have evaluated quicksort for the three case scenarious I noticed that ,
                                 we have the same order of growth(approximately) for best and average case scenario, 
                                 having O(nlgn). However in the worse case each partition divides the array into 1
                                 and the rest of the elements => we have recursive calls on 1 and n-1 elements=>
                                 n+(n-1)+(n-2)+=O(n^2) , that is quadratic. There are several methods in which we make sure that
                                 we never get in the worse case (Selects the ith smallest element from an unordered array,
                                 or Akls alg).
                                     
*/
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define nmax 10001
#define max 100

int n;
int H[nmax];
int A[nmax];



int nropH=0;//assignemnts + comparisons for heapsort
int nropQ=0;//assignemnts + comparisons for quicksort
int eval;//for worse case ,1 for best and avg

void swap(int *x,int *y)
{
   int temp;
   temp = *x;
   *x = *y;
   *y = temp;
}

/*
This function will generate a random vector with values between 0 and max-1.
*/

void random(int t)
{
	int i;
	srand(time(NULL));	
	for(i=1;i<=t;i++)
	{
		H[i]=rand()% max;
	}
}

/*
This function  will contruct the vector randomly generated ,v[] is a copy of A[].
*/

void copyArray(int t,int dest[],int source[])
{
	int i;
	for(i=1;i<=t;i++)
	{
		dest[i]=source[i];
	}
}

/*
This function builds a vector with t 
elements with random values in ascending order.
I have used it to generte the decreasing  random vector
*/

void randomC(int t)
{
	int i;
	srand(time(NULL));
	H[0]=rand()%50;
	for(i=1;i<t;i++)
	{
		H[i]=H[i-1]+rand()%100;

	}
}


int Parent(int i)
{
	return i/2;
}

int Left(int i)
{   
	return 2*i;
}

int Right(int i)
{    
	return 2*i+1;
}

void Heapify(int A[],int i,int n)
{
	int left,right,maxim,aux;
	    left=Left(i);
	    right=Right(i);	
 
 	    
	if ((left<=n)&&(A[left]>A[i])) 
		maxim=left;
	else
		maxim=i;	
  nropH=nropH+1;
         		
	if ((right<=n)&&(A[right]>A[maxim]))
		maxim=right;
        nropH=nropH+1;	
        
    if (maxim!=i)
	{   swap(&A[i],&A[maxim]);	
        nropH=nropH+3;			
        
		Heapify(A,maxim,n);
	}
}

void BuildHeapify(int H[],int n)
{
	int i;
	for(i=n/2;i>=1; i--)
		Heapify(H,i,n);
}

void HeapSortBu(int H[],int n)
{   nropH=0;
	int aux,i;
		BuildHeapify(H,n);
	for(i=n;i>1;i--)
	{  swap(&H[1],&H[i]);	
		nropH=nropQ+3;
		Heapify(H,1,i-1);//restaureaza heapul;
	}
}


int Partition(int H[],int p, int r){
   int x,i,j;
   nropQ=nropQ+1;
   
  //alege pivot cu if 
      if(eval==0)
         x=H[p];
      else
         x=H[(p+r)/2];   
   
   i=p-1;
   j=r+1;
      
 while (true){
	
    do{  //j takest values from r downto 1
		j--;
		nropQ=nropQ+1;
	}while(H[j]>x);
	
	do{ // i takes values from 0 to r-1
		i++;
        nropQ=nropQ+1;				
	}while(H[i]<x);
	
	if(i<j){
		nropQ=nropQ+3;
		swap(&H[i],&H[j]);
    }
     else return j;
  }
}  


/*The quicksort algorithm.*/

void QuickSort(int H[],int p,int r)
{  int q=0;

   if(p<r)
     { q=Partition(H,p,r);
       QuickSort(H,p,q);
       QuickSort(H,q+1,r);
     }     
 }

int main()
{
        int i,j,ind,t;
	    
     	FILE *f1,*f2;
      	f1=fopen("fa3_average.csv","w");
      	f2=fopen("fa3_quick.csv","w");
        
                    
        printf("Introduce an index :\n\n 0-sort a predefined vector \n\n 1-generate the tests for heapsort and quicksort\n ");       
        printf("\nindex=");
    	scanf("%d",&ind);
    	
	if(ind==0)
	  {                        
                       printf("n="); scanf("%d",&n);
                       random(n);
                       copyArray(n,A,H);//in A pastrez o copie e lui H                     
                       printf("The vector before sorting is:\n");	                
		               for(i=1;i<=n;i++)
                  	     printf("%d ",H[i]);
                            printf("\n");
                            
                      eval=1;
                      QuickSort(H,1,n);
                      printf("\n\nAfter QuickSort is :\n ");  
                      for(i=1;i<=n;i++)
                  	     printf("%d ",H[i]);
                            printf("\n");
                      copyArray(n,H,A);
                                           	
                      HeapSortBu(H,n);                    
                   	  printf("\n\nAfter HeapSort is :\n ");
	                  for(i=1;i<=n;i++)
                  	     printf("%d ",H[i]);
                            printf("\n");
                      copyArray(n,H,A);//vector refacut
                      
                                                                             
	}	
	if(ind==1)
     {
         	//t=100;
         	fprintf(f1,"n;nropH;nropQ;\n");                     	
         	/* Generate 100 test cases , each case having 5 vectors to reapeat the measurements for
             Heapsort and QuickSort for avg case .  */           
            
             for(int i=100;i<=10000;i=i+100)
	          {       int m1,m2;
                      m1=0;m2=0;                                                       
         	          eval=1;  
                      
                       nropH=0;    
                        nropQ=0;                                      
                  for(j=1;j<=5;j++)
	                {                           
                        random(i);       //generate H[] random                                               
                        copyArray(i,A,H);// in A[] keep a copy of H[] vector                        	                    
                        HeapSortBu(H,i);
                        m1=m1+nropH;
                        printf("AVG Case HeapSort(BU) %d.\n",i);	                    

	                                                         	
	                    copyArray(i,H,A); //H [] is assigned initial vector 	                                        	                    
	                    QuickSort(H,1,i);
	                    m2=m2+nropQ;
                        printf("AVG Case QuickSort %d.\n",i);
	                    copyArray(t,H,A);	                                                                   	                 	                 	
                  
                 }//end for2
              fprintf(f1,"%d;%d;%d;\n",i,m1/5,m2/5);       	    	        
	   }//end for1	  
              	  
	   fprintf(f2,"n;nAC;nWC;nBC;\n");
       	  	      	         
       for(int k=100;k<=10000;k=k+100)  
       {    //printf("Step :%d\n",k);
	            fprintf(f2,"%d;",k);
                                
          /* Average case testing.*/                  
                          
             int media=0;              	                  	                 		                      
             for(int i=1;i<=5;i++)
                {       nropQ=0;                                           
                        random(k); //generate H[] randomly
                        n=k;                             
                        copyArray(n,A,H);//keep a copy of H[]   in A[]
                        eval=1;//choose the pivot - middle                               
	                    QuickSort(H,1,n);
                        media+=nropQ;		                   
                        copyArray(n,H,A);	
                                                                                                                             	                 	                 	                  
               }   
             printf("QuickSort AVG Case.\n"); 
             fprintf(f2,"%d;",media/5);                                         
              /* Worse case testing*/
              
             nropQ=0;
             
             randomC(k);
             copyArray(k,A,H);
             eval=0;
             QuickSort(H,1,n);	
             printf("QuickSort %d WC Case.\n",k); 	
             fprintf(f2,"%d;",nropQ);                  
              copyArray(k,H,A);
            
             /* Best case testing */
             
             nropQ=0;
            // t=k;
             randomC(k);
             copyArray(k,A,H);
             eval=1;
             QuickSort(H,1,n);	
             printf("QuickSort %d BC Case.\n",k); 	
             fprintf(f2,"%d;",nropQ);  
                         
         fprintf(f2,"\n"); 
        } //for 100...100000                          
  }//end if(in==0)
    getch();
    fclose(f1);
    fclose(f2); 
}
