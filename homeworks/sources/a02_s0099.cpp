#include <conio.h>
#include <stdio.h>
#include <math.h>
#include "profiler.h"
Profiler profiler("heap");

/*
At the top-down heapbuild the assignments and the comparisons have a 3 to 1 ratio in all cases, because every time a comparison is made, three
assignments are made aswell. At bottom up there isn't an identified relationship between the assignments and the comparisons but the values of 
them are much closer to each-other than at top down build. At bottom up the time complexitie is O(n/2*lg n) in worst case, and at top down the
time complexity is O(n);

*/


void maxHeapify(int arr[], int i,int n)
{
	int largest,temp;
	profiler.countOperation("BU-comp",n,0);
	profiler.countOperation("BU-ass",n,0);
	int l = 2*i;
	int r = 2*i+1;
	profiler.countOperation("BU-comp",n,2);
	if (l <= n && arr[l] > arr[i])
	{
		largest = l;
	}
	else{
		largest = i;
	}
	if (r <= n && arr[r] > arr[largest])
	{
		largest = r;
	}
	if (largest != i)
	{
		temp=arr[i];
		arr[i]=arr[largest];
		arr[largest]=temp;
		profiler.countOperation("BU-ass",n,3);
		maxHeapify(arr,largest,n);
	}
}

void buildHeapBU(int arr[],int n)
{
	for(int i = n/2;i >= 1;i--)
	{
		maxHeapify(arr,i,n);
	}
}

void insertNext(int arr[],int heapsize,int n){
	int i = heapsize + 1,temp; 
	int parent = i/2;
	profiler.countOperation("TD-comp",n,0);
	profiler.countOperation("TD-ass",n,0);
	profiler.countOperation("TD-comp",n);
	while(i > 1 && arr[parent] < arr[i]){
		profiler.countOperation("TD-comp",n);
		temp=arr[i];
		arr[i]=arr[parent];
		arr[parent]=temp;
		profiler.countOperation("TD-ass",n,3);
		i = parent;
		parent = i/2;
	}
}


void buildHeapTD(int a[],int n){
	for(int i = 1; i < n;i++){
		insertNext(a,i,n);
	}
}

void buildHeapTD2(int a[],int i, int n) 
{ 
 if(i <= (n-2)/2) 
{ 
buildHeapTD2(a, 2*i+1, n); 
buildHeapTD2(a, 2*i+2, n); 
maxHeapify(a,i,n); 
}
}

void printSpaces(int n){
	for(int i = 0;i < n;i++){
		printf(" ");
	}
}

int length(int number){
	int aux = number;
	int count = 0;
	if (aux < 0){
		count++;
	}
	while(aux != 0){
		aux = aux / 10;
		count++;
	}
	return count;
}

void printHeap(int a[],int n){
	int depth = 0;
	int space;
	int i,last,nr,l;
	while(depth <= 3){
		nr = (int)pow(2.0,depth);
		space = 80/(nr*2);
		i = nr;
		last = 2*nr - 1 ;
		l =0 ;
		printSpaces(space);
		while(i <= last && i <= n){
			
			l = length(a[i]);
			printf("%d",a[i]);
			i++;
			
			printSpaces(2*(space)-l);
			
		}
		depth++;
		printf("\n\n\n");
	}


}


int main()
{
	int arr[10000],aux[10000];
	int i;
	//int n = sizeof(arr) / sizeof(int);
	/*for(int i = 100;i < 10000;i += 100)
	{
		for(int j = 0;j < 5;j++)
		{
		FillRandomArray(arr,i+1);
		for(int k=0;k<i;k++)
			aux[k]=arr[k];
		//for (i=0;i<n;i++)
			//printf("%d ",arr[i]);
		//printf("\n");
		buildHeapTD(arr,i);
		//printHeap(arr,n-1);
		//printf("\n");
		for(int k=0;k<i;k++)
			arr[k]=aux[k];
		buildHeapBU(arr,i);
		profiler.createGroup("Assignment","TD-ass","BU-ass");
		profiler.createGroup("Comparisons","TD-comp","BU-comp");
		profiler.addSeries("Top Down","TD-comp","TD-ass");
		profiler.addSeries("Bottom Up","BU-comp","BU-ass");
		profiler.createGroup("All Average","Top Down","Bottom Up");
		}
	}*/
	/*
	for(int i = 100;i < 10000;i += 100)
	{
		FillRandomArray(arr,i+1,0,i,true,1);
		for(int k=0;k<i;k++)
			aux[k]=arr[k];
		buildHeapBU(arr,i);
		FillRandomArray(arr,i+1,0,i,true,1);
		buildHeapTD(arr,i);
		profiler.createGroup("Assignment","TD-ass","BU-ass");
		profiler.createGroup("Comparisons","TD-comp","BU-comp");
		profiler.addSeries("Top Down","TD-comp","TD-ass");
		profiler.addSeries("Bottom Up","BU-comp","BU-ass");
		profiler.createGroup("All Worst","Top Down","Bottom Up");
	}*/

	FillRandomArray(arr,10,0,9);
	for (i=1;i<10;i++)
			//printSpaces(80);
			printf("%d ",arr[i]);
	
		printf("\n");
		buildHeapTD(arr,i);
		printHeap(arr,10);
		for (i=1;i<10;i++)
			//printSpaces(80);
			printf("%d ",arr[i]);
		getch();
		//profiler.showReport();
		
		
		
}