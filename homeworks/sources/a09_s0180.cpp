/**Assignment No.10: DFS - First Search */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define INF 10000

typedef struct Prop
{
	int color, dist, parent;
} PROP;


// A structure to represent an adjacency list node
typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

typedef struct Queue 
{
	int val;
	struct Queue *next;
}QUEUE; 

QUEUE *start;
NODE *head[60001], *tail[60001];
PROP prop[60001];
int V[100001];
int timp;
int k, nr_op;
int QUE[10001], nrq;
int nivel=1;

int Q[60001];

void BFS (int s, int nrv)
{
	int u;
	for (u=1; u<=nrv; u++)
		if (u!=s)
		{
			prop[u].color=WHITE;
			prop[u].dist=INF;
			prop[u].parent=0;
		}
	prop[s].color=GRAY;
	prop[s].dist=0;
	prop[s].parent=0;
	k=1;
	Q[k]=s;
	nr_op+=5;
	printf("%d \n", s);
	while(k!=0)
	{
		u=Q[1];
		nivel++;
		/*for(int i=0; i<nivel; i++)
				printf(" ");*/
		//printf("%d \n", u);
		for (int i=1; i<k; i++)
		{
			Q[i]=Q[i+1];
			//nr_op++;
		}
		k--;
		Node *temp;
		temp=head[u];
		nr_op++;
		while (temp!=NULL)
		{
			int v=temp->val;
			temp=temp->next;
			nr_op+=3;
			if (prop[v].color==WHITE)
			{
				prop[v].color=GRAY;
				prop[v].dist=prop[u].dist+1;
				prop[v].parent=u;
				k++;
				Q[k]=v;
				for(int i=0; i<nivel; i++)
					printf(" ");
				printf("%d \n", v);
				nr_op+=4;
			}
			//nivel++;
		}
		//k--;
		prop[u].color=BLACK;
		nr_op++;
	}
}



bool verif (int x, int y)
{
	NODE *p;
	p=head[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->next;
	}
	return true;

}

void genereaza(int nrv,int nre)  //nrv= numarul de varfuri, nre = numarul de edges
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		head[i]=tail[i]=NULL;
	i=1;
	while (i<=nre)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
				if(head[u]==NULL)
				{
					head[u]=(NODE *)malloc(sizeof(NODE *));
					head[u]->val=v;
					head[u]->next=NULL;
					tail[u]=head[u];
					i++;
				}
				else {
						if (verif (u, v))
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;
							p->next=NULL;
							tail[u]->next=p;
							tail[u]=p;
							i++;
						}
					}
		}
	}
}


void print(int nrv)
{
	int i;
	NODE *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=head[i];
		while(p!=NULL)
		{
			printf("%d ",p->val);
			p=p->next;
		}
		printf("\n");
	}
}


int main()
{
	int nre, nrv;
	///TEST
	nrv=5;
	nre=10;
	srand(time(NULL));
	genereaza(nrv, nre);
	print(nrv);
	BFS(1, nrv);

	/*FILE *f;

	f=fopen ("rez1.txt", "w");
	fprintf(f, "nre nr_op\n");
	nrv=100;
	for (nre = 100; nre<=5000; nre=nre+100)
	{
		nr_op=0;
		genereaza(nrv, nre);
		BFS(1, nrv);
		fprintf(f, "%d %d\n", nre, nr_op);
	}
	fclose(f);

	f=fopen ("rez2.txt", "w");
	fprintf(f, "nrv nr_op\n");
	nre=9000;
	for (nrv = 110; nrv<=200; nrv=nrv+10)
	{
		nr_op=0;
		genereaza(nrv, nre);
		BFS(1, nrv);
		fprintf(f, "%d %d\n", nrv, nr_op);
	}
	fclose(f);*/

	printf("The end%c\n",7);
	getch(); 
    return 0;
}