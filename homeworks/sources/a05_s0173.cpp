#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 9973
int t[N];
int n;
int test[3000];
long gasite,negasite,maxGasite=0,maxNegasite=0;

void initializazaT()
{
	for(int i=0;i<N;i++)
		t[i]=0;
}

int h(int k,int i)
{
	int hprim;
	int x;
	hprim=k%N;
	x=(hprim+(1*i)+(2*i*i))%N;
	return x;
}

void dispersieInsereaza(int t[N],int k)
{
	int i=0;
	int j;
	do 
	{
		j=h(k,i);
		if (t[j]==0)
		{
			t[j]=k;
			return;
		}
		else
			i=i+1;
	}
	while(i<N);
	return;

}

int dispersieCauta(int t[N],int k)
{
 int i=0;
 int j;
 int nr=0;
	 do {
		j=h(k, i);
		nr++;
		if (t[j]==k) 
		{ 
			gasite+=nr;
			if (maxGasite<nr) 
			{
				maxGasite=nr;
			}
			return t[j]; 
		}
		else
			i++;
		
	 } while (t[j]!=0 && i<N);
		
	 negasite+=nr;
		if (maxNegasite<nr) 
		{
			maxNegasite=nr;
		}
	 return 0;
}


void afisare()
{
	for(int i=0;i<100;i++)
		printf("nr %d : %d, nr %d : %d\n ",i,test[i],i+1500,test[i+1500]);
}


void faVectorulTest()
{
	for(int i=0;i<1500;i++)
		test[i]=t[i];
	for(int i=1500;i<3000;i++)
		test[i]=t[i-1500]+1;
}

void main()
{
	FILE *f;
	srand(time_t(NULL));
	f=fopen("fisier.csv","w");
	double alfa[]={0.8,0.85,0.9,0.95,0.99};
	int k;
	for(int i=0;i<5;i++)
	{
		gasite=0;
		negasite=0;
		maxGasite=0;
		maxNegasite=0;
		
		n=(int)(alfa[i]*N);
		for(int b=0;b<5;b++)
		{
			initializazaT();
			for(int j=0;j<n;j++)
			{
				k=rand()+1;
				dispersieInsereaza(t,k);
			}
			faVectorulTest();
			//afisare();
			for(int l=0;l<3000;l++)
				dispersieCauta(t,test[l]);
		}

		fprintf(f,"%5f %10f %10d %10f %10d\n",alfa[i],(float)gasite/7500,maxGasite,(float)negasite/7500,maxNegasite);

		//afisare();
	}
	fclose(f);
	printf("S-a terminat !");
	
	getch();
}