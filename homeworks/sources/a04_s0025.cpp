// Tema4_SirDeListe.cpp : Defines the entry point for the console application.
// ***ANONIM*** ***ANONIM***, grupa: ***GROUP_NUMBER***

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>


//mai intai declar structura unui nod din lista
typedef struct nod
{
	int key;
	struct nod *next;
}nod;


int nr_operatii;
nod *a[10001];  //vectorul de liste
nod *prim, *curent, *ultim;  //necesari pentru consrtuirea listelor

//functia de intretinere a heap-ului minim, de construire a lui prin tehnica Bottom-up

int parinte(int i)
{
	return i/2;
}

int stang(int i)
{
	return 2*i;
}

int drept(int i)
{
	return (2*i)+1;
}


//functia MIN_HEAPIFY de intretinere a heap-ului minim

void MIN_HEAPIFY(nod *b[], int i, int n)
{
	int l=stang(i);
	int r=drept(i);
	int min;
	nod *aux;
	nr_operatii++;
	if ((l<=n) && (b[l]->key<b[i]->key))
		min=l;
	else min=i;
	nr_operatii++;
	if ((r<=n) && (b[r]->key<b[min]->key))
		min=r;
	if (min!=i)
	{
		nr_operatii=nr_operatii+3;
		aux=b[i];
		b[i]=b[min];
		b[min]=aux;
		MIN_HEAPIFY(b,min,n);

	}


}


//cream vectorul de liste prin Bottom-up

void BUILD_MIN_HEAP_BU(nod *b[], int n)
{
	int i;
	for (i=(n/2); i>=1; i--)
		MIN_HEAPIFY(b,i,n);

}


//Functie de verificare daca MIN HEAP-UL este gol, pentru a stii daca mai avem elemnte de inserat in lista finala sau nu

int verificare_heap(nod *b[], int n)
{
	int i;
	int egol=1;
	for (i=1; i<=n; i++)
	{
		if (b[i]!=NULL)
			egol=0;

	}
    return egol;
}


//Functia de inserare a unui nod in lista finala sortata

void inserareNod(int val)
{
	if (prim==NULL)
	{
		prim=(nod*) malloc(sizeof(nod*));
		prim->key=val;
		prim->next=NULL;
		ultim=prim;
	}

	else 
	{
		curent=(nod*) malloc(sizeof(nod*));
		curent->key=val;
		curent->next=NULL;
		ultim->next=curent;
		ultim=curent;

	}


}


//functie pentru afisarea listei sortate
void afisareListaFinala()
{
	curent=prim;
	while(curent!=NULL)
	{
		printf("%d ",curent->key);
		curent=curent->next;

	}


}


//Functia de creare a vectorului de liste, unde k este numarul de liste, iar n este numarul de elemente ale vectorului. 
//Fiecare element va avea n/k elemente
void creareVectorListe(int k, int n)
{
	nod *primul, *curentul, *ultimul;
	primul=NULL;
	curentul=NULL;
	ultimul=NULL;
	int i,j;
	srand(time(NULL));
	for (i=1; i<=k; i++)
	{
		primul=NULL;
	    curentul=NULL;
	    ultimul=NULL;
		for (j=0; j<n/k; j++)
		{
			if (primul==NULL)
			{
				primul=(nod*)malloc(sizeof(nod*));
				primul->key=rand()%100;
				primul->next=NULL;
				ultimul=primul;
             }
			else 
			{
				curentul=(nod*) malloc(sizeof(nod*));
				curentul->key=ultimul->key+j;
				curentul->next=NULL;
				ultimul->next=curentul;
				ultimul=curentul;
             }

     


		}
  a[i]=primul;



	}



}

//Functie pentru afisarea vectorului de liste

void afisareVectorListe(nod *b[], int n)
{
	 int i;
    nod *curent;
    curent=NULL;
    for (i=1;i<=n;i++)
    {
        curent=b[i];
        while(curent!=NULL)
        {
            printf("%ld ",curent->key);
            curent=curent->next;
        }
        printf("\n");
    }

}


//Functia care construieste lista finala, prin extragerea elementelor din heap min si inserarea lor in lista finala. 
//Lista finala va fi sortata

void construireListaFinala(nod *b[], int n)
{
	int p;
    int elem_scos;
    nod *aux;
    prim=NULL;
    curent=NULL;
    ultim=NULL;
    BUILD_MIN_HEAP_BU(b,n);
    p=n;
    while (verificare_heap(a,p)==0)
    {
        elem_scos=b[1]->key;
        inserareNod(elem_scos);
        if (b[1]->next!=NULL)
        {
            nr_operatii++;
			b[1]=b[1]->next;
        }
            else
        {
            nr_operatii=nr_operatii+3;
			aux=b[1];
            b[1]=b[p];
            b[p]=aux;
            p--;
        }
        MIN_HEAPIFY(b,1,p);
    }

}


void test()
{
	int n,k;
    n=20;
    k=4;
    creareVectorListe(k,n);
    afisareVectorListe(a,k);
    printf("\n");
    construireListaFinala(a,k);
    afisareListaFinala();
    printf("\n");
	getch();


}

void average_case_1()
{
    int n;
    FILE *f;
    f=fopen("Rezultate1.csv","w");
    fprintf(f,"n,k1=5,k2=10,k3=100\n");
    int k1=5,k2=10,k3=100;
    for (n=100;n<=10000;n=n+100)
    {
        fprintf(f,"%d,",n);

        nr_operatii=0;
        creareVectorListe(k1,n);
        construireListaFinala(a,k1);
        fprintf(f,"%d,",nr_operatii);

        nr_operatii=0;
        creareVectorListe(k2,n);
        construireListaFinala(a,k2);
        fprintf(f,"%d,",nr_operatii);

        nr_operatii=0;
        creareVectorListe(k3,n);
        construireListaFinala(a,k3);
        fprintf(f,"%d\n",nr_operatii);
    }
    fclose(f);
    nr_operatii=0;
}
void average_case_2()
{
    int n=10000;
    int k;
    FILE *f;
    f=fopen("Rezultate2.csv","w");
    fprintf(f,"k,nr_operatii\n");
    for (k=10;k<=500;k=k+10)
    {
        nr_operatii=0;
        creareVectorListe(k,n);
        construireListaFinala(a,k);
        fprintf(f,"%d,%d\n",k,nr_operatii);
    }
    fclose(f);
    nr_operatii=0;
}


int main()
{
	
	
	
	test(); //pentru testarea functionarii metodelor
	//average_case_1();
	//average_case_2();
	getch();
	return 0;
}

