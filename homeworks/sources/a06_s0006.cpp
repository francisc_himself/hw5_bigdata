/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

codul de fata implementeaza un algoritm de determinare a permutarii Josephus.

O. Rezultate asteptate:
	
	Algoritmul optimal pentru determinara permutarii Josephus are complexitatea O(n*log n). Aceasta complexitate se obtine folosind
	un arbore binar de cautare imbogatit, echilibrat,  pentru determinarea numarului ce trebuie scos din lista. 

I. Implementare:
	
	Pentru implementarea algoritmului folosesc urmatoarele functii:
		BuildTree  -  functie pentru construirea arborelui echilibrat												-- complexitate O(n)
		OSSelect   -  functie pentru determinarea elementului cu numarul de ordine i din sirul sortat				-- complexitate O(log n)
		DeleteNode -  functie pentru stergerea unui nod																-- complexitate O(log n)			
		
	Functia Josephus are complexitatea O(n * log n)

II. Verificarea corectitudinii

	Functia VerificaCorectitudine verifica algoritmii de BuildTree, OSSelect si DeleteNode pentru valor mici ale lui n.
	Functia VerificaJosephus verifica corectutudinea algoritmului folosit pentru determinarea permutarii Josephus.

III. Grafice:
	
	Graficul pentru numarul de operatii se gaseste in fisierul "grafic.html". Acest grafic dovedeste ca algoritmul are complexitatea O(n * log n)

IV. Concluzii:
	
	Algoritmul Josephus are complexitatea O(n log n) daca se foloseste o structura de arbore binar de cautare echilibrat, imbogatit cu dimensiune. 
	Pentru a pastra complexitatea dorita, gestionarea dimensiunii in cazul stergerii trebuie sa se faca in timp logaritmic.
*/

#include<stdio.h>
#include"Profiler.h"



typedef struct _MY_NODE
{
	int Data;                     // informatii utile
	int Size;					  // dimensiunea subarborelui
	struct _MY_NODE *Left;		  // subarbore stang
	struct _MY_NODE *Right;		  // subarbore drept
} MY_NODE;

MY_NODE *Root;

Profiler profiler("Permutarea Josephson");

int DimCurenta;

FILE *fout;

/*Functie pentru construirea unui arbore echilibrat
 -- functioneaza recursiv prin inserarea mijlocului intervalului
*/
MY_NODE* PopulateTree(int Left,int Right)
{
	MY_NODE *Nod;
	int Middle;

	profiler.countOperation("NrOperatii",DimCurenta,1);
	if(Left <= Right)
	{
		
		profiler.countOperation("NrOperatii",DimCurenta,1);
		Middle = (Left + Right)/2;

		profiler.countOperation("NrOperatii",DimCurenta,5);
		Nod = new MY_NODE;
		Nod->Data = Middle;
		Nod->Size = 1;
		Nod->Left = PopulateTree(Left, Middle-1);
		Nod->Right = PopulateTree(Middle+1,Right);


		profiler.countOperation("NrOperatii",DimCurenta,1);
		if(Nod->Left != NULL)
		{
			profiler.countOperation("NrOperatii",DimCurenta,1);
			Nod->Size += Nod->Left->Size;
		}

		profiler.countOperation("NrOperatii",DimCurenta,1);
		if(Nod->Right != NULL)
		{
			profiler.countOperation("NrOperatii",DimCurenta,1);
			Nod->Size += Nod->Right->Size;
		}

		return Nod;
	}
	
	return NULL;
}

//functie pentru initializarea constructiei arborelui echilibrat
void BuildTree(int n)
{
	profiler.countOperation("NrOperatii",DimCurenta,1);
	Root = PopulateTree(1,n);
}

//functie pentru schimbarea dimensiunii nodurilor
void UpdateInfo(MY_NODE *Root,int Data)
{
	profiler.countOperation("NrOperatii",DimCurenta,2);
	if(Root != NULL && Root->Data != Data)
	{
		profiler.countOperation("NrOperatii",DimCurenta,1);
		Root ->Size --;
		profiler.countOperation("NrOperatii",DimCurenta,1);
		if(Root->Data >Data)
		{
			UpdateInfo(Root->Left,Data);
		}
		else
		{
			UpdateInfo(Root->Right,Data);
		}
	}
	else if(Root != NULL && Root->Data == Data)
	{
		Root->Size --;
	}
}

//functie ce sterge un nod
void ReallyDeleteNode(MY_NODE**Node)
{
	MY_NODE **Ref,*ToDelete;

	Ref = Node;
	profiler.countOperation("NrOperatii",DimCurenta,1);
	if((*Node)->Left == NULL)
	{
		profiler.countOperation("NrOperatii",DimCurenta,1);
		ToDelete = (*Node);
		*Node = (*Node)->Right;
		delete ToDelete;
	}

	else 
	{
		if((*Node)->Right == NULL)
			{
				profiler.countOperation("NrOperatii",DimCurenta,1);
				ToDelete = (*Node);
				*Node = (*Node)->Left;
				delete ToDelete;
			}
		else 
		{
			profiler.countOperation("NrOperatii",DimCurenta,1);
			Ref = &(*Ref)->Right;
			profiler.countOperation("NrOperatii",DimCurenta,1);
			while((*Ref)->Left!= NULL)
			{
				profiler.countOperation("NrOperatii",DimCurenta,1);
				profiler.countOperation("NrOperatii",DimCurenta,1);
				(*Ref)->Size--;
				profiler.countOperation("NrOperatii",DimCurenta,1);
				Ref = &((*Ref)-> Left);
			}
			profiler.countOperation("NrOperatii",DimCurenta,1);
			(*Node)->Data = (*Ref)->Data;
			ReallyDeleteNode(Ref);
		}

	}
}

//functie pentru initializarea stergerii unui nod
void DeleteNode(MY_NODE **Node)
{
	UpdateInfo(Root,(*Node)->Data);

	ReallyDeleteNode(Node);

}

//functie pentru determinarea elementului cu indicele i din sirul sortat
MY_NODE** OSSelect(MY_NODE **Root, int i)
{
	int r;
	profiler.countOperation("NrOperatii",DimCurenta,2);
	if(Root != NULL && *Root != NULL)
	{
		profiler.countOperation("NrOperatii",DimCurenta,1);
		if((*Root)->Left != NULL)
		{
			profiler.countOperation("NrOperatii",DimCurenta,1);
			r = (*Root)->Left->Size +1;
		}
		else
		{
			profiler.countOperation("NrOperatii",DimCurenta,1);
			r = 1;
		}
		profiler.countOperation("NrOperatii",DimCurenta,1);
		if(i == r)
		{
			return Root;
		}
		profiler.countOperation("NrOperatii",DimCurenta,1);
		if(i < r)
		{
			return OSSelect(&(*Root)->Left,i);
		}
		profiler.countOperation("NrOperatii",DimCurenta,1);
		return OSSelect(&(*Root)->Right,i -r);
	}
}

//functie pentru afisarea formatata a unui arbore
void PreatyPrint(MY_NODE *Root,int Tab)
{
	int LocalTab;
	if(Root!= NULL)
	{
		LocalTab = Tab;
		PreatyPrint(Root->Right,Tab+1);

		while(LocalTab >0)
		{
			fprintf(fout,"         ");
			LocalTab --;
		}

		fprintf(fout,"(%d,%d)\n\n",Root->Data,Root->Size);
				
		PreatyPrint(Root->Left,Tab+1);

	}
}

//algoritmul pentru determinarea permutarii Josephus
void Josephus(int n,
			  int m)
{
	int j,k;
	MY_NODE **x;

	Root = NULL;
	BuildTree(n);
	j=1;
	for (k = n; k >= 1; k--)
	{
		j = (j + m - 2) % k +1;
		x = OSSelect(&Root,j);
		
		DeleteNode(x);
		PreatyPrint(Root,0);
		fprintf(fout,"=====================================\n");
	}

}


//=====================================================================================

//functie pendru verificarea corectitudinii algoritmilor
void VerificaCorectitudine()
{
	BuildTree(10);
	PreatyPrint(Root,0);

	printf("=================================================================\n");

	DeleteNode(&Root);
	PreatyPrint(Root,0);

	printf("=================================================================\n");

	DeleteNode(&Root);
	PreatyPrint(Root,0);
}

//functie pentru verificarea algoritmului Josephus
void VerificaJosephus()
{

	/*MY_NODE **x;
	BuildTree(10);
	PreatyPrint(Root,0);
	x = OSSelect(&Root,3);

	printf("am gasit: %d \n",(*x)->Data);
	
	DeleteNode(x);*/

	//PreatyPrint(Root,0);


	Josephus(50,25);
}

//functie pentru generearea graficelor
void GenereazaGrafice()
{
	for(DimCurenta = 100;DimCurenta <= 10000; DimCurenta+=100)
	{
		printf("%d...\n",DimCurenta);
		Josephus(DimCurenta,DimCurenta/2);
	}
	profiler.showReport();
}

int main()
{
	fout = fopen("Out.txt","w");
	//VerificaCorectitudine();
	VerificaJosephus();
	//GenereazaGrafice();
}