
#include <stdio.h>
#include <conio.h>
#include  <stdlib.h>
#include <time.h>
#include<math.h>

double asig_bu,asig_td,comp_bu,comp_td;

void generator_aleator(int* a, int nr_elem)
{

	int i; //contor

	srand (time(NULL));
	for (i=0;i<nr_elem;i++)
	{

		*(a+i) = rand() % 100000;
	}

}


int index_min(int *a,int i,int nr_elem)
{
    if(2*i>nr_elem)
    {
        return i;
    }

    int min =a[i]<a[2*i]?i:(2*i);

    if (2*i+1>nr_elem) return min;
    min=a[min]<a[2*i+1]?min:(2*i+1);
    return min;
}

void Rec_H(int *a, int nr_elem, int i)//reconstructie heapului
{
	int ind=0,st,dr,aux;
	st=2*i;
	dr=2*i+1;
	ind=index_min(a,i,nr_elem);
	comp_bu+=2;

	if(ind!=i)
	{
		aux=a[i];
		a[i]=a[ind];
		a[ind]=aux;
		asig_bu+=3;
	Rec_H(a,nr_elem,ind);
	}
}
int H_Pop(int *a,int nr_elem)
{
	int x;
	x=a[1];
	a[1]=a[nr_elem];
	comp_td++;
	nr_elem=nr_elem-1;
	Rec_H(a,nr_elem,1);
	return x;
}

void H_Push(int *a, int nr_elem, int x)
{
	nr_elem=nr_elem+1;
	a[nr_elem]=x;
	asig_td++;
	int i=nr_elem;
	while((i>1) && (a[i]<a[i/2]) && (comp_td++))
	{

		int t=a[i];
		a[i]=a[i/2];
		a[i/2]=t;
		i=i/2;
		asig_td+=3;
	}
}


void afiseaza(int *a,int nr_elem,int i)
{
    if(i>nr_elem) return;

    int t = i;

    afiseaza(a,nr_elem,2*i+1);

    while (t>0){
        printf("   ");
        t=t/2;
    }
    printf("%d\n",a[i]);

    afiseaza(a,nr_elem,2*i);
}


void top_Down(int *a, int *b,int nr_elem)
{
	int i;
	for(i=1;i<=nr_elem;i++)
		H_Push(b,i-1,a[i]);

}

void bottom_up(int *a,int nr_elem)
{
    int i;
    for(i=nr_elem/2;i>0;i--)
    {
        Rec_H(a,nr_elem,i);

    }
}



int main()
{
	int a[10000], s[10000], h[10000];
	int nr_elem, j,k;
	FILE *f;


	f=fopen("fisier.csv", "w");
	fprintf(f, "n, atr_BU, comp_BU, atr+comp BU, atr TD, comp TD, atr+comp TD \n");

	for(nr_elem=100; nr_elem<=10000; nr_elem=nr_elem+100)
	{
    asig_bu=0;
	comp_bu=0;
	asig_td=0;
	comp_td=0;

		for(j=0;j<5;j++)
		{
			generator_aleator(a,nr_elem);
			for( k=0; k<nr_elem; k++)
			{
				s[k]=a[k];
			}

			top_Down(a,h,nr_elem);
			bottom_up(s,nr_elem);
		}
	fprintf(f, "%d, %2f, %2f, %2f, %2f, %2f, %2f \n", nr_elem, asig_bu/5, comp_bu/5, (asig_bu+comp_bu)/5, asig_td/5, comp_td/5, (asig_td+comp_td)/5);
	printf("%d\n",nr_elem);
	}

	return 0;
}
