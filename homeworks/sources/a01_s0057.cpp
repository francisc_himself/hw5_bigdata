/****ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***
Sunt de parere ca metota bulelor se afla in caz favorabil cand vectorul este crescator,deoarece aceasta metoda nu mai executa
operatiile de asignare,iar in caz defavorabil se afla atunci cand vectorul este descrescator,deoarece la fiecare comparare va
trebui sa faca asignari.In cazut mediu statistic metoda are o eficienta apropiata de cel defavorabil,doar ca datorita vectorului
cu elemente aleatoare,numarul de asignari variaza.
Metota prin selectie se afla in caz favorabil cand vectorul este descrescator,deoarece aceasta metoda va mai putin operatii de
asignare,minimul fiind la final de fiecare data,iar cazul defavorabil este atunci cand vectorul este crescator,deoarece minumul 
fiind totdeauna la inceput,la fiecare operatie de comparare se face o asignare.Iar cazul mediu statistic este cazul in care
vectorul este aleator,asignarile si compararile fiind variabile.
Metoda prin insertie se afla in cazul favorabil atunci cand vectorul este descrescator deoarece while nu s-ar executa niciodata,
iar cazul defavorabil este atunci cand vectorul este ordonat crescator,deoarece ciclul while se va executa de cele mai multe ori.*/
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("demo");
void bubble(int A[],int n)
{
	int aux;
    for (int i=1;i<=n-1;i++)
		for(int j=0;j<=n-1;j++)
		{
			if(A[j]>A[j+1]) 
			{
                 
                 aux=A[j];
				 A[j]=A[j+1];
				 A[j+1]=aux;
				 profiler.countOperation("bubble_assign",n,3);
			}
         profiler.countOperation("bubble_comp",n,1);
		}
			

}
void selectie(int a[],int n)
{
	
	int aux1;
   for(int i=0;i<=n;i++)
   {
     int imin=i;
	 for(int j=i+1;j<=n;j++)
	 {
		 if(a[j]<a[imin])
		 {
            
			imin=j;
		    
		 }
		  profiler.countOperation("selectie_comp",n);
	 }
      aux1=a[imin];
	  a[imin]=a[i];
	  a[i]=aux1;
	  profiler.countOperation("selectie_assign",n,3);

   }
}
void insertie(int a[],int n)
{
  for(int i=1;i<=n;i++)
  {
    int x=a[i];
	int j=0;
	while(a[j]<x)
	{
        j++;
		profiler.countOperation("insertie_comp",n,1);
	}
	for(int k=i;k>=j+1;k--)
	{
		a[k]=a[k-1];
		profiler.countOperation("insertie_assign",n,1);
	}
	a[j]=x;
	profiler.countOperation("insertie_assign",n,3);
  }
}

void main()
{

	int n;
	int v[10000],v1[10000];
	//Sortare folosind un vector random
	/*for(n=100;n<1000;n+=100)
	{
		
      FillRandomArray(v,n);
	  printf("n=%d\n",n);
	  memcpy(v1,v,n*sizeof(int));
	  bubble(v1,n);
	  memcpy(v1,v,n*sizeof(int));
      selectie(v1,n);
	  memcpy(v1,v,n*sizeof(int));
	  insertie(v1,n);
	}*/
	//Sortare folosind un vector crescator
	for(n=100;n<1000;n+=100)
	{
		
      FillRandomArray(v,n,10,50000,false,1);
	  printf("n=%d\n",n);
	  memcpy(v1,v,n*sizeof(int));
	  bubble(v1,n);
	  memcpy(v1,v,n*sizeof(int));
      selectie(v1,n);
	  memcpy(v1,v,n*sizeof(int));
	  insertie(v1,n);
	}
	//Soratare folosind un vector descrescator
	/*for(n=100;n<1000;n+=100)
	{
		
     FillRandomArray(v,n,10,50000,false,0);
	  printf("n=%d\n",n);
	  memcpy(v1,v,n*sizeof(int));
	  bubble(v1,n);
	  memcpy(v1,v,n*sizeof(int));
      selectie(v1,n);
	  memcpy(v1,v,n*sizeof(int));
	  insertie(v1,n);
	}*/
	profiler.createGroup("comp","bubble_comp","selectie_comp","insertie_comp");
	profiler.createGroup("assign","bubble_assign","selectie_assign","insertie_assign");
	profiler.addSeries("Serie_bub","bubble_comp","bubble_comp");
	profiler.addSeries("Serie_in","insertie_comp","insertie_assign");
	profiler.addSeries("Serie_se","selectie_comp","selectie_assign");
	profiler.createGroup("Afis","Serie_bub","Serie_in","Serie_se");
	profiler.showReport();

}