#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>

typedef struct Node
{
	long inf;
	struct Node *next;
}Nod;


Nod *a[1000],*b[1000],*c[1000];

long operatii=0;
long heapSize;
Nod *sol[10001];
int k;// k: 5,10,100


void heapify(Nod *arr[], int i)
{
   int l,r,min;
   long aux;

   l=2*i;
   r=2*i+1;

   operatii++;
   if (l<=heapSize && arr[l]->inf<arr[i]->inf)
       min=l;
   else
       min=i;

   operatii++;
   if (r<=heapSize && arr[r]->inf<arr[min]->inf)
       min=r;
   
	if (min!=i)
       {
           operatii+=3;
           arr[0]=arr[i];
           arr[i]=arr[min];
		   arr[min]=arr[0];
           heapify(arr,min);
       }
}

void bh_bottomup(Nod *arr[],int n)
{
   int i;
   heapSize=n;
   for (i=heapSize/2;i>=1;i--)
       heapify(arr,i);
}

int main()
{
   FILE *f,*g,*f2,*f3;
   f=fopen("k1.txt","w");
   f2=fopen("k2.txt","w");
   f3=fopen("k3.txt","w");
   g=fopen("n.txt","w");
   int i,j,l,m,n,dim,nr,r;
   long op;
   srand(time(NULL));
  
   // Pentru k=4 liste si n=20 termeni in total: verific corectitudinea algoritmilor.
   /*   Initial inserez in fiecare din lista primul element.Apoi inserez restul elementelor in liste */

   k=4;
   n=20;
	for (i=1;i<=k;i++)
           {
                a[i]=new Nod;
                a[i]->inf=rand() % (k*3)+1;
                a[i]->next=NULL;
                Nod *first=new Nod;
			    first=a[i];
				printf("%d ",a[i]->inf);
               for (j=2;j<=n/k;j++)
               {
                   Nod *q=new Nod;
                   q->inf=1+first->inf+rand() % (k*3);
                   q->next=NULL;
                   first->next=q;
                   first=q;
				   printf("%d ",q->inf);   
               }
			    printf("\n");
           }
           dim=k;
           nr=1;
           bh_bottomup(a,dim);
		  
           while (dim!=0)
           {
               sol[nr]=new Nod;
               sol[nr]->inf=a[1]->inf;
               a[1]=a[1]->next;
			   sol[nr]->next=NULL;
			   nr++;

               if (a[1]==NULL)
               {
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }

               heapify(a,1);
           }
   // afisam
	for(j=1;j<=n;j++)
		printf("%d ",sol[j]->inf);
  
   //k este constant, k=5
	k=5;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
           operatii=0;

           for (l=1;l<=k;l++)
           {
               a[l]=new Nod;
               a[l]->inf=rand() % (k*2);
               a[l]->next=NULL;
               Nod *interm=new Nod;
			   interm=a[l];
               for (m=2;m<=i/k;m++)
               {
                   Nod *q=new Nod;
                   q->inf=interm->inf+rand() % (k*2);
                   q->next=NULL;
                   interm->next=q;
                   interm=q;
               }
           }

           dim=k;
           nr=1;

           bh_bottomup(a,dim);

           while (dim!=0)
           {
               sol[nr]=new Nod;
               sol[nr]->inf=a[1]->inf;
               a[1]=a[1]->next;
			   sol[nr]->next=NULL;
			   nr++;

               if (a[1]==NULL)
               {
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }

               heapify(a,1);
           }

            op+=operatii;

       }
       fprintf(f, "%d %ld\n",i,op/5);
   }

   //k constant, k=100
   	k=100;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
           operatii=0;

           for (l=1;l<=k;l++)
           {
               a[l]=new Nod;
               a[l]->inf=rand() % (k*2);
               a[l]->next=NULL;
               Nod *interm=new Nod;
			   interm=a[l];
               for (m=2;m<=i/k;m++)
               {
                   Nod *q=new Nod;
                   q->inf=interm->inf+rand() % (k*2);
                   q->next=NULL;
                   interm->next=q;
                   interm=q;
               }
           }

           dim=k;
           nr=1;

           bh_bottomup(a,dim);

           while (dim!=0)
           {
               sol[nr]=new Nod;
               sol[nr]->inf=a[1]->inf;
               a[1]=a[1]->next;
			   sol[nr]->next=NULL;
			   nr++;

               if (a[1]==NULL)
               {
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }

               heapify(a,1);
           }

            op+=operatii;

       }
       fprintf(f2, "%d %ld\n",i,op/5);
   }

   //k constant, k=10
   	k=10;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
           operatii=0;

           for (l=1;l<=k;l++)
           {
               a[l]=new Nod;
               a[l]->inf=rand() % (k*2);
               a[l]->next=NULL;
               Nod *interm=new Nod;
			   interm=a[l];
               for (m=2;m<=i/k;m++)
               {
                   Nod *q=new Nod;
                   q->inf=interm->inf+rand() % (k*2);
                   q->next=NULL;
                   interm->next=q;
                   interm=q;
               }
           }

           dim=k;
           nr=1;

           bh_bottomup(a,dim);
		   // cat timp mai am elemente in lista si practic mai am liste, pun totul in lista finala
           while (dim!=0)
           {
               sol[nr]=new Nod;
               sol[nr]->inf=a[1]->inf;
               a[1]=a[1]->next;
			   sol[nr]->next=NULL;
			   nr++;
			   // aici se termina o lista
               if (a[1]==NULL)
               {
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }

               heapify(a,1);
           }

            op+=operatii;

       }
       fprintf(f3, "%d %ld\n",i,op/5);
   }

 //n constant; n=10000
   for (i=10;i<=500;i=i+10)
    {
        op=0;
		for (j=1;j<=5;j++)
        {
            operatii=0;

            for (l=1;l<=i;l++)
            {
                a[l]=new Nod;
                a[l]->inf=rand() % (l * 3);
                a[l]->next=NULL;
                Nod *interm=new Nod;
				interm=a[l];
                for (m=2;m<=10000/i;m++)
                {
                    Nod *q=new Nod;
                    q->inf=interm->inf+rand() % (l * 3);
                    q->next=NULL;
                    interm->next=q;
                    interm=q;
                }
            }

            dim=i;
            nr=1;

            bh_bottomup(a,dim);

            while (dim!=0)
            {
               sol[nr]=new Nod;
               sol[nr]->inf=a[1]->inf;
               a[1]=a[1]->next;
			   sol[nr]->next=NULL;

                if (a[1]==NULL)
                {
                    a[1]=a[dim];
                    dim--;
					heapSize--;
                }

                heapify(a,1);
            }

             op+=operatii;

        }
        fprintf(g,"%d %ld\n",i,op/5);
    }
fclose(f);
fclose(g);
getch();
return 0;
}


