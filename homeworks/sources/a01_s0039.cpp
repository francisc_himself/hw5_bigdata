# include <stdio.h>
# include <stdlib.h>
# include <conio.h>
# include <time.h>
/*
Observatii: Bubble sort e metoda cea mai putin eficienta , face cele mai multe comparari si atribuiri
*/
void bubble_sort (int v[10000], int n, long &nr_atribuiri_bubble, long &nr_comparari_bubble)
{
	int aux;

	for (int i=0; i<n-1; i++)
	{
		for (int j=0; j<n-1; j++)
		{
			nr_comparari_bubble++;
			if (v[j] > v[j+1])
			{
				nr_atribuiri_bubble = nr_atribuiri_bubble+3;
				aux = v[j];
				v[j] = v[j+1];
				v[j+1] = aux;
			}
		}
	}
}

void sortare_selectie (int v[10000], int n, long &nr_atribuiri_selectie, long &nr_comparari_selectie)
{
	int i_min;
	int aux;

	for (int i=1; i<n; i++)
	{
		i_min = i;
		for (int j=i+1; j<=n; j++)
		{
			nr_comparari_selectie++;
			if (v[i_min] > v[j])
				i_min = j;
		}
		nr_atribuiri_selectie+=3;
		aux =v[i];
		v[i] = v[i_min];
		v[i_min] = aux;
	}
}

void sortare_insertie (int v[10000], int n, long &nr_atribuiri_insertie, long &nr_comparari_insertie)
{
	int x;
	int j;

	for (int i=2; i<=n; i++)
	{
		nr_atribuiri_insertie++;
		x = v[i];
		j = 1;
		nr_comparari_insertie++;
		while (j<i && v[j]<x)
		{
			nr_comparari_insertie++;
			j++;
		}
		for (int k=i-1; k>=j; k--)
		{
			nr_atribuiri_insertie++;
			v[k+1] = v[k];
		}
		nr_atribuiri_insertie++;
		v[j] = x;
	}
}
void save_vector (int v[], int c[], int n) // Functie de salvare a vectorului
{
	for (int i=1; i<=n; i++)
		c[i] = v[i];
}

void sortare_caz_favorabil() //Elementele vectorului sunt sortate crescator
{
	int v[10000];
	int copy_of_v[10000];
	long nr_atribuiri,nr_comparari;
	FILE *f1;
	f1=fopen("favorabil_bubbleSort.csv","w");
	FILE *f2;
	f2=fopen("favorabil_selectie.csv","w");
	FILE *f3;
	f3=fopen("favorabil_insertie.csv","w");
	fprintf(f1,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	fprintf(f2,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	fprintf(f3,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	printf("Status: Incepe sortarile caz favorabil... \n");
	for (int n=100;n<10000;n=n+100)
	{
		for (int i=0;i<n;i++)
		{
			v[i]=i;
		}
			//apelam bubble sort sirului
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			nr_atribuiri=0; // Numaram atribuirile de la 0
			nr_comparari=0; // Numaram comparatiile de la 0
			printf("Caz: favorabil \t Status: bubble \t n=%d \n",n);
			bubble_sort(copy_of_v,n,nr_atribuiri,nr_comparari);
			fprintf(f1,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri,nr_comparari,nr_atribuiri+nr_comparari); //Scriem in fisier rezultatele

			//apelam sortarea selectie
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			nr_atribuiri=0; // Numaram atribuirile de la 0
			nr_comparari=0; // Numaram comparatiile de la 0
			printf("Caz: favorabil \t Status: selectie \t n=%d \n",n);
			sortare_selectie(copy_of_v,n,nr_atribuiri,nr_comparari);
			fprintf(f2,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri,nr_comparari,nr_atribuiri+nr_comparari); //Scriem in fisier rezultatele

			//apelam sortarea insertie
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			nr_atribuiri=0; // Numaram atribuirile de la 0
			nr_comparari=0; // Numaram comparatiile de la 0
			printf("Caz: favorabil \t Status: insertie \t n=%d \n",n);
			sortare_insertie(copy_of_v,n,nr_atribuiri,nr_comparari);
			fprintf(f3,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri,nr_comparari,nr_atribuiri+nr_comparari); //Scriem in fisier rezultatele
	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

void sortare_caz_defavorabil() //Elementele vectorului sunt sortate descrescator
{
	int v[10000];
	int copy_of_v[10000];
	long nr_atribuiri,nr_comparari;
	FILE *f1;
	f1=fopen("defavorabil_bubbleSort.csv","w");
	FILE *f2;
	f2=fopen("defavorabil_selectie.csv","w");
	FILE *f3;
	f3=fopen("defavorabul_insertie.csv","w");
	fprintf(f1,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	fprintf(f2,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	fprintf(f3,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	printf("Status: Incepe sortarile caz defavorabil... \n");
	for (int n=100;n<10000;n=n+100)
	{
		for (int i=0;i<n;i++)
		{
			v[i]=n-i+1;
		}
			//apelam bubble sort sirului
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			nr_atribuiri=0; // Numaram atribuirile de la 0
			nr_comparari=0; // Numaram comparatiile de la 0
			printf("Caz: defavorabil \t Status: bubble \t n=%d \n",n);
			bubble_sort(copy_of_v,n,nr_atribuiri,nr_comparari);
			fprintf(f1,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri,nr_comparari,nr_atribuiri+nr_comparari); //Scriem in fisier rezultatele

			//apelam sortarea selectie
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			nr_atribuiri=0; // Numaram atribuirile de la 0
			nr_comparari=0; // Numaram comparatiile de la 0
			printf("Caz: defavorabil \t Status: selectie \t n=%d \n",n);
			sortare_selectie(copy_of_v,n,nr_atribuiri,nr_comparari);
			fprintf(f2,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri,nr_comparari,nr_atribuiri+nr_comparari); //Scriem in fisier rezultatele

			//apelam sortarea insertie
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			nr_atribuiri=0; // Numaram atribuirile de la 0
			nr_comparari=0; // Numaram comparatiile de la 0
			printf("Caz: defavorabil \t Status: insertie \t n=%d \n",n);
			sortare_insertie(copy_of_v,n,nr_atribuiri,nr_comparari);
			fprintf(f3,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri,nr_comparari,nr_atribuiri+nr_comparari); //Scriem in fisier rezultatele
	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

void sortare_caz_mediu() //Elementele vectorului sunt aleatoare
{
	int v[10000];
	int copy_of_v[10000];
	long nr_atribuiri_bubble,nr_comparari_bubble,nr_atribuiri_selectie,nr_comparari_selectie,nr_comparari_insertie,nr_atribuiri_insertie;
	long nr_atribuiri_total_bubble,nr_atribuiri_total_selectie,nr_atribuiri_total_insertie,nr_total_comparari_bubble,nr_total_comparari_selectie,nr_total_comparari_insertie;
	FILE *f1;
	f1=fopen("mediu_bubbleSort.csv","w");
	FILE *f2;
	f2=fopen("mediu_selectie.csv","w");
	FILE *f3;
	f3=fopen("mediu_insertie.csv","w");
	printf("Status: Incepe sortarile caz mediu... \n");
	fprintf(f1,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	fprintf(f2,"n,atribuiri,comparatii,atribuiri+comparatii\n");
	fprintf(f3,"n,atribuiri,comparatii,atribuiri+comparatii\n");

	srand (time(NULL));

	for (int n=100;n<10000;n=n+100)
	{

		nr_atribuiri_total_bubble=0;
		nr_atribuiri_total_selectie=0;
		nr_atribuiri_total_insertie=0;
		nr_total_comparari_bubble=0;
		nr_total_comparari_selectie=0;
		nr_total_comparari_insertie=0;

		for (int k=1;k<=5;k++)
		{
			nr_comparari_bubble=0;
			nr_atribuiri_bubble=0;
			nr_comparari_insertie=0;
			nr_atribuiri_insertie=0;
			nr_comparari_selectie=0;
			nr_atribuiri_selectie=0;
			for (int i=0;i<n;i++)
			{
				v[i]=rand() % 5000;
			}
			//apelam bubble sort sirului
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			printf("Caz: mediu \t Status: bubble \t n=%d \t k=%d \n",n,k);
			bubble_sort(copy_of_v,n,nr_atribuiri_bubble,nr_comparari_bubble);
			nr_total_comparari_bubble=nr_total_comparari_bubble+nr_comparari_bubble;
			nr_atribuiri_total_bubble=nr_atribuiri_total_bubble+nr_atribuiri_bubble;

			//apelam sortarea selectie
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			printf("Caz: mediu \t Status: selectie \t n=%d \n",n);
			sortare_selectie(copy_of_v,n,nr_atribuiri_selectie,nr_comparari_selectie);
			nr_total_comparari_selectie=nr_total_comparari_selectie+nr_comparari_selectie;
			nr_atribuiri_total_selectie=nr_atribuiri_total_selectie+nr_atribuiri_selectie;

			//apelam sortarea insertie
			save_vector(v,copy_of_v,n); //Salvam vectorul sortat intrun vector auxiliar asupra caruia apelam functiile de sortare
			printf("Caz: mediu \t Status: insertie \t n=%d \n",n);
			sortare_insertie(copy_of_v,n,nr_atribuiri_insertie,nr_comparari_insertie);
			nr_total_comparari_insertie=nr_total_comparari_insertie+nr_comparari_insertie;
			nr_atribuiri_total_insertie=nr_atribuiri_total_insertie+nr_atribuiri_insertie;
		}
		
		fprintf(f1,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri_total_bubble /5,nr_total_comparari_bubble /5,(nr_atribuiri_total_bubble+nr_total_comparari_bubble)/5);
		fprintf(f2,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri_total_selectie /5,nr_total_comparari_selectie /5,(nr_atribuiri_total_selectie+nr_total_comparari_selectie)/5);
		fprintf(f3,"%ld,%ld,%ld,%ld\n",n,nr_atribuiri_total_insertie /5,nr_total_comparari_insertie /5,(nr_atribuiri_total_insertie+nr_total_comparari_insertie)/5);
	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
} 

int main()
{
	int k;
	printf("================================================================================ \n");
	printf("Status: Inceputul Programului! \n");
	printf("================================================================================ \n");
	sortare_caz_favorabil();
	sortare_caz_defavorabil();
	sortare_caz_mediu();
	printf("================================================================================ \n");
	printf("Status: Sfarsitul Programului! \n");
	printf("================================================================================ \n");
	getch();
	return 0;
}