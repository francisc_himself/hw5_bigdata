#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct Nod{
	int key;
	int size;
	Nod *left,*right,*parrent;
}nod;

nod *rad;
int nrOp=0;

nod* createNod(int key,int size,nod* left,nod* right){
	nod* x=(nod*) malloc(sizeof(nod));
	x->key=key;
	x->size=size;
	x->left=left;
	x->right=right;
	x->parrent=NULL;
	if(left!=NULL)
		left->parrent=x;
	if(right!=NULL)
		right->parrent=x;
	nrOp++;
	return x;
	
}


nod* buildTree(int a,int b){
	int c=(a+b)/2;
	if(a>b)
		return NULL;
	if(a==b)
		return createNod(c,1,NULL,NULL);
	if(a<b){
		nod *left=buildTree(a,c-1);
		nod *right=buildTree(c+1,b);
		return createNod(c,(b-a+1),left,right);
	}
}

int OSRang(nod* x){
	int rang;
	if(rang=(x->left!=NULL))
		x->left->size+1 ;
		else x->left->size=1;
	nod* y=x;
	while(y!=rad){
		if(y==(y->parrent)->right)
			rang+=(y->parrent)->left->size+1;
		y=y->parrent;
	}
	return rang;
}
//selecteaza index-lea element din arborele cu radacina toSelect
nod* OSSelect(nod* x,int i){
	int r=1;
	if(x->left!=NULL)
		r=x->left->size+1;
	if(r==i)
		return x;
	if(i<r)
		return OSSelect(x->left,i);
	else
		return OSSelect(x->right,i-r);
}

nod* treeMinimum(nod* x){
	while(x->left!=NULL){
		x=x->left;
		nrOp++;
		nrOp++;
	}
	return x;
}

nod* treeSuccesor(nod* x){
	nrOp++;
	if(x->right!=NULL)
		return treeMinimum(x);
	nod* y=x->parrent;
	while(y!=NULL && x==y->right){
		x=y;
		y=y->parrent;
		nrOp++;
		nrOp++;
	}
	return y;
}

void refaceLeg(nod** u,nod** v){
	nrOp++;
	nrOp++;
	if((*u)->parrent==NULL) 
		rad=(*v);
	else
		if((*u)==(*u)->parrent->left) 
			(*u)->parrent->left=(*v);
		else				
			(*u)->parrent->right=(*v);
		if((*v)!=NULL)					
			(*v)->parrent=(*u)->parrent;
}


//sterge un element din arborele
void treeDelete(nod *z){
	nod *p;
	if(z->left==NULL){
		nrOp++;
		p=z->parrent;
		while(p!=NULL){
		p->size--;
		p=p->parrent;
		nrOp++;
		nrOp++;
	}
		refaceLeg(&z,&(z->right));
	}
	else
		if(z->right==NULL){
			nrOp++;
			
			while(p!=NULL){
		p->size--;
		p=p->parrent;
		nrOp++;
		nrOp++;
	}
			refaceLeg(&z,&(z->left));
		}
		else{
			nod *y=treeMinimum(z->right);
			nrOp++;
			nod *t=y->parrent;
			while(t!=NULL){
		t->size--;
		t=t->parrent;
		nrOp++;
		nrOp++;
	}
			if(y->parrent!=z){
				refaceLeg(&y,&(y->right));
				y->right=z->right;
				y->right->parrent=y;				
			}
			refaceLeg(&z,&y);
			y->left=z->left;
			y->left->parrent=y;
			y->size=z->size;
		}
}

void prettyPrint(nod* rad,int i){
	if (rad->right!=NULL){
		prettyPrint(rad->right,i+1);
	}
	for (int m = 0; m<i; m++)
		printf("\t");
	printf("%d (%d) \n",rad->key,rad->size-1);
	if (rad->left!=NULL){
		prettyPrint(rad->left,i+1);
	}
}

void josephus(int n,int m){
	rad=buildTree(1,n);
	int j=1;
	for(int k=n;k>0;k--){
		j =((j+m-2) % k)+1;
		nrOp++;
		nod* x=OSSelect(rad,j);
		prettyPrint(rad,0);
		printf(" elimina nodul %d\n ",x->key);
		treeDelete(x);
	}
}

void main(){
	/**FILE *f;
	f=fopen("josephus.csv","w");
	fprintf(f,"Josephus result,nrDeOperatii\n");
	for(int i=100;i<10000;i+=100){
		nrOp=0;
		josephus(i,i/2);
		fprintf(f,"%d, %d\n",i,nrOp);
	};*/
	josephus(7,3);
	//fclose(f);
	//printf("gata\n");
	_getch();
}

