#include <time.h>
#include <list>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"


/* ***ANONIM*** ***ANONIM***  grupa ***GROUP_NUMBER***
  Eficienta  O(n logk) este obtinuta prin : 
	- parcurgerea a k liste de lungime n determina o eficienta de O(n * k),dar k fiind cunoscut il consideram ca si constanta 
	rezultand o efiecienta de O(n);
	- operatiile de POP si PUSH pe HEAP se executa cu o eficienta de O(logK) fiecare ;
	- pentru mentinerea eficientei O(n logK) sa sa folosim operatiile de scriere si citire pe lista rezultat fiecare
	avand eficienta O(1) si doi poantori indicand capul si sfarsitul listei rezultat;
*/



int na,nc;	//nr atribuiri, comparatii
int lungime;

typedef struct struct_NOD
{
	int valoare;
	struct_NOD *next,*prev;
}NOD;
struct elem{
	int val;	 //valoare
	int i;	 //index
};

elem HH[1001];

NOD *head[1000];
NOD *tail[1000];

NOD *joinedHead,*joinedTail;


NOD *prim, *ultim;
void creare_lista_vida (NOD **prim, NOD **ultim)
{
	*prim = 0;
	*ultim = 0;
}
void inserare(NOD **h,NOD **t,int val)
{
	NOD *x=(NOD *) malloc(sizeof(NOD));

	x->valoare=val;

	if((*h)==NULL){

		*h=x;
		*t=x;
	}
	else {

		(*t)->next=x;
		*t=x;
	}
	x->next=NULL;
}
void deleteList(NOD **h,NOD **t, NOD **x)
{
	if ((*x)->prev!=NULL) (*x)->prev->next=(*x)->next;
	else
		(*h)=(*x)->next;
	if ((*x)->next!=NULL) (*x)->next->prev=(*x)->prev;
}
void afisare(NOD *h)
{
	while(h!=NULL)
	{printf("%d ,",h->valoare);
	h=h->next;
	}
}



int pr(int x){return x/2;}
int dr(int x){return 2*x+1;}
int st(int x){return 2*x;}
void reconstructieHeap(int i, int n) //->MIN heap
{
	int max;   ////in loc de max =min 
	int s=st(i);
	int d=dr(i);
	if((s<=n)&&HH[s].val<HH[i].val) max=s; // Max heap ">" si Min heap "<"
	else max=i;
	if((d<=n)&&(HH[d].val<HH[max].val)) max=d;
	nc+=2;
	//	profiler.countOperation("buComp",lungime,2);
	if (max != i) {elem aux=HH[i];
	HH[i]=HH[max];
	HH[max]=aux;
	reconstructieHeap(max,n);
	na+=3;
	//	profiler.countOperation("buAssign",lungime,3);
	}

}
void H_init(int *dim)
{
	*dim=0;

}

elem H_pop(int *dim)
{
	elem x;
	if(*dim>0)
	{	x=HH[1];
	HH[1]=HH[*dim];
	*dim=*dim-1;

	na+=2;
	reconstructieHeap(1,*dim);
	}
	return x;
}
void H_push(int *dim,int vaal,int ind)
{	 
	*dim=*dim+1;
	int i;
	i=*dim;
	//
	na+=1;
	HH[*dim].i=ind;
	HH[*dim].val=vaal;
	while (i>=1 && HH[i].val<HH[pr(i)].val)  //i/2 parintele
	{
		nc+=1;
		na+=3;
		
		elem aux=HH[i];
		HH[i]=HH[pr(i)];
		HH[pr(i)]=aux;
		i=pr(i);

	}

	nc+=1;
}
NOD* read(NOD **h,NOD **t)
{
	NOD *x;
	if(*h==NULL)
	{
		return NULL;
	}
	x=*h; 
	*h=x->next;
	//free(*h);
	return x;

}
//interclasare
void interclasare(NOD **hcap,NOD **tcap, int k,int *dim)
{

	NOD *X;
	H_init(dim);
	creare_lista_vida(&joinedHead,&joinedTail);
	for(int i=0;i<k;i++)
	{	na+=1;
		X=read(&(hcap[i]),&(tcap[i]));
		H_push(dim,X->valoare,i);
	}


	while(*dim>0)
	{ elem e;
	e=H_pop(dim); 
	inserare(&joinedHead,&joinedTail,e.val);
	X=read(&(hcap[e.i]),&(tcap[e.i]));
	if(X!=NULL) H_push(dim,X->valoare,e.i);
	na+=2;
	nc+=1;
	}
	
}
void creazaLista(int i,int n)
{
	//srand((unsigned)time(NULL));
	int maxdiff=10;
	int x=1;
	
	for (int j=0;j<n;j++)
	{
		//int index=rand()%i;
		x=rand()%maxdiff+x;
		inserare(&(head[i]),&(tail[i]),x);
	}
//afisare(head[i]);
}
int main()
{   
	srand((unsigned)time(NULL));
	
	//test
	int v1[11]={1,2,3,4,5,6,7,8,9,10};
	int v2[11]={10,20,30,40,50,60,70,80,90,100};
	int v3[11]={15,25,35,45,55,65,75,85,95,105};

	for(int j=0;j<3;j++)
	{int v[11]={0,0,0,0,0,0,0,0,0,0};

	if(j==0) memcpy(v, v1, (10) * sizeof(int)); 
	if(j==1) memcpy(v, v2, (10) * sizeof(int)); 
	if(j==2) memcpy(v, v3, (10) * sizeof(int)); 
	for(int i=0; i<10;i++)
	{

		inserare(&(head[j]),&(tail[j]),v[i]);
	}	
	afisare(head[j]);
	printf("\n");

	}
	int dimensiune=10;
	interclasare(head,tail,3,&dimensiune);

	afisare(joinedHead);

	///////////end testare
	// return 0;
	//////k constant ,n variabil 100-1000
	//int k=5;
	FILE *pf;

	pf = fopen("kconst.csv","w");
	fprintf(pf,"k, n, na+nc\n");
	creare_lista_vida(head,tail);
	
	int n =10000;
	for(int k=10;k<=250;k+=10){
			
		for(int j=0;j<k;j++)	
			{


			lungime=n;
			printf("\n n=%d   k=%d\n", n,k);
			creazaLista(j,n/k);
			
			}
		
		na=0,nc=0;
		interclasare(head,tail,k,&dimensiune);
		fprintf (pf,"%d , %d , %d\n",k,n,na+nc);
		
		
	}
		//// k variabil , n -const
	return 0;
	
	
creare_lista_vida(head,tail);
	int k;

	FILE *f=fopen ("dateIn.csv","w");
	fprintf(f,"n, k10, k20, k100\n");
	
	for(int n=300;n<7000;n+=300){
	for(int i=0;i<3;i++){
		if(i==0) k=10; 
		if(i==1) k=20;
		if(i==2) k=100;
		na=0,nc=0;
		for(int j=0;j<k;j++)	
			{


			lungime=n;
			printf("\n n=%d   k=%d\n", n,k);
			creazaLista(j,n);
			
			}
		
		interclasare(head,tail,k,&dimensiune);
		switch (k)
			{
				case 10:
						fprintf (f,"%d, %d,",n,na+nc); 
					
						break;
				case 20:
						fprintf (f,"%d, ",na+nc);
					
						break;
				case 100:
						fprintf (f,"%d \n",na+nc);
						
						break;
			}
		
	}
	
	
	}

	fclose(f);
	fclose(pf);	
}
