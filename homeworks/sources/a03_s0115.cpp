
/*
 Quicksort is more efficient than Heapsort. HeapSort - nlogn (average case)
 
 QuickSort:
 
 Best: the array is divided into 2 equal parts by each partition
 Best case - nlogn time

 Average: it can easily be seen that the average case is similar to the best case

 Worst: each partition divides the array into 1 ***ANONIM*** the rest of the elements resulting in 
recursive call on 1 ***ANONIM*** n-1 elements
 Worst case - O(n^2) time


*/

#include<iostream>
#include<math.h>
#include<conio.h>
#include "Profiler.h"
#define MAX 10000
using namespace std;

Profiler profiler("Heapsort vs Quicksort");

int size,n;


void Print(int A[],int n){
	
	for(int i=1;i<=n;i++){
		cout<<A[i]<<" ";
	}

}

void heapify(int A[],int i,int n){
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
	

	profiler.countOperation("CompBU",n);
	if(l<=n && A[l]>A[i])
	{largest=l;}
	else 
	{largest=i;}

	profiler.countOperation("CompBU",n);
	if(r<=n && A[r]>A[largest])
	{largest=r;}


	if(largest!=i)
	{
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		profiler.countOperation("AssignBU",n,3);
		heapify(A,largest,n);
	}
	

}


void BuildMaxHeap(int A[],int n){
	size=n;
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}

void HeapSort(int A[]){
	int i,aux;
	BuildMaxHeap(A,n);
	for(i=n;i>=2;i--){
		aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		profiler.countOperation("AssHeapsort",n,3);
		size--;
		heapify(A,1,n);
	}

}

int PartitionBest(int A[], int p, int r){
	int x,i,j,aux;
	x=A[(r+p)/2];
	profiler.countOperation("AssQuicksort",n);
	i=p-1;
	for(j=p;j<=r-1;j++){
		profiler.countOperation("CompQuicksort",n);
		if(A[j]<=x){
			i++;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
			profiler.countOperation("AssQuicksort",n,3);
			}
	}
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;
	profiler.countOperation("AssQuicksort",n,3);
return i+1;
}

int PartitionWorst(int A[], int p, int r){
	int x,i,j,aux;
	x=A[r];
	profiler.countOperation("AssQuicksort",n);
	i=p-1;
	for(j=p;j<=r-1;j++){
		profiler.countOperation("CompQuicksort",n);
		if(A[j]<=x){
			i++;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
			profiler.countOperation("AssQuicksort",n,3);
			}
	}
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;
	profiler.countOperation("AssQuicksort",n,3);
return i+1;
}

int Partition(int A[], int p, int r){
	int x,i,j,aux;
	x=A[r];
	profiler.countOperation("AssQuicksort",n);
	i=p-1;
	for(j=p;j<=r-1;j++){
		profiler.countOperation("CompQuicksort",n);
		if(A[j]<=x){
			i++;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
			profiler.countOperation("AssQuicksort",n,3);
			}
	}
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;
	profiler.countOperation("AssQuicksort",n,3);
return i+1;
}

void QuickSort(int A[],int p, int r){
	int q;
	if(p<r){
		q=Partition(A,p,r);
		QuickSort(A,p,q-1);
		QuickSort(A,q+1,r);
	}
}

void QuickSortBest(int A[],int p, int r){
	int q;
	if(p<r){
		q=PartitionBest(A,p,r);
		QuickSortBest(A,p,q-1);
		QuickSortBest(A,q+1,r);
	}
}

void QuickSortWorst(int A[],int p, int r){
	int q;
	if(p<r){
		q=PartitionWorst(A,p,r);
		QuickSortWorst(A,p,q-1);
		QuickSortWorst(A,q+1,r);
	}
}


void main(){

int A[MAX],B[MAX],C[MAX];
int i,j,op;

cout<<"0.Exit! \n1.Manual Input. \n2.Run automatic(r***ANONIM***om)!\n3.Best Quicksort!\n4.Average Quicksort!\n5.Worst Quicksort!.\nType your coice:";
cin>>op;

switch(op){
	case 0 :return; break;
	case 1: cout<<"Insert n=";cin>>n;
			for(i=1;i<=n;i++)
			{cout<<"A["<<i<<"]=";cin>>A[i];}
			
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

				QuickSort(B,1,n);
				HeapSort(C);

				Print(A,n);
				cout<<"\n\n";
				Print(B,n);
				cout<<"\n\n";
				Print(C,n);

			break;
	case 2:	
		
		for(i=100;i<10000;i=i+100){
			FillR***ANONIM***omArray(A,i,0,10000);
				n=i;
			for(int k=1;k<=5;k++){
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

				QuickSort(B,1,n);
				HeapSort(C);

		}}

				break;

	case 3:for(i=100;i<=10000;i=i+100){
			FillR***ANONIM***omArray(A,i,0,10000,false,1);
			n=i;
		   	for(j=1;j<=i;j++)
				{B[j]=A[j];}
				QuickSortBest(B,1,n);
		   }
		break;
	case 4:
		for(i=100;i<=10000;i=i+100){
			FillR***ANONIM***omArray(A,i,0,10000);
			n=i;
		   	for(j=1;j<=i;j++)
				{B[j]=A[j];}
				QuickSort(B,1,n);
		   }
		break;
	case 5:
		for(i=100;i<=10000;i=i+100){
			FillR***ANONIM***omArray(A,i,0,10000,false,1);
			n=i;
		   	for(j=1;j<=i;j++)
				{B[j]=A[j];}
				QuickSortWorst(B,1,n);
		   }
		break;
	default: cout<<"Invalid input"; return;
		break;
}


profiler.addSeries("Heapsort","CompHeapsort","AssHeapsort");
profiler.addSeries("Quicksort","CompQuicksort","AssQuicksort");

profiler.createGroup("Comparisons","CompHeapsort","CompQuicksort");
profiler.createGroup("Assignments","AssHeapsort","AssQuicksort");
profiler.createGroup("Sum","Heapsort","Quicksort");

if(op!=0 && op!=1 )
	profiler.showReport();




getch();
}
