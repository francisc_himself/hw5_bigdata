/*
Assign 9
***ANONIM*** ***ANONIM*** Razvan
Grupa ***GROUP_NUMBER***

Cerinta: implementarea algoritmului BFS.

Pentru rezolvarea problemei am definit o structura numita Nod, care contine toate informatiile
necesare despre un nod al grafului. Apoi, am definit o lista de noduri care va reprezenta 
multimea varfurilor grafului si inca 2 liste pentru reprezentarea vecinilor fiecarui nod.

Ca si functii, am creat una de initializare a listelor de adiacente cu valori random in
intervalul specificat, am creat functia care implementeaza algoritmul BFS si inca 
o funcite pentru afisare.

Functia principala BFS parcurge toate nodurile grafului, iar pe masura ce ajunge la un nod 
alb( adica nevizitat inca ) , il face gri si ii adauga in coada toti vecinii, pentru 
a-i vizita la urmatorii pasi.

Timpul de executie este : O(V+E)

*/

#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


//structura de tip nod
typedef struct _nod{
	int p;	//parinte
	int d;	
	char color;
	int val;	
	_nod *next;	
}Nod;


Nod *first[1000], *last[1000];	
Nod *nod[1000];	 //lista de noduri
int k=0;
int Q[1000];	//noduri vizitate
int OP;	//nr operatii



void afisare_lista(int nrNoduri)		//afisare liste de adiacenta
{
	Nod *p;
	for(int i=1;i<=nrNoduri;i++)
	{
		printf("%d : ",i);		//nodul i
		p=first[i];
		while (p!=NULL)
		{
			printf("%d,",p->val);	//lista de adiac a lui i
			p=p->next;
		}
		printf("\n");
	}
}




void BFS ( int nrNoduri, int s)
{
	int i, u;

	for(i=1;i<=nrNoduri;i++)
	{
		OP++;
		if (i!=s)	//inainte de a aplica algoritmul
		{
			nod[i]=(Nod *)malloc(sizeof(Nod));
			nod[i]->color='w';			//setam toate varfurile albe = nevizitate , in afara de s
			nod[i]->d=1000;
			nod[i]->p=0;
			OP+=3;
		}
	}
	nod[s]=(Nod *)malloc(sizeof(Nod));
	nod[s]->color='g';			//nodul s = gri , adica e vizitat dar mai are vecini nevizitati
	nod[s]->d=0;
	nod[s]->p=0;
	OP+=3;
	k++;
	Q[k]=s;		//adaugare in coada


	while (k>0)
	{
		u=Q[1];		
		for(i=1;i<k;i++)
			Q[i]=Q[i+1];	//eliminare elem din coada
		k--;
		Nod *T;
		T = first[u];	//primul vecin al lui u
		printf("%d ", u);
		
		while(T!=NULL)
		{
			int val_T;
			val_T = T->val;
			T=T->next;		//T primeste valoarea vecinului sau
			OP++;

			if (nod[val_T]->color=='w')		//daca nodul e nevizitat
			{
				nod[val_T]->color='g';	//il facem vizitat, dar mai are vecini ce trebuie vizitati
				nod[val_T]->d=nod[u]->d+1;	//cheia lui = cheia parintelui + 1
				nod[val_T]->p=u;	//u - parintele
				OP+=3;
				k++;
				Q[k]=val_T;	//adaugam nodul in lista
			}
		}

		nod[u]->color='n';	//cand terminam cu toti vecinii lui u, il facem negru
	}
}


void lista_de_adiac(int nrMuchii, int nrNoduri)
{
	int u;
	int v;
	int i;

	for(i=0;i<=nrNoduri;i++)
		first[i]=last[i]=NULL;	//initializare
	i=1;
	while (i<=nrMuchii)
	{
		u=rand() % nrNoduri+1;	//generare random muchii
		v=rand() % nrNoduri+1;
		if(u!=v)
		{
			//daca lista e goala
			if (first[u]==NULL)		
			{
				first[u]=(Nod *)malloc(sizeof(Nod));
				first[u]->val=v;		//v = vecin al lui u
				first[u]->next=NULL;	//nu are alte elem in lista deocamdata
				last[u]=first[u];		//asadar, primul e totodata si ultimul
				i++;
			}
			//daca lista are elemente
			else
			{
				//verificare daca u=v
				bool g=true;
				Nod *p = first[u];
				while (p!=NULL)
				{
					if(v==p->val)
					{
						g=false;
						break;
					}	
					p=p->next;
				}
				//verificare incheiata 

				if (g==true)
				{
					Nod *p=(Nod *)malloc(sizeof(Nod));
					p->val=v;		//adaugam pe v in lista lui u
					p->next=NULL;
					last[u]->next=p;
					last[u]=p;
					i++;
				}

			}
		}

	}
}




int main()
{
	srand(time(NULL));
	int nrNoduri, nrMuchii, v, e;
	/*nrNoduri=9;
	nrMuchii=16;
	lista_de_adiac(nrMuchii, nrNoduri);
	afisare_lista(nrNoduri);
	printf("\n\n\n");
	BFS(nrNoduri, 1);
	*/

	FILE *f1, *f2;
	f1=fopen("fisier1.csv","w");
	fprintf(f1,"nrNoduri,nrMuchii,nrOperatii\n");

	nrNoduri = 100;
	for(nrMuchii=1000;nrMuchii<=5000;nrMuchii+=100)
	{
		OP=0;
		lista_de_adiac(nrMuchii,nrNoduri);
		BFS(nrNoduri,1);
		fprintf(f1,"%d,%d,%d\n",nrNoduri,nrMuchii,OP);
	}
	

	f2=fopen("fisier2.csv","w");
	fprintf(f2, "nrNoduri,nrMuchii,nrOperatii\n");

	nrMuchii=9000;
	for(nrNoduri=100;nrNoduri<=200;nrNoduri+=10)
	{
		OP=0;
		lista_de_adiac(nrMuchii,nrNoduri);
		BFS(nrNoduri,1);
		fprintf(f2,"%d,%d,%d\n", nrNoduri,nrMuchii,OP);
	}


	return 0;
}

