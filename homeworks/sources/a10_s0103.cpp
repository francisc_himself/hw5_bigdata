#include <stdio.h>
#include <stdlib.h>
#include <math.h>

///DFS with vertex coloring

typedef struct LIST
{
    int data;
    struct LIST *next;
};

LIST* sentinels[2000];
int op;
int topological[100];
int k=0;

void insert_header(int i)
{

	LIST* a = new(LIST);
	a->data=i;
	a->next=NULL;
	sentinels[i]=a;

}

void insert_in_list1(int i,int d)
{
	LIST* a = new(LIST);
	a->next=sentinels[i];
	a->data=d;
	sentinels[i]=a;
}
void insert_in_list(int i,int d)
{
	LIST* a = new(LIST);
	a->next=sentinels[i]->next;
	a->data=d;
	sentinels[i]->next=a;
}

int search_in_list(int i,int d)
{
	int ok=0;
	LIST* a = sentinels[i];
	while(a->next!=NULL)
	{
		if(a->data==d) ok=1;
		a=a->next;
	}
	return ok;
}

void delete_list(int i)
{
	LIST *a=sentinels[i];
	while(a->next!=NULL)
	{
	free(a);
	a=a->next;
	}
	//delete a;
	//a=NULL;
}

int color[1000],pred[1000]; //a-adjacency matrix
                                      //color-vector which stores the colors
                                      // 1-white ; 2-grey ; 3-black
                                      //pred-vector which stores the predecessor of vertex u
int n,time;  //n-nr of vertices
FILE *g;


void dfsvisit(int u)
{
	int d[105],f[105],i,adj;
	color[u]=2;	//set the color of vertex u to grey
	op+=2;
	time++;
	d[u]=time;  //discovery time
	fprintf(g,"%d ->",u); //print the vertex which is currently visited
	for (i=1;i<=n;i++)
	{
		adj=search_in_list(u,i); //i is the vertex which is adjacent to u
		if(adj==1){
			op++;
			if(color[i]==2)
			{
				topological[k]=u;
				k++;
			}
			else if (color[i]==1)  //if this vertex is unvisited
			{
				op++;
				pred[i]=u;   //the predecessor of vertex i is the currently visited vertex u
				dfsvisit(i); //call the dfsvisit function for every adjacent vertex to u
            }
		}
     color[u]=3;  //set the color of the visited vertex to black
	 time++;
	 f[u]=time;  //finished time
	 op+=2;
	 
	}
}


void dfs()
{
	int u;
	for(u=1;u<=n;u++)
	{
	color[u]=1;  //initially set every vertex's color to white
	pred[u]=NULL; //every vertex's predecessor is NULL
	time=0;
	}

	for(u=1;u<=n;u++)
		if (color[u]==1) //if the vertex is unvisited then visit it
 			{
				dfsvisit(u);
		    }
}



int main()

{
	/*
	int i,j,nr,m,b;
	freopen("in.txt","r",stdin);
	freopen("out.txt","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		scanf("%d %d",&m,&nr);
		for(j=0;j<m;j++)
		{
			scanf("%d",&b);
			insert_in_list1(nr,b);
		}

	}
	dfs();
	printf("\nTopological sort:");
	for(i=0;i<25;i++)
		printf("%d ",topological[i]);
	*/
	
	
	int i,v,e,v1,v2;
	op=0;
	FILE* f;
	n=100;
	f=fopen("out1.txt","w");
	g=fopen("out2.txt","w");

	for(e=1000;e<=5000;e=e+100)
	{
		fprintf(g,"\n%d\n",e);
		for(i=1;i<=100;i++)
		insert_header(i);
		i=0;
		while(i<e)
		{
			v = rand() % 100 + 1;
			v1 = rand() % 100 + 1;
			if(search_in_list(v,v1)!=1)
			{
			insert_in_list(v,v1);
			i++;
			}
		}
		dfs();
		fprintf(f,"%d %d\n",e,op);
		op=0;
	}
	/*
	for(v=100;v<=200;v=v+10)
	{
		i=0;
		while(i<9000)
		{
			v1 = rand() % v + 1;
			v2 = rand() % v + 1;
			if(search_in_list(v1,v2)!=1)
			{
			insert_in_list(v1,v2);
			i++;
			}
		}
		dfs();
		fprintf(f,"%d %d\n",v,op);
		op=0;
	}
	*/

	return 0;
}
