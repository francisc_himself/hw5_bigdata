#include <stdio.h>			
#include <stdlib.h>
#include <time.h>

int notfound=0, found=0;
int maxNotfound = 0, maxFound = 0;
int nrFound = 0, nrNotfound = 0;
int avgFound = 0, avgNotFound = 0;

int findPlace( int nr, int ii, int N )
{
	return ( nr % N + ii + ii*ii) % N;
}

void search( int nr, int table[], int N )
{
	int temp, index, ii;

	index = nr % N;
	ii = 0;
	temp = 0;
	while ( table[index] != -1 && table[index] != nr )
	{
		temp++;
		ii++;
		index = findPlace( nr, ii, N );
	}
	if ( table[index] == -1 )             
	{
		nrNotfound++;
		notfound += temp;
		avgNotFound = notfound/nrNotfound;
		if ( temp > maxNotfound ) maxNotfound = temp;
	}
	else
	{
		nrFound++;
		found += temp;
		avgFound = found/nrFound;
		if ( temp > maxFound ) maxFound = temp;
	}

}

int main()
{
	int n, N, index, nr, ii, i, k,j;
	double L;
	int table[10007];
	FILE *fp;
	int max=-99999;
	int tosearch[3001];
	int toput;

	fp = fopen( "hash.txt", "w");

	N = 10007;
	j = 0;
	fprintf(fp, "Avg. Effort not-found   Max. Effort not-found   Avg. Effort found   Max. Effort found\n");

	for ( k = 1; k <= 5; k++ )
	{
		if ( k == 1) 
			L = 0.8;
		else if ( k ==2 )
			L = 0.85;
		else if ( k == 3)
			L = 0.9;
		else if ( k == 4 )
			L = 0.95;
		else 
			L = 0.99;
	n = (int)(N * L);
	nrFound = 0, nrNotfound = 0;

	for ( i = 0; i < N; i++)
	{
		table[i] = -1; //empty
	}
   
	srand( time( NULL));
	//generate random numbers to put in hash table
	for ( i = 1; i <= n; i++ )
	{
		nr = rand() ;
		if (nr>max)
			max = nr;

		index = findPlace( nr, 0, N);
		ii = 0;
		while ( table[index] != -1 )
		{
			ii++;
			index = findPlace( nr, ii, N);
		}
		table[index] = nr;
	}

	found = 0;
	notfound = 0;
printf("asdfsadf %d", max);
	//average effort
	//generate random nrs to find in hashtable
    for ( i = 1; i <= 3000; i++)
	{
		toput = (rand() % (10000+1-0))+0;
		nr = (rand() % (100000+1-max))+max;
		if (i%2==0)
			tosearch[i] = table[toput];
		else tosearch[i] = nr;
		search(tosearch[i], table, N);
	}
	//fprintf(fp, "Avg. Effort not-found   Max. Effort not-found   Avg. Effort found   Max. Effort found\n");
	fprintf( fp, "%d	                   %d			           %d	               %d                    \n", avgNotFound, maxNotfound, avgFound , maxFound);
	
	}
	//fprintf( fp, "maxeffort for found: %d	maxeffort for not found: %d	  \n", maxFound, maxNotfound);
	fclose( fp);
	//scanf("%d", &n);
}