#include <stdio.h>
#include <stdlib.h>


typedef struct son_node{
                            int key;
                            int nr_fii;
                            struct son_node *son[10];
                            }son_node;

typedef struct binary_node{
                            int key;
                            struct binary_node *stg, *dr;
                            }binary_node;

son_node *root1;
binary_node *p;

son_node* multy_way (int a[], int n)
{
    son_node *root=NULL;
    son_node *nod[10];
    int i;

	for(i=1; i<=n; i++)
	{
	    nod[i] = (son_node *) malloc(sizeof(son_node));
	    nod[i] -> key = i;
	    nod[i] -> nr_fii = 0;

		if (a[i] == -1 )
			 root = nod[i];
	}

    for(i=1; i<=n; i++)
	{
		if (a[i] != -1){
            nod[a[i]] -> nr_fii ++;
            nod[a[i]] -> son[nod[a[i]]->nr_fii] = nod[i];

		}
	}
	return root;

}

void print_son (son_node *root, int nivel)
{
	int i;
	for (i=0;i<nivel;i++)
		printf("         ");
	printf ("%d\n",root->key);

	for (i=1;i<=root->nr_fii;i++)
		print_son(root->son[i],nivel+1);
}


binary_node* binary_tree (son_node* root)
{
    int i;
    binary_node *aux;
    p = (binary_node*) malloc(sizeof(binary_node));
    p->key = root -> key;
    p->stg = NULL;
    p->dr = NULL;
    son_node *root_aux;
    if(root->nr_fii > 0)
    {
        aux = p;
        p->stg = binary_tree(root->son[1]);
        for(i=2; i<= root->nr_fii; i++)
            p->dr = binary_tree(root->son[i]);
        p = aux;
    }


    return p;
}

void print_binar(binary_node *root, int nivel)
{
    int i;
    for(i = 0; i <= nivel; i++)
        printf("  ");
    printf("%d\n", root->key);
    if(root->stg != NULL)
        print_binar(root->stg, nivel+1);
    if(root->dr != NULL)
        print_binar(root->dr, nivel);
}

int main()
{
    int a[9],i;
    a[1] = 2;
    a[2] = 7;
    a[3] = 5;
    a[4] = 2;
    a[5] = 7;
    a[6] = 7;
    a[7] = -1;
    a[8] = 5;
    a[9] = 2;
    printf("Parent represantation:\n");
    for(i=1; i<=9; i++)
        printf("%d ", a[i]);
    printf("\nMulti-way tree:\n");
    root1 = multy_way(a,9);
    print_son(root1,0);
    printf("\nBinary tree:\n");
    binary_node *root2;
    root2 = binary_tree(root1);
    print_binar(root2,0);

}
