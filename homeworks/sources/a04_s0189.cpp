    /*
    --Name: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
    --Group: 30423
    --College: Technical University of Cluj Napoca
     
    */
     
    #include <conio.h>
    #include <stdio.h>
    #include <iostream>
    #include <fstream>
    #include <math.h>
    #include <stdlib.h>     /* srand, rand */
    #include <time.h>       /* time */
    using namespace std;
     
    //ofstream myfile; //files to write to
    int THERANDY = 0;
    int BUtotal = 0; int comp = 0; int assig = 0;
    int resultCount;
    int i;
    int A[10000];
    int B[10000];
    int heap[10001];
    int result[10000];
    static int heap_size = 0;
    ofstream myfile;
     
    #define Parent(i)         ((i-1)/2)
    #define Left(i)          (2*i+1)
    #define Right(i)        (2*i+2)
    #define swapT(x,y) int t;t=x;x=y;y=t;
     
    int pow(int a, int b){
            int rez=a;
            for (int i=0; i<b-1; i++)
                    rez=rez*a;
            return rez;
    }
     
    struct heapNode{
            int value;
            int originalList;
    };
     
    struct listNode { //list structure
            int value;
            listNode *nextNode;
    };
     
    static listNode *listStorage[10000]; //up to 500 value for k
    heapNode *heapStorage[10000]; //up to 10000 value for n
     
    //create an ordered linked list with random value increments
    listNode* generateOrderedList(int n){
            srand (time(NULL));
            listNode *origin;
            listNode *target;
            listNode *newNode;
            if (n>0){
                    origin = new listNode;
                    origin->value = rand() % 10 + THERANDY%9;
                    origin->nextNode = NULL;
                    target = origin;
                    if (n>1){
                            for(int i=1; i<n; i++){
                                    newNode = new listNode;
                                    newNode->value = target->value + rand() %10 + THERANDY%2; //increment value from last, randomly
                                    target->nextNode = newNode;
                                    newNode->nextNode = NULL;
                                    target = newNode;
                            }
                    }
            }
            else return NULL;
            return origin;
    }
     
    heapNode *generateNode(int k){
            heapNode *gloriousNode = new heapNode;
            gloriousNode->value = listStorage[k]->value;
            gloriousNode->originalList = k;
            listStorage[k] = listStorage[k]->nextNode;
            return gloriousNode;
    }
     
     
    void printTree(int A[], int A_length){
            //(this could have been done simpler recursively; adds 0s for nonexistent leaves on last row)
            int x=0;
            int totalrows=0;
            int current=1;
            int startspacing=0;
            int spacing=1;
            int signcount=0;
            while (x<A_length){
                    x=x+(int)pow(2,totalrows);
                    totalrows++;
            }
            printf("\n");
            while (totalrows>0)
            {
                    printf("\n");
                    for(int k=0; k<startspacing; k++) printf(" ");
                    for(i=(int) pow(2,totalrows)-1; i > (int) pow( 2, totalrows-1)-1; i--)
                    {
                            printf("%d", A[i]);
                            for(int k=0; k<spacing; k++) printf(" ");
                    }
                    if(totalrows>1){
                            printf("\n");
                            for(int k=0; k<startspacing; k++) printf(" ");
                            for(i=(int) pow(2,totalrows)-1; i > (int) pow(2,totalrows-1)-1; i--)
                            {
                                    if (signcount == 0){printf("\\"); signcount++;}
                                    else {printf("/"); signcount--;}
                                    for(int k=0; k<spacing; k++) printf(" ");
                            }
                    }
                    totalrows--;
                    current++;
                    startspacing=spacing;
                    spacing=(int) pow(2,current)-1;
            }
            printf("%d", A[i]);
    };
     
     
    void swapHeap(heapNode* A, heapNode* B)
    {
            heapNode* temp = new heapNode;
            temp->value=A->value; temp->originalList=A->originalList; assig++;
            A->value = B->value; A->originalList = B->originalList; assig++;
            B->value = temp->value; B->originalList = temp->originalList; assig++;
    }
     
    void min_heapify(heapNode* heapN[],int i) // O (lg n) time
    {
            int smallest = 0;
            int l = Left(i); int r = Right(i);
            comp++;
            if (l< heap_size && heapN[l]->value < heapN[i]->value)
                    smallest = l;
            else smallest = i;
            comp++;
            if (r< heap_size && (heapN[r]->value < heapN[smallest]->value))
                    smallest = r;
            if (smallest!= i){
                    swapHeap(heapN[i], heapN[smallest]);
                    min_heapify(heapN,smallest);}
    }
     
    void build_min_heap(heapNode* A[], int j){ // O(n)
     
            heap_size = j;
            for(i = (j-1)/2; i>=0; i--)
                    min_heapify(A,i);
    }
     
    void mergeLists(listNode* storage[], int n, int k){
            int storageCount = 0;
            //1. build a k-element min-heap using the first element of the k lists,
            //2. extract-min() to get the smallest element from the heap and append it to the result list,
            //3. pick the next element from the same list as the one we just extracted from the heap. Insert it into the heap and goto 2).
     
            for(int i=0; i<k; i++){
                    heapStorage[storageCount] = generateNode(i); //stores a list element in heap, removes from original list.
					assig++;
                    storageCount++;}
     
            //for(int x=0; x<k; x++){
           //         printf("%d ", heapStorage[x]->value);
           // }
     
            //_getch();
            build_min_heap(heapStorage, storageCount);
     
			 //for(int x=0; x<k; x++){
           // //        printf("%d ", heapStorage[x]->value);
            //}
     
            while (resultCount<n*k){
     
					
					result[resultCount] = heapStorage[0]->value; assig++;
                    resultCount++;

					comp++;
					if(storage[heapStorage[0]->originalList] != NULL)
					{
						heapStorage[0]=generateNode(heapStorage[0]->originalList); assig++;			
					}
					else
					{
                    heapStorage[0] = heapStorage[storageCount-1]; assig++;
                    storageCount--;
					}
                    heap_size=storageCount;
                    min_heapify(heapStorage,0);
            }
            //printf("\n \n");
            //for (int F =0; F<n*k; F++){printf("%d ", result[F]);}
    }
     
    int main()
    {
            //int k = 4; int n = 20;
			
            //for (int x=0; x<k; x++){listStorage[x]=generateOrderedList(n); THERANDY=THERANDY+3; }
            //mergeLists(listStorage, n, k);


			//for (int x=0; x<k; x++){listStorage[x]=NULL;}
			//            for (int x=0; x<k; x++){listStorage[x]=generateOrderedList(n); THERANDY=THERANDY+3; }
           // mergeLists(listStorage, n, k);
     
            _getch();

		assig=0; comp = 0;

					 myfile.open ("results.txt",ios::app);
			myfile << "Case 1" << endl;
			myfile.close();

			int k1=5, k2=10, k3=100; int n1=0,n2=0,n3=0;
			//n1=120;
			for (n1=10; n1<2000; n1=n1+80){
			 for (int x=0; x<k1; x++){listStorage[x]=generateOrderedList(n1); THERANDY=THERANDY+3; }
			 mergeLists(listStorage, n1, k1);
			 for (int x=0; x<k1; x++){listStorage[x]=NULL;}
			 for (int x=0; x<n1*k1; x++){result[x]=NULL;}
			 resultCount=0;
			 myfile.open ("results.txt",ios::app);
			myfile << n1 << "," << k1 << "," << assig << "," << comp << endl;
			myfile.close();
			assig = 0; comp = 0;
			}
			
								 myfile.open ("results.txt",ios::app);
			myfile << "Case 2" << endl;
			myfile.close();

			for (n2=10; n2<1000; n2=n2+40){
			 for (int x=0; x<k2; x++){listStorage[x]=generateOrderedList(n2); THERANDY=THERANDY+3; }
			 mergeLists(listStorage, n2, k2);
			 for (int x=0; x<k2; x++){listStorage[x]=NULL;}
			 for (int x=0; x<n2*k2; x++){result[x]=NULL;}
			 resultCount=0;
			 			 myfile.open ("results.txt",ios::app);
			myfile << n2 << "," << k2 << "," << assig << "," << comp << endl;
			myfile.close();
			assig = 0; comp = 0;
			}

											 myfile.open ("results.txt",ios::app);
			myfile << "Case 3" << endl;
			myfile.close();

			for (n3=10; n3<100; n3=n3+4){
			 for (int x=0; x<k3; x++){listStorage[x]=generateOrderedList(n3); THERANDY=THERANDY+3; }
			 mergeLists(listStorage, n3, k3);
			 for (int x=0; x<k3; x++){listStorage[x]=NULL;}
			 for (int x=0; x<n3*k3; x++){result[x]=NULL;}
			 resultCount=0;
			 			 			 myfile.open ("results.txt",ios::app);
			myfile << n3 << "," << k3 << "," << assig << "," << comp << endl;
			myfile.close();
			assig = 0; comp = 0;
			}

														 myfile.open ("results.txt",ios::app);
			myfile << "Case 4" << endl;
			myfile.close();

			int n4=10000; int k4; int res;
			for(k4=10; k4<500; k4=k4+10){
				res = (int) n4/k4;
				for (int x=0; x<k4; x++){listStorage[x]=generateOrderedList(res); THERANDY=THERANDY+3; }
			 mergeLists(listStorage, res, k4);
			 for (int x=0; x<k4; x++){listStorage[x]=NULL;}
			 for (int x=0; x<res*k4; x++){result[x]=NULL;}
			 resultCount=0;
			 			 			 			 myfile.open ("results.txt",ios::app);
			myfile << n4 << "," << k4 << "," << assig << "," << comp << endl;
			myfile.close();
			assig = 0; comp = 0;
			}
    }
