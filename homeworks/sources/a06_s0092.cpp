/**
Student: ***ANONIM*** Diana ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: The Josephus Permutation
Comments:
	
**/

#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
struct node {
   int val;
   int size;
   struct node *right;
   struct node *left;
   struct node *parent;
};
int n,effort=0;
node* root;
void pretty_print(node* p, int depth)
{
	if(p!=NULL)
	{ 
		pretty_print(p->right,depth+1);
		for(int j=0;j<depth;j++) 
			printf("	");
		if (p!=root)
			printf("%d(%d,%d)", p->val,p->size,p->parent->val);
		else printf("%d(%d,null)", p->val,p->size);
		printf("\n");
		pretty_print(p->left,depth+1);
	
	}

}

node* os_select(node *x, int i)
{
	effort++;
	int r;
	if(x->left == NULL)
		r=1;
	else
		r=x->left->size+1;	
	if (i==r)
		return x;
	else if (i<r)
			return os_select(x->left,i);
			else return os_select(x->right,i-r);

}
node* FindMin(node* aux)
{
	effort++;
	while (aux->left!=NULL) 
		{
			aux=aux->left;
			effort++;
		}
	return aux;
}
node* succesor(node* nod)
{
	effort++;
	if (nod->right!=NULL)
		return FindMin(nod->right);
	node* y=nod->parent;
	effort++;
	while (y!=NULL && nod==y->right)
	{
		nod=y;
		y=y->parent;
		effort+=2;
	}
	return y;
}

node* os_delete(node* z)
{
	node *y;
	node *x;
	effort++;
	if (z->left==NULL || z->right==NULL)
	{
			y=z;
			effort++;
	}
	else 
	{
			y=succesor(z);
			effort++;
	}
	effort++;
	if (y->left!=NULL)
	{
		x=y->left;effort++;
	}
	else
	{
			x=y->right;effort++;
	}
	effort++;
	if (x!=NULL)
	{
		x->parent=y->parent;effort++;
	}
	effort++;
	if (y->parent==NULL)
	{
		root=x;effort++;
	}
	else
	{	
			effort++;
			if (y==y->parent->left)
			{
					y->parent->left=x;effort++;
			}
			else
			{
					y->parent->right=x;effort++;
			}
	}
	if (z!=y)
	z->val=y->val;
	effort++;
	//node* ptsters=y;
	while(y!=NULL)
	{
		y->size--;
		y=y->parent;
	}
	free(y);
	return z;
	
}

node* build_tree(int start, int end)
{
    if (start>end) return NULL;
	else
	{
		int middle=start +(end-start)/2;
		node* aux=(node*)malloc(sizeof(node));
		aux->val=middle; 
		aux->size=end-start+1;
		aux->left=NULL; 
		aux->right=NULL;
		aux->left=build_tree(start, middle-1); 
		effort+=4;
		effort++;
		if (aux->left!=NULL)
		{
			aux->left->parent=aux;effort++;
		}
		effort++;
		aux->right=build_tree(middle+1,end);
		effort++;
		if (aux->right!=NULL)
		{
			aux->right->parent=aux;effort++;
		}
	
		return aux;
	}
}

void josephus(int n, int m)
{
 	//build tree
	root=(node*)malloc(sizeof(node));
	root->val=(n+1)/2;
	
	effort++;
	
	root->size=n; 
	
	effort++;
	
	root->parent=NULL;
	root->left=build_tree(1,(n+1)/2-1);
	
	effort++;
	
	root->left->parent=root;
	
	effort++;
	
	root->right=build_tree((n+1)/2+1,n); 
	
	effort++;
	
	root->right->parent=root; 
	
	
	effort++;
	
	//pretty_print(root,1);
	int j=1;
	for (int k=n;k>=1;k--)
	{
		j=((j+m-2)%k)+1;
		node* x=os_select(root,j);
		effort++;
		//printf("\n\nVal:%d\n\n", x->val);
		
		node* z=x->parent;
		effort++;
		os_delete(x);
		effort++;
		//pretty_print(root,1);
		printf("\n\n");
	}
	
}


int main()
{
	
	FILE *f;
	f=fopen("josephus.csv","w");
	fprintf(f,"n, effort\n");
	for (int n=100;n<=10000;n=n+100)
	{
		//int n=15;
		int m=n/2;
		effort=0;
		josephus(n,m);
		fprintf(f,"%d,%d\n",n,effort);
	}
		
	fclose(f);
	//getch();
}