/****ANONIM*** ***ANONIM*** Daniela - ***GROUP_NUMBER***
Acest algoritm de interclasare a k liste ordonate are eficienra O(n logk).Am tratat cazul mediu in cele 2 situatii.
In prima situatie am luat cele 3 serii de liste adica k1=5, k2=10 si k3=100 , n luand valori de la 100 la 10000.
Graficul obtinut cotine 3 drepte care cresc in functie de k (pt k=100 dreapta creste cel mai repede fiind urmata de k=10 si k=5).
In a doua situatie l-am luat pe n=10000 si k variaza intre 10 si 5000.Aici graficul contine o curba logaritmica.

*/

#include<math.h>
#include<time.h>
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#define NMAX 1001



typedef struct _NOD{
	int valoare;
	struct _NOD  *urm;
}NOD;



typedef struct _HEAP_ELT{
	int idxList;
	int element;
}HEAP_ELT;

NOD *head[NMAX];
NOD *joinedHead=NULL;
NOD *joinedTail=NULL;

int dim_heap;
int op=0;


void inserare(int nr)
{
	NOD *prim=(NOD *)malloc(sizeof(NOD));
	prim->valoare=nr;
	if(joinedHead==NULL)
	{
		joinedHead=prim;
		joinedTail=joinedHead;
	}
	else
	{
		joinedTail->urm=prim;
		joinedTail=prim;
	}
	joinedTail->urm=NULL;
}

 NOD *deleteNod(int i)
{
	NOD *prim=head[i]->urm;
	if(prim!=NULL)
		head[i]->urm=prim->urm;
	return prim;
}

void addLista(int id)
{
	head[id]=(NOD *)malloc(sizeof(NOD));
	head[id]->urm=joinedHead;
}



int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i+1);
}

void min_heapify(HEAP_ELT a[],int i)
{
	int min;
	HEAP_ELT aux;
	int l,r;
	l=left(i);
	r=right(i);

	op++;
	if(l<=dim_heap && a[l].idxList<a[i].idxList)
		min=l;
	else
		min=i;
	op++;
	if(r<=dim_heap && a[r].idxList<a[min].idxList)
		min=r;
	if(min!=i)
	{
		op+=3;
		aux=a[i];
		a[i]=a[min];
		a[min]=aux;
		min_heapify(a,min);
	}
}



HEAP_ELT popHeap(HEAP_ELT a[])
{
	HEAP_ELT b=a[1];
	op++;
	a[1]=a[dim_heap];
	dim_heap--;
	min_heapify(a,1);
	return b;
}

void pushHeap(HEAP_ELT a[],int x,int y)
{
	
	HEAP_ELT aux;
	int i;
	dim_heap++;
	op+=2;
	a[dim_heap].idxList=x;
	a[dim_heap].element=y;
	i=dim_heap;
	while(i>1 && a[i].idxList<a[parent(i)].idxList)
	{
		op+=4;
		aux=a[i];
		a[i]=a[parent(i)];
		a[parent(i)]=aux;
		i=parent(i);
	}
	op++;
}
void interclasareListe(int k)
{ 
	
	HEAP_ELT a[1000];
	HEAP_ELT info;
	joinedHead=NULL;
	joinedTail=NULL;
	dim_heap=0;
	int d;
	NOD *prim;
	for(int i=1;i<=k;i++)
	{
		d=(deleteNod(i))->valoare;
		op++;
		pushHeap(a,d,i);
	}
	while(dim_heap>0)
	{
		info=popHeap(a);
		op++;
		inserare(info.idxList);
		op++;
		prim=deleteNod(info.element);
		if(prim!=NULL)
			pushHeap(a,prim->valoare,info.element);
	}
}

void generare(int k,int n)
{
	int v[NMAX];
	int x,y=0;
	op=0;
	srand(time(NULL));
	for(int i=1;i<=k;i++)
	{
		joinedHead=NULL;
		joinedTail=NULL;
		for(int j=1;j<=n/k;j++)
		{
			x=y+rand();
			inserare(x);
			y=x;
		}
		addLista(i);
	}
	interclasareListe(k);
}

void generareK()
{
	int n,k;
	int op_k1,op_k2,op_k3;
	FILE *f=fopen("mediu_k.csv","w");
	fprintf(f,"n,k1=5,k2=10,k3=100\n");
	for(n=100;n<=10000;n+=100)
	{

		k=5;
		op=0;
		generare(k,n);
		op_k1=op;
         k=10;
		op=0;
		generare(k,n);
		op_k2=op;

		k=100;
		op=0;
		generare(k,n);
		op_k3=op;

		fprintf(f,"%d,%d,%d,%d\n",n,op_k1,op_k2,op_k3);
	}
	fclose(f);
}

void generareN()
{
	int n,k;
	FILE *f=fopen("mediu_n.csv","w");
	fprintf(f,"k,nr_operatii\n");
	n=10000;
	for(k=10;k<=500;k+=10)
	{
		op=0;
		generare(k,n);
		fprintf(f,"%d,%d\n",k,op);
	}
	fclose(f);
}



int main(void)
{
	int k=4;
    int a[]={0,3,5,7,9,11};
	int b[]={1,2,19,21};
	int c[]={4,8,10};
	int d[]={1,12,17,21,24};
	
	for(int i=0;i<6;i++)
		inserare(a[i]);
	    addLista(1);

	 joinedHead=NULL;
	 joinedTail=NULL;
	for(int i=0;i<4;i++)
		inserare(b[i]);
	    addLista(2);

	joinedHead=NULL;
	joinedTail=NULL;
	for(int i=0;i<3;i++)
		inserare(c[i]);
	    addLista(3);

	joinedHead=NULL;
	joinedTail=NULL;
	for(int i=0;i<5;i++)
		inserare(d[i]);
	    addLista(4);
	

	for(int i=1;i<=k;i++)
	{
	printf("\nElementele din lista %d sunt: " ,i);
	for(NOD *p=head[i]->urm;p!=NULL;p=p->urm)
	printf("%d ",p->valoare);
		
	}
    printf("\nListele interclasate sunt :");
	interclasareListe(k);
	for(NOD *p=joinedHead;p!=NULL;p=p->urm)
		printf("%d ",p->valoare);


	//generareN();
	//generareK();
    getch();
	return 0;
}
