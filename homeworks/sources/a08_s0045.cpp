/****ANONIM*** ***ANONIM***, ***GROUP_NUMBER***, Assign8
Complexitatea algoritmului connected_component iese liniara fiind dat de numarul de apeluri
a functiilor make_set, find_set, union.
Make_set este apelat de nr_varfuri de ori pentru fiecare numar de muchii. Numarul de varfuri fiind constant este
doar un factor de multiplicare.
Diferenta numarului de operatii este influentat de muchii, mai precis consta in apeluri find_set si eventuale
apeluri la union_sets (cel mult nr varfuri-1 ori).
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "Profiler.h"

#define MAX_NR_V 10000
#define MAX_NR_E 60000+500

typedef struct _multi_node
{
	int key;
	int rank;
	_multi_node *p;
}m_node;

//global variables
int adjacency_matrix[MAX_NR_V][MAX_NR_V];
int nrV=0;
int nrE=0;
m_node *nodes[MAX_NR_V];
int count[MAX_NR_E];

m_node *createMN(int key)
{
	m_node *nod=(m_node *)malloc(sizeof(m_node));
	nod->key=key;
	nod->rank=0;
	nod->p=nod;

	return nod;
}

void clear_nodes(m_node *nodes[MAX_NR_V])
{
	for (int i=0; i<MAX_NR_V; i++)
	{
		free(nodes[i]);
	}
}

void init()
{
	for (int i=0; i<MAX_NR_V; i++)
	{
		for (int j=0; j<MAX_NR_V; j++)
		{
			adjacency_matrix[i][j]=0;
		}

		nodes[i]=0;
	}
}

void finalize()
{
	clear_nodes(nodes);
}

void make_set(int x)
{
	m_node* nod=createMN(x);
	nodes[x]=nod;
}

m_node* find_set(m_node* x)
{
	if (x != x->p)
	{
		x->p=find_set(x->p);
	}

	return x->p;
}

void link(m_node* x, m_node* y)
{
	if (x->rank > y->rank)
	{
		y->p=x;
	}
	else
	{
		x->p=y;
		if (x->rank == y->rank)
		{
			(y->rank)++;
		}
	}
}

void union_sets(m_node* x, m_node* y)
{
	link(find_set(x), find_set(y));
}

void connected_component(int adj[][MAX_NR_V], int nrV)
{
	for (int i=0; i<nrV; i++)
	{
		make_set(i);

		count[nrE]++;
	}

	for (int i=0; i<nrV; i++)
	{
		for (int j=0; j<i; j++)
		{
			if (adj[i][j] == 1)
			{
				if (find_set(nodes[i]) != find_set(nodes[j]))
				{
					union_sets(nodes[i], nodes[j]);

					count[nrE]++;
				}

				count[nrE]+=2;
			}
		}
	}
}

void print_components(int nrV)
{
	for (int k=0; k<nrV; k++)
	{
		printf("Component with representant %d : ",k);

		for (int j=0; j<nrV; j++)
		{
			if (find_set(nodes[j])->key == k)
			{
				printf("%d ",j);
			}
		}

		printf("\n");
	}
	printf("\n");
}

void place_random_edges(int adj[][MAX_NR_V], int nrV,int nrE)
{
	for (int i=0; i<nrE; i++)
	{
		//generam muchii numai sub diagonala principala -- matricea de adiacenta este simetrica
		int x=rand() % (nrV-1);
		x++;
		int y=rand()%x;
		adj[x][y]=adj[y][x]=1;
	}
}

void init_counters()
{
	for (int i=0; i<MAX_NR_E; i++)
	{
		count[i]=0;
	}
}

int main()
{
	srand(time(NULL));

	//test
	init();

	nrV=10;

	adjacency_matrix[0][1]=adjacency_matrix[1][0]=1;
	adjacency_matrix[0][2]=adjacency_matrix[2][0]=1;
	adjacency_matrix[1][2]=adjacency_matrix[2][1]=1;
	adjacency_matrix[1][3]=adjacency_matrix[3][1]=1;
	adjacency_matrix[4][5]=adjacency_matrix[5][4]=1;
	adjacency_matrix[4][6]=adjacency_matrix[6][4]=1;
	adjacency_matrix[7][8]=adjacency_matrix[8][7]=1;

	connected_component(adjacency_matrix, nrV);
	print_components(nrV);

	finalize();

	//test random edges
	init();
	nrV=10;
	place_random_edges(adjacency_matrix, nrV, 7);
	connected_component(adjacency_matrix, nrV);
	print_components(nrV);
	finalize();

	//generare date
	printf("Computing...");
	init_counters();
	nrV=10000;

	int cantitate=10;
	for (nrE=10000; nrE<=60000; nrE+=cantitate)
	{
		init();
		place_random_edges(adjacency_matrix, nrV, nrE);
		connected_component(adjacency_matrix, nrV);
		finalize();
	}

	//exportare date
	FILE *f=fopen("assign8_10.csv", "w");
	if (f == 0)
	{
		perror("Eroare la deschidere fisier!");
		exit(2);
	}

	fprintf(f,"nr edges,nr op\n");

	for (nrE=10000; nrE<=60000; nrE+=cantitate)
	{
		fprintf(f,"%d,%d\n",nrE,count[nrE]);
	}

	fclose(f);

	printf("Done!\n");
	return 0;
}