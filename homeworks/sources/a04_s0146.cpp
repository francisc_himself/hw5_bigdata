//***ANONIM*** ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "Profiler.h"
#define MAX_SIZE 10000

Profiler profiler("Lab5");

typedef struct NOD 
{
	int valoare;
	NOD *next;
}NOD;

typedef struct HEAP_ELT 
{
	int idxList;
	NOD *elem;
}HEAP_ELT;

NOD *head[1000];
NOD *tail[1000];
NOD *joinedHead,*joinedTail;
HEAP_ELT heap[1000];


int i,m,n,l,r,largest,k,nivel,j;
int v[MAX_SIZE];
NOD *aux;

void inserare(NOD **h,NOD **t,int val)
{
	if((*h)!=NULL)
	{
		NOD *c;
		c = (NOD *)malloc(sizeof(NOD));
		c->valoare=val;
		(*t)->next=c;
		*t=c;
	}
	else 
	{
		(*h) = (NOD *)malloc(sizeof(NOD));
		(*h)->valoare=val;
		*t=*h;
	}
	(*t)->next=NULL;
}

void afisare(NOD *h)
{
	NOD *c;
	//c = (NOD *)malloc(sizeof(NOD));
	c=h;
	while(c!=NULL)
	{
		printf("%d ->",c->valoare);
		c=c->next;
	}
}

void deleteList(NOD **h,NOD **t)
{
	while((*h)!=NULL)
	{
		NOD *c;
		c = (NOD *)malloc(sizeof(NOD));
		c=(*h);
		(*h)=(*h)->next;
		free (c);
	}
}


///facem PUSH in heap

void heap_increase_key(HEAP_ELT *heap,int i, HEAP_ELT key)
{
	heap[i]=key;
	//profiler.countOperation("td",n);
	while ((i>1) && (heap[i/2].elem>heap[i].elem))
	{
		aux=heap[i].elem;
		heap[i].elem=heap[i/2].elem; //i/2=parent(i)
		heap[i/2].elem=aux;
		i=i/2;
		//profiler.countOperation("td",n,4);
	}
}

void min_heap_insert(HEAP_ELT *heap,HEAP_ELT key)
{
	m=m+1;
	//heap[m]=-INT_MAX;
	//profiler.countOperation("td",n,2);
	heap_increase_key(heap,m,key);
}

//Construim heap

void min_heapify(HEAP_ELT *heap,int i) 
{
	l=2*i;//left i
	r=2*i+1;//right i
	//profiler.countOperation("bu",n,2);
	if((l<=m)&&(heap[l].elem<heap[i].elem))
	{
		largest=l;
		//profiler.countOperation("bu",n);
	}
	else
	{
		largest=i;
		//profiler.countOperation("bu",n);
	}
	if((r<=m)&&(heap[r].elem<heap[largest].elem))
	{
		largest=r;
		//profiler.countOperation("bu",n);
	}
	if(largest!=i)
	{
		aux=heap[i].elem;
		heap[i].elem=heap[largest].elem;
		heap[largest].elem=aux;
		//profiler.countOperation("bu",n,3);
		min_heapify(heap,largest);
	}
}

/*void build_max_heap(int *a) 
{
	m=n;
	for(i=n/2;i>0;i--)
	{
		//profiler.countOperation("bu",n);
		max_heapify(a,i);
	}
}*/

// facem Pop in heap

HEAP_ELT heap_extract_min(HEAP_ELT *heap)
{
	HEAP_ELT min;
	//min = (NOD *)malloc(sizeof(NOD));
	min=heap[1];
	heap[1]=heap[m];
	m=m-1;
	min_heapify(heap,1);
	return min;
} 


void rez(int k,HEAP_ELT *heap)
{
	HEAP_ELT x;

	 for(i=0;i<k;i++)
	 {
		 min_heap_insert(heap,heap[i]);//push
	 }

	  while(m>0)
	  {
			x=heap_extract_min(heap);//pop
			if(x.elem->valoare!=NULL)inserare(&joinedHead,&joinedTail,x.elem->valoare);
			//x=get(&LISTS[x.index],x.index);
	
			if((heap[i].elem->next)!=NULL)  
			{
				heap[i].elem=heap[i].elem->next;
				min_heap_insert(heap,heap[i]);//push
			}

	 }

}


void generare_lista(int k, int n)
{
	int j,x,p;

	for(i=0;i<k;i++)
	{
		head[i]=0;
		tail[i]=0;
		x=0;
		FillRandomArray(v, n/k, 0, 32000, true, 1);
		p=0;
		for(j=0;j<(n/k);j++)
		{ 
			x=v[p];
			p++;
			inserare(&head[i],&tail[i],x);
			
		}
	}
}

void main()
{
	aux = (NOD *)malloc(sizeof(NOD));

	///*********Pentru n si k date: *************
	k=5; n=20;

	joinedHead=0;
	joinedTail=0;

	generare_lista(k, n);
	for(j=0;j<k;j++)
	{
		afisare(head[j]);
		printf("\n");
		//deleteList(&head[j],&tail[j]);
	}

	printf("lista finala: \n");
	rez( k,heap);
	afisare(joinedHead);
	//printf("\n");
	//deleteList(&head[0],&tail[0]);
	//afisare(head[0]);



}