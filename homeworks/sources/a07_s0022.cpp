#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#define length 10

typedef struct mnod{
	int key;
	int parent;
	int nrSons;
	//struct mnod *sons[length];
	struct mnod **sons;
}mNOD;

typedef struct bnod
{
	int key;
	struct bnod *son, *rel;

}bNOD;



int A[length] = {0,2,7,5,2,7,7,-1,5,2};
int visit[length] = {0,0,0,0,0,0,0,0,0,0};
int nrSons[length];
int n = length;
mNOD *rad;
mNOD * multi[100];
bNOD * bin[100];

//numarul de fii
void findSonsNumbers(int i){

	for(int i=0;i<n;i++){
		nrSons[i] = 0;
	}
	for(int i=0;i<n;i++){
		if(A[i] != -1){
			int index = A[i];
			nrSons[index]+=1;
		}
	}
}

//initializarea vectorului arborelui multicai
void initMultiVector(){

	for(int i=0;i<n;i++){
		
		int nrs = nrSons[i];

		multi[i] = (mNOD*)malloc(sizeof(mNOD));
		multi[i]->key = i;
		multi[i]->nrSons = 0;
		multi[i]->sons = (mNOD **)malloc(sizeof(mNOD*)*nrs);
		multi[i]->sons[1] = NULL;
	}
}
//trasformarea 1
void vectorToMulti(){
	
	int aux;

	initMultiVector();
	for(int i=1;i<n;i++){
	
		if(A[i] == -1){
		
			rad = multi[i];
		}else{
		
			int index = A[i];
			multi[index]->nrSons+=1;
			aux = multi[index]->nrSons;
			multi[index]->sons[aux] = multi[i];
		}
	}
}
//initializarea vectorului binar
void initBinVector(){

	for(int i=0;i<n;i++){
		
		bin[i] = (bNOD*)malloc(sizeof(bNOD));
		bin[i]->key = i;
		bin[i]->son = NULL;
		bin[i]->rel = NULL;
	}
}
//transformarea 2
void multiToBin(mNOD *multi){
	
	if(multi!=NULL){
	
		for(int i=1;i<=multi->nrSons;i++){
		
			if(i == 1){
			
				int aux = multi->key;
				bin[aux]->son = bin[multi->sons[1]->key];

			}else{
			
				bin[multi->sons[i-1]->key]->rel = bin[multi->sons[i]->key];
			}
			multiToBin(multi->sons[i]);
		}
	}
}




//Afisarea unui vector ca Pretty-Print - functie de afisare propiu-zisa
void afis(int V[length],int k,int l){
	int s=1;
	printf("\n");
	l++;
	for(int i=0;i<l;i++)
		printf("  ");
	printf("%d\\",k);
	visit[k]=1;
	for(s=1;s<length;s++){
		if(V[s]==k && visit[s]==0){
			afis(V,s,l);
		}
	}
}
//Afisarea unui vector ca Pretty-Print
void afisTreeR1(int V[length]){

	printf("\nTree R1");
	int k = 1;
	while(V[k]!=-1){
		k++;
	}
	afis(V,k,1);
	printf("\n");
}

//Afisarea unui arbore multi-cai ca Pretty-Print
void afisTreeR2(mNOD *multi,int l){

	//printf("\nTree R1");
	if(multi!=NULL){
		l++;
		printf("\n");
		for(int i=0;i<l;i++)
		printf("  ");
		
		printf(" %d\\",multi->key);
		if(multi->nrSons != 0){
		
			for(int i=1;i<=multi->nrSons;i++){
			
				afisTreeR2(multi->sons[i],l);
			}
		}
	}
	
}

//Afisarea unui arbore binar ca Pretty-Print
void afisTreeR3(int key, int l){
	
	if(bin!=NULL){
	
		printf("\n");
		for(int i=0;i<l;i++)
		printf("  ");
		
		printf(" %d\\",key);

		if(bin[key]->son!=NULL)
			afisTreeR3(bin[key]->son->key,l+1);
		if(bin[key]->rel!=NULL)
			afisTreeR3(bin[key]->rel->key,l);
	}

}



void main(){

	printf("\nA inceput aplicatia");
	afisTreeR1(A);
	getch();

	printf("\nTree R2");
	vectorToMulti();
	afisTreeR2(rad,0);
	printf("\n");

	getch();

	printf("\nTree R3");
	initBinVector();
	multiToBin(rad);
	afisTreeR3(bin[7]->key,0);
	printf("\n");
	printf("\nAplicatia s-a sfarsit!\n");
	getch();
}