/**
Student: ***ANONIM*** Diana ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: Multi-way Trees - Transformations between different representations
Comments:

**/
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>


int rootindex=0,n=0; 
int vector[200];

typedef struct node{
	node* next;
	int val;
}node;

typedef struct node_binary{
	node_binary* brother;
	node_binary* child;
	int val;
}node_binary;

node_binary* root;
node* start[200];
node* end[200];

void pretty_print_R3(node_binary* aux, int depth)
{
    int i;
    for (i=0;i<depth;i++)
        printf("    ");
    printf("%d\n",aux->val);
    if (aux->child!=NULL) 
		pretty_print_R3(aux->child, depth+1);
    if (aux->brother!=NULL) 
		pretty_print_R3(aux->brother, depth);
}
void pretty_print_R2()
{
	printf("\nR2:\n");
	printf("Tree rooted at %d with nodes:\n",rootindex);
	for (int i=1;i<=n;i++)
    {

        node* x=start[i];
		printf("%d ( ",i);
        int ok=0;
		while (x!=NULL)
        {
			printf("%d ",x->val);
            x=x->next;
			ok=1;
        }
		if (ok==1)
			printf(")\n");
		else printf("none)\n");
    }
}

void pretty_print_R1()
{
	printf("\nR1:\n");
	for (int i=1;i<=n;i++)
		printf("%d ",vector[i]);
	printf("\n");
}

void R2(int j, int i)
{
    
	node* m=(node*)malloc(sizeof(node));
	m->next=NULL;
    if (j!=-1)
    {
		if (end[j]==NULL)
		{
			start[j]=m;
			end[j]=start[j];
		}
		else 
		{	
			end[j]->next=m;
			end[j]=end[j]->next;
			end[j]->next=NULL;
		}
    }
    else rootindex=i;
    m->val=i;
}
void R3(int i, node_binary* parent)
{
    node* nod=start[i];
    node_binary* left;
    while (nod!=NULL)
    {
        node_binary* aux=(node_binary*)malloc(sizeof(node_binary*));
        aux->val=nod->val;
        aux->child=NULL;
        aux->brother=NULL;
        if (aux->val==start[i]->val) 
		{
			parent->child=aux;
			left=aux;
        }
        else 
		{
            left->brother=aux;
            left=left->brother;
        }
		R3(aux->val,aux);
        nod=nod->next;
    }
}
void R1()
{
	printf("n=");
    scanf("%d",&n);
	for (int i=1; i<=n;i++)
    {
        printf("Node value:");
        scanf("%d",&vector[i]);
    }
	pretty_print_R1();
}
int main()
{
	R1();
    for (int i=1;i<=n;i++)
    {
        start[i]=NULL;
        end[i]=start[i];
    }
	
    for (int i=1;i<=n;i++)
        R2(vector[i],i);
	pretty_print_R2();
    
	root=(node_binary*)malloc(sizeof(node_binary*));
    root->val=rootindex;
    root->child=NULL;
    root->brother=NULL;

    R3(rootindex,root);
	printf("\nR3:\n");
    pretty_print_R3(root,0);

	getch();
}
/**
void pretty_print(node* p, int depth)
{
	if(p!=NULL)
	{ 
		pretty_print(p->right,depth+1);
		for(int j=0;j<depth;j++) 
			printf("	");
		if (p!=root)
			printf("%d(%d,%d)", p->val,p->size,p->parent->val);
		else printf("%d(%d,null)", p->val,p->size);
		printf("\n");
		pretty_print(p->left,depth+1);
	
	}

}**/