#include <stdio.h>
#include <conio.h>
#include <cstdlib>

typedef struct nod
{
	int key;
	int numar;
	int distanta;
	struct nod *lista[20];
	char color;
} nod;

int dnr, bnr;
int nr, timp;

void BFS(nod *v[20], nod *sursa)
{
	printf("BFS: \n");
	nod *Q;
	sursa->color='g';
	sursa->distanta=0;
	Q=(nod*)malloc(sizeof(nod));
	printf("%d  ", sursa->key);
	Q=sursa;
	int z=1;
	while (z!=0)
	{
		
		for (int i=1; i<=Q->numar; i++)
		{
			bnr++;
			if (Q->lista[i]->color=='a')
			{
				bnr++;
				Q->lista[i]->color='g';
				Q->lista[i]->distanta=Q->distanta+1;
				printf("%d  ", Q->lista[i]->key);
				z++;
			}
		}
		Q->color='n';
		Q=v[z];
		z--;
	}

	printf("\n");
	for (int i=1; i<=nr; i++) printf("%d  ", v[i]->distanta);

}

void Vizita(nod *u)
{
	u->color='g';
	u->distanta=timp+1;
	timp++;
	for (int i=1; i<=u->numar; i++)
	{
		dnr++;
		if (u->lista[i]->color=='a') { Vizita(u->lista[i]); 
		                               printf(" %d ", u->lista[i]->key);}
	}
	u->color='n';
	u->distanta=timp+1;
	timp++;
}

void DFS(nod *v[20])
{
	printf("\n DFS: \n");
	timp=0;
	for (int i=1; i<=nr; i++)
	{
		dnr++;
		if (v[i]->color=='a') {printf("%d  ", v[i]->key);  
		                       Vizita(v[i]); }
	}

	printf("\n");
	for (int i=1; i<=nr; i++) printf("%d  ", v[i]->key);

	printf("\n");
	for (int i=1; i<=nr; i++) printf("%d  ", v[i]->distanta);
}

void main ()
{
	int matr[200][200];
	nod *vect[200], *vect2[200];

	printf("Nr. nodurilor:");
	scanf("%d",&nr);

	for (int i=1; i<=nr; i++)
	{
		vect[i]=(nod*)malloc(sizeof(nod));
		vect[i]->key=i;
		vect[i]->numar=0;
		vect[i]->color='a';
		vect[i]->distanta=100;
	}
	for(int i=1; i<=nr; i++)
		{printf("\n");
		for (int j=1; j<=nr; j++)
		{
			printf("m[%d][%d]=", i,j);
			scanf ("%d", &matr[i][j]);
			printf("    ");

			if (matr[i][j]==1) 
			{
				vect[i]->numar++;
				vect[i]->lista[vect[i]->numar]=vect[j];
			}

		}
	}

	for (int i=1; i<=nr; i++)
	{
		printf("\n%d :", vect[i]->key);
		for (int j=1; j<=vect[i]->numar; j++)
		{

			printf("%d ", vect[i]->lista[j]->key);

		}
	}
	printf("\n");
	for (int i=1; i<=nr; i++)
	{
		vect2[i]=(nod*)malloc(sizeof(nod));
		vect2[i]=vect[i];
		vect2[i]->color='a';
		vect2[i]->distanta=0;
	}
	int sursa;
	printf("\ Sursa este :");
	scanf("%d", &sursa);
	printf("\n");
	BFS(vect, vect[sursa]);

	DFS(vect2);
    FILE *f;
	/*f=fopen("file.csv","w");
	nr= 100;
	bnr=0;
	dnr=0;
	fprintf(f, "v, e, bnr, dnr\n");
	int numar;
	for (int i=1000; i<=5000; i=i+100)
	{
		
		for (int l=1; l<=nr; l++)
	{
		vect[l]=(nod*)malloc(sizeof(nod));
		vect[l]->key=i;
		vect[l]->numar=0;
		vect[l]->color='a';
		vect[l]->distanta=100;
	}
		numar=1;
        for (int j=1; j<=nr; j++)
		{
			for (int k=1; k<=j; k++)
			{
				if (numar<=i) { matr[j][k]=rand()%1; matr[k][j]=rand()%1; numar++;}
				if (matr[j][k]==1) 
			{
				vect[j]->numar++;
				vect[j]->lista[vect[j]->numar]=vect[k];

				vect[k]->numar++;
				vect[k]->lista[vect[k]->numar]=vect[j];
			}
			}
		}
			for (int i=1; i<=nr; i++)
	       {
		       vect2[i]=(nod*)malloc(sizeof(nod));
	        	vect2[i]=vect[i];
		       vect2[i]->color='a';
	        	vect2[i]->distanta=0;
	         }

			BFS(vect, vect[1]);
             DFS(vect2);
			 fprintf(f, "%d, %d, %d, %d\n", nr, i, bnr, dnr);



	}
    bnr=0; dnr=0;

	int it=9000;

	for (int nr=110; nr<=200; nr=nr+10)
	{
		
		for (int l=1; l<=nr; l++)
	{
		vect[l]=(nod*)malloc(sizeof(nod));
		vect[l]->key=it;
		vect[l]->numar=0;
		vect[l]->color='a';
		vect[l]->distanta=100;
	}
		numar=1;
        for (int j=1; j<=nr; j++)
		{
			for (int k=1; k<=j; k++)
			{
				if (numar<=it) { matr[j][k]=rand()%1; matr[k][j]=rand()%1; numar++;}
				if (matr[j][k]==1) 
			{
				vect[j]->numar++;
				vect[j]->lista[vect[j]->numar]=vect[k];

				vect[k]->numar++;
				vect[k]->lista[vect[k]->numar]=vect[j];
			}
			}
		}
			for (int x=1; x<=nr; x++)
	       {
		       vect2[x]=(nod*)malloc(sizeof(nod));
	        	vect2[x]=vect[x];
		       vect2[x]->color='a';
	        	vect2[x]->distanta=0;
	         }

			BFS(vect, vect[1]);
             DFS(vect2);
			 fprintf(f, "%d, %d, %d, %d\n", nr, it, bnr, dnr);



	}*/
	getch();

}