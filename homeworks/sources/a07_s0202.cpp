/**
 * Form implementing the problem I used 3 different classes, each one representing the structure in which the representation of the multiway tree is stored.
 * Each class is documented below. I made all the fields public for an easier use.
 * As auxiliary structure I used a vector of nodes due to its ease of accessing elements. I needed to store the already created nodes because when I would
 * have found a node having an already created parent I needed to access the same memory area previously allocated.
 * Each transformation is presented below.
 * Since each transformation is performed in a single loop it will take O(n) time. Surely, a more precise approximation would be O(c*n) where c is a constant
 * but for a largely enough n the constant is unimportant.
 */

#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <vector>

using namespace std;

/*
 * Class for storing nodes in parent representation.
 * Stores a key and a pointer to the parent of the node.
 */
class ParentRep{
public:
	int key;
	ParentRep *parent;
	ParentRep(){key=-1;parent=NULL;}
	~ParentRep(){}
};

/*
 * Class for storing nodes in multiway representation.
 * Stores the key, the number of children and an array of children.
 */
class MultiwayRep{
public:
	int key;
	int nrOfChild;
	MultiwayRep *children[3];
	MultiwayRep(){key=-1;nrOfChild=-1;children[0]=NULL;children[1]=NULL;children[2]=NULL;}
	~MultiwayRep(){};
};

/* 
 * Class for storing nodes in BinaryTree representation.
 * Stores the key, a pointer to a child and a pointer to the brother.
 */
class BTreeRep{
public:
	int key;
	BTreeRep *child;
	BTreeRep *brother;
	BTreeRep(){key=-1;child=NULL;brother=NULL;}
	~BTreeRep(){};
};

/**
 * Preety print method.
 * It prints the current node, then goes on the child branch and when it cannot further go on the child branch goes on the brother branch of the node.
 */
void prettyPrintKey (BTreeRep *root, int level )
{
	if (root == NULL)
	{
		for(int i=0;i<level;i++)
			cout<<"   ";
		cout<<"\n";
		return;
	}
	for (int i=0;i<level;i++)
		cout<<"   ";
	cout<<root->key<<endl;
	prettyPrintKey(root->child,level+1);
	prettyPrintKey(root->brother,level);
}


int main(){
	//parent array
	int parentArray[] = {1,6,4,1,6,6,-1,4,1};
	vector<ParentRep> parentRepresentation(9,ParentRep());
	//create a conventional parent for root
	ParentRep *rootParent = new ParentRep();
	//create the array of nodes in parent representation.
	//for each node the parent pointer is set to the place in the vector where the parent should be at the end of execution.
	for(int i=0;i<9;i++){
		ParentRep *node = new ParentRep();
		node->key=i;
		if (parentArray[i] != -1)
			node->parent=&parentRepresentation.at(parentArray[i]);
		else
			node->parent = rootParent;
		parentRepresentation.at(i) = *node;
	}
	//print the parent vector
	for(int i=0;i<9;i++)
		cout<<parentRepresentation.at(i).parent->key<<" ";
	cout<<endl;
	
	MultiwayRep *mroot = new MultiwayRep();
	vector<MultiwayRep> multiwayRepresentation(9,MultiwayRep());
	/*
	 * For each element in the parent vector get the parent. If the parent is -1 set that node to be the root of the multiway representation. Otherwise
	 * Increase the number of children for that node's parent and add that node as a parent for its parent.
	 */
	for(int i=0;i<9;i++){
		int aux = parentRepresentation.at(i).parent->key;
		multiwayRepresentation.at(i).key=i;
		if (aux != -1){
			multiwayRepresentation.at(aux).nrOfChild++;//increase nr of children
			multiwayRepresentation.at(aux).children[multiwayRepresentation.at(aux).nrOfChild] = &multiwayRepresentation.at(i);
		}
		else mroot = &multiwayRepresentation.at(i);
	}
	
	BTreeRep *root = new BTreeRep();
	vector<BTreeRep> binaryRepresentation(9,BTreeRep());
	
	/*
	 * For each node in multiway representation do the following:
	 *     - set the key if it has no children
	 *     - if it has at least one children set the key, add the children and set its brothers.
	 */
	for (int i=0;i<9;i++){
		if(multiwayRepresentation.at(i).nrOfChild == -1){//daca nu are niciun copil seteaza doar cheia
			binaryRepresentation.at(i).key = multiwayRepresentation.at(i).key;
		}
		else{//daca are copil seteaza-i copilul si fratii copilului
			int childKey = multiwayRepresentation.at(i).children[0]->key;
			binaryRepresentation.at(i).key=i;
			binaryRepresentation.at(i).child = &binaryRepresentation.at(childKey);
			if (multiwayRepresentation.at(i).nrOfChild > 0)//has more than one child
				binaryRepresentation.at(i).child->brother = &binaryRepresentation.at(multiwayRepresentation.at(i).children[1]->key);//set first brother
			if (multiwayRepresentation.at(i).nrOfChild > 1)//has more than two children
				binaryRepresentation.at(i).child->brother->brother = &binaryRepresentation.at(multiwayRepresentation.at(i).children[2]->key);//set second brother
		}
		if (i == mroot->key)
			root = &binaryRepresentation.at(i);
	}
	
	prettyPrintKey(root,0);
	
	getch();
	return 0;
}