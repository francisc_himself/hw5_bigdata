#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "Profiler.h"
#define WHITE 0
#define GRAY 1
#define BLACK 2
#define v_max 10000
#define e_max 60000
int disjunct[v_max];
int sor[1000];
int sore,sorv;



typedef struct 
{
	int parent;
	int distances;
	int color;
}graph_nod;

typedef struct _node
{
	int key;
	struct _node *next;
}node;

node *adj[v_max];
graph_nod g[v_max];
int edges[e_max];

void generate(int n, int m)
{
	memset(adj,0,n*sizeof(node *));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		node *nod=new node;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;


	}
}
void make_set(int *n,int szam)
{

	n[szam]=szam;
}
int find_set(int *n,int szam)
{
	
	while(n[szam]!=szam)
	{
		szam=n[szam];
	}
	return szam;
}
void union_set(int *n,int szam1,int szam2)
{
	int a=find_set(n,szam1);
	int b=find_set(n,szam2);

	n[b]=a;
}
void afisGraf(int m)
{
	for(int i=0;i<m;i++)
	{
		node * p = adj[i];
		printf("%d: ",i);
		while(p!=NULL)
		{
			printf("%d ",p->key);
			p=p->next;
		}
		printf("\n");
	}
}

void conex(int n)
{
	for(int i=0;i<n;i++)
	{
		make_set(disjunct,i);
	}

	for(int i=0;i<n;i++)
	{
		node * p = adj[i];
		
		while(p!=NULL)
		{
			union_set(disjunct,p->key,i);
			p=p->next;
		}
		
	}

}
void afis2(int *n,int m)
{
	for(int i=0; i<m;i++)
	{
		printf("%d : %d\n",i, find_set(n,i));
	}

}
void transformare(int m)
{
	for(int i=0;i<m;i++)
	{
		node * p = adj[i];
		
		while(p!=NULL)
		{
			int a=p->key;
			int b=i;
			node *nod=new node;
			nod->key=b;
			nod->next=adj[a];
			adj[a]=nod;
			p=p->next;
		}
		
	}

}

void bfs(int m)
{
	for(int i=0;i<m;i++)
	{
		g[i].color=WHITE;
	}
	for(int i=0;i<m;i++)
	{
		sore=0;
		sorv=1;
		sor[0]=i;
		while(sore!=sorv)
		{
			int a=sor[sore];
			sore++;

			if(g[a].color != BLACK)
			{
				if(g[a].color==WHITE)// nem listabbol jottt be, a bejarasifa tove lesz
				{
					g[a].parent=a;
				}
				
				node * p = adj[a];
		
				while(p!=NULL)
				{
					if(g[p->key].color == WHITE)
					{
						g[p->key].parent=a;
						sor[sorv]=p->key;
						sorv++;
						g[p->key].color=GRAY;
					}
					p=p->next;
				}

				g[a].color=BLACK;
			}
		}

	}
	
}
void afisfaseged(int szint, int k, int m ) //rekurzivan kiirja a nod kozvetlen szomszedjait
{
	for(int i=0;i<m;i++)
	{
		if(g[i].parent==k)
		{
			for(int i=0;i<szint*2;i++)
			{
				printf("  ");
			}
			printf("%d\n",i);
			afisfaseged(szint+1,i,m);
		}
	}

}

void afisFa(int m)
{
	for(int i=0;i<m;i++)
	{
		if(g[i].parent==i)// keressuk a tovet
		{
			g[i].parent=-1;//ne legyen vegtelen ciklus amikor kiiirjuk
			printf("%d \n",i);
			afisfaseged(1,i,m);
			g[i].parent=i;
		}

					
			
	}

}
int main()
{
	int noduri=10;
	int muky=5;
	generate(noduri,muky);
	
	afisGraf(noduri);
	conex(noduri);
	afis2(disjunct,noduri);

	transformare(noduri);
	afisGraf(noduri);
	bfs(noduri);
	afisFa(noduri);
	return 0;
}