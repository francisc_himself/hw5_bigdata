//***ANONIM*** ***ANONIM***
//grupa ***GROUP_NUMBER***
//Tema 4 Interclasarea a k liste ordonate crescator
//In cazul in care numarul total de numere din liste variaza iar numarul de liste ramane constant numarul de operatii creste in mod liniar
//In cazul in care numarul de numere din liste ramane constant dar creste numarul de liste in care sunt distribuite aceste numere, numarul de operatii variaza sub forma unei curbe logaritmice
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct nod
{
    int info;
    nod *urm;
} NOD;

typedef struct element
{
    int poz;
    int elem;
} ELEMENT;

typedef struct liste
{
    NOD *prim;
    NOD *ultim;
} LISTE;

LISTE Cap[500];
ELEMENT Heap[10000];
NOD *first ;
NOD *last;
int op,nrop[100];

void add(NOD **prim,NOD **ultim,int val)
{
    if((*prim)==NULL)
    {
        (*prim) = new NOD;
        (*prim)->info=val;
        (*prim)->urm=NULL;
        *ultim=*prim;
    }
    else
    {
        NOD *elem=new NOD;
        elem->urm=NULL;
        elem->info=val;
        (*ultim)->urm=elem;
        (*ultim)=elem;
    }
}

void heapify(ELEMENT Heap[],int i,int size)
{
    int l,r,min;
    ELEMENT aux;
    l=2*i;
    r=2*i+1;
	op++;
    if ((l<=size)&&(Heap[l].elem < Heap[i].elem))
        min=l;
    else
        min=i;
	op++;
    if ((r<=size)&&(Heap[r].elem < Heap[min].elem))
        min=r;
    if (min!=i)
    {
        aux=Heap[i];
        Heap[i] = Heap[min];
        Heap[min]=aux;
		op+=3;
        heapify(Heap,min,size);
		
    }
}
void insert_heap(ELEMENT Heap[],ELEMENT x,int *size)
{
    (*size)++;
    Heap[(*size)]=x;
    int i=(*size);
    ELEMENT aux;
	op++;
    while (i>1 && (Heap[i].elem)<(Heap[i/2].elem))
    {
        aux=Heap[i];
        Heap[i]=Heap[i/2];
        Heap[i/2]=aux;
		op+=4;
        i=i/2;
    }
}

ELEMENT extract_heap(ELEMENT Heap[],int *size)
{
    ELEMENT x;
    if((*size)>0)
    {
        x=Heap[1];
        Heap[1]=Heap[(*size)];
        (*size)--;
        heapify(Heap,1,(*size));
    }
    return x;
}


void merge(liste Cap[],int k)
{	
    int size = 0;
    ELEMENT v;
    for(int i=1; i<=k; i++)
    {
        v.poz=i;
        v.elem=Cap[i].prim->info;
        insert_heap(Heap,v,&size);
    }

    ELEMENT e;
    while (size>0)
    {

        e = extract_heap(Heap,&size);
        int poz =e.poz;
        add(&first,&last,e.elem);
        Cap[poz].prim=Cap[poz].prim->urm;
        if (Cap[poz].prim!=NULL)
        {
            v.poz=e.poz;
            v.elem=Cap[e.poz].prim->info;
            insert_heap(Heap,v,&size);
			
        }
    }
}


void generate(liste Cap[],int k,int n)
{
    int nr = 0,i,j;
    for (i=1; i<=k; i++)
        Cap[i].prim = Cap[i].ultim = NULL;

    for (j=1; j<=k; j++) 

    {	nr=rand()%100;
        for (i=1; i<=n/k; i++)
        {
            nr=nr+rand()%100;
            add(&Cap[j].prim,&Cap[j].ultim,nr);
        }
    }

}
void afisare_lista(NOD *prim,NOD *ultim)
{
    NOD *c;
    c=prim;
    while(c!=0)
    {
		
		printf("%d ",c->info);
		c=c->urm;
	}
	printf("\n");
}
void afisare(liste Cap[],int k)
{
    for (int i =1 ; i<=k;i++)
		afisare_lista(Cap[i].prim,Cap[i].ultim);
}
int main()
{
	FILE *pf,*pf1;
	pf=fopen("fis1.csv","a");
	pf1=fopen("fis2.csv","a");
	int n,k,i;
	k=5;
	for (n=100;n<=10000;n+=100)
	{
		op=0;
		generate(Cap,k,n);
		merge(Cap,k);
		nrop[n/100-1]=op;
	}
	for(i=0;i<100;i++)
		fprintf(pf,"%d,",nrop[i]);
		fprintf(pf,"\n");
	
	k=10;
	for (n=100;n<=10000;n+=100)
	{
		op=0;
		generate(Cap,k,n);
		merge(Cap,k);
		nrop[n/100-1]=op;
	}
	for(i=0;i<100;i++)
		fprintf(pf,"%d,",nrop[i]);
		fprintf(pf,"\n");
	k=100;
	for (n=100;n<=10000;n+=100)
	{
		op=0;
		generate(Cap,k,n);
		merge(Cap,k);
		nrop[n/100-1]=op;
	}
	for(i=0;i<100;i++)
		fprintf(pf,"%d,",nrop[i]);
		fprintf(pf,"\n");
	n=10000;
	int j=0;
	for(k=10;k<500;k+=10)
	{
		op=0;
		generate(Cap,k,n);
		merge(Cap,k);
		nrop[j]=op;
		j++;
	}
	for(i=0;i<j;i++)
		fprintf(pf1,"%d,",nrop[i]);
		fprintf(pf1,"\n");



	fclose(pf);
	fclose(pf1);
	getch();
	return 0;
}