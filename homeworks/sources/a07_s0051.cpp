// ***ANONIM*** Melania ***ANONIM***
//Grupa ***GROUP_NUMBER***


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

typedef struct MULTI_NODE{
	int key;
	int count;//=nr fii
	struct MULTI_NODE * child[50];
}MULTI_NODE;

typedef struct BINARY_TREE{
	int key;
	BINARY_TREE *fiu;
	BINARY_TREE *frate_dr;
}BINARY_TREE;


MULTI_NODE* root;
BINARY_TREE* ***ANONIM***;
int k;

MULTI_NODE *createMN(int key)
{
	MULTI_NODE* p=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	p->key=key;
	p->count=0;
	return p;
}

void insertMN(MULTI_NODE* parent,MULTI_NODE* child)
{
	parent->child[parent->count++]=child;
}

MULTI_NODE* transform1(int t[],int size)
{
	MULTI_NODE* nodes[50];
	MULTI_NODE* root=NULL;
	for (int i=0;i<size;i++)
	{
		nodes[i]=createMN(i);
	}

	for(int i=0;i<size;i++)
	{
		if(t[i]==-1)
		{
			root=nodes[i];
		}
		else {
			insertMN(nodes[t[i]],nodes[i]);
		}
	}

	return root;
}

void prettyPrintMN(MULTI_NODE* nod,int nivel=0)
{
	for (int i=0;i<nivel;i++)
	{
		printf("    ");
	}
	printf("%d \n",nod->key);
	for(int i=0;i<nod->count;i++)
	{
		prettyPrintMN(nod->child[i],nivel+1);
	}
}

//Transformarea in arbore binar:
void transform2(MULTI_NODE* root,BINARY_TREE* ***ANONIM***)
{
	if(root!=NULL)
	{
		if(root->count>0)
		{
			***ANONIM***->fiu=(BINARY_TREE*)malloc(sizeof(BINARY_TREE));
			***ANONIM***->fiu->key=root->child[0]->key; //cheia fiului ***ANONIM***acinii BT ia cheia primului fiu al MN 
			***ANONIM***=***ANONIM***->fiu;
			transform2(root->child[0],***ANONIM***);//apelam functia pentru primul copil al MN si fiul ***ANONIM***acinii BT
			

			for(int i=1;i<root->count;i++) //incepem de la frate dreapta
			{
				***ANONIM***->frate_dr=(BINARY_TREE*)malloc(sizeof(BINARY_TREE));
				***ANONIM***->frate_dr->key=root->child[i]->key;
				***ANONIM***=***ANONIM***->frate_dr;
				transform2(root->child[i],***ANONIM***);//apelam functia pentru copilul i al MN si fratele ***ANONIM***acinii BT
				
			}
			***ANONIM***->frate_dr=NULL;
		}
		else ***ANONIM***->fiu=NULL;

	}

}

//afisare arbore binar:
void prettyPrintBT(BINARY_TREE* nod,int nivel=0)
{
	if(nod!=NULL)
	{
		for (int i=0;i<nivel;i++)
		{
			printf("    ");
		}
		printf("%d \n",nod->key);
		prettyPrintBT(nod->fiu,nivel+1);
		prettyPrintBT(nod->frate_dr,nivel);//fratii raman pe acelasi nivel
	}
	else return;
}

//validare vector de tati:
void t_valid(int t[],int size)
{
	for(int i=0;i<=size;i++)
	{
		if(t[i]==-1)
			k++;
	}
}

int main()
{
	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(t[0]);
	
	k=0;
	t_valid(t,size);
	if(k==1)
		printf("Vectorul de tati este corect\n");
	else 
		{
			printf("Vectorul de tati nu este corect. Introduceti alt vector\n");
			exit(0);
	}

	//construim arbore
	root=transform1(t,size);

	printf("Arborele multi-cai:\n");
	prettyPrintMN(root);

	***ANONIM***=(BINARY_TREE*)malloc(sizeof(BINARY_TREE));
	***ANONIM***->key=root->key;
	***ANONIM***->frate_dr=NULL;

	transform2(root,***ANONIM***);

	printf("Arborele binar:\n");
	prettyPrintBT(***ANONIM***);

	return 0;
}
