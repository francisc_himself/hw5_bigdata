/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
In cazul mediu statistic putem zice ca quicksortul este mai eficient decat heapsortul, ajungand la o marime de 5000
diferenta de numar de operatii este de ordinul milioanelor.
Examinand quicksortul in cazul cel mai favorabil iara vedem o eficienta extrem de mare, dar in cazul cel mai
defavorabil quicksortul devine inefecient ajungand la O(n^2).
*/


#include <stdio.h>
#include <conio.h>
#include "Profiler.h"

#define MAX_SIZE 10000

int v[MAX_SIZE];
int v_copy[MAX_SIZE];
int n=0;
int heap_size=0;
int largest=0;
int hsum[MAX_SIZE+1]; //heapsort sum
int hcom[MAX_SIZE+1]; //heapsort comp
int hasi[MAX_SIZE+1]; //heapsort asi
int qcom[MAX_SIZE+1]; //quicksort comp
int qasi[MAX_SIZE+1]; //quicksort asi
int qsum[MAX_SIZE+1]; //quicksort sum

void exchange(int *v, int i, int j)
{
	int aux=v[i];
	v[i]=v[j];
	v[j]=aux;
}

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}

void max_heapify(int *v, int i)
{
	int l=left(i);
	int r=right(i);
	hasi[n]+=2;

	if (l<=heap_size && v[l]>v[i])
	{
		largest=l;
		hasi[n]++;
		hcom[n]++;
	}
	else
	{
		largest=i;
		hasi[n]++;
	}
	hcom[n]++;

	if (r<=heap_size && v[r]>v[largest])
	{
		largest=r;
		hasi[n]++;
		hcom[n]++;
	}
	hcom[n]++;

	if (largest != i)
	{
		int aux=v[i];
		v[i]=v[largest];
		v[largest]=aux;
		hasi[n]+=3;

		max_heapify(v, largest);
	}
	
	hcom[n]++;
}

void build_max_heap_bottom_up(int *v)
{
	heap_size=n;

	for (int i=n/2; i>0; i--)
	{
		max_heapify(v, i);
	}
}

void pritty_print(int *v, int n, int k, int nivel)
{
	if (k>n)
	{
		return;
	}

	pritty_print(v,n,2*k+1,nivel+1);
	for (int i=1; i<=nivel; i++)
	{
		printf("   ");
	}
	printf("%d\n", v[k]);
	pritty_print(v,n,2*k,nivel+1);
}

void heapsort(int *v)
{
	build_max_heap_bottom_up(v);
	for (int i=n; i > 1; i--)
	{
		int aux=v[1];
		v[1]=v[i];
		v[i]=aux;
		heap_size--;
		hasi[n]+=4;

		max_heapify(v,1);
	}
}

void afis_1(int *v,int n)
{
	for (int i=1; i<=n; i++)
	{
		printf("%d ",v[i]);
	}

	printf("\n");
}

int partition(int *v, int p, int r)
{
	int x=v[r];
	int i=p-1;
	qasi[n]+=2;

	for (int j=p; j < r; j++)
	{
		if (v[j] <= x)
		{
			i++;
			qasi[n]++;

			if (v[j] != v[i])
			{
				int aux=v[j];
				v[j]=v[i];
				v[i]=aux;
				qasi[n]+=3;
			}
			qcom[n]++;
		}
		qcom[n]++;
	}
	int aux=v[i+1];
	v[i+1]=v[r];
	v[r]=aux;
	qasi[n]+=3;

	return i+1;
}

void quicksort(int *v, int p, int r)
{
	if (p<r)
	{
		int q=partition(v,p,r);
		qasi[n]++;

		quicksort(v, p, q-1);
		quicksort(v, q+1, r);
	}
	qcom[n]++;
}

int partition_middle(int *v, int p, int r)
{
	exchange(v,(p+r) / 2,r);

	return partition(v, p, r);
}

void quicksort_middle(int *v, int p, int r)
{
	if (p<r)
	{
		int q=partition_middle(v,p,r);
		qasi[n]++;

		quicksort_middle(v, p, q-1);
		quicksort_middle(v, q+1, r);
	}
	qcom[n]++;
}

void initializeCounters()
{
	//initializare counteri
	for (int i=0; i < MAX_SIZE; i++)
	{
		hcom[i]=hasi[i]=hsum[i]=qcom[i]=qasi[i]=qsum[i]=0;
	}
}

void main()
{
	//testare heapsort
	n=10;
	FillRandomArray(v, n+1, 10, 50);
	memcpy(v_copy, v, (n+1)*sizeof(int));
	printf("v:      \t");
	afis_1(v ,n);
	printf("v_copy: \t");
	afis_1(v_copy ,n);
	printf("\n");
	
	heapsort(v);

	printf("heapsort: \t");
	afis_1(v ,n);

	//testare quicksort
	quicksort(v_copy,1,n);

	printf("\nquicksort: \t");
	afis_1(v_copy ,n);

	int limit=5000;

	//average case - generare date
	for (int i=100; i<=limit; i+=100)
	{
		for (int j=1; j<6; j++)
		{
			n=i;
			FillRandomArray(v, n+1, 1, 2*n);
			memcpy(v_copy, v, (n+1)*sizeof(int));
			heapsort(v);
			quicksort(v_copy,1,n);
		}
	}

	//calculare sum
	for (int i=0; i < MAX_SIZE; i++)
	{
		hsum[i]=hasi[i]+hcom[i];
		qsum[i]=qasi[i]+qcom[i];
	}

	FILE *f=fopen("assign3.csv","w");

	fprintf(f, "size,heapsort_sum,quicksort_sum\n");
	for (int i=100; i<=limit; i+=100)
	{
		fprintf(f, "%d,%d,%d\n", i, hsum[i], qsum[i]);
	}

	//best case quicksort_middle
	//crescator

	initializeCounters();

	for (int i=100; i<=limit; i+=100)
	{
			n=i;
			FillRandomArray(v, n+1, 1, 2*n, false, 1);
			quicksort_middle(v,1,n);
	}

	//calculare sum
	for (int i=0; i < MAX_SIZE; i++)
	{
		qsum[i]=qasi[i]+qcom[i];
	}

	fprintf(f, "\nsize,best_quicksort_middle_sum\n");
	for (int i=100; i<=limit; i+=100)
	{
		fprintf(f, "%d,%d\n", i, qsum[i]);
	}

	//worst case quicksort
	//descrescator

	//initializare counteri
	initializeCounters();

	for (int i=100; i<=limit; i+=100)
	{
			n=i;
			
			for (int k=0; k < n+1; k++)
			{
				v[k]=n+1-k;
			}

			quicksort(v,1,n);
	}

	//calculare sum
	for (int i=0; i < MAX_SIZE; i++)
	{
		qsum[i]=qasi[i]+qcom[i];
	}

	fprintf(f, "\nsize,worst_quicksort_sum\n");
	for (int i=100; i<=limit; i+=100)
	{
		fprintf(f, "%d,%d\n", i, qsum[i]);
	}

	fclose(f);
}