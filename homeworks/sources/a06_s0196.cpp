#include<conio.h>
#include<stdio.h>
#include<stdlib.h>

int nr_a,nr_c;

typedef struct nod{

int key;
int numar;
struct nod *stg,*dr,*parent;
}NOD;

NOD * RADACINA;

NOD* TREE_MINIMUM(NOD *x)
{
    nr_c++;
	while (x->stg!=0) 
   {
	  
	   x=x->stg;
       nr_a++;
	   nr_c++;
   }
    return x;
}


NOD* SUCCESOR(NOD* x)
{
	NOD *y;
	
	nr_c++;
	if(x->dr!=0)  return  TREE_MINIMUM(x->dr);
	y=x->parent; nr_a++;
	
	nr_c++;
	while((y!=0)&&(x==y->dr))
	{
	x=y;           nr_a++;
	y=y->parent;   nr_a++;
	}
	return y;

}

void update_nr(NOD *q)
{
	 nr_c++;
  while(q!=0)
  {
	  q->numar=1;    nr_a++;
	   nr_c++;
	  if(q->stg!=0)  
	  {
		  q->numar=q->numar+q->stg->numar;
		   nr_a++;
	  }
		  nr_c++;
	  if(q->dr!=0) 
	{
			  q->numar=q->numar+q->dr->numar;
		   nr_a++;
     }
	  q=q->parent; nr_a++;
  }
}



void TREE_DELETE(NOD *rad,NOD *z)
{
	NOD *y,*x,*q;
   
	nr_c++;
	nr_a++; 
	if((z->stg==0)||(z->dr==0))  
	{
		y=z;  //analiza cazuri 
	    
	 
	}
	else y=SUCCESOR(z);
    
	nr_a++; 
	q=y->parent;

    nr_c++; 
	nr_a++; 
	if(y->stg!=0) x=y->stg;
	else x=y->dr;
	
	
	nr_c++; 
	if(x!=0) 
	{
		x->parent=y->parent;
		nr_a++; 
	}
	
	nr_c++; 
	if(y->parent==0)  
	{
		rad=x;
		nr_a++; 
	}
	else 
	{   
		nr_c++; 
		if(y==y->parent->stg) 
		{
			y->parent->stg=x; nr_a++; 
		}
	     else   
		 {
			 y->parent->dr=x;
			 nr_a++; 
		}
	}
	
	nr_c++; 
	if(y!=z)  
	{   
		nr_a++; 
		z->key=y->key;
	}
    update_nr(q);


}



void afisare(NOD *r,int nivel)
{
   int i;
   if(r!=0)
   {
	   afisare(r->stg,nivel+1);
     for(i=0;i<nivel;i++) printf(" ");
	 
	 printf("(%d,nr=%d)\n",r->key,r->numar);
	 
	 afisare(r->dr,nivel+1);
   }
}


void TREE_INSERT(int key)
{
  NOD *p,*q,*r;
 
   p=(NOD*)malloc(sizeof(NOD));
	p->key=key;
	p->dr=0;
    p->stg=0;
	p->parent=0;
	p->numar=1;

//  nr_c++; 
  if(RADACINA==0)
  {
    RADACINA=p;	
//	nr_a++; 
	return ;
  }
  r=0;
  //nr_a++; 
  q=RADACINA;
  for(;;)
  {
    
	 // q->numar=q->numar+1;
	//  nr_c++; 
	  if(key<q->key)
	{ 
	  //nr_c++; 
	  if(q->stg==0)
	  {
		  p->parent=q; // nr_a++; 
		  q->stg=p;     //nr_a++; 
		  update_nr(q);
		  return;
	  }
	  else 
	  {
		  q=q->stg;
          //nr_a++; 	  
	  }
	}else 
	  {   
		  //nr_c++; 
		  if(key>q->key)
	      {
			// nr_c++; 
	         if(q->dr==0)
	         {
		        p->parent=q;  //nr_a++; 
		        q->dr=p;      //nr_a++; 
		
		        update_nr(q);
		         return;
	          }
	          else 
			  {
				  //nr_a++; 
				  q=q->dr;
			  }
	       }
       }
  }

}

void construire_arbore(int stanga,int dreapta)
{
   int m;
   NOD *y;

   if(stanga<=dreapta)
   if(stanga==dreapta) TREE_INSERT(stanga);
   
   else{
      m=(stanga+dreapta)/2;
     TREE_INSERT(m);
	  construire_arbore(stanga,m-1);
	  construire_arbore(m+1,dreapta);
	  
	  

    }
}

NOD* OS_SELECT(NOD *x,int i)
{
  int r;
  nr_c++;
  nr_a++;
 if (x->stg!=0)
 {
	 r=x->stg->numar+1; 
 }
   //if (x->stg!=0) r=x->stg->numar;
  else r=1;
  nr_c++;
  if(i==r) return x;
  else 
  {   nr_c++;
	  if(i<r) return OS_SELECT(x->stg,i);
      else return OS_SELECT(x->dr,i-r);
  }

}
void main(){

	NOD *z,*y;
	FILE *pf;

	int i,n,m,alfa,j;

	pf=fopen("rezultate.txt","w");
	RADACINA=0;

	
	



for(j=100;j<10000;j=j+100)
{
	
   n=j;
   RADACINA=0;

    construire_arbore(1,n);
	
	nr_a=0;
	nr_c=0;

	m=n/2;
	i=1;
   while(n>0)
  {
	   i=(i-2+m)%n+1;
	   z=OS_SELECT(RADACINA,i);
//	   printf("\n %d",z->key);
	   TREE_DELETE(RADACINA,z);
	   //free(&z);
	   n=n-1;
   } 
  
  
   fprintf(pf,"\n%d\t%d\t%d\t%d",j,nr_a,nr_c,nr_a+nr_c);
}


  //getch();
  fclose(pf);

}