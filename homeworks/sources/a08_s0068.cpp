/****************************
Assign8 ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
*****************************/

#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>
typedef struct tip_nod{
			       int key;
				   int rank;
		   	       struct tip_nod  *p,*stg,*dr;
			     } NOD;
NOD *rad,*vect[10];
NOD * make_set(NOD *x,int key)
{
	x->key=key;
	x->p=x;
	x->rank=0;
	return x;
}
NOD *find_set(NOD *x)
{
	if(x!=x->p)
		x->p=find_set(x->p);
	return x->p;
}
void link(NOD *x,NOD *y)
{
	if(x->rank>y->rank)
	{
		y->p=x;
	}
	else
	{
		x->p=y;
		if(x->rank==y->rank)
		{
			y->rank=y->rank+1;
		}
	}
}
void union_set(NOD *x,NOD *y)
{
	link(find_set(x),find_set(y));
}
void connected_components(int n,int a[8][8])
{
	//NOD *vect[10];
	for (int i=1;i<=n;i++)
	{
	 NOD *x=(NOD*)malloc(sizeof(NOD));
	 x=make_set(x,i);
	 vect[i]=(NOD*)malloc(sizeof(NOD));
	 vect[i]=x;
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			/*for(int k=1;k<=n;k++)
			{*/
			if(a[i][j]!=0)
			{
				if(find_set(vect[j])!=find_set(vect[i]))
				{
					union_set(vect[j],vect[i]);
				}
			}
			//}
		}
	}
}
void prettyPrint()
{
	NOD *x=(NOD*)malloc(sizeof(NOD));
	for(int i=1;i<=7;i++)
	{
		x=find_set(vect[i]);
		printf("%d este reprezentatul lui %d \n",x->key,vect[i]->key);
	}
}
void main()
{
	int n=7;
	int a[8][8];
	int b[8][8];
	for(int i=0;i<8;i++)
	{
		for(int j=0;j<8;j++)
		{
			a[i][j]=0;
			b[i][j]=0;
		}
	}
	a[1][2]=1;
	a[2][3]=1;
	a[3][4]=1;
	a[5][6]=1;
	a[6][7]=1;
	a[5][7]=1;
	
	b[1][2]=1;
	b[2][3]=1;
	b[3][4]=1;
	b[6][7]=1;
	connected_components(n,a);
	prettyPrint();
	printf("Al doilea caz\n");
	connected_components(n,b);
	prettyPrint();
}