/**
Student: ***ANONIM*** ***ANONIM*** Diana
Group: ***GROUP_NUMBER***
Interpretations:
	We take into consideration 3 cases: best, average and worst.
	Best case: 
		Comparisons: Selection sort increases quadratically. Bubble and Insertion are both linear.
		Assignments: Insertion sort increases linearly while Bubble and Selection are constant (nr of assignments is 0).
	Average case:
		Comparisons: All sort algorithms increase quadratically (as can be seen on the charts).Bubble sort increases quicker than the rest,followed by selection sort and insertion sort.
		Assignments: All sort algorithms increase quadratically.Selection sort has fewer nr assignments. Selection sort increases very slow compared with Bubble and Insertion due to the fact that it is linear.
	Worst case:
		Comparisons: All algorithms increase quadratically.As deduces from chart, Bubble sort increases quicker than Insertion and Selection, which increase approximately in the same rate.
		Assignments:  All algorithms increase quadratically. From charts it can be deduced that Selection sort,compared with the other alg, increases almost linearly. Bubble increases very fast, followed by Insertion sort.

In the case of bubble sort,all improvements made do not improve the worst case.
Efficiency,Complexity: 
	Selection:for small data sets;W,A-O(n^2),B-O(n);
	Bubble: for large lists, W,A-O(n^2),B-O(n);
	Insertion: for quite small data sets,W,A-O(n^2),B-O(n);for small arrays: the fastest
Stability: 
	The selection sort algorithm is an stable one, due to the "<" and not "<=" operator in de first if statement which ensures the preservation in the output of the relative order of the inputs with the same value.
	The bubble sort algorithm is stable only if we use ">" instead of ">=" in the if statement.
	The Insertion sort algorithm is also without doubt stable.
**/
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

long asgn=0, comp=0;
void selection(int a[],int n)
{
	int i,j,min,aux;
	asgn=0;comp=0;
	for (i=0;i<n-1;i++)
	{
		min=i;
		for (j=i+1;j<n;j++)
		{
			comp++;
			if (a[j]<a[min])
			{
				min=j;
								
			}
		}
		if(min!=i)
		{
			aux=a[i];
			a[i]=a[min];
			a[min]=aux;
			asgn=asgn+3;
		}
	}
	
}

void insertion(int a[], int n)
{
	int j,aux,k;
	asgn=0;comp=0;
	for(j=1;j<n;j++)
	{
		aux=a[j];
		k=j-1;
		asgn++;
		while(a[k]>aux && k>=0)
			{	
				a[k+1]=a[k];
				k--;
				asgn++;
				comp++;
			}
		a[k+1]=aux;
		comp++;
		asgn++;
	}
}

void bubble(int a[], int n)
{
	int i,j,aux,ordered;
	asgn=0;comp=0;
	do
	{
		ordered=0;
		for (i=0;i<n-1;i++)
		{	
			comp++;
			if (a[i]>a[i+1])
			{
				aux=a[i];
				a[i]=a[i+1];
				a[i+1]=aux;
				ordered=1;
				asgn=asgn+3;
			}
		}
	}while(ordered==1);

}
void main()
{
	int a[10000],i,var;

	//BEST
	FILE *f1;
	f1=fopen("best.csv","w");
	fprintf(f1,"n,cb,ab,cb+ab,ci,ai,ci+ai,csel,asel,csel+asel\n");

	FILE *f2;
	f2=fopen("avg.csv","w");
	fprintf(f2,"n,cb,ab,cb+ab,ci,ai,ci+ai,csel,asel,csel+asel\n");
	
	FILE *f3;
	f3=fopen("worst.csv","w");
	fprintf(f3,"n,cb,ab,cb+ab,ci,ai,ci+ai,csel,asel,csel+asel\n");

	long sumc[3],suma[3];
	for (int n=100;n<=10000;n=n+100)
	 {
	   //BEST

			for (int i=0;i<n;i++)
				a[i]=i;

			//Bubble
			bubble(a,n);
			fprintf(f1,"%d,%d,%d,%d,",n,comp,asgn,asgn+comp);
	   
			//Insertion
			insertion(a,n);
			fprintf(f1,"%d,%d,%d,",comp,asgn,asgn+comp);
		
			//Selection
			selection(a,n);
			fprintf(f1,"%d,%d,%d\n",comp,asgn,asgn+comp);
	 

	 //AVERAGE
			sumc[0]=0;sumc[1]=0;sumc[2]=0;
			suma[0]=0;suma[1]=0;suma[2]=0;
			int b[10000],c[10000];
			//5 measurements
			for(int j=0;j<5;j++) 
			{
			    srand(time(NULL));
				for (int i=0;i<n;i++)
				{
						a[i]=rand()%10000;
						b[i]=a[i];
						c[i]=a[i];
				}

				//Bubble
				bubble(a,n);
				sumc[0]=sumc[0]+comp;
				suma[0]=suma[0]+asgn;

				 //Insertion
				insertion(b,n);
				sumc[1]=sumc[1]+comp;
				suma[1]=suma[1]+asgn;

				//Selection
				selection(c,n);
				sumc[2]=sumc[2]+comp;
				suma[2]=suma[2]+asgn;
			}
			//Compute average of measurements
			suma[0]=suma[0]/5;sumc[0]=sumc[0]/5;
			suma[1]=suma[1]/5;sumc[1]=sumc[1]/5;
			suma[2]=suma[2]/5;sumc[2]=sumc[2]/5;
			fprintf(f2,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,sumc[0],suma[0],suma[0]+sumc[0],sumc[1],suma[1],suma[1]+sumc[1],sumc[2],suma[2],suma[2]+sumc[2]);
	

	 //WORST 
			
			//Bubble
			for (int i=0;i<n;i++)
				a[i]=n-1-i;
			bubble(a,n);
			fprintf(f3,"%d,%d,%d,%d,",n,comp,asgn,asgn+comp);

			//Insertion
			for (int i=0;i<n;i++)
				a[i]=n-1-i;
			insertion(a,n);
			fprintf(f3,"%d,%d,%d,",comp,asgn,asgn+comp);

			//Selection 
			for (int i=1;i<n;i++)
				a[i-1]=i;
			a[n-1]=0;
			selection(a,n);
			fprintf(f3,"%d,%d,%d\n",comp,asgn,asgn+comp);

	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

