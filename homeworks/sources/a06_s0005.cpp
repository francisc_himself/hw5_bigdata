/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Permutarea Josephus
Complexitate O(n lgn)
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct node {
	int key, size;
	node *st, *dr, *p;
};

int nr=0;

node* build_tree(node *p, int  st, int dr);
node* os_select(node *x, int i);
void os_del(node **root, node *t);
node* tree_min(node *x);
void josephus(node **root, int n, int m);
void printx(node *root, int depth);

int main () {
	node * root;
	int i;
	FILE *p;
	
	josephus(&root, 10, 5);
	

	/*p=fopen("josephus.csv", "w");
	fprintf(p, "n, nr\n");
	for(i=100;i<=10000;i=i+100) {
		nr=0;
		josephus(&root, i, i/2);
		fprintf(p, "%d, %d\n", i, nr);
	}*/
	
	return 0;
}


node* build_tree(node*p, int st, int dr) {
	node *x=NULL;
	int t;
	if(st<=dr) {
		t=(st+dr)/2;
		x=(node*) malloc(sizeof(node));
		x->p=p;
		x->key=t;
		x->st=build_tree(x, st, t-1);
		x->dr=build_tree(x, t+1, dr);
		nr++;
		x->size=dr-st+1;
		return x;
	}
	else return NULL;
}


void printx(node *root, int depth) {
	int i;
	if(root!=NULL) {
		printx(root->st, depth+1);
		for(i=0;i<depth;i++)
			printf("\t");
		printf("<%d, %d>\n", root->key, root->size);
		printx(root->dr, depth+1);
	}
}

node* os_sel(node *x, int i) {
	int r;
	x->size--;
	if(x->st!=NULL)
		r=x->st->size+1;
	else
		r=1;
	nr++;
	if(i==r)
		return x;
	else if(i<r)
		return os_sel(x->st, i);
	else
		return os_sel(x->dr, i-r);
}

void os_del(node **root, node *t) {
	node *x, *y;
	if(t->st==NULL || t->dr==NULL)
		y=t;
	else
		y=tree_min(t->dr);

	if(y->st!=NULL)
		x=y->st;
	else
		x=y->dr;

	if(x!=NULL)
		x->p=y->p;

	if(y->p==NULL)
		*root=x;
	else {
		if(y==y->p->st)
			y->p->st=x;
		else
			y->p->dr=x;
	}
	if(y!=t)
		t->key=y->key;
	nr+=6;
	free(y);
}

node* tree_min(node* x) {
	while(x->st!=NULL) {
		x->size--;
		x=x->st;
		nr++;
	}
	return x;
}

void josephus(node **root, int n, int m) {
	(*root)=build_tree(NULL, 1, n);
	printx(*root, 0);
	printf("\n\n");
	int j=1;
	int k;
	node *x;
	for(k=n;k>=1;k--) {
		j=(j+m-2)%k+1;
		x=os_sel(*root, j);
		printf("am scos %d \n", x->key);
		os_del(root, x);
		//printx(*root, 0);
		//printf("\n\n");
	}
}