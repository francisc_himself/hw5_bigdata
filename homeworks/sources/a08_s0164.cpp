/*
Student:					***ANONIM*** ***ANONIM***
Grupa:						***GROUP_NUMBER***
Cerinta:	

make set genereaza un arbore -O(1)
union face un arbore din 2 - O(1)- radacina unui arbore pointeaza spre radacina altui arbore
find representative- O(logn) 

Implementati corect si eficient operatiile de baza pentru multimi disjuncte:
-Make-set, Union, Find-set folosind arbori- paduri de arbori
-folositi path compression si union by rang in implementare pentru varfuri beneficia de timp mai bun de rulare
-gasirea componentelor conexe varfuri unui graf
-Cormen 21.1, 21.3

-Algoritmul de gasire varfuri componentelor conexe din graf trebuie testat pe un graf de dimensiuni mici, hardcore in functia main
-setati nr de noduri v=10000, variati numarul de muchii E de la 10000 ola 60000, in fiecare caz, generati grafuri aleatoare
aplicati algoritmul de componente conexe si numariti numarul de apeluri catre functiile make-set, union, find-set.
-generati un grafic in care sa aratati numarul de operatii in functie de cum variaza numarul de muchii



*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

int nrApeluri=0;//numarul de apeluri catre makeSet, myUnion, findSet

typedef struct node
{
	int cheie;
	int rang;
	node *parinte;
}NOD;// structura unui nod

typedef struct arc
{
	int x;
	int y;
}ARC;

void makeSet(NOD* &x)
{
	nrApeluri++;
	x->parinte=x;
	x->rang=0;
}

NOD* findSet(NOD* x)
{
	nrApeluri++;
	if(x!=x->parinte)
		x->parinte=findSet(x->parinte);
	return x->parinte;
}

void link(NOD* x,NOD* y)
{
	if(x->rang > y->rang)
		y->parinte=x;
	else
		x->parinte=y;
	if(x->rang==y->rang)
		y->rang=y->rang+1;
}


void myUnion(NOD* x, NOD* y)
{
	nrApeluri++;
	link(findSet(x),findSet(y));
}

void cerinta1()
{
	int V=8;//numarul de varfuri
	NOD **varfuri = (NOD**)malloc(V*sizeof(NOD*));
	
	for(int i=0;i<V;i++)
	{
		varfuri[i]=(NOD*)malloc(sizeof(NOD));
		varfuri[i]->cheie=i;
	}
	
	int E=6;

	ARC * arce = (ARC*)malloc(E*sizeof(ARC));
	arce[0].x =1;
	arce[0].y =2;

	arce[1].x =1;
	arce[1].y =5;

	arce[2].x =3;
	arce[2].y =5;

	arce[3].x =3;
	arce[3].y =4;

	arce[4].x =5;
	arce[4].y =6;

	arce[5].x =6;
	arce[5].y =8;


	for(int i=0;i<V;i++){
		makeSet(varfuri[i]);
    }


    
	for(int i=0; i<E; i++)
	{
		
		int aux1= arce[i].x-1;
		int aux2= arce[i].y-1;
	
		NOD *x = findSet(varfuri[aux1]);
		NOD *y = findSet(varfuri[aux2]);
			
		if(x!=y)
		{
			myUnion(x,y);
		}

	}

	
	for(int i=0; i<V; i++){
		printf("%d ",findSet(varfuri[i])->cheie);
	}
	printf("\n");
}


void cerinta2()
{
int V=10000;//numarul de varfuri
	int E;//numarul de muchii
	FILE* f;//fisierul in care vom scrie
	f=fopen("Apeluri.csv","w");//deschidem fisierul
	fprintf(f,"muchii , apeluri\n");//pregatim capul de tabel

	NOD** varfuri = (NOD**)malloc(V*sizeof(NOD*));

	
	for(int i=0;i<V;i++)
	{
		varfuri[i]=(NOD*)malloc(sizeof(NOD));
		varfuri[i]->cheie=i;
	}
	for(int i=0;i<V;i++)
	{
		makeSet(varfuri[i]);
	}
	for(E=10000;E<=60000;E+=1000)// variem numarul de muchii 
	{

	nrApeluri=0;// reinitializam numarul de apeluri
		for(int i=0;i<E;i++)
		{
			
				int a=rand()%9999;
				ptdiferite:int b=rand()%9999;
				if(a==b)
				{
					goto ptdiferite;
				}
				else{	
						NOD *x = findSet(varfuri[a]);
						NOD *y = findSet(varfuri[b]);
						if(x!=y)
							{
								myUnion(x,y);
							}
				}
					
					
			
			
		}

		fprintf(f,"%d,%d\n",E,nrApeluri);
	
	}	
	fclose(f);
}


void main()
{
	cerinta1();
	cerinta2();
	
	getch();
	
}