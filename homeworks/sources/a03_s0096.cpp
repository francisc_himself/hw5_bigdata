// Lab04Final.cpp : Defines the entry point for the console application.
//

#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

long aH,cH,aQ,cQ;

void generateAverage(int *x, int n);
void generateBest(int *x, int n);
void heapSort(int *x, int heapSize);
void resetVariables();
void demo();
void heapSort(int *x, int heapSize);
void quickSort(int* A, int p, int r,int op);
int left(int i){return 2*i+1;}
int right(int i){return 2*i+2;}
int parent(int i){ return (i-1)/2;}

void main(){
	//demo();
	FILE *buFile;
	int *A;
	int size;
	buFile = fopen("AverageHeapAndQuick.csv", "w");
	fprintf(buFile,"Size,HeapSort,QuickSort");
	for ( size=100; size<=10000; size+=100 ){
		printf("\n%d",size);
		A=(int*)malloc(sizeof(int)*size);
		for(int i=0;i<5;i++){
		generateAverage(A,size);
		quickSort(A,0,size,1);
		heapSort(A,size);		
		}
		fprintf(buFile, "\n%d,%d,%d", size, (aH + cH)/5, (aQ + cQ)/5);
		resetVariables();
	}
	buFile = fopen("QuickCases.csv", "w");
	fprintf(buFile,"Size,Average,Best,Worst");
	for ( size=100; size<=10000; size+=100 ){
		printf("\n%d",size);
		A=(int*)malloc(sizeof(int)*size);
		for(int i=0;i<5;i++){
		generateAverage(A,size);
		quickSort(A,0,size,1); //average case
		}
		fprintf(buFile, "\n%d,%d,", size, (aQ + cQ)/5);
		resetVariables();

		quickSort(A,0,size,2); //best case
		fprintf(buFile, "%d,", (aQ + cQ));
		resetVariables();

		quickSort(A,0,size,3); // worst case
		fprintf(buFile, "%d", (aQ + cQ));
		resetVariables();			
	}

}

int partition(int* A,int p, int r,int op){
	long x,i,aux,indexAux;
	//Average 1
	if(op==1){
	x=A[r];
	indexAux=r;
	}
	//Best 2
	if(op==2){
		x=A[(r+p)/2];
		indexAux=(r+p)/2;
	}
	//Worst 3
	if(op==3){
		indexAux=p;
		x=A[p];
	}
	aQ++;
	i=p-1;
	for(int j=p;j<=r-1;j++){
		cQ++;
		if(A[j]<=x){
			i++;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
			aQ+=3;
		}
	}
	aux=A[i+1];
	A[i+1]=A[indexAux];
	A[indexAux]=aux;
	aQ+=3;
	return (i+1);
}

void quickSort(int* A, int p, int r,int op){
	if(p<r){
		int q=partition(A,p,r-1,op);
		quickSort(A,p,q-1,op);
		quickSort(A,q+1,r,op);
	}
}

void maxHeapify(int *x, int heapSize, int i){

	int l=left(i),r=right(i),largest;
	cH++;
	if(l<heapSize &&  x[l]>x[i])
		largest = l;
	else largest = i;
	cH++;
	if(r<heapSize && x[r]>x[largest])
		largest = r;
	if(largest!=i){
		int aux=x[i];
		x[i]=x[largest];
		x[largest]=aux;
		aH+=3;
		maxHeapify(x,heapSize,largest);
	}
}

void buildMaxHeapBottomUp(int *x, int heapSize){
	for(int i=heapSize/2-1;i>=0;i--)
		maxHeapify(x,heapSize,i);
}

void heapSort(int *x, int heapSize){

	int aux,heapSizeAux=heapSize-1;

	buildMaxHeapBottomUp(x,heapSize);

	for(int i=heapSize-1;i>=0;i--){
			aux=x[i];
			x[i]=x[0];
			x[0]=aux;
			aH+=3;
			heapSizeAux--;
			maxHeapify(x,heapSizeAux,0);
	}
}

void generateAverage(int *x, int n){
	for(int i=0;i<n;i++)
		x[i]=rand();
}

void demo(){
	
	int length=10;
	int* A=(int*)malloc(sizeof(int)*length),*AuxA;
	generateAverage(A,length);
	AuxA=A;
	for(int i=0;i<length;i++)
		printf("%d ",A[i]);

	heapSort(A,length);
	printf("\nThe sorted array with the heap sort method is:\n");
	for(int i=0;i<length;i++)
		printf("%d ",A[i]);

	quickSort(A,0,length,1);
	printf("\nThe sorted array with the quick sort method is:\n");
	for(int i=0;i<length;i++)
		printf("%d ",A[i]);
}

void resetVariables(){aH=0;cH=0;aQ=0;cQ=0;}