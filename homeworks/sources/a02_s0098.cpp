#include<iostream>
#include<conio.h>
#include<fstream>

using namespace std;

int h,n,heapSize=10;

long compUp=0;
long assignUp=0,aTD=0,cTD=0,aBU=0,cBU=0;
/*
// bottom up construction of the heap goes from the half of the length to the first element
//the left child gets the value * 2
//the right child gets the value *2 +1
//Running time: for 1 element processing: O(h); for n elements: O(n)

//top down
//add a new element at the bottom
//Running time: for 1 element processing: O(h); for n elements: O(nlgn)

// after comparing the resulted outputs, it can be observed that the bottom up approach is better than the top down approach
// in average case both approaches increase liniarly, but bottom up increases faster
//even in worst case bottom up approach is better
//worst case for an increasingly ordered array

// Bottom-up approach is used for sorting, and top-down approach is used as priority queue
//regarding the running time-- both approaches are liniar
*/
void heapify2(long A[], int i, int n)  //i=index of the root, elem. to be added
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    compUp=compUp+1; cBU++;
    if (left<=n && A[left]>A[i]) 
        largest=left;	//root, left or right child index
    else
        largest=i;

    compUp=compUp+1; cBU++;
    if (right<=n && A[right]>A[largest])
        largest=right;
    if (largest!=i) //one of the children larger than the root
        {
            assignUp+=3; aBU=aBU+3;
			//swap root with largest child
            aux=A[i];
            A[i]=A[largest];
            A[largest]=aux;
            heapify2(A,largest,n);  //continue the process on the heap
        }
}

void bottomup(long A[],int n)
{
    int i;
    for (i=n/2;i>=1;i--) //from the non-leave nodes until we reach the root
        heapify2(A,i,n); //build the heap out of 2 already built heaps and 1 node
}


void topDown(long a[], long length, long *atribuiri, long *c)
{
	heapSize=1;
	int aux,dim;
	for(int i=2; i<=length; i++)
	{
		heapSize++;
		 dim=heapSize;
		(*c)++;
		while(dim>1 && a[dim/2]<a[dim])//parent >child
		{
			(*c)++;
			(*atribuiri)+=3 ;
			aux=a[dim];
			a[dim]=a[dim/2];
			a[dim/2]=aux;
			dim=dim/2;
			//length++;// altfel ,nu ar merge niciodata pana la finalul vectorului
		}
		
	}
}


void print(long a[],int n){
	int i,j,pow=2,level=1,nr=5; 
	printf("              %d\n\n ",a[1]); //print the first element in the array
	for(i=2;i<=n;){
		level=pow; //save the value of pow
		for(j=0;j<nr;j++) 
			printf("  ");  //print a number of spaces at the beginning of each level
		while(i<=n && level>0){  //at each level print 2^i elements
			printf(" %d ",a[i]);
			i++;  //increment the index in the array
			level--;  //decrement the pow
		}
			printf("\n\n");	
			pow=pow*2;  //prepare the power for the next level display
			nr--;   //decrement the nr of spaces to be displayed on the next level
	}
}
void main()
{
	
	ofstream averageCase("averageCase.txt");
	ofstream worstCase("worst.txt");
	long m,l,j,h,a[11],heapSize=10, atribuiri=0,c=0;
	long outBU[10001], outTD[10001],aTD=0,cTD=0,k;
	 long compup,compdown,assignup,assigndown;
	a[1]=4;
	a[2]=16;
	a[3]=10;
	a[4]=14;
	a[5]=7;
	a[6]=9;
	a[7]=3;
	a[8]=11;
	a[9]=8;
	a[10]=15;

	//bottom Up
	bottomup(a,10);
	print(a,10);
	
	a[1]=4;
	a[2]=16;
	a[3]=10;
	a[4]=14;
	a[5]=7;
	a[6]=9;
	a[7]=3;
	a[8]=11;
	a[9]=8;
	a[10]=15;
	
	//top down
	topDown(a,10,&aTD, &cTD);
	print(a,10);
	
	
	
	 for (int i=1;i<=10000;i=i+100)
	{	compUp=assignUp=0;
	
		long sumcBU=0,sumaBU=0,sumaTD=0,sumcTD=0;
		for (int j=1;j<=5;j++)
		{
			l=0;compup=0;
			assignup=0;
			for ( k=1;k<=i;k++)
			{
				outBU[k]=outTD[k]=rand();	
			}
			
			bottomup(outBU,i);
			topDown(outTD,i, &aTD,&cTD);
			sumcBU+=compUp;
			sumaBU+=assignUp;
			//sumcBU+=cBU;
			//sumaBU+=aBU;
			compup+=compUp;
			assignup+=assignUp;
			sumcTD+=cTD;
			sumaTD+=aTD;
			aBU=0;
			cBU=0;
			aTD=0;
			cTD=0;
		}
			averageCase<<k<<"              "<<(compup/5+assignup/5)<<"        "<<(sumaTD+sumcTD)/5<<"\n";
	}

	 //worst case
	 for(int i=1; i<10000; i=i+100)
	{
		for(int j=1; j<=i; j++)
		{
			outBU[j]=j;
			outTD[j]=j;
		}
		
		bottomup(outBU,i);
		topDown(outTD,i, &aTD,&cTD);
  		worstCase<<i<<"     "<<aBU+cBU<<"           "<<aTD+cTD<<"\n";
		
			aTD=0;
			cTD=0;
			aBU=0;
			cBU=0;
	}
getch();	
}