#include "stdafx.h"
#include<iostream>
#include<conio.h>
#include<stdio.h>
#include "Profiler.h"

#define MAX_SIZE 1000

Profiler profiler("demo");
using namespace std;

void duplicatearray();
void bubbleSort();
void insertionSort();
void selectionSort();
void print();
void averageCaseScenario();
void bestCaseScenario();
void worstCaseScenario();

int n, a[10000], b[10000], m = 5;

void duplicatearray()
{
    for(int i = 1; i <= n; i++)
        b[i] = a[i];
}
void print()
{
	printf("\n");
    for(int i = 1; i <= n; i++)
        printf("%d ",b[i]);
	printf("\n");
}
// :)
void averageCaseScenario()
{
	n=100;
	for(int i = 0; i < m; i++)
	{
		FillRandomArray(a, n);
		duplicatearray();
		bubbleSort();

	    duplicatearray();
		insertionSort();

		duplicatearray();
		selectionSort();

		n+=100;
	}
	
	profiler.createGroup("Comparisons","comparisons","comparisonsIns","comparisonsSel");
	profiler.createGroup("Selection Assignments","assignmentsSel");
	profiler.createGroup("Assignments","assignments","assignmentsIns","assignmentsSel");
	profiler.addSeries("Bubble", "assignments", "comparisons");
	profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
	profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
	profiler.createGroup("All","Bubble","Insertion","Selection");
	profiler.showReport();
}
void bestCaseScenario()
{
	n=100;
	//for Bubble Sort the best case is when the string is already sorted
	for(int j = 0; j < m; j++)
	{
	for(int i = 1; i <= n; i++)
		a[i] = i;
	duplicatearray();
	bubbleSort();

	duplicatearray();
	insertionSort();

	duplicatearray();
	selectionSort();

	n+=1500;
	}
	profiler.createGroup("BubbleBest assignments", "assignments");
	profiler.createGroup("BubbleBest", "assignments","comparisons");
	profiler.createGroup("InsertionBest comparisons","comparisonsIns");
	profiler.createGroup("InsertionBest", "assignmentsIns","comparisonsIns");
	profiler.createGroup("SelectionBest assignments","assignmentsSel");
	profiler.createGroup("SelectionBest", "assignmentsSel","comparisonsSel");
	profiler.showReport();
}
void worstCaseScenario()
{
	n=100;
	for(int j = 0; j < m; j++)
	{
		for(int i = 1; i <= n; i++)
			a[i] = n - i + 1;
		// Bubble Sort, the worst case is when the array is reversed
		duplicatearray();
		bubbleSort();

		duplicatearray();
		insertionSort();


		//Selection Sort is appropriate only for small n
		int k = n/2;
		for(int i = 1; i <= (n+1)/2; i++)
			b[i] = ++k;
		k = n/2;
		for(int i=(n+1)/2 + 1; i <= n; i++)
			b[i] = k--;
		selectionSort();

	n+=100;
	}
	profiler.createGroup("BubbleWorst", "assignments","comparisons");
	profiler.createGroup("InsertionWorst", "assignmentsIns","comparisonsIns");
	profiler.createGroup("SelectionWorst", "assignmentsSel","comparisonsSel");
	profiler.showReport();
}
void bubbleSort()
{
 //   printf("\nBubble Sort of the array: ");
    int inter=0, aux;
	int nbofAssign = 0;
	int nbofComp = 0;
    do{
        inter = 0;
        for(int i = 1; i < n; i++)
        {
			profiler.countOperation("comparisons",n);
		//	nbofComp++;
            if(b[i] > b[i+1])
            {
                aux    = b[i];
                b[i]   = b[i+1];
                b[i+1] = aux;
				profiler.countOperation("assignments",n,3);
               nbofAssign +=3;
                inter = 1;
            }
        }
    }while(inter);
	if(nbofAssign == 0) profiler.countOperation("assignments",n,0);

}
void insertionSort()
{
//    printf("\nInsertion sort of the array: ");
    int i, buff, k;
	int nbofAssign = 0;
	int nbofComp = 0;
    for(i = 2; i <= n; i++)
    {
        k = i-1;
        buff = b[i];
		profiler.countOperation("assignmentsIns",n);
 //      nbofAssign++;
        while(b[k] > buff && k>0)
        {    
			profiler.countOperation("comparisonsIns",n);
   //         nbofComp ++;
            b[k+1] = b[k];
			profiler.countOperation("assignmentsIns",n);
  //         nbofAssign++;
            k--;
		}
       b[k+1] = buff;
	   profiler.countOperation("assignmentsIns",n);
  //          nbofAssign++;
	}  
		profiler.countOperation("comparisonsIns",n);
    //   nbofComp++;
}	
void selectionSort()
{
    int imin, min;
	int nbofAssign = 0;
	int nbofComp = 0;
    for(int i=1; i<n; i++)
    {

		 imin = i;
        for(int j = i + 1; j <= n; j++)
        {
			profiler.countOperation("comparisonsSel",n);
//            nbofComp ++;
            if(b[j] > b[imin]);
            else
            {
                imin = j;
            }
        }
        if(i != imin)
        {
            min     = b[imin];
            b[imin] = b[i];
            b[i]    = min;
			profiler.countOperation("assignmentsSel",n,3);
//            nbofAssign+=3;
        }
    }
}

int main()
{
   /* read();
    duplicatearray();
	printf("\n	The unordered sequence: ");
    print();
    bubbleSort();
	printf("\n	The output sequence after bubble sort: ");
    print();
    duplicatearray();
    insertionSort();
	printf("\n	The output sequence after insertion sort: ");
    print();
    duplicatearray();
    selectionSort();
	printf("\n	The output sequence after selection sort: ");
    print();*/
	averageCaseScenario();
	//bestCaseScenario();
	//worstCaseScenario();
    return 0;
}

