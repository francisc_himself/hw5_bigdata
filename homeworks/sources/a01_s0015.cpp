/*	
	datorita oprimizarii pe sortarea bulelor se poate observa ca aceasta metoda este cea mai buna O(n) in cazul 
	favorabil.
	de asemenea se observa ca toate cele 3 metode de sortare sunt de complexitate O(n^2)
	In cazul defavorabil, eficienta este O(n^2). 
*/


#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <time.h>

#define MAX 10001

//contoarele care vor numara atribuirile si comparatiile, corespunzatoare fiecarui tip de sortare
int atrb, cmpb, atrs, cmps, atri, cmpi; 
int j;
//Sortare prin METODA BULELOR(interschimbare)
//Buble sort este un test de vecinatate; dupa prima parcurgere, el maxim se afla pe ultima pozitie

void buble(int A[], int n)
{int aux;
int i=1;
bool ok= false;


while(!ok)
{	ok= true;
		for(int j= 0; j< n-i ;j++)
		{	cmpb++;    //contorizeaza numarul de comparatii
			if(A[j]>A[j+1])
				{atrb=atrb+3;
				 //contorizeaza numarul de atribuiri
				aux=A[j];
				A[j]=A[j+1];
				A[j+1]=aux;
				ok= false;}
		}
		i++;
}
	
}

//Sortare prin SELECTIE
void selectie(int A[],int n)
{
	int min, aux;
	int i_min;

	for(int i=0; i< n-1; i++)
	{ atrs++;
		min= A[i];
	    i_min=i;
		for(int j=i+1; j<n; j++)
		{cmps++;  //contorizeaza numarul de comparatii
			if(A[j]<min)
			{atrs++;
			min=A[j];
			i_min=j;}
		}
		aux=A[i];
		A[i]=A[i_min];
		A[i_min]=aux;
		atrs+=3; //contorizeaza numarul de atribuiri
	}
}

//Sortare prin  INSERTIE
void insertie(int A[], int n)
{
	for(int i=0; i<n; i++)
	{ 
	int x=A[i];
	int j=1;
	atri++;
	cmpi++; //contorizeaza numarul de comparatii
			while (A[j]<x)
			{j=j+1;
			}
			for(int k=i; k>=j+1; k--)
			{atri++;//contorizeaza numarul de atribuiri
				A[k]=A[k-1];
			 
			}
	A[j]=x;
	atri++; 
	}
}

void copie_sir(int sursa[], int dest[], int n)
{
	int i;
	for(i=1; i<=n;i++)
		dest[i]=sursa[i];
}

void main()
{	//declratii de variabile
	FILE *f;
	int n;
	int j;
	int i;
	int a[MAX];
	int b[MAX];
	int c[MAX];
	int d[MAX];
	int e[MAX];
	int g[MAX];

	srand(time(NULL)); //initializeaza pasul pentru functia rand

//	f=fopen("favorabil.csv","w");
	f=fopen("defavorabil.csv","w");
//	f=fopen("mediu_statistic.csv","w");

	//scrierea capului de tabel
//	fprintf(f,"n,bubble_favorabil_atr,bubble_favorabil_comp,bubble_favorabil_comp+atr,selectie_favorabil_atr,selectie_favorabil_conp,selectie_favorabilatr+comp,insertie_favorabil_atr,insertie_favorabil_comp,insertie_favorabil_atr+comp\n");
//	fprintf(f, "n, bubble_mediu_atr,bubble_mediu_comp,bubble_mediu_atr+comp,selectie_mediu_atr,selectie_mediu_comp,selectie_mediu_atr+comp,insertie_mediu_atr,insertie_mediu_comp,insertie_mediu_comp+atr\n");
	fprintf(f, "n, buble_defavorabil_atr,buble_defavorabil_comp,buble_defavorabil_atr+comp,selectie_defavorabil_atr,selectie_defavorabil_cmp,selectie_defavorabil_atr+cmp,insert_def_atr,insert_def_comp,insert_def_atr+comp\n");

	
	for(n=100; n<10001; n=n+100)
	{ atrb=0;
	  cmpb=0;
	  atrs=0;
	  cmps=0;
	  atri=0;
	  cmpi=0;
	 
	  
	  /*
	  //////////////////////////////////////////////////////////////////////////////////////
	//se foloseste doar pentru mediul statistic,pentru a efectua 5 masuratori
	  for(j=1;j<=5;j++) 
	  {	
		  for( i=1; i<=n; i++)
		  { 	
					//genereaza sirul aleator
					a[i]=rand()%RAND_MAX;;
					b[i]=a[i];
					c[i]=a[i];
		  }
		  for( i=1; i<=n; i++)
		  {		    b[i]=a[i];
					c[i]=a[i];}

		  buble(a,n);
		  selectie(b,n);
		  insertie(c,n);

	}
			//scrierea in fisier pentru cazul mediu statistic
	  fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,atrb/5,cmpb/5,(atrb+cmpb)/5,atrs/5,cmps/5,(atrs+cmps)/5,atri/5,cmpi/5,(atri+cmpi)/5);

	 */    
		
	  //introduce valori pentru cazul favorabil (ordonate crescator) si defavorabil ordonate descrescator
	
		  for( i=0; i<=n; i++)
		  { 		a[i]=i;
					b[i]=i;
					c[i]=i;
					d[i]=n-i;
					e[i]=n-i;
					g[i]=n-i;
		  }
		  //apelul functiilor de sortare pentru cazul favorabil
		/*	buble(a, n);
			selectie(b,n);
			insertie(c,n);*/

		 //apelul functiilor de sortare pentru cazul defavorabil

			buble(d,n);
			selectie(e,n);
			insertie(g,n);

		//scrierea in fisier pentru cazurile favorabil/defavorabil
	    fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,atrb,cmpb,atrb+cmpb,atrs,cmps,atrs+cmps,atri,cmpi,atri+cmpi);
		

			 
		
	
		

	}
	
} 




//main pentru testarea algoritmilor de sortare

/*
int main()
{
	int vect[10]={3, 5, 6, 9, 7, 10, 8, 0, 13, 2};
	int vect2[10]= {3, 5, 6, 9, 7, 10, 8, 0, 13, 2};
	int vect3[10]= {3, 5, 6, 9, 7, 10, 8, 0, 13, 2};
	buble(vect, 10);
	cout<<"Buble sort ";
	 for(int i=0; i<10; i++)
		 cout<<vect[i]<<" ";
	 cout<<"\n";

	 cout<<"Sortare prin selectie ";
	selectie(vect2,10);
	 for(int j=0; j<10;j++)
		 cout<<vect2[j]<<" ";
	 cout<<"\n";

	 cout<< "Sortare prin insertie";
	 insertie(vect3, 10);
	 for(int k=0; k<10;k++)
		 cout<< vect3[k]<<" ";
	
	
	getch();
	 return 0;
     } */