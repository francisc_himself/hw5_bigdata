﻿
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>

/*
Analysis of HeapSort and QuickSort:
-measurements performed on values from 100 to 10000, with an increment of 100

Average case:
OVERALL: QuickSort is more efficient, performing between 1962 and 438404 total operations, while Heapsort performs between 3017 and 631186
 
Analyzing Quicksort:
a)Best: each partition divides the array into 2 equal parts
b)Average: is close to best case
c)Worst: each partition divides the array into 1 and the rest of the elements

Quicksort: Not an optimal alg O(n^2)>Ω(n·lgn). 
		  O(nlgn) for best and average case
		  Worst case occurs seldom

Conclusion: Heapsort is an optimal sorting algorithm. In practice, quicksort, even not optimal, behaves better.
			A good implementation of quicksort IS optimal.
			


*/



long compH;
long assignH;
long compQ;
long assignQ;
long heapSize;

void heapify(long A[], int i)  //i=index of the root, elem. to be added
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    compH=compH+1;
    if (left<=heapSize && A[left]>A[i]) 
        largest=left;	//root, left or right child index
    else
        largest=i;

    compH=compH+1;
    if (right<=heapSize && A[right]>A[largest])
        largest=right;
    if (largest!=i) //one of the children larger than the root
        {
            assignH+=3;
			//swap root with largest child
            aux=A[i];
            A[i]=A[largest];
            A[largest]=aux;
            heapify(A,largest);  //continue the process on the heap
        }
}

void bh_bottomup(long A[],int n)
{
    int i;
	heapSize=n;
    for (i=heapSize/2;i>=1;i--) //from the non-leave nodes until we reach the root
        heapify(A,i); //build the heap out of 2 already built heaps and 1 node
}



void heapSort(long A[],int n)
{
    int k;
    long aux;
    bh_bottomup(A,n);
    for (k=n;k>=2;k--)     //except for the first and last els, we have a heap
    {
        assignH+=3;
        aux=A[1];
        A[1]=A[k];
        A[k]=aux;
        heapSize=heapSize-1; //the last element is not in the heap any longer from now on (heap_size should decrement by 1)
        heapify(A,1);   //apply heapify; repeat until the dim of the heap becomes 1
		/*for(int i=1;i<=10;i++)
			printf("%d ",A[i]);
		printf("\n");*/
    }
}

int partition(long A[],int p, int r,int choosePivot){

	long x,aux;
	int i,j;
	if(choosePivot==1)
		x=A[r];    //pivot for average case
	else if(choosePivot==2)
		x=A[(p+r)/2];   //pivot for best case(median)
	else if(choosePivot==3)
		x=A[p+1]; //pivot for the worst case: partitions of the form 1, n-1
	assignQ++;
	i=p-1;
	for(j=p;j<=r-1;j++){
		compQ++;
		if(A[j]<=x)
			{i++;
			assignQ+=3;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
		}
	}
	assignQ+=3;
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;

	return i+1;
}

void quickSort(long A[],int p, int r,int choosePivot)
{	if ( p < r )
    {
        int q = partition(A, p, r,choosePivot); 
			quickSort(A, p, q-1,choosePivot);
			quickSort(A, q+1, r,choosePivot);
    }
}

int main()
{
	srand(time(NULL));
    long heap[10001];
    long quick[10001];
    int j,k,nr;
    long compHeap=0,compQuick=0,assignHeap=0,assignQuick=0;
    FILE *f,*av,*b,*w;
    f=fopen("average.txt","w");
	w=fopen("worst_quicksort.txt","w");
	av=fopen("average_quicksort.txt","w");
	b=fopen("best_quicksort.txt","w");
	
	long a[11];
	a[0]=0;
	a[1]=1;
	a[2]=4;
	a[3]=11;
	a[4]=12;
	a[5]=25;
	a[6]=26;
	a[7]=27;
	a[8]=28;
	a[9]=29;
	a[10]=30;
	
	heapSort(a,10);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n");
	assignQ=compQ=0;
	quickSort(a,1,10,1);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	//printf("\n%d %d ",assignQ,compQ);


	//average case for quicksort and heap sort
    for (int i=100;i<=10000;i=i+100)
	{
		compH=compQ=assignH=assignQ=0;

		for (j=1;j<=5;j++)
		{
			nr=0;
			compHeap=compQuick=assignHeap=assignQuick=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				heap[nr]=quick[nr]=rand();
			}

			heapSort(heap,i);
			quickSort(quick,1,i,1);

			compHeap+=compH;
			compQuick+=compQ;
			assignHeap+=assignH;
			assignQuick+=assignQ;
		}
			fprintf(av,"%d %ld %ld %ld \n",i,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5);
			fprintf(f,"%d %ld %ld %ld %ld %ld %ld\n",i,compHeap/5,assignHeap/5,compHeap/5+assignHeap/5,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5);
	}

	//best case for quicksort
	for (int i=100;i<=10000;i=i+100)
	{
		compQ=assignQ=0;
		nr=1;
		for (j=1;j<=i;j++) //sorted 
		{
	
			quick[nr]=j;
			nr++;
		}
		quickSort(quick,1,i,2);
		fprintf(b,"%d %ld %ld %ld \n",i,compQ,assignQ,compQ+assignQ);
		
	}

	
	//worst case
	for (int i=100;i<=10000;i=i+100)
	{
		compQ=assignQ=0;
		nr=1;
		for (j=1;j<=i;j++) //sorted 
		{
			quick[nr]=j;
			nr++;
		}
		quickSort(quick,1,i,3);
		fprintf(w,"%d %ld %ld %ld \n",i,compQ,assignQ,compQ+assignQ);
		
	}
	
	
	fclose(f);
	fclose(av);
	fclose(b);
	fclose(w);
	//getch();
	return 0;

	
}
