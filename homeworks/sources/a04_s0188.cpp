/*
I.    - k = constant; 
	k1= 10
	k2=100
	k3=500
		- n = 500..10000 (increment de 500) 
II. - n = constant; - k = 10..500 (increment de 50)


*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>


#define PAS_TEST 500
#define NR_TEST 20

#define PAS_TEST_B 50
#define NR_TEST_B 10
const int dim=600000;  //reprezinta dimensiunea maxima heap-ului

int nrop;

typedef struct element_heap{
	                       int val,index;
                           }EHEAP;

EHEAP h[dim];
int n=0; //lungimea heapului

const int corect=1;
const int error=0;
typedef struct tip_nod{
                       int inf;
                       struct tip_nod *urm,*prec;
                       }TIP_NOD;

TIP_NOD *cap[1000];
TIP_NOD *li=0,*li_last=0;  //lista in care se pun elementele finale li=referinta la primul nod  li_last=referinta la ultimul elem
int k;

#define NMAXX 20000 
//vreau numere generate aleator intre 0 si NMAXX

//operatii pe heap
int parinte(int i)
{
	return i/2;
}

int stanga(int i)
{
	return 2*i;
}

int dreapta(int i)
{
	return 2*i+1;
}


void reconstituie_heap(EHEAP a[],int i,int n)
{
	int l,r,maxim;
	l=stanga(i);
	r=dreapta(i);
	nrop+=5;//3 atribuiri si 2 comparatii;
	if ((l<=n)&&(a[l].val>a[i].val)) 
		maxim=l;
	else
		maxim=i;
	nrop+=2;
	if ((r<=n)&&(a[r].val>a[maxim].val)){
		maxim=r;++nrop;
	}
	++nrop;//comparatie
	if (maxim!=i)
	{
		EHEAP aux;
		aux=a[i];
		a[i]=a[maxim];
		a[maxim]=aux;
		nrop+=3;
		reconstituie_heap(a,maxim,n);
	}
}


void insertHeap(int dim,int x,int index) //dim e dimensiunea heapului ; x e elementul care se insereaza in heap
{
	dim++;
	int	i=dim;
	h[dim].val=x;
	h[dim].index=index;

	i=dim;
	nrop+=7;//5 atrib, 2 comp

	while((i>1)&&(h[parinte(i)].val<h[i].val))
	{
		EHEAP aux;
		aux=h[i];
		h[i]=h[parinte(i)];
		h[parinte(i)]=aux;
		i=parinte(i);
		nrop+=6;
	}
}
EHEAP extractMaxHeap(int dim)
{
  if (dim<1) printf("depasire inferioara heap\n");
  int max=h[1].val;
  int imax=h[1].index;
h[1]=h[dim];
dim--;
nrop+=5;
reconstituie_heap(h,1,dim);
	EHEAP eh;
	eh.val=max;
	eh.index=imax;
	nrop+=2;
return eh;
}

//inserare in heap
void hPush(int val,int i){
	insertHeap(n,val,i);
	++n;
	++nrop;
}
//scoatere din heap a elementului din radacina
EHEAP hPop(){
	EHEAP eh=extractMaxHeap(n);
	--n;
	nrop+=2;
	return eh;
}


//-----------------------------------------------------------------------
//operatiile cu liste


int citire_date_in_nod(TIP_NOD *p,int inf)
{
    //printf("p->inf=");scanf("%d",&p->inf);
	p->inf=inf;
	++nrop;
    return corect;
}
   
TIP_NOD *creare_nod(int inf)
{

TIP_NOD *p;
p=(TIP_NOD *)malloc(sizeof(TIP_NOD));
nrop+=2;
if (p==0)
{
	++nrop;
	printf("\neroare malloc:nu s a alocat memorie in heap!");
    return 0;
}
++nrop;
if (citire_date_in_nod(p,inf)==corect)
        return p;
        else return 0;
}


void elib_nod(TIP_NOD **p){
     free(*p);
	 ++nrop;
}


void creare_lista_vida(TIP_NOD **prim)
{
*prim=0;
++nrop;
}


void listare_inainte(FILE *f,TIP_NOD *prim)

{
	TIP_NOD *p;
     if (prim==0) fprintf(f,"lista este vida!!\n");
     else
     {
		 p=prim;
         while(p!=0)
                {
                fprintf(f,"%d\n",p->inf);
                p=p->urm;
                }
      }
}

                        
          
void inserare_inaintea_primului_nod(TIP_NOD **prim,TIP_NOD *p)
{
   
   p->prec=0;
   nrop+=2;
   if(*prim==0)    //lista e vida
   {
	   *prim=p;
       p->urm=0;
	   nrop+=2;
   }  
   else {           //lista nu e vida
	   p->urm=*prim;
	   (*prim)->prec=p;
	   *prim=p;
	   nrop+=3;
        }
}

void inserare_dupa_ultimul_nod(TIP_NOD **ultim,TIP_NOD *p)
{
    p->urm=0;
	p->prec=*ultim;
	(*ultim)->urm=p;
	*ultim=p;
	nrop+=4;
}



void sterge_primul_nod(TIP_NOD**prim)
{

TIP_NOD*p;
if(*prim==0){
	        printf("\nlista e vida!");nrop++;
            return;
            }
p=*prim;
*prim=(*prim)->urm;
elib_nod(&p);
nrop+=5;
if(*prim!=0){
	(*prim)->prec=0;
	++nrop;
}
} 

                                    


void stergere_lista(TIP_NOD**prim)
{

 TIP_NOD*p;
 while(*prim!=0)
   { p=*prim;
   *prim=(*prim)->urm;
   elib_nod(&p);
   nrop+=4;
   }
}


//------------------------------------
int readL(TIP_NOD **prim){    //citire  prim element din lista si stergere
	TIP_NOD *p=*prim;
	int val=p->inf;
	sterge_primul_nod(prim);
	nrop+=2;
	return val;
}

void writeL(TIP_NOD **prim,int info){ //creare nod si inserare element in lista 
	TIP_NOD *p=creare_nod(info);
	inserare_inaintea_primului_nod(prim,p);
	++nrop;
}

void writeLi(int info){          //creare nod si scriere informatii in lista finala                 
	TIP_NOD *p=creare_nod(info);
	p->urm=0;
	p->prec=0;
	nrop+=4;
	if(li!=0){                  //lista nu e vida
		inserare_dupa_ultimul_nod(&li_last,p);
	}else{                        //primul nod in lista finala
		li=p;
		li_last=p;
		nrop+=2;
	}
}

int notEmpty(TIP_NOD *p){
	++nrop;
	return (p!=0);
}

//------------------------


void interclasare(){
	int i,val;
	EHEAP eh;
	for(i=1;i<=k;++i){
		val=readL(&cap[i]);
		hPush(val,i);
		nrop+=3;
	}
	while(n>0){
		nrop+=2;
		eh=hPop();
		writeLi(eh.val);
		if(notEmpty(cap[eh.index])){
			val=readL(&cap[eh.index]);
			++nrop;
			hPush(val,eh.index);
		}
	}
}


void create_test(int k,int n){
	//k indica cate liste trebuie generate cu dimensiunea n
	//n-numarul de elemente sau dimensiunea listei
	//nr-bag nr elemnte in fiecare lista din cele n numere generate
	int nr=n/k;
	int i,j,val;
	for(i=1;i<=k;++i){
		//lista i;
		creare_lista_vida(&cap[i]);
		val=rand()%5+1;
		for(j=1;j<=nr;++j){
			writeL(&cap[i],val);
			val+=rand()%5;
		}
	}
	stergere_lista(&li);
	creare_lista_vida(&li);
}





int main (void)
{  
    /*initializeaza generatorul de nr aleatoare*/
	srand(time(NULL));
	
	// TESTE a, k={10,100,500}, n=500..10000
	int lung[]={10,100,500};
	int tot;
	int tot_op;
	FILE *f=fopen("dataout.txt","w");
	for(int i=0;i<3;++i){
		k=lung[i];
		tot=0; //indica dimensiunea listei care se genereaza
		for(int nr_test=1;nr_test<=NR_TEST;++nr_test){
			tot+=PAS_TEST;
			create_test(k,tot);
	
			tot_op=0;
			for(int j=0;j<5;++j){
				create_test(k,tot);
				nrop=0;
				interclasare();
				tot_op+=nrop/5;
			}
		fprintf(f,"%d %d %d\n",k,tot,tot_op);
		}
	}
	fclose(f);

	/*
	//TESTE b, n=10000 k=10..500
	int tot_op;
	FILE *f=fopen("dataout.csv","w");
	int tot=10000;//indica dimensiunea listei care se genereaza
	k=-40;
	for(int i=0;i<NR_TEST_B;++i){
		k+=PAS_TEST_B;
		printf("%d\n",k);
		tot_op=0;
		for(int j=0;j<5;++j){
			create_test(k,tot);
		nrop=0;
		interclasare();
		tot_op+=nrop/5;
		}
		fprintf(f,"%d  %d\n",k,tot_op);
	}
	fclose(f);
	*/

	return 0;
}
