#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
/*

*/
#define MAX_SIZE 10000

long long unsigned cmp_inter=0,cmp_sel=0,cmp_inser=0,atr_inter=0,atr_sel=0,atr_inser=0,sum_inter=0,sum_sel=0,sum_inser=0;

void interschimbare(int a[],int n)
{
	int i=1,j,aux,sortat=0;
	do
	{	sortat=1;
			for(j=0;j<n-i;j++)
			{	cmp_inter++;//
				if(a[j]>a[j+1])
					{
						atr_inter+=3;//
						aux=a[j];
						a[j]=a[j+1];
						a[j+1]=aux;
						sortat=0;
					}
				
			}
			i++;
	 }
		while(!sortat);
	sum_inter=cmp_inter+atr_inter;
}
void selectie(int a[],int n)
{int i,j,aux;
	for(i=0;i<n-1;i++)
	{	atr_sel++;//
		int min = a[i];
		int imin = i;
		
		for(j=i;j<n;j++)
		{
			cmp_sel++;//
			if(a[j]<min)
			{
				atr_sel++;//
				min=a[j];
				imin=j;
			}
		}
	atr_sel+=3;//
	
	aux=a[i];
	a[i]=a[imin];
	a[imin]=aux;
	}

sum_sel=cmp_sel+atr_sel;
}
void insertie(int a[],int n)
{
	int i,j,x,k;
	for(i=1;i<n;i++)
	{	atr_inser++;//

		x=a[i];
		j=0;
		cmp_inser++;//
		while(a[j]<x)
		{	cmp_inser++;//
			j++;
		}
		for(k=i;k>=j+1;k--)
		{	atr_inser++;//
			a[k]=a[k-1];
		}
		atr_inser++;//
		a[j]=x;

	}
	sum_inser=cmp_inser+atr_inser;
}

int main() {

	int in[MAX_SIZE];
	int ins[MAX_SIZE];
	int sel[MAX_SIZE];

	FILE *fisier1;//cazul favorabil
	FILE *fisier2;//cazul mediu
	FILE *fisier3;//cazul defavorabil

	//cazul favorabil
	printf("Cazul favorabil\n");

	fisier1 = fopen("favorabil.csv", "w");
	fprintf(fisier1, "n,atr_inter,atr_sel,atr_insertie,cmp_inter,cmp_sel,cmp_insertie,suma_inter,suma_sel,suma_insertie\n");


	for(int n=100; n<=MAX_SIZE; n += 100)
	{
		in[0]=rand();
		ins[0]=in[0];
		sel[0]=in[0];
		
		for (int i=1; i<=n; i++)
		{
			int aux = in[i-1]+rand();
			in[i]=aux;
			ins[i]=aux;
			sel[i]=aux;
		}
		interschimbare(in,n);
		insertie(ins,n);
		selectie(sel,n);
		fprintf(fisier1,"%d,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n",n,atr_inter,atr_sel,atr_inser,cmp_inter,cmp_sel,cmp_inser,sum_inter,sum_sel,sum_inser);
	
	cmp_inter=0;
	cmp_sel=0;
	cmp_inser=0;
 
	atr_inter=0;
	atr_sel=0;
	atr_inser=0;
	
	sum_inter=0;
	sum_sel=0;
	sum_inser=0;
	
	}
	fclose(fisier1);
	
	
	fisier2 = fopen("mediu.csv", "w");
	fprintf(fisier2,"n,atr_inter,atr_sel,atr_insertie,cmp_inter,cmp_sel,cmp_insertie,suma_inter,suma_sel,suma_insertie\n");
	
	//cazul mediu
	printf("\nCazul mediu\n");

	for(int n=100; n<=MAX_SIZE; n += 100)
	{	printf("\n%d / 10 000",n);
		for (int j=1;j<=5; j++)
		{
			for (int i=0; i<=n; i++)
			{	
				int aux = rand();
				in[i]=aux;
				ins[i]=aux;
				sel[i]=aux;
			}

			interschimbare(in,n);
			insertie(ins,n);
			selectie(sel,n);
		}
		fprintf(fisier2, "%d,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n",n,atr_inter/5,atr_sel/5,atr_inser/5,cmp_inter/5,cmp_sel/5,cmp_inser/5,sum_inter/5,sum_sel/5,sum_inser/5);

    cmp_inter=0;
	cmp_sel=0;
	cmp_inser=0;
 
	atr_inter=0;
	atr_sel=0;
	atr_inser=0;

	sum_inter=0;
	sum_sel=0;
	sum_inser=0;
	}

	fclose(fisier2);

	printf("\nCazul defavorabil\n");

	fisier3 = fopen("defavorabil.csv", "w");
	fprintf(fisier3, "n,atr_inter,atr_sel,atr_insertie,cmp_inter,cmp_sel,cmp_insertie,suma_inter,suma_sel,suma_insertie\n");

	for(int n=100; n<=MAX_SIZE; n += 100)
	{	
		in[n]=rand();
		ins[n]=in[n];
		sel[n]=in[n];
		for (int i=n-1; i>=0; i--)
		{	
			int aux = in[i+1]+rand();
			in[i]=aux;
			ins[i]=aux;
			sel[i]=aux;
		}

		interschimbare(in,n);
		insertie(ins,n);
		selectie(sel,n);
		
		fprintf(fisier3,"%d,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n",n,atr_inter,atr_sel,atr_inser,cmp_inter,cmp_sel,cmp_inser,sum_inter,sum_sel,sum_inser);
    
    cmp_inter=0;
	cmp_sel=0;
	cmp_inser=0;
 
	atr_inter=0;
	atr_sel=0;
	atr_inser=0;
	
	sum_inter=0;
	sum_sel=0;
	sum_inser=0;
	}
	
	
	fclose(fisier3);

	
	
	getch();
	return 0;

}
