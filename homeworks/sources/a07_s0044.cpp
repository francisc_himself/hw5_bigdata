#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct _MULTI_NODE{
	int key;
	int count;
	struct _MULTI_NODE *child[50];
}MULTI_NODE;

typedef struct BIN_TRE{
	int info;
	BIN_TRE* left;  //copil
	BIN_TRE* right; //frate
	}BIN_TRE;

MULTI_NODE *create_MULTI_NODE (int key){
	MULTI_NODE *p;
	p = (MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	p->key = key;
	p->count = 0;
	return p;
}

void insert_MULTI_NODE (MULTI_NODE *parent, MULTI_NODE *child){
	parent->child[parent->count++] = child;
}

MULTI_NODE *t1 (int t[], int size){
	MULTI_NODE *root = NULL;
	MULTI_NODE *nodes[50];
	for (int i = 0; i < size; i++)
	{
		nodes[i] = create_MULTI_NODE(i);
	}
	for (int i = 0; i < size; i++)
	{
		if (t[i] == -1)
		{
			root = nodes [i];
		}
		else
		{
			insert_MULTI_NODE (nodes[t[i]], nodes[i]);
		}
		
	}
	return root;
}

void prettyprint (MULTI_NODE *nod, int nivel = 0){
	for (int i = 0; i < nivel; i++)
		printf ("    ");
	printf ("%d \n", nod->key);
	for (int i = 0; i < nod->count; i++)
		prettyprint (nod->child[i], nivel + 1);
}

BIN_TRE* Create_Bin_Tree (MULTI_NODE *p, MULTI_NODE *parent, int i){
	BIN_TRE *nou = new BIN_TRE;
	nou->info = p->key;
	nou->left = NULL;
	nou->right = NULL;
	if ( p->count == 0)
		nou->left = NULL;
	else
		nou->left = Create_Bin_Tree (p->child[0], p, 0);
	if ( parent != NULL)
		if ( i + 1 < parent->count)
			nou->right = Create_Bin_Tree (parent->child[i+1], parent, i+1);
		else
			nou->right = NULL;
	else
		nou->right = NULL;
	return nou;
}

void Pretty_print_bin (BIN_TRE *p, int i){
	if ( p != NULL )
	{
		Pretty_print_bin (p ->right, i+1);
		for ( int j = 0; j < i; j++)
			printf("\t");
		printf("%d", p->info);
		printf("\n");
		Pretty_print_bin (p ->left, i + 1);
		
	}
}
			


int main(){
	int t[] = {3,3,3,-1,2,2,2,2,5,1,1};
	int size = sizeof(t)/sizeof(t[0]);
	BIN_TRE *root1;
	MULTI_NODE *root = t1(t, size);
	prettyprint (root);

	printf("Binary Tree\n");
	int b = 0;
	root1 = Create_Bin_Tree (root, NULL, 0);
	Pretty_print_bin (root1, 0);

	return 0;
}