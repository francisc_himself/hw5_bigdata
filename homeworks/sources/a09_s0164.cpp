/*
Student:					***ANONIM*** ***ANONIM***
Grupa:						***GROUP_NUMBER***
Cerinta:	
Implementati corect si eficient BFS
- reprezentaria grafurilor - liste de adiacenta
- Section 22.2 din Cormen

Evaluare:
- hardcore program pentru testare
- variati numarul de muchii intre 1000 si 5000 cu increment de 100
si setati numarul de varfuri la 100, aplicati bfs
- variati numarul de varfuri intre 100 si 100 cu increment de 10, 
si setati numarul de muchii la 900

graficele pentru cele 2 cerinte se gasesc in cerinta1.xlsx si cerinta2.xlsx

Complexitate:
O(|E|+|V|)


*/
#include<iostream>
#include<conio.h>

#define ALB  1
#define GRI 2
#define NEGRU 3
#define INFINIT 9999



typedef struct muchie
{
	int varf1;
	int varf2;
}MUCHIE;
typedef struct nod_graf
{
	int cheie;
	struct nod_graf *parinte;
	int culoare;
	int distanta;
}NOD_GRAF;
typedef NOD_GRAF *VARF;
typedef struct graf
{
	int nrVarfuri;
	int nrMuchii;
	VARF *varfuri;
	MUCHIE *muchii;
	int  marime[100];
	int adiacente[100][100];
}GRAF;

//declaratii de variabile
GRAF *graf;
int coada[100],prim,ultim;
int operatii;

void puneInCoada(int *coada,int *prim,int *ultim,int cheie_nod)
{
	coada[*ultim] = cheie_nod;
	
	if(*ultim == 100)
		{
			(*ultim) = 0;
		}
	else
		{
			(*ultim)++;
		}
	
}


int scoateDinCoada(int *coada,int *prim,int *ultim)
{
	int cheie_nod = coada[*prim];
	if(*prim == *ultim)
		{
			return INFINIT;
		}
	if(*prim == 100)
		{
			*prim = 0;
		}
	else
		{
			(*prim)++;
		}
	return cheie_nod;
}

void afiseazaCoada(int *coada)
{
	printf("\n");
	for(int i=0;i<=7;i++)
	{
		printf("%d ",coada[i]);
		
	}
	printf("\n");
}


GRAF* creareG(int nrVarfuri,int nrMuchii)
{
	graf = (GRAF*)malloc(sizeof(GRAF));
	graf-> nrVarfuri = nrVarfuri;
	graf-> nrMuchii = nrMuchii;
	graf-> varfuri = (VARF *) malloc(nrVarfuri*sizeof(VARF));
	graf-> muchii = (MUCHIE *)malloc(nrMuchii*sizeof(MUCHIE));
	
	for(int i =0 ;i<nrVarfuri;i++)
	{
		graf->varfuri[i] = (VARF)malloc(sizeof(VARF));
		graf->varfuri[i]->cheie = i;
		graf->varfuri[i]->culoare = 0;
		graf->varfuri[i]->distanta = INFINIT;
		graf->varfuri[i]->parinte = NULL;
		graf->marime[i]=0;
	}
	return graf;
}


void inserareGraf( int nrMuchii, MUCHIE *muchii)
{
for(int i = 0;i< nrMuchii;i++)
	{
			graf-> muchii[i] = muchii[i]; 
			graf->marime[graf-> muchii[i].varf1]++;
			graf->adiacente[graf-> muchii[i].varf1][graf->marime[graf-> muchii[i].varf1]]=graf-> muchii[i].varf2;
			graf->marime[graf-> muchii[i].varf2]++;
			graf->adiacente[graf-> muchii[i].varf2][graf->marime[graf-> muchii[i].varf2]]=graf-> muchii[i].varf1;
	}
}




void BFS(GRAF* graf,int s)
{
	for(int i =0;i<graf->nrVarfuri;i++)
	{
			operatii+=3;
			graf->varfuri[i]->culoare = ALB;
			graf->varfuri[i]->parinte = NULL;
			graf->varfuri[i]->distanta = INFINIT;
		
	}
	operatii+=3;
	graf->varfuri[s]->culoare = GRI;
	graf->varfuri[s]->parinte = NULL;
	graf->varfuri[s]->distanta = 0;
	prim = ultim = 0;

	
	puneInCoada(coada,&prim,&ultim,s);
	printf("\n");
	
	
	//printf("%d",s);
	//printf("\n");
	while(prim!=ultim)
	{
		operatii++;
		int u = scoateDinCoada(coada,&prim,&ultim);
		
		for(int i =1;i<=graf->marime[u];i++)
		{
			operatii++;
			if(graf->varfuri[graf->adiacente[u][i]]->culoare == ALB)
			{
				operatii+=6;
				graf->varfuri[graf->adiacente[u][i]]->culoare = GRI;
				graf->varfuri[graf->adiacente[u][i]]->distanta = graf->varfuri[u]->distanta +1;
				graf->varfuri[graf->adiacente[u][i]]->parinte = graf->varfuri[u];
				
				puneInCoada(coada,&prim,&ultim,graf->adiacente[u][i]);
			
				afiseazaCoada(coada);
			}
		}
		operatii++;
		graf->varfuri[u]->culoare = NEGRU;
		
	}

}



void corect()
{
	GRAF *graf = (GRAF*)malloc(sizeof(GRAF));
	MUCHIE M[] = {{0,1},{1,4},{0,5},{5,2},{2,6},{6,3},{3,7},{2,3},{6,7},{5,6}};

	graf= creareG(8,10);
    inserareGraf( 10, M);
	
	printf("varfurile grafului sunt: \n");
	for(int i=0;i<=7;i++)
		printf("%d ",graf->varfuri[i]->cheie);
	printf("\n\n");
	printf("muchiile grafului sunt: \n");
	for(int i=0;i<=9;i++)
			printf("%d, %d\n",graf->muchii[i].varf1,graf->muchii[i].varf2);

	
	printf("\n\n");
	printf("evolutia cozii: ");
	BFS(graf,0);
	

}

void cerinta1()
{
	MUCHIE muchii[60001];
	int nrVarfuri=100;
	int nrMuchii;
	FILE* f=fopen("cerinta1.csv","w");
	fprintf(f,"nrVarfuri, operatii");
	
	for(nrMuchii = 1000;nrMuchii<5000;nrMuchii+=100)
	{
		operatii=0;
		printf("%d\n",nrMuchii);
		GRAF *graf = (GRAF*)malloc(sizeof(GRAF));
		
		for(int i = 0;i<nrMuchii;i++)
		{
			
				muchii[i].varf1 = rand()%nrVarfuri;
				a:muchii[i].varf2 = rand()%nrVarfuri;
			
				if(muchii[i].varf1==muchii[i].varf2)
					goto a;
			
		}
		operatii=0;
		graf= creareG(nrVarfuri,nrMuchii);
			inserareGraf( nrMuchii, muchii);
		
		BFS(graf,0);
		fprintf(f,"%d,%d\n",nrMuchii, operatii);
	}

	
}

void cerinta2()
{
	MUCHIE muchii[60001];
	int nrVarfuri;
	int nrMuchii=900;
	
	FILE* f=fopen("cerinta2.csv","w");
	fprintf(f,"nrMuchii, operatii");
	for(nrVarfuri = 100;nrVarfuri<=200;nrVarfuri+=10)
	{
		operatii=0;
		GRAF *graf2 = (GRAF*)malloc(sizeof(GRAF));
		printf("%d\n",nrVarfuri);
		
		for(int i = 0;i<nrMuchii;i++)
		{
		
				muchii[i].varf1 = rand()%nrVarfuri;
				a:muchii[i].varf2 = rand()%nrVarfuri;
			if(muchii[i].varf1 ==muchii[i].varf2)
				goto a;
		}
		operatii=0;
			graf2= creareG(900,nrMuchii);
			inserareGraf( nrMuchii, muchii);
		
		BFS(graf,0);
		fprintf(f,"%d,%d\n",nrMuchii, operatii);

	}


}

void main()
{
	corect();
	//cerinta1();
	//cerinta2();
	getch();
}