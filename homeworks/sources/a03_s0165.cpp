/*
***ANONIM*** ***ANONIM*** ***ANONIM***

Tema3: HeapSort VS QuickSort
28 martie 2013


S-a testat functionalitatea algoritmilor pe doi vectori de dimensiunea 10
Se observa ca Quicksort este mai eficient decat HeapSort.
Graficele au iesit liniare



*/
#include<fstream>
#include<iostream>

using namespace std;

ofstream g("date.csv");
ofstream y("quick.csv");

int a[10001], b[10001], n, i, j;
int atrQ, compQ, atrH, compH;

int partitie(int a[10001],int p, int r)
{
	atrQ++;
	//int x = a[p];
	int x = a[(p+r)/2];
	i = p - 1;
	j = r + 1;
	while(1)
	{
		do
		{
			j--;
			compQ++;
		}
		while(a[j]<x);

		do
		{
			i++;
			compQ++;
		}
		while(a[i]>x);

		if(i<j)
		{
			int aux = a[i];
			a[i] = a[j];
			a[j] = aux;
			atrQ = atrQ + 3;
		}
		else
			return j;
	}
}

void quicksort(int a[10001],int p, int r)
{
	
	if(p<r)
	{
		int q = partitie(a,p,r);
		quicksort(a,p,q);
		quicksort(a,q+1,r);
	}
}

////heapsport
void Heapify(int a[10001],int size, int i)
{
	int left = 2*i;
	int right = 2*i + 1;
	int min;
	
	compH++;
	if((left <= size) && (a[left] < a[i]))
	{
		
		min = left;
	}
	else
	{
		min = i;
	}
	compH++;
	if ((right<=size) && (a[right] < a[min]))
	{
		min = right;
	}
	
	if(min != i)
	{
		atrH = atrH + 3;
		int aux = a[i];
		a[i] = a[min];
		a[min] = aux;
		Heapify(a,size,min);
	}
}



void BuildHeapBU(int *a, int size)
{
	for(int i = (size/2) ; i >= 1 ; i--)
	{
		Heapify(a,size,i);
	}
}

void HeapSort(int a[10001], int size)
{
	
	for(int i = size ; i >= 2 ; i--)
	{
		atrH = atrH + 3;
		int aux = a[1];
		a[1] = a[i];
		a[i] = aux;
		Heapify(a,i-1,1);
	}	
}



void main()
{

	/*a[1]=23;
	a[2]=2;
	a[3]=64;
	a[4]=32;
	a[5]=67;
	a[6]=12;
	a[7]=2;
	a[8]=80;
	a[9]=85;
	a[10]=223;
	
	

	quicksort(a,1,10);

	for(i=1;i<=10;i++)
		cout<<a[i]<<" ";
	cout<<endl;

	BuildHeapBU(a,10);
	HeapSort(a,10);
	for(i=1;i<=10;i++)
	cout<<a[i]<<" ";*/

	//Comparam cele doua la mediu
	// Comparam quicksort in best si worst

	g<<"n,"<<"Atribuiri Heap,"<<"Comparatii Heap,"<<"Atribuiri Quicksort,"<<"Comparatii Quicksort,"<<"Suma Heap,"<<"Suma Quicksort"<<endl;
	for(n=100;n<=10000;n=n+100)
	{
		atrH = compH = compQ = atrQ = 0; 
		int l = 5;
		while(l>=1)
		{
			for(i=1;i<=n;i++)
				{
					a[i] = rand()%200;
					b[i] = a[i];
				}
			
			quicksort(a,1,n);
		
			BuildHeapBU(b,n);
			HeapSort(b,n);
			l--;
		}
		g<<n<<","<<atrH/5<<","<<compH/5<<","<<atrQ/5<<","<<compQ/5<<","<<atrH/5+compH/5<<","<<atrQ/5+compQ/5;
		g<<endl;
	}


	// de aici faza cu best si worst de la quick

	y<<"n,"<<"Comparatii best,"<<"Atribuiri best,"<<"Suma best,"<<"Comparatii worst,"<<"Atribuiri worst,"<<"Suma worst";
	y<<endl;

	for(n=100;n<=10000;n=n+100)
	{
		compQ = atrQ = 0; 
		for(i=1;i<=n;i++)
			{
				a[i] = i; //worst
				b[i] = n-i; //best
			}
		quicksort(a,1,n);
		y<<n<<","<<compQ<<","<<atrQ<<","<<compQ+atrQ;
		compQ = atrQ = 0; 
		
		quicksort(b,1,n);
		y<<","<<compQ<<","<<atrQ<<","<<compQ+atrQ<<endl;
		compQ = atrQ = 0; 
	}

}