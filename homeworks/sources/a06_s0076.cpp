#include<stdio.h>
#include<conio.h>
#include<stdlib.h>


#include "Profiler.h" 
Profiler profiler("Josephus");




int n,m,mx;
int joselist[10010],cnt=0;


typedef struct nod
{
    int key;
    int size;
    nod *left, *right, *parent;
}NOD;
NOD* root;

void pretty_print(NOD *p,int level)
{

  if (p!=NULL){

		 pretty_print(p->left,level+1);
		 for( int i=0;i<=level;i++) printf("\t");
		 printf("%d\n", p->key);
		 pretty_print(p->right,level+1);
	       }
}


NOD* OS_SELECT(NOD *x, int i)
{
    int r=1;
    if (x->left != NULL){							//nodul de sters este OS_SELECTed pe ramurile din stanga si dreapta
       profiler.countOperation("Josephus Total", mx, 1);
	   r = x->left->size+1;}
	else 
        r = 1;
    profiler.countOperation("Josephus Total", mx, 2);
	if (r == i){
        profiler.countOperation("Josephus Total", mx, 1);
		return x;}
    else
    {
        profiler.countOperation("Josephus Total", mx, 2);
		if (i < r)
            return OS_SELECT(x->left,i);
        else
            return OS_SELECT(x->right,i-r);
    }
}

NOD* GENERATE(int x, int dim, NOD *lft, NOD *rght)
{
    NOD *q;
    q= new NOD;
    q->key=x;					//q este noul nod si ia  valoarea lui x
    q->left=lft;				//copilul stang este creat
    q->right=rght;				//copilul drept este creat
    q->size=dim;				// adaugam dimensiunea
    profiler.countOperation("Josephus Total", mx, 6);
	if (lft!=NULL)  {  
        lft->parent = q;
		profiler.countOperation("Josephus Total", mx, 1);		
	}
	if (rght!=NULL){
		profiler.countOperation("Josephus Total", mx, 1);
        rght->parent=q;
	}
    q->parent=NULL;				//q este radacina
	
    return q;
}

NOD* BUILD_T(int lft, int rght)
{
    NOD *ds,*d;
	profiler.countOperation("Josephus Total", mx, 1);
	if (lft > rght)
        return NULL;
    profiler.countOperation("Josephus Total", mx, 1);
	if (lft == rght)
		return GENERATE(lft, 1, NULL, NULL);
	 else
    {
        int mid =(lft + rght)/2;
        ds = BUILD_T(lft, mid-1);
        d  = BUILD_T(mid+1, rght);
		profiler.countOperation("Josephus Total", mx, 3);
        return GENERATE(mid, rght-lft+1,ds, d);
    }
}


NOD* MINIMUM(NOD* x)
{
    while(x->left!=NULL){
        profiler.countOperation("Josephus Total", mx, 1);
		x = x->left;
	}
    profiler.countOperation("Josephus Total", mx, 1);
	return x;						//ne intoarcem la nodul din stanga
}

NOD* SUCCESOR(NOD* x)
{
    NOD *y;
    profiler.countOperation("Josephus Total", mx, 1);
	if(x->right!=NULL)						
        return MINIMUM(x->right);			
    profiler.countOperation("Josephus Total", mx, 2);
	y = x->parent;							
    while (y != NULL && x == y->right)		
    {        								
		profiler.countOperation("Josephus Total", mx, 2);
		x = y;								
        y = y->parent;
    }
    return y;								
}

NOD* DELETEN(NOD* z)
{
    NOD *y;
    NOD *x;
	profiler.countOperation("Josephus Total", mx, 1);
    if ((z->left == NULL) || z->right == NULL){		
        y=z;
		profiler.countOperation("Josephus Total", mx, 1);
	}
    else{
		profiler.countOperation("Josephus Total", mx, 1);
        y = SUCCESOR(z);							
	 }
	profiler.countOperation("Josephus Total", mx, 1); 
	if (y->left != NULL){
		 profiler.countOperation("Josephus Total", mx, 1);
         x = y->left;
	 }
    else{
		profiler.countOperation("Josephus Total", mx, 1);
		x = y->right;
	}
    profiler.countOperation("Josephus Total", mx, 1);
	if (x != NULL){
		profiler.countOperation("Josephus Total", mx, 1);
        x->parent = y->parent;
	}
	profiler.countOperation("Josephus Total", mx, 1);
    if (y->parent == NULL){
		profiler.countOperation("Josephus Total", mx, 1);
		root = x;
	}
	else if (y == y->parent->left)
	{
		profiler.countOperation("Josephus Total", mx, 2);
		y->parent->left = x;
	}
    else{
		profiler.countOperation("Josephus Total", mx, 2);
        y->parent->right = x;
	}
	profiler.countOperation("Josephus Total", mx, 1);
	if (y != z)
	{
		profiler.countOperation("Josephus Total", mx, 1);
		z->key = y->key;
	}
    return y;
}

void new_size(NOD *p)
{
    while (p != NULL)
    {
	    profiler.countOperation("Josephus Total", mx, 3);
		p->size--;			//pt fiecare nod eliminat vom decreste dimensiunea cu 1      
		p=p->parent;
	}
	profiler.countOperation("Josephus Total", mx, 1);
}

void display(int joselist[],int cnt){ //afisam nodurile scoase 
    printf("\nDeleted numbers: \n");
    for ( int i = 0 ; i < cnt ;i++)		
        printf("%d ", joselist[i]);
}

void JOSEPHUS(int n, int m)
{
    NOD *y,*z;
    int aux = n+1;
    int mid = m;
	profiler.countOperation("Josephus Total", mx, 3);
    root = BUILD_T(1, n);		//construim arborele
    //printf("\narborele este: \n");
    //pretty_print(root,0);
    for (int i = 1; i < aux; i++)	                                    
    {
		
		y = OS_SELECT(root, m);	    //selectam nodul pt stergere
        profiler.countOperation("Josephus Total", mx, 1);
		//joselist[cnt] = y->key;
       // cnt++;
        //display(joselist,cnt);
        profiler.countOperation("Josephus Total", mx, 1);
		z = DELETEN(y);			//stergem nodul
      // printf("\nThe tree is: \n");
      // pretty_print(root,0);
        new_size(z);			//vom redimensiona toate incepand cu cel sters parinte
                                    //cel eliminat
        delete(z);					//curatam memoria
        profiler.countOperation("Josephus Total", mx, 2);
		n--;						//decrementam lungimea arborelui
        if (n > 0)	{				//generam urmatoarea pozitie pt stergere
            profiler.countOperation("Josephus Total", mx, 1);
			m = (m - 1 + mid) % n;
		}
        if (m == 0){
			profiler.countOperation("Josephus Total", mx, 1);
            m = n;
		}
	}

}



int main()
{
    //JOSEPHUS(7,3);
	for(mx = 100; mx <= 10000; mx = mx + 100){
		JOSEPHUS(mx, mx/2);
	    printf("Done for %d\n", mx);
	}
	printf("Done all!");
	profiler.showReport();
	getch();
	

}
