
#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>


typedef struct muchie
{
	int x, y; 
}
MUCHIE;

MUCHIE edge[60001];

typedef struct nod
{
	int key, rank;
	struct nod* parent;
} NOD;

NOD *V[10001];   //folosit la determinarea componentelor conexe

int op;

void init()
{
	int i;
	for(i=1;i<=60001;i++)
		edge[i]=*(MUCHIE*)malloc(sizeof(MUCHIE));
}

bool verif(int x, int y, int nrm)  //daca nu mai exista muchia generata o data
{
	int i;
	for(i=1; i<nrm; i++)
	{
		if(((edge[i].x==x) && (edge[i].y==y)) || ((edge[i].y==x) && (edge[i].x==y)))
		{
			return false;
		}
	}
	return true;
}

void generate(int nrv, int nrm)  //generam nr de muchii intre nr varfuri
{
	int i,x,y;
	int nre=1;   //numar curent de muchii generate
	while(nre<=nrm)
		for(i = 1; i<=nrv; i++)
		{
			x = (rand()%(nrv))+1;
			y = (rand()%(nrv))+1;
			if((x != y) && (verif(x,y,nrm)))
			{
				edge[nre].x = x;
				edge[nre].y = y;
				nre++;
			}
		}
}

NOD *make_set(int x)  //initializeaza seturile
{
	NOD *p;
	p=(NOD*)malloc(sizeof(NOD));
	p->key=x;
	p->parent=p;
	p->rank=0;
	op+=3;
	return p;
}

NOD *find_set(NOD *p)
{
	op++;
	if(p!=p->parent)
	{
		p->parent=find_set(p->parent);
		op++;//
	
	}
	return p->parent;
}

void link(NOD *x, NOD *y)  //leaga doua componente conexe cu legatura
{
	op++;//
	if(x->rank > y->rank)
	{
		y->parent=x;
		op++;
		
	}
	else
	{
		x->parent=y;
		op++;//
		
		op++;
		if(x->rank == y->rank)
		{
			op++;//
			y->rank=y->rank+1;
		}
	}
	
}

void my_union(NOD *x, NOD *y)
{
	link(find_set(x),find_set(y));
}

void componente_conexe(int nrv, int nrm)
{
	int i;
	for(i=1; i<=nrv; i++)
	{
		op++;
		V[i]=make_set(i);
		//printf("key: %d, parent: %d, rank: %d\n",V[i]->key,V[i]->parent->key,V[i]->rank);
	}
	for(i=1;i<=nrm;i++)
	{
		op++;
		if(find_set(V[edge[i].x])!=find_set(V[edge[i].y]))
		{
			my_union(V[edge[i].x], V[edge[i].y]);
			//printf("key: %d, parent: %d, rank: %d\n",V[edge[i].x]->key,V[edge[i].x]->parent->key,V[edge[i].x]->rank);
			//printf("key: %d, parent: %d, rank: %d\n",V[edge[i].y]->key,V[edge[i].y]->parent->key,V[edge[i].y]->rank);
		}
	}
}

void afisare_muchii (int nrm)
{
	int i;
	for(i=1;i<=nrm; i++)
	{
		printf("[%d, %d]\n", edge[i].x, edge[i].y);
	}
}

void afisare_conexa(int nrm, int nrv)
{
	int i,j;
	for(i=1;i<=nrv;i++)
		printf("nodul %d are parintele %d\n",V[i]->key,V[i]->parent->key,V[i]->rank);

	for(i=1;i<=nrv;i++)
	{
		printf("Multimea %d: ",i);
		for(j=1;j<=nrv;j++)
		{
			if(find_set(V[j])->key==i)
				printf("%d ",V[j]->key);
			
		}
		printf("\n");
	}
}

void main()
{
	int nrv=6;
	int nrm=3;
	srand(time(NULL));
	generate (nrv, nrm);
	afisare_muchii(nrm);
	componente_conexe(nrv,nrm);
	afisare_conexa(nrm,nrv);


	/*FILE *f;
	f=fopen("rez.txt","w");
	nrv=10000;
	fprintf(f,"muchii op\n");
	for(nrm=10000; nrm<=60000; nrm+=1000)
	{
		op=0;
		init();
		generate(nrv,nrm);
		componente_conexe(nrv,nrm);
		fprintf(f,"%d %d\n",nrm,op);
	}
	fclose(f);
	printf("The End!%c",7);*/
	getch();
}