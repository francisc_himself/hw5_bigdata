/*
***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Am avut de implementat o metoda eficienta de interclasare a k liste inlantuite, fiecare cu cate n elemente
Pentru aceasta, am avut nevoie de un HEAP

Intai am avut de variat dimensiunea listelor intre 100 si 10000 , si de facut interclasare
a 5, 10 si apoi a 100 de liste, urmarid numarul de operatii efectuate

Apoi, am avut de fixat n=10000 si de variat numarul listelor intre 10 si 500,
facand apoi din nou interclasare

Se observa ca acest algoritm are complexitatea O(n*logn),
lucru dedus din numarul de operatii efectuate si graficele create

In al doilea caz, putem observa ca numarul de operatii creste aproape liniar
cu crestenea numarului de liste


*/

#include <stdio.h>
#include <conio.h>
#include  <stdlib.h>
#include <time.h>
#include<math.h>


typedef struct NOD      //structura ce reprezinta un nod din lista
{
    int key;
    struct NOD *next;

}NOD;


typedef struct element      //structura ce reprezinta un element al heap-ului,
{                           //contine o valoare numerica(cheia elementului extras) si numarul listei din care a fost extras elementul
    int value;
    int nr_lista;
}ELEM;


NOD *head[1000];    //siruri de head si tail pentru liste
NOD *tail[1000];
NOD *List_head;     //head si tail pentru lista finala
NOD *List_tail;
int atribuiri, comparatii;

//===============================================================


int calculare_index(ELEM* a,int i,int *nr_elem) //functie ajutatoare la reconstituirea heap-ului
{
	int min;
    if(2*i>*nr_elem)
        return i;


	if (a[i].value < a[2*i].value )
		min=i;
	else
		min=2*i;

    if (2*i+1>*nr_elem)
		return min;

	if (a[min].value >= a[2*i+1].value)
		min=2*i+1;

    return min;
}


void Reconst_Heap(ELEM *A, int *n, int i)       //reconstituirea heap-ului
{
    int index=0;
	int x,y;
    ELEM aux;

    x=2*i;
    y=2*i+1;
    index=calculare_index(A,i,n);
    comparatii+=2;


    if(index!=i)
    {
        aux=A[i];
        A[i]=A[index];
        A[index]=aux;
        atribuiri+=3;
    Reconst_Heap(A,n,index);
    }
}

ELEM H_Pop(ELEM *A,int *n)      //stergere elementului din varful heap-ului
{
    ELEM x;
    x=A[1];
    A[1]=A[*n];
    //atribuiri++;
    (*n)=(*n)-1;
    Reconst_Heap(A,n,1);
    return x;
}

void H_Push(ELEM *A, int *n, ELEM x)    //adaugare element in heap
{
    (*n)=(*n)+1;
    A[*n]=x;
    //atribuiri++;
	ELEM z;

	int i=(*n);

    while(i>1 && A[i].value<A[i/2].value)// && ++comparatii)
    {
        z=A[i];
        A[i]=A[i/2];
        A[i/2]=z;
        i=i/2;
        //atribuiri+=3;
    }
}




void generator_aleator(int* v, int n)       //generator de numere aleatoare
{

    int i;

    v[0] = rand() %50;

    for (i=1;i<n;i++)
    {

        *(v+i) = *(v+i-1) + rand() % 100 + 1;
    }

}




void afisare(NOD *h)    //functie de afisare unei liste pornind de la head
{
    NOD *N=h;
    while(N!= NULL )
    {
        printf("%d ",N->key);
        N=N->next;
    }
    printf("\n");
}


void inserare(NOD **h, NOD**t,int value)    //inserarea unui nou nod in liste, de cheie "value"
{
    NOD* N = new NOD;
    N->key = value;
    N->next = NULL;

    if(*t == NULL)
    {
        *t =N;
        *h =N;
    }
    else
    {
        (*t)->next = N;
        *t = N;
        if(*h==NULL)
            *h=N;
    }
}

void stergere_nod(NOD **h)      //stergerea unui nod din lista
{
	NOD *p;
	if(*h!=0)
	{
		p=*h;
		*h=(*h)->next;
		free(p);
	}
}



ELEM read_list(NOD** h, int i)      //functie ajutatoare la interclasare, returneaza un element ce va fi introdus in heap
{                                   //si il sterge din lista
    ELEM c;

	if(*h==0)
	{
		c.value=0;
		c.nr_lista=0;
	}
	else{
		c.value=(*h)->key;
		c.nr_lista=i;
		stergere_nod(h);
		atribuiri++;
	}


    return c;
}




void testare()      //functie de testare a corectitudinii
{
    NOD* H = NULL;
    NOD* T = NULL;

	for(int i=1;i<15;i++)
		inserare(&H,&T,i);


    afisare(H);

	while(H!=NULL)
	{
		printf("stergere element cu cheia %d \n ", H->key);
		stergere_nod(&H);

	}

    //stergere_elemente(&H,&T);
    afisare(H);
	printf("test efectuat");
}



void creare_liste(int n,int k)      //functie de creare liste
{
    int i,j;
    int x[10000];
    for(i=0;i<k;i++)
    {
        generator_aleator(x, n);
        for(j=0;j<n;j++)
        inserare(&head[i],&tail[i], x[j]);
    }
}


void interclasare(NOD* h[], int k, ELEM *H)     //functie de interclasare a celor k liste
{
    int i;
    ELEM x;
	int heap_dim=0;

	List_head=0;List_tail=0;

    for(i=0;i<k;i++)
    {
       x=read_list(&h[i], i);
       H_Push(H,&heap_dim,x);
    }

    while(heap_dim>0)
    {
        x=H_Pop(H,&heap_dim);
        inserare(&List_head, &List_tail, x.value);
		atribuiri++;

		x=read_list(&h[x.nr_lista], x.nr_lista);

        if (x.value!=0)
            H_Push(H,&heap_dim,x);
    }

}


int main()
{
    srand (time(NULL));

	//TESTAREA CORECTITUDINII FUNCTIILOR
	/*
    int n=12,k=5,i;
    creare_liste(n,k);

    for(i=0;i<k;i++) afisare(head[i]);

    ELEM H[100];
    interclasare(head, k, H);
	printf("Lista rezuList_tailat:\n");
    afisare(List_head);
	*/
	//apelam functia "testare"

	//testare();

	int n;
	int k1=5;
	int k2=10;
	int k3=100;
	FILE *f;
	ELEM H[1000];

	f=fopen("fisier.csv", "w");
	fprintf(f, "n, k=5, k=10, k=100 \n");


	for(n=100; n<=10000; n+=100)     //cerinta 1, n=100...10000 , k=5, k=10, k=100
	{

		atribuiri=0;comparatii=0;

		creare_liste(n, k1);
		interclasare(head, k1, H);
		fprintf(f, "%d,", n);
		fprintf(f, "%d,", atribuiri+comparatii);


		atribuiri=0;comparatii=0;

		creare_liste(n, k2);
		interclasare(head, k2, H);
		fprintf(f, "%d,", atribuiri+comparatii);

		atribuiri=0;comparatii=0;

		creare_liste(n, k3);
		interclasare(head, k3, H);
		fprintf(f, "%d\n", atribuiri+comparatii);


	}

	fclose(f);


/*	n=10000;            //cerinta 2, n=10000, k=10...500
	FILE *f2;
	f2=fopen("fisier2.csv", "w");

	fprintf(f2, "k=nr liste, operatii\n");

	for(k1=10;k1<=500;k1=k1+10)
	{
	    atribuiri=0;comparatii=0;

	    creare_liste(n/k1, k1);
	    interclasare(head, k1, H);
	    fprintf(f2, "%d, %ld \n", k1, atribuiri+comparatii);
	}

    fclose(f2);
*/
	getch();

return 0;
}
