#include<iostream>
#include<conio.h>
#include<stdlib.h>

#define ALB 1
#define GRI 2
#define NEGRU 3

using namespace std;
int i,j, k;
int n,m; //nr de varfuri si nr de mucheieii
int v;
int d[10], f[10],timp;
int culoare[10], parinte[10];
int stanga[10], dreapta[10], nrm, val[10];
typedef struct nod{
			int cheie;
			struct nod *urm;
			}NOD;

NOD *prim[10];
NOD *head[60001], *tail[60001];


void CAvizita(int u)
{
	NOD *p;
	culoare[u] = 2; //culoare gri
	timp = timp+1;
	d[u] = timp;

	p = prim[u];	//ma plasez pe primul nod
	while(p->urm!=0)	//***ANONIM*** timp sunt in lista de adiacenta a lui u
		{
			p=p->urm;	
			if(culoare[p->cheie] == ALB)	//daca culoarea este alpa
			{
				parinte[p->cheie]=u;	//parintele lui v este u
				printf(" %d ", p->cheie);	//afisez cheia
				nrm++;	//cresc numarul muchiei
				stanga[nrm]=u;	//retin u	
				dreapta[nrm]=p->cheie;	//retin v
				val[nrm]=1;	//valoarea e 1 - muchie in arbore

				CAvizita(p->cheie);	//apelez recursiv
			}
		}
	culoare[u]= NEGRU;	//culoarea e negru

	
	timp=timp+1;
	f[u]=timp;
}


void print()
{
	for(i = 1;i <= nrm; i++)
	{
		if(val[i] == 1)
			printf("Muchia (%d, %d) este in arbore.\n", stanga[i], dreapta[i]);
	}
}

void main()
{
	NOD *x;
	NOD *p;

	printf("Dati numarul de varfuri: ");
	scanf("%d",&n);

	printf("\nDati numarul de mucheieii: ");
	scanf("%d", &m);

	for(i=1; i<=n; i++){
		prim[i]=0;
	}

	printf("\nIntroduceti mucheiile: \n");
	for(k = 1;k <= m; k++){
		scanf("%d %d", &i, &j);
		if(prim[i] == 0){//daca lista e nula
			prim[i]=(NOD *)malloc(sizeof(NOD));	//creez un nod
			prim[i]->cheie=i;	//cu cheia i
			
			x=(NOD *)malloc(sizeof(NOD));	//aloc un nou nod
			x->cheie=j;	//cu cheia j
			x->urm=0;
			prim[i]->urm=x;
		}
		else{//adaug in lista de adiacenta
			p = prim[i];	//altfel, ma plasez pe primu' nod
			while(p->urm!=0)	//parcurg lista
				p=p->urm;
			x=(NOD *)malloc(sizeof(NOD));	//creez un nou nod
			x->cheie=j;	//cheia va fi j
			x->urm=0;
			p->urm=x;
		}
	}

	for(k = 1; k <= n; k++){
		if(prim[k] == 0){	//daca k e null
			prim[k]=(NOD *)malloc(sizeof(NOD));	//creez un nod nou cu cheia k
			prim[k]->cheie=k;
			prim[k]->urm=0;
		}
	}
	//initializez nodurile
	for(k = 1; k <= n; k++){
		culoare[k]=1;
		parinte[k]=0;
	}
	timp=0;

	printf("Varfurile in ordinea vizitarii lor:\n");
	for(k=1;k<=n;k++){
		if(culoare[k]==1)
			{
				printf("%d ", k);
				CAvizita(k);
			}
	}
	printf("\n");
	print();


	printf("Apasati un buton pentru a termina.%c\n",7);
	getch();
}