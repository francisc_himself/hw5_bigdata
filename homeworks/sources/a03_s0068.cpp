/*************
***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Ambele metode de sortare au o complexitate de O(nlgn),numarul de comparari si atribuiri fiind mult mai mici decat la restul metodelor de sortare.
Din analiza celor doua metode de sortare putem observa ca metoda QuickSort este metoda ***ANONIM*** mai rapida de sortare.


************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#define MAX 10000
#define FIX 5
int heapSize;
int n=100;
long compbottom,atbottom,compq,atq;
void inserareValori(int *a,int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%MAX;
	}
}

void bestQuick(int *a,int n)
{
	int x=a[0];
	a[0]=a[1];
	a[1]=x;
	a[0]=x;
	a[0]=a[n/2];
	a[n/2]=x;
	a[n-1]=x;
	a[n-1]=a[n/2];
	a[n/2]=x;
	
}

void inserareValoriCrescator(int *a,int n)
{	
	
	srand(time(NULL));
	a[0]=rand()%MAX;
	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]+rand()%MAX;
	}
}
void inserareValoriDescrescator(int *a,int n)
{
	
	srand(time(NULL));
	a[0]=rand()%MAX;
	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]-rand()%MAX;
	}
}

void afisareValori(int *a,int n)
{

	for(int i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
	printf("\n");
}



int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return (2*i+1);
}

int right(int i)
{
	
	return (2*i+2) ;
}


void maxHeapify(int *a,int i)
{
	int largest;
	int	l=left(i);
	int	r=right(i);
	compbottom=compbottom+2;
	
	if ((l<heapSize) && (a[l]>a[i]))
	{
		largest=l;
	}
	else 
	{
		largest=i;
	}
	if ((r<heapSize) && (a[r]>a[largest]))
	{
		largest=r;
	}
	if(largest!=i)
	{
		int x=a[i];
		a[i]=a[largest];
		a[largest]=x;
		atbottom=atbottom+3;
		maxHeapify(a,largest);
	}

}

void bottomUpHeap(int *a,int n)
{
	heapSize=n;
	for(int i=n/2;i>=0;i--)
	{
		maxHeapify(a,i);
	}
}

void heapSort(int *a,int n)
{
	bottomUpHeap(a,n);
	heapSize=n;
	for(int i=n-1;i>=1;i--)
	{
		//printf("a[0]=%d\n",a[0]);
		int x=a[i];
		a[i]=a[0];
		a[0]=x;
		atbottom=atbottom+3;
		heapSize=heapSize-1;
		maxHeapify(a,0);
	}
}

int partition(int *a,int p, int r)
{
	int x=a[r];
	
	int i=p-1;
	for(int j=p;j<r;j++)
	{ compq=compq+1;
		if(a[j]<x)
		{
			i=i+1;
			int k=a[i];
			a[i]=a[j];
			a[j]=k;
			atq=atq+3;
		}
	}
	atq=atq+4;
	int y=a[i+1];
	a[i+1]=a[r];
	a[r]=y;
	return i+1;
}

void quickSort(int *a,int p,int r)
{
	if (p<r)
	{
		int q=partition(a,p,r);
		quickSort(a,p,q-1);
		quickSort(a,q+1,r);
	}
}

void prettyPrint(int *a,int n,int i,int nivel)
{
	if(i<n){

	for(int j=0;j<nivel;j++)
		printf("\t");
	printf("%d\n",a[i]);
	prettyPrint(a,n,left(i),nivel+1);
	prettyPrint(a,n,right(i),nivel+1);
	}
}
void copiereValori(int *a,int *b,int n)
{
	for(int i=0;i<n;i++)
	{
		b[i]=a[i];
	}
}

void main()
{
	int tempcompb=0,tempatb=0,tempcompt=0,tempatt=0;
	FILE *f=fopen("Assig3Average.csv","w");
	FILE *fb=fopen("Assign3Best.csv","w");
	FILE *fw=fopen("Assign3Wors.csv","w");
	int m=20;
	int *c=(int*)malloc(sizeof(int)*m);
	int *a=(int*)malloc(sizeof(int)*m);
	inserareValori(a,m);
	afisareValori(a,m);
	quickSort(a,0,m-1);
	afisareValori(a,m);
	free(a);//*/
	//inserarevalori(c,m);
	//afisarevalori(c,m);
	////bottomupheap(c,m);
	////afisarevalori(c,m);
	////prettyprint(c,m,0,0);
	////heapsize=n;
	//heapsort(c,m);
	//afisarevalori(c,m);
	//prettyprint(c,m,0,0);
	//free(c);
	
	//int *a=(int*)malloc(sizeof(int)*n);
	//int *b=(int*)malloc(sizeof(int)*n);
	for(n=100;n<=10000;n=n+100)
	{
	int *a=(int*)malloc(sizeof(int)*n);
	int *b=(int*)malloc(sizeof(int)*n);
	//int *c=(int*)malloc(sizeof(int)*n);
	//int *d=(int*)malloc(sizeof(int)*n);
	//int *e=(int*)malloc(sizeof(int)*n);
	//int *g=(int*)malloc(sizeof(int)*n);
	heapSize=n;
		for(int i=0;i<5;i++)
		{
			inserareValori(a,n);
			copiereValori(a,b,n);
			heapSort(a,n);
			quickSort(b,0,n-1);
		}
		
		printf("%d ",n);
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d\n",n,compbottom/FIX,compq/FIX,atbottom/FIX,atq/FIX,(compbottom/FIX)+(atbottom/FIX),(atq/FIX)+(compq/FIX));
		compbottom=0;
		atbottom=0;
		atq=0;
		compq=0;
		/*inserareValoriCrescator(a,n);
		bestQuick(a,n);
		inserareValoriDescrescator(b,n);
		quickSort(a,0,n-1);
		heapSort(b,n);
		fprintf(fb,"%d,%d,%d,%d,%d,%d,%d\n",n,compbottom,compq,atbottom,atq,atbottom+compbottom,atq+compq);
		compbottom=0;
		atbottom=0;
		compq=0;
		atq=0;
		inserareValoriDescrescator(a,n);
		inserareValoriCrescator(b,n);
		quickSort(a,0,n-1);
		heapSort(b,n);
		fprintf(fw,"%d,%d,%d,%d,%d,%d,%d\n",n,compbottom,compq,atbottom,atq,atbottom+compbottom,atq+compq);
		compq=0;
		atq=0;
		compbottom=0;
		atbottom=0;*/
		free(a);
		free(b);
	}
	fclose(f);
	fclose(fb);
	fclose(fw);


}