#include <conio.h>
#include <stdio.h>
#include "Profiler.h"
#define M 9973
#define h(k,i) (k+11*i+997*i*i)%M


Profiler profiler("hash_table");


int hInsert(int t[],int k) {
	int i=0,j;
	while (i<M) {
		j=h(k,i);
		if (t[j]==0) {
			t[j]=k;
			return j;
		} else 
			i++;
	}
	return -2;
}


int hSearch(int t[],int k,int *op) {
	int i=0,j;
	*op=*op+1;
	do {
		j=h(k,i);
		if (t[j]==k)
			return j;
		i++;
		*op=*op+1;
	} while (t[j]!=0 && i<M);
	return 0;
}


void empty(int t[]) {
	for (int j=0;j<M;j++) 
		t[j]=0;
}


int main() {
	FILE *f=fopen("hash.csv","w");
	int t[M];
	int y,da=0,nu=0,op=0,dmax=0,nmax=0,max1,max2,sum1=0,sum2=0,ssum1=0,ssum2=0,sda=0,snu=0;
	int a[10000],j=0;

	fprintf(f,"Filling factor,Avg effort found,Max found,Avg effort not-found,Max not-found\n");
	srand (34);
	for (int p=1;p<=5;p++) {
		
		empty(t);
		FillRandomArray(a,7978,1,50000,true,0);

		j=0;
		for (int i=0;i<7978;i++) { // 0.8
			y=hInsert(t,a[i]);	
			if (y==-2)
				printf("Hash full!");
		}

		da=nu=max1=max2=sum1=sum2=j=0;
		for (int i=0;i<3000;i++) {
			op=0;
			y=hSearch(t,(i%2==0)?a[rand()%7978]:rand()+100000,&op);
			if (y!=0) {
				da++;
				if (op>max1)
					max1=op;
				sum1+=op;
			}
			else {
				nu++;
				if (op>max2)
					max2=op;
				sum2+=op;
			}
		}
		printf("%d %d %d %d \n",da,nu,sum1,sum2);
		dmax+=max1;
		nmax+=max2;
		ssum1+=sum1;
		ssum2+=sum2;
		sda+=da;
		snu+=nu;
	}
	fprintf(f,"0.8,%f,%d,%f,%d\n",(float)ssum1/sda,dmax/5,(float)ssum2/snu,nmax/5);
	
	dmax=nmax=ssum1=ssum2=sda=snu=0;
	for (int p=1;p<=5;p++) {
		empty(t);
		FillRandomArray(a,8477,1,50000,true,0);

		j=0;
		for (int i=0;i<8477;i++) { // 0.85
			y=hInsert(t,a[i]);	
			if (y==-2)
				printf("Hash full!");
		}

		da=nu=max1=max2=sum1=sum2=j=0;
		for (int i=0;i<3000;i++) {
			op=0;
			y=hSearch(t,(i%2==0)?a[rand()%8477]:rand()+100000,&op);
			if (y!=0) {
				da++;
				if (op>max1)
					max1=op;
				sum1+=op;
			}
			else {
				nu++;
				if (op>max2)
					max2=op;
				sum2+=op;
			}
		}
		printf("%d %d %d %d \n",da,nu,sum1,sum2);
		dmax+=max1;
		nmax+=max2;
		ssum1+=sum1;
		ssum2+=sum2;
		sda+=da;
		snu+=nu;
	}
	fprintf(f,"0.85,%f,%d,%f,%d\n",(float)ssum1/sda,dmax/5,(float)ssum2/snu,nmax/5);

	dmax=nmax=ssum1=ssum2=sda=snu=0;
	for (int p=1;p<=5;p++) {
		empty(t);
		FillRandomArray(a,8975,1,50000,true,0);

		j=0;
		for (int i=0;i<8975;i++) { // 0.9
			y=hInsert(t,a[i]);	
			if (y==-2)
				printf("Hash full!");
		}

		da=nu=max1=max2=sum1=sum2=j=0;
		for (int i=0;i<3000;i++) {
			op=0;
			y=hSearch(t,(i%2==0)?a[rand()%8975]:rand()+100000,&op);
			if (y!=0) {
				da++;
				if (op>max1)
					max1=op;
				sum1+=op;
			}
			else {
				nu++;
				if (op>max2)
					max2=op;
				sum2+=op;
			}
		}
		printf("%d %d %d %d \n",da,nu,sum1,sum2);
		dmax+=max1;
		nmax+=max2;
		ssum1+=sum1;
		ssum2+=sum2;
		sda+=da;
		snu+=nu;
	}
	fprintf(f,"0.9,%f,%d,%f,%d\n",(float)ssum1/sda,dmax/5,(float)ssum2/snu,nmax/5);

	dmax=nmax=ssum1=ssum2=sda=snu=0;
	for (int p=1;p<=5;p++) {
		empty(t);
		FillRandomArray(a,9475,1,50000,true,0);

		j=0;
		for (int i=0;i<9474;i++) { // 0.95
			y=hInsert(t,a[i]);
			if (y==-2)
				printf("Hash full!");
		}
		da=nu=max1=max2=sum1=sum2=j=0;
		for (int i=0;i<3000;i++) {
			op=0;
			y=hSearch(t,(i%2==0)?a[rand()%9474]:rand()+100000,&op);
			if (y!=0) {
				da++;
				if (op>max1)
					max1=op;
				sum1+=op;
			}
			else {
				nu++;
				if (op>max2)
					max2=op;
				sum2+=op;
			}
		}
		printf("%d %d %d %d \n",da,nu,sum1,sum2);
		dmax+=max1;
		nmax+=max2;
		ssum1+=sum1;
		ssum2+=sum2;
		sda+=da;
		snu+=nu;
	}
	fprintf(f,"0.95,%f,%d,%f,%d\n",(float)ssum1/sda,dmax/5,(float)ssum2/snu,nmax/5);

	dmax=nmax=ssum1=ssum2=sda=snu=0;
	for (int p=1;p<=5;p++) {
		empty(t);
		FillRandomArray(a,9874,1,50000,true,0);
		j=0;
		for (int i=0;i<9873;i++) { // 0.99
			y=hInsert(t,a[i]);
			if (y==-2)
				printf("Hash full!");
		}

		da=nu=max1=max2=sum1=sum2=j=0;
		for (int i=0;i<3000;i++) {
			op=0;
			y=hSearch(t,(i%2==0)?a[rand()%9873]:rand()+100000,&op);
			if (y!=0) {
				da++;
				if (op>max1)
					max1=op;
				sum1+=op;
			}
			else {
				nu++;
				if (op>max2)
					max2=op;
				sum2+=op;
			}
		}
		printf("%d %d %d %d\n",da,nu,sum1,sum2);
		dmax+=max1;
		nmax+=max2;
		ssum1+=sum1;
		ssum2+=sum2;
		sda+=da;
		snu+=nu;
	}
	fprintf(f,"0.99,%f,%d,%f,%d\n",(float)ssum1/sda,dmax/5,(float)ssum2/snu,nmax/5);


	/*	for (int i=1;i<500;i++) {
		hInsert(t,i*4);
	}
	for (int i=1;i<550;i++) {
		y=hSearch(t,i*4);
		if (y==0)
			printf("no ");
		else 
			printf("yes ");
	} */


	getch();
	return 0;
}