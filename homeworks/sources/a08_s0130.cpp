#include <stdio.h>
#include "Profiler.h"
#include <iostream>
#include <conio.h>
#include <fstream>
using namespace std;

Profiler profiler("DisjointSet");
struct node
{
	node *p;
	int rank;
	int val;
};

struct edge
{
	node *x, *y;
};

struct Graph
{
	node *V[10000];
	edge *E[60000];
};

void makeSet(node *x, long size)
{
	profiler.countOperation("MakeSet", size);
	x->p=x;
	x->rank=0;
}

void link(node *x, node *y)
{
	if(x->rank>y->rank)
		y->p=x;
	else {
		x->p=y;
		if(x->rank==y->rank)
			y->rank++;
	}
}

node * find(node *x, long size)
{
	profiler.countOperation("Find", size);
	
	if(x!=x->p)
		x->p=find(x->p, size);

	return x->p;
}


void Union(node *x, node *y, long size)
{
	profiler.countOperation("Union", size);
	link(find(x, size),find(y, size));
}

/*
CONNECTED-COMPONENTS.G/
1 for each vertex  2 G:V
2 MAKE-SET./
3 for each edge .u; / 2 G:E
4 if FIND-SET.u/ � FIND-SET./
5 UNION.u; /*/

void connectComponents(Graph *G, int v, long e)
{
	int k=0;
	for(int i=0; i<v; i++)
	{

		makeSet(G->V[i], e);
	}
	for(int i=0; i<e; i++)
	{
		if(find(G->E[i]->x, e)!=find(G->E[i]->y, e))
		{
			Union(G->E[i]->x,G->E[i]->y, e);
			printf("Connected %d with %d\n", G->E[i]->x->val,G->E[i]->y->val);
		}
	}
}

Graph * generateGraph(long v, long e)
{
	Graph *g;
	srand(time(NULL));
	g=(Graph*)malloc(sizeof(Graph));
	
	for(int i=0; i<v; i++)
	{
		g->V[i]=(node *)malloc(sizeof(node));
		g->V[i]->val=i;
	}
	for(int i=0; i<e; i++)
	{
		g->E[i]=(edge*)malloc(sizeof(edge));
		g->E[i]->x=g->V[rand()%v];
		g->E[i]->y=g->V[rand()%v];
	}

	return g;
}


int main ()
{
	for(long i=10000; i<60000; i+=1000)
		 connectComponents(generateGraph(10000,i),10000,i);
	
	profiler.addSeries("SumAux", "Find", "MakeSet");
	profiler.addSeries("SumTotal", "SumAux", "Union");
	profiler.createGroup("Sum", "SumAux", "SumTotal");
	profiler.showReport();
	
	getch();
	return 0;
}

