// First graph: linear growth(for all 3 ops)
// Second graph: log growth
#include "Profiler.h"
#define n 4
int k=4;
int op=0;
//int ops5=0,ops10=0;ops100=0;
int outputSize=0;
typedef struct lista
{
	int val;
	lista *next; 
}liste;
typedef struct h
{
	int valoare;
	int index; 
}Heap;
liste *vector[501];
int heapSize=0;
int aux[n];
Heap *heap[100000];
int output[100000];
int parent(int x)
{
	if(x>1)
	return x/2;
	else return 1;
}

int left_child(int x)
{
	return 2*x;
}

int right_child(int x)
{
	return 2*x+1;
}
void heapIncreaseKey(Heap *heap[], int x, int key)
{
	heap[x]->valoare=key;
	int aux;
	while ((x>1) && (heap[parent(x)]->valoare>heap[x]->valoare))
	{
	aux=heap[x]->valoare;
	heap[x]->valoare=heap[parent(x)]->valoare;
	heap[parent(x)]->valoare=aux;
	x=parent(x);
	op=op+4;
	}
	op=op+1;
}

void heapInsert(Heap *heap[], int key, int idx)
{
	heapSize+=1;
	heap[heapSize] = (Heap*)malloc(sizeof(Heap*));
	heap[heapSize]->index=idx;
	heapIncreaseKey(heap,heapSize,key);
}
void minHeapify(Heap *heap[], int x)
{
	int left = left_child(x);
	int right = right_child(x);
	Heap* aux;
	int min;
	if (left<=heapSize && heap[left]->valoare<heap[x]->valoare) 
	{
		min=left;
		op++;
	}
	else min = x;

	if (right<=heapSize && heap[right]->valoare<heap[min]->valoare)
	{
		min = right;
		op++;
	}

	if (min != x)
	{
		aux=heap[min];
		heap[min]=heap[x];
		heap[x]=aux;
		op=op+3;
		minHeapify(heap, min);
	}

}
int main()
{
	liste *last;
	liste *p;
	for(int i=1;i<=100000;i++)
	{
		output[i]=0;
		heap[i]=NULL;
		vector[i]=NULL;
	}
	//FILE *f;
	//f=fopen("Test1.csv","w");
	//f1=fopen("Test2.csv","w");
	p=NULL;
	last=NULL;
	//fprintf(f,"Number of lists, Number of operations \n");
	//for(int k=10;k<=500;k+=10)
	//{
	//printf("Genereaza %d liste\n",k);
	//op=0;
	for(int i=1;i<=k;i++)
			vector[i]=NULL;
	for(int i=1;i<=k;i++)
	{
		printf("Lista %d\n",i);
		//for(int j=0;j<n;j++)
		//FillRandomArray(aux,j,10,50000,false,1);
		FillRandomArray(aux,n+1,10,50000,false,1);
		int a=0;
		for(int j=1;j<=n;j++)
		{
			if(vector[i]==NULL)
			{
				vector[i]=(liste *)malloc(sizeof(liste));
				vector[i]->val=aux[a];
				vector[i]->next=NULL;
				a++;
				last=vector[i];
				printf("%d\n",vector[i]->val);
			}
			else
			{
				p=(liste *)malloc(sizeof(liste));
				p->val=aux[a];
				p->next=NULL;
				last->next=p;
				last=p;
				a++;
			printf("%d\n",aux[a]);
			}
		}
	}	
	for(int i=1;i<=k;i++)
	{
		heapInsert(heap,vector[i]->val, i);
		vector[i]=vector[i]->next;
	}
	
	while(heapSize>0)
	{
		outputSize++;
		output[outputSize]=heap[1]->valoare;
		int idx = heap[1]->index;
		if(vector[idx]!=NULL)
		{
			heap[1]->valoare=vector[idx]->val;
			vector[idx]=vector[idx]->next;
			minHeapify(heap,1);
		}
		else
		{
			heap[1]=heap[heapSize];
			heapSize--;
			minHeapify(heap,1);
		}
	}	

	//op=op/5;
	//fprintf(f,"%d\n",k,op);
	//fprintf(f1,"%d\n",n,ops5,ops10,ops100);
	//}
	//fclose(f);
	//fclose(f1);
	printf("\nOutput: \n");
	for(int i=1;i<=outputSize;i++)
	{
		printf("%d ",output[i]);
	}
	printf("\n");
	char c;
	scanf("%c",&c);
	return 0;
}