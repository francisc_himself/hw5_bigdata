﻿/* 

***ANONIM***ș ***ANONIM*** grupa ***GROUP_NUMBER***

--------------------------------------------------------------------------------------
Se cere implementarea corectă și eficientă a două metode de construire a structurii de
date Heap i.e. “de jos în sus” (bottom-up) și “de sus în jos” (top-down).

Se cere compararea celor două metode de constructie heap în cazul mediu
statistic. Pentru cazul mediu statistic va trebui să repetați măsurătorile de m ori
(m=5) și să raportați valoarea lor medie; de asemenea, pentru cazul mediu
statistic, asigurați-vă că folosiți aceleași date de intrare pentru cele două metode.
----------------------------------------------------------------------------------------
Data început: 14.03.2013
Data sfârșit: 20.03.2013
----------------------------------------------------------------------------------------
						INTERPRETĂRI
BUTTOM-UP
	-> construire ”de jos în sus”;
	-> se bazează pe ideea că jumătatea superioară a vectorului ce reprezintă heap-ul sunt frunze, adică heap-uri de un element
	-> pentru fiecare din celelalte elemente se reconstituie heap-ul încercând să-l ducă la locul potrivit astfel încât să fie respectată proprietatea
	-> procedura de reconstituire a heap-ului necesită un timp maxim O(log n), dar depinde de înălțimea nodului în arbore
	-> datorită apelurilor repetate timpul maxim al acestei metode devine: O(nlog n)
	-> în caz favorabil, când vectorul e ordonat, complexitatea devine O(n)
TOP_DOWN
	-> construire ”de sus în jos”;
	-> se consideră inițial un heap vid
	-> fiecare element se inserează pe ultima poziție și apoi este mutat la locul său, unde va indeplini proprietatea
	-> procedura de push a heap-ului necesită un timp maxim O(log n)
	-> datorită apelurilor repetate timpul maxim al acestei metode devine: O(n log n) in cazul defavorabil
COMPARARE
	-> metoda TOP_DOWN necesită memorie suplimentară
	-> reprezentarea grafică dovedește că metoda Buttom-Up este mai eficientă
	-> cele doua metode au o complexitate liniara in cazul mediu
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include<time.h>

#define DIM_MAX 10000 

int dimA, dimB;
long compBU=0, atrBU=0, sumaBU;
long compTD=0, atrTD=0, sumaTD;

////////////////////////////////////////  BUTTOM_UP /////////////////////////
//functie ce returneaza indicele tatalui unui nod din heap
int Parinte(int i)
{
	return (i/2);
}

//functie ce returneaza indicele fiului stang unui nod din heap
int Stanga(int i)
{
	return (2*i);
}

//functie ce returneaza indicele fiului drept unui nod din heap
int Dreapta(int i)
{
	return (2*i+1);
}

/*Functie ce reconstituie un heap astfel incat proprieteatea sa fie verificata*/
void ReconstituieHeap(int A[], int i)
{
	int s, d, max, aux;

	s=Stanga(i);
	d=Dreapta(i);

	/******/compBU++;/******/
	if(s<=dimA && A[s]<A[i])
		max = s;
	else max = i;

	/******/compBU++;/******/
	if(d<=dimA && A[d]<A[max])
		max = d;

	if(max!=i)
	{
		/******/atrBU+=3;/******/
		aux = A[i];
		A[i]=A[max];
		A[max]=aux;
		ReconstituieHeap(A, max);
	}
}

void ConstruiesteHeap(int A[], int lung)
{
	int i;
	dimA = lung;
	for(i = (lung/2); i>=1; i--)
		ReconstituieHeap(A, i);
}

////////////////////////////////////////  TOP-DOWN  /////////////////////////
void HPush(int B[], int x)
{
	int i, aux, p;

	dimB++;
	B[dimB]=x;
	/******/atrTD++;/******/

	i=dimB;
	/******/compTD++;/******/
	while(i>1 && B[Parinte(i)] >B[i])
	{
		/******/compTD++;/******/
		p=Parinte(i);
		aux = B[i];
		B[i] = B[p];
		B[p]=aux;
		i=p;
		/******/atrTD+=3;/******/
	}

}

void ConstruiesteHeap_TopDown(int C[],int B[], int lung)
{
	int i;
	dimB = 0;

	for(i=1; i<=lung; i++)
		HPush(B, C[i]);
}

//////////////////////////////////////////// Crearea fisierului csv  //////////////
void MediuStatistic()
{
	int A[DIM_MAX], B[DIM_MAX], C[DIM_MAX];
	int n,i, caz, m=5, var;
	long buComp, buAtr, buSum=0, tdComp, tdAtr, tdSum=0;

	FILE *pf;
	pf = fopen("mediu.csv", "w");

	if(!pf)
	{
		perror("Eroare deschidere fisier!");
		exit(1);
	}

	srand(time(NULL));

	fprintf(pf, "n, compBU, atribBU,sumaBU, compTD, atribTD, sumaTD\n");
	for(n=100;n<=10000;n+=100)
	{
		buComp=0; buAtr=0; buSum=0;
		tdComp=0; tdAtr=0; tdSum=0;
		for(caz=1;caz<=m;caz++)
		{
			compBU=0, atrBU=0;
			compTD=0, atrTD=0;
			for(i=1;i<=n;i++)
			{
				var = rand();
				A[i] = var;
				C[i] = var;
			}

			ConstruiesteHeap(A, n);
			ConstruiesteHeap_TopDown(C, B, n);
			sumaBU = compBU + atrBU;
			sumaTD = compTD + atrTD;
			
			buComp += compBU;
			buAtr += atrBU;
			buSum += sumaBU;
			tdComp += compTD;
			tdAtr += atrTD;
			tdSum += sumaTD;

		}
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d\n", n, buComp/5, buAtr/5, buSum/5, tdComp/5, tdAtr/5, tdSum/5 );

	}

	fclose(pf);
	printf("Gata!");
}

////////////////////////////////////////  testarea ////////////////////////////////////
void citire(int A[],int C[], int *n)
{
	int i;
	printf("n=");
	scanf("%d", n);

	for(i = 1; i<=*n; i++)
	{
		printf("A[%d]=", i);
		scanf("%d", &A[i]);
		C[i]=A[i];
	}

}


void afiseaza(int A[], int n)
{
	int i, j, p=2, spatii=n;

	for(j=1;j<=spatii;j++)
			printf(" ");
	 for(i = 1; i<=n; i++)
	 {
		
		if(i==p)
		{	 p*=2;
			spatii-=p;
			printf("\n");
			for(j=1;j<=spatii;j++)
			printf(" ");
		}
		
		printf(" %d    ", A[i]);
	 }
	

}

void Testare()
{
	int n,i;
	int A[DIM_MAX], B[DIM_MAX], C[DIM_MAX];

	citire(A, C, &n);
	ConstruiesteHeap(A, n);
	printf("\n******** BUTTOM-UP  *********\n");
	afiseaza(A, n);
	ConstruiesteHeap_TopDown(C, B, n);
	printf("\n******** TOP_DOWN  *********\n");
	afiseaza(B, n);
	getch();
}

int main()
{
	Testare();
	//MediuStatistic();
	return 0;
}
