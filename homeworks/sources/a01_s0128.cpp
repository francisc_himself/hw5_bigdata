#include "stdio.h";
#include "conio.h";
#include "stdlib.h";
#include "Profiler.h";

#define MAX_NR 10000

Profiler profiler("SortTest");

int a[MAX_NR];
int sorted[MAX_NR+1];
//int n = 5;					//length of the array fixed for correctness proving
int n;							//length of the array

/*********************************************************************************************
/
/									OBSERVATIONS, INTERPRETATIONS
/
/  1. Some charts could not be generated to represent all three sorting methods on the same chart
/ either because the difference in values is so big that the others are not visibleor so small 
/ that they almost cover each other. These charts are:
/		- avg case comparisons			- selection sort represented separately
/       - avg case assignments			- selection sort represented separately
/		- avg case total operations		- insertion sort represented separately
/		- best case comparisons			- bubble and insertion sort have same value and represented 
/                                         separately
/		- best case assignments			- bubble and selection sort have same value
/		- best case total operations	- selection sort has higher values, others represented 
/                                         separately
/		- worst case comparisons		- selection and bubble sort have same values, insertion sort
/										  represented separately
/		- worst case assignments		- selection sort has small values, represented separately
/
/  2. Some algorithms cannot be represented on the same chart with the others because of the running
/ time. For example selection sort generally has linear growth because of O(n) running time while the
/ others have exponential growth.
/
/  3. Efficiency of algorithms:
/		- in average case bubble sort highly takes the most operations to complete while selection sort
/		  is efficient when it comes to assignments, it takes O(n) time.
/		- in best case bubble sort is the most efficient because it runs in O(n) time while selection
/		  sort becomes the worst, with exponential growth and high number of operations
/		- in worst case the total operations chart shows that bubble sort takes the most number of
/		  operations, insertion sort is the second and selection sort is the best with the minimum
/		  of operations
/
/  4. All 3 algorithms are devised to be stable which means that they keep the order of the same
/ input values in the original array. This is done by not allowing swaps and insertions when equality
/ is found between two elements.
/
/********************************************************************************************/

void insertionSort()
{
	int k = 0;
	//copy the initial array
	sorted[0] = 0;
	for(int i = 0; i < n; i++)
	{
		sorted[i+1] = a[i];
	}
	// "initialize" the counter
	profiler.countOperation("insertionAssignment",n,0);
	profiler.countOperation("insertionComparison",n,0);
	for(int i = 2; i<= n; i++)
	{
		profiler.countOperation("insertionAssignment",n);
		sorted[0] = sorted[i];
		k = i-1;
		while(sorted[k] > sorted[0])
		{
			profiler.countOperation("insertionComparison",n);
			profiler.countOperation("insertionAssignment",n);
			sorted[k+1] = sorted[k];
			k--;
		}
		profiler.countOperation("insertionComparison",n);
		profiler.countOperation("insertionAssignment",n);
		sorted[k+1] = sorted[0];
	}
	profiler.addSeries("insertionTotal","insertionComparison","insertionAssignment");
	return;
}

void selectionSort()
{
	int imin;
	int aux;
	//create a copy of the original array
	for(int i = 0; i < n; i++)
	{
		sorted[i+1] = a[i];
	}
	//"initialize" counters
	profiler.countOperation("selectionComparison",n,0);
	profiler.countOperation("selectionAssignment",n,0);
	for(int i = 1; i <= n; i++)
	{
		imin = i;
		for(int j = i+1; j <= n; j++)
		{
			profiler.countOperation("selectionComparison",n);
			if(sorted[j] < sorted[imin])
			{
				imin = j;
			}
		}
		if(imin != i)
		{
			profiler.countOperation("selectionAssignment",n,3);
			aux = sorted[imin];
			sorted[imin] = sorted[i];
			sorted[i] = aux;
		}
	}
	profiler.addSeries("selectionTotal","selectionComparison","selectionAssignment");
	return;
}

void bubbleSort()
{
	int lastSwap = n;
	int swapCondition = n;
	int aux;
	bool swapped = false;
	//make a copy of the array
	for(int i = 0; i < n; i++)
	{
		sorted[i+1] = a[i];
	}
	//"initialize" counters
	profiler.countOperation("bubbleComparison",n,0);
	profiler.countOperation("bubbleAssignment",n,0);
	do
	{
		swapped = false;
		swapCondition = lastSwap;
		for(int i = 1; i <= swapCondition-1; i++)
		{
			profiler.countOperation("bubbleComparison",n);
			if(sorted[i] > sorted[i+1])
			{
				profiler.countOperation("bubbleAssignment",n,3);
				aux = sorted[i+1];
				sorted[i+1] = sorted[i];
				sorted[i] = aux;
				lastSwap = i;
				swapped = true;
			}
		}
	}
	while (swapped);
	profiler.addSeries("bubbleTotal","bubbleComparison","bubbleAssignment");
}

void main()
{
	/*// Unit to prove correctness
	//hard-coded array used for testing methods
	a[0] = 3; a[1] = -1; a[2] = 1; a[3] = 6; a[4] = 1;
	//print the array
	printf("The original array\n");
	for(int i = 0; i < n; i++)
	{
		printf("%d ",a[i]);
	}
	//testing insertionSort
	insertionSort();
	printf("\nThe sorted array with insertion sort\n");
	for(int i = 1; i <= n; i++)
	{
		printf("%d ",sorted[i]);
	}
	//testing selectionSort
	selectionSort();
	printf("\nThe sorted array with selection sort\n");
	for(int i = 1; i <= n; i++)
	{
		printf("%d ",sorted[i]);
	}
	//testing bubbleSort
	bubbleSort();
	printf("\nThe sorted array with bubble sort\n");
	for(int i = 1; i <= n; i++)
	{
		printf("%d ",sorted[i]);
	}*/
	

	//unit for evaluating avg case

	/*for(int i = 1; i <= 5; i++)
	{
	for(n = 100; n <= 10000; n+=100)
	{
		FillRandomArray(a,n, -1000, 5000,false,0);
		insertionSort();
		selectionSort();
		bubbleSort();
		printf("%d\n",n);
	}
	}
	profiler.createGroup("avgCaseComparisons","insertionComparison","selectionComparison","bubbleComparison");
	profiler.createGroup("avgCaseSelectionComparisons","selectionComparison");
	profiler.createGroup("avgCaseAssignments","insertionAssignment","selectionAssignment","bubbleAssignment");
	profiler.createGroup("avgCaseSelectionAssignments","selectionAssignment");
	profiler.createGroup("avgCaseTotalOperations","insertionTotal","selectionTotal","bubbleTotal");
	profiler.createGroup("avgCaseInsertionTotalOperations","insertionTotal");*/
	

	//unit for evaluating best case
	//best case for all of them is when the array is already ordered

	/*for(n = 100; n <= 10000; n+=100)
	{
		FillRandomArray(a,n,-1000,5000,false,1);
		insertionSort();
		selectionSort();
		bubbleSort();
		printf("%d\n",n);
	}
	profiler.createGroup("bestCaseComparisons","insertionComparison","selectionComparison","bubbleComparison");
	profiler.createGroup("bestCaseBubbleAndInsertionComparisons","insertionComparison");
	profiler.createGroup("bestCaseAssignments","insertionAssignment","selectionAssignment","bubbleAssignment");
	profiler.createGroup("bestCaseTotalOperations","insertionTotal","selectionTotal","bubbleTotal");
	profiler.createGroup("bestCaseInsertionTotalOperations","insertionTotal");
	profiler.createGroup("bestCaseBubbleTotalOperations","bubbleTotal");*/

	//unit for evaluating worst case
	//worst case for insertion sort and bubble sort is when the array is sorted reversely
	//worst case for selection sort is when the array is sorted until n-1 and the n-th element
	//contains the smallest element on the array

	for(n = 100; n <= 10000; n+=100)
	{
		FillRandomArray(a,n,-1000,5000,false,2);
		insertionSort();
		bubbleSort();
		printf("%d\n",n);
	}
	for(n = 100; n <= 10000; n+=100)
	{
		FillRandomArray(a,n-1,-5000,6000,true,1);
		a[n-1] = -5001;
		selectionSort();
		printf("%d\n",n);
	}
	profiler.createGroup("worstCaseComparisons","insertionComparison","selectionComparison","bubbleComparison");
	profiler.createGroup("worstCaseInsertionComparisons","insertionComparison");
	profiler.createGroup("worstCaseAssignments","insertionAssignment","selectionAssignment","bubbleAssignment");
	profiler.createGroup("worstCaseSelectionAssignments","selectionAssignment");
	profiler.createGroup("worstCaseTotalOperations","insertionTotal","selectionTotal","bubbleTotal");
	profiler.showReport();
	return;
}
