/*
--Name: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
--Group: ***GROUP_NUMBER***
--College: Technical University of Cluj Napoca

--Interpretation: The effort performed by quadratic probing increases linearly, based on fill factor. The more duplicates exist,
the more accesses are required.

*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

#define N 10007
int H[N];
int stored[9906];
int acc_total, max_effort, curr_effort;
int n; //10007 * filling factor
ofstream myfile;

int hprime( int k)
{
	return k%N;
}

int h(int k, int i){
    int c1=1;
    int c2=1;

    int res=(hprime(k) + c1*i +c2*i*i) % N;
    return res;
}

int hashInsert( int T[], int k)
{
	
    int j;
    int i=0;
    do
    {
        j=h(k,i);
        if (T[j]==NULL)
        {
            T[j]=k;
            return j;
        }
        else i=i+1;
    } while (i!=N);
    printf("Hash table overflow");
    return 0;
}

int hashSearch( int T[], int k)
{
	curr_effort=0;
    int j;
    int i=0;
    do
    {
        j=h(k,i);
		acc_total++;
		curr_effort++;
		if(curr_effort>max_effort)
		{
			max_effort=curr_effort;
		}
        if (T[j]==k)
            return j;
        i=i+1;

    }while((T[j]!=NULL) && (i!=N));
    return -1;
}

void newTable(int n){
		for(int i=0; i<n; i++)
	{
		stored[i]=rand()%100000;
		hashInsert(H,stored[i]);
	}
}

void searchNFound(int n){
		for(int j=0; j<1500; j++)
	{
		hashSearch(H,stored[rand()%n]+100000);
	}

}
void searchFound(int n){
		for(int j=0; j<1500; j++)
	{
		hashSearch(H,stored[rand()%n]);
	}

}

void resetHash(){
	for(int i=0; i<10007; i++) H[i]=NULL;
}

void main()

{
	srand(time(NULL)); //change the random seed
	max_effort=0;

	myfile.open ("hashMeas.csv",ios::out); //rewrite file
	myfile << "";
	myfile.close();

	myfile.open ("hashMeas.csv",ios::app);

	acc_total=0; max_effort=0;
	resetHash();

	//case 1
	n=8005; //0.8 filling

	myfile << "Filling factor," << "Avg effort F," << "Max effort F," << "Avg effort NF," << "Max effort NF" << endl;

	myfile << "0.8,";
	printf("\n0.8 \n");

	newTable(n);
	
	searchFound(n);

	myfile << acc_total/1500 << "," << max_effort << ",";
	printf("%d ", acc_total/1500); 	printf("%d ", max_effort);

	acc_total=0; max_effort=0;

	searchNFound(n);

	printf("%d ", acc_total/1500); 	printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort;

	//case 2
	acc_total=0; max_effort=0;
	resetHash();
		
	n=8505; //0.85
	myfile << endl << "0.85,";
	printf("\n0.85 \n");
	newTable(n);

	searchFound(n);

	printf("%d ", acc_total/1500);
	printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort << ",";

	acc_total=0; max_effort=0;

	searchNFound(n);

	printf("%d ", acc_total/1500); 	printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort;

	//case 3
	acc_total=0; max_effort=0;
	resetHash();

	n=9006;

	printf("\n0.9 \n");
	myfile << endl << "0.9,";
	newTable(n);

	searchFound(n);
	printf("%d ", acc_total/1500); printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort << ",";

	acc_total=0; max_effort=0;

	searchNFound(n);

	printf("%d ", acc_total/1500); 	printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort;

	//case 4
	acc_total=0; max_effort=0;
	resetHash();

	n=9506;

	printf("\n0.95 \n");
	myfile << endl << "0.95,";
	newTable(n);

	searchFound(n);

	printf("%d ", acc_total/1500); 	printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort << ",";

	acc_total=0; max_effort=0;

	searchNFound(n);

	printf("%d ", acc_total/1500); 	printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort;

	//case 5
	acc_total=0; max_effort=0;
	resetHash();

	n=9906;
	myfile << endl << "0.99,";
	printf("\n0.99 \n");
	newTable(n);

	searchFound(n);
	printf("%d ", acc_total/1500); printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort << ",";

	acc_total=0; max_effort=0;

	searchNFound(n);

	printf("%d ", acc_total/1500); printf("%d ", max_effort);
	myfile << acc_total/1500 << "," << max_effort;
	myfile.close();
	_getch();
}

	
	
