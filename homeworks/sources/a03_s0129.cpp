﻿/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
  FA - Assignment 3 - Analysis&Comparison of Advanced Sorting Methods Heap sort and Quick sort*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <time.h>

///////////////////////////////////////////////////////////////////////////

int A[10002],C[10001];
int i; //citit
int n, cn;
int qs_eval;
int hs_eval;


/**We generated two charts for this problem. One chart for observing how quick sort acts for different array in the worst and best case and another chart to compare the quick sort with heap sort in the average case. 
Quick sort has a linear evolution. The differences between worst and best case are big, but the advantage of this method is that an avarage case is closly related to the best case.
Heapt sort's  upper bound is nlogn, that means the chart is logarithmic. Heap sort is more effecient than quick sort. Althogth Heap Sort is optimal, it is preferable quick sort in practice. */

///////////////////////////////////////
void quicksort(int A[],int l,int r)
{
   int key,i,j,k;
   if( l < r)
   {
 //   k = (m+n)/2; // pivotul
	  k=r;
	  int aux=A[l];
	  A[l]=A[k];
	  A[k]=aux;
      key = A[l];
	  qs_eval+=4;
      i = l+1;
      j = r;
      while(i <= j)
      {
         while((i <= r) && (A[i] <= key))                 
                i++;          
         while((j >= l) && (A[j] > key))
                j--;
		 qs_eval+=2;
         if( i < j)
		 {
			  aux=A[i];
			  A[i]=A[j];
			  A[j]=aux;
			  qs_eval+=3;
		 }
      }
      // swap two elements from m and j index
      aux=A[l];
	  A[l]=A[j];
	  A[j]=aux;
	  qs_eval+=3;
      // recursively sort the lesser list
      quicksort(A,l,j-1);
      quicksort(A,j+1,r);
   }
}

/**void generate_worst (int n)
{
	for(i=1;i<=n/2; i++)
		  A[i]=n/2+i;
    for(i=n/2+1; i<=n; i++)
		  A[i]=i-n/2;
}*/

//daca alegi pivotul primul worst-ul e sirul descrescator
void generate_worst (int n)
{
    for(int i=1;i<=n;i++)
			      A[i]=n-i+1;

}


void generate_best (int n)
{
    for(int i=1;i<=n;i++)
	     A[i]=i;
}

void generate_average (int n)
{
    for(int i=1;i<=n;i++)
	     C[i]=rand()%10000;
}


void heapify (int A[], int j)
{
	int l, r, max;
	l=2*j;
	r=2*j+1;
	if((l<=n) && A[l]>A[j])
		 max=l;
	else max=j;
	hs_eval+=1;
    if((r<=n) && A[r]>A[max])
		  max=r;
	hs_eval+=1;
	if(max!=j) 
	{
		int aux;
		aux=A[j];
		A[j]=A[max];
		A[max]=aux;
		hs_eval+=3;
		heapify(A, max);
	}
}

void BuildHeapBU (int A[])
{
  for(i=n/2; i>=1; i--)
	  { 
		  heapify (A, i);
	  }
}

void HeapSort(int A[])
{
	BuildHeapBU(A);
	int aux;
	for(i=cn; i>=1; i--)
				{
				  aux=A[i];
				  A[i]=A[1];
				  A[1]=aux;
				  hs_eval+=3;
				  n--;	 
                  heapify (A, 1);
	}
	
}

void main()
{
	/*FILE *f;

	srand(time(NULL));
	f=fopen("avg.txt","w");
	fprintf(f,"n quick heap \n");
	for(n=100; n<=10000; n=n+100)
		  {  
			int tot_hs_eval=0;
			int tot_qs_eval=0;
			for(int k=1;k<=5;k++)
			       { 
					        qs_eval=hs_eval=0;

		        			generate_average(n);
							for(int j=1;j<=n;j++)
								 A[j]=C[j];
							HeapSort(A);
							tot_hs_eval+=hs_eval;

							for(int j=1;j<=n;j++)
								 A[j]=C[j];
							quicksort(A,1,n);
							tot_qs_eval+=qs_eval;

							
			}
			fprintf(f,"%d %d %d\n",n, tot_qs_eval/5, tot_hs_eval/5 );
	}

	fclose(f);

	f=fopen("quick.txt","w");
	fprintf(f,"n best worst\n");
	 for(n=100; n<=10000; n=n+100) //n=100; n<=10.000; n=n+100;
	 {  
		       int best=0, worst=0;
			   qs_eval=0;
		       generate_best(n);
			   quicksort(A, 1, n);
			   best=qs_eval;
			   generate_worst(n);
			   quicksort(A, 1, n);
			   worst=qs_eval;
			   fprintf(f,"%d %d %d \n",n, best, worst);
	 }
	 fclose(f);
	 printf("The End %c", 7);*/

	scanf("%d", &n);
	generate_worst(n);
	for(i=1;i<=n;i++)
		   printf("%d ", A[i]);
    printf("\n");
	quicksort(A,1,n);
	for(i=1;i<=n;i++)
		   printf("%d ", A[i]);
	printf("\n%d", qs_eval);

	//testare HeapSort
	generate_worst(n);
    printf("\n");
	cn=n;
	BuildHeapBU(A);
	for(i=1;i<=n;i++)
		   printf("%d ", A[i]);
	HeapSort(A);
	printf("\n");
		for(i=1;i<=cn;i++)
		   printf("%d ", A[i]);
		   
		   
	getch(); 
}