    /*
    Name: ***ANONIM*** ***ANONIM***
	Group: ***GROUP_NUMBER***
	
	Analysis of merging algorythm for k ordered lists :

	-As is, the code runs the algorithms on a small predifined set to check for correctness.
	-To rerun the analysis, the code sections at the bottom have to be uncommented one at a time to perform the appropriate analysis case.
    
	For a fixed number of lists, the number of total operations performed increases slightly with the number of lists.
	

     
    */
     
    #include <conio.h>
    #include <stdio.h>
	#include <math.h>
    #include <stdlib.h>     
    #include <time.h>   
    #include <iostream>
    #include <fstream>
       
    using namespace std;
    
    int A[10000];
    int B[10000];
    int heap[10001];
    int res[10000];
    static int heap_size = 0;
    ofstream fileStr;
     
	struct NodeH
	{
           int value;
           int sourceList;
    };


	 struct NodeL 
	 { 
           int value;
           NodeL *nextNode;
    };

	 

    #define Parent(i)       ((i-1)/2)
    #define Left(i)         (2*i+1)
    #define Right(i)        (2*i+2)
    #define swapT(a,b) int buf; buf=a; a=b; b=buf;
     
   
     int randomizer = 0;
	 int BUtotal = 0; int comp = 0; int assig = 0;
    static NodeL *listDat[10000]; 
    NodeH *heapDat[10000]; 
	int cRes;
    int i;
     
	//swap function for nodes
	void hSwap(NodeH* A, NodeH* B)
    {
            NodeH* temp = new NodeH;

            temp->value=A->value;
			temp->sourceList=A->sourceList; 
			assig++;

            A->value = B->value; 
			A->sourceList = B->sourceList; 
			assig++;

            B->value = temp->value;
			B->sourceList = temp->sourceList;
			assig++;
    }

	void heapify(NodeH* heapN[],int i) 
    {
            int smallest = 0;

            int l = Left(i); int r = Right(i);

            comp++;
            if (l< heap_size && heapN[l]->value < heapN[i]->value)
                    smallest = l;

            else smallest = i;

            comp++;
            if (r< heap_size && (heapN[r]->value < heapN[smallest]->value))
                    smallest = r;

            if (smallest!= i)
			{
				hSwap(heapN[i], heapN[smallest]);
                heapify(heapN,smallest);
			}
    }
     
    void buildHeap(NodeH* A[], int j)
	{ 
     
            heap_size = j;
            for(i = (j-1)/2; i>=0; i--)
				heapify(A,i);
    }
   //Generate random list of length n and return head
    NodeL* randListBuild(int n)
	{
            srand (time(NULL));

            NodeL *head;
            NodeL *tempN;
            NodeL *newNode;

            if (n>0)
			{
                    head = new NodeL;
                    head->value = rand() % 15 + randomizer%9;
                    head->nextNode = NULL;
                    tempN = head;

                    if (n>1)
					{
                            for(int i=1; i<n; i++)
							{

                                    newNode = new NodeL;
                                    newNode->value = tempN->value + rand() %15 + randomizer%2;

                                    tempN->nextNode = newNode;

                                    newNode->nextNode = NULL;
                                    tempN = newNode;
                            }
                    }
            }

            else return NULL;

            return head;
    }
     
	//for transfering nodes from the list
    NodeH *nodeTransf(int k)
	{
            NodeH *tempNode = new NodeH;

            tempNode->value = listDat[k]->value;
            tempNode->sourceList = k;

            listDat[k] = listDat[k]->nextNode;

            return tempNode;
    }
     
    // build heap out of first list nodes, remove min and add to res, add next node, heapify, repeat
     
    void listmerge(NodeL* storage[], int n, int k)
	{
            int cStore = 0;
            
            for(int i=0; i<k; i++)
			{
                heapDat[cStore] = nodeTransf(i); 
				assig++;
                cStore++;
			}
     
           
            buildHeap(heapDat, cStore);
    
            while (cRes<n*k)
			{
					res[cRes] = heapDat[0]->value; 
					assig++;
                    cRes++;

					comp++;

					if(storage[heapDat[0]->sourceList] != NULL)
					{
						heapDat[0]=nodeTransf(heapDat[0]->sourceList); 
						assig++;			
					}

					else
					{
						heapDat[0] = heapDat[cStore-1]; 
						assig++;
						cStore--;
					}

                    heap_size=cStore;
                    heapify(heapDat,0);
            }
            
    }
     
    void main()
    {

		// FUNCTIONALITY TEST
  //     /*      
		int k = 4; int n = 20;
			
            for (int i=0; i<k; i++)
			{
				listDat[i]=randListBuild(n); 
				randomizer=randomizer+3; 
			}

            listmerge(listDat, n, k);


			for (int i=0; i<k; i++)
			{
				listDat[i]=NULL;
			}
			
			for (int i=0; i<k; i++)
			{
				listDat[i]=randListBuild(n); 
				randomizer=randomizer+3; 
			}

            listmerge(listDat, n, k);

			printf("\nMerged list: \n");

			for (int i=0; i<k * n; i++)
				printf("%d ", res[i]);
	//			*/
     
     /*      

		// Case 1 test for k= 5
		assig=0; 
		comp = 0;

			fileStr.open ("ress.txt",ios::app);
			fileStr << "Case 1" << endl;

			fileStr.close();

			int k1=5, k2=10, k3=100; 
			int n1=0,n2=0,n3=0;
			
			for (n1=10; n1<2000; n1=n1+80)
			{

				for (int i=0; i<k1; i++)
				{
					listDat[i]=randListBuild(n1); 
					randomizer=randomizer+3; 
				}

			 listmerge(listDat, n1, k1);

			 for (int i=0; i<k1; i++)
			 {
				 listDat[i]=NULL;
			 }

			 for (int i=0; i<n1*k1; i++)
			 {
				 res[i]=NULL;
			 }

			 cRes=0;
			 fileStr.open ("ress.txt",ios::app);

				fileStr << n1 << "," << k1 << "," << assig << "," << comp << endl;
				fileStr.close();

				assig = 0; comp = 0;

			}
			
			/////Case 2 for k=10

			fileStr.open ("ress.txt",ios::app);
			fileStr << "Case 2" << endl;
			fileStr.close();

			for (n2=10; n2<1000; n2=n2+40)
			{
			 for (int i=0; i<k2; i++)
			 {
				 listDat[i]=randListBuild(n2); 
				 randomizer=randomizer+3; 
			 }

			 listmerge(listDat, n2, k2);

			 for (int i=0; i<k2; i++)
			 {
				 listDat[i]=NULL;
			 }

			 for (int i=0; i<n2*k2; i++)
			 {
				 res[i]=NULL;
			 }
			 cRes=0;

			fileStr.open ("ress.txt",ios::app);
			fileStr << n2 << "," << k2 << "," << assig << "," << comp << endl;
			fileStr.close();

			assig = 0; comp = 0;

			}

			//// Case 3 for k=100

			fileStr.open ("ress.txt",ios::app);
			fileStr << "Case 3" << endl;
			fileStr.close();

			for (n3=10; n3<100; n3=n3+4)
			{
			 for (int i=0; i<k3; i++)
			 {
				 listDat[i]=randListBuild(n3); 
				 randomizer=randomizer+3; 
			 }

			 listmerge(listDat, n3, k3);

			 for (int i=0; i<k3; i++)
			 {
				 listDat[i]=NULL;
			 }

			 for (int i=0; i<n3*k3; i++)
			 {
				 res[i]=NULL;
			 }
			 cRes=0;

			fileStr.open ("ress.txt",ios::app);
			fileStr << n3 << "," << k3 << "," << assig << "," << comp << endl;
			fileStr.close();

			assig = 0; comp = 0;

			}

			//Final case for n=10000
			fileStr.open ("ress.txt",ios::app);
			fileStr << "Case 4" << endl;
			fileStr.close();

			int n4=10000; int k4; int resCount;
			for(k4=10; k4<500; k4=k4+10)
			{
				resCount = (int) n4/k4;

				for (int i=0; i<k4; i++)
				{
					listDat[i]=randListBuild(resCount); 
					randomizer=randomizer+3; 
				}

			 listmerge(listDat, resCount, k4);

			 for (int i=0; i<k4; i++)
			 {
				 listDat[i]=NULL;
			 }

			 for (int i=0; i<resCount*k4; i++)
			 {
				 res[i]=NULL;
			 }
			 cRes=0;

			fileStr.open ("ress.txt",ios::app);
			fileStr << n4 << "," << k4 << "," << assig << "," << comp << endl;
			fileStr.close();
			assig = 0; comp = 0;
			}

		*/
    }
