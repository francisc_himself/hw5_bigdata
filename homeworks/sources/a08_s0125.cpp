#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

ofstream myfile;
#define M 60000
#define N 10000
int callCount = 0;

struct TreeNode{
int key;
int rank;
TreeNode *p;
};

struct Vertex{
int key;
TreeNode* setElem;
};

Vertex* vertArr[N];

struct Edge{
Vertex* v1;
Vertex* v2;
};

Edge* edgeArr[M];

void makeSet(TreeNode* x)
{
callCount++;
x->p=x;
x->rank=0;
}

void link(TreeNode* x, TreeNode* y)
{
if (x->rank>y->rank)
y->p=x;
else {
x->p=y;
if (x->rank==y->rank)
y->rank=y->rank+1;
}
}

TreeNode* findSet(TreeNode* x)
{
callCount++;
if (x!=x->p)
x->p=findSet(x->p);
return x->p;

}

void setUnion(TreeNode* x, TreeNode* y)
{
callCount++;
link(findSet(x),findSet(y));
}

int sameComponent(Vertex* u, Vertex* v)
{
if(findSet(u->setElem)==findSet(v->setElem))
return 1;
else return 0;
}

void connectedComponents(int x)
{
for(int i=0; i<N; i++) //for each vertex v belongs to Graph verts
{
makeSet(vertArr[i]->setElem);
}
for(int i=0; i<x; i++) //for each edge (u,v) belongs to Graph edges
{
if(edgeArr[i]->v1!=NULL && edgeArr[i]->v2!=NULL) //if the edge is properly set
if(findSet(edgeArr[i]->v1->setElem)!=findSet(edgeArr[i]->v2->setElem)) //if they're in different sets
{
setUnion(edgeArr[i]->v1->setElem, edgeArr[i]->v2->setElem); //perform union
}
}
}

void generateGraph(int x) //generates an x-edge graph
{
int vertA;
int vertB;
//bool goOn = true;
for(int i=0; i<x; i++)
{	
//do{

vertA=rand()%N;
vertB=rand()%N;
while (vertA==vertB) vertB=rand()%N;
//	goOn = true;
//for(int j=0; j<x; j++)
//	if (edgeArr[j]->v1->key == vertA && edgeArr[j]->v2->key == vertB) goOn = false;
//} while (goOn == true);

edgeArr[i]->v1=vertArr[vertA];
edgeArr[i]->v2=vertArr[vertB];
//}
}
}

void main(){
//srand(time(NULL)); //new seed
for(int i=0; i<N; i++) //generate n vertices
{
vertArr[i]=new Vertex;
vertArr[i]->key=i;
vertArr[i]->setElem=new TreeNode;
}
for(int i=0; i<M; i++) //generate m edgeArr
{
edgeArr[i]=new Edge;
edgeArr[i]->v1=NULL;
edgeArr[i]->v2=NULL;
}

//demo - connected
/*

edgeArr[0]->v1=vertArr[0]; edgeArr[0]->v2=vertArr[1]; 
edgeArr[1]->v1=vertArr[0]; edgeArr[1]->v2=vertArr[2];
edgeArr[2]->v1=vertArr[3]; edgeArr[2]->v2=vertArr[4];
edgeArr[3]->v1=vertArr[4]; edgeArr[3]->v2=vertArr[5];

connectedComponents(4);

for(int i=0; i<4; i++)
{
printf("Edge: %d - %d\n", edgeArr[i]->v1->key, edgeArr[i]->v2->key);
}

printf("%d for 1 and 0\n", sameComponent(vertArr[0],vertArr[1]));
printf("%d for 3 and 4\n", sameComponent(vertArr[3],vertArr[4]));
printf("%d for 1 and 3\n", sameComponent(vertArr[1],vertArr[3]));
_getch();
*/

//generate csv
///*

myfile.open ("disjoint.csv",ios::out);

for (int n=10000; n<60000; n+=1000)
{
callCount=0;
for(int i=0; i<n; i++)
{
edgeArr[i]=new Edge;
edgeArr[i]->v1=NULL;
edgeArr[i]->v2=NULL;
}
generateGraph(n);
connectedComponents(n);
 myfile << n << "," << callCount << endl;
}
myfile.close();
//*/
}