
/* curba la metoda top-down creste mai repede decat la metoda bottom-up.
	1)Media atribuirilor la top-down este mult mai mare decat media atribuirilor la botton-up
	2)Media comparatiilor este relativ apropiata la cele doua metode
	3)Media sumei dintre comparatii si atribuiri la metoda top-down este mai mare decat la botton-up

*/
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
# include<iostream>
# define CRT_SECURE_NO_WARNINGS
using namespace std;
int dim_heap=0;
int bu_a=0,bu_c=0,td_a=0,td_c=0 ;



//--------------------------------------------------------------------------
void genRandom(int a[], int inc)
{
	for(int i=0;i<inc;i++)
		a[i]=rand();
}

void genRandomCresc(int a[], int inc)
{
	a[0]=1;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]+rand();
}

void genRandomDescresc(int a[], int inc)
{
	a[0]=INT_MAX;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]-rand();
}//---------------------------------------------------------------------------

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i)+1;
}
//constructia heap-ului prin tehnica bottom-up
//eficienta: o(n log n) aproximat la o(n)
void max_heapfy(int a[],int i,int n)
{
	int l,r,max,aux;
	l=left(i);
	r=right(i);
	bu_c++;
	//detectez elementul cel mai mic
	if(l<=dim_heap && a[l]<a[i])
		max=l;
	else
		max=i;
	bu_c++;
	if(r<=dim_heap && a[r]<a[max])
		max=r;
	//interschimb elementul minim cu radacina
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		bu_a+=3;
		max_heapfy(a,max,n);
	}
}

void build_max_heap(int a[],int n)
{
	dim_heap=n;
	for(int i=n/2;i>=1;i--)
		max_heapfy(a,i,n);
}


//constructia heap-ului prin insertie(Top Down)

void heap_insert(int a[],int key)
{
	int i,aux;
	dim_heap++;
	i=dim_heap;
	td_a++;
	a[dim_heap]=key;
	td_c++;
	while(i>1 && a[parent(i)]>a[i])
	{
		td_c++;
		td_a+=3;
		aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}

}
//eficienta: O(n logn)
void build_max_heap_td(int a[],int b[],int n)
{
	dim_heap=0;
	for(int i=1;i<=n;i++)
		heap_insert(b,a[i]);

}

void afisare(int *v,int n,int k,int nivel)
{
	if (k>n)
	    return;
	else
	{
	afisare(v,n,2*k+1,nivel+1);
	for(int i=1;i<nivel;i++)
		cout<<"  ";
	cout<<v[k]<<endl;
	afisare(v,n,2*k,nivel+1);
	
	}
}

void average()
{
	//srand(time(NULL));
	int a[100000],b[100000];
	FILE *f=fopen("average.csv","w");
    fprintf(f,"n,td_a/5,td_c/5,bu_a/5,bu_c/5,(td_a+td_c)/5,(bu_a+bu_c)/5\n");

	for(int n=100;n<=10000;n+=100)
	{
		
		bu_a=0;
		bu_c=0;
		td_a=0;
		td_c=0;
		for(int k=1;k<=5;k++)
		{
			for(int j=0;j<n;j++)
			{
				a[j]=rand();
			}

			build_max_heap_td(a,b,n);
			build_max_heap(a,n);
		}
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d\n",n,td_a/5,td_c/5,bu_a/5,bu_c/5,(td_a+td_c)/5,(bu_a+bu_c)/5);
		
	}fclose(f);
}

void defavorabil()
{
	//srand(time(NULL));
	int a[100000],b[100000];
	FILE *g=fopen("defav.csv","w");
    fprintf(g,"n,td_a/5,td_c/5,bu_a/5,bu_c/5,(td_a+td_c)/5,(bu_a+bu_c)/5\n");
	
	for(int n=100;n<=10000;n+=100)
	{
		
		bu_a=0;
		bu_c=0;
		td_a=0;
		td_c=0;
		for(int k=1;k<=5;k++)
		{
			genRandomCresc(a,n);

			build_max_heap_td(a,b,n);
			build_max_heap(a,n);
		}
		fprintf(g,"%d,%d,%d,%d,%d,%d,%d\n",n,td_a/5,td_c/5,bu_a/5,bu_c/5,(td_a+td_c)/5,(bu_a+bu_c)/5);
		
	}fclose(g);
}

int main()
{

	int c[11]={0,20,13,10,7,2,5,1,14,8,7};
	int a[100],b[100];
	

	printf("Constructia top-down este :\n");
	build_max_heap_td(c,b,10);
	afisare(b,10,1,1);
	printf("Constructia bottom-up este :\n");
	build_max_heap(c,10);
    afisare(c,10,1,1);
	average();
	defavorabil();
	


	
	
	
	getch();
	return 0;
}