// Tema3-Quicksort_Heapsort.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 
//n este dimensiunea max-heap-ului
int n,p;  
int a[10001];
int c[10001],b[10001],b1[10001];
int ch=0, ah=0, cq=0, aq=0;

//functie parinte
int parent(int i)
{
	return i/2;

}
//functie pentru indicele din stanga
int left(int i)
{

	return (2*i);
}
//functie pentru indicele din dreapta
int right(int i)
{
    return (2*i)+1;
}




//construire min-heap prin tehnica de bottom-up

//functia MAX_HEAPIFY este pentru intretinerea heap-ului
void MAX_HEAPIFY(int a[],int i,int n)
{
	int l=left(i);
	int r=right(i);
	int max, aux;
	ch++;
	if ((l<=n) && (a[l]>a[i]))
		max=l;
	else max=i;
	ch++;
	if ((r<=n) && (a[r]>a[max]))
		max=r;
	if (max!=i)
	{   ah++; 
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		MAX_HEAPIFY(a,max,n);
    }
	

}

//cu ajutorul functiei MIN_HEAPIFY construim heap-ul prin tehnica Bottom-up

void BUILD_MAX_HEAP(int a[], int n)
{
	int i;
	ch=0; ah=0;
    for (i=n/2; i>=1; i--)
	  	MAX_HEAPIFY(a,i,n);


}


//implementez algoritmul de heapsort
void HEAPSORT(int a[],int n)
{
	int i,aux,p;
	int x=n;
	BUILD_MAX_HEAP(a,x);
	for (i=n; i>=2; i--)
	{   ah=ah+3;
		aux=a[1];
		a[1]=a[x];
		a[x]=aux;
		x=x-1;
		MAX_HEAPIFY(a,1,x);
		
	}

}

//quicksortul
//partitionare, care returneaza pivotul
int PARTITIONARE(int a[], int st,int dr)
{
	int x=a[dr];  //this is teh pivot element around which to partiton the subarray a[st,dr]
	int i=st-1;
	int j, aux, aux1;
	for (j=st; j<dr; j++)
	{   cq++; 
		if (a[j]<=x)
		{   aq=aq+3;
			i=i+1;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	aq=aq+3;
	aux1=a[i+1];
	a[i+1]=a[dr];
	a[dr]=aux1;
	return (i+1);
}

//implementarea quicksort-ului

void QUICKSORT (int a[], int st, int dr)
{   
	int q;
	cq++;
	if (st<dr)
	{
		q=PARTITIONARE(a,st,dr);
		QUICKSORT(a,st,q-1);
		QUICKSORT(a,q+1,dr);

	}

}


int main ()
{
	int i,k,j; 
	//Verificarea functionarii algoritmilor de quicksort si heapsort
	/*printf("Dati numarul de elemente: ");
	scanf("%d",&n);
	printf("\nDati elemntele sirului: ");
	for (i=1; i<=n; i++)
		{ 
           a[i]=(n+5-i);		   
 	    }
	for (i=1; i<=n; i++)
		printf("%d ",a[i]);
	//BUILD_MAX_HEAP(a);
	printf("\n");
	//HEAPSORT(a,n);
	QUICKSORT(a,1,n);
	//printf("\nSirul dupa quicksort: ");
	for (i=1; i<=n;i++)
		printf("%d ",a[i]);*/

	//scrierea rezultatelor in tabelul Excel
	FILE *fp;
	fp=fopen("Tema3A2.csv","w");
	//scrierea capurilor de tabel in excel
	fprintf(fp,"n,Heapsort_av_at,Heapsort_av_comp,Heapsort_av_at+comp,Quicksort_av_at,Quicksort_av_comp,Quicksort_av_at+comp,Quicksort_bs_at,Quicksort_bs_comp,Quicksort_bs_at+comp,Quicksort_wc_at,Quicksort_wc_comp,Quicksort_wc_at+comp\n");
	
	//scrierea atribuirilor si comparatiilor in fisierul "Tema3.csv"
	int at1=0,at2=0,comp1=0,comp2=0;
	j=0;
	for (n=100; n<=10000; n=n+100)
	{
		fprintf(fp,"%d,",n);
		at1=0; at2=0; comp1=0; comp2=0;
		j++;
		printf("%d \n",j);
		for (int k=1; k<=5; k++)
		{
			srand(time(NULL));
			for (i=1; i<=n; i++)
			{
				a[i]=rand()%1000;
				c[i]=a[i];
            }
			HEAPSORT(a,n);
			comp1=comp1+ch;
			at1=at1+ah;

			QUICKSORT(c,1,n);
			comp2=comp2+cq;
			at2=at2+aq;



		}
		fprintf(fp,"%d,%d,%d,%d,%d,%d,",at1/5,comp1/5,(at1+comp1)/5, at2/5,comp2/5, (at2+comp2)/5);
		//pt Quicsort BestCase
	  /*   at2=0;  comp2=0;
		for (i=1; i<=n; i++)
			b[i]=i;
		QUICKSORT(b,1,n);
			comp2=comp2+cq;
			at2=at2+aq;
        fprintf(fp,"%d,%d,%d,",at2,comp2,(at2+comp2));   

		//pt WorstCase
		at2=0; comp2=0;
		int y=1;
		for (i=n; i>=1; i--)
		   {	b1[y]=i;
		         y++;
		    }
		QUICKSORT(b1,1,n);
			comp2=comp2+cq;
			at2=at2+aq;
        fprintf(fp,"%d,%d,%d",at2,comp2,(at2+comp2)); */
		fprintf(fp,"\n");

   }
	fclose(fp);
	

	getch();
	return 0;
}






	