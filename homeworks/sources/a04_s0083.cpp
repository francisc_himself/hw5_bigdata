/*Student:					***ANONIM*** ***ANONIM***-***ANONIM***
	Grupa:						30222
	Cerinte:					Se cere implementarea corecta si eficienta a operatiilor de adaugare si cautare in tabela de dispersie cu adresare deschisa si verificare patratica.
	Cerinte speciale:			*folosirea unei tabele de dispersie cu adreasare deschisa
	                            *se va folosi o functie de HASH-ing de forma h(k,i)=(h'(k)+c1*i+c2*i*i) mod m unde c1,c2 sunt constante auxiliare
	                            *programul va fi testat pentru factori de umplere 80%,85%,90%,95%,99%
	                            *se vor cauta 1500 elemente exitente in tabela si 1500 elemente ce nu exista in tabela; si se va retine numarul de operatii pentru fiecare caz
                                *datele rezultate se vor afisa intr-un tabel cu urmatoarele coloane valoarea |factorului de umplere|Efort mediu cautare elemente gasite|Efort maxim gasite|Efort mediu cautare negasite|Efort maxim negasite|
                                *5 rulari pentru fiecare valoare a factorului de umplere, se raporteaza media celor 5 rulari.

	Concluzii, interpretari:	. :)
	                             *pentru gasirea elementelor existente in tabela numarul de operatii este mic si aproape constant indiferent de dimensiunea tabelei, in medie 1
								 *in cazul cautarii unui elemnt inexistent in tabela numarul de operatii este relativ mic in comparatie cu dimensiunea tabelei
								 *numarul de operatii creste in functie de factorul de umplere(lucru care se vede clar la cautarea elementelor ce nu exista in tabela si mai putin vizibil la cautarea elemntelor existente in tabela)
                                 *valorile relativ mici se datoreaza constructiei functiei de HASH-ing

*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>
#define N 10001

int T[N];
int nr_OPgasite,nr_OPnegasite,nr_OPgasite_m,nr_OPnegasite_m,a,b;

int h_2(int k)    // functia de hash auxiliara
{
  int x,y;
  double z,A;

  A=(sqrt(5.0)-1)/2;
  x=(int)(k*A);
  z=k*A-x;
  y=(int)(N*z);
  return y;
}

int h(int k,int i)   //functia de hash principala
{
  int x,c1=3,c2=5;

  x=(h_2(k)+c1*i+c2*i*i)%N;
  return x;
}

int Hash_Insert(int *T, int k)   ///functia de inserare
{
   int i=0,j;
   do{
       j=h(k,i);
       if(T[j]==NULL)
        {
           T[j]=k;
	       return j;
        }
       else i++;

   }while(i!=N);

return -1;
}

int Hash_Search(int *T,int k)// functia de cautare
{
  int i=0,j,op=0;

   do{
      j=h(k,i);
      op++;
       if(T[j]==k)
        {
	     nr_OPgasite=op; //daca numarul a fost gasit atunci va retuna numarul de operatii efectuate pana la gasirea acestuia
	     return j;
        }
        else i++;
    op++;
   }while((T[j]!=NULL)&&(i!=N));

nr_OPnegasite=op;// daca numarul nu a fost gasit atunci va returna numarul total de opartii efectuate
return -1;
  }

void main()
{
   FILE *pf;
	int i,j,n,k,l,max1,max2,maximum=0,w,xyz;
	double factori_de_umplere[5]={0.8,0.85,0.9,0.95,0.99};//valorile facorilor de umplere pentru care se va testa programul

 pf=fopen("tabel.txt","w");

for(i=0;i<5;i++)
{
     n=factori_de_umplere[i]*N;//n=numarul de elemente ce vor fi introduse in tabela
      a=0;// a & b contribuie la mediere
	  b=0;
	  max1=0;// retine cel mai mare numar de operatii care s-au efectuat pentru gasirea elementului existent in tabela
	  max2=0;// retine cel mai mare numar de operatii care s-au efectuat pentru cautarea  elementului neexistent in tabela

	  for(l=0;l<5;l++)//pentru fiecare valoarea a factorului de umplere se testeaza 5 cazuri diferite (mediere)
    {
	  nr_OPgasite_m=0;
      w=0;

	  for(xyz=0;xyz<N;xyz++)  T[xyz]=NULL;//reinitializarea tabelei

         srand(l);//contribuie la rularea 5 cazuri de test diferite, oferind numere aleatoare diferite pentru fiecare caz de test; contribuie la mediere
	     for(j=0;j<n;j++)
	     {
            k=rand();                     //"umplerea" tabelei cu numere aleatoare in functie de factorul de umplere
			if(k>maximum)  maximum=k;    //contribuie la genereara celor 1500 de numere ce nu exista in tabela
	        Hash_Insert(T,k);
	      }

         srand(l);
		 for(j=0;j<1500;j++)  //for 1  cautare elemente existente
		  {
             nr_OPgasite=0;

			 k=rand();
		     Hash_Search(T,k);

			 if(nr_OPgasite>max1) max1=nr_OPnegasite; //gasirea numraului maxim de operatii efectuate pentru gasirea elemetului

			 nr_OPgasite_m=nr_OPgasite_m+nr_OPgasite;

		  }
		     a=a+nr_OPgasite_m/1500;//nr_OPgasite_m/1500= numarul mediu de operatii pentru gasirea celor 1500 de elemente in tabela

		  srand(l);
		  for(j=0;j<1500;j++)   //for 2 cautare elemente inexistente
		  {
              nr_OPnegasite_m=0;
			  k=rand()+maximum;
		      Hash_Search(T,k);

			  w=w+nr_OPnegasite;

			  if(nr_OPnegasite>max2) max2=nr_OPnegasite;

		  }
		  b=b+w/1500;

  }

    fprintf(pf,"\n %1.2f \t %d \t %d \t%d \t%d ",factori_de_umplere[i],a/5,max1,b/5,max2);
}

fclose(pf);



}

