#include <stdio.h>
#include <stdlib.h>
#include<time.h>

/** ***ANONIM*** ***ANONIM***, ***GROUP_NUMBER*** 
	Using 2 data structures, node and edge, the procedure becomes very easy. The edge uses the nodes as endpoints.
	The small test applies the connected components test, then checks with the method sameComponent() if two nodes
	are in the same component. Prints true if they are, false otherwise. 
	Method link is a helper method for the doUnion(). This method sets as parent of the node with the smaller rank
	the node with the higher rank. If their rank is equal, then one of them is set as parent and its rank is increased.
	The heuristic union by rank is used for the doUnion() method.
	Method findSet is recursive and makes each node point to the root, without changing any rank. The heuristic called
	path compression is used here.
	Using the 2 heuristics, union by rank and path compression, the performance of the algorithm is improved. 
**/

struct Node {
	int info;
	int rank;
	Node *p;
};

struct Edge {
	Node* start;
	Node* end;
};

int calls;

void makeSet(Node *root){
	calls++;
	root->p = root;
	root->rank = 0;
}

Node* findSet(Node *x) {
	calls++;
	if(x != x->p)
		x->p = findSet(x->p);
	return x->p;
}

void link(Node* x, Node* y){
	if(x->rank > y->rank)
		y->p = x;
	else {
		x->p = y;
		if(x->rank == y->rank)
			y->rank = y->rank + 1;
	}
}

void doUnion(Node* x, Node* y){
	calls++;
	link(findSet(x), findSet(y));
}

bool sameComponent(Node *x, Node* y){
	if(findSet(x) == findSet(y))
		return true;
	return false;
}

void main(){
	/*SMALL TEST*/
	/**
	Node* nodes[6];
	Edge edges[3];
	for(int i=0;i<6;i++){
		nodes[i] = (Node*) malloc (sizeof(Node));
		makeSet(nodes[i]);
		nodes[i]->info = i+1;
	}
	edges[0].end = nodes[0];
	edges[0].start = nodes[1];
	edges[1].end = nodes[0];
	edges[1].start = nodes[2];
	edges[2].end = nodes[3];
	edges[2].start = nodes[4];

	//apply algoritm for connectedness
	for(int i = 0; i<3;i++)
		if(findSet(edges[i].end) != findSet (edges[i].start))
			doUnion(edges[i].end, edges[i].start);
	bool res = sameComponent(nodes[0], nodes[4]);
	if(res==true) printf("true");
	else if(res==false)
			printf("false");**/
	

	//initial set up
	FILE *disjointSets;
	disjointSets = fopen("disjointSets.csv", "w");
	fprintf(disjointSets,"nbEdges, calls\n");
	int nbVertices = 10000, i, j, k, initCalls = 0;
	Node *nodes[10000];
	Edge edges[60000];
	srand(time(0));

	//assignment loop; for each nb of edges
	for(j = 10000; j<=60000; j += 100){
		calls = 0;
		//create & initialize vertices;
		for(i = 0; i<nbVertices; i++){
			nodes[i] = (Node*) malloc(sizeof(Node));
			makeSet(nodes[i]);
			nodes[i]->info = i+1;
		}
		//generate random graphs by randomizing edges:
		for(k = 0; k<j; k++){
			edges[k].start = nodes[rand()%nbVertices];
			edges[k].end = nodes[rand()%nbVertices];
		}
		//then apply connected-components algorithm
		for(k = 0; k<j; k++){
			if(findSet(edges[k].end) != findSet(edges[k].start))
				doUnion(edges[k].start, edges[k].end);
		}
		fprintf(disjointSets,"%lu, %lu\n", j, calls);
	}
	fclose(disjointSets);
	printf("ready");
	getchar();
}
