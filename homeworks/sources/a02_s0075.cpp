/****ANONIM*** ***ANONIM*** Mihai, Grupa ***GROUP_NUMBER***
In aceasta lucrare de laborator am studiat structurile de tip heap. In urma efectuarii tuturor operatiilor si crearii graficelor, am observat ca
din toate punctele de vedere (atribuiri,comparatii,atribuiri+comparatii), cele doua metode folosesc un numar apropiat de operatii, insa metoda up
este putin mai avantajoasa.
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int nra=0,nrc=0,nra1=0,nrc1=0,sir1[10000],sir2[10000];

//------------------------------------Bottom-Up-----------------------------------
void heapify(int A[],int i,int hp_size) {
int l,r,max,aux;
	l=2*i+1;
	r=2*i+2;
	nrc+=1;
	if (l<=hp_size && A[l]>A[i]) 
	{
		max=l;
	} else {
		max=i;
	}
	nrc+=1;
	if (r<=hp_size && A[r]>A[max]) 
	{
		max=r;
	}
	nrc+=1;
	if (max!=i) {
		aux=A[i];
		A[i]=A[max];
		A[max]=aux;
		nra+=3;
		heapify(A,max,hp_size);
	}

}
void h_push_up(int A[],int t,int *size)
{
	int i;
	(*size)+=1;
	i=(*size);
	
	while ((i>0) && (A[(int) (i-1)/2]<t))
	{
		nrc++;
		nra++;
		A[i]=A[(int) (i-1)/2];
		i=(int) (i-1)/2;
	}
	A[i]=t;
	nra++;
}


void constrUp(int A[],int B[],int n,int *size) 
{
	(*size)=-1;
	for (int i=0;i<n;i++) 
	{
		h_push_up(B,A[i],size);
	}
}

void heaplyUp(int A[],int B[],int n){
int aux,heapSize;
	constrUp(A,B,n,&heapSize);
	for (int i=n-1;i>=1;i--) {
		aux=B[0];
		B[0]=B[i];
		B[i]=aux;
		nra+=3;
		heapSize--;
		heapify(B,0,heapSize);
	}
}

//-------------------------TOP-DOWN------------------------//
void heapifydn(int A[],int i,int hp_size) {
int l,r,max,aux;
	l=2*i+1;
	r=2*i+2;
	nrc1+=1;
	if (l<=hp_size && A[l]>A[i]) 
	{
		max=l;
	} else {
		max=i;
	}
	nrc1+=1;
	if (r<=hp_size && A[r]>A[max]) 
	{
		max=r;
	}
	nrc1+=1;
	if (max!=i) {
		aux=A[i];
		A[i]=A[max];
		A[max]=aux;
		nra1+=3;
		heapifydn(A,max,hp_size);
	}

}


void constrDown(int A[],int n,int *size) {
int heapSize;

	(*size)=n-1;
	heapSize=(*size);
	for (int i=n/2;i>=0;i--) 
	{
		heapifydn(A,i,heapSize);
	}
}

void heaplyDown(int A[],int n){
int aux,heapSize;
	constrDown(A,n,&heapSize);
	for (int i=n-1;i>=1;i--) {
		aux=A[0];
		A[0]=A[i];
		A[i]=aux;
		nra1+=3;
		heapSize--;
		heapifydn(A,0,heapSize);
	}
}
void random(int a[],int n){
	for(int i=0;i<n;i++)
		a[i]=rand();
}
int main()
{
	FILE *pf,*pf2;
	pf=fopen("LabHeap.csv","w");
	fprintf(pf,"n,atribuiriUp,comparatiiUp,atribuiri+comparatiiUp,atribuiriDown,comparatiiDown,atribuiri+comparatiiDown\n");
	pf2=fopen("f1.txt","w");
	random(sir1,10000);
	//generare 5 fisiere cu numere aleatoare
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f2.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f3.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f4.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f5.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
    fclose(pf2);


	for(int k=100;k<=10000;k+=100)
	{
		nra=0;nrc=0;nra1=0;nrc1=0;
			for (int j=1;j<=5;j++) 
			{
				switch (j) 
				{
					case(1): pf2=fopen("f1.txt","r"); break;
					case(2): pf2=fopen("f2.txt","r"); break;
					case(3): pf2=fopen("f3.txt","r"); break;
					case(4): pf2=fopen("f4.txt","r"); break;
					case(5): pf2=fopen("f5.txt","r"); break;
					default: break;
			}
			if(pf2)
			{
				for(int i=0;i<k;i++)
				fscanf_s(pf2,"%d",&sir1[i]);
			}
		
			heaplyUp(sir1,sir2,k);
			heaplyDown(sir1,k);
			fclose(pf2);
			printf("%d%c\r",k/100,'%');
		}
		fprintf(pf,"%d,%d,%d,%d,%d,%d,%d\n",k,nra/5,nrc/5,(nra+nrc)/5,nra1/5,nrc1/5,(nra1+nrc1)/5);
	}
	fclose(pf);
	pf=fopen("LabHeap2.csv","w");
	
	return 0;
}
		