// ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***

#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<fstream>
#include<iostream>

using namespace std;

int n,m;
int suma=0;
int di;
int x;
int y;
int joselist[10],cnt=0;
typedef struct nod
{
    int key;
    int dimensiune;
    nod *left, *right, *parinte;
} NOD;
NOD* rad;



NOD* OS_SELECT(NOD *x, int i)
{
    int r=1;
	suma++; 
	suma++; 
    if (x->left != NULL)							//se cauta nodul ce trebuie eliminat
    {    r = x->left->dimensiune+1;					//pe ramurile stanga si dreapta
		  suma++;
	}else
        {
			r = 1;
			 suma++;
	}
	 suma++;
    if (r == i)
        return x;
    else
    {	 suma++;
        if (i < r)
        {
            return OS_SELECT(x->left,i);
        }
        else
        {
            return OS_SELECT(x->right,i-r);
        }
    }
}

NOD* GENERARE(int x, int dim, NOD *stg, NOD *dr)
{
    NOD *q;
    q= new NOD;
	suma+=4;
    q->key=x;					//q este noul nod si primeste valoare x
    q->left=stg;				//se creeaza fiu stang
    q->right=dr;				//se creeaza fiu dreapta
    q->dimensiune=dim;			//se adauga dimensiunea
	suma++;
    if (stg!=NULL)
    {suma++;
        stg->parinte = q;		//se fac legaturile parinte fiu
    }suma++;
    if (dr!=NULL)
    {suma++;
        dr->parinte=q;
    }suma++;
    q->parinte=NULL;			//q este radacina si nu are parinte
    return q;
}

NOD* CONSTRUCTIE(int stg, int dr)
{
    NOD *ds,*d;
	suma++;
    if (stg > dr)
        return NULL;
	suma++;
    if (stg == dr)
        return GENERARE(stg, 1, NULL, NULL);		//se genereaza arborele pentru partea stanga
    else
    {	suma++;
        int mij =(stg + dr)/2;
		suma++;
        ds = CONSTRUCTIE(stg, mij-1);
		suma++;
        d  = CONSTRUCTIE(mij+1, dr);
        return GENERARE(mij, dr-stg+1,ds, d);
    }
}


NOD* MINIM(NOD* x)
{
    while(x->left!=NULL)
    {suma++;
	suma++;
        x = x->left;

    }
    return x;						//se returneaza nodul din stanga cel mai jos
}

NOD* SUCCESOR(NOD* x)
{
    NOD *y;
    if(x->right!=NULL)						//se cauta succesorul in dreapta
    {suma++;
        return MINIM(x->right);				//daca mai exista in dreapta fii se cauta minimul cel mai de jos
    }
	suma++;
    y = x->parinte;							//y primeste parintele nodului daca nu se intra in cautarea de mai sus
    while (y != NULL && x == y->right)		//daca y nu este nul si x este egal cu fiul dreapta al lui y
    {suma+=4;
        								//x devine y
        x = y;								//se trece la urmatorul urcand in sus
        y = y->parinte;
    }
    return y;								//se returneaza succesorul
}

NOD* STERGERE(NOD* z)
{
    NOD *y;
    NOD *x;

    if ((z->left == NULL) || z->right == NULL)		//se verifica daca nodul este frunza
    {suma++;
	suma++;
	suma++;

        y=z;
    }
    else
	{
        y = SUCCESOR(z);							//daca nu se cauta succesorul lui
		suma++;
	}suma++;
    if (y->left != NULL)							//daca succesorul are fiu stanga
    {suma++;
        x = y->left;
    }
    else
    {
            suma++;                                //succesorul are fiu dreapta
        x = y->right;
    }suma++;
    if (x != NULL)
    {suma++;
        x->parinte = y->parinte;
    }suma++;
    if (y->parinte == NULL)
      {
		  rad = x;
		suma++;
	}
	else if (y == y->parinte->left)
    {suma++;
        y->parinte->left = x;
    }
    else
    {suma++;
        y->parinte->right = x;
    }
	suma++;
    if (y != z)
    {
		suma++;
        z->key = y->key;
    }
    return y;
}

void DIMENSIUNE_NOUA(NOD *p)
{
    while (p != NULL)
    {
		suma++;
		suma++;
		suma++;
        p->dimensiune--;			//pentru fiecare nod se scade dimensiunea cu 1 incepand cu cel
        //ce sa pus pe pozitia celui eliminat sau pe pozitia noua
        p=p->parinte;

    }
}


void JOSEPHUS(int n, int m)
{
    NOD *y,*z;
    int aux = n+1;
    int mij = m;
	suma++;
	suma++;
	suma++;

    rad = CONSTRUCTIE(1, n);		//se construieste arborele pentru fiecare lungime 1,n
  
    for (int i = 1; i < aux; i++)	//se parcurge de fiecare data pana la aux care va
                                    //scadea de fiecare data cand se elimina un element
    {
        y = OS_SELECT(rad, m);	    //se selecteaza nodul pentru eliminare
		 suma++;
		 suma++;
        joselist[cnt] = y->key;
		 
        cnt++;
      
        z = STERGERE(y);			//se elimina nodul
    
        suma++;
		
        DIMENSIUNE_NOUA(z);			//se reface dimensiunea tuturor nodurilor incepand de la
                                    //cel eliminat
        delete(z);					//se elibereaza memoria
        suma++;
		n--;						//decrementam lungimea arborelui
         
		if (n > 0)					//se genereaza urmatoarea pozitie de eliminat
		{suma++;
			m = (m - 1 + mij) % n;
			 
		}
        if (m == 0)
		{suma++;
            m = n;
			 
		}
	}

}



int main()
{
  //  JOSEPHUS(7,3);
	di=300;
	ofstream f("test.csv");
	cout<<"#################################"<<endl;
	cout<<"##           Josephson         ##"<<endl;
	cout<<"#################################"<<endl;
	cout<<endl;
	f<<"n"<<","<<","<<"suma"<<endl;

	for(x=50;x<=di;x+=50)
	{	cout<<"-> "<<x<< " ";
		suma=0;
		y=x/2;
		JOSEPHUS(x,y);
		f<<x<<","<<y<<","<<suma<<endl;
		cout<<"done."<<endl;

	}
	cout<<endl;
	cout<<"#################################"<<endl;
	cout<<"##       Josephson done.       ##"<<endl;
	cout<<"#################################"<<endl;
	_getch();
}
