#include <stdio.h>
#include <conio.h>
#include <stdlib.h>



typedef struct multiw {
    int key;
	int nrChild;
    int* child;
} MultiNode;

typedef struct binary {
	int key;
	struct binary *first,*brother;
	}BinaryNode;

MultiNode *temp[20];
BinaryNode *bn[20];
int a[10];
int k=0;


void parentToMultiway(int vector[],int n)
{
    int x=0;
	for (int i=0; i<n; i++)
		{
		temp[i]=(MultiNode *)malloc(sizeof(MultiNode));
		temp[i]->key=i+1;
		temp[i]->child=(int *)malloc(sizeof(int));
		temp[i]->child[0]=-1;
		temp[i]->nrChild=0;
		}

	for (int i=0; i<n; i++)
		{
		    x=vector[i]-1;
			if (x>=0)
			{
			temp[x]->child[temp[x]->nrChild]=i+1;
			temp[x]->nrChild++;
			a[k]=temp[x]->key;
			k++;
			}
		}
    for (int i=0; i<k;i++)
        {
            temp[i]->child[temp[i]->nrChild]=-1;
        }
    for (int i=0; i<n; i++)
        {
            printf("%d ---->",temp[i]->key);
            for (int j=0; j<temp[i]->nrChild;j++ )
                printf("%d ",temp[i]->child[j]);
            printf("\n");
        }
        printf("\n");
 //   for (int i=0; i<k; i++)
      //  printf("%d ",a[i]);

};


void multiwayToBinary(int n)
{
    int b[10];
    int x;
    for (int i=0; i<10; i++)
        b[i]=0;
    for (int i=0; i<n; i++)
    {
        bn[i]=(BinaryNode *)malloc(sizeof(BinaryNode));
        bn[i]->key=i+1;
        bn[i]->brother=(BinaryNode *)malloc(sizeof(BinaryNode));
        bn[i]->brother->key=-1;
        bn[i]->first=(BinaryNode *)malloc(sizeof(BinaryNode));
        bn[i]->first->key=temp[i]->child[0];
    }
    printf("\n");
    for (int i=0; i<k; i++)
    {
         x=b[a[i]-1];

         bn[temp[a[i]-1]->child[x]-1]->brother->key=temp[a[i]-1]->child[x+1];
        // printf("%d ", bn[temp[a[i]-1]->child[x]-1]->brother->key);
         b[a[i]-1]++;
    }
    printf("\n");

    for (int i=0; i<n; i++)
    {
        printf("%2d   %2d   %2d\n",bn[i]->key,bn[i]->first->key,bn[i]->brother->key);
    }


}


int nivel=0;

void prettyPrint(int root)
{

    for (int i=0; i<nivel; i++)
        printf(" ");
    nivel++;
    printf("%d\n",bn[root]->key);

    if (bn[root]->first->key!=-1)
        prettyPrint(bn[root]->first->key-1);
    else nivel--;

    if (bn[root]->brother->key!=-1)
        prettyPrint(bn[root]->brother->key-1);
    else nivel--;

}




void afis(int a[],int n)
{
	for (int i=0; i<n; i++)
		printf("%d ",a[i]);

};


int main()
{
    int root=-1;
	int vector[9];
	vector[0]=2;
	vector[1]=7;
	vector[2]=5;
	vector[3]=2;
	vector[4]=7;
	vector[5]=7;
	vector[6]=-1;
	vector[7]=5;
	vector[8]=2;

	printf("\nParent:\n");
	afis(vector,9);

	printf("\n\n\n");
	printf("Parent to multiway:\n");
	parentToMultiway(vector,9);

	printf("\nMultiway to binary:");
	multiwayToBinary(9);

    for (int i=0; i<9; i++)
        if (bn[i]->brother->key==-1 && bn[i]->first->key!=-1)
            root=i;

	printf("\n\n");
	prettyPrint(root);



	getch();
	return 0;
}
