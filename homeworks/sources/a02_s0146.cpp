// ***ANONIM*** ***ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER***
//** Pentru Grafic:
//Ca date de intrare avem un vector cu valori aleatoare. Graficul este o functie lin***ANONIM***a.
// Pentru TOP-DOWN valorile sunt mai mari decat pentru BOTTOM-UP

//** Pentru afisare (pretty_print):
// Am dat de la tastatura ce doi vectori. Rezultatul, adica arborele, este afisat in ambele cazuri.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"
#define MAX_SIZE 10000

Profiler profiler("Lab3");

int i,m,n,l,r,largest,k,nivel,aux,max;

void max_heapify(int *a,int i) 
{
	l=2*i;//left i
	r=2*i+1;//right i
	profiler.countOperation("bu",n,2);
	if((l<=m)&&(a[l]>a[i]))
	{
		largest=l;
		profiler.countOperation("bu",n);
	}
	else
	{
		largest=i;
		profiler.countOperation("bu",n);
	}
	if((r<=m)&&(a[r]>a[largest]))
	{
		largest=r;
		profiler.countOperation("bu",n);
	}
	if(largest!=i)
	{
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		profiler.countOperation("bu",n,3);
		max_heapify(a,largest);
	}
}


//***** BOTTOM UP *******
void build_max_heap(int *a) 
{
	m=n;
	for(i=n/2;i>0;i--)
	{
		profiler.countOperation("bu",n);
		max_heapify(a,i);
	}
}

//-------------------------------------
/*int heap_extract_max(int *a2)
{
	if (a2[m]<1)
		printf("heap underflow");
	max=a2[1];
	a2[1]=a2[m];
	m=m-1;
	max_heapify(a2,1);
	return max;
} */ 

void heap_increase_key(int *a2,int i, int key)
{
	if(key<a2[i])
		printf("new key is smaller than current key");
	a2[i]=key;
	profiler.countOperation("td",n);
	while ((i>1) && (a2[i/2]<a2[i]))
	{
		aux=a2[i];
		a2[i]=a2[i/2]; //i/2=parent(i)
		a2[i/2]=aux;
		i=i/2;
		profiler.countOperation("td",n,4);
	}
}

void max_heap_insert(int *a2,int key)
{
	m=m+1;
	a2[m]=-INT_MAX;
	profiler.countOperation("td",n,2);
	heap_increase_key(a2,m,key);
}

//***** TOP DOWN *****
void build_max_heap2(int *a2)
{
	m=1;
	for(i=2;i<=n;i++)
	{
		profiler.countOperation("td",n);
		max_heap_insert(a2,a2[i]);
	}
}

//Asta am facut in plus ora trecuta.
/*void heapsort(int *a)
{
	build_max_heap(a);
	for(i=n;i>0;i--)
	{
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		m=m-1;
		max_heapify(a,1);
	}
}
*/

void afisare(int *v,int n,int k,int nivel)
{
	if(k>n)
		return;
	afisare(v,n,2*k+1,nivel+1);
	for(i=1;i<=nivel;i++)
		printf("   ");
	printf("%d \n",v[k]);
	afisare(v,n,2*k,nivel+1);
}

void main()
{
	 //n este lungimea vectorului, ***ANONIM*** m este lungimea heap`ului

	/*int a[MAX_SIZE];
	int a2[MAX_SIZE];
	int v[MAX_SIZE];
	
	

	for (n=100;n<10000;n=n+100)
  {
 
	FillRandomArray(v, n, 0, 10000, true, 1);
 
	memcpy(a,v,n*sizeof(int));
	memcpy(a2,v,n*sizeof(int));

	m=0;
	build_max_heap(a);//=BU
	

	m=0;
	build_max_heap2(a2);//TD
	

	}
	
	profiler.createGroup("rezultat","bu","td");*/
	
	//heapsort(a);

//********************************************88
	//pentru un vector dat:

	int a[20]={0,2,12,1,4,5,6,10};
	int a2[20]={0,2,12,1,4,5,6,10};

	n=7;

	printf("BU: \n");
	m=0;
	build_max_heap(a);
	afisare(a,n,1,0);

	printf("\n TD: \n");
	m=0;
	build_max_heap2(a2);
	afisare(a2,n,1,0);

		
	profiler.showReport();

}