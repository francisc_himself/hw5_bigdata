#include <stdio.h>
#include "Profiler.h"
Profiler p("sort");

int n;
int i;
int j;
int arr_b[10000];
int arr_s[10001];
int arr_i[10000];

void bubble_sort( int arr[], int n){
	p.countOperation("BubbleSortAssignments", n, 0);
	p.countOperation("BubbleSortComparison", n, 0);
	bool swapped = true;
	int j = 0;
	int tmp;
	while (swapped) {
		swapped = false;
		j++;
		for (int i = 0; i < n - j; i++) {
			p.countOperation("BubbleSortComparison", n);
			if (arr[i] > arr[i + 1]) {
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				swapped = true;
				p.countOperation("BubbleSortAssignments", n, 3);
			}
		}
	}
}

void insertion_sort( int arr[], int n){
	p.countOperation("InsertionSortComparison", n, 0);
	p.countOperation("InsertionSortAssignments", n, 0);
	int i, j, tmp;
	for (i = 1; i < n; i++) {
		j = i;
		while (j > 0 && arr[j - 1] > arr[j]) {
			p.countOperation("InsertionSortComparison", n);
			tmp = arr[j];
			arr[j] = arr[j - 1];
			arr[j - 1] = tmp;
			j--;
			p.countOperation("InsertionSortAssignments", n, 3);
		}
		p.countOperation("InsertionSortComparison", n);
	}
}

void selection_sort( int arr[], int n){
	p.countOperation("SelectionSortComparisons",n, 0);
	p.countOperation("SelectionSortAssignments", n, 0);
	int i, j, minIndex, tmp;   
	for (i = 0; i < n - 1; i++) {
		minIndex = i;
		for (j = i + 1; j < n; j++){
			p.countOperation("SelectionSortComparisons",n);
			if (arr[j] < arr[minIndex]){
				minIndex = j;
			}
		}
		if (minIndex != i) {
			tmp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = tmp;
			p.countOperation("SelectionSortAssignments", n, 3);
		}
	}
}


void best_case(){
	for(n=100; n<10000; n+=100){
		FillR***ANONIM***omArray(arr_b, n, 10, 50000, false, 1);
		for(i=0; i<n; i++){
			arr_s[i] = arr_b[i];
			arr_i[i] = arr_b[i];
		}
		selection_sort(arr_s, n);
		bubble_sort(arr_b, n);
		insertion_sort(arr_i, n);
	}
	

	p.addSeries("BubbleSortTotal", "BubbleSortComparison", "BubbleSortAssignments");
	p.addSeries("InsertionSortTotal", "InsertionSortComparison", "InsertionSortAssignments");
	p.addSeries("SelectionSortTotal", "SelectionSortComparisons", "SelectionSortAssignments");

	p.createGroup("BestCaseComparisonOperations", "BubbleSortComparison", "InsertionSortComparison", "SelectionSortComparisons");
	p.createGroup("BestCaseAssignmentOperations", "BubbleSortAssignments", "InsertionSortAssignments", "SelectionSortAssignments");
	p.createGroup("BestCaseTotalOperations", "BubbleSortTotal", "InsertionSortTotal", "SelectionSortTotal");

	p.showReport();
}

void average_case(){
	for(int i=0; i<5; i++){
		for(n=100; n<10000; n+=100){
			FillR***ANONIM***omArray(arr_b, n, 10, 50000, false, 0);
			for(j=0; j<n; j++){
				arr_s[j] = arr_b[j];
				arr_i[j] = arr_b[j];
			}
			selection_sort(arr_s, n);
			bubble_sort(arr_b, n);
			insertion_sort(arr_i, n);
		}
		printf("%d", i);
	}

	p.addSeries("BubbleSortTotal", "BubbleSortComparison", "BubbleSortAssignments");
	p.addSeries("InsertionSortTotal", "InsertionSortComparison", "InsertionSortAssignments");
	p.addSeries("SelectionSortTotal", "SelectionSortComparisons", "SelectionSortAssignments");

	p.createGroup("AverageCaseComparisonOperations", "BubbleSortComparison", "InsertionSortComparison", "SelectionSortComparisons");
	p.createGroup("AverageCaseAssignmentOperations", "BubbleSortAssignments", "InsertionSortAssignments", "SelectionSortAssignments");
	p.createGroup("AverageCaseTotalOperations", "BubbleSortTotal", "InsertionSortTotal", "SelectionSortTotal");

	p.showReport();
}

void worst_case(){

	int i;
	for(n=100; n<10000; n+=100){
		FillR***ANONIM***omArray(arr_b, n, 10, 50000, false, 2);
		for(int i=0; i<n-1; i++){
			arr_i[i] = arr_b[i];
			arr_s[i] = arr_b[n-i-1];
		}
		arr_i[n-1] = arr_b[n-1];
		arr_s[n-1] = arr_b[n-1];

		selection_sort(arr_s, n);
		bubble_sort(arr_b, n);
		insertion_sort(arr_i, n);
	}

	p.addSeries("BubbleSortTotal", "BubbleSortComparison", "BubbleSortAssignments");
	p.addSeries("InsertionSortTotal", "InsertionSortComparison", "InsertionSortAssignments");
	p.addSeries("SelectionSortTotal", "SelectionSortComparisons", "SelectionSortAssignments");

	p.createGroup("WorstCaseComparisonOperations", "BubbleSortComparison", "InsertionSortComparison", "SelectionSortComparisons");
	p.createGroup("WorstCaseAssignmentOperations", "BubbleSortAssignments", "InsertionSortAssignments", "SelectionSortAssignments");
	p.createGroup("WorstCaseTotalOperations", "BubbleSortTotal", "InsertionSortTotal", "SelectionSortTotal");

	p.showReport();

}

void main(){

//	best_case();
//	average_case();
//	worst_case();
	
}

/*
At the best case, the comparisons number is determinant, because the assignments number is null.
The selection sort  is much worse than the other sorts, that are quite equal.

At the average case, the comparisons number is smaller at insertin sort ***ANONIM*** the bubble sort seams to be a litter better than the selection sort.
The assignmets number is much smaller at the selection sort compared to the other sorts, that have the same number of assignments.
When we compute the total, the selection sort is the best, fallowed by the insertion sort ***ANONIM*** then the bubble sort.

At the worst case, the comparison number is almoast equal to the 3 sorting algorithms.
The assignments number, witch is determinant, is much smaller at the selection sort, ***ANONIM*** the other 2 sorting algorithms have quite equal number.
Over all, the best algorithm at worst case is selection sort.

In conclusion, althow worst at the best case, the selection sort seams to be the best sorting algorithm form the compared ones.
*/