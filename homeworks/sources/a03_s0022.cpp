#include "stdlib.h"
#include "conio.h"
#include "stdio.h"
#include "math.h"
#include "time.h"

int A[10001], B[10001], C[10001];
int dimA,heapA;

long nQ;
long nH;
int nh;

void afis(int A[],int n)
{
	int i;
	for(i=1;i<n; i++)
		printf("%d ",A[i]);
	printf("\n");
}

void maxHeapify(int A[],int i){
    int largest;
    int stg = 2*i;
    int drp = 2*i+1;

	nH++;
	nh++;
    if((stg<=heapA) && (A[stg]>A[i])){
        largest = stg;
    }else{
        largest = i;
    }
    nH++;
	nh++;
    if((drp<=heapA) && (A[drp]>A[largest])){
        largest = drp;
    }

    if(largest != i){
        int aux = A[i];
        A[i] = A[largest];
        A[largest] = aux;
        nH+=3;
        nh+=3;
		maxHeapify(A,largest);
    }
}

void buildMaxHeap(int A[]){
    int i;
    for(i=dimA/2;i>=1;i--){
        maxHeapify(A,i);
    }
}

void heapSort(int A[]){
    int i;
    heapA = dimA;
    buildMaxHeap(A);
    for(i=dimA;i>=2;i--){
        int aux = A[1];
        A[1] = A[i];
        A[i] = aux;
        nH+=3;//----
        nh+=3;
		heapA--;
        maxHeapify(A,1);
    }
}
//quickSort
int partition(int *A,int p, int r){
    int x = A[r];
    nQ++;
    int i=p-1;
    int j;

    for(j=p;j<=r-1;j++){

        if(A[j]<=x){
            i++;
            nQ+=3;//----
            int aux = A[i];
            A[i] = A[j];
            A[j] = aux;
        }
        nQ++;
    }
    nQ+=3;//----
    int ax = A[i+1];
    A[i+1] = A[r];
    A[r] = ax;
    return i+1;
}

void quickSort(int *V,int p,int r){
    int q;
    if(p<r){
        q=partition(V,p,r);
        quickSort(V,p,q-1);
        quickSort(V,q+1,r);
    }
}

//Generare AVG pentru HeaptSort si QuickSort
void genAvg(int dimV){
    int i;
    for(i=1;i<=dimV;i++){
        C[i] = rand()%10000;
    }
}

void genWorst (int n)
{
    int i;
	for(i=1;i<=n; i++)
		C[i]=n-i+1;
	C[n/2] = n;
	
}

//Generare Best si Worst pentru QuickSort
void genBest(int V[],int dimV){
    int i;
   // V[1] = rand()%100;
    for(i=1;i<=dimV;i++){
        V[i] = i;
    }
	


}

void copy(int C[], int A[], int n)
{
	int i;
for(i=1; i<=n; i++)
A[i]=C[i];
}

int main ()
{
    srand(time(NULL));
    FILE *file;
	long tot_H=0,tot_Q=0;
    int i,j;
	
	//Cazul Best si Worst
	file = fopen("bestSiWorst.csv","w");
	fprintf(file,"n,nrOpBestQuickS,nrOpWorstQuickS\n");
	i=0;
	for(i=100;i<=10000;i+=100)
	{
		nQ = 0;
		genBest(A,i);
        quickSort(A,1,i);
		printf("\n%d\n", i);
		fprintf(file,"%d, %d,",i,nQ);
		nQ = 0;
		genWorst(i);
		copy(C,A,i);
        quickSort(A,1,i);
		fprintf(file,"%d\n",nQ);
    }

	fclose(file);

	//Cazul mediu statistic
	file = fopen("mediuStatistic12.csv","w");
    fprintf(file,"n,nrOpHeapS,nrOpQuickS\n");
	for(j=100;j<10000;j+=100){
		dimA=j;
		tot_H = tot_Q=0;
		for(i=0;i<5;i++){
		//    printf("\naici %d\n",i);
	//		printf("\nINITIAL: \n");
			genAvg(dimA);
			copy(C,A,dimA);
	//		afis(A,dimA);
        
	//	printf("Dupa heapSort: \n");
        heapSort(A);
     //   afis(A,dimA);
	//	
		//printf("Dupa quickSort: \n");
        copy(C,A,dimA);
        quickSort(A,1,dimA);
    //    afis(A,dimA);
		
		tot_Q+=nQ;
		tot_H+=nH;
		printf("\nH: %d Q: %d\n",nH,nQ);
		nQ=0;
		nH=0;
		}

		fprintf(file,"%ld, %ld, %ld\n",j,(long)(tot_H/5),(long)(tot_Q/5));
	}
	fclose(file);
	
	dimA=10;
	for(i=0;i<5;i++){
	    printf("\naici %d\n",i);
			printf("\nINITIAL: \n");
			genAvg(dimA);
			copy(C,A,dimA);
			afis(A,dimA);
        
			printf("Dupa heapSort: \n");
			heapSort(A);
			afis(A,dimA);
		
			printf("Dupa quickSort: \n");
			copy(C,A,dimA);
			quickSort(A,1,dimA);
			afis(A,dimA);
		
		
			printf("\nH: %d Q: %d\n",nH,nQ);
			nQ=0;
			nH=0;
	
	}

    printf("Am terminat");
	getch();
    return 0;
}
