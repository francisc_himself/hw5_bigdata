// ***ANONIM*** ***ANONIM***-Petre,***GROUP_NUMBER***
// You are required to implement correctly and efficiently an O(nlogk) method for merging k sorted sequences, where n is the total number of elements
// (Hint: use a heap, see seminar no. 2 notes).


#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include <time.h>

int nr_atr,nr_comp,nr_atr_lista;      


typedef struct nod{                              
	int info;                     
	struct nod  *urm;             
}NOD;

NOD *prim,*ultim,*primf,*ultimf;              

NOD *CAP[10000];                           

typedef struct prov{                       
	int cheie;             
	int index;           
}PROV;

PROV H[10000];                           


void pretty_print(PROV *T,int n, int i, int depth)
{
	int j=0;
	if(i<=n)
	{

		pretty_print(T,n, 2*i+1, depth+1);
		for(j=0;j<depth;j++)
			printf("          ");
		printf("%d\n",T[i].cheie);

		pretty_print(T,n,2*i,depth+1);
	}

}

void inserareLista(NOD **prim,NOD **ultim,int x)  

{
	NOD *p;
	p=(NOD*)malloc(sizeof(NOD));               
	p->info=x;                                    
	p->urm=0;                                     

	if(*prim==0)                               
	{
		*prim=p;
		*ultim=p;

	}
	else (*ultim)->urm=p;                 

	*ultim=p;                                  

}



void stergere(NOD **prim)       
{
	NOD *p;
	if(*prim!=0)
	{
		p=*prim;              
		*prim=(*prim)->urm;   
		free(p);               
	}
}

void inter(PROV *a,PROV *b)      
{ PROV aux;                     

aux=(*a);
(*a)=(*b);
(*b)=aux;
}
int LEFT(int i)                    
{
	return 2*i;
}

int RIGHT(int i)                   
{
	return 2*i+1;
}

int PARENT(int i)                  
{
	return i/2;
}

void PushH(PROV *Heap,PROV x,int *dimH)  
{
	int i;


	(*dimH)=(*dimH)+1;                     
	Heap[*dimH]=x; nr_atr++;
	i=(*dimH);

	nr_comp++;
	while((i>1)&&(Heap[PARENT(i)].cheie > Heap[i].cheie))
	{
		nr_comp++;
		inter(&Heap[PARENT(i)],&Heap[i]); nr_atr=nr_atr+2;
		i=PARENT(i);
	}
}

void reconstructieH(PROV *Heap,int i,int hsize) 
{  int l=LEFT(i);                  
int r=RIGHT(i);                             
int large;

nr_comp++;

if((l<=hsize)&&(Heap[l].cheie < Heap[i].cheie))
	large=l;
else large=i;
nr_comp++;
if((r<=hsize)&&(Heap[r].cheie < Heap[large].cheie))
	large=r;

if(large!=i)
	{
		inter(&Heap[i],&Heap[large]);   nr_atr=nr_atr+2;
		reconstructieH(Heap,large,hsize);  
	}
}

PROV PopH(PROV *Heap,int *dimH)   
{                                   
	PROV x;
	x=Heap[1];
	Heap[1]=Heap[*dimH];nr_atr++;
	(*dimH)=(*dimH)-1;  
	reconstructieH(H,1,*dimH);  
	return x;
}

PROV read(NOD **NOD,int i)  
{                            
	PROV x;
	if(*NOD==0)             
	{
		x.cheie=0;
		x.index=-1;
	}
	else{
		x.cheie=(*NOD)->info;
		x.index=i;
		stergere(NOD);  
		nr_atr_lista++;  
	}
	return x;
}

void afisare(NOD *prim)
{
NOD *p;
if(prim==0) printf("\n Lista e vida \n");
else
	{ p=prim;
		while(p!=0)
		{
			printf(" %d",p->info);
			p=p->urm;
		}
	}
}

void interclasare(int k,PROV *Heap)
{
	int i;
	PROV x;
	int dimH=0; 

	primf=0;  
	ultimf=0;

	for(i=0;i<k;i++)
	{
		x=read(&CAP[i],i);   
		PushH(Heap,x,&dimH);
	}
	
	//pretty_print(H,dimH,1,0);				
	//printf("\n");						

	while(dimH>0)
	{
		x=PopH(Heap,&dimH);
		inserareLista(&primf,&ultimf,x.cheie);
		//printf("Numarul adaugat in lista rezultat este %d \n",  x.cheie);
		//printf("Lista rezultat : ");
		//afisare(primf);
		//printf("\n");
		nr_atr_lista++;

		x=read(&CAP[x.index],x.index);
		if(x.index!=-1)  PushH(Heap,x,&dimH);

		//pretty_print(H,dimH,1,0);			
		//printf("\n");						

	}
}


void generareListe(int k,int n)	 
{
	int i,x,j;
	for(i=0;i<k;i++)
	{
		prim=0;
		ultim=0;
		x=0;
		for(j=0;j<(n/k);j++)
		{
		
			x=x+rand()%10;
			inserareLista(&prim,&ultim,x);
		}
		CAP[i]=prim;
		//afisare(prim);				
		//printf("\n");		
	}
}





void main(void)
{
	FILE *f1, *f2;					
	int n;
	int k1,k2,k3,k;
	srand(time(NULL));
		f1=fopen("D:\\CAZ1.xls","w");
		f2=fopen("D:\\CAZ2.xls","w");
		fprintf(f1,"n\t k=5\t k=10\t k=100 ");
		fprintf(f2,"k\t nr_operatii");
	//Caz 1: n variabil, k constant

	for(n=100;n<=10000;n+=100)
	{

		nr_atr=0;
		nr_comp=0;
		nr_atr_lista=0;
		k1=5;

		generareListe(k1,n);                       
		interclasare(k1,H);                        
		fprintf(f1,"\n%d \t%d",n,nr_atr+nr_comp+nr_atr_lista);

		nr_atr=0;
		nr_comp=0;
		nr_atr_lista=0;

		k2=10;

		generareListe(k2,n);                   
		interclasare(k2,H);                     
		fprintf(f1,"\t%d",nr_atr+nr_comp+nr_atr_lista);


		nr_atr=0;
		nr_comp=0;
		nr_atr_lista=0;

		k3=100;
		generareListe(k3,n);                        
		interclasare(k3,H);                          
		fprintf(f1,"\t%d",nr_atr+nr_comp+nr_atr_lista);

	}


	//Caz 2: n constant, k variabil

	n=10000;

	for(k=10;k<=500;k+=10)
	{
		nr_atr=0;
		nr_comp=0;
		nr_atr_lista=0;

		generareListe(k,n);         
		interclasare(k,H);          
		fprintf(f2,"\n%d \t%d",k,nr_atr+nr_comp+nr_atr_lista);
	}																	
	
	//DEMO
  /*  
	k=5;
	n=20;
	generareListe(k,n);
	interclasare(k,H);
	*/
	
	fclose(f1);
	fclose(f2);
	getch();
}
