#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>

#define MAX_SIZE 10000
#define LOW 10
#define HIGH 100
//#define N 8

void copiere_a_a2(int a[],int a2[], int n)
{
	 for (int i=1;i<=n;i++) a[i]=a2[i];	        
}

void Interschimbare (int a[], int n, int &nr_atr, int &nr_comp)
{
	int aux;
	bool ok;
    ok=false;
	nr_comp=0;
	nr_atr=0;
	while (!ok)
	{  
		ok=true;
		for (int j=1;j<n;j++)
		{   nr_comp++;
			if (a[j]>a[j+1])
			{   
                aux=a[j];
                a[j]=a[j+1];
				a[j+1]=aux;
				ok=false;
				nr_atr=nr_atr+1;
			}	
		}
	}
}
void Selectie (int a[], int n, int &nr_atr, int &nr_comp)
{
	int i,j,imin,aux;
	nr_atr=0;
	nr_comp=0;
	for(i=1;i<n;i++)
	{
		imin=i;
		for(j=i+1;j<=n;j++)
		{   nr_comp++;
			if (a[j]<a[imin]) 
				imin=j;
		}
		aux=a[i];
		a[i]=a[imin];
		a[imin]=aux;
		nr_atr++;
		
	}
}
void Insertie(int a[], int n, int &nr_atr, int &nr_comp)
{
	int i,j,k,x;
	nr_atr=0;
	nr_comp=0;
	for (i=2;i<=n;i++)
	{
		x=a[i];
		nr_atr++;
		j=1;
		nr_comp++;
		while (a[j]<x)
		{   nr_comp++;
			j=j+1;
		}
		for (k=i;k<=j+1;j--)
		{   nr_atr++;
			a[k]=a[k-1];
		}
		a[j]=x;
		nr_atr++;
	}

}

void main()
{
	int a[MAX_SIZE];
	int a2[MAX_SIZE];
	int i,nr_atr[4],nr_comp[4], n;

	//pregatire fisier
	FILE *f;
	f = fopen("grafic.csv","w");
	fprintf(f,"n,interschimbare_bst_atr,interschimbare_bst_comp,interschimbare_bst_atr+comp,selectie_bst_atr,selectie_bst_comp,selectie_bst_atr+comp,insertie_bst_atr,insertie_bst_comp,insertie_bst_atr+comp,interschimbare_av_atr,interschimbare_av_comp,interschimbare_av_atr+comp,selectie_av_atr,selectie_av_comp,selectie_av_atr+comp,insertie_av_atr,insertie_av_comp,insertie_av_atr+comp,interschimbare_ws_atr,interschimbare_ws_comp,interschimbare_ws_atr+comp,selectie_ws_atr,selectie_ws_comp,selectie_ws_atr+comp,isertie_ws_atr,isertie_ws_comp,isertie_ws_atr+comp\n");
    
	srand(time(NULL));
	

	for(n=100; n<=10000; n=n+100)
	{
		//nr_atr si comp
		nr_atr[0]=0;
		nr_atr[1]=0;
		nr_atr[2]=0;
		nr_atr[3]=0;
		nr_comp[1]=0;
		nr_comp[2]=0;
		nr_comp[3]=0;
		nr_comp[0]=0;

		//screire n in tabel
		fprintf(f,"%d,",n);

		//CAZUL FAVORABIL
      for (i=1;i<=n;i++) a[i]=i;  //sir sortat
	  //interschimbarea
	  Interschimbare(a,n,nr_atr[0],nr_comp[0]);
	  fprintf(f,"%d,%d,%d,",nr_atr[0],nr_comp[0],nr_atr[0]+nr_comp[0]);
	  //selectie
	  Selectie(a,n,nr_atr[0],nr_comp[0]);
	  fprintf(f,"%d,%d,%d,",nr_atr[0],nr_comp[0],nr_atr[0]+nr_comp[0]);
	  //insertie
	  Insertie(a,n,nr_atr[0],nr_comp[0]);
	  //scriere rezultate
	  fprintf(f,"%d,%d,%d,",nr_atr[0],nr_comp[0],nr_atr[0]+nr_comp[0]);

	   //CAZUL MEDIU-STATISTIC
	  for (int j = 1; j <= 5; j++){
		for (i = 1; i <= n; i++){
				a[i] = rand() % 10000;
				a2[i] = a[i];
			}
		//interschimbare
		Interschimbare(a,n,nr_atr[0],nr_comp[0]);
		nr_atr[1]=nr_atr[1]+nr_atr[0];
		nr_comp[1]=nr_comp[1]+nr_comp[0];
		copiere_a_a2(a,a2,n);
		//selectie
		Selectie(a,n,nr_atr[0],nr_comp[0]);
		nr_atr[2]=nr_atr[2]+nr_atr[0];
		nr_comp[2]=nr_comp[2]+nr_comp[0];
		copiere_a_a2(a,a2,n);
		//insertie
		Insertie(a,n,nr_atr[0],nr_comp[0]);
		nr_atr[3]=nr_atr[3]+nr_atr[0];
		nr_comp[3]=nr_comp[3]+nr_comp[0];
		copiere_a_a2(a,a2,n);
	  }
	  //scriere rezultate
	  fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,", nr_atr[1] / 5, nr_comp[1] / 5, (nr_atr[1] + nr_comp[1]) / 5, nr_atr[2] / 5, nr_comp[2] / 5, (nr_atr[2] + nr_comp[2]) / 5, nr_atr[3] / 5, nr_comp[3] / 5, (nr_atr[3] + nr_comp[3]) / 5);

	  //CAZUL DEFAVORABIL
	  for(i=1;i<=n;i++){
	  a[i]=n-i;
	  a2[i]=n-i;
	  }
	  //interschimbare
	  Interschimbare(a,n,nr_atr[0],nr_comp[0]);
	  fprintf(f, "%d,%d,%d,", nr_atr[0], nr_comp[0], nr_atr[0]+nr_comp[0]);
	  copiere_a_a2(a,a2,n);
	  //selectie
	  Selectie(a,n,nr_atr[0],nr_comp[0]);
	  fprintf(f, "%d,%d,%d,", nr_atr[0], nr_comp[0], nr_atr[0]+nr_comp[0]);
	  copiere_a_a2(a,a2,n);
	  //insertie
	  Insertie(a,n,nr_atr[0],nr_comp[0]);
	  fprintf(f, "%d,%d,%d\n", nr_atr[0], nr_comp[0], nr_atr[0]+nr_comp[0]);
	  copiere_a_a2(a,a2,n);
	}
	printf("s-a terminat\n");
	


  /*  //verificare sortare
	int b[6];
	int i;
    for (i=1;i<=5;i++) scanf("%d ",&b[i]);
	int n=5;
	int nr_atr=0;
	int nr_comp=0;
	Interschimbare(b,n,nr_atr,nr_comp);
   //Selectie(b,n,nr_atr,nr_comp);
	//Insertie(b,n,nr_atr,nr_comp);
	printf("sirul sortat:\n");
	for (i=1;i<=5;i++) printf("%d ",b[i]);
	*/

	fclose(f);
	getch();
}