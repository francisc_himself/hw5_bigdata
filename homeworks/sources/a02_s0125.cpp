/*
--Name: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
--Group: ***GROUP_NUMBER***
--College: Technical University of Cluj Napoca

--Algorithms presented and compared: Build Heap, Top Down and Bottom Up methods of implementation

--Start date: 12 Mar 2013
--End date: 19 Mar 2013

--Evaluation requirements: Interpretations, advantages/disadvantages of each approach

Interpretations:
Functionality: A build heap method serves to create a heap from an array of unsorted elements. The heap-max property implies that each child is smaller or equal to its parent node.
Each parent can have up to two children.

Bottom up: The heap is the original array, and we just "fix" it by swapping elements until they are in correct positions. Efficiency - O(n)
Top down: We start with an empty heap, into which we insert new values. Because of the way checks are performed, these new values will always be inserted in appropriate locations. Efficiency - O(n lg n)

Charts: The bottom up approach has a significant advantage over the top down approach.

Advantages/disadvantages:
TD: Dynamic size, slow runtime
BU: Static size, fast runtime

Worst case, TD: New root swapped on each level until it reaches the top, therefore ordered array. (TD works considerably slower, while BU is not significantly affected)
BU worst case: If right or left would always be bigger than the parent, causing the swap to occur. This means that the worst case for both is the same. (?)

Final note: pow is defined here to make it compatible with Visual Studio 2010 (and it messes up the output for some reason)
In 2012 this is not necessary, and the printTree function prints the first element as well.
*/



#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;

//ofstream myfile; //files to write to
int TDtotal = 0;
int BUtotal = 0;
int A[]; //values
int A_heapsize;
int i;
int Hardcode[] = {3,6,12,0,1,2,55,32,10,12,9,1,14,15,19,20,21,18,31,40};
//A[20]=
int A[10000];
int B[10000];
int heap[10001];
int heap_size = 0;
int A_length = sizeof(Hardcode)/sizeof(Hardcode[0]);
int ALRef = A_length;
ofstream myfile;

#define Parent(i)	  (i/2)
#define Left(i)		 (2*i) 
#define Right(i)	(2*i+1)
#define swapT(x,y) int t;t=x;x=y;y=t;

int pow(int a, int b){
	int rez=a;
for (int i=0; i<b-1; i++) 
	rez=rez*a;
return rez;
}

void printTree(int A[], int A_length){
	//(this could have been done simpler recursively; adds 0s for nonexistent leaves on last row)
	int x=0;
	int totalrows=0;
	int current=1;
	int startspacing=0;
	int spacing=1;
	int signcount=0;
	while (x<A_length){
		x=x+(int)pow(2,totalrows);
		totalrows++;
	}
	printf("\n");
	while (totalrows>0)
	{
		printf("\n");
		for(int k=0; k<startspacing; k++) printf(" ");
		for(i=(int) pow(2,totalrows)-1; i > (int) pow( 2, totalrows-1)-1; i--)
		{
			printf("%d", A[i]);
			for(int k=0; k<spacing; k++) printf(" ");
		}
		if(totalrows>1){
		printf("\n");
		for(int k=0; k<startspacing; k++) printf(" ");
				for(i=(int) pow(2,totalrows)-1; i > (int) pow(2,totalrows-1)-1; i--)
		{
			if (signcount == 0){printf("\\"); signcount++;}
			else {printf("/"); signcount--;}
			for(int k=0; k<spacing; k++) printf(" ");
		}
		}
		totalrows--;
		current++;
		startspacing=spacing;
		spacing=(int) pow(2,current)-1;
	}
};


void max_heapify(int heap[],int i) // O (lg n) time
{
	int largest = 0;
	int l = Left(i); int r = Right(i);
	BUtotal++;
	if (l<= heap_size && heap[l] > heap[i])
		largest = l;
	else largest = i;
	BUtotal++;
	if (r<= heap_size && (heap[r] > heap[largest]))
	largest = r;
	//BUtotal++;
	if (largest!= i){
		BUtotal++;BUtotal++;BUtotal++;
			swapT(heap[i], heap[largest]);
			max_heapify(heap,largest);}
}

void build_max_heap(int A[]){ // O(n)

	for (i=0; i<A_length; i++) heap[i+1] = A[i];

	heap_size = A_length;
	for(i = (A_length/2); i>=1; i--)
		max_heapify(heap,i);
}



void heapsort(int A[]){ // O(n lg n)
	printf("Heapsort called \n");
	build_max_heap(A);
	
	for (i = A_length; i>1; i--){
		swapT(A[1],A[i]);
		heap_size = heap_size - 1;
		max_heapify(A,1);
	}

}
//topdown functions follow
int heap_maximum(int heap[]){ // 1 op
	return heap[0];
}

int heap_extract_max(int heap[]){ //O (lg n)
	if (heap_size < 1) {printf("Underflow"); return -1;}
	else {
		int max = heap[0];
		heap[0] = heap[heap_size];
		heap_size = heap_size-1;
		max_heapify(heap,0);
		return max;
	}
}

void heap_increase_key(int heap[], int i, int key) //O (lg n)
{
	TDtotal++;
	if(key < heap[i]) {printf("New key smaller than current key"); return;}
	else {
		heap[i] = key;
		TDtotal++;

		while (i>1 && (heap[Parent(i)] < heap[i])){
			TDtotal++;
			swapT(heap[i], heap[Parent(i)]);
			TDtotal++;TDtotal++;TDtotal++;

			i = Parent(i);
		}
		TDtotal++;
		}
}

void max_heap_insert(int heap[], int key){ //O (lg n)
heap_size = heap_size + 1;
//TDtotal++; // ?
heap[heap_size] = -9999;
TDtotal++;
heap_increase_key(heap, heap_size, key);

}

void build_max_heap_TD(int A[]){
	//printf("Top down build called \n");

	//if(i<=(A_length-2)/2)
	//{
		//build_max_heap_TD(heap, 2*i+1);
		//build_max_heap_TD(heap, 2*i+2);

		//max_heap_insert(heap,i);
	//						for (i=0; i<(sizeof(Hardcode)/sizeof(Hardcode[0])); i++)
	//{printf("%d " , Hardcode[i]);}
	//				printf("\n");
		//printTree(Hardcode, ALRef);

	for(i = 1; i < A_length; i++) heap[i]=0;
	heap_size=1;
	heap[1] = A[0];
	for(i = 1; i < A_length; i++) max_heap_insert(heap, A[i]);


	}

int main()
{
		for (i=0; i<(sizeof(Hardcode)/sizeof(Hardcode[0])); i++)
	{printf("%d " , Hardcode[i]);}
					printf("\n");

			heap_size = 1;
		for (i=0; i<A_length; i++) heap[i+1] = 0;

	build_max_heap_TD(Hardcode);
	//build_max_heap(Hardcode);
	//for (i=0; i<(sizeof(Hardcode)/sizeof(Hardcode[0])); i++)
	//{printf("%d " , Hardcode[i]);}
	for (i=0; i<=(sizeof(Hardcode)/sizeof(Hardcode[0])); i++)
	{printf("%d " , heap[i]);}
					printf("\n");
	printTree(heap, ALRef+1);

	_getch();

	//Generate csv measurement file
	for(int k=0; k<5; k++){
	for(int n=100; n<=10000; n += 100){
		printf("\n");
	for (i=100; i<n; i ++)
	{
		A[i] = rand() % 100;
		B[i] = A[i];
	}
	TDtotal = 0; BUtotal = 0;
	A_length = i; ALRef = A_length;
	build_max_heap(A);
	build_max_heap_TD(B);
	printf("Set size:%d TD: %d BU: %d", n, TDtotal, BUtotal);
	myfile.open ("TDvsBU.csv",ios::app);
	myfile << n << "," << TDtotal << "," << BUtotal << endl;
	myfile.close();
	}
	}
	
	/*
	for(int k=0; k<5; k++){
	for(int n=100; n<=10000; n += 300){
		printf("\n");
	for (i=100; i<n; i ++)
	{
		A[i] = i;
		B[i] = A[i];
	}
	TDtotal = 0; BUtotal = 0;
	A_length = i; ALRef = A_length;
	build_max_heap(A);
	build_max_heap_TD(B);
	printf("WA Set size:%d TD: %d BU: %d", n, TDtotal, BUtotal);
	//myfile.open ("TDvsBU.txt",ios::app);
	//myfile << n << "," << TDtotal << "," << BUtotal << endl;
	//myfile.close();
	}
	}
	*/
	/*
	for(int k=0; k<5; k++){
	for(int n=100; n<=10000; n += 300){
		printf("\n");
	for (i=100; i<n; i ++)
	{
		A[i] = i;
		B[i] = A[i];
	}
	TDtotal = 0; BUtotal = 0;
	A_length = i; ALRef = A_length;
	build_max_heap(A);
	build_max_heap_TD(B);
	printf("WA Set size:%d TD: %d BU: %d", n, TDtotal, BUtotal);
	//myfile.open ("TDvsBU.txt",ios::app);
	//myfile << n << "," << TDtotal << "," << BUtotal << endl;
	//myfile.close();
	}
	}
	*/
	_getch();

}