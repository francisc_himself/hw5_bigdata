#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"
#include "stddef.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 6 - The Jospehus Permutation
/***************************************************/

/****************************************************************************
/							OBSERVATIONS, INTERPRETATIONS
/
/	1. The algorithm takes O(nlgn) time to complete in average case.
/	2. Build tree was done by creating the node to be inserted and then the
/	   insertion was made as in a BST.
/	3. Size was decremented when a node was deleted by going upward to the
/	   deleted node's parent and decrementing the size up to the root.
/***************************************************************************/

#define MAX_NR 10001

int arr[MAX_NR];
int n;

Profiler profiler("JosephusTest");

typedef struct nodetype
{
	int value;
	int size;
	struct nodetype *left;
	struct nodetype *right;
	struct nodetype *parent;
}NodeT;

typedef NodeT* NodePtr;

NodePtr root;

void insert(NodePtr z)
{
	NodePtr y = NULL;
	NodePtr x = root;
	profiler.countOperation("Comparisons",n,1);
	while(x != NULL)
	{

		y = x;
		profiler.countOperation("Comparisons",n,1);
		if(z->value < x->value)
		{
			x = x->left;
		}
		else
		{
			x = x->right;
		}
		profiler.countOperation("Comparisons",n,1);
	}
	profiler.countOperation("Assignments",n,1);
	z->parent = y;
	profiler.countOperation("Comparisons",n,1);
	if(y == NULL)
	{
		profiler.countOperation("Assignments",n,1);
		root = z;
	}
	else
	{
		profiler.countOperation("Comparisons",n,1);
		if(z->value < y->value)
		{
			profiler.countOperation("Assignments",n,1);
			y->left = z;
		}
		else
		{
			profiler.countOperation("Assignments",n,1);
			y->right = z;
		}
	}
}

void buildTree(int left, int right, int size)
{
	int med = (left + right) / 2;
	NodePtr z = (NodePtr) malloc(sizeof(NodeT));
	z->value = arr[med];
	z->left = NULL;
	z->right = NULL;
	z->size = size;
	z->parent = NULL;
	profiler.countOperation("Assignments",n,5);
	insert(z);
	profiler.countOperation("Comparisons",n,1);
	if(left <= med - 1)
	{
		buildTree(left,med-1,med - left);
	}
	profiler.countOperation("Comparisons",n,1);
	if(right >= med + 1)
	{
		buildTree(med+1,right,right - med);
	}
}

NodePtr osSelect(NodePtr node, int i)
{
	int r;
	profiler.countOperation("Comparisons",n,1);
	if(node->left != NULL)
	{
	r = node->left->size + 1;
	}
	else
	{
		r = 1;
	}
	profiler.countOperation("Comparisons",n,1);
	if(i == r)
	{
		return node;
	}
	else 
		{
			profiler.countOperation("Comparisons",n,1);
			if(i < r)
	{
		return osSelect(node->left,i);
	}
	else
	{
		return osSelect(node->right,i-r);
	}
	}
}

NodePtr minimum(NodePtr x)
{
	profiler.countOperation("Comparisons",n,1);
	while(x->left != NULL)
	{
		profiler.countOperation("Assignments",n,1);
		x = x->left;
		profiler.countOperation("Comparisons",n,1);
	}
	return x;
}

NodePtr successor(NodePtr x)
{
	profiler.countOperation("Comparisons",n,1);
	if(x->right != NULL)
	{
		return minimum(x->right);
	}
	profiler.countOperation("Assignments",n,1);
	NodePtr y = x->parent;
	profiler.countOperation("Comparisons",n,1);
	while(y != NULL && x==y->right)
	{
		x = y;
		profiler.countOperation("Assignments",n,2);
		y = y->parent;
		profiler.countOperation("Comparisons",n,1);
	}
	return y;
}

void decreaseDim(NodePtr z)
{
	profiler.countOperation("Comparisons",n,1);
	while(z != NULL)
	{
		profiler.countOperation("Assignments",n,2);
		z->size = z->size - 1;
		z = z->parent;
		profiler.countOperation("Comparisons",n,1);
	}
}

void osDelete(NodePtr z)
{
	NodePtr y;
	NodePtr x;
	profiler.countOperation("Comparisons",n,1);
	if(z->left == NULL || z->right == NULL)
	{
		profiler.countOperation("Assignments",n,1);
		y = z;
	}
	else
	{
		profiler.countOperation("Assignments",n,1);
		y = successor(z);
	}
	profiler.countOperation("Comparisons",n,1);
	if(y->left != NULL)
	{
		profiler.countOperation("Assignments",n,1);
		x = y->left;
	}
	else
	{
		profiler.countOperation("Assignments",n,1);
		x = y->right;
	}
	profiler.countOperation("Comparisons",n,1);
	if(x!= NULL)
	{
		profiler.countOperation("Assignments",n,1);
		x->parent = y->parent;
	}

	decreaseDim(y->parent);

	profiler.countOperation("Comparisons",n,1);
	if(y->parent == NULL)
	{
		profiler.countOperation("Assignments",n,1);
		root = x;
	}
	else
	{
		profiler.countOperation("Comparisons",n,1);
		if(y == y->parent->left)
		{
			profiler.countOperation("Assignments",n,1);
			y->parent->left = x;
		}
		else
		{
			profiler.countOperation("Assignments",n,1);
			y->parent->right = x;
		}
	}
	profiler.countOperation("Comparisons",n,1);
	if(y != z)
	{
		profiler.countOperation("Assignments",n,1);
		z->value = y->value;
	}
}

void prettyPrint(NodePtr node, int level)
{
	if(node != NULL)
	{
		prettyPrint(node->right,level+1);
        for (int i = 0; i <= level; i++ )
            printf( "          " ); /* for nice listing */
		printf( "%d-%d \n", node->value,node->size);
		prettyPrint(node->left,level+1);
	}
}

void josephus(int n, int m)
{
	buildTree(1,n,n);
	//prettyPrint(root,1);
	//printf("\n\n\n");
	int j = 1;
	for(int k = n; k >= 1; k--)
	{
		j = (( j + m - 2) % k) +1;
	NodePtr node = osSelect(root,j);
	//printf("%d \n",node->value);
	osDelete(node);
	//prettyPrint(root,1);
	//getch();
	//printf("\n\n\n");
	}
}

void main()
{
	/*
	//PROVING CORRECTNESS

	n = 7;
	for(int i = 1; i <= n; i++)
	{
		arr[i] = i;
	}
	josephus(7,3);
	*/

	//TESING ALGORITHM
	for(n = 100; n <= 10000; n+=100)
	{
		for(int i = 1; i<= n; i++)
		{
			arr[i] = i;
		}
		josephus(n,n/2);
		printf("%d\n",n);
		profiler.addSeries("Total","Comparisons","Assignments");
	}
	profiler.showReport();
	return;
}