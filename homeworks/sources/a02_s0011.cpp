/****ANONIM*** ***ANONIM***
Grupa:***GROUP_NUMBER***
Tema 2 : Compararea metodelor de consructie HEAP: TOP-DOWN si BOTTOM-UP, in cazul mediu statistic.
Complexitatea algoritmilor este  O(n)

*/

#include<iostream>
#include<conio.h>
#include<fstream>

#define MAX_SIZE 10000

int a[10001], b[10001], heap_size,l;
int op_td=0, op_bu=0;
using namespace std;
ofstream fout("date.csv");

//pt metoda TOP_DOWN
int p(int i)
{
	return i/2;
}

void h_init(int a[])
{
	l=0;
}

void h_push(int a[], int x)
{
	l=l+1;
	op_td++;
	a[l]=x;
	int i=l;
	op_td++;
	while((i>1) &&(a[p(i)]>a[i]))
	{
		op_td=op_td+3;
		int aux=a[p(i)];
		a[p(i)]=a[i];
		a[i]=aux;
		i=p(i);
	}
}

void heap_top_down(int a[], int b[])
{
	h_init(b);
	for(int i=1;i<=heap_size;i++)
		h_push(b,a[i]);
}

//pt metoda BOTTOM-UP

void min_heapify(int a[], int i)
{
	int ind; //indexul minim
	op_bu++;
	if((2*i<=heap_size)&&(a[2*i]<a[i]))
		ind=2*i;
	else
		ind=i;
	op_bu++;
	if(((2*i+1)<=heap_size) &&(a[2*i+1]<a[ind]))
		ind=2*i+1;
	if(ind!=i)
	{
		op_bu=op_bu+3;
		int aux=a[i];
		a[i]=a[ind];
		a[ind]=aux;
		min_heapify(a,ind);
	}
}

void heap_bottom_up(int a[])
{
	for(int i=heap_size/2;i>0;i--)
		min_heapify(a,i);
}

void pretty_print(int a[], int dim)
{
	int adancime=1;
	for(int i=1;i<=dim;i++)
	{
		for(int j=1;j<=dim/2-adancime;j++)
			cout<<"\t";
		cout<<a[i];
		if((i==1)||(i==3)||(i==7))
		{
			adancime++;
			cout<<endl;
		}
	}
}

int main()
{
	///////////////////////////////////////////////Verificare corectitudine cod///////////////////////////////////////
	
	heap_size=10;
	a[1]=5;a[2]=11;a[3]=3;a[4]=7;a[5]=9;a[6]=15;a[7]=23;a[8]=6;a[9]=4;a[10]=1;
		
	cout<<"Vectorul initial este ";
	for(int i=1;i<=10;i++)
		cout<<a[i]<<" ";
	heap_top_down(a,b);
	cout<<endl<<"Heap-ul construit prin metoda top-down este "<<endl;
	pretty_print(b,heap_size);
	heap_bottom_up(a);
	cout<<endl<<"Heap-ul construit prin metoda bottom-up este "<<endl;
	pretty_print(a, heap_size);

	/////////////////////////////////////////Generare grafice pentru cazul mediu statistic//////////////////////////////////////
	/*fout<<"NR ,Operatii_TD, Operatii_BU"<<endl;
	for(heap_size=100; heap_size<=MAX_SIZE; heap_size+=100)
	{
		op_td=0;
		op_bu=0;
		for(int nr=0;nr<5;nr++)
		{
			for(int i=1;i<=heap_size;i++)
			{
				a[i]=rand()%RAND_MAX;
				//c[i]=a[i];
			}
			heap_top_down(a,b);
			heap_bottom_up(a);
		}
		op_td=op_td/5;
		op_bu=op_bu/5;
		fout<<heap_size<<","<<op_td<<","<<op_bu<<endl;
	}
	fout.close();*/
	
	getch();
	return 1;
}
