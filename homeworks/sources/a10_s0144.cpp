#include<stdio.h>
#include<stdlib.h>
#include<time.h>
 
/* In algoritmul de cautare in adancime, strategia folosita este aceea de a cauta "mai adanc": muchiile sunt explorate pornind din vf s cel mai recent
descoperit care mai are inca muchii neexplorate,care pleaca din el. O prop. interesanta a DFS-ului este clasificarea muchilor:
  1.muchia de arbore: (u,v) este o m.a. daca v afost descoperit explorand (u,v); ->culoarea alba
  2.muchia inapoi: acele muchii (u,v) care unesc un vf u cu un stramos v intr-un arbore de adancime;-> culoarea gri
  3.muchii inainte: muchii care nu sunt muchii arbore si conecteaza un vf. u cu un descedent v intr-un arbore de adancime.->culoarea neagra
  4.muchii transversale: toate celelalte muchii ->culoarea neagra
 
 Dupa cum se poate observa, atat din graficele obtinute, cat si la prima vedere, algoritmul este unul liniar, avand o eficienta de O(n)
  
  Sortarea topologica a unui graf aciclic orientat, este o ordonare liniara a tuturor vf sale a.i daca exista o muchie (u,v), atunci u apare inaintea lui v in ordonare.
  Daca un graf are cicluri(muchii inapoi), at. nu este posibila nici o ordonare liniara.
		-implementare: dupa ce se calculeaza timpii finali de terminare si fiecare vf terminat, acesta se insereaza in capul unei liste(sau putem folosi si cozi), iar 
		apoi se returneaza lista de vf*/


#define WHITE 0
#define GRAY 1
#define BLACK 2


int op=0;

typedef struct node
{
    int info;
    struct node *next;
} NOD;

NOD *prim[1001], *ultim[1001];


int cul[10000],dis[10000],fin[10000],parinte[10000],timp[10000];

//inserarea in lista: dupa ultimul element
void inserare(int muchii,int vf)  
{
    int y,x;
    NOD *p;

    for(x=0; x<vf; x++)
        ultim[x]=prim[x]=NULL;

    for(int i=0; i<muchii; i++)
    {

        y=rand()%vf;
        x=rand()%vf;

		//op++;
        if(prim[x] == NULL)
        {

            p=new NOD;                   
            p->info=y;											
            p->next=NULL;                                      
            prim[x] = p;
            prim[x]->next = NULL;
            ultim[x]=prim[x];
        }
        else
        {
            p=new NOD;
            p->info=y;
            p->next=NULL;
            ultim[x]->next=p;
            ultim[x]=p;
        }
    }
}

void push(int y)
{
	NOD *p;
	p=new NOD;
	p=*prim;
	while(p!=NULL)
		p=p->next;
	p->info=y;
	p->next=NULL;
}


void dfs_visit(int u)
{  
	//op=op+3;
    int ciclu=0; 
    cul[u]=GRAY;
    dis[u]=timp[u];
    timp[u]++;
    
	NOD *q=prim[u];
	op++;
    while(q!=NULL)
    {
        int v=q->info;
		op++;
        if(cul[v]==WHITE)
        {  
			op=op+1;
            printf("%d -> %d muchie arbore\n", u,v);
            parinte[v]=u;
            dfs_visit(v);

        }
       else if(cul[v]==1) { 
	             printf("%d -> %d muchie inapoi\n", u,v); 
				 ciclu++; //op++;
				 }
			
        else if(cul[v]==2 && dis[u]<dis[v]){
			   // op++;  
				printf("%d -> %d muchie inainte\n", u,v);
		}
			
       else  printf("%d -> %d muchie transversala\n", u,v);
			

        q=q->next;
    }
    
	//op=op+3;
    cul[u]=BLACK;
    fin[u]=timp[u];
    timp[u]++;
	//Sortare topologica: 
	/*if(ciclu>0)
	    //pun vf in coada
		push(u); */

	

}
void dfs(int n)
{
    for(int i=0; i<n; i++)
    {  
		//op=op+3;
        cul[i]=WHITE;
        parinte[i]=0;
        timp[i]=0;
    }
    for(int j=0; j<n; j++)
    {   
		op++;
        if(cul[j]==WHITE)
            dfs_visit(j);
    }

}
void afisare(NOD* nod){
	while(nod){
		printf("%d ",nod->info);
		nod=nod->next;
	}
}

int main()
{ 
	FILE *f,*f2;
   f=fopen("dfs.txt","w");
   f2=fopen("dfs2.txt","w");
    int vf,e;
    srand(time(NULL));

	inserare(9,5);
	dfs(5);

	//Sortare topologica: afisez coada

	/*for(int m=1000;m<=5000;m=m+100){
		op=0;
		vf=100;
		inserare(m,vf);
		dfs(vf);
		fprintf(f,"%d %d %d\n",m,vf,op);
	}

	for(int v=110;v<=200;v=v+10){
		op=0;
		e=9000;
		inserare(e,v);
		dfs(v);
		fprintf(f2,"%d %d %d\n",e,v,op);
	}
	*/
    return 0;
}
