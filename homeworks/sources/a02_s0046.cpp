
/*

***ANONIM*** ***ANONIM***-L
gr ***GROUP_NUMBER***

assignment 2

Ambele metode sunt destul de apropiate. 

Bottom-up pare mai eficient,face mai putine intructiuni in total. La comparatia are aproape aceasi valoare ca top-down dar la asignari are castig de cauza.

*/
#include "stdafx.h"
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
# include<iostream>
#include "stdafx.h"
# define CRT_SECURE_NO_WARNINGS
using namespace std;
int dim_heap=0;
int as_b=0,co_b=0,as_t=0,co_t=0 ;



int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i)+1;
}
//constructia heap-ului prin tehnica bottom-up
void max_heapfy(int a[],int i,int n)
{
	int l,r,max,aux;
	l=left(i);
	r=right(i);
	co_b++;
	if(l<=dim_heap && a[l]<a[i])
		max=l;
	else
		max=i;
	co_b++;
	if(r<=dim_heap && a[r]<a[max])
		max=r;
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		as_b+=3;
		max_heapfy(a,max,n);
	}
}

void max_heap(int a[],int n)
{
	dim_heap=n;
	for(int i=n/2;i>=1;i--)
		max_heapfy(a,i,n);
}


//constructia heap-ului prin insertie
void heap_insert(int a[],int key)
{
	int i,aux;
	dim_heap++;
	i=dim_heap;
	as_t++;
	a[dim_heap]=key;
	co_t++;
	while(i>1 && a[parent(i)]>a[i])
	{
		co_t++;
		as_t+=3;
		aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}

}

void max_heap_td(int a[],int b[],int n)
{
	dim_heap=0;
	for(int i=1;i<=n;i++)
		heap_insert(b,a[i]);

}

void afisare(int *v,int n,int k,int nivel)
{
	if (k>n)
	    return;
	else
	{
	afisare(v,n,2*k+1,nivel+1);
	for(int i=1;i<nivel;i++)
		printf("  ");
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);
	
	}
}



int main()
{

	int c[11]={0,11,20,5,7,45,12,14,19,72,6};
	int a[100],b[100];
	

	printf("Constructia bottom-up este :\n");
	max_heap(c,10);
    afisare(c,10,1,1);


	printf("Constructia top-down este :\n");
	max_heap_td(c,b,10);
	afisare(b,10,1,1);
	
	
	
		//srand(time(NULL));
	int x[10000],y[10000];
	FILE *f=fopen("average.csv","w");
    fprintf(f,"n,as_t,co_t,as_b,co_b,top_down,bottom_up\n");

	for(int n=100;n<=10000;n+=100)
	{
		
		as_b=0;
		co_b=0;
		as_t=0;
		co_t=0;
		for(int k=1;k<=5;k++)
		{
			for(int j=0;j<n;j++)
			{
				x[j]=rand();
			}

			max_heap_td(x,y,n);
			max_heap(x,n);
		}
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d\n",n,as_t,co_t,as_b,co_b,(as_t+co_t),(as_b+co_b));
		
	}fclose(f);
	
	
	
	getch();
	return 0;
}

