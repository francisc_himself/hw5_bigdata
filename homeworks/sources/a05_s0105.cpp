
#include<fstream>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<conio.h>

/*
- The task of this homework is to perform insert and search operations in a hash T using open addressing and quadratic probing.
- We have to make an evaluation of the search operation for hash tables using open addressing and quadratic probing, in the average case
- first we populate the hash table, and the we search
- we have to analize the search operation for different values of the filling factor {0.8,0.85,0.9,0.95,0.99}
- from the obtained results we can deduce that the number of accesses increases with the filling factor
- average search found is almost constant

Avg. Effort = total_effort / no_elements
Max. Effort = maximum number of accesses performed by one search operation 

Results:
				Avg. search found	Avg. search not found	Max. search found	Max. search not found
0.8						2.0						5.0				29.4					44.8
0.85					2.0						7.2				32.0					52.8
0.9						2.0						11.2			47.4					82.6
0.95					3.0						24.0			70.2					151.2
0.99					4.2						101.4			186.8					736.2

*/


int N=9973; //size of the hashTable(prime number close to 10000 -> 9973, I choose 9973)
float alpha[6]={0.8,0.85,0.9,0.95,0.99};
long T[9973];
int searching=0; //will hold the number of accesses


//hash function for quadratic probing
int hash_function(int k, int i)
{
	int c1=1;
	int c2=1;
	int aux = ((k % N)+c1*i+c2*i*i)%N;  //(k % N) ordinary hash fct
	return aux;
}


//insert in a hash table
int hash_insert (long k)
{
	int i,j;
	i=0;
	do
	{	j=hash_function(k,i);
		if(T[j]==-1)  //if empty->proceed to insert
		{	T[j]=k;
			printf("%d->%d  ",T[j],k);
			return j;
		}
		else	i++;
	}while (i<N);

	return -1;
}


//hash search function
int hash_search(long k)
{
	int i,j;
	i=0;
	do{
		searching++;  //increase the number of times we have searched in the table
		j=hash_function(k,i);
		if(T[j]==k)   //the element was found
			return j;
	i=i+1;
	}while(T[j]!=-1 && i<=N);

	return -1;
}


int main()
{
	srand(time(NULL));
	int i,m,maxEffortFound,total,a,maxEffortNotFound,c;
	float  avFound,avNotFound,avmf,avmnf;
	long key,aux,vectorf[1500],vectornf[1500],n;
	FILE *f;
	f=fopen("hash_average.txt","w");

	//try if the algorithm works
	for (i=0;i<N;i++)   //initialize the table
    {
        T[i]=-1;
    }
	for (i=1; i<=6; i++)  //6 keys , N=7 => alpha=6/7=0.85
	{
		key = rand()%6+1;  
		hash_insert(key);
	}
	printf("\n");
	for(i=0;i<N;i++){
		printf("%ld ",T[i]);	
	}
	for(i=1;i<=6;i++){
		int l=hash_search(T[i]);
		if(T[i]!=-1)
			printf("\nKey %ld is on slot %d ",T[i],l);
	
	}
	
  
	for(int a=0;a<5;a++)   // 5 values for the filling factor
	{	
		avFound=0;
		avNotFound=0;
		avmf=0;
		avmnf=0;
		for(c=1;c<=5;c++){	   //we repeat the experiment 5 times

			for (i=0;i<N;i++)   //initialize the table
			 T[i]=-1;

		n=alpha[a]*N;
		for (i=1;i<=n;i++)
		{
			key = (rand()+1)*rand()%500000;     //populate the hash table
			hash_insert(key);
	    }

		int val;
		for(m=1;m<=1500;m++)      //ensure that here we found the keys; from the 3000 random elements, 1500 are in the hash table for sure
		{	val=rand()%N;
			vectorf[m]=T[val];    
		}

	   for(m=1;m<=1500;m++)      //ensure that here we do not found the keys
			  vectornf[m]=rand()+500001;

		total=maxEffortFound=0;
		for (i=1;i<=1500;i++)
		{	searching=0;
			int l=hash_search(vectorf[i]);
			total=total+searching;
			if (searching>maxEffortFound)  //find the maximum effort 
				maxEffortFound=searching;
		}

		avFound+=(total/1500);
		avmf+=maxEffortFound;

		total=maxEffortNotFound=0;
		for (i=1;i<=1500;i++)
		{	searching=0;
			int l=hash_search(vectornf[i]);
			// if(l!=-1) 
			total=total+searching;
			if (searching>maxEffortNotFound)
				maxEffortNotFound=searching;
		}
		avNotFound+=(total/1500);
		avmnf+=maxEffortNotFound;
		}

		fprintf(f,"For alpha=%.2f we've found:\nAvg found: %.2f\n Avg not found: %.2f\n  Max found: %.2f\n Max not found: %.2f\n\n\n\n",alpha[a],avFound/5,avNotFound/5,avmf/5,avmnf/5);
	}

		fclose(f);
		getch();
		return 0;
	}
