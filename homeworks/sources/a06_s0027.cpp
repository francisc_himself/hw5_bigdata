// Tema6_Josephus.cpp : Defines the entry point for the console application.
//***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***

#include "stdafx.h"
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct TIP_NOD 
	{
		TIP_NOD *stanga;
		TIP_NOD *dreapta;
		TIP_NOD *parinte;
		int val;
		int dimensiune;
	}TIP_NOD;

int nr_comparatii, nr_atribuirii;

TIP_NOD *radacina;

TIP_NOD *ARBORE_MINIM(TIP_NOD* x)  //determina elementul avand cheia minima din arbore		
	{
		nr_comparatii++;
		while(x->stanga != NULL)
		{
			nr_comparatii++;
			x = x->stanga;
			nr_atribuirii++;
		}
		return x;
	}


TIP_NOD *ARBORE_SUCCESOR(TIP_NOD* x) //procedura returneaza succesorul unui nod x, daca succesorul exista,sau NULL, daca x are cea mai mare cheie din arbore
	{
		TIP_NOD  *y;
		if(x->dreapta != NULL)				  //daca subarborele drept al nodului x nu este vid, atunci succesorul lui x este  
			return ARBORE_MINIM(x->dreapta); //chiar cel mai din stanga nod din acest subarbore drept
		y = x->parinte;
		nr_atribuirii;
		
		nr_comparatii++;
		while( (y != NULL) && (x == y->dreapta))
			{
				nr_comparatii = nr_comparatii+2;
				x = y;
				y = y->parinte;
				nr_atribuirii = nr_atribuirii+2;
			}
		return y;

	}

/*TIP_NOD *CONSTRUIRE_ARBORE(int stg, int dr)
	{
		TIP_NOD *ds; // ds - dimensiunea subarborelui stang
		TIP_NOD *dd; // dd - dimensiunea subarborelui drept
		int m;
		if( stg > dr)
				return 0;
		if( stg == dr)
				return CONSTRUIRE_NOD(stg, 1, NULL, NULL);
		else
			{
				m = (stg + dr)/2;
				ds = CONSTRUIRE_ARBORE(stg, m-1);
				dd = CONSTRUIRE_ARBORE(m+1, dr);
				return CONSTRUIRE_NOD(m, dr-stg+1, ds, dd);
			}	
		}*/

TIP_NOD *CONSTRUIRE_ARBORE(int stg, int dr, int *dim)
	{
		TIP_NOD *x; 
		int n;
		n=sizeof(TIP_NOD); //aloc spatiu de memorie
		x=(TIP_NOD*)malloc(n);
		int dim1 = 0, dim2 = 0;
	
		if( stg > dr)
			{
				*dim = 0;
				nr_atribuirii++;
				return 0;
			}
		else
			{
				x->val = (stg+dr)/2; //m
				x->stanga = NULL;
				x->dreapta = NULL;
				x->dimensiune = dr-stg+1;
				nr_atribuirii = nr_atribuirii+4;
				if(stg < dr)
					{
						x->stanga =  CONSTRUIRE_ARBORE(stg, (stg+dr)/2-1,&dim1);
						nr_comparatii++;
						if(x->stanga != NULL)
							{
								x->stanga->parinte = x;
								nr_atribuirii++;
							}
						x->dreapta = CONSTRUIRE_ARBORE((stg+dr)/2+1,dr,&dim2);
						nr_atribuirii++;
						nr_comparatii++;
						if(x->dreapta != NULL)
							{	
								x->dreapta->parinte = x; 
								nr_atribuirii++;
							}
			}
	      }
  *dim =x->dimensiune;
   nr_atribuirii++;
   return x;
}


void reface_dimensiune(TIP_NOD *p)  //procedura care reface dimensiunea la stergere
{
	while (p!=NULL)
	{
	 nr_comparatii++;
	 p->dimensiune--;
	 p=p->parinte;
	 nr_atribuirii++;
	}
}

TIP_NOD *SO_SELECTEAZA(TIP_NOD* x, int i)
	{
	
		int rang; 
		nr_atribuirii++;

		nr_comparatii++;
		if(x->stanga != NULL)
				rang = x->stanga->dimensiune+1;	//x->stanga->dimensiune + 1- rangul lui x in subarbore avand radacina x
			else
				rang = 1;

			if (rang == i)
				return x;
		else
			{
				if(i < rang)
						return SO_SELECTEAZA(x->stanga,i);
				else
						return SO_SELECTEAZA(x->dreapta,i-rang);
			}
	}


void STERGERE_NOD(TIP_NOD **rad,TIP_NOD* z)  //stergerea unui nod z dintr-un arbore
	{
		TIP_NOD *x;
		TIP_NOD *y;
		nr_comparatii = nr_comparatii + 2;
		if((z->stanga == NULL) || (z->dreapta == NULL))
			{
				y=z;
				nr_atribuirii++;
			}
		else
			y = ARBORE_SUCCESOR(z); // se determina nodul care va fi sters
			nr_atribuirii++;

		nr_comparatii++;
		if(y->stanga != NULL)
			{
				x = y->stanga;
				nr_atribuirii++;
			}
		else
			{
				x = y->dreapta;
				nr_atribuirii++;
			}
		nr_comparatii++;
		if(x != NULL)
			{
				x->parinte = y->parinte;
				nr_atribuirii++;
			}
		
	   reface_dimensiune(y->parinte);

		nr_comparatii++;
		if(y->parinte == NULL)
			{
				*rad=x;
				nr_atribuirii++;
			}

		else
		{
			nr_comparatii++;
			if(y == y->parinte->stanga)
				{
					y->parinte->stanga = x;
					nr_atribuirii++;
				}
			else
				{
					y->parinte->dreapta = x;
					nr_atribuirii++;
				}
		}
			nr_comparatii++;
			if (y != z)
				{
					z->val = y->val;
					nr_atribuirii++;
				}
			 free(y);
	}

void inordine(TIP_NOD *x, int nivel)
{
  int i;
  if (x!=NULL)
	{
	  inordine(x->stanga,nivel+1);
	  for(i=0;i<=nivel;i++) 
	  printf("  ");
	  printf("%d\n",x->val);
	  inordine(x->dreapta,nivel+1);
	}
}

void JOSEPHUS(int n, int m)
{
	
	int i,j,dim;
	TIP_NOD *x;
	radacina=CONSTRUIRE_ARBORE(1, n, &dim);
	radacina->parinte = NULL;
	j=1;

	for (i=n;i>0;i--)
	{
		inordine(radacina,0);
		printf("\n--------------------\n");
		j=((j+m-2)%i)+1;
		x=SO_SELECTEAZA(radacina, j);
		//printf("\n %d", x->val);
		STERGERE_NOD(&radacina, x);
	}
}


int main()
{
	JOSEPHUS(10,4);
	/*FILE *fp;
	int n;
	fp = fopen("josephus.csv", "w");
	fprintf(fp, "n, operatii\n");
		nr_atribuirii = 0;
				nr_comparatii = 0;
		for(n=100;n<=10000;n+=100)
			{

				JOSEPHUS(n,n/2);
			
				fprintf(fp,"%d,%d \n",n,nr_atribuirii+nr_comparatii);
				printf("%d \n", n);

			}
	fclose(fp);*/
	getch();
}	