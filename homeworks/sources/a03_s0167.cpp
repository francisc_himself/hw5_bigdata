

//***ANONIM*** ***ANONIM*** Daniela gr ***GROUP_NUMBER***

//In cazul mediu si favorabil quicksort are eficienta de O(n logn),iar in cazul defavorabil O(n2)
//In cazul mediu curba de la heapsort creste mai repede decat la quicksort ceea ce inseamna ca
//quicksort este mai eficient in acest caz.
//La algoritmul de sortate quicksort ,curba de la cazul mediu defavorabil creste cel mai rapid 
//fiind urmata de cea de la cazul favorabil si cazul  mediu

#include<time.h>
#include<cstdlib>
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>


int n,dim_heap;
int v[30];
int heap_opmed=0,quick_opmed=0,quick_opfav=0,quick_opdef=0;

//heapsort 
int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}

void max_heapify(int a[],int i)
{
	int l,r,max,aux;
	l=left(i);
	r=right(i);
     heap_opmed++;
	if(l<=dim_heap && a[l]>a[i])
		max=l;
	else
		max=i;
	  heap_opmed++;
	if(r<=dim_heap && a[r]>a[max])
		max=r;
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		heap_opmed+=3;
		max_heapify(a,max);
	}
}

void build_max_heap(int a[])
{
	dim_heap=n;
	for(int i=n/2;i>=1;i--)
		max_heapify(a,i);
}

void heapSort(int a[])
{
	int aux;
	build_max_heap(a);
	for(int i=n;i>=2;i--)
	{
		heap_opmed+=3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		dim_heap--;
		max_heapify(a,1);
	}
}

//quicksort
int partition(int a[],long st,int dr)
{
	int j,aux;
	quick_opmed++;
    int pivot=a[dr];
	int i=st-1;
	for(j=st;j<=dr-1;j++)
	{
		quick_opmed++;
		if(a[j]<=pivot)
		{
			i++;
			quick_opmed+=3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	quick_opmed+=3;
	aux=a[i+1];
	a[i+1]=a[dr];
	a[dr]=aux;
	return i+1;

}
void quickSort(int a[],int st,int dr)
{
	int q;

	if(st<dr)
	{
		q=partition( a, st, dr);
		quickSort(a,st,q-1);
		quickSort(a,q+1,dr);
	}
}


void citire(int a[],int n)
{
	for(int i=1;i<=n;i++)
	{
		printf("Valoarea  a[%d]  este: ",i);
		scanf("%d",&a[i]);
		v[i]=a[i];//retinem si in alt vector pentru a testa fiecare algoritm pe 
		         //aceleasi valori
	}	
}




int main()
{  
	//testam pe un exemplu
	int a[30];
	n=6;
	
	citire(a,n);
	printf("\n Vectorul sortat cu  heapsort este: \n");
	heapSort(a);
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);


	printf("\n Vectorul sortat cu quickpsort este: \n");
	quickSort(v,1,n);
	   for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	
	//generam fisierul
	//srand(time(NULL));
	//int a[10000],b[10000];
	//int worst[10000],best[10000];
  //  FILE *f=fopen("comparare2.csv","w");
	//fprintf(f,"nr , quicksort favorabil, quicksort defavorabil, heapsort  mediu,quicksort  mediu,  \n");
//	for( n=100;n<=10000;n+=100)
	//{
	//	heap_opmed=0;
	//	quick_opmed=0;
   //     quick_opfav=0;
		//quick_opdef=0;
		

	//	for(int j=1;j<n;j++)
	//	{
	///		worst[j]=n-j;
	//		best[j]=j; 
	//	}
	//	best[n]=n/2;
	//	quickSort(best,1,n);
	//	quick_opfav=quick_opmed;
	//	quick_opmed=0;
	//	quickSort(worst,1,n);
	//	quick_opdef=quick_opmed;
	//	quick_opmed=0;
	//	for(int k=1;k<=5;k++)
	//	{
			
	//		for(int j=1;j<=n;j++)
	//		{
	//			a[j]=rand();
	//			b[j]=a[j];
		//	}
	
		//	heapSort(a);
		//	quickSort(b,1,n);
		//}
		
		//fprintf(f,"%d,%d,%d,%d,%d\n",n,quick_opfav,quick_opdef,heap_opmed/5,quick_opmed/5);
//
//	}
	
	
	getch();
	return 0;

}