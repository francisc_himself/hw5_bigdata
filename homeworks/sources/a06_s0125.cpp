/*
Student: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
College: Technical University of Cluj Napoca
Date: 30 March 2013
Subject: Josephus Permutation in BST
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

static int comp=0;
static int assig=0;
static int n=0;
ofstream myfile;


struct TreeNode{
        TreeNode* parent;
        TreeNode* left;
        TreeNode* right;
        int size;
        int value;
};

TreeNode* T;


TreeNode* buildTree(int l, int r, TreeNode* prnt)
{
        TreeNode* root = NULL;
        if(r-l>=0){
                root = new TreeNode;
                root->value = (l+r)/2; //assig++;
                //printf("%d ", root->value);
                root->parent = prnt;
                root->size = r-l+1; //assig++;
                root->left = buildTree(l, (l+r)/2-1, root);
                root->right = buildTree((l+r)/2+1, r, root);
        }
        return root;
}

TreeNode* OS_Select( TreeNode* x, int i){
        int r = 1;
        if (x->left!=NULL){ comp++;
                r = x->left->size + 1; assig++;}
        if (i==r)
                return x;
        else if (i<r) {//printf("\n Going left ");
        return OS_Select(x->left, i);}
        else {//printf("\n Going right ");
        return OS_Select(x->right, i-r);}
}


void OS_Delete(TreeNode* root)
{
                //case 0: Deleting the root.
        TreeNode* target = NULL; comp++; comp++; comp++;
                if (root->parent == NULL && root->left == NULL && root->right == NULL){ 
                         assig++; free(root); root=NULL; T=NULL; return;
                }

        //case 1: No children.
				comp++; comp++;
        if (root->left == NULL && root->right == NULL){ 
                                        target = root; 
                                        while (target->parent!=NULL)
                                        {
                                                target->parent->size-=1; target=target->parent; assig++;
                                        }

                                
                if (root->parent->left == root) 
				{root->parent->left = NULL; assig++;}
                                else {
                                        root->parent->right = NULL; assig++;
                                }
                                return;
        }

        //case 2: One child.
		comp++;comp++;
        if (root->left !=NULL && root->right ==NULL) //on left
                {
                target = root->left;

                root->value = target->value; assig++;
                root->size = target->size;
                root->right = target->right;
                root->left = target->left;

				comp++;
                                if (target->left!=NULL){assig++; target->left->parent = root;}
								comp++;
                                if (target->right!=NULL){assig++; target->right->parent = root;}

                target = NULL;

                target = root; 
                                while (target->parent!=NULL){
                                        target->parent->size-=1; assig++;
                                        target=target->parent;
                                }
                return;
                                }

        if (root-> right != NULL && root->left ==NULL){ //on right
                target = root->right;

                root->value = target->value; assig++;
                root->size = target->size;
                root->left = target->left;
                root->right = target->right;
				comp++;
                                if (target->left!=NULL){target->left->parent = root; assig++;}
								comp++;
                                if (target->right!=NULL){target->right->parent = root; assig++;}
                target = NULL;
                target = root; 
                                while (target->parent!=NULL){
                                        target->parent->size-=1; assig++;
                                        target=target->parent;
                                }
                return;
        }


        //case 3: Two children.
                comp++;comp++;
        if (root->left !=NULL && root->right != NULL)
                {
                                //TreeNode* ductTape = root->right->right;
                                //TreeNode* ductTape2 = root->left;
                target = root->right;
				comp++;
                while (target->left != NULL)  //find inorder succ
                                {
                                        target = target->left; assig++;
                                }
                                TreeNode* toDel = target;
                root->value = target->value; //swap value
				assig++;


                                if (target->right!=NULL) {
                                        target->right->parent = target->parent; assig++;//relink the tree in case a right subtree exists on succ
                                        
										if (target->parent-> left == target){
									target->parent->left = target->right; assig++;}
										else if (target->parent->right == target){target->parent->right = target->right; assig++;}
                                }

                while (target->parent!=NULL){target->parent->size-=1; assig++; target=target->parent;} //adjust size
                                
                                
                                if (toDel->parent->left == toDel) toDel->parent->left = NULL; else if (toDel->parent->right == toDel) toDel->parent->right = NULL;
								assig++;
                                free(toDel);

                return;
                }
}

void gloriousPrint(TreeNode *root, int start){
        start++;
        if(root!=NULL){
            gloriousPrint(root->right, start);
        
                        for (int i= 0 ; i<= start; i++)
                        {
                                        cout << "    ";
                        }
                        cout << root->value << "(" << root->size<< ")" << endl;
                        
            gloriousPrint(root-> left, start);
                }
}

void Josephus(int n, int m){
T = buildTree(1,n,NULL);
int j=1;
//gloriousPrint(T,0);
//printf("\n"); printf("\n");
for (int k=n; k>=1; k--){
        j = ((j+m-2)%k) + 1; assig++;
        TreeNode* x = OS_Select(T, j);
        //printf("%d to be removed \n \n", x->value);
        OS_Delete(x);
//if (T!=NULL) {printf("\n"); printf("\n");
//} //gloriousPrint(T,0);}
}
}


void main()
{
        //TreeNode* xa;
        //xa = buildTree(1, 7, NULL);
        //gloriousPrint(xa);
        //_getch();
       // Josephus(2000,1000);
      //  _getch();

myfile.open ("josephus.csv",ios::app);
for (n=100; n<10000; n=n+100){
	assig=0; comp=0;
Josephus(n, (int)n/2);
myfile << n << "," << assig << "," << comp << endl;
printf("%d", n);
}

myfile.close();
}
