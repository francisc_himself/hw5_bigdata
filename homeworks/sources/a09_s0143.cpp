#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N_MIN_NODURI 100
#define N_MAX_NODURI 200
#define N_MIN_MUCHII 1000
#define N_MAX_MUCHII 5000

int nrOperatii;

typedef struct lista_adiacenta
{
    int vecin;
    struct lista_adiacenta *urm;
    struct lista_adiacenta *prec;
} lista_adiacenta;

typedef struct nod_graf
{
    lista_adiacenta *lista;
    int culoare;
    int distanta;
    struct nod_graf *precedent;
} nod_graf;

typedef struct que
{
    int nr;
    struct que* urm;
}que;

void enqueue (que **Q, int s)
{
    if(*Q == NULL)
    {
        *Q = (que*)malloc(sizeof(que));
        (*Q)->nr = s;
        (*Q)->urm = NULL;
    }
    else
        enqueue(&(*Q)->urm, s);
}

int denqueue (que **Q)
{
    if(*Q != NULL)
    {
        que *aux = *Q;
        int aux1;
        *Q = (*Q)->urm;
        aux1 = aux->nr;
        free(aux);
        return aux1;
    }
    else
        return -1;
}

void BFS(nod_graf *graf[], int nr_noduri, int nod)
{
    int i, u;
    lista_adiacenta *aux;
    for(i = 1; i <= nr_noduri; i++)
    {
        nrOperatii = nrOperatii + 3;
        graf[i]->culoare = 0;
        graf[i]->distanta = nr_noduri;
        graf[i]->precedent = NULL;
    }
    graf[nod]->culoare++;
    graf[nod]->distanta = 0;
    que *Q = NULL;
    nrOperatii = nrOperatii + 2;
    enqueue(&Q, nod);
    while(Q != NULL)
    {
        u = denqueue(&Q);
        aux = graf[u]->lista;
        while(aux != NULL)
        {
            nrOperatii = nrOperatii + 1;
            if(graf[aux->vecin]->culoare == 0)
            {
                nrOperatii = nrOperatii + 3;
                graf[aux->vecin]->culoare++;
                graf[aux->vecin]->distanta = graf[u]->distanta + 1;
                graf[aux->vecin]->precedent = graf[u];
                enqueue(&Q, aux->vecin);
            }
            aux = aux->urm;
        }
    }
}

void dealocare_memorie_lista_adiacenta_nod(nod_graf **nod)
{
    lista_adiacenta *aux = (*nod)->lista;
    while (aux != NULL)
    {
        while(aux->urm != NULL)
        {
            aux = aux->urm;
        }
        //verific daca nu cumva am de-aface cu primul element
        //daca nu
        if (aux->prec != NULL)
        {
            aux->prec->urm = NULL;
            free(aux);
            aux = (*nod)->lista;
        }
        //daca da
        else
        {
            free(aux);
            (*nod)->lista = NULL;
            aux = NULL;
        }
    }
}

int creare_muchie(nod_graf **nod, int muchie)
{
    lista_adiacenta *aux = (*nod)->lista, *aux1;
    if (aux != NULL)
    {
        while(aux->urm != NULL)
        {
            if(aux->vecin == muchie)
                return 0;
            aux = aux->urm;
        }
        aux1 = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
        aux1->urm = NULL;
        aux1->prec = aux;
        aux1->vecin = muchie;
        aux->urm = aux1;
    }
    else
    {
        aux1 = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
        aux1->urm = NULL;
        aux1->prec = NULL;
        aux1->vecin = muchie;
        (*nod)->lista = aux1;
    }
    return 1;
}

void preety_print(nod_graf *graf[], int nr_noduri)
{
    int i, j;
    for(i = 1; i <= nr_noduri; i++)
    {
        for(j = 1; j <= graf[i]->distanta; j++)
            printf("\t");
        printf("%d\n", i);
    }
}

int main()
{
    nod_graf *graf[N_MAX_NODURI];
    //n - nr noduri, m - nr muchii
    int n, m, i, j, vecin, ok;
    FILE *f = fopen("rezultate.csv", "w");
    fprintf(f, "V,E,nrOp\n");

    srand(time(NULL));
    /***EXEMPLU***/
    for(i = 1; i <= N_MAX_NODURI; i++)
    {
        graf[i] = (nod_graf*)malloc(sizeof(nod_graf));
        graf[i]->culoare = 0;
        graf[i]->lista = NULL;
    }
    /***initializare graf***/
    graf[1]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[1]->lista->vecin = 2;
    graf[1]->lista->urm = NULL;
    graf[1]->lista->prec = NULL;

    graf[2]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[2]->lista->vecin = 1;
    graf[2]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[2]->lista->prec = NULL;
    graf[2]->lista->urm->vecin = 3;
    graf[2]->lista->urm->urm = NULL;
    graf[2]->lista->urm->prec = graf[2]->lista;

    graf[3]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[3]->lista->vecin = 2;
    graf[3]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[3]->lista->prec = NULL;
    graf[3]->lista->urm->vecin = 4;
    graf[3]->lista->urm->urm = NULL;
    graf[3]->lista->urm->prec = graf[3]->lista;

    graf[4]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[4]->lista->vecin = 3;
    graf[4]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[4]->lista->prec = NULL;
    graf[4]->lista->urm->vecin = 5;
    graf[4]->lista->urm->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[4]->lista->urm->prec = graf[4]->lista;
    graf[4]->lista->urm->urm->vecin = 6;
    graf[4]->lista->urm->urm->urm = NULL;
    graf[4]->lista->urm->urm->prec = graf[4]->lista->urm;

    graf[5]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[5]->lista->vecin = 4;
    graf[5]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[5]->lista->prec = NULL;
    graf[5]->lista->urm->vecin = 6;
    graf[5]->lista->urm->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[5]->lista->urm->prec = graf[5]->lista;
    graf[5]->lista->urm->urm->vecin = 8;
    graf[5]->lista->urm->urm->urm = NULL;
    graf[5]->lista->urm->urm->prec = graf[5]->lista->urm;

    graf[6]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[6]->lista->vecin = 4;
    graf[6]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[6]->lista->prec = NULL;
    graf[6]->lista->urm->vecin = 5;
    graf[6]->lista->urm->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[6]->lista->urm->prec = graf[6]->lista;
    graf[6]->lista->urm->urm->vecin = 7;
    graf[6]->lista->urm->urm->urm = NULL;
    graf[6]->lista->urm->urm->prec = graf[6]->lista->urm;

    graf[7]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[7]->lista->vecin = 5;
    graf[7]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[7]->lista->prec = NULL;
    graf[7]->lista->urm->vecin = 6;
    graf[7]->lista->urm->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[7]->lista->urm->prec = graf[7]->lista;
    graf[7]->lista->urm->urm->vecin = 8;
    graf[7]->lista->urm->urm->urm = NULL;
    graf[7]->lista->urm->urm->prec = graf[7]->lista->urm;

    graf[8]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[8]->lista->vecin = 7;
    graf[8]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[8]->lista->prec = NULL;
    graf[8]->lista->urm->vecin = 5;
    graf[8]->lista->urm->urm = NULL;
    graf[8]->lista->urm->prec = graf[8]->lista;
    /***initializare graf***/
    n = 8;
    BFS(graf, n, 3);
    preety_print(graf, n);
    for(i = 1; i <= n; i++)
    {
        //printf("nod: %d distanta: %d\n", i, graf[i]->distanta);
        dealocare_memorie_lista_adiacenta_nod(&graf[i]);
    }
    /***EXEMPLU***/

    n = 100;
    m = N_MIN_MUCHII;
    for(m = N_MIN_MUCHII; m <= N_MAX_MUCHII; m = m + 100)
    {
        nrOperatii = 0;
        for(i = 1; i <= n; i++)
        {
            for(j = 1; j <= m/n; j++)
            {
                vecin = rand()%n+1;
                if(vecin != i)
                {
                    ok = creare_muchie(&graf[i], vecin);
                    if(ok == 0)
                    {
                        j--;
                    }
                }
                else
                    j--;
            }
        }
        BFS(graf, n, rand()%(n-1)+1);
        //printf("\n\n");
        //preety_print(graf, n);
        for(i = 1; i <= n; i++)
        {
            //printf("nod: %d distanta: %d\n", i, graf[i]->distanta);
            dealocare_memorie_lista_adiacenta_nod(&graf[i]);
        }
        fprintf(f, "%d,%d,%d\n", n, m, nrOperatii);
    }
    m = 9000;
    for(n = 100; n <= 190; n = n + 10)
    {
        nrOperatii = 0;
        for(i = 1; i <= n; i++)
        {
            for(j = 1; j <= m/n; j++)
            {
                vecin = rand()%n+1;
                if(vecin != i)
                {
                    ok = creare_muchie(&graf[i], vecin);
                    if(ok == 0)
                    {
                        j--;
                    }
                }
                else
                    j--;
            }
        }
        BFS(graf, n, rand()%(n-1)+1);
        //printf("\n\n");
        //preety_print(graf, n);
        for(i = 1; i <= n; i++)
        {
            //printf("nod: %d distanta: %d\n", i, graf[i]->distanta);
            dealocare_memorie_lista_adiacenta_nod(&graf[i]);
        }
        fprintf(f, "%d,%d,%d\n", n, m, nrOperatii);
        printf("%d\n", n);
    }

    return 0;
}
