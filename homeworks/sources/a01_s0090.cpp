//============================================================================
// Name        : ThreeSort.cpp
// Author      : crc
// Version     :
// Copyright   : Your copyright notice
//
// Description : Three Sort in C++, Ansi-style
// This program compares three different Sorting Algorithms: Bubble Sort, Insertion Sort, Selection Sort.
// For each algorithms we count Assignment and Comparison Operations.
// We perform computations for three different cases: Best Case, Worst Case, Average Case.
// Result analysis:
// For Best Case:
//		Selection sort performs much more Comparisons than Insertion and Bubble Sort 
//              => Quadric equation for Selection and equally linear equations for Bubble and Insertion Sort
//      All of these three algorithms doesn't perform any Assignment operations => constant equations
//      Overall: Bubble Sort and Insertion sort are equally more efficient than Selection Sort 
// For Average Case:
//		Bubble Sort and Selection Sort perform comparable number of Comparison while Insertion is more efficient
//				=> quadric equations
//		Bubble Sort and Insertion Sort perform comparable number of Assignments while Selection is MUCH more efficient
//				=> quadric equations for Bubble and Insertion and linear for Selection
//		Overall: Selection is the best algorithm for Average Case, followed by Insertion, followed by Bubble
// For Worst Case:
//		All three algorithms perform comparable equal number of comparisons => quadric equation
//      Selection sort performs much fewer assignments than both Bubble and Insertion 
//				=> linear equation for Selection for Selection and quadric equations for Bubble and Insertion
//      Overall: Bubble Sort and Insertion Sort are equally more inefficient then Selection Sort
//
// Conclusions: I would choose Selection algorithm because it behaves muche better than both Insertion and Bubble
//              sorting algorithms for both average and worst cases. Even if for best case is the worst choice, 
//				in practice these situations are extremely rare.
//==========================================================================================================

#include "stdAfx.h"
#include "Profiler.h"
using namespace std;

int A[10000];

Profiler p("sort");

void selectionSort(int A[], int n);
void insertionSort(int A[], int n);
void bubbleSort(int A[], int n);
void display(int A[], int n);
void bestCase();
void worstCase();
void averageCase();


int main() {

	//bestCase();
	averageCase();
	//worstCase();
	
	return 0;
}

void selectionSort( int initA[], int n){
	int i, j, min, aux;
	int A[10000];
	for (i=0; i<n; i++){
		A[i] = initA[i];
	}

	for (i = 0; i < n - 1; i++) {
		min = i;
		for (j = i + 1; j < n; j++){
			p.countOperation("SelectionSort Comparisons",n, 1);
			//search for element with smallest value
			if (A[j] < A[min]){
				min = j;
			}
		}
		//interchange current element with smallest one
		if (min != i) {
			aux = A[i];
			A[i] = A[min];
			A[min] = aux;
			p.countOperation("SelectionSort Assignments", n, 3);
		}
	}
}

void insertionSort( int initA[], int n){
	int i, j, aux;
	int A[10000];
	for (int i = 0; i<n; i++){
		A[i] = initA[i];
	}
	for (i = 1; i < n; i++) {
		j = i;
		while (j > 0 && A[j - 1] > A[j]) {
			//interchange each two consecutive elements which are in decreasing order
			p.countOperation("InsertionSort Comparisons", n, 1);
			aux = A[j];
			A[j] = A[j - 1];
			A[j - 1] = aux;
			j--;
			p.countOperation("InsertionSort Assignments", n, 3);
		}
		p.countOperation("InsertionSort Comparisons", n, 1);
	}
}

void bubbleSort(int initA[], int n){
	int A[10000];
	bool ok = true;
	int j = 0;
	int aux;
	//create copy;
	for (int i = 0; i<n; i++){
	    A[i] = initA[i];
	}

	while (ok){
		ok = false;
		j++;
		for (int i = 0; i< n-j; i++){
			p.countOperation("BubbleSort Comparisons", n);
			//search for consecutive elements which are in decreasing order
			if (A[i] > A[i+1]) {
				//swap elements
				aux = A[i];
				A[i] = A[i+1];
				A[i+1] = aux;
				ok = true;
				p.countOperation("BubbleSort Assignments", n, 3);
			}
		}

	}
	//display(A, n);

}

void display(int A[], int n){
	int i;
	for (i=0; i<n; i++){
		printf("%i ", A[i]);
	}
	printf("\n");
}

void bestCase(){
	int n;
	for(n=100; n<10001; n+=100){
		p.countOperation("BubbleSort Assignments", n, 0);
		p.countOperation("BubbleSort Comparisons", n, 0);
		p.countOperation("InsertionSort Comparisons", n, 0);
		p.countOperation("InsertionSort Assignments", n, 0);
		p.countOperation("SelectionSort Comparisons",n, 0);
		p.countOperation("SelectionSort Assignments", n, 0);
		
		for (int i = 0; i<n; i++){
			A[i] = i;
		}
		//best case = all elements are in increasing order
		selectionSort(A, n);
		bubbleSort(A, n);
		insertionSort(A, n);
	}

	p.addSeries("BubbleSort Total", "BubbleSort Comparisons", "BubbleSort Assignments");
	p.addSeries("InsertionSort Total", "InsertionSort Comparisons", "InsertionSort Assignments");
	p.addSeries("SelectionSort Total", "SelectionSort Comparisons", "SelectionSort Assignments");

	p.createGroup("Best Case Comparison Operations", "BubbleSort Comparisons", "InsertionSort Comparisons", "SelectionSort Comparisons");
	p.createGroup("Best Case Assignment Operations", "BubbleSort Assignments", "InsertionSort Assignments", "SelectionSort Assignments");
	p.createGroup("Best Case Total Operations", "BubbleSort Total", "InsertionSort Total", "SelectionSort Total");
	p.createGroup("Best Case Total Selection Sort Operations", "InsertionSort Total");


	p.showReport();
}

void worstCase(){
	int n;
	//worst case for bubble/insertion sort
	for(n=100; n<10001; n+=100){
		p.countOperation("BubbleSort Assignments", n, 0);
		p.countOperation("BubbleSort Comparisons", n, 0);
		p.countOperation("InsertionSort Comparisons", n, 0);
		p.countOperation("InsertionSort Assignments", n, 0);
		p.countOperation("SelectionSort Comparisons",n, 0);
		p.countOperation("SelectionSort Assignments", n, 0);

		for (int i=0; i<n; i++){
			A[i] = n - i;
		}
		bubbleSort(A, n);
		insertionSort(A, n);

		//worst case for selection sort
		for (int i=1; i<n; i++){
			A[i] = i;
		}
		A[0] = n;
		selectionSort(A, n);

	}

	p.addSeries("BubbleSort Total", "BubbleSort Comparisons", "BubbleSort Assignments");
	p.addSeries("InsertionSort Total", "InsertionSort Comparisons", "InsertionSort Assignments");
	p.addSeries("SelectionSort Total", "SelectionSort Comparisons", "SelectionSort Assignments");

	p.createGroup("Worst Case Comparison Operations", "BubbleSort Comparisons", "InsertionSort Comparisons", "SelectionSort Comparisons");
	p.createGroup("Worst Case Assignment Operations", "BubbleSort Assignments", "InsertionSort Assignments", "SelectionSort Assignments");
	p.createGroup("Worst Case Assignment Selection Sort Operations", "SelectionSort Assignments");
	p.createGroup("Worst Case Total Operations", "BubbleSort Total", "InsertionSort Total", "SelectionSort Total");

	p.showReport();
}

void averageCase(){
	int n;
	for(n=100; n<10001; n+=100){
		p.countOperation("BubbleSort Assignments", n, 0);
		p.countOperation("BubbleSort Comparisons", n, 0);
		p.countOperation("InsertionSort Comparisons", n, 0);
		p.countOperation("InsertionSort Assignments", n, 0);
		p.countOperation("SelectionSort Comparisons",n, 0);
		p.countOperation("SelectionSort Assignments", n, 0);
		for(int i=0; i<5; i++){
			// repeat counting for 5 different arrays
			FillRandomArray(A, n, 0, 30000, false, 0);
			bubbleSort(A, n);
			selectionSort(A, n);
			insertionSort(A, n);
		}
	}
	p.addSeries("BubbleSort Total", "BubbleSort Comparisons", "BubbleSort Assignments");
	p.addSeries("InsertionSort Total", "InsertionSort Comparisons", "InsertionSort Assignments");
	p.addSeries("SelectionSort Total", "SelectionSort Comparisons", "SelectionSort Assignments");

	p.createGroup("Average Case Comparison Operations", "BubbleSort Comparisons", "InsertionSort Comparisons", "SelectionSort Comparisons");
	p.createGroup("Average Case Assignment Operations", "BubbleSort Assignments", "InsertionSort Assignments", "SelectionSort Assignments");
	p.createGroup("Average Case Assignment Selection Sort Operations", "SelectionSort Assignments");
	p.createGroup("Average Case Total Operations", "BubbleSort Total", "InsertionSort Total", "SelectionSort Total");

	p.showReport();
}

