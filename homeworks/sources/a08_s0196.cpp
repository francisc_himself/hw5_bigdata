
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

/*Fiecare nod din graf este reprezentat de un nod, care are campurile cheie, dimensiunea listei din care face parte, reprezentant dat de catre capul listei
din care face parte si nodul urmator din lista*/

typedef struct nod{
	int cheie;
	int dimensiune;
	struct nod *reprezentant;
	struct nod *urm;
}NOD;

typedef struct edge{
	int p;
	int d;
}edge;

NOD	*prim[10000],*ultim[10000];
int m,t,j;
edge e[60000];
bool matr[10000][10000]; //matr reprezinta matricea de adiacenta a grafului neordonat generat aleatoriu

//filtru genereaza un numar intreg aleator intre 0 si 10000
int filtru() {
	return  rand() % 10000 ;
}

//generare_graf realizeaza generarea unui graf neorientat cu numarul de muchii dat de parametrul z
void generare_graf(int z)
{
	memset(matr,0,sizeof(matr));
	int contor=0;					
	while(contor<z)
	{	int nr1 =filtru();
		int nr2 =filtru();
		if((matr[nr1][nr2]==0) && (nr1!=nr2))
		{
			matr[nr1][nr2]=1;
			matr[nr2][nr1]=1;
			e[contor].p=nr1;
			e[contor].d=nr2;
			contor++;
		}
	}
}

//find_set returneaza reprezentantul unui nod, care are cheia egala cu parametrul x al functiei
int find_set(int x)
{   m++;
	return prim[x]->reprezentant->cheie;
}

//make_set creeaza o multime noua cu un singur element x, care este si reprezentant
void make_set(int x)
{   m++;
	prim[x]=(NOD*)malloc(sizeof(NOD));
	prim[x]->cheie=x;
	prim[x]->dimensiune=1;
	prim[x]->reprezentant=prim[x];
	ultim[x]=prim[x];
}

/*reuneste doua multimi disjuncte,adaugand la finalul listei cu dimensiunea mai mare nodurile listei cu dimensiunea mai mica
si setand pentru nodurile mutate ca si reprezentant reprezentantul listei mai lungi. 
*/
void link(int x, int y)
{	NOD *p;
	if(prim[x]->dimensiune >= prim[y]->dimensiune)
	{   
		ultim[x]->urm=prim[y];
		prim[x]->dimensiune+=prim[y]->dimensiune;
		ultim[x]=ultim[y];
		p=prim[y];
		for(int i=0;i<prim[y]->dimensiune;i++)
		{
			p->reprezentant=prim[x];
			p=p->urm;
		}
	}
	else	
	{	
		ultim[y]->urm=prim[x];
		prim[y]->dimensiune+=prim[x]->dimensiune;
		ultim[y]=ultim[x];
		p=prim[x];
		for(int i=0;i<prim[x]->dimensiune;i++)
		{ p->reprezentant=prim[y];
		  p=p->urm;
		}
	}
}

//reuneste doua multimi disjuncte
void unio(edge t)
{   m++;
	link(find_set(t.p),find_set(t.d));
}

//componenta_conexa determina componentele conexe ale unui graf neorientat
void componenta_conexa(int t, edge e[])
{  
	for(j=0;j<10000;j++)    //initializam varfurile
	{	make_set(j);
	}
	for(int i=0;i<t;i++)	//pentru fiecare muchie a grafului verificam daca varfurile ei se afla in aceeasi multime
	{						//in caz contrar, reunim multimile din care acestea fac parte.		
		if((find_set(e[i].p))!=(find_set(e[i].d))) 
		{
			unio(e[i]);
		}
	}
}

void main()
{	FILE *pf;
pf=fopen("fis.txt","w");
srand ( time(NULL) );
for(int g=10000;g<=60000;g+=1000)
{	generare_graf(g);
	m=0;
	componenta_conexa(g,e);
	fprintf(pf,"%d		%d		 \n",g,m);
	for(int u=0;u<10;u++)
		free(prim[u]);
}
fclose(pf);	
}
