//Name: ***ANONIM*** ***ANONIM***
//Group: ***GROUP_NUMBER***
//Date: 08 march 2013
//laboratorul 3 heap-uri
/*scopul acestui laborator este testarea eficientei construirii structurilor de tip heap cu cele 2 metode: TopDown si
BottomUP.

Teoria spune ca metoda Top Down are eficienta O(n*log n) iar metoda Bottom Up are eficienta O(n). in urma rularii programului
se constata acelasi lucru, vizibil pe grafic. numarul de operatii (comparatii + asignar) este putin mai ridicat la TopDown decat
la BottomUp.

La ce ne foloseste structura de heap? structura de heap are prorpietatea ca nodul i este mai mic decat fii sai, cu indicii
2*i si 2*i+1. de aici deducem ca structurile de heap pot fi folosite cu usurinta in sortari, si de asemenea sunt foarte eficiente.

*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//dimensiunea maxima a intervalului de testat
#define MAX_SIZE 10000
//dimensiunea minima a intervalului de testat
#define MIN_SIZE 100
//incrementul
#define INC 100

//functie ce genereaza un vector de n elemente aleator
void creareVect(int vect[], int n){
	int i;
	for (i=1; i<=n; i++){
		vect[i]=rand() % 20;
	}
}

//functie ce generaza un vector de n elemente aleator, dar se poate crea acelasi vector de mai multe ori
void creareVector(int vect[], int n, int srandc){
	int i;
	srand(srandc);
	for (i=1; i<=n; i++){
		vect[i]=rand() % 100;
	}
}

// genereaza un vector ordonat crescator
void creareVectorFav(int vect[], int n){
	int i;
	for (i=1; i<=n; i++){
		vect[i]=i;
	}
}

//genereaza un vector ordonat descrescator
void creareVectorDef(int vect[], int n){
	int i;
	for (i=1; i<=n; i++){
		vect[i]=n-i;
	}
}

// parintele lui i
int p(int i){
	return i/2;
}

// fiul din stanga al lui i
int st(int i){
	return i*2;
}

// fiul din dreapta al lui i
int dr(int i){
	return i*2+1;
}

// reconstruieste structura de heap pe ramura din arbore ce are ca radacina nodul i
// la fiecare pas determina minimul dintre radacina, fiu stanga, fiu dreapta, si il pune la radacina, urmand sa apeleze
// functia mai departe, pe fiul cu care s-a schimbat radacina
void reconstituieHeap(int vect[], int i, int dim, double *operatii){
	int l, r, min, aux;

	l = st(i);
	r = dr(i);

    // testeaza fiul din stanga
	if(l <= dim && vect[l] < vect[i])
	{
		min = l;
	}
	else
	{
		min = i;
	}

    // testeaza fiul din dreapta
	if( r <= dim && vect[r] < vect[min])
	{
		min = r;
	}
	*operatii += 2;

    // daca minimul nu e pe radacina
	if(min != i)
	{
		aux = vect[i];
		vect[i] = vect[min];
		vect[min] = aux;

		*operatii += 3;

		reconstituieHeap(vect, min, dim, operatii);
	}
}

// initializeaza vectorul de heap
void Hinit(int *dim){
    int i;
    (*dim) = 0;
}

// adauga un element la structura de heap
// il punem ultimul in lista si il mutam in sus pana i gasim pozitia potrivita
void Hpush(int vect[], int *dim, int x, double *operatii){
    int i;
    *dim = *dim + 1;
    vect[*dim] = x;
    *operatii = *operatii + 1;
    i = *dim;
    while((i>1)&&(x<vect[p(i)])){
            vect[i] = vect[p(i)];
            *operatii = *operatii + 2;
            i = p(i);
    }
    vect[i] = x;
    *operatii = *operatii + 1;
}

// scoatem nodul radacina, adica minimul, punem ultimul element din vector pe prima pozitie, si reconstruim heapul
int Hpop(int vect[], int *dim, double *operatii){
    int min;
    if (*dim<=0){
        return -1;
    }
    min = vect[1];
    *operatii = *operatii + 1;
    vect[1] = vect[*dim];
    *operatii = *operatii + 1;
    *dim = *dim - 1;
    reconstituieHeap(vect,1,*dim,operatii);
    return min;
}

// pentru fiecare nod care nu e frunza, recostruim heapul
void construiesteHeapBU(int vect[], int dim, double *operatii){
	int i;
	*operatii = 0;
	for(i = p(dim); i>=1; i--){
		reconstituieHeap(vect,i,dim,operatii);
	}
}

// luam o structura auxiliara si tot adaugam elemente la ea, si reconstruim heapul la fiecare pas
void construiesteHeapTD(int vect[], int *dim, double *operatii){
    int aux[MAX_SIZE+1];
    int i, dimA;
    (*operatii) = 0;
    dimA = *dim;
    Hinit(dim);
    for (i = 1; i <=dimA; i++){
        Hpush(aux, dim, vect[i], operatii);
    }
    for(i = 1; i<=dimA; i++){
        vect[i] = aux[i];
        *operatii = *operatii + 1;
    }
}

// afiseaza heapul intr-o structura user-friendly
void afisareHeap(int vect[], int dim, int i, int depth){
	int level;
	if(i>dim){
	return;
	}
	afisareHeap(vect,dim,2*i+1,depth+1);
	for(level=0; level<depth; level++)
		printf("    ");
	printf("%d\n",vect[i]);
	afisareHeap(vect,dim,2*i,depth+1);

}


int main(){
	int vector[MAX_SIZE+1];
	int n = 10,i;
	double operatii,avgoperatii;
	FILE *heapfile;
	heapfile = fopen("heap.csv","w");
	fprintf(heapfile,"n,operatiiTopDown,operatiiBottomUp\n");

	for (n = MIN_SIZE; n<=MAX_SIZE; n+=INC){
        printf("%d\n",n);

        // numaram cazul mediu statistic pentru constructia TopDown
        avgoperatii = 0;
        for(i=1;i<6;i++){
            creareVector(vector,n,i);
            construiesteHeapTD(vector,&n,&operatii);
            avgoperatii += operatii;
        }
        avgoperatii /= 5;
        fprintf(heapfile,"%d,%f,",n,avgoperatii);

         // numaram cazul mediu statistic pentru constructia BottomUP
        avgoperatii = 0;
        for(i=1;i<6;i++){
            creareVector(vector,n,i);
            construiesteHeapBU(vector,n,&operatii);
            avgoperatii += operatii;
        }
        avgoperatii /= 5;
        fprintf(heapfile,"%f\n",avgoperatii);

	}

    fclose(heapfile);
}
