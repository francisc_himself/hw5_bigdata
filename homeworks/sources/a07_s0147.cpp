#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct node{
	int val;
	node *copil;
	int nrc;
};

struct nodee{
	int val;
	nodee *first,*last;
};

node *noduri;
int root;

void Transformata1(int *t,int n)
{
	noduri=new node[n];
	int pos[100],i;

	for(i=0;i<n;i++)
	{
		noduri[i].nrc=0;
		noduri[i].copil=NULL;
	}

	for(i=0;i<n;i++)
		if(t[i]>=0)
			noduri[t[i]].nrc++;
		else
			root=i;

	for(i=0;i<n;i++)
		if(noduri[i].nrc>0)
			noduri[i].copil=new node[noduri[i].nrc];

    for(i=0; i<100; i++)
        pos[i]=0;

	for(i=0;i<n;i++)
	{
		noduri[i].val=i;
		if(t[i]>=0)
		{
			noduri[t[i]].copil[pos[t[i]]]=noduri[i];
			pos[t[i]]++;
		}
	}

}

void Transformata2(nodee *current, int rooth)
{
	if(noduri[rooth].nrc>0)
	{

		current->first=new nodee;
		current->first->first=NULL;
		current->first->last=NULL;
		current->first->val=noduri[rooth].copil[0].val;

		nodee *son=current->first;


		for(int i=1;i<noduri[rooth].nrc;i++)
		{
			son->last=new nodee;
			son->last->last=NULL;
			son->last->first=NULL;
			son->last->val=noduri[rooth].copil[i].val;
			son=son->last;
		}
	}


	if(current->first!=NULL)
		Transformata2(current->first,current->first->val);

	if(current->last!=NULL)
		Transformata2(current->last,current->last->val);
}

void Preety_Print(nodee *r,int spatii)
{
	int i;

	for(i=1;i<=spatii;i++)
		printf(" ");

	printf("%d\n",r->val);

	if(r->first!=NULL)
		Preety_Print(r->first,spatii+1);

	if(r->last!=NULL)
		Preety_Print(r->last,spatii);
}

int main()
{
    int i;
	int n=9,t[9]={1, 6, 4, 1, 6, 6, -1, 4, 1};
	nodee *rootB;
	printf("Arborele intial :\n");
for(i=0; i<9; i++)
    printf("%d ",t[i]);
    for(i=0; i<9; i++)
    if(t[i]==-1)
    printf("\nRadacina arborelui este: %d ",i);

	Transformata1(t,n);

	rootB=new nodee;
	rootB->last=NULL;
	rootB->first=NULL;
	rootB->val=root;
	Transformata2(rootB,root);
    printf("\nArborele binar :\n");
	Preety_Print(rootB,0);
	return 0;
}
