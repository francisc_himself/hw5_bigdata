#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

using namespace std;

typedef struct Node
{
	int id;
	char *color;
	Node **child;
}Node;

typedef struct qNode
{
	int info;
	qNode *next;
}qNode;

typedef struct Queue
{
	qNode *head, *tail;
}Queue;

Queue *Q;

void enqueue(Node* x)
{
	qNode *p;
	p=(qNode*)malloc(sizeof(qNode));
	p->info=x->id;
	p->next=NULL;

	if(Q->head==NULL)
	{
		Q->head=p;
		Q->tail=p;
	}
	else
	if(Q->tail!=NULL)
	{
		Q->tail->next=p;
		Q->tail=p;	
	}
}


int dequeue()
{
	qNode *p;
	int x=-1;
	if(Q->head!=NULL)
	{
		p=Q->head;
		Q->head=Q->head->next;
		x=p->info;
		free(p);
		if(Q->head==NULL)
		{
			Q->tail=NULL;
		}
	}
	else 
	{
		p=NULL;
	}
	return x;
}

Node *v[200];

void connect(Node *x, Node*y)
{
	int i=0;
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	child=x->child;
	while(child[i]!=NULL)
		i++;
	child[i]=(Node*)malloc(sizeof(Node));
	child[i]=y;
	i++;
	child[i]=NULL;
	x->child=child;
}

void print()
{
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	printf("\n");
	for(int i = 0; i<6; i++)
	{
		printf(" node %d is %s  ", i, v[i]->color);
		child = v[i]->child;
		printf("\n");
	}
}

void BFS(int n, Node* s)
{
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	int u;
	
	s->color="GRAY";
	print();
	
	Q=(Queue*)malloc(sizeof(Queue));
	Q->head=NULL;
	Q->tail=NULL;
	enqueue(s);
	
	while(Q->head!=NULL)
	{
		u=dequeue();
		child=v[u]->child;
		for(int i=0;child[i]!=NULL;i++)
		{
			if(child[i]->color=="WHITE")
			{
				child[i]->color="GRAY";
				enqueue(child[i]);
				print();
			}
		}
		v[u]->color="BLACK";
		v[u]->child=child;
		print();
	}
}

void test()
{
	for(int i = 0; i<6;i++)
	{
		v[i]=(Node*)malloc(sizeof(Node));
		v[i]->child=(Node**)malloc(10*sizeof(Node*));
		v[i]->id=i;
		v[i]->child[0]=NULL;
		
	}
	connect(v[0],v[1]);
	connect(v[0],v[3]);
	connect(v[0],v[4]);
	connect(v[1],v[4]);
	connect(v[2],v[4]);
	connect(v[2],v[5]);
	connect(v[4],v[5]);

	for(int i=0; i<6; i++)
	{
		v[i]->color="WHITE";
	}
	print();
	for(int i=0;i<6;i++)
	{
		if(v[i]->color=="WHITE")
			BFS(6, v[i]);
	}
}

void main()
{
	test();
	getch();
}