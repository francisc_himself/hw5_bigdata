#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>

typedef struct nod{
	int key;
	int dim;
	struct nod *parinte;
	struct nod *left;
	struct nod *right;
}NOD;

NOD *rad=NULL;
int atrib=0,comp=0;

//construire arbore binar echilibrat cu elemente de la min la max
NOD *construire_arbore(int st,int dr)
{
	NOD *p;
	int m,dims=0,dimd=0;
	if(st<=dr)
	{
		m=(st+dr)/2;
		p=(NOD *)malloc(sizeof(NOD));
		p->left=construire_arbore(st,m-1);
		p->right=construire_arbore(m+1,dr);
		if(p->left!=NULL)
		{
			dims=p->left->dim;
			p->left->parinte=p;
		}
		if(p->right!=NULL)
		{
			dimd=p->right->dim;
			p->right->parinte=p;
		}
		p->dim=dims+dimd+1;
		p->key=m;
		return p;
	}
	return NULL;
}

//selecteaza nodul de rang j din arbore
NOD *os_select(NOD *rad,int j)
{
	int rang=1;

	comp++;
	if(rad->left!=NULL)
	{
		rang=rad->left->dim+1;
		atrib++;
	}

	if(j==rang)
		return rad;
	else
	{
		if(j<rang)
			return os_select(rad->left,j);
		else
			return os_select(rad->right,j-rang);
	}
}

//extrage minimul din partea stanga a nodului x
NOD * tree_minim(NOD *x)
{
	comp++;
	while(x->left!=NULL)
	{ 
		comp++;
		atrib++;
		x=x->left;
	}
	return x;
}

//muta nodul v pe pozitia nodului u, si reface legaturile cu celelalte noduri
void transplant(NOD *u,NOD *v)
{
	comp++;
	if(v!=NULL)
		v->dim=u->dim;
	comp++;
	if(u->parinte==0)
	{
		rad=v;
		atrib++;
	}
	else
	{	
		comp++;
		if(u==u->parinte->left)
		{
			u->parinte->left=v;
			atrib++;
		}
		else
		{
			u->parinte->right=v;
			atrib++;
		}
	}
	comp++;
	if(v!=NULL)
	{
		v->parinte=u->parinte;
		atrib++;
	}

}

//sterge nodul z (aduce pe pozitia sa un alt nod, pe care il sterge)
void os_delete(NOD *z)
{
	NOD *y;
	comp++;
	if(z->left==NULL)
		transplant(z,z->right);
	else
	{
		comp++;
		if(z->right==NULL)
			transplant(z,z->left);
		else
		{
			y=tree_minim(z->right);
			comp++;
			if(y->parinte!=z)
			{
				transplant(y,y->right);
				atrib+=2;
				y->right=z->right;
				y->right->parinte=y;
			}
			transplant(z,y);
			atrib+=2;
			y->left=z->left;
			y->left->parinte=y;
		}
	}
}

void pretty_print(NOD *r)
{
	if(r!=NULL)
	{
		pretty_print(r->left);
		printf("\n");
		for(int i=1;i<r->dim;i++)
			printf(" ");
		printf("%d",r->key);
		pretty_print(r->right);
	}
}

void josephus(int n, int m)
{
	int j=1;
	NOD *x,*y;

	rad=(NOD *)malloc(sizeof(NOD));
	rad=construire_arbore(1,n);
	rad->parinte=0;
	pretty_print(rad);
	printf("\n\n");

	for(int k=n;k>=1;k--)
	{
		j=(j+m-2)%k+1;
		x=os_select(rad,j);
		y=x;
		atrib++;
		y->dim--;
		while(y->parinte!=0)
		{
			atrib++;
			comp++;
			y=y->parinte;	
			y->dim--;
		}
		os_delete(x);
		pretty_print(rad);
		printf("\n\n");
	}
}

void generare_test()
{
	int n,m;
	FILE *f=fopen("rez.csv","w");
	fprintf(f,"n,m,atrib,comp,operatii\n");
	for(int n=100;n<500;n+=100)
	{
		m=n/2;
		atrib=0;
		comp=0;
		josephus(n,m);
		fprintf(f,"%d,%d,%d,%d,%d\n",n,m,atrib,comp,atrib+comp);
	}
}

void main()
{
	int n=7,m=3;
	josephus(n,m);
	//generare_test();
	getch();
}