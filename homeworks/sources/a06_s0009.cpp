#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>



typedef struct tree
{
	int poz;
	int dim;
	struct tree *stg;
	struct tree *dr;
	struct tree *par;
}NOD;


NOD *rad;
int comp,attr;


NOD *construire_arbore_echilibrat(int stg,int dr)
{
	int m;
	int dim_stg,dim_dr;
	
	NOD *p;
	attr++;
	p=(NOD *)malloc(sizeof(NOD));

	comp++;
	if (stg>dr)
	{
		attr++;
		p->dim=0;
		return NULL;
	}
	else
	{
		m=(stg+dr)/2;
		
		attr=attr+4;
		p->poz=m;
		p->stg=NULL;
		p->dr=NULL;

		p->stg=construire_arbore_echilibrat(stg,m-1);
		comp++;
		if(p->stg != NULL)
			{
			p->stg->par = p; 
			attr++;
			}

		attr++;
		p->dr=construire_arbore_echilibrat(m+1,dr);
		comp++;
		if(p->dr != NULL)
			{
			p->dr->par =p; 
			attr++;
			}

		attr=attr+2;
		dim_stg = 0;
		dim_dr=0;

		comp++;
		if(p->stg){
			attr++;
			dim_stg = p->stg->dim;
		}

		comp++;
		if(p->dr){
			attr++;
			dim_dr = p->dr->dim;
		}
		attr++;
		p->dim=dim_stg+dim_dr+1;
		return p;
	}

};



void afis(char ch, int n)
{
	int i;
	for (i=0; i<n; i++)
			putchar(ch);
}

void pretty_print(NOD *rad, int level)
{
	comp++;
	if (rad==NULL)
	{
		afis('\t',level);
		puts(" ");
	}

	else
	{
		pretty_print(rad->dr,level+1);
		afis('\t',level);
		printf("%d (%d)\n",rad->poz,rad->dim);
		pretty_print(rad->stg,level+1);
	}
}


NOD *os_select(NOD *x,int i)
{
	int r=1;
	comp++;
	if (x->stg!=NULL)
	{
		r=(x->stg->dim)+1;
		attr++;
	}


	comp++;
	if (i==r)
			return x;
	else 
	{
		comp++;
		if (i<r)
				return os_select(x->stg,i);
		else return os_select(x->dr,i-r);
	}
}


NOD *tree_min(NOD *x)
{
	comp++;
	while (x->stg!=NULL)
	{
		comp++;
		attr++;
		x=x->stg;
	}
	return x;
}

NOD *tree_success(NOD *x)
{
	NOD *y;
	comp++;
	if (x->dr!=NULL)
		return tree_min(x->dr);
	attr++;
	y=x->par;
	comp=comp+2;
	while (y!=NULL && x==y->dr)
	{
		comp=comp+2;
		attr=attr+2;
		x=y;
		y=y->par;
	}
	return y;
}

	
void os_delete(NOD *z)
{ 
  NOD *y,*x,*parz;
  comp=comp+2;
  if ( (z->stg==NULL) || (z->dr==NULL) )
     { 
		 y=z; 
		 attr++;
     }
     else
	 {
		 y=tree_success(z); 
		 attr++;
	 }
  
  comp++;
  if (y->stg!=NULL)
  { 
	  x=y->stg; 
	 attr++;
  }
     else
  {	 
	  x=y->dr; 
	 attr++;
  }

  comp++;
  if (x!=NULL)
     {   
		 attr++;
		 x->par=y->par;
     }
  
	attr++;
	comp++;
  	parz=y->par;
	while(parz!=NULL)
	{
		comp++;
		attr++;
		attr++;
		parz->dim--;
		parz=parz->par;
	}

  comp++;
  if (y->par==NULL)
  {
	  rad=x;
	  attr++;
  }
     else
	 {
		 comp++;
	 if (y==y->par->stg)
	 { 
		 y->par->stg=x; 
		attr++;
	 }
	    else
	{ 
		y->par->dr=x; 
		attr++;
	 }
  }
  comp++;
  if (y!=z)
     {  
		 attr++;
		 z->poz=y->poz;
     }
  free(y);
}





void josephus(int n,int m)
{
	
	int j=1;
	int k;
	NOD *x,*y;
	attr++;
	rad=construire_arbore_echilibrat(1,n);
	pretty_print(rad,0);
	attr++;
	rad->par=NULL;

	for (k=n; k>=1; k--)
	{
		j=((j+m-2)% k)+1;
		attr++;
		x=os_select(rad,j);
		printf("Cheie x=%d\n",x->poz);
		os_delete(x);
		printf("\n\n\n\n");
		pretty_print(rad,0);
	}
}

			


int main()
{
	/*FILE *fis;

	fis=fopen("josephus.csv","w");
	fprintf(fis,"n,Suma\n");

	for (int n=100; n<10000; n=n+100)
	{
		rad=NULL;
		josephus(n,n/2);
		fprintf(fis,"%d,%d\n",n,attr+comp);
		attr=0;
		comp=0;
	}

	fclose(fis);*/

	
	 josephus(7,3);
	 

	 getch();
	 return 0;
}