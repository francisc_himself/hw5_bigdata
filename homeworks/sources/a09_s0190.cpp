#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>

using namespace std;

int atrib=0;
int comp=0;

struct nod
{
    int inf;
    int no_edges;
    int adj[1000];
    int pred;
    int d;
    int color;

};

struct coada
{
	int inf;
	coada *urm;
};


coada *prim;
coada *ultim;


void enqueue(int el)
{
	//cream elementul
	coada *nCoada;
	nCoada = (coada*) malloc(sizeof(coada));
	
	//punem valoarea
	nCoada->urm=NULL;
	nCoada->inf=el;

	if(ultim==NULL)
	{
		prim=ultim=nCoada;
	}
	else
	{
		ultim->urm=nCoada;
		ultim=nCoada;
	}
}

int dequeue()
{
	coada *p;
	int el;

	if(prim==NULL)
	{
		return 0;
	}
	else
		if(prim->urm==NULL)
		{
			el = prim->inf;
			p=prim;
			prim=ultim=NULL;
			delete p;
		}
		else
		{
			el=prim->inf;
			p=prim;
			prim=prim->urm;
			delete p;
		}

		return el;
}

void bfs(nod *s, int n, nod *sir[])
{

	for(int i=0;i<n;i++)
	{
													atrib=atrib+3;
		sir[i]->color=0;
		sir[i]->pred=0;
		sir[i]->d=100;
	}
													atrib=atrib+3;
	sir[s->inf]->color=1;
	sir[s->inf]->pred=0;
	sir[s->inf]->d=0;

	enqueue(s->inf);

	int u, noe;
	int k;
													comp++;
	while(prim)
	{
		u=dequeue();
		noe = sir[u]->no_edges;

		printf("%d ",sir[u]->inf);
		for(int j=0;j<noe;j++)
		{
			k = sir[u]->adj[j];
													comp++;
			if(sir[k]->color==0)
			{
													atrib=atrib+3;
				sir[k]->color=1;
				sir[k]->d=sir[u]->d+1;
				sir[k]->pred=u;
				enqueue(k);
			}
													atrib++;
			sir[u]->color=2;
		}
	}

}




int main()
{
	coada *prim;
	coada *ultim;
	nod *sir[10000];
	nod *nodNou;

	prim = (coada*) malloc(sizeof(coada));
	ultim = (coada*) malloc(sizeof(coada));
	
	prim=NULL;
	ultim=NULL;

	//----------------------------------------------------------------

	int nrUnghiuri=10;
	

	for(int i=0;i<nrUnghiuri;i++)
	{
		nodNou = (nod*) malloc(sizeof(nod));
		nodNou->inf=i;

		nodNou->no_edges=rand()%(10 - 0) + 0;
		for(int j=0;j<nodNou->no_edges;j++)
		{
			
			nodNou->adj[j]=rand()%(10 - 0) + 0;
		}

		sir[i]=nodNou;
	
	}

	bfs(sir[0],nrUnghiuri,sir);

	//----------------------------------------------------------------


	/*int nrNoduri=100;
	int nrMuchi=1000;
	int i;
	FILE *out;
	out=fopen("iesire.csv","w");
	fprintf(out,"atrib,comp,sum\n");
	atrib=0;
	comp=0;

	while(nrMuchi<=5000)
	{
		atrib=0;
		comp=0;
		//calculam aproximativ numarul de muchi per nod
		int nrMuchiMed=nrMuchi/nrNoduri;

		//creez/recreez nodurile
		for(i=0;i<nrNoduri;i++)
		{
			nodNou = (nod*) malloc(sizeof(nod));
			nodNou->inf=i;
			
			//cream la fiecare nod nrMuchiMed muchii
			for(int j=0;j<nrMuchiMed;j++)
			{
				//ii generam o latura
				nodNou->adj[j]=rand()%(100 - 0) + 0;
				//printf("%d",nodNou->adj[j]);
				
				//verificam daca exista deja
				int ok=0;

				for(int k=0;k<nodNou->no_edges;k++)
				{

					if(sir[nodNou->adj[j]]->adj[k]==nodNou->adj[j])
						ok++;
				}

				if(ok>0)
					j--;
			}
			
			nodNou->no_edges=nrMuchiMed;
			sir[i]=nodNou;
		}

		 bfs(sir[0],nrNoduri,sir);
		 fprintf(out,"%d,%d,%d\n",atrib,comp,atrib+comp);

		 nrMuchi=nrMuchi+100;
	}
	fclose(out);*/

	//----------------------------------------------------------------

	/*int nrNoduri=100;
	int nrMuchi=9000;
	int i;
	FILE *out;
	out=fopen("iesire.csv","w");
	fprintf(out,"atrib,comp,sum\n");
	atrib=0;
	comp=0;


	while(nrNoduri<=200)
	{
		printf("+");
		atrib=0;
		comp=0;
		//calculam aproximativ numarul de muchi per nod
		int nrMuchiMed=nrMuchi/nrNoduri;

		//creez/recreez nodurile
		for(i=0;i<nrNoduri;i++)
		{
			nodNou = (nod*) malloc(sizeof(nod));
			nodNou->inf=i;
			
			//cream la fiecare nod nrMuchiMed muchii
			for(int j=0;j<nrMuchiMed;j++)
			{
				//ii generam o latura
				nodNou->adj[j]=rand()%(100 - 0) + 0;
				//printf("%d",nodNou->adj[j]);
				
				//verificam daca exista deja
				int ok=0;

				for(int k=0;k<nodNou->no_edges;k++)
				{

					if(sir[nodNou->adj[j]]->adj[k]==nodNou->adj[j])
						ok++;
				}

				if(ok>0)
					j--;
			}
			
			nodNou->no_edges=nrMuchiMed;
			sir[i]=nodNou;
		}

		bfs(sir[1],nrNoduri,sir);
		fprintf(out,"%d,%d,%d\n",atrib,comp,atrib+comp);
		nrNoduri=nrNoduri+10;
	}
	fclose(out);*/
}
