#include <iostream>
#include <stdio.h>
#include <conio.h>

void BubbleSort ( int a[], int n){
	int i, j, temp;
	for ( i = n-1 ; i > 0 ; i--)
	{
		for ( j=1; j <= i; j++ )
		{
			if ( a[j-1] > a[j] )
			{
				temp = a[j-1];
				a[j-1] = a[j];
				a[j] = temp;
			}
		}
	}
}

void InsertionSort ( int a[], int n ){
	int i, temp, j;
	for ( i = 1; i < n; i++ )
	{
		temp = a[i];
		j = i-1;
		while ( temp < a[j] && j >= 0)
		{
			a[j+1] = a[j];
			j = j-1;
		}
		a[j+1] = temp;
	}
}

void SelectionSort ( int a[], int n ){
	int i, j, min, temp;
	for ( i = 0; i < n-1; i++)
	{
		min = i;
		for ( j = i+1; j < n; j++)
			if ( a[j] < a[min])
				min = j;
		if ( min != 1 )
		{
			temp = a[i];
			a[i] = a[min];
			a[min] = temp;
		}
	}
}

int main(){
	int n=5,i;
	int a[] = {20, 5, 8, 10, 2};
	BubbleSort (a, 5);
	printf ("Bubblesort: ");
	for ( i = 0; i < n; i++)
	{
		printf ("%d ", a[i]);
	}
	InsertionSort (a, 5);
	printf ("\nInsertionSort: ");
	for ( i = 0; i < n; i++)
	{
		printf ("%d ", a[i]);
	}
	SelectionSort (a, 5);
	printf ("\nSelectionSort: ");
	for ( i = 0; i < n; i++)
	{
		printf ("%d ", a[i]);
	}
}
	

