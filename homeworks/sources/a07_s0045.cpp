/****ANONIM*** ***ANONIM***, ***GROUP_NUMBER***, Assign7
	Pentru repezentarea multicai am folosit un nod special cu alocare dinamica, deci stocarea fiilor nu
necesita memorie suplimentara de marime statica. Nodul pe langa cheia propriu-zisa mai are campurile:
count-numarul fiilor, capacity-capacitatea actuala a blocului de memorie alocat pentru stocarea adreselor
fiilor(nr fii adaugati+nr locuri libere), un poiter child spre un bloc de memorie care contine adresele
fiilor(poiter spre pointer). Daca am folosi in loc de dublu pointer pointer simplu spre un bloc de memorie cu
multinoduri, operatiile cu arbori ar necesita o copiere a continutui(! nu a adresei) nodului cauzand cod greu de intretinut
si eventuale inconsistente.
	Pentru reprezentarea binara am folosit un nod "obisnuit" cu cheia, adresa fiului stang si drept. Adresa 0
reprezinta lipsa fiului respectiv.
	Algoritmii de transformare sunt de cimplexitate O(n). Transformarea in arbore multicai este format din doua parcurgeri
a vectorului de tati: O((n*(1+1))+(n*1))=O(n). Convertirea in reprezentarea binara este un algoritm recursiv-
aplicand teorema master iese eficienta dorita.
	Algoritmul de transformare multicai mai necesita o memorie aditionala in timpul apelului functiei:
avem nevoie de un bloc de memorie unde pastram temporar adresele nodurilor nou create pana la refacerea legaturilor.
Marimea memoriei implicata este: sizeof(adresa unui nod) * numarul maxim de noduri(constanta globala).
*/
#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 500 //numarul maxim de noduri

typedef struct _multi_node
{
	int key;
	int count;
	int capacity;
	_multi_node **child;
}m_node;

typedef struct _binary_node
{
	int key;
	_binary_node *left, *right;
}binary_node;

binary_node *create_binary_node(int key)
{
	binary_node *nod=(binary_node *)malloc(sizeof(binary_node));
	nod->key=key;
	nod->left=0;
	nod->right=0;
	return nod;
}

m_node *createMN(int key)
{
	m_node *nod=(m_node *)malloc(sizeof(m_node));
	nod->count=0;
	nod->capacity=0;
	nod->child=0;
	nod->key=key;

	return nod;
}

//fc auxiliara
void print_dateMN(m_node *nod)
{
	printf("\nkey=%d",nod->key);
	printf("\ncount=%d",nod->count);
	printf("\ncapacity=%d",nod->capacity);
	printf("\nchild address=%d\n",nod->child);
}

void insertMN(m_node *parent, m_node *child)
{
	if (parent->count >= parent->capacity)
	{
		parent->capacity= (2*parent->capacity == 0) ? 1 : 2*parent->capacity;

		parent->child=(m_node**) realloc (parent->child, parent->capacity * sizeof(m_node *));

		if (parent->child == 0)
		{
			perror("Error allocating memory at insertMN!");
			exit(1);
		}
	}

	parent->child[(parent->count)++]=child;
}

m_node *transform_multi_node(int t[], int size)
{
	m_node *v[MAX_SIZE];
	int index_rad=-1;

	for (int i=0; i<size; i++)
	{
		v[i]=createMN(i);
		if (t[i] == -1)
		{		
			index_rad=i;
		}
	}

	for (int i=0; i<size; i++)
	{
		if (t[i] != -1)
		{
			insertMN(v[t[i]],v[i]);
		}
	}

	return v[index_rad];
}

binary_node *transform_binary_node(m_node *rad_m)
{
	if (rad_m == 0)
	{
		return 0;
	}

	binary_node *rad_b=create_binary_node(rad_m->key);

	if (rad_m->count > 0)
	{
		rad_b->left=transform_binary_node(rad_m->child[0]);
		binary_node *p=rad_b->left;

		for (int i=1; i<rad_m->count; i++)
		{
			p->right=transform_binary_node(rad_m->child[i]);
			p=p->right;
		}
	}

	return rad_b;
}

void pritty_print_child_(m_node *rad, int nivel)
{
	if (rad == NULL)
	{
		return;
	}

	for (int i=0; i<nivel; i++)
	{
		printf("   ");
	}
	printf("%d\n", rad->key);

	for (int i=0; i<rad->count; i++)
		pritty_print_child_(rad->child[i],nivel+1);
}

void pritty_print_child(m_node *rad, int nivel)
{
	printf("-------------------------------------------------------\n");
	printf("Multi-way representation:\n\n");
	pritty_print_child_(rad,nivel);
	if (rad == 0)
	{
		printf("pritty_print: Arbore vid!\n");
	}
	printf("-------------------------------------------------------\n");
}

void pritty_print_binary_(binary_node *rad, int nivel)
{
	if (rad == NULL)
	{
		return;
	}

	for (int i=0; i<nivel; i++)
	{
		printf("   ");
	}
	printf("%d\n", rad->key);

	binary_node *p=rad->left;

	while (p != 0)
	{
		pritty_print_binary_(p, nivel+1);
		p=p->right;
	}
}

void pritty_print_binary(binary_node *rad, int nivel)
{
	printf("-------------------------------------------------------\n");
	printf("Binary representation:\n\n");
	pritty_print_binary_(rad,nivel);
	if (rad == 0)
	{
		printf("pritty_print: Arbore vid!\n");
	}
	printf("-------------------------------------------------------\n");
}

void print_parent_vector(int t[], int size)
{
	printf("-------------------------------------------------------\n");
	printf("Parent representation:\n\n");
	for (int i=0; i<size; i++)
	{
		printf("%d ", t[i]);
	}
	printf("\n");
	printf("-------------------------------------------------------\n");
}

int main()
{
	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(t[0]);
	m_node *rad;
	binary_node *rad_b;

	print_parent_vector(t, size);

	rad=transform_multi_node(t,size);
	pritty_print_child(rad,1);

	rad_b=transform_binary_node(rad);
	pritty_print_binary(rad_b, 1);

	return 0;
}