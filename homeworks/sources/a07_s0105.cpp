#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/*
The requirement of this assignment is to implement correctly and efficiently linear time transformations between three different representations for a multi-way tree:
	R1: parent representation: for each key you are given the parent key, in a vector.
	R2: multi-way tree representation: for each node you have the key and a vector of children nodes
	R3: binary tree representation: for each node, you have the key, and two pointers: one to the first child node,
		and one to the brother on the right (i.e. the next brother node)
Initially, for each key(in this case: from 1-9) is given the parent key in  a vector called 'p'
For the multiway representation, a structure containing the key, the number of children, and a vector of children for each key is used.
For the binary representation is used a structure containing the key, a pointer to the left child, and another one to his right brother.

*/

//parent representation: parent key in a vector
int p[10]={0,2,7,5,2,7,7,-1,5,2};
int nr[11];

//structure for a multiway representation:key+vector of children nodes
typedef struct nodeM
{
	int key;
	int nr_children;
	nodeM **children;
}nodeR2;


//structure for a binary representation
typedef struct nodeB
{
	int key;
	nodeB *firstChild,*rightBrother;
}nodeR3;

//finds the root
int find_root(int n)
{
	int x;
	for(int i=1;i<n;i++)
		if(p[i]==-1)  
			 x=i;
	return x;
}

void pretty_print(nodeR3 *nod, int spaces)
{	
	for(int i=0; i<spaces; i++)
		printf(" ");
	printf("%d\n",nod->key);

	if(nod->firstChild!=NULL)
		pretty_print(nod->firstChild,spaces+2);
	if (nod->rightBrother != NULL)
		pretty_print(nod->rightBrother,spaces);

}



nodeR2 *multi=new nodeR2[9];
//T1: transformation from parent representation to a multiway tree representation
void T1(int n)
{
	int root;

	for (int i=1; i<n; i++) { //initializes each node with the key and sets the nr of children to zero 
        multi[i].key= i;
		multi[i].nr_children=0;
    }
	
	for(int i=1; i<n; i++){  //increases the nr of children; i is the child of p[i]
		int aux=p[i];
		if(p[i]!=-1)
			multi[aux].nr_children=multi[aux].nr_children+1;  
	}

	for(int i=1;i<n;i++){
		if(multi[i].nr_children!=0)   //in case the node has children
			 multi[i].children=(nodeR2**)malloc(multi[i].nr_children*sizeof(nodeR2));   //allocate memory, proportional with the nr of children 
			else
			  multi[i].children=NULL;  //otherwise set the children to NULL
		}

	 for (int i=1; i<n; i++) {
		if (p[i]!=-1){
			nr[p[i]]++;
			multi[p[i]].children[nr[p[i]]]=&multi[i];  //link each children to his parent
		}
	}
	printf("Multiway representation:\n");

	printf("Number of children: ");
	for(int i=1; i<n; i++)
		printf("%d  ",multi[i].nr_children);

	printf("\n\n");
	for (int i=1;i<n;i++){  
		if(multi[i].nr_children!=0){
			printf("%d parent of ",multi[i].key);
		for(int j=1;j<=multi[i].nr_children;j++)
				 printf("%d ",multi[i].children[j]->key);
	}
	else  printf("%d is a leaf ",multi[i].key);
	printf("\n");
    }
	printf("\n\n");
	
	
}


nodeR3 *BT=new nodeR3[9];
//T2: transformation from parent representation to a multiway tree representation
void T2(int n)
{
    for (int i=1; i<n; i++)  //initialize each node with key and set the pointers to childs NULL
    {
       BT[i].key = i;
	   BT[i].firstChild = NULL;
	   BT[i].rightBrother = NULL;
    }

    for (int i=1; i<n; i++)  
    {
		if(multi[i].nr_children>0 && multi[i].children[1]!=NULL)    //if the node has at least one child 
				 BT[i].firstChild = &BT[multi[i].children[1]->key];   //link the firstchild to the corresponding node
       else
            BT[i].firstChild = NULL;  

       if (BT[i].firstChild != NULL) 
				for(int j=2;j<=multi[i].nr_children;j++)
					if (multi[i].children[j] != NULL)   //if that node has more children, link the rightbrother to the node
						BT[multi[i].children[j-1]->key].rightBrother = &BT[multi[i].children[j]->key];     
            
    }
	
	printf("Binary representation:\n");
	printf("\n");
	for (int i=1;i<n;i++)
	{  printf("node: %d has as",BT[i].key);
	if (BT[i].firstChild!=NULL)
		printf(" first child %d ",BT[i].firstChild->key);
	if (BT[i].rightBrother!=NULL)
		printf(", right brother %d ",BT[i].rightBrother->key);
	printf("\n");
	}
}



int main()
{
	
	int n=10;

	//parent representation
	printf("Parent representation:\n");
	for (int i=1;i<n;i++)
	{
		if(p[i]==-1)
			printf("%d is the root\n",i);
		else
				printf("%d is child of %d\n",i,p[i]);		
	}
	printf("\n\n");
	//multiway representation
	
	T1(n);
	printf("\n");
	//printf("\n%d -------------------",c[5].children[2]->key);

	//binary tree representation;
	T2(n);
	printf("\n\n");
	int r=find_root(n);
	pretty_print(&BT[r],2);
	
	getch();
	return 0;
}


