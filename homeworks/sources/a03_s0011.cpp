/* ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***
Tema: Compararea algoritmilor HeapSort si QuickSort in cazul mediu statistic si a QuickSort-ului in cazul favorabil si defavorabil
In cazul mediu statistic, ambele metode au complexitatea O(n*log2(n)).
In cazul favorabil, QuickSort-ul are complexitate O(n*log2(n)), iar in cazul defavorabil O(n^2).
*/

#include<iostream>
#include<conio.h>
#include<fstream>

#define MAX_SIZE 10000

int a[10001],b[10001],c[10001], heap_size,l,dr,st;
int op_h=0, op_q=0;
using namespace std;
ofstream fout("date.csv");


//////////pt HEAPSORT
void max_heapify(int a[], int i, int heap_size)
{
	int ind; //indexul maxim
	op_h++;
	if((2*i<=heap_size)&&(a[2*i]>a[i]))
		ind=2*i;
	else
		ind=i;
	op_h++;
	if(((2*i+1)<=heap_size) &&(a[2*i+1]>a[ind]))
		ind=2*i+1;
	if(ind!=i)
	{
		op_h=op_h+3;
		int aux=a[i];
		a[i]=a[ind];
		a[ind]=aux;
		max_heapify(a,ind, heap_size);
	}
}

void heap_bottom_up(int a[], int heap_size)
{
	for(int i=heap_size/2;i>0;i--)
		max_heapify(a,i, heap_size);
}

void heap_sort(int a[], int heap_size)
{
	heap_bottom_up(a, heap_size);
	for(int i=heap_size;i>=2;i--)
	{
		op_h=op_h+3;
		int aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		heap_size--;
		max_heapify(a,1, heap_size);
	}
}
//////pt QUICKSORT
int partitionare(int a[],int st,int dr)
{
	//swap  intre dr si (dr+st)/2 si atunci apelam exact la fel
	op_q++;
	int x=a[dr]; //pt cazul mediu si defavorabil
	//int x=a[(st+dr)/2]; //pt cazul favorabil
	int i=st-1;
	for(int j=st;j<=dr-1;j++)
	{
		op_q++;
		if(a[j]<=x)
		{
			i++;
			op_q=op_q+3;
			int aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	op_q=op_q+3;
	int aux1=a[i+1];
	a[i+1]=a[dr];
	a[dr]=aux1;
	return i+1;
}

void quicksort(int a[], int st, int dr)
{
	if(st<dr)
	{
		
		int m=partitionare(a,st,dr);
		quicksort(a,st,m-1);
		quicksort(a,m+1,dr);
	}
}

void pretty_print(int a[], int dim)
{
	int adancime=1;
	for(int i=1;i<=dim;i++)
	{
		for(int j=1;j<=dim/2-adancime;j++)
			cout<<"\t";
		cout<<a[i];
		if((i==1)||(i==3)||(i==7))
		{
			adancime++;
			cout<<endl;
		}
	}
}

int main()
{
	/////////////////////Verificare corectitudine
	heap_size=10;
	l=10;
	a[1]=5;a[2]=11;a[3]=3;a[4]=7;a[5]=9;a[6]=15;a[7]=23;a[8]=6;a[9]=4;a[10]=1;
		
	cout<<"Vectorul initial este ";
	for(int i=1;i<=10;i++)
		cout<<a[i]<<" ";
	for(int i=1;i<=10;i++)
	{
		b[i]=a[i];
		c[i]=a[i];
	}
	heap_sort(b,l);
	cout<<endl<<"HeapSort Vector: ";
	for(int i=1;i<=10;i++)
		cout<<b[i]<<" ";
	cout<<endl<<"Heap_sort "<<endl;
	pretty_print(b, l);
	quicksort(c,1,l);
	cout<<endl<<"Quicksort ";
	for(int i=1;i<=10;i++)
		cout<<c[i]<<" ";

	////////////Quisort si Heapsort pt cazul mediu statistic 
	/*fout<<"NR ,Operatii_HS, Operatii_QS"<<endl;
	for(heap_size=100; heap_size<=MAX_SIZE; heap_size+=100)
	{
		op_h=0;
		op_q=0;
		for(int nr=0;nr<5;nr++)
		{
			for(int i=1;i<=heap_size;i++)
			{
				a[i]=rand()%RAND_MAX;
				b[i]=a[i];
			}
			heap_sort(a, heap_size);
			quicksort(b,1,heap_size);
		}
		op_h=op_h/5;
		op_q=op_q/5;
		fout<<heap_size<<","<<op_h<<","<<op_q<<endl;
	}*/
	

	/////////////////Quicksort caz favorabil si defavorabil-Vectorul este sortat crescator
	/*fout<<"NR ,Operatii_caz_favorabil, Operatii_caz_defavorabil"<<endl;
	for(heap_size=100; heap_size<=MAX_SIZE; heap_size+=100)
	{
		op_q=0;
		for(int i=1;i<=heap_size-10;i++)
			{
				a[i]=i;
			}
		quicksort(a,1,heap_size);
		fout<<heap_size<<","<<op_q<<endl;
	}
	fout.close();*/
	getch();
	return 1;
}