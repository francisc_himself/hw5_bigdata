//permutarea josephus
/*
Permutarea josephus face o permutare a unui vector oarecare.
Pentru a avea timp de rulare buna, este folosit un arbore
augmentat unde fiecare nod stite marimea subarborelui.
Astfel complexitatea se reduce de la n*m la n*ln(n)
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

int counter = 0;

//#define movs(a,b) memcpy(a,b,sizeof(a));

inline void swap(int &a, int &b)
{
	int x = a;
	a = b;
	b = x;
}

class dvector
{
public:
	int* data;
	int size;

	dvector(dvector& other)
	{
		size = other.size;
		data = new int[size];
		memcpy(data,other.data,size*sizeof(int));
	}

	dvector()
	{
		size=0;
		data=0;
	}

	~dvector()
	{
		if (data!=0)
		{
			delete[] data;
		}
	}

	int& operator [] (int i)
	{
		if (i<size)
		{
			return data[i];
		}
		else
		{
			int *ndata = new int[i+1];

			if (data != 0)
			{
				memcpy(ndata,data,size*sizeof(int));
				delete[] data;
			}
			for (int j = size;j<i+1; j++)
			{
				ndata[j]=0;
			}
			data=ndata;
			size=i+1;
			return data[i];
		}
	}

	void print()
	{
		for (int i=0; i<this->size; i++)
		{
			printf("%d ",data[i]);
		}
		printf("\n");
	}

	void heapsort();

	void prealloc(int size)
	{
		if (size > this->size)
		{
			size--;
			(*this)[size]=0;
		}
	}

	void quicksort()
	{
		quicksort(0,size-1);
	}

	int partition(int left, int right)
	{
		int x = data[right];
		int i = left-1;
		int j;
		for (j=left; j<right; j++)
		{
			counter++;
			if (data[j]<=x)
			{
				i++;
				counter+=3;
				swap(data[i],data[j]);
			}
		}
		counter+=3;
		swap(data[i+1],data[j]);
		return i+1;
	}

	void quicksort(int left, int right)
	{
		if (left<right)
		{
			int mid = partition(left,right);
			quicksort(left,mid-1);
			quicksort(mid+1,right);
		}
	}

	void quicksort_better()
	{
		quicksort_better(0,size-1);
	}

	int partition_better(int left, int right)
	{
		int x = 0;
		int c = 0;
		for (int i=0; i<4 && left+i<right; i++)
		{
			x+=data[left+i];
			c++;
		}
		for (int i=0; i<4 && left<right-i; i++)
		{
			x+=data[right-i];
			c++;
		}
		x = x / c;

		int i = left-1;
		int j;
		for (j=left; j<right; j++)
		{
			counter++;
			if (data[j]<=x)
			{
				i++;
				counter+=3;
				swap(data[i],data[j]);
			}
		}
		counter+=3;
		swap(data[i+1],data[j]);
		return i+1;
	}

	void quicksort_better(int left, int right)
	{
		if (left<right)
		{
			int mid = partition_better(left,right);
			quicksort_better(left,mid-1);
			quicksort_better(mid+1,right);
		}
	}

	void josephus(int m);

};

class onode
{
private:
	int children;
	onode *left,*right,*parent;

	void unlink(onode* other)
	{
		if (this->left == other)
		{
			other->parent = 0;
			this->left = 0;
		}
		if (this->right == other)
		{
			other->parent = 0;
			this->right = 0;
		}
		if (this->parent == other)
		{
			if (other->left == this)
				other->left = 0;
			if (other->right == this)
				other->right = 0;
			this->parent = 0;
		}
	}

	void inc_child_count()
	{
		children ++;
		if (parent)
			parent->inc_child_count();
	}

	void dec_child_count()
	{
		children --;
		if (parent)
			parent->dec_child_count();
	}

	void print_as_vector_rec()
	{
		if (left) left->print_as_vector_rec();
		printf("%d ",this->key);
		if (right) right->print_as_vector_rec();
	}

	void print_depth(int k)
	{
		if (left) left->print_depth(k+1);

		for (int i=0; i<k;i++)
		{
			printf(" ");
		}
		if (this->isRoot())
		{
			printf("<r");
		}
		else
		{
			if(parent->left == this)
			{
				printf("/");
			}
			else
			{
				printf("\\");
			}
		}
		if (this->isLeaf())
		{
			printf("l");
		}
		printf("k%dc%d\n",this->key,this->children);
		
		if (right) right->print_depth(k+1);
	}

public:

	void print_as_vector()
	{
		this->print_as_vector_rec();
		printf("\n");
	}

	int select(int index)
	{
		counter++;
		if(left && index < left->size())
			return left->select(index);
		counter++;
		if((left && left->size()==index) ||(!left && index == 0))
			return this->key;
		counter++;
		if((right && left && index > left->size()) || (!left && right && index>0))
			return right->select(index-left->size()-1);
	}

	int size()
	{
		if (this)
			return children;
		else
			return 0;
	}

	int key;
	void *data;

	onode()
	{
		data = 0;
		key = 0;
		children = 1;
		left = 0;
		right = 0;
		parent = 0;
	}

	onode(int key, void* data = 0)
	{
		this->data = data;
		this->key = key;
		children = 1;
		left = 0;
		right = 0;
		parent = 0;
	}

	bool isLeaf()
	{
		return left == 0 && right == 0;
	}

	bool isRoot()
	{
		return parent == 0;
	}

	void insert(onode *other)
	{
		if (key > other->key)
		{
			if (left)
				left->insert(other);
			else
			{
				left = other;
				other->parent = this;
				inc_child_count();
			}
		}
		else
		{
			if (right)
				right->insert(other);
			else
			{
				right = other;
				other->parent = this;
				inc_child_count();
			}
		}
	}

	onode* leftmost()
	{
		onode* p = this;
		while (p->left)
			p = p->left;
		return p;
	}

	onode* rightmost()
	{
		onode* p = this;
		while (p->right)
			p = p->right;
		return p;
	}

	onode* pred()
	{
		if (left)
		{
			return left->rightmost();
		}
		else
		{
			return 0;
		}
	}

	onode* succ()
	{
		if (right)
		{
			return right->leftmost();
		}
		else
		{
			return 0;
		}
	}

	void print()
	{
		this->print_depth(0);
	}

	onode* find(int key)
	{
		if (this->key == key)
		{
			return this;
		}
		if (key<this->key)
		{
			if (left)
				return left->find(key);
			else
				return 0;
		}
		if (key>this->key)
		{
			if (right)
				return right->find(key);
			else
				return 0;
		}
	}

	onode* remove()
	{
		if (isLeaf())
		{
			if (!isRoot())
			{
				parent->dec_child_count();
				parent->unlink(this);
			}
			return this;
		}
		else
		{
			onode *rep;
			if (left && right)
				if (left->children > right->children)
					rep = left->rightmost();
				else
					rep = right->leftmost();
			else if (left)
				rep = left->rightmost();
			else if (right)	
				rep = right->leftmost();
			
			//rep->dec_child_count();
			

			onode temp;

			temp = *this;
			*this = *rep;
			*rep = temp;
			rep->parent = this->parent;
			rep->left = this->left;
			rep->right = this->right;
			rep->children = this->children;
			this->parent = temp.parent;
			this->left = temp.left;
			this->right = temp.right;
			this->children = temp.children;

			rep = rep->remove();
			
			rep->left = rep->right = rep->parent = 0;
			return rep;
		}
	}

};

class otree
{
private:
	onode *root;

	void build_from_vector(int* vec_start, int* vec_end)
	{
		if (vec_start<=vec_end)
		{
			unsigned count = vec_end-vec_start;
			int* vec_mid = vec_start+(count>>1);
			insert(*vec_mid);
			if (vec_start<vec_end)
			{
			
				build_from_vector(vec_start,vec_mid-1);
				build_from_vector(vec_mid+1,vec_end);
			}
		}
	}

public:

	otree()
	{
		root = 0;
	}

	otree(int* vec, int count)
	{	
		root = 0;
		build_from_vector(vec,vec+count-1);
	}

	otree(dvector &vec)
	{
		root = 0;
		dvector *nv = new dvector(vec);
		//nv->quicksort();
		build_from_vector(nv->data,nv->data+nv->size-1);
	}

	otree(dvector *vec)
	{
		root = 0;
		dvector *nv = new dvector(*vec);
		//nv->quicksort();
		build_from_vector(nv->data,nv->data+nv->size-1);
	}

	void insert(int key, void* data = 0)
	{
		if (root)
			root->insert(new onode(key,data));
		else
			root = new onode(key,data);
	}

	void print()
	{
		if (root)
			root->print();
		else
			printf("null\n");
	}

	onode* find(int key)
	{
		if (root)
			return root->find(key);
		else
			return 0;
	}

	onode* remove(int key)
	{
		onode *rem = find(key);
		if (rem)
		{
			if (rem->isRoot() && rem->isLeaf())
			{
				root = 0;
			}
			return rem->remove();
		}
	}

	int select(int index)
	{
		counter++;
		if (root->size()>0)
		{
			return root->select(index);
		}
		else
		{
			return 0xffffffff;
		}
	}

	~otree()
	{
		while (root)
		{
			delete remove(root->key);
		}
	}

	int size()
	{
		counter++;
		if (root)
			return root->size();
		else
			return 0;
	}

	void print_as_vector()
	{
		if (root)
			root->print_as_vector();
		else
			printf("null\n");
	}

};

void dvector::josephus(int m)
{
	otree t(*this);
	
	m--;

	int x;
	int i = 0;
	int s = m;

	memset(this->data,0,this->size*sizeof(int));

	do
	{
		counter++;
		s = s % t.size();
		x = t.select(s);
		s += m;
		counter++;
		t.remove(x);
		
		
		counter++;
		data[i++] = x;

		//t.print_as_vector();
		//this->print();
	}
	while (t.size()>0);



}

void main()
{
	dvector a;
	for (int i=0; i<10; i++)
	{
		a[i]=i;
	}

	otree b(a);

	b.print();

	a.print();
	a.josephus(2);
	a.print();

	FILE* f = fopen("output.csv","w");
	fprintf(f,"n,m\n");

	
    //#pragma omp parallel for schedule(dynamic, 1)
	for (int test_size = 100; test_size<10000; test_size+=100)
	{
		
		dvector *a = new dvector();
		a->prealloc(test_size);
		for (int i=0; i<test_size; i++)
			(*a)[i] = i;
		counter = 0;
		a->josephus(test_size/2);

		printf("%d ",test_size);

		fprintf(f,"%d,%d\n",test_size,counter);

		delete a;

	}

	printf("\n");

	fclose(f);
}
