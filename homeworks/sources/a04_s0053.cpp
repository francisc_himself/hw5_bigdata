#include <stdlib.h>
#include <iostream>
#include <conio.h>
#include <stdio.h>


typedef struct _nod{
	int val;
	struct _nod *urm;
}nod;

typedef struct heap{
	int val;
	nod* lprim,*lultim;
}nod_heap;

void bottup_heapify(nod_heap* h, int n){
	while(h[n].val<h[n/2].val){
		nod_heap aux=h[n];
		h[n]=h[n/2];
		h[n/2]=aux;
		n=n/2;
	}}

void topdown_heapify(nod_heap* h, int sizeheap,int n){
	while(true){
		int p;
		if(2*n+1<=sizeheap){
			if(h[2*n].val>h[2*n+1].val){
				p=2*n+1;
			}else p=2*n;
		}else{
			if(2*n<=sizeheap) p=2*n;
			else break;
		}
				
		if(h[p].val<h[n].val){
			nod_heap aux=h[p];
			h[p]=h[n];
			h[n]=aux;
		}
		else{
			break;
		}
		n*=2;
	}}

nod_heap pop(nod_heap *h,int &sizeheap){
	nod_heap p=h[1];
	h[1]=h[sizeheap];
	sizeheap--;
	topdown_heapify(h, sizeheap,1);
	return p;
}

void push(nod_heap *h,nod_heap x,int &sizeheap){
	sizeheap++;
	h[sizeheap]=x;
	bottup_heapify(h,sizeheap);
}

void inserare(nod **prim, nod **ultim, int x){
	nod *q = (nod*)malloc(sizeof(nod));
	q->val=x;
    q->urm = NULL;
    if ( *prim == NULL){
        *prim = q;
        *ultim = q;
    }else{
        (*ultim) -> urm = q;
        *ultim = q;
    }}

void stergeprimul(nod **prim, nod **ultim){
    if ( *prim == NULL){
        printf("gol!");
    }else{
        if( (*prim)->urm == NULL ){  
			free(*prim);
            *prim = NULL;
            *ultim = NULL;
        }else{
            nod *p = (*prim)->urm;
            free(*prim);
            *prim = p;
        }}}

void list_afisare(nod *prim){
    printf("elemete: ");
    if (prim == NULL){
        printf(" GOL! \n");
    }else{
        while (prim != NULL){
            printf("%d ",prim->val);
            prim = prim->urm;
        }
        printf("\n");
    }}
void interclasare(nod **prim, nod**ultim, int k){
	int sizeheap=0;
	nod *rez=NULL;
	nod *rezPrim=NULL;
	nod *rezUltim=NULL;
	nod_heap *h=(nod_heap*)malloc(sizeof(nod_heap)*(k+1));
	for(int i=0;i<k;i++){
		int val=prim[i]->val;
		nod_heap x={val,prim[i]->urm,ultim[i]};
		
		push(h,x ,sizeheap);
		stergeprimul(&prim[i],&ultim[i]);
	}
	while(sizeheap>0){
		nod_heap q=pop(h,sizeheap);
		inserare(&rezPrim,&rezUltim,q.val);
		if(q.lprim!=NULL){
			int val=q.lprim->val;
			nod_heap x={val,q.lprim->urm,q.lultim};
			push(h,x ,sizeheap);
			stergeprimul(&q.lprim,&q.lultim);
		}}
	list_afisare(rezPrim);
}

int main(){
	int k, n;
	printf("numarul listelor:");
	scanf("%d",&k);
	printf("numarul elemnetelor");
	scanf("%d",&n);
	
	nod **listePrim = (nod**) malloc(k*sizeof(nod*));
	nod **listeUltim = (nod**) malloc(k*sizeof(nod*));
	memset(listePrim,0,k*sizeof(nod*));
	memset(listeUltim,0,k*sizeof(nod*));

	int ujszam=0;
	for(int i=0;i<n; i++ ){
		ujszam+=rand()%10;
		int nr=rand()%k;
		inserare(&listePrim[nr],&listeUltim[nr],ujszam);	
	}
	for(int i=0;i<k; i++ ){
		list_afisare(listePrim[i]);
	}
	interclasare(listePrim,listeUltim,k);

	
	return 0;
}