#include<stdlib.h>
#include<iostream>


typedef struct MULTI_NODE
{
	int key;
	int count; // numar fii
	struct MULTI_NODE *child[50];
}MULTI_NODE;

MULTI_NODE *nodes[];

MULTI_NODE *createM_N(int key)
{

	MULTI_NODE *p=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	p->key=key;
	p->count=0;
	return p;

}

typedef struct arbbin{
	int cheie;
	struct arbbin *copil, *dr;
}binar;


void insertM_N (MULTI_NODE *parent, MULTI_NODE * child)
{
	parent->child[parent->count++]=child;

}

MULTI_NODE *transform1 (int t[], int size)
{
	MULTI_NODE *root=NULL;
	MULTI_NODE *nodes[50];
	for(int i=0; i<size; i++)
	{
		nodes[i]=createM_N(i);
	}
	for(int i=0; i<size; i++)
	{
		if (t[i]==-1)
		{
			root=nodes[i];
		}
		else {
			insertM_N(nodes[t[i]], nodes[i]);
		}
	}
	return root;

}


void transform2(MULTI_NODE *root, binar *root2)
{
	if(root->count>0)
	{
		binar *nod=(binar*)malloc(sizeof(binar));
		root2->copil=nod;
		nod->cheie=root->child[0]->key;
		transform2(root->child[0],nod);
		if(root->count>1)
		{
			int test=1;
			while(test<root->count)
			{
				nod->dr=(binar*)malloc(sizeof(binar));
				nod->dr->cheie=root->child[test]->key;
				nod=nod->dr;
				transform2(root->child[test], nod);
				test++;
			}
			nod->dr=NULL;
		}
		else root2->copil->dr=NULL;
		root2=nod;
	}
	else root2->copil=NULL;

}

void afisarebinar(binar *node,int nivel=0)
{
	if(node==NULL) return;
	afisarebinar(node->dr,nivel);

	for(int i=0; i<nivel; i++)
		printf("   ");
	printf("%d\n", node->cheie);
	afisarebinar(node->copil, nivel+1);

}

void prettyprint (MULTI_NODE *nod, int nivel=0)
{
	for(int i=0; i<nivel; i++)
	{
		printf("   ");
	}
	printf("%d \n", nod->key);
	for(int i=0; i<nod->count; i++)
	{
		prettyprint(nod->child[i],nivel+1);
	}

}


int main()
{
	int t[]={0,2,7,5,2,7,7,-1,5,2}; //vectorul de tati
	int size=sizeof(t)/sizeof(t[0]); // dimensiunea 
	MULTI_NODE *root=transform1(t, size);
	int nivel=0;
	printf("Afisare arbore multicai:///////////////////////////////\n");
	prettyprint(root,nivel);
	binar *root2=(binar*)malloc(sizeof(binar));
	root2->cheie=root->key;
	transform2(root, root2);
	root2->dr=NULL;
	printf("Afisare arbore binar://////////////////////////////////\n");
	afisarebinar(root2,nivel);
	return 0;
}


