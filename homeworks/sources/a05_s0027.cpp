// Tema5_Hashtable.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*  
	***ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***
    CERINTE: Se cere implementarea corecta si eficienta a operatiilor de adaugare si cautare in tabela
			 de dispersie cu adresare deschisa si verificare patratica.
	1. Pentru factorii de umplere inserati n elemente astfel incat sa reflecte valoarea factorului de umplere cerut
	2. Cautati, in fiecare caz, m elemente aleator (m~3000), astfel incat aproximativ
	   jumatate dintre elementele cautate sa se afle in tabela, cealalta jumatate nu
	Concluzii: mi-am reamintit cum se lucreaza cu tabelele de dispersie
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>
#define N 10001    //numarul de elemente a tabelei


int T[N];			//vector cu N componente
int op_gasire,op_negasire;


	// h(k)=int(m(kA mod1))
	//kA mod 1=kA -int(kA)
	//a=(sqrt(5) -1)/2
	int h_aux(int k)		//functie auxiliara
	{
			int l,m;
			double n,A;
			A=(sqrt(5.0)-1)/2;
			m=(int)(k*A);
			n=k*A-m;
			l=(int) (N*n);
			return l;
	}


	int examinare_patratica(int k, int i)
		{
	    	//h(k,i)=(h_aux(k) +c1*i + c2*i*i)mod m

			int a,c1=0,c2=1;					//c1, c2 - constante auxiliare
			a=(h_aux(k) +c1*i + c2*i*i)%N;		// h_aux - functia h auxiliara
			return a;
		}


	int Hash_Insert(int *T, int k)   //functia de inserare 
									//inserarea se face la inceputul listei
	{
	 int i=0,j;
	 do{
		 j=examinare_patratica(k,i);
		 if(T[j]==NULL)
		 {
			   T[j]=k;
			  return j;
		 }
		 else i++;

	 }while(i!=N);

	return -1;
	}


	int Hash_Search(int *T,int k)// functia de cautare 
	{
	 int i=0,j,nr=0;

	 do{
      j=examinare_patratica(k,i);
      nr++;
       if(T[j]==k)
        {
	     op_gasire=nr; //daca numarul a fost gasit atunci va returna numarul de operatii efectuate pana la gasirea acestuia
	     return j;
        }
        else i++;
	  nr++;
	 }while((T[j]!=NULL)&&(i!=N));

	op_negasire=nr;// daca numarul nu a fost gasit atunci va returna numarul total de opartii efectuate
	return -1;
  }


	void main()
	{
    FILE *pf, *pf3;
	int i,j,n,k,l,m,m1,m2;
	int elem_max=0,mediu2,mediu1;
	int mediere_gas,mediere_neg;
	double ump[5]={0.8,0.85,0.9,0.95,0.99};//valorile factorilor de umplere

	pf=fopen("hash.csv","w");
	pf3=fopen("nr2.txt","w");


	fprintf(pf," \nFactor de umplere,Nr_mediu_cautari(f), Nr_max_cautari(f), Nr_mediu_cautari(not_f), Numar_max_cautari(not_f) " );
	for(i=0;i<5;i++)
		{
		  n=ump[i]*N;    //n=numarul de elemente ce vor fi introduse in tabela
		 mediere_gas=0;
		 mediere_neg=0;
		 m1=0;		// numarul maxim de operatii pt gasirea elementului in tabele
		 m2=0;		// numarul maxim de operatii pt elemente neexistente in tabele

	 for(j=0;j<5;j++)		//pentru fiecare valoarea a factorului de umplere se testeaza 5 cazuri diferite (mediere)
			{
		 for(m=0;m<N;m++) 
			  T[m]=NULL;//reinitializarea tabelei
		
		 srand(j);//realizeaza rularea a 5 cazuri cu numere diferite
	     for(l=0;l<n;l++)
	     {
            k=rand(); 
			if(k>elem_max)  
				elem_max=k;    //elementul maxim introdus in tabela
	        Hash_Insert(T,k);
	      }

         srand(j); 
		 mediu1=0;
		 for(l=0;l<7500;l++)  //caut elementele existente
		  {
             op_gasire=0;
			 k=rand();
			 if(l%5==0)
			 {

		     Hash_Search(T,k);

			 if(op_gasire>m1) m1=op_gasire; 
			  
			 mediu1=mediu1+op_gasire;
		
			 }
	 
		  }
		     mediere_gas=mediere_gas + mediu1/1500;//nr mediu operatii pentru gasire element

		  srand(j); 
		  mediu2=0;
		  for(l=0;l<1500;l++)   //caut elemente inexistente
		  {
              
			  k=rand()+ elem_max;
			  fprintf(pf3,"\n %d",k);
		      Hash_Search(T,k);
			 
			  mediu2=mediu2+op_negasire;
	        
			  if(op_negasire>m2) m2=op_negasire; 
						
		  }
		  mediere_neg=mediere_neg+mediu2/1500;	
	}	
    
    fprintf(pf,"\n %1.2f, \t %d, \t %d, \t %d, \t %d ",ump[i],mediere_gas/5,m1,mediere_neg/5,m2); 
	}

	fclose(pf);
	getch();

}






