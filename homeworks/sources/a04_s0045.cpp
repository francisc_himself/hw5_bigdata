/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Complexitatea algoritmului este O(n*lgk).
In priuml caz cand n variaza pentru diferite valori ale lui k,
graficul arata ca si o dreapta pentru fiecare k, panta fiind mai mare pentru
o k mai mare din cauza lui lgk(constant).
In al doilea caz n fiind constant (10000) k variaza si graficul este
de tip logaritmic din cauza lui lgk(cel care variaza).
*/

#include <stdio.h>
#include <conio.h>

#include "Profiler.h"

#define MAX_SIZE 12000

typedef struct NOD {
	int val;
	NOD * urm;
} nod;

typedef struct NOD_heap {
	int  val;
	int idList;
} nod_heap;

nod_heap heap_vector[MAX_SIZE];
int heap_size=0;

int n=0; //nr elemente
int k=0; //nr liste

nod *cap[MAX_SIZE];
nod *prim[MAX_SIZE];
nod *ultim[MAX_SIZE];
nod *primJ; //joined
nod *ultimJ; //joined

//measure
int ck1[MAX_SIZE]; //count k1
int ck2[MAX_SIZE]; //count k2
int ck3[MAX_SIZE]; //count k3

int cn[MAX_SIZE]; //count n stable


void inserare(nod **prim, nod **ultim, int val)
{
	nod *p=(nod *)malloc(sizeof(nod));
	p->val=val;

	if (*prim == 0)
	{
		*prim=*ultim=p;
	}
	else
	{
		(*ultim)->urm=p;
		*ultim=p;
	}

	p->urm=0;
}

void afisare(nod *prim)
{
	nod *p;

	if(prim == 0)
	{
		printf("Lista este vida!\n");
	}
	else
	{
		p=prim;
		while(p != 0)
		{
			printf("%d ", p->val);
			p=p->urm;
		}
	}
	printf("\n");
}

void deletePrim(nod **prim, nod **ultim)
{
	nod *p;
	if (*prim == 0)
	{
		return;
	}
	p=*prim;
	*prim=(*prim)->urm;
	free(p);
	if (*prim == 0)
	{
		*ultim=0;
	}
}

void deleteList(nod **prim, nod **ultim)
{
	nod *p, *p1;
	p=*prim;

	if (p == 0)
	{
		return;
	}
	do
	{
		p1=p;
		p=p->urm;
		free(p1);
	} while (p != 0);
	*prim=*ultim=0;
}

void exchange(nod_heap *v, int i, int j)
{
	nod_heap aux=v[i];
	v[i]=v[j];
	v[j]=aux;
}

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}

void pritty_print(nod_heap *heap_vector, int n, int k, int nivel)
{
	if (k>n)
	{
		return;
	}

	pritty_print(heap_vector,n,2*k+1,nivel+1);
	for (int i=1; i<=nivel; i++)
	{
		printf("   ");
	}
	printf("%d\n", heap_vector[k].val);
	pritty_print(heap_vector,n,2*k,nivel+1);
	printf("\n");
}



void heap_increase_key(nod_heap *heap_vector, int i, nod_heap *nodH)
{
	if (nodH->val > INT_MAX)
	{
		perror("new key is smaller than current key");
	}

	heap_vector[i]=*nodH;

	while (i>1 && heap_vector[parent(i)].val > heap_vector[i].val)
	{
		//nod_heap aux=heap_vector[i];
		//heap_vector[i]=heap_vector[parent(i)];
		//heap_vector[parent(i)]=aux;

		exchange(heap_vector, i, parent(i));
		i=parent(i);

		ck1[n]+=5;
		cn[k]+=5;
	}
}

void min_heap_insert(nod_heap *heap_vector, nod_heap *nodH)
{
	heap_size++;

	heap_increase_key(heap_vector, heap_size, nodH);
}

void min_heapify(nod_heap *v, int i)
{
	int l=left(i);
	int r=right(i);
	int smallest;

	if (l<=heap_size && v[l].val<v[i].val)
	{
		smallest=l;
		ck1[n]+=2;
		cn[k]+=2;
	}
	else
	{
		smallest=i;
		ck1[n]+=2;
		cn[k]+=2;
	}

	ck1[n]+=1;
	cn[k]+=1;

	if (r<=heap_size && v[r].val<v[smallest].val)
	{
		smallest=r;
		ck1[n]+=2;
		cn[k]+=2;
	}

	ck1[n]+=1;
	cn[k]+=1;

	if (smallest != i)
	{
		//nod_heap aux=v[i];
		//v[i]=v[smallest];
		//v[smallest]=aux;

		exchange(v, i, smallest);

		ck1[n]+=4;
		cn[k]+=4;

		min_heapify(v, smallest);
	}
}

nod_heap *min_heap_pop(nod_heap *heap_vector)
{
	if (heap_size > 0)
	{
		ck1[n]++;
		cn[k]++;

		int val=heap_vector[1].val;
		int idList=heap_vector[1].idList;
		exchange(heap_vector, 1, heap_size);
		heap_size--;

		ck1[n]+=4;
		cn[k]+=4;

		min_heapify(heap_vector, 1);

		//free(&heap_vector[heap_size+1]);

		nod_heap *h=(nod_heap*)malloc(sizeof(nod_heap));
		h->val=val;
		h->idList=idList;

		return h;
	}
	else
	{
		return 0;
	}
}

void H_init()
{
	heap_size = 0;
}

void initialize()
{
	for (int i=0; i < MAX_SIZE; i++)
	{
		cap[i]=NULL;
		deleteList(&prim[i], &ultim[i]);
	}

	deleteList(&primJ, &ultimJ);

	H_init();
}

void initializeCounters()
{
	//initializare counteri
	for (int i=0; i < MAX_SIZE; i++)
	{
		ck1[i]=0;
		cn[i]=0;
	}
}

void fillLists(nod **cap, nod **prim, nod **ultim, int k, int n)
{
	int contor_cap = 0;

	for (int i=1; i<=n; i++)
	{
		int r=rand()%5;
		inserare(&prim[contor_cap], &ultim[contor_cap], i+r);
		
		contor_cap++;
		if (contor_cap >= k)
		{
			contor_cap = 0;
		}
	}

	for (int i=0; i<k; i++)
	{
		cap[i]=prim[i];
	}
}

nod_heap *read_l(nod **p, nod **u, int cap)
{
	if (*p == NULL)
	{
		return NULL;
	}

	int val=(*p)->val;

	deletePrim(p, u);

	nod_heap *h=(nod_heap *)malloc(sizeof(nod_heap));

	h->val=val;
	h->idList=cap;

	return h;
}

void write_l(nod **p, nod **u, nod_heap *h)
{
	int val=h->val;
	free(h);

	inserare(p, u, val);
}

void interclasare(nod **prim,nod **ultim, int k, nod **primJ, nod **ultimJ, nod_heap *heap_vector)
{
	H_init();
	nod_heap *aux;
	int cap;

	for(int i=0; i<k; i++)
	{
		ck1[n]+=k;
		cn[k]+=k;

		aux=read_l(&prim[i],&ultim[i],i);

		if (aux == NULL)
		{
			printf("\nLista nu este initializata!\n");
		}

		ck1[n]++;
		cn[k]++;

		//printf("\n%d %d\n", aux->val, aux->idList);
		min_heap_insert(heap_vector, aux);
	}

	while (heap_size > 0)
	{
		ck1[n]++;
		cn[k]++;

		aux=min_heap_pop(heap_vector);
		cap=aux->idList;
		write_l(primJ, ultimJ, aux);

		ck1[n]++;
		cn[k]++;

		aux=read_l(&prim[cap],&ultim[cap],cap);

		ck1[n]++;
		cn[k]++;

		if (aux != NULL)
		{
			min_heap_insert(heap_vector, aux);
		}
	}
}

void main()
{
	srand(time(NULL));

	//testare linked list
	nod *head[10], *tail[10];
	head[0]=0; tail[0]=0;
	for(int i=1; i<10; i++)
	{
		inserare(&head[0], &tail[0], i);
	}
	afisare(head[0]);
	deletePrim(&head[0], &tail[0]);
	deletePrim(&head[0], &tail[0]);
	afisare(head[0]);
	//cap[0]=head;
	//afisare(cap[0]);
	deleteList(&head[0], &tail[0]);
	afisare(head[0]);


	//test build_min_heap_top_down
	H_init();

	for (int i=0; i<10; i++)
	{
		int r=rand() % 50+1;
		nod_heap *nodh=(nod_heap *)malloc(sizeof(nod_heap));
		nodh->val=r;

		min_heap_insert(heap_vector, nodh);
	}

	pritty_print(heap_vector, heap_size, 1, 1);

	nod_heap *h=min_heap_pop(heap_vector);

	printf("\n%d\n", h->val);
	pritty_print(heap_vector, heap_size, 1, 1);

	h=min_heap_pop(heap_vector);

	printf("\n%d\n", h->val);
	pritty_print(heap_vector, heap_size, 1, 1);

	k=3;
	n=20;
	//test fillLists
	fillLists(cap, prim, ultim,k,n);
	afisare(cap[0]);
	afisare(cap[2]);
	////////

	//test read_l/write_l
	nod_heap *aux=read_l(&prim[0],&ultim[0], 0);
	write_l(&primJ, &ultimJ, aux);
	afisare(primJ);

	aux=read_l(&prim[0],&ultim[0], 0);
	write_l(&primJ, &ultimJ, aux);
	afisare(primJ);

	aux=read_l(&prim[0],&ultim[0], 0);
	write_l(&primJ, &ultimJ, aux);
	afisare(primJ);

	//test init
	initialize();
	afisare(cap[0]);
	afisare(prim[0]);
	afisare(primJ);
	////

	fillLists(cap, prim, ultim,k,n);
	afisare(prim[0]);
	afisare(prim[1]);
	afisare(prim[2]);

	//test interclasare
	interclasare(prim, ultim, k, &primJ, &ultimJ, heap_vector);
	printf("\n");
	pritty_print(heap_vector, heap_size, 1, 1);
	afisare(primJ);
	//////

	//test init///////////////////////////////
	//initialize();
	//fillLists(cap, prim, ultim,k,n);
	//interclasare(prim, ultim, k, &primJ, &ultimJ, heap_vector);
	//afisare(primJ);
	//////////////////////////////////////

	//generare date
	//punctul 1
	int k1=5;
	int k2=10;
	int k3=100;

	k=k1;
	initializeCounters();
	for (n=100; n<=10000; n+=100)
	{
		initialize();
		fillLists(cap, prim, ultim,k,n);
		interclasare(prim, ultim, k, &primJ, &ultimJ, heap_vector);
	}
	memcpy(ck2, ck1, sizeof(ck1));

	k=k2;
	initializeCounters();
	for (n=100; n<=10000; n+=100)
	{
		initialize();
		fillLists(cap, prim, ultim,k,n);
		interclasare(prim, ultim, k, &primJ, &ultimJ, heap_vector);
	}
	memcpy(ck3, ck1, sizeof(ck1));

	k=k3;
	initializeCounters();
	for (n=100; n<=10000; n+=100)
	{
		initialize();
		fillLists(cap, prim, ultim,k,n);
		interclasare(prim, ultim, k, &primJ, &ultimJ, heap_vector);
	}
	
	FILE *f=fopen("assign4.csv", "w");

	fprintf(f,"n,k=5,k=10,k=100\n");

	for (n=100; n<=10000; n+=100)
	{
		fprintf(f, "%d,%d,%d,%d\n",n,ck2[n],ck3[n],ck1[n]);
	}
	fprintf(f,"\n");

	//punctul 2
	n=10000;

	initializeCounters();
	for (k=10; k<=500; k+=10)
	{
		initialize();
		fillLists(cap, prim, ultim,k,n);
		interclasare(prim, ultim, k, &primJ, &ultimJ, heap_vector);
	}

	fprintf(f,"k,nr_op\n");
	for (k=10; k<=500; k+=10)
	{
		fprintf(f, "%d,%d\n",k,cn[k]);
	}

	fclose(f);

}