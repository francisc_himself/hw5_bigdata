﻿#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
/*
Prof. Baja Vlad
Student; ***ANONIM*** ***ANONIM*** Ramona Grupa ***GROUP_NUMBER***
Assign 6 -Josephus Permutation
Eficienta algoritmului este de O(n*log n) deoarece stergerea dintr-un arbore binar de cautare are eficienta O(log n), si
se vor sterge toate cele n elemente, pe care se va efectua functia Josephus
*/



typedef struct NOD{
	int val;
	int dim;
	struct NOD *parent,*st, *dr;
}NOD;
NOD *p;
int m;
int nr_op;//nr de asignari si comparatii
/*
typedef struct RET{
	int dim;
	struct NOD *node ;
}RET;
*/
NOD *build_tree_echilibrat(int st, int dr)
{
	//RET *p;
	NOD *p;
	int m;
	//int dim;  int d, d_st,d_dr;
	if(st<=dr)
	{
		p=((NOD*)malloc(sizeof(NOD)));
		m=(st+dr)/2;
		nr_op++;
	//x->val=m;
	p->val=m;
	//(d_st,x->st)=build_tree_echilibrat(st,m-1);
	//p->node=x->st;
//p->dim=d_st;
	p->st=build_tree_echilibrat(st,m-1);
	//(d_dr,x->dr)=build_tree_echilibrat(m+1,dr);
	//p->node=x->dr;
//p->dim=d_dr;
	p->dr=build_tree_echilibrat(m+1,dr);

	//d=d_st+d_dr+1;
	//p->dim=d;
	int d1=0, d2=0;
	if(p->st != NULL){
		d1 = p->st->dim;
	}
	if(p->dr != NULL)
	{
		d2=p->dr->dim;
	}
	p->dim=d1+d2+1;
	//if(p->st!=NULL && p->dr!=NULL)
		//p->dim=p->st->dim + p->dr->dim +1;
	//_st+d_dr+1;
	//return (d,x);
	return p;
	}
	else
		return NULL;
	//return (0,NULL);
}
void afisare_inordine(NOD *p, int nivel)
{
	int i;
	if(p!=0)
	{
		afisare_inordine(p->st,nivel+1);
		for(i=0;i<=nivel;i++) printf(" ");
		printf("%2d\n",p->val);
		afisare_inordine(p->dr,nivel+1);

	}

}
void afisare_inordine2(NOD *p, int nivel)
{
	int i;
	if(p!=0)
	{
		afisare_inordine2(p->st,nivel+1);
		for(i=0;i<=nivel;i++) printf(" ");
		printf("%2d,%d\n",p->val,p->dim);
		afisare_inordine2(p->dr,nivel+1);

	}

}
void afisare_inordine_dim(NOD *p, int nivel)
{
	int i;
	if(p!=0)
	{
		afisare_inordine_dim(p->st,nivel+1);
		for(i=0;i<=nivel;i++) printf(" ");
		printf("%2d\n",p->dim);		
		afisare_inordine_dim(p->dr,nivel+1);
		

	}

}
NOD* SO_Selectie(NOD* x,int i)
{
	int r;
	if(x==NULL || x->val==0) return NULL; 
	if(x->st==NULL)
	{  
		nr_op++;
		r=1;
	}
	else
		r=x->st->dim +1;
	if(i==r)
	{
		nr_op++;
	return x;
	}
	else
	{
		if (i<r)
		{
			nr_op++;
			return SO_Selectie(x->st,i);
		}
		else 
		{
			nr_op++;
			return SO_Selectie(x->dr,i-r);
		}
	}
	
}
NOD *arbore_minim(NOD *x)
{
	while(x->st!=0)
	{
		nr_op++;
		x=x->st;
	}
	return x;
}
NOD  *arbore_succesor(NOD *x)
{
	NOD *y;
	nr_op++;
	if(x->dr!=NULL)
		return arbore_minim(x->dr);
	y=x->parent;
	nr_op++;
	while((y!=NULL) && (x==y->dr))
	{
		x=y;
		y=y->parent;
		nr_op+=2;

	}
	return y;
}

void SO_Delete(NOD* rad,NOD* z)
{
	NOD *y,*x;
	nr_op+=2;
	if((z->st==NULL) || (z->dr==NULL))
		y=z;
	else //y devine fiu lui z
		y=arbore_succesor(z);
	nr_op+=2;
	if(y->st!=NULL)
		x=y->st;//x devine fiu lui y
	else
		x=y->dr;
	nr_op++;
	if(x!=NULL)//x deine fra cu y
		x->parent=y->parent;
	nr_op++;
	if(y->parent!=NULL)
		rad=x;
	else if (y==y->parent->st)
			y->parent->st=x;
		else y->parent->dr=x;
		nr_op++;
	if(y!=z)
		z->val=y->val;
	//return y;//
	
printf("S-a sters nodul:",z->val);
	free(y);
}

void Josephus(int n, int m)
{
	NOD *t,*x;
	int j,k;
	printf("Construire Arbore! \n");
	t=build_tree_echilibrat(1,n);
	j=1;
	for(k=n;k>=1;k--)
	{
		j= ((j+m-2) % k) + 1 ;
		x=SO_Selectie(t, j);
	printf("Cheia este: %d \n",x->val);
	//OS-DELETE(t, x);
	SO_Delete(t,x);
	}
	
}
	/*pseudocod din lab
T = BUILD_TREE(n) 
j ←1 
for k ←n downto 1 do 
j ←(( j + m − 2) mod k) + 1 
x ←OS-SELECT(root[T], j ) 
print key[x] 
OS-DELETE(T, x)*/

/*
void OS_delete(NOD* rad,NOD* z)
{
		NOD *y,*x;
	
	nr_op+=2;
	if(z->st==p || z->dr==p)
		y=z;
	else
		y=arbore_succesor(z);
	
	//toti de deasupra nodului ce se va sterge, sa piarda 1 la dimensiune
	x=y;
	nr_op++;
	while(x!=p && x!=NULL)
	{
		nr_op+=2;
		x->dim=x->dim-1;
		x=x->parent;
	}
	x=NULL;
	

	nr_op+=2;
	if(y->st!=NULL && y->st!=p)
		x=y->st;
	else
		x=y->dr;
	

	nr_op++;
	if(x!=NULL && x!=p){
		x->parent=y->parent;
		nr_op++;
	}

	nr_op++;
	if(y->parent==NULL || y->parent==p)
	{
		rad=x;
		++nr_op;
	}
	else if(y==y->parent->st)
	       {
		        y->parent->st=x;
		        nr_op+=2;
			}
		else
		   {
			y->parent->dr=x;
			nr_op+=2;
			}

	nr_op++;
	if(y!=z)
	{
		z->val=y->val;
		++nr_op;
	}
	printf("S-a sters nodul:",y->val);
	free(y);
}
*/
/*
NOD* remove(NOD *n) //Function to delete a node
{
 NOD *temp = head,*deleted;
 while(strcmp(temp -> next -> name,n) != 0)
   temp = temp -> next;

 deleted = temp -> next;
 temp -> next = temp -> next -> next;
 return deleted; //return deleted node
}
*/
void main()
{
	NOD *q;
	NOD *t;
	printf("Afisare Arbore in Inordine! \n");
	q=build_tree_echilibrat(1,8);
	afisare_inordine(q,0);
	printf("\n");
	printf("Dimensiunea fiecarui nod este\n");
	afisare_inordine_dim(q,0);
	printf("Arbore cu Dimensiunile nodurilor\n");
	afisare_inordine2(q,3);
	
	t=SO_Selectie(q,2);
		printf("Cheia nodului este: %d \n",t->val);
	//afisare_inordine(q,0);
		SO_Delete(q,t);
		//OS_delete(q,t);
			afisare_inordine(q,0);
			//printf("finish");
		//Josephus(3,7);
			FILE *f=fopen("josephus.csv","w");
    int n;
	int contor;

 
	for( n=100;n<=10000;n+=100)
	{
		contor=0;
		Josephus(n,n/2);
	
	     fprintf(f,"%d %d\n",n,contor);
	}

	fclose(f);


	getch();
}