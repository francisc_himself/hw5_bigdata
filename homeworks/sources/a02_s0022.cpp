/* ***ANONIM*** ***ANONIM*** ***ANONIM***
	Grupa ***GROUP_NUMBER***
	Tema 2: HeapSort

	S-a dovedit ca ButtomUp face mai putine operatii, in timp ce TopDown confera o mai usoara inserare a elementelor

*/
#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "conio.h"
//Variabile Globale

FILE* outPut;
FILE* file;
int dimB=0;
int nrAtrib =0,medAtrib=0,nrComp=0,medComp=0;
int M[100][100];

//Proceduri folosite
void afisC(int A[],int dimA){
    int i;
    printf("\nSirul este\n");
    for(i=1;i<=dimA;i++){
        printf("%d ",A[i]);
    }
}

void generareAvg(int a[], int n)
{
	int i;
	a[0]=0;
	for (i=1;i<=n;i++)
		a[i] =rand() % 1500;
}

//Buttom-UP
int parinte(int i){
    return i/2;
}

int st(int i){
    return 2*i;
}

int dr(int i){
    return 2*i+1;
}

int indexMin (int a, int b, int c, int j){
    if(a<=b){
        nrComp++;///comparatie
        if(a<=c){
            nrComp++;///comparatie
            return j;
        }else{
            nrComp++;///comparatie
            return 2*j+1;
        }
    }else{
        if(b<=c){
            nrComp++;///comparatie
            return 2*j;
        }else{
            nrComp++;///comparatie
            return 2*j+1;
        }

    }
}

void reconstituireHEAP(int A[],int i,int dimA){
    if(2*i<=dimA || 2*i+1<=dimA){
        int ind = indexMin(A[i],A[st(i)],A[dr(i)],i);
        if (ind!=i){
            nrComp++;///comparatie
            nrAtrib+=3;///atribuire +3
            int aux = A[i];
            A[i] = A[ind];
            A[ind] = aux;
            reconstituireHEAP(A,ind,dimA);
        }
    }
}

void construireHEAP(int A[],int dimA){
    int i;
    for(i=dimA/2;i>=1;i--){
        reconstituireHEAP(A,i,dimA);
    }
}
//Top-DOWN
int hInit(){
    return 0;
}

void HPop(int A[],int *dimA){
    int x = A[1];
    nrAtrib++;///atribuire
    A[1] = A[*(dimA)];
    *(dimA)--;
    reconstituireHEAP(A,1,*(dimA));
}

void HPush(int A[],int *dimA,int x){
    *(dimA)++;
    nrAtrib++;///atribuire
    A[*(dimA)]=x;
    int i=*(dimA);
    while(i>1 && A[parinte(i)]>A[i]){
        nrComp++;///comparatie
        nrAtrib+=3;///atribuire +3
        int aux = A[parinte(i)];
        A[parinte(i)] = A[i];
        A[i] = aux;
        i = parinte(i);
    }
}

void  construireHEAP_TD(int A[],int dimA,int B[]){
    dimB = 0;
    int i;
    for(i=1;i<=dimA;i++){
        dimB++;
        nrAtrib++;///atribuire
        B[dimB]=A[i];
        int j=dimB;
        while(j>1 && B[parinte(j)]>B[j]){
            nrComp++;///comparatie
            nrAtrib+=3;///atribuire +3
            int aux = B[parinte(j)];
            B[parinte(j)] = B[j];
            B[j] = aux;
            j = parinte(j);
        }
       
    }
}


void main()
{
    file = fopen("rezultate.csv","w");

    int C[10000];
    int dimC =0;

    int D[10000];
    int dimD =0;

    int F[10000];

    int t=0;

    printf("Incepe programul\n");

    fprintf(file,"n, nrAtribBU, nrCompBU, SumaBU, nrAtribTD, nrCompTD, SumaTD\n");

    int j=0,to=0;
    for(j=100;j<=10000;j+=100){
        fprintf(file,"%d, ",j);
        medComp=medAtrib=0;
        for(to=0;to<5;to++){
            generareAvg(C,j);
            //afisC(C,j);
            nrAtrib=nrComp=0;
            //fprintf(outPut,"\n   HeapSort Buttom-UP");
                //afis(C,dimC);
                //fprintf(outPut,"\n");
                construireHEAP(C,j);
                //afis(C,dimC);
            medComp+=nrComp;
            medAtrib+=nrAtrib;
            //afisC(C,j);
            //printf("\n%d %d",medAtrib,medComp);
        }
        //afisare in fisier csv
        fprintf(file,"%d, %d, %d, ",medAtrib/5, medComp/5,medAtrib/5 + medComp/5);
        printf("\n...");
      //  system("clear");
        medComp=medAtrib=0;
        for(to=0;to<5;to++){
            generareAvg(D,j);
            nrAtrib=nrComp=0;
            //fprintf(outPut,"\n\n   HeapSort Top_DOWN");
                //afis(D,dimD);
                //fprintf(outPut,"\n");
            //afis(E,dimB);
            //fprintf(outPut,"\n");
            construireHEAP_TD(D,j,F);
            //afis(E,dimB);
            medComp+=nrComp;
            medAtrib+=nrAtrib;
        }
        fprintf(file,"%d, %d, %d\n",medAtrib/5, medComp/5,medAtrib/5 + medComp/5);

    }

	//Demo
	int H[9]={0,7,6,5,4,3,2,1,8};
	int G[9]={0,7,6,5,4,3,2,1,8};
	int T[9];
	afisC(H,8);
	printf("\n");

	construireHEAP(H,7);
	afisC(H,8);
	printf("\n");


	afisC(G,8);
	printf("\n");

	construireHEAP_TD(G,8,T);
	afisC(T,8);
	printf("\n");
	

    printf("\nS-a terminat");
    fclose(file);
	getch();
}

