/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

Acest cod are rolul de a implementa algoritmul BFS si de a ii studia complexitatea.

0. Rezultate asteptate
	Complexitatea asteptata a algoritmului BFS este O( |V| + |E|)
1. Testarea corectitudinii
	Functia TesteazaCorectitidine() afiseaza rezultatul parcurgerii BFS a unui graf simplu
2. Grafice
	-Graficul numarMuchii.html prezinta variatia numarului de operatii in functie de numarul de muchii.
	-Graficul numarVarfuri.html prezinta variatia numarului de operatii in functie de numarul de varfuri.
3. Interpretarea graficelor
	Ambele grafice prezinta variatii liniare. In concluzie, numarul de operatii a algoritmului BFS este O( |V| + |E|)
*/
#include<iostream>
#include<stdlib.h>
#include<time.h>
#include "Profiler.h"
using namespace std;

#define NUMAR_VARFURI 100
#define NUMAR_MUCHII 900
#define WHITE  0
#define GRAY 1
#define BLACK 2
#define INFINITY -1

Profiler profiler("Numar de operatii BFS in functie de numarul de varfuri");
int nrMuchii,nrVarfuri;

//==============================================================
//types
typedef struct _NODE
{
	int Data;
	struct _NODE *Parent;
	int Color;
	int d;
}NODE;

typedef NODE *VARF;

typedef struct _MUCHIE
{
	int V1;
	int V2;
}MUCHIE;

typedef struct _GRAF
{
	int NrVarfuri;
	int NrMuchii;
	VARF *Varfuri;
	MUCHIE *Muchii;
	int  Size[200];
	int Adj[200][200];
}GRAF;
//=========================================================================


//=========================================================================
//graf functions
void MakeGraph(GRAF **G, int nrVarfuri, int nrMuchii, MUCHIE *Muchii)
{
	*G  = new GRAF;
	(*G )-> NrVarfuri = nrVarfuri;
	(*G )-> NrMuchii = nrMuchii;
	(*G )-> Varfuri = (VARF *) malloc(nrVarfuri*sizeof(VARF));
	(*G )-> Muchii = (MUCHIE *)malloc(nrMuchii*sizeof(MUCHIE));
	
	for(int i =0 ;i<nrVarfuri;i++)
	{
		(*G )->Varfuri[i] = (VARF)malloc(sizeof(VARF));
		(*G )->Varfuri[i]->Data = i;
		(*G )->Varfuri[i]->Color = 0;
		(*G )->Varfuri[i]->d = INFINITY;
		(*G )->Varfuri[i]->Parent = NULL;
		(*G )->Size[i]=0;
	}

	for(int i = 0; i< nrVarfuri;i++)
	{
		(*G)->Size[i] = 0;
	}

	for(int i = 0;i< nrMuchii;i++)
	{
			(*G )-> Muchii[i] = Muchii[i]; 
			(*G)->Size[(*G )-> Muchii[i].V1]++;
			(*G)->Adj[(*G )-> Muchii[i].V1][(*G)->Size[(*G )-> Muchii[i].V1]]=(*G )-> Muchii[i].V2;
			(*G)->Size[(*G )-> Muchii[i].V2]++;
			(*G)->Adj[(*G )-> Muchii[i].V2][(*G)->Size[(*G )-> Muchii[i].V2]]=(*G )-> Muchii[i].V1;
	}
	
}

//============================================================================================
//functii pentru stiva
void ENQUE(int *Q,int *head,int *tail,int value)
{
	Q[*tail] = value;
	
	if(*tail == 200)
	{
		(*tail) = 0;
	}
	else
	{
		(*tail)++;
	}
	
}

int DEQUE(int *Q,int *head,int *tail)
{
	int val = Q[*head];
	if(*head == *tail)
	{
		return INFINITY;
	}
	if(*head == 200)
	{
		*head = 0;
	}
	else
	{
		(*head)++;
	}
	return val;
}
//============================================================================================
//functia BFS si variabilele aferente

int Q[201],head,tail;

void BFS(GRAF *G,int s)
{
	for(int i =0;i<G->NrVarfuri;i++)
	{
		profiler.countOperation("Operatii",nrVarfuri,1);
		if(G->Varfuri[i]->Color != BLACK)
		{
			profiler.countOperation("Operatii",nrVarfuri,3);
			G->Varfuri[i]->Color = WHITE;
			G->Varfuri[i]->Parent = NULL;
			G->Varfuri[i]->d = INFINITY;
		}
	}
	profiler.countOperation("Operatii",nrVarfuri,6);
	G->Varfuri[s]->Color = GRAY;
	G->Varfuri[s]->Parent = NULL;
	G->Varfuri[s]->d = 0;
	head = tail = 0;
	ENQUE(Q,&head,&tail,s);

	while(head!=tail)
	{
		profiler.countOperation("Operatii",nrVarfuri,1);
		int u = DEQUE(Q,&head,&tail);
		for(int i =1;i<=G->Size[u];i++)
		{
			profiler.countOperation("Operatii",nrVarfuri,1);
			if(G->Varfuri[G->Adj[u][i]]->Color == WHITE)
			{
				profiler.countOperation("Operatii",nrVarfuri,6);
				G->Varfuri[G->Adj[u][i]]->Color = GRAY;
				G->Varfuri[G->Adj[u][i]]->d = G->Varfuri[u]->d +1;
				G->Varfuri[G->Adj[u][i]]->Parent = G->Varfuri[u];
				ENQUE(Q,&head,&tail,G->Adj[u][i]);
			}
		}
		profiler.countOperation("Operatii",nrVarfuri,1);
		G->Varfuri[u]->Color = BLACK;
	}
}

//============================================================================================
//functii si variabile folosite pentru verificarea corectitudinii si construirea graficelor

typedef struct _MULTI_NODE
{
	int Key;
	int Size;
	int Capacity;
	struct _MULTI_NODE **Child;
}MULTI_NODE;


typedef struct _BINARY_MULTI_NODE
{
	int Key;
	struct _BINARY_MULTI_NODE *Left, *Right;
}BINARY_MULTI_NODE, *PBINARY_MULTI_NODE;

MULTI_NODE 
*MakeNode (int Key)
{
	MULTI_NODE *MNode = new MULTI_NODE;
	
	MNode -> Key = Key;
	MNode -> Size = 0;
	MNode -> Capacity = 50;
	//MNode -> Capacity = 1;
	MNode -> Child = (MULTI_NODE **) malloc(MNode-> Capacity* sizeof(MULTI_NODE*));
	return MNode;
}

void 
InsertMultiNode(MULTI_NODE *Parent,
				MULTI_NODE *Child)
{
	if(Parent != NULL && Child != NULL)
	{
		Parent -> Size ++;
		Parent -> Child[Parent -> Size-1] = Child;
	}
}

MULTI_NODE* Nodes[8];

void
TransformNode (int t[], int size)
{
	int i;

	for(i = 0 ; i < size ; i++)
	{
		Nodes[i] = MakeNode (i);
	}

	for(i =0; i< size; i++)
	{
		if(t[i] != -1)
		{
			InsertMultiNode(Nodes[t[i]],Nodes[i]);
		}
		else 
		{
			;
		}
	}
}

void 
PrettyPrint(
		MULTI_NODE *Root,
		int Nivel = 0
		)
{
	for(int i = 0; i < Nivel; i++ )
	{
		printf("     ");
	}
	
	printf("%d\n", Root->Key);

	for(int i =0; i < Root->Size; i++)
	{
		PrettyPrint(Root->Child[i], Nivel + 1);
	}
}


void TesteazaCorectitidine()
{
	GRAF *G = new GRAF;
	MUCHIE M[] = {{0,1},{0,2},{1,2},{1,3},{3,4},{5,6}};
	
	/*
	Graful este:
			1	-	3			5
		/
	0		|		|			|			7
		\	
			2		4			6
	
	*/

	MakeGraph(&G,8,6,M);
	int tati[10];
	
	BFS(G, 0);
	for(int i=1;i<8;i++)
	{
		if (G->Varfuri[i]->d == INFINITY)
		{
			BFS(G, i);
		}
	}
	for(int i=0;i<8;i++)
	{
		if(G->Varfuri[i]->Parent == NULL)
		{
			ENQUE(Q,&head,&tail,i);
			tati[i]=-1;
		}
		else
		{
			tati[i]=G->Varfuri[i]->Parent->Data;
		}
	}
	TransformNode(tati,8);
	while(head!=tail)
	{
		int u = DEQUE(Q,&head,&tail);
		PrettyPrint(Nodes[u],0);
		printf(".............................................................................\n");
	}
}

void ConstruiesteGraficMuchii()
{
	MUCHIE muchii[60001];
	srand(time(NULL));
	bool gasit;
	for(nrMuchii = 1000;nrMuchii<5000;nrMuchii+=100)
	{
		GRAF *G = new GRAF;
		cout<<"nrMuchii = "<<nrMuchii<<endl;
		for(int i = 0;i<nrMuchii;i++)
		{
			do
			{
				gasit = false;
				muchii[i].V1 = rand()%NUMAR_VARFURI;
				muchii[i].V2 = rand()%NUMAR_VARFURI;
				for(int j=0;j<i;j++)
				{
					if((muchii[j].V1==muchii[i].V1 && muchii[j].V2 == muchii[i].V2) || (muchii[j].V2==muchii[i].V1 && muchii[j].V1 == muchii[i].V2) || muchii[i].V1 == muchii[i].V2)
					{
						gasit = true;
					}
				}
			}
			while (gasit);
		}

		MakeGraph(&G,NUMAR_VARFURI,nrMuchii,muchii);
		BFS(G,0);
		for(int i=1;i<NUMAR_VARFURI;i++)
		{
			if (G->Varfuri[i]->d == INFINITY)
			{
				BFS(G, i);
			}
		}
	}

	profiler.showReport();
}

void ConstruiesteGraficVarfuri()
{
	MUCHIE muchii[60001];
	srand(time(NULL));
	bool gasit;
	for(nrVarfuri = 100;nrVarfuri<200;nrVarfuri+=10)
	{
		GRAF *G = new GRAF;
		cout<<"nrVarfuri = "<<nrVarfuri<<endl;
		for(int i = 0;i<NUMAR_MUCHII;i++)
		{
			do
			{
				gasit = false;
				muchii[i].V1 = rand()%nrVarfuri;
				muchii[i].V2 = rand()%nrVarfuri;
				for(int j=0;j<i;j++)
				{
					if((muchii[j].V1==muchii[i].V1 && muchii[j].V2 == muchii[i].V2) || (muchii[j].V2==muchii[i].V1 && muchii[j].V1 == muchii[i].V2) || muchii[i].V1 == muchii[i].V2)
					{
						gasit = true;
					}
				}
			}
			while (gasit);
		}

		MakeGraph(&G,nrVarfuri,NUMAR_MUCHII,muchii);
		BFS(G,0);
		for(int i=1;i<nrVarfuri;i++)
		{
			if (G->Varfuri[i]->d == INFINITY)
			{
				BFS(G, i);
			}
		}
	}

	profiler.showReport();
}

int main()
{
	TesteazaCorectitidine();
	//ConstruiesteGraficMuchii();
	//ConstruiesteGraficVarfuri();
}