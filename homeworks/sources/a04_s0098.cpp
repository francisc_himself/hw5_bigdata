
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<fstream>
#include<conio.h>
#include <cstdlib>
#include<time.h>

#define MAX 100
#define MIN 10

using namespace std;

int attr=0, comp=0;
int k;
int heapsize=0, listNb;

// A heap is used in order to merge k lists having an increased efficiency
// For push the complexity is O(logk)
//For pop also the complexity is O(logk)
//The maximum elements for each list are placed into the heap
//There are two cases :
//----------------------when n is known
//---------------------	and when k is known
//after we apply heapify, a min heap is constructed
//analyzing the graphs it can be observed that for n, the graph is logarithmic
//also, by analyzing the graph it can be observed that for k the graph is linear
//Optimal by means of time
//the running time is O(nlogk)
//By using Master Theorem c=1 , a=2, b=2
int finalSize; 

typedef struct node
{
	int val;
	node *next;

}node;

node *lista[10], *heap[10000], *x[10001];


int parent(int k)
{
	return k/2;
}

int left(int k)
{
	return 2*k;
}

int right(int k)
{
	return 2*k+1;
}

//bottom - up

void heapify(node *A[], int i)  //i=index of the root (element which is added)
{
	int l=left(i);
	int r=right(i);
	int largest;
	//n=heapsize;

	comp++;
	if(l<=heapsize && A[l]->val<A[i]->val)
		largest=l;
	else largest=i;
	
	comp++;
	if(r<=heapsize && A[r]->val<A[largest]->val)
		largest=r;
	
	if (largest!=i) //it has a child larger than the root
	{
		A[0]=A[i];
		A[i]=A[largest];
		A[largest]=A[0];
		attr=attr+3;      //swap root with the largest child
		heapify(A,largest);
	}
}

//build max heap 
// goes through the remaining nodes of the tree and runs MAX-HEAPIFY on each one.

void build_max_heap(node *A[], int n)  //bottom up
{
	int i;
	heapsize=n;
	for(i=n/2;i>=1;i--) //from the non leaf node to the root
		heapify(A,i);  //builds the heap from the 2 previously build heaps + 1 node
}

//node *a;
node* insert(node *list, int value)
{
	 node *temp,*right;
	temp = (node *)malloc(sizeof(node));
	right = (node *)malloc(sizeof(node));
	temp->val = value;
	if (list == NULL)
	{
		list = temp;
		list->next=NULL;
	}
	else
	{
		right= list;
		while(right->next != NULL)
		{
			right=right->next;
		}
		right->next = temp;
		right = temp;
		temp->next = NULL;

	}

	return list;
}

node* deleteFirstElement(node *list)
{
	
	if(list!=NULL)
	{
		node *temp;
		temp = list;
		list = list->next;
		free(temp);
	}
	return list;
}

void display_list(node *list)
{
	node *temp;
	temp = (node *)malloc(sizeof(node));
	temp = list;
	while (temp != NULL)
	{
		printf("%d ",temp->val);
		temp = temp->next;
	}
	printf("\n");
}


int main()
{
   ofstream k1;
   k1.open("k1.csv");
    ofstream k2;
   k2.open("k2.csv");
    ofstream k3;
   k3.open("k3.csv");
   ofstream nAnalysis;
   nAnalysis.open("n.csv");
   int i,j,l,m,n,size,nr,r;
   long op;
   srand(time(NULL));
  
   k=4;
   n=20;
   for (i=1;i<=k;i++)
           {
                heap[i]=(node *)malloc(sizeof(node));
                heap[i]->val=rand() % (k);
                heap[i]->next=NULL;
                node *first=(node *)malloc(sizeof(node));
			    first=heap[i];
				printf("%d ",heap[i]->val);
               for (j=2;j<=n/k;j++)
               {
                   node *q=(node *)malloc(sizeof(node));
                   q->val=first->val+rand() % (k);
                   q->next=NULL;
                   first->next=q;
                   first=q;
				   printf("%d ",q->val);   
               }
			    printf("\n");
           }
           size=k;
           nr=1;
          build_max_heap(heap,size);
		  
           while (size!=0)
           {
               x[nr]=(node *)malloc(sizeof(node));
               x[nr]->val=heap[1]->val;
               heap[1]=heap[1]->next;
			   x[nr]->next=NULL;
			   nr++;

               if (heap[1]==NULL)
               {
                   heap[1]=heap[size];
                   size--;
				   heapsize--;
               }

               heapify(heap,1);
           }
   // print the result
	for(j=1;j<=n;j++)
		printf("%d ",x[j]->val);
 
	
	//k is constant

	k=5;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
          comp=0, attr=0;
		 
           for (l=1;l<=k;l++)
           {
               heap[l]=(node *)malloc(sizeof(node));
               heap[l]->val=rand() % (k);
               heap[l]->next=NULL;
               node *nod=(node *)malloc(sizeof(node));
			   nod=heap[l];
               for (m=2;m<=i/k;m++)
               {
                   node *q=(node *)malloc(sizeof(node));
                   q->val=nod->val+rand() % (k);
                   q->next=NULL;
                   nod->next=q;
                   nod=q;
               }
           }

           size=k;
           nr=1;

          build_max_heap(heap,size);

           while (size!=0)
           {
               x[nr]=(node *)malloc(sizeof(node));
               x[nr]->val=heap[1]->val;
               heap[1]=heap[1]->next;
			   x[nr]->next=NULL;
			   nr++;

               if (heap[1]==NULL)
               {
                   heap[1]=heap[size];
                   size--;
				   heapsize--;
               }

               heapify(heap,1);
           }

            op+=comp;

       }
		k1<<i<<","<<op/5<<"\n";
     
   }


   k=10;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
          comp=0, attr=0;
		 
           for (l=1;l<=k;l++)
           {
               heap[l]=(node *)malloc(sizeof(node));
               heap[l]->val=rand() % (k);
               heap[l]->next=NULL;
               node *nod=(node *)malloc(sizeof(node));
			   nod=heap[l];
               for (m=2;m<=i/k;m++)
               {
                   node *q=(node *)malloc(sizeof(node));
                   q->val=nod->val+rand() % (k);
                   q->next=NULL;
                   nod->next=q;
                   nod=q;
               }
           }

           size=k;
           nr=1;

          build_max_heap(heap,size);

           while (size!=0)
           {
               x[nr]=(node *)malloc(sizeof(node));
               x[nr]->val=heap[1]->val;
               heap[1]=heap[1]->next;
			   x[nr]->next=NULL;
			   nr++;

               if (heap[1]==NULL)
               {
                   heap[1]=heap[size];
                   size--;
				   heapsize--;
               }

               heapify(heap,1);
           }

            op+=comp;

       }
		k2<<i<<","<<op/5<<"\n";
     
   }

    k=100;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
          comp=0, attr=0;
		 
           for (l=1;l<=k;l++)
           {
               heap[l]=(node *)malloc(sizeof(node));
               heap[l]->val=rand() % (k);
               heap[l]->next=NULL;
               node *nod=(node *)malloc(sizeof(node));
			   nod=heap[l];
               for (m=2;m<=i/k;m++)
               {
                   node *q=(node *)malloc(sizeof(node));
                   q->val=nod->val+rand() % (k);
                   q->next=NULL;
                   nod->next=q;
                   nod=q;
               }
           }

           size=k;
           nr=1;

          build_max_heap(heap,size);

           while (size!=0)
           {
               x[nr]=(node *)malloc(sizeof(node));
               x[nr]->val=heap[1]->val;
               heap[1]=heap[1]->next;
			   x[nr]->next=NULL;
			   nr++;

               if (heap[1]==NULL)
               {
                   heap[1]=heap[size];
                   size--;
				   heapsize--;
               }

               heapify(heap,1);
           }

            op+=comp;

       }
		k3<<i<<","<<op/5<<"\n";
     
   }

   //n constant; n=10000
   for (i=10;i<=500;i=i+10)
    {
        op=0;
		for (j=1;j<=5;j++)
        {
            comp=0;

            for (l=1;l<=i;l++)
            {
                heap[l]=(node *)malloc(sizeof(node));
                heap[l]->val=rand() % (l);
                heap[l]->next=NULL;
              node *nod=(node *)malloc(sizeof(node));
				nod=heap[l];
                for (m=2;m<=10000/i;m++)  
                {
                    node *q=(node *)malloc(sizeof(node));
                    q->val=nod->val+rand() % (l);
                    q->next=NULL;
                    nod->next=q;
                    nod=q;
                }
            }

            size=i;
            nr=1;

            build_max_heap(heap,size);

            while (size!=0)
            {
               x[nr]=(node *)malloc(sizeof(node));
               x[nr]->val=heap[1]->val;
               heap[1]=heap[1]->next;
			   x[nr]->next=NULL;

                if (heap[1]==NULL)
                {
                    heap[1]=heap[size];
                    size--;
					heapsize--;
                }

                heapify(heap,1);
            }
			
             op+=comp;

        }
		nAnalysis<<i<<","<<op/5<<"\n";
   
    }

   k1.close();
   nAnalysis.close();
system("PAUSE");
return 0;
}
