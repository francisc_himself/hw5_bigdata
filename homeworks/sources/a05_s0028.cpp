#include "stdafx.h"
#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#define m 10007

int T[m];
int exista[m/2];
int max=-1;
int efort=0,efortTotal;

int h(int key, int i)//functia de hash quadratic probing
{
	int c1=1,c2=1;
	int p=(key % m + c1*i +c2*i*i) % m;
	return p;
}

int insert(int *T, int key)
{
	int i,j;
    i=0;
    do
	{
    j=h(key,i);
    if (T[j]==-1) 
		{
                    T[j]=key;
                    return j;
		}
    else i=i+1;
	} while (i < m);
	return -1;
}

void search(int *T, int key)
{
	efort=0;
	int i,j;
    i=0;
	do
	{
    j=h(key,i);
	efort=efort+1;
    if (T[j]==key) 
		break;
    i=i+1;
	}
    while ((T[j]!=-1)&&(i!=m));
	
	efortTotal=efortTotal + efort;//suma efort = efortul total

	if (efort > max) //max efort pt operatia de search
		max=efort;
}

void ecran(double alfa)
{
	for(int k=0;k<=7;k++)
		T[k]=-1;
	for (int k=0; k < alfa * 7  ; k++) 
		{
			int x=k%10000;

			insert(T,x);
	    }
	insert(T,8);
	for(int k=0; k<=7;k++)
		printf("%d ",T[k]);
	printf("\n");
}
void testare(double alfa)
{ 
	int x;
	int indice=0;

	int efMaxNegasite=0;
	int efMedNegasite=0;
	int efMedGasite=0;
	int efMaxGasite=0;
	efortTotal=0;
	max=0;

	for(int k=0;k<=m;k++)
		T[k]=-1;

	for (int k=0; k<m/2; k++)
		exista[k]=0;

	for (int j=0; j < alfa * m ; j++) 
		{
			x=rand()*rand()%100000;

			insert(T,x);
		
			if ((j%7)==0)
				{
					exista[indice]=x;
					indice++;
				}		
		}
	//cautare elemente care exista
	for (int k = 0; k < indice; k++)
		search(T,exista[k]);

	efMedGasite=efortTotal/indice; //efort mediu gasite
	efMaxGasite=max; //efort maxim gasite

	efortTotal=0;
	max=0;

	//cautare elemente ce nu sunt => negasite
	for (int k=0; k < (3000-indice) ; k++)
		{
			int y=rand()+ 100000;
			search(T,y);
		}
	efMedNegasite=efortTotal/(3000-indice); //efort mediu=suma eforturilor/nr elemente
	efMaxNegasite=max;//nr de efort maxim pt o op de search

	printf("\n %0.2f\t\t%d\t\t%d\t\t%d\t\t%d",alfa, efMedGasite, efMaxGasite, efMedNegasite,efMaxNegasite);
}
int main()
{
	srand(time(NULL));
	ecran(0.99);
	printf("\n Fact.umplere|\tgasiteEfMED|\tgasiteEfMAX|\tnegasiteEfMED|\tnegasitEfMAX|");
	testare(0.8);
	testare(0.85);
	testare(0.9);
	testare(0.95);
	testare(0.99);
	getch();
	return 0;
}


