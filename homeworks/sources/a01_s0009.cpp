/*Cazul favorabil: 
		Cel mai eficient:-bubble sort(are nr de atribuiri=0) complexitate:n
		Eficienta selectiei~Eficienta insertiei
  Cazul mediu statistic:
		Cel mai putin eficient:-bubble
		Cel mai eficient:insertie,selectie
		complexitate:
			bubble: n^2
			insert:n^2
			select:n^2
  Cazul defavorabil:
		Cel mai eficient:insertie
		Cel mai putin eficient:bubble
		complexitate:
			bubble: n^2
			insert:n^2
			select:n^2

Observatie:	La insertie cazul cel mai defavorabil la atribuiri este cazul cel mai favorabil la insertii
si invers.
						

*/


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>


#define MAX_SIZE 10000

unsigned long long atr_bubble,comp_bubble,atr_sel,comp_sel,atr_insert,comp_insert;

//sortarea bulelor


void interschimba(int *a, int *b)
{

	int s=0;
	s=*a;
	*a=*b;
	*b=s;
}

void bubbleSort(int a[], int n)
{
	int i,j; //contoare
	int sortat=0; 
	i=0;
	while(sortat==0)
{
	sortat=1;
	i++;

		for(j=0;j<n-i;j++)
		{
			comp_bubble++;
			if (a[j]>a[j+1])
		{

			atr_bubble=atr_bubble+3;
			interschimba(&a[j],&a[j+1]);
			sortat=0;
		}
		}
	}
}

void afis(int a[],int n)
{
	int i;
	for (i=0; i<n; i++)
		printf("%d ",a[i]);
}




//sortarea prin insertie
void insertionSort(int a[],int n)
{
	int i,j,x,k;

	for (i=1; i<n; i++)
	{
		atr_insert++;

		x=a[i];
		j=0;

		comp_insert++;

		while (a[j]<x)
		{
			comp_insert++;
			j++;
		}
		for (k=i; k>=j+1; k--)
		{
			atr_insert++;
			a[k]=a[k-1];
		}
		atr_insert++;
		a[j]=x;
		
	}

}
//sortarea prin selectie
void selectionSort(int a[],int n)
{

	int i=0;
	int min,imin,j;
	for (int i=0; i<n-1; i++)
	{
		atr_sel++;
		min=a[i];
		imin=i;
		for (j=i+1; j<n; j++)
		{
			comp_sel++;
			if (a[j]<min) 
			{
				
				atr_sel++;
				min=a[j];
				imin=j;
			}
		}
		
		interschimba(&a[i],&a[imin]);
		atr_sel=atr_sel+3;

	}
}


int main()
{
	int a[MAX_SIZE];
	int b[MAX_SIZE];
	int c[MAX_SIZE];
/*
	a[0]=2;
	a[1]=1;
	a[2]=5;
	a[3]=2;
	a[4]=8;
	a[5]=11;
	a[6]=7;

	for (int i=0; i<7 ;i++)
	{
		c[i]=a[i];
		b[i]=a[i];
	}
	bubbleSort(a,7);
	afis(a,7);
	printf("\n");
	selectionSort(b,7);
	afis(b,7);
	printf("\n");
	insertionSort(c,7);
	afis(c,7);

*/

	FILE *file1;//cazul favorabil
	FILE *file2;//cazul mediu-statistic
	FILE *file3;//cazul defavorabil
	
	
	//cazul favorabil

	file1 = fopen("Favo.csv", "w");
	
	
	fprintf(file1, "n,Atr_Bubble,Atr_Sel,Atr_Insert,Comp_Bubble,Comp_Sel,Comp_Insert,Suma_Bubble,Suma_Sel,Suma_Insert\n");
	
	for(int n=100; n<MAX_SIZE; n += 100)
	{
		
		a[0]=rand();
		b[0]=a[0];
		c[0]=a[0];
		for (int i=1; i<n; i++)
		{
			a[i]=rand()+a[i-1];
			b[i]=a[i];
			c[i]=a[i];
		}
		bubbleSort(a,n);
		insertionSort(b,n);
		selectionSort(c,n);
		fprintf(file1, "%d,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n",n,atr_bubble,atr_sel,atr_insert,comp_bubble,comp_sel,comp_insert,atr_bubble+comp_bubble,atr_sel+comp_sel,atr_insert+comp_insert);
atr_bubble=0;
atr_sel=0;
atr_insert=0;
comp_bubble=0;
comp_sel=0;
comp_insert=0;

	
	}
	
	
	fclose(file1);
	

	file2 = fopen("Med.csv", "w");
	fprintf(file2, "n,Atr_Bubble,Atr_Sel,Atr_Insert,Comp_Bubble,Comp_Sel,Comp_Insert,Suma_Bubble,Suma_Sel,Suma_Insert\n");

	
	

	


//mediu
for(int n=100; n<MAX_SIZE; n += 100)
{
		
	for (int j=1;j<=5; j++)
	{
		for (int i=0; i<n; i++)
		{	
			
			a[i]=rand();
			b[i]=a[i];
			c[i]=a[i];
		}
		bubbleSort(a,n);
		insertionSort(b,n);
		selectionSort(c,n);
	}
	fprintf(file2, "%d,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n",n,atr_bubble/5,atr_sel/5,atr_insert/5,comp_bubble/5,comp_sel/5,comp_insert/5,(atr_bubble+comp_bubble)/5,(atr_sel+comp_sel)/5,(atr_insert+comp_insert)/5);

atr_bubble=0;
atr_sel=0;
atr_insert=0;
comp_bubble=0;
comp_sel=0;
comp_insert=0;
}

	fclose(file2);



	file3 = fopen("Defavo.csv", "w");
	

	fprintf(file3, "n,Atr_Bubble,Atr_Sel,Atr_Insert,Comp_Bubble,Comp_Sel,Comp_Insert,Suma_Bubble,Suma_Sel,Suma_Insert\n");

	


	for(int n=100; n<MAX_SIZE; n += 100)
	{
		
		a[n-1]=rand();
		b[n-1]=a[n-1];
		c[n-1]=a[n-1];

		for (int i=n-2; i>=0; i--)
		{
			a[i]=a[i+1]+rand();
			b[i]=a[i];
			c[i]=a[i];
		}
		bubbleSort(a,n);
		insertionSort(b,n);
		selectionSort(c,n);
		fprintf(file3, "%d,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu,%llu\n",n,atr_bubble,atr_sel,atr_insert,comp_bubble,comp_sel,comp_insert,atr_bubble+comp_bubble,atr_sel+comp_sel,atr_insert+comp_insert);
atr_bubble=0;
atr_sel=0;
atr_insert=0;
comp_bubble=0;
comp_sel=0;
comp_insert=0;
	}
	
	
	fclose(file3);
	

	getch();
	return 0;

}
