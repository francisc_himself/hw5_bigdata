/* ***ANONIM*** ***ANONIM*** Mihai
		  ***GROUP_NUMBER***
	 Tema 5-Hash Tables 
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <fstream>
#include <iostream>

using namespace std;

ofstream g("date.csv");

#define m 9973

int hashTable[m], gasit[1501], negasit[1501], aux[m];

int s = 1500, N = m;

float fillFactor[] = {0.80, 0.85, 0.9, 0.96, 0.99};
float efort;
float efortGasit,efortNegasit,efortGasitMediu,efortNegasitMediu;
float gasitAux, negasitAux;
float gasitToate, negasitToate;


int h(int k,int i){
	int aux = (k%N+1*i+1*(i*i))%N;
	return aux;
}


void initialization(int k){
	int i;
	for(i = 0;i<k;i++){
		hashTable[i] = 0;
	}
}

int hashInsert(int k){
	int i=0;
	int j;
	efort = 0;
	do{
		j=h(k,i);
		if(hashTable[j] == k)
			return 0;
		if(hashTable[j]==NULL){
			hashTable[j] = k;
			return j;
		}else{
			i++;
			efort++;
		}
	}while(i!=N);
	
	return 0;
}

int hashSearch(int k){
	int i = 0;
	efort = 0;
	int j;
	do{
		j = h(k,i);
		efort++;
		if(hashTable[j]==k){ 
			return j;
		}
		i++;
		
	}while(hashTable[j]!=NULL && i!=N);
	return 0;
}

void hashPrint(int hashTable[],int N){
	int i=0;
	for(i=0;i<N;i++){
		cout<<i<<endl;
		if(hashTable[i]!=0){
			cout<<" "<<hashTable[i];
		}
	}
}

int generate(int n){
	int x = (1 + rand()*rand())%n;
	return x;
}

void generateFound(int n){
	int i;
	for(i=0;i<1500;i++){
		int x=rand()%n;
		while(aux[x] ==0)
			x=rand()%n;

		gasit[i]=aux[x];
		aux[x] = 0;
	}
}

void generateNotFound(int n){
	int i;
	for(i=1;i<1500;i++){
		negasit[i]= n+rand();
	}
}

void main(){
	int nr=0;
	float alfa;
	int x,i,init;
	srand(time(NULL));
	
	
	N=23;
	s = N/3;
	nr = alfa = x = 0;

	initialization(N);
	
	for(i=0;i<=s;i++){
		gasit[i]=0;
	}
	init=0;
	do{ 
		x=generate(200);

		if(hashSearch(x)==0){
		int key = hashInsert(x);

			if(key!=0){
				nr++;
				if(nr%3==0){
					gasit[nr/3] = x;
				}
			}
		}
		alfa = (float) nr/N;
	}while(alfa<0.85);
	
	//hashPrint(hashTable,N);
	
	generateNotFound(10);

	int j;
	for(j=0;j<s;j++){

		efortGasit = efort;
		gasitAux = gasitAux + efortGasit;
		if(efortGasit>efortGasitMediu){
			efortGasitMediu = efortGasit;
		}
		efort = 0;
		
		
		efortNegasit = efort;
		negasitAux = negasitAux + efortNegasit;
		if(efortNegasit>efortNegasitMediu){
			efortNegasitMediu = efortNegasit;
		}
		efort = 0;
	}
	
	cout<<"FillFactor |||"<<"AvgEffFound |||"<<"MasEffFound |||"<<"AvgEffNotFound |||"<<"MaxEffNotFound "<<endl;
	cout<<(float)alfa<<"          "<<(float)gasitAux/s<<"            "<<(float)efortGasitMediu<<"           "<<(float)negasitAux/s<<"               "<<(float)efortNegasitMediu;

	///////////////////////////////////////////////////////

	


	g<<"FillFactor;"<<"Average Effort Found;"<<"Max Effort Found;"<<"Avgerage Effort NotFound;"<<"Max Effort NotFound"<<endl;
	N = m;
	for(i=0;i<5;i++)
	{
		int k;
		for(k=0;k<5;k++) //se fac 5 inserari si 5 cautari pentru fiecare factor de umplere
		{
			initialization(m);
			alfa = nr = 0;
			do{
				x = generate(500000);
				if(hashSearch(x)==0){
					if(hashInsert(x)){
						nr++;
						aux[nr] = x;
						init = x;
					}
			}
			alfa = (float) nr/N;
			}while (alfa<fillFactor[i]);
			
			generateFound(nr);
			generateNotFound(800000);

			int j;
			for(j = 0;j<1500;j++){
				
				hashSearch(gasit[j]);
				efortGasit = efort;
				gasitAux += efortGasit;
				if(efortGasit>efortGasitMediu){
					efortGasitMediu = efortGasit;
				}
				efort = 0;

				hashSearch(negasit[j]);
				efortNegasit = efort;
				negasitAux += efortNegasit;
				if(efortNegasit>efortNegasitMediu){
					efortNegasitMediu = efortNegasit;
				}
				efort = 0;
			}
			gasitToate = gasitToate + (float)gasitAux/1500;
			negasitToate = negasitToate + (float)negasitAux/1500;
			
			gasitAux = negasitAux =0;
		}
		g<<fillFactor[i]<<";"<<gasitToate/5<<";"<<efortGasitMediu<<";"<<negasitToate/5<<";"<<efortNegasitMediu<<endl;
		nr=0;
		efortGasitMediu = efortNegasitMediu = gasitToate = negasitToate = efortGasit = efortNegasit = 0;
	}
	
	
}