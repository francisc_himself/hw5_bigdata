#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <windows.h>
#define MAX 10000

int heapSize=0;
int n=100;
int comp,at;

typedef struct nod{
			int key;
			struct  nod *urm;
			}Nod;

Nod *listOfLists[1000];
Nod* resultList;

typedef struct heapElement{
					int key;
					int listIndex;
					}Heap;
Heap heap[1000];


void creare(int *a,int n)
{
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%MAX;
	}
}


void creareCrescator(int *a,int n)
{	
	
	
	a[0]=rand()%MAX;

	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]+rand()%MAX;
	}
	
}
void creareDescrescator(int *a,int n)
{
	
	srand(time(NULL));
	a[0]=rand()%MAX;
	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]-rand()%MAX;
	}
}

void afisareValori(int *a,int n)
{

	for(int i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
}

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return (2*i);
}

int right(int i)
{
	
	return (2*i+1) ;
}


void heapify(int i)
{

	int smallest;
	int	l=left(i);
	int	r=right(i);
	comp=comp+2;
	
	if ((l<=heapSize) && (heap[l].key<heap[i].key))
	{
		smallest=l;
	}
	else 
	{
		smallest=i;
	}
	if ((r<=heapSize) && (heap[r].key<heap[smallest].key))
	{
		smallest=r;
	}
	if(smallest!=i)
	{
		Heap x=heap[i];
		heap[i]=heap[smallest];
		heap[smallest]=x;
		at=at+3;
		heapify(smallest);
	}
}

Heap H_Pop()
{
	Heap x;
	x=heap[1];
	heap[1]=heap[heapSize];
	heapSize=heapSize-1;
	heapify(1);
	return x;
}

void inserare(Nod **first,Nod *p)
{
	p->urm=NULL;
	if(*first==NULL)
	{
		*first=p;
	}
	else
	{
		(*first)->urm=p;
		*first=p;
	}
}
void H_Push(int value,int listIndex)
{
	heapSize=heapSize+1;
	heap[heapSize].key=value;
	heap[heapSize].listIndex=listIndex;
	int i=heapSize;
	at=at+1;
	comp=comp+1;
	while((i>1) &&(heap[parent(i)].key>heap[i].key))
	{
		Heap y=heap[i];
		heap[i]=heap[parent(i)];
		heap[parent(i)]=y;
		at=at+3;
		comp=comp+1;
		i=parent(i);
	}
}

void afisareLista(Nod *p)
{
	while(p!=NULL)
	{
		printf("%d ",p->key);
		p=p->urm;
	}
	printf("\n\n");
}
void buildKListe(int nTotal,int nrListe)
{
	int n=nTotal/nrListe;
	int* sortedArray=(int*) malloc(sizeof(int)*n);
	for(int i=0;i<nrListe;i++)
	{
		creareCrescator(sortedArray,n);
		nod *first=NULL;
		nod *prim=NULL;
		for(int j=0;j<n;j++){
			nod* x=(nod*) malloc(sizeof(Nod));
			x->key=sortedArray[j];
			inserare(&first,x);
			if(j==0)
			{
				prim=first;
			}
			first=x;
			
		}
		listOfLists[i]=prim;
		afisareLista(listOfLists[i]);
	}
}

void ordonare(int k)
{
	int i=0;
	Nod *prim=NULL;
	int value;
	Heap aux;
	for(int i=0;i<k;i++)      
	{
		at=at+1;
		value=listOfLists[i]->key;
		H_Push(value,i); 	
		listOfLists[i]=listOfLists[i]->urm;
	}
	comp=comp+1;
	while (heapSize>0){
		aux=H_Pop();
		nod *x=(nod*) malloc(sizeof(nod));
		x->key=aux.key;
		comp=comp+3;
		at=at+2;
		inserare(&resultList,x);
		if(i==0)
		{
			at=at+1;
			prim=resultList;
		}
		i++;
		if ( (listOfLists[aux.listIndex])!=0 )
			{
				value=listOfLists[aux.listIndex]->key;
				H_Push(value,aux.listIndex);
				listOfLists[aux.listIndex]=listOfLists[aux.listIndex]->urm;
				at=at+1;
			}
 	}
	resultList=prim;
	at=at+1;
}

void main()
{
	srand((unsigned int)time(NULL));
	int i;
	int k=5,n=25;
	buildKListe(n,k);
	for(i=0;i<k;i++);
	{
		afisareLista(listOfLists[i]);
	}
	ordonare(k);
	afisareLista(resultList);
	
	getch();
}