#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<String.h>


#define nr_vf  101   //numarul maxim de noduri ***ANONIM*** graf

//definirea culoarilor
#define alb 0        
#define gri 1
#define negru 2

typedef struct nod
{
int key;            //camp in care se va memora numarul nodului
struct nod *urm;    //se va retine referinta la nodul urmator
}NOD;                    //Element de baza al listei de adiacenta ce contine vecinii fiecrui nod

NOD *Adj[nr_vf];         //vector in care fiecare casuta va retine referinta la lista de adiacenta a vecinilor nodului ***ANONIM*** graf aflat la 
                         //indexul respectiv    

int pi[nr_vf],d[nr_vf],f[nr_vf],cul[nr_vf];  //vectorii de parinte,timpul descoperirii, timpul terminarii, si culoare
int time;                                    // variabila ce retine timpul

char rezultat[nr_vf+nr_vf];                 // string ce retine  rezultatul parcurgerii DFS a grafului generat
char aux[5];

  
int numar_de_varfuri; // retine numarul de varfuri ale grafului        
double media_a=0,media_c=0;  //retine media opertatiilor de atribuire si comparatii
double a;   //retine numarul de atribuiri
int nr_c;      //retine numarul de comparatii

//-------Algoritm de parcurgere si etichetare muchii--------//
void DFS_VISIT(int u)
{
  NOD *v;

  cul[u]=gri;  a=a+1;
  time++;      a=a+1;
  d[u]=time;   a=a+1;

  v=Adj[u];    a=a+1; 
  nr_c++;
  while(v!=0)    //parcurgere lista de adiacenta a vecinilor
  {
	  nr_c++;
	  
	  nr_c++;
	  if(cul[v->key]==alb)     //testeaa daca varful e nevizitat
		  {
			  pi[v->key]=u;  a=a+1;   
			  printf("\n (%d,%d) muchie arbore",u,v->key);
			 
			  DFS_VISIT(v->key);
	       }
	       else 
	       {   
		     nr_c++;
		     if(cul[v->key]==gri)   printf("\n (%d,%d) muchie inapoi",u,v->key);        //afisare & etichetare muchii
	         else
	         {
		        nr_c++;
		        if(d[u]>f[v->key])   printf("\n (%d,%d) muchie transversala",u,v->key); //afisare & etichetare muchii
	            else  printf("\n (%d,%d) muchie inainte",u,v->key);
		     }
	       }
	 v=v->urm;  a=a+1; 
  }

  cul[u]=negru; a=a+1; 
  time++;       a=a+1;
  f[u]=time;    a=a+1; 
 
}

void DFS()
{
   int u;

   for(u=1;u<numar_de_varfuri+1;u++)  
   {
	   cul[u]=alb;
       a=a+1;
   }
   time=0;   
   for(u=1;u<numar_de_varfuri+1;u++)
   {   
	  nr_c++;
	  if(cul[u]==alb)
	      DFS_VISIT(u);
	   
   }

}
//------Functie folosita la analiza algoritmului de DFS cand numarul de varfuri variaza de la 10 la 100--------//
void DFS_VISIT2(int u)
{
  NOD *v;

  cul[u]=gri;  a=a+1;
  time++;      a=a+1;
  d[u]=time;   a=a+1;

  v=Adj[u];    a=a+1; 
 
  nr_c++;
  while(v!=0)    //parcurgere lista de adiacenta 
  {
	  nr_c++;
	  nr_c++;
	  if(cul[v->key]==alb)
		  {
			 // strcpy(aux,"(");
              strcat(rezultat,aux);
			  itoa(v->key,aux,10);
			  strcat(rezultat,aux);

			
			  pi[v->key]=u;  a=a+1;   
			  printf("\n (%d,%d) muchie arbore",u,v->key);
			 
			  DFS_VISIT(v->key);
	  }
	  else 
	  {   
		  nr_c++;
		  if(cul[v->key]==gri)   printf("\n (%d,%d) muchie inapoi",u,v->key);
	      else
	             {
		            nr_c++;
		            if(d[u]>f[v->key])   printf("\n (%d,%d) muchie transversala",u,v->key);
	                else  printf("\n (%d,%d) muchie inainte",u,v->key);
		         }
	  }
	  v=v->urm;  a=a+1; 
  }

  cul[u]=negru; a=a+1; 
  time++;       a=a+1;
  f[u]=time;    a=a+1; 
 
  //printf(" %d )",u);
  itoa(u,aux,10);
  strcat(rezultat,aux);
  strcpy(aux,")");
  strcat(rezultat,aux);
}

void DFS2()
{
   int u;

   for(u=1;u<numar_de_varfuri+1;u++)  
   {
	   cul[u]=alb;
       a=a+1;
   }
   time=0;   
   for(u=1;u<numar_de_varfuri+1;u++)
   {   
	   nr_c++;
	   if(cul[u]==alb)
	   {
		   // printf("( %d ",u);
		    strcpy(aux,"(");
            strcat(rezultat,aux);
		    itoa(u,aux,10);
		    strcat(rezultat,aux);
		      
			DFS_VISIT2(u);
	   }
	}

     printf("\n \n rezultatul parcurgerii: \n %s",rezultat);

}

//--------Algoritm de generare Grafuri aleatoare-------//
void generare_graf(int numar_varfuri)
{
  int u,v,nr,ok;
  int numar_muchii=2*numar_varfuri;
  NOD *vecin,*p,*q;
  
  for(nr=0;nr<nr_vf;nr++)  Adj[nr]=0;

  numar_de_varfuri=numar_varfuri;
  
  for(nr=0;nr<numar_muchii;nr++)
  {
    
    ok=0;
	while(ok==0)
	{
	   ok=1;
	
	   u=rand()%(numar_varfuri+1);
       v=rand()%(numar_varfuri+1);

	   if((v==0)||(u==0))  ok=0;
	   else
	   {
	     if(Adj[u]==0)
		 {
		   p=(NOD*)malloc(sizeof(NOD));
		   p->key=v;
		   p->urm=0;
		   Adj[u]=p;
		 }
		 else{
		   vecin=Adj[u];
		   while(vecin!=0)
		   {
			   if(vecin->key==v) ok=0;
		       q=vecin;
			   vecin=vecin->urm;
		   }
		   if(ok==1) 
		   {
		     p=(NOD*)malloc(sizeof(NOD));
		     p->key=v;
		     p->urm=0;
			 q->urm=p;
		   }
		 }
	   
	   }
	}
  
  }
}
//=====================================================
  void afisare()   //functie folosita la afisa grafului sub forma de varfuri carora sunt atasate listele de adiacenta ce contin vecinii acestora
 {
    int i;
	NOD *q;

	for(i=1;i<numar_de_varfuri+1;i++)
	{
		printf("\n Vecinii lui %d: ",i);
		q=Adj[i];
		while(q!=0)
		 { 
			 printf(" %d ",q->key);
			 q=q->urm;
		}
	}
    
	printf("\n");
  }


 void test()  //Caz de test...genereaza un graf de 6 varfuri pe care se poate verifica daca algorimul functioneaza cum trebuie
 {
	NOD *p,*q;
	 
     
    p=(NOD*)malloc(sizeof(NOD));
		     p->key=2;
		     p->urm=0;
	q=(NOD*)malloc(sizeof(NOD));
		     q->key=4;
		     q->urm=0;
	p->urm=q; 
	Adj[1]=p;
   
	p=(NOD*)malloc(sizeof(NOD));
		     p->key=3;
		     p->urm=0;
    Adj[2]=p;
   
    p=(NOD*)malloc(sizeof(NOD));
		     p->key=4;
		     p->urm=0;
    Adj[3]=p;
   
	p=(NOD*)malloc(sizeof(NOD));
		     p->key=2;
		     p->urm=0;
    Adj[4]=p;
    
	p=(NOD*)malloc(sizeof(NOD));
		     p->key=3;
		     p->urm=0;
    q=(NOD*)malloc(sizeof(NOD));
		     q->key=6;
		     q->urm=0;
			 p->urm=q;
    Adj[5]=p;
   
    p=(NOD*)malloc(sizeof(NOD));
		     p->key=6;
		     p->urm=0;
    Adj[6]=p;

	afisare();
	printf("\n");
	
	nr_c=0;
	DFS();
	
   
	
    
  }

  void reset()   ///realizeaza setarea variabilelor la valorile pe care le-au avut inaintea rularii algorimului DFS
	            //este necesara pentru a preveni eventuale erori  logice datorita datelor obtinute anterior ***ANONIM*** alt caz de test
  {
	  int h;
         
	     a=0;
		 nr_c=0;
		 for(h=0;h<nr_vf;h++);
		 {
		   f[h]=0;
		   d[h]=0;
		   Adj[h]=0;
		   pi[h]=0;
		   cul[h]=0;
		 }
  
 }
  /*
  Functia de mai jos realizeaza:
    -Algoritmul DFS corect implementat si aplicat pe un graf orientat avand 10 varfuri
	-Etichetarea muchiilor in muchii arbore,inainte,inapoi si transversala
	-calculand  media sumei ***ANONIM***tre atribuiri si comparatii, in cazul etichetarii muchiilor unui graf orientat cu 10 noduri.
  */
  
  void calculare()
{
	 int caz;
  
     
         reset();
		 printf("================================\n");
		 printf("\n CAZ %d\n",caz+1);
		 printf("---------------------------------\n");
	     generare_graf(10);
	     afisare();
         DFS2();

         printf("\n \n atribuiri= %2.0f comparatii=%d atribuiri+comparatii=%d",a,nr_c,(int)a+nr_c);
		 media_a=media_a+(int)a;
		 media_c=media_c+nr_c;
         printf("\n================================\n");
	
	 printf("\n atribuire(medie)= %2.2f comparatii(medie)=%2.2f Atribuiri+comparatie(medie)=%2.2f",media_a/5,media_c/5,(media_a+media_c)/5);
     getch();
  }

  void grafuri_aleatore()	// analiza algoritmului DFS pentru grafuri aleatoare avand numarul de varfuri variabil intre 10 si 100
							// dupa rularea algoritmului DFS pentru fiecare ***ANONIM*** valorile pe care le ia numarul de varfuri se va scrie in fisier 
							//numarul de atribuiri si comparatii precum si suma ***ANONIM***tre ele pentru cazul respectiv
  {
     FILE *pf;
	 int noduri;

	 pf=fopen("analiza.txt","w"); 
	 for(noduri=10;noduri<100;noduri=noduri+10)
	 {
		reset();
		generare_graf(noduri);
	    DFS();
		fprintf(pf,"%d \t %d\t %d\t %d\n",noduri,(int)a,nr_c,nr_c+(int)a);
	 
	 }
    fclose(pf);
  
 }

  void main()
  {
   calculare();   
   grafuri_aleatore();  
	
}
