#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>
#define max 10000
using namespace std;

int size;

int asgn, comp;

void heapify(int A[], int i, int heapsize)
{
	int l, r, largest;
	l = 2*i;
	r = l+1;
	
	comp++;
	if(l <= heapsize && A[l] > A[i])
		largest = l;
	else
		largest = i;
	comp++;
	if(r <= heapsize && A[r] > A[largest])
		largest = r;
	if(largest != i)
	{
		int temp;
		temp = A[i];
		A[i] = A[largest];
		A[largest] = temp;
		heapify(A, largest, heapsize);
		asgn=asgn+3;
	}
}

void bottomup_buildheap(int A[], int n)
{
	int heapsize;
	heapsize = n;
	for(int i = (n/2); i >= 1; i--)
		heapify(A, i, heapsize);
}


void HeapIncreaseKey(int A[],int i, int key)
{
	int aux;

	A[i]=key;
	asgn++;
	
	comp++;
	while(i>1 && A[i/2]<A[i])
	{
		aux=A[i/2];
		A[i/2]=A[i];
		A[i]=aux;
		i=i/2;
		asgn=asgn+3;
		comp++;
	}
	
}

void MaxHeapInsert(int A[],int key)
{
	size++;
	A[size]=-100000;
	asgn++;
	HeapIncreaseKey(A,size,key);
}

void BuildMaxHeap_topdown(int  A[],int n)
{
	int i;
	size=1;
	for(i=2;i<=n;i++)
		MaxHeapInsert(A,A[i]);
}

void listInInorder(int A[],int i,int n){
	
	int ok=1;
	double j=i,count=0;
	if(i<=n){ 

	listInInorder(A,2*i+1,n);

	do{
		if(i>=pow(2,count) && i<pow(2,count+1))
			ok=0;
		count++;
	}while(ok!=0);

	for(int k=1;k<count;k++) cout<<"      "; 

	cout<<A[i]<<" \n";
	listInInorder(A,2*i,n);
	
	}
}



int main()
{
	int i, n, heapsize;
	//n=100;
	cout << "Enter the no of elements: ";
	cin >> n;
	int A[max],b[max],c[max];
	ofstream f;
	srand(time(NULL));
	/*f.open("worst.csv");
	f<<"n,asgn_bottom_up,comp_bottom_up,asgn+comp_bottom_up,asgn_top_down,comp_top_down,asgn+comp_top_down \n";
	while(n!=10000)
	{	
		f<<n<<",";
		for(int i=1;i<=n;i++)
			A[i]=i;
		for(i=1;i<=n;i++)
		{
			b[i]=A[i];
			c[i]=A[i];
		}
		n=n+100;
		asgn=0;
		comp=0;
		bottomup_buildheap(b,n);
		f<<asgn<<","<<comp<<","<<asgn+comp<<",";
		asgn=0;
		comp=0;
		BuildMaxHeap_topdown(c,n);
		f<<asgn<<","<<comp<<","<<asgn+comp<<"\n";
		asgn=0;
		comp=0;

	}*/

	for(i=1;i<=n;i++)
	{
		cout<<"A["<<i<<"]=";
		cin>>A[i];
	}
	
	for(i=1;i<=n;i++)
		{
			b[i]=A[i];
			c[i]=A[i];
		}
	
	
	bottomup_buildheap(b,n);
	listInInorder(b,1,n);
	cout<<endl<<endl<<endl;

	BuildMaxHeap_topdown(c,n);
	listInInorder(c,1,n);

		
	getch();
	


		
}