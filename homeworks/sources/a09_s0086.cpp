#include <conio.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <limits>
#include <cmath>

#define MAX 101

using namespace std;



enum colors {BLACK, WHITE, GRAY};

int color[MAX], d[MAX], p[MAX], vertex, edge;


void BFS(vector<int> G[], int s)
{
    for(int i=0; i<=vertex; i++) 
	{

        color[i] = WHITE;

		d[i] = _MAX_INT_DIG;
        p[i] = NULL;
    }

    color[s] = GRAY;

    d[s] = 0;
    p[s] = NULL;

    queue<int> Q; Q.push(s);

    while( !Q.empty()) 
	{
        int u = Q.front(); Q.pop();
        for(int i=0; i<G[u].size(); i++) 
		{
            if(color[G[u][i]] == WHITE) 
			{
                color[G[u][i]] = GRAY;

                d[G[u][i]] = d[u] + 1;
                p[G[u][i]] = u;

                Q.push(G[u][i]);
            }
        }

        color[u] = BLACK;

    }

}

void PRINT_PATH(vector<int> G[], int s, int v)
{
    if(v == s)
        cout << s;
    else  if(p[v] == NULL)
			cout << "no path from " << s << " to " << v << " exists";
    else
	{
		 PRINT_PATH(G, s, p[v]);

		 cout << " -> ";
		 cout << v;
    }

}

int main(void)

{
    vector<int> adjList[MAX];
    int u, v;

    cin >> vertex >> edge;
    for(int e=1; e<=edge; e++)
	{
        cin >> u >> v;

        adjList[u].push_back(v);
        adjList[v].push_back(u);
   }

    BFS(adjList, 1);

	cout << "Dati 2 noduri";
	cin >> u >> v;
	PRINT_PATH(adjList, u, v);

	getch();

}

 


