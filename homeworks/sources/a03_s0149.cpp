﻿#include <stdio.h>
#include <process.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
/******ANONIM*** ***ANONIM*** ,GR ***GROUP_NUMBER***
 *** Heap vs Quick
 **Se cere implementarea  corectă  și  eficientă  a două metode de construire a structurii de date Heap i.e. “de jos în sus” (bottom-up) și “de sus în jos” (top-down) +  Quicksort => metode avansate de sortare.
 ** Quick_sort este mai rapidã decât orice metoda de sortare simplã, se executã bine pentru fisiere sau tabele mari, dar ineficient pentru cele 
 *  mici.Strategia de baza folositã este "divide et impera". 
 **   Heapsort este un algoritm de sortare care nu necesita memorie suplimentara (sorteaza vectorul pe loc), care functioneaza in doua etape: 
 *       [A]  transfomarea vectorului de sortat intr-un arbore heap; 
 *       [B]  reducerea  treptata  a  dimensiunii  heap-ului  prin  scoaterea  valorii  din  radacina. 
 *     Valorile rezultate in ordine descrescatoare sint plasate in vector pe spatiul eliberat, de la dreapta la stinga. 
 **Ordinul de complexitate la heapsort este T(N)O(NlogN),si la Quicksort T(N)=O(NlgN) in cazurile favorabil si mediu statistic si O(N^2) in cazul defavorabil.
 **Quicksort e un algoritm mult mai stabil decat Heapsort,si de aceea este mai folosit mai des.
 */

FILE *g;
int c;   // retine numarul de operatii critice pentru Bottom-Up
int c1;  // retine numarul de operatii critice pentru Top-Down
int a; //Bottom-Up
int A1; //Top-Down
int lung;
int aq; // atribuiri quick sort
int Cq; //comparatii quick sort
int qu[10001];


////////////////////////////////////////////////bottom-up
void max_heapify(int *A,int i, int m){       
 int l, r, max, aux; 

 l = 2*i;
 r = 2*i + 1;
 c++;
 if ((l <= m) && (A[l]>A[i])) 
 {   max = l;
 }   else { max = i; }
 c++;
 if ((r<=m) && (A[r]>A[max]))
 { max = r; }
 if (max != i) {
  aux = A[i];
  A[i] = A[max];
  A[max] = aux;
  a=a+2;
  max_heapify(A,max,m);
 }
}

void build_max_heap1(int *A , int m) {  //bottom-up
 int i;
 for (i=m/2;i>=0;i--)
  max_heapify(A,i,m);
}

int heap_extract_max(int *A, int *m) { // pop
 int max,aux;
 if (*m < 1)
  printf ( "heap underflow" );
 max = A[1];
 aux = A[1];
 A[1]= A[*m];
 A[*m]=aux; a++;
 m=m-1;
 max_heapify(A, 1, *m);
 return max;
}



///////////////////////////////////////////////////////////////////////top -down
void heap_increase_key(int *A, int i, int key) { // push
 int aux;
 if ( key < A[i])
  printf (" error : noua cheie este mai mica decat cheia curenta");
 A[i]=key;
 A1++; c1++;
 while ((i > 0) && (A[i/2] < A[i])) {
  aux = A[i];
  A[i]= A[i/2];
  A[i/2]=aux;
  i=i/2;
  A1=A1+2;
  c1=c1+1; }
}

void max_heap_insert(int *A, int key) {
 lung++;
 A[lung] = -1;
 heap_increase_key(A, lung, key);
}

void build_max_heap2(int *A , int m){    //top-down
 int i;
 lung =0;
 for (i = 1;i < m ; i++) {
  max_heap_insert(A, A[i]);
 }
}

void HeapSort(int *A, int m)
{
	int i;
	int aux;
	build_max_heap1(A,lung);
	for (i=m;i>=2;i--)
	{
		aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		lung--;
		//c=c+3;
		max_heapify(A,1,lung);
	}
}

/////////////////////////////////////////////////////////////////////  QuickSort

int partition( int lq, int rq) {
	int pivot, iq, jq, tq;
	pivot = qu[lq];
	iq = lq; jq = rq+1;

	while(true)
	{
		do{ ++iq; Cq++;} while( qu[iq] <= pivot && iq <= rq );
		do{ --jq; Cq++;} while( qu[jq] > pivot );
		if( iq >= jq ) break;
		tq = qu[iq]; qu[iq] = qu[jq]; qu[jq] = tq;
		aq+=2;
	}
	tq = qu[lq]; qu[lq] = qu[jq]; qu[jq] = tq; aq+=2;
	return jq;
}

void quickSort(  int lq, int rq)
{
	int jq;

	if( lq < rq ) 
	{
		// divide ***ANONIM*** conquer (devide et impera)
		jq = partition( lq, rq);
		quickSort( lq, jq-1);
		quickSort( jq+1, rq);
	}

}



/////////////////////////////////////////////////////////////

int main( void )
{
 int m=100;  // variabila care retine marimea vectorului care urmeaza sa fie ordonat
 int x;
 int k;
 int q;
 int i;  
 int j;   // variabile auxiliare
 int v[10001];  // vectorul propriu-zis
 int u[10001];

 int z;  
int n; //nr de elemente
 c=0;// initializarea contorului de operatii critice
  c1=0;
  a=0;
  A1=0;
  aq=0;
  Cq=0;
int sel;
 fopen_s( &g, "Out.csv", "w" );   // fisierul de iesire 

printf("Introduceti \n1-Manual\n2-Automat");
scanf("%d",&sel);

switch(sel)
{
case 2:
  
	while (m<=10000) // testarea dimensiunii maxime 
 {
 
   for (q=0;q<1;q++)
  {
   for (j=0;j<m;j++)
   {

	  
    z=r***ANONIM***();  //  alocarea unor elemente aleatoare vectorului 
    v[j]=z;
    u[j]=z;
    qu[j]=z; 
   }         
   
    build_max_heap1(v,m); // bottom up strategy 
    build_max_heap2(u,m); // top down strategy
	HeapSort(v,m);
	HeapSort(u,m);
   quickSort(0,m-1);
   sr***ANONIM*** ( time(NULL) );  // schimbarea seedului  
   }  
   fprintf_s(g,"%d;%d;%d;%d;%d;%d;%d;%d;%d;%d\n",m,a,c,a+c,A1,c1,A1+c1,aq,Cq,aq+Cq); // scrierea in fisierul de iesire a valorilor medii de op. critice

   m=m+100;   // schimbarea dimensiunii vectorului
	}  
	fclose( g ); 
break;

case 1:
printf("Introduceti n:\n");
	scanf("%d",&n);
	for (int ii = 0; ii<n; ii++)
		scanf("%d",&qu[ii]);

printf("\n\nSirul introdus ");
   for(int i1=0; i1<n; i1++)
		printf("%d ",qu[i1]);
printf("\n\nGenerare Heap ");
 build_max_heap1(qu,n);
 for(int i2=0; i2<n; i2++)
	 printf("%d ",qu[i2]);

 lung=m;
 HeapSort(qu,n);
 printf("\n\nHeap-Sorted ");
 for(int i3=0; i3<n; i3++)
	 printf("%d ",qu[i3]);
getch();
break;

default :
	break;
  
}
 //getch();
}