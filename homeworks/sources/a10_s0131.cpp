#include<iostream>
#include<conio.h>

using namespace std;

typedef struct Node
{
	int id;
	char *color;
	Node **child;
}Node;

Node *v[200];
int t;

void connect(Node *x, Node*y)
{
	int i=0;
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	child=x->child;
	while(child[i]!=NULL)
		i++;
	child[i]=(Node*)malloc(sizeof(Node));
	child[i]=y;
	i++;
	child[i]=NULL;
	x->child=child;
}


void pretty(Node *n, int i)
{
	int j;
	for (j=0; j<i; j++)
	{
		printf(" ");
	}
	printf("%d", n->id);
	i++;
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	for(int j = 0;child[j]!=NULL;j++)
		pretty(child[j], i);
}

void printList()
{
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	for(int i = 0; i<6; i++)
	{
		printf(" node %d is %s ", i, v[i]->color);
		child=v[i]->child;
		printf("\n");
	}
	printf("\n");
}

void visit(Node *u, int size)
{
	t++;
	u->color="GRAY";
	printList();
	Node **child;
	child=(Node**)malloc(10*sizeof(Node*));
	child=u->child;
	for(int i=0; child[i]!=NULL;i++)
	{
		
		if(child[i]->color=="WHITE")
		{
			visit(child[i],size);
		}
	}
	u->color="BLACK";
	printList();
	t++;
}

void DFS(int n, int size)
{
	for(int i=0; i<n; i++)
	{
		v[i]->color="WHITE";
	}
	printList();
	t=0;
	int i;
	for(i = 0;i<n;i++)
	{
		if(v[i]->color=="WHITE")
		{
			visit(v[i],size);
		}
	}
	
}


void test()
{
	for(int i = 0; i<6;i++)
	{
		v[i]=(Node*)malloc(sizeof(Node));
		v[i]->child=(Node**)malloc(10*sizeof(Node*));
		v[i]->id=i;
		v[i]->child[0]=NULL;
		
	}
	connect(v[0],v[1]);
	connect(v[0],v[3]);
	connect(v[1],v[4]);
	connect(v[2],v[4]);
	connect(v[2],v[5]);
	connect(v[3],v[1]);
	connect(v[4],v[3]);
	connect(v[5],v[5]);

	DFS(6,8);
}

void main()
{
	test();
	getch();
}
