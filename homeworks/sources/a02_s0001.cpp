#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>

/*
***ANONIM*** ***ANONIM***

am 3 metode de sortare: prin selectie, prin insertie si prin sortarea bulelor
pe care le-am implementat conform algoritmilor, si am nr de asignari si comparatii dupa fiecare sortare
apoi am creat 3 fisiere:worstf, averagef, baestf, pentru fiecare caz: favorabil, mediu si defavorabil 
in cazul defavorabil am apelat cele 3 metodele si am afisat in fisier numarul de asignari, iteratii si suma lor
in fisierul worstf am afisat sirul invers si am afisat la fel ca in celalat
in fisierul average am afisat suma comparatiilor si asignarilor pt ficare metoda impartita la 5


*/

void insertionSort(long n, long *a,long *c, long A[])
{
	long i,j,buff;
	for(i=2;i<=n;i++)
	{
		(*a)++;
		buff=A[i];  
		j=i-1;
		while(A[j]>buff && j>0)
		{
			(*a)++;
			(*c)++;
			A[j+1]=A[j];
			j--;
		}
	(*a)++;
	(*c)++;  
	A[j+1]=buff;
	}
}



void selectionSort(long n, long *a,long *c, long A[])
{
	long i,j,pos,aux;
	for(j=1;j<=n-1;j++)
	{
		pos=j;
		for(i=j+1;i<=n;i++)
		{
			(*c)++;
			if(A[i]<A[pos])
				pos=i;
		}
		if(j!=pos)
		{
			(*a)+=3;
			aux=A[j];
			A[j]=A[pos];
			A[pos]=aux;
		}
	}
	
}


void bubbleSort(long n, long *a,long *c, long A[])
{
	int ok;
	long i,aux;
	do
	{
		ok=0;
		for(i=1;i<=n-1;i++)
		{
			(*c)++;
			if(A[i]>A[i+1])
			{
				(*a)+=3;
				aux=A[i];
				A[i]=A[i+1];
				A[i+1]=aux;
				ok=1;
			}
		}
	}while(ok==1);

}



int main()
{	
	
	/*
	long a=0,c=0,x[20],i, n;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d",&x[i]);
	for(i=1;i<=n;i++)
		//insertionSort(n,&a,&c,x);
		//selectionSort(n,&a,&c,x);
		  bubbleSort(n,&a,&c,x);
	for(i=1;i<=6;i++)
		printf(" %ld",x[i]);
	*/

	FILE *worstf,*bestf,*averagef;
	worstf=fopen("worst.txt","w");
	bestf=fopen("best.txt","w");
	averagef=fopen("average.txt","w");

	long as,ai,ab,cs,ci,cb,i,j,k,sumcb,sumci,sumcs,sumab,sumai,sumas;
	long outb[10001],outi[10001],outs[10001];
	long nr;

	//best case
		for(i=100;i<=10000;i=i+100) 
		{
			ai=0;as=0;ab=0;
			ci=0;cs=0;cb=0;
			for(j=1;j<=i;j++)
			{
				outb[j]=j;
			}
			bubbleSort(i,&ab,&cb,outb);
			insertionSort(i,&ai,&ci,outb);
			selectionSort(i,&as,&cs,outb);
			
			fprintf(bestf," %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,ab,cb,cb+ab,ai,ci,ci+ai,as,cs,cs+as);
		}


	//worst case	
	for (i=100;i<=10000;i=i+100)
	{
		cb=0;ci=0;cs=0;
		ab=0;ai=0;as=0;
		nr=1;
		for (j=i;j>=1;j--) 
		{
			outb[nr]=j;
			outi[nr]=j;
			nr++;
		}

		
		for (j=1;j<i;j++) // worst case-selection sort
			outs[j]=j+1;
		outs[i]=1;
		
		/*
		k=1;
		for(int j=i; j>i/2; j--)
		{   
			outs[k]=j;
			k++;
		}
		
		for(int j=1; j<=i/2; j++)
			{outs[k]=j;
			k++;
		}
		*/
		bubbleSort(i,&ab,&cb,outb);
		insertionSort(i,&ai,&ci,outi);
		selectionSort(i,&as,&cs,outs);
		fprintf(worstf," %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,ab,cb,cb+ab,ai,ci,ci+ai,as,cs,cs+as);
	}


	//average case:
	 for (i=100;i<=10000;i=i+100)
	{
		sumcb=0;sumci=0;sumcs=0;
		sumab=0;sumai=0;sumas=0;
		for (j=1;j<=5;j++)  
		{
			nr=0;
			cb=0;ci=0;cs=0;
			ab=0;ai=0;as=0;
			for (k=1;k<=i;k++)
			{
				nr++;
				outb[nr]=outi[nr]=outs[nr]=rand();
			}
			bubbleSort(i,&ab,&cb,outb);
			sumcb+=cb;
			sumab+=ab;
			insertionSort(i,&ai,&ci,outi);
			sumci+=ci;
			sumai+=ai;
			selectionSort(i,&as,&cs,outs);
			sumcs+=cs;
			sumas+=as;
		}
			fprintf(averagef," %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,sumab/5,sumcb/5,sumcb/5+sumab/5,sumai/5,sumci/5,sumci/5+sumai/5,sumas/5,sumcs/5,sumcs/5+sumas/5);
	}

	//getch();
	return 0;
}