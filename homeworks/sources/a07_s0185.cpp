#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<fstream>

using namespace std;


int T[]={0,2,7,5,2,7,7,-1, 5, 2}, a[8][8];

typedef struct arbore
{
	int key;
	int nrCopii;
	struct arbore *copii[];
}nod;

typedef struct multi
{
	int key;
	struct multi *copilSt, *frateDr;
}nod2;

nod *rad=(nod *)malloc(sizeof(nod));
nod2 *rad2=(nod2 *)malloc(sizeof(nod2));

void creare_matrice()
{
	int j;

	for(int i=0; i<10; i++)
	{
		j=0;
		if(T[i]==-1) a[0][0]=i;
		else {	while(a[T[i]][j]!=0) j++;
				a[T[i]][j]=i;
			 }
	}
}

void creare_arbore(nod* p)
{
	int c=p->key;
	int j=0, f;

	while(a[c][j]!=0)
	{
		nod *q=(nod *)malloc(sizeof(nod));
		q->key = a[c][j];
		q->nrCopii = 0;
		f=p->nrCopii;
		p->copii[f] = q;
		p->nrCopii++;
		creare_arbore(q);
		j++;
	}
}

void preordine(nod *p)
{
	if(p!=NULL)
	{
		printf("%d ", p->key);
		for(int i=0; i<p->nrCopii; i++)
			preordine(p->copii[i]);
	}
}

void multicai(nod* p, nod2* q)
{
	int f = p->nrCopii, j=0;
	q->key=p->key;

	if(f!=0)
	{
		nod2 *x=(nod2 *)malloc(sizeof(nod2));
		x->key=p->copii[j]->key;
		j++;
		x->copilSt=NULL;
		x->frateDr=NULL;
		q->copilSt=x;
		f--;

		while(f!=0)
		{
			nod2 *y=(nod2 *)malloc(sizeof(nod2));
			y->key = p->copii[j]->key;
			j++;
			y->copilSt=NULL;
			y->frateDr=NULL;
			x->frateDr=y;
			x=y;
			f--;
		}

		nod2 *z=(nod2 *)malloc(sizeof(nod2));
		z=q->copilSt;
		for(int i=0; i<p->nrCopii; i++)
		{
			multicai(p->copii[i], z);
			z=z->frateDr;
		}
	}
}

int nivel=0;

void pretty_print(nod2* p)
{
	for(int i=0; i<nivel; i++){
		
		cout<<"   ";
	}

	if (p->key != 0 ){
			cout<<p->key;
			cout<<endl;
		}

	if(p->copilSt != NULL){
		nivel++;
		pretty_print(p->copilSt);
		nivel--;
	}

	while(p->frateDr != NULL)
	{
		pretty_print(p->frateDr);
		p=p->frateDr;
	}
}

void main()
{
	creare_matrice();
	cout<<"  ";
	for(int i=0; i<8; i++){

		for(int j=0; j<8; j++){
		cout<<a[i][j]<<" ";
		}

     	cout<<endl;
	}

	cout<<endl;
	rad->key = a[0][0];
	rad->nrCopii = 0;
	creare_arbore(rad);
	preordine(rad);


	rad2->key = rad->key;
	multicai(rad, rad2);
	cout<<endl<<endl;
	pretty_print(rad2);

}
