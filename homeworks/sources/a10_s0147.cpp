#include <conio.h>;
#include <stdlib.h>;
#include <stdio.h>;
#include <malloc.h>

struct nod{
	int key;
	nod *urm;
};
const int marime = 8;
const int marime2 =6;
int graph2[6][6] = {0,1,0,1,0,0,
					1,0,1,0,0,0,
					0,1,0,0,0,0,
					1,0,0,0,1,1,
					0,0,0,1,0,1,
					0,0,0,1,1,0};
					
int graph[marime][marime]={0,1,0,0,0,0,1,1,
						   1,0,1,0,0,0,1,0,
						   0,1,0,0,1,0,0,0,
						   0,0,0,0,1,1,0,0,
						   0,0,1,1,0,1,0,1,
						   0,0,0,1,1,0,1,0,
                           1,1,0,0,0,1,0,0,
                           1,0,0,0,1,0,0,0};
int stiva[10];
int stivaIndex =-1;
int amRamasLa;
int auxRamas = 0;
int vizitate[marime];
nod *cap = NULL, *curent = NULL;
bool ready = false;
bool first = true;

void editList(int id){
	if(cap == NULL){
		cap = (nod*)malloc(sizeof(nod));
		cap->key = id;
		cap->urm = NULL;
		curent = cap;
		return;
	}
	curent->urm =(nod*)malloc(sizeof(nod));
	curent->urm->key = id;
	curent->urm->urm= NULL;
	curent = curent->urm;
}
void vizitare_noduri(int s){
	bool stivaContine = false;
	for(int i =0; i<marime; i++){
		bool stivaContine = false;
		if(graph[s][i]==1){
			for(int j = 0; j<=stivaIndex; j++){
				if(stiva[j]==i){
					stivaContine = true;
				}
			}
			if(!stivaContine){
				stivaIndex ++;
				stiva[stivaIndex] = i;
			}
		}
	}
}

void parcurgere_stiva(int first);
void verificare(){
	for(int i= 0; i< marime; i++){
		if(vizitate[i] == 1){
			stivaIndex++;
			stiva[stivaIndex]=i;
			parcurgere_stiva(i);
		}
	}
	ready = true;
}
void parcurgere_stiva(int n){
	//amRamasLa=auxRamas;
	if(first){
		stivaIndex++;
		stiva[stivaIndex] = n;
		vizitate[n] = 0;
		editList(n);
		vizitare_noduri(n);
		first = false;
	} 
	while(amRamasLa!=stivaIndex){
		vizitare_noduri(stiva[amRamasLa]);
		vizitate[stiva[amRamasLa]] = 0;
		editList(stiva[amRamasLa]);
		amRamasLa++;
		//auxRamas  = amRamasLa;
	}
	if(!ready){
		verificare();
	}
}
int stiva2[marime2+1];
int parcurse[marime2];
bool nuEsteParcurs(int n){
	if(parcurse[n]==1){

		return true;
	}
	return false;
}


void DFS(int rad, int tab){
	for(int j=0; j<tab; j++){
		printf("\t");
	}
	printf("%d\n",rad);

	parcurse[rad]=0;
	int i = 0;
	while(i<marime2){
		if(graph2[rad][i]==1){
			if(nuEsteParcurs(i)){
				DFS(i,tab+1);

			}
		}
		
		i++;
	}
}

void main(){
	for(int i = 0; i< marime; i++){
		vizitate[i] = 1;
		parcurse[i] = 1;
	}
	printf("BFS:\n");
	parcurgere_stiva(2);
	nod* aux = cap->urm;
	while(aux!=NULL){
		printf("%d->",aux->key);
		aux = aux->urm;
	}
	printf("NIL");
	printf("\n\nDFS:\n");
	DFS(0,0);
	getch();
}