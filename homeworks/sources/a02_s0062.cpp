/*
* Constructia Heap-ului Buttom-Up este mai eficient decat Top-Down.
* Student: ***ANONIM*** ***ANONIM*** ***ANONIM***
* Grupa: ***GROUP_NUMBER***
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

//variabile globale
int n, dim;
int buA=0,buC=0,tdA=0,tdC=0;


/////////// Crearea heap-ului Bottom-Up //////////////

//returneaza indicele elementului din stanga
int stanga(int j)
{
  return (2 * j) ;
}

//returneaza indicele elementului din dreapta
int dreapta(int j)
{
  return (2 * j) + 1;
}

void heap_BU(int a[], int i)
{
  int l = stanga(i);
  int r = dreapta(i);
  int t;
  buC++;

  if ((l <= n) && (a[l] > a[i]))
   {
     t = l;
   }
  else
   {
     t = i;
   }
  buC++;

  if ((r <= n) && (a[r] > a[t]))
  {
    t = r;
  }

  if (t!= i)
  {
    buA=buA+3;
    int temp = a[i];
    a[i] = a[t];
    a[t] = temp;

    heap_BU(a,t);
  }
}

void Build_H_BU(int x[])
{
	buC=0;
	buA=0;
	int i;
	for (i=n/2; i>0;i--)
	{
		heap_BU(x, i);
	}
}
//////////////////////////////////////////////////////


///////// Construirea Heap-ului Top-Down ////////////
int parent(int i)
{
   return (i / 2);
}

void H_Push(int a[],int k)
{
	dim++;
	tdA++;
	tdC++;
	a[dim]=k;
	int i=dim;

	while(i>1 && a[parent(i)]>a[i])
	{	tdC++;
		tdA=tdA+3;
		int aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}
}


void Build_H_TD(int x[] ,int y[])
{
    tdC=0;
	tdA=0;
	dim=0;
	int i;
	for( i=1;i<=n;i++)
	{
		H_Push(y,x[i]);
	}
}
//////////////////////////////////////////////////////

void main()
{   int x[10001], h[10001], y[10001];
    FILE *f; f=fopen("tabel.csv","w");
	fprintf(f,"n,BU_at,BU_comp,BU_at+comp,TD_at,TD_comp,TD_at+comp \n");

	int aux1[2],aux2[2]; // pentru comparatii si atribuiri
	for(n=100;n<=10000;n=n+100)
	{
		aux1[0]=0;
		aux1[1]=0;
	    aux2[0]=0;
		aux2[1]=0;
		for(int m=1;m<=5;m++)
		 {
			srand (time(NULL));

			for(int i=1;i<=n;i++)
			{
				x[i]=rand()%1000;
				y[i]=x[i];
			}

			//apelare B-U
			Build_H_BU(y);
			//apelare T-D
			Build_H_TD(x,y);

			aux1[0]=aux1[0]+ buA;
			aux2[0]=aux2[0]+ buC;
			aux1[1]=aux1[1]+ tdA;
			aux2[1]=aux2[1]+ tdC;		
	}
     fprintf(f, "%d,%d,%d,%d,%d,%d,%d \n", n, aux1[0]/5, aux2[0]/5, (aux1[0]+aux2[0])/5, aux1[1]/5,aux2[1]/5, (aux1[1]+aux2[1])/5);
   }
   fclose (f);
}
