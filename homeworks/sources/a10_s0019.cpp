#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

/**
La explorarea in latime, dupa vizitarea varfului initial, se exploreaza toate varfurile adiacente
lui, se trece apoi la primul varf adiacent si se exploreaza toate varfurile adiacente acestuia 
si neparcurse inca, s.a.m.d. Codificare e la fel ca in cazul parcurgerii BFS. Ce se schimba este 
faptul ca lista folosita pentru noduri de dupa structura unei stive (nu ca o coada ca in cazul BFS).
Fiecare varf se parcurge cel mult odata. 
Sortarea topologica tine minte intr-un sir toate varfurile care au fost parcurse. In cazul in care
graful contine cicluri (muchii inapoi) aceasta sortare nu se poate face.
*/

#define ALB 0
#define GRI 1
#define NEGRU 2

typedef struct vrf
{
	int color, dist, parent, f;
	int	gi;
} vrf;


typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

NODE *head[60001], *tail[60001];
vrf VARF[60001];
int V[100001];
int timp;
int k, op;
int QUE[10001], nrq;
bool sort=true;

void DFS_visit (int vertex)
{
	VARF[vertex].color=GRI;
	VARF[vertex].dist=timp;
	timp++;
	Node *temp;
	temp=head[vertex];
	op+=4;
	while (temp!=NULL)
	{
			int p=temp->val;
			temp=temp->next;
			op+=3;
			if(VARF[p].color==ALB)
				{	
					op++;
					VARF[p].parent=vertex; 
					printf("(%d,%d) e muchie de arbore\n", vertex, p);
					DFS_visit(p);
				}
			else 
			{
				//op++;
				if (VARF[p].color==GRI)
				{
					sort=false;
					printf("(%d,%d) e muchie inapoi\n", vertex, p);
				}
				else 
				{
					//op++;
					if (VARF[p].f<VARF[vertex].dist) 
							printf("(%d,%d) e muchie traversal\n", vertex, p);
					else printf("(%d,%d) e muchie inainte\n", vertex, p);
				}
			}
	}
	VARF[vertex].color=NEGRU;
	VARF[vertex].f=timp;
	timp++;
	op+=3;
	//calcule pt sortare topologica
	QUE[nrq]=vertex;
	nrq--;
	
}

void DFS (int nrv)
{
	int i;
	for (i=1; i<=nrv; i++)
	{
		op+=2;
		VARF[i].color=ALB; 
		VARF[i].parent=0;
	}
	timp=0;
	for (i=1; i<=nrv; i++)
	{
		op++;
		if (VARF[i].color==ALB)
			DFS_visit(i);
	}
}

void topologic_sort (int nrv)
{
	if (sort==true)
		for (int i=1;i<=nrv; i++)
			printf("%d ", QUE[i]);
	else printf("graful contine cicluri\n");
}


/*void topologic_sort (int nrv)
{
	int v, u;
	int Q[60001];

	for(u=1; u<=nrv; u++)
		VARF[u].gi=0;
	for(u=1; u<=nrv; u++)
	{
		Node *temp;
		temp=head[u];
		while (temp!=NULL)
		{
			v=temp->val;
			temp=temp->next;
			VARF[v].gi++;
			printf("VARF[%d].gi =  %d\n", v, VARF[v].gi);
		}
	}
	k=-1;
	for (u=1; u<=nrv; u++)
		if (VARF[u].gi==0)
		{
			k++;
			Q[k]=u;
			//k++;
			printf("k = %d\n", k);
		}
	while (k!=-1)
	{
		u=Q[k];
		k--;
		printf ("%d ", u );
		if(head[u]!=NULL)
		{
			Node *temp;
			temp=head[u];
			while (temp!=NULL)
			{
				v=temp->val;
				temp=temp->next;
				VARF[v].gi--;
				printf("VARF[%d].gi =  %d\n", v, VARF[v].gi);
				if (VARF[v].gi==0)
				{
					Q[k]=v;
					k++;
				}
			}
		}
	}
	for (int i=1;i<=k; i++)
		printf("%d ", Q[i]);
}*/

bool verif (int x, int y)
{
	NODE *p;
	p=head[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->next;
	}
	return true;

}

void genereaza(int nrv,int nre)  //nrv= numarul de varfuri, nre = numarul de edges
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		head[i]=tail[i]=NULL;
	i=1;
	while (i<=nre)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
				if(head[u]==NULL)
				{
					head[u]=(NODE *)malloc(sizeof(NODE *));
					head[u]->val=v;
					head[u]->next=NULL;
					tail[u]=head[u];
					i++;
				}
				else {
						if (verif (u, v))
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;
							p->next=NULL;
							tail[u]->next=p;
							tail[u]=p;
							i++;
						}
					}
		}
	}
}

void print(int nrv)
{
	int i;
	NODE *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=head[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->next;
		}
		printf("\n");
	}
}

int main()
{
	int nrv, nre;
	FILE *f;
	srand(time(NULL));
	//test
	nrv=5;
	nre=5;
	nrq=nrv;
	srand(time(NULL));
    genereaza(nrv, nre);
	print(nrv);
	DFS(nrv);
	topologic_sort(nrv); 

	/*
	//program
	f=fopen ("rez1.txt", "w");
	fprintf(f, "nre op\n");
	nrv=100;
	for (nre = 1000; nre<=5000; nre=nre+100)
	{
		op=0;
		genereaza(nrv, nre);
		DFS(nrv);
		fprintf(f, "%d %d\n", nre, op);
	}
	fclose(f);
	*/

	f=fopen ("rez2.txt", "w");
	fprintf(f, "nrv op\n");
	nre=9000;
	for (nrv = 110; nrv<=200; nrv=nrv+10)
	{
		op=0;
		genereaza(nrv, nre);
		DFS(nrv);
		fprintf(f, "%d %d\n", nrv, op);
	}
	fclose(f);
	
	
	printf("\nAm Terminat%c\n",7);
	getch(); 
    return 0;

}
