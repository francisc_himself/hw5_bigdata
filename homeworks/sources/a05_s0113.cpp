/*
	Name: ***ANONIM*** ***ANONIM***�n
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently the insert and search operations in a 
					hash table using open addressing and quadratic probing.

	Analysis: - the results obtained show a demonstration for the theoretical limits of the search operation
			  - for the average number of successful searches, the result remains always under the theoretical 
				value of 1/a*ln(1/(1-a)), where a is the loading factor
			  - for the average number of unsuccessful searches, the result goes a bit over the theoretical 
				value of 1/(1-a) due to secondary clustering
*/
#include<stdio.h>
#include "profiler.h"
#define N 10007

int c1=7919, c2=9991;
int hashTable[N];
int values[N];
float alfa[6]={0,0.8, 0.85,0.9,0.95,0.99};
int count, MAX;
int foundCount, foundMax, notFoundCount, notFoundMax;
int nr;

int hashValueLinear(int x,int i)
{
	return (x+i)%N;
}

int hashValueQuadratic(int x,int i)
{
	return (hashValueLinear(x,i)+i*c1%N+i*i*c2%N)%N;
}

int hash_insert(int x)
{
	int i=0;
	do
	{
		int code;
		code=hashValueQuadratic(x,i);
		if(hashTable[code]==0)
		{
			hashTable[code]=x;
			return code;
		}
		else i++;
	}while(i!=N);
	return -1;
}

int hash_search(int x)
{
	int i=0, code;
	count++;
	do
	{
	   code=hashValueQuadratic(x,i);
		if(hashTable[code]==x)
			return code;
		else
		{
			i++;
			if(i>MAX)
				MAX=i;
			count++;
		}
	}while(hashTable[code]!=0&&i!=N);
	count--;
	return -1;
}

void reset()
{
	for(int i=0;i<N;i++)
		hashTable[i]=0;
}

int maximum(int a, int b)
{
	if(a>b)
		return a;
	else return b;
}

void calculate()
{
	srand(time(NULL));
	for(int i=1;i<=5;i++)
	{
		foundCount=0;
		notFoundCount=0;
		foundMax=0;
		notFoundMax=0;
		nr=alfa[i]*N;
		for(int k=1;k<=5;k++)
		{
			FillRandomArray(values, N, 1, 1000000000,true,0);
			count=0;
			int j;
			for(j=0;j<nr;j++)
				hash_insert(values[j]);
			count=0;
			MAX=0;
			for(j=1;j<=1500;j++)
			{
				MAX=0;
				hash_search(values[rand()%nr]);
				foundMax=maximum(foundMax, MAX);
			}
			foundCount+=count;
			count=0;
			MAX=0;
			for(j=1;j<=1500;j++)
			{
				MAX=0;
				hash_search(rand()%1000000000+1000000000);
				notFoundMax=maximum(notFoundMax, MAX);
			}
			notFoundCount+=count;
			reset();
		}
		foundCount/=5;
		notFoundCount/=5;
		printf("%f,%f,%d,%f,%d\n", alfa[i], (float)foundCount/1500, foundMax, (float)notFoundCount/1500, notFoundMax);
	}

}

void test()
{
	for(int i=1;i<=5;i++)
		hash_insert(i);
	for(int i=1;i<=10;i++)
		printf("%d\n",hash_search(i));
	reset();
}

int main()
{
	test();
	freopen("quadratic.csv", "w", stdout);
	printf("Filling factor,Avg. Effort found,Max. Effort found, Avg. Effort not found,Max. Effort not found\n");
	calculate();
	return 0;
}