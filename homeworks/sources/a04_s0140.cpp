#include<stdlib.h>
#include<stdio.h>
#include <conio.h>
typedef struct arb_bin {
int valoare;
struct arb_bin * right, * left;
};
typedef struct arb_bin node;

void insert(node ** tree, int val)
{
    node *temp = NULL;
    if(!(*tree))
    {
        temp = (node *)malloc(sizeof(node));
        temp->left = temp->right = NULL;
        temp->valoare = val;
        *tree = temp;
        return;
    }

    if(val < (*tree)->valoare)
    {
        insert(&(*tree)->left, val);
    }
    else if(val > (*tree)->valoare)
    {
        insert(&(*tree)->right, val);
    }

}

void preordine(node * tree,int nivel)
{
    if (tree!=NULL)
    {
        printf(" %d\n",tree->valoare);
       preordine(tree->left,nivel++);
        preordine(tree->right,nivel++);
    }

}

void inordine(node * tree,int nivel)
{
    if (tree)
    {
        inordine(tree->left,nivel++);
        printf(" %d\n",tree->valoare);
        inordine(tree->right,nivel++);
    }
}

void postordine(node * tree,int nivel)
{
    if (tree!=NULL)
    {
        postordine(tree->left,nivel++);
        postordine(tree->right,nivel++);
        printf(" %d\n",tree->valoare);
    }
}

void deltree(node * tree)
{
    if (tree!=NULL)
    {
        deltree(tree->left);
        deltree(tree->right);
        free(tree);
    }
}

node* search(node ** tree, int val)
{
    if(!(*tree))
    {
        return NULL;
    }

    if(val < (*tree)->valoare)
    {
        search(&((*tree)->left), val);
    }
    else if(val > (*tree)->valoare)
    {
        search(&((*tree)->right), val);
    }
    else if(val == (*tree)->valoare)
    {
        return *tree;
    }
}

void main()
{
    node *rad;
    node *tmp;
    int i,n;
	int a[100];
    rad = NULL;
	printf("introduceti nr de noduri");
	scanf("%d",&n);
    /* Inserare noduri in arbore */
	for(i=0;i<n;i++){
	printf("a[%d]=\n",i);
	scanf("%d",&a[i]);
    insert(&rad, a[i]);
	}
   

    /* Printare */
    printf("Preordine \n");
    preordine(rad,0);

    printf("Inordine \n");
    inordine(rad,0);

    printf("Postordine \n");
    postordine(rad,0);

    /* Cautare nod in arbore */
    tmp = search(&rad, 3);
    if (tmp)
    {
        printf("Sa gasit nodul =%d\n", tmp->valoare);
    }
    else
    {
        printf("Nodul cautat nu se afla in arbore\n");
    }

    /* Stergere arborelui */
    deltree(rad);
	getch();
}