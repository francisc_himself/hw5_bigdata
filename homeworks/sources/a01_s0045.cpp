/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Pe baza numarului de comparatii metoda de sortare cu insertie este mult mai eficienta decat cealalta doua metoda
iar pe baza numarului de asignari selectia este cea mai buna optiune(de 40 de ori mai bun decat insertia).
Dupa suma asignarilor si comparatilor selectia si insertia sunt de eficienta egala, iar bubble sortul este cel mai 
ineficient dintre toti trei.
*/


#include <stdio.h>
#include <conio.h>
#include "Profiler.h"

#define MAX_SIZE 5000

Profiler profiler("demo");

int a1[MAX_SIZE];
int a2[MAX_SIZE];
int a3[MAX_SIZE];
int n;

void bubble(int *v, int n)
{
	for (int i=1; i<n; i++)
	{
		for (int j=0; j<n-i; j++)
		{
			if (v[j] > v[j+1])
			{

				int s=v[j];
				v[j]=v[j+1];
				v[j+1]=s;

				profiler.countOperation("bubble-asignare", n, 3);
			}
			profiler.countOperation("bubble-comparatie", n);
		}
	}
}

void afis(int *v,int n)
{
	for (int i=0; i<n; i++)
	{
		printf("%d ",v[i]);
	}

	printf("\n");
}

void selectie(int *v, int n)
{
	int imin;
	for (int i=0; i<n-1; i++)
	{
		imin=i;
		for (int j=i+1; j<n; j++)
		{
			if (v[j]<v[imin])
			{
				imin=j;
			}
			profiler.countOperation("selectie-comparatie", n);
		}
		int s=v[imin];
		v[imin]=v[i];
		v[i]=s;
		profiler.countOperation("selectie-asignare", n, 3);
	}
}

void insertie(int *v, int n)
{
	for (int i=1; i<n; i++)
	{
		int x=v[i];
		profiler.countOperation("insertie-asignare", n);
		int j=0;

		while (v[j] < x)
		{
			profiler.countOperation("insertie-comparatie", n);
			j++;
		}
		profiler.countOperation("insertie-comparatie", n);

		for (int k=i; k>j; k--)
		{
			v[k]=v[k-1];
			profiler.countOperation("insertie-asignare", n);
		}

		v[j]=x;
		profiler.countOperation("insertie-asignare", n);
	}
}


void check()
{
	int test[]={4,9,1,8,3};
	int test2[5];
	int test3[5];
	int test4[5];

	memcpy(test2,test,5*sizeof(int));
	bubble(test2, 5);
	printf("bubble: ->isSorted= ");
	if (IsSorted(test2,5))
	{
		printf("true\t");
	}
	else
	{
		printf("false\t");
	}
	afis(test2, 5);

	memcpy(test3,test,5*sizeof(int));
	selectie(test3, 5);
	printf("selectie:  ->isSorted= ");
	if (IsSorted(test3,5))
	{
		printf("true\t");
	}
	else
	{
		printf("false\t");
	}
	afis(test3, 5);

	memcpy(test4,test,5*sizeof(int));
	insertie(test4, 5);
	printf("insertie:  ->isSorted= ");
	if (IsSorted(test4,5))
	{
		printf("true\t");
	}
	else
	{
		printf("false\t");
	}
	afis(test4, 5);
}

void main()
{
	check();
	int limit=2000;

	//average case
	for (int n=100; n<=limit; n+=100)
	{
		for (int k=0; k<5; k++)
		{
			FillRandomArray(a1, MAX_SIZE);
			memcpy(a2,a1,n*sizeof(int));
			memcpy(a3,a1,n*sizeof(int));

			bubble(a1, n);
			selectie(a2,n);
			insertie(a3,n);
		}
	}

	profiler.createGroup("AVERAGE-comparatie", "bubble-comparatie", "selectie-comparatie", "insertie-comparatie");
	profiler.createGroup("AVERAGE-asignare", "bubble-asignare", "selectie-asignare", "insertie-asignare");
	profiler.addSeries("AVERAGE-bubble-sum", "bubble-comparatie", "bubble-asignare");
	profiler.addSeries("AVERAGE-insertie-sum", "insertie-comparatie", "insertie-asignare");
	profiler.addSeries("AVERAGE-selectie-sum", "selectie-comparatie", "selectie-asignare");
	profiler.createGroup("AVERAGE-sum", "AVERAGE-bubble-sum", "AVERAGE-selectie-sum", "AVERAGE-insertie-sum");
	

	//initializare profiler?

	//best case
	for (int n=100; n<=limit; n+=100)
	{
			for (int i=0; i<n; i++) //deja sortat
			{
				a1[i]=i;
			}
			bubble(a1, n);

			/*
			la selectie si la insertie nu avem cazuri favorabili si defavorabili
			1.la selectie  oricum trebuie sa comparam toate elementele la gasirea minimului 
				si oricum efectuam interschimbarea
			2. la insertie numarul de comparatii si numarul de asignari sunt invers proportionali
				deci suma este constanta
			*/
			FillRandomArray(a2, MAX_SIZE);
			selectie(a2,n);
			FillRandomArray(a3, MAX_SIZE);
			insertie(a3,n);
	}
	profiler.createGroup("BEST-comparatie", "bubble-comparatie", "selectie-comparatie", "insertie-comparatie");
	profiler.createGroup("BEST-asignare", "bubble-asignare", "selectie-asignare", "insertie-asignare");
	profiler.addSeries("BEST-bubble-sum", "bubble-comparatie", "bubble-asignare");
	profiler.addSeries("BEST-insertie-sum", "insertie-comparatie", "insertie-asignare");
	profiler.addSeries("BEST-selectie-sum", "selectie-comparatie", "selectie-asignare");
	profiler.createGroup("BEST-sum", "BEST-bubble-sum", "BEST-selectie-sum", "BEST-insertie-sum");

	//initializare profiler?

	//worst case
	for (int n=100; n<=limit; n+=100)
	{
			for (int i=n-1; i>-1; i--) //sortat descrescator
			{
				a1[i]=i;
			}
			bubble(a1, n);

			/*
			la selectie si la insertie nu avem cazuri favorabili si defavorabili
			1.la selectie  oricum trebuie sa comparam toate elementele la gasirea minimului 
				si oricum efectuam interschimbarea
			2. la insertie numarul de comparatii si numarul de asignari sunt invers proportionali
				deci suma este constanta
			*/
			FillRandomArray(a2, MAX_SIZE);
			selectie(a2,n);
			FillRandomArray(a3, MAX_SIZE);
			insertie(a3,n);
	}

	profiler.createGroup("WORST-comparatie", "bubble-comparatie", "selectie-comparatie", "insertie-comparatie");
	profiler.createGroup("WORST-asignare", "bubble-asignare", "selectie-asignare", "insertie-asignare");
	profiler.addSeries("WORST-bubble-sum", "bubble-comparatie", "bubble-asignare");
	profiler.addSeries("WORST-insertie-sum", "insertie-comparatie", "insertie-asignare");
	profiler.addSeries("WORST-selectie-sum", "selectie-comparatie", "selectie-asignare");
	profiler.createGroup("WORST-sum", "WORST-bubble-sum", "WORST-selectie-sum", "WORST-insertie-sum");
	
	profiler.showReport();
}