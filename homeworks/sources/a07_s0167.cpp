/*
***ANONIM*** ***ANONIM*** Daniela
In acest program am realizat 2 metode de reprezrntare a arborilor: reprezentarea multi-cai si reprezentarea  binara.
Cele doua transformari au eficienta O(n).
*/
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>


typedef struct MULTI_NODE
{
	int key;
	int count; 
	struct MULTI_NODE *child[50];
}MULTI_NODE;


typedef struct BIN_NODE{
	int key;
	struct BIN_NODE *stanga,*dreapta;
}BIN_NODE;


MULTI_NODE *nodes[50];


MULTI_NODE* createMN(int cheie)
{
MULTI_NODE* p; 
p=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
p->key=cheie;
p->count=0;
return p;
} 

void insert_MN(MULTI_NODE *parent,MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
}

MULTI_NODE* transf1(int t[],int size)
{
	MULTI_NODE *root=NULL;
	
for (int i=0;i<size;i++)
{
	nodes[i]=createMN(i);
}
for(int i=0;i<size;i++)
{
	if(t[i]==-1) root=nodes[i];
	else
	{
		insert_MN(nodes[t[i]],nodes[i]);
	}
}
return root;
}
void pretty_printMN(MULTI_NODE *nod,int nivel=0)
{
	for (int i=0;i<nivel;i++)
		printf("    ");
	printf("%d\n",nod->key);
	for(int i=0;i<nod->count;i++)
	{
		pretty_printMN(nod->child[i],nivel+1);
	}
}

BIN_NODE* createNB(int cheie)
{
BIN_NODE* p; 
p=(BIN_NODE*)malloc(sizeof(MULTI_NODE));
p->stanga=NULL;
p->stanga=NULL;
p->key=cheie;
return p;
} 

BIN_NODE *transf2(BIN_NODE *r2,MULTI_NODE *r1)
{ 
	BIN_NODE *l,*r,*aux;
   if(r1!=NULL)
   {
	   if (r1->count==1){
		   l=createNB(r1->child[0]->key);
		   r2->stanga=transf2(l,r1->child[0]);}
	   else if(r1->count!=0)
	   {
		   l=createNB(r1->child[0]->key);
		   r2->stanga=transf2(l,r1->child[0]);
		   aux=r2;
		   r2=r2->stanga;
		   
		   for(int i=1;i<r1->count;i++){
			   l=createNB(r1->child[i]->key);
			   r2->dreapta=transf2(r,r1->child[i]);
			   r2=r2->dreapta;
			 

		   }
		   r2=aux;
	   }
	   return r2;
   }
   return NULL;
}
void pretty_printNB(BIN_NODE *nod,int nivel=0)
{
	if(nod!=0){
	for (int i=0;i<nivel;i++)
		printf("    ");
	printf("%d\n",nod->key);
	pretty_printNB(nod->stanga,nivel+1);
	pretty_printNB(nod->dreapta,nivel);
     }
}


int main()
{
	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(t[0]);
   
    MULTI_NODE *rad1=transf1(t,size);
    pretty_printMN(rad1,0);
	printf("\n");
	BIN_NODE *rad2=createNB(rad1->key);
	rad2=transf2(rad2,rad1);
	 pretty_printNB(rad2,0);

	return 0; 
}