/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
  FA - Assignment  - Assignment No. 5: Search Operation in Hash Tables. Open Addressing with Quadratic Probing
*/



/**Depending on the filling factor the effort fort finding an element (key) in the hash tables differs. 
The search function works like this: it stops when it finds the key or when it reaches a null key. 
A hash table with a filling factor smaller has more null elements so the search function stops faster. 
The maximum value for the "not found effort " is bigger than 700, in the case of the filling factor equal with 0.99.*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#define c1 1
#define c2 2

typedef struct type_node {
                int key;
                struct type_node *urm;
} TYPE_NODE;

int HT[10001]; //the hash table
int found[1501];
int notfound[1501];
int nr_cautari=1500;     //1500;
int N=10007;       //number of elements = 10007
float alfa[]={0.8, 0.85, 0.9, 0.96, 0.99};   //filling factor
int effort;//
int ef,enf,efm,enfm;//ef=effort found; enf=effort notfound; m=maxim;
int max;

int h(int k, int i)
{
    int temp = (k % N + c1*i + c2*i*i) % N;
    return temp;
}

void init(int k)
{
    int i;
    for(i=0;i<k;i++)
        HT[i]=NULL;
}

int insert(int k)
{
    int j, i=0;
    do
    {
        j=h(k,i);
        if (HT[j]==NULL)
        {
            HT[j]=k;
            return j;
        }
        else
            i++;
    }while (i!=N);
    return 0;//***ANONIM***'t insert anymore
}

//searching for an element in the hash and modifying the variable effort 
int search(int k)
{
    int i=0;
	effort=0;
    int j;
    do
    {
        j=h(k,i);
		effort++;
        if(HT[j]==k)
            return j;
        i++;
    }
	while((HT[j]!=NULL) && (i!=N));
    return 0;
}

void print (int HT[])
{
	int i;
	for(i=0; i<N; i++)
	{
		printf("\n%d:",i);
		if(HT[i]!=0) 
			printf(" %d",HT[i]);
	}
}

int gen(int n)
{
	int x=rand() * rand() % 100000;
	//while((x % 2 !=0) || (x==0))
	if(max<x)
		 max=x;
		// x=rand() * rand() % 100000;
		//x=rand()%n;
    return x;
}

void genfound(int n)
{
	int i;
	for(i=0; i<nr_cautari; i++)
	{
		int k=rand()%N;
		found[i]=HT[k];
	}

}

void gennotfound(int n)
{
	int i, x;
	x=rand() * rand() % 100000;

	for(i=0; i<nr_cautari; i++)
	{
		//while(x % 2==0)
		//while(x<=max)
		      x=rand() * rand() % 100000 + max;
		notfound[i]=x;
	}
}

void main()
{

    int n; //numarul de inserat
    int nr=0;//numarul de inserari reusite
    int i;					// number of diffrent filling factors
	int j;					// number of searched elements
	int k;
    float a;				//current filling factor
	int efaux=0, enfaux=0;	// auxiliars for avg
	int eftot=0, enftot=0;

	FILE *f;
	f=fopen("hashtable.txt","w");
	fprintf(f,"effort avg_effort_found max_effort_found avg_effort_notfound max_effort_notfound\n");
	printf("FF\t \tAEF \tMAXEF \tAENF \tMAXENF\n");


	srand(time(NULL));
    for(i=0; i<5; i++)        ///////////////////for every filling factor
    {
		int o=0;
        init(N);
	//	max=0; //!!!!!!
        do
        {
            n=gen(1000);
            if(insert(n))
                nr++;
            a=(float)nr/N;
        }
		while(a<alfa[i]);// the generation of the array depending on  the filling factor


		for(k=0; k<5; k++)// 5 times for avg
		{
			genfound(1000);
			gennotfound(1000);
			for(j=0; j<nr_cautari; j++)// verificam gasirile
			{
				search(found[j]);
				ef=effort;
				efaux+=ef;
				if(ef>efm) 
					 efm=ef; 
				effort=0;
				search(notfound[j]);
				enf=effort;
				enfaux+=enf;
				if(enf>enfm) enfm=enf; 
				effort=0;
			}
			eftot+=efaux/nr_cautari;
			enftot+=enfaux/nr_cautari;
			efaux=enfaux=0;
		}
		//printf("\nbm e %d cm e %d\n",bm, cm);
		fprintf(f,"%f %d %d %d %d\n", alfa[i],eftot/5,efm,enftot/5,enfm);
		printf("%f \t%d \t%d \t%d \t%d\n", alfa[i],eftot/5,efm,enftot/5,enfm);
        nr=efm=enfm=eftot=enftot=ef=enf=0;
    }


	//Test for smaller values
	N=11;
	nr_cautari=5;
	nr=0;
	init(N);
	int o=0;
    do
    {
        n=gen(10);
        if(insert(n))
            nr++;
        a=(float)nr/N;
    }
	while(a< 0.8 );
	print(HT);
	printf("\nWe have %d numbers",nr);
	

	genfound(10);
	gennotfound(10);

	for(j=0; j<nr_cautari; j++)// the found elements
	{
		search(found[j]);
		printf("\nsearch for %d from found with the effort: %d",found[j],effort);
		ef=effort;
		efaux+=ef;
		if(ef>efm) efm=ef; //saving for maxim
		effort=0;
		search(notfound[j]);
		printf("\nsearch for %d from notfound with the effort: %d",notfound[j],effort);
		enf=effort;
		enfaux+=enf;
		if(enf>enfm) enfm=enf; //saving for maxim
		effort=0;
	}
	printf("\nfactorul: %f, avgfound: %d, maxfound: %d,  avgnotfound: %d maxnotfound: %d\n", a,efaux/nr_cautari,
		efm,enfaux/nr_cautari,enfm);
	
	

    getch();
}
