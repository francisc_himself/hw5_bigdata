/*
***ANONIM*** ***ANONIM***-Mihai, grupa ***GROUP_NUMBER***
Lucrare 5 Algoritmi fundamentali
In aceasta lucrare de laborator am studiat tabelele de dispersie, cu operatiile de inserare si cautare.
Complexitatea algoritmului de cautare este O(N) pentru elementele negasite, si O(1) pentru cele gasite.
*/

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 9973
int t[N];
int n;
int test[3000];
long gasite,negasite,maxGasite=0,maxNegasite=0;
//Initializare t
void init()
{
	for(int i=N-1;i>=0;i--)
		t[i]=0;
}
//Calculul codului de dispersie
int cod_dispersie(int k,int i)
{
	int x;
	x=(k%N+(1*i)+(2*i*i))%N;
	return x;
}
//Inserare in tabela
void insert(int t[N],int k)
{
	int i=0;
	int j;
	do 
	{
		j=cod_dispersie(k,i);
		if (t[j]==0)
		{
			t[j]=k;
			return;
		}
		else
			i=i+1;
	}
	while(i<N);
	return;

}
//Cautare element in tabela
int search(int t[N],int k)
{
 int i=0;
 int j;
 int nr=0;
	 do {
		j=cod_dispersie(k, i);
		nr++;
		if (t[j]==k) 
		{ 
			gasite+=nr;
			if (maxGasite<nr) 
			{
				maxGasite=nr;
			}
			return t[j]; 
		}
		else
			i++;
		
	 } while (t[j]!=0 && i<N);
		
	 negasite+=nr;
		if (maxNegasite<nr) 
		{
			maxNegasite=nr;
		}
	 return 0;
}

void creareTest()
{
	for(int i=0;i<1500;i++)
		test[i]=t[i];
	for(int i=1500;i<3000;i++)
		test[i]=t[i-1500]+1;
}

void main()
{
	FILE *f;
	srand(time_t(NULL));
	f=fopen("fisier.csv","w");
	fprintf(f,"Fill factor,AvgFound,MaxFound,AvgNotFound,MaxNotFound");
	double alfa[]={0.8,0.85,0.9,0.95,0.99};
	int k;
	for(int i=0;i<5;i++)
	{
		gasite=0;
		negasite=0;
		maxGasite=0;
		maxNegasite=0;
		
		n=(int)(alfa[i]*N);
		for(int b=0;b<5;b++)
		{
			init();
			for(int j=0;j<n;j++)
			{
				k=rand()+1;
				insert(t,k);
			}
			creareTest();
			for(int l=0;l<3000;l++)
				search(t,test[l]);
		}

		fprintf(f,"\n%2f,%10f,%10d,%10f,%10d",alfa[i],(float)gasite/7500,maxGasite,(float)negasite/7500,maxNegasite);
		
	}
	fclose(f);
	
	
	getch();
}