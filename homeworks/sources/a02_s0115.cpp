#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"


#define inf 999999
#define left(node) (2*node)
#define right(node) (2*node+1)
#define parent(node) (node/2)

#define SWAP(a,b) int aux;aux=a;a=b;b=aux;

Profiler profiler("BuildingHeap");

int a[10000];
int heap[10000+1];
//int size;
int size = 10; 
int heapSize = 0;

void Heapify(int node)
{
	int l = 0;
	int left = left(node);
	int right = right(node);
	profiler.countOperation("bottomUpOverall",size,1);
	if((left <= size) && (heap[left] > heap[node]))
	{
		l = left;
	}
	else
	{
		l = node;
	}
	profiler.countOperation("bottomUpOverall",size,1);
	if((right <= size) && (heap[right] > heap[l]))
	{
		l = right;
	}
	if (l != node)
	{
		profiler.countOperation("bottomUpOverall",size,3);
		SWAP(heap[node],heap[l]);
		Heapify(l);
	}
}

void bottomup()
{
	for(int i = 0; i < size; i++)
	{
		heap[i+1] = a[i];
	}
	heapSize = size;
	//build the heap
	for(int i = size/2; i >= 1; i=i-1)
	{
		Heapify(i);
	}
}

//top-down functions;
int pop()
{
	int max = 0;
	if (heapSize < 1)
	{
		printf("Error. Heap underflow.\n");
		return inf;
	}
	max = heap[1];
	heap[1] = heap[heapSize];
	heapSize=heapSize-1;
	Heapify(1);
	return max;
}

void IncreaseKey(int node, int key)
{
	profiler.countOperation("topDownOverall",size,1);
	while((node > 1) && (heap[parent(node)] < key))
	{
		heap[node] = heap[parent(node)];
		node = parent(node);
		profiler.countOperation("topDownOverall",size,2);
	}
	profiler.countOperation("topDownOverall",size,1);
	heap[node] = key;
	return;
}

void HeapInsert(int key)
{
	heapSize++;
	profiler.countOperation("topDownOverall",size,1);
	heap[heapSize] = -inf;
	IncreaseKey(heapSize,key);
}

void topdown()
{
	// make the "heap" elements 0 
	for(int i = 1; i <= size; i++)
	{
		heap[i] = 0;
	}
	heapSize = 1;
	profiler.countOperation("topDownOverall",size,1);
	heap[1] = a[0];
	for(int i = 1; i < size; i++)
	{
		HeapInsert(a[i]);
	}
}

void PrintHeap(int node, int level)
{
    if (node <= size && node > 0)
	{
        PrintHeap(right(node), level + 1 );
        for (int i = 0; i <= level; i++ )
            printf( "       " ); 
        printf( "%d \n", heap[node]);
        PrintHeap(left(node), level + 1 );
    }
}

void main()
{
	
	// Proof

	a[0] = 0; 
	a[1] = 14; 
	a[2] = 1; 
	a[3] = 3; 
	a[4] = 7; 
	a[5] = 8; 
	a[6] = -3;
	a[7] = 4; 
	a[8] = -7; 
	a[9] = 9;
	for(int i = 0; i < size; i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n\n\n");
	bottomup();
	PrintHeap(1,1);
	printf("\n\n\n");
	topdown();
	PrintHeap(1,1);
	getch();
	

	/*
	// Average case
	for(int q = 0; q <= 4; q++)
	{
		printf("Step %d out of 5 \n", q+1);
		for(size = 100; size <= 10000; size= size + 100)
		{
			FillRandomArray(a,size, -1000, 5000,false,0);
			bottomup();
			topdown();
			printf("%d\n",size);
		}
	}
	profiler.createGroup("OverallOperations","bottomUpOverall","topDownOverall");
	profiler.showReport();

	return; */
}