#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
int N=10;
int parent[]={0,2,7,5,2,7,7,-1,5,2};
int presence[11];

typedef struct nodemulty
{
	int key;
	nodemulty ** children;
	int nrchld;
}nodeM;
typedef struct nodebinary
{
	int key;
	nodebinary* parent;
	nodebinary* left;
	nodebinary* right;
}nodeB;
nodeM * Multy;
nodeB * Binary;

void transform()
{
    int i;

    for (i=1; i<N; i++)
    {
       Binary[i].key = i;
	   Binary[i]. left = NULL;
	   Binary[i]. right = NULL;
    }

    for (i=1; i<N; i++)
    {
		//the first child node
		if(Multy[i].nrchld!=0)
			{
				if ( Multy[i].children[0] != NULL)
						Binary[i].left = &Binary[Multy[i].children[0]->key];}
				else
						Binary[i].left = NULL;

       // the brother on the right (i.e. the next brother node)
       if (Binary[i] .left != NULL)
            {
				for(int j=1;j<Multy[i].nrchld;j++)
					 if (Multy[i].children[j] != NULL)
							Binary[Multy[i].children[j-1]->key]. right = &Binary[Multy[i].children[j]->key];     
            }
		
    }
	
}

void pretty_print (nodeB *p, int tabs)
{	

	int i;
	nodeB *q;

	for(i=0; i<tabs; i++)
		printf(" ");
	printf("%d\n",p->key);
	
	if(p->left != NULL)
		pretty_print(p->left,tabs+2);
	
	q = p->right;
	if (q != NULL)
	{
		pretty_print(q,tabs);
	}
}
void print_multi(nodeM *x){
	if(x!=NULL){
		printf("%d ",x->key);
		if(x->nrchld!=0)
		for(int i=0;i<x->nrchld;i++){
			print_multi(x->children[i]);
		}
		else
			printf("\n");
	}
}
int main()
{
	int k[11];
	int i,root;
	for(i=1;i<N;i++)
		if(parent[i]!=-1)
		presence[parent[i]]++;
		else
			root=i;

	Multy=(nodemulty *)malloc(9 * sizeof(nodemulty));
	for(i=1;i<N;i++)
		{
			Multy[i].key=i;	
			if(presence[i]>0)
				Multy[i].children=(nodemulty **)malloc(presence[i] * sizeof(nodemulty));
			else
				Multy[i].children=NULL;
			Multy[i].nrchld=presence[i];
			k[i]=0;
	
	}
	
printf("Number of childern  :\n\n");
for(int i=1; i<N; i++)
		printf("Node %d has %d children\n",i,Multy[i].nrchld);
		
for(int i=1; i<N; i++){
		/*	
		--parent[i] is the parent of i--
		*/
		if(parent[i]>=0 && Multy[parent[i]].nrchld>0) 
			{Multy[parent[i]].children[k[parent[i]]]=&Multy[i];
				k[parent[i]]++;
		}
	}
printf("\nMulty-way  :\n\n");
print_multi(&Multy[root]);
Binary=(nodebinary *)malloc(9 * sizeof(nodebinary));
transform();
printf("\nBinary  :\n\n");
pretty_print(&Binary[root],2);
getch();
return 0;
}