/* Nume Prenume : ***ANONIM*** ***ANONIM***-Cornel
   Grupa : ***GROUP_NUMBER***

 
 Metode de sortare directa


 CONCLUZII : 
 Metodele se sortare au complexitatile : 

	Bubble - fav O(n)
			- nefav O(n*n)
			- mediu O(n*n)

	Selection - fav O(n*n)
			 - nefav O(n*n)
			 - mediu O(n*n)

	Insertion - fav O(n)
			  - nefav O(n*n)
			  - mediu O(n*n)
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>
#include <time.h>

//fisier in care scriem;
FILE *fout = fopen("lab2.csv", "a");

#define NMAX 10000 //numarul de numere aleatoare generate
#define INCREMENT 100 //pasul de incrementare

//vector cu numere generate aleator
int allRandom[NMAX];

//vectori pentru nr de atribuiri si comparatii la fiecare metoda
long a1[NMAX / INCREMENT]; // atribuiri bubbleSort
long c1[NMAX / INCREMENT]; // comparatii bubbleSort

long a2[NMAX / INCREMENT]; // atribuiri selectionSort
long c2[NMAX / INCREMENT]; // comparatii selectionSort

long a3[NMAX / INCREMENT]; // atribuiri insertionSort
long c3[NMAX / INCREMENT]; // comparatii insertionSort



//metoda ce genereaza random
void generareRandom(int n){

	int i;
	//generare random seed
	srand (time(NULL));

	//generare NMAX numere random intre 1 si NMAX
	for(i=0;i<n;i++)
	{
		allRandom[i] = rand() % NMAX;
	}

}

//metoda ce genereaza randomCrescator
void generareRandomCrescator(int n)
{
	int i;

	srand(time(NULL));
	allRandom[0] = rand() % NMAX;
	for(i=1;i<n;i++)
	{
		allRandom[i] = allRandom[i-1] + rand() % NMAX;
	}

}

//metoda ce genereaza randomDescrescator
void generareRandomDescrescator(int n)
{
	int i;
	srand(time(NULL));
	
	allRandom[NMAX-1] = rand() % NMAX;
	
	for(i=n-2;i>=0;i--)
	{
		allRandom[i] = allRandom[i+1] + rand() % NMAX;
	}

}

//metoda bulelor
// @param temp[] -> vector ce trebuie sortat
// @param n -> lungimea vectorului
void bubbleSort(int n)
{
	int ok;
	int i;
	int aux;
	int temp[NMAX+1];

	for(i=0;i<n;i++)
	{
		temp[i] = allRandom[i];
	}


	do{
		ok = 0;
		for(i = 0; i<n;i++)
		{
			c1[n/INCREMENT - 1]++;

			if( temp[i] > temp[i+1] )
			{

				a1[n/INCREMENT - 1]+=3;

				aux = temp[i];
				temp[i] = temp[i+1];
				temp[i+1] = aux;
				ok=1;
			}
		}

	}while(ok == 1);
}

//metoda sortarii prin selectie
// 
void selectionSort(int n)
{
	int i,j;
	int pozMin;
	int temp[NMAX+1];
	int aux;

	for(i=0;i<n;i++)
	{
		temp[i] = allRandom[i];
	}

	for(i=0;i<n;i++)
	{
		pozMin = i;
		for(j=i;j<n;j++)
		{
			c2[n/INCREMENT - 1]++;
			if( temp[j] < temp[pozMin] )
				pozMin = j;
		}
		if(i!=pozMin)
		{
			a2[n/INCREMENT - 1]+=3;
			aux = temp[i];
			temp[i] = temp[pozMin];
			temp[pozMin] = aux;
		}

	}

}

//metoda sortarii prin insertie

void insertionSort(int n)
{
	int i,j;
	int x;
	int temp[NMAX+1];

	for(i=0;i<n;i++)
	{
		temp[i] = allRandom[i];
	}

	for(j=1;j<n;j++)
	{
		a3[n/INCREMENT - 1]++;
		x=temp[j]; 
		i = j-1;
	
		c3[n/INCREMENT - 1]++;
		while( i>=0 && x<temp[i])
		{
			c3[n/INCREMENT - 1]++;
			a3[n/INCREMENT - 1]++;
			temp[i+1] = temp[i];
			i--;
		}
		temp[i+1] = x;
		a3[n/INCREMENT - 1]++;
	}
}

//metoda pentru afisarea nr de atribuiri/comparatii in fisier
void afisare(long a[], long c[])
{
	int i;
	fprintf(fout, "\n");
	for(i=0;i<NMAX/INCREMENT;i++)
	{
		fprintf(fout, "%d,", a[i]);
	}

	fprintf(fout, "\n");

	for(i=0;i<NMAX/INCREMENT;i++)
	{
		fprintf(fout, "%d,", c[i]);
	}

	fprintf(fout, "\n");
	for(i=0;i<NMAX/INCREMENT;i++)
	{
		fprintf(fout, "%d,", a[i]+c[i]);
	}

}

//functie MAIN
int main(){

	int n;

	//generareRandom(NMAX);
	//generareRandomCrescator(NMAX);
	generareRandomDescrescator(NMAX);


	//fprintf(fout, "\n bubbleSort,allRandom \n");
	//fprintf(fout, "\n bubbleSort,allRandomCrescator \n");
	//fprintf(fout, "\n bubbleSort,allRandomDescrescator \n");

	//fprintf(fout, "\n selectionSort,allRandom \n");
	//fprintf(fout, "\n selectionSort,allRandomCrescator \n");
	//fprintf(fout, "\n selectionSort,allRandomDescrescator \n");

	//fprintf(fout, "\n insertionSort,allRandom \n");
	//fprintf(fout, "\n insertionSort,allRandomCrescator \n");
	//fprintf(fout, "\n insertionSort,allRandomDescrescator \n");

	for(n=100;n<=NMAX;n+=INCREMENT)
	{
		bubbleSort(n);
		selectionSort(n);
		insertionSort(n);
	}
	
	fprintf(fout, "\nbubbleSort,allRandomDescrescator\n");
	afisare(a1, c1);
	fprintf(fout, "\n\nselectionSort,allRandomDescrescator\n");
	afisare(a2, c2);
	fprintf(fout, "\n\ninsertionSort,allRandomDescrescator\n");
	afisare(a3, c3);

	fclose(fout);
	return 0;
}