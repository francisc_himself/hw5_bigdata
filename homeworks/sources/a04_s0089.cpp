#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fstream>
#include "Profiler.h"
using namespace std;


Profiler P("***ANONIM***");


typedef struct node{
	int val;
	node *next;
} node;

typedef struct positionitionElementent{
	int position;
	int element;
} positionitionElementent;

struct list{
	node *first;
	node *last;
};




struct list lists[501];
node *interfirst ;
node *interlast;
positionitionElementent Heap[10001];
int a[10001];




void insert(node **first,node **last,int val){
	if((*first)==NULL){
		(*first) = new node;
		(*first)->val=val;
		(*first)->next=NULL;
		*last=(*first);
	}
	else{
		node *element=new node;
		element->next=NULL;
		element->val=val;
		(*last)->next=element;
		(*last)=element;
	}
}

void max_heapify_down(positionitionElementent Heap[],int i,int size,int &comparisons,int &assignments){
	int left,right,largest;
	positionitionElementent aux;
	left=2*i;
	right=2*i+1;
	if ((left<=size)&&(Heap[left].element < Heap[i].element)){
		largest=left;
	}
	else{
		largest=i;
	}
	if ((right<=size)&&(Heap[right].element < Heap[largest].element)){
		largest=right;
	}
	
	comparisons+=2;

	if (largest!=i)	{
		aux=Heap[i];
		Heap[i] = Heap[largest];
		Heap[largest]=aux;
		assignments+=3;
		max_heapify_down(Heap,largest,size,comparisons,assignments);
	}
}

void insertInHeap(positionitionElementent Heap[],positionitionElementent x,int *size,int &comparisons,int &assignments){
	(*size)++;
	Heap[(*size)]=x;
	int i=(*size);
	positionitionElementent aux;
	
	while (i>1 && (Heap[i].element)<(Heap[i/2].element))	{
		comparisons++;
		aux=Heap[i];
		Heap[i]=Heap[i/2];
		Heap[i/2]=aux;
		assignments+=3;
		i=i/2;
	}
	comparisons++;
}

positionitionElementent removeFromHeap(positionitionElementent Heap[],int *size,int &comparisons,int &assignments){
	positionitionElementent x;
	if((*size)>0)	{
		x=Heap[1];

		Heap[1]=Heap[(*size)];
		assignments++;
		(*size)--;
		
		max_heapify_down(Heap,1,(*size),comparisons,assignments);
	}
	return x;
}

void mergeLists(list lists[],int k,int &comparisons,int &assignments){
	int size = 0;
	positionitionElementent v;
	for(int i=1; i<=k; i++)	{
		v.position=i;
		v.element=lists[i].first->val;
		assignments++;
		insertInHeap(Heap,v,&size,comparisons,assignments);
	}
	positionitionElementent pop;
	while (size>0){

		pop = removeFromHeap(Heap,&size,comparisons,assignments);

		int position = pop.position;
		insert(&interfirst,&interlast,pop.element);

		lists[position].first=lists[position].first->next;
		assignments++;
		comparisons++;
		
		if (lists[position].first!=NULL){
			v.position=pop.position;
			v.element=lists[pop.position].first->val;
			assignments++;
			insertInHeap(Heap,v,&size,comparisons,assignments);
		}
	}
}


void createLists(list lists[],int k,int n){
	int nr = 0;
	
	for (int i=1; i<=k; i++){
		lists[i].first = lists[i].last = NULL;
	}
	int arr[10000];
	for (int j=1; j<=k; j++){
		FillR***ANONIM***omArray(arr,n,1,50000,false,1);
		for (int i=1; i<=n/k; i++){
			insert(&lists[j].first,&lists[j].last,arr[i]);
		}
	}
}


void print(node *first,node *last){
    node *c;
    c=first;
    while(c!=0){
		cout<<c->val<<" ";
        c=c->next;
    }
	cout<<"\n";
    cout<<endl;
}

void show(list CAP[],int k){
    for (int i =1 ; i<=k;i++){
        print(CAP[i].first,CAP[i].last);
	}
}

void example(){
	
	int comparisons,assignments;

	int n=20;
	int k=5;
	createLists(lists,k,n);
	show(lists,k);


	mergeLists(lists,k,comparisons,assignments);

	print(interfirst,interlast);
	
	getch();
}


int main(){
	
	P.createGroup("Total Operations","Operations 5 Lists","Operations 10 Lists","Operations 100 Lists");
	P.createGroup("Constant length","Operations");
	int comparisons,assignments;


	int n;


	int k =5;
	for(int index=100;index<=10000;index+=100)
	{
		createLists(lists,k,index);
		comparisons=0;
		assignments=0;
		mergeLists(lists,k,comparisons,assignments);

		P.countOperation("Operations 5 Lists",index,comparisons);
		P.countOperation("Operations 5 Lists",index,assignments);
	}
	
	k=10;
	for(int index=100;index<=10000;index+=100)
	{
		createLists(lists,k,index);
		comparisons=0;
		assignments=0;
		mergeLists(lists,k,comparisons,assignments);

		P.countOperation("Operations 10 Lists",index,comparisons);
		P.countOperation("Operations 10 Lists",index,assignments);
	}

	k=100;
	for(int index=100;index<=10000;index+=100)
	{
		createLists(lists,k,index);
		comparisons=0;
		assignments=0;
		mergeLists(lists,k,comparisons,assignments);

		P.countOperation("Operations 100 Lists",index,comparisons);
		P.countOperation("Operations 100 Lists",index,assignments);
	}
	

	n=10000;
	for(int i=10;i<=500;i+=10)
	{
		createLists(lists,i,n);
		comparisons=0;
		assignments=0;
		mergeLists(lists,i,comparisons,assignments);

		P.countOperation("Operations",i,comparisons);
		P.countOperation("Operations",i,assignments);
	}
	
	
	
	P.showReport();
	example();
}


// The opperations number in the 3 cases have a linear increase, increasing as the list number is bigger.
// The opperations number for the same length lists has logarithmic step-like shape ***ANONIM*** it increases as the lists number is bigger.