/* Hobor Edwin-Wilhelm
Grupa : ***GROUP_NUMBER***
10.03.2013 */

/* Metoda de sortare Bubble Sort este cea mai ineficienta atunci cand luam in considerare numarul de atribuiri si numarul de comparatii.
Metoda de sortare prin selectie este cea mai eficienta daca luam in considerare numarul de atribuiri.
Metoda de insertie este cea mai eficienta daca luam in considerare numarul de comparatii.
Adunand numarul de atribuiri si numarul de comparatii putem trage concluzia ca cea mai eficienta metoda de sortare este prin insertie. */


#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <fstream>


void randomMare(long int a[],long int n)
{
	int i = 0;
	for(i=1;i<=n;i++)
	{
		a[i] = rand()%1000;
	}
}

void randomCresc(long int a[],long int n)
{
	int i = 0;
	a[0] = rand() % 10;
	for(i=1;i<=n;i++)
	{
		a[i] = a[i-1] + rand() % 10;
	}
}

void randomDescr(long int a[],long int n)
{
	int i = 0;
	a[0] = rand() % 100 + n;
	for(i=1;i<=n;i++)
	{
		a[i] = a[i-1] - rand() % 10;
	}
}

void bubble_sort(long int A[],long int n, long &na, long &nc)
{
	int aux;

	for (int i=1; i<=n-1; i++)
		for (int j=1; j<=n-1; j++)
		{
			nc++;
			if (A[j] > A[j+1])
			{
				na = na +3;
				aux = A[j];
				A[j] = A[j+1];
				A[j+1] = aux;
			}
		}
}

void select_sort(long int A[],long int n,long &na, long &nc)
{
	int imin,aux,i;

	for(i=1;i<=n;i++)
	{
		imin = i;
		for(int j=i+1;j<=n;j++)
		{
			nc++;
			if(A[j]<A[imin])
			{
				imin = j;
			}
		}
		na=na+3;
		aux=A[i];
		A[i]=A[imin];
		A[imin]=aux;
	}
}

void insert_sort(long int A[],long int n, long &na, long &nc)
{
	int x,j;

	for(int i=2;i<=n;i++)
	{
		na++;
		x=A[i];
		j=1;
		nc++;
		while(j<i && A[j]<x)
		{
			nc++;
			j++;
		}
		for(int k=i-1;k>=j;k--)
		{
			na++;
			A[k+1]=A[k];
		}
		na++;
		A[j]=x;

	}
}

void copy(long int A[], long int B[],long int n)
{
	for (int i=1; i<=n; i++)
		B[i] = A[i];
}

void favorable_case()
{
	long int A[10001],B[10001];
	long int n;
	long na = 0;
	long nc = 0;

	FILE *file1;
	file1 = fopen ("bubble_favorabil.txt","w");

	FILE *file2;
	file2 = fopen ("select_favorabil.txt","w");

	FILE *file3;
	file3 = fopen ("insert_favorabil.txt","w");

	for (n=100; n<=10000; n=n+100)
	{

		randomCresc(A,n);
		printf("%d  ",n);
		//bubble_sort
		na = 0;
		nc = 0;
		copy(A,B,n);
		bubble_sort(B,n,na,nc);
		fprintf(file1,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//select_sort
		na = 0;
		nc = 0;
		copy(A,B,n);
		select_sort(B,n,na,nc);
		fprintf(file2,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//insert_sort
		na = 0;
		nc = 0;
		copy(A,B,n);
		insert_sort(B,n,na,nc);
		fprintf(file3,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);


	}

	fclose(file1);
	fclose(file2);
	fclose(file3);
}

void defavorable_case ()
{
	long int A[10001], B[10001];
	long int n;
	long na = 0;
	long nc = 0;

	FILE *file1;
	file1 = fopen ("bubble_dfav.txt","w");
	FILE *file2;
	file2 = fopen ("select_dfav.txt","w");
	FILE *file3;
	file3 = fopen ("insert_dfav.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		randomDescr(A,n);
		printf("%d  ",n);
		//bubble_sort
		na = nc = 0;
		copy(A,B,n);
		bubble_sort(B,n,na,nc);
		fprintf(file1,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//select_sort
		na = nc = 0;
		copy(A,B,n);
		select_sort(B,n,na,nc);
		fprintf(file2,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);

		//insert_sort
		na = nc = 0;
		copy(A,B,n);
		insert_sort(B,n,na,nc);
		fprintf(file3,"%d \t %d \t %d \t %d\n",n,na,nc,na+nc);
	}
}

void medium_case ()
{
	long int A[10001],B[10001];
	long int n;
	long na1,na2,na3,nc1,nc2,nc3;
	long na,nc;

	FILE *file1;
	file1 = fopen ("bubble_med.txt","w");
	FILE *file2;
	file2 = fopen ("selectie_med.txt","w");
	FILE *file3;
	file3 = fopen ("insertie_med.txt","w");

	for (n=100; n<=10000; n=n+100)
	{
		na1 = 0;
		na2 = 0;
		na3 = 0;
		nc1 = 0;
		nc2 = 0;
		nc3 = 0;

		for (int k=1; k<=5; k++)
		{
			randomMare(A,n);
			printf("%d  ",n);
			//bubble_sort
			na=0;
			nc=0;
			copy(A,B,n);
			bubble_sort(B,n,na,nc);
			na1+=na;
			nc1+=nc;

			//select_sort
			na=0;
			nc=0;
			copy(A,B,n);
			select_sort(B,n,na,nc);
			na2+=na;
			nc2+=nc;

			//insert_sort
			na=nc=0;
			copy(A,B,n);
			insert_sort(B,n,na,nc);
			na3+=na;
			nc3+=nc;
		}

		na1/=4;nc1/=4;
		na2/=4;nc2/=4;
		na3/=4;nc3/=4;

		fprintf (file1,"%d \t %d \t %d \t %d\n",n,na1,nc1,na1+nc1);
		fprintf (file2,"%d \t %d \t %d \t %d\n",n,na2,nc2,na2+nc2);
		fprintf (file3,"%d \t %d \t %d \t %d\n",n,na3,nc3,na3+nc3);
	}
}

int main()
{
	favorable_case();
	defavorable_case();
	medium_case();

	return 0;
}
