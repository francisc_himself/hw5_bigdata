#include "stdio.h"
#include "conio.h"
#include "Profiler.h"

//macros
#define N 9973

//global variables
Profiler profiler("josephus_permutations");
struct node *T;
int z;

struct node {
    int data;
	int dim;
    struct node* left;
    struct node* right;
	struct node* parent;
};

/*
 Helper function that allocates a new node
 with the given data and NULL left and right
 pointers.
*/
struct node* NewNode(int data, int dim) {
  struct node* node = new(struct node);    // "new" is like "malloc"
  node->dim = dim;
  node->data = data;
  node->left = NULL;
  node->right = NULL;
  node->parent = NULL;

  return (node);
} 

/*
 Given a binary search tree, print out
 its data elements in increasing
 sorted order.
 Inorder traversal
*/
void printTree(struct node* node) {
  if (node == NULL) return;

  printTree(node->left);
  printf("%d ", node->data);
  printTree(node->right);
}
 
struct node* sortedArrayToBST(int arr[], int start, int end) {
  if (start > end) return NULL;
  // same as (start + end)/2, avoids overflow.
  int mid = start + (end - start) / 2;
  profiler.countOperation("josephus", z, 1);
  struct node* n = NewNode(arr[mid], end - start + 1);
  profiler.countOperation("josephus", z, 2);
  n->left = sortedArrayToBST(arr, start, mid - 1);
  if(n->left != NULL) {
	  n->left->parent = n;
  }
  profiler.countOperation("josephus", z, 2);
  n->right = sortedArrayToBST(arr, mid + 1, end);
  if(n->right != NULL) {
	  n->right->parent = n;
  }
  return n;
}

void padding ( char ch, int n ) {
	int i;
	for ( i = 0; i < n; i++ )
	putchar ( ch );
}

void structure ( struct node *root, int level ) {
	int i;
	if ( root == NULL ) {
		padding ( '\t', level );
		puts ( "~" );
	}
	else {
		structure ( root->right, level + 1 );
		padding ( '\t', level );
		printf ( "%d\n", root->data );
		structure ( root->left, level + 1 );
	}
}

struct node* OS_SELECT(struct node* root, int i) {
	
	int r;
	if(root->left != NULL)
		r = root->left->dim + 1;
	else r = 1;
	profiler.countOperation("josephus", z, 3);
	if(i == r) {
		return root;
	} 
	else if(i < r) {
		profiler.countOperation("josephus", z, 1);
		OS_SELECT(root->left, i);
	}
	else {
		OS_SELECT(root->right, i - r);
	}
}

void redim(struct node* tmp)
{
    while(tmp != NULL)
    {
        tmp->dim--;
		tmp = tmp->parent;
		profiler.countOperation("josephus", z, 1);
        
    }
}



/* delete a node from the binary search tree */

void OS_DELETE(struct node* x) {
    struct node *tmp = NULL;
	int ok = 0;
	profiler.countOperation("josephus", z, 1);
	/*Delete node with no Children: x - node to be deleted
									tmp-> parent of that node :) */
    if(x->left == NULL && x->right == NULL) {
        if(x->parent !=NULL){ // If not Root
			tmp = x->parent;
			//Check which child of tmp is x and destroy bond
			if(tmp->right == x)
				tmp->right = NULL;
			else
				tmp->left = NULL;
		profiler.countOperation("josephus", z, 3);
        }
		else{ //if Root.it's last node in tree we null it
			T = NULL;
			profiler.countOperation("josephus", z, 1);
		}
		//If wasn't the root, we redim. and free x
		if(tmp != NULL)
            redim(tmp);
        free(x);

    }
    else /*Case 2 children*/
        if(x->left != NULL && x->right != NULL)
        {
			profiler.countOperation("josephus", z, 1);
			/*Go 1 step right, then left until NULL (Lowest data value from the right subtree (succesor of node-to-be-deleted)*/
            tmp = x->right;
			profiler.countOperation("josephus", z, 1);
			while(tmp->left != NULL){
                tmp = tmp->left;
				ok++;  /*If ok is 0 then we know that x->right = tmp. */
				profiler.countOperation("josephus", z, 1);
			}

            /**Change data*/
            x->data = tmp->data;
			profiler.countOperation("josephus", z, 1);

            /**Change Parent field of right child*/
			if(ok == 0){
				if(tmp->right != NULL){
					tmp->right->parent = x;
					x->right = tmp->right;	
					profiler.countOperation("josephus", z, 2);
				}
				else
					x->right = NULL;
			}
			else{
				if(tmp->right != NULL){
					tmp->right->parent = tmp->parent;
					profiler.countOperation("josephus", z, 2);
				}
				tmp->parent->left = tmp->right;
				profiler.countOperation("josephus", z, 1);
				
			}
            
            /* redim for tmp parent */
            redim(tmp->parent);
            /* Delete the node */
            free(tmp);
        }
        else
        {
            if(x->right != NULL)
            {
				profiler.countOperation("josephus", z, 1);
				if(x == T){
					T = x->right;
					x->right->parent = NULL;
					profiler.countOperation("josephus", z, 2);
				}
				else{
					profiler.countOperation("josephus", z, 1);
					if(x->parent->left == x)
					  x->parent->left = x->right;
					else
					 x->parent->right = x->right;
					 profiler.countOperation("josephus", z, 2);
					 x->right->parent = x->parent;
					profiler.countOperation("josephus", z, 1);
					redim(x->parent);
				}
				free(x);
            }
            else
            {
                if(x == T){
					T = x->left;
					x->left->parent = NULL;
					profiler.countOperation("josephus", z, 2);
				}
				else{
				x->left->parent = x->parent;
				profiler.countOperation("josephus", z, 2);
				if(x->parent->left == x)
                    x->parent->left = x->left;
                else
                    x->parent->right = x->left;
				profiler.countOperation("josephus", z, 2);
				}
                redim(x->parent);
				
				free(x);
            }
        }
}



void Josephus(int n, int m)
{
    int k = n, j = 1;
    struct node *x;
	int array[10000];
	for(int i = 0; i < n; i++)
		array[i] = i + 1;
	T = sortedArrayToBST(array, 0, n - 1);
    //structure(root, 0);

    while(k > 0)
    {
        j = ((j + m - 2) % k) + 1;
        x = OS_SELECT(T, j);
        printf("______________________________________%d______________________________________", x->data);
		OS_DELETE(x);
		//structure(T, 0);
        printf("\n");
        //if(k > 1)
        //    structure(root, 0);
        k--;
    }
}

int main() {
	
	//int n = 10;
	//int array[10];
	//for(int i = 1; i <= n; i++)
	//	array[i] = i;
	//printTree(sortedArrayToBST(array, 0, n - 1));
	//structure(sortedArrayToBST(array, 0, n - 1), 0);
	//struct node* root = sortedArrayToBST(array, 0, n - 1);
	//struct node* node = OS_SELECT(root, 6);
	//printf("%d\n", node->data);
	//for(z = 100; z <= 10000; z += 100)
	//	Josephus(z, z/2);
	Josephus(7, 3);

	profiler.createGroup("Josephson: Assignments + Comparisons", "josephson");
	profiler.showReport();
	return 0;
}