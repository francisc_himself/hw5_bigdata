/****ANONIM*** ***ANONIM***
***GROUP_NUMBER****/
#include <stdio.h>
#include <conio.h>
#include <iostream>
using namespace std;

struct nod{
int valoare;
nod *next;};

nod *head[1000];
nod *tail[1000];
nod *joinedHead,*joinedTail;

int n; //numarul de noduri

nod *prim;
nod *ultim;

//************* CREAREA LISTEI **************

void inserare() // functia creaza si adauga cat eun nou element
{
nod *c;
if(!prim) //lista e vida (p este 0) - se adauga primul nod in lista
	{
		prim= new nod;
		cout<<"Vlaoarea primului nod: ";
		cin>>prim->valoare;
		ultim=prim; // in acest caz, primul si ultimul nod sunt egali
	}
else // se adauga nod la sfarsitul listei
	{
		c=new nod;
		cout<<"Valoare nod ";
		cin>>c->valoare;
		ultim->next=c; //adaugare dupa ultimul nod
		ultim=c; //noul nod c se marcheaza ca fiind ultimul
	}
ultim->next=0; //ultimul nod pointeaza spre null;
}

//***************** AFISAREA LISTEI **************
void afisare() //parcurge elementele si le afiseaza
{
nod *c;
c=prim; //se porneste de la primul nod
while(c)
	{
		cout<<c->valoare<<" ";
		c=c->next;// se trece la nodul urmator
	}
cout<<endl;
}
//***************** Stergerea Lprimului nod din lista **************
void stergere(int val)
{nod *c,*a; //a se sterge, c este precedentul sau. Se va genera o noua //legatura intre c si a->next
 c=prim;
 if(prim->valoare==val) //daca primul nod retine val se sterge primul
   {a=prim;          //se retine in a
    prim=prim->next; //primul va deveni urmatorul element
    delete a;}          //se elibereaza memoria
 else
    {while(c->next->valoare!=val &&c)//se pozitioneaza pe elementul ce urmeaza a    //fi sters
       c=c->next;
     a=c->next;
     c->next=a->next;
     if(a==ultim) ultim=c;
     delete a;}// se elibereaza memoria
 }

// main
void main()
{	
	//int m;
	//cout<<"Dati numaul de liste: "; cin>>m;
	//for(int j=0;j<m;j++)
	//{
	//cout<<"Dati dimensiunea listei "<<j<<" "; cin>>n;
	cout<<"n= ";cin>>n;
		for(int i=0;i<n;i++)
		{
			inserare();
		}
		cout<<endl;
	//}
	//for(int j=0;j,m;j++)
	//{
		afisare();
	//}
	getch();
	
}