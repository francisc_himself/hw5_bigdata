#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>


/*

O(nlogk) method for merging k sorted sequences, where n is the total number of elements: logaritmic growth + liniar growth  
Use linked lists to represent the k sorted sequences and the output sequence
Using a heap we don't get running time 


There are 2 analysis cases:
1. k is constant: 3 constant values for k (k1=5, k2=10, k3=100);n varies between 100 and 10000
   For k1=5(i.e 5 sorted lists generated randomly) the algorithm takes between 815-83140 total nr. of operations
   For k2=10(10 sorted lists generated randomly) the algorithm takes between 1170-123219 total nr. of operations
   For k3=100(100 sorted lists generated randomly) the algorithm takes between 2680-275474 total nr. of operations

2. n is constant: n = 10.000; the value of k must vary between 10 and 500 with an increment of 10;
   Charts analysis: The total number of operations performed in this case are in the range 92050-348827

   - after applying heapify on the elements on the first position in the list, we construct a min heap and take the root of the heap 
(the minimum element) and add it in the final list
   - when one of the lists has no more elements we decrement the size of the heap by 1
*/

typedef struct Node
{
	long info;
	struct Node *next;
}NodeT;


NodeT *a[1000];

long operations=0;
long heapSize;
NodeT *merged[10001];
int k;// k takes the following values: 5,10,100


void heapify(NodeT *A[], int i) //min heapify
{
   int l,r,min;
   long aux;

   l=2*i;
   r=2*i+1;

   operations++;
   if (l<=heapSize && A[l]->info<A[i]->info)
       min=l;
   else
       min=i;

   operations++;
   if (r<=heapSize && A[r]->info<A[min]->info)
       min=r;
   
	if (min!=i)
       {
           operations+=3;
           A[0]=A[i];
           A[i]=A[min];
		   A[min]=A[0];
           heapify(A,min);
       }
}

void bh_bottomup(NodeT *A[],int n)
{
   int i;
   heapSize=n;
   for (i=heapSize/2;i>=1;i--)
       heapify(A,i);
}

void insert(NodeT *first, int data){  //just this case when already the first elemnt from the list was already inserted
	
	 NodeT *q=new NodeT;
     q->info=data;
     q->next=NULL;
	 first->next=q;
     first=q;
}


int main()
{
   FILE *f,*g;
   f=fopen("k1.txt","w");
   g=fopen("n.txt","w");
   int i,j,l,m,n,size,nr,r;
   long op;
   srand(time(NULL));
  
   //show that the algorithm works(k=4,n=20)
   k=4;
   n=20;
   for (i=1;i<=k;i++)
           {
                a[i]=new NodeT;
                a[i]->info=rand() % (k);
                a[i]->next=NULL;
                NodeT *first=new NodeT;
			    first=a[i];
				printf("%d ",a[i]->info);
               for (j=2;j<=n/k;j++)
               {
                   NodeT *q=new NodeT;
                   q->info=first->info+rand() % (k);
                   q->next=NULL;
                   first->next=q;
                   first=q;
				   printf("%d ",q->info);   
               }
			    printf("\n");
           }
           size=k;
           nr=1;
           bh_bottomup(a,size);
		  
           while (size!=0)
           {
               merged[nr]=new NodeT;
               merged[nr]->info=a[1]->info;
               a[1]=a[1]->next;
			   merged[nr]->next=NULL;
			   nr++;

               if (a[1]==NULL)
               {
                   a[1]=a[size];
                   size--;
				   heapSize--;
               }

               heapify(a,1);
           }
   // print the result
	for(j=1;j<=n;j++)
		printf("%d ",merged[j]->info);
  

   //k is constant
	k=5;
   for (i=100;i<=10000;i=i+100)
   {
       op=0;

		for (j=1;j<=5;j++)
       {
           operations=0;

           for (l=1;l<=k;l++)
           {
               a[l]=new NodeT;
               a[l]->info=rand() % (k);
               a[l]->next=NULL;
               NodeT *nod=new NodeT;
			   nod=a[l];
               for (m=2;m<=i/k;m++)
               {
                   NodeT *q=new NodeT;
                   q->info=nod->info+rand() % (k);
                   q->next=NULL;
                   nod->next=q;
                   nod=q;
               }
           }

           size=k;
           nr=1;

           bh_bottomup(a,size);

           while (size!=0)
           {
               merged[nr]=new NodeT;
               merged[nr]->info=a[1]->info;
               a[1]=a[1]->next;
			   merged[nr]->next=NULL;
			   nr++;

               if (a[1]==NULL)
               {
                   a[1]=a[size];
                   size--;
				   heapSize--;
               }
               heapify(a,1);
           }
            op+=operations;

       }
       fprintf(f, "%d %ld\n",i,op/5);
   }



   //n constant; n=10000
   for (i=10;i<=500;i=i+10)
    {
        op=0;
		for (j=1;j<=5;j++)
        {
            operations=0;

            for (l=1;l<=i;l++)
            {
                a[l]=new NodeT;
                a[l]->info=rand() % (l);
                a[l]->next=NULL;
                NodeT *nod=new NodeT;
				nod=a[l];
                for (m=2;m<=10000/i;m++)  
                {
                    NodeT *q=new NodeT;
                    q->info=nod->info+rand() % (l);
                    q->next=NULL;
                    nod->next=q;
                    nod=q;
                }
            }
            size=i;
            nr=1;

            bh_bottomup(a,size);

            while (size!=0)
            {
               merged[nr]=new NodeT;
               merged[nr]->info=a[1]->info;
               a[1]=a[1]->next;
			   merged[nr]->next=NULL;

                if (a[1]==NULL)
                {
                    a[1]=a[size];
                    size--;
					heapSize--;
                }
			heapify(a,1);
            }
             op+=operations;
        }
        fprintf(g,"%d %ld\n",i,op/5);
    }


fclose(f);
fclose(g);
getch();
return 0;
}

