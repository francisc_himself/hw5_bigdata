#include <stdio.h>
#include <stdlib.h>
#define N_MAX 20
#define MAX_FII 10


typedef struct R1
{
    int nr_fii;
    int fii[MAX_FII];
}R1;

typedef struct R2
{
    int key;
    int nr_fii;
    struct R2 *fii[MAX_FII];
} R2;

typedef struct R3
{
    int key;
    struct R3 *stg;
    struct R3 *dr;
} R3;

//O(n)
void transformare_T1(R2 **r, R1 fii[])
{
    int i, parinte = (*r)->key;

    for(i = 0; i < fii[parinte].nr_fii; i++)
    {

        (*r)->fii[i] = (R2*)malloc(sizeof(R2));
        (*r)->fii[i]->key = fii[parinte].fii[i];
        (*r)->fii[i]->nr_fii = 0;
        (*r)->nr_fii++;

        transformare_T1(&((*r)->fii[i]), fii);
    }
}

void afisare_R2(R2 *r, int nr)
{
    int i;
    for(i = 0; i <= nr; i++)
        printf(" ");
    printf("%d\n", r->key);
    for(i = 0; i < r->nr_fii; i++)
        afisare_R2(r->fii[i], nr+1);
}

//O(n)
void creare_arbore_R3(R3 **binar, R2 *multicai)
{
    R3 *aux;
    int i;
    if(multicai->nr_fii > 0)
    {
        (*binar)->stg = (R3*)malloc(sizeof(R3));
        (*binar)->stg->key = multicai->fii[0]->key;
        (*binar)->stg->stg = NULL;
        (*binar)->stg->dr = NULL;
        aux = (*binar)->stg;
        for(i = 1; i < multicai->nr_fii; i++)
        {
            aux->dr = (R3*)malloc(sizeof(R3));
            aux->dr->key = multicai->fii[i]->key;
            aux->dr->stg = NULL;
            aux->dr->dr = NULL;
            creare_arbore_R3(&aux,multicai->fii[i-1]);
            aux = aux->dr;
        }

    }
}

void afisare_R3(R3 *r, int nr)
{
    int i;
    for(i = 0; i <= nr; i++)
        printf(" ");
    printf("%d\n", r->key);
    if(r->stg != NULL)
        afisare_R3(r->stg, nr+1);
    if(r->dr != NULL)
        afisare_R3(r->dr, nr);
}

int main()
{

    int a[N_MAX], i, n = 9;
    R1 aux[MAX_FII];
    R2 *r2 = NULL;
    R3 *r3 = NULL;
    a[1] = 2;
    a[2] = 7;
    a[3] = 5;
    a[4] = 2;
    a[5] = 7;
    a[6] = 7;
    a[7] = -1;
    a[8] = 5;
    a[9] = 2;

    printf("R1:\n");
    for(i = 1; i <= n; i++)
        printf("%d ", a[i]);
    printf("\n");

    //O(n)

    for(i = 1; i <= n; i++)
        aux[i].nr_fii = 0;
    //O(n)

    for(i = 1; i <= n; i++)
    {
        if(a[i] != -1)
        {
            aux[a[i]].fii[aux[a[i]].nr_fii] = i;
            aux[a[i]].nr_fii++;
        }
    }

    i = 1;
    while(a[i] != -1)
        i = a[i];
    //inserez radacina
    r2 = (R2*)malloc(sizeof(R2));
    r2->key = i;
    r2->nr_fii = 0;
    //O(n)
    transformare_T1(&r2, aux);
    printf("R2:\n");
    afisare_R2(r2, 0);

    r3 = (R3*)malloc(sizeof(R3));
    r3->key = r2->key;
    r3->stg = NULL;
    r3->dr = NULL;
    creare_arbore_R3(&r3, r2);

    printf("R3:\n");
    afisare_R3(r3, 0);


    return 0;
}
