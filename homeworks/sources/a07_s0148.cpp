#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**parintele si fii lui**/
typedef struct parinte_fii
{
    int nr_fii;
    int fii[10];
}parinte_fi0i;

/**R2**/
typedef struct multi_cai
{
    int key;
    int nr_fii;
    struct multi_cai *fii[10];
} multi_cai;

/**R3**/
typedef struct binar
{
    int key;
    struct binar *stg;
    struct binar *dr;
} binar;

//O(n) - adaug n-1 fii radacina fiind deja inserata
void creareArboreMulticai(multi_cai **r, parinte_fii fii[])
{
    int i, parinte = (*r)->key;
    //printf("parinte: %d\n", parinte);
    for(i = 0; i < fii[parinte].nr_fii; i++)
    {
        //adaugare fiu
        (*r)->fii[i] = (multi_cai*)malloc(sizeof(multi_cai));
        (*r)->fii[i]->key = fii[parinte].fii[i];
        (*r)->fii[i]->nr_fii = 0;
        (*r)->nr_fii++;
        //printf("fiu %d: %d\n", (*r)->nr_fii, (*r)->fii[i]->key);
        creareArboreMulticai(&((*r)->fii[i]), fii);
    }
}

void afisare_multicai(multi_cai *r, int nr_spatii)
{
    int i;
    for(i = 0; i <= nr_spatii; i++)
        printf(" ");
    printf("%d\n", r->key);
    for(i = 0; i < r->nr_fii; i++)
        afisare_multicai(r->fii[i], nr_spatii+1);
}

//O(n) - adaug n-1 fii radacina fiind deja inserata
void creareArboreBinar(binar **b, multi_cai *m)
{
    binar *aux;
    int i;
    if(m->nr_fii > 0)
    {
        (*b)->stg = (binar*)malloc(sizeof(binar));
        (*b)->stg->key = m->fii[0]->key;
        (*b)->stg->stg = NULL;
        (*b)->stg->dr = NULL;
        aux = (*b)->stg;
        for(i = 1; i < m->nr_fii; i++)
        {
            aux->dr = (binar*)malloc(sizeof(binar));
            aux->dr->key = m->fii[i]->key;
            aux->dr->stg = NULL;
            aux->dr->dr = NULL;
            creareArboreBinar(&aux,m->fii[i-1]);
            aux = aux->dr;

		}

    }
}

void afisare_binar(binar *r, int nr_spatii)
{
    int i;
    for(i = 0; i <= nr_spatii; i++)
        printf(" ");
    printf("%d\n", r->key);
    if(r->stg != NULL)
        afisare_binar(r->stg, nr_spatii+1);
    if(r->dr != NULL)
        afisare_binar(r->dr, nr_spatii);
}

int main()
{
    //variabile
    int a[20], i, n = 9;
    parinte_fii aux[10];
    multi_cai *r1 = NULL;
    binar *r2 = NULL;
    a[1] = 2;
    a[2] = 7;
    a[3] = 5;
    a[4] = 2;
    a[5] = 7;
    a[6] = 7;
    a[7] = -1;
    a[8] = 5;
    a[9] = 2;

    /******Printare R1******/
    printf("R1:\n");
    for(i = 1; i <= n; i++)
        printf("%d ", a[i]);
    printf("\n");
    
	/**T1**/
        //initializez aux
    for(i = 1; i <= n; i++)
        aux[i].nr_fii = 0;
    
        //creez vectori cu fii fiecarui nod
    for(i = 1; i <= n; i++)
    {
        if(a[i] != -1)
        {
            aux[a[i]].fii[aux[a[i]].nr_fii] = i;//inserez fiul
            aux[a[i]].nr_fii++;//cresc numarul de fii
        }
    }
    
        //caut radacina
    i = 1;
    while(a[i] != -1)
        i = a[i];
    //inserez radacina
    r1 = (multi_cai*)malloc(sizeof(multi_cai));
    r1->key = i;
    r1->nr_fii = 0;
    //O(n)
    creareArboreMulticai(&r1, aux);
    /**T1**/

    /********Printare R2**********/
    printf("R2:\n");
    afisare_multicai(r1, 0);
    

    /**T2**/
    r2 = (binar*)malloc(sizeof(binar));
    r2->key = r1->key;
    r2->stg = NULL;
    r2->dr = NULL;
    creareArboreBinar(&r2, r1);
    /**T2**/

    /********Printare R3**********/
    printf("R3:\n");
    afisare_binar(r2, 0);
    
	getch();
    return 0;
}
