/**
BubbleSortFavorabil:
- compararile cresc liniar
- indiferent de numarul de elemente nu avem atribuiri 
=> suma celor doua va fi chiar numarul de comparari;
BubbleSortDefavorabil:
- atat compararile,cat si atribuirile cresc exponential
=> suma lor va creste exponential odata cu cresterea numarului de elemente;
BubbleSortMediu:
- compararile,atribuirile si suma acestora cresc exponential si au valori tot mai mari cu cat creste numarul elemenetelor care trebuie sortate.
InsertionSortFavorabil:
- compararile si atribuirile sunt in acelasi numar
- ele cresc liniar
=> suma lor creste liniar
InsertionSortDefavorabil:
- compararile si atribuirile au valori foarte mari inca de la un numar mic de elemente,ele crescand
exponential
=> suma lor creste tot exponential
InsertionSortMediu:
- Compararile si atribuirile sunt egale ele exponential odata cu cresterea numarului de elemente
=> suma lor cresteexponential odata cu cresterea numarului de elemente
SelectionSort: 
- indiferent de ce cazuri avem, compararile vor avea valori mari pentru un numar mare de elemente si vor creste exponential,
atribuirile vor avea valori mici si vor creste liniar,iar suma acestora va creste exponential
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAXIMUM 1000
int n=100;
int compb,atb,compi,ati,comps,ats;
#define FIX 5
void generate(int *a,int n)
{
	
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%(MAXIMUM+1);
	}
}
void generateB(int *a,int n)
{
	
	a[0]=rand()%MAXIMUM;
	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]+rand()%MAXIMUM;
	}
}
void generateW(int *a,int n)
{
	
	a[0]=rand()%MAXIMUM;
	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]-rand()%MAXIMUM;
	}
}
void copy(int *a,int *b,int n)
{
	for(int i=0;i<n;i++)
	{
		b[i]=a[i];
	}
}
void afisareValori(int *a,int n)
{
	for( int i=0;i<n;i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n");
}
void selectionSort(int *a,int n)
{
	for(int i=0;i<n;i++)
	{
		int min=i;
		for(int j=i+1;j<n;j++)
		{comps=comps+1;
			if(a[j]<a[min])
			{
				min=j;
			}
		}
		int x=a[i];
		a[i]=a[min];
		a[min]=x;
		ats=ats+1;
	}	
}

void bubbleSort(int *a,int n)
{
	int j=0,x;
	int ok=0;
	do
	{
		ok=1;
		j++;
		for(int i=0;i<n-j;i++)
		{	compb=compb+1;
			if(a[i]>a[i+1])
			{	
				atb=atb+1;
				x=a[i];
				a[i]=a[i+1];
				a[i+1]=x;
				ok=0;
			}
		}
	}while(ok==0);
}
void insertionSort(int *a,int n)
{
	for(int j=1;j<n;j++)
	{
		int x=a[j];
		int i=j-1;
		compi=compi+1;
		while((i>=0) && (a[i]>x))
		{
			compi=compi+1;
			a[i+1]=a[i];
			i=i-1;
			ati=ati+1;
		}
		a[i+1]=x; ati=ati+1;
	}
}

void main()
{	srand(time(NULL));
	FILE *f=fopen("SortariMediu.csv","w");
	FILE *g=fopen("SortariBest.csv","w");
	FILE *h=fopen("SortariWorst.csv","w");
	int *a=(int*)malloc(n*sizeof(int));
		int *b=(int*)malloc(n*sizeof(int));
		int *c=(int*)malloc(n*sizeof(int));
		int *d=(int*)malloc(n*sizeof(int));
	while(n<=10000)
	{
		

		int *a=(int*)malloc(n*sizeof(int));
		int *b=(int*)malloc(n*sizeof(int));
		int *c=(int*)malloc(n*sizeof(int));
		int *d=(int*)malloc(n*sizeof(int));

		//average
		for(int i=0;i<5;i++)
		{
			generate(a,n);
			copy(a,b,n);
			copy(a,c,n);
			copy(a,d,n);
			bubbleSort(b,n);
			selectionSort(c,n);
			insertionSort(d,n);
			printf("%d ",n);
		}
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb/FIX,compi/FIX,comps/FIX,atb/FIX,ati/FIX,ats/FIX,(compb/FIX)+(atb/FIX),(compi/FIX)+(ati/FIX),(comps/FIX)+(ats/FIX));
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		
		
		//best case
		generateB(a,n);
		copy(a,b,n);
		copy(a,c,n);
		copy(a,d,n);
		bubbleSort(b,n);
		selectionSort(c,n);
		insertionSort(d,n);
		fprintf(g,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb,compi,comps,atb,ati,ats,compb+atb,compi+ati,comps+ats);
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;

		//worst
		generateW(a,n);
		copy(a,b,n);
		copy(a,c,n);
		copy(a,d,n);
		bubbleSort(b,n);
		selectionSort(c,n);
		insertionSort(d,n);
		fprintf(h,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb,compi,comps,atb,ati,ats,compb+atb,compi+ati,comps+ats);
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		n=n+100;
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		free(a);
		free(b);
		free(c);
		free(d);
	}
	fclose(f);
	fclose(g);
	fclose(h);
	getchar();
}
