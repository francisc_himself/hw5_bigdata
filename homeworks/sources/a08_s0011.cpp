/*Nume: ***ANONIM*** ***ANONIM***
Grupa: ***GROUP_NUMBER***
Tema: Generarea de componente conexe ale unu graf folosind seturi disjuncte
*/
#include<iostream>
#include<conio.h>
#include <ctime>
#include<fstream>


using namespace std;
ofstream fout("date.csv");
#define V 5 //numarul de varfuri
#define E 11 //numarul de muchii
int apeluri;

struct nod
{
	int ch;
	int rank;
	nod *p;
}*noduri[V];

struct muchie
{
	int u;
	int v;
}*muchii[E];

void link(nod *x, nod *y)
{
	if(x->rank>y->rank)
		y->p=x;
	else
	{
		x->p=y;
		if(x->rank==y->rank)
			y->rank++;
	}
}


nod *find_set( nod*x)
{
	if(x!=x->p)
		x->p=find_set(x->p);
	return x->p;
}

void make_set(nod *x)
{
	x->p=x;
	x->rank=0;
};

void uniune(nod *x, nod*y)
{
	link(find_set(x), find_set(y));
}



void connected_components(int nrMuchii)
{
	for(int i=0;i<V;i++)
	{	
		apeluri++;
		make_set(noduri[i]);
	}

	for(int i=0;i<nrMuchii;i++)
	{ 
		apeluri=apeluri+2;
		if(find_set(noduri[muchii[i]->u])!=find_set( noduri[muchii[i]->v]))
		{
			apeluri=apeluri+2;
			uniune(noduri[muchii[i]->u],noduri[muchii[i]->v]);
		}
	}
	
}

void afisare()
{
	cout<<"componentele conexe sunt: "<<endl;
	for(int i=0;i<V;i++)
	{
		if(noduri[i]->p==noduri[i])
		{
			for (int j=0;j<V;j++)
			{
				if(find_set(noduri[j])==noduri[i])
					cout<<"-"<<noduri[j]->ch;
			}
			cout<<endl;
		}
	}
}



////////////////////DEMO/////////////////
void main()
{
	for (int i=0;i<V;i++)
	{
		noduri[i]=(nod*)malloc(sizeof(nod*));
		noduri[i]->ch=i;
	}

	int aux[V][V];
	int nrM=0;
	srand(time(0));
	for(int i=0;i<V;i++)
		for(int j=0;j<V;j++)
			aux[i][j]=0;
	while(nrM<E)
	{
		muchii[nrM]=(muchie*)malloc(sizeof(muchie*));
		int x=rand()%V;
		int y=rand()%V;
		if(x!=y)
		{
			if(aux[x][y]==0&&aux[y][x]==0)
			{
				muchii[nrM]->u=x;
				muchii[nrM]->v=y;
				aux[x][y]=1;
				aux[y][x]=1;
				nrM++;
			}
		
		}
	}
	
	cout<<"Numarul de varfuri este "<<V<<endl<<"Numarul de muchii este "<<E<<endl;
	cout<<"Muchiile sunt: "<<endl;
	for(int i=0;i<E;i++)
		cout<<"("<<muchii[i]->u<<","<<muchii[i]->v<<")"<<endl;
	connected_components(E);
	afisare();
	getch();
}


///////////////////////////EVALUARE////////////////
/*void main()
{
	fout<<"Nr muchii;apeluri"<<endl;
	for(int nrE=10000;nrE<=60000;nrE=nrE+1000)
	{
		apeluri=0;
		for (int i=0;i<V;i++)
		{
			noduri[i]=(nod*)malloc(sizeof(nod*));
			noduri[i]->ch=i;
		}
		int nrM=0;
		srand(time(0));
		while(nrM<nrE)
		{
			muchii[nrM]=(muchie*)malloc(sizeof(muchie*));
			int x=rand()%V;
			int y=rand()%V;
			if(x!=y)
			{
				muchii[nrM]->u=x;
				muchii[nrM]->v=y;
				nrM++;
			}
		
		}
		connected_components(nrE);
		fout<<nrE<<";"<<apeluri<<endl;
	}
	fout.close();
}*/
