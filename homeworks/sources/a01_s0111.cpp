#include <stdio.h>
#include <conio.h>
#include "Profiler.h"

/*
-The worst sorting method in worst case is bubble sort which pass each time through whole array; 
it has O(n^2) complexity for average and worst case and O(n) for best case with no assignments
-Selection sort's principle is based on selecting as pivot the position of an element form array(beginning with first one) and passing through the rest of it;
if it finds an element smaller than that from the position given by the pivot, 
the swap is made between those two elements and the element swaped will be in the right position for final sort; complexity will be O(n^2) for all 3 cases
-Insertion sort's principle is based on storing successively each element of the array in a buffer and finding 
the first element which is smaller than that from buffer from the part of the array previous to the position of the element chosen; 
if such an element is found right after it a new position is made by shifting the positions of the array to the right by one position, 
where the buffer element is stored; complexity is O(n^2) for average and worst and O(n) for best with no assignments  
*/

Profiler profiler("Sorting");
void bubble(int a[],int n,int *asg1,int *c1) {
	int swap,aux;
	do {
		swap=0;
		for(int i=1;i<n;i++) {
				(*c1)++;
			//	profiler.countOperation("bubble_cmp",n,1);
				if (a[i-1] > a[i]) {
					aux=a[i-1];
					a[i-1]=a[i]; 
					a[i]=aux;
					swap=1;
				//	profiler.countOperation("bubble_asg",n,3);
					(*asg1)=(*asg1)+3;
				}
		}
	}
	while(swap==1);
}


void selection(int a[], int n,int *asg2,int *c2) {
	int pos,aux;
	for (int j=0; j<n-1; j++) {
		pos=j;	
		for (int i=j+1; i<n; i++) {	
			(*c2)++;
			//profiler.countOperation("sel_cmp",n,1);
			if (a[i]<a[pos]) {
				pos = i;
			}
		}
		if ( pos!=j ) {
			aux=a[j]; 
			a[j]=a[pos];
			a[pos]=aux;
			(*asg2)=(*asg2)+3;
			//profiler.countOperation("sel_asg",n,3);
		}
	}
}

void insertion(int a[], int n,int *asg3,int *c3) {
	int buff,j;
	for (int i=1;i<n;i++) {
		buff=a[i];
		(*c3)++;
		//profiler.countOperation("ins_asg",n,1);
		j=i-1;
		//profiler.countOperation("ins_cmp",n,1);
		(*asg3)++;
		while (a[j]>buff && j>0) {
			(*asg3)++;
			(*c3)++;
			a[j+1]=a[j];
			j--;
		}
		a[j+1]=buff;
		(*asg3)++;
		//profiler.countOperation("ins_asg",n,1);
		//profiler.countOperation("ins_cmp",n,1);
	}
}

int main() {
	int n,a[10000],a1[10000],a2[10000],a3[10000],asg1,c1,asg2,c2,asg3,c3,sum1,sum2,sum3,csum1,csum2,csum3;
	
	for (n=100;n<10000;n+=100) {
	//	sum1=sum2=sum3=csum1=csum2=csum3=0;
	//	for(int j=1;j<=5;j++) {
			FillRandomArray(a,n,100,10000,false,1);
			for(int i=0;i<n;i++) {
				a1[i]=a[i];
				a2[i]=a[i];
				a3[i]=a[i];	
			}	
			asg1=asg2=asg3=c1=c2=c3=0;
			bubble(a1,n,&asg1,&c1);
			selection(a2,n,&asg2,&c2);
			insertion(a3,n,&asg3,&c3);
		//sum1+=asg1;
		//sum2+=asg2;
		//sum3+=asg3;
		//csum1+=c1;
		//csum2+=c2;
		//csum3+=c3;
	//	}
		profiler.countOperation("bub_asg",n,asg1);
		profiler.countOperation("ins_asg",n,asg2);
		profiler.countOperation("sel_asg",n,asg3);
		profiler.countOperation("bub_cmp",n,c1);
		profiler.countOperation("ins_cmp",n,c2);
		profiler.countOperation("sel_cmp",n,c3);
	}

//	profiler.addSeries("bub_asg_cmp","bub_asg","bub_cmp");
//	profiler.addSeries("sel_asg_cmp","sel_asg","sel_cmp");
//	profiler.addSeries("ins_asg_cmp","ins_asg","ins_cmp");
//	profiler.createGroup("assignment","bub_asg","sel_asg","ins_asg");
//	profiler.createGroup("comparison","bub_cmp","sel_cmp","ins_cmp");
//	profiler.createGroup("asg_cmp","bub_asg_cmp","sel_asg_cmp","ins_asg_cmp");
	
	profiler.showReport();

return 0;
}