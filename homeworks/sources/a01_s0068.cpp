/**
BubbleSortFavorabil:compararile cresc liniar si nu avem atribuiri indiferent de numarul de elemente si astfel suma celor doua
va fi chiar numarul de comparari;
BubbleSortDefavorabil:atat compararile,cat si atribuirile cresc exponential,astfel si suma lor va creste exponential odata cu
cresterea numarului de elemente;
BubbleSortMediu:compararile,atribuirile si suma acestora au cresc exponential si au valori tot mai ***ANONIM*** cu cat creste numarul
elemenetelor care trebuie sortate.
InsertionSortFavorabil:compararile si atribuirile sunt egale ele crescand liniar,astfel si suma lor crescand liniar
InsertionSortDefavorabil:compararile si atribuirile au valori foarte ***ANONIM*** inca de la un numar mic de elemente,ele crescand
exponential,astfel si suma lor crescand exponential
InsertionSortMediu:Compararile si atribuirile sunt egale ele crescand
crescand exponential odata cu cresterea numarului de elemente,astfel si suma lor crescand exponential odata cu cresterea numarului de elemente
La SelectionSort indiferent de ce cazuri avem compararile vor avea valori ***ANONIM*** pentru un numar mare de elemente si vor creste exponential,
atribuirile vor avea valori mici si vor creste liniar,iar suma acestora va creste exponential

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_RUN 1000
int n=100;
int compb,atb,compi,ati,comps,ats;
#define FIX 5
void inserareValori(int *a,int n)
{
	
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
	//	if(i==0)
		//{
		a[i]=rand()%MAX_RUN;
		//}else
		//{
		//	a[i]=a[i-1]+rand()%MAX_RUN;
	//}
	}
}
void inserareValoriB(int *a,int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
		if(i==0)
		{
			a[i]=rand()%MAX_RUN;
		}
		else
		{
			a[i]=a[i-1]+rand()%MAX_RUN;
		}
	}
}
void inserareValoriW(int *a,int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
			if(i==0)
		{
			a[i]=rand()%MAX_RUN;
		}
		else
		{
			a[i]=a[i-1]-rand()%MAX_RUN;
		}
	}
}
void copiereValori(int *a,int *b,int n)
{
	for(int i=0;i<n;i++)
	{
		b[i]=a[i];
	}
}
void afisareValori(int *a,int n)
{
	for( int i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
	printf("\n");
}
void selectionSort(int *a,int n)
{
	for(int i=0;i<n;i++)
	{
		int min=i;
		for(int j=i+1;j<n;j++)
		{comps=comps+1;
			if(a[j]<a[min])
			{
				min=j;
			}
			
		}
		int x=a[i];
		a[i]=a[min];
		a[min]=x;
		ats=ats+1;
}
}

void bubbleSort(int *a,int n)
{
	int j=0;
	int ok=0;
	do
	{
		ok=1;
		j=j+1;
		
		for(int i=0;i<n-j;i++)
		{	compb=compb+1;
			if(a[i]>a[i+1])
			{	
				
				atb=atb+1;
				int x=a[i];
				a[i]=a[i+1];
				a[i+1]=x;
				ok=0;
			}
}
	}while(ok==0);
}
void insertSort(int *a,int n)
{
	for(int j=1;j<n;j++)
	{
		int x=a[j];
		int i=j-1;
		compi=compi+1;
		while((i>=0) && (a[i]>x))
		{
			compi=compi+1;
			a[i+1]=a[i];
			i=i-1;
			ati=ati+1;
		}
		a[i+1]=x; ati=ati+1;
	}
}

void main()
{	
	FILE *f=fopen("SortariMediu.csv","w");
	FILE *g=fopen("SortariBest.csv","w");
	FILE *h=fopen("SortariWorst.csv","w");
	//int n=10;
//int *a=(int*)malloc(n*sizeof(int));
	//inserareValori(a,n);
	//afisareValori(a,n);
	//bubbleSort(a,n);
	//afisareValori(a,n);
	//selectionSort(a,n);
	//afisareValori(a,n);
//	insertSort(a,n);
	//afisareValori(a,n);
	while(n<=10000)
	{
		int *a=(int*)malloc(n*sizeof(int));
		int *b=(int*)malloc(n*sizeof(int));
		int *c=(int*)malloc(n*sizeof(int));
		int *d=(int*)malloc(n*sizeof(int));
		
		for(int i=0;i<5;i++)
		{
			inserareValori(a,n);
			copiereValori(a,b,n);
			copiereValori(a,c,n);
			copiereValori(a,d,n);
			bubbleSort(b,n);
			selectionSort(c,n);
			insertSort(d,n);
	
		}
		printf("%d ",n);
		//fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb/FIX,atb/FIX,(compb/FIX)+(atb/FIX),compi/FIX,ati/FIX,(compi/FIX)+(ati/FIX),comps/FIX,ats/FIX,(comps/FIX)+(ats/FIX));
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb/FIX,compi/FIX,comps/FIX,atb/FIX,ati/FIX,ats/FIX,(compb/FIX)+(atb/FIX),(compi/FIX)+(ati/FIX),(comps/FIX)+(ats/FIX));
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		inserareValoriB(a,n);
		copiereValori(a,b,n);
		copiereValori(a,c,n);
		copiereValori(a,d,n);
		bubbleSort(b,n);
		selectionSort(c,n);
		insertSort(d,n);
		fprintf(g,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb,compi,comps,atb,ati,ats,compb+atb,compi+ati,comps+ats);
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		inserareValoriW(a,n);
		copiereValori(a,b,n);
		copiereValori(a,c,n);
		copiereValori(a,d,n);
		bubbleSort(b,n);
		selectionSort(c,n);
		insertSort(d,n);
		fprintf(h,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compb,compi,comps,atb,ati,ats,compb+atb,compi+ati,comps+ats);
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		n=n+100;
		atb=0;
		compb=0;
		comps=0;
		ats=0;
		ati=0;
		compi=0;
		free(a);
	}
	fclose(f);
	fclose(g);
	fclose(h);
	//bubbleSort(a,n);
	//afisareValori(a,n);
}
