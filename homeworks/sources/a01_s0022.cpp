/*
Nume: ***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa: ***GROUP_NUMBER***
Tema 1: Metode Directe de Sortare
Discutie: 
		S-au folosit ca si metode de sortare: BubbleSort, Selectie, Insertie. 
		Se poate observa ca in cazul favorabil, metoda de sortare BubbleSort 
	a fost mai eficienta decat metoda Insertiei sau a Selectiei. 
		Numarul de atribuiri si comparatii a metodei BubbleSort este considerabil 
	mai mic decat a ***ANONIM***orlalte metodde, eficienta acestei metode fiind exponentiala. 
		In cazul nefavorabil se obesrva ca Selectia are numarul ***ANONIM*** mai mic de comparatii, 
	dar Insertia are numarul ***ANONIM*** mai mic de atribuiri. 
		In cazul mediu statistic, atat BubbleSort, cat si Insertia s-au dovedit la fel de folositoare, 
	diferenta dintre aceste doua fiind foarte mica.
*/
#include "stdafx.h"
#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "math.h"
#include "time.h"

///Global Variable



void bubbleSort(int a[],int n, double*atrib,double*comp){
	int i=1;
	int j;
	int aux;
	(*atrib)=0;
	(*comp)=0;
	bool sortat = false;
	while(!sortat){
		sortat = true;
		
		for(j=0;j<n-1;j++){
			
			if(a[j]>a[j+1]){
				(*atrib)+=3;
				aux = a[j];
				a[j] = a[j+1];
				a[j+1] = aux;
				sortat = false;
			}
			(*comp)++;
		}
	}
}

void selectie(int v[], int n, double* atrib, double* comp)
{
	int i,***ANONIM***,i***ANONIM***,j;
	for (i=0; i<n-1; i++)
	{
		***ANONIM***=v[i];
		i***ANONIM***=i;
		(*atrib)++;
		for (j=i+1; j<n; j++)
		{
			(*comp)++;
			if (v[j]<***ANONIM***)
			{
				***ANONIM***=v[j];
				i***ANONIM***=j;
				(*atrib)+=1;
			}
		}
		int aux=v[i];
		v[i]=v[i***ANONIM***];
		v[i***ANONIM***]=aux;
		(*atrib)+=3;
	}
}

void insertie(int v[],int n,double*atrib,double*comp){
	int i, j, k, x;
	for(i=1; i<n; i++)
	{
		x=v[i];
		(*atrib)++;
		j=0;
		(*comp)++;
		while(v[j]<x){
			(*comp)++;
			j++;
		}
		(*comp)++;
		for(k=i; k>=j; k--)
		{
			(*atrib)++;
			v[k]=v[k-1];
		}
		v[j]=x;
		(*atrib)++;
	}
}

void generareBest(int a[],int n){
		int i,c;

	a[0] = rand() % 1500;
	for (i=1;i<n;i++)
	{
		c = rand() % 1500;
		while(a[i-1]>c){
			c = rand() % 1500;
		}
		a[i]= c;
	}
}
void generareWorst(int a[],int n){
	int i,c;

	a[0] = rand() % 1500;
	for (i=1;i<n;i++)
	{
		c = rand() % 1500;
		while(a[i-1]<c){
			c = rand() % 1500;
		}
		a[i]= c;
	}
}
void generareAvg(int a[], int n)
{
	int i;
	for (i=0;i<n;i++)
		a[i] = rand() % 1500;
}

void copiere(int a[],int b[],int n){
	int i=0;
	for(i=0;i<n;i++){
		a[i] = b[i];
	}
}


void afisare(int a[],int n){
	printf("\n Sirul este ");
	int i=0;
	for(i=0;i<n;i++){
		printf("%d ",a[i]);
	}
}

int main()
{
    double atrib,comp;
	int a[10000];
	int bubble[10000];
	int select[10000];
	int insert[10000];
	int i=100;
	srand(time(NULL));
	//int n = 5;

	FILE *avg;
	avg = fopen("avg.csv","w");

//Mediu Stastistic
    int j=0;
    double avgA=0,avgC=0;

     fprintf(avg, "n, AtrBubble, CompBubble, SumaBubble, AtrSel, CompSel, SumaSel, AtrIns, CompIns, SumaIns \n");
	generareAvg(a,10000);

	for(i=100;i<10000;i=i+100){
        fprintf(avg,"%d,",i);
        for(j=0;j<5;j++){
            generareAvg(a,10000);
        //bubbleSort
            printf("\nbubbleSort  AVG\n");
            copiere(bubble,a,i);
            bubbleSort(bubble,i,&atrib,&comp);
            avgA=avgA+atrib;
            avgC=avgC+atrib;
            atrib=0;comp=0;
        }
        fprintf(avg,"%lf,%lf,%lf,",avgA/5,avgC/5,avgA/5+avgC/5);

        for(j=0;j<5;j++){
            generareAvg(a,10000);
        //selectie
            printf("\nselectie  AVG\n");
            copiere(select,a,i);
            selectie(select,i,&atrib,&comp);
            avgA=avgA+atrib;
            avgC=avgC+atrib;
            atrib=0;comp=0;
        }
        fprintf(avg,"%lf,%lf,%lf,",avgA/5,avgC/5,avgA/5+avgC/5);
        for(j=0;j<5;j++){
            generareAvg(a,10000);
        //insertie
            printf("\ninsertie  AVG\n");
            copiere(insert,a,i);
            insertie(insert,i,&atrib,&comp);
            
			avgA=avgA+atrib;
            avgC=avgC+atrib;
            
			atrib=0;comp=0;
        }

        fprintf(avg,"%2lf,%2lf,%2lf",avgA/5,avgC/5,avgA/5+avgC/5);

        fprintf(avg,"\n");
	}
	fclose(avg);
//Mediu Favorabil
    FILE *best;
	best = fopen("best.csv","w");

    fprintf(best, "n, AtrBubble, CompBubble, SumaBubble, AtrSel, CompSel, SumaSel, AtrIns, CompIns, SumaIns \n");

	generareBest(a,10000);
	for(i=100;i<10000;i=i+100){
        fprintf(best,"%d,",i);
    	//bubbleSort
        printf("\nbubbleSort  BEST\n");
		copiere(bubble,a,i);
		bubbleSort(bubble,i,&atrib,&comp);
	fprintf(best,"%lf,%lf,%lf,",atrib,comp,atrib+comp);
	atrib=0;comp=0;
    	//selectie
		printf("\nselectie  BEST\n");
		copiere(select,a,i);
		selectie(select,i,&atrib,&comp);
	fprintf(best,"%lf,%lf,%lf,",atrib,comp,atrib+comp);
	atrib=0;comp=0;
	//insertie
        printf("\ninsertie  BEST\n");
		copiere(insert,a,i);
		insertie(insert,i,&atrib,&comp);
	fprintf(best,"%lf,%lf,%lf",atrib,comp,atrib+comp);
	atrib=0;comp=0;
        fprintf(best,"\n");
	}
	fclose(best);
//Mediu Nefavorabil
    FILE *worst;
	worst = fopen("worst.csv","w");

     fprintf(worst, "n, AtrBubble, CompBubble, SumaBubble, AtrSel, CompSel, SumaSel, AtrIns, CompIns, SumaIns \n");
	generareWorst(a,10000);
	for(i=100;i<10000;i=i+100){
	    fprintf(worst,"%d,",i);
    	//bubbleSort
        printf("\nbubbleSort  WORST\n");
		copiere(bubble,a,i);
		bubbleSort(bubble,i,&atrib,&comp);
	fprintf(worst,"%lf,%lf,%lf,",atrib,comp,atrib+comp);
	atrib=0;comp=0;
    	//selectie
		printf("\nselectie  WORST\n");
		copiere(select,a,i);
		selectie(select,i,&atrib,&comp);
	fprintf(worst,"%lf,%lf,%lf,",atrib,comp,atrib+comp);
	atrib=0;comp=0;
	//insertie
        printf("\ninsertie  WORST\n");
		copiere(insert,a,i);
		insertie(insert,i,&atrib,&comp);
	fprintf(worst,"%lf,%lf,%lf",atrib,comp,atrib+comp);
	atrib=0;comp=0;
        fprintf(worst,"\n");
	}
	fclose(worst);

	return 0;
}
