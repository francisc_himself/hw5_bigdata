/**
Assignment No. 3: Analysis & Comparison of Advanced Sorting Methods � Heapsort and Quicksort

Name: ***ANONIM*** ***ANONIM*** Mihai
Group: ***GROUP_NUMBER***


Implementation:

You are required to implement correctly and efficiently the Heapsort and Quicksort advanced sorting methods.

You may find any necessary information and pseudo-code in your course notes, or in the book1:
	Heapsort: chapter 6 (Heapsort)
	Quicksort: chapter 7 (Quicksort)

	Evaluate Quicksort in the best and worst cases also � total number of operations. 
	Compare the performance of Quicksort in the three analysis cases. Interpret the results.
	
Start date: 22.03.2013
Finish date: 26.03.2013

Conclusions:

Comparing the Heap Sort and the Quick Sort: both algorithms have a "n log n" complexity in number of operations performed. So, in the average case, 
it can be seen on the generated graphics that the two algorithms are behaving in very efficient ways(comparing to other sorting algorithms), but even
though, Quick Sort is considerably better than the Heap Sort.

Comparing the three performance cases of the Quick Sort, one can observe that Best Case is behaving similarly to Average Case, and Worst Case is behaving a little worse, obviously.
For obtaining average case, one should generate a random input of data, and the pivot can be taken in more than one way. For Best Case, the input data sequence should be sorted,
for example ascending, and the pivot should be taken right in the middle - n/2. Worst Case implementation is similar to the Best case's, with the exception that the pivot
should be taken right at the beginning og the input sequence, or at the end of it.


**/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>


#define MAX 1000
#define MIN 100
#define STARTER 100
#define ENDER 10000


/**
 ** Code for the Heap Sort algorithm.
 **
 **/

void generate(int* a,int n){
	int i=0;
	for (i=0;i<n;i++) {
		a[i]=rand()%(MAX-MIN)+MIN;
	}
}
void generateBest(int* a,int n){
	int i=0;
	for (i=0;i<n;i++) {
		a[i]=i;
	}
}
void generateWorst(int* a,int n){
	int i=0;
	for (i=0;i<n;i++) {
		a[i]=n-i+1;
	}
}

int Left(int i) {
	return 2*i+1;
}

int Right(int i) {
	return 2*i+2;
}

void MaxHeapify(int *x, int n, int k, int* opNr) {
	int l=Left(k);
	int r=Right(k);
	int largest=k;
	int aux;
	(*opNr)++;
	if (l<=n && x[l]>x[k]) {
		largest=l;
	}
	else {
		largest=k;
	}
	(*opNr)++;
	if (r<=n && x[r]>x[largest]) {
		largest=r;
	}

	if (largest != k) {
		aux=x[k];
		x[k]=x[largest];
		x[largest]=aux;
		(*opNr)++;(*opNr)++;(*opNr)++;
		MaxHeapify(x,n,largest, opNr);
	}
}

void BuildHeap(int *x,int n) {
	int i;
	int q;
	for (i=n/2;i>=0;i--) {
		MaxHeapify(x,n,i,&q);
	}
}

int HeapSort(int* x, int n, int *opNr)
{	
	BuildHeap(x,n);
	int heapSize=n-1;
	int aux;
	int i;
	for (i=n-1;i>0;i--) {
		aux=x[0];
		x[0]=x[i];
		x[i]=aux;
		(*opNr)++;(*opNr)++;(*opNr)++;
		heapSize--;
		MaxHeapify(x,heapSize,0,opNr);		
	}
	return 0;
}
/**
 ** Code for the Quick Sort algorithm.
 **
 **/
 int c,a;
 void generate_avg(int *v, int *vcopy, int n)
{
	int a,b,c,d,e;
	for (int i = 1; i <= n; i++)
	{
		a = rand();
		b = rand();
		c = rand();
		d = rand();
		e = rand();
		v[i] = (a+b+c+d+e)/5;
		vcopy[i] = v[i];
	}
}


int partition(int *v, int p, int r)
{
	int x = v[p];
	a++;
	int i = p-1;
	int j = r+1;
	int aux;
	while (i<j)
	{
		do
		{
			j--;
			c++;
		}
		while (v[j] > x);

		do
		{
			i++;
			c++;
		}
		while (v[i] < x);

		if (i < j)
		{
			aux = v[i];
			v[i] = v[j];
			v[j] = aux;
			a+=3;
		}
		else return j;
	}

}


void quick(int *v, int p, int r)
{
	int q;
	if (p<r)
	{
		q = partition(v,p,r);
		quick(v,p,q);
		quick(v,q+1,r);
	}
}

void quick_best(int *v, int p, int r)
{
	int q = r/2;
	if (p<r)
	{
		q = partition(v,p,r);
		quick_best(v,p,q);
		quick_best(v,q+1,r);
	}
}

void quick_worst(int *v, int p, int r)
{
	int q = r;
	if (p<r)
	{
		q = partition(v,p,r);
		quick_worst(v,p,q);
		quick_worst(v,q+1,r);
	}
}

void quick_sort(int *v, int n)
{

	quick(v,0,n-1);

}

void quick_sort_best(int *v, int n)
{

	quick_best(v,0,n-1);

}

void quick_sort_worst(int *v, int n)
{

	quick_worst(v,0,n-1);

}

int main()
{	

/**
 ** Code for testing Heap Sort algorithm.
 **
 **/
	
	int op = 0;
	int n=5;
	int x[5];
	x[0]=1;
	x[1]=7;
	x[2]=3;
	x[3]=9;
	x[4]=0;

	printf("\nUnsorted values:\n\n");
	for (int i=0;i<5;i++) 
	{
		printf("%d ", x[i]); 
	}
	
	HeapSort(x,n,&op);
	
	printf("\n\nDemo Heap Sort:\n");
	printf("\n");
	for (int i=0; i<5; i++) 
	{
		printf("%d ", x[i]); 
	}

	FILE* fa;
	if ((fa=fopen("heap***ANONIM***.csv","w"))==0) {printf("opening output file failed.");}
	int opBest=0;
	int opNorm=0;
	int opWorst=0;

	
	for (n=STARTER;n<ENDER;n+=100) {
		int* x=(int*)malloc(n*sizeof(int));
		
		generateBest(x,n);
		//printf("Sorting BEST for n=%d\n",n);
		opBest=0;
		HeapSort(x,n,&opBest);
		
		//printf("\n%d",opBest);
		
		generate(x,n);
		//printf("Sorting AVG for n=%d\n",n);
		opNorm=0;
		HeapSort(x,n,&opNorm);
		//printf("\n%d",opNorm);
		
		
		generateWorst(x,n);
		//printf("Sorting WORST for n=%d\n",n);
		opWorst=0;
		HeapSort(x,n,&opWorst);
		//printf("\n%d",opWorst);
		
		
		
		fprintf(fa,"%d,%d,%d,%d\n",n,opBest,opNorm,opWorst);
		
		free(x);
	}
	printf("\n\nCalculating number of operations for HeapSort in Best Case done.");
	printf("\nCalculating number of operations for HeapSort in Average Case done.");
	printf("\nCalculating number of operations for HeapSort in Worst Case done.");
	printf("\nDone with Heap Sort.\n\n");
	fclose(fa);
	
/**
 ** Code for testing Quick Sort algorithm.
 **
 **/

	int m=5;
	int z[5];
	z[0]=13;
	z[1]=72;
	z[2]=3;
	z[3]=93;
	z[4]=23;

	printf("\nUnsorted values:\n\n");
	for (int i=0;i<5;i++) 
	{
		printf("%d ", z[i]); 
	}
	
	quick_sort(z,m);
 	printf("\n\nDemo Quick Sort:\n");
 
	printf("\n");
	for (int i=0; i<5; i++) 
	{
		printf("%d ", z[i]); 
	}
	
	
	
	
	
	int *v,*vcopy;
	
	FILE *fb,*fc,*fd;
	fb = fopen("quick***ANONIM***.csv","w");
	fc = fopen("quickBest.csv","w");
	fd = fopen("quickWorst.csv","w");
	
	srand(time(NULL));

	for (int n = 100; n <= 10000; n+=100)
	{
		v = new int[n+1];
		vcopy = new int[n+1];
	
		a = 0; c = 0;
		generate_avg(v,vcopy,n);
		quick_sort(vcopy,n);
		
		fprintf(fb,"%d,%d,%d,",a,c,a+c);
		fprintf(fb,"%s","\n");
		
		a = 0; c = 0;
		generate_avg(v,vcopy,n);
		quick_sort_best(vcopy,n);
		
		fprintf(fc,"%d,%d,%d,",a,c,a+c);
		fprintf(fc,"%s","\n");
		
		a = 0; c = 0;
		generate_avg(v,vcopy,n);
		quick_sort_worst(vcopy,n);
		
		fprintf(fd,"%d,%d,%d,",a,c,a+c);
		fprintf(fd,"%s","\n");
		
	}	
	printf("\n\nCalculating number of operations for Quick Sort in Best Case done.");
	printf("\nCalculating number of operations for Quick Sort in Average Case done.");
	printf("\nCalculating number of operations for Quick Sort in Worst Case done.");
	printf("\nDone with Quick Sort.\n\n");
	fclose(fb);

	getch();
	return 0;
	
}