#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include<time.h>
#define V 10000
/*
->
set the number of vertices V = 10000 
and vary the number of edges E from 10000 to 60000; in each case, generate random 
graphs, apply the connected components algorithm and count the number of calls to 
Make-Set(x), Union(x, y) and Find-Set(x) performed in the algorithm. 
Generate a chart which shows how the number of operations varies with E.
->
we represent sets by rooted trees, with each node containing one member and each tree representing one set
		-each member points only to its parent
		-2 heuristics��union by rank� and �path compression��we can achieve an asymptotically optimal disjoint-set data structure
Union by rank: For each node, we maintain a rank, which is an upper bound on the height of the node. In union by rank, we
	make the root with smaller rank point to the root with larger rank during a UNION operation.
->
Path compression: we use it during FIND-SET operations to make each node on the find path point directly to the root.
						Path compression does not change any ranks.

	When we use both union by rank and path compression, the worst-case running time is O(m*alpha(n)), where alpha(n) 
	is a very slowly growing function; we can view the running time as linear in m in all practical situations.
*/
long op;
typedef struct node{
	int key;
	int rank;//number of edges in the longest simple path between x and a descendant leaf
	node *parent;
}nodeG;

typedef struct edge{
	int nod1;
	int nod2;
}edgeG;
//creats a singleton set
nodeG* Make_Set(int i)
{
	op++;
	nodeG *p = new nodeG;
	p->key = i;
	p->rank= 0;
	p ->parent = p;
	return p;
}
//returns a pointer to the representative of the set containing i
nodeG* Find_Set(nodeG *i){

	op++;
	if(i != i->parent){
		i->parent = Find_Set(i->parent);//1st pass finds the root and second update each node to point to the root
	}
	return i->parent;
}
//links nodes i and j
void Link(nodeG *i, nodeG *j){
	
	if(i->rank > j-> rank)//rank grater
		j->parent = i;
	else 
		if(i->rank < j->rank)//rank lower
			i->parent = j;
		else
			if(i->rank == j->rank)//rank equal
			{
				i->parent = j;
				j->rank = j->rank + 1;
	}
}
//makes the union of the two sets containing i and j
void Union_Set(nodeG *i, nodeG *j){
	Link(Find_Set(i),Find_Set(j));
}

bool Same_Component(nodeG* i, nodeG* j)
{
	if (Find_Set(i) == Find_Set(j))
			return true;
		else 
			return false;
}
//fisrt each vertex is in its own set then for each edge (u,v) it makes union the sets contaning u and v
void Connected_Components(int ed)
{
	nodeG *Graph[V];
	int a,b;

		edgeG *edges = new edgeG[ed];
		
		for(int i=0;i<V;i++){
			Graph[i] = Make_Set(i);
		}

		for(int i=0;i<ed;i++){

			edges[i].nod1 = rand()%V;
			edges[i].nod2 = rand()%V;
		}

		for(int i=0; i<ed; i++){
			a= edges[i].nod1;
			b= edges[i].nod2;
			nodeG *x = Graph[a];
			nodeG *y = Graph[b];
			if(Same_Component(x,y)==false)
				Union_Set(x,y);
		}	
}


int  main(){
	srand(time(NULL));

	/*
	nodeG *Graph[10];
	edgeG *edg = new edgeG[6];
	int a,b;

	for(int i=1;i<=10;i++){
		Graph[i] = Make_Set(i);
    }
	
	for(int i=1;i<=6;i++)
	{
		edg[i].nod1=rand()%10+1;
		edg[i].nod2=rand()%10+1;
	}
    
	op=0;
	printf("\n");
	for(int i=1; i<=6; i++){
		a= edg[i].nod1;
		b= edg[i].nod2;
		printf("%d %d \n",a,b);
		nodeG *x = Graph[a];
		nodeG *y = Graph[b];
		if(Same_Component(x,y)==false)
		        Union_Set(x,y);
	}



	for(int i=1; i<=10; i++){
		printf("%d %d  ",i,Find_Set(Graph[i])->key);
	}
	*/
	op=0;
	FILE *f = fopen("DisjointSets.txt","w");
	for(int ed=10000;ed<=60000;ed=ed+500){
		op=0;
		Connected_Components(ed);
		fprintf(f,"%ld %ld\n",ed,op);
	}
	
	fclose(f);
  // getch();
	return 0;
}