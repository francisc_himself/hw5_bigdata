/**
	Student:***ANONIM*** Diana ***ANONIM***
	Grupa:***GROUP_NUMBER***
	Problem specification: BFS - Breadth-First Search
	Comments:
	
**/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

//1 - white
//2 - gray
//3 - black
typedef struct edge{
	int p;
	int d;
}edge;


typedef struct nod{
	nod* next;
	int value;
	int color;
	nod* predecessor;
	int distance;
}nod;


edge e[9001];//e[5001];
nod* start[201];
nod* end[201];
int nr_edges,nr_vertices;

bool ok=true;
int sizeQ=0;
int Q[201];
int tailQ,headQ;
int op=0;
void enqueue(nod*s)
{
	op++;
	Q[tailQ]=s->value;
	if (tailQ==sizeQ-1)
		tailQ=1;
	else tailQ++;
}
nod* dequeue()
{
	op++;
	int x=Q[headQ];
	if (headQ==sizeQ-1)
		headQ=1;
	else headQ++;
	return start[x];
}
void change_color(int x)
{
	for (int k=0;k<nr_vertices;k++)
	{
		nod* a=start[k];
		while (a!=NULL)
		{
			if (a->value==x)
					a->color=2;
					a=a->next;
		}
	}
}
void BFS(nod* s)
{
	op+=3;
	s->color=2;
	s->distance=0;
	s->predecessor=NULL;
	//Q=null
	sizeQ=nr_vertices;
	headQ=0;
	tailQ=0;
	enqueue(s);
	ok=false;
	while ((tailQ!=headQ)||(ok==false))
	{
		ok=true;
		op++;
		nod* u=dequeue();
		nod* aux=start[u->value]->next;op++;
		while (aux!=NULL)
		{
			op++;
			if (aux->color==1)
			{
				op+=3;
				aux->color=2;
				aux->distance=u->distance+1;
				aux->predecessor=u;
				change_color(aux->value);
				enqueue(aux);
			}
			aux=aux->next;
			op++;
		}
		u->color=3;op++;
		change_color(u->value);
		printf("%d ",u->value);
	}
	op++;
}

int generate_random(int x) 
{
	return  rand()%x;
}
void generate_graph(int nr_vertices,int nr_edges)
{
	int i=0;
	for (i=0;i<nr_vertices;i++)
	{
		start[i]=(nod*)malloc(sizeof(nod));
		start[i]->next=NULL;
		start[i]->value=i;
		end[i]=start[i];
	}
	i=0;
	while(i<nr_edges)
	{	
		int nr1 =generate_random(nr_vertices);
		int nr2 =generate_random(nr_vertices);
		if((nr1!=nr2))
		{
			bool ok=false;
			for (int j=0;j<i;j++)
			{
				if ((e[j].p==nr1)&&(e[j].d==nr2))//||((e[j].p==nr2)&&(e[j].d==nr1)))
				{
					ok=true;
				}
			}
			if (ok==false)
			{	
				e[i].p=nr1;
				e[i].d=nr2;	
				printf("%d-%d\n",e[i].p,e[i].d);
				nod* aux=(nod*)malloc(sizeof(nod));
				aux->next=NULL;
				aux->value=e[i].d;
				end[e[i].p]->next=aux;
				end[e[i].p]=aux;
				i++;
			}
		}
	}

}

void prettyprint(int i, int depth, int dep, int s)
{
	if (s<sizeQ)
	{
		for (int j=0;j<depth;j++)
			printf("    "); 
		
		printf("%d", Q[i]);
		printf("\n");
		if(Q[i+1] != NULL)
		if( start[Q[i+1]]->distance > dep )
			prettyprint(i+1,depth+1,start[Q[i+1]]->distance,s+1);
		else 
			prettyprint(i+1,depth,start[Q[i+1]]->distance,s+1);
	}
}

void print_adj_list(int nr_vertices)
{
	for (int i=0;i<nr_vertices;i++)
	{
		nod* aux=start[i];
		while (aux!=NULL)
		{
			if (aux==start[i])
				printf("%d -> ",aux->value);
			else printf("%d ",aux->value);
			aux=aux->next;
		}
		printf("\n");
	}
}
void test()
{
	nr_edges=7;
	nr_vertices=5;
	generate_graph(nr_vertices,nr_edges);
	print_adj_list(nr_vertices);
	
	for (int i=0;i<nr_vertices;i++)
	{
		op+=4;
		start[i]->color=1;
		start[i]->distance=INT_MAX;
		start[i]->predecessor=NULL;
		nod* o=start[i]->next;
		while(o!=NULL)
		{
			op+=4;
			o->color=1;
			o->distance=INT_MAX;
			o->predecessor=NULL;
			o=o->next;
		}
	}
	for (int i=0;i<nr_vertices;i++)
	{
		if (start[i]->color==1)
		{
			printf("\nBFS din %d:",i);
			BFS(start[i]);
			printf("\nPrettyPrint:\n");
			prettyprint(0,0,0,0);
			for (int j=0;j<sizeQ;j++)
				Q[j]=0;
		}
	}
	getch();
}

int main()
{
	srand(time(NULL));
	test();
	/*
	//evaluation 1
	FILE *pf1;
	pf1=fopen("BFS1.csv","w");
	fprintf(pf1,"nr_edges,nr_op\n");
	nr_vertices=100; 
	for (nr_edges=1000;nr_edges<5000;nr_edges+=100)
	{
		op=0;
		generate_graph(nr_vertices,nr_edges);	
		
		for (int i=0;i<nr_vertices;i++)
		{
			op+=4;
			start[i]->color=1;
			start[i]->distance=INT_MAX;
			start[i]->predecessor=NULL;
			nod* o=start[i]->next;
			while(o!=NULL)
			{
				op+=4;
				o->color=1;
				o->distance=INT_MAX;
				o->predecessor=NULL;
				o=o->next;
			}
		}
		for (int i=0;i<nr_vertices;i++)
		{
			if (start[i]->color==1)
			{
				BFS(start[i]);
				fprintf(pf1,"%d,%d\n",nr_edges,op);
			}
		}
	}
	fclose(pf1);
	/*
	
	//evaluation 2
	FILE *pf2;
	pf2=fopen("BFS2.csv","w");
	fprintf(pf2,"nr_vertices,nr_op\n");
	nr_edges=9000;
	for(nr_vertices=100;nr_vertices<=200;nr_vertices+=10)
	{
		op=0;
		generate_graph(nr_vertices,nr_edges);
		for (int i=0;i<nr_vertices;i++)
		{
			op+=4;
			start[i]->color=1;
			start[i]->distance=INT_MAX;
			start[i]->predecessor=NULL;
			nod* o=start[i]->next;
			while(o!=NULL)
			{
				op+=4;
				o->color=1;
				o->distance=INT_MAX;
				o->predecessor=NULL;
				o=o->next;
			}
		}
		for (int i=0;i<nr_vertices;i++)
		{
			if (start[i]->color==1)
			{
				BFS(start[i]);
				fprintf(pf2,"%d,%d\n",nr_vertices,op);
			}
		}
		
	}
	fclose(pf2);	*/
}