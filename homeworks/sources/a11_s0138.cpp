/*
	Complexitatea acestui algoritm este identica cu cea de la BFS O(vf+muchii).Am ales sa folosesc Profiler-ul deoarece m-am obisnuit cu el si mi
	se pare mult mai usor sa generez grafice in acest mod.
*/


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include "Profiler.h"


Profiler profiler("DFS");


struct AdjacentNod{//lista de adiacenta pentru un nod
	int neighbour; 
	int edgetype; //tipul de muchie(inapoi,transversala,inainte)
	struct AdjacentNod *next;
}AdjacentNods;

AdjacentNod **vertices;
AdjacentNod *topological,*go;
int color[200];//culoarea nodurilor
int parent[200];//parintele fiecarui nod
int d[200],f[200];//momentele in care un anumit nod nedive gri,respectiv negru
int timed,top;//timpul dintre Td si Tf,respectiv top-verifica daca e topologic sau nu
int edges[10000];//muchiile grafului
int n,e;//numarul de noduri,respectiv muchii


void creareVect(int n, int m){ //functia de creeare a muchiilor
	int i,j;
	int found;//marcheaza daca s-a construit un graf valid
	AdjacentNod *p,*newn;
		
	topological = (AdjacentNod*)malloc(sizeof(AdjacentNod));//construirea listei de adiacenta topologica
	go = topological;
	go->next = NULL;
	go->neighbour = 0;
	vertices = (AdjacentNod**)malloc(sizeof(AdjacentNod)*(200));//se construiesc listele de adiacenta pentru varfuri
	for(i = 0; i < n; i++){
		vertices[i] = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		vertices[i]->neighbour = 0;
		vertices[i]->edgetype = -1;
		vertices[i]->next = NULL;	
	}
	for (i=0; i<m; i+=2){ //se construiesc muchiile astfel incat sa fie diferite doua cate doua
		
		do{	
			found = 0;
			edges[i]=rand() % (n-2);
			edges[i+1]=rand() % (n-2);
			while(edges[i+1]!=edges[i]){//se verifica daca extremitatile unei muchii sunt sau nu identice
				edges[i+1]=rand() % (n-2);			
			}
			for(j = 0; j<i; j+=2){
				if ((edges[i]==edges[j] && edges[i+1]==edges[j+1])){ // se compara muchiile doua cate doua
					found = 1;
				}				
			}			
		}while(found == 1);
		p = vertices[edges[i]];//p se pozitioneaza pe lista de adiacenta a nodului i
		while(p->next!=NULL){//se traverseaza aceasta lista pana la capat
			p = p->next;
		}
		newn = (AdjacentNod*)malloc(sizeof(AdjacentNod)); //newnod se pozitioneaza pe lista de adiacenta a vf muchii[i+1],adica a doua extremitate a muchiei orientate ij
		newn->next = NULL;
		newn->neighbour = 0;
		newn->edgetype = -1;
		p->next = newn;
		p->neighbour = edges[i+1];//se fixeaza i+1 ca vecin al nodului i(in lista p)
		
	}
}


void creareTestVect(int n, int m){//functia de test DFS pt n si m relativ mici
	int i,j;
	int edges[200] = {//se construieste un graf de test
		1,2,
		1,3,
		2,3,
		3,4,
		1,5,
		5,6,
		6,7,
		5,8,
		10,9	
	};
	
	AdjacentNod *p,*newn;
		
	topological = (AdjacentNod*)malloc(sizeof(AdjacentNod));
	go = topological;
	go->next = NULL;
	go->neighbour = 0;
	vertices = (AdjacentNod**)malloc(sizeof(AdjacentNod)*(200));
	for(i = 0; i < n; i++){
		vertices[i] = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		vertices[i]->neighbour = 0;
		vertices[i]->edgetype = -1;
		vertices[i]->next = NULL;	
	}
	for (i=0; i<m; i+=2){
		
		p = vertices[edges[i]];
		while(p->next!=NULL){
			p = p->next;
		}
		newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		newn->next = NULL;
		newn->neighbour = 0;
		newn->edgetype = -1;
		p->next = newn;
		p->neighbour = edges[i+1];
		
	}
}

void DFS_VISIT(int nod,int a){ //functia care construieste un arbore DFS din graful dat
	AdjacentNod *v,*newn;
	color[nod] = 1;//culoarea devine gri
	timed++;//creste timpul
	d[nod] = timed;//Td devine nenul
	v = vertices[nod];//v primeste lista de adiacenta a nodului nod
	if(a==0){//a verifica daca suntem in cazul nr_vf=constant sau nr_muchii=constant
			profiler.countOperation("noperations",n,3);
		}
		else{
			profiler.countOperation("eoperations",e,3);
		}
	while (v->next != NULL){//se verifica daca mai avem vecini ai lui v nevizitati
		if(color[v->neighbour] == 0){//daca culoarea nodului e alba
			v->edgetype = 0;   //muchie obisnuita
			newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
			newn->next = NULL;
			newn->neighbour = 0;
			go->neighbour = v->neighbour;//in arbore se adauga noua muchie(vecinul nodului curent)
			go->next = newn;
			go = go->next;
			parent[v->neighbour] = nod;//aceasta muchie care ca parinte nodul curent
			DFS_VISIT(v->neighbour,a);//se apeleaza functia recursiva DFS_VISIT pentru a cauta in adancime in continuare
			if(a==0){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
		}
		else{
			if(color[v->neighbour] == 1){//daca nodul a fost deja vizitat,se merge inapoi
				v->edgetype = 1;   //inapoi
				top = 0;
			}
			else{
				if(d[nod]>f[v->neighbour]){//daca Td al nodului nod depaseste Tf al nodului curent(v)
					v->edgetype = 2; //transversala
				}
				else{
					v->edgetype = 3; //inainte;
				}
			}
		}
		v = v->next;//ne pozitionam pe urmatorul nod din lista de adiacenta
		if(a==0){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
		
	}
	color[nod] = 2;//dupa parcuregerea listei,nodul nod devine negru
	timed++;//timpul creste din nou
	f[nod] = timed;//Tf devine nenul
	if(a==0){
			profiler.countOperation("noperations",n,3);
		}
		else{
			profiler.countOperation("eoperations",e,3);
		}
}

void DFS(int a){//functia care parcurge toate nodurile grafului,construind padurea de arbori,a este variabila care semnaleaza ca suntem in cazul
	//nr_noduri variabile,respectiv in cazul nr_muchii variabile
	AdjacentNod *newn;
	int i;
	for(i = 0; i < n; i++){//se construiesc eventualele radacini ale arborilor
		color[i]=0;
		parent[i]=-1;
		if(a==0){
			profiler.countOperation("noperations",n,2);
		}
		else{
			profiler.countOperation("eoperations",e,2);
		}
	}
	top = 1;//se presupune ca suntem in cazul topologic
	timed = 0;//setam timpul initial
	if(a==0){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
	for(i = 0; i < n; i++){
		if(a==0){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
		if(color[i] == 0){//in cazul in care avem un nod alb
			newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
			newn->next = NULL;
			newn->neighbour = 0;
			go->neighbour = i;
			go->next = newn;
			go = go->next;			//il adaugam ca vecin al nodului newn
			DFS_VISIT(i,a);			//se viziteaza in adancime pornind de la nodul i
		}
	}
}

void printTopologicSort(){ //sortare si printare topologica
	if(top == 0){
		printf("nu se poate sorta topologic\n");
	}
	else{
		printf("sortarea topologica este:\n");
		go = topological;
		while (go->next!=NULL){
			printf("%d ",go->neighbour);
			go = go->next;
		}
	}
}

int main(){
	int i;
	//n = 11;//n si e pentru cazul de test
	//e = 9;
	//creareTestVect(n,e*2);
	//DFS();
	//printTopologicSort();
	//getch();

	n = 110;//e variabil,n fixat
	for (e = 1000; e < 5000; e+=100){
		printf("e %d",e);
		creareVect(n,2*e);
		for(i = 0; i < n; i++){
			color[i] = 0;
			parent[i] = -1;
		}
				DFS(1);
		
		
	}	
	
	e = 5000;//n variabil,e fixat
	for(n = 110; n <= 200; n += 10){
		printf("n %d",n);
		creareVect(n,2*e);
		for(i = 0; i < n; i++){
			color[i] = 0;
			parent[i] = -1;
		}
				DFS(0);
	}
	profiler.showReport();//afisarea graficului cu operatiile calculate

}