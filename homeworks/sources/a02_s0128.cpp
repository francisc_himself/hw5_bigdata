#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 2 - Heap building algorithms
/***************************************************/


/*****************************************************************************************
/
/								OBSERVATIONS, INTERPRETATIONS
/
/	1. When proving correctness, one can observ that the two algorithms don't always
/	   build the same heap, but both of them have the heap property.
/	2. I only counted both the operations in the same counter, total operations.
/	3. The graphs are plotted on the same chart and they are visible.
/	4. As efficiency, we can see that top-down heap building algorithm requires more
/	   operations as the bottom-up version.
/	5. Both algorithms have O(n log n) running time.
/	6. Bottom-up heap build:
/		Pros: - more efficient than top-down build
/		Cons: - fixed array length
/	7. Top-down heap build:
/		Pros: - elements can be inserted and removed any time (array length not fixed)
/		Cons: - less efficient than bottom-up build
/****************************************************************************************/

#define MAX_NR 10000
#define inf 999999
#define left(node) (2*node)
#define right(node) (2*node+1)
#define parent(node) (node/2)

#define SWAP(x,y) int t;t=x;x=y;y=t;

Profiler profiler("HeapBuildTest");

int a[MAX_NR];
int heap[MAX_NR+1];
//int size;
int size = 10;  //used for proving correctness
int heapSize = 0;

void heapify(int node)
{
	int largest = 0;
	int left = left(node);
	int right = right(node);
	profiler.countOperation("bottomUpTotal",size,1);
	if((left <= size) && (heap[left] > heap[node]))
	{
		largest = left;
	}
	else
	{
		largest = node;
	}
	profiler.countOperation("bottomUpTotal",size,1);
	if((right <= size) && (heap[right] > heap[largest]))
	{
		largest = right;
	}
	if (largest != node)
	{
		profiler.countOperation("bottomUpTotal",size,3);
		SWAP(heap[node],heap[largest]);
		heapify(largest);
	}
}

void bottomup()
{
	//copy array in the "heap"
	for(int i = 0; i < size; i++)
	{
		heap[i+1] = a[i];
	}
	heapSize = size;
	//build the heap
	for(int i = size/2; i >= 1; i--)
	{
		heapify(i);
	}
}

//top-down functions;
int pop()
{
	int max = 0;
	if (heapSize < 1)
	{
		printf("Error. Heap underflow.\n");
		return inf;
	}
	max = heap[1];
	heap[1] = heap[heapSize];
	heapSize--;
	heapify(1);
	return max;
}

void increaseKey(int node, int key)
{
	profiler.countOperation("topDownTotal",size,1);
	while((node > 1) && (heap[parent(node)] < key))
	{
		heap[node] = heap[parent(node)];
		node = parent(node);
		profiler.countOperation("topDownTotal",size,2);
	}
	profiler.countOperation("topDownTotal",size,1);
	heap[node] = key;
	return;
}

void heapInsert(int key)
{
	heapSize++;
	profiler.countOperation("topDownTotal",size,1);
	heap[heapSize] = -inf;
	increaseKey(heapSize,key);
}

void topdown()
{
	//make the "heap" elements 0 (mostly used in proving correctness)
	for(int i = 1; i <= size; i++)
	{
		heap[i] = 0;
	}
	heapSize = 1;
	profiler.countOperation("topDownTotal",size,1);
	heap[1] = a[0];
	for(int i = 1; i < size; i++)
	{
		heapInsert(a[i]);
	}
}

void printHeap(int node, int level)
{
    if (node <= size && node > 0)
	{
        printHeap(right(node), level + 1 );
        for (int i = 0; i <= level; i++ )
            printf( "       " ); /* for nice listing */
        printf( "%d \n", heap[node]);
        printHeap(left(node), level + 1 );
    }
}

void main()
{

	//Hard-coded part, for proving correctness

	a[0] = 1; a[1] = 15; a[2] = 2; a[3] = 4; a[4] = 8; a[5] = 9; a[6] = -2;
	a[7] = 5; a[8] = -6; a[9] = 10;
	for(int i = 0; i < size; i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n\n\n");
	bottomup();
	printf("Heap built with bottom-up algorithm\n\n");
	printHeap(1,1);
	printf("\n\n\n");
	topdown();
	printf("Heap built with top-down algorithm\n\n");
	printHeap(1,1);
	printf("Press any key to exit\n");
	getch();

	/*
	// TESTING AVERAGE CASE
	for(int m = 1; m <= 5; m++)
	{
		for(size = 100; size <= MAX_NR; size+=100)
		{
			FillRandomArray(a,size, -1000, 5000,false,0);
			bottomup();
			topdown();
			printf("%d\n",size);
		}
	}
	profiler.createGroup("totalOperations","bottomUpTotal","topDownTotal");
	profiler.showReport();
	*/
	return;
}