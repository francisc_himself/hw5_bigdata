/******************************************************************************************

	This program generates 3 files with data for comparing 3 different sorting algorithms.
	Output files contain data about the best case, worst case and an average of 5 tests
	for the average case of the following sorting algorithms:
		-Bubble Sort
		-Insertion Sort
		-Selection Sort
	Comparisons for the best and worst case are pretty straight forward, it is to be 
	mentioned though that for the test to be thorough, in the average case the same
	random input data was supplied to all 3 algorithms. Also the test is performed on a
	variable input size ranging from 100 to 10000 numbers to be sorted ascendingly.
	The program outputs the number of operations each algorithm takes in turn for 
	sorting the data represented as number of assignments and number of comparisons
	which are later added to get the final number of operations.
	
	By analyzing the charts made from the output data one can see that even though 
	Bubble sort looks promising in the best case (compared with the other 2) it soon
	starts to fall behind insertion and selection in the average and worst cases, 
	producing a final number of operations way higher than the other 2.
	
	So the question is insertion or selection?
	By looking once more at the charts we do not see quite that big of a difference
	between the 2, but there is a key factor at stake. Taking a closer look we can
	see that selection sort will always have a final number of computations = 
	n(n-1)/2 -> O(n^2) complexity. This happens because at each iteration the 
	algorithm searches for the minimum in the unsorted part of the array and even though
	it could find it on the first position, it has to go through the whole remainder of
	numbers just to make sure it is the minimum. If we look at insertion sort we can 
	identify that even though it is also an O(n^2) algorithm it is slightly better than
	selection sort because n(n-1)/2 happens only in the worst case scenario. Which means
	there are cases in which, by a slight margin, insertion sort performs better.
	
******************************************************************************************/

#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
using namespace std;

void bubble(int v[],int &a,int &c,int n)
{
	int i,aux,swap=1;

	while (swap != 0)
	{
		swap = 0;

		for (i=0;i<n-1;i++)
		{
			c++;
			if (v[i] > v[i+1])
			{
				aux = v[i];
				v[i] = v[i+1];
				v[i+1] = aux;
				swap = 1;
				a+=3;
			}
		}
	}
}


void selection(int v[],int &a,int &c,int n)
{
	int pos,j,i,aux;

	for (j=0; j<n-1; j++)
	{
		pos = j;
		for (i=j+1; i<n; i++)
		{
			c++;
			if (v[i] < v[pos])
				pos = i;
		}
		if (j != pos)
		{
			aux = v[j];
			v[j] = v[pos];
			v[pos] = aux;
			a+=3;
		}

	}
}

void insertion(int v[],int &a,int &c,int n)
{
	int i,j,buff;

	for (i=1; i<n; i++)
	{
		buff = v[i]; a++;
		j=i-1;

		while (v[j] > buff && j>=0)
		{
			v[j+1] = v[j]; a++;c++;
			j--;
		}
		v[j+1] = buff; a++; c++;
	}

}

int main()
{
	int x[10001],v[10001],z[10001],n,i;
	int a=0,c=0;
    FILE *best = fopen("best_bun.csv","w");
    FILE *avg = fopen("avg_bun.csv","w");
    FILE *worst = fopen("worst_bun.csv","w");

	//BEST CASE
	fprintf(best,"n,A_Bubble,C_Bubble,A_Insertion,C_Insertion,A_Selection,C_Selection\n");
	for (n=100;n<=10000;n+=100)
	{
            for (i=0;i<n;i++)
                z[i]=x[i]=v[i]=i;

            a=0;c=0;
            bubble(v,a,c,n);
            fprintf(best,"%d,%d,%d,",n,a,c);

            a=0;c=0;
            insertion(x,a,c,n);
            fprintf(best,"%d,%d,",a,c);

            a=0;c=0;
            selection(z,a,c,n);
            fprintf(best,"%d,%d\n",a,c);
	}

	//AVERAGE CASE
	int ab,cb,as,cs,ai,ci;
    fprintf(avg,"n,A_Bubble,C_Bubble,A_Insertion,C_Insertion,A_Selection,C_Selection\n");
	for (n=100;n<=10000;n+=100)
	{
		ab=cb=as=cs=ai=ci=0;

		for (int q=0;q<5;q++)
		{
            for (i=0; i<n; i++)
                 z[i] = x[i] = v[i] = rand()%n;

            a=0;c=0;
            bubble(v,a,c,n);
            ab+=a; cb+=c;

            a=0;c=0;
            insertion(x,a,c,n);
            ai+=a; ci+=c;

            a=0;c=0;
            selection(z,a,c,n);
            cs+=c; as+=a;
        }
        ab/=5;cb/=5;
        ai/=5;ci/=5;
        as/=5;cs/=5;
        fprintf(avg,"%d,%d,%d,",n,ab,cb);
        fprintf(avg,"%d,%d,",ai,ci);
        fprintf(avg,"%d,%d\n",as,cs);
	}

	//WORST CASE
	fprintf(worst,"n,A_Bubble,C_Bubble,A_Insertion,C_Insertion,A_Selection,C_Selection\n");
	for (n=100;n<=10000;n+=100)
	{
	    for (i=0; i<n; i++)
            v[i] = x[i] = n-i;

        for (i=0; i<n; i++)
            z[i] = i+1;
        z[n-1] = 0;

        a=0;c=0;
        bubble(v,a,c,n);
        fprintf(worst,"%d,%d,%d,",n,a,c);

        a=0;c=0;
        insertion(x,a,c,n);
        fprintf(worst,"%d,%d,",a,c);

        a=0;c=0;
        selection(z,a,c,n);
        fprintf(worst,"%d,%d\n",a,c);
	}


	//getch();
	return 0;
}
