//[af2013] Assign1_***ANONIM***_***GROUP_NUMBER***5
/*
Am putut observa forma liniara si constanta a grficelor in cazurile defavorabile si favorabile, numarul de
comparatii, atribuiri si sume find unul constant.
Am putut sa observ fluctuatia in jurul unei valori fixe in cazurile medii statistice
*/
#include<stdio.h>
#include<fstream>
using namespace std;

int atrib1=0;
int suma1=0;
int comp1=0;

int atrib2=0;
int suma2=0;
int comp2=0;

int atrib3=0;
int suma3=0;
int comp3=0;

//afisare
void afisare(int a[], int n)
{
	for(int i=0;i<n;i++)
		printf("%d ",a[i]);
	printf("\n");

}

//sortari
//1. Metoda bulelor

void bubbleSort(int a[], int n)
{
	int sortat=0;
																
	int aux;
	while(sortat==0)
	{
		sortat = 1;												
		for(int i=0;i<n-1;i++)
		{														comp1++;													
			if(a[i]>a[i+1])								
			{
				aux = a[i];										atrib1++;
				a[i] = a[i+1];									atrib1++;
				a[i+1] = aux;									atrib1++;
				sortat = 0;										
			}
			n--;	
			atrib1++;											
		}
	}
}

//2. Sortare prin insertie
void insertie(int a[], int n)
{
	int key, i;
	for(int j=1;j<n;j++)					
	{
		key=a[j];						atrib2++; 
		i = j-1;						comp2++;
		while((i>=0)&&(a[i]>key))
		{
			a[i+1]=a[i];				atrib2++; 
			i = i-1;					
		}//i+1==j?
		if(i+1==j)
			a[i+1] =key;					atrib2++; 
		
	}
	printf("iesire %d \n", atrib2);
}

//3. Sortarea prin selectie
void selectie(int a[], int n)
{
	int i, j;
	int imin=4, aux;					
	for(i=0;i<n;i++)
	{
		imin=i;							
		//printf("%d \n",i);
		for( j=i+1;j<n;j++)
		{								 comp3++;
			if(a[j]<a[imin])
				imin=j;
			//printf("+");
		}
		if(imin!=i)
		{
			aux = a[i];						atrib3++;
			a[i]=a[imin];					atrib3++; 
			a[imin]=aux;					atrib3++; 
		}
	}
}


void genRandom(int a[], int inc)
{
	for(int i=0;i<inc;i++)
		a[i]=rand();
}

void genRandomCresc(int a[], int inc)
{
	a[0]=1;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]+rand();
}

void genRandomDescresc(int a[], int inc)
{
	a[0]=INT_MAX;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]-rand();
}

void copiereSir(int b[], int a[], int n)
{
	for(int i=0;i<n;i++)
		b[i]=a[i];
}


//
int main()
{
	//int a[10]={5, 7, 9, 1, 3};
	
	//bulele
	//bubbleSort(a,n);
	//insertie(a,n);
	//selectie(a,n);
	
	int n = 0, i;
	int a[100000];
	int med11=0, med12=0, med13=0;
	int med21=0, med22=0, med23=0;
	int med31=0, med32=0, med33=0;
	
	int b[100000];
	FILE *out1, *out2, *out3;
	out1=fopen("buble.csv","w");
	out3=fopen("selectie.csv","w");
	out2=fopen("inserare.csv","w");
	
	for(int con=0;con<100;con++)
	{
		n=con;
		
		atrib1=0;
		suma1=0;
		comp1=0;
	
		atrib2=0;
		suma2=0;
		comp2=0;

		atrib3=0;
		suma3=0;
		comp3=0;
		
		med11=0, 
		med12=0, 
		med13=0;
		med21=0,
		med22=0,
		med23=0;
		med31=0,
		med32=0,
		med33=0;

		printf("tura: %d \n", con);
		for(i=0;i<5;i++)
		{
			

			//generam sirul de numere random
			genRandomCresc(a,n);
			//si ii facem o copie
			copiereSir(b,a,n);
			
			//prima sortare
			bubbleSort(a,n);
			med11=med11+atrib1;
			med12=med12+comp1;


			//a doua sortare
			copiereSir(a,b,n);
			bubbleSort(a,n);
			insertie(a,n);
			med21=med21+atrib2;
			med22=med22+comp2;

			//a treia sortare
			copiereSir(a,b,n);
			bubbleSort(a,n);
			selectie(a,n);
			med31=med31+atrib3;
			med32=med32+comp3;
		}

		
		med11=med11/5;
		med12=med12/5;

		med21=med21/5;
		med22=med22/5;
		

		med31=med31/5;
		med32=med32/5;

		med13=med11+med12;
		med23=med21+med22;
		med33=med31+med32;

		fprintf(out1, "%d,%d,%d,%d\n",con,med11,med12,med13);
		fprintf(out2, "%d,%d,%d,%d\n",con,med21,med22,med23);	
		fprintf(out3, "%d,%d,%d,%d\n",con,med31,med32,med33);


	}

	//-------------test
	/*
	genRandom(a);
	insertie(a,100);
	afisare(a,n);
	*/

	fclose(out1);
	fclose(out2);
	fclose(out3);
	

	return 0;
}
