#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("demo");

typedef struct NODE{
	int key;
	NODE *next;
}NODE;

NODE *adj[10000];
int edges[60000], operations;

typedef struct Queue {
	int data[60000];
	int first,last;
}Queue;

typedef struct node {
	node *next;
	int vertex;
}node;

void insert(int, int);
int visited[60000]; 
node *headLL[10000];      
int verticeNr, edgeNr;                    

int empty(Queue *point) {
	profiler.countOperation("operations", operations);
	if(point->first == -1)
		return 1;
	return 0;
}

void generate(int verticeNr, int edgeNr) {
	int ins1, ins2;
	memset(adj, 0, verticeNr*sizeof(NODE *));
	FillRandomArray(edges, edgeNr, 0, verticeNr*verticeNr-1, true);
	for(int i = 0; i < edgeNr; i++) {
		profiler.countOperation("operations", operations, 2);
		ins1 = edges[i] / verticeNr;
		ins2 = edges[i] % verticeNr;

		insert(ins1, ins2);
		insert(ins1, ins2);
	}
}

void enqueue(Queue *point, int x) {
	profiler.countOperation("operations", operations);
	if(point->first == -1) {
		profiler.countOperation("operations", operations, 2);
		point->first = point->last = 0;
		point->data[point->first] = x;
	}
	else {
		profiler.countOperation("operations", operations, 2);
		point->first = point->first + 1;
		point->data[point->first] = x;
	}
}

int dequeue(Queue *point) {
	int x;
	profiler.countOperation("operations", operations, 2);
	x = point->data[point->last];
	if(point->first == point->last) {
		profiler.countOperation("operations", operations, 2);
		point->first = -1;
		point->last = -1;
	}
	else {
		profiler.countOperation("operations", operations);
		point->last = point->last + 1;
	}
	return x;
}

void insert(int vi, int vj) {
	node *p, *q;
	q = (node *)malloc(sizeof(node));
	profiler.countOperation("operations", operations);
	q->vertex = vj;
	q->next = NULL;
	if(headLL[vi] == NULL)
		headLL[vi] = q;
	else {
		p = headLL[vi];
		while(p->next != NULL)
			p = p->next;
		p->next = q;
	}
}

void BFS(int v) {
	int visit, visited[60000];
	Queue q;

	node *p;
	q.first = q.last = -1;              
	for(int i = 0; i < verticeNr; i++)
		visited[i] = 0;
	if(verticeNr < 90 && edgeNr < 90)
		printf("\nThe queue:\n");
	enqueue(&q, v);
	if(verticeNr < 90 && edgeNr < 90)
		printf("%d ", v);
	visited[v] = 1;
	while(!empty(&q)) {
		v = dequeue(&q);
		for(p = headLL[v]; p != NULL; p = p->next)
		{
			visit = p->vertex;
			if(visited[visit] == 0)
			{
				enqueue(&q, visit);
				visited[visit] = 1;
				if(verticeNr < 90 && edgeNr < 90)
					printf("%d ", visit);
			}
		}
	}
}

int main() {
	//demo
	printf("\nDemo for BST!\n\nVertice number: ");
	scanf("%d", &verticeNr);
	for(int i=0; i<verticeNr; i++)
		headLL[i]=NULL;
	printf("\nEdge number :");
	scanf("%d", &edgeNr);
	generate(verticeNr, edgeNr);
	for(int i=0; i<verticeNr; i++)
		BFS(i);

	/*
	verticeNr = 100;
	printf("Please wait!");
	for(edgeNr = 1000; edgeNr <= 5000; edgeNr += 100) {
		operations = edgeNr;
		generate(verticeNr, edgeNr);
		for(int i = 0; i < verticeNr; i++)
			BFS(i);
	}
	profiler.createGroup("Vertices-BST","operations");
	


	*/
	/*
	edgeNr=9000;
	printf("Please wait!");
	for(verticeNr=100; verticeNr<=200; verticeNr+=10) {
		operations = verticeNr;
		generate(verticeNr, edgeNr);
		for(int i = 0; i < verticeNr; i++)
			BFS(i);
	}
	profiler.createGroup("Edges-BST","operations");
	*/
	printf("\nReady!");
	getch();
	profiler.showReport();
	return 0;
}