/**** TEMA 7 ARBORI MULTI-CAI
*				***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
*	
*			Algoritmele pentru cele 3 reprezentari au fost realizate de catre mine. 
*			Primul algoritm	presupune reprezentarea intr-un vector a asociatiilor
*		fiu - parinte, unde i este fiul si vector[i] parintele fiului i.
*			Cel de-al doilea algoritm corespunzator reprezentarii T2, presupune inversarea
*		asociatiei i->vector[i] in vector[i]->i. Se pastreaza in structura de date TIP_NOD2
*		parintele si un vector de pointeri la fii sai.
			Cel de-al treilea algoritm corespunzator reprezentarii T3, presupune preluarea 
		informatiilor din noduri , respectiv al fiecarui copil in parte, primul copil este facut fiu,
		iar ceilalti copii vor fi fratii lui.
			Algoritmul respecta complexitatea stabilita , respectiv O(n).
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#define MAX = 10;

typedef struct tip_nod{
			       int cheie;/*informatie */
				   struct tip_nod *parent;
				   struct tip_nod *stg,*dr;
			     } TIP_NOD;
TIP_NOD *rad;

typedef struct tip_nod2{
	int cheie;
	struct tip_nod2 *children[10];
	int nrChildren;
} TIP_NOD2;

typedef struct tip_nod3{
	int cheie;
	struct tip_nod3 *children;
	struct tip_nod3 *brother;
	int nrChildren;
} TIP_NOD3;

int count=0;
TIP_NOD2 *rad2;
TIP_NOD3 *rad3;
int t2[10];
int t2Size=0;
int vector[10];
TIP_NOD3 *xpointer= (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));
TIP_NOD *p;
  TIP_NOD *nodes[10];
  TIP_NOD2 *nodes2[10];
  TIP_NOD3 *nodes3[10];
  int i, n,key;
  char ch;

  TIP_NOD3 *p1 = (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));
	TIP_NOD3 *p2 = (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));



void readVector(int n)
{
	for(int i=1;i<=n;i++)
  {
	  printf("Parinte[Nod[%d]]=",i);
	  scanf("%d",&vector[i]);



	  if(vector[i]==-1)
	  {
		  rad = (TIP_NOD *)malloc(sizeof(TIP_NOD));
		  rad->cheie = i;
		  rad2= (TIP_NOD2 *)malloc(sizeof(TIP_NOD2));
		  rad2->cheie=i;
	  }
	  	
	}
}

void initT1(int n)
{
	for(int i=1;i<=n;i++)
  {
	  nodes[i] = (TIP_NOD *)malloc(sizeof(TIP_NOD));
	  nodes[i]->cheie = i;
  }
}

void T1(int n)
{
	for(int i=1;i<=n;i++)//<=n
  {

	 

	  if(vector[i]!=-1)
	  {
	  nodes[i]->parent = (TIP_NOD *)malloc(sizeof(TIP_NOD));
	  nodes[i]->parent->cheie=vector[i];	 
	  printf("test 1 : %d node is and %d child is \n",nodes[i]->cheie,nodes[i]->parent->cheie);
	  }
	  else
	  {
		  
		  
		  nodes[i]=rad;
		  rad = nodes[i];
		  printf("\n \n ROOT IS %d \n\n",nodes[i]->cheie);
	  }

	  
  }
}

void initT2(int n)
{
	for(int i=1;i<=n;i++)
  {
	  
	  nodes2[vector[i]] = (TIP_NOD2 *)malloc(sizeof(TIP_NOD2));
	  if(vector[i]!=-1)
	  {
	  nodes2[vector[i]]->cheie = vector[i];
	  // suplimentar 
		  
	  }
	  else
	  {
	  nodes2[vector[i]]=rad2;
	  }
	  nodes2[vector[i]]->nrChildren=0;
	  
  }
}

void T2(int n)
{
	t2Size=1;
	bool inVector=false;
	for(int i=1;i<=n;i++)
  {
	 
	  nodes2[vector[i]]->nrChildren++;
	  nodes2[vector[i]]->children[nodes2[vector[i]]->nrChildren] = (TIP_NOD2 *)malloc(sizeof(TIP_NOD2));
	  nodes2[vector[i]]->children[nodes2[vector[i]]->nrChildren]->cheie = i;
	   for(int j=1;j<=t2Size;j++)
	  {
		  if(t2[j]==vector[i])
		  {
			  inVector=true;
		  }
	   }
	   if(inVector==false)
	   {
		   t2[t2Size]=vector[i];
		   printf("T2[%d]=%d",t2Size,t2[t2Size]);
		   t2Size++;
	   }
	   
  }
	t2Size--;
}

void afisareT2()
{
	TIP_NOD2 *pointer;
  
  for(int i=1;i<=t2Size;i++)
  {
  for(int j=1;j<=nodes2[t2[i]]->nrChildren;j++)
  {
	 
	  pointer=nodes2[t2[i]]->children[j];
	  if(pointer->cheie!=-1)
	  printf("Children of %d is %d \n",nodes2[vector[i]]->cheie,pointer->cheie);
  }
  }
}

void initT3(int n)
{
	for(int i=0;i<n;i++)
	{
      nodes3[i] = (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));
	  nodes3[i]->cheie = i;
	  nodes3[i]->brother=NULL;
	  nodes3[i]->children=NULL;
	}

}

void T3(int n)
{
	

	///
	int i=1;
	TIP_NOD3 *pointer = (TIP_NOD3 *)malloc(sizeof(TIP_NOD3 *));
	if(count==0)
	{
		rad3 = (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));
		rad3->children= (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));
		rad3->cheie = rad2->cheie;
		count++;
	}
	
	 while(count<=nodes2[t2[i]]->nrChildren && count<=t2Size)
	 {
		 if(nodes2[t2[i]]->nrChildren>0 && count==1)
		 {
			nodes3[t2[i]]->children= nodes3[nodes2[t2[i]]->children[count]->cheie];
			pointer = nodes3[t2[i]]->children;
			printf("\n");
			printf("NODES3-> KEY = %d , children is %d ->",nodes3[t2[i]]->cheie,pointer->cheie);
			count++;
		 }
		 if(nodes2[t2[i]]->nrChildren>0 && count>=2 && count<=nodes2[t2[i]]->nrChildren)
		 {
			 
			 pointer->brother=nodes3[nodes2[t2[i]]->children[count]->cheie];
			 printf("%d->",nodes2[t2[i]]->children[count]->cheie);
			 pointer = pointer->brother;
			 
			 count++;
		 }
		 if(count>nodes2[t2[i]]->nrChildren)
		 {
			 i++;
			 count=1;
		 }
		 if(i>t2Size)
		 {
			 break;
		 }
	 }
	
	 count=0;
	 printf("\n");
}

TIP_NOD3 *search(int key)
{
	for(int i=1;i<t2Size;i++)
	{ 
		if(nodes3[t2[i]]->cheie==rad3->cheie)
		{
			return nodes3[t2[i]];
		}
	}
	return NULL;
}
		/*void afisareT3()
		{
			TIP_NOD3 *pointer = (TIP_NOD3 *)malloc(sizeof(TIP_NOD3));
			
			for(int i=1;i<t2Size;i++)
			{
				pointer=nodes3[t2[i]];
				printf("NODES3 KEY = %d",pointer->cheie);
				if(nodes3[t2[i]]->brother!=NULL)
				{
					printf("->%d",pointer->brother->cheie);
				}
				if(nodes3[t2[i]]->children!=NULL)
				{
					printf("| %d",pointer->children->cheie);
				}
			}
		}*/

		
void prettyPrint(TIP_NOD3 *node,int nivel)
{
	if(node!=NULL)
	{
		for(int i=0;i<nivel;i++)
			printf("  ");
		
		printf("%d\n",node->cheie);
		prettyPrint(node->children,nivel+1);
		prettyPrint(node->brother,nivel);
		
	}
		
}

int main()
{
  

  printf("Numar de elemente : ");
  scanf("%d",&n);
  printf("\n");


  //Citire vector si gasire cheie
  readVector(n);

  //Init T1
  initT1(n);

  //Transformare T1
  T1(n);

  //Init T2
  initT2(n);

  //Transformare T2 O(n)
  T2(n);

  //afisare partiala T2
  //afisareT2();
	for(int i=1;i<=t2Size;i++)
	{
		printf("Node t2 %d\n",nodes2[t2[i]]->cheie);
	}

  //
  initT3(n);

  //
T3(n);
	TIP_NOD3 *radacina = search(rad2->cheie);
	xpointer=radacina;
	prettyPrint(xpointer,0);

  return 0;


}
