#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define MAX 101

int static n, v[MAX], rad;

struct multiway {
	int cheie;
	int copii; // numarul de copii
	multiway *fiu; // vectorul de fii
} t1[MAX];

struct binary {
    int cheie;
    binary *copil; // copilul din stanga
    binary *frate; // frate din dreapta
} t2[MAX];

void T1() {

    int i;

    // initializam cheia, numarul de copii este la inceput 0
	// cautam radacina
    // adunam numarul fiilor pt fiecare tata 
	for(i = 0; i < n; i++) {
        t1[i].cheie = i;
        t1[i].copii = 0;
	}
	for(i = 0; i < n; i++) {
		if(v[i] == -1) rad = i;
        else t1[v[i]].copii++;
	}
        
    // alocarea dinamica pentru vectorul de fii pentru fiecare nod
	 for(i = 0; i < n; i++) {
        t1[i].fiu = (multiway*)malloc(t1[i].copii);
        t1[i].copii = 0;
		}

    // punem elementele in vectorul de fii
		for(i = 0; i < n; i++)
        if(v[i] != -1) {
            t1[v[i]].fiu[t1[v[i]].copii].cheie = i;
            t1[v[i]].copii++;
        }
	 
	
}


void T2() {

    int i;
	for(i = 0; i < n; i ++) {
	
		t2[i].copil = (binary*)malloc(sizeof(binary));
		t2[i].frate = (binary*)malloc(sizeof(binary));
	}
	
    for(i = 0; i < n; i ++) {
        t2[i].cheie = i;

        // copilul din stanga
        // verificam daca exista alfel -1
		
        if(t1[i].copii > 0)
            t2[i].copil->cheie = t1[i].fiu[0].cheie;
        else t2[i].copil->cheie = -1;
        
        // copilul din dreapta
        // verificam daca exista alfel -1
		int j;
		
		for(j = 0; j < t1[i].copii-1 ; j++){
			t2[t1[i].fiu[j].cheie].frate->cheie = t1[i].fiu[j+1].cheie;
		}
		if(t1[i].copii > 0) 	
			t2[t1[i].fiu[j].cheie].frate->cheie = -1;
		if(i == rad) 
			t2[i].frate->cheie = -1;
    }
}

// Pretty Printing
void PP(int node, int tab) {
    int i;

    for(i = 0; i <= 4*tab; i++)
        printf(" ");
    printf("%d\n", node);

    // pentru fiecare copil sare la un nou nivel
    if(t2[node].copil != NULL && t2[node].copil->cheie != NULL) 
		PP(t2[node].copil->cheie, tab+1);
    // pentru fiecare frate sare la un nou nivel
	if(t2[node].frate !=NULL && t2[node].frate->cheie != NULL) 
		PP(t2[node].frate->cheie, tab);
}

int main() {

    FILE* f = fopen("Data.txt", "r");
    int i, j;
    fscanf(f, "%d", &n);
    for(i = 0; i < n; i++) fscanf(f, "%d", &v[i]);
	
	for(i = 0; i < n; i++) {
		printf("%d ",v[i]," ");
	}
    T1();
	printf("Radacina: %d\n",rad);
	  for (int i=0;i<n;i++){
		  printf("Nodul %d:",t1[i].cheie);
		  for (int j=0;j<t1[i].copii;j++){
			  printf("%d ", t1[i].fiu[j].cheie);
		  }
		  printf("\n");
	  }
	  printf("---------------------------\n");
    
	//T2();

	PP(rad, 0);
	getch();
    fclose(f);
    return 0;
	
}