/*
	Name: ***ANONIM*** ***ANONIM***�n
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently the Heapsort and Quicksort advanced 
				  sorting methods.

	Average case for both of the algorithm takes O(nlogn), but quicksort performs better, 
				  since it has smaller time constant.
	Best case for quicksort also takes  O(nlogn), which was generated as a sorted array. The partition then
				  selects the middle element, which is also the median, so the interval is divided into two.
	Worst case for quicksort is O(n^2), since every time the size of the interval is decreased by 1.

	Observation: the best choice overall would be a quicksort with randomized pivot, as seen on charts.


*/
#include<stdio.h>
#include<math.h>
#include<cstdlib>
#include <time.h>

int n, length, A[10001], aux[10001], auxn;
int i;
int T,I,M;
int heapcount, quickcount, quickrandcount;
int ok;

void save_array()
{
	auxn=n;
	for(int i=1;i<=n;i++)
		aux[i]=A[i];
}

void restore_array()
{
	n=auxn;
	for(int i=1;i<=n;i++)
		A[i]=aux[i];
}

void print_array()
{
	for(int i=1;i<=n;i++)
		printf("%d ", A[i]);
	printf("\n");
}

void max_heapify(int pos)
{
	int left=2*pos;
	int right=left+1;
	int largest;
	if(left <= n && A[left] > A[pos])
		largest=left;
	else largest=pos;
	heapcount++;
	if(right <= n && A[right] > A[largest])
		largest=right;
	heapcount++;
	if(largest!=pos)
	{
		int aux=A[pos];
		A[pos]=A[largest];
		A[largest]=aux;
		heapcount+=3;
		max_heapify(largest);
	}
}

void build_heap()
{
	for(i=n/2;i>=1;i--)
		max_heapify(i);
}

void heapsort()
{
	int naux=n;
	build_heap();
	for(i=n;i>1;i--)
	{
		int aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		heapcount+=3;
		n--;
		max_heapify(1);
	}
	n=naux;
}

int partition(int p, int r)
{
	int x=A[r],i,j,aux;
	quickcount++;
	i=p-1;
	for(j=p;j<r;j++)
	{
		if(A[j]<=x)
		{
			i++;
			if(i!=j)
			{
				aux=A[i];
				A[i]=A[j];
				A[j]=aux;
				quickcount+=3;
			}
		}
		quickcount++;
	}
	if(i+1!=r)
	{
		aux=A[i+1];
		A[i+1]=A[r];
		A[r]=aux;
		quickcount+=3;
	}
	return i+1;
}

void quicksort(int p, int r)
{
	int q=partition(p, r);
	if(p<q-1)
		quicksort(p, q-1);
	if(q+1<r)
		quicksort(q+1, r);
}

int partition_for_rand(int p, int r)
{
	int x=A[r],i,j,aux;
	quickrandcount++;
	i=p-1;
	for(j=p;j<r;j++)
	{
		if(A[j]<=x)
		{
			i++;
			if(i!=j)
			{
				aux=A[i];
				A[i]=A[j];
				A[j]=aux;
				quickrandcount+=3;
			}
		}
		quickrandcount++;
	}
	if(i+1!=r)
	{
		aux=A[i+1];
		A[i+1]=A[r];
		A[r]=aux;
		quickrandcount+=3;
	}
	return i+1;
}

int randomized_partition(int p, int r)
{
	int i=rand()%(r-p+1)+p;
	int aux=A[r];
	A[r]=A[i];
	A[i]=aux;
	quickrandcount+=3;
	return partition_for_rand(p,r);

}

void quicksort_randomized(int p, int r)
{
	int q;
	q=randomized_partition(p, r);
	if(p<q-1)
		quicksort_randomized(p, q-1);
	if(q+1<r)
		quicksort_randomized(q+1, r);
}

int middle_pivot(int p, int r)
{
	int aux=A[(p+r)/2];
	A[(p+r)/2]=A[r];
	A[r]=aux;
	quickcount+=3;
	return partition(p,r);
}

void quicksort_middle_pivot(int p, int r)
{
	int q=middle_pivot(p, r);
	if(p<q-1)
		quicksort_middle_pivot(p, q-1);
	if(q+1<r)
		quicksort_middle_pivot(q+1, r);
}

void test()
{
	A[1]=4; A[2]=1; A[3]=3; A[4]=2; A[5]=16; A[6]=9; A[7]=10; A[8]=14; A[9]=8; A[10]=7;
	n=10;
	save_array();
	heapsort();
	print_array();
	restore_array();
	quicksort(1, n);
	print_array();
	restore_array();
	quicksort_randomized(1, n);
	print_array();
}


int main()
{
	srand (time(NULL));
	test();
	
	//average case
	freopen("heap_quick_average.csv", "w", stdout);
	printf("n,Heapsort,Quicksort,Quicksortrandomized\n");
	freopen("average.txt", "r", stdin);
	scanf("%d", &T);
	for(I=1;I<=T;I++)
	{
		heapcount=0;
		quickcount=0;
		quickrandcount=0;

		for(M=1;M<=5;M++)
		{
			scanf("%d", &n);
			for(i=1;i<=n;i++)
				scanf("%d", &A[i]);
			save_array();
			heapsort();
			restore_array();
			quicksort(1, n);
			quicksort_randomized(1, n);

		}
		printf("%d,%d,%d,%d\n",n,heapcount/5, quickcount/5, quickrandcount/5);
	}
	
	//worst case
	freopen("quick_worst.csv", "w", stdout);
	printf("n,Quicksort, QuicksortRand\n");
	freopen("worst.txt", "r", stdin);
	scanf("%d", &T);
	for(I=1;I<=T;I++)
	{
		quickcount=0;
		quickrandcount=0;
			scanf("%d", &n);
			for(i=1;i<=n;i++)
				scanf("%d", &A[i]);
			save_array();
			quicksort(1, n);
			restore_array();
			quicksort_randomized(1, n);
		printf("%d,%d,%d\n",n,quickcount, quickrandcount);
	}

	//best case
	freopen("quick_best.csv", "w", stdout);
	printf("n,Quicksort, QuicksortRand\n");
	freopen("best.txt", "r", stdin);
	scanf("%d", &T);
	ok=1;
	for(I=1;I<=T;I++)
	{
		quickcount=0;
		quickrandcount=0;
		scanf("%d", &n);
		for(i=1;i<=n;i++)
			scanf("%d", &A[i]);
		save_array();
		quicksort_middle_pivot(1, n);
		for(i=1;i<=n;i++)
			if(A[i]!=i)
				ok=0;
		restore_array();
		quicksort_randomized(1, n);
		printf("%d,%d,%d\n",n,quickcount, quickrandcount);
	}

	

	return 0;
}