/* in cazul cel mai defavorabil si in cazul mediu quicksortul este mai eficient decat heapsortul */




#include <stdio.h>
#include <iostream>
using namespace std;
#include "profiler.h"
Profiler pro("demo");

int cont_size;

int partition(int *v, int p, int r)
{
	int x=v[r];
	int i=p-1;
	 pro.countOperation("quicksort", cont_size, 2);

	for (int j=p; j < r; j++)
	{
		if (v[j] <= x)
		{
			i++;
			 pro.countOperation("quicksort", cont_size, 2);

			if (v[j] != v[i])
			{
				int aux=v[j];
				v[j]=v[i];
				v[i]=aux;
				 pro.countOperation("quicksort", cont_size, 3);
			}
			
		}
		
	}
	int aux=v[i+1];
	v[i+1]=v[r];
	v[r]=aux;
	 pro.countOperation("quicksort", cont_size, 3);

	return i+1;
}

void quicksort(int *v, int p, int r)
{
	if (p<r)
	{
		int q=partition(v,p,r);
		 pro.countOperation("quicksort", cont_size);

		quicksort(v, p, q-1);
		quicksort(v, q+1, r);
	}
	
}

void max_heapify(int *v, int i,int p)
{
 int l,r;
 int largest=0;
 l=2*i;
 r=2*i+1;
 
 int heapsize=p;
 pro.countOperation("heapsort", cont_size, 5);
 if ((l<=heapsize) && (v[l]>v[i]))
 {
	 largest=l;
	 pro.countOperation("heapsort",cont_size);
 }
 else
 {
	 largest=i;
	 pro.countOperation("heapsort", cont_size);
 }
 
 if ((r<=heapsize) && (v[r]>v[largest]))
 {
	 largest=r;
	 pro.countOperation("heapsort", cont_size);
 }
 if (largest!=i)
 {
	int aux=v[i];
	 v[i]=v[largest];
	 v[largest]=aux;
	 pro.countOperation("heapsort", cont_size, 3);
	 max_heapify(v,largest,p);
 }
}


void build_max_heap_bottom_up(int *v, int p)
{
	int heapsize=p;
	pro.countOperation("heapsort",cont_size);
	for (int i=(p/2);i>=2;i--)
	{
		max_heapify(v,i,p);
	}
}
void heapsort(int *v,int p)

{
	int m=p;
	pro.countOperation("heapsort",cont_size);
	build_max_heap_bottom_up( v, m);
	for(int i=m;i>=2;i--)
	{
		int aux=v[1];
		v[1]=v[i];
		v[i]=aux;
		m--;
		pro.countOperation("heapsort", cont_size,4);
		max_heapify(v,1 ,m);
	}

}

void afisare(int *v, int n)
{
	for(int i=0;i<=n;i++)
	{
		printf("%d ",v[i]);
	}

}


int main()
{
	/*int test[]={1,15,10,9,3,4,5,8};
	int test1[]={1,15,10,9,3,4,5,8};
	int p=7;
	int p1=7;
	int st=0;
	int dr=p;
	printf("rezultatul de la quicksort: \n");
	quicksort(test,st,dr);
	afisare(test,p);
	printf("\n rezultatul de la heapsort: \n");
	heapsort(test1,p);
	afisare(test1,p1);
	printf("\n");*/
	int beav;

	printf("best case sau avrage case: 1 pentru best 2 pentru avgrage:");
	scanf("%d",&beav);
	if(beav==2)
	{

//=================avrage case===============
	
	int n;
	
	int va[10000],v1a[10000];
	
	for(n=10;n<500;n+=10)
	{
		

		cont_size=n;
		FillRandomArray(va,n);
	    memcpy(v1a,va,n*sizeof(int));
		//printf("%d ",n);
		if (n==10)
		{
			for(int j=0;j<n;j++)
			{
				printf("%d ",va[j]);
			}
		}

		heapsort(va,n);
	    quicksort(v1a,0,n);
		
		//printf("%d ",&va[n]);
							
	}
	
	
	
	pro.createGroup("avrage","heapsort","quicksort");
	pro.showReport();
	}
	else{
//================best case============
	int nb;
	
	int vb[10000],v1b[10000];
	for(nb=10;nb<500;nb+=10)
	{
		cont_size=nb;
		//printf("%d ",nb);
		FillRandomArray(vb,nb,10,10000,false,1);
		memcpy(v1b,vb,nb*sizeof(int));
		if (nb==10)
		{
			for(int j=0;j<nb;j++)
			{
				printf("%d ",vb[j]);
			}
		}
        heapsort(vb,nb);
	    quicksort(v1b,0,nb);
		
		
		//printf("%d ",vb[nb]);
							
	}
	
	pro.createGroup("best","heapsort","quicksort");
	pro.showReport();
	}
	
	
}