#include <stdio.h>
#include <stdlib.h>
#define N_MAX 20
#define MAX_FII 10

/**parintele si fii lui**/
typedef struct parinte_fii
{
    int nr_fii;
    int fii[MAX_FII];
}parinte_fii;

/**R2**/
typedef struct multi_cai
{
    int key;
    int nr_fii;
    struct multi_cai *fii[MAX_FII];
} multi_cai;

/**R3**/
typedef struct binar
{
    int key;
    struct binar *stg;
    struct binar *dr;
} binar;

//O(n) - adaug n-1 fii radacina fiind deja inserata
void creare_arbore_multicai(multi_cai **r, parinte_fii fii[])
{
    int i, parinte = (*r)->key;
    //printf("parinte: %d\n", parinte);
    for(i = 0; i < fii[parinte].nr_fii; i++)
    {
        //adaugare fiu
        (*r)->fii[i] = (multi_cai*)malloc(sizeof(multi_cai));
        (*r)->fii[i]->key = fii[parinte].fii[i];
        (*r)->fii[i]->nr_fii = 0;
        (*r)->nr_fii++;
        //printf("fiu %d: %d\n", (*r)->nr_fii, (*r)->fii[i]->key);
        creare_arbore_multicai(&((*r)->fii[i]), fii);
    }
}

void print_preordine_multicai(multi_cai *r, int nr_spatii)
{
    int i;
    for(i = 0; i <= nr_spatii; i++)
        printf(" ");
    printf("%d\n", r->key);
    for(i = 0; i < r->nr_fii; i++)
        print_preordine_multicai(r->fii[i], nr_spatii+1);
}

//O(n) - adaug n-1 fii radacina fiind deja inserata
void creare_arbore_binar(binar **b, multi_cai *m)
{
    binar *aux;
    int i;
    if(m->nr_fii > 0)
    {
        (*b)->stg = (binar*)malloc(sizeof(binar));
        (*b)->stg->key = m->fii[0]->key;
        (*b)->stg->stg = NULL;
        (*b)->stg->dr = NULL;
        aux = (*b)->stg;
        for(i = 1; i < m->nr_fii; i++)
        {
            aux->dr = (binar*)malloc(sizeof(binar));
            aux->dr->key = m->fii[i]->key;
            aux->dr->stg = NULL;
            aux->dr->dr = NULL;
            creare_arbore_binar(&aux,m->fii[i-1]);
            aux = aux->dr;
        }

    }
}

void print_preordine_binar(binar *r, int nr_spatii)
{
    int i;
    for(i = 0; i < nr_spatii; i++)
        printf("\t");
    printf("%d\n", r->key);
    if(r->stg != NULL)
        print_preordine_binar(r->stg, nr_spatii+1);
    if(r->dr != NULL)
        print_preordine_binar(r->dr, nr_spatii);
}

int main()
{
    //variabile
    int a[N_MAX], i, n = 9;
    parinte_fii aux[MAX_FII];
    multi_cai *r2 = NULL;
    binar *r3 = NULL;
    a[1] = 2;
    a[2] = 7;
    a[3] = 5;
    a[4] = 2;
    a[5] = 7;
    a[6] = 7;
    a[7] = -1;
    a[8] = 5;
    a[9] = 2;

    /******Printare R1******/
    printf("R1:\n");
    for(i = 1; i <= n; i++)
        printf("%d ", a[i]);
    printf("\n");
    /******Printare R1******/

    /**T1**/
    //O(n)
        //initializez aux
    for(i = 1; i <= n; i++)
        aux[i].nr_fii = 0;
    //O(n)
        //creez vectori cu fii fiecarui nod
    for(i = 1; i <= n; i++)
    {
        if(a[i] != -1)
        {
            aux[a[i]].fii[aux[a[i]].nr_fii] = i;//inserez fiul
            aux[a[i]].nr_fii++;//cresc numarul de fii
        }
    }
    //O(n) - worst case
        //caut radacina
    i = 1;
    while(a[i] != -1)
        i = a[i];
    //inserez radacina
    r2 = (multi_cai*)malloc(sizeof(multi_cai));
    r2->key = i;
    r2->nr_fii = 0;
    //O(n)
    creare_arbore_multicai(&r2, aux);
    /**T1**/

    /********Printare R2**********/
    printf("R2:\n");
    print_preordine_multicai(r2, 0);
    /********Printare R2**********/

    /**T2**/
    r3 = (binar*)malloc(sizeof(binar));
    r3->key = r2->key;
    r3->stg = NULL;
    r3->dr = NULL;
    creare_arbore_binar(&r3, r2);
    /**T2**/

    /********Printare R3**********/
    printf("R3:\n");
    print_preordine_binar(r3, 0);
    /********Printare R3**********/

    return 0;
}
