/*
	Name: ***ANONIM*** ***ANONIM***án
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently the basic operations for disjoint 
					sets: Make-Set(x), Union(x, y) and Find-Set(x) (section 21.1 from Cormen),
					using trees as underlying structures for each disjoint set (i.e. disjoint set forests – see 
					section 21.3 from Cormen). Also, you should use path compression and union by rank
					heuristics in your implementations, to improve the running time.
					Moreover, you are required to employ these data structures in an algorithm for finding 
					the connected components of a graph (see section 21.1 from Cormen for details and 
					pseudo-code)

	Analysis: - in union by rank, I make the root with smaller rank point to the root with larger rank during a
				UNION operation
			  - I use path compression during Find-Set operations to make each node on the find path point 
				directly to the root
			  - by using union by rank and path compression, the worst-case running time is O(m a(n)), where
				a(n) is a very slowly growing function (inverse Ackerman), thus a(n)<=4
			  - practically the running time is linear with respect to the number of ops
*/
#include<stdio.h>
#include<iostream>
#include<time.h>

using namespace std;

#define N 10010
#define M 60010

struct NODE
{
	NODE *p;
	int rank;
}*nodes[N];

int nodeNr;
int n,m;
int calls;

bool in[N][N];

struct edge
{
	NODE* u, * v;
}edges[M];

struct intEdge
{
	int u,v;
}intEdges[M];

void make_set(NODE* x)
{
	x->p=x;
	x->rank=0;
	calls++;
}

void link(NODE* x,  NODE* y)
{
	if(x->rank>y->rank)
		y->p=x;
	else
	{
		x->p=y;
		if(x->rank==y->rank)
			y->rank++;
	}
}

NODE* find_set(NODE* x)
{
	calls++;
	if(x!=x->p)
		x->p=find_set(x->p);
	return x->p;
}

void unionSet(NODE* x, NODE* y)
{
	link(find_set(x), find_set(y));
	calls++;
}

void createIntNode(int value)
{
	nodes[value]=new NODE;
	nodeNr++;
}

NODE* findIntNode(int value)
{
	return nodes[value];
}

void connected_component()
{
	for(int i=1;i<=n;i++)
		make_set(findIntNode(i));
	for(int i=1;i<=m;i++)
		if(find_set(edges[i].u)!=find_set(edges[i].v))
			unionSet(edges[i].u, edges[i].v);
}

bool same_component(NODE* u, NODE *v)
{
	return find_set(u)==find_set(v);
}

void create_nodes()
{
	for(int i=1;i<=n;i++)
		createIntNode(i);
}

void free_all()
{
	for(int i=1;i<=n;i++)
		free(nodes[i]);
	for(int i=1;i<=m;i++)
		in[intEdges[i].u][intEdges[i].v]=in[intEdges[i].v][intEdges[i].u]=false;
}

void test()
{
	//10 nodes
	n=10;
	create_nodes();
	/*7 edges
	1 2
	2 3
	2 4
	1 3
	5 6
	5 7
	8 9
	*/
	m=7;
	edges[1].u=findIntNode(1); edges[1].v=findIntNode(2);
	edges[2].u=findIntNode(2); edges[2].v=findIntNode(3);
	edges[3].u=findIntNode(2); edges[3].v=findIntNode(4);
	edges[4].u=findIntNode(1); edges[4].v=findIntNode(3);
	edges[5].u=findIntNode(5); edges[5].v=findIntNode(6);
	edges[6].u=findIntNode(5); edges[6].v=findIntNode(7);
	edges[7].u=findIntNode(8); edges[7].v=findIntNode(9);
	connected_component();
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++)
			if(same_component(findIntNode(i), findIntNode(j)))
				cout<<"Node "<<i<<" and "<<j<<" are in the same connected component\n";
			else cout<<"Node "<<i<<" and "<<j<<" are not in the same connected component\n";
	free_all();
	
}

int main()
{
	srand(time(NULL));
	//test();
	freopen("out.csv", "w", stdout);
	printf("Number of edges,Number of calls\n");
	n=10000;
	for(int i=10000;i<=60000;i+=1000)
	{
		create_nodes();
		m=i;
		calls=0;
		//printf("%d\n", i);
		for(int j=1;j<=m;j++)
		{

			int u=rand()%n+1, v=rand()%n+1;
			while(in[u][v]==true)
			{
				u=rand()%n+1;
				v=rand()%n+1;
			}
			edges[j].u=findIntNode(u);
			edges[j].v=findIntNode(v);
			intEdges[j].u=u;
			intEdges[j].v=v;
			in[u][v]=in[v][u]=true;
		}
		connected_component();
		printf("%d,%d\n", m, calls);
		free_all();
		
	}
	return 0;
}