#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#define _CRT_SECURE_NO_DEPRECATE(fopen_s)

/**
***ANONIM*** ***ANONIM*** - ***GROUP_NUMBER***
Am implementat sortarea prin metoda QuickSort si HeapSort.
Eficienta algoritmului QuickSort in cazul favorabil si mediu statistic este O(n log n), iar in cazul defavorabil este O(n^2).
Eficienta algoritmului HeapSort in cazul mediu statistic este O(nlog n).
Pentru cazul mediu statistic este mai eficient sa folosim sortarea prin metoda QuickSort.
De observat diferenta mare de eficienta pentru QuickSort in cazul defavorabil.
*/

int n,dim=0;
int a[10001];
int b[10001];
int Qu=0,He=0;
int Qufav,Qudefav;

int parinte(int i)//returneaza parintele nodului i;
{
	return i/2;
}

int stanga(int i)//returneaza fiul stang a lui i
{
	return 2*i;
}

int dreapta(int i)//returneaza fiul drept;
{
	return (2*i)+1;
}

void reconstructie_heap(int a[],int i) 
{
	int l,r,max,aux;
	l=stanga(i);
	r=dreapta(i);
	He++;
	if(l<=dim && a[l]>a[i])
		max=l;
	else
		max=i;
	He++;
	if(r<=dim && a[r]>a[max])
		max=r;
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		He+=3;
		reconstructie_heap(a,max);
	}
}

void buttom_up(int a[]) // construieste heap prin metoda buttom up
{
	dim=n;
	for(int i=n/2;i>=1;i--)
		reconstructie_heap(a,i);
}

void heapsort(int a[])// sorteaza vectorul prin metoda heapSort.
{
	int aux;
	buttom_up(a);
	for(int i=n;i>=2;i--)
	{
		He+=3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		dim--;
	reconstructie_heap(a,1);
	}
}

int partitionare(int a[], int st, int dr)
{
   int pivot, i, j, aux;//variabilele pivot, i, j, aux
   Qu++;
   pivot = a[st];//pivotul se initializeaza cu elementul de pe pozitia st din matrice
   i=st-1;	
   j=dr+1;   
	while (1) 
   {
	do {
		--j;
		Qu++;//o operatie critica e compararea a[j]>pivot
	   }
	while (a[j]>pivot);

	do 
		{
		Qu++;//o alta operatie critica e compararea a[i]<pivot 
		++i;
		}

		while (a[i]<pivot);
	if (i<j) {
				Qu=Qu+3;//op. critica a[j]=a[i];
				aux = a[j];
				a[j]=a[i];
				a[i]=aux;
				}
		   else 
			   return j;
   }
}

void quickSort(int a[],int st,int dr)
{
	int m;

	if(st<dr)
	{
		m=partitionare( a, st, dr);
		quickSort(a,st,m-1);
		quickSort(a,m+1,dr);
	}
}

void main()
{	


	FILE *f=fopen("comparari.csv","w");
	//FILE *g=fopen("Quick.csv","w");
	fprintf(f,"nr , heapsort,quicksort mediu\n");
	//fprintf(g,"nr , Quick fav,quick defav\n");
	for( n=100;n<=10000;n+=100)
	{
		He=0;
		Qu=0;
		Qufav=0;// numarul de operatii pentru algoritmul QuickSort in cazul favorabil
		Qudefav=0;// nr de operatii in cazul defavorabil

		/*for(int j=1;j<=n;j++)
		{
			best[j]=j;//se genereaza cazul favorabil
			worst[j]=n-j;// cazul defavorabil
		}
		worst[n]=0;
		quickSort(worst,1,n);
		Qudefav=Qu;
		Qu=0;
		best[n]=n/2;
		quickSort(best,1,n);
		Qufav=Qu;
		Qu=0;
		**/
		
		
		for(int l=1;l<=5;l++){
			for(int j=1;j<=n;j++)
			{
				a[j]=rand()%100;// cazul mediu statistic
				b[j]=a[j];
			}
	
			heapsort(a);
			quickSort(b,1,n);
		}
	
		fprintf(f,"%d,%d,%d\n",n,He/5,Qu/5);
		//fprintf(g,"%d,%d,%d\n",n,Qufav,Qudefav);
}

printf("gata\n");

	
}