/*
* DFS and BFS graph traversals
* DIANA BALC
* Given a graph G = (V,E), DFS traverses all vertices o G and constructs 
* a forest together with a set of vertices. Classification of edges: 
* direct edges and back edges.
* 
* Idea of Topological sorting: Run the DFS on the DAG and output the vertices 
* in reverse order of finishing time - after each vertex is finished, insert
* it into the front of a list. Running Time: O (|V|+|E|), as for DFS.
*
*/
#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int dfs_OPS;
int bfs_OPS;
FILE *fDFS, *fBFS; 

enum EdgeType
{
	None,
	Discovery, // also direct or forward edges if u.d < v.d
	Cross, // if the finishing time u.d > v.d 
	Back // also backward edges
};

struct TreeNode
{
	int visited;
	int startTime;
	int finishTime;
	int value; // TreeNode->value is actually referring to the node itself in the graph
};
struct LLNode
{		// equivalent structure for an edge
	TreeNode *destination;
	EdgeType edgeType;
	LLNode* next;
};
struct LinkedList
{		
	TreeNode *source;
	LLNode* nodeStart;
};


/*******************************************************/
//GRAPH OPERATIONS

int EdgeNotExistent (LinkedList* graph, int src, int dst)
{
	if (graph[src].nodeStart == NULL)
		return 1;
	LLNode* node = graph[src].nodeStart;
	while (node != NULL)
	{
		if (node->destination->value == dst)
			return 0;
		node = node->next;
	}
	return 1;
}
void AddEdge (LinkedList *graph, int src, int dst)
{
	LLNode* node = new LLNode();
	node->next = NULL;
	node->destination = graph[dst].source;
	node->edgeType = None;

	if (graph[src].nodeStart == NULL)
	{		
		graph[src].nodeStart = node;
	}
	else
	{
		LLNode* curNode = graph[src].nodeStart;
		while (curNode->next != NULL)
			curNode = curNode->next;
		curNode->next = node;
	}
}

/*******************************************************/

int* topologyList;
int topologySize = 0;

void AllocateGraph(LinkedList **graph, int n)
{
	*graph = new LinkedList[n];
	topologyList = new int[n];
	topologySize = 0;
}
void DeallocateGraph(LinkedList* graph, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (LLNode* node = graph[i].nodeStart; node != NULL; )
		{
			LLNode* next = node->next;
			free(node);
			node = next;
		}
		free(graph[i].source);
	}
	free(topologyList);
}
void GenerateGraph(LinkedList* graph, int E, int V)
{
	int n = 0;

	if ((V-1)*(V-1) < E)
	{
		cout<<"Too many edges!";
		return;
	}

	for (int i = 0; i < V; i++)
	{
		LinkedList value;
		TreeNode* node = new TreeNode();
		node->value = i;
		node->visited = 0;
		value.nodeStart = NULL;
		value.source = node;
		graph[i] = value; 
	}

	while (n < E)
	{
		int src = rand()%V;
		int dst = rand()%V;
		if (src!=dst && EdgeNotExistent(graph, src, dst))
		{
			AddEdge(graph, src, dst);
			n++;
		}
	}
}

int DFS(LinkedList* graph, TreeNode* node, int time = 1)
{
	//N++;
	node->visited = 1; dfs_OPS++;
	node->startTime = time;
	int curTime = time;
	
	for (LLNode* nd = graph[node->value].nodeStart; nd != NULL; nd = nd->next)
	{
		//E++
		dfs_OPS++;
		if (nd->destination->visited)
			nd->edgeType = (time < nd->destination->startTime) ? Cross : Back;
		else
		{
			nd->edgeType = Discovery;
			curTime = DFS(graph, nd->destination, curTime + 1);
		}
	}
	node->finishTime = curTime + 1;
	topologyList[topologySize++] = node->value;
	return node->finishTime;
}

TreeNode **queue;
int numberQueue;

void BFS(LinkedList* graph)
{
	if (numberQueue == 0)
		return;
	TreeNode *front = queue[0]; bfs_OPS++;

	for (int i = 0; i < numberQueue - 1; i++)
		
		queue[i] = queue[i+1]; 
		bfs_OPS += 2;
	numberQueue--;

	front->visited = 1; 
	for (int i = 1; i < front->startTime; i++)
		cout<<"  ";
	cout<<front->value<<endl;

	for (LLNode* node = graph[front->value].nodeStart; node != NULL; node = node->next)
	{
		bfs_OPS += 2;
		if (node ->destination ->visited == 0 && node -> destination -> startTime == 0)
		{ 
			bfs_OPS += 2;
			queue[numberQueue++] = node->destination; 
			node->destination->startTime = front->startTime + 1;
		}
	}

	BFS(graph);
}

void DisplayAdjacencyList(LinkedList* graph, int V)
{
	for (int i = 0; i < V; i++)
	{
		cout<<i<<": ";
		for (LLNode* nd = graph[i].nodeStart; nd != NULL; nd = nd->next)
		{
			cout<<nd->destination->value<<" ";
		}
		cout<<endl;
	}
}
void DisplayEdgeLabels(LinkedList* graph, int V)
{
	for (int i = 0; i < V; i++)
	{
		for (LLNode* nd = graph[i].nodeStart; nd != NULL; nd = nd->next)
		{
			char et;
			switch (nd -> edgeType)
			{
			case Discovery: et = 'D'; break;
			case Back: et = 'B'; break;
			case Cross: et = 'C'; break;
			default: et = 'N';
			}

			cout<<"("<<i<<", "<<nd->destination->value<<", "<<et<< ") ";
		}
		cout<<endl;
	}
}
int DisplayTopology(LinkedList* graph, int V)
{
	for (int i = 0; i < V; i++)
	{
		for (LLNode* node = graph[i].nodeStart; node != NULL; node = node->next)
			if (node->edgeType == Back)
			{
				cout << "There is a back edge in the graph!";
				return 0;
			}
	}
	for (int i = V - 1; i >= 0; i--)
	{
		int index = topologyList[i];
		cout<<index<<": Start Time = "<<graph[index].source->startTime<<", End Time = "<<graph[index].source->finishTime<<endl;
	}
	return 1;
}

void main()
{
	int topologyResult = 0;
	fDFS = fopen("DFS.txt", "w");
	//fBFS = fopen("BFS.txt", "w");

	srand(time(NULL));
	LinkedList* graph;
	
    int V,E;
	V = 100; //pentru DFS
	//V = 5; //pentru BFS sample traversal

	/*************************** CASE 1 DFS *****************************/
	for (int E = 1000; E <= 5000; E += 100) // CASE 1 evaluation DFS
	//for (int E = 10; E <= 10; E++)  //sample code BFS
	{
		dfs_OPS = 0;
		cout<<"E="<<E<<endl; //sample code BFS & DFS
		AllocateGraph(&graph, V); //sample code BFS & DFS
		GenerateGraph(graph, E, V); //sample code BFS & DFS

	    DisplayAdjacencyList(graph, V); //sample code BFS & DFS
		for (int i = 0; i < V; i++)
			if (graph[i].source->visited == 0)
			{
				DFS(graph, graph[i].source);
			}
		fprintf(fDFS, "%d %d\n", E, dfs_OPS);

	//	//for (int i = 0; i < V; i++)
	//	//{
	//	//	if (graph[i].source->visited == 0)
	//	//	{
	//	//		queue = new TreeNode*[V];
	//	//		queue[0] = graph[0].source;
	//	//		queue[0]->startTime = 1;
	//	//		numberQueue = 1;
	//	//		BFS(graph); 
	//	//		free(queue);
	//	//	}
	//	//}

	//	DisplayEdgeLabels(graph, V);
	//	DisplayTopology(graph, V);

		DeallocateGraph(graph, V);
	}
	
	/*************************** CASE 2 DFS *****************************/
	//E = 9000;
	//while (topologyResult == 0) // decomenteaza pentru topologie
	//{// decomenteaza pentru topologie
	//E = 7;// decomenteaza pentru topologie
	//for (int V = 5; V <= 5; V++)// decomenteaza pentru topologie

	//for (int V = 110; V <= 200; V+=10)
	//{// decomenteaza pentru topologie
	//	dfs_OPS = 0;
	//	cout<<"V="<<V<<endl;// decomenteaza pentru topologie
	//	AllocateGraph(&graph, V);// decomenteaza pentru topologie
	//	GenerateGraph(graph, E, V);// decomenteaza pentru topologie

		//for (int i = 0; i < V; i++)
		//{
		//	bfs_OPS = 0;
		//	
		//	if (graph[i].source->visited == 0)
		//	{
		//		queue = new TreeNode*[V];
		//		queue[0] = graph[0].source;
		//		queue[0]->startTime = 1;
		//		numberQueue = 1;
		//		BFS(graph); 
		//		//fprintf(fBFS, "%d %d\n", V, bfs_OPS);
		//		free(queue);
		//	}
		//	//bfs_OPS = 0;
	
		//}
		
	//	DisplayAdjacencyList(graph, V);// decomenteaza pentru topologie
	//	for (int i = 0; i < V; i++)// decomenteaza pentru topologie
	//		if (graph[i].source->visited == 0)// decomenteaza pentru topologie
	//			DFS(graph, graph[i].source); //pentru DFS// decomenteaza pentru topologie
		
	//	fprintf(fDFS, "%d %d\n", V, dfs_OPS);
	//	DisplayEdgeLabels(graph, V);// decomenteaza pentru topologie
	//	topologyResult = DisplayTopology(graph, V);// decomenteaza pentru topologie
		//getch();

	//	DeallocateGraph(graph, V);//pentru BFS// decomenteaza pentru topologie
	//}
	//}

	/********************************************************************/
	fclose(fDFS);
	//fclose(fBFS);
	getch();
		
	
	
	}
