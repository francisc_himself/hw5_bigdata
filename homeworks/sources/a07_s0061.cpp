#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct nod{
    int val;
    nod *copil;
    int nr_ch;
};

struct binar{
    int val;
    binar *fch,*fbr;
};

nod *noduri;
int radacina;

void T1(int *t,int n)
{
    noduri=new nod[n];
    int pos[100],i;

    memset(pos,0,sizeof(pos)); 

    for(i=0;i<n;i++)
    {
        noduri[i].nr_ch=0;
        noduri[i].copil=NULL;
    }

    //  numarul copiilor creste cu 1 pentru cazul in care e mai mare sau egal cu 0
    for(i=0;i<n;i++)
        if(t[i]>=0)
            noduri[t[i]].nr_ch++;
        else
            radacina=i;

    
	//pentru nodurile care au un copil se face un nou nod in vectorul copil
    for(i=0;i<n;i++)
        if(noduri[i].nr_ch>0)
            noduri[i].copil=new nod[noduri[i].nr_ch];

    //
    for(i=0;i<n;i++)
    {
        noduri[i].val=i;
        if(t[i]>=0)
        {
            noduri[t[i]].copil[pos[t[i]]]=noduri[i];   //copilul de la pozitia valorii lui t[i] ia valoarea nodurilor de i
            pos[t[i]]++; // pozitia lui creste
        }
    }

}

void PFP(binar *r,int spatiu)
{
    int i;

    for(i=1;i<=spatiu;i++)
        printf(" ");

    printf("%d\n",r->val);

    if(r->fch!=NULL)
        PFP(r->fch,spatiu+1);

    if(r->fbr!=NULL)
        PFP(r->fbr,spatiu);
}

int main()
{
    int n=9,t[9]={1, 6, 4, 1, 6, 6, -1, 4, 1};


}
