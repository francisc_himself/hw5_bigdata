/*
	In the average case the bottom-up is better than top-down.Bottom-up is linear and has the complexity O(n) while 
	top-down is linear and has the complexity O(n).

	The worst case is when the array is ascendent ordered and the bottom-up is also better than top-down.Bottom-up has 
	the complexity O(n) and top-down O(n log n).

*/

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <conio.h>
#include "Profiler.h"
#define MAX 10000
using namespace std;

Profiler profiler("Heap");

int size,n;

void heapify(int A[],int i,int n){
	int l,r,largest,aux;
	l=2*i;//fiul din stanga
	r=2*i+1;//fiul din dreapta	

	profiler.countOperation("Bottom-up",n);
	if(l<=n && A[l]>A[i])
	{largest=l;}
	else 
	{largest=i;}

	profiler.countOperation("Bottom-up",n);
	if(r<=n && A[r]>A[largest])
	{largest=r;}


	if(largest!=i)
	{
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		profiler.countOperation("Bottom-up",n,3);
		heapify(A,largest,n);
	}
}

//porneste de la ultimul nod c***ANONIM*** ***ANONIM*** copii pana la radacina
void Bottomup(int A[],int n){
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}


void HeapIncreaseKey(int A[],int i, int key){
	int aux;
	A[i]=key;
	profiler.countOperation("Top-down",n);
	while(i>1 && A[i/2]<A[i]){//se verifica daca parintele e mai mic decat copilul
		profiler.countOperation("Top-down",n);	
		aux=A[i/2];
		A[i/2]=A[i];
		A[i]=aux;
		profiler.countOperation("Top-down",n,3);
		i=i/2;
	}
		profiler.countOperation("Top-down",n);
}
//se insereaza un element(creste marimea)
void MaxHeapInsert(int A[],int key){
	size++;
	A[size]=-36000;
	profiler.countOperation("Top-down",n);
	HeapIncreaseKey(A,size,key);//vede cat de sus poate sa trimita(se compara cu parintele)
}

void Topdown(int  A[],int n){
	int i;
	size=1;
	for(i=2;i<=n;i++)
		MaxHeapInsert(A,A[i]);
}

void print(int A[],int n)
{
	for (int i=1;i<=n;i++)
		printf("%d ",A[i]);
}

void average(){
	int A[MAX],B[MAX],C[MAX];
	int i,j;
	for(i=100;i<=10000;i=i+100)
	{
		FillRandomArray(A,i,0,10000);
		for(int k=1;k<=5;k++)
		{
			n=i;
			for(j=1;j<=n;j++)
			{
				B[j]=A[j];C[j]=A[j];
			}
			Bottomup(B,n);
			Topdown(C,n);
		}
	}
	profiler.createGroup("AverageOperations","Bottom-up","Top-down");
	profiler.showReport();
}

void worst(){
		int A[MAX],B[MAX],C[MAX];
		int i,j;
		for(i=100;i<=10000;i=i+100)
	{
		FillRandomArray(A,i,0,10000,false,1);
		for (j=1; j<=i;j++) { B[j]=A[j];C[j]=A[j]; }
			int n=i;
			Bottomup(B,n);
			Topdown(C,n);
	}
	profiler.createGroup("WorstCaseOperations","Bottom-up","Top-down");
	profiler.showReport();
}

void test(){
	int A[MAX],B[MAX],C[MAX];
	FillRandomArray(A,100,0,100);
	print(A,10);
	printf("\n");
	for(int j=1;j<=10;j++)
			{
				B[j]=A[j];
				C[j]=A[j];
			}
	Bottomup(B,10);
	Topdown(C,10);
	print(B,10);
	printf("\n");
	print(C,10);
	getch();
}

void main()
{
	test();
}
