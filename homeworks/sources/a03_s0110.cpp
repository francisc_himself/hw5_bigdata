#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include "time.h"

/*
    ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
    From the graphs we can observe that the quicksort is faster than the heapsort in the avarage case.
    But knowing that only heapsort is optimal and quicksort is not it can be proven from the worst case graphs(it takes O(n^2) more precisly O(2n^2) from the graphs).
    Worst case in the quicksort is when we have a sorted array and we set the pivot at the end so it will have n nr of partitions.
    The best case occurs when we have a sorted array and we set the pivot as the median of the array.

*/
int n, HeapSize;
int a[1000000];

long heapatr,heapcmp,quickatr,quickcmp,sumQ,sumH;

void swap(int &x,int &y)
{
    int tmp;
    tmp=x;
    x=y;
    y=tmp;
}
void heapify (int i)
{
    int largest=i,left=2*i,right=2*i+1;

    heapcmp++;
    if (left<=HeapSize && a[left]>a[largest])
        largest=left;
    heapcmp++;
    if (right<=HeapSize && a[right]>a[largest])
        largest=right;
    if (largest!=i)
    {
        heapatr=heapatr+3;
        swap(a[i],a[largest]);
        heapify(largest);
    }

}

void BottomUp()
{
    for (int i=HeapSize/2;i>0;i--)
        heapify(i);
}

void HeapSort()
{
    HeapSize=n;
    BottomUp();

    for (int i=n;i>1;i--)
     {
         heapatr+3;
         swap(a[1],a[i]);
         HeapSize--;
         heapify(1);
     }

}

int partition(int p,int r,char pivot)
{   int x;
    switch(pivot)
    {
        case 'A':x=a[r];break;
        case 'B':x=a[r];break;
        case 'W':x=a[r];break;
        default:x=a[r];break;
    }
    quickatr++;

    int i=p-1;
    for (int j=p;j<r;j++)
       {quickcmp++;
           if (a[j]<=x)
            {
                i++;
                quickatr=quickatr+3;
                swap(a[i],a[j]);
            }
        }
    quickatr=quickatr+3;
    swap(a[i+1],a[r]);
    return i+1;
}

void quicksort(int p,int r,char pivot)
{
    int q;
    if (p<r)
    {
        q=partition(p,r,pivot);
        quicksort(p,q-1,pivot);
        quicksort(q+1,r,pivot);
    }
}


int main()
{
    FILE *fs=fopen("avg.csv","w");
	FILE *wc=fopen("worst.csv","w");
	FILE *bc=fopen("best.csv","w");
	fprintf(fs,"n,HeapSort,QuickSort\n");
	fprintf(wc,"n,QuickSort\n");
	fprintf(bc,"n,QuickSort\n");
    srand(time(NULL));
   for (n=100;n<=10000;n=n+100)
    {
    for (int j=1;j<=5;j++)
        {
        quickatr=quickcmp=heapatr=heapcmp=0;

        for (int i=1;i<=n;i++)
            a[i]=rand()%1000;
         HeapSort();

         for (int i=1;i<=n;i++)
            a[i]=rand()%1000;
         quicksort(1,n,'A');

        sumQ=sumQ+quickatr+quickcmp;
        sumH=sumH+heapatr+heapcmp;
       }
     fprintf(fs,"%d,%d,%d\n",n,sumH/5,sumQ/5);
    }
    for (n=20;n<=21;n=n+100)
    {
        sumQ=0;
        quickatr=quickcmp=0;
        for (int i=1;i<=n;i++)
            a[i]=i;
         quicksort(1,n,'B');
         for (int i=1;i<=n;i++)
            printf("%d  ",a[i]);
        sumQ=quickatr+quickcmp;
        fprintf(bc,"%d,%d\n",n,sumQ);
    }
    for (n=20;n<=21;n=n+100)
    {
        sumQ=0;
        quickatr=quickcmp=0;
        for (int i=1;i<=n;i++)
            a[i]=i;
         a[1]=n;
         a[n]=1;
         quicksort(1,n,'W');
        sumQ=quickatr+quickcmp;
        fprintf(wc,"%d,%d\n",n,sumQ);
    }
}
