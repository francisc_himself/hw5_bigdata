#include <stdio.h>
#include <conio.h>
#include "stdlib.h"
#include "time.h"
//#include "profiler.h"
/*
***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
From the graph we observe that the selection sort and the insertion sort are both almost the same.
From the point of view of comparisons the insertion sort is better then the other two are almost the same.
Summing this up, as we observed in the avarage case the bubble sort is much worse then the seleciton sort and the insertion sort.
The best case for every sorting algorithm is has the form of an already sorted array,and they all behave the same n comparisons, no associations.
The worst case for the insertion sort and the bubble sort is an reverse ordered array.
The worst case for selection sort happens when the largest element is located at the beginning of the array and the rest of the elements of the array is sorted.
All the sortings have a running time of O(n^2);
*/



//Profiler profiler ("Sorting algorithms");
int insertiona,insertionc,selectiona,selectionc,bubblea,bubblec;

void insertionSort (int a[],int n)
{
	int j,buff;
	for (int i=1;i<n;i++)
	{
		buff=a[i];
		j=i-1;
	//	profiler.countOperation("insertionSort-atr",n);
		//profiler.countOperation("insertionSort-comp",n);
		insertiona++;
		insertionc++;
			while ((a[j] > buff) && (j>=0))
			{
				a[j+1]=a[j];
				j--;
				//profiler.countOperation("insertionSort-atr",n);
			//	profiler.countOperation("insertionSort-comp",n);
			insertiona++;
			insertionc++;
			}
	   a[j+1]=buff;
	   insertiona++;
	   //profiler.countOperation("insertionSort-atr",n);
	}
	return;
}

void selectionSort(int a[],int n)
{
	int pos,tmp;
	for (int j=0;j<n-1;j++)
	{
		pos=j;

		for (int i=j+1;i<n;i++)
		{//profiler.countOperation("selectionSort-comp",n);
		    selectionc++;
			if (a[i]<a[pos])
			{
				pos=i;
			}
		}
		if (pos!=j)
		{
			selectiona++;selectiona++;selectiona++;
			//profiler.countOperation("selectionSort-atr",n,3);
			tmp=a[j];
			a[j]=a[pos];
			a[pos]=tmp;
		}

	}
	return ;
}

void bubbleSort(int a[],int n)
{
	int swap,tmp;
	do
	{
		swap = 0;
		for (int i=0;i<n-1;i++)
		{
			//profiler.countOperation("bubbleSort-comp",n);
			bubblec++;
			if (a[i]>a[i+1])
			{
			    bubblea++;bubblea++;bubblea++;
				//profiler.countOperation("bubbleSort-atr",n,3);
				tmp=a[i];
				a[i]=a[i+1];
				a[i+1]=tmp;
				swap =1;
			}
		}
	//profiler.countOperation("bubbleSort-comp",n);
	//bubblec++;
	}
	while (swap==1);
	return ;
}

int main ()
{	int a[10000],n;
	//int a[]={4,3,2,5,1,8,6,10,7,9},n=10;
	/*printf("Enter the size of the vector:");
	scanf("%d",&n);
	printf("Enter the numbers:\n");
	for (int i=0;i<n;i++)
		scanf("%d",&a[i]);*/
	FILE *fa=fopen("atr.csv","w");
	FILE *fc=fopen("cmp.csv","w");
	FILE *fs=fopen("sum.csv","w");
	FILE *bc=fopen("best.csv","w");
	FILE *wc=fopen("worst.csv","w");
	fprintf(fa,"n,insertiona,selectiona,bubblea\n");
	fprintf(fc,"n,insertionc,selectionc,bubblec\n");
	fprintf(fs,"n,insertionall,selectionall,bubbleall\n");
	fprintf(bc,"n,insertionall,selectionall,bubbleall\n");
	fprintf(wc,"n,insertionall,selectionall,bubbleall\n");
	srand(time(NULL));
	for (n=100;n<=10000;n=n+100)
	{
	 insertiona=0;insertionc=0;selectiona=0;selectionc=0;bubblea=0;bubblec=0;

	 for (int i=0;i<=n;i++)
	    a[i]=rand()%1000;

    //FillRandomArray(a,n);
	insertionSort(a,n);
	for (int i=0;i<=n;i++)
	    a[i]=rand()%1000;
	selectionSort(a,n);
	for (int i=0;i<=n;i++)
	    a[i]=rand()%1000;
	bubbleSort(a,n);


	fprintf(fa,"%d,%d,%d,%d\n",n,insertiona,selectiona,bubblea);
	fprintf(fc,"%d,%d,%d,%d\n",n,insertionc,selectionc,bubblec);
	fprintf(fs,"%d,%d,%d,%d\n",n,insertiona+insertionc,selectiona+selectionc,bubblea+bubblec);
	insertiona=0;insertionc=0;selectiona=0;selectionc=0;bubblea=0;bubblec=0;


	//best case
	for (int i=0;i<=n;i++)
		a[i]=i;
	insertionSort(a,n);
	selectionSort(a,n);
	bubbleSort(a,n);
	/*profiler.addSeries("selectionSort-all","selectionSort-comp","selectionSort-atr");
	profiler.addSeries("insertionSort-all","insertionSort-comp","insertionSort-atr");
	profiler.addSeries("bubbleSort-all","bubbleSort-comp","bubbleSort-atr");
	profiler.createGroup("best case","selectionSort-all","insertionSort-all","bubbleSort-all");*/

	fprintf(bc,"%d,%d,%d,%d\n",n,insertiona+insertionc,selectiona+selectionc,bubblea+bubblec);
	insertiona=0;insertionc=0;selectiona=0;selectionc=0;bubblea=0;bubblec=0;


	//worst case
	for (int i=0;i<=10000;i++)
		a[i]=10000-i;
	insertionSort(a,n);
	for (int i=0;i<=10000;i++)
		a[i]=10000-i;
	bubbleSort(a,n);
	for (int i=1;i<=10000;i++)
		a[i]=i-1;
	a[0]=10000;
	selectionSort(a,n);
	fprintf(wc,"%d,%d,%d,%d\n",n,insertiona+insertionc,selectiona+selectionc,bubblea+bubblec);
	//profiler.addSeries("selectionSort-all","selectionSort-comp","selectionSort-atr");
	//profiler.addSeries("insertionSort-all","insertionSort-comp","insertionSort-atr");
	//profiler.addSeries("bubbleSort-all","bubbleSort-comp","bubbleSort-atr");
	//profiler.createGroup("worst case","selectionSort-all","insertionSort-all","bubbleSort-all");
	printf("%d",n);
	}

	/*for (int i=0;i<n;i++)
		printf("%d",a[i]);*/
	//printf("Press a key to exit!");
	/*profiler.addSeries("selectionSort-all","selectionSort-comp","selectionSort-atr");
	profiler.addSeries("insertionSort-all","insertionSort-comp","insertionSort-atr");
	profiler.addSeries("bubbleSort-all","bubbleSort-comp","bubbleSort-atr");
	profiler.createGroup("selectionSort-all","insertionSort-all","bubbleSort-all");
	profiler.showReport();
	profiler.createGroup("selectionSort-atr","insertionSort-atr","bubbleSort-atr");
	profiler.showReport();*/
	//getch();
	//profiler.showReport();
	return 0;
}
