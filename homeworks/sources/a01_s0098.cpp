#include<iostream>
#include<conio.h>
#include<fstream>

using namespace std;
//* After analysing the graphs, we can observe that the most efficient sort is the insertion sort
//*the least efficient algorithm is bubble sort despite the fact that in the best case it appears to be pretty efficient


/**
*linear
*it is a stable algorithm (if it would be >= at comparisons it won't be stable anymore)
*best case is for an ordered array
*worst case is for a decreasingly ordered array
*average case is for an array with random values
*best case
*minimum complexity for assignments = 2(n-2) which is O(n) => values will increase with 200 at each step
*minimum complexity for comparisons = n which is O(n) => values will increase with 100 (because n increases with 100) at each step
*increases linearly
*worst case
*maximum complexity for assignments = n(n-1)/2-1+2(n-1) which is O(n^2) => values will increase from 0 to 49029750
*maximum complexity for comparisons  is O(n^2) => values will increase from 0 to 49019850, so we can see that the values are approximately the same
*average case
*the values are smaller than the ones on the worst case but bigger than the ones on the best case
*/
void insertionSort(long v[], long n, long *a, long*c)
{

	long buff;
	int j,i;

	for( i=2; i<=n; i++)
	{
		buff=v[i];
		(*a)++;
		j=i-1;
		(*c)++;
		while(v[j] > buff && j>0)
		{
			v[j+1]=v[j];
			(*a)++;
			(*c)++;
			j--;
		}
		v[j+1]=buff;
		(*a)++;
	}

}

/**
*it is a stable algorithm (if it would be <= at comparisons it won't be stable anymore)
*best case is for an ordered array
*worst case is for a decreasingly ordered array until the middle and increasingly ordered array from the middle to the last position
*average case is for an array with random values
*cheap in terms of assignment
*comparisons for the best case and the worst case are the same as we can see on the resulted values, increase from 0 to 49009950
*look the same on the chart for best and worst case
*best case
*minimum complexity for assignments = 0 => values will be =0 at every n
*complexity for comparisons = n^2 - n(n-1)/2-(n-1) which is O(n^2) => values will increase 0 to 49009950
*worst case
*complexity for comparisons = n^2 - n(n-1)/2-(n-1) which is O(n^2) => values will increase 0 to 49009950
*maximum complexity for assignments is = 3*n which is O(n) 
*average case
*the values are smaller than the ones on the worst case but bigger than the ones on the best case
*the values have the least significant increase (being compared to insertion assignments and bubble assignments)
*/
void selectionSort(long v[], long n, long *a, long *c)
{
	int i,j,pos,aux;
	for(j=1; j<n; j++)
	{
		pos=j;
		for(i=j+1; i<=n;i++)
		{
			
			if(v[i]<v[pos])
			 pos=i;
			(*c)++;
		}
            if(j!=pos)
            {
                aux=v[j];
				v[j]=v[pos];
				v[pos]=aux;
				(*a)=(*a)+3;

			}
		}
	}

/**
*it is a stable algorithm (if it would be <= at comparisons it won't be stable anymore)
*best case is for an ordered array
*worst case is for a decreasingly ordered array 
*average case is for an array with random values
*best case
*minimum complexity for assignments = 0 => values will be =0 at every n
*minimum complexity for comparisons = n which is O(n) => values will increase with 100 (because n increases with 100) at each step
*comparisons increase liniarly
*worst case
*complexity for comparisons = n^2 - n(n-1)/2-(n-1) which is O(n^2) => values will increase 0 to 49009950
*maximum complexity for assignments is = O(n^2) 
*assignments for bubble sort increase most significantly(being compared to insertion assignments and selection assignments)
*average case
*the values are smaller than the ones on the worst case but bigger than the ones on the best case
*the assignments have the most significant increase (being compared to insertion assignments and selection assignments)
*/
void bubbleSort2(long v[], long n, long *a, long*c)
{
	
	long t;
	int ok=0;
	do
	{
		ok=1;
	
		for(int i=1;i<n ;i++)
		{	
			if(v[i]>v[i+1])
		{
			t=v[i];
			v[i]=v[i+1];
			v[i+1]=t;
			ok=0;
			(*a)=(*a)+3;
		}
			(*c)++;
		
			
		}
		n--;  // used to increase efficiency
		
	}while(!ok) ;
}
void main()
{
	ofstream f("output.txt");
	long a=0,c=0,v[7],n=6;
	long ai=0,ci=0,as=0,cs=0,ab=0,cb=0;
	int k,l;
	ofstream bestCase("best.txt");
	ofstream worstCase("worst.txt");
	ofstream averageCase("average.txt");
    long outi[10000], outs[10000], outb[10000];
	v[1]=3;
	v[2]=0;
	v[3]=6;
	v[4]=9;
	v[5]=1;
	v[6]=8;
	
	//best case
	bestCase<<"n     ai     ci   ai+ci     as   cs   as+cs   ab   cb   ab+cb \n";
	for(int i=1; i<10000; i=i+100)
	{
		for(int j=1; j<=i; j++)
		{
			outi[j]=j;
			outs[j]=j;
			outb[j]=j;
		}
		
		insertionSort(outi,i,&ai,&ci);
		selectionSort(outs,i,&as,&cs);
		bubbleSort2(outb,i,&ab,&cb);
	
  		bestCase<<i<<"      "<<ai<<"     "<<ci<<"     "<<ai+ci<<"           "<<as<<"             "<<cs<<"           "<<as+cs<<"               "<<ab<<"                "<<cb<<"                "<<ab+cb<<"\n";
		
		ai=0;
		ci=0;
		as=0;
		cs=0;
		ab=0;
		cb=0;
	}

	//worst case
	worstCase<<"n        ai       ci        ai+ci         ab          cb        ab+cb \n";
	for(int i=1; i<10000; i=i+100)
	{ k=0;
		for(int j=i; j>=1; j--)
		{   
			k++;
			outi[k]=j;
			outs[k]=j;
			outb[k]=j;
		}
		
		insertionSort(outi,i,&ai,&ci);
		bubbleSort2(outb,i,&ab,&cb);
  	    
		worstCase<<i<<"      "<<ai<<"     "<<ci<<"     "<<ai+ci<<"            "<<ab<<"               "<<cb<<"                "<<ab+cb<<"\n";
	
		ai=0;
		ci=0;
		as=0;
		cs=0;
		ab=0;
		cb=0;
	}
	
	 k=1;
	//worst case SELECTION
	f<<"n     as           cs          as+cs \n";
	for(int i=1; i<10000; i=i+100)
	{k=1;
		for(int j=i; j>i/2; j--)
		{   
			outs[k]=j;
			k++;
		}
		
		for(int j=1; j<=i/2; j++)
			{outs[k]=j;
		k++;
		}
		
	     selectionSort(outs,i,&as,&cs);
		
		f<<i<<"      "<<as<<"     "<<cs<<"     "<<as+cs<<"\n";
	
		
		as=0;
		cs=0;
		
	}
	
	
	averageCase<<"n        ai        ci      ai+ci           as          cs          as+cs          ab           cb     ab+cb\n";
	
	 for (int i=1;i<=10000;i=i+100)
	{
		long sumcb=0,sumci=0,sumcs=0,sumab=0,sumai=0,sumas=0;
		for (int j=1;j<=5;j++)
		{
			l=0;
			for ( k=1;k<=i;k++)
			{
				l++;
				outb[l]=rand()%i;
				outi[l]=rand()%i;
				outs[l]=rand()%i;
				
			}
			
			insertionSort(outi,i,&ai,&ci);
			selectionSort(outs,i,&as,&cs);
			bubbleSort2(outb,i,&ab,&cb);
			sumcs+=cs;
			sumas+=as;
			sumci+=ci;
			sumai+=ai;
			sumcb+=cb;
			sumab+=ab;
			ai=0;
			ci=0;
			as=0;
			cs=0;
			ab=0;
			cb=0;
		}
			averageCase<<k<<"             "<<sumai/5<<"             "<<sumci/5<<"                "<<(sumai+sumci)/5<<"                  "<<sumas/5<<"             "<<sumcs/5<<"               "<<(sumas+sumcs)/5<<"             "<<sumab/5<<"                  "<<sumcb/5<<"                     "<<(sumab+sumcb)/5<<"\n";
	}


}
