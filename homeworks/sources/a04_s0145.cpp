/*
Print introducerea unei structuri de heap, operatia
de cautare a minimului dintre cele k list este redus 
la complexitate O(ln(k))
Totusi, crestera numarul de liste dar total avand aceleasi
numar de elemente, performata tot scade.

Pentru 9000 de elemente in 5 liste programul a numarat
156595 de operati. Cand aceleasi 9000 de elemente sunt in
10 liste, numarul de operatii este 278812. 

La dublarea numarului de liste, numarul de operatii a
crescut 1.78 de ori. Din acesta se vede ca numarul operatiilor nu
depinde liniar de numarul listelor.

Acesta diferenta se poate observa si mai bine
daca luam cazul cand k = 10 si k = 100.

Cand k = 10 numarul de operatii este 278812. Cand
numarul de liste este inmultit cu 10.0 numarul de operatii
creste la 1880788. Cresterea este de numai 6.71 ori, 
crestere si mai putina cu cazul anterior.

Rezultatele din grafice arata ca intradevar, algoritmul
are complexitate mai buna decat O(n). Acesta poate fii observat
din graficul in care numarul listelor merge pana la 1000. 
Funtia incepe sa incetineasca la numere mai mari.

*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int counter = 0;

//#define movs(a,b) memcpy(a,b,sizeof(a));

inline void swap(int &a, int &b)
{
	int x = a;
	a = b;
	b = x;
}

class dvector
{
public:
	int* data;
	int size;

	dvector(dvector& other)
	{
		size = other.size;
		data = new int[size];
		memcpy(data,other.data,size*sizeof(int));
	}

	dvector()
	{
		size=0;
		data=0;
	}

	~dvector()
	{
		if (data!=0)
		{
			delete[] data;
		}
	}

	int& operator [] (int i)
	{
		if (i<size)
		{
			return data[i];
		}
		else
		{
			int *ndata = new int[i+1];

			if (data != 0)
			{
				memcpy(ndata,data,size*sizeof(int));
				delete[] data;
			}
			for (int j = size;j<i+1; j++)
			{
				ndata[j]=0;
			}
			data=ndata;
			size=i+1;
			return data[i];
		}
	}

	void print()
	{
		for (int i=0; i<this->size; i++)
		{
			printf("%d ",data[i]);
		}
		printf("\n");
	}

	void heapsort();

	void prealloc(int size)
	{
		if (size<this->size)
		{
			size--;
			(*this)[size]=0;
		}
	}

	void quicksort()
	{
		quicksort(0,size-1);
	}

	int partition(int left, int right)
	{
		int x = data[right];
		int i = left-1;
		int j;
		for (j=left; j<right; j++)
		{
			counter++;
			if (data[j]<=x)
			{
				i++;
				counter+=3;
				swap(data[i],data[j]);
			}
		}
		counter+=3;
		swap(data[i+1],data[j]);
		return i+1;
	}

	void quicksort(int left, int right)
	{
		if (left<right)
		{
			int mid = partition(left,right);
			quicksort(left,mid-1);
			quicksort(mid+1,right);
		}
	}

	void quicksort_better()
	{
		quicksort_better(0,size-1);
	}

	int partition_better(int left, int right)
	{
		int x = 0;
		int c = 0;
		for (int i=0; i<4 && left+i<right; i++)
		{
			x+=data[left+i];
			c++;
		}
		for (int i=0; i<4 && left<right-i; i++)
		{
			x+=data[right-i];
			c++;
		}
		x = x / c;

		int i = left-1;
		int j;
		for (j=left; j<right; j++)
		{
			counter++;
			if (data[j]<=x)
			{
				i++;
				counter+=3;
				swap(data[i],data[j]);
			}
		}
		counter+=3;
		swap(data[i+1],data[j]);
		return i+1;
	}

	void quicksort_better(int left, int right)
	{
		if (left<right)
		{
			int mid = partition_better(left,right);
			quicksort_better(left,mid-1);
			quicksort_better(mid+1,right);
		}
	}

};

class heap
{
public:
	dvector* vec;
	int heap_size;
	int direction;
	
	heap()
	{
		vec = new dvector;
		heap_size=0;
		direction=0;
	}
	~heap()
	{
		delete vec;
	}
	int& operator [] (int i)
	{
		int &d = (*vec)[i];
		
		if (i>heap_size)
		{
			heap_size=i;
		}
		
		return d;
	}

	void max_up_heapify(int i)
	{
		if (i>1) //boundary check
		{
			int &a = (*vec)[i]; //current key
			int &b = (*vec)[i>>1]; //parent key
			counter ++;
			if (b<a)
			{
				counter +=3;
				swap(a,b);
				//print(); //step by step print;
				max_up_heapify(i>>1);
			}
		}
	}

	void max_down_heapify(int i)
	{
		int ch[2];
		ch[0] = i<<1;
		ch[1] = ch[0]+1;
		for (int k=0; k<2; k++) //for each immediate child
		{
			int &c = ch[k];
			if (c<=heap_size) //boundary check
			{
				int& iv = (*this)[i];
				int& cv = (*this)[c];
				counter++;
				if (iv<cv)
				{
					counter+=3;
					swap(iv,cv);
					//print();//step by step print
					max_down_heapify(c);
				}
			}
		}
	}

	void min_up_heapify(int i)
	{
		if (i>1) //boundary check
		{
			int &a = (*vec)[i]; //current key
			int &b = (*vec)[i>>1]; //parent key
			counter ++;
			if (b>a)
			{
				counter +=3;
				swap(a,b);
				//print(); //step by step print;
				min_up_heapify(i>>1);
			}
		}
	}

	void min_down_heapify(int i)
	{
		int ch[2];
		ch[0] = i<<1;
		ch[1] = ch[0]+1;
		for (int k=0; k<2; k++) //for each immediate child
		{
			int &c = ch[k];
			if (c<=heap_size) //boundary check
			{
				int& iv = (*this)[i];
				int& cv = (*this)[c];
				counter++;
				if (iv>cv)
				{
					counter+=3;
					swap(iv,cv);
					//print();//step by step print
					min_down_heapify(c);
				}
			}
		}
	}

	void bottom_up()
	{
		direction = 0;
		for (int i=this->heap_size>>1; i>0; i--)
		{
			max_down_heapify(i);	
		}
	}

	void top_down()
	{
		direction = 0;
		for (int i=1; i<=this->heap_size; i++)
		{
			max_up_heapify(i);
		}
	}

	void max_bottom_up()
	{
		direction = 0;
		bottom_up();
	}

	void max_top_down()
	{
		direction = 0;
		top_down();
	}

	void min_bottom_up()
	{
		direction = 1;
		for (int i=this->heap_size>>1; i>0; i--)
		{
			min_down_heapify(i);	
		}
	}

	void min_top_down()
	{
		direction = 1;
		for (int i=1; i<=this->heap_size; i++)
		{
			min_up_heapify(i);
		}
	}

	void set_data(dvector *vec2)
	{
		delete this->vec;
		vec=new dvector(*vec2); //copy
		heap_size=vec->size;

		(*vec)[vec->size]=0;

		for (int i=heap_size; i>0; i--)//align
		{
			vec->data[i] = vec->data[i-1];
		}
		(*vec)[0]=-1;
	}

	void bottom_up(dvector *vec2)
	{
		set_data(vec2);
		bottom_up();
	}

	void top_down(dvector *vec2)
	{
		set_data(vec2);
		top_down();
	}

	void max_bottom_up(dvector *vec2)
	{
		set_data(vec2);
		bottom_up();
	}

	void max_top_down(dvector *vec2)
	{
		set_data(vec2);
		top_down();
	}

	void min_bottom_up(dvector *vec2)
	{
		set_data(vec2);
		min_bottom_up();
	}

	void min_top_down(dvector *vec2)
	{
		set_data(vec2);
		min_top_down();
	}

	void insert(int value)
	{
		(*this)[heap_size+1]=value;		
		max_up_heapify(heap_size);
	}

	void insert_min(int value)
	{
		(*this)[heap_size+1]=value;		
		min_up_heapify(heap_size);
	}

	void print(int i,int depth=0)
	{
		int c1 = i<<1;
		int c2 = c1+1;

		if (c1<=heap_size)
			print(c1,depth+1);
	
		for(int k=0; k<depth; k++)
			printf("\t");

		printf("%d\n",(*vec)[i]);

		if (c2<=heap_size)
			print(c2,depth+1);
	}

	void print()
	{
		print(1,0);
	}

	int pop()
	{
		counter++;

		int x = (*vec)[1];
		(*vec)[1] = (*vec)[heap_size];
		heap_size--;
		if (direction == 0)
			this->max_down_heapify(1);
		else
			this->min_down_heapify(1);
		return x;
	}

	int pop_min()
	{
		counter++;

		int x = (*vec)[1];
		(*vec)[1] = (*vec)[heap_size];
		heap_size--;
		this->min_down_heapify(1);
		return x;
	}

	int pop_max()
	{
		counter++;

		int x = (*vec)[1];
		(*vec)[1] = (*vec)[heap_size];
		heap_size--;
		this->max_down_heapify(1);
		return x;
	}

	void push(int x)
	{
		heap_size++;
		(*vec)[heap_size]=x;
		this->max_up_heapify(heap_size);
	}

	void push_max(int x)
	{
		direction = 0;
		heap_size++;
		(*vec)[heap_size]=x;
		this->max_up_heapify(heap_size);
	}

	void push_min(int x)
	{
		direction = 1;
		heap_size++;
		(*vec)[heap_size]=x;
		this->min_up_heapify(heap_size);
	}
};

void dvector::heapsort()
{
	heap h;
	h.max_bottom_up(this);

	//h.print();

	for (int i=0; i<size; i++)
	{
		int n = h.pop();
		(*this)[size-i-1]=n;
	}
	
}


class node
{
public:
	int key;
	//void *data;
	node* prev;
	node* next;
	
	node()
	{
		key = 0;
		//data = 0;
		next = prev = this;
	}

	node(int key)
	{
		this->key = key;
		next = prev = this;
	}

	~node()
	{
		unlink();
	}

	void unlink()
	{
		this->next->prev = prev;
		this->prev->next = next;
	}

	void insertAfter(node* x)
	{
		x->prev = this;
		x->next = this->next;
		this->next->prev = x;
		this->next = x;
	}

	void insertBefore(node *x)
	{
		x->next = this;
		x->prev = this->prev;
		this->prev->next = x;
		this->prev = x;
	}

};

class list
{
	int size;
	node* first;
public:

	list()
	{
		first = 0;
		size = 0;
	}

	list(const list& other)
	{
		this->first=0;
		this->size=0;

		if (other.first)
		{
			node* i = other.first;
			do
			{
				this->insertLast(i->key);
				i = i->next;
			}
			while (i!=other.first);
		}
		else
		{
			printf("NULL");
		}
	}

	~list()
	{
		while (!isEmpty())
		{
			removeFirst();
		}
	}

	void print()
	{
		if (first)
		{
			node* i = first;
			do
			{
				printf("%d ",i->key);
				i = i->next;
			}
			while (i!=first);
		}
		else
		{
			printf("NULL");
		}
	}


	void println()
	{
		print();
		printf("\n");
	}

	void insertFirst(int key)
	{
		if (first)
		{
			first->insertBefore(new node(key));
			first = first->prev;
			size++;
		}
		else
		{
			this->first = new node(key);
			size = 1;
		}
	}

	void insertLast(int key)
	{
		if (first)
		{
			first->insertBefore(new node(key));
			size++;
		}
		else
		{
			first = new node(key);
			size = 1;
		}
	}

	int removeFirst()
	{
		counter+=2;
		if (first)
		{
			size--;
			node *x = first;
			first = first->next;
			if (first == x)
			{
				first = 0;
				size = 0;
			}
			x->unlink();
			int retval = x->key;
			delete x;
			return retval;
		}
		else
		{
			return 0;
		}
	}

	bool isEmpty()
	{
		return size == 0 || first == 0;
	}

	int length()
	{
		return size;
	}

};


/////lab4

list** generateLists(int n, int k)
{
	//n - total element count
	//k - list count

	list** x = new list*[k];

	for (int i=0; i<k; i++)
	{
		x[i] = new list;
	}

	int generated = 0;

	for (int i=0; i<n; i++)
	{
		generated += rand()%10;
		int list_index = rand()%k;
		x[list_index]->insertLast((generated<<10)|list_index);
	}

	return x;
}

void killLists(list** x,int k)
{
	//k - list count
	for (int i=0; i<k; i++)
	{
		delete x[i];
	}
}

void printLists(list** x,int k)
{
	for (int i=0; i<k; i++)
	{
		x[i]->println();
	}
}

int test(int n, int k)
{
	

	heap *h = new heap();
	list *full = new list();

	list** temp = generateLists(n,k);
	//printf("\n");
	//printLists(temp,k);
	//printf("\n");
	counter = 0;

	for (int i=0; i<k; i++)
	{
		if (!temp[i]->isEmpty())
		{
			h->push_min(temp[i]->removeFirst());
		}
	}

	//h->print();

	//printf("\n");

	while (h->heap_size>0)
	{
		int x = h->pop_min();

		//h->print();

		int index = x&((1<<10)-1);
		if (!temp[index]->isEmpty())
			h->push_min(temp[index]->removeFirst());
		full->insertLast(x);
		/*
		printLists(temp,k);
		h->print();
		full->println();
		printf("\n");*/
	}
	killLists(temp,k);

	//full->println();
	delete full;
	delete h;

	return counter;
}

int main()
{
	list *x = new list();

	for (int i=0; i<10; i++)
	{
		x->insertLast(i);
	}
	
	list *y = new list(*x);

	y->println();

	while (!x->isEmpty())
	{
		printf("%d was removed, remaning: ",x->removeFirst());
		x->print();
		printf("\n");
	}
	delete x;
	delete y;

	heap *h = new heap();

	int k = 10;
	list *full = new list();

	list** temp = generateLists(50,k);
	printf("\n");
	printLists(temp,k);
	printf("\n");

	for (int i=0; i<k; i++)
	{
		if (!temp[i]->isEmpty())
		{
			h->push_min(temp[i]->removeFirst());
		}
	}

	h->print();

	printf("\n");

	while (h->heap_size>0)
	{
		int x = h->pop_min();

		//h->print();

		int index = x&((1<<10)-1);
		if (!temp[index]->isEmpty())
			h->push_min(temp[index]->removeFirst());
		full->insertLast(x);
		/*
		printLists(temp,k);
		h->print();
		full->println();
		printf("\n");*/
	}
	killLists(temp,k);

	full->println();
	delete full;
	//f(n) k = 5 10 100
	//f(k) n = 10000

	printf("first f(n), k = 5\n n = ");
	{
		FILE *out = fopen("first.txt","w");
		int k = 5;
		fprintf(out,"n,op,k=%d\n",k);
		for (int n = 100; n<10000; n+=100)
		{
			printf("%d ",n);
			fprintf(out,"%d,%d\n",n,test(n,k));
		}
		printf("\n");
		fclose(out);
	}

	printf("second f(n), k = 10\n n = ");
	{
		FILE *out = fopen("second.txt","w");
		int k = 10;
		fprintf(out,"n,op,k=%d\n",k);
		for (int n = 100; n<10000; n+=100)
		{
			printf("%d ",n);
			fprintf(out,"%d,%d\n",n,test(n,k));
		}
		printf("\n");
		fclose(out);
	}

	printf("third f(n), k = 100\n n = ");
	{
		FILE *out = fopen("third.txt","w");
		int k = 100;
		fprintf(out,"n,op,k=%d\n",k);
		for (int n = 100; n<10000; n+=100)
		{
			printf("%d ",n);
			fprintf(out,"%d,%d\n",n,test(n,k));
		}
		printf("\n");
		fclose(out);
	}

	printf("fourth f(k), n = 10000\n k = ");
	{
		FILE *out = fopen("fourth.txt","w");
		int n = 10000;
		fprintf(out,"k,op,n=%d\n",n);
		for (int k = 10; k<1000; k+=10)
		{
			printf("%d ",k);
			fprintf(out,"%d,%d\n",k,test(n,k));
		}
		printf("\n");
		fclose(out);
	}


	return 0;
}