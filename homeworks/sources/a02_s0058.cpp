#include <conio.h>
#include <stdio.h>
#include <iostream>
using namespace std;
#include "Profiler.h"
Profiler profiler("demo");


void max_heapify(int *v, int i,int p)
{
 int l,r,x;
 int largest=0;
 l=2*i;
 r=2*i+1;
 
 int heapsize=i;
 profiler.countOperation("top-down", p,4);
 profiler.countOperation("bottom-up", p, 4);
 if ((l<=heapsize) && (v[l]>v[i]))
 {
	 largest=l;
	 profiler.countOperation("top-down", p);
	 profiler.countOperation("bottom-up", p);
 }
 else
 {
	 largest=i;
	 profiler.countOperation("top-down", p);
	 profiler.countOperation("bottom-up", p);
 }
 
 if ((r<=heapsize) && (v[r]>v[largest]))
 {
	 largest=r;
	 profiler.countOperation("top-down", p);
	 profiler.countOperation("bottom-up", p);
 }
 if (largest!=i)
 {
	 x=v[i];
	 v[i]=v[largest];
	 v[largest]=x;
	 profiler.countOperation("top-down", p,3);
	 profiler.countOperation("bottom-up", p, 3);
	 max_heapify(v,i,p);
 }
}
void build_max_heap_bottom_up(int *v, int p)
{
	int heapsize=p;
	profiler.countOperation("bottom-up", p);
	for (int i=(p/2);i>=1;i--)
	{
		max_heapify(v,i,p);
	}
}

/* Build-Heap(Item A[n], i) 
{ 
 if(i = (n-2)/2) 
{ 
Build-Heap(A, 2i+1); 
Build-Heap(A, 2i+2); 
Heapify(A,i); 
} 
 }  */


void build_max_heap_top_down(int *v,int p)
{
	

	profiler.countOperation("top-down", p);
	
	for(int i=0;i<=p;i++)
	{
		profiler.countOperation("top-down", p);
		max_heapify(v,i,p);
	}

		/*if(i==(p-2)/2)
		{
			build_max_heap_top_down(v, 2*i+1,p);
			build_max_heap_top_down(v, 2*i+2,p);
			max_heapify(v, i,p );
		
	}*/
}

void afisare(int *v, int n, int k, int nivel)
{
	if (k>n)
	{
		return;
	}
	afisare(v,n,2*k+1,nivel+1);
	for (int i=1;i<=nivel;i++)
	{
		printf("   ");
	}
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);

}
void main()
{
	//int test[]={1,15,10,9,3,4,5,8};
	//int p=7;
	///*build_max_heap_bottom_up(test,p);*/
	//build_max_heap_top_down(test,p);
	//afisare(test,p,1,0);

	int n;
	int v[10000],v1[10000];
	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
		build_max_heap_bottom_up(v1,n);
		memcpy(v1,v,n*sizeof(int));
		build_max_heap_top_down(v1,n);
		
	}
	profiler.createGroup("comp","top-down","bottom-up");
	profiler.showReport();

}