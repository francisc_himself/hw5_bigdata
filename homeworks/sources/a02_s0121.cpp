/*
	Name: ***ANONIM*** ***ANONIM***
	Group: ***GROUP_NUMBER***
	
	Comparison of bottom-up and top-down heap construction methods:

	-As is, the code runs the algorithms on a small predifined set to check for correctness.
	-To rerun the analysis, the code sections at the bottom have to be uncommented one at a time to perform the appropriate analysis case.

	Analysis of results:
	
	Overall, as total number of operations in the average case, the bottom-up heap construction method performs about half the number
	of operations compared to the top-down one.
	Therefore the main advantage of the bottom-up technique is its speed, the construction taking O(n) time, while the top-down method takes O(nlgn).
	The drawback of the bottom-up method is that it only works on sets of a fixed dimension, but its speed makes it the better choice for sorting, 
	while the variable dimension of the top-down method makes it useful for priority queues.
	The worst case for both heap building methods is when the nodes have to move all the way down (for bottom-up) or up (top-down) the height of the tree.

	The algorithms build different heaps.
	


*/




#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h> 

int A[20]={3,6,12,0,1,2,55,32,10,12,9,1,14,15,19,20,21,18,31,40};
int A2[20]={3,6,12,0,1,2,55,32,10,12,9,1,14,15,19,20,21,18,31,40};
int temp[21];
int temp2[21];
int size;
int s;
FILE *f;
int bot_tot,top_tot;


#define MAX_SIZE 10000


void heapify(int arr[], int i, int dim)
{
	int n=dim; //sizeof(arr)/sizeof(arr[0]); //length of array
	//printf("%d",n );
	int left, right;
	int max=0;
	int buff;

	left = 2*i; right=2*i+1;
	
	bot_tot++;

	if (left<=n && arr[left]>arr[i])
		max=left;
	else
		max=i;

	bot_tot++;
	if (right<=n && arr[right]>arr[max])
		max=right;
	if (max!=i)
	{
	buff=arr[i];
	arr[i]=arr[max];
	arr[max]=buff;

	bot_tot=bot_tot+3;

	heapify(arr,max,dim);
	}

}

void increaseKey(int a[], int i, int key)
{
	int buff;
	top_tot++;
	if (key<a[i])
		{
			printf("error, key smaller than current key\n");
			return;
		}
	top_tot++;
	a[i]=key;

	while (i>1 && a[i/2]<a[i])
	{
		buff=a[i];
		a[i]=a[i/2];
		a[i/2]=buff;
		i=i/2;

		top_tot=top_tot+4;
	}
	top_tot++;

}

void maxHeapInsert(int a[], int key)
{
	size=size+1;
	a[size]=-32000;
	top_tot++;
	increaseKey(a, size, key);
}


void buildBot(int arr[], int out[], int dim)
{	
	int i;
	int n=dim;

	for (int j=1; j<n+1; j++)
	{
		out[j]=0;
	}

	//sizeof(arr)/sizeof(arr[0]);
	for (int j=0; j<dim; j++)
	{
		out[j+1]=arr[j];
	}

	for (i=n/2; i>=1; i--)			//i=((n-1)-1)/2; i>=0; i--
		heapify(out, i, dim);
}

void buildTop(int arr[], int out[], int dim)
{
	
	int n=dim;
	
	for (int i=1; i<n+1; i++)
	{
		out[i]=0;
	}

	size=1;
	out[1]=arr[0];
	for (int i=1; i<n; i++)
		maxHeapInsert(out, arr[i]);

}



void printHeap(int arr[], int i, int lvl, int dim)
{
    if (i <= dim && i > 0)
        {
        printHeap(arr, 2*i+1, lvl + 1,dim );
        for (int j = 0; j<= lvl; j++ )
            printf( "          " ); 
        printf( "%d \n", arr[i]);
        printHeap(arr, 2*i, lvl + 1,dim );
    }
}



void main()
{
	
buildBot(A2,temp2,20);
buildTop(A,temp,20);


for (int i=1; i<21; i++)
{
	printf("%d ", temp2[i]);
}
printf("\n");

for (int i=1; i<21; i++)
{
	printf("%d ", temp[i]);
}

printf("\n");

//for (int i=0; i<20; i++)
//{
//	printer[i+1]=A2[i];
//}

printHeap(temp2,1,1,20);

printf("\n");
printf("\n");
printf("\n");
printf("\n");
printHeap(temp,1,1,20);


//Analysis starts here

f = fopen("out1.txt", "w");
fprintf(f, "n,bot_up,top_down\n");

int v1[MAX_SIZE];
int v2[MAX_SIZE];
int t1[MAX_SIZE];
int t2[MAX_SIZE];
srand (time(NULL));

/*
// Average case
for (int m=0; m<5; m++)
	{

		for (s=100; s<MAX_SIZE; s+=200)
			{
				for (int i=1; i<s; i++)
					{
						v1[i]=rand() % 10000 +1;
						v2[i]=v1[i];
						

					}
				bot_tot=0;
				top_tot=0;
				//
				//
				buildBot(v1,t1,s);
				buildTop(v2,t2,s);
				//printf("%d ",s);
				fprintf(f, "%d,%d,%d\n",s,bot_tot,top_tot);
			}
		fprintf(f,"\n");
		

	}
*/
//Worst case
//not fully done
/*
for (s=100; s<MAX_SIZE; s+=200)
			{
				for (int i=1; i<s; i++)
					{
						v1[i]=i;
						v2[i]=i;
						

					}
				bot_tot=0;
				top_tot=0;
				//
				//
				buildBot(v1,t1,s);
				buildTop(v2,t2,s);
				//printf("%d ",s);
				fprintf(f, "%d,%d,%d\n",s,bot_tot,top_tot);
			}



*/
}