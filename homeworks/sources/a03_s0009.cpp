/*heapsort: O(n*lnn) mediu
quicksort:

best si mediu : O(n*lnn)
worst:O(n^2)

*/

#include <stdio.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <fstream>
#include <iostream>
#include <deque>
#include <iomanip>
#include <sstream>
#include <string>
#include <cmath>





#define  MINUS_INFINIT -32000000
#define MAX_SIZE 10000

int a[MAX_SIZE];//defavorabil
int b[MAX_SIZE];//mediu
int c[MAX_SIZE];//favorabil

int heapsize;
unsigned long long atr_heap=0,cmp_heap=0,atr_quick=0,cmp_quick=0;
unsigned long long atr_favo,cmp_favo;
int k=0;




int parinte(int i) {

     return (i-1)/2;
}

int stanga(int i)
{
return 2*i+1;
}

int dreapta(int i)
{
	return 2*i+2;
}






void interschimba(int *a,int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}

//----------------------------------------BOTTOM-UP-------------------------------------------------------------------

void maxHeapify(int a[],int i,int length)
{
	int largest;
	int l=stanga(i);
	int r=dreapta(i);
	cmp_heap++;
	if (l<heapsize && a[l]>a[i])
			largest=l;
	else largest=i;
	cmp_heap++;
	if (r<heapsize && a[r]>a[largest])
		largest=r;
	if (largest!=i)
	{
		atr_heap=atr_heap+3;
		interschimba(&a[i],&a[largest]);
		//printf("\n%d <=> %d\n",a[i],a[largest]);
		maxHeapify(a,largest,length);
	}
}


void buildMaxHeap(int a[],int length)
{
	int i;
	heapsize=length;
	for (i=length/2-1; i>=0; i--)
	{
		//printf("\n%d \n",i);
		maxHeapify(a,i,length);
	}
}

void heapSort(int a[],int length)
{
	heapsize=length;
	buildMaxHeap(a,length);
	int i;
	for(i=length-1; i>=1;i--)
	{
		atr_heap=atr_heap+3;
		interschimba(&a[0],&a[i]);
		heapsize--;
		maxHeapify(a,0,length);
	}

}

//----------------------------------------------QUICK-SORT----------------------------------------------------------------




int Partition(int a[],int p,int r)
{
	atr_quick++;
	int x;
	int temp;
	x=a[r];
	

	int i=p-1;

	int j;
	for (j=p; j<=r-1;j++)
	{

		cmp_quick++;
		if (a[j]<=x)
		{
			
			atr_quick=atr_quick+3;
			i++;
			
			interschimba(&a[i],&a[j]);
			
		}
	}
	atr_quick=atr_quick+3;
	interschimba(&a[i+1],&a[r]);
	
	return i+1;
}


void quickSort(int a[],int p,int r)
{
	int q;
	if (p<r) 
	{
		q=Partition(a,p,r);
		quickSort(a,p,q-1);
		quickSort(a,q+1,r);
	}
}





void quickSortFavo(int a[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = a[(left + right) / 2];
	  cmp_favo++;
 
      /* partition */
      while (i <= j) {
		    cmp_favo++;
            while (a[i] < pivot)
			{
                  i++;
				  cmp_favo++;
			}
			cmp_favo++;
            while (a[j] > pivot)
			{
                  j--;
				  cmp_favo++;
			}
            if (i <= j) {
				  atr_favo=atr_favo+3;;
				  interschimba(&a[i],&a[j]);
                  i++;
                  j--;
            }
      };
 
      if (left < j)
            quickSortFavo(a, left, j);
      if (i < right)
            quickSortFavo(a, i, right);
}






//-----------------------------------------------AFISARE-----------------------------------------------------------------
void afis(int a[],int length)
{
	int i;
	for (i=0; i<length;i++)
		printf("%d ",a[i]);

} 
//--------------------------------------------MAIN-----------------------------------------------------------------------------
int main()
{


	int a[10]={4,1,3,2,16,9,10,14,8,7};
	int b[10]={4,1,3,2,16,9,10,14,8,7};


	int length=sizeof(a)/sizeof(int);
	
	printf("Heapsort\n\n\n\n");
	printf("Inainte de sortare: \n");
	afis(a,length);
	printf("\nDupa sortare:\n");
	heapSort(a,length);
	afis(a,length);

	printf("\n\n\n\nQuickSort\n\n\n");
	printf("Inainte de sortare: \n");
	afis(b,length);
	printf("\nDupa sortare:\n");
	quickSort(b,0,length-1);
	afis(b,length);


	
/*

	int a[MAX_SIZE];
	int b[MAX_SIZE];
	

	FILE *file1;

	file1 = fopen("HeapQuick.csv", "w");
	
	
	fprintf(file1, "n,Suma_Heap,Suma_Quick\n");

	for(int n=100; n<MAX_SIZE; n += 100)
{

	for (int j=1;j<=5; j++)
	{
		for (int i=0; i<n; i++)
		{	
			
			a[i]=rand();
			b[i]=a[i];
		}
		heapsize=0;
		heapSort(a,n-1);
		quickSort(b,0,n-1);
		
	}
	fprintf(file1, "%d,%llu,%llu\n",n,(atr_heap+cmp_heap)/5,(atr_quick+cmp_quick)/5);

atr_heap=0;
cmp_heap=0;
atr_quick=0;
cmp_quick=0;
}

	fclose(file1);



*/
	

/*

	
	

	FILE *file2;

	file2 = fopen("QuickDefavo.csv", "w");
	
	
	fprintf(file2, "n,Suma_Defavo\n");

	for(int n=100; n<MAX_SIZE; n += 100)
{

		a[0]=rand();

		for (int i=1; i<n; i++)
		{	
			a[i]=a[i-1]+rand();
		}

		
		
		quickSort(a,0,n-1);
		fprintf(file2, "%d,%llu\n",n,atr_quick+cmp_quick);
atr_quick=0;
cmp_quick=0;
}*/

/*
	FILE *file3;

	file3 = fopen("QuickMediu2.csv", "w");
	
	
	fprintf(file3, "n,Suma_Mediu\n");

	for(int n=100; n<MAX_SIZE; n += 100)
{

	for (int j=1;j<=5; j++)
	{
		for (int i=0; i<n; i++)
		{	
			
			a[i]=rand();
		}
		quickSort(a,0,n-1);
		
	}
	fprintf(file3, "%d,%llu\n",n,(atr_quick+cmp_quick)/5);

atr_quick=0;
cmp_quick=0;
}

	fclose(file3);*/
/*

	FILE *file4;

	file4 = fopen("QuickFavo2.csv", "w");
	
	
	fprintf(file4, "n,Suma_Favo\n");

	for(int n=100; n<MAX_SIZE; n += 100)
{

		c[0]=rand();

		for (int i=1; i<n; i++)
		{	
			c[i]=c[i-1]+rand();
		}

		
		
		quickSortFavo(c,0,n-1);
		fprintf(file4, "%d,%llu\n",n,atr_favo+cmp_favo);
atr_favo=0;
cmp_favo=0;
}



		
	fclose(file4);*/



	getch();
	return 0;
}


