#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* ***ANONIM*** ***ANONIM***  grupa ***GROUP_NUMBER***
CAZUL MEDIU STATISTIC	(VECTOR RANDOM):
	Metodele selectie si insertie au aceeasi eficienta, iar bublle este mai putin eficient,
deoarece are un numar de asignari mult mai mare fata de celelalte metode.
VECTOR CRESCATOR:
	Este un caz favorabil pentru metoda bubble deoarece nu necesita nici o interschimbare de elemente implicand asignarile.
Toate cele trei metode au numar asemanator de comparari, iar difernta de eficienta se datoreaza diferentelor foarte mici al numarului de asignari.
VECTOR DESCRESCATOR:
	Caz defavorabil pentru metoda bubble,dar favorabil pentru insertie deorece nu necesita comparatii, iar diferenta de eficienta intre metodele
insertie si selectie este foarte mica.
CONCLUZIE:
	Concluzia dedusa din grafice este ca metoda selectiei are un comportament uniform pe toate cele trei cazuri avand o eficienta ridicata.
*/
Profiler profiler("demo");

#define MAX 100



void genereaza(int* v,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v[i]=rand()%MAX;
	}
}

	//Metida selectiei
void selectie(int* v,int n)
{
	int aux,imin;
	for(int i=0;i<n;i++)
	{
		imin=i;
		for(int j=i+1;j<n;j++)
		{
			if(v[j]<v[imin]) imin=j;
			profiler.countOperation("selectie_CompSelectie",n,1);
		}
		aux=v[imin];
		v[imin]=v[i];
		v[i]=aux;
		profiler.countOperation("selectie_AssignSelectie",n,3);
	}	
}

	//Metoda insertiei
void insertie(int* v,int n)
{
	int j,x;
	for(int i=0;i<n;i++)
	{
		x=v[i];
		j=0;
		while(v[j]<x)
		{	
			j++;
			profiler.countOperation("Insertie_CompInsertie",n,1);				
		}
		for(int k=i;k>=j+1;k--)
		{
			v[k]=v[k-1];
			profiler.countOperation("insertie_AssignInsertie",n,1);
		}	
		v[j]=x;
		profiler.countOperation("insertie_AssignInsertie",n,2);
	}
}


	//Metoda bubble
void interschimbare(int* v,int n)
{
	printf("\n");
	int j;
	int i;
	int aux;
	for(i=1;i<n;i++)
	{
		for(j=0;j<n-i;j++)
		{
			if (v[j]>v[j+1]) 
			{ 
				aux=v[j];
				v[j]=v[j+1];
				v[j+1]=aux;
				profiler.countOperation("bubble_AssignInterschimbare",n,3);
			}
		profiler.countOperation("bubble_CompInterschimbare",n,1);
		}
	}
}


void main(){
	srand(time(NULL));//sa nu genereze aceleasi nr aleatoare
	/*int test[]={4, 9, 1, 8, 3};
	int test2[]={4, 9, 1, 8, 3};
	int test3[]={4, 9, 1, 8, 3};

	genereaza(a,MAX);
	interschimbare(test,5);
	printf("\n");
	for (int i=0;i<5;i++){printf("%d ",test[i]);	}
	selectie(test2,5);
	printf("\n");
	for (int i=0;i<5;i++){printf("%d ",test2[i]);	}
	insertie(test3,5);
	printf("\n");
	for (int i=0;i<5;i++){printf("%d ",test3[i]);	}*/

	int n;
	int v[10000], v1[10000];
	char choose;
	printf("Tasta: R - Random   C - Crescator D - Descrescator\n Tasta: ");
	scanf("%c",&choose);

	switch(choose){
	//Random
	case 'r':
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
		printf("n=%d\n", n);
		genereaza(v, n);//fillrandomarray(v,n);
		memcpy(v1, v, n * sizeof(int));
		interschimbare(v1, n);
		memcpy(v1, v, n * sizeof(int));
		selectie(v1, n);
		memcpy(v1, v, n * sizeof(int));
		insertie(v1, n);
		}
	}
	break;
	//Sortat crescator
	case 'c':
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
		printf("n=%d\n", n);
		FillRandomArray(v,n,10,50000,false,1);
		memcpy(v1, v, n * sizeof(int));
		interschimbare(v1, n);
		memcpy(v1, v, n * sizeof(int));
		selectie(v1, n);
		memcpy(v1, v, n * sizeof(int));
		insertie(v1, n);
		}
	}
	break;
	//Sortat descrescator
	case 'd':
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
			printf("n=%d\n", n);
			FillRandomArray(v,n,10,50000,false,2);
			memcpy(v1, v, n * sizeof(int));
			interschimbare(v1, n);
			memcpy(v1, v, n * sizeof(int));
			selectie(v1, n);
			memcpy(v1, v, n * sizeof(int));
			insertie(v1, n);
		}
	}

	}
	profiler.createGroup("comp","Insertie_CompInsertie","bubble_CompInterschimbare","selectie_CompSelectie");
	profiler.createGroup("assign","selectie_AssignSelectie","insertie_AssignInsertie","bubble_AssignInterschimbare");

	profiler.addSeries("bubleSerie","bubble_CompInterschimbare","bubble_AssignInterschimbare");
	//profiler.createGroup("bubble","bubleSerie","bubble_CompInterschimbare","bubble_AssignInterschimbare");

	profiler.addSeries("insertieSuma","Insertie_CompInsertie","insertie_AssignInsertie");
	//profiler.createGroup("Insertie","insertieSuma","Insertie_CompInsertie","insertie_AssignInsertie");

	profiler.addSeries("selectieSuma","selectie_CompSelectie","selectie_AssignSelectie");
	//profiler.createGroup("Selectie","selectieSuma","selectie_CompSelectie","selectie_AssignSelectie");

	profiler.createGroup("SERIE","bubleSerie","insertieSuma","selectieSuma");
	profiler.showReport();
}
