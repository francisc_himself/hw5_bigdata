#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <math.h>
#include "Profiler.h"

#define HASH_SIZE 10007
#define SEARCH_SIZE 3000

int samples[HASH_SIZE+SEARCH_SIZE/2];
int indices[SEARCH_SIZE/2];
int a[5]= {80, 85, 90, 95, 99};
long op=0,op2;


int functie(int val,int c1,int c2,int i)
{
    int x;
    x=(val+c1*i+c2*i*i) % HASH_SIZE;
    return x;
}

int insereaza(int *hash,int c1,int c2,int k)
{
    int i=0,j;
    do
    {
    j=functie(k,c1,c2,i);
    if (hash[j]==-1)
        {
            hash[j]=k;
            return j;
        }
    else i++;
    }while (i<=HASH_SIZE);
    return -1;
}

int cauta(int *hash,int c1,int c2,int k)
{
    int i=0,j;
     do
    {
		op+=1;
		j=functie(k,c1,c2,i);
		if (hash[j]==k) return j;
		else i++;
    }while (i<=HASH_SIZE && hash[j]!=-1);
    return -1;
}

int main()
{
    int c1=7,c2=5,i,n,j,k,l;

    int t[HASH_SIZE];
	int *operatii,*operatii2;
    //for(i=0;i<HASH_SIZE;i++) t[i]=-1;
	//for(i=0;i<HASH_SIZE+SEARCH_SIZE/2;i++) samples[i]=2;
    //for(i=0;i<HASH_SIZE;i++) insereaza(t,c1,c2,2*i);
    //for(i=0;i<HASH_SIZE;i++) printf("%d ",t[i]);
	for(j=0;j<5;j++)
	{
		n=100*a[j];
		
	for(i=0;i<SEARCH_SIZE/2+n;i++) samples[i]=0;
	for(i=0;i<SEARCH_SIZE/2;i++) indices[i]=0;
	for(i=0;i<HASH_SIZE;i++) t[i]=-1;
    FillRandomArray(samples,SEARCH_SIZE/2+n,1,1000000,true,0);
    FillRandomArray(indices,SEARCH_SIZE/2,0,n-1,true,0);
    
    for(i=0;i<n;i++) k=insereaza(t,c1,c2,samples[i]);
		
		
	
	//for(i=0;i<20;i++) printf("%d ",t[i]);
	//printf("\n");
	op=0;
	for(i=n;i<n+ SEARCH_SIZE/2;i++)
		k=cauta(t,c1,c2,samples[i]);
	printf("Numarul de operatii pentru elementele care nu sunt in sir este %ld \n",op/1500);
	op=0;
	for(i=0;i< SEARCH_SIZE/2;i++) k=cauta(t,c1,c2,samples[indices[i]]);
	printf("Numarul de operatii pentru elementele care sunt in sir este %d \n",op/1500);
	}
	getch();
    return 0;
}
