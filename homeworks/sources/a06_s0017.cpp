/*
***ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER*** sg 2
Tema 6

Permutarea Josephus

- este vorba despre un joc sau mai bine zis un exemplu:

sa zicem ca avem 7 elemente intr-o lista sau arbore
1 2 3 4 5 6 7
- ideea este ca sa permutam elementele astfel:
 fiecare k este sters din lista... fie k=3
 3-ul, si 6 dispar dupa prima runda... repetand operatia ajungem la o permutare:
 3 6 4 1 7 2 5

*/



#include "stdio.h"
#include "conio.h"
#include "stdlib.h"

typedef struct tip_nod
			{ int nr,dimensiune;
			  tip_nod *stg,*dr,*parinte;
		    } TIP_NOD;
int vec[10000];
FILE *f;
int comparari,atribuiri, op;

TIP_NOD* frunza_stanga(TIP_NOD *arb)
{
	while (arb->stg!=NULL)
	{
		atribuiri++;
        arb=arb->stg;
	}
  return arb;
} //frunza stanga

TIP_NOD* succesor(TIP_NOD *arb)
{ TIP_NOD *y;
  comparari++;
  if (arb->dr!=NULL)
     return frunza_stanga(arb->dr);
  atribuiri++;
  y=arb->parinte;
  while ( (y!=NULL) && (arb==y->dr) )
	{
	  atribuiri+=2;
	  arb=y;
	  y=y->parinte;
	}
  return y;
} // succesorul unui nod


TIP_NOD *TREE_INSERT(int s, int d, int *dim)
{ TIP_NOD *p;
  int n=sizeof(TIP_NOD);
  int dim1=0,dim2=0;
  comparari++;
  if (s>d)
	  { *dim=0;
   	    return NULL;
	  }
	  else
	      {
            atribuiri=atribuiri+5;
			p=(TIP_NOD*) malloc(n);
	        p->nr=(s+d)/2;
			p->stg = NULL;
			p->dr = NULL;
			p->dimensiune=d-s+1;
			if(s<d)
			{
				p->stg=TREE_INSERT(s,(s+d)/2-1,&dim1);
				if(p->stg != NULL)
				{	p->stg->parinte = p;  atribuiri++; }
				p->dr=TREE_INSERT((s+d)/2+1,d,&dim2);
				if(p->dr != NULL)
				{	p->dr->parinte = p; atribuiri++;}
			}
	      }
  atribuiri++;
  *dim=p->dimensiune;
  return p;
}


void scade_dim(TIP_NOD *z)
{
	while (z!=NULL)
	{
      atribuiri=atribuiri+2;
	  z->dimensiune--;
	  z=z->parinte;
	}
}//scadarea dimensiunii

void OS_DELETE(TIP_NOD **arb, TIP_NOD *z)
{ TIP_NOD *y,*x;
  comparari=comparari+5;
  if ( (z->stg==NULL) || (z->dr==NULL) )
     { y=z; atribuiri++;
     }
     else
	 {y=succesor(z); atribuiri++; }
  if (y->stg!=NULL)
  { x=y->stg; atribuiri++; }
     else
	 {	 x=y->dr; atribuiri++; }
  if (x!=NULL)
     {   atribuiri++;
		 x->parinte=y->parinte;
     }
  scade_dim(y->parinte);
  if (y->parinte==NULL)
     *arb=x;
     else
	 if (y==y->parinte->stg)
	 { y->parinte->stg=x; atribuiri++;}
	    else
		{ y->parinte->dr=x; atribuiri++; }
  if (y!=z)
     {   atribuiri++;
		 z->nr=y->nr;
     }
  free(y);
}

TIP_NOD* OS_SELECT(TIP_NOD *arb, int i)
{ int r;
  comparari=comparari+3;
  atribuiri++;
  if (arb->stg!=NULL) r=arb->stg->dimensiune+1;
     else r=1;

  if (i==r)
  return arb;
  if (i<r)
     return OS_SELECT(arb->stg,i);
     else
	 return OS_SELECT(arb->dr,i-r);
}

void josephus(int n, int m)
{
  TIP_NOD *rad,*x;
  int h,k,elem;
  int index=0;
  elem=n;
  rad=TREE_INSERT(1,elem,&h);
  rad->parinte=NULL;

  k=m;
  while (elem>0)
	{
		op++;
	  x=OS_SELECT(rad,k);
	  vec[index]=x->nr;
	  index++;
	  OS_DELETE(&rad,x);
	  elem--;
	  if (elem)
	     {
		   k=(k+m-1)%elem;
	       if (k==0)
		  k=elem;
	     }
	}
}

int main()
{
  f=fopen("rezultat.csv","w");
  fprintf(f, "n, atribuiri+comparari\n");
  for (int n=100;n<10000;n+=100)
  {
    atribuiri=0;
	comparari=0;
    int m=n/10;
    josephus(n,m);
    fprintf(f,"%d, %d \n",n,atribuiri+comparari);
  }
  atribuiri= comparari= 0; op=0;
	josephus(7,3);
	
	printf("%d    %d", 7, op);

    fclose(f);
    return 0;
}
