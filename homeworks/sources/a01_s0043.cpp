#include <stdio.h>
#include <stdlib.h>
const int N=11000;
const int MAX=1000;
const int MIN=1;
const int CAR=10000;


void generareRand(int *a,int n)
{
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%(MAX-MIN)+MIN;
	}
}
void generareUp(int *arr,int n){
    for(int i=0;i<n;i++){
        arr[i]=i;
    }

}
void generareDown(int *arr,int n){
    for(int i=0;i<n;i++){
        arr[i]=n-i;
    }
}

void print(int n,int *a)
{
	
	for(int i=0;i<n;i++)
		printf(" %d ",a[i]);
}
void selectionSort(int *a,int n,int &atr,int &comp,int lo=0,int hi=0)
{
	int i;
	for(i=0;i<n-1;i++)
	{
		int j,kmin=i;
		for(j=i+1;j<n;j++)
		{
			comp++;
			if(a[kmin]>a[j])
				kmin=j;
		}
		comp++;
		if(kmin!=i){
			atr+=3;
			int aux=a[kmin];
			a[kmin]=a[i];
			a[i]=aux;
		}
		
	}
}
void insertionSort(int *a,int n,int &atr,int &comp,int lo=0,int hi=0)
{
	int i;
	for(i=1;i<n;i++)
	{
		int j=i;
		int salv=a[i];
		comp+=2;
		while((j>0)&&(salv<a[j-1]))
		{
			atr+=1;
			a[j]=a[j-1];
			j--;
		}
		atr+=1;
		a[j]=salv;
	}

}


void bubbleSort(int *a,int n,int &atr,int &comp,int lo,int hi)
{
	int aux;

	for (int i=1; i<=n-1; i++)
		for (int j=1; j<=n-1; j++)
		{
			comp++;
			if (a[j] > a[j+1])
			{
				atr = atr+3;
				aux = a[j];
				a[j] = a[j+1];
				a[j+1] = aux;
			}
		}
}


void generalizare(char *s, void (*sortare)(int*,int,int&,int&,int,int),void generare(int*,int))
{
	int atr=0,comp=0;
	int a[N],n;
	FILE *f=fopen(s,"w");
	for(n=20;n<=CAR;n+=200){
		//citire(n,a);
		generare(a,n);
		
		//print(n,a);
		sortare(a,n,atr,comp,0,n-1);
		fprintf(f,"%d,%d,%d,%d\n",n,atr,comp,atr+comp);
		atr=0;
		comp=0;
		//printf("\n");
		//print(n,a);
	}
	fclose(f);
}

void main()
{
	/*
	generalizare("selectionSort.csv",&selectionSort,&generareRand);
	generalizare("selectionSortBest.csv",&selectionSort,&generareUp);
	generalizare("selectionSortWorst.csv",&selectionSort,&generareDown);*/
	
	
	
	/*generalizare("insertionSort.csv",&insertionSort,&generareRand);
	//printf("\n");
	generalizare("insertionSortBest.csv",&insertionSort,&generareUp);
	//printf("\n");
	generalizare("insertionSortWorst.csv",&insertionSort,&generareDown);*/



	
	generalizare("bubbleSort.csv",&bubbleSort,&generareRand);
	//printf("\n");
	generalizare("bubbleSortBest.csv",&bubbleSort,&generareUp);
	//printf("\n");
	generalizare("bubbleSortWorst.csv",&bubbleSort,&generareDown);




	
}