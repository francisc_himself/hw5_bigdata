﻿/*
***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***

Se cere implementarea corecta și eficienta a 3 metode directe de sortare (sortarea bulelor, 
sortarea prin inserție – folosind inserție liniară sau binară, și sortarea prin selecție).

Se poate observa ca in caz favorbail, cand sirurile sunt ordonate crescator, metoda bulelor este mai eficienta deorece numarul atributiilor va fi 0(nu are de facut interschimbari).
In caz defavorbail insa va fi cea mai ineficienta, realizand numarul maxim de atribuiri. Aici metoda bulelor are complexitate O(n) pe cand celelalte de O(n2).

In cazul mediu statistic si in cel defaforabil complexitatea este la toate metodele O(n2). Dupa cum se vede din grafice metoda buble scade in eficienta comparativ cu celelalte doua metode.


*/
#include<stdio.h>
#include<conio.h>
#include<cstdlib>
#include<time.h>

long comp_bubble, atr_bubble, suma_bubble;
long comp_selectie, atr_selectie, suma_selectie;
long comp_insertie, atr_insertie, suma_insertie;

void bubble(int n, int a[])
{
	comp_bubble = 0;
	atr_bubble = 0;
	int sortat = 0;
	int i, ok=0, aux;
	
	while( !sortat)
	{
		sortat = 1;
		for(i=0;i<n-ok-1;i++)
		{
			comp_bubble ++; 
			if(a[i]>a[i+1])
			{
				aux = a[i];
				a[i]=a[i+1];
				a[i+1]=aux;
				sortat = 0;
				atr_bubble+=3;
			}
		}
		ok++;
	}

	suma_bubble = comp_bubble + atr_bubble;
}

void selectie(int n, int a[])
{
	int i, j, min, imin;
	comp_selectie = 0;
	atr_selectie = 0;

	for(i=0; i<n-1; i++)
	{
		atr_selectie++;
		min= a[i];
		imin=i;
		for(j=i+1; j<n; j++)
		{
			comp_selectie++;
			if(a[j]<min)
			{
				min=a[j];
				imin=j;
				atr_selectie++;
			}
		}
		a[imin]=a[i];
		a[i]=min;
		atr_selectie+=2;
	}
	suma_selectie = comp_selectie + atr_selectie;
}

void insertie(int n, int a[])
{
	int i, j, k,x;
	comp_insertie = 0;
	atr_insertie = 0;

	for(i=1; i<n; i++)
	{
		atr_insertie++;
		x = a[i];
		j=0;

		comp_insertie++;
		while(a[j]<x)
		{
			comp_insertie++;
			j++;
		}

		for(k=i; k>j; k--)
		{
			atr_insertie++;
		    a[k] = a[k-1];
		}
		atr_insertie++;
		a[j]=x;

	}
	suma_insertie = comp_insertie + atr_insertie;
}


void Favorabil()
{
	int n, i, a[10000];
	FILE *pf=fopen("favorabil.csv", "w");
	fprintf(pf, "n, comp_bubble, atr_bubble, comp_selectie, atr_selectie, comp_insertie, atr_insertie, suma_bubble, suma_selectie, suma_insertie\n");
	
	for(n=100; n<=10000; n+=100)
	{
		for(i = 0; i <n; i++)
			a[i]=i;

		bubble(n,a);
		selectie(n, a);
		insertie(n, a);
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", n, comp_bubble, atr_bubble, comp_selectie, atr_selectie, comp_insertie, atr_insertie, suma_bubble, suma_selectie, suma_insertie);

	}
	fclose(pf);
	printf("Fisier creat!");
}

void MediuStatistic()
{
	int n, i, caz,val, a[10000], b[10000], c[10000];
	FILE *pf=fopen("mediu.csv", "w");
	long c_bubble =0, a_bubble = 0, c_selectie = 0, a_selectie = 0, c_insertie = 0, a_insertie = 0, s_bubble = 0, s_selectie = 0, s_insertie = 0;
	fprintf(pf, "n, comp_bubble, atr_bubble, comp_selectie, atr_selectie, comp_insertie, atr_insertie, suma_bubble, suma_selectie, suma_insertie\n");
	
	srand(time(NULL)); 
	for(n=100; n<=10000; n+=100)
	{
		c_bubble =0;
		a_bubble = 0;
		c_selectie = 0;
		a_selectie = 0;
		c_insertie = 0;
		a_insertie = 0;
		s_bubble = 0; 
		s_selectie = 0;
		s_insertie = 0;

		for(caz=1;caz<=5;caz++)
		{
			for(i = 0; i <n; i++)
			{
			   val=rand();
			   a[i]=val;
			   b[i]=val;
			   c[i]=val;
			}

			bubble(n,a);
			selectie(n, b);
			insertie(n, c);

			c_bubble+=comp_bubble;
			a_bubble+=atr_bubble;
			s_bubble+=suma_bubble;
			c_selectie+=comp_selectie;
			a_selectie+=atr_selectie;
			s_selectie+=suma_selectie;
			c_insertie+=comp_insertie;
			a_insertie+=atr_insertie;
			s_insertie+=suma_insertie;
			
		}
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", n, c_bubble/5, a_bubble/5, c_selectie/5, a_selectie/5, c_insertie/5, a_insertie/5, s_bubble/5, s_selectie/5, s_insertie/5);

	}
	fclose(pf);
	printf("Fisier creat!");

}

void Defavorabil()
{
	int n, i,val, a[10000], b[10000], c[10000];
	FILE *pf=fopen("defavorabil.csv", "w");
	fprintf(pf, "n, comp_bubble, atr_bubble, comp_selectie, atr_selectie, comp_insertie, atr_insertie, suma_bubble, suma_selectie, suma_insertie\n");
	
	for(n=100; n<=10000; n+=100)
	{
		for(i = 0; i <n; i++)
		{
			val=n-i;
			a[i]=val;
			b[i]=val;
			c[i]=val;
		}

		bubble(n,a);
		selectie(n, b);
		insertie(n, c);
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", n, comp_bubble, atr_bubble, comp_selectie, atr_selectie, comp_insertie, atr_insertie, suma_bubble, suma_selectie, suma_insertie);

	}
	fclose(pf);
	printf("Fisier creat!");
}

void VerificareSortari()
{

	int n = 4 , a[]= { 9, -4, 0, 3}, b[]={ 9, -4, 0, 3}, c[]={ 9, -4, 0, 3};
	int i;

	printf("SORTARI:\n");
	printf("bubble:\n");
	bubble(n, a);
	for(i = 0; i <n; i++)
		printf("%d ", a[i]);

	printf("selectie:\n");
	selectie(n, b);
	for(i = 0; i <n; i++)
		printf("%d ", b[i]);

	printf("Insertie:\n");
	insertie(n, c);
	for(i = 0; i <n; i++)
		printf("%d ", c[i]);
}
int main()
{

	VerificareSortari();
	Favorabil();
	MediuStatistic();
	Defavorabil();
	
	getch();
	return 0;
}
