﻿//***ANONIM*** ***ANONIM***
//Grupa : ***GROUP_NUMBER***

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

#define MAX_CHILDREN 100

//noduri arbore 
typedef struct nod{
    int ch;
    int nr_fii;
    struct nod *adr_fiu[MAX_CHILDREN];
	struct nod *st, *dr;
}NOD;

//noduri lista
typedef struct nod_lista{
    NOD *adr_nod;                 
    struct nod_lista *urm;
}NOD_LISTA;

NOD *rad1, *rad2;
NOD_LISTA *first, *last;


NOD *creeaza_nod(int ch){
    NOD *p;

	p = (NOD *)malloc(sizeof(NOD));

    if (p == NULL){
        printf ("\n Memorie nealocata! \n");
        return 0;
    }

    p->ch = ch;
    p->nr_fii = 0;

    return p;
}

void genereaza_arb_multicai(int parent[], int n, NOD *rad){
	NOD *fiu;
	int i;

	//cautam fii radacinii
	for (i=1; i <= n; i++){
		if (parent[i] == rad->ch){
			fiu = creeaza_nod(i);
			
			rad->adr_fiu[rad->nr_fii] = fiu;
			rad->nr_fii++;
		}
	}

	for (i=0; i < rad->nr_fii; i++){
		genereaza_arb_multicai(parent, n, rad->adr_fiu[i]);
	}
}

void T1(int parent[], int n)
{
    int rad = -1;
	int i;

	//cautam radacina
    for (i=1; (i<=n) && (rad == -1); i++){
        if (parent[i] == -1){
            rad = i;
		}
	}
    
	rad1 = creeaza_nod(rad);

	printf("\nRadacina este %d \n", rad1->ch);
	
	genereaza_arb_multicai(parent, n, rad1);
}

int ENQUEUE(NOD *p){
    NOD_LISTA *l;
    
	l = (NOD_LISTA *)malloc(sizeof(NOD_LISTA));
    
	if (l == NULL){
        printf ("\n Memorie nealocata! \n");
        return 0;
    }

    l->adr_nod = p;
    l->urm = 0;
    
	if (first == 0){
        first = l;
        last = l;
    }
    else{
        last->urm = l;
        last = l;
    }
    
	return 1;
}

NOD *DEQUEUE(){
    NOD_LISTA *p;
    NOD *q;
    
	if(first == 0){
        return 0;
	}

    p = first;
    first = first->urm;
    
	if (first == 0){
        last = 0;
	}

    q = p->adr_nod;
    
	free (p);
    
	return q;
}

int print(NOD* root){
    NOD *p;
    int i;

    first = 0;
    last = 0;

    if ((ENQUEUE(root)) == 0){
        return 0;
	}

    do{
        p = DEQUEUE();
        if (p!=0){
            printf ("%d ",p->ch);

            for (i=0; i < p->nr_fii; i++){
                if ((ENQUEUE(p->adr_fiu[i])) == 0){
                    return 0;
				}
			}
        }
    }while (p != 0);

    return 1;
}

void generate_binary_tree(NOD *rad1, NOD *rad2){
	int i;
	NOD *p;

	//fiul stang
	if (rad2->st != NULL){
		if (rad1->adr_fiu[0]->nr_fii >= 1){
			//setam fiul pentru fiul nodului
			rad2->st->st = rad1->adr_fiu[0]->adr_fiu[0];
		}
		else{
			//nu avem fiu
			rad2->st->st = NULL;
		}
	
		p = rad2->st;
		//setam fratii primului fiu al nodului
		for (i=1; i < rad1->nr_fii; i++){	
			p->dr = rad1->adr_fiu[i];
			p = p->dr;
		}

		p->dr = NULL;
	}

	//pentru fratele drept
	if (rad2->dr != NULL){
		if (rad2->dr->nr_fii >= 1){
			//setam fiul fratelui
			rad2->dr->st = rad2->dr->adr_fiu[0];
		}
		else{
			//fratele nu are fiu
			rad2->dr->st = NULL;
		}
	}

	if (rad2->st != NULL){
		generate_binary_tree(rad1->adr_fiu[0], rad2->st);
	}

	if (rad2->dr != NULL){
		generate_binary_tree(rad2->dr, rad2->dr);
	}
}

//Transformarea din reprezentarea multi-cai in reprezentarea binara
void T2()
{
	//rad2 va contine reprezentarea binara
	rad2 = rad1;

	if(rad1->nr_fii != 0){
		//setam primul fiu al radacinii
		rad2->st = rad1->adr_fiu[0];
	}
	else{
		rad2->st = NULL;
	}

	//radacina nu are frate
	rad2->dr = NULL;

	generate_binary_tree(rad2, rad1);
}

void pretty_print(NOD *rad2, int d){
	int d2 = d;

	if (rad2 != NULL){
		
		for (int i=1; i<=d; i++){
			printf (" ");
		}
		
		printf ("%d\n", rad2->ch);

		if(rad2->st != NULL){
			d2++;
		}

		pretty_print(rad2->st, d2);

		pretty_print(rad2->dr, d);
	}
}

int main(){
	int nod[100], n;

	printf ("Nr de noduri = ");
    scanf ("%d",&n);

	for (int i=1; i<=n; i++){
		printf ("nod[%d]=", i);
		scanf ("%d", &nod[i]);
	}

	printf ("\nVectorul de noduri:\n");
	for (int i=1; i<=n; i++){
		printf ("%d ", nod[i]);
	}
	
	printf ("\n");

	//prima transformare
    T1(nod, n);
	printf ("\nArborele multicai :\n");
	print (rad1);

	//a doua transformare
	T2();
	printf ("\n\nReprezentare binara \nPretty print:\n");
	pretty_print (rad2, 0);

	getche ();
	return 0;
}