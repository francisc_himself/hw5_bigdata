#include <stdio.h>
#include <stdlib.h>

typedef struct TREE
{
    int nr;
    struct TREE *right;
	struct TREE *left;
	struct TREE *parent;
	int rank;
};

TREE* PBT;
int a[10000];
int heap[10000];
int operation;

void pretty_print(TREE* node, int level)
{
	if(node!=NULL)
	{
		pretty_print(node->right, level+1);
		for(int i=1;i<=2*level;i++)
			printf(" ");
		printf("%d\n", node->nr);
		pretty_print(node->left, level+1);
	}
}

TREE* build_tree(TREE* node,TREE* parent,int start,int end)
{
	if (start<=end)
	{
		TREE* tree=new (TREE);
		tree->parent=parent;
		tree->rank=1;
		tree->nr=a[(start+end)/2];
		operation++;
		tree->left = build_tree(node,tree,start,(end+start)/2-1);
		tree->right = build_tree(node,tree,(end+start)/2+1,end);
		if(tree->left!=NULL)
			tree->rank+=tree->left->rank;
		if(tree->right!=NULL)
			tree->rank+=tree->right->rank;
		return tree;
	}
	else return NULL;
}

TREE* find_tree_min(TREE* x)
{
	while(x->left!=NULL)
	{
		x->rank--;
		operation++;
		x=x->left;
	}
	return x;
}

TREE* successor(TREE* x)
{
	return find_tree_min(x->right);
}

TREE* os_select(TREE* x,int i)
{
	operation++;
	if(x==NULL)
		return 0;
	int r=1;
	x->rank--;
	if(x->left!=NULL)
	 r += x->left->rank;
	if(i==r)
		return x;
	else if (i<r) 
		return os_select(x->left,i);
	else return os_select(x->right,i-r);
}

void os_delete(TREE* T,TREE* z)
{
	TREE* y,*x;
	operation++;
	if(z->left==NULL || z->right==NULL)
		y=z;
	else y=successor(z);
	z->nr=y->nr;
	if(y->left!=NULL)
		x = y->left;
	else x = y->right;
	operation++;
	if(x!=NULL)
		x->parent=y->parent;
	if(y->parent==NULL)
		PBT=x;
	else if(y==y->parent->left)
		y->parent->left=x;
	else y->parent->right=x;
	free(y);
}

void josephus(int n,int m)
{
	int j=1;
	int k;
 	for(k=n;k>=1;k--)
	{
		j=((j+m-2)%k)+1;
		TREE* x = os_select(PBT,j);
		//printf("%d \n",x->nr);
		os_delete(PBT,x);
		//pretty_print(PBT, 1);
	}
}


void preorder(TREE* node)
{
	if(node==NULL) return;
	printf("%d ",node->nr);
	preorder(node->left);
	preorder(node->right);
}


int main()
{
/*
	int i;
	for(i=1;i<=12;i++)
		a[i]=i;
	PBT=build_tree(PBT,NULL,1,12);
	pretty_print(PBT, 1);
	//preorder(PBT);
	//toheap(PBT,1);
	//print_heap(7,1,1);
	josephus(12,5);
	*/
	int n;
	FILE *f;
	f = fopen("out.csv","w");
	for(n=100;n<=10000;n+=100)
	{
		PBT=build_tree(PBT,NULL,1,n);
		josephus(n,n/2);
		printf("%d\n",n);
		fprintf(f,"%d,%d\n",n,operation);
		operation=0;
	}
return 0;
}