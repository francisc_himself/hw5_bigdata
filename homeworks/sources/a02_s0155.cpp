/*
For Average: Selection>Insertion>Bubble
Insertion is best for comparisons
Slection the best for assignments

Selection assignments has linear growth

---------------------------------------------------
Worst case: Selection>>Insetion, Bubble
comparisons=comparable
selection, much better at assignments\

selection assignments has linear growth
---------------------------------------------------



Best Case: Insertion,Bubble>>Selection
Selection is very bad in comparisons

-----------------------------------------------


Selection sort:
worst=n^2
best=n^2
average=n^2

Insertion sort:
worst=n^2
best=n
Average=n^2

Bubble sort:
worst=n^2
best=n
average=n^2


*/


#include <stdio.h>
#include <iostream>
#include <conio.h>
#include "Profiler.h"
using namespace std;

Profiler compProfiler("Operations");

void bubbleSort(int arr[], int n) {
	compProfiler.countOperation("Bubble Assignments", n, 0);
	bool swapped = true;
	int j = 0;
	int tmp;
	while (swapped) {
		swapped = false;
		j++;
		for (int i = 0; i < n - j; i++) {
			compProfiler.countOperation("Bubble Comparisons", n);
			if (arr[i] > arr[i + 1]) {
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				swapped = true;
				compProfiler.countOperation("Bubble Assignments", n, 3);
			}
		}
	}
}

void selectionSort(int arr[], int n) {
	int i, j, minIndex, tmp;   
	compProfiler.countOperation("Selection Assignments", n, 0);
	for (i = 0; i < n - 1; i++) {
		minIndex = i;
		for (j = i + 1; j < n; j++){
			compProfiler.countOperation("Selection Comparisons", n, 1);
			if (arr[j] < arr[minIndex]){
				minIndex = j;
			}
		}
		if (minIndex != i) {
			tmp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = tmp;
			compProfiler.countOperation("Selection Assignments", n, 3);
		}
	}
}

void insertSort(int arr[], int length) {
	int i, j, tmp;
	compProfiler.countOperation("Insertion Assignments", length, 0);
	for (i = 1; i < length; i++) {
		j = i;

		while (j > 0 && arr[j - 1] > arr[j]) {
			compProfiler.countOperation("Insertion Comparisons", length, 1);
			tmp = arr[j];
			arr[j] = arr[j - 1];
			arr[j - 1] = tmp;
			j--;
			compProfiler.countOperation("Insertion Assignments", length, 3);
		}compProfiler.countOperation("Insertion Comparisons", length, 1);
	}
}
void copyVector(int vect1[],int vect2[],int n)
{
	for(int i=0;i<n;i++)
		vect1[i]=vect2[i];
}

int main() {
	compProfiler.createGroup("Comparisons", "Bubble Comparisons", "Insertion Comparisons","Selection Comparisons");
	compProfiler.createGroup("Assignments", "Bubble Assignments", "Insertion Assignments","Selection Assignments");
	compProfiler.createGroup("Total-Ops", "TotBubble","TotInsertion","TotSelection");
	int vect[10000];
	int vect2[10000];
	int vect3[10000];
	/*
	for(int inputLength = 100; inputLength <= 10000; inputLength += 100){
	FillRandomArray(vect,inputLength,0,50000,false,1);
	bubbleSort(vect, inputLength);
	selectionSort(vect, inputLength);
	insertSort(vect, inputLength);
	}
	compProfiler.addSeries("TotBubble","Bubble Comparisons","Bubble Assignments");
	compProfiler.addSeries("TotInsertion","Insertion Comparisons","Insertion Assignments");
	compProfiler.addSeries("TotSelection","Selection Comparisons","Selection Assignments");
	compProfiler.showReport();
	*/
	
	for(int j=0;j<5;j++){
		for(int inputLength = 100; inputLength <= 10000; inputLength += 100){
			//boost::thread_group group;
			FillRandomArray(vect,inputLength,0,50000,false,0);
			for(int i=0;i<inputLength ;i++){
				vect2[i]=vect[i];
				vect3[i]=vect[i];
			}
			bubbleSort(vect, inputLength);
			
			selectionSort(vect2, inputLength);
		
			insertSort(vect3, inputLength);
			cout<<j<<" "<<inputLength<<endl;
		}
		cout<<j;
	}
	compProfiler.addSeries("TotBubble","Bubble Comparisons","Bubble Assignments");
	compProfiler.addSeries("TotInsertion","Insertion Comparisons","Insertion Assignments");
	compProfiler.addSeries("TotSelection","Selection Comparisons","Selection Assignments");
	compProfiler.showReport();
	
	/*
	int aux;

		for(int inputLength = 100; inputLength <= 10000; inputLength += 100){
			//boost::thread_group group;
			FillRandomArray(vect,inputLength,0,50000,false,2);
			for(int i=0;i<inputLength ;i++){
				vect2[i]=vect[i];
			}

			FillRandomArray(vect3,inputLength,0,50000,false,1);
			aux=vect3[0];
			for(int i=0;i<inputLength-2;i++)
				vect3[i]=vect3[i+1];
			vect3[inputLength-1]=vect3[0];
			bubbleSort(vect, inputLength);
			
			selectionSort(vect3, inputLength);
		
			insertSort(vect2, inputLength);
			cout<<inputLength<<endl;
	}

	compProfiler.addSeries("TotBubble","Bubble Comparisons","Bubble Assignments");
	compProfiler.addSeries("TotInsertion","Insertion Comparisons","Insertion Assignments");
	compProfiler.addSeries("TotSelection","Selection Comparisons","Selection Assignments");
	compProfiler.showReport();
	*/
	return 0;
}