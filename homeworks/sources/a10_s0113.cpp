/*
	Name: ***ANONIM*** ***ANONIM***án
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently the Depth-First Search (DFS) graph 
					algorithm (Section 22.3 from the book1). For graph representation, you should use adjacency lists. 
				  You are also required to:
					 Perform edge labeling on the graph (look for Classiﬁcation of edges sub-section in 
					section 22.3)
					 Implement the topological sort algorithm (section 22.4 from the book1)

	Analysis: - classification of edges: - if the end is WHITE, is is a tree edge
										 - if the end is GRAY, it is a back edge
										 - if the end is BLACK, and the discovery time is smaller, it is a forward edge
										 - if the end is BLACK, and the discovery time is larger, it is a cross edge
			  - a graph does not have a topological sort, if it has back edges (GRAY endpoint)
			  - topological sort does not increase running time, since it is just +1 line to DFS
			  - charts clearly shows a running time of O(V+E)
			  - to verify that edges were not generated twice, the following technique was used:
					a matrix in[i][j]=1 where edge i-j is present, 0 otherwise. This way I generate edges
						until the edge is not present

*/
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<vector>
#include<queue>
#define N 1000
#define M 10000

using namespace std;


vector<int> g[N];
deque<int> topo;
int hasTopo;
int n,m;
int edge[N][N], parent[N], discovery[N], finish[N];
enum COLORS{WHITE, GRAY, BLACK}color[N];
typedef enum EDGE_TYPE{TREE, BACK, FORWARD, CROSS};
struct EDG{
	EDGE_TYPE type;
	int u, v;
}edges[M];
int edgenr;
int TIME;
int op;

void DFS_VISIT(int node)
{
	TIME++;
	discovery[node]=TIME;
	color[node]=GRAY;
	op++;
	for(int j=0;j<g[node].size();j++)
	{
		op++;
		if(color[g[node][j]]==WHITE)
		{
			parent[g[node][j]]=node;
			DFS_VISIT(g[node][j]);
			edges[++edgenr].type=TREE;
		}
		else if(color[g[node][j]]==GRAY)
		{
			edges[++edgenr].type=BACK;
			hasTopo=0;
		}
		else if(discovery[node]<discovery[g[node][j]])
			edges[++edgenr].type=FORWARD;
		else edges[++edgenr].type=CROSS;
		edges[edgenr].u=node;
		edges[edgenr].v=g[node][j];
	}
	color[node]=BLACK;
	TIME++;
	finish[node]=TIME;
	topo.push_back(node);
}

void DFS()
{
	int i;
	for(i=1;i<=n;i++)
	{
		color[i]=WHITE;
		parent[i]=0;
		op++;
	}
	TIME=0;
	for(i=1;i<=n;i++)
		if(color[i]==WHITE)
			DFS_VISIT(i);
}

void initialize()
{
	for(int i=1;i<=n;i++)
	{
		g[i].clear();
		for(int j=1;j<=n;j++)
			edge[i][j]=0;
	}
	edgenr=0;
	hasTopo=1;
	op=0;
	topo.clear();
}

void display()
{
	printf("Adjacency list:\n");
	for(int i=1;i<=n;i++)
	{
		printf("List for node %d:", i);
		for(int j=0;j<g[i].size();j++)
			printf("%d ", g[i][j]);
		printf("\n");
	}
	printf("\nEdge classification\n"); 
	for(int i=1;i<=edgenr;i++)
		printf("The edge from %d to %d is of type %d\n", edges[i].u, edges[i].v, edges[i].type);
	if(hasTopo==0)
		printf("\nThe graph doesnt have a topological sort!\n");
	else
	{
		printf("\nThe graph has a topological sort!\nThe order is: ");
		while(!topo.empty())
		{
			printf("%d ", topo.back());
			topo.pop_back();
		}

	}


}

void test()
{
	initialize();
	n=6;
	g[1].push_back(2);
	g[1].push_back(3);
	g[1].push_back(4);
	g[2].push_back(4);
	//g[4].push_back(1);
	g[3].push_back(2);
	g[5].push_back(6);
	DFS();
	display();
}


int main()
{
	//test();
	srand(time(NULL));
	freopen("1.csv", "w", stdout);
	printf("v,e,op\n");
	n=100;
	int x,y;
	for(int i=1000;i<=5000;i+=100)
	{
		m=i;
		initialize();
		for(int j=1;j<=i;j++)
		{
			do{
			x=rand()%n+1;
			y=rand()%n+1;
			}while(edge[x][y]==1);
			edge[x][y]=1;
			g[x].push_back(y);
		}
		DFS();
		printf("%d,%d,%d\n", n, i,op);
	}
	
	freopen("2.csv", "w", stdout);
	printf("v,e,op\n");
	m=9000;
	for(int i=110;i<=200;i+=10)
	{
		n=i;
		initialize();
		for(int j=1;j<=m;j++)
		{
			do{
			x=rand()%n+1;
			y=rand()%n+1;
			}while(edge[x][y]==1);
			edge[x][y]=1;
			g[x].push_back(y);
		}
		DFS();
		printf("%d,%d,%d\n", n, m,op);
	}
	return 0;
}