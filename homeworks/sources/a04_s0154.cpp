/*
	The complexity of this algorithm is O(n*lg k) in theory.
	It is shown that in practice it behaves indees like n*lg k, becasue:
		1) fixing k, the plot shows that the number of operations increseases linearly, O(n), lg k behaves like a constant.
		2) fixing n, the plot shows that the number of operations increseases logarithmly O(lg k), n behaves like a constant.
*/

#include <cstdio>
#include <cstdlib>
#include <conio.h>

FILE *fdebug = fopen("log.txt", "w");
					 
struct nod{

	int x;
	nod* next;
};

struct heapData{
	int x;
	int lista;
};

nod* v[5000];

heapData H[99999];
int heapSize = 0;
int finalArray[10005];
int op1[10001], op2[10001], op3[10001];
int n, k;

void add(int number, nod* &list){
//	for(;list->next!= NULL; list = list->next);
	nod* t = new nod;
	t->x = number;
	t->next = list;
	list = t;
}

void init(int &op){
	for(int i = 1; i <= n/k; ++i){
		for(int j = 1; j <= k; ++j){
			add(n/k-i+1, v[j]);
		}

	}
}

void printSequences(){
	fprintf(fdebug, "||===========||\n");
	for(int i = 1; i <= k; ++i){
		fprintf(fdebug, "Sequence %d : ", i);
		for(nod* t = v[i]; t != NULL; t = t->next){
			fprintf(fdebug, "%d ", t->x);
		}
		fprintf(fdebug, "\n");
	}
}

inline static void swap(heapData& h1, heapData& h2){
	heapData aux = h1;
	h1 = h2;
	h2 = aux;
}

void heapify(int i, int &op){
	if(i == 0)
		return ;

	++op;
	if(H[i].x < H[i/2].x){
		op += 3;
		swap(H[i], H[i/2]);
		heapify(i/2, op);
	}
}


void pushHeap(int x, int lista, int &op){
	heapData nou;
	nou.x = x;
	nou.lista = lista;

	++op;
	H[++heapSize] = nou;
	heapify(heapSize, op);
}

inline void printHeap(){
	fprintf(fdebug,"heap: ");
	for(int i = 1; i <= heapSize; ++i)
		fprintf(fdebug, "%d,%d; ", H[i].x, H[i].lista);
	fprintf(fdebug,"\n");
}

void pop(int i, int &op){
	nod *t = v[i];
	v[i] = v[i]->next;

	delete t;
}

void heapifyDown(int i, int &op){
	if(2*i > heapSize)
		return;

	int largest = i;
	++op;
	if(H[2*i].x < H[i].x){
		largest = 2*i;
	}

	if(2*i + 1 <= heapSize){
		++op;
		if(H[2*i+1].x < H[i].x){
			++op;
			if(H[2*i+1].x < H[2*i].x)
				largest=2*i+1;
		}
	}
	if(i != largest){
		op+=3;
		swap(H[i], H[largest]);
		heapifyDown(largest, op);
	}
}

void popHeap(int &op){
	H[1] = H[heapSize];
	heapSize--;
	heapifyDown(1, op);
}

void initHeap(int &op){
	for(int i = 1; i <= k; ++i){
		pushHeap(v[i]->x, i, op);
		pop(i, op);
	}
}

void printFinalArray(){
	fprintf(fdebug, "Final array; size=%d\n", n);
	for(int i = 1; i <= n; ++i)
		fprintf(fdebug, "%d ", finalArray[i]);
	fprintf(fdebug,"\n");
}

void perform(int &op){
	if(n%1000)
		printf("n = %d\n", n);
	init(op);
	initHeap(op);
	for(int i = 1; i <= n; ++i){
		finalArray[i] = H[1].x;
		int idx = H[1].lista;
		popHeap(op);	
		if(v[ idx ] != NULL){	
			pushHeap(v[idx ]->x, idx, op);
			pop(H[1].lista, op);
					
		}
		
	}
}

int main(){
	

#define N 10000
#define INC 200
#define FOR1(x) for(x = 100; x <= N; x += INC)

	printf("Working ...\n");

	
	
	for(int turn = 1; turn <= 3; ++turn){
		switch (turn)
		{
		case 1:
			k=5;
			break;
		case 2:
			k=10;
			break;
		case 3:
			k=100;
			break;
		default:
			printf("Error!");
			return 1;
		}

		//be sure that n is multiple of k!s
		int op = 0;
		FOR1(n){
			op = 0;
			perform(op);
			
			switch (turn){
			case 1:
				op1[n] = op;
				break;
			case 2:
				op2[n] = op;
				break;
			case 3:
				op3[n] = op;
				break;
			default:

			return 1;
			}
		}
		
	}

	FILE *file1 = fopen("nVar.csv", "w");
	fprintf(file1, "n,k1,k2,k3\n");
	FOR1(n){
		fprintf(file1, "%d,%d,%d,%d\n", n, op1[n], op2[n], op3[n]);
	}
	fclose(file1);


	FILE* file2 = fopen("kVar.csv", "w");
	fprintf(file2, "k,op\n");
	/*
#define FOR2 
	n = 10000;

	for(k = 10; k <= 500; k += 10){
		int op = 0;
		init(op);
		perform(op);
		fprintf(file2, "%d,%d\n", k, op);
	}

	fclose(file2);
	*/
	printf("Done!\nPress any key to continue ...\n");
	getch();

	fclose(fdebug);
	return 0;
}