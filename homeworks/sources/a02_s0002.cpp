#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>

#define MAX_SIZE 10000
int k_compTD,k_asigTD,k_compBU,k_asigBU,tab;

void generare(int sir[],int n) //functie pt generare sir aleator
{
	srand(time(NULL));
	for(int i=1;i<=n;i++)
		sir[i]=rand();
}

void afisare(int sir[],int n) //functie pt afisare sir
{
	for(int i=1;i<=n;i++)
		printf("%d  ", sir[i]);
	printf("\n");

}

int p(int i)
{
	return i/2;
}


int dr(int i)
{
	return 2*i+1;
}

int st(int i)
{
	return 2*i;
}

void ReconstituieHeap(int A[], int i, int n)
{
	int s,d,ind,aux;
	s=st(i);
	d=dr(i);
	if(s<=n && A[s]>A[i])
		ind=s;
	else
		ind=i;
	k_compBU+=2;
	if(d<=n && A[d]>A[ind])
	{
		ind=d;
	}
	if(ind!=i)
	{
		aux=A[i];
		A[i]=A[ind];
		A[ind]=aux;
		k_asigBU+=3;
		ReconstituieHeap(A,ind,n);
	}
}

void construireHeap_BU(int A[], int n)
{
	for(int i = n/2; i>0; i--)	
		ReconstituieHeap(A, i, n);
}


void construireHeap_TD(int A[], int n, int k)
{
	int aux;
	A[n] = k;
	k_asigTD++;
	k_compTD++;
	while(A[n] > A[n/2] && n > 1)
	{
		aux = A[n];
		A[n] = A[n/2];
		A[n/2] = aux;
		n = n/2;
		k_asigTD+=3;
		k_compTD++;
	}
}

void afiseazaHeap(int A[], int i, int n, int tab)
{
	if(i>n)
	{
		return ;
	}
	afiseazaHeap(A, st(i), n,tab+1);
	for(int k=0;k<tab;k++)
		printf("\t");
	printf("%d \n", A[i]);
	//afiseazaHeap(A, st(i), n,tab+1);
	afiseazaHeap(A, dr(i), n,tab+1);
	
}

void HEAPSORT(int A[],int n)
{
	int aux;
	for(int i=n;i>1;i--)
	{
		aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		n=n-1;
		construireHeap_BU(A,1);
	}
}

void main()
{
	int A[MAX_SIZE],A1[MAX_SIZE], dim,i,sir[MAX_SIZE],k;
	FILE *f;
	/*printf("dim=");
	scanf("%d", &dim);
	while(dim>MAX_SIZE)
	{
		printf("Dimensuinea introdusa este prea mare!\ndim=");
		scanf("%d", &dim);
	}
	printf("Construire heap TOP DOWN\n");
	generare(sir,dim);
	afisare(sir,dim);
	for(i = 1; i <= dim; i++)
		construireHeap_TD(A, i, sir[i]);
	afisare(A,dim);
	afiseazaHeap(A, 1, dim,tab);
	printf("\nConstruire heap BOTTOM UP\n");
	// generare sir
	generare(sir,dim);
	afisare(sir,dim);
	for(i=1;i<=dim;i++)
		A1[i]=sir[i];
	construireHeap_BU(A1, dim);
	//afisare(A1,dim);
	afiseazaHeap(A1, 1, dim, tab);*/
	f=fopen("heap.csv","w");
	if(f==NULL)
    {
		perror("EROARE DESCIDERE!");
        exit(1);
    }
	else
	{
		for(dim=100;dim<=10000;dim+=100)
		{
			//printf("dim=%d\n",dim); 
			k_compTD=0;
			k_asigTD=0;
			k_compBU=0;
			k_asigBU=0;
			for(k=1;k<=5;k++)
			{
				generare(sir,dim);
				for(i = 1; i <= dim; i++)
					construireHeap_TD(A, i, sir[i]);
				for(i=1;i<=dim;i++)
					A1[i]=sir[i];
				construireHeap_BU(A1, dim);
			}
			fprintf(f,"%d,%d,%d,%d,%d,%d,%d \n",dim,k_asigTD/5,k_compTD/5,(k_asigTD+k_compTD)/5,k_asigBU/5,k_compBU/5,(k_asigBU+k_compBU)/5);
		}
	}
	fclose(f);
	printf("Terminare construire HEAP-uri\n");
	getch();
}