#include "stdafx.h"
#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//nod de arbore multicai
typedef struct Multi_cai {
	int key;
	int nr_fii; 
	struct Multi_cai *copil[50]; //numarul de fii prt un nod
}multi_cai;


//nod al arborelui binar
typedef struct arbore_binar{
    int key;
    arbore_binar* st; //adica primul fiu
    arbore_binar* dr;	//fratele din dreapta
}nod_binar;


//alocare spatiu pt multi-nod
multi_cai *creareMulti_nod(int key)//creez nod nou si ii atribui o cheie
{
	multi_cai *x = (multi_cai*) malloc(sizeof(multi_cai)); //aloc parametru
	x->key=key;
	for (int i=0; i<50; i++)		//aloc spatiu pt fiecare "copil" si setez cheile cu -1 
	{								
		x->copil[i] = (multi_cai *)malloc(sizeof(multi_cai)); //atribui spatiu ptr toti fii
		x->copil[i]->key=-1;//daca au cheia -1 nu exista momentan
	}
	x->nr_fii=0;

	return x;
}

void insert_nod_multi_way(multi_cai *parinte, multi_cai *copil)//insereaza un nod in arborele multi cai
{
	parinte->copil[parinte->nr_fii]=copil;
	parinte->nr_fii++;
}

//transformare din vector de tati in arbore multi-cai
multi_cai *transformare1(int t[], int size)
{
	multi_cai *rad;
	multi_cai *nod[50];
	int i;

	for(i=0; i<size; i++) //aloca spatiu
		nod[i]=creareMulti_nod(i);
	for(i=0;i<size;i++)
		if (t[i]!=-1)
			insert_nod_multi_way(nod[t[i]], nod[i]);
		else
			rad=nod[i];

	return rad;
}

//alocare spatiu pt nod binar

nod_binar *creareArbore_binar(int key)
{
    nod_binar *x; 
	x = (nod_binar*) malloc(sizeof(nod_binar));
    x->key=key;//atribui chieia
    x->st = (nod_binar*) malloc(sizeof(nod_binar));
    x->dr = (nod_binar*) malloc(sizeof(nod_binar));
    return x;
}

//transformare nod multi-cai in nod binar
nod_binar* transformare2(multi_cai *p, multi_cai *par, int i)	
{  //q=nodul, par=parintele lui q, i=iterator care numara al catelea fiu e

    nod_binar *nod_b;
    if(p->key!=-1)	//daca nodul exista  
    {
        nod_b = (nod_binar*)malloc(sizeof(nod_binar));	//cream un nou nod binar
        nod_b->key=p->key;	//copiem valoarea cheii
        if (p->nr_fii!=0)	//daca are fii
            nod_b->st = transformare2(p->copil[0], p, 0);	//apelare recursiva de primul fiu al lui p, de p si 0 ( pt ca e primul fiu )
        else
            nod_b->st = NULL;	//altfel, multi-nodul nu are fii  =>  nod_binar->st = null  
        if (par==0)
			 nod_b->dr=NULL;		//daca nu are parinte, adica e radacina =>  nu are nici frati => nod_binar->dr=null
		else	//daca are parinte, apelam functia pt urmatorul fiu al parintelui, adica pt primul frate al nodului la care suntem
            nod_b->dr = transformare2(par->copil[i+1], par, i+1);	       
    }
    else	//daca nodul nu exista, adica nod->key=-1 (e alocat spatiu din "createMN" dar el nu exista)
        nod_b=NULL;
	 return nod_b;
}


void pretty_print_multiway(multi_cai *rad, int nivel=0)
{
	for(int i=0;i<nivel;i++)
		{
			printf("   ");
		}
	printf("%d\n", rad->key);
	for(int i=0;i<rad->nr_fii;i++)
		pretty_print_multiway(rad->copil[i], nivel+1);
}

void pretty_print_arbore_binar(nod_binar *x, int dimensiune)
{
    int i;
	if (x==NULL)
		return;	
	pretty_print_arbore_binar(x->st, ++dimensiune);
	for(i=0;i<dimensiune;i++)
		printf("   ");
	printf("%d \n", x->key);
	pretty_print_arbore_binar(x->dr, dimensiune);
}


int main()
{
	int t[]={9,5,5,9,-1,4,4,5,5,4,2};
	int size=sizeof(t)/sizeof(t[0]);
	multi_cai *radacina = creareMulti_nod(0);
		radacina = transformare1(t, size);

	pretty_print_multiway(radacina,  0);

	printf("\n\n\n\n");

    nod_binar *rad = transformare2(radacina, 0, 0);
    pretty_print_arbore_binar(rad, 0);

	getch();
}
