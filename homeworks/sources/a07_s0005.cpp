#include <stdio.h>
#include <stdlib.h>

typedef struct _MULTI_NODE {
	int key;
	int count;
	struct _MULTI_NODE *child[50];
}MN;

typedef struct _BINARY_NODE {
	int key;
	struct _BINARY_NODE *st;
	struct _BINARY_NODE *dr;
}BN;

MN* createMN(int key) {
	MN *nod=(MN*)malloc(sizeof(MN));
	nod->key=key;
	nod->count=0;
	return nod;
}

void insertMN(MN* parent, MN* child) {
	parent->child[parent->count++]=child;
}

MN* transform1(int t[], int size) {
	MN* root;
	MN* nodes[50];
	int i;
	for(i=0;i<size;i++) {
		nodes[i]=createMN(i);
	}
	for(i=0;i<size;i++) {
		if(t[i]!=-1)
			insertMN(nodes[t[i]], nodes[i]);
		else
			root=nodes[i];
	}
	return root;
}

void transform2(MN* root, BN** root1)
{
	BN* aux;
	if( *root1 ==NULL)
	{
		(*root1) = (BN*)malloc(sizeof(BN));
		(*root1)->key= root->key;
		(*root1)->dr=NULL;
		(*root1)->st=NULL;
	}

	if(root->count!=0)
	{
		BN *q;
		q=(BN*)(malloc(sizeof(BN)));
		q->st=NULL;
		q->dr=NULL;
		q->key=root->child[0]->key;
		(*root1)->st=q;
		BN *p;
		p=(*root1)->st;
		transform2(root->child[0], &p);
		for(int i=1;i<root->count;i++)
		{
			aux=(BN*)(malloc(sizeof(BN)));
			aux->key=root->child[i]->key;
			aux->dr=NULL;
			aux->st=NULL;
			p->dr=aux;

			p=p->dr;
			transform2(root->child[i], &p);
		}
	}
}

void printx(BN *root,int depth)
{
	 if (root!=NULL)
	 {	 
			for (int j=0;j<depth;j++)
				printf("\t");
			printf("%d",root->key);
			printf("\n");
			printx(root->st,depth+1);
			printx(root->dr,depth);
	 }
}


void prettyPrint(MN* root, int nivel=0) {
	int i;
	for(i=0;i<nivel;++i) {
		printf("  ");
	}
	printf("%d\n", root->key);
	for(i=0;i<root->count;i++) {
		prettyPrint(root->child[i], nivel+1);
	}
}


int main () {
	int t[]={5, 5, 5, 9, -1, 4, 3, 4, 5, 4, 2};
	int size=sizeof(t)/sizeof(t[0]);
	MN* root=transform1(t, size);
	prettyPrint(root);
	BN* root1=NULL;
	printf("\n********************\n");
	transform2(root, &root1);
	printx(root1, 0);

	return 0;
}