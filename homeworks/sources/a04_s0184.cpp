#include<stdio.h>
#include<stdlib.h>
#include<time.h>
typedef struct nod
{
    int val;
    struct nod *next;
}NOD, *PNOD;

int nrOp;

void min_heapify(NOD *(*t)[5001], NOD *(*h)[5001], int i, int dimA)
{
	int l, r, smallest;
	NOD *aux;

	l = 2*i;
	r = 2*i + 1;

	if(l <= dimA && (*h)[l]->val < (*h)[i]->val)
	{
		smallest = l;
	}
	else
	{
		smallest = i;
	}

	if( r <= dimA && (*h)[r]->val < (*h)[smallest]->val)
	{
		smallest = r;
	}
	nrOp += 2;

	if(smallest != i)
	{
		aux = (*h)[i];
		(*h)[i] = (*h)[smallest];
		(*h)[smallest] = aux;

		aux = (*t)[i];
		(*t)[i] = (*t)[smallest];
		(*t)[smallest] = aux;

		nrOp += 6;

		min_heapify(t, h, smallest, dimA);
	}
}

void buildH_BU(NOD *(*t)[5001], NOD *(*h)[5001], int dimH)
{
	int i;
	for(i = dimH/2; i>0; i--)
	{	
		min_heapify(
			t, 
			h, 
			i, 
			dimH);
	}
}


void inserare(NOD **t, NOD **h, int val)	//
{
    NOD *n;

    //construim noul nod care va fi inserat
    n =(NOD *) malloc(sizeof(NOD));
    n->val = val;
    n->next = NULL;
    nrOp += 5; //1 alocare + 2 atribuiri + 2 cmp if

    if(*h == NULL) // lista este vida, inseram primul element
    {
        *t = *h = n;
        nrOp += 2; //2 atribuiri
        return;
    }

    if(h == t) // lista contine un singur element, il adaugam pe al 2-lea
    {
        *t = n;
        (*h)->next = *t;
        nrOp += 2; //2 atribuiri
        return;
    }

    //cazul general, avem mai mult de un element

    (*t)->next = n;
    *t = n;
    nrOp += 2; // 2 atribuiri
}
void extrageHead(NOD **t, NOD **h)
{
    NOD *p;
    // daca lista este vida ne oprim

    nrOp += 2; //2 cmp if
    if(*h == NULL)
    {
        return;
    }

    // daca lista are un singur element ea va deveni vida
    if(*h == *t)
    {
        free(*h);
        *h = *t = NULL;
        nrOp += 3; //1 free + 2 atribuiri
        return;
    }

    //cazul general
    p = *h;
    *h = (*h)->next;
    free(p);
    nrOp += 3; // 1 free + 2 atribuiri
}
void afiseazaCoada(NOD *head, NOD *tail)
{
    NOD *p;

    p = head;
    while(p != tail)
    {
        printf("%d ", p->val);
        p = p->next;
    }
    if( p!= NULL) // afisam si ultimul element
        printf("%d ", p->val);

    printf("\n");
}

//caseste headul minim si returneaza indexul acestuia
int minim(NOD *head[], int nrofH)
{
    int i, min, imin;
    
    //sarim peste headurile nule
    i = 0;
    nrOp ++; //compare din while
    while(head[i] == NULL && i<nrofH)
    {
        i++;
        nrOp ++; //compare din while
    }
    //daca tate au fost nulle ne oprim
    nrOp ++; //cmp din if
    if(i == nrofH)
        return -1;

    nrOp += 2; //2 atribuiri
    min = head[i]->val;
    imin = i;

    i++;
    nrOp ++; //cmp din while
    while(i<nrofH)
    {
        nrOp += 3; //cmp din while + 2 cmp if
        if(head[i] == NULL)
        {
            i++;
            continue;
        }

        if(min > head[i]->val)
        {
            min = head[i]->val;
            imin = i;
            nrOp += 2; //2 atribuiri
        }
        i++;
    }

    return imin;
}

int main(int argc, char *argv[])
{
    NOD *head[5001], *tail[5001], *headF, *tailF;
    int i, nr, j, k = 10, n = 10, ok, nrListe, nrOp1, nrOp2;
    FILE *f;

    f = fopen("date.csv", "w");

	fprintf(f, "lungime, k = 5, k = 10, k = 100\n");

    srand(time(NULL));
    headF = tailF = NULL;

        

        for(n = 100; n <= 10000; n+=100 )
        {

			//////////////////////////////////////////////////////////////////////////// k = 5
			k = 5;
            //generam K cozi ordonate de lungime N
            for(j = 1; j <= k; j++)
            {
                head[j] =tail[j] = NULL;     

                nr = 0;
                for(i = 0; i < n/k; i++)
                {
                    nr += rand()%100+1;
                    inserare(&tail[j], &head[j], nr);
                }

                //afiseazaCoada(head[j], tail[j]);
				
            }
			buildH_BU(
				&tail, 
				&head, 
				k);

			/*afiseazaCoada(head[1], tail[1]);
			afiseazaCoada(head[2], tail[2]);
			afiseazaCoada(head[3], tail[3]);*/
            //printf("\n");
			//getch();
            //for(i = 0; i<= 11 ; i++)                    // verificare stergere
            //    extrageHead(&tail[1], &head[1]);
            //printf("\n");
            //afiseazaCoada(head[1], tail[1]);

			printf("n=%d, k=%d\n", n, k);
            // interclasar
            nrOp = 0;
            ok = 0;
			nrListe = k;
            while(1)
            {   
				nrOp ++;
				if(head[1] == NULL)		// daca varful este null toate listele sunt nule
					break;

                inserare(&tailF, &headF, head[1]->val);		
                extrageHead(&tail[1], &head[1]);
                ok ++;  

				nrOp ++;
				while(head[1] == NULL && nrListe > 0) 
				{
					head[1] = head[nrListe];
					tail[1] = tail[nrListe];

					head[nrListe] = NULL;
					tail[nrListe] = NULL;

					nrListe --;
					nrOp +=4;
				}
				if(nrListe == 0)
					break;
				min_heapify(&tail, &head, 1, nrListe);

                if(ok > k * n)
                {
                    printf("[ERROR]CEVA NU A MERS BINE IN COD !!\n");
                    printf("while a mers de mai mult de n*k ori\n");
                    fprintf(f, "[WARNING] DATE NERELEVANTE\n");
                    break;
                }        
            }
            

			//afiseazaCoada(headF, tailF);
			//getch();
            //elibereaza lista headF (pentru urmatorul ciclu)
            for(i = 0; i<= k*n ; i++)                    
                extrageHead(&tailF, &headF);
			nrOp1 = nrOp;


	////////////////////////////////////////////////////// k = 10

			k = 10;
            //generam K cozi ordonate de lungime N
            for(j = 1; j <= k; j++)
            {
                head[j] =tail[j] = NULL;     

                nr = 0;
                for(i = 0; i < n/k; i++)
                {
                    nr += rand()%100+1;
                    inserare(&tail[j], &head[j], nr);
                }

                //afiseazaCoada(head[j], tail[j]);
				
            }
			buildH_BU(
				&tail, 
				&head, 
				k);

			/*afiseazaCoada(head[1], tail[1]);
			afiseazaCoada(head[2], tail[2]);
			afiseazaCoada(head[3], tail[3]);*/
            //printf("\n");
			//getch();
            //for(i = 0; i<= 11 ; i++)                    // verificare stergere
            //    extrageHead(&tail[1], &head[1]);
            //printf("\n");
            //afiseazaCoada(head[1], tail[1]);

			printf("n=%d, k=%d\n", n, k);
            // interclasar
            nrOp = 0;
            ok = 0;
			nrListe = k;
            while(1)
            {   
				nrOp ++;
				if(head[1] == NULL)		// daca varful este null toate listele sunt nule
					break;

                inserare(&tailF, &headF, head[1]->val);		
                extrageHead(&tail[1], &head[1]);
                ok ++;  

				nrOp ++;
				while(head[1] == NULL && nrListe > 0) 
				{
					head[1] = head[nrListe];
					tail[1] = tail[nrListe];

					head[nrListe] = NULL;
					tail[nrListe] = NULL;

					nrListe --;
					nrOp +=4;
				}
				if(nrListe == 0)
					break;
				min_heapify(&tail, &head, 1, nrListe);

                if(ok > k * n)
                {
                    printf("[ERROR]CEVA NU A MERS BINE IN COD !!\n");
                    printf("while a mers de mai mult de n*k ori\n");
                    fprintf(f, "[WARNING] DATE NERELEVANTE\n");
                    break;
                }        
            }
            

			//afiseazaCoada(headF, tailF);
			//getch();
            //elibereaza lista headF (pentru urmatorul ciclu)
            for(i = 0; i<= k*n ; i++)                    
                extrageHead(&tailF, &headF);
			nrOp2 = nrOp;

			///////////////////////////////////////////////////////////// k = 100

			k = 100;
            //generam K cozi ordonate de lungime N
            for(j = 1; j <= k; j++)
            {
                head[j] =tail[j] = NULL;     

                nr = 0;
                for(i = 0; i < n/k; i++)
                {
                    nr += rand()%100+1;
                    inserare(&tail[j], &head[j], nr);
                }

                //afiseazaCoada(head[j], tail[j]);
				
            }
			buildH_BU(
				&tail, 
				&head, 
				k);

			/*afiseazaCoada(head[1], tail[1]);
			afiseazaCoada(head[2], tail[2]);
			afiseazaCoada(head[3], tail[3]);*/
            //printf("\n");
			//getch();
            //for(i = 0; i<= 11 ; i++)                    // verificare stergere
            //    extrageHead(&tail[1], &head[1]);
            //printf("\n");
            //afiseazaCoada(head[1], tail[1]);

			printf("n=%d, k=%d\n", n, k);
            // interclasar
            nrOp = 0;
            ok = 0;
			nrListe = k;
            while(1)
            {   
				nrOp ++;
				if(head[1] == NULL)		// daca varful este null toate listele sunt nule
					break;

                inserare(&tailF, &headF, head[1]->val);		
                extrageHead(&tail[1], &head[1]);
                ok ++;  

				nrOp ++;
				while(head[1] == NULL && nrListe > 0) 
				{
					head[1] = head[nrListe];
					tail[1] = tail[nrListe];

					head[nrListe] = NULL;
					tail[nrListe] = NULL;

					nrListe --;
					nrOp +=4;
				}
				if(nrListe == 0)
					break;
				min_heapify(&tail, &head, 1, nrListe);

                if(ok > k * n)
                {
                    printf("[ERROR]CEVA NU A MERS BINE IN COD !!\n");
                    printf("while a mers de mai mult de n*k ori\n");
                    fprintf(f, "[WARNING] DATE NERELEVANTE\n");
                    break;
                }        
            }
            

			//afiseazaCoada(headF, tailF);
			//getch();
            //elibereaza lista headF (pentru urmatorul ciclu)
            for(i = 0; i<= k*n ; i++)                    
                extrageHead(&tailF, &headF);


			fprintf(f, "%d, %d, %d, %d\n", n, nrOp1, nrOp2, nrOp);
        }

///////////////////////////////////// k variabil, n constant /////////////////////////////////////
		fprintf(f, "\n\nk, nrOp\n");

		n = 10000;
		for(k = 10; k<=500; k+=10)
		{
			//generam K cozi ordonate de lungime N
            for(j = 1; j <= k; j++)
            {
                head[j] =tail[j] = NULL;     

                nr = 0;
                for(i = 0; i < n/k; i++)
                {
                    nr += rand()%100+1;
                    inserare(&tail[j], &head[j], nr);
                }

                //afiseazaCoada(head[j], tail[j]);
				
            }
			buildH_BU(
				&tail, 
				&head, 
				k);

			/*afiseazaCoada(head[1], tail[1]);
			afiseazaCoada(head[2], tail[2]);
			afiseazaCoada(head[3], tail[3]);*/
            //printf("\n");
			//getch();
            //for(i = 0; i<= 11 ; i++)                    // verificare stergere
            //    extrageHead(&tail[1], &head[1]);
            //printf("\n");
            //afiseazaCoada(head[1], tail[1]);

			printf("n=%d, k=%d\n", n, k);
            // interclasar
            nrOp = 0;
            ok = 0;
			nrListe = k;
            while(1)
            {   
				nrOp ++;
				if(head[1] == NULL)		// daca varful este null toate listele sunt nule
					break;

                inserare(&tailF, &headF, head[1]->val);		
                extrageHead(&tail[1], &head[1]);
                ok ++;  

				nrOp ++;
				if(head[1] == NULL && nrListe > 0) 
				{
					head[1] = head[nrListe];
					tail[1] = tail[nrListe];

					head[nrListe] = NULL;
					tail[nrListe] = NULL;

					nrListe --;
					nrOp +=4;
				}
				if(nrListe == 0)
					break;
				min_heapify(&tail, &head, 1, nrListe);

                if(ok > k * n)
                {
                    printf("[ERROR]CEVA NU A MERS BINE IN COD !!\n");
                    printf("while a mers de mai mult de n*k ori\n");
                    fprintf(f, "[WARNING] DATE NERELEVANTE\n");
                    break;
                }        
            }
            

			//afiseazaCoada(headF, tailF);
			//getch();
            //elibereaza lista headF (pentru urmatorul ciclu)
            for(i = 0; i<= k*n ; i++)                    
                extrageHead(&tailF, &headF);

			fprintf(f, "%d, %d\n", k, nrOp);
		}
  
    //afiseazaCoada(headF, tailF);

    return 0;
}