/*
--Name: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
--Group: ***GROUP_NUMBER***
--College: Technical University of Cluj Napoca

--Algorithms presented and compared: Insertion sort, selection sort, bubble sort

--Start date: 5 Mar 2013
--End date: 12 Mar 2013

--Evaluation requirements: Discussion, interpretations, efficiency, comparison, stability

-- Discussion:

In the following, we compare the performance of three sorting algorithms: Insertion sort, Selection sort, and Bubble sort.
All three algorithms have been improved compared to their standard implementations:
Insertion sort - Starts from 2nd element.
Selection sort - Stores only element positions, during evaluation
Bubble sort - Considers elements after last swap to be sorted

-- Interpretation:

Selection sort: Exchanges the first nonsorted element with the minimum element of the nonsorted group.

Insertion sort: Inserts an unsorted element into the sorted group after finding an appropriate position.
This is what humans being intuitively do, for example while arranging cards.

Bubble sort: Checks every if the current element is larger than the element following it, and swaps them if that is the case.

-- Efficiency: 

Selection sort has an O(n^2) performance in all cases, making it inefficient for large data sets.
It performs at most n swaps, making it useful when swapping elements is expensive.

Insertion sort has an O(n^2) average and worst case performance, meaning it is inefficient when cosnidering large data sets.
It relies on shifting elements, which is expensive.

Bubble sort has an O(n^2) average and worst case performance. It is therefore not a very efficient algorithm
for dealing with large data sets. It is useful on small data sets, or on sets of any size that are almost sorted.

-- Comparison:

Best case: Bubble sort is the most efficient. Insertion sort is second most efficient, selection sort is the least efficient.

Average case:
Bubble sort performs worst. Selection sort and insertion sort have similar performances, being around 2.2 times faster than bubble sort.
Insertion sort tends to outperform selection sort by a (very) small margin, but they mostly overlap in the chart.

Worst case:

Bubble sort performs worst. Selection sort is worse than insertion by 2x insertion sort is middle-way, around 2 times faster than bubble sort.

Stability: All 3 algorithms are stable due to their comparison conditions.
*/


//#include "Profiler.h"
#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
using namespace std;

//Profiler profiler("demo");


#define ARRLEN 25
#define MAX_SIZE 10000
int A[ARRLEN];
int B[ARRLEN];
int C[ARRLEN];
int iSassig, iScomp;
int sSassig, sScomp;
int bSassig, bScomp;
int n = ARRLEN; //the number of array elems
  ofstream myfile; //files to write to

void insertionSort (int B[])
{
int k; //rightmost element of ordered partition
int i; //counter
int buff; //insertion element

  myfile.open ("iSort.txt",ios::app);
  //myfile << "Writing this to a file.\n" << n << endl;
  //myfile.close();

iSassig = 0; iScomp=0;
	for(i=2; i<n; i++)
	{
		k = i - 1;
		//iSassig++; //not sure
		//profiler.countOperation("insertionsort-assig", n);
		iSassig++;
		buff = B[i]; //Assigments++;
		//profiler.countOperation("insertionsort-assig", n);
		while (B[k]> buff && k >=0) //Comparing with > instead of >= ensures algorithm stability
		{
			//profiler.countOperation("insertionsort-comp", n);
			iScomp++;
			B[k+1] = B[k]; //Assignments++
			iSassig++;
			//profiler.countOperation("insertionsort-assig", n);
			k--;
		} //Comparisons++, while condition check
		iScomp++;
		//profiler.countOperation("insertionsort-comp", n); //Comparisons++, 1 additional check after the while loop ends
		B[k+1]=buff; //Assignments++
		iSassig++;
		//profiler.countOperation("insertionsort-assig", n);
	}
	printf("Insertion: A: %d C: %d T: %d \n",iSassig, iScomp, iSassig+iScomp);
		myfile << n << "," << iSassig << "," << iScomp<< "," << iSassig+iScomp << endl ;

	myfile.close();
}

void selectSort (int A[])
{
	int min, i_min; //min is the value, i_min is position of min value
	int i, j; //counters
	sSassig = 0; sScomp=0;
	  myfile.open ("sSort.txt",ios::app);
  //myfile << "Writing this to a file.\n";



	for(i=0; i<n-1; i++)
	{
		i_min = i;
		//profiler.countOperation("selectsort-assig", n);
		//sSassig++;
		for (j=i+1; j<n; j++){
			if (A[j]<A[i_min]){ //Comparisons++, maintains stability
				//sScomp++;
				i_min=j;
				//profiler.countOperation("selectsort-assig", n);
				//sSassig++;
			}
			sScomp++;
		//profiler.countOperation("selectsort-comp", n);
		}
		//sScomp++;
		//profiler.countOperation("selectsort-comp", n);
		if (i!=i_min) //Swap, only if between different elements
		{
			min = A[i_min]; //prepare min for swap
			sSassig++;
			//profiler.countOperation("selectsort-assig", n);
			sSassig++;
			A[i_min]=A[i];
			//profiler.countOperation("selectsort-assig", n);
			sSassig++;
			A[i]=min; //Assignments +3;
			//profiler.countOperation("selectsort-assig", n);
		}
	}
	printf("Selection: A: %d C: %d T: %d \n",sSassig, sScomp, sSassig+sScomp);
	myfile << n << "," << sSassig << "," << sScomp<< "," << sSassig+sScomp << endl ;

	myfile.close();
}

void bubbleSort (int A[])
{
int newlength; int length = n; int temp;
int i; //counter
bSassig = 0; bScomp=0;
	  myfile.open ("bSort.txt",ios::app);


	while (length>0)
	{
		//bScomp++;
		//profiler.countOperation("bubblesort-comp", n);
		newlength=0;
		for(i=1; i<length; i++)
		{
			//profiler.countOperation("bubblesort-comp", n);
			bScomp++;
			if (A[i-1] > A[i]){ //Ensures algorithm stability
				bSassig++;
				temp = A[i-1]; //swap(A[i-1], A[i])
				//profiler.countOperation("bubblesort-assig", n);
				A[i-1]=A[i];
				bSassig++;
				//profiler.countOperation("bubblesort-assig", n);
				A[i]=temp;
				bSassig++;
				//profiler.countOperation("bubblesort-assig", n);
				newlength=i;
			}
		}
		length = newlength;
	}
	//bScomp++;
	printf("Bubble: A: %d C: %d T: %d \n",bSassig, bScomp, bSassig+bScomp);
	  myfile << n << "," << bSassig << "," << bScomp<< "," << bSassig+bScomp << endl ;
  myfile.close();
	//profiler.countOperation("bubblesort-comp", n);
}

int main()
{

	//Algorithm corectness hardcode
printf("Hello Politehnica!\n");
printf("\n A short hardcoded demo of algorithm corectness follows. \n");
printf ("\n Original array: ");
int A[] = { 1,5,1,4,1,
	2, 1, 5, 139, 351,
	222, 214, 151, 363, 359,
	22, 11, 25, 25, 599, 135,
354, 222, 21, 252, 252, 111};
for(int k=0; k<n; k++)
{
printf("%d ",A[k]);
}
printf ("\n Bubble sort: ");
bubbleSort(A);
for(int k=0; k<n; k++)
{
printf("%d ",A[k]);
}
int B[] = { 1,5,1,4,1,
	2, 1, 5, 139, 351,
	222, 214, 151, 363, 359,
	22, 11, 25, 25, 599, 135,
354, 222, 21, 252, 252, 111};
printf ("\n Insertion sort: ");
insertionSort(B);
for(int k=0; k<n; k++)
{
printf("%d ",B[k]);
}
int C[] = { 1,5,1,4,1,
	2, 1, 5, 139, 351,
	222, 214, 151, 363, 359,
	22, 11, 25, 25, 599, 135,
354, 222, 21, 252, 252, 111};
printf ("\n Selection sort: ");
selectSort(C);
for(int k=0; k<n; k++)
{
printf("%d ",C[k]);
}
printf("\n\n Algorithm evaluation follows. The input size will be printed during evaluation, and a report will be generated at its conclusion. \n");
_getch();




	int v[MAX_SIZE];
	int x[MAX_SIZE];
	int y[MAX_SIZE];
	int z[MAX_SIZE];
	
	
	//average case
	printf("\n Average case evaluation: \n\n");


	for (int j=0; j<5; j++){

					  myfile.open ("iSort.txt",ios::app);
		myfile <<"Average case " << j << endl << endl;

	myfile.close();

						  myfile.open ("bSort.txt",ios::app);
		myfile <<"Average case " << j << endl << endl;

	myfile.close();
						  myfile.open ("sSort.txt",ios::app);
		myfile <<"Average case " << j << endl << endl;

	myfile.close();

		for(n=100; n<10000; n += 500){
			printf ("%d \n\n ", n);
			for (int j=0; j<n; j++){
			v[j] = rand() % 1010;
			x[j] = v[j];
			y[j] = v[j];
			z[j] = v[j];
			}
			bubbleSort(x); selectSort(y); insertionSort(z);	
			}
	}
		  myfile.open ("iSort.txt",ios::app);
		myfile <<"Worst case" << endl << endl;

	myfile.close();

	
						  myfile.open ("bSort.txt",ios::app);
		myfile <<"Worst case " << endl << endl;

	myfile.close();
						  
		//worst case
		printf("\n Worst case evaluation: \n\n");
			for(n=100; n<10000; n += 500){
			printf ("%d \n\n", n);
			for (int p=n; p>0; p--){

			x[p] = n-p;
			y[p] = n-p;
			z[p] = n-p;
			}

			bubbleSort(x); 
			//selectSort(y); 
			insertionSort(z); 

	}
			myfile.open ("sSort.txt",ios::app);
		myfile <<"Worst case " << endl << endl ;

	myfile.close();

			printf("\n Worst case evaluation (SS): \n\n");
			for(n=100; n<10000; n += 500){
			printf ("%d \n\n", n);
			for (int p=n-1; p>0; p--){

			y[p] = n-p+1;
			y[n] = 0;
			}

			selectSort(y); 

	}
	
	//best case
	  myfile.open ("iSort.txt",ios::app);
		myfile <<"Best case" << endl << endl ;

			
						  myfile.open ("bSort.txt",ios::app);
		myfile <<"Best case " << endl << endl ;

	myfile.close();
						  myfile.open ("sSort.txt",ios::app);
		myfile <<"Best case " << endl << endl ;

	myfile.close();


	myfile.close();
	printf("\n Best case evaluation: \n\n");
			for(n=100; n<10000; n += 500){
			printf ("%d \n\n", n);
			for (int j=0; j<n; j++){
			x[j] = j;
			y[j] = j;
			z[j] = j;
			}
			bubbleSort(x); selectSort(y); insertionSort(z);
			
	}
	//	profiler.addSeries("insertionsort", "insertionsort-comp", "insertionsort-assig");
	//profiler.addSeries("selectsort", "selectsort-comp", "selectsort-assig");
	//profiler.addSeries("bubblesort", "bubblesort-comp", "bubblesort-assig");
	//profiler.createGroup("Comparisons", "insertionsort-comp","bubblesort-comp","selectsort-comp");
	//profiler.createGroup("Assignments",  "insertionsort-assig",  "bubblesort-assig",  "selectsort-assig");
	//	profiler.createGroup("Overall Operations", "insertionsort", "selectsort", "bubblesort");
	//profiler.showReport();

			_getch();

}

