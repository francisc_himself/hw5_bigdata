#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

int b[20000];
int w[20000];
int r[20000];
int n;

int comp;
int attr;

FILE *f=fopen("Rezultate1.xls","w+");

//CREAREA SIRURILOR
//best, worst, random

void sir(int n)
{
	for (int i=0; i<n; i++)
	{
		b[i] = i;
		r[i] = rand() % n;
		w[i] = n-i-1;
		
	}
}

//INSERTION SORT

void insertion (int a[1000])
{
	int j, i, index;

	comp = 0;
	attr = 0;

	for (i = 0; i < n; i++)
	{
		index = a[i];
		j = i;
		attr+=1;

		while ( comp+=1, ((j > 0) && (a[j-1] > index)))
		{
			a[j] = a[j-1];
			j = j-1;
			attr+=1;
		}
		a[j] = index;
		attr++;
	}

fprintf(f,"\t%d\t", comp);
fprintf(f,"\t%d\t", attr);
fprintf(f,"\t%d\t", attr+comp);
}

//Selection Sort
void selection (int a[1000])
{
	int j, i, min, temp;
	comp = 0;
	attr = 0;
	for (i = 0; i < n-1; i++)
	{
		min = i;
		for (j = i+1; j < n; j++)
		{
			if (comp++, a[j] < a[min])
				min = j;
		}
		temp = a[i];
		a[i] = a[min];
		a[min] = temp;
		attr+=3;
	}
fprintf(f,"\t%d\t", comp);
fprintf(f,"\t%d\t", attr);
fprintf(f,"\t%d\t", attr+comp);
}


//Bubble Sort
void bubblesort (int a[2000])
{
	int sorted;
	int temp,i;

	comp = 0;
	attr = 0;

	do
	{
		sorted = 1;
		for (i = 0;  i < (n-1); i++)
			if (comp++, a[i] > a[i+1])
			{
				temp = a[i];
				a[i] = a[i+1];
				a[i+1] = temp;
				sorted = 0;
				attr+=3;
			}
	}
	while (!sorted);

fprintf(f,"\t  %d\t", comp);
fprintf(f,"\t  %d\t", attr);
fprintf(f,"\t%d\t", attr+comp);

}


//Binary Insertion Sort
void binary_insertion (int a[1000])
{
	int i, j, m;
	int hi, lo, temp;
	comp = 0;
	attr = 0;

	for (i = 1; i < n; i++)
	{
		lo = 0;
		hi = i;
		m = i >> 1;

		do
		{
			if (comp++, a[i] > a[m])
			{
				lo = m + 1;
			}
			else if (comp++, a[i] < a[m])
			{
				hi = m;
			}
			else break;

			m = (hi + lo) >> 1;
		}
		while (lo < hi);

		if (m < i)
		{
			temp = a[i];
			attr++;
			for (j = i; j > m; j--)
			{
				a[j] = a[j-1];
				attr++;
			}
			a[m] = temp;
			attr++;
		}
	}
fprintf(f,"\t%d\t", comp);
fprintf(f,"\t%d\t", attr);
fprintf(f,"\t%d\t", attr+comp);
}


void InsertionSort()
{
	insertion(b);	
	insertion(r);
	insertion(w);
}

void SelectionSort()
{
	selection(b);
	selection(r);
	selection(w);
}

void BubbleSort()
{
	bubblesort(b);
	bubblesort(r);
	bubblesort(w);	
}

void BinaryInsertionSort()
{
	binary_insertion(b);
	binary_insertion(r);
	binary_insertion(w);	
}

void main ()
{
	srand( (unsigned)time( NULL ) );
		fprintf(f,"\n Bubble Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");
	for (n = 10; n<=10000; n+=300)
	{

		sir(n);
		BubbleSort();
		fprintf(f,"\n");
	}

		fprintf(f,"\n\n Selection Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");
	for (n = 10; n<=10000; n+=300)
	{
		sir(n);
		InsertionSort();
		fprintf(f,"\n");
	}

		fprintf(f,"\n\n Insertion Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");
	for (n = 10; n<=10000; n+=300)
	{
		sir(n);
		SelectionSort();
		fprintf(f,"\n");
	}

		fprintf(f,"\n\n Binary Insertion Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");
	for (n = 10; n<=10000; n+=300)
	{
		sir(n);
		BinaryInsertionSort();
		fprintf(f,"\n");
	}


	printf(" Rezultatele au fost scrise in fisierul 'Rezultate1.xls'.");
	printf("\n Apasati o tasta pentru a termina rularea programului");
	getch();
fclose(f);
}