#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
/* Tehnica Bottom Up vs Bottom Down
 AVERAGE CASE:  se poate observa cu usurinta o clasificare in functie de comparatii,
				atribuiri si suma lor
				1.COMPARATII: d.p.d.v al comparatiilor, tehnica top down este mai eficienta (nr comparatilor este cuprins intre 112->12621)
				decat tehnica bottom up ( 238->24582)
				2.ATRIBUIRI: si aici, tehnica top down este mai eficienta, avand un numar de atribuiri cuprins intre 112->12621, in timp ce
				tehnica bottom up are un numar de atribuiri cuprins intre 208 si 22023
				3.ATRIBUIRI+COMPARATII: de asemenea, si in acest caz, tehnica top down este mai eficienta (224-25242) decat tehnica bottom up (446-46605)

Per ansamblu, tehnica Top Down este mai eficienta decat tehnica Bottom Up
*/

using namespace std;
//ofstream g ("afisare.txt");
long upcomp=0;
long upatrib=0;
long downcomp=0;
long downatrib=0;

//Heap
void heapify(long v[], int i, int n)
{
    int l,r,max;
    long aux;

    l=2*i;
    r=2*i+1;

    upcomp++;
    if (l<=n && v[l]>v[i])
        max=l;
    else
        max=i;

    upcomp++;
    if (r<=n && v[r]>v[max])
        max=r;
    if (max!=i)
        {
            upatrib+=3;
            aux=v[i];
            v[i]=v[max];
            v[max]=aux;
            heapify(v,max,n);
        }
}
//Bottom-up-->O(lgn)
void bh_bottomup(long v[],int n)
{
    int i;
    for (i=n/2;i>=1;i--)
        heapify(v,i,n);
}

//Construirea unui heap prin insertie
void insert_in_heap(long v[],long key,int *dim)
{
    (*dim)=(*dim)+1;
    int i;
    i=(*dim);
    while (i>1 && v[i/2]<key)
    {
        downcomp++;
        downatrib++;
        v[i]=v[i/2];
        i=i/2;
    }
    v[i]=key;
}
//Top Down--> O(nlgn)
void bh_topdown(long v[],int n)
{
    int dimension=1,i;
    for (i=2;i<=n;i++)
        insert_in_heap(v,v[i],&dimension);
}

void afisare(long *v,long n,long k,long nivel)
{
	if (k>n)
	    return;
	else
	{
	afisare(v,n,2*k+1,nivel+1);
	for(int i=1;i<nivel;i++)
		cout<<"  ";
	cout<<v[k-1]<<endl;
	afisare(v,n,2*k,nivel+1);
	// g.close();
	}
}


int main()
{
   srand(time(NULL));
	long a[10]={4,1,3,2,16,9,10,14,8,7};
	//ofstream g("afisare.txt");
    long bu[10000];
    long td[10000];
    int i,j,k,nr;
    long compup,compdown,atribup,atribdown;
	ofstream f ("buildheap.txt");
	
     for (i=100;i<10000;i=i+100)
	{
		//suma dintre comparatii si atribuiri;
		upcomp=downcomp=upatrib=downatrib=0;

		for (j=1;j<=5;j++)
		{
			nr=0;
			compup=0;compdown=0;
			atribup=0;atribdown=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				bu[nr]=td[nr]=rand();
			}
			bh_bottomup(bu,i);
			bh_topdown(td,i);
			compup+=upcomp;
			compdown+=downcomp;
			atribup+=upatrib;
			atribdown+=downatrib;
		}
			f<<i<<","<<compup/5<<","<<atribup/5<<","<<compup/5+atribup/5<<","<<compdown/5<<","<<atribdown/5<<","<<compdown/5+atribdown/5<<endl;
			
	}      
	bh_bottomup(a,10);
	cout<<"Tehnica Bottom-Up"<<endl;
	afisare(a,10,1,1);
	cout<<"Tehnica Top-Down"<<endl;
	bh_topdown(a,10);
	afisare(a,10,1,1);
   f.close();
   //g.close();
	return 0;
}
