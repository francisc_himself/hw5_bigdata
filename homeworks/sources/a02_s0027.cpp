// Tema_2.cpp : Defines the entry point for the console application.
//Se cere implementarea corecta si eficienta a doua metode de construire a structurii de date Heap "de jos in sus" (bottom-up) si "de sus in jos" top-down
//Observatii: eficienta top down: O(nlog2(n))
//			  eficienta bottom up: O(n)				

//Din grafic se poate observa k metoda bottom up este mai eficienta decat metoda top down

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

int n,dim=0,nr_comp_bu=0, nr_at_bu=0 ,nr_comp_td=0, nr_at_td=0;
int  b[10001];
int a[10001];

int Parinte(int i)
	{
		return (i/2); 
	}


int Stanga(int i)
	{
		return (2*i);
	}


int Dreapta(int i)
	{
		return ((2*i) + 1);
	}


void min_heapify(int a[], int i )
	{
		int aux1,min;
		int l = Stanga(i);
		int r = Dreapta(i);
		nr_comp_bu++;
		if( ( l<= n ) && (a[l] < a[i] ))
				min = l;
		else
				min = i;
		nr_comp_bu++;
		if((r <= n ) && (a[r] < a[min]))
				min = r;
		if( min!=i )
			{
				nr_at_bu++;
				aux1 = a[i];
				a[i] = a[min];
				a[min] = aux1;
				min_heapify(a, min);
			}
	}


//Construirea heap-ului prin tehnica Bottom up
	void Construire_H_BU(int a[])
		{
			nr_comp_bu=0;
			nr_at_bu=0;
			int i;
			for(i=n/2;i>=1;i--)
				min_heapify(a, i);
		}



//Initializare
	void H_init(int a[])
		{
			dim = 0;
		}


//Inserare element
	void H_push(int a[], int x)
		{
			int i,aux2 ;
			nr_at_td++;
			dim++;
			a[dim]=x;
			i = dim;
			nr_comp_td++;
			while ((i>1) && (a[Parinte(i)] > b[i]))
				{
					nr_comp_td++;
					nr_at_td++;
					aux2 = a[Parinte(i)];
					a[Parinte(i)] = a[i];
					a[i] = aux2;
					i = Parinte(i);
				}
		}



	void Construire_H_TD(int b[] ,int a[])
		{
		H_init(b); //sau dim=0
		nr_comp_td=0,nr_at_td=0;
		for(int i=1;i<=n;i++)
			H_push(b,a[i]);
		
		}

	
	void Preety_print(int *v,int k,int nivel)  
		{
		int i;
		if (k>n)
		return;
			Preety_print(v,2*k+1,nivel+1);
				for(i=1; i<=nivel;i++)
					printf("  ");
					printf("%d\n",v[k]);
			Preety_print(v,2*k,nivel+1);
		}

	int main()
	{
		int i,j=0;
		int c[10001];

		int sir1[6];
		int sir[6];
		sir1[1]=20;sir1[2]=13;sir1[3]=8;sir1[4]=5;sir1[5]=15;sir1[6]=3;n=6;
		//heapify(test,n);
		sir[1]=20;sir[2]=13;sir[3]=8;sir[4]=5;sir[5]=15;sir[6]=3;n=6;
	
		printf("Construire arbore Bottom up\n \n");
		Construire_H_BU(sir);
		Preety_print(sir,1,1);
		printf("\n\n\n");
		printf("Construire arbore Top down\n \n");
		Construire_H_TD(sir1,sir1);
		Preety_print(sir1,1,1);

		FILE *fp=fopen("tema2.csv","w");
		int nr_at1,nr_at2,nr_comp1,nr_comp2;

		//Afisarea detelor in tabel
		 fprintf(fp,"n,Bottom_up_at, Bottom_up_comp,Bottom_up_at+comp,Top_down_at, Top_down_comp,Top_down_at+comp \n");

		for(n=100;n<=10000;n=n+100)
			{
				nr_at1=0;
				nr_at2=0;
				nr_comp1=0;
				nr_comp2=0;
				printf("%d\n", n);
		for(int m=1;m<=5;m++)
			{	
				int i;
				srand (time(NULL));
				for(i=1;i<=n;i++)
					{				
					a[i]=rand()%1000;
					c[i]=a[i];
					}	

			Construire_H_BU(c);
			nr_comp1=nr_comp1+nr_comp_bu;
			nr_at1=nr_at1+nr_at_bu;

			Construire_H_TD(b,a);
			nr_comp2=nr_comp2+ nr_comp_td;
			nr_at2=nr_at2+ nr_at_td;

		}
		fprintf(fp, "%d,%d,%d,%d,%d,%d,%d \n",n,nr_at1/5,nr_comp1/5,(nr_at1+nr_comp1)/5,nr_at2/5,nr_comp2/5,(nr_at2+nr_comp2)/5);
		
	}
		fclose(fp);

		getch();
	
	return 0;
}



