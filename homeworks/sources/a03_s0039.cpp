#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 10001

int A[MAX];
int B[MAX];
int n;
long contor_heap_total=0,contor_quick_total=0;
long contor_heap=0,contor_quick=0;

int elementul_stang(int i)
{
	return 2*i;
}

int elementul_drept(int i)
{
	return (2*i)+1;
}

void Reconstituire_Heap(int A[],int i,int n)
{
	int stanga=elementul_stang(i);
	int dreapta=elementul_drept(i);
	int poz_max,aux;
	contor_heap++;
	if(stanga<=n && A[stanga]>A[i])
	{
		poz_max=stanga;
	}
	else
	{
		poz_max=i;
	}
	contor_heap++;
	if(dreapta<=n && A[dreapta]>A[poz_max])
	{
		poz_max=dreapta;
	}
	if(poz_max !=i)
	{
		contor_heap+=3;
		aux=A[i];
		A[i]=A[poz_max];
		A[poz_max]=aux;
		Reconstituire_Heap(A,poz_max,n);
	}
}
void Buttom_Up(int A[],int n)
{
	int i;
	for(i=n/2;i>=1;i--) Reconstituire_Heap(A,i,n);
}
void heap_sort(int A[],int n)
{
	int i;
	int aux;
	int k=n;
	Buttom_Up(A,k);
	for(i=n;i>=2;i--)
	{
		contor_heap+=3;
		aux=A[1];
		A[1]=A[k];
		A[k]=aux;
		k=k-1;
		Reconstituire_Heap(A,1,k);

	}
}

int Pivot(int A[],int st,int dr)
{
	contor_quick++;
	int x=A[dr];
	int i=st-1;
	int j,aux;
	for(j=st;j<=dr-1;j++)
	{
		contor_quick++;
		if(A[j]<=x)
		{
			contor_quick+=4;
			i=i+1;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
		}
	}
	contor_quick+=3;
	aux=A[i+1];
	A[i+1]=A[dr];
	A[dr]=aux;
	return(i+1);
}

void quick_sort(int A[],int st,int dr)
{
	int q;
	if(st<dr)
	{
		q=Pivot(A,st,dr);
		quick_sort(A,st,q-1);
		quick_sort(A,q+1,dr);
	}
}

void test()
{
	srand(time(NULL));
	printf("Inceputul procedurii de testare a algoritmului: \n");
	n=10;
	int i;
	for (i=1;i<=n;i++)
	{
		A[i]=rand() % 100;
		B[i]=A[i];
	}
	printf("Sirul pe care se testeaza algoritmii: \n");
	for (i=1;i<=n;i++)
	{
		printf("%d ",A[i]);
	}
	printf(" \n");
	heap_sort(A,n);
	quick_sort(B,1,n);
	printf ("Heap Sort: \n");
	for (i=1;i<=n;i++)
	{
		printf("%d ",A[i]);
	}
	printf("\n Quick Sort: \n");
	for (i=1;i<=n;i++)
	{
		printf("%d ",B[i]);
	}
	printf ("\n ");
}

void comparare_avarage()
{
	printf("------------------Start---------------------- \n");
	FILE *f;
	f=fopen("Rezultate.csv","w");
	fprintf(f,"n,Heap Sort,Quick Sort \n");

	for (n=100;n<=10000;n=n+100)
	{
		contor_heap_total=0;
		contor_quick_total=0;
		int k;
		for (k=1;k<=5;k++)
		{
			contor_heap=0;
			contor_quick=0;

			int i;
			srand(time(NULL));
			for (i=1;i<=n;i++)
			{
				A[i]=rand() % 1000;
				B[i]=A[i];
			}
			printf("Status: Heap-Sort\tn=%d\tk=%d\n",n,k);
			heap_sort(A,n);
			printf("Status: Quick-Sort\tn=%d\tk=%d\n",n,k);
			quick_sort(B,1,n);

			contor_heap_total+=contor_heap;
			contor_quick_total+=contor_quick;
		}
		fprintf(f,"%d,%ld,%ld\n",n,contor_heap_total/5,contor_quick_total/5);
	}
	fclose(f);
	printf("-----------------Sfarsit---------------------------------------------- \n");

}

int main()
{
	test();
//	comparare_avarage();
	return 0;
}


