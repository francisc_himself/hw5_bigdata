/*************
***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Algoritmul de interclasare a k liste ordonate de n elemente in total are complexitatea de O(n*lgk)
Pentru un caz average in care si numarul listelor si numarul de elemente este variabil la fiecare incercare se observa ca graficele pentru cele
cazuri in care numarul listelor este variabil, au o crestere de n*lgk
Pentru cazul in care numarul maxim de elemente din lista este constant si numarul listelor variaza se poate vedea cum graficul creste dupa n*lgk in functie
de numarul de liste.

************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <windows.h>
#define MAX 10000
#define FIX 5
int heapSize=0;
int n=100;
int comp,at;
typedef struct nod{ int key;
			struct  nod *urm;
			}Nod;
Nod *listOfLists[1000];
Nod* resultList;
typedef struct heapElement{int key;
						   int listIndex;
						  }Heap;
Heap heap[1000];

void inserareValori(int *a,int n)
{
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%MAX;
	}
}


void inserareValoriCrescator(int *a,int n)
{	
	
	
	a[0]=rand()%MAX;

	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]+rand()%MAX;
	}
	//Sleep(500);
}
void inserareValoriDescrescator(int *a,int n)
{
	
	srand(time(NULL));
	a[0]=rand()%MAX;
	for(int i=1;i<n;i++)
	{
		a[i]=a[i-1]-rand()%MAX;
	}
}

void afisareValori(int *a,int n)
{

	for(int i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
}

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return (2*i);
}

int right(int i)
{
	
	return (2*i+1) ;
}


void maxHeapify(int i)
{

	int smallest;
	int	l=left(i);
	int	r=right(i);
	comp=comp+2;
	
	if ((l<=heapSize) && (heap[l].key<heap[i].key))
	{
		smallest=l;
	}
	else 
	{
		smallest=i;
	}
	if ((r<=heapSize) && (heap[r].key<heap[smallest].key))
	{
		smallest=r;
	}
	if(smallest!=i)
	{
		Heap x=heap[i];
		heap[i]=heap[smallest];
		heap[smallest]=x;
		at=at+3;
		maxHeapify(smallest);
	}
}

Heap H_Pop()
{
	Heap x;
	x=heap[1];
	heap[1]=heap[heapSize];
	heapSize=heapSize-1;
	maxHeapify(1);
	return x;
}

void inserareLista(Nod **first,Nod *p)
{
	p->urm=NULL;
	if(*first==NULL)
	{
		*first=p;
	}
	else
	{
		(*first)->urm=p;
		*first=p;
	}
}
void H_Push(int value,int listIndex)
{
	heapSize=heapSize+1;
	heap[heapSize].key=value;
	heap[heapSize].listIndex=listIndex;
	int i=heapSize;
	at=at+1;
	comp=comp+1;
	while((i>1) &&(heap[parent(i)].key>heap[i].key))
	{
		Heap y=heap[i];
		heap[i]=heap[parent(i)];
		heap[parent(i)]=y;
		at=at+3;
		comp=comp+1;
		i=parent(i);
	}
}

void afisareLista(Nod *p)
{
	printf("Se afiseaza lista ");
	while(p!=NULL)
	{
		printf("%d ",p->key);
		p=p->urm;
	}
	printf("\n\n");
}
void buildKListe(int nTotal,int nrOfLists)
{
	int n=nTotal/nrOfLists;
	int* sortedArray=(int*) malloc(sizeof(int)*n);
	for(int i=0;i<nrOfLists;i++)
	{
		inserareValoriCrescator(sortedArray,n);
		//afisareValori(sortedArray,n);
		nod *first=NULL;
		nod *prim=NULL;
		for(int j=0;j<n;j++){
			nod* x=(nod*) malloc(sizeof(Nod));
			x->key=sortedArray[j];
			inserareLista(&first,x);
			if(j==0)
			{
				prim=first;
			}
			first=x;
			
		}
		listOfLists[i]=prim;
		afisareLista(listOfLists[i]);
	}
}

//void interclasare(int nrOfLists, int totalNrOfElements){
//	int value;
//	Heap aux;
//	Nod *prim=NULL;
//	int n=totalNrOfElements/nrOfLists;
//	for(int j=0;j<n;j++)
//	{
//			for(int i=0;i<nrOfLists;i++)
//			{
//			Nod *x=(Nod*) malloc(sizeof(Nod));
//				x->key=listOfLists[i]->key;
//				inserareLista(&resultList,x);
//				if((j==0)&&(i==0))
//				{
//					prim=resultList;
//				}
//				listOfLists[i]=listOfLists[i]->urm;
//			}
//	}
//	resultList=prim;
//	
//}

void interclasareListe(int k)
{
	int i=0;
	Nod *prim=NULL;
	int value;
	Heap aux;
	for(int i=0;i<k;i++)      
	{
		at=at+1;
		value=listOfLists[i]->key;
		H_Push(value,i); 	
		listOfLists[i]=listOfLists[i]->urm;
	}
	comp=comp+1;
	while (heapSize>0){
		aux=H_Pop();
		nod *x=(nod*) malloc(sizeof(nod));
		x->key=aux.key;
		comp=comp+3;
		at=at+2;
		inserareLista(&resultList,x);
		if(i==0)
		{
			at=at+1;
			prim=resultList;
		}
		i++;
		if ( (listOfLists[aux.listIndex])!=0 )
			{
				value=listOfLists[aux.listIndex]->key;
				H_Push(value,aux.listIndex);
				listOfLists[aux.listIndex]=listOfLists[aux.listIndex]->urm;
				at=at+1;
			}
 	}
	resultList=prim;
	at=at+1;
	
}
void prettyPrint(int *a,int n,int i,int nivel)
{
	if(i<n){

	for(int j=0;j<nivel;j++)
		printf("\t");
	printf("%d\n",a[i]);
	prettyPrint(a,n,left(i),nivel+1);
	prettyPrint(a,n,right(i),nivel+1);
	}
}



void main()
{
	srand((unsigned int)time(NULL));
	int i;
	int k=4,n=20;
	buildKListe(n,k);
	for(i=0;i<k;i++);
	{
		afisareLista(listOfLists[i]);
	}
	interclasareListe(k);
	afisareLista(resultList);
	int kValue[]={5,10,100};
	/*FILE *f=fopen("AverageTema4.csv","w");
	for(int i=0;i<3;i++)
	{
		for (int n=100;n<=10000;n=n+100)
		{
			printf("%d ",n);
			for(int j=0;j<5;j++)
			{
				buildKListe(n,kValue[i]);
				interclasareListe(kValue[i]);
			}
			fprintf(f,"%d,%d\n",n,(comp+at)/5);
			at=0;
			comp=0;
		}
	}
	fclose(f);
	FILE *g=fopen("Tema4Part2.csv","w");
	n=10000;
	for(int i=10;i<=500;i=i+10)
	{
		printf("%d ",i);
		for(int j=0;j<5;j++)
		{
			buildKListe(n,i);
			interclasareListe(i);
		}
		fprintf(g,"%d,%d\n",i,(comp+at)/5);
		at=0;
		comp=0;
	}
	fclose(g);*/
}