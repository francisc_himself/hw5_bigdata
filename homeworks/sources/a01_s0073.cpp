#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <time.h>

int compb =0;
int atrb =0;
int comps =0;
int atrs =0;
int compi =0;
int atri =0;
int compmed =0;
int atrmed =0;
void bubble_sort(int a[], int n)
{
	int aux;
	int i = 0;
	bool b = false;
	
	while(!b)
	{
	
		b= true;
		for(i =0; i<n-1; i++)
		{
			compb++;
			if(a[i]>a[i+1])
			{
				
				b= false;
				aux = a[i+1]; atrb++;
				a[i+1] = a[i]; atrb++;
				a[i] = aux; atrb++;
			}
		}
	}

}

void insert_sort(int a[], int n)
{
	int aux;
	int i,j,k;
	bool b = false;

	for(i=0; i<n-1;i++)
	{
		compi++;
		if(a[i]>a[i+1])
		{
			b= false;
			aux = a[i+1]; atri++;
			j=i; 
			while(!b)
			{
				b= true;
				compi++;
				if(aux<a[j])
				{
					a[j+1] = a[j]; atri++;
					b=false;
					j--;
				}
			
			}
			a[j+1]=aux; atri++;

		}
	}
	/*for(i=0; i<n;i++)
		printf("%d ",a[i]);*/

}

void selection_sort(int a[], int n)
{
	int aux;
	int i,j =0;
	int min;
	min = a[0];
	j=1;
	
	while(j<n){
		min=a[j];
		for(i =j; i<n; i++){
			comps++;
			if(a[i]<=min){
				min = a[i]; atrs++;
				aux=i;
			}
		}
		a[aux] = a[j-1]; atrs++;
		a[j-1] = min; atrs++;
		j++;
	}
}

void random_gen(int **a, int **b, int**c, int n)
{
	int i;
	srand((unsigned)time(NULL));
	
	*a=(int*)calloc(n,sizeof(int));
	*b=(int*)calloc(n,sizeof(int));
	*c=(int*)calloc(n,sizeof(int));
	for(i=0; i<n; i++)
	{
		(*c)[i] = (*b)[i] = (*a)[i] = rand();
	}
}

void sortari()
{
	int i,j;
	int *a,*b,*c;
	FILE *file1;
	FILE *file2;
	FILE *file3;
	file1 = fopen("bubble.csv","w");
	file2 = fopen("insert.csv","w"); 
	file3 = fopen("selection.csv","w");
	for(i=100; i<=10000; i+=100)
	{
		
		compb =0;	
		atrb = 0;
		compi =0;	
		atri = 0;
		comps =0;
		atrs=0;
		for(j=1; j<=5; j++)
		{	
			random_gen(&a,&b,&c,i);
			bubble_sort(a,i);
			insert_sort(b,i);
			selection_sort(c,i);

		}
		
		compmed = compb /5;
		atrmed = atrb /5;
		fprintf(file1,"%d %d %d \n",i,compmed,atrmed);	

		compmed = compi /5;
		atrmed = atri /5;
		fprintf(file2,"%d %d %d \n",i,compmed,atrmed);	
		
		
		compmed = comps /5;
		atrmed = atrs /5;
		fprintf(file3,"%d %d %d \n",i,compmed,atrmed);	
		
	/*	comp=0;
		atr=0;
		for(j=1; j<=5; j++)
		{
			insert_sort(b,i);
		}
		compmed = comp /5;
		atrmed = atr /5;
		fprintf(file2,"%d %d %d \n",i,compmed,atrmed);*/
	}
	fclose(file1);
	fclose(file2);
}

void main()
{
	int a[10];
	a[0] = 1;
	a[1] = 2 ;
	a[2] = 5;
	a[3] = 8 ;
	a[4] =12;
	a[5] =91;
	a[6] =23;
	a[7] =0;
	a[8] =6;
	a[9] =7;
	sortari();
	//selection_sort(a, 10);

;
}