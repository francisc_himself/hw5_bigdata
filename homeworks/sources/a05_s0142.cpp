#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "Profiler.h"
#define HASH_SIZE 10007
#define NMAX 50000
#define SEARCH 3000
int t[HASH_SIZE],sir[NMAX],ind[SEARCH/2];
int m;
int c1,c2,contor;

int h (int k,int i)
{

    int x;
	//functie de hash
    x=(k+c1*i+c2*i*i) % HASH_SIZE;
    return x;

}
int insert_hash(int k)
{
     int i,j;
     i=0;
     do{
        j = h(k,i);
		//verificam daca pozitia returnta este libera
        if(t[j]==NULL)
        {
            t[j]= k;
            return j;
        }
		//daca nu este libera continuam algoritmul
        else i++;
     }while(i<=HASH_SIZE);
     printf("depasire tabela de dispersie\n");
}

int search_hash(int k)
{
    int i,j;
	contor=0;
    i = 0;
    do{
        j=h(k,i);
		contor++;
		//verificam daca pe pozitia returnata este elementul cautat
        if(t[j] == k)
            return j;
		//in caz contrar continuam cautarea
            i++;
			
        
    }while(t[j]!=NULL && i<=HASH_SIZE);
    return -1;
}
//functie care realizeaza initializarea unui sir
void init (int n, int a[])
{
	int i;
	for(i=1;i<=n;i++)
		a[i]=0;
}

int main()
{
    int i,j,a[NMAX],n,k,suma_reusite=0,suma_nereusite=0,max_reusit=0,max_nereusit=0;
	double alfa[] = {0.8, 0.85, 0.9, 0.95, 0.99};
	FILE *f;
    c1 = 1;
    c2 = 1;
	//generarea unui exemplu care sa verifica functionarea algoritmilor
	FillRandomArray(sir,10,10,100,false,0);
	printf("sirul initial: ");
	for(i=0;i<10;i++)
		printf("%d ", sir[i]);
	printf("\n");
	printf("indicii tabelei de disperie: ");
	for(i=0;i<10;i++)
	{
		int k = insert_hash(sir[i]);
		printf("%d ",k);
	}
	printf("\npozitia pe care a fost inserat al sase-lea element: ");
	printf("%d",search_hash(sir[5]));
	//deschiderea fisierului
	f = fopen("analiza.csv", "w");
	if(f==NULL){
		perror("Eroare deschidere fisier!");
		exit(1);
	}
	//scrierea capului de tabel
	fprintf(f, "Filling factor, Avg. Effort found, Max Effort found,Avg. Effort not found, Max Effort not found\n");
	for(i=0; i<5; i++)
	{
		n = alfa[i]*HASH_SIZE;
		for(j=1; j<=5;j++)
		{	
			//cream un sir pe care il introducem in tabela de dispersie
			FillRandomArray(sir,SEARCH/2+n,11,50000,true,0);
			for(k=0;k<n;k++)
			{
				insert_hash(sir[k]);
			}
			//cautam 1500 de elemnte care sunt gasit 
			FillRandomArray(ind,SEARCH/2,10,n-1,true,0);
			for(m=0;m<SEARCH/2;m++)
			{
				
				search_hash(sir[ind[m]]);
				suma_reusite = suma_reusite +contor;
				if (contor > max_reusit)
						max_reusit = contor;
					
			}
			//cautam 1500 elemente care nu sunt gasite
			for(m=n; m<=n+SEARCH/2; m++)
			{
				search_hash(sir[m]);
				suma_nereusite = suma_nereusite + contor;
				if(contor > max_nereusit)
					max_nereusit = contor;
			}
			//reinitializarea variabilelor folosite
			init(HASH_SIZE,t);
			init(SEARCH/2,ind);
			init(NMAX,sir);
	}
		//afisare rezultatelor
		fprintf(f,"%lf,%d,%d,%d,%d\n",alfa[i],suma_reusite/7500,max_reusit,suma_nereusite/7500,max_nereusit);
		suma_reusite=0;
		max_reusit=0;
		suma_nereusite=0;
		max_nereusit=0;


}
				
		printf("succes");


	getch();



}
