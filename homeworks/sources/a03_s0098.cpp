#include<iostream>
#include<conio.h>
#include<fstream>

using namespace std;

long heapSize;
long a[11]={0,5,3,4,6,1,10,2,11,0,13};
long aH,cH,aQ,cQ;
long outH[10000], outQ[10000];

/*
After comparing the results, it can be observed that quickSort is more efficient than heap sort in average case

Best case for quick sort:
In order to obtain best case for quick sort the pivot must be chosen in such a way that 2 partitions will be obtained, one of length n/2 and one of length n/2-1, so the 
pivot will be chosen in the middle.
Also, an increasingly ordered array will be chosen
The running time will be O(n*logn)

Worst case:
In order to obtain worst case for quick sort, the pivot must be chosen in such a way that 2 partitions, one of length 1 and one of length n-1 will
be obtained, so the pivot is the first position
Also, an increasingly ordered array will be chosen
The running time is O(n^2). So, the worst-case running time of quicksort is no better than that of insertion sort. 
The partitioning costs O(n) time.

*/
void heapify(long A[], int i, int n)  //i=index of the root, elem. to be added
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

   cH++;
    if (left<=n && A[left]>A[i]) 
        largest=left;	//root, left or right child index
    else
        largest=i;

    cH++;
    if (right<=n && A[right]>A[largest])
        largest=right;
    if (largest!=i) //one of the children larger than the root
        {
			aH=aH+3;
			//swap root with largest child
            aux=A[i];
            A[i]=A[largest];
            A[largest]=aux;
            heapify(A,largest,n);  //continue the process on the heap
        }
}

void buildMaxHeap(long A[],int n)
{
    int i;
    for (i=n/2;i>=1;i--) //from the non-leave nodes until we reach the root
        heapify(A,i,n); //build the heap out of 2 already built heaps and 1 node
}

void heapSort(long a[], long n)
{
	int aux;
	buildMaxHeap(a,n);
	
	for(int i=n; i>=2; i--)
	{
		aH=aH+3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		n--;
		heapify(a,1,n);
	}
}

int partition(long a[], long p, long r, int pivot)
{
	long x,i,aux;
	if(pivot==0) // pivot is chosen for average case
	x=a[r];
	else if(pivot==1) // pivot is chosen for best case
	{
	aux=a[(p+r)/2];
	a[(p+r)/2]=a[r];
	a[r]=aux;
	x=a[r];
	}
	else if(pivot==2)
		{aux=a[p+1];
	a[p+1]=a[r];
	a[r]=aux;
	x=a[r];
	}//pivot is chosen for worst case
	aQ++;
	i=p-1;
	for(int j=p; j<r; j++)
	{
		cQ++;
		if(a[j]<=x)
		{
			aQ=aQ+3;
			i++;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	aQ=aQ+3;
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;
	return i+1;
}

void quickSort(long a[], long p, long r, int pivot)
{
	long q;
	if(p<r)
	{
		q=partition(a,p,r,pivot);
		quickSort(a,p,q-1,pivot);
		quickSort(a,q+1,r,pivot);
	}
}

void main()
{
	ofstream averageCase("averageCase.txt");
	ofstream worst("worstCase.txt");
	ofstream best("best.txt");
	int i,j,k;

  heapSort(a,10);
	for(int i=1;i<11; i++)
		printf("%d ",a[i]);
		
	printf("\n");
	quickSort(a,1,10,2);
	for(int i=1;i<11; i++)
		printf("%d ",a[i]);

	 for (i=1;i<=10000;i=i+100)
	{	
		aQ=cQ=aH=cH=0;
		long sumcQ=0,sumaQ=0,sumaH=0,sumcH=0;

		for (j=1;j<=5;j++)
		{
			aQ=cQ=aH=cH=0;
			for ( k=1;k<=i;k++)
			{
				outQ[k]=outH[k]=rand();	
			}
			
			heapSort(outH,i);
			quickSort(outQ,1,i,0);
			
			sumcQ+=cQ;
			sumaQ+=aQ;
			sumcH+=cH;
			sumaH+=aH;
		}
			averageCase<<k<<"              "<<(sumaH/5+sumcH/5)<<"        "<<(sumaQ+sumcQ)/5<<"\n";
	}
	
	 
	 //best case for quickSort
	for( i=1; i<10000; i=i+100)
	{
      aQ=cQ=0;
		for( j=1; j<=i; j++)
		{
			outQ[j]=j;
		}
		
	  quickSort(outQ,1,j,1);
    	best<<j<<"     "<<aQ+cQ<<"\n";	
	}
	
	//worst case for quickSort
	for( i=1; i<10000; i=i+100)
	{
      aQ=cQ=0;
		for( j=1; j<=i; j++)
		{
			outQ[j]=j;
		}
		
	 quickSort(outQ,1,j,2);
    	worst<<j<<"     "<<aQ+cQ<<"\n";	
	}
	
	getch();

}