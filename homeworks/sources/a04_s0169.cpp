/***************************************ANONIM*** ***ANONIM*** ,gr.***GROUP_NUMBER*********************************************************
.*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>

using namespace std;

typedef struct nod{
	int valoare;
	struct nod *next;
}nod;
nod *head[500];
nod *tail[500];
nod *joinedHead,*joinedTail;
nod *x=new nod,*poped=new nod;
typedef struct heap_elt{
	int idxList;
	int valoare;
	//nod * element;
}heap_elt;
heap_elt *heap[500];
heap_elt *h=new heap_elt;
int lng, size_h,a,c;
void afisareHeap(int *v,int n,int k,int nivel);
void afisare(nod *h);
void inserare(nod **h,nod**t,int val);

int left(int i)	 //returneaza indicele fiului din stanga
{
	return 2*i;
}
int right(int i)  //returneaza indicele fiului din dreapta
{
	return 2*i+1; 
}
void inserare(nod **h,nod**t,int val){			//la capatul listei

	nod*c=new nod;
	c->valoare=val;
	c->next=NULL;//...si dupa ultimul nu e nimic, deci nici o adresa

	if(*h==NULL)
	{	
		*h=new nod;
		*h=c;
		*t=*h; //evident, avand un singur element acesta va fi si primul                                                                                                                                                                 //            si ultimul
	}
	else
	{		

		(*t)->next=c; //se "agata" noul nod c, dupa ultimul din lista
		*t=c; // noul nod e ultimul...
		(*t)->next=NULL;

	}
}
void afisare(nod *h)  //afisare lista
{
	nod *c;
	c=h;
	while(c!=NULL)//cat timp mai sunt in lista
	{
		cout<<c->valoare<<"  ";
		c=c->next;//avansez in lista trecand la urmatoarea adresa
	}
}
int stergere(int el)  //sterge primul element din lista
{int i;
	for(i=0;i<=500;i++)
		if(head[i]->valoare==el)

			if((head[i])->next==NULL)
				free(h);
			else{
				nod *a; //a se sterge, c este precedentul sau.Se va genera o noua //legatura intre c si a->next
				a=head[i];          //se retine in a
				head[i]=head[i]->next; //primul va deveni urmatorul element
				free(a);         //se elibereaza memoria
			}
		else break;
		return i;
}
void swap(heap_elt &a, heap_elt &b){
	heap_elt aux=a;
	a=b;
	b=aux;
}
void minHeapify(heap_elt *heap, int i) {
	int l,r,largest;
	r = right(i);
	l = left(i);
	//compare the root with the left child
	c++;
	if (l <= size_h && (heap[i].valoare > heap[l].valoare)) {
		largest = l;
	} else {
		largest = i;
	}
	//compare the root with the right child
	c++;
	if (r <= size_h && (heap[r].valoare < heap[largest].valoare)) {
		largest = r;
	}
	//heapify if needed
	c++;
	if (largest != i) {		
		swap (heap[largest],heap[i]);
		a+=3;
		minHeapify(heap, largest);
	}
}

heap_elt* h_pop(heap_elt **heap)
{	a=a+3;
heap_elt* x=heap[1];
heap[1]=heap[size_h];
heap[size_h]=x;  
size_h--;
minHeapify(heap[1],1);
return x; 
}
void h_push(heap_elt **heap,int val,int l)
{
	int i=++size_h;
	h->idxList=l;
	h->valoare=val;
	heap[size_h]=h;
	c++;
	while ((i>1)&&(heap[i]->valoare<heap[i/2]->valoare) )
	{
		c++;a=a+3;
		heap_elt* aux=heap[i/2];
		heap[i/2]=heap[i];
		heap[i]=aux;
		i=i/2;
	}
}

void interclasareListe(nod** head,int k,heap_elt *heap[],nod**joinedHead)
{
	int c=0,a=0;
	size_h=0;
	for(int i=0;i<k;i++)     //O(k log k)
	{	
		if(&head[i]!=NULL)
		{
			h_push(heap,(head[i])->valoare,i+1);
		}
	}
	int j;
	while (size_h>0)    //O(n log k)
	{
		j=size_h;
		heap_elt* y=h_pop(heap); c++;
		int li=stergere(y->valoare);
		inserare(joinedHead, &joinedTail,y->valoare);
		cout<<endl<<"S-a inserat "<<(*joinedTail).valoare<<" din lista "<<li;
		if (&head[li]!=NULL)
			h_push(heap,(head[li])->valoare,j+1);
	}
}
void generateL(nod **h,nod **t, int k, int n)
{
	int key;
	int i,j;
	for(i=0; i < k; i++)
	{
		key=0;
		for(j=0; j < n/k; j++)
		{		
			key=key+rand() % k;
			inserare(&h[i],&t[i],key);
		}
	}
}
void test()
{	
	int k=4,n=20;
	generateL(&head[0],&tail[0],k,n);
	for(int i=0;i<k;i++)
	{
		afisare(head[i]);
		cout<<endl;
	}

	interclasareListe(head,k,heap,&joinedHead);
	afisare(joinedHead);
}
int main()
{
	//test();
	//statistic();
	 FILE *f=fopen("heapchart1.csv","w");

    for (int n = 100; n < 5000; n += 100) {

        printf("\n n currently %d " , n);
        //test the merging for 5 lists
       int k = 5;
       c=0,a=0;
        generateL(&head[0],&tail[0],k,n);
		interclasareListe(head,k,heap,&joinedHead);
        fprintf(f," %d, %d, %d, %d,", n, a, a, a+c);

        //test the merging for 10 lists
        k = 10;
        //make sure all the lists are empty
        for(int i=1;i<=k;i++){
            head[i]=NULL;
            tail[i]=NULL;
        }
		  c=0,a=0;
        generateL(&head[0],&tail[0],k,n);
        interclasareListe(head,k,heap,&joinedHead);
        fprintf(f," %d, %d, %d,",a,c,a+c);
        //test the merging for 100 lists
        k = 100;
		//make sure all the lists are empty
        for(int i=1;i<=k;i++){
            head[i]=NULL;
            tail[i]=NULL;
        }
		  c=0,a=0;
        generateL(&head[0],&tail[0],k,n);
        interclasareListe(head,k,heap,&joinedHead);
        fprintf(f," %d, %d, %d,",a,c,a+c);

    }

    fclose(f);
    f=fopen("heapchart2.csv","w");
    int n=5000;
    for (int k = 100; k < 500; k+=10) {
		//make sure all the lists are empty
        for(int i=1;i<=k;i++){
            head[i]=NULL;
            tail[i]=NULL;
        }
		  c=0,a=0;
        generateL(&head[0],&tail[0],k,n);
        interclasareListe(head,k,heap,&joinedHead);
        fprintf(f," %d, %d, %d,",a,c,a+c);
    }

    fclose(f);
	_getch();
	return 0;
}