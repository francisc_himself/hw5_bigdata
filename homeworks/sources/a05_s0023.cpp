//***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
//Cerinta temei este cea de a implementa corect si eficient operatiile de inserare si cautare intr-un hash table.
	


#include<math.h>
#include <time.h> 
#include<conio.h>
#include<fstream>
#include<iostream>
#define N 9973
using namespace std;



int T[N];
int gasite,negasite;

int hashT(int k, int i)
{
	int a,c1=3,c2=5;
	a=(k%N +c1*i + c2*i*i)%N;
	return a;
}

int Hash_Insert(int *T, int k)  //functia de inserare
{
   int i=0,j;
   do{
       j=hashT(k,i);
       if(T[j]==NULL)
        {
           T[j]=k;
		   return j;
        }
       else i++;

   }while(i!=N);

return -1;
}

int Hash_Search(int *T,int k)  //functia de cautare in tabela
{
  int i=0,j,nr=0;

   do{
	  nr++;
      j=hashT(k,i);
      nr++;
       if(T[j]==k)
        {
			nr++;
			 gasite=nr;
			
			 return j;
        }
        else i++;
		nr++;
   }while((T[j]!=NULL)&&(i!=N));
 
  
   negasite=nr;// in cazul in care nu se gaseste numarul
return -1;
}


void test()
{
	double alfa[5]={0.8,0.85,0.9,0.95,0.99};// factorul de umplere al tabelei
	
	
	
	ofstream f("excel.csv");
	int i,j,k,l;
	double nr_elem;
	int med_gasite=0,med_negasite=0;  //variabile folosite pentru contorizarea operatiilor
	int elem_existente[1500];  //vector care retine primele 1500 de elemente gasite
	int max2=0, max1=0;

	
	f<<"alfa"<<","<<"Media_gasite"<<","<<"Media_negasite"<<","<<"Maximul_gasite"<<","<<"Maximul_Negasite"<<endl;
		for(i=0;i<5;i++)
		{
			for(int i=0;i<N;i++)   //initializare
			{
				T[i]=NULL;
			}

			nr_elem =alfa[i]*N;
			
			
			for(j=0;j<nr_elem;j++)
			{
				k=rand()%10000+1;  //generam numere mai mici decat 10000
			
				if(j<1500)
				{
					elem_existente[j]=k;
				}
				
				Hash_Insert(T,k);
			}

			//pentru elemente neexistente ; 
			for(l=0;l<1500;l++)
			{
				negasite=0;
				k=rand();
				if(k>10000)
				{
					Hash_Search(T,k);
					med_negasite+=negasite;
				}
				else
				{
					Hash_Search(T,k+10000);
					med_negasite+=negasite;
				}

				if(negasite>max2)
					max2=negasite;
			}

			//retin elementele existente intr-un vector
			for(l=0;l<1500;l++)
			{
				gasite=0;
				Hash_Search(T,elem_existente[l]);

			if(gasite>max1)
				max1=gasite;
			med_gasite+=gasite;

			}
				f<<alfa[i]<<","<<med_gasite/1500<<","<<med_negasite/1500<<","<<max1<<","<<max2<<endl;

		}
		f.close();
		cout<<"done.";
		
}

int main()
{	cout<<"################################"<<endl;
	cout<<"##          hashtable         ##"<<endl;
	cout<<"################################"<<endl;
	
		test();
		_getch();
}