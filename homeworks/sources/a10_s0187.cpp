#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct
{
	int info;         //informatia din nod 
	char culoare;     //culoarea varfului la parcurgere 
	int vecini[100];  //lista vecinilor pentru fiecare varf
	int numar_vecini; //numarul de vecinini: nr de elemente din lista de adiacenta
	char eticheta[100];		
}varf;

varf v[100];	        //graful pe care se va efectua functia de parcurgere in adancime DFS
int p[100];		        //sirul de parinti
int d[100],f[100];		//d[i]<f[i]; vectori folositi pentru memorarea timpilor de vizitare a fiecarul nod
int timp,atribuiri,comparatii;
FILE *file,*file2;
	
	
void init_param()
{
	atribuiri=0;
	comparatii=0;
}

//initializarea fiecarul nod dintr-un graf cu n noduri, n trimis ca parametru
void initializare(int n,int dimensiune1)
{
	int i,j,dimensiune2,nod,ok;
	for(i=0;i<n;i++)//pentru fiecare nod din graf se initializeaza culoarea,vectorii timpilor
	{//se seteaza culoarea alb, ca nevizitat, si se alege aleator un numar de vecini
		d[i]=0;
		f[i]=0;
		p[i]=-1;
		v[i].culoare='w';
		v[i].info=i;
		v[i].numar_vecini=0;
		atribuiri+=2;
		//dimensiune1=rand()%(n/2);//se alege un numar de muchii aleator intre 0 si jumatate din numarul de varfuri
		//pentru primul caz: nrNoduri=100, 1000<nrMuchii<5000,inc=100
		//for(dimensiune1=1000; dimensiune1<5000; dimensiune1+=100)
		//{
			//printf("%d ",v[i].info);
			dimensiune2=0;
			while(dimensiune1>0)//cat timp numarul de vecini ai nodului este mai mare decat 0, selecteaza un nou nod
			{
				
				ok=1;
				nod=rand()%n;//cat timp nodul ales aleator este egal cu nodul pentru care se cauta vecini, se alege un nou nod
				while(nod==v[i].info)
				{
					nod=rand()%n;
				}
				for(j=0;j<dimensiune2;j++)
				{
					if (v[i].vecini[j]==nod) 
					ok=0;//se verifica daca legatura exista deja
				}
				if (ok==1)
				{
					dimensiune1--;//daca nu exista legatura, scade dimensiune1
					v[i].vecini[dimensiune2]=nod;//se actualizeaza vecinii
					dimensiune2++;//creste dimensiune2
					v[i].numar_vecini++;//incrementez numarul de vecini
				}
				
			}
		//}
			
	}
}

void dfs_vizitat(varf *u);
void dfs(int n)
{
	//pentru cazul1
	//for(int dimensiune1=1000; dimensiune1<5000; dimensiune1+=100)
	//{
		//int dimensiune1=9000;
		int dimensiune1=rand()%(n/2);//se alege un numar de muchii aleator intre 0 si jumatate din numarul de varfuri
		initializare(n,dimensiune1);//initializarea grafului
		timp=0;
		for(int i=0;i<n;i++)
		{
			comparatii++;
			if (v[i].culoare=='w')	//daca este nevizitat - alb
			{
			  printf("Culoarea nodului %d este alba.Se parcurge nodul.\n",v[i].info);
			  dfs_vizitat(&v[i]);	//parcurg varful
			}
			//pt cazul2
				//if (v[n].culoare=='w')	//daca este nevizitat - alb
			    //dfs_vizitat(&v[n]);//parcurg varful

			//afisare cazul2: nrNoduri=(110,200) cu pas de 10 si nrMuchii=9000
			//fprintf(file2,"%d,%d \n",n,atribuiri+comparatii);
		
		}
		//afisare pentru primul caz:nrMuchii=(1000,5000) cu pas de 100 si nrNoduri=100
		//fprintf(file,"%d , %d",dimensiune1,atribuiri+comparatii);
		//fprintf(file,"\n");
		
	//}
}

void dfs_vizitat(varf *u)
{
	int j;
	u->culoare='g';	//am vizitat - devine gri
	printf("Culoarea nodului %d devine gri.Nodul a fost vizitat\n",u->info);
	atribuiri++;
	d[u->info]=timp;//prima vizita - memorata in d
	timp++;
	for(j=0;j<u->numar_vecini;j++)  
	{
		comparatii++;
		if (v[u->vecini[j]].culoare == 'w') 
		{
			p[v[u->vecini[j]].info]=u->info;//actualizez parintele
			dfs_vizitat(&v[u->vecini[j]]);//parcurg vecinul nodului transmis ca parametru
		}

	}
	u->culoare='b';//am parcurs nodul - negru
	atribuiri++;
	f[u->info]=timp;//vizita finala - memorata in f
	timp++;
}



void etichetare(int n)
{
	int i,j,u1,u2;
	for(i=0;i<n;i++)
	{
		u1=v[i].info;//retin infoormatia din varfului actual
		atribuiri++;
		for(j=0;j<v[i].numar_vecini;j++)//parcurg vecinii varfului actual
		{			
			v[i].eticheta[j]=NULL;//pentru fiecare vecin am eticheta nula initial
			u2=v[i].vecini[j];//retin informatia vecinului
			atribuiri+=2;
			comparatii++;
			if ((d[u1]<d[u2])&&(d[u2]<f[u2])&&(f[u2]<f[u1])) //coeficentii lui u2 intre coef lui u1
			{
				if((d[u2]-d[u1]==1)||(f[u1]-f[u2]==1))
					v[i].eticheta[j]='t';	//tree
				else
					v[i].eticheta[j]='f';	//forward
				atribuiri++;
			}
			if ((d[u2]<d[u1])&&(d[u1]<f[u1])&&(f[u1]<f[u2])) //coeficentii lui u2 in afara coef lui u1
			{
				v[i].eticheta[j]='b';	//back
				atribuiri++;
			}
			if ((d[u2]<f[u2])&&(f[u2]<d[u1])&&(d[u1]<f[u1])) //nod in afara arborelui
			{
				v[i].eticheta[j]='c';	//cross
				atribuiri++;
			}
		}
	}
}

int main()
{
	
	int i;
	int nrNoduri=50;
	//for(i=0; i<nrNoduri; i++)
		dfs(nrNoduri);
	//file=fopen("graf.csv","w");
	//fprintf(file,"nrMuchii,atribuiri+comparatii");
	//fprintf(file,"\n");
	//file2=fopen("graf2.csv","w");
	//fprintf(file2,"nrNoduri,atribuiri+comparatii");
	//fprintf(file2,"\n");
////////////////////////////cazul1///////////////////////	
	//init_param();
	//dfs(100);
////////////////////////////cazul2///////////////////////
	/*init_param();
	for (int n=110;n<=200;n=n+10)
	{
		dfs(n);
	}*/ 
	//fclose(file);
	//fclose(file2);
	return 0;
}