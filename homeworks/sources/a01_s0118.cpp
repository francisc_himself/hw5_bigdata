/*
* A 1: SORTING 
* Author : ***ANONIM*** ***ANONIM***
* Date:    05.03.2013
* Finished: 09.03.2013
*
* Comparison of three direct sorting methods: Insertion Sort, Selction Sort, Bubble Sort
* We can observe the nr of (assignments+comparisons) in order to evaluate algorithms, e.g.
* for best case (array already sorted) bubblesort is best, in worst case, selection sort 
* still tends to be better than bubblesort or insertion sort, in average insertion sort and
* selection sort are comparable, but better than bubble sort.
*
*	Insertion Sort: efficient for sorting small array, it is an "in-place" algorithm, because
* it sorts the array with only one element stored outside the array.
*	Best case: an array which is already sorted;
*	Worst case: the array is sorted in reverse order;
*	Average case: roughly as bad as the worst case, depending on the input size of the array.
*	Order of growth is (n^2)
*	Insertion sort is a stable algorithm(preserves the relative order of elements).
*
*	Selection Sort: also "in-place", the running time is n^2 for all cases, performs worse than 
* insertion sort, faster for small arrays
*	Is not a stable algorithm.
*	Average case: O(n^2), in the average case, selection sort tends to perform better (nr of assignments
* + nr of comparisons is the lowest)
*	Best case: array already sorted
*	Worst case: smallest element is on the last position, others are sorted ascending.
*
*	Bubble Sort: works by swapping repeatedly adjacent elements.
*	Insertion sort tends to have a better performance than bubble sort.
*	Best case: array is already sorted. In this case, it performs better than insertion or selection sort.
*	Worst case: array sorted in descending order. In worst case, bubble sort performs worst.
*	Average: also in the average case, insertion and selection sort are better.
*	Not a stable algorithm.
*/


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

#define NMAX 10000;

FILE *f;

int ass=0, comp=0;

void InsertionSort (int m, int arr[10000]) {
	int i,j;
	int buff; //store the element outside the array, untill you find its place
	int ok;
	// move elements until find their position
	for (j=1;j<m;j++) {
		buff=arr[j]; ass++;
		i=j-1;
		
		ok = 1;
		comp++;
		// insert elements into the sorted sequence, equivalent to while-loop
		for (i = j; i >= 1 && buff < arr[i-1]; i--)
		{
			arr[i] = arr[i-1]; ass++;
			comp++;
			ok = 0;
		}
		// for-loop termination indicates that the algorithm is correct
		if (ok == 0)
		{
			arr[i] = buff; ass++;
		}
	}
}

void SelectionSort(int m, int arr[10000]) {
	int i,j;
	int minIndex;
	int tmp;
	// selection sort needs to go only till m-1 elements
	// find the index of the smallest element in the array
	// and exchange it with the first
	for (i=0;i<m-1;i++) 
	{
		tmp=arr[i];
		ass++;
		minIndex=i;
		for(j=i+1;j<m;j++)
		{
			comp++;
			if (arr[j]<tmp) 
			{
				tmp=arr[j];
				ass++;
				minIndex=j;
			}
		} //outer for terminates when j>m
		arr[minIndex]=arr[i];
		arr[i]=tmp;
		ass=ass+2;
	}
}

void BubbleSort(int m,int arr[10000]) {
	int i,j,ok;
	int tmp;

	j=0;
	do {
		ok=1;
		j=j+1;
		for (i=0;i<m-j;i++)
		{
			comp++;
			if (arr[i]>arr[i+1]) {
				ok=0;
				tmp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=tmp;
				ass=ass+3;
			}
		}
	} while (ok==0);
}

void Print(int m, int arr[]) {
	for (int i=0; i<m; i++) printf("%d ",arr[i]);
	printf("\n");
}

void generate_array_avg (int a[10000], int n)
{
	for (int i = 0; i < n ; i++)
	{
		a[i] = rand();
	}
}

void generate_array_best (int a[10000], int n)
{
	for (int i = 0; i < n ; i++)
		a[i] = i;
}

void generate_array_worst(int a[10000], int n)
{
	for (int i = 0; i < n ; i++)
		a[i] = n-i;
}


void generate_array(int a[10000], int n)
{
	generate_array_worst(a,n);
}


int main() {
	int a[10000],n;

	srand((unsigned)time(0)); 
	f = fopen("date_sort_worst.txt", "w");
	for (n = 100; n <= 10000; n+= 100)
	{
		printf("n=%d\n", n);
		fprintf(f, "%d ", n);
		ass = comp = 0; generate_array(a, n); InsertionSort(n, a); fprintf(f, "%d %d ", ass, comp);
		ass = comp = 0; generate_array(a, n); SelectionSort(n, a); fprintf(f, "%d %d ", ass, comp); 
		ass = comp = 0; generate_array(a, n); BubbleSort(n, a);    fprintf(f, "%d %d ", ass, comp); 
		fprintf(f, "\n");
	}
	fclose(f);
	
	return 0;
}