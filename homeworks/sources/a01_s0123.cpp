/*
BEST CASE
Comparisons: best method: Insertion Sort
Assignments: best method: Bubble Sort and Insertion Sort
Comp + Ass:  Insertion Sort: : most efficient method

WORST CASE
Comparisons: best method: Insertion Sort
Assignments: best method: Selection Sort
Comp + Ass: Selection Sort: : most efficient method

AVERAGE CASE 
Comparisons  - best method: Insetion Sort
Assignments  - best method: Selection Sort
Comp + Ass   - Insertion Sort: most efficient method
*/

#include<iostream>
#include<conio.h>
#include<stdio.h>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;

void copy();
void bubbleSort();
void insertionSort();
void selectionSort();
void bestCase();
void worstCase();
void averageCase();

int a[10002], v[10002], n;

//create copy of the array
void copy(){
    for(int q=1;q<=n;q++)
        v[q] = a[q];
}

//bubble sorting
void bubbleSort(){
    int newn=0, auv, ass = 0, comp = 0;
    do{
        newn = 0;
        for(int i=1; i<n-1; i++){
			profiler.countOperation("comparisons",n);
			comp++;
            if(v[i]>v[i+1]){
                auv = v[i];
                v[i] = v[i+1];
                v[i+1] = auv;
				profiler.countOperation("assignments",n,3);
                ass +=3;
                newn = 1;
            }
        }
    }while(newn);
	if(ass == 0) profiler.countOperation("assignments",n,0);
    printf("\nAssignments: %d ",ass);
	printf("Comparisons: %d", comp);
}

//insertion sorting
void insertionSort(){
    int i, buff, k, ass = 0, comp = 0;
    for(i=2; i<=n; i++) {
        k = i-1;
        buff = v[i];
		profiler.countOperation("assignmentsIns",n);
        ass++;
        while(v[k] > buff && k>0) {    
			profiler.countOperation("comparisonsIns",n);
            comp ++;
            v[k+1] = v[k];
			profiler.countOperation("assignmentsIns",n);
            ass++;
            k--;
		}
       v[k+1] = buff;
	   profiler.countOperation("assignmentsIns",n);
       ass++;
	}  
	profiler.countOperation("comparisonsIns",n);
    comp++;
    printf("\nAssignments: %d ",ass);
	printf("Comparisons: %d", comp);
}

//selection sorting
void selectionSort(){
    int imin, min, comp = 0, ass = 0;
    for(int i=1; i<n; i++){
        min = v[i];
		profiler.countOperation("assignmentsSel",n);
        ass ++ ;
		imin = i;
        for(int j = i+1; j <= n; j++){
			profiler.countOperation("comparisonsSel",n);
            comp ++;
            if(v[j] > min);
            else {
                min = v[j];
				profiler.countOperation("assignmentsSel",n);
                ass++;
                imin = j;
            }
        }
        if(i != imin) {
            min = v[imin];
            v[imin] = v[i];
            v[i] = min;
			profiler.countOperation("assignmentsSel",n,3);
            ass+=3;
        }
    }
    printf("\nAssignments: %d ",ass);
	printf("Comparisons: %d", comp);
}

//the mest case method
void bestCase(){	
	n=100;
	while(n <= 10000)	{
		FillRandomArray(a,n,1,10000,false,1);

		//Bubble Sort:already sorted
		copy();
		bubbleSort();
	
		//Insertion Sort: already sorted
		copy();
		insertionSort();
	
		//Selection Sort: already sorted
		copy();
		selectionSort();

		n+=100;
	}
	profiler.createGroup("BestCaseComparisons","comparisons","comparisonsIns","comparisonsSel");
	profiler.createGroup("BestCaseComparisonsInsertion","comparisonsIns");
	profiler.createGroup("BestCaseAssignments","assignments","assignmentsIns","assignmentsSel");
	profiler.createGroup("Best Assign Bubble","assignments");
	profiler.addSeries("Bubble", "assignments", "comparisons");
	profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
	profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
	profiler.createGroup("BestCaseAll","Bubble","Insertion","Selection");
	profiler.createGroup("BestCaseAllInsertion","Insertion");
	profiler.showReport();
}

//worse case method
void worstCase(){
	n=100;
	while(n <= 10000){
		FillRandomArray(a,n,1,10000,false,2);

		//Bubble Sort: reversed
		copy();
		bubbleSort();

		//Insertion Sort: reversed
		copy();
		insertionSort();

		//Selection sort: appropriate for small n
		FillRandomArray(v,n/2,1,10000,false,2);
		FillRandomArray(v+(n+1)/2+1,n/2,1,10000,false,1);
		selectionSort();	
		n+=100;
	}

	profiler.createGroup("WorstCaseComparisons","comparisons","comparisonsIns","comparisonsSel");
	profiler.createGroup("Worstcase Comparison Insertion","comparisonsIns");
	profiler.createGroup("WorstCaseAssignments","assignments","assignmentsIns","assignmentsSel");
	profiler.addSeries("Bubble", "assignments", "comparisons");
	profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
	profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
	profiler.createGroup("WorstCaseAll","Bubble","Insertion","Selection");
	profiler.showReport();
}

//average case method
void averageCase(){
	for(int i=1; i<=5; i++){
		n=100;
		while(n<=10000){	
			FillRandomArray(a, n);

			copy();
			bubbleSort();
			copy();

			insertionSort();
			copy();

			selectionSort();
			n+=100;
		}
		profiler.createGroup("Comparisons Average","comparisons","comparisonsIns","comparisonsSel");
		profiler.createGroup("Assignments Averagw","assignments","assignmentsIns","assignmentsSel");
		profiler.createGroup("Selection Sort assignments", "assignmentsSel");
		profiler.addSeries("Bubble", "assignments", "comparisons");
		profiler.addSeries("Insertion", "assignmentsIns", "comparisonsIns");
		profiler.addSeries("Selection", "assignmentsSel", "comparisonsSel");
		profiler.createGroup("All","Bubble","Insertion","Selection");
		profiler.createGroup("All-Inserton","Insertion");
		profiler.showReport();
	}
}


int main(){
	//averageCase();
	bestCase();
	//worstCase();
    return 0;
}
