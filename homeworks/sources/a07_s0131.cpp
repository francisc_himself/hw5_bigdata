#include <iostream>
#include <conio.h>

using namespace std;

typedef struct nodeMW
{
    int key;
    struct nodeMW *child[3];
}NodeM;

NodeM* T1(int A[10][3], int key)
{
    NodeM *root;
    root = (NodeM *)malloc(sizeof(NodeM));
    root->key=key;
	
    if(A[key][0]!=0)
    {
        root->child[0] = T1(A, A[key][0]);
    }
    else
    {
        root->child[0]=NULL;
    }
    if(A[key][1]!=0)
    {
        root->child[1] = T1(A, A[key][1]);
    }
    else
    {
        root->child[1]=NULL;
    }
    if(A[key][2]!=0)
    {
        root->child[2] = T1(A, A[key][2]);
    }
    else
    {
        root->child[2]=NULL;
    }
	return root;

}

void print(NodeM *n)
{
	if(n!=NULL)
	{
		printf("%d ", n->key);
		
		print(n->child[0]);
		print(n->child[1]);
		print(n->child[2]);
	}
}

void main()
{
	NodeM *root;
	root = (NodeM *)malloc(sizeof(NodeM));
    
	int parent[]={2,7,5,2,7,7,-1,5,2};
	int n = sizeof(parent)/sizeof(parent[0]);
	int rootkey, i=0, j;
	int A[10][3]= {0};

	for(i=0;i<n;i++)
	{
		if(parent[i]==-1)
		{
			rootkey = i+1;
		}
		else
		{
			j=0;
			while(A[parent[i]][j]!=0)
			{
				j++;
			}
			A[parent[i]][j] = i+1;

		}
	}

	root = T1(A, rootkey);

	print(root);
    
	getch();
}
