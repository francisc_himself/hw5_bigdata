/**Assignment No.9: Breadth - First Search
	***ANONIM*** ***ANONIM*** - ***ANONIM***
	Grupa ***GROUP_NUMBER***
	Eficienta acestui logaritm este O(E + V) - eficienta liniara.
	Pe baza diagramelor putem observa ca numarul operatilor pentru o variatie foarte mare a numarul de muchii este mai mic de***ANONIM*** numarul operatilor
	pentru o variatie mica a numarul de varfuri.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define INF 10000

//structura care memoreaza datele pentru dfs
typedef struct atribute{
	int culoare; 
	int distanta;
	int parinte;
} ATRIBUTE;


//structura care reprezinta un nod in lista de adiacenta
typedef struct Node{
    int val;
    struct Node* next;
}NODE;

//structura care reprezinta datele pentru o coada
typedef struct Queue {
	int val;
	struct Queue *next;
}QUEUE; 

QUEUE *start;
NODE *head[60001], *tail[60001];
ATRIBUTE atr[60001];
int V[100001];
int timp;
int k, nr_op;
int QUE[10001], nrq;
int nivel=1;

int Q[60001];

void BFS (int s, int nrVarfuri){
	int u;
	for (u=1; u<=nrVarfuri; u++)
		if (u!=s){
			atr[u].culoare=WHITE;
			atr[u].distanta=INF;
			atr[u].parinte=0;
		}
	atr[s].culoare=GRAY;
	atr[s].distanta=0;
	atr[s].parinte=0;
	k = 1;
	Q[k] = s;
	nr_op += 5;
	printf("%d \n", s);	//printam pe s
	while(k!=0){
		u = Q[1];	//scoatem din coada
		nivel++;	//crestem nivelul
		for (int i=1; i<k; i++){
			Q[i]=Q[i+1];
		}
		k--;
		Node *temp;
		temp = head[u];	//temp e capul listei de adiacenta
		nr_op++;
		while (temp!=NULL){	//***ANONIM*** timp avem elemente
			int v=temp->val;	//retinem valoarea
			temp=temp->next;	//trecem la urmatorul elemente
			nr_op+=3;
			if (atr[v].culoare == WHITE)
			{
				atr[v].culoare=GRAY;
				atr[v].distanta = atr[u].distanta+1;
				atr[v].parinte=u;
				k++;	
				Q[k]=v;	//punem pe v in coada
				for(int i=0; i<nivel; i++)
					printf(" ");	//lasam spatiu pentru urmatorul nivel
				printf("%d \n", v);	//afisam valoarea
				nr_op+=4;
			}
		}
		atr[u].culoare=BLACK;
		nr_op++;
	}
}

//verificam daca este muchie intre x si y
bool verificaMuchie (int x, int y)
{
	NODE *p;
	p = head[x];
	while(p!=NULL){
		if(y == p->val)
			return false; 
		p = p->next;
	}
	return true;
}

void randomize(int nrVarfuri,int nrEdges)  //nrVarfuri= numarul de varfuri, nrEdges = numarul de edges
{
	int i,u,v;
	for(i = 0; i <= nrVarfuri; i++)
		head[i] = tail[i] = NULL;
	i = 1;
	while (i <= nrEdges){
		u=rand()%nrVarfuri+1;
		v=rand()%nrVarfuri+1;
		if (u!=v){
				if(head[u]==NULL){	//daca lista de adiacenta este nula
					head[u]=(NODE *)malloc(sizeof(NODE *));	//cream primul nod care este capul si coada listei
					head[u]->val=v;
					head[u]->next=NULL;
					tail[u]=head[u];
					i++;
				}
				else {//altfel, daca lista nu e nula
						if (verificaMuchie (u, v))	//daca intre u si v este muchie
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;	//il adaugam pe v in lista de adiacenta a lui u
							p->next=NULL;
							tail[u]->next=p;
							tail[u]=p;
							i++;
						}
					}
		}
	}
}


void print(int nrVarfuri)
{
	int i;
	NODE *p;
	printf("Lista de adiacenta pentru fiecare nod:\n\n");
	for(i=1;i<=nrVarfuri;i++)
	{
		printf("%d : ",i);	//printez nr varfului
		p=head[i];
		while(p!=NULL)
		{
			printf("%d ",p->val);	//printez valorile din lista de adiacenta a nodului respectiv
			p=p->next;
		}
		printf("\n");
	}
}


int main()
{
	int nrEdges, nrVarfuri;
	nrVarfuri=5;
	nrEdges=10;
	srand(time(NULL));
	randomize(nrVarfuri, nrEdges);
	print(nrVarfuri);
	printf("\n");
	BFS(1, nrVarfuri);

	FILE *f;

	/*f=fopen ("rez1.txt", "w");
	fprintf(f, "nrEdges nr_op\n");
	nrVarfuri=100;
	for (nrEdges = 100; nrEdges<=5000; nrEdges=nrEdges+100)
	{
		nr_op=0;
		genereaza(nrVarfuri, nrEdges);
		BFS(1, nrVarfuri);
		fprintf(f, "%d %d\n", nrEdges, nr_op);
	}
	fclose(f);*/

	/*f=fopen ("rez2.txt", "w");
	fprintf(f, "nrVarfuri nr_op\n");
	nrEdges=9000;
	for (nrVarfuri = 110; nrVarfuri<=200; nrVarfuri=nrVarfuri+10)
	{
		nr_op=0;
		genereaza(nrVarfuri, nrEdges);
		BFS(1, nrVarfuri);
		fprintf(f, "%d %d\n", nrVarfuri, nr_op);
	}
	fclose(f);

	printf("The end%c\n",7);*/
	printf("\nApasati un buton pentru terminarea programului.");
	getch(); 
    return 0;
}