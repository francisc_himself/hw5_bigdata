
#include "stdafx.h"

#include"stdio.h"
#include"conio.h"
#include<stdlib.h>


#include "Profiler.h" 
Profiler profiler("Josephus");


int n,m,steps;
int joselist[10010],cnt=0;


typedef struct nod
{
    int key;
    int size;
    nod *left, *right, *parent;
}NOD;
NOD* root;

void pretty_print(NOD *p,int level)
{

  if (p!=NULL){

		 pretty_print(p->left,level+1);
		 for( int i=0;i<=level;i++) printf("\t");
		 printf("%d\n", p->key);
		 pretty_print(p->right,level+1);
	       }
}


NOD* OS_SELECT(NOD *x, int i)
{
    int r=1;
    if (x->left != NULL)							//the node to be deleted is OS_SELECTed on the left and right branches
	{ profiler.countOperation("Josephus", steps, 1);
	r = x->left->size+1; }
	else
        r = 1;
    profiler.countOperation("Josephus", steps, 2);
	if (r == i){
        profiler.countOperation("Josephus", steps, 1);
		return x;}
    else
    {
        profiler.countOperation("Josephus", steps, 2);
		if (i < r)
            return OS_SELECT(x->left,i);
        else
            return OS_SELECT(x->right,i-r);
    }
}

NOD* GENERATE(int x, int dim, NOD *lft, NOD *rght)
{
    NOD *q;
    q= new NOD;
    q->key=x;					//q is the new node and it gets the value x
    q->left=lft;				// left child is created
    q->right=rght;				// right child is created
    q->size=dim;				// we add the size
    profiler.countOperation("Josephus", steps, 6);
	if (lft!=NULL)  {  
        lft->parent = q;
		profiler.countOperation("Josephus", steps, 1);		//father-child links are made
	}
	if (rght!=NULL){
		profiler.countOperation("Josephus", steps, 1);
        rght->parent=q;
	}
    q->parent=NULL;				//q is root
	
    return q;
}

NOD* BUILD_T(int lft, int rght)
{
    NOD *ds,*d;
	profiler.countOperation("Josephus", steps, 1);
	if (lft > rght)
        return NULL;
    profiler.countOperation("Josephus", steps, 1);
	if (lft == rght)
		return GENERATE(lft, 1, NULL, NULL);
	 else
    {
        int mid =(lft + rght)/2;
        ds = BUILD_T(lft, mid-1);
        d  = BUILD_T(mid+1, rght);
		profiler.countOperation("Josephus", steps, 3);
        return GENERATE(mid, rght-lft+1,ds, d);
    }
}


NOD* MINIMUM(NOD* x)
{
    while(x->left!=NULL){
        profiler.countOperation("Josephus", steps, 1);
		x = x->left;
	}
    profiler.countOperation("Josephus", steps, 1);
	return x;						//we will return the leftmost node
}

NOD* SUCCESOR(NOD* x)
{
    NOD *y;
    profiler.countOperation("Josephus", steps, 1);
	if(x->right!=NULL)						
        return MINIMUM(x->right);			
    profiler.countOperation("Josephus", steps, 2);
	y = x->parent;							
    while (y != NULL && x == y->right)		
    {        								
		profiler.countOperation("Josephus", steps, 2);
		x = y;								
        y = y->parent;
    }
    return y;								
}

NOD* DELETEN(NOD* z)
{
    NOD *y;
    NOD *x;
	profiler.countOperation("Josephus", steps, 1);
    if ((z->left == NULL) || z->right == NULL){		
        y=z;
		profiler.countOperation("Josephus", steps, 1);
	}
    else{
		profiler.countOperation("Josephus", steps, 1);
        y = SUCCESOR(z);							
	 }
	profiler.countOperation("Josephus", steps, 1); 
	if (y->left != NULL){
		 profiler.countOperation("Josephus", steps, 1);
         x = y->left;
	 }
    else{
		profiler.countOperation("Josephus", steps, 1);
		x = y->right;
	}
    profiler.countOperation("Josephus", steps, 1);
	if (x != NULL){
		profiler.countOperation("Josephus", steps, 1);
        x->parent = y->parent;
	}
	profiler.countOperation("Josephus", steps, 1);
    if (y->parent == NULL){
		profiler.countOperation("Josephus", steps, 1);
		root = x;
	}
	else if (y == y->parent->left)
	{
		profiler.countOperation("Josephus", steps, 2);
		y->parent->left = x;
	}
    else{
		profiler.countOperation("Josephus", steps, 2);
        y->parent->right = x;
	}
	profiler.countOperation("Josephus", steps, 1);
	if (y != z)
	{
		profiler.countOperation("Josephus", steps, 1);
		z->key = y->key;
	}
    return y;
}

void new_size(NOD *p)
{
    while (p != NULL)
    {
	    profiler.countOperation("Josephus", steps, 3);
		p->size--;			//for each node starting from the deleted one we decrement the dimension by 1       
		p=p->parent;
	}
	profiler.countOperation("Josephus", steps, 1);
}

void display(int joselist[],int cnt){
    printf("\nDeleted numbers: \n");
    for ( int i = 0 ; i < cnt ;i++)		
        printf("%d ", joselist[i]);
}

void JOSEPHUS(int n, int m)
{
    NOD *y,*z;
    int aux = n+1;
    int mid = m;
	profiler.countOperation("Josephus", steps, 3);
    root = BUILD_T(1, n);		//we build the tree
    //printf("\nThe tree is: \n");
    //pretty_print(root,0);
    for (int i = 1; i < aux; i++)	                                    
    {
		
		y = OS_SELECT(root, m);	    //we select the node for deletion
        profiler.countOperation("Josephus", steps, 1);
        //display(joselist,cnt);
        profiler.countOperation("Josephus", steps, 1);
		z = DELETEN(y);			//we delete the node
      // printf("\nThe tree is: \n");
      // pretty_print(root,0);
        new_size(z);			//we will resize all the starting with the deleted one's parent
                                    //cel eliminat
        delete(z);					//we free the memory
        profiler.countOperation("Josephus", steps, 2);
		n--;						//we decrement the tree length
        if (n > 0)	{				//we generate the next position to delete
            profiler.countOperation("Josephus", steps, 1);
			m = (m - 1 + mid) % n;
		}
        if (m == 0){
			profiler.countOperation("Josephus", steps, 1);
            m = n;
		}
	}

}



int main()
{
	for(steps = 100; steps <= 10000; steps = steps + 100){
		JOSEPHUS(steps, steps/2);
	}
	profiler.showReport();
	getch();
}
