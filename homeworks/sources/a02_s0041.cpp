/*
Nume:     ***ANONIM*** ***ANONIM***
Grupa:      ***GROUP_NUMBER***

Cerinte:    Implementarea corecta si eficienta a doua metode de constructie a heap:
- top-down
- bottom-up
Implementarea algoritmului heap-sort.

Rezultate:
-top-down este cel mai eficient atat din punct de vedere al compararilor cat si din punct de vedere al asignarilor .

*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

int natd, nabu, nahs ;   //na = nr atribuiri (td=top-down, bu=bottom-up, hs=heapsort)
int nctd, ncbu, nchs ;	 //nc = nr comparatii

int parinte (int i)
{
	return i/2;
}

int st (int i)
{
	return 2*i;
}

int dr (int i)
{
	return 2*i+1;
}

//BOTTOM UP
void reconstructie_heap (int a[10001],int n, int i, int dim_heap)
{
	int s,d,ind,aux;

	s = st (i);
	d = dr (i);

	ind = i;

	if (s<=dim_heap && a[s]<a[ind])
		ind = s;
	ncbu++;
	nchs++;
	if (d<=dim_heap && a[d]<a[ind])
		ind = d;
	ncbu++;
	nchs++;

	if (ind != i)
	{
		nabu+=3;
		nahs++;
		aux = a[i];
		a[i]=a[ind];
		a[ind] = aux;
		reconstructie_heap (a,n,ind,dim_heap);
	}	
}

void construieste_heap (int a[10001], int n,int dim_heap)
{
	int i;
	for (i=dim_heap/2; i>=1; i--)
		reconstructie_heap (a,n,i,dim_heap);
}

void heap_sort (int a[10001], int n)
{
	int aux;
	int dim_heap=n;
	construieste_heap (a,n,dim_heap);
	for (int i=n; i>=2; i--)
	{
		nahs+=3;
		aux = a[1];
		a[1] = a[i];
		a[i] = aux;
		dim_heap = dim_heap - 1;
		reconstructie_heap (a,n,1,dim_heap);
	}
}
//TOP-DOWN
void initH (int a[10001],int &dim_a)
{
	dim_a = 0;
}

void Hpush (int a[10001], int &dim_a, int x)
{
	int i,aux;

	dim_a = dim_a + 1;
	natd++;
	a[dim_a] = x;
	i = dim_a;
	while (i>1 && a[i]<a[parinte(i)])
	{
		nctd++;
		natd+=3;
		aux = a[i];
		a[i] = a[parinte(i)];
		a[parinte(i)] = aux;
		i = parinte (i);
	}
	nctd++;
}

void construieste_heap_td (int a[10001],int b[10001],int n)
{
	int dim_b;
	initH (b,dim_b);

	for (int i=1; i<=n; i++)
		Hpush (b,dim_b,a[i]);
}

void copy (int a[10001], int c[10001], int n)
{
	for (int i=1; i<=n; i++)
		c[i] = a[i];
}

void cazul_mediu ()
{
	int a[10001],b[10001],c[10001];
	int natdm, nabum, nahsm, naqm;
	int nctdm, ncbum, nchsm, ncqm;

	FILE *pf1;
	pf1 = fopen ("heap_td.csv","w");
	FILE *pf2;
	pf2 = fopen ("heap_bu.csv","w");
	FILE *pf3;
	pf3 = fopen ("heap_sort.csv","w");

	fprintf(pf1,"\n ;;heap top-down;;;;;");
	fprintf(pf1,"\n n;;;;;;;;nr_asignari bottom-up ;;;;;;;nr_comparari b-up;;;;;;; nr_asignari+nr_comparari \n");

	fprintf(pf2,"\n ;;heap bottom-up;;;;;;");
	fprintf(pf2,"\n n;;;;;;nr_asignari bottom-up ;;;;;;nr_comparari b-up;;;;;; nr_asignari+nr_comparari \n");

	fprintf(pf3,"\n ;;heap sort;;;;;;");
	fprintf(pf3,"\n n;;;;;nr_asignari sort ;;;;;; nr_comparari sort;;;;;; nr_asignari+nr_comparari \n");

	srand (time(NULL));

	for (int n=100; n<=10000; n=n+100)
	{
		natdm = nabum = nahsm = naqm = 0;
		nctdm = ncbum = nchsm = ncqm = 0;

		for (int k=1; k<=5; k++)
		{
			for (int j=1; j<=n; j++)
				a[j] = rand() % 5000 + 1;

			//constructie heap top-down
			natd = 0;
			nctd = 0;
			copy (a,c,n);
			construieste_heap_td (a,b,n);
			natdm += natd;
			nctdm += nctd;

			//constructie heap buttom-up
			nabu = 0;
			ncbu = 0;
			copy (a,c,n);
			construieste_heap (c,n,n);
			nabum += nabu;
			ncbum += ncbu;

			//heap sort
			nahs = 0;
			nchs = 0;
			copy (a,c,n);
			heap_sort (c,n);
			nahsm += nahs;
			nchsm += nchs;


		}

		natd/=5;nctd/=5;
		nabu/=5;ncbu/=5;
		nahs/=5;nchs/=5;
		;


		fprintf (pf1,"%d;;;;;;%d ;;;;;;; %d  ;;;;; %d\n",n,natd,nctd,natd+nctd);
		fprintf (pf2,"%d;;;;;;%d ;;;;;;; %d  ;;;;; %d\n",n,nabu,ncbu,nabu+ncbu);
		fprintf (pf3,"%d;;;;;;%d ;;;;;;; %d  ;;;;; %d\n",n,nahs,nchs,nahs+nchs);

	}
}

void test ()
{
	int a[10001],h[10001], n;

	printf ("n=");
	scanf ("%d",&n);
	printf ("\n");
	for (int i=1; i<=n; i++)
	{
		printf ("a[%d]=",i);
		scanf ("%d",&a[i]);
		printf ("\n");
	}

	construieste_heap_td (a,h,n);
	printf ("Rezultat construire heap (TOP-DOWN):\n");
	for (int i=1; i<=n; i++)
		printf ("%d ",h[i]);

	printf ("\n");
}
void Pretty_print(int *A,int n,int k,int nivel)
{
	if(k>n) return;
	Pretty_print(A,n,2*k+1,nivel+1);
	//printf("%d\n",A[k]);
		for(int i=1;i<=nivel;i++)
		{
			printf("   ");
		}
		printf("%d\n",A[k]);
	Pretty_print(A,n,2*k,nivel+1);
}
int main ()
{
	int i;
	int c[9];
	c[0]=1;
	c[1]=5;
	c[3]=2;
	c[4]=3;
	c[5]=10;
	c[6]=9;
	c[7]=7;
	c[8]=6;
	c[9]=8;
	c[10]=0;
	//getche ();
	
	for (i=0;i<9;i++)
	{
    heap_sort(c,i);
		//printf("vectorul sortat este","%d",c[i]);
	Pretty_print(c,i,1,0);
	printf("\n\n-------------------------------------\n\n");
	}
	return 0;
}
