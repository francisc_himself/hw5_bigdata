/*
	Cerinta         : Se cere implementarea corecta si eficienta a 
					�	Algoritmului DFS pentru un graf reprezentat prin listele vecinilor.
					�	Etichetarea muchiilor pentru un graf orientat cu 10 noduri. Muchiile vor fi 
						etichetate in muchii de arbore, muchii inainte, muchii inapoi si muchii transversale.
*/


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

 //int n=6;
int color[100], d[100], pi[100],f[100];
int timp;
int a[100][100];
int atr,comp,comp1;

void Vizitare(int i,int n){
	color[i] = 1;
    timp++;
    d[i] = timp;
	atr+=3;
    for (int j = 1; j <= n; j++){
        if (a[i][j] != 0)
		{
			comp1++;
            if (color[j] == 0)
				printf("%d %d %s", i, j, "muchie de arbore");
			else
				comp1++;
				if (color[j] == 1)
					printf("%d %d %s", i, j, "muchie inapoi");
				else
				comp1++;
				if (color[j] == 2)
					comp1++;
					if (d[i] < d[j])
						printf("%d %d %s", i, j, "muchie inainte");
					else
						printf("%d %d %s", i, j, "muchie transversala");
            printf("\n");
			comp++;
            if (color[j] == 0)
			{
				atr++;
                pi[j] = i;
                Vizitare(j,n);
            }
        }
    }
    color[i] = 2;
    f[i] = timp + 1;
	atr+=2;
}

void dfs(int n){
    for (int i = 1; i <= n; i++)
	{
			
        color[i] = 0;
        pi[i] = 0;
    }
    timp = 0;
	atr++;
    for (int i = 1; i <= n; i++)
	{
		comp++;
        if (color[i] == 0)
		{
            Vizitare(i,n);
        }
    }
}
void afisare(int nivel,int n)
{
     for (int i = 1; i <= n; i++)
		{
         printf("Nodul %d are muchii spre",i);
		 for (int j = 1; j <= n; j++){
			if (a[i][j] != 0){
				printf(" %d ",j);
	             }
		}
	  printf("\n");
     }
}


int main()
{
	FILE *f,*f1;
	f=fopen("rezultat.csv","w");
	f1=fopen("rezultat1.csv","w");
	srand(time(NULL));
	for(int k=10;k<=100;k+=10)
	{
		comp=0;
		atr=0;
		for (int i = 1; i <=k; i++)
		{
			for (int j = 1; j <=k; j++) 
			{
				if(i==j) a[i][j]=0;
				else a[i][j]=rand()%2;
			
			}
		}
		printf("Arborele %d:\n",k/10);
		printf("-------------------------------------------------");
		afisare(0,k);
		dfs(k);
		fprintf(f,"%10d %10d\n",k,atr+comp);
	}
	fclose(f);

	int x=0;
	for(int k=0;k<5;k++)
	{
		comp1=0;
		atr=0;
		for (int i = 1; i <=10; i++)
		{
			for (int j = 1; j <=10; j++) 
			{
				if(i==j) a[i][j]=0;
				else a[i][j]=rand()%2;
			
			}
		}
		//afisare(0,k);
		dfs(10);
		x=x+atr+comp1;
		
	}
	
	fprintf(f1,"%d\n",x/5);
	fclose(f1);
	getchar();
}
