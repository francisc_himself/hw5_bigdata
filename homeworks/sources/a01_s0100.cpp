/*
	Assignment 1 
	Start date: March - 6 - 2013
	End date:	March - 13 - 2013

	***ANONIM*** ***ANONIM***
	GROUP: ***GROUP_NUMBER***
*/

#include<fstream>
#include<iostream>
#include<conio.h>
#include<ctime>

using namespace std;
int AS,CM;
int b[10000];


void selection(int a[], int n)
{
	AS = 0 ;
	CM = 0 ;
	for(int flag = 0 ; flag < n - 1; flag++)
	{
		// initializing the flag with the current position
		int pos = flag;
		for(int i = flag + 1; i < n ; i++) // search for the smallest element
		{
			CM++;
			if(a[i] < a[pos]) // find the smallest element
				pos = i;
		}
		if(pos != flag) // we interchange a[pos] ( the minimum we found ) with a[flag] ( the current position )
		{
			int aux = a[pos];
			a[pos] = a[flag];
			a[flag] = aux;
			AS += 3;
		}
		// if pos = flag it means we did not find a smaller element than the one we are currently at
	}
}
void insertion(int a[], int n)
{
	AS = 0 ;
	CM = 0 ;
	for(int flag = 1 ; flag < n ; flag++) // we start with the second element ( the first one is considered to be ordered )
	{
		for(int j = flag; j > 0 && CM++ && a[j] < a[j-1] ; j--) // we exchange the current element with the one behind it until he is at its place
		{

			int aux = a[j];
			a[j] = a[j-1];
			a[j-1] = aux;
			
			AS += 3;
		}
		
	}

	

}

void bubble(int a[],int n)
{
	AS = 0 ;
	CM = 0 ;
	// we define lowerBound and higherBound the bounds in which the array is NOT ordered and we only perform operations between these bounds
	int lowerBound = 0; 
	int higherBound = n;
	bool exchangeDone = true;
	while(exchangeDone)
	{
		exchangeDone = false;
		for(int i = lowerBound ; i < higherBound - 1; i++) // we cross the array from lowerBound to higherBound and compare each two adjacent elements and exchange them if needed
		{
			CM++;
			if(a[i] > a[i+1])
			{	
				int aux = a[i];
				a[i] = a[i+1];
				a[i+1] = aux;
				AS += 3;
				exchangeDone = true;
			}
		}
		for(int i = higherBound - 1; i > lowerBound ; i--)// we cross the array from higherBound to lowerBound and compare each two adjacent elements and exchange them if needed
		{
			CM++;
			if(a[i] < a[i-1])
			{	
				int aux = a[i];
				a[i] = a[i-1];
				a[i-1] = aux;
				AS += 3;
				exchangeDone = true;
			}
		}
		// after the first crossing, the maximum in he array gets to the last position, so we can lower the range we perform operations in
		higherBound--;
		// after the second crossing, the minimum in he array gets to the first position, so we can lower the range we perform operations in
		lowerBound++;
	}
	
}

int main()
{
	int a[10000],n;
	srand(time(NULL));

	ofstream g("date.txt");
	//	BEST CASE SCENARIO
	g<<"BEST CASE\n";
	g<<"n, bubble_AS, bubble_CM, bubble_AS+CM, insertion_AS, insertion_CM, insertion_AS+CM, selection_AS, selection_CM, selection_AS+CM";
	g<<"\n";
	n = 100;
	float x = 0.; // Used to calculate the complete % while generating the results for the tests
	for(int n = 100 ; n < 10000 ; n += 200)
	{
		// 0 1 2 3 ... n   for all methods
		for(int i = 0 ; i < n ; i++)
			a[i] = i;
		g<<n<<", ";

		bubble(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<", ";
		insertion(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<", ";
		selection(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<"\n";
		

		// 0 - 33 %
		x = n*100/10000/3;
		cout<<"\n"<<x<<"%\n";
	}

	//	AVERAGE CASE
	g<<"\n\nAVERAGE CASE \n";
	g<<"n, bubble_AS, bubble_CM, bubble_AS+CM, insertion_AS, insertion_CM, insertion_AS+CM, selection_AS, selection_CM, selection_AS+CM";
	g<<"\n";
	n = 100;
	for(int n = 100 ; n < 10000 ; n += 200)
	{
		
		for(int i = 0 ; i < n ; i++)
			b[i] = rand()%1200;
		g<<n<<", ";

		for(int i = 0 ; i < n ; i++) a[i] = b[i];
		bubble(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<", ";
		
		for(int i = 0 ; i < n ; i++) a[i] = b[i];
		insertion(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<", ";

		for(int i = 0 ; i < n ; i++) a[i] = b[i];
		selection(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<"\n";
	

		// 33  - 66%
		x = 33 + n*100/10000/3;
		cout<<"\n"<<x<<"%\n";
	}



	
	//	WORST CASE SCENARIO
	g<<"\n\WORST CASE \n";
	g<<"n, bubble_AS, bubble_CM, bubble_AS+CM, insertion_AS, insertion_CM, insertion_AS+CM, selection_AS, selection_CM, selection_AS+CM";
	g<<"\n";

	for(int n = 100 ; n < 10000 ; n += 200)
	{
		for(int i = 0 ; i < n ; i++)
			b[i] = n - i;
		g<<n<<", ";

		// n n-1 n-2 ... 0 for bubble
		for(int i = 0 ; i < n ; i++)
			a[i] = b[i];
		bubble(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<", ";
		
		
		// n n-1 n-2 ... 0 for insertion
		for(int i = 0 ; i < n ; i++) 
			a[i] = b[i];
		insertion(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<", ";

		// 1 2 3 ... n 0 for selection
		for(int i = 0 ; i < n - 1 ; i++) 
			a[i] = i + 1;
		a[n-1] = 0;
		selection(a,n);
		g<<AS<<", "<<CM<<", "<<AS+CM<<"\n";
	

		// 66 - 100 %
		x = 66. + n*100/10000/3;
		cout<<"\n"<<x<<"%\n";
	}
	return 0;
}