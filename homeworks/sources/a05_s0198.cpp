/*
Interpretation:
	- the number of operations increases once the filling factor increases
	- for finding elements in the hash table the number of operations is low and relatively constatnt (1,2,2,3,4)
	- for finding elements not in the hash table, the number of operations is alfo relatively small, i. e.
	in the average case it is 1/(1-alfa) where alfa is the filling factor in each case
*/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <conio.h>
using namespace std;

fstream file;
int nr_el=9973;
  int found[1501], counter, increment, Found, NotFound, FoundMax, NotFoundMax;
long h[10001];

void init()
{
	int i;
	for(i=0;i<nr_el;i++)
		h[i]=0;
}


int h_func_aux(int x)
{
	return x%nr_el;
}

int h_func_q_p(int x,int i)
{
	int c1=3, c2=5;
	return (h_func_aux(x)+c1*i+c2*i*i)%nr_el;
}

int H_insert(int x)
{
	int i=0,j;

	do{
		j=h_func_q_p(x,i);
		if(h[j]==0)
		{	
			increment++;
			h[j]=x;
			return j;
		}
		else i++;
	  }while(i!=nr_el);
}

int H_search(int x)
{
	int i=0,j;

	do{
		j=h_func_q_p(x,i);
		increment++;
		if(h[j]==x)
			return j;
		i++;
	}while(h[j]!=0 && i!=nr_el);

}

void main()
{
	int n, i,j,k;
	float af;
	float alfa[]  = {0.8, 0.85, 0.9, 0.95, 0.99};
	long x;
	float a;
	int y=0;

	file.open("hash.txt", ios::out);
	 srand(3);
	file<<"FACTOR "<<"AVG FOUND "<<"AVG NOT FOUND "<<"MAX FOUND "<<"MAX NOT FOUND\n";
/*
	cout<<"nr_el of elements: \n";
	cin>>n;
	cout<<"\nintroduce element: \n";

    for(i=1;i<=n;i++)
	{
		cin>>x;
		H_insert(x);
	}

	for(i=1;i<=n;i++)
	{
		if(h[i])
			cout<<"Position "<<i<<"->"<<h[i]<<endl;
	}
*/
	for(i = 0; i< 5; i++)
	{
		af=alfa[i];
		init();
		int y;
		
		Found=0; NotFound=0;
		
		
		file<<alfa[i]<<",    ";
		for(k=1;k<=5;k++)
		{
			
			init();
			counter=0;
			FoundMax=0; 
			NotFoundMax=0;

		for(j=1;j<=af*nr_el;j++)
		{ 
			x=rand()*rand()%100000+1;
			H_insert(x);
		}

		int z;
			for(z=1;z<1501;z++)
				found[z]=0;
			
			counter=0;
//formam lista de elemente ce le vom cauta
		while (counter<=1500)
		{
			
			y=rand()%nr_el+1;
			//cout<<y<<"\n";
			if(h[y] != 0)
			{
				counter++;
				found[counter]=h[y];
				
			}
		}

		FoundMax=0; 
		NotFoundMax=0;
         
		for(j=1;j<=3000;j++)
		{
			increment=0;
			if(j<=counter)
			{ 
				H_search(found[j]);
				if(increment>FoundMax)
					FoundMax=increment;
			}
			Found+=increment;
			
			
			increment=0;
			//cand j>1500 cautam si elemente ce lipsesc
			if(j>counter)
			{
				x=100000+rand()*rand();
				H_search(x);
				
				if(increment>NotFoundMax)
					NotFoundMax=increment;
				
			}
			NotFound+=increment;
			
		}
		}
        //counter = nr of elements
		NotFound=NotFound/5;
		Found=Found/5;
		file<<Found/counter<<",         ";
	
		file<<NotFound/(3000-counter)<<",             ";
		
		file<<FoundMax<<",         ";
     
		file<<NotFoundMax<<endl;
		
		FoundMax=0; 
		NotFoundMax=0;

	}



}








