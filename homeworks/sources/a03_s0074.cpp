//***ANONIM*** ***ANONIM***
//Grupa ***GROUP_NUMBER***
//Heapsort vs Quicksort

#include <stdio.h>
#include<stdlib.h>
#include<conio.h>
#include <time.h>
int n,n1,largest,l,r,nra1,nrc1,nra2,nrc2;
int a[10000],tmp1[10000],tmp2[10000];
int nr_c1[100],nr_a1[100],nr_c2[100],nr_a2[100];
//functie de reconstructie heap
void max_heapify(int a[],int i)
{
	int aux;
	l=2*i;
	r=2*i+1;
	nrc1++;
	if(l<=n1 && a[l]>a[i])	
	{
		largest=l;
	}
	else
		largest=i;
	nrc1++;
	if (r<=n1 &&a[r]>a[largest])
		largest=r;
	if(largest!=i)
	{
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		nra1=nra1+3;
		max_heapify(a,largest);
	}

}
//construieste heap-ul prin metoda bottom-up
//Eficienta metodei este O(n) este preferabila metodet Top-Doun deoarece numarul de atribuiri si comparatii
//efectuate este mai mic
void build_max_heap( int a[])
{
	n1=n;
	for(int i=n/2;i>=1;i--)
		max_heapify(a,i);
}


void heapsort(int a[])
{
	int aux,i;
	build_max_heap(a);
	for(i=n;i>=1;i--)
	{
		
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		n1--;
		max_heapify(a,1);
	}
}
//sortarea prin metoda Quicksort
//eficienta metodei pe in caul mediu statistic este O(n*log n)
void quicksort(int v[], int stanga, int dreapta)
{
   int i, j, mijloc, aux;         // variabilele
   i=stanga;                       //  initializarea
   j=dreapta;//   indicilor
   nra2++;
   mijloc=v[(stanga+dreapta)/2];  // initializarea variabilei - pivot
   
  while(i<=j)
  {
	  nrc2++;
   while(v[i]<mijloc)  // apropierea i-ului de mijloc
    {
		i++;
		nrc2++;
   }
		nrc2++;
   while(v[j]>mijloc)   // apropierea j-ului de mijloc
      {
		  j--;
		  nrc2++;
   }
    if(i<=j)   //conditia efectuarii operatiei de interschimbare
    {
       aux=v[i];
       v[i]=v[j];   // operatia de interschimbare
       v[j]=aux;
       i++;
       j--;
	   nra2+=3;
     }
  }                    
 if(stanga<j)                    //recursivitatea            
   quicksort(v, stanga, j);   // in partea stanga    
 if(i<dreapta)                    
   quicksort(v, i, dreapta);  // in partea dreapta
}
//Se observa ca in cazul mediu statistic numarul de atribuiri si comparatii efectuate de metoda quicksort este mai mic decat la metoda heapsort 
int main()
{
	FILE *pf;
	int i,j;
	pf=fopen("fis1.csv","a");
	for(j=0;j<5;j++)
	{
		for (i=1;i<=10000;i++)
			a[i]=(int)rand()%50000;
		for (n=100;n<=10000;n+=100)
			{
				for(i=1;i<=n;i++)
					{
						tmp1[i]=a[i];
						tmp2[i]=a[i];
					}
				nra1=0;
				nrc1=0;
				
				heapsort(tmp1);
				nr_a1[n/100-1]=nra1;
				nr_c1[n/100-1]=nrc1;
				nra2=0;
				nrc2=0;
				quicksort(tmp2,1,n);
				nr_a2[n/100-1]=nra2;
				nr_c2[n/100-1]=nrc2;
			}
	}
	for(i=0;i<n/100-1;i++)
			fprintf(pf,"%d,",nr_a1[i]/5);
		fprintf(pf,"\n");
		for(i=0;i<n/100-1;i++)
			fprintf(pf,"%d,",nr_c1[i]/5);
		fprintf(pf,"\n");
		for(i=0;i<n/100-1;i++)
			fprintf(pf,"%d,",nr_a2[i]/5);
		fprintf(pf,"\n");
		for(i=0;i<n/100-1;i++)
			fprintf(pf,"%d,",nr_c2[i]/5);
		fprintf(pf,"\n");
		for(j=1;j<=1000;j++)
			printf("%d ",tmp1[j]);
		printf("\n\n");
		for(j=2;j<=1001;j++)
			printf("%d ",tmp2[j]);
		printf("\n");
		fclose(pf);
	getch();
	return 0;
}