#include<iostream>
#include<conio.h>
#include"Profiler.h"
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
using namespace std;
Profiler profiler("demo");
long assTD=0;
long assBU=0;



void heapifyBU(int A[], int i, int n)
{
int left = 2 * i + 1;
int right = 2 * i + 2;

int largest = i;
if(left < n)
{
profiler.countOperation("CMP BottomUp",n);
if(A[left] > A[largest])
largest = left;
}
if(right < n)
{
profiler.countOperation("CMP BottomUp",n);
if(A[right] > A[largest])
largest = right;
}
if(largest != i)
{
profiler.countOperation("ASS BottomUp",n,3);
assBU+=3;
int aux = A[i];
A[i] = A[largest];
A[largest] = aux;

heapifyBU(A,largest,n);
}
}
void bottomUp(int A[], int n)
{
for(int i = (n-2)/2 ; i >= 0 ; i-- )
heapifyBU(A,i,n);
}



int getParent(int i)
{
return (i==0)?0:(i-1)/2;
}

void insertKey(int res[],int i,int value,int n)
{
res[i] = value;
profiler.countOperation("CMP TopDown",n);
while (i >= 0 && res[getParent(i)] < res[i])
{
profiler.countOperation("CMP TopDown",n);

int aux = res[i];
res[i] = res[getParent(i)];
res[getParent(i)] = aux;

profiler.countOperation("ASS TopDown",n,3);
i = getParent(i);
}
}
void topDown(int result[],int source[], int n)
{
for (int i = 0; i < n; i++)
insertKey(result,i,source[i],n);
}


int main()
{
int n,a[12000],temp[12000];


for(n = 100; n <= 10000 ; n+=100)
{
FillRandomArray(a,n,0,10000,true,0);
for(int i=0;i<n;i++)
temp[i]=a[i];
bottomUp(a,n);
topDown(a,temp,n);
cout<<n<<endl;
}

profiler.addSeries("sum TopDown","CMP TopDown","ASS TopDown");
profiler.addSeries("sum BottomUp","CMP BottomUp","ASS BottomUp");
profiler.createGroup("sum","sum TopDown","sum BottomUp");

profiler.createGroup("ASS","ASS TopDown","ASS BottomUp");
profiler.createGroup("CMP","CMP TopDown","CMP BottomUp");


profiler.showReport();

return 0;
}