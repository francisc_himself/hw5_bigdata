#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "math.h"
#include "time.h"

/*
Quick sort pe grafic pare liniar dar defapt e logaritmic.
Diferentele intre cazurile favorabile si defavorabile la quick sort sunt mari, desi cazul mediu este mai apropiat de cazul favorabil.
Daca luam pivotul la ultimul element, cazul favorabil o sa fie sirul sortat si cazul defavorabil o sa fie sirul sortat invers.
Pe grafic se vede ca si heap sortul e logaritmic, dar quick sort face mai putine operatii, deci e mai eficient.
*/

int A[10001], B[10001], C[10001];
int n,dim;
int i,j;
int tot_q, tot_h;
int best, worst;
int qu, he;

void afis(int A[],int n);

void Reconstituie_heap(int A[], int j)
{
	int l, r, max;
	l=2*j;
	r=2*j+1;
	if((l<=dim) && A[l]>A[j])
		 max=l;
	else max=j;
    if((r<=dim) && A[r]>A[max])
		  max=r;
	he=he+2;//
	if(max!=j) 
	{
		int aux;
		aux=A[j];
		A[j]=A[max];
		A[max]=aux;
		he=he+3;//
		Reconstituie_heap(A, max);
	}
}

void Constructie_heap_BUTTOMUP(int A[])
{
	for(i=dim/2; i>=1; i--)
	{ 
		Reconstituie_heap (A, i);
	}
}

void HEAPSORT(int A[])
{
	dim=n;///////////////////////
	Constructie_heap_BUTTOMUP(A);
	for(i=n; i>=1; i--)
	{
		int aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		he=he+3;//
		dim--;
		Reconstituie_heap(A,1);
	}
}

int repartitie(int A[], int p, int r)
{
	int aux;
	//int x=A[r];
	int x=A[(p+r)/2];
	qu++;//
	i=p-1;
	for(j=p; j<=r-1; j++)
	{
		if(A[j]<=x)
		{
			i=i+1;
			aux=A[j];
			A[j]=A[i];
			A[i]=aux;
			qu=qu+3;//
		}
		qu++;//
	}
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;
	qu=qu+3;//
	return i+1;
}

void QUICKSORT(int A[], int p, int r)
{
	if (p<r)
	{
		int q=repartitie(A,p,r);
		QUICKSORT(A,p,q-1);
		QUICKSORT(A,q+1,r);
	}
}

void generate_average (int n)
{
    for(int i=1;i<=n;i++)
	     C[i]=rand()%10000;
}

void generate_worst (int n)
{
	for(i=1;i<=n/2; i++)
		  A[i]=n/2+i;
    for(i=n/2+1; i<=n; i++)
		  A[i]=i-n/2;
}

void generate_worst2 (int n)
{
	for(int i=1;i<=n; i++)
		C[i]=n-i;
}

void generate_best (int n)
{
    for(int i=1;i<=n;i++)
	     A[i]=i;
}

void generate_best2 (int n)
{
    for(int i=1;i<=n;i++)
	     A[i]=i;
	int aux=A[n-1];
	A[n-1]=A[n/2];
	A[n/2]=aux;
}

void copy(int C[], int A[], int n)
{
	for(i=1; i<=n; i++)
		A[i]=C[i];
}

void afis(int A[],int n)
{
	for(i=1;i<n; i++)
		printf("%d ",A[i]);
	printf("\n");
}

void main ()
{
	//n=11;
	//generate_best2 (n);
	//afis(A,n);

	FILE *f;
	srand(time(NULL));
	f=fopen("avg.txt","w");
	fprintf(f,"n avg_quick avg_heap \n");
	for(n=100; n<=10000; n=n+100)
	{
		tot_q=tot_h=0;
		for(int k=0; k<5; k++)
		{
			he=qu=0;
			generate_average(n);

			copy(C,A,n);
			HEAPSORT(A);
			//afis(A,n);

			copy(C,A,n);
			QUICKSORT(A,1,n);
			//afis(A,n);

			tot_q=tot_q+qu;
			tot_h=tot_h+he;
			printf(".");
		}
		fprintf(f,"%d %d %d \n", n, tot_q/5, tot_h/5);
	}
	fclose(f);

	f=fopen("quick.txt","w");
	fprintf(f,"n best worst \n");
	for(n=100; n<=10000; n=n+100)
	{
			qu=0;

			generate_best(n);
			copy(C,A,n);
			QUICKSORT(A,1,n);
			best=qu;
			//afis(A,n);

			//generate_worst(n);
			generate_worst2(n);
			copy(C,A,n);
			QUICKSORT(A,1,n);
			//afis(A,n);
			worst=qu;

			printf(".");
		fprintf(f,"%d %d %d \n", n, best, worst);
	}
	fclose(f);



	n=9;
	printf("\nINITIAL: \n");
	generate_average(n);
	copy(C,A,n);
	afis(A,n);
	printf("Dupa HEAPSORT: \n");
	HEAPSORT(A);
	afis(A,n);
	printf("Dupa QUICKSORT: \n");
	QUICKSORT(A,1,n);
	afis(A,n);

	printf("Am terminat %c", 7);
	getch();
}