/****ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***
	Acest algoritm prezentat mai jos este cunoscut ca avand o eficienta de O(nlgk).Aceasta eficienta reiese din eficienta functiilor folosite in derularea programului.
	Functiile folosite sunt heap_push,heap_pop,heap_init,reconstructie si interclasare.Fiecare dintre functiile prezente au un rol important in obtinerea rezultatului.
	Eficientele acestor functii sunt: pentru heap_push avem eficienta O(lgk),pentru heap_pop avem eficienta O(lgk),pentru heap_init avem eficienta O(1),iar 
	interclasarea va avea la final eficienta O(nlgk) datorita while-ului care transforma efiecienta heap_push-ului in cea din urma.
*/
#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"


typedef struct nod{
	int valoare;
	
	struct nod *next;
}nod;
typedef struct HEAP{
	int id;
	int val;
}HEAP;
HEAP H[1000];
nod *head[1000];
nod *tail[1000];
nod *joinedHead,*joinedTail;
int assign,comp,lungime;
int parinte(int i)
{
	return i/2;
}
int stg(int i)
{
	return 2*i;
}
int dr(int i)
{
	return 2*i+1;
}
void heap_init(HEAP *H,int *n)
{
   *n=0;
	
	
}
void reconstituie(int i,int dim)
{
	int st=stg(i);
	int drp=dr(i);
	int min;
	comp=comp+2;
	if(st<=dim && H[st].val<H[i].val)
		min=st;
	else min=i;
	if(drp<=dim && H[drp].val<H[min].val)
		min=drp;
	if(min!=i)
	{
		HEAP aux=H[i];
		H[i]=H[min];
		H[min]=aux;
		assign=assign+3;
		reconstituie(min, dim);

	}
}
void heap_push(int x,int index,int *dim)
{
	
	(*dim)++;
	H[*dim].val=x;
	H[*dim].id=index;
	assign++;
	int i=*dim;
	while((i>1)&&(H[i].val<H[parinte(i)].val))
	{
		HEAP aux=H[i];
		H[i]=H[parinte(i)];
		H[parinte(i)]=aux;
		i=parinte(i);
		assign=assign+3;
		comp++;
	}
	comp++;
}
HEAP heap_pop(int *dim)
{
	
	HEAP aux;
	if(*dim>0)
	{
		 aux=H[1];
		H[1]=H[*dim];
		H[*dim]=aux;
		(*dim)--;
		assign=assign+3;
		reconstituie(1,*dim);


	}
	
	return aux;
}

void inserare(nod **h,nod **t,int n)
{
	nod *x=(nod*)malloc(sizeof(nod));
	x->valoare=n;
	
	if(*h==0 )
	{
		
		(*h)=x;
		(*t)=x;
	}
	else
	{
		x->next=NULL;
	(*t)->next=x;
	(*t)=x;
	


	}
}
nod* stergere(nod **h,nod**t)
{
	nod*p=*h;
	if(*h!=NULL)
	{
		
		*h=(*h)->next;
		
		if(*h==0) *t=0;
		return p;
	}
	else return NULL;
	

}
void afisare(nod *h)
{
	nod *p;
	p=h;
	if(p==0) printf("");
	else
	{
		do
		{
			printf("%d  ",p->valoare);
			p=p->next;
		}while(p!=NULL);
	}
}
void interclasare(nod **h,nod **t,int k,int *n)
{
	heap_init(H,n);
	nod*p,*q;
	//*head=0;
	//*tail=0;
	
	p=(nod*)malloc(sizeof(nod));
	for(int i=0;i<k;i++)
	{
		//afisare(h[i]);
		p=stergere(&(h[i]),&(t[i]));
		assign++;
		heap_push(p->valoare,i,n);
	}
	while(*n>0)
	{
		HEAP h1=heap_pop(n);
		inserare(&joinedHead,&joinedTail,h1.val);
		q=(nod*)malloc(sizeof(nod));
		q=stergere(&(h[h1.id]),&(t[h1.id]));
		assign=assign+2;
		if(q!=NULL)
			heap_push(q->valoare,h1.id,n);
			comp++;

	}
}
void main()
{
	srand(time(NULL));
	int v1[]={0,1,2,3,4,5};
	int v2[]={6,7,8,9,10,11};
	int v3[]={0,1,2,3,4,5};
	int v[30];
	for(int i=0;i<3;i++)
	{
				
			if(i==0) memcpy(v,v1,6*sizeof(int));
			if(i==1) memcpy(v,v2,6*sizeof(int));
			if(i==2) memcpy(v,v3,6*sizeof(int));
	
	for(int j=0;j<=5;j++)
	{
	    inserare(&head[i],&tail[i],v[j]);
	}
	afisare(head[i]);
	printf("\n");
	}
	int i=0;
	interclasare(head,tail,3,&i);
	afisare(joinedHead);
	FILE *f;
	f=fopen("afis.csv","w");
	fprintf(f,"n ; k=5 ; k=10 ; k=100\n");
	int k;
	for(int n=100;n<5000;n+=100){
		for(int i=0;i<3;i++){
			if(i==0) k=5;
			if(i==1) k=10;
			if(i==2) k=100;
			lungime=n;
			printf("n=%d k=%d\n",n,k);
			for(int j=0;j<k;j++){
				int v0[10000];
				FillRandomArray(v0,n,1,50000,false,1);
				for(int i=0;i<n;i++){
				inserare(&head[j],&tail[j],v0[i]);
				}
			}
			assign=0;
			comp=0;
			interclasare(head,tail,k,&n);
			switch(k)
			{
			case 5: fprintf(f,"%d ; %d ;",lungime,comp+assign);
					break;
			case 10: fprintf(f,"%d ; ",comp+assign);
					break;
			case 100: fprintf(f,"%d \n",comp+assign);
					break;

			}
			n=lungime;
}
	}
	fclose(f);
	k=5;
	FILE *fd;
	fd=fopen("afisConst.csv","w");
	fprintf(fd,"k ; n ; comp+assign\n");
	for(int n=100;n<=5000;n+=100){
		for(int i=0;i<k;i++){
			lungime=n;
			printf("n=%d k=%d\n",n,k);
			for(int j=0;j<k;j++){
				int v0[10000];
				FillRandomArray(v0,n,1,50000,false,1);
				for(int i=0;i<n;i++){
				inserare(&head[j],&tail[j],v0[i]);
				}
			}
			assign=0;
			comp=0;
			interclasare(head,tail,k,&n);
			n=lungime;
	}
	fprintf(fd,"%d ; %d ; %d\n",k,lungime,comp+assign);
}
fclose(fd);

}