/*
Cerinte:    Implementarea corecta si eficienta a algoritmilor heap-sort si quick-sort
 In cazul mediu algoritmul quick sort se descurca mai bine decat cel quick-sort.


*/
#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

int marime=0;
int heap_asignari,heap_comparari, quick_asignari, quick_comparari ;
int n;
int dimHeap;
int a[10001],b[10001];

int stanga(int i)
{
	return 2*i;
}

int parinte(int i)
{
	return i/2;
}

int dreapta(int i)
{
	return (2*i)+1;
}

void ReconstructieHeap(int a[],int i) 
{
	int st,dr,index;
	long aux;
	st=stanga(i);
	dr=dreapta(i);
	heap_comparari++;
	if(st<=dimHeap && a[st]>a[i])
		index=st;
	else
		index=i;
	heap_comparari++;
	if(dr<=dimHeap && a[dr]>a[index])
		index=dr;
	if(index!=i)
	{
		aux=a[i];
		a[i]=a[index];
		a[index]=aux;
		heap_asignari+=3;
		ReconstructieHeap(a,index);
	}
}

void ConstruiesteHeapBU(int a[]) 
{
	dimHeap=n;
	for(int i=dimHeap/2;i>=1;i--)
		ReconstructieHeap(a,i);
}

void HeapSort(int a[],int n)
{
	ConstruiesteHeapBU(a);
	
	int	i,aux;
	for (i=n;i>=2; i--)
	{
		heap_asignari=heap_asignari+3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		dimHeap=dimHeap-1;
		ReconstructieHeap(a,1);
	}
}

int Partitie(int a[], int p, int r)    // quicksort partitie
{
	int x,i,j;
	quick_asignari++;
	x=a[p];
	i=p-1;
	j=r+1;
	int aux;
	while (1) 
	{
		
		do
		{
			quick_comparari++;
			j=j-1;

		}while (a[j]<x);

		
		do
		{
			i++;
			quick_comparari++;
		}while (a[i]>x);

	
		if (i<j)
		{
			quick_asignari=quick_asignari+3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		} else return j;	



	}

}


void QuickSort(int a[], int p, int r)
{
	int q;
	if (p<r)
	{
		q=Partitie(a,p,r);
		QuickSort(a,p,q);
		QuickSort(a,q+1,r);
	}

}


int main()
{
	
	int k;
	
	
	FILE *f=fopen("cazul mediu.csv","w");
	FILE *g=fopen("cazul defavorabil.csv","w");
	FILE *h=fopen("cazul favorabil.csv","w");
	fprintf(f, "n  ;;;;atribuiri heap  ;;;;comparari heap  ;;;;sumaheap, ;;;atribuiri quick, ;;;;comparari quick, ;;;;suma quick\n");
	fprintf(g, "n  ;;;;atribuiri quick  ;;;;comparar quick  ;;;;suma quick \n");
	fprintf(h, "n  ;;;;atribuiri quick  ;;;;comparar quick  ;;;;suma quick \n");
	
	//cazul mediu
	for(n=100;n<=10000;n+=100)
	{
		
		heap_asignari=0;
		heap_comparari=0;
		quick_asignari=0;
		quick_comparari=0;
		
	
		for(int numar=1;numar<=5;numar++)
		{
			
			for(int i=0;i<n;i++)
			{
				srand(time(NULL));
				a[i]=rand();
			}
			
			HeapSort(a,n);
			QuickSort(a,1,n);

		
		}

		
		fprintf(f,"%d;;;; %d  ;;;;%d, ;;;;%d;;; ", n , heap_asignari, heap_comparari, heap_asignari+heap_comparari);
		fprintf(f,"%d ;;;;%d  ;;;;%d\n ", quick_asignari, quick_comparari, quick_asignari+quick_comparari);

		// avem cele doua sortari in cazul mediu 
	    // urmeaza quicksortul in cazul favorabil si defavorabil
		
		k=n;
		//cazul defavorabil
		for (int i=1; i<=n; i++)
		{
			a[i]=k;
			k--;
		}
		 
		
		quick_asignari=0;
		quick_comparari=0;
		QuickSort(a,1,n);
		fprintf(g,"%d;;;; %d ;;;; %d ;;;;%d\n ", n, quick_asignari, quick_comparari, quick_asignari+quick_comparari);

		//cazul favorabil
		quick_asignari=0;
		quick_comparari=0;
		
		
		for (int i=1; i<=n; i++)
		{
			a[i]=i;
		}
		QuickSort(a,1,n);
		fprintf(h,"%d ;;;;%d ;;;;%d  ;;;;%d\n ", n, quick_asignari, quick_comparari, quick_asignari+quick_comparari);

		
	}

	fclose(f);
	fclose(g);
	fclose(h);
	
	int v[11],q[10];
	v[0]=0;
	v[1]=5;
	v[2]=3;
	v[3]=2;
	v[4]=9;
	v[5]=10;
	v[6]=8;
	v[7]=6;
	v[8]=7;
	v[9]=1;
	v[10]=4;

	q[0]=5;
	q[1]=3;
	q[2]=2;
	q[3]=9;
	q[4]=10;
	q[5]=8;
	q[6]=6;
	q[7]=7;
	q[8]=1;
	q[9]=4;
	n=9;
	QuickSort(q,0,9);
	HeapSort(v,10);
	printf("Vectorul sortat prin metoda heap-sort est : ");
	for (int i=1; i<=10;i++)
	   {
		   
       printf("%d ", v[i]);
	}
	   printf("\n--------------------------------------------------\n");
	   printf("Vectorul sortat prin metoda quick-sort est : ");
	 for (int i=0; i<=n;i++)
	 {
		  
		 printf("%d ",q[i]);
	 }
	 getch();
	   return 0;
     
}