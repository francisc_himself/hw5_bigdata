// constructia Bottom-Up are timpul de executie O(n)
//constructia Top-Down are timpul de executie O(nlg n)

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int A=0,C=0;
int heapsize,i;
FILE *pf;
int sir2[10000];

//genereaza un sir aleator
int random(int sir[], int n)
{
	for (int i=0; i<=n; i++)
	{ sir[i]=rand(); }
	return 0;
}

//genereaza un sir descrescator
int descresc(int sir[], int n)
{
	for (int i=0; i<=n-1; i++)
	{ sir[i]=n-i; }
	return 0;
}

//initializeaza heapul O(1)
void H_init(int sir[])
{
	heapsize=0;
}

int Parinte(int i)
{
	return i/2;
}

int Stanga(int i)
{
	return 2*i;
}

int Dreapta(int i)
{
	return 2*i+1;
}

void afisare(int n,int sir[])
{
    int i;
	for(i=1; i<=n; i++)
        printf("%d ",sir[i]);
}

void pretty_print(int sir[], int i, int depth)
{
	if (i<=heapsize)
	{
		pretty_print(sir, 2*i, depth+1);
		for (int j=0; j<=depth; j++)
			fprintf(pf," ");
		fprintf(pf, "%d", sir[i]);
		fprintf(pf, "\n");
		pretty_print(sir, 2*i+1, depth+1);
	}
}

//interschimba 2 elemente
void interschimb(int *x,int *y)
{
	int aux;
	aux=*x;
	*x=*y;
	*y=aux;
}

//reconstruiesc heapul astfel incat cel mai mare element
//va fi radacina heapului
void MaxHeapify(int sir[], int i, int *A, int *C)
{
	int l=Stanga(i);
	int r=Dreapta(i);
	int a=0, c=0;
	int max;
	int j=0;
	
	c++;
	if ((l<=heapsize) && (sir[l]>sir[i]))
	{max=l;}
	else {max=i;}
	c++;
	if ((r<=heapsize) && (sir[r]>sir[max]))
	{max=r;}
	if (max!=1)
	{
		interschimb(&sir[i], &sir[max]);
		a+=3;

		/*j++;
		fprintf(pf, "pasul %d\n", j);
		pretty_print(sir, 1, 0);
		fprintf(pf, "\n");*/
		afisare(10,sir);
		printf("\n");
		
		MaxHeapify(sir, max, &a, &c);
	}
	*A+=a;
	*C+=c;
}

//constructia heap bottom-up
void ConstrMaxHeap(int sir[], int lenght, int *A, int *C)
{
	int a=0, c=0;
	heapsize=lenght;
	for(int i=(lenght/2); i>=1; i--)
		{
			MaxHeapify(sir, i, &a, &c);
			afisare(10, sir);
			printf("\n");
		}
	*A=a;
	*C=c;
}

//O(nlg n)
void HeapSort(int sir[], int *A, int *C)
{
	int a=0, c=0;
	ConstrMaxHeap(sir, heapsize, &a, &c);
	for (int i=heapsize; i>=1; i--)
	{
		interschimb(&sir[i], &sir[1]);
		a+=3;
		heapsize=heapsize-1;
		ConstrMaxHeap(sir, 1, &a, &c);
	}
	*A+=a;
	*C+=c;
}

//H-Pop extrage elementul radacina din heap O(log2 n)
int HeapExtractMax(int sir[], int *A, int *C)
{
	int a=0;
	int	c=0;
	int max;
	while (heapsize>=1)
	{
		max=sir[1];
		sir[1]=sir[heapsize];
		heapsize--;
		MaxHeapify(sir, 1, &a, &c);
	}
	*A+=a;
	*C+=c;
	return max;
}

//H-Push insereaza un element in heap
void HeapIncreaseKey(int sir[], int i, int key, int *A, int *C)
{
	int a=0,c=0;
	sir[i]=key;
	a++;
	c++;
	while ((i>1) && (sir[Parinte(i)]<sir[i]))
	{
		c++;
		interschimb(&sir[i], &sir[Parinte(i)]);
		a+=3;
		i=Parinte(i);
	}
	*A+=a;
	*C+=c;
}

//constructia td O(nlg n)
void MaxHeapInsert(int sir[], int key, int *A, int *C)
{
	int a=0, c=0;
	heapsize++;
	sir[heapsize]=0;
	a++;
	HeapIncreaseKey(sir, heapsize, key, &a, &c);
	*A+=a;
	*C+=c;
}

int main()
{
	int atd=0, ctd=0, abu=0, cbu=0;
	int sir[10000];
	srand(time(NULL));
	H_init(sir);
	
	pf=fopen("Pretty.txt", "w");
	for (int i=1; i<=10; i++)
	{
		sir[i]=rand();
	}
	fprintf(pf, "Incepe constructia heap BU:\n");
	afisare(10, sir);
	printf("\n");
	ConstrMaxHeap(sir, i, &A, &C);
	afisare(10,sir);
	printf("\n");
	fclose(pf);

	/*pf=fopen("TDandBU_Constr.csv", "w");
	fprintf(pf, " n , BU , TD \n");
	for (int i=100; i<=10000; i+=100)
	{
		atd=0;
		ctd=0;
		abu=0;
		cbu=0;
		for (int k=1; k<=5; k++)
		{
			A=C=0;
			random(sir, i);
			for (int j=0; j<=i; j++)
				sir2[j]=sir[j];
			ConstrMaxHeap(sir,i,&A,&C);
            atd+=A;
            ctd+=C;

            MaxHeapInsert(sir2,i,&A,&C);
            abu+=A;
            cbu+=C;
		}
		fprintf(pf, "%d, %d, %d\n", i, (atd+ctd)/5, (abu+cbu)/5);
	}
	fclose(pf);*/

	getchar();
	return 0;
}