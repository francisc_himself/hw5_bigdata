//#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include  <stdlib.h>
#include <time.h>
#include<math.h>


int attr1=0,comp1=0,attr2=0,comp2=0;


void generator_aleator(int* v, int n)
{
	
	int i; //contor
	
	srand (time(NULL));
	for (i=0;i<n;i++)
	{
		
		*(v+i) = rand() % 100000;
	}

}


int index_min(int *a,int i,int nr_elem)
{
    if(2*i>nr_elem)
    {
        return i;
    }

    int min =a[i]<a[2*i]?i:(2*i);

    if (2*i+1>nr_elem) return min;
    min=a[min]<a[2*i+1]?min:(2*i+1);
    return min;
}

void Rec_H(int A[], int n, int i)//reconstructie heapului
{
	int ind=0,x,y,aux;
	x=2*i;
	y=2*i+1;
	ind=index_min(A,i,n);
	comp2+=2;

	if(ind!=i)
	{
		aux=A[i];
		A[i]=A[ind];
		A[ind]=aux;
		attr2+=3;
	Rec_H(A,n,ind);
	}
}


void quickSort(int arr[], int left, int right)
{
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
	  attr1++;
      /* partitionare */
      while (i <= j) {
            while (arr[i] < pivot && ++comp1)
                  i++;
            while (arr[j] > pivot && ++comp1)
                  j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
				attr1+=3;
                i++;
                j--;
            }
      };
      /* recursivitate */
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}

void afiseaza(int v[],int nr_elem,int i)
{
    if(i>nr_elem) return;

    int tab = i;

    afiseaza(v,nr_elem,2*i+1);

    while (tab>0){
        printf("   ");
        tab=tab/2;
    }
    printf("%d\n",v[i]);

    afiseaza(v,nr_elem,2*i);
}

/*void heapSort(int* x, int n) {
  int i, aux;

  for (i = (n / 2); i >= 0; i--) {
    Rec_H(x, n, i);
  }

  for (i = n; i >= 2; i--) {
    attr2+=3;
    aux = x[1];
    x[1] = x[i];
    x[i] = aux;
	n=n-1;
    Rec_H(x, i-1, 0);
  }

}*/

void Hsort(int a[], int n)
{
	int i, aux;
	for(i=n/2;i>=1;i--)
		Rec_H(a,n,i);

	for(i=n;i>=2;i--)
	{
		attr2+=3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		n=n-1;
		Rec_H(a,n,1);	
	}

}

int main()
{
	int v[10000], g[10000], heap[10000];
	int n, j,k;
	FILE *f;

	printf("n=");
	scanf("%d", &n);

	for(int i=1;i<=n;i++)
		{
			printf("\nv[%d]=", i);
			scanf("%d", &v[i]);
		}

	quickSort(v,1,n);


		
	for(int i=1;i<=n;i++)
		{
			printf("%d ", v[i]);
		}

/*
	f=fopen("fisier.csv", "w");
	fprintf(f, "n, atr_QS, comp_QS, atr+comp QS, atr HS, comp HS, atr+comp HS \n");

	for(n=100; n<=10000; n=n+100)
	{
		attr1=0;comp1=0;attr2=0;comp2=0;
		
		for(j=0;j<5;j++)
		{
			generator_aleator(v,n);
			for(int k=0; k<n; k++)
			{
				g[k]=v[k];
			}

			quickSort(v,1,n);
			Hsort(g,n);

		}
	fprintf(f, "%d, %d, %d, %d, %d, %d, %d \n", n, attr1/5, comp1/5, (attr1+comp1)/5, attr2/5, comp2/5, (attr2+comp2)/5);
	printf("%d\n",n);
	}*/

	getch();
}