#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"
#define SIZE 101
Profiler profiler("Merge");



typedef struct NodeT
{
	int data;
	struct NodeT *prev;
	struct NodeT *next;
} NodeT;

NodeT* heap[SIZE];
int indexes[SIZE];
int lastindex;
int aux[10001];
int currentSize;
int heapsize;


void swap(NodeT* arr[],int i,int j)
{
	profiler.countOperation("Merge-ass",currentSize,3);
	int aux = arr[i]->data;
	arr[i]->data = arr[j]->data;
	arr[j]->data = aux;
	aux = indexes[i];
	indexes[i] = indexes[j];
	indexes[j] = aux;
}

void maxHeapify(NodeT* arr[], int i)
{
	int largest;
	int l = 2*i;
	int r = 2*i+1;
	profiler.countOperation("Merge-comp",currentSize,1);
	if (l <= heapsize && arr[l]->data > arr[i]->data)
	{
		largest = l;
	}
	else
	{
		largest = i;
	}
	profiler.countOperation("Merge-comp",currentSize,1);
	if (r <= heapsize && arr[r]->data > arr[largest]->data)
	{
		largest = r;
	}
	if (largest != i)
	{
		swap(arr,i,largest);
		maxHeapify(arr,largest);
	}
}

void insertNext(NodeT* arr[])
{
	heapsize++;
	int i = heapsize;
	int parent = i/2;
	profiler.countOperation("Merge-comp",currentSize,1);
	while(i > 1 && arr[parent]->data < arr[i]->data)
	{
		profiler.countOperation("Merge-comp",currentSize,1);
		swap(arr,i,parent);
		i = parent;
		parent = i/2;
	}
}

NodeT* getMax(NodeT * arr[])
{
	if(heapsize < 1)
	{
		return NULL;
	}
	profiler.countOperation("Merge-ass",currentSize,2);
	NodeT * max = arr[1];
	lastindex = indexes[1];
	indexes[1] = indexes[heapsize];
	arr[1] = arr[heapsize];
	heapsize--;
	maxHeapify(arr,1);
	return max;
}

void buildHeapTD(NodeT* arr[],int n)
{
	for(int i = 1; i < n;i++)
	{
		insertNext(arr);
	}
}


NodeT* makeNode (int pdata)
{
	NodeT * p;
	p = (NodeT*) malloc(sizeof(NodeT));
	if (p == NULL)
	{
		printf("makeNode error: insuficient memory to create node");
		return NULL;
	}
	p->data = pdata;
	p->next = NULL;
	p->prev = NULL;
	return p;
}

NodeT* makeList()
{
	NodeT * p = (NodeT*) malloc(sizeof(NodeT));
	if (p == NULL)
	{
		printf("makeList error: insufitient memory to create node");
	return NULL;
	}
	p->data=0;
	p->next=p;
	p->prev=p;
	return p;
}


int insertnode(NodeT* L,NodeT* node)
{
	if (L == NULL || node == NULL)
	{
		printf("insertnode error: list or node does not exist");
		return 0;
	}
	node->next=L->next;
	node->prev=L;
	L->next->prev=node;
	L->next=node;
	L->data=L->data +1; 
	return 1;
}

NodeT * popLast(NodeT * L)
{
	if(L == NULL)
	{
		printf("popLast empty list");
		return NULL;
	}
	if(L->data == 0)
	{
		return NULL;
	}
	NodeT * result = L->prev;
	L->prev = result->prev;
	L->data=L->data - 1; 
	return result;
}

NodeT * makeOrderedList(int length)
{
	FillRandomArray(aux,length,0,500,false,1);
	NodeT * List = makeList();
	NodeT * node;
	for(int i = length-1;i >= 0;i--)
	{
		node = makeNode(aux[i]);
		insertnode(List,node);
	}
	return List;
}

void printList(NodeT * L)
{
	if(L == NULL)
	{
		printf("list doesnt exist");
		return;
	}
	NodeT * iterator;
	iterator = L->next;
	while(iterator != L)
	{
		if(iterator != NULL)
		{
			printf(" %d ",iterator->data);
			iterator = iterator -> next;
		}
		else
		{
			printf("error print");
		}
	}
	return;
}

NodeT * mergeLists(NodeT * lists[],int k)
{
	NodeT* result = makeList();
	NodeT * next;
	for(int i = 0;i < k;i++)
	{
		profiler.countOperation("Merge-ass",currentSize,1);
		heap[i+1] = popLast(lists[i]);
		indexes[i+1] = i;
	}
	buildHeapTD(heap,k+1);
	profiler.countOperation("Merge-comp",currentSize,1);
	while((next = getMax(heap)) != NULL)
	{
		profiler.countOperation("Merge-comp",currentSize,1);
		insertnode(result,next);
		profiler.countOperation("Merge-ass",currentSize,3);
		if((next = popLast(lists[lastindex])) != NULL)
		{
			indexes[heapsize+1] = lastindex;
			profiler.countOperation("Merge-ass",currentSize,1);
			heap[heapsize+1] = next;
			insertNext(heap);
		}
	}
	return result;
}

int main()
{
	NodeT * Lists[SIZE];
	NodeT * result; 
/*demo*/
	int n = 50;
	int k = 5;
	int listSize = n/k;
	for(int i = 0;i < k;i++)
	{
		Lists[i] = makeOrderedList(listSize);	
		printList(Lists[i]);
		printf("\n");
	}
	result = mergeLists(Lists,k);
	printList(result);
/*1st */
	/*for (int i = 100; i < 10000; i+=100)
	{
		listSize = i/k;
		currentSize = i; 
		for(int j = 0;j < k;j++)
		{
			
			Lists[j] = makeOrderedList(listSize);	
		}
		result = mergeLists(Lists,k);
		
		
	}*/
/*2nd*/
	/*for (int k = 10; k < 501; k+=10)
	{
		listSize = 10000/k;
		currentSize = k; 
		for(int j = 0;j < k;j++)
		{
			Lists[j] = makeOrderedList(listSize);	
		}
		result = mergeLists(Lists,k);
		
		
	}*/

	//profiler.addSeries("Merge-All","Merge-ass","Merge-comp");
	//profiler.showReport();
	getch();
}