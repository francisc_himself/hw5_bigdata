/*
In cazul mediu statistic numarul asignarilor este 
mai mic pentru algoritmul bottom up decat pentru algoritmul 
top down. Numarul comparatiilor este putin mai mare in cazul 
bottom up decat in cazul top-down. Cand calculam suma lor se observa
ca bottom up este mai eficient decat top down.

In cazul defavorabil diferenta intre cele doua metode creste,
observandu-se ca algoritmul top down este mult mai ineficeint 
decat constructia bottom up.
*/
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include "Profiler.h"
#define NMAX 20000

int a[NMAX],b[NMAX],c[NMAX], dim_heap, comp_bu, comp_td, ass_bu, ass_td;

void ReBuild(int a[NMAX],int i)
{
    int max,aux,l,r;
	l= 2*i;
	r= 2*i+1;
	comp_bu++;
	if(l<= dim_heap && a[l]>a[i])
		max = l;
	else
		max = i;
	comp_bu++;
	if(r<=dim_heap && a[r]>a[max])
		max = r;
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		ass_bu = ass_bu++;
		ReBuild(a,max);
	}
}

void bottom_up(int a[NMAX], int n)
{
	int i;
	comp_bu =0; ass_bu=0;
	dim_heap = n;
    for(i=n/2;i>=1;i--)
	{
		ReBuild(a,i);
		
	}
	
}
void insert_heap(int a[], int key)
{
	int i;
	dim_heap = dim_heap+1;
	i= dim_heap;
	comp_td++;
	while(i>1 && a[i/2] < key)
	{	
		comp_td++;
		ass_td ++;
		a[i] = a[i/2];
		i = i/2;
	}
	ass_td++;
	a[i] = key;
}

void top_down (int a[] , int n)
{
	int i;
	ass_td = 0; comp_td = 0;
	dim_heap = 1; 
	for(i=2; i<=n; i++)
		insert_heap(a,a[i]);
}




void pretty_print(int T[],int i,int adancime)
{
	if(i<=dim_heap){ 
	pretty_print(T,(2*i+1),(adancime+1));
	for(int j=1;j<=adancime;j++) printf("	");
		printf("%d", T[i]);
		printf("\n");
	pretty_print(T,2*i,(adancime+1));
	}
}

void copy(int A[], int B[], int n)
{
   for(int i=1;i<=n;i++)
	   B[i]=A[i];
}

void init(int a[], int n)
{
	for(int i=1;i<=n;i++)
		a[i]=0;
}

void print (int a[], int n)
{
	int i;
	for(i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
}

void main()
{
	int i, sum_compBU, sum_assBU, sum_assTD, sum_compTD,k;	
	FILE *f;
	//initializarea unui sir pentru exemplificarea algoritmilor
	c[1] =4;c[2]=1;c[3]=3;c[4]=2;c[5]=16;c[6]=9;c[7]=10;c[8]=14;c[9]=8;c[10]=7;
	print(c,10);
	copy (c,b,10);
	printf("\ntop_down:\n");
	//aplicarea algoritmului top_down
	top_down(c,10);
	print(c,10);
	pretty_print(c,1,0);
	print(b,10);
	printf("\nbottom_up:\n");
	//aplicarea algoritmului bottom_up
	bottom_up(b,10);
	print(b,10);
	pretty_print(b,1,0);
	//initializarea sirurilor cu care voi opera
	init(a,NMAX);
	init(b,NMAX);
	//deschiderea fisierul analizei
	f = fopen("analiza.csv","w");
	if(f==NULL)
	{
		perror("Error opening file!");
		exit(1);
	}
	//scrierea capului de tabel
	fprintf(f,"n, bottomUp_ass, bottomUp_comp, bottomUp_sum, topDown_ass, topDown_comp, topDown_sum\n");
	
	for(i=100;i<=10000; i=i+100)
	{
		sum_compBU=0; sum_assBU=0; sum_assTD=0; sum_compTD=0;
		for(k=1;k<=5;k++){
			//creearea unui sir aleator
			FillRandomArray(a,i,1,20000,true,0);
			copy(a,b,i);
			//aplicarea algoritmului bottom_up
			bottom_up(b,i);
			//calcularea sumelor
			sum_assBU = sum_assBU + ass_bu;
			sum_compBU = sum_compBU +comp_bu;
			init(b,i);
			copy(a,b,i);
			//aplicarea algoritmului top_down
			top_down(b,i);
			sum_assTD = sum_assTD + ass_td;
			sum_compTD = sum_compTD +comp_td;
		}
		fprintf(f,"%d, %d, %d, %d, %d, %d, %d\n",i, sum_assBU/5, sum_compBU/5,(sum_assBU+sum_compBU)/5, sum_assTD/5,sum_compTD/5,(sum_assTD+sum_compTD)/5);
	}
	printf("\n");
	//pregatirea capului de tabel pentru cazul nefavorabil
	fprintf(f,"\nn, bottomUp_ass, bottomUp_comp, bottomUp_sum, topDown_ass, topDown_comp, topDown_sum\n");
	init(a,NMAX);
	init(b,NMAX);
	for(i=100;i<=10000; i=i+100)
	{
		sum_compBU=0; sum_assBU=0; sum_assTD=0; sum_compTD=0;
		//crearea unui sir crescator
		FillRandomArray(a,i,1,20000,true,1);
		copy(a,b,i);
		bottom_up(b,i);
		init(b,i);
		copy(a,b,i);
		top_down(b,i);
		fprintf(f,"%d, %d, %d, %d, %d, %d, %d\n",i, ass_bu,comp_bu,ass_bu+comp_bu,ass_td,comp_td,ass_td+comp_td);
	}
	printf("succes");
	getch();
	fclose(f);

}
