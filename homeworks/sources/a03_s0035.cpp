/*
Atat HEAP-SORT cat si QUICK-SORT sunt de complexitate n*log n insa din graficele realizate pe baza datelor 
returnate de program reiese ca algoritmul Quick-Sort este de o complexitate mai mica decat Heap-Sort
Algoritmul Heap-Sort are la baza un MAX-HEAP si foloseste constructia Buttom-Up pentru a obtine maximum de eficienta
*/

#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int nr_at,nr_comp;        
#define NMAX 10000  





void copiere(int n,int *sir1,int *sir2) 
{
	int i;  
	for (i=0;i<n;i++)
		sir2[i]=sir1[i];
}

void creare_sir_m(int n,int v[])      //functie ce creeza un sir de lungime "n" cu elemente aleatoare
{
	int i;
	
	for(i=0;i<n;i++)
		v[i]=rand()%10000;

	                                  
}
 //Functie ce returneaza indicele fiului stang nodului i
int LEFT(int i)                     
{
  return 2*i+1;
}
//Functie ce returneaza indicele fiului drept a nodului i
int RIGHT(int i)                        
{
  return 2*i+2;
}




void heapify(int s[],int i,int hsize)       
{  int l=LEFT(i);                     
   int r=RIGHT(i);
   int great;
   nr_comp++;
   if((l<hsize)&&(s[l]>s[i]))
       great=l;
   else great=i;
   nr_comp++;
   if((r<hsize)&&(s[r]>s[great]))
	   great=r;
  
   if(great!=i)
   {    nr_at++;
	  int aux;
      aux=s[i];
	  s[i]=s[great];
	  s[great]=aux;
	  heapify(s,great,hsize);
   }


}

//constructie heap BoTTOM - UP
void buildHeapBU(int a[],int n,int *h_size) 
{
  (*h_size)=n;
  int i;
  for(i=(n/2);i>=0;i--)
       heapify(a,i,*h_size);
}

void HeapSort(int a[],int n)
{
  int i,k;
  int size=0;
  buildHeapBU(a,n,&size);
  for(i=n-1;i>=1;i--)
  {		
	  int aux;
		aux=a[0];
		a[0]=a[i];
		a[i]=aux;
	   nr_at++;
	 size--;
     heapify(a,0,size);
  }

}

void QuickSort(int prim,int ultim,int a[])
{
	int i,j;
	int x,pivot;
	i=prim;
	j=ultim;

	pivot=a[(prim+ultim)/2];nr_at++;

	do{

		while(a[i]<pivot)
		{i++;
		nr_comp++;
		}
		while(a[j]>pivot)
		{
		    nr_comp++;
		    j--;}
	if(i<=j){
			 x=a[i];
			 a[i]=a[j];
			 a[j]=x;
			 i++;
			 j--;
			 nr_at++;
		}

	}while(i<=j);
if(j>prim)
	QuickSort(prim,j,a);
if(i<ultim)
	QuickSort(i,ultim,a);

}
//Metoda main
int main()
{
   int n,i;
  FILE* pf;
  pf=fopen("mediu.xls","w");
 int sir1[NMAX],sir2[NMAX],a[100];
 int a2,c2,a3,c3;
 srand(time(NULL));

 fprintf(pf,"\n n\t HS_atribuiri\t  HS_comparatii \t   HS_atr+comp\t  QuickSort_atrib\t QuickSort_comp \t  QuickSort_atrib+comp\t ");
for(n=100;n<=NMAX;n+=100)    
  {                           
      a2=0;
	  c2=0;
	  a3=0;
	  c3=0;
	  printf("%d\n",n);
	  for(int k=0;k<5;k++)  //contribuie la realizarea unei medieri cu 5
	  {

         creare_sir_m(n,sir1);//Creare Sir
	      copiere(n,sir1,sir2);
      
	      nr_comp=0;
	      nr_at=0;
          HeapSort(sir2,n);
         
          a2=a2+nr_at;
		  c2=c2+nr_comp;

	      nr_comp=0;
	      nr_at=0;
	      copiere(n,sir1,sir2);
          QuickSort(0,n-1,sir2);
	     
          a3=a3+nr_at;
		  c3=c3+nr_comp;
	  }
	 
	  a2=a2/5;
	  c2=c2/5;
	  a3=a3/5;
	  c3=c3/5;
	  
       fprintf(pf,"\n%d  \t%d   \t%d   \t%d  \t%d \t%d  \t%d ",n,a2,c2,a2+c2,a3,c3,a3+c3);
       
     }
fclose(pf);
//**********************************************

printf("\n\nSirul inainte de sortare\n");
 
   for(int i=0;i<10;i++)
   {
	   a[i]=rand()%10000;
	}
for(i=0;i<10;i++)
  printf(" %d ",a[i]);
printf("\n \n \n");
printf("\n Dupa quicksort\n");

QuickSort(0,9,a);
for(i=0;i<10;i++)
  printf(" %d ",a[i]);

HeapSort(a,10);
 printf("\n Dupa heapsort\n"); 
for(i=0;i<10;i++)
  printf(" %d ",a[i]);

   
getch();
return 0;
}
   





