/*
 ***ANONIM*** ***ANONIM***-Daniela gr ***GROUP_NUMBER***
In cazul de fata am ales alfa={0.8,0.85,0.90,0.95,0.99}. Odata cu  cresterea lui alfa creste si efortul depus atat in
cazul gasirii elementelor cat si in celalalt caz.
 In cazul dispersiei uniforme, o tabela de dispersie cu adresare directa si factorul de umplere alfa n/m <1,
 numarul mediu de modificari intr-o cautare fara succes este de 1/(1-alfa), iar pt o cautare cu succes este de (1/alfa) ln 1/(1-alfa).
*/

#include<stdlib.h>
#include <time.h>
#include<conio.h>
#include<stdio.h>
#define hash_size 9973
#define search_size 3000
int efMaxG=0;
int efMedG=0;
int efMaxN=0;
int efMedN=0;
int m,n;
int op=0;
int efTotal;
int H[hash_size];
int found[hash_size/2];

	

int hash(int cheie, int i)
{

	int h=(cheie %hash_size+ i +i*i) % hash_size;
	return h;
}

int search(int *H, int cheie)
{
	
	int i=0,j;
  
	do
	{
    j=hash(cheie,i);
	op++;
    if (H[j]==cheie) 
	{
		return j;
	}
	     i=i+1;
	}
    while ((H[j]!=-1)&&(i!=hash_size));
	
	 return -1;
	
}
int Hash_Insert(int *H,int k)
{
	int i=0;
	int j;
	do{
		j=hash(k,i);
		if (H[j]==-1) 
		{
			 H[j]=k;
			return j;
		}
		else 
		
			i++;
			
		
	}
	while (i<hash_size);
	return -1;

}

void verificare_hash(int *H,int n,double alfa)
{
	int i,nr=0;
	
	m = (int)(n/alfa);


	for (i=0;i<m;i++)
	{
		H[i]=-1;
	
	}

	for (i=0;i<n;i++)
	{
		nr = (i+nr)%(5*n);
		Hash_Insert(H,nr);
	}
}
void afis_hash(int *H,int n)
{
	
	int i;

	for (i=0;i<m;i++)
		printf("%d\n",H[i]);

}


void test_fisier(double a)
{ 
	
	FILE *f;
	efTotal=0;
	int max1=0;
	int max2=0;
	int val;
	int gasit=0;
	int index=0;
	f=fopen("hash.txt","w");
    fprintf(f,"\n Alfa \t GaEfortMED\t GaEfortMAX\t NeEfortMED\t NeEfortMAX");
    for(int i=0;i<=hash_size;i++)
	  	H[i]=-1;
    for (int i=0; i<hash_size/2; i++)
		found[i]=0;
    for (int j=0; j < a * hash_size ; j++) 
		{
			val=rand()%1000;
            Hash_Insert(H,val);
            if ((j%5)==0)
				{
					found[index++]=val;
					
				}		
		}
	//cautare elemente care exista
	for (int i = 0; i < index; i++)
	{ 
		op=0;
		gasit=search(H, found[i]);
		efTotal=efTotal + op;
		if (op > max1)
		max1=op;
		
	}
	efMaxG=max1; 
	efMedG=efTotal/index;
	

	efTotal=0;
	//cautare elemente negasite
	for (int i=0; i < (search_size-index) ; i++)
		{
			op=0;
			int y=rand()+ 1000;
			gasit=search(H,y);
	       efTotal=efTotal + op;
	if (op > max2) 
		max2=op;
			
		}
	efMaxN=max2;
    efMedN=efTotal/(search_size-index); 


	fprintf(f,"\n %0.2f\t\t%d\t\t%d\t\t%d\t\t%d\n",a, efMedG, efMaxG, efMedN,efMaxN);
	fclose(f);

}

int main()
{

    srand(time(NULL));
	printf("Exemplu:\n ");
    double a =0.7;
    H[10];
     n=7;
     verificare_hash(H,n,a);
	search(H,1);
	afis_hash(H,n);
	int p=search(H,3);
	printf ("Returneaza valoarea cautata sau -1 daca nu este in tabela!\n");
	printf (" %d",p);
    test_fisier(0.99);
	
    getch();
	return 0;
}


