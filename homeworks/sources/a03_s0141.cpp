// ***ANONIM*** ***ANONIM***-Petre, ***GROUP_NUMBER***
// Analysis & Comparison of Advanced Sorting Methods�Heapsort and Quicksort

//Metodele heapsort si quicksort au aceeasi complexitate in cazul mediu statistic: O(n log n)



#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <algorithm> 

#define NMAX 10000
int A[NMAX];
int B[NMAX];

char nume_fis1[50]="D:\\quicksort.txt";
char nume_fis2[50]="D:\\heapsort.txt";

FILE *f1, *f2;

int dim_heap;

void swap (int* a, int* b)
{
	int aux;
	aux = *a;
	*a = *b;
	*b = aux;
}

void genereazaSirRandom(int n,int a[])
{
	int ok=1,x;
	srand(time(NULL));
	for(int j=1;j<=n;j++)
	{
		x=rand()%n;
		a[j]=x;
	}
}


//Quicksort
int partition(int A[], int p, int r, int& atr, int& comp)
{
	int i = p - 1;
	int x = A[r];
	int j;
	for(j=p; j<=r-1; j++)
	{
      comp++;
	  if (A[j] <=x) 
		{
			i++;
			atr+=3;
			swap(&A[j], &A[i]);
	    }
	}
	  atr+=3;
	  swap(&A[i+1], &A[r]);
	  return i+1;
}

void quicksort(int A[], int p, int r, int &atr, int &comp)
{
	int q;
	if (p<r)
	{
	   q = partition(A, p, r, atr,comp);
	   quicksort(A,p,q-1, atr, comp);
	   quicksort(A,q+1,r, atr, comp);
	}
}


//Heap-sort

void maxHeapify(int* a, int i, int dimensiune,int& atr, int& comp )
{   
	int aux,maxim;
	int fiu_stang = 2*i;
	int fiu_drept = 2*i+1;
	comp+=2;
	if (fiu_stang < dimensiune && a[fiu_stang] > a[i])
		maxim = fiu_stang; 	
	else 
		maxim=i;
	if (fiu_drept < dimensiune && a[fiu_drept] > a[maxim]) 
	maxim = fiu_drept;
	comp++;
	if(maxim!=i)
	{   
		aux=a[i];
		a[i]=a[maxim];
		a[maxim]=aux;
		atr+=3;
		maxHeapify(a,maxim,dimensiune,atr,comp);
	} 
}   

void insert(int *a,int cheie, int &dim_heap, int &comp, int &attr)
{
	dim_heap++;
	int i=dim_heap;
	comp++;
	while(i>1 && a[int(i/2)] < cheie)
	{	comp++; attr+=2;
		a[i]=a[int(i/2)];
		i=i/2;
		a[i]=cheie;
	}

}

void bottomUp( int* a, int dimensiune,int& atr, int& comp )
{   
	int i;
	for (i = dimensiune/2; i >= 1; i--)
		maxHeapify(a, i, dimensiune,atr,comp);

}   

void topDown( int* a,int dimensiune,int& atr, int& comp )
{   
	dim_heap=1;
	for(int j=2;j<=dimensiune;j++)
	{
		insert(a,a[j],dim_heap,comp,atr);
	}
} 






void heapsort(int A[], int n, int &atr, int &comp)
{


    topDown( A, n, atr, comp);
	for(int i = n ;i>=2 ;i--)
	{
		swap(&A[1], &A[i]);
		dim_heap = dim_heap-1;
	    maxHeapify(A,1,dim_heap,atr,comp);
	}
}
void scrieInFisier(FILE *f,int n, int atr, int comp)
{

	fprintf(f,"%d,%d\n",n,atr+comp);

}

void main()
{
	int atr=0;
	int comp=0;
	
	f1=fopen(nume_fis1,"w");
	f2=fopen(nume_fis2,"w");

	fprintf(f1,"n,na+nc\n");
	fprintf(f2,"n,na+nc\n");
	
	int atr1=0;
	int comp1=0;
	int atr2=0;
	int comp2=0;
	
	for(int n=100;n<=NMAX;n+=200)
		{
		

			for (int k=1; k<=5; k++)
			{
				genereazaSirRandom(n,A);
							atr = 0;
		                	comp =0;
				for(int j=1; j<=NMAX; j++)
					B[j] = A[j];

			quicksort(A, 1, n, atr, comp);

			atr1=atr1+atr;
			comp1=comp1+comp;

			atr=0;
			comp=0;

			heapsort(B, n, atr, comp);
			atr2+=atr;
			comp2+=comp;




			}
			scrieInFisier(f1, n, atr1/5, comp1/5);
			scrieInFisier(f2, n, atr2/5, comp2/5);

			atr1=0;
			atr2=0;
			comp1=0;
			comp2=0;

	}
    
	genereazaSirRandom(10, A);
		for (int i=1; i<=10; i++)
		printf("%d ", A[i]);
	    printf("\n");
	heapsort(A, 10,atr, comp);
		for (int i=1; i<=10; i++)
		printf("%d ", A[i]);
	    printf("\n");
	
		
    
	genereazaSirRandom(10, A);
	for (int i=1; i<=10; i++)
		printf("%d ", A[i]);
	    printf("\n");
	quicksort(A, 1, 10, atr, comp);
		for (int i=1; i<=10; i++)
		printf("%d ", A[i]);
	
	fclose(f1);
	fclose(f2);

	getch();
}


