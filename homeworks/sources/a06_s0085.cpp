#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<iostream>

using namespace std;

int n, m;
int joselist[10], cnt = 0;

typedef struct nod
{
    int key;
    int dimensiune;
    nod *left, *right, *parinte;
} NOD;
NOD* rad;

void pretty_print( NOD *p, int nivel )
{

  if ( p!=NULL )
  {

		 pretty_print( p->left, nivel + 1 );
		 for( int i = 0; i <= nivel; i++ )
			 cout<<"\t";
		 cout<<p->key<<endl;
		 pretty_print (p->right, nivel + 1 );
	}
}


NOD* OS_SELECT( NOD *x, int i )
{
    int r = 1;
    if ( x->left != NULL )							//Cautam nodul pe care trebuie sa il stergem in dreapta si stanga radacinii
        r = x->left->dimensiune + 1;					
    else
        r = 1;
    if ( r == i )
        return x;
    else
    {
        if ( i < r )
        {
            return OS_SELECT( x->left, i );
        }
        else
        {
            return OS_SELECT( x->right, i - r );
        }
    }
}

NOD* GENERARE( int x, int dim, NOD *stg, NOD *dr )
{
    NOD *q;
    q = new NOD;
    q->key = x;					//q este noul nod si primeste valoare x
    q->left = stg;				//se creeaza fiu stang
    q->right = dr;				//se creeaza fiu dreapta
    q->dimensiune = dim;		//se adauga dimensiunea

    if ( stg != NULL )
    {
        stg->parinte = q;		//se fac legaturile parinte-fiu
    }
    if ( dr != NULL )
    {
        dr->parinte = q;
    }
    q->parinte = NULL;			//q este radacina si nu are parinte
    return q;
}

NOD* CONSTRUCTIE_EC( int stg, int dr )
{
    NOD *ds, *d;
    if ( stg > dr )
        return NULL;
    if ( stg == dr )
        return GENERARE( stg, 1, NULL, NULL );		//se genereaza arborele pentru partea stanga
    else
    {
        int mij = (stg + dr ) / 2;
        ds = CONSTRUCTIE_EC( stg, mij-1 );
        d  = CONSTRUCTIE_EC ( mij+1, dr );
        return GENERARE( mij, dr-stg+1, ds, d );
    }
}


NOD* MINIM( NOD* x )
{
    while( x->left != NULL )
    {
        x = x->left;

    }
    return x;						//se returneaza nodul din stanga cel mai jos
}

NOD* SUCCESOR( NOD* x )
{
    NOD *y;
    if( x->right != NULL )						//se cauta succesorul in dreapta
    {
        return MINIM( x->right );				//daca mai exista in dreapta fii se cauta minimul cel mai de jos
    }

    y = x->parinte;								//y primeste parintele nodului daca nu se intra in cautarea de mai sus
    while ( y != NULL && x == y->right )		//daca y nu este nul si x este egal cu fiul dreapta al lui y
    {
        									//x devine y
        x = y;								//se trece la urmatorul nod, urcand in sus
        y = y->parinte;
    }
    return y;								//se returneaza succesorul
}

NOD* STERGERE( NOD* z )
{
    NOD *y;
    NOD *x;

    if ( ( z->left == NULL ) || z->right == NULL )		//se verifica daca nodul este frunza
    {

        y = z;
    }
    else
        y = SUCCESOR(z);							//daca nu se cauta succesorul lui

    if ( y->left != NULL )							//daca succesorul are fiu stanga
    {
        x = y->left;
    }
    else
    {
                                            //succesorul are fiu dreapta
        x = y->right;
    }
    if ( x != NULL )
    {
        x->parinte = y->parinte;
    }
    if ( y->parinte == NULL )
        rad = x;
    else if ( y == y->parinte->left )
    {
        y->parinte->left = x;
    }
    else
    {
        y->parinte->right = x;
    }

    if ( y != z )
    {

        z->key = y->key;
    }
    return y;
}

void DIMENSIUNE_NOUA( NOD *p )
{
    while ( p != NULL )
    {

        p->dimensiune--;			//pentru fiecare nod se scade dimensiunea cu 1 incepand cu cel
									//ce s-a pus pe pozitia celui eliminat sau pe pozitia noua
        p=p->parinte;

    }
}

void afisare( int joselist[], int cnt )
{
    cout<<"\nNumerele eliminate: \n";
    for ( int i = 0 ; i < cnt; i++ )
        cout<<joselist[i]<<" ";
}

void JOSEPHUS( int n, int m )
{
    NOD *y, *z;
    int aux = n + 1;
    int mij = m;

    rad = CONSTRUCTIE_EC( 1, n );		//se construieste arborele pentru fiecare lungime 1,n
    cout<<"\nArborele este: \n";
    pretty_print( rad, 0 );
    for ( int i = 1; i < aux; i++ )	//se parcurge de fiecare data pana la aux care va
                                    //scadea de fiecare data cand se elimina un element
    {
        y = OS_SELECT( rad, m );	    //se selecteaza nodul pentru eliminare
        joselist[cnt] = y->key;
        cnt++;
        afisare( joselist, cnt );
        z = STERGERE(y);			//se elimina nodul
        cout<<"\nArborele este: \n";
        pretty_print( rad, 0 );
        DIMENSIUNE_NOUA(z);			//se reface dimensiunea tuturor nodurilor incepand de la
                                    //cel eliminat
        delete(z);					//se elibereaza memoria
        n--;						//decrementam lungimea arborelui
        if ( n > 0 )					//se genereaza urmatoarea pozitie de eliminat
            m = ( m - 1 + mij ) % n;
        if ( m == 0 )
            m = n;
    }

}



int main()
{
    JOSEPHUS(7,3);
	getch();

}
