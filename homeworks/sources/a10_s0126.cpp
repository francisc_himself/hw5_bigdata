#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
#include<iostream>
#include<fstream>
#include "Profiler.h"

Profiler profiler;

struct nod{
	int v;
	nod *urm;
};

nod * prim[60001], *ultim[60001];
int c[60001],p[60001],d[60001],f[60001],t;
int cnt;

void generate_graph(int n,int nr)
{
	int i,u,v;
	for(i=1;i<=n;i++)
		prim[i]=ultim[i]=NULL;
	for(i=1;i<=nr;i++)
	{
        do{
		u=rand()%n+1;
		v=rand()%n+1;
        }while(u==v);
		if(prim[u]==NULL)
		{
			prim[u]=new nod;
			prim[u]->v=v;
			prim[u]->urm=NULL;
			ultim[u]=prim[u];
		}
		else {
			nod *p;
			p=new nod;
			p->v=v;
			p->urm=NULL;
			ultim[u]->urm=p;
			ultim[u]=p;
		}
	}
}

void dfs_visit(int u, int size)
{
	c[u]=1;
	d[u]=t;
	t++;
	nod *x;
	int v;
	x=prim[u];
	profiler.countOperation("operations", size);
	while(x!=NULL)
	{
		v=x->v;
		x=x->urm;
		profiler.countOperation("operations", size);
		if(c[v]==0)
		{
			p[v]=u;
			profiler.countOperation("operations", size);
			dfs_visit(v,size);
		}
	}
	c[u]=2;
	f[u]=t;
	t=t+1;
}

void initializare(int n)
{
	t=0;
	cnt=0;
	int i;
	for(i=1;i<=n;i++)
	{
		f[i]=0;
		d[i]=0;
		p[i]=0;
		c[i]=0;
	}
}

void dfs(int n,int size)
{
	int u;
	for(u=1;u<=n;u++)
	{
		c[u]=0;
		p[u]=0;
	}
	for( u=1;u<=n;u++)
	{
		profiler.countOperation("operations",size);
		if(c[u]==0)
		{
			dfs_visit(u,size);
		}
	}
}

int main()
{

int n,i,nr;
srand(time(NULL));
//for(n=1000;n<=5000;n=n+100)
//{
//	nr=100;
//	generate_graph(n,nr);
//	initializare(n);
//	dfs(n,n);
//}
//profiler.showReport();

//for(nr=110;nr<=200;nr=nr+10)
//{
//	n = 9000;
//	generate_graph(n,nr);
//	initializare(n);
//	dfs(n,nr);
//}
//profiler.showReport();
return 0;
}
