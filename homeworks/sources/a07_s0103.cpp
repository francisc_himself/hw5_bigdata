/*
author:***ANONIM***�si ***ANONIM***
  group:***GROUP_NUMBER***
  date:15.05.2013
  program description : the program performs transformations on trees from one representation to another.
  The first transformation is from parent representation to multiway tree representation and the second one from multiway to binary tree representation,
  where the lesft pointer points to a child and the right to a brother.
  The program runs in linear time, every node is accessed only once.
  */
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>

typedef struct node
{
	struct node *next;
	int key;
}Node;

typedef struct tree
{
	struct tree *right;
	struct tree *left;
	int key;
}Tree;

int t1[100]={2,7,5,2,7,7,-1,5,2} ;
Node t2[100];
Tree* t3;
int n,head;


void T2()
{
	int i = 0;

	for (i=0;i<n;i++)
		t2[i].next=NULL;

	for (i=n-1;i>=0;i--)
	{
		Node *p = new Node;
		p -> key = i;
		p -> next = NULL;
		if (t1[i]!=-1)
		{
			p->next=t2[t1[i]-1].next;
			t2[t1[i]-1].next=p;
		}
		else
		{
			head=i;
		}
	}
}

void T3(Tree* T)
{
	Node* p = t2[T -> key].next;
	if (p!=NULL)
	{
		Tree* t= new Tree;
		t -> key = p->key;
		t -> left = NULL;
		t -> right = NULL;
		T -> left = t;
		T3(t);
		p=p->next;
		while(p!=NULL && p->key!=-1)
		{
			Tree* nw = new Tree;
			nw -> key = p -> key;
			nw -> left = NULL;
			nw -> right = NULL;
			t -> right = nw;
			t = t -> right;
			T3(t);
			p = p -> next;
		}
	}
	else
	{
		T->left=NULL;
	}
}

void pretty_print(int start,Tree *t)
{
	for (int i=0;i<start;i++)
		printf(" ");
	printf("%d\n",t->key+1);
	if (t->left!=NULL)
		pretty_print(start+4,t->left);
	if (t->right!=NULL)
	{
		pretty_print(start,t->right);
	}

}

int main()
{
	n=9;
	T2();
	t3 = new Tree;
	t3 -> key = head;
	t3 -> left = NULL;
	t3 -> right = NULL; 
	T3(t3);
	pretty_print(0,t3);
	getch();
	return 0;
}
