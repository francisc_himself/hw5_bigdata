/*
		Una dintre cele mai usoare teme din cauza faptului ca a fost discutata si inceputa la laborator iar algoritmi au fost
	implementati in cartea lui Cormen. Partea mai dificila pe care am intampianta a fost initializarea nodurilor daca respectam
	pseudocodul dinc arte dar am folosit ideea lui Radu Alexandru de a face o functie separata
		Eficienta programului , ca si a oricarui program ce lucreaza cu structuri de grafuri, este de O(|V|+|E|)
	Student : ***ANONIM*** ***ANONIM*** - ***ANONIM***
	grupa : ***GROUP_NUMBER***
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include "Profiler.h"

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define V_MAX 10000
#define E_MAX 60000
#define inf 32767

int count;
int tail;
int head;
int lungimeQ;

typedef struct{
	int key;
	int parent;
	int distance;
	int color;
}GRAPH_NODE;

typedef struct _nod{
	int key;
	struct _nod *next;
}nod;

GRAPH_NODE Q[E_MAX];
nod  *adj[V_MAX];
GRAPH_NODE graph[V_MAX];
int edges[E_MAX];

void generate(int n,int m)
{
	memset(adj,0,n*sizeof(nod*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		nod *p=(nod*)malloc(sizeof(nod));
		p->key=b;
		p->next=adj[a];
		adj[a]=p;
	}
}

void initQ()
{
	tail=0;
	head=0;
	lungimeQ=0;
}

//Carte Cormen
void ENQUEUE( GRAPH_NODE x)
{
	count++;
	Q[tail]=x;
	tail++;
	lungimeQ++;
}

//Carte Cormen
GRAPH_NODE DEQUEUE()
{
	count++;
	lungimeQ--;
	GRAPH_NODE x=Q[head];
	head++;
	return x;
}

//idee primita de la Radu Alexandru
void initWhite(int n,int j)
{
	for(int i=0;i<n;i++)
	{ 
		if(i!=j)
		{
			graph[i].color=WHITE;
			graph[i].distance=inf;
			graph[i].parent=0;
		}
	}
}
///

//Carte Cormen
void BFS(int j)
{
	count++;
	graph[j].color=GRAY;
	graph[j].distance=0;
	graph[j].parent=0;
	initQ();
	ENQUEUE(graph[j]);
	while(lungimeQ!=0)
	{
		count++;
		GRAPH_NODE u=DEQUEUE();
		nod *v=adj[u.key];
		while(v!=NULL)
		{	
			count++;
			if (graph[v->key].color==WHITE)
			{
				graph[v->key].color=GRAY;
				graph[v->key].distance=u.distance+1;
				graph[v->key].parent=u.key;
				ENQUEUE(graph[v->key]);
				count++;
			}
			if(v!=NULL)v=v->next;
		}
		u.color=BLACK;
		graph[u.key]=u;
		count++;
	}
}

int main()
{	
	///incepere testare partiala
	int n=5;
	int m=7;
	generate(n,m);
	for(int i=0;i<n;i++)
	{	
		printf("\n%d",i);
		nod *v=adj[i];
		while(v!=NULL)
		{
			printf("->%d ",v->key);
			v=v->next;
		}
	}
	for(int i=0;i<V_MAX;i++)
		graph[i].key=i;
	initWhite(n,0);
	for(int i=0;i<n;i++)
		if(graph[i].color==WHITE)
			BFS(i);
	printf("\nAfisare graf BFS\n");
	for (int i=0;i<n;i++)
	{
		printf("%d -> %d culoare: %d d=%d\n",graph[i].parent,graph[i].key,graph[i].color,graph[i].distance);
	}
    /////final testare partiala


	FILE *f =fopen("iesireBFS.csv","w");
	
	fprintf(f,"Muchii,count\n");
	for (int numarMuchii=1000;numarMuchii+1 <6000;numarMuchii+=100)
	{
		generate(100,numarMuchii);
		for(int i=0;i<V_MAX;i++)
			graph[i].key=i;
		initWhite(100,0);
		count=0;
		for(int i=0;i<100;i++)
			if(graph[i].color==WHITE) 
				BFS(i);	
	 	fprintf(f,"%d,%d\n",numarMuchii,count);
	}

	fprintf(f,"\n\n");

	fprintf(f,"Nod,count\n");
	for (int numarN=100;numarN <200;numarN+=10)
	{
		generate(numarN,9000);
		for(int i=0;i<V_MAX;i++)
		graph[i].key=i;
			initWhite(numarN,0);
		count=0;
		for(int i=0;i<numarN;i++)
			if(graph[i].color==WHITE) 
				BFS(i);	
	 	fprintf(f,"%d,%d\n",numarN,count);
	}
	return 0;
}