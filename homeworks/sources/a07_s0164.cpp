/*
Student:						***ANONIM*** ***ANONIM***
Grupa:						    ***GROUP_NUMBER***
Cerinta:				
Implementati corect si eficient intr-un timp liniar urmatoarele transformari intre diferitele
reprezentari ale unui arbore multi-cai
-reprezentare prin vectori de parinti(tati)
-reprezentare multi-way
-reprezentare prin arbore binar

Implementati 2 functii de transformare intre primele 2 reprezentari si intre ultimele 2 reprezentari
pentru a 2-a transformare afisati in preordine arborele rezultat

-transformarile ruleaza in O(n)




*/
#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Multi_node {
	int key;//cheia nodului
	int count;//numarul de copii
	struct Multi_node *child[50];//copii nodului
}MN;//structura de tip multi-way

typedef struct Binary_way {
	int key;//cheia nodului
	struct Binary_way *left, *right;// descendent dreapta, respectiv stanga

}BW;//structura de tip binara

MN *createMN(int key)//functie de creare si initializare a unui nod dintr-un arbore multi-cai
{
	MN *x = (MN*) malloc(sizeof(MN));//alocam memorie
	x->key=key;
	x->count=0;

	return x;
}


void insertMN(MN *parent, MN *child)//functie de inserare datelor legate de descendentii unui nod in arbore multi-cai
{
	parent->child[parent->count]=child;
	parent->count++;
}


MN *transf(int t[], int size)//prima dunctie de transformare dintr-un vectori de tati, intr-un arbore multi-cai
{
	MN *root;
	MN *nodes[50];

	for(int i=0; i<size; i++)
		nodes[i]=createMN(i);

	for(int i=0;i<size;i++)
		if (t[i]!=-1)
			insertMN(nodes[t[i]], nodes[i]);
		else
			root=nodes[i];

	return root;
}

BW* createBW(int key)
{
	BW *p=(BW *)malloc(sizeof(BW));
	p->key=key;
	p->right=NULL;
	p->left=NULL;
	return p;
}
//functie de transformare din arbore multi-no in arbore binar
//primul copil al nodului devine descendentul stanga, iar restul de copii devin descendenti dreapta al nodului din stanga
BW * transform2(MN * root)
{
	BW *p;
	BW *rootBW =createBW(root->key);

	if(root->count == 0)
	{
		return rootBW;
	}

	rootBW->left= transform2(root->child[0]);// descendentul stanga este primul copil
	
	p = rootBW->left;

	for(int i = 1; i < root->count;i++)
	{
		p->right = transform2(root->child[i]);//descendentii dreapta, restul de copii
		p=p->right;
	}

	p->right = 0;

	return rootBW;
}

//afisare frumoasa pentru arbore multi-cai
void prettyPrint(MN *root,int nivel=0)
{
	for(int i=0;i<nivel;i++)
	{
		printf("    ");
	}
	printf("%d\n",root->key);
	for(int i=0;i<root->count;++i)
	{
		prettyPrint(root->child[i],nivel+1);
	}

}
//afisare frumoasa pentru arbore binar 
void prettyPrint3(BW *root,int nivel=0)
{

	int niv;
	if(root!= NULL)
	{
		niv =nivel;
		
		while(niv >0)
		{
			printf("    ");
			niv --;
		}

		printf("%d \n",root->key);
				
		prettyPrint3(root->left,nivel+1);
		prettyPrint3(root->right,nivel);	
	}
}

//afisare frumoasa pentru arbore binar
void prettyPrint2(BW *root,int nivel=0)
{

	if(root!=NULL)
	{
		prettyPrint2(root->right,nivel+1);
		
for(int i=0;i<nivel;i++)
		{
			printf("    ");	
		}
		printf("%d\n",root->key);
		prettyPrint2(root->left, nivel+1);
	}
}
int main()
{
	
	int t[] = {9,5,5,9,-1,4,4,5,5,4,2};
	int size = sizeof(t)/(sizeof(t[0]));
	MN* rootMN;
	BW *rootBW;
	rootMN = transf(t,size); 
	prettyPrint(rootMN,0);
	rootBW = transform2(rootMN);
	printf("\n\n");
	prettyPrint2(rootBW,0);
	printf("\n\n");
	prettyPrint3(rootBW,0);
	getch();
}


