#include <stdio.h>
#include<vector>
#define n 100010
 
using namespace std;
 
vector<int> g[n];
 
int line[n], dist[n], ***ANONIM***[n], start1, end1, actual,N,M,S;
int i,x,y;
 
int main()
{
    freopen("bfs.in","r",stdin);
    scanf ("%d %d %d",&N,&M,&S);
    for (i=1;i<=N;i++)
        dist[i]=-1;
     
    for (i=1;i<=M;i++)
        {
            scanf("%d %d",&x,&y);
            g[x].push_back(y);
        }
    line[1]=S;***ANONIM***[S]=1;dist[S]=0;start1=1;end1=1;
    while (start1<=end1)
    {
    actual=line[start1];
    for (i=0;i<g[actual].size();i++)
    if (***ANONIM***[g[actual][i]]==0)
    {
    line[++end1]=g[actual][i];
    dist[g[actual][i]]=dist[actual]+1;
    ***ANONIM***[g[actual][i]]=1;
    }
    start1++;
    }
    freopen("bfs.out","w",stdout);
    for (i=1;i<=N;i++)
        printf ("%d ",dist[i]);
 
    return 0;
}