//***ANONIM*** ***ANONIM*** Melania
//Grupa ***GROUP_NUMBER***
//BFS:

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler profiler ("tema9");

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define V_MAX 10000
#define E_MAX 60000
#define infinit 100000

typedef struct GRAPH_NODE {
	int distance;
	int color;
	int parent;
}GRAPH_NODE;

typedef struct _NODE{
	int key;
	struct _NODE *next;
}NODE;

/*typedef struct{
	//int length; 
	int key;
	//struct Q_NOD * next;
	//int key;
}Q_NOD;*/

NODE * adj[V_MAX];
GRAPH_NODE graph[V_MAX];
int edges[E_MAX];
int tail,head,length=V_MAX;
//GRAPH_NODE * u,*v;
int n,m,u;
GRAPH_NODE g[V_MAX];
int s,op;

void generate(int n,int m)
{
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE *nod=new NODE;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;
	}
}

void ENQ(int  Q[], int x)
{
	Q[tail]=x;
	if(tail==length)
		tail=1;
	else tail=tail+1;
	profiler.countOperation("BFS",op,1);
	
}

int DEQ(int  Q[])
{
	int x=Q[head];
	if(head==length)
		head=1;
	else head=head+1;
	return x;
	profiler.countOperation("BFS",op,1);
}

void BFS(int s)
{
	for (int i=0;i<n;i++)//each vertex u din G.V-{s} n=nr vf
	{
		if(i!=s)
		{
		//u=adj[i]->key;
		g[i].color=WHITE;
		g[i].distance=infinit;
		g[i].parent=-1;
		profiler.countOperation("BFS",op,3);
		}
	}
	g[s].color=GRAY;
	g[s].distance=0;
	g[s].parent=-1;
	profiler.countOperation("BFS",op,3);

	int Q[V_MAX];
	memset(Q,0,n*sizeof(NODE*));//cate elem?
	ENQ(Q,s);
	while(head<tail)//while(Q!=0)
	{
		profiler.countOperation("BFS",op,1);
		//int u=adj[s];
		int u=DEQ(Q);
		//for(int v=0;v<m;v++) //each v din G.Adj[u]
		/*if(adj[u]->next!=NULL)
		{	
			adj[u]=adj[u]->next;*/
			while(adj[u]!=NULL)
			{
				profiler.countOperation("BFS",op,1);
				int v=adj[u]->key;
				if (g[v].color==WHITE)
				{
					g[v].color=GRAY;
					g[v].distance=g[u].distance+1;
					g[v].parent=u;
					ENQ(Q,v);
					profiler.countOperation("BFS",op,3);
				}
				adj[u]=adj[u]->next;
				profiler.countOperation("BFS",op,1);
			}
	//	}
		g[u].color=BLACK;
		profiler.countOperation("BFS",op,1);
		
	}
//afisam noul graf:
	/*printf("Noul graf: \n");
	for(int i=0;i<n;i++)
	{
		printf("%d %d %d \n",g[i].color,g[i].distance,g[i].parent);
	}
	printf("\n");*/
}

void afisare(GRAPH_NODE *s)
{

}

void afisareAdj(NODE * ad[])
{
	for(int i=0;i<n;i++)
	{
		while(ad[i]!=NULL)
		{
			printf("%d ,",ad[i]->key);
			ad[i]=ad[i]->next;
		}
		printf("\n");
	}

}

void main()
{
	head=0;
	tail=0;
	n=7;
	m=7;
	op=0;
	// a. n=100; m variaza intre 1000 su 5000 din 100 in 100;op incepe= m
	//b. m= 9000 si n variaza intre 100 si 200 din 10 in 10; op incepe= n
	//a:
/*	for( m=1000;m<5000;m+=100)
	{
		op=m;
		n=100;
		generate(n,m);
	//	afisareAdj(adj);
		for (int i=0;i<n;i++)
		{
			g[i].color=WHITE;
			g[i].distance=infinit;
			g[i].parent=-1;
		}
	
		s=0;
		for(int j=0;j<n;j++)
		{
			head=0;tail=0;
			if(g[j].color==WHITE)
			BFS(j);
		}
	}*/
	
	//b:
	for( n=100;n<200;n+=10)
	{
		op=n;
		m=9000;
		generate(n,m);
	//	afisareAdj(adj);
	for (int i=0;i<n;i++)
		{
			g[i].color=WHITE;
			g[i].distance=infinit;
			g[i].parent=-1;
		}
	
		s=0;
		for(int j=0;j<n;j++)
		{
			head=0;tail=0;
			if(g[j].color==WHITE)
			BFS(j);
		}
	}
	profiler.showReport();
//	BFS(s);

}