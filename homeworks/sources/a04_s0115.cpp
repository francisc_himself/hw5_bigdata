
#include "stdio.h"
#include "conio.h"
#include "Profiler.h"
#include "stdlib.h"
#include "stddef.h"


#define MAX_NR 10000
#define inf 999999
#define MAX_HEAP 500

#define left(node) (2*node)
#define right(node) (2*node+1)
#define parent(node) (node/2)

int a[MAX_NR];


int size = 5;
int nbLists;
int heapSize;


Profiler profiler("MergingKSeq");


typedef struct nodetype
{
	int belongsTo;
	int value;
	struct nodetype *next;
}NodeT;

typedef NodeT *NodePtr;
NodePtr first[MAX_HEAP+1];
NodePtr last[MAX_HEAP+1];

#define SWAP(x,y) NodePtr t;t=x;x=y;y=t;

NodePtr heap[MAX_HEAP];


void append(int value, int k);
void append(NodePtr node, int k);

// Heap operations
void heapify(int index)
{
	int smallest = 0;
	int left = left(index);
	int right = right(index);
	profiler.countOperation("mergeComp",size,1);
	if((left <= heapSize) && (heap[left]->value < heap[index]->value))
	{
		smallest = left;
	}
	else
	{
		smallest = index;
	}
	profiler.countOperation("mergeComp",size,1);
	if((right <= heapSize) && (heap[right]->value < heap[smallest]->value))
	{
		smallest = right;
	}
	if (smallest != index)
	{
		profiler.countOperation("mergeAssign",size,3);
		SWAP(heap[index],heap[smallest]);
		heapify(smallest);
	}
}

NodePtr pop()
{
	NodePtr min = 0;
	if (heapSize < 1)
	{
		printf("Error. Heap underflow.\n");
		return NULL;
	}
	min = heap[1];
	profiler.countOperation("mergeAssign",size,1);
	heap[1] = heap[heapSize];
	heapSize--;
	heapify(1);
	return min;
}

void increaseKey(int index, NodePtr node)
{
	profiler.countOperation("mergeComp",size,1);
	while((index > 1) && (heap[parent(index)]->value > node->value))
	{
		heap[index] = heap[parent(index)];
		index = parent(index);
		profiler.countOperation("mergeComp",size,1);
		profiler.countOperation("mergeAssign",size,1);
	}
	profiler.countOperation("mergeAssign",size,1);
	heap[index] = node;
	return;
}

void push(NodePtr node)
{
	if(node != NULL)
	{
	heapSize++;
	profiler.countOperation("mergeAssign",size,1);
	heap[heapSize] = NULL;
	increaseKey(heapSize,node);
	}
}

void merge(int nbLists)
{
	NodePtr temp;
	heapSize = 0;
	for(int i = 0; i < nbLists; i++)
	{
		push(first[i]);
	}
	while(heapSize != 0)
	{
		NodePtr node = pop();
		append(node,MAX_HEAP);
		first[node->belongsTo] = first[node->belongsTo]->next;
		if(first[node->belongsTo] != NULL)
		{
			push(first[node->belongsTo]);
		}
		temp = NULL;
	}
}

void initializeLists(int nbLists)
{
	for(int i = 0; i < nbLists; i++)
	{
		first[i] = NULL;
		last[i] = NULL;
	}
}

// Generate one sorted list
void generateList(int size, int k)
{
	FillRandomArray(a,size,-2000,5000,false,1);
	for(int i = 0; i < size; i++)
	{
		append(a[i],k);
	}
}

// Add a new element to the end of the specified list
void append(int value, int k)
{
	NodePtr newNode = (NodeT*)malloc(sizeof(NodeT));
	if(newNode != NULL)
	{
		newNode->value = value;
		newNode->belongsTo = k;
		newNode->next = NULL;
		if(first[k] == NULL)
		{
			first[k] = newNode;
			last[k] = newNode;
		}
		else
		{
			last[k]->next = newNode;
			last[k] = newNode;
		}
	}
}


void append(NodePtr node, int k)
{
	if(node != NULL)
	{
		//profiler.countOperation("mergeComp",nbLists*size,1);
		if(first[k] == NULL)
		{
		//	profiler.countOperation("mergeAssign",nbLists*size,2);
			first[k] = node;
			last[k] = node;
		}
		else
		{
		//	profiler.countOperation("mergeAssign",nbLists*size,2);
			last[k]->next = node;
			last[k] = node;
		}
	}
}

// Print the list
void printList(int k)
{
	NodePtr node = first[k];
	if(node == NULL)
	{
		printf("List is empty.\n");
	}
	else
	{
		while(node != NULL)
		{
			printf("%d ",node->value);
			if(node->next!=NULL)
			{
				printf("|  ");
			}
			node = node->next;
		}
		printf("\n\n");
	}
}

// Deletes the lists
void deleteLists(int nbLists)
{
	for(int i = 0; i < nbLists; i++)
	{
		NodePtr node;
		node = first[i];
		while(node != NULL)
		{
			NodePtr temp = node;
			node = node->next;
			free(temp);
		}
	}
	initializeLists(nbLists);
}

// Testing min-heap
void printHeap(int node, int level)
{
    if (node <= size && node > 0)
	{
        printHeap(right(node), level + 1 );
        for (int i = 0; i <= level; i++ )
            printf( "       " );
        printf( "%d \n", heap[node]);
        printHeap(left(node), level + 1 );
    }
}

void main()
{
	
	// Correctness of the algorithm
	int k = 4;
	initializeLists(k);
	for(int i = 0; i < k; i++)
	{
		printf ("Sequence %d:  ", i+1);
		generateList(size,i);
		printList(i);
	}
	merge(k);
	printf("Resulting sequence is: ");
	printList(MAX_HEAP);
	getch();
	
	/*
	// Test for average case
	//for k1 = 100
	nbLists = 100;
	initializeLists(nbLists);
	for(size = 100; size <=10000; size+=100)
	{
		for(int i = 0; i < nbLists; i++)
		{
			generateList(size/nbLists,i);
		}
		merge(nbLists);
		deleteLists(nbLists);
		profiler.addSeries("mergeTotal","mergeComp","mergeAssign");
		printf("%d\n",size);
	}
	profiler.showReport(); */
}