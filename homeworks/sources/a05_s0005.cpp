
#include	<iostream>
#include	<time.h>
#include	"Profiler.h"
#define HASH_SIZE 10007
#define SEARCH_SIZE 3000
/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Inserarea si cautarea intr-un tabel de hash folosind 'Open Addressing' si 'Quadratic Probing'
Functia de hash folosita este h(x)= x + i + i^2;

	Se observa faptul ca numarul operatiilor efectuate pe hash table este foarte mic, chiar si in cazul
in care cautam elemente care nu se gasesc in el.
*/

int samples[HASH_SIZE + SEARCH_SIZE/2], indices[SEARCH_SIZE/2], hash[HASH_SIZE];
int n, avg_eff_found=0, max_eff_found=0, avg_eff_notfound=0, max_eff_notfound=0, eff=0;
float a[5]={0.8f, 0.85f, 0.9f, 0.95f, 0.99f};

void init_hash() {
	for(int i=0;i<HASH_SIZE;i++)
		hash[i]=-1;
}

int h(int x, int i) {
	return ((x+i+i*i)%HASH_SIZE);
}

void inserare_hash(int x) {
	int i=0, j;
	j=h(x, i);
	while(hash[j]!=-1 && i<=n) {
		i++;
		j=h(x, i);
	}
	hash[j]=x;
}

int cautare_hash(int x) {
	int i=0, j;
	
	do{
		j=h(x,i);
		eff++;
		if(hash[j]==x){
			return j;
		}
		if(hash[j]==-1)
			return -1;
		i++;
	}
	while(hash[j]!=x && i<=n);
	return -1;
}



int main () {
	int i, j;
	init_hash();
	printf("F.F.\tA.E.F.\tM.E.F.\tA.E.N.\tM.E.N.\n");
	//printf("%.2f\t%d\t%d\t%d\t%d\n", a[0], avg_eff_found, max_eff_found, avg_eff_notfound, max_eff_notfound);
	for(i=0;i<5;i++) {
		init_hash();
		avg_eff_found=0;
		avg_eff_notfound=0;
		max_eff_found=0;
		max_eff_notfound=0;


		n=a[i]*HASH_SIZE;
		FillRandomArray(samples, n+(SEARCH_SIZE/2), 1, 5000000, true, 0);
		FillRandomArray(indices, SEARCH_SIZE/2, 0, n-1, true, 0);
		//Inserez n+ 1500 elemente in hash
		for(j=0;j<n;j++)
			inserare_hash(samples[j]);

		//Caut 1500 de elemente care nu sunt in hash
		for(j=0;j<SEARCH_SIZE/2;j++) {
			eff=0;
			cautare_hash(samples[n+j]);
			if(eff>max_eff_notfound)
				max_eff_notfound=eff;
			avg_eff_notfound+=eff;
		}
		avg_eff_notfound/=1500;


		//Caut 1500 de elemente care sunt in hash
		for(j=0;j<SEARCH_SIZE/2;j++) {
			eff=0;
			cautare_hash(samples[indices[j]]);
			if(eff>max_eff_found)
				max_eff_found=eff;
			avg_eff_found+=eff;
		}
		avg_eff_found/=1500;

		printf("%.2f\t%d\t%d\t%d\t%d\n", a[i], avg_eff_found, max_eff_found, avg_eff_notfound, max_eff_notfound);
	}

	/*
	//Testare pentru valori mici

	n=20;
	for(i=0;i<n;i++)
		printf("%d ", hash[i]);
	printf("\n");
	inserare_hash(5);
	inserare_hash(10);
	inserare_hash(7);
	inserare_hash(21);
	for(i=0;i<n;i++)
		printf("%d ", hash[i]);
	printf("\n");
	printf("pozitia lui 5 este %d\n", cautare_hash(5));
	printf("pozitia lui 3 este %d\n", cautare_hash(3));
	printf("pozitia lui 21 este %d\n", cautare_hash(21));
	*/
	return 0;
}