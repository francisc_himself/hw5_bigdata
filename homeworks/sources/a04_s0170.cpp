
/*
Nume Prenume : ***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa : ***GROUP_NUMBER*** CTI 
Interclasarea  'inseamna'  combinarea  a doua  sau  mai  multe grupuri de inregistrari ordonate.
Pentru  interclasarea interna se considera doua sau  mai  multe liste  de inregistrari ordonate si se 
genereaza o singura  lista continind toate inregistrarile listelor de ***ANONIM***are, de  asemenea ordonate.
OBS : Algoritmul are o complexitate de : O(nlogk)
Cu cat crestem valoarea lui k , implicit cresc numarul de atribuiri si comparatii.
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h> 
#include <time.h>





int nratribuiri,nrcomparari;	//nr atribuiri, comparatii 

//structuri
struct element {
	int v;		//valoare
	int i;		//index
};

typedef struct nod{
	int cheie;	
	struct nod *urm;
}MyNod;

//lista
void creare_lista_vida (MyNod **prim, MyNod **ultim)
{
	*prim = 0;
	*ultim = 0;
}

void inserare_dupa_ultimul_nod (MyNod **prim, MyNod **ultim, int k)
{
	MyNod *p;
	p = (MyNod *) malloc(sizeof(MyNod));
	p->cheie = k;

	if (*prim == 0)
	{
		*prim = p;
		*ultim = p;
	}
	else
	{
		(*ultim)->urm = p;
		*ultim = p;
	}
	p->urm = 0;
}

MyNod* sterge_primul_nod (MyNod **prim, MyNod **ultim)
{
	MyNod *p;
	
	if (*prim == 0)
	{
		return NULL;
	}

	p = *prim;
	*prim = (*prim)->urm;
	return p;
}

void afis_lista (MyNod *prim)
{
	MyNod *p;
	p = (MyNod *) malloc(sizeof(MyNod));
	
	if (prim==0)
		printf ("Lista e vida!");
	else
	{
		p = prim;
		while (p!=0)
		{
			printf ("%d ",p->cheie);
			p = p->urm;
		}
	}
	printf ("\n");
}

//heap
int st (int i)
{
	return 2*i;
}

int dr (int i)
{
	return 2*i+1;
}

int parinte (int i)
{
	return i/2;
}

void initializareH (element H[10001],int *dim_heap)
{
	*dim_heap = 0;
}

void Hpush (element a[10001], int *dim_a, int valoare, int index)
{
	int i;
	element aux;

	*dim_a = *dim_a + 1;
	nratribuiri+=1;
	a[*dim_a].i = index;
	a[*dim_a].v = valoare;
	i = *dim_a;
	while (i>1 && a[i].v < a[parinte(i)].v)
	{
		nrcomparari++;
		nratribuiri+=3;
		aux = a[i];
		a[i] = a[parinte(i)];
		a[parinte(i)] = aux;

		i = parinte (i);
	}
	nrcomparari++;
}

void reconstructie_heap (element a[10001],int dim_heap, int i)
{
	int s,d,ind;
	element aux;

	s = st (i);
	d = dr (i);

	ind = i;

	nrcomparari++;
	if (s<=dim_heap && a[s].v<a[ind].v)
		ind = s;

	nrcomparari++;
	if (d<=dim_heap && a[d].v<a[ind].v)
			ind = d;

	if (ind != i)
	{
		nratribuiri+=3;
		aux = a[i];
		a[i] = a[ind];
		a[ind] = aux;

		reconstructie_heap (a,dim_heap,ind);
	}	
}


element Hpop (element a[10001], int *dim_a)
{
	element e;
	if (*dim_a > 0)
	{
		nratribuiri+=2;
		e = a[1];
		a[1] = a[*dim_a];
		*dim_a = *dim_a -1;
		reconstructie_heap (a,*dim_a,1);
	}
	return e;
}

//interclasare
void interclasare (MyNod* pcap[10001],MyNod* ucap[10001], int k,MyNod **pout,MyNod **uout,element H[10001], int *dim_heap)
{
	initializareH (H,dim_heap);
	creare_lista_vida (pout,uout);
	MyNod* p;
	p=(MyNod *) malloc(sizeof(MyNod));
	

	for (int i=1; i<=k; i++)
	{
		nrcomparari++;
		if (pcap[i]!=NULL)
		{
			nratribuiri++;
			p = sterge_primul_nod (&(pcap[i]),&(ucap[i]));
		}
		else
			p = NULL;
		nrcomparari++;
		if (p != NULL)
			Hpush (H,dim_heap,p->cheie,i);
	}

	while (*dim_heap > 0)
	{
		nratribuiri++;
		element e = Hpop (H,dim_heap);
		inserare_dupa_ultimul_nod (pout,uout,e.v);
		MyNod *q;
		nratribuiri++;
		q = sterge_primul_nod (&(pcap[e.i]),&(ucap[e.i]));
		nrcomparari++;
		if (q != NULL)
			Hpush (H,dim_heap,q->cheie,e.i);
	}
}

void punctulA ()
{
	int n,k;
	int j,l,val,c;	//  l = lungimea unei liste; lultima = lungimea ultimei liste
	MyNod *prim[10001],*ultim[10001];		//listele cu elementele prim su ultim
	MyNod *pout,*uout;
	element H[10001];
	int dim_heap;
	srand(time(NULL));

	FILE *pf5,*pf10,*pf100;

	pf5 = fopen("cazul_k5.txt","w");
	pf10 = fopen("cazul_k10.txt","w");
	pf100 = fopen("cazul_k100.txt","w");

	// k1 = 5
	k = 5;
	
	while (k<=100)
	{
		//generam k liste ca caror dimensiuni insumate e n
		for (n=100; n<=10000; n+=100)
		{
			nratribuiri = nrcomparari = 0;
			//generam lungimea listelor
			l = n/k;
			if (l * k == n)
			{
				//avem k liste de lungime egala
				for (j=1;j<=k;j++)
				{		
					creare_lista_vida (&prim[j],&ultim[j]);
					
					//generam primul element
					val = rand () %100 +1;

					inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);

					//generam celelalte elemente
					for (c=2;c<=l;c++)
					{
						val = val + rand()%10 + 1;
						inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);
					}
				}
			}
			else
			{
				if (l != 0)
				{
					//generam k-1 liste de lungime egala l si o lista de lungime mai mare
					for (j=1;j<k;j++)
					{
						creare_lista_vida (&prim[j],&ultim[j]);
					
						//generam primul element
						val = rand () %100 +1;
						inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);

						//generam celelalte elemente
						for (c=2;c<=l;c++)
						{
							val = val + rand()%10 + 1;
							inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);
						}
					}
					//lungimea ultimei liste
					l = n- (k-1) * l;

					creare_lista_vida (&prim[k],&ultim[k]);
					
					//generam primul element
					val = rand () %100 +1;
					inserare_dupa_ultimul_nod (&prim[k],&ultim[k],val);

					//generam celelalte elemente
					for (c=2;c<=l;c++)
					{
						val = val + rand()%10 + 1;
						inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);
					}
				}
				else
				{
					for (j=1;j<=k;j++)
					{
						creare_lista_vida (&prim[j],&ultim[j]);
					}

					for (c=1; c<=n; c++)		//cat mai avem elemente punem cate unul in fiecare lista
					{
						val = rand () %100 +1;
						inserare_dupa_ultimul_nod (&prim[c],&ultim[c],val);
					}
				}
			}
			
			
			interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
			switch (k)
			{
				case 5:
						fprintf (pf5,"%d %d %d\n",k,n,nratribuiri+nrcomparari);
						break;
				case 10:
						fprintf (pf10,"%d %d %d\n",k,n,nratribuiri+nrcomparari);
						break;
				case 100:
						fprintf (pf100,"%d %d %d\n",k,n,nratribuiri+nrcomparari);
						break;
			}
		}
		if (k == 5)
			k = 10;
		else 
		{
			if (k == 10)
				k = 100;
			else
				k = 101;
		}	
	}

}

void punctulB ()
{
	int n,k;
	int j,l,val,c;	//l = lungimea unei liste; lultima = lungimea ultimei liste
	MyNod *prim[10001],*ultim[10001];		//listele cu elementele prim su ultim
	MyNod *pout,*uout;
	element H[10001];
	int dim_heap;
	srand(time(NULL));

	FILE *pf;

	pf = fopen("cazul_n_10000.txt","w");
	
	n = 10000;

	for (k=10; k<=500; k+=10)
	{
		//generam listele
		nratribuiri = nrcomparari = 0;
		//generam lungimea listelor
		l = n/k;
		if (l * k == n)
		{
			//avem k liste de lungime egala
			for (j=1;j<=k;j++)
			{		
				creare_lista_vida (&prim[j],&ultim[j]);
					
				//generam primul element
				val = rand () %100 +1;

				inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);

				//generam celelalte elemente
				for (c=2;c<=l;c++)
				{
					val = val + rand()%10 + 1;
					inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);
				}
			}
		}
		else
		{
			if (l != 0)
			{
				//generam k-1 liste de lungime egala l si o lista de lungime mai mare
				for (j=1;j<k;j++)
				{
					creare_lista_vida (&prim[j],&ultim[j]);
					
					//generam primul element
					val = rand () %100 +1;
					inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);

					//generam celelalte elemente
					for (c=2;c<=l;c++)
					{
						val = val + rand()%10 + 1;
						inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);
					}
				}
				//lungimea ultimei liste
				l = n- (k-1) * l;

				creare_lista_vida (&prim[k],&ultim[k]);
					
				//generam primul element
				val = rand () %100 +1;
				inserare_dupa_ultimul_nod (&prim[k],&ultim[k],val);

				//generam celelalte elemente
				for (c=2;c<=l;c++)
				{
					val = val + rand()%10 + 1;
					inserare_dupa_ultimul_nod (&prim[j],&ultim[j],val);
				}
			}
			else
			{
				for (j=1;j<=k;j++)
				{
					creare_lista_vida (&prim[j],&ultim[j]);
				}

				for (c=1; c<=n; c++)		//cat mai avem elemente punem cate unul in fiecare lista
				{
					val = rand () %100 +1;
					inserare_dupa_ultimul_nod (&prim[c],&ultim[c],val);
				}
			}
		}
			
		
		interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
		
		fprintf (pf,"%d %d %d\n",k,n,nratribuiri+nrcomparari);
	}
}


void testare_afisare_ecran ()
{
	int n,k;
	k=3;
	n=8;
	MyNod *prim[4],*ultim[4];
	MyNod *pout,*uout;
	element H[10001];
	int dim_heap;

	creare_lista_vida (&prim[1],&ultim[1]);
	inserare_dupa_ultimul_nod (&prim[1],&ultim[1],2);
	inserare_dupa_ultimul_nod (&prim[1],&ultim[1],7);
	inserare_dupa_ultimul_nod (&prim[1],&ultim[1],10);
	
	afis_lista (prim[1]);


	creare_lista_vida (&prim[2],&ultim[2]);
	inserare_dupa_ultimul_nod (&prim[2],&ultim[2],1);
	inserare_dupa_ultimul_nod (&prim[2],&ultim[2],4);
	inserare_dupa_ultimul_nod (&prim[2],&ultim[2],12);

	afis_lista (prim[2]);

	creare_lista_vida (&prim[3],&ultim[3]);
	inserare_dupa_ultimul_nod (&prim[3],&ultim[2],4);
	inserare_dupa_ultimul_nod (&prim[3],&ultim[2],6);

	afis_lista (prim[3]);
	interclasare (prim,ultim,k,&pout,&uout,H,&dim_heap);
	printf ("Lista rezultat:\n");
	afis_lista (pout);
}

int main ()
{
	//testare_afisare_ecran ();        // asta am inclus-o doar pentru a testa daca ***ANONIM***-adevar se face sortarea
	punctulA ();
	punctulB();
	getche ();
	return 0;
}