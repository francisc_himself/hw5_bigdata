/*
Student:		***ANONIM*** ***ANONIM***	
Grupa:			***GROUP_NUMBER***

Cerinta:

construiti si testati cele 2 metode de sortare a hepurilor, HS si QS
Construiti grafice pentru cazul mediu statistic si comparati numarul de operatii pe care le face HS si QS. ce se observa?
construiti grafice pentru QuickSort in cazul favorabil si defavorabil. 





*/





#include<stdlib.h>
#include<conio.h>
#include<fstream>
#include<iostream>


int n, A[10000], B[10000], C[10000], dimB,op_hs,op_qs;
FILE *g;

void generare_aleator(int A[], int n)// functie folosita pentru a generea alemente aleatoare pentru heap
{
	for(int i=1;i<=n;i++)
	   A[i]=rand();				// rand() va asigura atribuirea unei valori aleatoare
}
void generare_defavorabil(int A[], int n)// functie folosita pentru a genera elemente pentru heap in cazul defavorabil, ex [3,2,1]
{
	for(int i=1;i<n;i++)
		A[i]=A[i-1]-rand();   // rand() va asigura atribuirea unei valori aleatoare
}
void generare_favorabil(int A[], int n)   //functie care va crea un sir crescator de numere intregi cu valori aleatoare 
                                   
{ 
   	int i;                                     
	A[0]=rand();
	for(i=1;i<n;i++)
		A[i]=A[i-1]+rand();      // rand() va asigura atribuirea unei valori aleatoare
	                             
}
void copiere(int A[], int B[], int n)// functie folosita pentru a face o copie a elementelor lui A in B, pentru a se putea apela QS si HS concomitent
{
   for(int i=1;i<=n;i++)
	   B[i]=A[i];
}



void Rec_H(int A[], int i)// reconstructie Heap
{
	int ind,x,y,aux;
	x=2*i;
	y=2*i+1;

	op_hs++;
	if (x<=dimB && A[x]<A[i] )
		ind=x;	
    else
        ind = i;


	op_hs++;
	if (y<=dimB && A[y]<A[ind] )
			ind=y;
	
	
	if(ind!=i)
	{
		aux=A[i];
		A[i]=A[ind];
		A[ind]=aux;
		op_hs+=3;
	Rec_H(A,ind);
	}
}





void Bottom_Up(int A[], int n)// metoda de parcurgere a Heap-ului de "sus in jos"
{
	dimB=n;
	for(int i=n/2;i>=1;i--)
		Rec_H(A,i);

}

//Compelxitatea Algoritmului de sortare HeapSort este:O(nlogn)
void HEAP_sort(int A[], int )// metoda de sortare a Heapului,Heapsort																	
{
	int i, aux;
	
	Bottom_Up(A,n);
	for(int i=n;i>=2;i--)
	{
		aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		dimB--;
		op_hs+=3;
		Rec_H(A,1);
	}
}


//Complexitatea algoritmului de sortare Quciksort este:O(nlogn)
void quickSort(int a[], int st, int dr)// metoda de sortare a Heapului, Quicksort
{
	int i=st,j=dr;
    int m=a[(st+dr)/2];
	
	int aux;
     
   
	while (i<=j)
	  {
		  op_qs++;
		  while(a[i]<m)
			{
				  
			    i++;
				op_qs++;
			}
		  op_qs++;
          while(a[j]>m )
			{   
				j--;
		      op_qs++;
			}
		  
           if(i<=j)
		   {
			   aux=a[i];
               a[i]=a[j];
               a[j]=aux;
			   op_qs+=3;
               i++;
               j--;
		   }
           
      }
      if (st<j)
            quickSort(a,st,j);
      if (i<dr)
            quickSort(a,i,dr);
}

void quickSort2(int a[], int st, int dr)// metoda de sortare a Heapului, Quicksort
{
	int i=st,j=dr;
    int m=a[st];
	
	int aux;
     
   
	while (i<=j)
	  {
		  op_qs++;
		  while(a[i]<m)
			{
				  
			    i++;
				op_qs++;
			}
		  op_qs++;
          while(a[j]>m )
			{   
				j--;
		      op_qs++;
			}
		  
           if(i<=j)
		   {
			   aux=a[i];
               a[i]=a[j];
               a[j]=aux;
			   op_qs+=3;
               i++;
               j--;
		   }
           
      }
      if (st<j)
            quickSort2(a,st,j);
      if (i<dr)
            quickSort2(a,i,dr);
}

void mediu2()// functie care se apeleaza in main pentru a genera in fisierul "mediuHEAPQUICK" numarul de operatii pe care cele 
	//2 metode de sortare le vor face si pentru a construi graficul aferent
{

g=fopen("mediuHEAPQUICK.csv","w");
fprintf(g,"n, op_hs, op_qs\n");
	for (n=100;n<=10000;n+=100)
	{

		for(int j=1;j<=5;j++)
		{
			op_hs=op_qs=0;
			generare_aleator(A,n);
			copiere(A,C,n);
			
			HEAP_sort(A,n);
		

			copiere(C,A,n);
			
			quickSort(A,1,n);
		
		}

	
		fprintf(g,"%d,%d,%d\n",n,op_hs/5,op_qs/5);
	    
	}
	fclose(g);


}


//functie care se apeleaza in main pentru a putea observa cum se comporta metoda quicksort in cazul cel mai defavorabil
//rezultatele pe care le urmarim se gasesc in fisierul "defavorabilQS", impreuna cu graficul aferent
void defavorabilQS()
{

	int n;
  
	g=fopen("defavorabilQS.csv","w");
	fprintf(g,"n, op_qs\n");
	op_qs=0;
for (n=100;n<=10000;n+=100)
	{
		
		
			generare_defavorabil(A,n);
			op_qs=0;
			
			quickSort2(A,1,n);
			

		
	
	fprintf(g,"%d, %d\n",n,op_qs);
}
fclose(g);
}
//functie care se apeleaza in main pentru a putea observa cum se comporta metoda quicksort in cazul favorabil
//rezultatele pe care le urmarim se gasesc in fisierul "favorabilQS", impreuna cu graficul aferent
void favorabilQS()
{

	int n;
  
	g=fopen("favorabilQS.csv","w");
	fprintf(g,"n, op_qs\n");
	op_qs=0;
for (n=100;n<=10000;n+=100)
	{
		
		
			generare_favorabil(A,n);
			op_qs=0;
			quickSort(A,1,n);
			
		
	
	fprintf(g,"%d, %d\n",n,op_qs);
}
fclose(g);
}



void main()
{
	//mediu2();
	//favorabilQS();
	defavorabilQS();
	
	//getch();
}