#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include "Profiler.h"
#define NMAX 20000

int a[NMAX],b[NMAX], dim_heap, BUcmp, TDcmp, BUatr, TDatr;


//BU - O(n log n)
void ReBuild(int a[NMAX],int i)
{
    int ind,aux,l,r;
	l= 2*i;
	r= 2*i+1;
	BUcmp++;
	if(l<= dim_heap && a[l]>a[i])
		ind = l;
	else
		ind = i;
	BUcmp++;
	if(r<=dim_heap && a[r]>a[ind])
		ind = r;
	if(ind!=i)
	{
		aux=a[i];
		a[i]=a[ind];
		a[ind]=aux;
		BUatr = BUatr+3;
		ReBuild(a,ind);
	}
}

void bottom_up(int a[NMAX], int n)
{
	int i;
	BUcmp =0; BUatr=0;
	dim_heap = n;
    for(i=n/2;i>=1;i--)
	{
		ReBuild(a,i);
		
	}
	
}

//TD - O(n log n)
void insert_heap(int a[], int key)
{
	int i,aux;
	dim_heap = dim_heap+1;
	i= dim_heap;
	TDcmp++;
	while(i>1 && a[i/2] < key)
	{
		TDcmp++;
		TDatr+=3;
		aux = a[i/2];
		a[i/2] = a[i];
		a[i] = aux;
		i = i/2;
	}
	TDatr++;
	a[i] = key;
}

void top_down (int a[] , int n)
{
	int i;
	TDatr = 0; TDcmp = 0;
	dim_heap = 1; 
	for(i=2; i<=n; i++)
		insert_heap(a,a[i]);
}


void init(int a[], int n)
{
	for(int i=1;i<=n;i++)
		a[i]=0;
}

void printp(int x[],int i,int adancime)
{
	if(i<=dim_heap){ 
	printp(x,(2*i+1),(adancime+1));
	for(int j=1;j<=adancime;j++) printf("   ");
		printf("%d", x[i]);
		printf("\n");
	printp(x,2*i,(adancime+1));
	}
}

void main()
{
	int i, BUcmps, BUatrs, TDatrs, TDcmps,k;	
	
	int d[NMAX],e[NMAX];
	for(i=1;i<=10;i++)
		scanf("%d",&d[i]);
	
	//{20, 13, 9, 7, 1, 3, 5, 4, 43, 10};
	
	for(int j=1;j<=10;j++)
		e[j]=d[j];		
	printf("top_down:\n");
	top_down(d,10);

	for(i=1; i<=10; i++)
	printf("%d ",d[i]);
	printf("\n");
	printp(d,1,0);

	printf("\nbottom_up:\n");
	bottom_up(e,10);
	for(i=1; i<=10; i++)
	printf("%d ",e[i]);
	printf("\n");
	printp(e,1,0);
	

	//init(a,NMAX);
	//init(b,NMAX);
	
	/*
	FILE *pf;
	pf = fopen("file1.csv","w");
	if(pf==NULL)
	{
		perror("Error opening file!");
		exit(1);
	}
	
	fprintf(pf,"n, BUatr, BUcmp, BUsum, TDatr, TDcmp, TDsum\n");
	
	for(i=100;i<=10000; i=i+100)
	{
		BUcmps=0; BUatrs=0; TDatrs=0; TDcmps=0;
		
		for(k=1;k<=5;k++){
			FillRandomArray(a,i,1,20000,true,0);

			for(int j=1;j<=i;j++)
			b[j]=a[j];
			
			bottom_up(b,i);

			BUatrs = BUatrs + BUatr;
			BUcmps = BUcmps +BUcmp;
			
			init(b,i);
			
			for(int j=1;j<=i;j++)
			b[j]=a[j];

			top_down(b,i);
			TDatrs = TDatrs + TDatr;
			TDcmps = TDcmps +TDcmp;
		}
		fprintf(pf,"%d, %d, %d, %d, %d, %d, %d\n",i, BUatrs/5, BUcmps/5,(BUatrs+BUcmps)/5, TDatrs/5,TDcmps/5,(TDatrs+TDcmps)/5);
	}*/
//	fclose(pf);
	
	//cazul defavorabil
	
	/*FILE *pf2;
	init(a,NMAX);
	init(b,NMAX);
	pf2 = fopen("file2.csv","w");
	if(pf2==NULL)
	{
		perror("Error opening file!");
		exit(1);
	}
	fprintf(pf2,"n, BUatr, BUcmp, BUsum, TDatr, TDcmp, TDsum\n");

	for(i=100;i<=10000; i=i+100)
	{
		BUcmps=0; BUatrs=0; TDatrs=0; TDcmps=0;
			FillRandomArray(a,i,1,20000,true,1);

			for(int j=1;j<=i;j++)
			b[j]=a[j];
			
			bottom_up(b,i);
			
			init(b,i);
			
			for(int j=1;j<=i;j++)
			b[j]=a[j];

			top_down(b,i);
			
			fprintf(pf2,"%d, %d, %d, %d, %d, %d, %d\n",i, BUatr, BUcmp,(BUatr+BUcmp), TDatr,TDcmp,(TDatr+TDcmp));
	}
	fclose(pf2);	*/
		
	printf("\ndone`");
	getch();
}

/*
Pe baza graficului, se observa ca metoda bottom_up este mai eficienta decat Top-down in crearea unei structuri de tip heap
*/