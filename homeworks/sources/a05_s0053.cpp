//#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#define N 10007
#define c1 1
#define c2 5

int H[N+1];
int searched;
int found,notfound,search,n,sf,snf;

int hash(int x, int i){
	return (x%N+i*c1+i*i*c2)%N;}

void init(){
	for (int i=0;i<=N;i++)
		H[i]=0;}

int hashinsert(int k){
	int i=0,j;
	do{
		j=hash(k,i);
		if (H[j]==NULL){
			H[j]=k;
			return j;}
		else
			i++;
	}while (i!=N);
	printf("Hash table overflow");
	return -1;}

int hashsearch(int k){
	int i=0,j;
	do{
		j=hash(k,i);
		search++;
		searched++;
		if (H[j]==k){			
			return j;}
		else
			i++;
	}while (i!=N && H[j]!=NULL);
	printf("Not found!\n");
	return -1;}

int main(){
	int t[N],x;
	for (int i=0;i<N;i++){
		t[i]=rand() % 10000;}
	printf("%d",t);
		
	for (int i=0;i<N;i++){
		hashinsert(t[i]);}

	scanf("%d",&x);
	
	int y=hashsearch(x);
	
	printf("pe locul %d este numarul %d in tabela hash",y,H[y]);
		
	return 0;}