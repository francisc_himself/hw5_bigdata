#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct nod {
    int key;
    int dim;
    struct nod* urm;
    struct nod* reprez;
} Nod;

typedef struct muchie {
    int x;
    int y;
} Muchie;

Nod* cap[10000];
Nod* coada[10000];
Muchie m[60000];
int v = 10000;
int e;

static int M = 0;

//crearea multimii cu reprezentantul x
void formeaza_multime(int x) {
    Nod* elem = (Nod*)malloc(sizeof(Nod));
    elem -> key = x;
    elem -> dim = 1;
    elem -> urm = NULL;
    elem -> reprez = elem;
    cap[x] = coada[x] = elem;
}

//gasirea reprezentantului unei multimi
int gaseste_multime(int x) {
    return cap[x] -> reprez -> key;
}

//uniunea a doua multimi disjuncte
void uneste(int x, int y) {
	Nod* elem;
    int i = gaseste_multime(x);
    int k = gaseste_multime(y);
    M += 2;
    int j;
    int aux;

    if (i == k) return;

    if (cap[i] -> dim > cap[k] -> dim) {
        aux = i;
        i = k;
        k = aux;
    }

    coada[k] -> urm = cap[i];
    coada[k] = coada[i];

    elem = cap[i];
    while(elem) {
        elem -> reprez = cap[k];
        elem = elem -> urm;
    }

    cap[k] -> dim += cap[i] -> dim;
    cap[i] -> dim = 0;
}

void componente_conexe() {
	int i;
	for (i = 0; i < v; i++) {
        formeaza_multime(i);
        M++;
	}
	for (i = 0; i < e; i++) {
        uneste(m[i].x, m[i].y);
        M++;
	}
}

int main() {
	int i, j, e1, e2;
	FILE *f;
	f=fopen("multimi.csv","w");
	srand(time(NULL));
	for (e = 10000; e <= 60000; e += 1000) 
	{
		M=0;
		for (j = 0; j < e; j++) 
		{
			e1 = rand()%v;
			e2 = rand()%v;
			m[j].x = e1;
			m[j].y = e2;
		}
		
		componente_conexe();
		fprintf(f,"%10d %10d\n", e, M);
	}
	fclose(f);
}

