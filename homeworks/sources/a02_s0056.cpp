/*
***ANONIM*** ***ANONIM***-***ANONIM***, grupa 3004

Exemplul de mai jos executa o constructie de heap folosind cele doua abordari  ( top down si botom up), prin intermediul a 
doua proceduri (sift si percolate).

SIFT - se foloseste pentru abordarea bottom up; pentru un anumit element k cerne acest elemenet la nivele mai mari in lista (mai jos) pana cand 
acesta ajunge intr-o pozittie corecta a definirii de heap. La inceput subarborii lui k au a structura corecta de heap.

PERCOLATE se foloseste pentru abordarea top down, iar in acest caz  toti parintii lui k din arbore au o constructie corecta de heap.
Functia muta elementul k in sus pe arbore pana acesta ajunge intr-o pozitie corecta.





*/




#include <stdio.h>
#include<time.h>
#include <stdlib.h> 
#include<string.h>
#include "Profiler.h"
#define MAX 10000

int v[MAX];
Profiler p("lab2");

void genv(int v[],int n,int x)
{
	int i;

	for(i=1;i<=n;i++)
	{
		v[i]=rand()%x;
	}
}

int father(int x)
{
	return x/2;
}

int lson(int x)
{
	return 2*x;
}

int rson(int x)
{
	return 2*x+1;
}

void is(int &a,int &b)
{
	int temp;
	temp=a;
	a=b;
	b=temp;
}

void sift(int v[],int n,int k,int mod)
{
	int son;

	do
	{
		if(mod==0){p.countOperation("bottom_up",n,3);}else{p.countOperation("top_down",n,3);}
		son=0;
		if(lson(k)<=n)
		{
			son=lson(k);
			if((rson(k)<=n)&&(v[rson(k)]>v[lson(k)]))
			{
				son=rson(k);
				if(mod==0){p.countOperation("bottom_up",n,1);}else{p.countOperation("top_down",n,1);}

			}
				
			if(v[son]<=v[k])
			{
				son=0;
				if(mod==0){p.countOperation("bottom_up",n,3);}else{p.countOperation("top_down",n,3);}
			}
			if(mod==0){p.countOperation("bottom_up",n,3);}else{p.countOperation("top_down",n,3);}
		}

		if(mod==0){p.countOperation("bottom_up",n,1);}else{p.countOperation("top_down",n,1);}
		if(son!=0)
		{
			is(v[son],v[k]);
			k=son;
			if(mod==0){p.countOperation("bottom_up",n,4);}else{p.countOperation("top_down",n,4);}
		}


	}
	while(son!=0);

}



void percolate(int v[],int n,int k,int mod)
{
	int key=v[k];

	while((k>1)&&(key>v[father(k)]))
	{
		if(mod==0){p.countOperation("bottom_up",n,3);}else{p.countOperation("top_down",n,3);}
		v[k]=v[father(k)];
		k=father(k);
	}
	v[k]=key;
	if(mod==0){p.countOperation("bottom_up",n,3);}else{p.countOperation("top_down",n,3);}
}



void bottom_up(int v[],int n,int mod)
{
	int i;
	for(i=n/2;i>=1;i--)
	{
		sift(v,n,i,mod);
	}

}

void top_down(int v[],int n,int mod)
{
	int i;
	for(i=2;i<=n;i++)
	{
		percolate(v,n,i,mod);
	}
}


void print_heap(int v[],int n,int k,int level)
{
	if(rson(k)<=n)print_heap(v,n,rson(k),level+1);
	for(int i=0;i<=level;i++)printf("  ");
	printf("%d\n",v[k]);
	if(lson(k)<=n)print_heap(v,n,lson(k),level+1);
}

int main()
{
	int n=18;
	srand(time(NULL));

	/*
	genv(v,n,30);
	bottom_up(v,n);
	print_heap(v,n,1,0);

	printf("\n\n\n");

	genv(v,n,30);
	top_down(v,n);
	print_heap(v,n,1,0);
	*/

	
	for(int i=1;i<=5;i++)
	{
		for(n=100;n<=5000;n+=100)
		{
			printf("%d\n",n);
			genv(v,n,1000);
			bottom_up(v,n,0);
			genv(v,n,1000);
			top_down(v,n,1);
			
		}

	}


	p.createGroup("topDown_bottomUp","bottom_up","top_down");

	p.showReport();
	
	return 0;
}