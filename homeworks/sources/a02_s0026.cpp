#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 
#include <math.h>
int at=0, comp=0;
void H_PUSH(int b[10000],int x,int *db){
	int i;
	*db=*db+1;
	at++;
	b[*db]=x;
	i=*db;
	int aux;
	while (i>1 && b[i]<b[i/2]){
		comp++;
		at=at+3;
		aux=b[i];
		b[i]=b[i/2];
		b[i/2]=aux;
		i=i/2;
		//printf("%d i: ",i);
	}
}
void constr_heap_td(int a[10000],int b[10000],int da,int *db){
	int i;
	at=0; comp=0;
	for(i=1;i<=da;i++) H_PUSH(b,a[i],db);
}

void reconstruire(int b[],int i,int n){
  int l = 2*i, great;
  int r = 2*i+1;
  int aux;
  comp++; 
  if (  (l <= n) && (b[l] > b[i])) {
    great = l;
  }
  else {
    great = i;
  }
  comp++;
  if ( (r <= n) && (b[r] > b[great])) {
    great = r;
  }
  if (great != i) {
	  at=at+3;
    aux = b[i];
    b[i] = b[great];
    b[great] = aux;
    reconstruire(b, great,n);
  }
}

void constr_heap_bu(int b[], int n){
	int i;
	at=0;
	comp=0;
	for (i=n/2; i > 0;i--) 
	{   
		reconstruire(b,i,n); 
	}
}
int main(){
	int a[10000];
	int b[10000];
	int c[10000]; //copie
    int n,i;
	int db;
	int at_h[1],comp_h[1];
	FILE *f;
	f=fopen("heap.csv","w");
	fprintf(f,"n, heap_td_at,heap_td_comp,heap_td_at+comp,heap_bu_at,heap_bu_comp,heap_bu_at+comp \n");

	srand(time(NULL));
	for(n=100;n<=10000;n=n+100){
		at_h[0]=0;
		at_h[1]=0;
		comp_h[0]=0;
		comp_h[1]=0;
		for(int j=1;j<=5;j++){	
			for(i=1;i<=n;i++){				
				a[i]=rand()%1000;
				c[i]=a[i];
			}	
		constr_heap_bu(c,n);
		at_h[0]=at_h[0]+at;
		comp_h[0]=comp_h[0]+comp ;
	    db=0;
		constr_heap_td(a,b,n,&db);
		at_h[1]=at_h[1]+ at;
		comp_h[1]=comp_h[1]+ comp;
		
	}
	fprintf (f, "%d,%d,%d,%d,%d,%d,%d \n",n,at_h[1]/5,comp_h[1]/5,(at_h[1]+comp_h[1])/5,at_h[0]/5,comp_h[0]/5,(at_h[0]+comp_h[0])/5 );
	}
	//verificare functionalitate alg pt 10 elem
	int x[11]={0, 2, 13, 13, 9, 7, 1, 3, 5, 15, 3}; 
	int x2[11]={0, 2, 13, 13, 9, 7, 1, 3, 5, 15, 3}; 

    constr_heap_bu(x,10);
	printf("\n bu:\n");

	int lungime=0;
	for (int i=1;i<=10;i++){
		for(int j=0;j<=10-lungime-2;j++)printf(" ");
		printf("%d",x[i]);
		if(i==3 || i==7 || i==1) {
			printf("\n\n");
			lungime+=4;
		}
		else lungime-=1;
	}
    
	int dx2=0;
	int nx=10;
	lungime=0;
	printf("\n\n td:\n");
	constr_heap_td(x2,x2,nx,&dx2);
	for (int i=1;i<=10;i++){
	for(int j=0;j<=10-lungime-2;j++)printf(" ");
	printf("%d",x2[i]);
	if(i==3 || i==7 || i==1) {
		printf("\n\n");
		lungime+=4;
	}
	else lungime-=1;
	}
fclose(f);
printf("\n\n s-a terminat");
getch();
return 0;
}