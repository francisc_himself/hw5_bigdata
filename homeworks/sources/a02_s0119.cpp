//Average case - as we can see from the chart, it is obvious that Bottom-Up Heapsort is more efficient, 
//the number of comparisons executed by this operation is considerably smaller than the ones executed 
//by Top-Down Heapsort

//It is a stable algorithm.
//Both methods of building the heap have a linear growth.

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "Profiler.h"

Profiler profiler("demo");
using namespace std;
#define NEG_INF INT_MIN

int heap[10001],length, heap_size, x[10001],prettyBU[13],prettyTD[13];

int left(int );
int right(int);
int parent(int);
void maxHeapify(int [],int);
void buildMaxHeapBU(int []);
void heapsort(int []);
int heapMax(int []);
int extractHeapMax(int []);
void heapIncreaseKey(int [], int, int);
void maxHeapInsert(int[], int);
void averageCase();
void bestCase();
void worstCase();
void copy(int[], int[]);
void buildMaxHeapTD(int []);
void display(int []);
void prettyPrint(int [], int, int);
bool IsPowerOfTwo(int);

int left(int i)
{
	return 2*i;
}
int right(int i)
{
	return 2*i+1;
}
int parent(int i)
{
	return i/2;
}
//for the bottom-up method
void maxHeapify(int heap[], int i)
{
	int l, r, largest, aux;
	l = left(i);
	r = right(i);
	profiler.countOperation("operationBU",length);
	if(l <= heap_size && heap[l] > heap[i])
		largest = l;
	else 
		largest = i;
	profiler.countOperation("operationBU",length);
	if(r <= heap_size && heap[r] > heap[largest])
		largest = r;
	if(largest != i)
	{
		profiler.countOperation("operationBU",length,3);
		aux = heap[i];
		heap[i] = heap [largest];
		heap[largest] = aux;
		maxHeapify(heap, largest);
	}
}
void buildMaxHeapBU(int heap[])
{
	heap_size = length;
	for(int i = length/2; i >= 1; i--)
		maxHeapify(heap, i);
}
void heapsort(int heap[])
{
	int aux;
	buildMaxHeapBU(heap);
	for(int i = length; i >= 2; i--)
	{
		profiler.countOperation("operationBU",length,3);
		aux = heap[1];
		heap[1] = heap[i];
		heap[i] = aux;
		heap_size --;
		maxHeapify(heap, 1);
	}
}
//for the top-down method
int heapMax(int heap[])
{
	return heap[1];
}
int heapExtractMax(int heap[])
{
	int max;
	profiler.countOperation("operationTD",length);
	if(heap_size < 1)
		printf("\nHeap underflow!");
	profiler.countOperation("operationTD",length,2);
	max = heap[1];
	heap[1] = heap[heap_size];
	--heap_size;
	maxHeapify(heap, 1);
	return max;
}
void heapIncreaseKey(int heap[], int i, int key)
{
	int aux;
	profiler.countOperation("operationTD",length);
	if(key < heap[i])
		printf("\nNew key is smaller than current key");
	profiler.countOperation("operationTD",length,2);
	heap[i] = key;
	while(i > 1 && heap[parent(i)] < heap[i])
	{
		profiler.countOperation("operationTD",length,3);
		aux = heap[i];
		heap[i] = heap[parent(i)];
		heap[parent(i)] = aux;
		i = parent(i);
	}
}
void maxHeapInsert(int heap[], int key)
{
	++heap_size;
	profiler.countOperation("operationTD",length);
	heap[heap_size] = NEG_INF;
	heapIncreaseKey(heap, heap_size, key);
}
void buildMaxHeapTD(int heap[])
{
	heap_size = 1;
	for(int i = 2; i <= length; i++)
		maxHeapInsert(heap, heap[i]);
}
void copy(int heap[], int x[])
{
	for(int i=1; i <= length; i++)
		x[i] = heap[i];
}
void averageCase()
{
	for(int i=1; i <= 5; i++)
		for(length = 100; length <= 10000; length += 100)
		{
			FillRandomArray(heap,length);
			copy(heap,x);
		//	printf("\n Unsorted array: \n");
			//display(heap);
		//	printf("\nx=\n");
		//	display(x);
			buildMaxHeapBU(x);
			buildMaxHeapTD(heap);
			printf("\n meas : %d, length = %d\n",i, length);
		//	printf("\nBottom-Up:\n");
			//display(x);
		//	printf("\nTop-Down: \n");
			//display(heap);
		//	getchar();
		}
	profiler.createGroup("BuildHeap","operationTD","operationBU");
	profiler.showReport();
}
void display(int xy[])
{
	for(int i =1; i <= length; i++)
		printf("%d ",xy[i]);
}
void prettyPrint(int he[], int i, int level)
{
	if(i > length)
		return;
	prettyPrint(he,left(i), level+1);
	for(int j=0;j<level;j++)
		printf("         ",level);
	printf("%d\n",he[i]);
	prettyPrint(he,right(i), level+1);
		
}
bool IsPowerOfTwo(int x)
{
    return (x != 0) && ((x & (x - 1)) == 0);
}
int main()
{
	averageCase();
	
	length = 31;
	FillRandomArray(prettyBU, length+1,1, 100, true);
	/*for(int i=1 ; i <= length; i++)
		scanf("%d", prettyBU+i);*/
	copy(prettyBU,prettyTD);
	display(prettyTD);
	buildMaxHeapBU(prettyBU);
	buildMaxHeapTD(prettyTD);
	printf("\n Bottom-Up pretty print of the array: \n");
	//display(prettyBU);
	prettyPrint(prettyBU, 1, 0);
	printf("\n Top-Down pretty print of the array: \n");
	prettyPrint(prettyTD, 1, 0);
	return 0;
}