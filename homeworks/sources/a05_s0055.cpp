#include <stdlib.h>
#include <stdio.h>
#define hs 10007
int ht[hs];
int hfct(int a)
{
	return a%hs;
}

void ins(int a)
{
	for(int i=0;i<hs;i++)
	{
		if(ht[hfct(a)+i]==0)
		{
			ht[hfct(a)+i]=a;
			return;
		}
	}
}

void prnt()
{
	for(int i=0;i<hs;i++)
	{
		printf("%d ", ht[i]);
	}
	printf("\n");
}

void reset()
{
	printf("Resetting the hash table, please use prnt() function to check if the resetting procedure was succesful\n");
	for(int i=0;i<=hs;i++)
	{
		ht[i]=0;
	}
}

void search(int a)
{
	for(int i=0;i<hs;i++)
	{
		if(ht[hfct(a)+i]==a)
		{
			printf("Found %d\n",a);
			return;
		}
	}
	printf("%dDoesn't exist in hash table\n",a);
}

int main()
{
	ins(10);
	ins(287);
	search(10);
	search(287);
	search(28);
	prnt();
	reset();
	return 0;
}