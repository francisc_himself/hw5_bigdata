#include<stdio.h>
#include<time.h>
#include<Windows.h>

unsigned long long nrOperatii;

void max_heapify(int A[], int i, int dimA)
{
	int l, r, largest, aux;

	l = 2*i;
	r = 2*i + 1;

	if(l <= dimA && A[l] > A[i])
	{
		largest = l;
	}
	else
	{
		largest = i;
	}

	if( r <= dimA && A[r] > A[largest])
	{
		largest = r;
	}
	nrOperatii += 2;

	if(largest != i)
	{
		aux = A[i];
		A[i] = A[largest];
		A[largest] = aux;

		nrOperatii += 3;

		max_heapify(A, largest, dimA);
	}
}

void buildH_BU(int A[], int dimA)
{
	int i;

	for(i = dimA/2; i>0; i--)
	{	
		max_heapify(A, i, dimA);
	}
}


void buildH_TD(int A[], int dimA, int k)
{
	int aux;

	dimA++;
	A[dimA] = k;

	nrOperatii += 2;

	while(A[dimA] > A[dimA/2] && dimA > 1)
	{
		aux = A[dimA];
		A[dimA] = A[dimA/2];
		A[dimA/2] = aux;

		dimA = dimA/2;

		nrOperatii += 4;			
	}
}

void afiseazaH(int A[], int i, int n, int depth)
{
	int k;
	if(i>n)
	{
		return ;
	}
	for(k=0; k<depth; k++)
	{
		printf("    ");
	}

	printf("%d \n", A[i]);
	afiseazaH(A, 2*i, n, depth+1);
	afiseazaH(A, 2*i + 1, n, depth+1);
	
}

int main()
{
	int n, A[10001], i, k,j, B[10001];
	FILE *f;
	DWORD t1,t2, timpBU, timpTD;

	srand(time(NULL));
	f = fopen("date.csv","w");
	fprintf(f, "lungime, nrOpBU, nrOpTD\n");
	//printf("TOP DOWN\n");
	//////////////////////////TD////////////////////////////
	for(j = 100; j <=10000; j += 100)
	{
		printf("j = %d\n", j); // ca sa vad ca lucreaza 
		nrOperatii = 0;
		n = j;
		
		timpTD = 0;
		for(i = 0; i < n; i++)
		{
			k = rand();
			B[i] = k; //generare acelasi sir pt BU
			t1 = GetTickCount();
			buildH_TD(A, i, k); //se adauga cate un element pentru constructia TD
			t2 = GetTickCount();
			timpTD += t2 - t1;
		}
		//printf("nr operatii = %d\n", nrOperatii);
		fprintf(f,"%d, %d, ", j, nrOperatii);
		//afiseazaH(A, 1, n, 0);
	
		//////////////////////////BU////////////////////////////
		nrOperatii = 0;
		//printf("\nBOTTOM UP\n\n");
		
		// constr heap
		t1 = GetTickCount();
		buildH_BU(B, n);	//se trimite sirul pt constructia BU
		t2 = GetTickCount();
		
		timpBU = t2 - t1;

		printf("l=%d, TD=%d, BU=%d\n",n , timpTD, timpBU );

		fprintf(f,"%d\n", nrOperatii);
		//printf("nr operatii = %d \n", nrOperatii);
	}
	//afisare
	//afiseazaH(B, 1, n, 0);


	return 0;
}