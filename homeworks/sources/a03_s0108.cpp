#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int heapsize=0;
long ass=0;
long comp=0;
int arrSize = 10;
int a[10000];
int a2[10000];
int a3[10000];



int left(int k)
{
	return 2*k+1;
}
int right(int k)
{
	return 2*k+2;
}
int parent(int k)
{
	return (k-1)/2;
}

void swap(int &a, int &b)
{
    int aux=a;
    a=b;
    b=aux;
}
/*
void generate(int* x)
{
    int n;
    n = arrSize;
	for (int i=0;i<n;i++)
	{
		x[i] = rand()%(MAX-MIN)+MIN;
	}
}
*/

void heapify(int *a, int i)
{
    int largest=i;
    int l=left(i);
    int r=right(i);
    comp++;
    if((l<heapsize)&&(a[l]>a[i])) largest=l;
    else largest=i;
    comp++;
    if((r<heapsize)&&(a[r]>a[largest])) largest=r;
    if(largest!=i)
    {
        ass=ass+3;
        swap(a[largest],a[i]);
        heapify(a,largest);
    }
}

void buildheap(int *a,int n)
{
	for(int i=n/2;i>0;i--)
	{
		heapify(a,i);
    }
}

void build_heap_top_down(int *a, int n)
{
    int i;
    int k;
    for (i=1;i<n;i++)
    {
        heapsize = i;
        k=i;
        while (k>0 && a[k] > a[parent(k)])
        {
            comp++;
            ass=ass+3;
            swap(a[k],a[parent(k)]);
            k = parent(k);
        }
        comp++;
    }
}

void heap_sort_top_down(int *a, int n)
{
    ass=0;
    comp=0;
    heapsize=n;
    build_heap_top_down(a,n);
    for(int i=n-1;i>=1;i--)
    {
        ass=ass+3;
        swap(a[0],a[i]);
        heapify(a,0);
        heapsize--;
    }
}

void heap_sort_bottom_up(int *a, int n)
{
    ass=0;
    comp=0;
    heapsize=n;
    buildheap(a,n);
    for(int i=n-1;i>=1;i--)
    {
        ass=ass+3;
        swap(a[0],a[i]);
        heapify(a,0);
        heapsize--;
    }
}

int partition(int* x, int p, int r)
{
    int y,i,j,temp;
    y = x[r];
    ass = ass+1;
    i = p-1;
    for (j=p;j<=r-1;j++)
    {
        comp++;
        if (x[j]<=y)
        {
            i=i+1;
            temp=x[i];
            x[i]=x[j];
            x[j]=temp;
            ass=ass+3;
        }
    }
    temp=x[i+1];
    x[i+1]=x[r];
    x[r]=temp;
    ass=ass+3;
    return i+1;
}

int* quickSort(int* x, int p, int r)
{
    int q;
    if (p < r)
    {
        q = partition(x,p,r);
        quickSort(x,p,q-1);
        quickSort(x,q+1,r);
    }
    return x;
}

int main()
{
    FILE *faverage = fopen("Avg_Heap_Quick.csv","w");
    FILE *fbest = fopen("Best_Quick.txt","w");
    FILE *fworst = fopen("Best_Quick.txt","w");
    int i;
    int j;
    long abu,atd,cbu,ctd;
    long averageQass, averageQcomp, worstQass, worstQcomp, bestQass, bestQcomp;
    int nr = 0;

    fprintf(faverage,"n ass_bu comp_bu sum_bu ass_td comp_td sum_td ass_quick comp_quick sum_quick\n");

    //cheking if quick sort and heap sort top down and bottom up are working properly
/*
    int sir[5];
    int n=5;
    for (int i = 0; i<n;i++)
        scanf("%d",&sir[i]);
    quickSort(sir,0,5);
    heap_sort_bottom_up(sir,4);
    heap_sort_top_down(sir,5);
    for (int i = 0; i<n;i++)
        printf("%d ",sir[i]);
*/

    //best case fot quick sort, the complexity is O(nlogn)

    //worst case for quck sort, the complexity is Q(n^2)

    //average case for quick sort O(nlogn) and heap sort
	for (i=100;i<10000;i=i+100)
	{
		arrSize = i;

		abu=0;atd=0;cbu=0;ctd=0;
		averageQass=0; averageQcomp=0; worstQass=0; worstQcomp=0; bestQass=0; bestQcomp=0;

		for (j=1;j<=5;j++)
		{
		    nr=0;
		  //  generate(a);
            for (long k=1;k<=i;k++)
			{

				nr++;
				a[nr]=rand();
			}
		    for (int t=0;t<arrSize;t++)
		    {
		        a2[t] = a[t];
		        a3[t] = a[t];
		    }
            ass=0;
            comp=0;
            heap_sort_top_down(a,arrSize);
            ctd+=comp;
            atd+=ass;

            ass=0;
            comp=0;
            heap_sort_bottom_up(a2,arrSize);
            cbu+=comp;
            abu+=ass;

            ass=0;
            comp=0;
            quickSort(a3,0,arrSize-1);
            averageQcomp+=comp;
            averageQass+=ass;
		}
        fprintf (faverage,"%d %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",arrSize,abu/5,cbu/5,abu/5+cbu/5,atd/5,ctd/5,atd/5+ctd/5,averageQass/5,averageQcomp/5,averageQass/5+averageQcomp/5);
	}

    return 0;
}
