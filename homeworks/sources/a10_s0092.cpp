/**
	Student:***ANONIM*** Diana ***ANONIM***
	Grupa:***GROUP_NUMBER***
	Problem specification: DFS - Depth-First Search + Topological Sort
	Comments:
	
**/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

typedef struct list_node{
	int val;
	list_node* next;
}list_node;

list_node *first, *last;
typedef struct edge{
	int p;
	int d;
}edge;

//1 - white
//2 - gray
//3 - black
typedef struct nod{
	nod* next;
	int value;
	int d,f;
	nod* predecessor;
	int color;
	bool tree_edge,back_edge,forward_cross_edge;
}nod;

edge e[10000];
nod* start[200];
nod* end[200];
int nr_edges,nr_vertices,op=0,timp;
bool sort=true;
int generate_random(int x) 
{
	return rand()%x;
}
void generate_graph(int nr_vertices,int nr_edges)
{
	int i=0;
	for (i=0;i<nr_vertices;i++)
	{
		start[i]=(nod*)malloc(sizeof(nod));
		start[i]->next=NULL;
		start[i]->value=i;
		end[i]=start[i];
	}
	i=0;
	while(i<nr_edges)
	{	
		int nr1 =generate_random(nr_vertices);
		int nr2 =generate_random(nr_vertices);
		if((nr1!=nr2))
		{
			bool ok=false;
			for (int j=0;j<i;j++)
			{
				if ((e[j].p==nr1)&&(e[j].d==nr2))
				{
					ok=true;
				}
			}
			if (ok==false)
			{	
				e[i].p=nr1;
				e[i].d=nr2;	
				printf("%d-%d\n",e[i].p,e[i].d);
				nod* aux=(nod*)malloc(sizeof(nod));
				aux->next=NULL;
				aux->value=e[i].d;
				end[e[i].p]->next=aux;
				end[e[i].p]=aux;
				i++;
			}
		}
	}
	
}
void print_adj_list(int nr_vertices)
{
	for (int i=0;i<nr_vertices;i++)
	{
		nod* aux=start[i];
		while (aux!=NULL)
		{
			
			if (aux==start[i])
				printf("%d -> ",aux->value);
			else printf("%d ",aux->value);
			aux=aux->next;
		}
		printf("\n");
	}
}
void change_color(int x,int color)
{
	for (int k=0;k<nr_vertices;k++)
	{
		nod* a=start[k];
		while (a!=NULL)
		{
			if (a->value==x)
			{
				a->color=color;
				
				if (color==1)
				{	
					a->tree_edge=true; 
					a->back_edge=false; 
					a->forward_cross_edge=false;
					printf("Edge %d is a tree edge\n",a->value);
				}
				else if (color==2)
				{
					a->forward_cross_edge=false; 
					a->back_edge=true; 
					a->tree_edge=false;
					printf("Edge %d is a back edge\n",a->value);

				}
				else 
				{
					a->forward_cross_edge=true; 
					a->back_edge=false; 
					a->tree_edge=false;
					printf("Edge %d is a forward of cross edge\n",a->value);
				}

			}
			a=a->next;
		}
	}
}
void insert(nod* x)
{
	if (last==NULL)
	{
		list_node* first=(list_node*)malloc(sizeof(list_node));
		first->val=x->value;
		first->next=NULL;
		last=first;
	}
	else
	{
		list_node* aux=(list_node*)malloc(sizeof(list_node));
		aux->next=last;//NULL;
		aux->val=x->value;
		
		last=aux;
	}
}
void DFS_visit(nod* u)
{
	timp++;
	u->d=timp;op+2;
	u->color=2; u->tree_edge=true; u->back_edge=false; u->forward_cross_edge=false;
	change_color(u->value,2);
	nod* v=start[u->value]->next;
	while(v!=NULL)
	{
		op++;
		if (v->color==2)
			sort=false;
		else if (v->color==1)
		{
			op+=2;
			v->predecessor=u;
			DFS_visit(v);
		}
		v=v->next;op+=2;
	}
	u->color=3; op+=2;
	printf("%d ",u->value);
	change_color(u->value,3);
	u->forward_cross_edge=true; u->back_edge=false; u->tree_edge=false;
	
	timp++;
	u->f=timp;op++;
	insert(u);
}
list_node* DFS()
{
	for (int i=0;i<nr_vertices;i++)
	{
		start[i]->color=1;op+=2;
		start[i]->tree_edge=true; start[i]->back_edge=false; start[i]->forward_cross_edge=false;
		change_color(i,1);
		start[i]->predecessor=NULL;
		
		for (int j=0;j<nr_vertices;j++)
		{
			nod* x=start[i]->next;
			while(x!=NULL)
			{
				x->tree_edge=true; x->back_edge=false; x->forward_cross_edge=false;
				x->predecessor=NULL;
				x=x->next;
			}
		}
	}
	timp=0;
	
	for (int i=0;i<nr_vertices;i++)
	{
		if (start[i]->color==1)
		{
			DFS_visit(start[i]);
			printf("\n");
		}
	}
	return last;
}
void topological_sort()
{
	// Din serminar
	
	DFS();
	list_node* aux=last;
	if (sort==false)
		printf("No topological sort");
	else
	{
		printf("\nTopological:");
		while(aux!=NULL)
		{
			printf("%d ",aux->val);
			aux=aux->next;
		}
	}

}
void test()
{
	first=NULL;
	last=first;
	nr_edges=5;
	nr_vertices=5;
	generate_graph(nr_vertices,nr_edges);
	print_adj_list(nr_vertices);
	
	topological_sort();
	//DFS();
	getch();
}
int main()
{
	srand(time(NULL));
	test();
	/*
	//evaluation 1
	FILE *pf1;
	pf1=fopen("DFS1.csv","w");
	fprintf(pf1,"nr_edges,nr_op\n");
	nr_vertices=100;
	for (nr_edges=1000;nr_edges<=5000;nr_edges+=100)
	{
		op=0;
		generate_graph(nr_vertices,nr_edges);
		DFS();
		fprintf(pf1,"%d,%d\n",nr_edges,op);
	}
	fclose(pf1);
	
	
	//evaluation 2
	FILE *pf2;
	pf2=fopen("DFS2.csv","w");
	fprintf(pf2,"nr_vertices,nr_op\n");
	nr_edges=9000;
	for(nr_vertices=110;nr_vertices<200;nr_vertices+=10)
	{
		op=0;
		generate_graph(nr_vertices,nr_edges);
		DFS();
		fprintf(pf2,"%d,%d\n",nr_vertices,op);
	}
	fclose(pf2);*/
}