﻿/* ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Cautare in adancime (DFS) este un algoritm pentru traversarea sau căutarea unei structuri de date de arbori sau grafic.
Incepem de la rădăcină (selectarea unui nod ca rădăcină în cazul grafic) și explorează cât posibil de-a lungul fiecărei ramuri.
Complexitate O(|v|+|e|)
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>


#define WHITE 0
#define GREY 1
#define BLACK 2

typedef struct varf
{
	int culoare, dist, parent, f;
	int	gi;
} varf;


typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

NODE *head[60001], *tail[60001];
varf VARF[60001];
int V[100001];
int timp;
int k, operatii;
int QUE[10001], nrq;
bool sort=true;

// Vizitare DFS
void Vizitare (int vertex)
{
	VARF[vertex].culoare=GREY;
	VARF[vertex].dist=timp;
	timp++;
	Node *temp;
	temp=head[vertex];
	operatii+=4;
	while (temp!=NULL)
	{
			int p=temp->val;
			temp=temp->next;
			operatii+=3;
			if(VARF[p].culoare==WHITE)
				{	
					operatii++;
					VARF[p].parent=vertex; 
					printf("(%d,%d) muchie de arbore\n", vertex, p);
					Vizitare(p);
				}
			else 
			{
				//operatii++;
				if (VARF[p].culoare==GREY)
				{
					sort=false;
					printf("(%d,%d) muchie inapoi\n", vertex, p);
				}
				else 
				{
					//operatii++;
					if (VARF[p].f<VARF[vertex].dist) 
							printf("(%d,%d) este muchie traversal\n", vertex, p);
					else printf("(%d,%d) este muchie inainte\n", vertex, p);
				}
			}
	}
	VARF[vertex].culoare=BLACK;
	VARF[vertex].f=timp;
	timp++;
	operatii+=3;
	//calcule pt sortare operatii logice
	QUE[nrq]=vertex;
	nrq--;
	
}

void DFS (int nrv)
{
	int i;
	for (i=1; i<=nrv; i++)
	{
		operatii+=2;
		VARF[i].culoare=WHITE; 
		VARF[i].parent=0;
	}
	timp=0;
	for (i=1; i<=nrv; i++)
	{
		operatii++;
		if (VARF[i].culoare==WHITE)
			Vizitare(i);
	}
}




bool verificare (int x, int y)
{
	NODE *p;
	p=head[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->next;
	}
	return true;

}

void generare(int nrv,int nrm)  //nrv= numarul de varfuri, nrm = numarul de muchii
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		head[i]=tail[i]=NULL;
	i=1;
	while (i<=nrm)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
				if(head[u]==NULL)
				{
					head[u]=(NODE *)malloc(sizeof(NODE *));
					head[u]->val=v;
					head[u]->next=NULL;
					tail[u]=head[u];
					i++;
				}
				else {
						if (verificare (u, v))
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;
							p->next=NULL;
							tail[u]->next=p;
							tail[u]=p;
							i++;
						}
					}
		}
	}
}

void print(int nrv)
{
	int i;
	NODE *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=head[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->next;
		}
		printf("\n");
	}
}

int main()
{
	int nrv, nrm;
	FILE *f;
	srand(time(NULL));
	nrv=7;
	nrm=7;
	nrq=nrv;
	srand(time(NULL));
    generare(nrv, nrm);
	print(nrv);
	DFS(nrv);

	if (sort==true)
		for (int i=1;i<=nrv; i++)
			printf("%d ", QUE[i]);
	else printf("Graful contine cicluri\n");

	f=fopen ("DFS.txt", "w");
	fprintf(f, "nrv  numar operatii\n");
	nrm=9000;
	for (nrv = 110; nrv<=200; nrv=nrv+10)
	{
		operatii=0;
		generare(nrv, nrm);
		DFS(nrv);
		fprintf(f, "%d %d\n", nrv, operatii);
	}
	fclose(f);
	getch(); 
    return 0;

}
