#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define MAX_SIZE 10000


void copiere_sir(int x[], int n, int y[])
{
	int i;
	for (i = n; i <= n; i++)
		y[i] = x[i];
}

void insertie(int x[],int n,int &nr_atr,int &nr_cmp)
{	int j,v;
	for (int i=2; i <= n; i++) { 
		v=x[i];
		j=1;
		nr_cmp++;
		while(x[j]<v)
		{j=j+1;
		 nr_cmp++;
		}
		for(int k=i;k>=j+1;k--)
		{
			x[k]=x[k-1];
			nr_atr++;
		}
		x[j]=v;
		nr_atr++;
	} 

}


//void generare(int nrelem)
//{for(int i=100;i<nrelem;i=i+100)

//}

void selectie(int x[],int n,int &nr_atr,int &nr_cmp)
{	int imin,temp;
	nr_atr=0;
	nr_cmp=0;
	for(int i=1;i<=n-1;i++){
		imin=i;

      for(int j=i+1;j<=n;j++){
		  nr_cmp++;
           if(x[j]<x[imin]){
			   imin=j;
		   }
	
      temp=x[i]; 
	  x[i]=x[imin];
      x[imin]=temp;
	  nr_atr++;
		   
      }
	}
}
/*void generaredate(int x[])
{	int t;
	srand(time(NULL));
	printf("numere generate: ");
	for(int i=1;i<=5;i++)
	{x[i]=rand();
			
			printf("%d ",x[i]);	
		}
	
	
}
*/
void bubblesort(int x[],int n,int &nr_atr,int &nr_cmp)
{
	int aux,da=0;
	nr_atr=0;
	nr_cmp=0;
	while(!da)
	{da=1;
	 for(int i=1;i<=n-1;i++)
	 {nr_cmp++;
		 if(x[i]>x[i+1])
			 {aux=x[i];
			  x[i]=x[i+1];
			  x[i+1]=aux;
			  nr_atr++;
			  da=0;
				}
	 }
	
	}
	
}

int main()
{
	//declarare variabile
	FILE *f;
	int  o[100],n = 100, m = 5, j, i, x[MAX_SIZE], y[MAX_SIZE], atr[4], cmp[4];
		srand (time(NULL));

	
	//fisier unde se scriu datele obtinute
	f = fopen("out.csv","w");
	srand (time(NULL));
	
	fprintf(f, "n,bubble_bst_atr,bubble_bst_cmp,bubble_bst_atr+cmp,selection_bst_atr,selection_bst_cmp,selection_bst_atr+cmp,insertion_bst_atr,insertion_bst_cmp,insertion_bst_atr+cmp,bubble_av_atr,bubble_av_cmp,bubble_av_atr+cmp,selection_av_atr,selection_av_cmp,selection_av_atr+cmp,insertion_av_atr,insertion_av_cmp,insertion_av_atr+cmp,bubble_ws_atr,bubble_ws_cmp,bubble_ws_atr+cmp,selection_ws_atr,selection_ws_cmp,selection_ws_atr+cmp,insertion_ws_atr,insertion_ws_cmp,insertion_ws_atr+cmp\n");
	
	
	
	for(n=100;n<=10000;n=n+100)
	{	
		atr[1] = 0;
		atr[2] = 0;
		atr[3] = 0;
		
		cmp[1] = 0;
		cmp[2] = 0;
		cmp[3] = 0;
		//scrie n in tabel
		fprintf(f, "%d,", n);
	
		//introduce valori pentru cazul favorabil, ord cresc
		for (i = 1 ; i <= n; i++)
			x[i] = i;
	
		//BUBBLE SORT BEST CASE APELARE
		bubblesort(x, n, atr[0], cmp[0]);
		fprintf(f, "%d,%d,%d,", atr[0], cmp[0], atr[0]+cmp[0]);

		//Selectie BEST CASE APELARE
		selectie(x, n, atr[0], cmp[0]);
		//scrie rezultate pentru SELECTIE best case
		fprintf(f, "%d,%d,%d,", atr[0], cmp[0], atr[0]+cmp[0]);

		//Insertie BEST CASE APELARE
		insertie(x, n, atr[0], cmp[0]);
		//scrie rezultate pentru INSERTIE best case
		fprintf(f, "%d,%d,%d,", atr[0], cmp[0], atr[0]+cmp[0]);
		//atribuire valori n cazul avrage de 5 ori
		for (j = 1; j <= m; j++){
			for (i = 1; i <= n; i++){
				x[i] = rand() % 10000;
				y[i] = x[i];
			}

			//BUBBLE SORT AVRAGE CASE APELARE
			bubblesort(x, n, atr[0], cmp[0]);
			atr[1] = atr[1] + atr[0];
			cmp[1] = cmp[1] + cmp[0];
			copiere_sir(y, n, x);

			//SELECTION BEST CASE APELARE
			selectie(x, n, atr[0], cmp[0]);
			atr[2] = atr[2] + atr[0];
			cmp[2] = cmp[2] + cmp[0];
			copiere_sir(y, n, x);

			//INSERTION BEST CASE APELARE
			insertie(x, n, atr[0], cmp[0]);
			atr[3] = atr[3] + atr[0];
			cmp[3] = cmp[3] + cmp[0];
			copiere_sir(y, n, x);
		}

		//scrie rezultate caz avrage
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,", atr[1] / 5, cmp[1] / 5, (atr[1] + cmp[1]) / 5, atr[2] / 5, cmp[2] / 5, (atr[2] + cmp[2]) / 5, atr[3] / 5, cmp[3] / 5, (atr[3] + cmp[3]) / 5);

		//valori pentru cazul defavorabil, ord descrescator
		for (i = 1; i <= n; i++){
			x[i] = n - i;
			y[i] = n - i;
		}

		//BUBBLE SORT WORST CASE APELARE
		bubblesort(x, n, atr[0], cmp[0]);
		//scrie rezultate pentru BUBBLE SORT best case
		fprintf(f, "%d,%d,%d,", atr[0], cmp[0], atr[0]+cmp[0]);

		//SELECTION WORST CASE APELARE
		selectie(x, n, atr[0], cmp[0]);
		//scrie rezultate pentru SELECTIE best case
		fprintf(f, "%d,%d,%d,", atr[0], cmp[0], atr[0]+cmp[0]);

		//INSERTION WORST CASE APELARE
		insertie(x, n, atr[0], cmp[0]);
		//scrie rezultate pentru INSERTIE best case
		fprintf(f, "%d,%d,%d\n", atr[0], cmp[0], atr[0]+cmp[0]);
	}
	printf("....");
	/*printf("\n");
	for(int i=1;i<=5;i++)
		o[i]=rand() % 100;
	for(int i=1;i<=5;i++)
		printf("%d ",o[i]);
	printf("\n");
	bubblesort(o,5,atr[0],cmp[0]);
	for(int i=1;i<=5;i++)
		printf("%d ",o[i]);
	selectie(o,5,atr[0],cmp[0]);
	for(int i=1;i<=5;i++)
		printf("%d ",o[i]);
	insertie(o,5,atr[0],cmp[0]);
	for(int i=1;i<=5;i++)
		printf("%d ",o[i]);
		*/
	getch();
	return 0;
}