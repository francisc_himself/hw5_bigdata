#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include<cstdlib>


typedef struct nod{
	int cheie;
	struct nod *urm;
}NOD;

typedef struct info{
	int val;
	int poz;
}INFO;

NOD *prim=NULL,*ultim=NULL,*cap[1001];
int dimensiune_heap,operatii=0;

//construire liste
void adauga_nod(int val)
{
	NOD *p=(NOD *)malloc(sizeof(NOD));
	p->cheie=val;
	if(prim==NULL)
	{
		prim=p;
		ultim=prim;
	}
	else
	{
		ultim->urm=p;
		ultim=p;
	}
	ultim->urm=NULL;
}

NOD *elimina_nod(int i)//elimina si returneaza primul nod din lista
{
	NOD *p=cap[i]->urm;
	if(p!=NULL)
		cap[i]->urm=p->urm;
	return p;
}

void adauga_lista(int indice)
{
	cap[indice]=(NOD *)malloc(sizeof(NOD));
	cap[indice]->urm=prim;
}

///////construire heap;
int parinte(int i)
{
	return i/2;
}

int fiu_stanga(int i)
{
	return 2*i;
}

int fiu_dreapta(int i)
{
	return 2*i+1;
}

void reconstituie_heap(INFO a[],int i)
{
	int l,r,minim;
	INFO aux;
	l=fiu_stanga(i);
	r=fiu_dreapta(i);
	operatii++;//!!!!!
	if(l<=dimensiune_heap && a[l].val<a[i].val)
		minim=l;
	else
		minim=i;
	operatii++;///!!!!!!!
	if(r<=dimensiune_heap && a[r].val<a[minim].val)
		minim=r;
	if(minim!=i)
	{
		operatii+=3;///!!!!!!!
		aux=a[i];
		a[i]=a[minim];
		a[minim]=aux;
		reconstituie_heap(a,minim);
	}
}

void init_heap()
{
	dimensiune_heap=0;
}

void push_heap(INFO a[],int x,int p)
{
	int i;
	INFO aux;
	dimensiune_heap++;
	operatii+=2;///!!!!
	a[dimensiune_heap].val=x;
	a[dimensiune_heap].poz=p;
	i=dimensiune_heap;
	while(i>1&&a[i].val<a[parinte(i)].val)
	{
		operatii+=4;///!!!!
		aux=a[i];
		a[i]=a[parinte(i)];
		a[parinte(i)]=aux;
		i=parinte(i);
	}
	operatii++;///!!!!
}

INFO pop_heap(INFO a[])
{
	INFO x=a[1];
	operatii++;///!!!!
	a[1]=a[dimensiune_heap];
	dimensiune_heap--;
	reconstituie_heap(a,1);
	return x;
}

void interclasare_liste(int k)
{
	int x;
	INFO a[1000];
	NOD *p;
	INFO inf;
	prim=NULL;
	ultim=NULL;
	init_heap();
	for(int i=1;i<=k;i++)
	{
		x=(elimina_nod(i))->cheie;
		operatii++;///!!!!
		push_heap(a,x,i);
	}
	while(dimensiune_heap>0)
	{
		inf=pop_heap(a);//extrag nodul din varful heap-ului
		operatii++;///!!!!
		adauga_nod(inf.val);//adaug nodul la lista finala
		operatii++;///!!!!!
		p=elimina_nod(inf.poz);//extrag primul nod din lista din care era ultimul nod
		if(p!=NULL)
			push_heap(a,p->cheie,inf.poz);
	}
}

void generare_k(int k,int n)
{
	int v[10001];
	int x,xa=0;
	operatii=0;
	srand(time(NULL));
	for(int i=1;i<=k;i++)
	{
		prim=NULL;
		ultim=NULL;
		for(int j=1;j<=n/k;j++)
		{
			x=xa+rand();
			adauga_nod(x);
			xa=x;
		}
		adauga_lista(i);
	}
	interclasare_liste(k);
	srand(1);
}

void generare_aleator_n_variabil()
{
	int n,k,operatii_k1,operatii_k2,operatii_k3;
	FILE *f=fopen("mediu1.csv","w");
	fprintf(f,"n,k=5,k=10,k=100\n");
	for(n=100;n<=10000;n+=100)
	{

		k=5;
		operatii=0;
		generare_k(k,n);
		operatii_k1=operatii;

		k=10;
		operatii=0;
		generare_k(k,n);
		operatii_k2=operatii;

		k=100;
		operatii=0;
		generare_k(k,n);
		operatii_k3=operatii;

		fprintf(f,"%d,%d,%d,%d\n",n,operatii_k1,operatii_k2,operatii_k3);
	}
	fclose(f);
}

void generare_aleator_k_variabil()
{
	int n,k;
	FILE *f=fopen("mediu_k1.csv","w");
	fprintf(f,"k,operatii\n");
	n=10000;
	for(k=10;k<=500;k+=10)
	{
		operatii=0;
		generare_k(k,n);
		fprintf(f,"%d,%d\n",k,operatii);
	}
	fclose(f);
}

void test()
{
	int a[]={1,4,7,9};
	int b[]={2,3,6,8,12};
	int c[]={0,5,10};
	int d[]={1,2,11,13};
	int k=4;

	for(int i=0;i<4;i++)
		adauga_nod(a[i]);
	adauga_lista(1);

	prim=NULL;
	ultim=NULL;
	for(int i=0;i<5;i++)
		adauga_nod(b[i]);
	adauga_lista(2);

	prim=NULL;
	ultim=NULL;
	for(int i=0;i<3;i++)
		adauga_nod(c[i]);
	adauga_lista(3);

	prim=NULL;
	ultim=NULL;
	for(int i=0;i<4;i++)
		adauga_nod(d[i]);
	adauga_lista(4);

	printf("prima lista\n");
	for(NOD *q=cap[1]->urm;q!=NULL;q=q->urm)
		printf("%d ",q->cheie);
	printf("\na doua lista\n");
	for(NOD *q=cap[2]->urm;q!=NULL;q=q->urm)
		printf("%d ",q->cheie);
	printf("\na treia lista\n");
	for(NOD *q=cap[3]->urm;q!=NULL;q=q->urm)
		printf("%d ",q->cheie);
	printf("\na patra lista\n");
	for(NOD *q=cap[4]->urm;q!=NULL;q=q->urm)
		printf("%d ",q->cheie);
	printf("\nlitele interclasate\n");
	interclasare_liste(k);
	for(NOD *q=prim;q!=NULL;q=q->urm)
		printf("%d ",q->cheie);
}


int main(void)
{
	test();  //pentru a arata functionalitatea programului
	generare_aleator_n_variabil();
	generare_aleator_k_variabil();
	getch();
	return 0;
}
