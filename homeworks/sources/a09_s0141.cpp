#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define white 0
#define gray 1
#define black 2
#define inf 99999

typedef struct Struct
{
	int color, d, parent;
} Struct;


int nr_operatii=0;

typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

NODE *first[60001], *last[60001];
Struct Struct[60001];
int nr_elemente;
int Q[60001];

void BFS (int start, int nr_varfuri)
{
	int u;
	nr_elemente=0;
	nr_operatii=0;
	for (u=1; u<=nr_varfuri; u++)
		if (u!=start)
		{
			Struct[u].color=white;
			Struct[u].d=nr_operatii;
			Struct[u].parent=0;
			nr_operatii++;
		}
	nr_operatii++;
	Struct[start].color=gray;
	Struct[start].d=0;
	Struct[start].parent=0;
	nr_elemente++;
	Q[nr_elemente]=start;
	int i;
	while(nr_elemente!=0)
	{
		u=Q[1];
		nr_operatii++;
		printf("  Nod selectat:%d\n",u);

		for ( i=1; i<nr_elemente; i++)
			Q[i]=Q[i+1];
		nr_elemente--;
		Node *aux;
		aux=first[u];
		nr_operatii++;
		while (aux!=NULL)
		{
			nr_operatii++;
			int v=aux->val;
			aux=aux->next;
			if (Struct[v].color==white)
			{
				Struct[v].color=gray;
				Struct[v].d=Struct[u].d+1;
				Struct[v].parent=u;
				nr_elemente++;
				Q[nr_elemente]=v;
			}
		}
		Struct[u].color=black;
		nr_operatii++;
	}
}

bool duplicate (int x, int y)
{
	NODE *p;
	p=first[x];
	nr_operatii++;
	while(p!=NULL)
	{
		nr_operatii++;
		if(y==p->val)
			return false;
		p=p->next;
	}
	return true;

}

void createAdjacencyList(int nr_varfuri,int nr_muchii)
{
	int i,u,v;
	for(i=0;i<=nr_varfuri;i++)
		first[i]=last[i]=NULL;
	i=1;
	while (i<=nr_muchii)
	{
		u=rand()%nr_varfuri+1;
		v=rand()%nr_varfuri+1;
		if (u!=v)
		{
				if(first[u]==NULL)
				{
					first[u]=(NODE *)malloc(sizeof(NODE *));
					first[u]->val=v;
					first[u]->next=NULL;
					last[u]=first[u];
					i++;
				}

				else {
					if (duplicate (u, v) && duplicate(v,u)==true)
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;
							p->next=NULL;
							last[u]->next=p;
							last[u]=p;
							i++;
						}
					}
		}
	}
}


void print(int nr_varfuri)
{
	int i;
	NODE *p;
	for(i=1;i<=nr_varfuri;i++)
	{
		printf("%d : ",i);
		p=first[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->next;
		}
		printf("\n");
	}
}


int main()
{
	int nr_muchii=0, nr_varfuri=0;
	nr_varfuri=5;
	nr_muchii=8;
	srand(time(NULL));
	createAdjacencyList(nr_varfuri, nr_muchii);
	printf("\nLista de adiacenta:\n");
	print(nr_varfuri);
	printf("\nBFS:\n");
	BFS(1, nr_varfuri);
	
	
	/*int i;
	FILE *f1=fopen("D://tema91.xls","w");
	
	for(i=1000;i<5000;i+=100)
	{
		nr_varfuri=100;
		createAdjacencyList(nr_varfuri,i);
		BFS(1,nr_varfuri);
		fprintf(f1,"%d\t %d\n",i,nr_operatii);
	}
	fclose(f1);
	FILE *f2=fopen("D://tema92.xls","w");
	for(i=140;i<=200;i+=10)
	{
		nr_muchii=9000;
		createAdjacencyList(i,nr_muchii);
		BFS(1,i);
		fprintf(f2,"%d\t %d\n",i,nr_operatii);
		nr_operatii=0;
	}
	fclose(f2);*/
	
	getch();
    return 0;
}
