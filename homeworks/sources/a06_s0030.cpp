#include <stdio.h>
#include <conio.h>
#include <cstdlib> 

typedef struct nod{
	struct nod *l, *r;
	int size;
	int key;
	struct nod *parinte;
}nod;

nod *A[20], *r;
int k=1;
int b[100];
int nr=0;

nod *BUILD_TREE(nod *rad, int stg,int dr)
{
 int m;
 int z=0;
 nr++;
  if (stg<=dr)
  {
	  m=(stg+dr)/2;
	  nr++;
	   rad->key=m;
	   nr++;
	   rad->size=dr-stg+1;
	   printf("%d ", rad->key);
		rad->l=(nod*)malloc(sizeof(nod));
		nr++;
		 rad->l->parinte=rad;
	   rad->l=BUILD_TREE(rad,stg, m-1);
	   rad->r=(nod*)malloc(sizeof(nod));
	   nr++;
		rad->r->parinte=rad;
	   rad->r=BUILD_TREE(rad,m+1, dr);
	   return rad;
  }
  else return NULL;

}

nod *SELECT(nod *x, int j)
{
	int r;
	if (x->l!=NULL) { nr=nr+2; r=x->l->size;}
	else r=1;

	if (j==r)
	{
		nr++;
		return x;
	}
	else if (j<r)
	{
	    nr++;
		return SELECT(x->l, j);
	}
	else return SELECT(x->r, j-r);
	

}

nod *ARBORE_MINIM(nod *x)
{
	while (x->l!=NULL)
	{
		nr++;
		x=x->l;
	}
	return x;
}

nod *ARBORE_SUCCESOR(nod *x)
{
	nod *m;
	nod *y;
	if (x->r!=NULL)
	{ nr=nr+2; m=ARBORE_MINIM(x->r); return m;}
	nr++;
	y=x->parinte;
	while((y!=NULL)&&(x==y->r))
	{
		nr=nr+2;
		x=y;
		y=y->parinte;
	}
	return y;
}

nod *Delete(nod *rad, nod *x)
{
	nod *y;
	nod *z;
	int a;
	if ((x->l==NULL)||(x->r==NULL)) {nr=nr+2; y=x;}
	else{nr=nr+2; y=ARBORE_SUCCESOR(x);}

	if (y->l!=NULL){nr=nr+2; z=y->l;}
		else
		{
			nr=nr+2; z=y->r;
		}
		if (z!=NULL) {nr=nr+2; z->parinte=y->parinte;}
		if (y->parinte!=NULL) {nr=nr+2; rad=z;}
		else if (y->parinte!=NULL) if (y==y->parinte->l){nr=nr+2; y->parinte->l=z;}
		else {nr=nr+2;y->parinte->r=z;}
		if (y!=x)
		{
			nr=nr+2;
			x->key=y->key;
		
	}
		return y;
}



nod *resize(nod *x)
{
	nr=nr+2;
	x=x->parinte;
    if (x!=NULL)	x->size--;
	return x;
}

void JOSEPHUS(int n, int m)
{
	nod *radacina;
	radacina=(nod*)malloc(sizeof(nod*));
	radacina->parinte=NULL;
	nod *x;
	radacina=BUILD_TREE(radacina,1,n);
	int j=m;
	printf("\n");
	for (int z=n; z>=1; z--)
	{
		
		x=SELECT(radacina,j);
		printf("%d ", x->key);
		Delete(radacina, x);
		x=resize(x);
		j=(j+m-2)%n+1;
	}

}


void main()
{
	
	JOSEPHUS(7,3);
	
	/*FILE *f;
	f=fopen("fisier.csv", "w");
	fprintf(f,"n, nr. de operatii \n");
	for (int i=100; i<=10000; i=i+100)
	{
		nr=0;
		JOSEPHUS(i, i/2);
		fprintf(f,"%d,%d\n", i, nr);

	}*/
	getch();
}