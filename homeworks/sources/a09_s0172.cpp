// ***ANONIM*** ***ANONIM***
// Grupa ***GROUP_NUMBER***

// Eficienta functiei de BFS este de O(V + E).

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define ALB 0
#define GRI 1
#define NEGRU 2
#define INF 10000

//structura reprezinta proprietatile unui nod
typedef struct Propr
{
	int cul;    //culoarea
	int dist;   //distanta
	int par;    //parintele
}PROPR;

//structura reprezinta un nod
typedef struct Nod
{
    int val;
    struct Nod* next;
}NOD;

NOD *head[60001], *tail[60001];
PROPR prop[60001];

int V[100001];
int k;          // index folosit pentru vrectorul ce reprezinta coada
int nr_op;      // nr total de opeartii efectuate
int nivel = 1;  // folosit la pretty-print

int Q[60001];  // vector ce reprezinta coada

//Functia de BFS
void BFS (int s, int V)
{
	int u;
	
	for (u=1; u<=V; u++){
		if (u!=s){
			prop[u].cul = ALB;
			prop[u].dist = INF;
			prop[u].par = 0;
		}
	}

	prop[s].cul = GRI;
	prop[s].dist = 0;
	prop[s].par = 0;
	//enqueue
	k=1;
	Q[k]=s;
	nr_op+=5;
	
	printf("%d \n", s);
	
	while(k!=0){
		//dequeue
		u = Q[1];
		nivel++;

		for(int i=1; i<k; i++){
			Q[i]=Q[i+1];
		}
		
		k--;
		Nod *temp;
		temp = head[u];
		nr_op++;
		
		while (temp!=NULL){
			int v = temp->val;
			temp = temp->next;
			nr_op += 3;
			
			if (prop[v].cul==ALB){
				prop[v].cul = GRI;
				prop[v].dist = prop[u].dist + 1;
				prop[v].par = u;
				//enqueue
				k++;
				Q[k]=v;
				
				nr_op+=4;

				for(int i=0; i<nivel; i++){
					printf(" ");
				}
				
				printf("%d \n", v);
			}
		}
		
		prop[u].cul = NEGRU;
		nr_op++;
	}
}

// functia verifica daca muchia (u,v) este unica
bool verifica_unic(int u, int v){
	NOD *p;
	
	p = head[u];
	
	while(p!=NULL){
		if(v == p->val){
			return false; 
		}
		p = p->next;
	}
	
	return true;
}

void generate_random(int V,int E){
	int u,v;
	
	//initializam listele cu NULL
	for(int i=0;i<=V;i++){
		head[i] = NULL;
		tail[i] = NULL;
	}

	int m = 1;

	while (m <= E){
		u = rand() % V + 1;
		v = rand() % V + 1;
		
		if (u != v){
			if(head[u]==NULL){
				head[u] = (NOD *)malloc(sizeof(NOD *));
				head[u]->val = v;
				head[u]->next = NULL;					tail[u] = head[u];
				
				m++;
			}
			else{
				if(verifica_unic(u, v)){
					NOD *p;
					p = (NOD *)malloc(sizeof(NOD *));
					p->val = v;
					p->next = NULL;
					tail[u]->next = p;
					tail[u] = p;
					
					m++;
				}
			}
		}
	}
}

// functie pentru afisarea tuturor muchiilor
void print_muchii(int V){
	int i;
	NOD *p;

	printf("Muchii :\n");

	for(i=1;i<=V;i++){
		printf("%d : ",i);
		p=head[i];
		
		while(p!=NULL){
			printf("%d ",p->val);
			p = p->next;
		}
		printf("\n");
	}
}

void test(){
	int E = 7;
	int V = 5;

	srand(time(NULL));
	
	generate_random(V, E);
	
	print_muchii(V);
	printf("\n");
	
	BFS(1, V);
}

void resolve(){
	int E, V;
	FILE *f1, *f2;

	srand(time(NULL));

	f1 = fopen ("results1.txt", "w");
	fprintf(f1, "E nr_op\n");
	
	// primul caz
	V=100;
	for (E = 1000; E<=5000; E=E+100){
		printf("%d\n",E);
		nr_op = 0;
		generate_random(V, E);
		BFS(1, V);
		
		fprintf(f1,"%d %d\n", E, nr_op);
	}
	
	f2 = fopen ("results2.txt", "w");
	fprintf(f2, "V nr_op\n");
	
	//al doilea caz
	E=9000;
	for (V = 100; V<=200; V=V+10){
		printf("%d\n",V);
		nr_op=0;
		generate_random(V, E);
		BFS(1, V);
		
		fprintf(f2, "%d %d\n", V, nr_op);
	}
	
	fclose(f1);
	fclose(f2);
}

int main()
{
	test();

	//resolve();

	getch(); 
    return 0;
}