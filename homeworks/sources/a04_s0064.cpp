#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <limits.h>

#define MAX 10000
int n=100;
int dimHeap=0;

int nrComp,nrAtrib;

typedef struct nod{
			int val;
			struct  nod *urm;
			}Nod;

Nod *listaListe[1000];
Nod *rezultat;

typedef struct heapElement{
					int val;
					int index;
					}Heap;
Heap heap[1000];



void creareDescrescator(int *a,int n)
{
	int i;
	
	a[0]=rand()%MAX;
	for(i=1;i<n;i++)
	{
		a[i]=a[i-1] - rand()%MAX;
	}
}


void creareCrescator(int *a,int n)
{	
	int i;
	
	a[0]=rand()%MAX;

	for(i=1;i<n;i++)
	{
		a[i]=a[i-1] + rand()%MAX;
	}
	
}


int parent ( int i)
{
	return i/2;
}

int left ( int i)
{
	return (2*i);
}

int right (int i)
{
	
	return (2*i+1) ;
}


void heapify(int i)
{

	int min;
	int	l=left(i);
	int	r=right(i);
	nrComp=nrComp+2;
	
	if ((l<=dimHeap) && (heap[l].val<heap[i].val))
	{
		min=l;
	}
	else 
	{
		min=i;
	}
	if ((r<=dimHeap) && (heap[r].val<heap[min].val))
	{
		min=r;
	}
	if(min!=i)
	{
		Heap x=heap[i];
		heap[i]=heap[min];
		heap[min]=x;
		nrAtrib=nrAtrib+3;
		heapify(min);
	}
}

Heap H_Pop()
{
	Heap x;
	
	x=heap[1];
	heap[1]=heap[dimHeap];
	dimHeap--;
	heapify(1);
	return x;
}

void inserare(Nod **elem , Nod *n)
{
	n->urm=NULL;

	if(*elem==NULL)
	{
		*elem=n;
	}
	else
	{
		(*elem)->urm=n;
		*elem=n;
	}
}
void H_Push(int value,int index)
{
	dimHeap=dimHeap+1;
	heap[dimHeap].val=value;
	heap[dimHeap].index=index;
	int i=dimHeap;
	nrAtrib=nrAtrib+1;
	nrComp=nrComp+1;
	while((i>1) &&(heap[parent(i)].val>heap[i].val))
	{
		Heap y=heap[i];
		heap[i]=heap[parent(i)];
		heap[parent(i)]=y;
		nrAtrib=nrAtrib+3;
		nrComp=nrComp+1;
		i=parent(i);
	}
}

void afisareLista(Nod *p)
{
	printf("Lista: ");

	while(p!=NULL)
	{
		printf("%d ",p->val);
		p=p->urm;
	}

	printf("\n");
}


void creareKliste(int nTotal,int nrListe)
{
	int n=nTotal/nrListe;
	int* sortedArray=(int*) malloc(sizeof(int)*n);
	for(int i=0;i<nrListe;i++)
	{
		creareCrescator(sortedArray,n);
		nod *elem=NULL;
		nod *prim=NULL;
		for(int j=0;j<n;j++){
			nod* x=(nod*) malloc(sizeof(Nod));
			x->val=sortedArray[j];
			inserare(&elem,x);
			if(j==0)
			{
				prim=elem;
			}
			elem=x;
			
		}
		listaListe[i]=prim;
		afisareLista(listaListe[i]);
	}
}

void ordonare(int k)
{
	int i=0;
	Nod *prim=NULL;
	int value;
	Heap aux;
	for(int i=0;i<k;i++)      
	{
		nrAtrib=nrAtrib+1;
		value=listaListe[i]->val;
		H_Push(value,i); 	
		listaListe[i]=listaListe[i]->urm;
	}
	nrComp=nrComp+1;
	while (dimHeap>0)
	{
		aux=H_Pop();
		nod *x=(nod*) malloc(sizeof(nod));
		x->val=aux.val;
		nrAtrib=nrAtrib+2;
		nrComp=nrComp+3;
		
		inserare(&rezultat,x);
		if(i==0)
		{
			nrAtrib=nrAtrib+1;
			prim=rezultat;
		}

		i++;
		if ( (listaListe[aux.index])!=0 )
			{
				value=listaListe[aux.index]->val;
				H_Push(value,aux.index);
				listaListe[aux.index]=listaListe[aux.index]->urm;
				nrAtrib=nrAtrib+1;
			}
 	}
	rezultat=prim;
	nrAtrib=nrAtrib+1;
	
}

void main()
{
	srand( (unsigned int) time(NULL));
	
	int i,k=4,n=20;
	creareKliste(n,k);
	for(i=0;i<k;i++);
	{
		afisareLista(listaListe[i]);
	}
	ordonare(k);
	afisareLista(rezultat);
	

	getch();
}