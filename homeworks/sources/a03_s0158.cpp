﻿/*
Quicksort is a sorting algorithm whose worst-case running time is Θ(n^2) on an input array of n
numbers. In spite of this slow worst-case running time, quicksort is often the best practical
choice for sorting because it is remarkably efficient on the average: its expected running time
is Θ(nlgn), and the constant factors hidden in the Θ(nlgn) notation are quite small. It also
has the advantage of sorting in place.
The average-case running time of quicksort is much closer to the best case than to the worst
case.

Its average-case running time is Θ(nlgn), though, and it generally outperforms
heapsort in practice. It is a popular algorithm for sorting large input arrays.
*/
#include "stdio.h"
#include "math.h"
#include "Profiler.h"
#define MINUSINFINITY -30000
#define MAX_SIZE 10001
void Build_Max_Heap_Bottom_Up(int *A, int n);
void Build_Max_Heap_Top_Down(int *A, int n);
void quickSort(int *A, int p, int r);
void heapSort(int *A, int n);

// global variables
int heap_size;
Profiler profiler("Heap Construction");
int Array[MAX_SIZE];
int repeat = 5;
int dim;

/* copy the content of Array into aux */
void copy(int *aux, int n) {
    for(int i = 1; i <= n; i++)
        aux[i] = Array[i];
}

void averageCase() {
	int aux[MAX_SIZE]; // auxiliar array in which we copy the random generated one
	for(int n = 101; n < 10001; n += 100) {
		dim = n;
		FillRandomArray(Array, n);
		for(int i = 1; i <= 5; i++) { // do the measurements 5 times on each set of input data
			copy(aux, n);
			heapSort(aux, n);
			copy(aux, n);
			quickSort(aux, 1, n);						
		}	
	}
	profiler.createGroup("Heap vs Quick Sort Average", "heap", "quick");
	profiler.showReport();
}

void worstCaseQuick() {
	int aux[MAX_SIZE]; // auxiliar array in which we copy the random generated one
	for(int n = 101; n < 10001; n += 100) {
		dim = n;
		FillRandomArray(Array, n, 10, 30000, false, 2);
		copy(aux, n);
		quickSort(aux, 1, n);
			}
	profiler.createGroup("QuickCase_Worst", "quick");
	profiler.showReport();
}

void bestCaseQuick() {
	int aux[MAX_SIZE]; // auxiliar array in which we copy the random generated one
	for(int n = 101; n < 10001; n += 100) {
		dim = n;
		FillRandomArray(Array, n, 10, 30000, false, 1);
		copy(aux, n);
		quickSort(aux, 1, n);
			}
	profiler.createGroup("QuickSort_Best_Case", "quick");
	profiler.showReport();
}

void Max_Heapify(int *A, int i, int n) {
	int l = 2 * i;
	int r = 2 * i + 1;
	int largest;
	if(l <= n && A[l] > A[i]) {
		largest = l;
	}
	else largest = i;
	if(r <= n && A[r] > A[largest]) {
		largest = r;
	}
	profiler.countOperation("heap", dim - 1, 2);
	if(largest != i) {
		profiler.countOperation("heap", dim - 1, 3);
		int temp = A[i];
		A[i] = A[largest];
		A[largest] = temp;
		Max_Heapify(A, largest, n);
	}

}

void Build_Max_Heap_Bottom_Up(int *A, int n) {
	//profiler.countOperation("bottom_up_count", n, 0); 
	for(int i = n/2; i >= 1; i--) // take all non-leaf nodes
		Max_Heapify(A, i, n);
}

void Build_Max_Heap_Top_Down_DC(int *A, int n, int i) {
	if(i <= n/2) {
		Build_Max_Heap_Top_Down_DC(A, n, 2 * i);
		Build_Max_Heap_Top_Down_DC(A, n, 2 * i + 1);
		Max_Heapify(A, i, n);
	}

}

void Heap_Increase_Key(int *A, int k, int key, int n) {
	int i = k;
	if(key < A[i]) {
		printf("new key is smaller than current key");
		return;
	}
	profiler.countOperation("top_down_count", n - 1);
	A[i] = key;
	profiler.countOperation("top_down_count", n - 1, 2); // + 1 for while exit
	while(i > 1 && A[i/2] < A[i]){
		profiler.countOperation("top_down_count", n - 1, 4);
		int temp = A[i];
		A[i] = A[i/2];
		A[i/2] = temp;
		i = i/2;
	}

}

void Max_Heap_Insert(int *A, int key, int n) {
	heap_size++;
	A[heap_size] = MINUSINFINITY;
	profiler.countOperation("top_down_count", n - 1);
	Heap_Increase_Key(A, heap_size, key, n);
}

void Build_Max_Heap_Top_Down(int *A, int n) {
	//profiler.countOperation("top_down_count", n, 0);
	heap_size = 1;
	for(int i = 2; i <= n; i++)
		Max_Heap_Insert(A, A[i], n);
}

void heapSort(int *A, int n) {
	heap_size = n;
	Build_Max_Heap_Bottom_Up(A, n);
	for(int i = n; i >= 2; i--) {
		int temp = A[1];
		A[1] = A[i];
		A[i] = temp;
		profiler.countOperation("heap", dim - 1, 3);
		heap_size--;
		Max_Heapify(A, 1, heap_size);
	}
}

int partition(int *A, int p, int r) {
	int x = A[(p+ r) / 2]; // A[r]
	profiler.countOperation("quick", dim - 1, 1);
	int i = p - 1; //  dont know for sure if to count i or not 
	for(int j = p; j <= r - 1; j++) {
		profiler.countOperation("quick", dim - 1, 1);
		if(A[j] <= x) {
			i++;
			profiler.countOperation("quick", dim - 1, 3);
			int temp = A[i]; // swap A[i] -- A[j]
			A[i] = A[j];
			A[j] = temp;
		}
	}
	profiler.countOperation("quick", dim - 1, 3);
	int temp = A[i + 1]; // swap A[i + 1] -- A[r]
	A[i + 1] = A[r];
	A[r] = temp;
	return i + 1;
}

void quickSort(int *A, int p, int r) {
	int q;
	if(p < r) {
		q = partition(A, p, r);
		profiler.countOperation("quick", dim - 1, 1);
		quickSort(A, p, q - 1);
		quickSort(A, q + 1, r);
	}
}



void inorder(int *A, int k, int level, int n) {
	int i;
	if (k <= n) {
		inorder(A, 2 * k + 1, level + 1, n) ;
		for (i = 0; i <= level ; i++) 
			printf("   "); /* for nice listing */
		printf ("%d\n", A[k]) ;
		inorder(A, 2 * k, level + 1, n) ;
	}
}

int main() {
	//int n = 10;
	//int A[MAX_SIZE] = {-10000, 4, 1, 3, 2, 16, 9, 10, 14, 8, 7}; 
	//int B[MAX_SIZE] = {-10000, 4, 1, 3, 2, 16, 9, 10, 14, 8, 7};

	//heapSort(A, n);
	//quickSort(B, 1, n);
	//Build_Max_Heap_Top_Down(B, n);
	//Heap_Increase_Key(B, 9, 15);
	//heap_size = 10;
	//Max_Heap_Insert(B, 25);
	//Build_Max_Heap_Top_Down(B, n);
	/*
	for(int i = 1; i <= n; i++)
		printf("%d ", A[i]);
	printf("\n");
	for(int i = 1; i <= n; i++)
		printf("%d ", B[i]);
	*/
	//inorder(A, 1, 0, n);
	//printf("\n");
	//inorder(B, 1, 0, n);
	//averageCase();
	//worstCaseQuick();
	bestCaseQuick();
	return 0;
}