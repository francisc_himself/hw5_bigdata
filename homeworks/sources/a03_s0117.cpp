/*
In the average case testing, between the two sorting algorithns, the QuickSort sorting algorithm is better than HeapSort;

- The running time of the HeapSort algorithm is O(n*log n);
- The running time of the QuickSort algorithm is O(n^2);

For the Quicksort Best & Worst case testing:
- Best case running time for QuickSort is O(n*log n);
- Worst case running time for QuickSort is O(n^2);
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler;

const int VMAX = 10000;
const int VMIN = 0;


int* generate_random(int n)
{
  int* a=new int[n];
  srand(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=rand()%(VMAX-VMIN) +VMIN;
  return a;
}

int* generate_ordered(int n)
{
 int* a=new int[n]; 
 int maxdiff=5;
 a[0]=rand()%maxdiff;
  for (int i=1;i<n;i++)
    a[i]=a[i-1]+rand()%maxdiff;
  return a;
}

int* generate_decreasing(int n)
{
 int* a=new int[n]; 
 int maxdiff=5;
 a[n-1]=rand()%maxdiff;
  for (int i=n-2;i>0;i--)
    a[i]=a[i+1]+rand()%maxdiff;
  return a;
}

int * deep_copy(int *a, int n)
{
	int *b = new int[n];
	for(int i = 0; i<n;i++)
	{
		b[i] = a[i];
	}
	return b;
}

void rec_heap_BU(int heap[],int i,int k,int n)
{
	int aux;
    int l = 2*i;
    int r = 2*i+1;
    int max;
	profiler.countOperation("HeapSort Comparisons",n);
    if ((l<=k)&&(heap[l]>heap[i]))
		max=l;
    else
        max=i;
	  profiler.countOperation("HeapSort Comparisons",n);
    if ((r<=k)&&(heap[r]>heap[max]))
        max=r;
	
    if (max!=i)
	{
        aux = heap[i];
        heap[i] = heap[max];
        heap[max] = aux;
		profiler.countOperation("HeapSort Assignments",n,3);
        rec_heap_BU(heap,max,k,n);
	}
}

void build_heap_BU(int heap[],int n)//BOTTOM UP
{
    for (int i = n/2 ; i>0 ; i--)
        rec_heap_BU(heap,i,n,n);
}

void heap_sort(int a[],int heap[],int k,int n)
{
    build_heap_BU(heap,k);
    while (k!=0)
    {
        a[k] = heap[1];
        heap[1]=heap[k];
        k--;
		profiler.countOperation("HeapSort Assignments",n,2);
        rec_heap_BU(heap,1,k,n);
    }
}


void quick_sort(int a[], int l, int r,int n)
{
    int i=l;
	int j=r;
    int aux;
    int piv=a[(l + r) / 2]; 
	 //int piv=a[l];
	profiler.countOperation("QuickSort Assignments",n);
    while (i<=j)
    {
		profiler.countOperation("QuickSort Comparisons",n);
        while (a[i]<piv)
        {
            i++;
            profiler.countOperation("QuickSort Comparisons",n);
        }

        profiler.countOperation("QuickSort Comparisons",n);
        while (a[j]>piv)
        {
            j--;
            profiler.countOperation("QuickSort Comparisons",n);
        }
        profiler.countOperation("QuickSort Comparisons",n,2);

        if (i<=j)
        {
            aux=a[i];
            a[i]=a[j];
            a[j]=aux;
            i++;
            j--;
            profiler.countOperation("QuickSort Assignments",n,3);
        }
    }
    profiler.countOperation("QuickSort Comparisons",n,2);
    if (l<j)
        quick_sort(a,l,j,n);
    profiler.countOperation("QuickSort Comparisons",n,2);
    if (i<r)
        quick_sort(a,i,r,n);
}

void quick_sort_worst(int a[], int l, int r,int n)
{
    int i=l;
	int j=r;
    int aux;
    
	 int piv=a[l];
	profiler.countOperation("QuickSort Assignments",n);
    while (i<=j)
    {
		profiler.countOperation("QuickSort Comparisons",n);
        while (a[i]<piv)
        {
            i++;
            profiler.countOperation("QuickSort Comparisons",n);
        }

        profiler.countOperation("QuickSort Comparisons",n);
        while (a[j]>piv)
        {
            j--;
            profiler.countOperation("QuickSort Comparisons",n);
        }
        profiler.countOperation("QuickSort Comparisons",n,2);

        if (i<=j)
        {
            aux=a[i];
            a[i]=a[j];
            a[j]=aux;
            i++;
            j--;
            profiler.countOperation("QuickSort Assignments",n,3);
        }
    }
    profiler.countOperation("QuickSort Comparisons",n,2);
    if (l<j)
        quick_sort_worst(a,l,j,n);
    profiler.countOperation("QuickSort Comparisons",n,2);
    if (i<r)
        quick_sort_worst(a,i,r,n);
}

void main()
{
	int *heap;
	int *a;
	int *a1;
	//Test
	/*
	  a=generate_random(10);
	  a1=deep_copy(a,10);
	  heap=deep_copy(a,10);

	  printf("The random array is:\n");
	  for (int i=1;i<10;i++)
	  {
		  printf("%d ",a[i]);
		  heap[i]=a[i];
	  }
	  heap_sort(a,heap,9,9);
	  printf("\n");
	  printf("\n");
	  printf("\nAfter HeapSort:\n");
	  for (int i=1; i<10;i++)
	  {
		  printf("%d ",a[i]);
	  }
	  quick_sort(a1,1,9,9);
	  printf("\n");
	  printf("\n");
	  printf("\nAfter QuickSort:\n");
	  for (int i=1; i<10;i++)
		  printf("%d ",a1[i]);
	  getch();
	  */


	
	// Average Case Test
	for (int j=0;j<5;j++)
		for (int n=100;n<10000;n=n+100)
		{
		  printf("n=%d\n",n);
		  a = generate_random(n);
		  a1 = deep_copy(a,n);
		  heap = deep_copy(a,n);
		  heap_sort(a,heap,n-1,n-1);
		  quick_sort(a1,1,n-1,n-1);
		}
	profiler.addSeries("QuickSort Total","QuickSort Comparisons","QuickSort Assignments");
	profiler.addSeries("HeapSort Total","HeapSort Comparisons","HeapSort Assignments");
	profiler.createGroup("Average Testing","QuickSort Total","HeapSort Total");
	profiler.createGroup("Comparisons","HeapSort Comparisons","QuickSort Comparisons");
	profiler.createGroup("Assignments","HeapSort Assignments","QuickSort Assignments");
	
	profiler.showReport();
	


	//QuickSort Best Case
	/*
	printf("QuickSort Best Case Sort\n");
	for (int n=100;n<10000;n=n+100)
	{
		printf("n=%d\n",n);
		a=generate_ordered(n);
		quick_sort(a,1,n-1,n-1);
	}
		profiler.createGroup("Best Case QuickSort","QuickSort Assignments");
	    profiler.createGroup("Best Case QuickSort","QuickSort Comparisons");
		profiler.addSeries("Total QuickSort","QuickSort Assignments","QuickSort Comparisons");
		profiler.createGroup("Best Case QuickSort","Total QuickSort");
		profiler.showReport();
	
	*/

	//QuickSort Worst Case
/*
	printf("QuickSort Worst Case Sort\n");
	for (int n=100;n<10000;n=n+100)
	{
		printf("n=%d\n",n);
		a=generate_ordered(n);
		quick_sort_worst(a,1,n-1,n-1);
	}
		profiler.createGroup("Worst Case QuickSort","QuickSort Assignments");
		profiler.createGroup("Worst Case QuickSort","QuickSort Comparisons");
		profiler.addSeries("Total QuickSort","QuickSort Assignments","QuickSort Comparisons");
		profiler.createGroup("Worst Case QuickSort","Total QuickSort"); 
		profiler.showReport();
		*/
}
