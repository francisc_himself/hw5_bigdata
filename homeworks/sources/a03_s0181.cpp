#include <iostream>
#include <conio.h>
#include <fstream>
#include <stdio.h>

using namespace std;

#define MAX_SIZE 10000

int aah=0,ch=0,oph=0,aaq=0,cq=0,opq=0;

int parent(int i)
{
	return (i/2);
}

int left(int i)
{
	return (2*i);
}

int right(int i)
{
	return (2*i+1);
}

void heapify(int a[], int i, int n)
{
	int largest,aux,l,r;
	l=left(i);
	r=right(i);
	ch++;
	if (l<=n  && a[l]>a[i])
		largest=l;
    else 
		largest=i;
	
	ch++;
	if (r<=n && a[r]>a[largest])
		largest=r;
	
	if (largest!=i)
	{
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		aah+=3;
        heapify(a,largest,n);
	}
}

void buildheap(int a[], int n)
{
	int i;
	for(i=n/2;i>=0;i--)
		heapify(a,i,n);
}

void heapsort(int a[],int n)
{
	int i,aux,n1;
	n1=n;
	buildheap(a,n);
	for(i=n;i>1;i--)
	{
		aux=a[0];
		a[0]=a[i];
		a[i]=aux;
		aah+=3;
		n--;
		heapify(a,0,n-1);
	}
}
/*
int partition(int a[], int p, int r)
{
	int aux,x,i,j;
	x=a[r];
	i=p-1;
	for (j=p;j<r-1;j++)
	{
		cq++;
		if (a[j]<=x)
		{
			i++;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
			aaq+=3;
		}
	}
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;
	aaq+=3;
	return i+1;
}
*/
void quicksort(int a[], int p, int r)
{
	int i=p, j=r;
	int aux;
	int pivot = a[(p + r) / 2];

/* partition */
	while(i<=j) 
	{
		
		while(a[i]<pivot)
		{
			cq++;
			i++;
		}
		while(a[j]>pivot)
		{
			cq++;
			j--;
		}
		if(i<=j) 
		{
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
			aaq+=3;
			i++;
			j--;
		}
	}
	/* recursion */
	if (p<j)
		quicksort(a, p, j);
	if (i<r)
		quicksort(a, i, r);
}

/*
void quicksort(int a[], int p, int r)
{
	int q;
	if(p<r)
	{
		q=partition(a,p,r);
		quicksort(a,p,q);
		quicksort(a,q,r);
	}
}
*/

void main()
{
	int k,a[MAX_SIZE],b[MAX_SIZE],n,aux,i,j,r,m;
	
	//cout<<"n= ";cin>>n;
	/*
	ofstream f;

	f.open("hqsort.txt");
	
	
	for (n=100;n<=10000;n=n+100)
	{
		//BEST CASE
		//for (i=0;i<n;i++)
		//	a[i]=i;
		// WORST CASE
		//for (i=0;i<n;i++)
		//	a[i]=n-i;
		for(r=0;r<5;r++)
		{
			srand(r);
			for(i=0;i<n;i++)
			{
			b[i]=a[i]=rand();
			}
			
			heapsort(a,n);
			quicksort(b,0,n-1);
		}
		if(n==100)
		{
			for(i=0;i<n;i++)
				cout<<a[i]<<" ";
			cout<<endl;
			for(i=0;i<n;i++)
				cout<<b[i]<<" ";
		}
	aah=aah/5;
	ch=ch/5;
	oph=aah+ch;
	aaq=aaq/5;
	cq=cq/5;
	opq=aaq+cq;
	f<<n<<","<<aah<<","<<ch<<","<<oph<<","<<aaq<<","<<cq<<","<<opq<<endl;
	aah=0;
	ch=0;
	oph=0;
	}
	f.close();
	
	*/
	cout<<"m= ";cin>>m;
	for (i=0; i<n;i++)
	{
		cout<<"a["<<i<<"]= ";cin>>a[i];
		b[i]=a[i];
	}

	heapsort(a,m-1);
	quicksort(b,0,m-1);

	for(i=0;i<m;i++)
			cout<<a[i]<<" ";
		cout<<endl;
	for(i=0;i<n;i++)
		cout<<b[i]<<" ";


	getch();

}
