/****ANONIM*** ***ANONIM***, ***GROUP_NUMBER***, Assign9
In amandoua cazuri (numarul de arce / numarul de varfuri este constant iar cealalta valoare variaza)
numarul de operatii creste liniar depinzand de amandoua parametri. Daca avem mai multe noduri avem
mai multe de parcurs deci cresterea operatiilor este evidenta iar daca avem mai multe arce intr-un
ciclu trebuie testat culoarea mai multor vecini.
In laboratorul prezent folosesc mai multe tipuri de afisari: lista de adiacenta, vectorul de tati,
afisarea in preordine a vectorului de tati- pritty print. Pritty printul este implementat cu ajutorul
laboratorului 7 (multi-way trees) cu putine modificari - aici avem mai multi radacini posibili.
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "Profiler.h"

enum{WHITE, GRAY, BLACK};

#define V_MAX 300
#define E_MAX 10000

typedef struct
{
	int parent;
	int distance;
	int color;
}graph_node;

typedef struct _node
{
	int key;
	struct _node* next;
}node;

//forward declaration of functions
void BFS_component(int s, int n);

typedef struct _multi_node
{
	int key;
	int count;
	int capacity;
	_multi_node **child;
}m_node;

//global variables
node *adj[V_MAX];
graph_node graph[V_MAX];
int edges[E_MAX];
node *firstQ=0;
node *lastQ=0;
int nrV=0;
int nrE=0;
//displaying
m_node *roots[V_MAX];
int nrRoots=0;
//count op
int countV[V_MAX+10];
int countE[E_MAX+10];
int saveCountAux[E_MAX+10];

//fc aux pt hardcoded graphs
//a-start node, b-end node
void addEdge(int a, int b)
{
	node *nod=new node;
	nod->key=b;
	nod->next=adj[a];
	adj[a]=nod;

	////pt grafuri neorientate
	//nod=new node;
	//nod->key=a;
	//nod->next=adj[b];
	//adj[b]=nod;
}

/* n-noduri, m muchii */
void generate(int n, int m)
{
	int a;
	int b;
	//memset(adj, 0, n*sizeof(node *));
	FillRandomArray(edges, m, 1, n*n-1,true);
	for (int i=0; i<m; i++)
	{
		a=edges[i]%n;
		b=edges[i]/n;
		addEdge(a,b);
	}
}

void print_adj_list()
{
	printf("\n");
	for (int i=0; i<nrV; i++)
	{
		printf("%d: ",i);
		for (node *p=adj[i]; p!=0; p=p->next)
		{
			printf("%d ",p->key);
		}
		printf("\n");
	}
	printf("\n");
}

void deleteListAdj()
{
	for (int i=0; i<V_MAX; i++)
	{
		if (adj[i] != 0)
		{
			node *p=adj[i];		
			while (p != 0)
			{			
				node *del=p;
				p=p->next;
				delete del;
			}
			adj[i]=0;
		}
	}
}

void enqueueQ(node **first, node**last, int x)
{
	node *p=new node;
	p->key=x;
	p->next=0;

	//empty queue
	if (*first == 0)
	{
		*first=*last=p;
		return;
	}

	(*last)->next=p;
	*last=p;
}

int dequeueQ(node **first, node**last)
{
	//empty queue
	if (*first == 0)
	{		
		return -1;
	}

	int x=(*first)->key;
	node *del=*first;
	*first=(*first)->next;
	delete del;

	if (*first == 0)
	{
		*last=0;
	}

	return x;
}

//s-nr nod start, n-nr noduri in graf
void BFS_graph(int n)
{
	for (int i=0; i<n; i++)
	{
		graph[i].color=WHITE;
		graph[i].distance=INT_MAX;
		graph[i].parent=-1;
	}

	for (int i=0; i<n; i++)
	{
		if (graph[i].color == WHITE)
		{
			BFS_component(i,n);
		}
	}
}

//s-nr nod start, n-nr noduri in graf
void BFS_component(int s, int n)
{
	graph[s].color=GRAY;
	graph[s].distance=0;
	enqueueQ(&firstQ,&lastQ,s);

	countE[nrE]+=3;
	countV[nrV]+=3;

	while(firstQ != 0)
	{
		int u=dequeueQ(&firstQ,&lastQ);
		for (node *p=adj[u]; p!=0; p=p->next)
		{
			if (graph[p->key].color == WHITE)
			{
				graph[p->key].color=GRAY;
				(graph[p->key].distance)++;
				graph[p->key].parent=u;
				enqueueQ(&firstQ,&lastQ,p->key);

				countE[nrE]+=4;
				countV[nrV]+=4;
			}
			countE[nrE]+=1;
			countV[nrV]+=1;

		}
		graph[u].color=BLACK;

		countE[nrE]+=3;
		countV[nrV]+=3;
	}
}

void init_counters()
{
	for (int i=0; i<V_MAX; i++)
	{
		countV[i]=0;
	}
	for (int i=0; i<E_MAX; i++)
	{
		countE[i]=0;
	}
}

void finalize()
{
	deleteListAdj();
}

m_node *createMN(int key)
{
	m_node *nod=(m_node *)malloc(sizeof(m_node));
	nod->count=0;
	nod->capacity=0;
	nod->child=0;
	nod->key=key;

	return nod;
}

void insertMN(m_node *parent, m_node *child)
{
	if (parent->count >= parent->capacity)
	{
		parent->capacity= (2*parent->capacity == 0) ? 1 : 2*parent->capacity;

		parent->child=(m_node**) realloc (parent->child, parent->capacity * sizeof(m_node *));

		if (parent->child == 0)
		{
			perror("Error allocating memory at insertMN!");
			exit(1);
		}
	}

	parent->child[(parent->count)++]=child;
}

//size-nr vertices
void transform_multi_node(graph_node *graph, int nrV)
{
	int t[V_MAX];
	for (int i=0; i<nrV; i++)
	{
		t[i]=graph[i].parent;
	}

	m_node *v[V_MAX];
	nrRoots=0;

	for (int i=0; i<nrV; i++)
	{
		v[i]=createMN(i);
		if (t[i] == -1)
		{
			roots[nrRoots++]=v[i];
		}
	}

	for (int i=0; i<nrV; i++)
	{
		if (t[i] != -1)
		{
			insertMN(v[t[i]],v[i]);
		}
	}
}

void pritty_print_child_(m_node *rad, int nivel)
{
	if (rad == NULL)
	{
		return;
	}

	for (int i=0; i<nivel; i++)
	{
		printf("   ");
	}
	printf("%d\n", rad->key);

	for (int i=0; i<rad->count; i++)
		pritty_print_child_(rad->child[i],nivel+1);
}

void pritty_print_child(int nivel)
{
	printf("-------------------------------------------------------\n");
	printf("Multi-way representation:\n\n");
	m_node *rad=0;
	for (int i=0; i<nrRoots; i++)
	{
		rad=roots[i];
		pritty_print_child_(rad,nivel);
		if (rad == 0)
		{
			printf("pritty_print: Arbore vid!\n");
		}
		if (i < nrRoots-1)
		{
			printf("~~~~~~~~~~~~~~~~\n");
		}
	}

	printf("-------------------------------------------------------\n");
}

int main()
{
	//initializare adj cu NULL
	memset(adj, 0, V_MAX*sizeof(node *));

	//test queue
	enqueueQ(&firstQ, &lastQ, 15);
	enqueueQ(&firstQ, &lastQ, 20);
	printf("queue test 15==%d\n",dequeueQ(&firstQ, &lastQ));
	printf("queue test 20==%d\n",dequeueQ(&firstQ, &lastQ));

	//test
	init_counters();

	nrV=8;
	nrE=9;
	addEdge(2,0);
	addEdge(5,2);
	addEdge(5,3);
	addEdge(2,6);
	addEdge(6,3);
	addEdge(3,0);
	addEdge(4,1);
	addEdge(7,1);
	addEdge(7,4);

	//nrV=8;
	//nrE=8;
	//generate(nrV,nrE);

	print_adj_list();

	BFS_graph(nrV);
	printf("Parent vector: ");
	for (int i=0; i<nrV; i++)
	{
		printf("%d ",graph[i].parent);
	}
	transform_multi_node(graph, nrV);
	printf("\nnrRoots=%d\n",nrRoots);
	pritty_print_child(1);
	finalize();

	//generare date
	printf("Computing...");

	//|V| const
	init_counters();
	nrV=100;
	int cantitate=100;
	for (nrE=1000; nrE<=5000; nrE+=cantitate)
	{
		generate(nrV,nrE);
		BFS_graph(nrV);
		finalize();
	}

	//save the count
	memcpy(saveCountAux, countE, sizeof(countE));

	//|E| const
	init_counters();
	nrE=9000;
	cantitate=10;
	for (nrV=100; nrV<=200; nrV+=cantitate)
	{
		generate(nrV,nrE);
		BFS_graph(nrV);
		finalize();
	}

	//exportare date
	FILE *f=fopen("assign9.csv", "w");
	if (f == 0)
	{
		perror("Eroare la deschidere fisier!");
		exit(2);
	}

	fprintf(f,"nr edges,nr op\n");
	cantitate=100;
	for (nrE=1000; nrE<=5000; nrE+=cantitate)
	{
		fprintf(f,"%d,%d\n",nrE,saveCountAux[nrE]);
	}

	fprintf(f,"nr vertices,nr op\n");
	cantitate=10;
	for (nrV=100; nrV<=200; nrV+=cantitate)
	{
		fprintf(f,"%d,%d\n",nrV,countV[nrV]);
	}

	fclose(f);

	printf("\nDone!\n");
	return 0;
}