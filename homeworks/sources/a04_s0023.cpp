/*
***ANONIM*** ***ANONIM***-Cristian, grupa ***GROUP_NUMBER***
Tema 4: Sortare a k liste cu n elemente.
Eficienta : O(nlogk).
*/
#include<iostream>
#include<conio.h>
#include<fstream>
#include <time.h>

using namespace std;

typedef struct nod     //structura nodului       
{
    long info ;
    struct nod *urm;
}NOD;


NOD *v[10001];   //Vectorul folosit la liste
NOD *prim,*ultim,*curent; // 3 noduri standard pentru parcurgerea listelor
long op; // un long pt numarul de operatii




void addList(int nr)     // se adauga un int intro lista 
{
    if (prim==NULL)   //daca lista e goala, numarul va devenii primul nod, daca nu devine ultimul dupa ce sa creat legaturile aferente
    {
        prim=(NOD*)malloc(sizeof(NOD*));
        prim->info=nr;
        prim->urm=NULL;
        ultim=prim;
    }
        else           
    {
        curent=(NOD*)malloc(sizeof(NOD*));
        curent->info=nr;
        curent->urm=NULL;
		ultim->urm=curent;
        ultim=curent;
    }
}

void printLista()         // //afiseaza lista finala sortata

{
    NOD *curent;
    curent=prim;
    while(curent!=NULL)
    {	cout<<curent->info<<" ";
       
        curent=curent->urm;
    }
}

int left(int i)
{
	return 2*i;
}

//left si right, algoritmii 'la pachet' cu heapify

int right(int i)
{
	return (2*i)+1;
}



int EmptyH(NOD *a[],int n)   // subprogram folosit pt a vedea daca lista e goala sau nu, returneaza 0 daca nu si 1 daca da.
{
    int i;
    int isEmpty=1;
    for (i=1;i<=n;i++)
    {
        if (a[i]!=NULL)             
        {
            isEmpty=0;
        }
    }
    return isEmpty;
}

void Heapify(NOD *A[],int i,int n) // algoritmul de constructie a heapului 
{
	int l=left(i);
	int r=right(i);

	int maxim;

	NOD *aux;
	op++;
	if(l<=n && A[l]->info < A[i]->info)
	{
		maxim=l;
	}
	else
	{
		maxim=i;
	}
	op++;
	if(r<=n && A[r]->info < A[maxim]->info)
	{
		maxim=r;
	}
	if(maxim !=i)
	{
		op+=3;

		aux=A[i];
		A[i]=A[maxim];
		A[maxim]=aux;

		 Heapify(A,maxim,n);
	}
}



void BU(NOD *A[],int n)          //folosit pentru a crea heapul dupa metoda buttom - up;
{                                               
	int i;
	for(i=n/2;i>=1;i--)
		Heapify(A,i,n);
}

void listaFinala(NOD *a[],int n) // se creaza lista finala 
{
    int k;
    int scos;
    NOD *aux;
    prim=NULL;
    curent=NULL;
    ultim=NULL;
    BU(a,n);
    k=n;
    while (EmptyH(a,k)==0)
    {
        scos=a[1]->info;
        addList(scos);
        if (a[1]->urm!=NULL)
        {
            op++;
            a[1]=a[1]->urm;
        }
            else
        {
            op+=3;
            aux=a[1];
            a[1]=a[k];
            a[k]=aux;
            k--;
        }
        Heapify(a,1,k);
    }

}
void afisareVectorListe(NOD *a[],int n)  // //afiseaza cele k liste care se genereaza la inceput
{
    int j;
    NOD *current;
    current=NULL;
	
    for (j=1;j<=n;j++)
    {	cout<<"Lista "<<j<<" :";
        current=a[j];
        while(current!=NULL)
		{	cout<<current->info<<" ";
            
            current=current->urm;
        }
        cout<<endl;
    }

}
void construireVectorListe(int k,int n) // creeam vectorul de liste din cele k liste cu n elemente.
{
    NOD *primul,*current,*ultimul;
    int i,j;
    primul=NULL;
    current=NULL;
    ultimul=NULL;
    srand(time(NULL));
    for (i=0;i<n/k+n%k;i++) 
    {
        if (primul==NULL)
        {
            primul=(NOD*)malloc(sizeof(NOD*));
            primul->info=rand() % 100;
            primul->urm=NULL;
            ultimul=primul;
        }
            else
        {
            current=(NOD*)malloc(sizeof(NOD*));
            current->info=ultimul->info+i;
            current->urm=NULL;
            ultimul->urm=current;
            ultimul=current;
        }
    }
    v[1]=primul;    
    for (j=2;j<=k;j++) 
    {
        primul=NULL;
        current=NULL;
        ultimul=NULL;
        for (i=0;i<n/k;i++) 
        {
            if (primul==NULL)
            {

                primul=(NOD*)malloc(sizeof(NOD*));
                primul->info=rand() % 100;
                primul->urm=NULL;
                ultimul=primul;
            }
                else
            {
                current=(NOD*)malloc(sizeof(NOD*));
                current->info=ultimul->info+i;
                current->urm=NULL;
                ultimul->urm=current;
                ultimul=current;
            }
        }
        v[j]=primul;
    }
}






void Rezolvare()
{int a=0,b=0,c=0;
    int n;
  
	ofstream f("excel2.csv");

	f<<"n"<<","<<"k1=5"<<","<<"k2=10"<<","<<"k3=300"<<endl;
    int k1=5,k2=10,k3=100;
    for (n=100;n<=10000;n=n+100)
    {
       
        a=0;
		op=0;
		construireVectorListe(k1,n);
        listaFinala(v,k1);
        a=op;
		
		if(n==10000)
		{
			cout<<"k=  5 | done."<<endl;
		}


        b=0;
		op=0;
        construireVectorListe(k2,n);
        listaFinala(v,k2);
		if(n==10000)
		{
			cout<<"k= 10 | done."<<endl;
		}
		b=op;
        c=0;
		op=0;
        construireVectorListe(k3,n);
        listaFinala(v,k3);
      if(n==10000)
		{
			cout<<"k=300 | done."<<endl;
		}
		c=op;
		f<<n<<","<<a<<","<<b<<","<<c;
		f<<endl;
    }
   f.close();
    op=0;
	cout<<"Rezolvare | done.";
}
void test()
{
    int n,k;
	
    n=28;
    k=7;
	cout<<"Nr de elemente : "<<n<<endl;
	cout<<"Nr de liste : "<<k<<endl;
    construireVectorListe(k,n);
    afisareVectorListe(v,k);
    cout<<endl;

    listaFinala(v,k);
	cout<<"Lista finala : ";
    printLista();
    cout<<endl;
	cout<<"---------------"<<endl;
	cout<<"Testare | done."<<endl;
	cout<<"---------------"<<endl;
}

int main()
{
	cout<<"Incepe testul"<<endl;
	cout<<"----------------"<<endl;
	test();
	
	cout<<"Incepe Rezolvarea"<<endl;
	cout<<"----------------"<<endl;
	Rezolvare();
	getch();
	return 0;
}