#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#define PAS_TEST 100
#define NR_TEST 100

typedef struct tip_nod{
		int key;
		int dim;
		struct tip_nod *p,*st,*dr;
	}NOD;

NOD *arbore;
int nrop;

void init_arbore(void){
	//initializare arbore
	arbore=(NOD*)malloc(sizeof(NOD));
	arbore->key=0;
	arbore->dim=0;
	arbore->st=NULL;
	arbore->dr=NULL;
	arbore->p=NULL;
}
// calculeaza structura arboreului, daca e prima mai mare ca a 2 , e ok
NOD *ec_arb(int a,int b){
	if(a>b) return arbore;
	int m=(a+b)/2;//valoarea de mijloc
	nrop++;
	NOD *x=(NOD*)malloc(sizeof(NOD));//aloca nod nou
	x->key=m;
	x->p=arbore;
	x->st=ec_arb(a,m-1);
	x->st->p=x;
	x->dr=ec_arb(m+1,b);
	x->dr->p=x;
	x->dim=x->st->dim+x->dr->dim+1;//calculeaza dimensiunea
	return x;
}

NOD *so_selectie(NOD *x,int i){ //se selecteaza al i lea nod
	if(x==NULL || x->key==0) return NULL;
	int r=x->st->dim+1; //numarul de noduri mai mici decat x->key   + 1
	++nrop;
	if(i==r){
		++nrop;
		return x;
	}else if(i<r){
		++nrop;
		return so_selectie(x->st,i);
	}else{
		++nrop;
		return so_selectie(x->dr,i-r);
	}
}

NOD *minim(NOD *x){//  minimul ***ANONIM*** arbore
	while(x->st!=arbore){
		++nrop;
		x=x->st;
	}
	return x;
}

NOD *succesor(NOD *x){
	++nrop;//comparatie
	if(x->dr!=arbore)
		return minim(x->dr);
	NOD *y=x->p;
	++nrop;
	while(y!=arbore && x==y->dr){
		x=y;
		y=y->p;
		nrop+=2;
	}
	return y;
}

void stergere(NOD **rad,NOD *z){
	NOD *y,*x;

	nrop+=2;//o comparatie + o atribuire
	if(z->st==arbore || z->dr==arbore){
		y=z;
	}else{
		y=succesor(z);
	}

	//toti deasupra nodului ce se va sterge, o sa piarda 1 la dimensiune
	x=y;++nrop;
	while(x!=arbore){           
		nrop+=2;
		--(x->dim);
		x=x->p;
	}
	nrop+=1;
	if(y->st!=NULL && y->st!=arbore){
		x=y->st;
	}else{
		x=y->dr;
	}
	//nrop++;
	if(x!=NULL && x!=arbore){
		x->p=y->p;
		++nrop;
	}
	nrop++;
	if(y->p==NULL || y->p==arbore){
		*rad=x;
		++nrop;
	}else if(y==y->p->st){
		y->p->st=x;
		nrop+=2;
	}else{
		y->p->dr=x;
		nrop+=2;
	}
	if(y!=z){
		z->key=y->key;
		nrop+=2;
	}
	free(y);
}
void permutare_j(int n,int m){
	NOD *x=ec_arb(1,n),*y;
	
	int nr=n,s=m;//variabile folosite pt a nu se modifica n si m
	while(x!=NULL && x!=arbore){
		s=s%nr;
		if(s==0)
                s=nr;
		y=so_selectie(x,s);
		nrop+=3;
		stergere(&x,y);
		s+=m-1;//atunci cand il stergi pe y treci automat la urmatoarea pozitie de aia se aduna m-1
		--nr;  //se decrementeaza numarul de noduri al arborelui
		nrop+=3;
	}
}


void pprint(NOD *rad,int align){
    //rad- nodul curent
    //align - cu cate spatii este aliniat la dreapta
    if(rad==arbore) return;
    char s[10];
    int i;
    sprintf(s,"%d ",rad->key);//il scriem intai intr-un string, ca sa-i putem determina lungimea
    align+=strlen(s);//incrementam align-ul, cu lungimea lui
    printf("%s",s);//il scriem efectiv
    if(rad->st!=arbore)
        pprint(rad->st,align);//fiul stang il scriem imediat in continuare
    if(rad->dr!=arbore){
        //pentru fiul drept, trecem la linia urmatoare
        printf("\n");
        for(i=0;i<align;++i)
            printf(" ");//punem spatiile corespunzatoare aliniamentului
        pprint(rad->dr,align);//il scriem
    }
}

int main(){
	init_arbore();
    int n=10;    //indica dimensiunea arborelui adica numarul de noduri
	NOD *x=ec_arb(1,n);
	printf("pretty print\n");
	pprint(x,0);
	FILE *rez=fopen("rezultate.txt","w");

	 for(int i=1;i<=NR_TEST;++i){
		n=i*PAS_TEST;
		nrop=0;
		permutare_j(n,n/10);
		fprintf(rez,"%d %d\n",n,nrop);
	}
	fclose(rez);

}
