/*
	Name: ***ANONIM*** ***ANONIM***án
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently an O(nlgn)-time algorithm which
				  outputs the (n,m)-Josephus permutation

	Analysis: -it takes O(n lg n) time to build up the order-statistic tree T , and
			   then we make O(n) calls to the order-statistic-tree procedures, each of which
			   takes O(lg n) time. Thus, the total time is O(n lg n).
			  -when we want to remove a node from the tree, the size fields must be also updated.
				For this, the search procedure decreases by 1 the size of every node from the branch
				from the root the the specific node. If the node has two children, then the find successor
				procedure is also decrementing the sizes along the branch of the successor.
*/
#include<stdio.h>

int n;
int COUNT;

struct NODE
{
	int size;
	int key;
	NODE *left, *right, *parent;
}*root;

NODE * build_balanced_bst(NODE *node, NODE *parent, int start, int end)
{
	if(start<=end)
	{
		NODE *newNode=new NODE;
		newNode->size=1;
		newNode->parent=parent;
		newNode->key=(start+end)/2;
		newNode->left=build_balanced_bst(newNode->left, newNode, start, (start+end)/2-1);
		newNode->right=build_balanced_bst(newNode->right, newNode, (start+end)/2+1, end);
		COUNT++;
		if(newNode->left!=NULL)
			newNode->size+=newNode->left->size;
		if(newNode->right!=NULL)
			newNode->size+=newNode->right->size;
		node=newNode;
		return node;
	}
	return NULL;
}

void pretty_print(NODE *node, int level)
{
	if(node!=NULL)
	{
		pretty_print(node->right, level+1);
		for(int i=1;i<=3*level;i++)
			printf(" ");
		printf("%d\n", node->key);
		pretty_print(node->left, level+1);

	}

}

NODE * os_select(NODE * node, int rank)
{
	node->size--;
	COUNT++;
	int r=1;
	if(node->left!=NULL)
		r=node->left->size+1;
	if(rank==r)
		return node;
	else if(rank<r)
		return os_select(node->left, rank);
	else return os_select(node->right, rank-r);
}


NODE * successor(NODE * x)
{
	x=x->right;
	while(x->left!=NULL)
	{
		x->size--;
		x=x->left;
		COUNT++;
	}
	COUNT+=2;
	return x;
}

void os_delete(NODE * node, NODE *z)
{
	NODE *y, *x;
	if(z->left==NULL || z->right==NULL)
		y=z;
	else y=successor(z);
	COUNT+=2;
	if(y->left!=NULL)
		x=y->left;
	else x=y->right;
	COUNT++;
	if(x!=NULL)
		x->parent=y->parent;
	if(y->parent==NULL)
		root=x;
	else
	{
		if(y->parent->left!=NULL && y==y->parent->left)
			y->parent->left=x;
		else y->parent->right=x;
		COUNT+=2;
	}
	COUNT++;
	z->key=y->key;
	delete(y);
}

void josephus(int n, int m)
{
	root=build_balanced_bst(root, NULL, 1, n);
	//pretty_print(root,0);
	int j=1,k;
	NODE * x;
	for(k=n;k>=1;k--)
	{
		j=(j+m-2)%k+1;
		x=os_select(root, j);
		//printf("\n\nNode to be deleted: %d\n\n", x->key);
		os_delete(root, x);
		//pretty_print(root,0);

	}
}

int main()
{
	josephus(7,3);
	freopen("out.csv", "w", stdout);
	printf("n,c+a\n");
	for(int i=100;i<=10000;i+=100)
	{
		COUNT=0;
		josephus(i, i/2);
		//printf("%d\n", i);
		printf("%d,%d\n", i, COUNT);
	}
	return 0;
}
