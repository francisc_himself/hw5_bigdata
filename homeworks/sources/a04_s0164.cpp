/*Student:					***ANONIM*** ***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Cerinte:					
	Se cere implementarea corecta si eficienta a unei metode de timp O(n logk) pentru a interclasa k liste ordonate, 
	unde n este numarul total de elemente din listele de intrare.
    Algoritmul are eficienta O (n*log k)
    Numarul de operatii creste cu cat numarul de liste este mai mare doarece dimensiunea heap-ului devine mai mare

	Cerinte si Observatii
	- sa se testeze algoritmul pentru cazul in care n se tot incrementeaza pana la 10000, iar k ia valorile 5,10,100
	- sa se testeze algoritmul pentru cazul in care n este fixat, cu valoarea 1000, iar k este variabil, pana la 500
	Complexitatea algoritmilor se poate observa cu usurinta in graficele generate, "k constant.xlsx", "k variabil.xlsx".Complexitate O(n*logk)

*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int nr_op;
FILE* f;

int AuxK,AuxB;
typedef struct elemente
	{
	int numar,indice_lista;			//lista de elemente cu 2 campuri: valoarea curenta si lista 
									//din care face parte fiecare
	}HEAP;							//numar=valoarea numarului curent din lista;
									//indice_lista= nr de ordine al listei in care se afla valoarea

typedef struct tip_nod
	{
	int nr;
	struct tip_nod *prec,*urm;
	}TIP_NOD;

HEAP a[1000000]; //heap-ul
int n=0;         //lungime heap
int k;

TIP_NOD *cap[10000];     //reprezinta primul element al fiecarei liste

TIP_NOD *prim=NULL,*ultim=NULL; //capetele listei finale



//Pentru Heap-uri
//=========================================================================
int parinte(int i)
{
	return i/2;				//returneaza pozitia parintelui lui i
}
int stanga(int i)            
{
	return 2*i;				// returneaza pozitia fiul stang
}
int dreapta(int i)          
{
	return 2*i+1;			// returneaza pozitia fiului drept
}

void inserareInaintePrimieInHeap(int dimHeap,int nr,int nrLista)  //dimensiunea heapului, 
		                   //valoarea care se va dauga in heap si lista de care apartine
{										
	dimHeap=dimHeap+1;//se incrementeaza dimensiunea heap-ului

	a[dimHeap].numar=nr;//ultimului element adaugat i se va atribui 
						   //valoarea transmisa ca si parametru
	a[dimHeap].indice_lista=nrLista;
	nr_op=nr_op+2;//inc. nr. op. critice
	int i=dimHeap;
	while ((i>1) && ( (a[parinte(i)].numar) < (a[i].numar) ) )
	{
		HEAP temp=a[i];
		a[i]=a[parinte(i)];       //interschimba elementele daca nu sunt in ordine
		a[parinte(i)]=temp;
		i=parinte(i);
		nr_op=nr_op+1;//inc. nr. op. critice
	}
}
void hPush(int val,int i)       //primeste ca si parametrii valoarea ce o va lua noul elemnent ce o vom extrage din lista , iar
{										//i este pozitia listei in care se afla elementul
	inserareInaintePrimieInHeap(n,val,i);   //procedura Hpush ce apeleaza procedura
	n=n+1;						//inserareInaintePrimieInHeap ce are rol de a adauga un element nou in structura heapului
	nr_op=nr_op+1;//inc. nr. op. critice
}

void reconstructHeap(HEAP a[],int i, int n) //reconstructie heap; parametrii: vectorul, indexul nodului, nr de elemente
{
	int l,r;
	int largest=0;
	l=stanga(i);	        //l ia valoare fiului din stanga
	r=dreapta(i);		    //r ia valoarea fiului din dreapta
	nr_op=nr_op+2;       //inc. nr. op. critice

	if ((l<=n) && (a[l].numar>a[i].numar))
		largest = l;
	else
		largest = i;
	nr_op=nr_op+1;        //inc. nr. op. critice

	if ((r<=n) && (a[r].numar>a[largest].numar))
	{
		largest = r;
		nr_op=nr_op+1;        //inc. nr. op. critice
	}
	
	if (largest != i)
	{
		HEAP aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		nr_op=nr_op+1;        
      	reconstructHeap(a,largest,n);
    }
}

HEAP extrageDinHeap(int dim)  //functia are ca rol extragerea unui element din heap, primeste ca parametru lungimea heapului
{
	HEAP aux;                //elementul extras din heap va fi de tipul HEAP avand  si el 2 campuri
	aux.numar=a[1].numar;     //el va lua valoarea primului element deoarece acesta este minim
	aux.indice_lista=a[1].indice_lista;
	a[1]=a[dim];                      //primul element va lua valoarea ultimului
	dim=dim-1;                          //dimensiunea heapului scade cu 1 deoarece am scos un element
	nr_op=nr_op+2;//inc. nr. op. critice
	reconstructHeap(a,1,dim);           //se apeleaza procedura de reconstruire a heapului
return aux;
}

HEAP hPop()
{
	HEAP aux=extrageDinHeap(n);
	n=n-1;   //lungimea heapului scade cu 1
	nr_op=nr_op+1;//inc. nr. op. critice
	return aux;
}





//Pentru Lista
//===========================================================================================
void creareLista(TIP_NOD **p)   //primeste ca parametru transmis prin referinta primul nod al unei liste
{
	 *p=NULL;
     nr_op=nr_op+1;//inc. nr. op. critice
}

TIP_NOD *adauga(int x)    //are rol de a adauga un numar in cadrul unei liste 
	//primeste ca si parametru valoarea numarului ce va fi adaugat
{
	TIP_NOD *p;
	p=(TIP_NOD*)malloc(sizeof(TIP_NOD));    //se aloca memoria necesara pentru noul nod adaugat
	p->nr=x;                                //campul nr al nodului p va lua valoarea transmisa ca si parametru x
	nr_op+=1;
	return p;
	free(p);
}

void inserareInaintePrim(TIP_NOD **prim,TIP_NOD *p)  //procedura are rol de a insera inainte primul nod in lista 
													 //primeste parametrii prin referinta
{							//primul nod al unei liste iar p noul nod
   p->prec=NULL;
   if(*prim==NULL)       //se testeaza daca primul nod e prim
   {
	   *prim=p;			//in caz afirmativ p va fi primul nod din lista
       p->urm=NULL;
	   nr_op+=2;//inc. nr. op. critice
   }
   else						//altfel inseara inaintea primului nod in lista
   {
	   p->urm=*prim;		//noul nod va poanta carte prim
	   (*prim)->prec=p;		//se fac legaturile
	   *prim=p;
	   nr_op+=3;
   }
}

void inserareUltimNod(TIP_NOD **ultim,TIP_NOD *p)   //insereaza un ultim nod in lista, 
	//primeste ca si parametrii ultim transmis prin referinta si noul nod ultim adica p
{
	p->urm=NULL;      //p va poanta catre null
	p->prec=*ultim;   //se fac legaturile corespunzatoare
	(*ultim)->urm=p;
	*ultim=p;
	nr_op+=4;
}

void stergePrimNod(TIP_NOD **prim)   //procedura sterge primul nod al unei liste, este transmis ca si parametru prin referinta
{
 TIP_NOD *p;
  p=*prim;
  *prim=(*prim)->urm;			//prim va fi urmatorul nod din lista
  free(p);                     //se elibereaza
  nr_op+=3;
  if(*prim!=NULL)
  {
	(*prim)->prec=NULL;
	nr_op=nr_op+1;
  }
}

void stergeLista(TIP_NOD **prim)  //procedura ce are rol sa streaga o lista
{				                    //se transmite prin referinta primul nod al unei liste care dorim sa o stergem
TIP_NOD *p;
 while( *prim!=NULL )           
   {
	   p=*prim;
	   *prim=(*prim)->urm;
	   free(p);
	   nr_op+=3;
   }
}

int readL(TIP_NOD **prim) //procedura folosita la interclasare a listelor.
	//are rol de a memora primul element din lista, iar apoi de a-l sterge
{
	TIP_NOD *p=*prim;		//vom folosi aceasta procedura de fiecare numar cand vom scoate un element dint-o lista si-l vom pune in heap
	int aux=p->nr;         //returneaza valoarea primului nod sters
	stergePrimNod(prim);  //se apeleaza procedura de stergerea a primului nod
	nr_op+=2;
	return aux;
}
void insereaza(TIP_NOD **prim,int numar)  //primeste ca si parametru primul nod transmis prin referinta precum si 
	//valoarea care o va avea nodul nod inserat
{
	TIP_NOD *p=adauga(numar);         //se creaza nodul cu informatia trimisa ca si parametru
	inserareInaintePrim(prim,p);		//apeleaza procedura de inserare inaitnea primului nod
	nr_op++;
}
void listaRezultat(int x)//procedura de creare a listei finale
{
	TIP_NOD *p=adauga(x);               //se creaza noul nod cu valoarea transmisa ca si parametru fiind x
	p->urm=NULL;
	p->prec=NULL;
	nr_op+=3;
	if(prim!=NULL)
	{													//lista nu e vida
		inserareUltimNod(&ultim,p);			//se va insera dupa ultimul nod
	}
	else
	{                        //lista a fost vida deci p este primul nod inserat
		prim=p;
		ultim=p;
		nr_op=nr_op+2;//inc. nr. op. critice
	}
}
void generare(int k, int n)  //se genereaza n elemente aleatoare
{
	int nr=n/k;
	int i,j,val;
	for(i=1;i<=k;i++)
	{
		creareLista(&cap[i]);
		val=rand()%5+1;
		for(j=1;j<=nr;j++)
		{
			insereaza(&cap[i],val);
			val+=rand()%5;
		}
	}
	stergeLista(&prim);    //deoarece vom face mai multe teste pentru mai multe liste dupa ce am terminat un test
	creareLista(&prim);	//va trenui sa stergem si sa recreem lista rezultat deoarece aceasta va contine elemente diferinte
}											//in functie de teste

void interc_k_liste()   //procedura ce interclaseaza cele K liste
{
	int val;
	HEAP aux;
	for(int i=1;i<=k;i++)       //prima numar se vor adauga capetele fiecarei liste in heap sunt k liste
	{
		val=readL(&cap[i]);      //procedura ce citeste valoarea primului nod si strege in acelasi timp primul nod
		hPush(val,i);           //se adauga valoarea in heap
		nr_op+=2;
	}

	while (n>0)   //cat timp dimensiunea heapului adica n este mai mare ca si 0 se va executa corpul de mai jos
	{
		aux=hPop();    //se scoate primul element din heap
		listaRezultat(aux.numar);   //se adauga in lista finala
		if ( (cap[aux.indice_lista])!=0 )  //se testeaza daca lista din care s-a scos elementul din heap este vida daca nu se va adauga
			{										//urmatorul element din ea,se va inainta in lista din care am scos un element
				val=readL(&cap[aux.indice_lista]);	//la fel se va sterge primul nod si se va citi valoarea lui
				hPush(val,aux.indice_lista);
                nr_op++;
			}
 	}
}
void showList(TIP_NOD *h)
{
	if(h!= NULL)
	{

		printf("%d ",h->nr);
		showList(h->urm);
	}
	else printf("NULL\n");
}

void cerinta1()
{
 //se vor face 2 teste primul va fi pentru  k={5,10,100},  n=100..10000
													//liste  ,  nr de elemente
	int v[]={5,10,100};    //dimensiuneile listelor
	f=fopen("K constant.csv","w");
	fprintf(f,"n,nr_elemente,nr_op\n");
	for(int i=0;i<3;++i) //se vor parcurge valorile lui k
	{
		k=v[i];
	
		for(int nr_elem=500;nr_elem<=10000; nr_elem=nr_elem+500)
		{
				printf("\n%d -> %d",k,nr_elem);//pentru urmarirea generarii in consola
			
			
                generare(k,nr_elem);
                    nr_op=0;
						interc_k_liste();
					     
			
				fprintf(f,"%d, %d, %d\n",k,nr_elem,nr_op);
		}
	}
	fclose(f);
}


void cerinta2()
{
	
    
	//al doilea set de teste in care n este fix iar k variaza; n=10000 k=10..500
	f=fopen("K variabil.csv","w");
	fprintf(f,"k,nr_elemente,nr_op\n");
	int nr_elem=10000;   //indica dimensiunea listei care se genereaza
	for(int i=10;i<500;i=i+10)//variaza dimensiunile listelor componente
	{
		k=i;    //dimensiunile listelor
		
		printf("\n %d->%d",k,nr_elem);//pentru urmarirea generarii in consola 
		
			generare(k,nr_elem);
			nr_op=0;
			interc_k_liste();   //apelarea procedurii de interclasare_k_liste_k_liste
		
	
		fprintf(f,"%d, %d, %d\n",k,nr_elem,nr_op);
	}
	fclose(f);
}
void main(){

	cerinta1();
	cerinta2();


//getch();
}

