/***********************ANONIM*** ***ANONIM***,gr.***GROUP_NUMBER**********************************
In hash table cautarea unei valori existente este mai usoara decat cautarea 
unei valori care nu exista pentru ca pentru a verifica ca o valoare nu exista 
trebuie parcurs tot hash'u ,in schimb pt o valoare existenta se intrerupe 
cautarea ei cand se gaseste.
Atat insertia cat si cautarea au complexitatea O(1).
Daca factorul de umplere este mai mare atunci si efortul depus pentru gasirea elementelor este mai mare.
*/
#include<iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
using namespace std;

#define hs 9973 //HASH_SIZE
#define ss 3000 //SEARCH_SIZE

int samp[hs+ss/2]; //samples
int ind[ss/2+1]; //indices
int T[hs],i;
double alfa[6]={0,0.8, 0.85, 0.9, 0.95, 0.99};

int h(int k,int i)
{
	return (k%hs+1*i+1*i*i)%hs;
}
int hash_insert(int T[],int k)
{
	int j;
	i=0;
	do{
		j=h(k,i);
		if (T[j]==NULL)
		{
			T[j]=k;
			return j;
		}
		else i++;
	}while (i<hs);
	return -1;//printf("hash table overflow");
}
int hash_search(int T[],int k)
{
	int j;
	i=0;
	do{
		j=h(k,i);
		if(T[j]==k)
			return j;
		i++;
	}while((T[j]!=NULL)&&(i<hs));
	return NULL;
}

void test() 
{
	hash_insert(T,325448);
	hash_insert(T,32548);
	hash_insert(T,99887);
	hash_insert(T,365489);
	hash_insert(T,987456);
	//pentru elementele care nu se gasesc
	if(hash_search(T,568)!=NULL) 
		printf("%d val exista in hash \n ",(i+1));
	else  printf("val nu exista in hash \n");
	//pentru elementele care se gasesc	
	if(hash_search(T,987456)!=NULL)
		printf("val exista in hash %d\n ",hash_search(T,987456)+1);
	else  printf("val nu exista in hash \n");
}
void main()
{
	test();	
	FILE * f=fopen("hashTable.csv","w");
	fprintf (f,"Filling factor,avg found,max found,avg not found,max not found \n");
	for (int p=1;p<6;p++)
	{
		double n,found=0,found_max=0,not_found=0, not_found_max=0;
		memset(T,256,hs*sizeof(int));
		n=alfa[p]*hs;
		FillRandomArray(samp,n+ss/2,1,1000000,true,0);
		for(int k=0;k<n;k++)
			if(hash_insert(T,samp[k])==-1) break;

		for(int k=n;k<n+ss/2;k++)
		{	
			hash_search(T,samp[k]);
					not_found+=(i+1);
					if((i+1)>not_found_max)
					not_found_max=(i+1);
		}

		FillRandomArray(ind,ss/2,0,(int)n-1,true,0);
		for(int k=0;k<ss/2;k++)
		{
			hash_search(T,samp[ind[k]]);
				found+=(i+1);
				if((i+1)>found_max)
				found_max=i+1;
		}
		fprintf(f,"%f, %f, %f, %f, %f, \n",alfa[p],found/(ss/2),found_max,not_found/(ss/2),not_found_max);
	}
	fclose(f);
	cout<<"done!";
	_getch();
}