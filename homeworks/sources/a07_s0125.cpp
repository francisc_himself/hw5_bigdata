/*Student: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
College: Techincal University of Cluj-Napoca
Date: 13 May, 2013
Subject: Multi-Way Trees
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

//static int comp=0;
//static int assig=0;
//static int n=0;
//ofstream myfile;


struct TreeNode1{
        //TreeNode* parent;
        //TreeNode* left;
        //TreeNode* right;
TreeNode1* children[10];
        //int size;
        int key;
int count;
};

struct TreeNode2{
        //TreeNode* parent;
        TreeNode2* left;
        TreeNode2* right;
//TreeNode1** children;
        //int size;
        int key;
//int count;
};

TreeNode1* T1[10000];
TreeNode2* T2[10000];
int A[9]= {2,7,5,2,7,7,-1,5,2};

int t1(int* A,int size){
	//TreeNode1* root; 
	int root;
	for (int i=0; i<size; i++){
		T1[i] = new TreeNode1;
		T1[i]->count = 0;
		T1[i]->key = i+1;
		//printf("%d initialized \n", T1[i]->key);
	}

	for (int i=0; i<size; i++){

		if (A[i]!=-1){
			T1[A[i]-1]->children[T1[A[i]-1]->count] = T1[i]; 
			T1[A[i]-1]->count +=1; 
			//printf("%d now has %d children \n", A[i],T1[A[i]-1]->count);
		}
		else {//root = T1[i]; printf("%d is the root \n", T1[i]->key);}

			root = i; 
			//printf("%d is the root and has %d children \n ", T1[i]->key, T1[i]->count);
		}
	}
	return root;
}


TreeNode2* t2(TreeNode1* t1, TreeNode2* t2){
TreeNode2* root = new TreeNode2; TreeNode2* target; TreeNode2* oldtarget;
root->key = t1->key;
oldtarget = root;

if (t1->count>0)
{ //must refernece 2nd array somehow
if (t1->children[0]!=NULL){ target = new TreeNode2; target->key = t1->children[0]->key; root->left = target;} //first left child
if (t1->children[1]!=NULL){ for(int x=1; x<t1->count; x++){ //right list
	oldtarget = target; target = new TreeNode2; target->key = t1->children[1]->key; oldtarget->right = target;}
}
}

return root;
}

void prettyPrint(TreeNode1* root, int height){
	printf("\n");
	if (root!=NULL){
		for (int x=0; x<height; x++){printf("    ");}
		printf("%d", root->key);
		if (root->count>0){
			//printf("\n");
			//printf("%d has %d children \n", root->key, root->count);
		}
	//_getch();
	if (root->count>0){
		//printf("Children of %d:\n", root->key);

	}
		for(int i=0; i<root->count; i++){
	prettyPrint(root->children[i],height+1);

	}
			//	for(int i=0; i<root->count; i++){
		//printf("%d ", root->children[i]->key); //for (int x=9; x>height;x=x-2) printf(" ");
	//}

		}


}

void main(){

	int res = t1(A,9);
	prettyPrint(T1[res],0);
	_getch();
}