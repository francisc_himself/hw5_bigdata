#include <stdio.h>
#include <stdlib.h>

#define V 10000     // no of vertices

typedef struct node {
    int key;
    int height;
    struct node *parent;
} NodeT;

NodeT *nodes[V];
int count;

NodeT *makeSet (int x)
{
    count++;

    NodeT *p;
    p = (NodeT*) malloc(sizeof(NodeT));

    p->key = x;
    p->height = 0;
    p->parent = p;

    //printf("Created node: %d with parent %d\n", p->key, p->parent->key);
    return p;
}

NodeT *findSet (NodeT *x)
{
    //using path compresion
    count++;

    if (x != x->parent)
    {
        x->parent = findSet(x->parent);
    }

    return x->parent;
}

void link(NodeT *x, NodeT *y)
{
    if(x->height > y->height)
    {
        y->parent = x;
    }
    else
    {
        x->parent = y;
        if(x->height = y->height)
            y->height = y->height + 1;
    }
}

NodeT *setUnion (NodeT *u, NodeT *v)
{
    count++;
    link(findSet((u)), findSet(v));
	return 0;
}



int main()
{
/*
    FILE *fin = fopen ("sets.in", "r");

    int i;
    int u, v;

    int n; //nodes
    fscanf(fin, "%d", &n);

    for (i = 0; i < n; i++)
        nodes[i] = makeSet(i);

    int e; //edges
    fscanf(fin, "%d", &e);

    for (i = 0; i < e; i++)
    {
        fscanf (fin, "%d %d", &u, &v);
        //printf("%d %d\n", u, v);
        if (findSet(nodes[u]) != findSet(nodes[v]))
        {
            setUnion(nodes[u], nodes[v]);
        }
    }

    for (i = 0; i < n; i++)
        printf("%d with parent %d\n", nodes[i]->key, nodes[i]->parent->key);
*/

    FILE *fout = fopen ("disjointSets.csv", "w");

    int e;
    int i;
    int u, v;

    for (e = 10000; e <= 60000; e += 1000) //edges
    {
        count = 0;

        for (i = 0; i < V; i++)
        {
            nodes[i] = makeSet(i);
        }

        for (i = 0; i < e; i++)
        {
            u = rand()%V;
            v = rand()%V;

            if (findSet(nodes[u]) != findSet(nodes[v]))
            {
                setUnion(nodes[u], nodes[v]);
            }
        }

        printf("Step: %d, count %d\n", e, count);
        fprintf(fout, "%d %d\n", e, count);

        for (i = 0; i < V; i++)
        {
            free(nodes[i]);
        }
    }

    fclose(fout);
    return 0;
}
