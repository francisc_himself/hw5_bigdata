﻿/*
**************************ANONIM*** ***ANONIM***,gr ***GROUP_NUMBER************************************
din grafic rezulta crestere liniar logaritmica a algoritmului
 eficienta nlgn
 nu ne intereseaza reechilibrarea arborelui pentru ca daca apelam 
 josephus(n,n/2) avem intotdeauna un nod sters din stanga radacinii si 
 unul din dreapta care duce la echilibrarea arborelui de la sine dupa 
 fiecare stergere
*/

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<malloc.h>
#include<conio.h>
using namespace std;

typedef struct nod{
	int val;
	int dim;
	struct nod* stg;
	struct nod* dr;
	struct nod* par;
}nod;
nod* root;

int as,comp;

nod* build_tree(nod* root, int *arr, int l, int r) {   //l-left r-right
	if (l <= r) {
		//daca radacina este null
		comp++;
		if (root == NULL) {
			as=as+3;
			root = (nod*) malloc(sizeof(nod));
			root->stg = NULL;
			root->dr = NULL;
			root->par = NULL;
			root->dim = 0;
		}
		as=as+3;
		root->val = arr[(l+r)/2];  //ia val din mijloc
		root->stg = build_tree(root->stg,arr,l,(l+r)/2-1);
		root->dr = build_tree(root->dr,arr,(l+r)/2+1,r);
		//links to parents
		comp++;
		if (root->stg != NULL){
			root->stg->par = root; as++;}
		comp++;
		if (root->dr != NULL){ 
			root->dr->par = root; as++;}
	}
	return root;
}
void prettyPrint(nod *v,int n,int k,int nivel)
{
	if(k>n) return;
	if (v!=NULL){
		prettyPrint(v->dr,n,2*k+1,nivel+1);
		for(int i=0;i<=nivel-1;i++)
			printf("     ");
		printf("%d,%d\n",v->val,v->dim);
		prettyPrint(v->stg,n,2*k,nivel+1);
	}
}
int os_rank(nod* root)
{
	comp++;
	if (root == NULL) return 0;
	else {	
		as++;
		root->dim = (int) os_rank(root->dr) + (int) os_rank(root->stg) + 1;
		printf("\n %d : %d" ,root->val, root->dim);
	}
	return root->dim;
}

nod* os_select(nod* x, int i) {
	int r;//rank
	comp++;
	if(x->stg!=NULL)
		r= x->stg->dim + 1;  
	else   r=1;
	if (i == r)
		return x;
	else if ( i < r) 
		return os_select(x->stg,i);
	else 
		return os_select(x->dr,i-r);
}

nod* minimum(nod* root) { //min key
	comp++;
	if (root != NULL) { 
		while (root->stg != NULL){
			root = root->stg; as++; comp++;}
	}
	return root;	   
}

nod* maximum(nod* root) { //max key
	comp++;
	if (root!=NULL) {
		while (root->dr != NULL) {
			root = root->dr; as++; comp++;}
	}
	return root;
}
nod* succ(nod* root) {
	return minimum(root->dr);
}
nod* pred(nod* root){
	return maximum(root->stg);
}
nod* os_delete(nod *root, int key) {
	nod aux;
	comp++;
	if (key < root->val) {		//pt stg
		root->stg = os_delete(root->stg,key);		as++;
		root->dim--;								as++;
	} else 	if (key > root->val) {						//pt dr
			root->dr = os_delete(root->dr,key);		as++;
			root->dim--;							as++;
		} else				//am gasit nodul
			{	comp++;
				if (root->stg && root->dr) {				//daca are 2 copii
				aux = *succ(root);						//gasim succesorul
				root->val = (&aux)->val;				//il plasam in locul nodului de sters
				root->dim--;						as++;
				root->dr = os_delete(root->dr,root->val);//si stergem succesorul
				} else {									//unu sau nici un copil
					comp++;
					if(root->dr == NULL) {
						root->dim--;
						root = root->stg;  as++;
					} else
						if (root->stg == NULL) {
							root->dim--;
							root= root->dr; as++;
						}
				}
		}
			return root;
}
void josephus(int n, int m) {
int j=1,key; 
nod *s;
for(int k=n;k>=1;k--){
	j =((j+m-2)% k) + 1 ;
	s =os_select(root,j);
	printf("delete %d \n\n",s->val);
	root = os_delete(root,s->val);		as++;
	if(root) prettyPrint(root,n,1,0);	comp++;
	}
}
int main() {
	FILE * f=fopen("Josephus.csv","w");
	fprintf (f," n ,asignarii,comparatii,operatii\n");
	int i,m =3,n =7, v[7],arr[10000];
	for(i=1;i<=n;i++){
		v[i-1]=i;
		cout<<v[i-1]<<" ";
	}
	root = build_tree(NULL,v,0,n-1);
	root->dim = os_rank(root);
	printf("\n");
	prettyPrint(root,n,1,0);
	printf("\n");
	josephus(n,m);
	printf("%d, %d, %d, %d\n",n,as,comp,as+comp);
	for(n=100;n<=10000;n=n+100)
	{ as=0;comp=0;
		m=n/2;
		for(int i=1;i<=n;i++){
		arr[i-1]=i;
		cout<<arr[i-1]<<" ";
	}
	root = build_tree(NULL,arr,0,n-1);
	root->dim = os_rank(root);
	printf("\n");
	prettyPrint(root,n,1,0);
	printf("\n");
	josephus(n,m);
	fprintf(f,"%d, %d, %d, %d\n",n,as,comp,as+comp);
	}
	fclose(f);
	cout<<"done!";
	getch();
	return 0;
}