/****ANONIM*** ***ANONIM***-***ANONIM*** gr.***GROUP_NUMBER***
	Algoritmul de mai jos prezinta implementarea operatiilor de inserare si cautare
intr-o tabela de dispersie utilizand adresare deschisa si si verificare patratica. 
La acest tip de adresare tabela de dispersie se poate umple astfel incat sa nu mai 
fie posibila nicio inserare,adica factorul de incarcare este maxim 1.
	Inserarea se face verificand tabela pana cand se gaseste o locatie libera, locatie 
determinata printr-o functie de dispersie FH(k,i)=(k%m+c1*i+c2*i^2)%m ,unde m este numarul
maxim de elemente din tabela. Procedura de cautare utilizeaza aceeasi functie de dispersie
doar ca acum se cauta elementele in tabela la pozitia specificata de functie, si se repeta
pana cand pe pozitia cautata este 0 sau cautarea s-a efectuat pentru toate pozitiile.
	Eficienta algoritmului este de O(1+x) unde x este dat de factorul de umplere.

*/

#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"

#define HS 9973
#define SS 3000
#define c1 1
#define c2 1 
int H[HS];
int samples[HS+SS/2];
int indices[SS/2+1];

int H1[5];
//int v1[3];
//int v2[3];

int FH(int k,int i)
{
	return(k%HS+c1*i+c2*i*i)%HS;
}
int hash_insert(int k)
{
	int i=0;
	int j;
	do
	{
		j=FH(k,i);
		if (H[j]==0)
		{
			H[j]=k;
			return j;
		}
		else i++;
	} while (i!=HS);
	printf("depasire\n");
	return -1;
}

int  hash_cauta(int k,int *caut)
{
	int i=0;
	int j;
	do
	{
		j=FH(k,i);
		if(H[j]==k)
		{
			*caut=i+1;
			return j;
		}
		i++;
	}while(H[j]!=NULL && i!=HS);
	*caut=i+1;
	return -1;
}

void test(){
	int	v[5]={213,1231,4122,53246,13214};
	for(int i=0;i<5;i++)
	hash_insert(v[i]);
	int gasit=0,negasit=0;
	int r=hash_cauta(1231,&gasit);
	if(r!=0 && r!=-1){	
		printf("am gasit elementul cautat din %d incercari\n",gasit);
	}
	int t=31231;
	hash_cauta(t,&negasit);
	printf("am cautat %d elementul si nu l-am gasit",t,negasit);
}
void main()
{
	int nrE=0;
	
	test();

	//double a,n,m;
	/*int n;
	int m;
	n=30;
	m=(int)(n*90)/100;
	int v[30];
	FillRandomArray(v,m,1,30,true,0);
	for(int i=0;i<m;i++)
	{
		hash_insert(v[i]);
		nrE++;
	}
	for(int i=0;i<15;i++)
	{
		hash_cauta(v[i],&gasit);
		elemg+=gasit;
	}
	for(int i=0;i<15;i++)
	{
		hash_cauta(v[i],&negasit);
		elemng+=negasit;
	}
	printf("%d elem OK %d nOK",elemg,elemng);*/
	//int maxG=0,maxNG=0;
	//FILE *f=fopen("TBdedispersie.csv","w");
	//fprintf(f,"Factor ; Media efort-gasit; Maxim efort-gasit; Media efort-negasit; Maxim efort-negasit;\n");
	//
	//for(int i=0;i<5;i++)
	//{
	//int gasit=0,negasit=0;
	//int elemg=0,elemng=0;
	//
	//double mediang,mediag;
	//	if(i==0){a=0.8;}
	//	if(i==1){a=0.85;}
	//	if(i==2){a=0.9;}
	//	if(i==3){a=0.95;}
	//	if(i==4){a=0.99;}
	//	n=a*HS;
	//	m=n+SS/2;
	//		printf("%f ",m);
	//		memset(samples,0,sizeof(samples));
	//		memset(H,0,sizeof(H));
	//	FillRandomArray(samples,int(m),1,1000000,true,0);
	//	
	//	
	//	for(int j=0;j<int(n);j++)
	//	{
	//		hash_insert(samples[j]);

	//		nrE++;
	//	}
	//	FillRandomArray(indices,SS/2,0,(int)n-1,true,0);
	//	for(int j=0;j<SS/2;j++)
	//	{
	//		hash_cauta(samples[indices[j]],&gasit);
	//	elemg+=gasit;
	//	if(gasit>maxG)
	//		maxG=gasit;
	//	
	//	}
	//	mediag=(float)elemg/(SS/2);
	//	FillRandomArray(indices,SS/2,(int)n,(int)m-1,true,0);
	//	for(int j=0;j<SS/2;j++)
	//	{
	//		hash_cauta(samples[indices[j]],&negasit);
	//	elemng+=negasit;
	//	if(negasit>maxNG)
	//		maxNG=negasit;
	//	
	//	}
	//	mediang=(float)elemng/(SS/2);
	//	fprintf(f,"%.2f ; %.2f ; %d ; %.2f ; %d\n",a,mediag,maxG,mediang,maxNG);
	//}
}
