#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define m 10007
#define c1 1
#define c2 1 
//---Variabile globale---
//numarul de elemente
int N = m;
//hashTable
int hashT[m];
//elements that must be found 
int found[1501];
//elements that must not be found 
int notFound[1501];
//search numbers
int searches = 1500;
int temp[m];
//alfa : filling factor = n/N
float alfa[] = {0.80, 0.85, 0.9, 0.96, 0.99};
float effort;
float foundEff,notFoundEff,foundEffM,notFoundEffM;

//---Metode folosite---
//calcularea permutarii
int h(int k,int i){
	int temp = (k%N+c1*i+c2*(i*i))%N;
	return temp;
}

//initialisation 
void init(int k){
	int i;
	for(i = 0;i<k;i++){
		hashT[i] = 0;
	}
}
//inserare nod nou
int hashInsert(int k){
	int i=0;
	int j;
	effort = 0;
	do{
		j=h(k,i);
		if(hashT[j] == k)
			return 0;
		if(hashT[j]==NULL){
			hashT[j] = k;
			return j;
		}else{
			i++;
			effort++;
		}
	}while(i!=N);
	//no insert ca be made
	return 0;
}
//cautare
int hashSearch(int k){
	int i = 0;
	effort = 0;
	int j;
	do{
		j = h(k,i);
		if(hashT[j]==k){ 
			return j;
		}
		i++;
		effort = (float)i+1;
	}while(hashT[j]!=NULL && i!=N);
	return 0;
}
//afisare tabel de dispersie
void show(int hashT[],int N){
	int i=0;
	for(i=0;i<N;i++){
		printf("\n%d : ",i);
		if(hashT[i]!=0){
			printf(" %d",hashT[i]);
		}
	}
}
//generare numai numere diferite de zero
int generate(int n){
	int x = (1 + rand()*rand())%n;
	return x;
}
//generare gasite
void generateFound(int n){
	int i;
	for(i=0;i<searches;i++){
		int x=rand()%n;
		while(temp[x] ==0)
			x=rand()%n;

		found[i]=temp[x];
		temp[x] = 0;
	}
}
//generare negasite
void generateNotFound(int n){
	int i;
	for(i=1;i<searches;i++){
		notFound[i]= n+rand();
	}
}

int main(){
	float foundAux =0, notFoundAux = 0;
	float foundAll = 0, notFoundAll = 0;
	int nr=0;
	float alf;
	int x,i,ini;
	FILE *f;

	printf("\nStart Aplicatie!");
	srand(time(NULL));
	
	//------------------Evaluation-------------------------
	f = fopen("hashTable.csv","w");
	fprintf(f,"FactorUmplere,AvgEfortGasit,MaxEfortGasit,AvgEfortNegasit,MaxEfortNegasit\n");
	N = m;
	for(i=0;i<5;i++){
		

		int k;
		for(k=0;k<5;k++){
			init(m);
			alf = 0;
			nr=0;
			do{
				x = generate(50000);
				if(hashSearch(x)==0){
					if(hashInsert(x)){
						nr++;
						temp[nr] = x;
						ini = x;//max
					}
			}
			alf = (float) nr/N;
			}while (alf<alfa[i]);
			
			generateFound(nr);
			generateNotFound(50000);

			int j;
			for(j = 0;j<searches;j++){
				
				hashSearch(found[j]);
				foundEff = effort;
				foundAux += foundEff;
				if(foundEff>foundEffM){
					foundEffM = foundEff;
				}
				effort = 0;

				hashSearch(notFound[j]);
				notFoundEff = effort;
				notFoundAux += notFoundEff;
				if(notFoundEff>notFoundEffM){
					notFoundEffM = notFoundEff;
				}
				effort = 0;
			}
			foundAll +=(float)foundAux/searches;
			notFoundAll +=(float)notFoundAux/searches;


			foundAux = 0;
			notFoundAux =0;
		}
		fprintf(f,"%2.2f, %2.2f, %2.2f, %2.2f, %2.2f\n",alfa[i],foundAll/5,foundEffM,notFoundAll/5,notFoundEffM);
		nr=0;
		foundEffM =0;
		notFoundEffM =0;
		foundAll =0;
		notFoundAll =0;
		foundEff =0;
		notFoundEff =0;
	}
	fclose(f);
	printf("\nS-a sfarsit evaluarea algoritmului!");
	
	//-----------------Demo---------------------------------
	printf("\nStart Demo!");
	//------init date-----
	N=23;
	searches = N/3;
	//------init var------
	nr =0;
	alf =0;
	x =0;
	//------incepere Demo-
	//---consideram un factor de umplere alfa = 0.85---
	//---consideram numere aleatoare pana la 20---
	init(N);
	
	for(i=0;i<searches+1;i++){
		found[i]=0;
	}
	ini=0;
	do{ 
		
		
		x=generate(200);
		printf("\nelement generat  = %d",x);
		if(hashSearch(x)==0){
		int key = hashInsert(x);
		printf("\nnumarul generat : %2d si pus pe pozitia %2d",x,key);
			if(key){
				nr++;
				if(nr%3==0){
					found[nr/3] = x;
				}
			}
		}
		alf = (float) nr/N;
	}while(alf<0.85);
	//---afisare tabel de dispersie---
	show(hashT,N);
	//---generare numere

	//generateFound();
	generateNotFound(10);

	int j;
	for(j=0;j<searches;j++){
		//---gasirea elementelor ce ar trebuii sa fie gasite---
		printf("\nelementul %2d s-a gasit :\n - pe pozitia %2d \n - cu efortul %2d",found[j],hashSearch(found[j]),effort);
		foundEff = effort;
		foundAux+=foundEff;
		if(foundEff>foundEffM){
			foundEffM = foundEff;
		}
		effort = 0;
		//---incercarea gasirii unor elemente ce 
		//				nu exista in tabelul de dispersie ---
		printf("\nelementul %2d nu s-a gasit :\n - efortul cautarii %2d",notFound[j],hashSearch(notFound[j]),effort);
		notFoundEff = effort;
		notFoundAux+=notFoundEff;
		if(notFoundEff>notFoundEffM){
			notFoundEffM = notFoundEff;
		}
		effort = 0;
	}
	printf("\nRaport: \n - Factor de umplere                      %3.2f \n - Efortul mediu pentru elemente gasite   %3.2f \n - Efortul maxim pentru elemente gasite   %3.2f \n - Efortul mediu pentru elemente negasite %3.2f \n - Efortul maxim pentru elemente negasite %3.2f \n",(float)alf,(float)foundAux/searches,(float)foundEffM,(float)notFoundAux/searches,(float)notFoundEffM);
	printf("\nS-a sfarsit Demo-ul!");
	printf("\nSfarsitul aplicatiei!");
	getch();
	return 0;
}