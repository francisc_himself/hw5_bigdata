// Temaa1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>

#define MAX 10000

 int A[MAX];
 int Q[MAX];
 int P[MAX];

void sortarea_bulelor (int A[], int n, int &atr, int &comp)
{
	atr = 0;
	comp = 0;
	int j, k, ord = 0;
	while (ord == 0){
		ord = 1;
		for (j = 1; j <= n - 1; j++){
			comp=comp+1;
			if (A[j] > A[j+1]){
				k = A[j];
				A[j] = A[j+1];
				A[j+1] = k;
				atr=atr+1;
				ord = 0;
			}
		}
	}
}

void sortarea_prin_selectie (int A[], int n, int &atr, int &comp)
{
	atr = 0;
	comp = 0;
	int i, j, imin, k;
	for (i = 1; i <= n - 1; i++)
	{
		imin = i;
		for (j = i + 1; j <= n; j++)
		{
			comp=comp+1;
			if (A[j] < A[imin])
				imin = j;
		}
		k = A[i];
		A[i] = A[imin];
		A[imin] = k;
		atr=atr+1;
	}
}

void sortarea_prin_insertie (int A[], int n, int &atr, int &comp)
{
	int i, j, k, a;
	for (i = 2; i <= n; i++)
	{
		a = A[i];
		j = 1;
		comp=comp+1;
		while (A[j] < a)
		{
			j = j + 1;
			comp=comp+1;
		}
		for (k = i; k >= j + 1; k--)
		{
			A[k] = A[k-1];
			atr=atr+1;
		}
		A[j] = a;
		atr=atr+1;
	}
}

void main()
{

	FILE *fp;
	int n = 100, m = 5;
	int a[4], c[4];
	int i,j;
	int p,v[MAX];
	printf("Introduceti numarul de elemente=");
	scanf("%d", &p);
	for(i=1;i<=p;i++)
		{
			printf("Dati elementele vectorului A[%d]= ", i);
			scanf("%d", &A[i]);
		}
	printf("Afisare elemente");
	for(i=1;i<=p;i++)
		{
			printf("%d ", A[i]);
		}

	sortarea_bulelor(A,p,a[0],c[0]);
	printf("\n");
	printf("Afisare elemente sortate \n");
	for(i=1;i<=p;i++)
		{
			printf("%d ", A[i]);
		}
	
	fp = fopen("bsi.csv","w");
	
	//initializare pas pt functia rand
	srand (time(NULL));
	fprintf(fp, "n,bubble_bst_atr,bubble_bst_comp,bubble_bst_suma,selection_bst_atr,selectie_bst_comp,selectie_bst_suma,insertie_bst_atr,insertie_bst_comp,insertie_bst_suma,bubble_av_atr,bubble_av_comp,bubble_av_suma,selectie_av_atr,selectie_av_comp,selectie_av_suma,insertie_av_atr,insertie_av_comp,insertie_av_suma,bubble_ws_atr,bubble_ws_comp,bubble_ws_suma,selectie_ws_atr,selectie_ws_comp,selectie_ws_suma,insertie_ws_atr,insertie_ws_comp,insertie_ws_suma\n");
	
	for (n = 100; n <= 10000; n = n + 100)
	  {
		a[1] = 0; a[2] = 0; a[3] = 0; c[1] = 0; c[2] = 0; c[3] = 0;

		fprintf(fp, "%d,", n);
		for (i = 1 ; i <= n; i++)
			Q[i] = i;
	
		//sortarea bulelor cazul favorabil
		sortarea_bulelor(Q, n, a[0], c[0]);
		fprintf(fp, "%d,%d,%d,", a[0], c[0], a[0]+c[0]);

		//sortarea prin selectie cazul favorabil
		sortarea_prin_selectie(Q, n, a[0], c[0]);
		fprintf(fp, "%d,%d,%d,", a[0], c[0], a[0]+c[0]);

		//sortarea prin insertie cazul favorabil
		sortarea_prin_insertie(Q, n, a[0], c[0]);
		fprintf(fp, "%d,%d,%d,", a[0], c[0], a[0]+c[0]);

		//atribuire valori n cazul mediu statistic de 5 ori
		for (j = 1; j <= m; j++){
			for (i = 1; i <= n; i++)
			{
				Q[i] = rand() % 10000;
				P[i] = Q[i];
			}

			//sortarea bulelor cazul mediu statistic
			sortarea_bulelor(Q, n, a[0], c[0]);
			a[1] = a[1] + a[0];
			c[1] = c[1] + c[0];
			for (i = 1; i <= n; i++)
				{
					P[i] = Q[i];
				}

			//sortarea prin selectie cazul mediu statistic
			sortarea_prin_selectie(Q, n, a[0], c[0]);
			a[2] = a[2] + a[0];
			c[2] = c[2] + c[0];
			for (i = 1; i <= n; i++)
				{
					P[i] = Q[i];
			    }

			//sortarea prin insertie cazul mediu statistic
			sortarea_prin_insertie(Q, n, a[0], c[0]);
			a[3] = a[3] + a[0];
			c[3] = c[3] + c[0];
			for (i = 1; i <= n; i++)
				{
					P[i] = Q[i];
			    }
		}
		fprintf(fp, "%d,%d,%d,%d,%d,%d,%d,%d,%d,", a[1] / 5, c[1] / 5, (a[1] + c[1]) / 5, a[2] / 5, c[2] / 5, (a[2] + c[2]) / 5, a[3] / 5, c[3] / 5, (a[3] + c[3]) / 5);


		//valori pentru cazul defavorabil, ord descrescator
		for (i = 1; i <= n; i++)
			{
			Q[i] = n - i;
			P[i] = n - i;
			}

		//sortarea bulelor cazul defavorabil
		sortarea_bulelor(Q, n, a[0], c[0]);
		fprintf(fp, "%d,%d,%d,", a[0], c[0], a[0]+c[0]);

		//sortare prin selectie cazul defavorabil
		sortarea_prin_selectie(Q, n, a[0], c[0]);
		fprintf(fp, "%d,%d,%d,", a[0], c[0], a[0]+c[0]);

		//sortarea prin insertie cazul defavorabil
		sortarea_prin_insertie(Q, n, a[0], c[0]);
		fprintf(fp, "%d,%d,%d\n", a[0], c[0], a[0]+c[0]);
	}

	fclose(fp);
	getch();
}



