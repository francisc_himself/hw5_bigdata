// Assign1_***ANONIM******ANONIM***__***GROUP_NUMBER***.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


/*
student: ***ANONIM*** ***ANONIM***;
grupa: ***GROUP_NUMBER***;

Cerinta temei nr.1 este implementarea corecta si eficienta a celor de metode directe de sortare: bubble sort, insertion sort si selection sort, precum si
generarea graficelor in scopul compararii numarului de comparari, atribuiri si suma acestora care s-au realizat in cele trei cazuri pozibile: best case- 
cazul favorabil, worst case- cazul defavorabil sau average case- cazul mediu statistic.

In primele 3 metode am descris acesti 3 algoritmi, dupa pseudocodul prezentat in carte.
Dupa aceea, am observat faptul ca:
	-cazul favorabil la toate cele 3 metode este dat vectorul sortat crescator, deoarece au loc cele mai putine atribuiri
	-cazul defavorabil la cele 3 metode este dat de vectorul sortat descrescator, deoarece astfel se vor realiza cele mai multe atribuiri 
	-la cazul mediu statistic se genereaza valori aleatorii cu functia rand(), pentru a asigura ca nu avem nici unul din primele 2 cazuri

Observatiile pe care le putem face in urma generarii graficelor sunt:
	-in caul favorabil ,metoda bulelor este cea mai eficienta deoarece numarul atribuirilor este 0
	-in cazul defavorabil, metoda mai sus amintita va fi insa cea mai eficienta, lucru observabil din numarul atribuirilor care este maxim in comparatie
cu celelalte metode
	-metoda bulelor scade deci din eficienta, de la cazul favorabil la cel defavorabil
	-in cazul mediu statistic, selectia este cea mai eficienta si metoda bulelor cea mai ineficienta
	-in cazul favorabil, bubble sort are eficienta O(n), pe cand celelalte O(n^2)
	-in celelalte doua cazuri, algoritmii au eficienta O(n^2).

Urmatoarele 3 metode am facut simularea celor 3 cazuri, dand valori vectorilor.
De asemenea, in metoda Exemplu() am declarat un vector aranjat aleator, iar apoi am aplicat metodele de sortare, pentru verificarea corectitudinii.Vectorul
declarat este unul de dimensiune mica. In urma rularii putem observa ca toate metodele de sortare dau acelasi rezultat, deci sunt corecte.

O observatie proprie ar fi faptul ca desi bubble sortul este cel mai cunoscut in randul celor care stiu un limbaj de programare, fiind studiat inca
din liceu, sortarea prin selectie este mult mai eficienta si mai indicat de utilizat.

*/


#include<stdio.h>
#include<conio.h>
#include<cstdlib>
#include<time.h>

long comp_b, atrib_b, suma_b;
long comp_s, atrib_s, suma_s;
long comp_i, atrib_i, suma_i;

void bubble(int a[], int n)
{
	comp_b = 0;
	atrib_b = 0;
	int sortat = 0;
	int i, ok=0, aux;

	while( !sortat)
	{
		sortat = 1;
		for(i=0;i<n-ok-1;i++)
		{
			comp_b ++;
			if(a[i]>a[i+1])
			{
				aux = a[i];
				a[i]=a[i+1];
				a[i+1]=aux;
				sortat = 0;


				atrib_b+=3;
			}
		}
		ok++;
	}

	suma_b = comp_b + atrib_b;
}

void selection(int a[], int n)
{
	int i,j;
	int min,imin;
	comp_s =0;
	atrib_s =0;

	for(i=0; i<n-1; i++)
	{
		min= a[i];
		imin=i;

		atrib_s++;

		for(j=i+1; j<n; j++)
		{
			comp_s++;
			if(a[j]<min)
			{
				min=a[j];
				imin=j;

				atrib_s++;
			}
		}
		a[imin]=a[i];
		a[i]=min;

		atrib_s+=2;
	}
	suma_s = comp_s + atrib_s;
}

void insertion(int a[], int n )
{
	int i, j, k,x;
	comp_i = 0;
	atrib_i = 0;

	for(i=1; i<n; i++)
	{
		x = a[i];
		j=0;

		atrib_i++;
		comp_i++;

		while(a[j]<x)
		{
			j++;
			comp_i++;
		}

		for(k=i; k>j; k--)
		{
		    a[k] = a[k-1];

			atrib_i++;
		}
		atrib_i++;
		a[j]=x;

	}
	suma_i = comp_i + atrib_i;
}


void Exemplu()
{

	int n = 6, i;
	int v1[]= { 60, -11, 0, -1, 10, 3};
	int v2[]={ 60, -11, 0, -1, 10, 3};
	int v3[]={  60, -11, 0, -1, 10, 3};

	printf("Se da vectorul: ");
	for(i = 0; i <n; i++)
		printf("%d ", v1[i]);
	
	printf("\n Demonstram corectitudinea algoritmilor:\n");
	printf("Sortare bubble: ");
	bubble(v1, n);
	for(i = 0; i <n; i++)
		printf("%d ", v1[i]);

	printf("\n Sortare selection: ");
	selection(v2, n);
	for(i = 0; i <n; i++)
		printf("%d ", v2[i]);

	printf("\n Sortare insertion:");
	insertion(v3, n);
	for(i = 0; i <n; i++)
		printf("%d ", v3[i]);
}

void Best()
{
	int n, i, v1[10000],v2[10000], v3[10000];
	FILE *pf=fopen("D:\\best.csv", "w");
	fprintf(pf, "n, comp_b, comp_s, comp_i, atrib_b, atrib_s, atrib_i, suma_b, suma_s, suma_i\n");

	for(n=100; n<=10000; n+=100)
	{
		for(i = 0; i <n; i++)
			{
				v1[i]=i;
				v2[i]=i;
				v3[i]=i;
		}

		bubble(v1,n);
		insertion(v2, n);
		selection(v3, n);
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", n,comp_b, comp_s, comp_i, atrib_b, atrib_s, atrib_i, suma_b, suma_s, suma_i);

	}
	fclose(pf);
	printf(" \nS-a creeat best.csw!");
}

void Worst()
{
	int n, i,val;
	int v1[10000],v2[10000], v3[10000];
	FILE *pf=fopen("D:\\worst.csv", "w");
	fprintf(pf, "n, comp_b, comp_s, comp_i, atrib_b, atrib_s, atrib_i, suma_b, suma_s, suma_i\n");
	for(n=100; n<=10000; n+=100)
	{
		for(i = 0; i <n; i++)
		{
			val=n-i;
			v1[i]=val;
			v2[i]=val;
			v3[i]=val;
		}

		bubble(v1,n);
		insertion(v2, n);
		selection(v3, n);
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", n,comp_b, comp_s, comp_i, atrib_b, atrib_s, atrib_i, suma_b, suma_s, suma_i);
	}
	fclose(pf);
	printf(" S-a creeat fisierul worst.csv ! \n");
}

void Average()
{
	int n, i, caz,val;
	int v1[10000],v2[10000], v3[10000];

	FILE *pf=fopen("D:\\average.csv", "w");
	long mediu_bcomp, mediu_scomp, mediu_icomp, mediu_batrib, mediu_satrib, mediu_iatrib, mediu_bsuma, mediu_ssuma, mediu_isuma;
	
	fprintf(pf, "n, comp_b, comp_s, comp_i, atrib_b, atrib_s, atrib_i, suma_b, suma_s, suma_i\n");

	srand(time(NULL));
	for(n=100; n<=10000; n+=100)
	{
		mediu_bcomp=0;
		mediu_scomp=0;
		mediu_icomp=0;
		mediu_batrib=0;
		mediu_satrib=0;
		mediu_iatrib=0;
		mediu_bsuma=0;
		mediu_ssuma=0;
		mediu_isuma=0;

		for(caz=1;caz<=5;caz++)
		{
			for(i = 0; i <n; i++)
			{
			   val=rand();
			   v1[i]=val;
			   v2[i]=val;
			   v3[i]=val;
			}

			bubble(v1,n);
			selection(v2, n);
			insertion(v3, n);

			
			// facem suma atribuirilor, a comparatiilor si a sumei, pentru ca apoi sa putem calcula o valoare medie
			
			mediu_bcomp+=comp_b;
			mediu_scomp+=comp_s;
			mediu_icomp+=comp_i;

			mediu_batrib+=atrib_b;
			mediu_iatrib+=atrib_i;
			mediu_satrib+=atrib_s;
			
			mediu_ssuma+=suma_s;
			mediu_bsuma+=suma_b;
			mediu_isuma+=suma_i;

		}
		fprintf(pf, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", n, mediu_bcomp/5, mediu_scomp/5, mediu_icomp/5, mediu_batrib/5, mediu_satrib/5, mediu_iatrib/5, mediu_bsuma/5, mediu_ssuma/5, mediu_isuma/5);

	}
	fclose(pf);
	printf("S-a creeat average.csw! \n");

}


int main()
{

	Exemplu();
	Best();
	Worst();
	Average();
	getch();
	return 0;
}
