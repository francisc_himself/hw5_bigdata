#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define white 0
#define gray 1
#define black 2 

typedef struct nod{
int key;
struct nod *next;
}NODE;



typedef struct{
int key;
int parent;
int d;//discovery
int f;//finish
int color;
#define V_MAX 10000
#define E_Max 60000
}GRAPH_NODE;
NODE  *adj[V_MAX];
GRAPH_NODE graph[V_MAX];
bool topologie;
int TOP[V_MAX];
int edges[E_Max];
int times;
int count;
void generate(int n,int m)
{
memset(adj,0,n*sizeof(NODE*));
FillRandomArray(edges,m,0,n*n-1,true);
for(int i=0;i<m;i++)
{
int a=edges[i]/n;
int b=edges[i]%n;
NODE *nod=(NODE*)malloc(sizeof(NODE));
nod->key=b;
nod->next=adj[a];
adj[a]=nod;
}
}
void DFS_VISIT(GRAPH_NODE u)
{
times++;
count++;
graph[u.key].d=times;
graph[u.key].color=gray;
NODE *v=adj[u.key];
while(v!=NULL)
{	count++;
	if (graph[v->key].color==white)
	{
		count++;
		graph[v->key].parent=u.key;
		if(u.key!=v->key);
 		DFS_VISIT(graph[v->key]);
	}
	v=v->next;
}
graph[u.key].color=black;
times++;
graph[u.key].f=times;

}
void DFS(int n)
{
for(int i=0;i<n;i++)
{
	graph[i].color=white;
	graph[i].parent=0;
}
times=0;
for(int i=0;i<n;i++)
{	count++;
	if (graph[i].color==white) DFS_VISIT(graph[i]);
}
}



void initG()
{
for(int i=0;i<V_MAX;i++)
	graph[i].key=i;
}

void afisareAdj(int n)
{ 

for(int i=0;i<n;i++)
{	printf("\n%d",i);
NODE *v=adj[i];
while(v!=NULL){
	printf("->%d ",v->key);
	v=v->next;
}
}
}



void afisareGraf(int n)
{
	printf("\nAfisare graf BFS\n");
for (int i=0;i<n;i++)
{
	printf("%d -> %d culoare: %d d=%d  f=%d\n",graph[i].parent,graph[i].key,graph[i].color,graph[i].d,graph[i].f);
}

}

void parcurgeLista(int n)
{
	topologie=true;
	for(int i=0;i<n;i++)
{	
	NODE *v=adj[i];
	while(v!=NULL){
		if(graph[v->key].parent==i){printf("\n%d -> %d tree edges",i,v->key);}
		else if((graph[i].d < graph[v->key].d) && (graph[v->key].d < graph[v->key].f) && (graph[v->key].f < graph[i].f) ){printf("\n%d -> %d forword edges",i,v->key);}
		else if((graph[v->key].d <= graph[i].d) && (graph[i].d < graph[i].f) && (graph[i].f <= graph[v->key].f) ){printf("\n%d -> %d back edges",i,v->key);topologie=false;}
		else if((graph[v->key].d < graph[v->key].f) && (graph[v->key].f < graph[i].d) && (graph[i].d < graph[i].f) ){printf("\n%d -> %d cross edges",i,v->key);}
		
		else {printf("\n%d -> %d ",i,v->key);}
		v=v->next;
	 
	}
}
}
void Topologie(int n)
{	
	if(topologie)
	{
	for(int i=0;i<n;i++)
	{	
	TOP[graph[i].d]=graph[i].key;	 	
	
	}
	printf("\n %d",TOP[1]);
	
	for(int i=2;i<=n*2;i++)
	{	
	if(TOP[i]!=0)
	printf(" -> %d",TOP[i]);
	}
	
	}
	else printf("\n!!!!  Graf cu cicluri\n");
}
void test()
{memset(adj,0,7*sizeof(NODE*));
	adj[0]=new NODE;adj[0]->next=new NODE;adj[0]->next->next=new NODE;
	adj[0]->key=1;
	adj[0]->next->key=2; 
	adj[0]->next->next->key=3;
	adj[0]->next->next->next=NULL;
	adj[1]=new NODE;adj[1]->next=new NODE;
	adj[1]->key=4;
	adj[1]->next->key=2;
	adj[1]->next->next=NULL;
	adj[2]=new NODE;
	adj[2]=NULL;
	//adj[2]->key=4;adj[1]->next->key=2;
	adj[3]=new NODE;
	adj[3]->key=2;
	adj[3]->next=NULL;
	adj[4]=new NODE;
	adj[4]->key=5;
	adj[4]->next=NULL;
	
	adj[5]=new NODE;
	adj[5]->key=6;
	adj[5]->next=NULL;
	
	adj[6]=new NODE;
	//adj[6]->key=2;
	adj[6]->key=0;
	adj[6]->next=NULL;
	
	afisareAdj(7);
	initG();
	DFS(7);
	afisareGraf(7);
	parcurgeLista(7);

	Topologie(7);
}
int main()
{
FillRandomArray(edges,10000,0,10000,true,0);
	int n=5;
	int m=7;
	generate(n,m);
	afisareAdj(n);
	initG();

	DFS(n);

afisareGraf(n);
	parcurgeLista(n);
	Topologie(n);
	printf("\n*****test");
	test();

FILE *f =fopen("rezM.csv","w");
fprintf(f,"numarMuchii \t count\n");
	for (int numarMuchii=1000;numarMuchii+1 <6000;numarMuchii+=100){
		FillRandomArray(edges,numarMuchii,0,100*100-1,true,0);
		generate(100,numarMuchii);
		initG();
		count=0;
		 DFS(100);	
	 	fprintf(f,"%d,%d\n",numarMuchii,count);
	}

FILE *l =fopen("rezN.csv","w");
fprintf(l,"numarN \t count\n");
	for (int numarN=100;numarN <200;numarN+=10){
		FillRandomArray(edges,9000,0,numarN*numarN-1,true,0);
		generate(numarN,9000);
		initG();
		
		count=0;
		DFS(numarN);	
	 	fprintf(l,"%d,%d\n",numarN,count);
	}
}