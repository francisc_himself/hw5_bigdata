/*
author:***ANONIM***�si ***ANONIM***
  group:***GROUP_NUMBER***
  date:27.03.2013
  program description : the program sorts an array using quicksort and heapsort.

  Average case: in average case the heapsort algorithm runs in O(nlogn) time because by swapping the first element from the heap with all elements,
				we do like a push heap, the heapify with the build heap takes O(n) and the swapping adds logn time for every node.

				The quicksort algorithm runs in O(n) time being more efficient than the heapsort.

Worst case for the quicksort is partitioning the array in such a way to have 1,n-1 elements every time(n varies between 1 and length of the array).
The best case is when the pivot is aleays the median. In best case the quicksort has linear running time.
*/

#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler p("lab3");

int hs=0;
int qs=0;
int qsr=0;
//int A[10]={0,3,1,4,6,7,2,8,9,5};
int A[10001];

int parent(int i)
{
	return (int)i/2;
}
int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i+1);
}

void heapify(int heap_size,int i)
{
	int l = left(i);
	int r = right(i);
	int largest = 0;
	if (l<=heap_size && A[l]>A[i]) largest = l;
		else largest = i;
		hs=hs+2;
    if (r<=heap_size && A[r]>A[largest]) largest = r;
	if (largest != i) 
		{
			int aux = A[i];
			A[i]=A[largest];
			A[largest]=aux;
			hs=hs+3;
			heapify(heap_size,largest);
		}	
}

void build_heap_BU(int n)
{
	for (int k=n/2;k>0;k--)
	heapify(n,k);
}

void heapsort(int n)
{
	build_heap_BU(n);
	int heap_size=n;
	for (int i=n;i>1;i--)
	{
		int aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		hs=hs+3;
		heap_size--;
		heapify(heap_size,1);

	}
}

int aux,x;

int partition(int p,int r)
{
	int aux1 = A[r];
	A[r]=A[(p+r)/2];
	A[(p+r)/2] = aux1;
	x = A[r]; qs++; qsr++;
	int i = p - 1;
	for (int j=p;j<=r-1;j++)
	{
		qs++;
		qsr++;
		if (A[j]<=x) 
		{
			
			i++;
			if (i!=j)
			{
			aux = A[j];
			A[j] = A[i];
			A[i] = aux;
			qs=qs+3;
			qsr=qsr+3;
			}
		}
	}
    if(i+1!=r)
	{
	aux = A[i+1];
	A[i+1] = A[r];
	A[r] = aux;
	qs=qs+3;
	qsr=qsr+3;
	}
	return i+1;
}

int q;


void quicksort(int p,int r)
{
	if (p<r)
	{
	q = partition(p,r);
	quicksort(p,q-1);
	quicksort(q+1,r);
	}
}

int randomized_partition(int p,int r)
{
   int i = rand()%(r-p+1)+p;
   int aux = A[i];
   A[i] = A[r];
   A[r] = aux;
   qsr=qsr+3;
   return partition(p,r);
}

void randomized_quicksort(int p,int r)
{
	if (p<r)
	{
		int q = randomized_partition(p,r);
		randomized_quicksort(p,q-1);
		randomized_quicksort(q+1,r);
	}
}

int main()
{
	
	//heapsort(10);
	//quicksort(1,10);

	//randomized_quicksort(1,10);

	//for(int i=1;i<=10;i++)
		//printf("%d ",A[i]);
	/*
	freopen("rand.txt","w",stdout);
	for(int n=100;n<=10000;n+=100)
	{
		printf("%d\n",n );
	for(int k=0;k<5;k++)
	{
		FillRandomArray(A,n);
		for(int i=0;i<n;i++)
			printf("%d ",A[i]);
		printf("\n");
	}
	printf("\n");
	}
	*/
	
	FILE *f,*g,*h,*l,*w,*m,*o,*z;
	//f=fopen("rand.txt","r");
	//g=fopen("heapsort.csv","w");
	//h=fopen("quicksort.csv","w");
	//l=fopen("quicksortrand.csv","w");
	//w=fopen("quicksortworst.csv","w");
	//m=fopen("randquicksortworst.csv","w");
	o=fopen("quicksortbest.csv","w");
	int n,k,i;
	for(int j=100;j<=10000;j+=100)
	{
		/*
		fscanf(f,"%d",&n);
		for(k=0;k<5;k++)
		{
			for(i=0;i<n;i++)
				fscanf(f,"%d",&A[i]);
			heapsort(n);
			//quicksort(1,n);
			//randomized_quicksort(1,n);
		}
		//fprintf(g,"%d,%d\n",n,hs/5);
		//fprintf(h,"%d,%d\n",n,qs/5);
		//fprintf(l,"%d,%d\n",n,qsr/5);
		/*
		for(i=1;i<=j;i++)
			A[i]=j-i+1;
		printf("%d\n", j);
		quicksort(1,j);
		fprintf(w,"%d,%d\n",j,qsr);
		*/
		for(i=0;i<j;i++)
			A[i]=i;

		printf("%d\n", j);
		quicksort(1,j);
		fprintf(o,"%d,%d\n",j,qsr);
		
		//hs=0;
		qs=0;
		//qsr=0;
		
	}
	

return 0;
}