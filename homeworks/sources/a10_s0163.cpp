/*Assign 10 ***ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***:
	DFS: sa demonstrat conform graficelor ca algoritmul de parcurgere in adancime are complexitate liniara: |V|+|E|
	deci algoritmi de parcurgere a grafurilor au aceeasi complexitate
	opertiile pe care am ales sa le numar sunt cele de comparatie respectiv assignare efectuate asupra vectorului visited,
	in care retin daca nodul curent a fost luat in considerare la unul din pasii anteriori si in caz contrar reapelez 
	DFS pentru acesta.
	De asemenea oricare dintre algoritmi poate fi folosit pentru sortarea topologica.
*/
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>

int no_op;
int graph[10000][10000];
int size[5000]={0},visited[5000]={0};
int sorted[100];
int count=0;

void DFS(int presentVertex)
{
	int iter;
	visited[presentVertex] = 1;
	no_op++;
	for(iter=1;iter<=size[presentVertex];iter++)
	{
		no_op++;
		if(!visited[graph[presentVertex][iter]])
		{	
			DFS(graph[presentVertex][iter]);
		}
	}
	sorted[count]=presentVertex;
	count++;
	return;
}

int contains(int vertex1,int vertex2,int size[])
{
	if(vertex1==vertex2)
		return 1;
	for(int i=0;i<size[vertex1];i++)
		if(graph[vertex1][i]==vertex2)
			return 1;
	return 0;
}

int main()
{
	int vertices,edges,iter;
	FILE *f=fopen("dfs.csv","w");
	srand(NULL);
	vertices=100;
	for (edges=1000;edges<5000;edges+=100)
	{
		int vertex1,vertex2;
		for (int k=0;k<5000;k++)
		{
			size[k]=0;
			visited[k]=0;
		}

		for(iter=0;iter<edges;iter++)
		{
			while(1)
			{
				vertex1=rand()%100;
				vertex2=rand()%100;
				if(contains(vertex1,vertex2,size)==0)
				{
					size[vertex1]++;
					size[vertex2]++;
					//printf("%d , %d \n",size[vertex1],size[vertex2]);
					graph[vertex1][size[vertex1]] = vertex2;
					graph[vertex2][size[vertex2]] = vertex1;
					break;
				}
			}
		}
		printf("%d\n",edges);
		int presentVertex;
		for(presentVertex=0;presentVertex<vertices;presentVertex++)
		{
			no_op++;
			if(!visited[presentVertex])
			{
				DFS(presentVertex);
			}
		}
		fprintf(f,"%d, %d,\n",edges,no_op);
		no_op=0;
	}

	edges=4500;
	for (vertices=100;vertices<200;vertices+=10)
	{
		int vertex1,vertex2;
		for (int k=0;k<5000;k++)
		{
			size[k]=0;
			visited[k]=0;
		}

		for(iter=0;iter<edges;iter++)
		{
			while(1)
			{
				vertex1=rand()%vertices;
				vertex2=rand()%vertices;
				if(contains(vertex1,vertex2,size)==0)
				{
					size[vertex1]++;
					size[vertex2]++;
					//printf("%d , %d \n",size[vertex1],size[vertex2]);
					graph[vertex1][size[vertex1]] = vertex2;
					graph[vertex2][size[vertex2]] = vertex1;
					break;
				}
			}
		}
		printf("%d\n",edges);
		int presentVertex;
		for(presentVertex=0;presentVertex<vertices;presentVertex++)
		{
			no_op++;
			if(!visited[presentVertex])
			{
				DFS(presentVertex);
			}
		}
		fprintf(f,"%d, %d,\n",vertices,no_op);
		no_op=0;
	}
	fclose(f);
	vertices=9;
	for (int i=0;i<10000;i++){
		size[i]=0;
		visited[i]=0;
	}
	size[1]++;
	graph[1][size[1]]=2;
	size[1]++;
	graph[1][size[1]]=4;
	size[1]++;
	graph[1][size[1]]=6;
	size[2]++;
	graph[2][size[2]]=3;
	size[4]++;
	graph[4][size[4]]=3;
	size[4]++;
	graph[4][size[4]]=5;
	size[5]++;
	graph[5][size[5]]=3;
	size[6]++;
	graph[6][size[6]]=5;
	size[6]++;
	graph[6][size[6]]=7;
	size[7]++;
	graph[7][size[7]]=8;
	size[8]++;
	graph[8][size[8]]=9;
	edges=11;
	int presentVertex;
	for(presentVertex=1;presentVertex<vertices;presentVertex++)
	{
		no_op++;
		if(!visited[presentVertex])
		{
			DFS(presentVertex);
		}
	}
	for (int i=count-1;i>=0;i--){
		printf("%d, ",sorted[i]);

	}
	return 0;
}