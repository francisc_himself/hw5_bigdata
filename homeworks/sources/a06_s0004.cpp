/*
Assign 6
***ANONIM*** ***ANONIM*** Razvan
Grupa ***GROUP_NUMBER***

Cerinta : implementati un algoritm eficient care calculeaza permutarea Josephus (n,m) , cand m e constant 

Vom construi un arbore binar in care nodurile au 2 campuri, primul din ele fiind numerele din multimea {1,2,...n},
iar al doilea fiind dimensiunea arborelui care are ca radacina acel nod. Arborele se doreste a fi cat mai echilibrat
pentru o cautare eficienta. 

Folosim o structura denumita NOD si una denumita Arbore.

Avem functii care aloca memorie , initializeaza arborele ( radacina acestuia ), construieste acest arbore
setand si dimensiunile nodurilor, functii de inserare, cautare, stergere, afisare, etc.

In main, generam valori pt n de la 100 la 10.000, iar m il luam ca fiind n/2

Eficienta generala : O(n*log(n)) , deoarece in functia Josephus se apeleaza OS_Select ( de eficienta log(n) ) intr-un "for"
ce depinde de "n".


*/



#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int atribuiri, comparatii;

typedef struct NOD 
{
	int key;
	int dim;
	struct NOD *st;
	struct NOD *dr;
	struct NOD *parent;
}Nod;


typedef struct tree
{
	Nod *rad;
}Arbore;

//arborele, declarat global
Arbore *T;


//initializare arbore
void init()
{
	T = (Arbore*) malloc(sizeof(Arbore));
	T->rad = 0;
	atribuiri++;
}

//construire arbore
Nod *construire(int st, int dr, Nod *p)
{
	int m;
	Nod *x;
	x=(Nod*)malloc(sizeof(Nod));
	//x->dim=1;
	if (st>dr)
		return 0;
	else
	{
		m=(st+dr)/2;
		x->key=m;
		x->parent=p;
		x->st=construire(st, m-1, x);
		x->dr=construire(m+1, dr, x);
		x->dim=0;
		atribuiri+=6;
	}

	return x;
}



//setarea dimensiunii
int setDim2(Nod *p)
{

	if (p==NULL)
		return 0;
	else
	{
		if((p->st)!=NULL)
			if(p->dr!=NULL)
				p->dim=setDim2(p->st)+setDim2(p->dr)+1;
			else
				p->dim=setDim2(p->st)+1;
		else
			if(p->dr!=NULL)
				p->dim=setDim2(p->dr)+1;
			else
				p->dim=1;
	
	}

	return p->dim;
}

//inserare in arbore
void inserare(Arbore *T, Nod *p)
{
	Nod *x, *y;
	x=T->rad;
	y=0;

	while(x!=0)
	{
		y=x;
		atribuiri++;
		if(p->key < x->key)
			x=x->st;
		else
			x=x->dr;
		atribuiri+=2;;
	
	}

	p->parent=y;
	atribuiri+=2;
	if(y==0)
		T->rad=p;
	
	else
		if(p->key < y->key)

			y->st=p;
		else
			y->dr=p;
	atribuiri+=2;

}


//cautare minim
Nod *cautareMin(Nod *p)
{
	while(p->st!=0)
	{
		p->dim--;
		p=p->st;
	}

	return p;
}


//functie de gasire a succesorului unui nod, ajutatoare la stergere
Nod *getSuccesor(Nod *p)
{
	Nod *q;
	
	if(p->dr!=0)
		return cautareMin(p->dr);

	q=p->parent;
	atribuiri++;
	while((q!=0) && (p==q->dr))
	{
		p=q;
		q=p->parent;
	}

	return q;
}

//afisare
void afisInordine(Nod *p, int nivel)
{
	int i;

	if(p==NULL)
		return;

	afisInordine(p->st, ++nivel);

	for(i=0;i<nivel;i++)
		printf("   ");

	printf("%d,%d \n", p->key, p->dim);

	afisInordine(p->dr, nivel);
}

//cautarea unui nod dupa cheie
Nod *cautare(Nod *p, int cheie)
{
	if (p==NULL || cheie==p->key)
		return p;

	if (cheie < p->key)
		return cautare(p->st, cheie);
	else
		return cautare(p->dr, cheie);
}


//stergere din arbore
Nod *stergere(Arbore *T, Nod *p)
{
	Nod *x, *y;

	atribuiri++;
	comparatii++;
	
	if(p->st==0 || p->dr==0)
		y=p;
	else
		y=getSuccesor(p);

	atribuiri++;
	comparatii++;

	if(y->st==0)
		x=y->dr;
	else
		x=y->st;

	atribuiri++;
	comparatii++;

	if(x!=0) 
		x->parent=y->parent;

	if(y->parent==0) 
		T->rad=x;
	else
		if(y==y->parent->st)
			y->parent->st=x;
		else y->parent->dr=x;

	if(y!=p)
		p->key=y->key;


	setDim2(T->rad);
	return y;
}


//functia OS_select, apelata din "Josephus"
Nod *OS_select(Nod* x, int i)
{
	int r;

	comparatii++;
	if (x!=NULL)
	{
		comparatii++;
		if(x->st!=NULL)
		{
			r=x->st->dim+1;
			atribuiri++;
			
		}
		else
			r=1;
		
		if (i==r)
			return x;
		else
		{
			if (i<r)
				return OS_select(x->st, i);
			else
				return OS_select(x->dr, i-r);
		}

	}
	
}


//functia care calculeaza efectiv permutarea Josephus
void Josephus(int n, int m)
{
	int i,j;
	Nod *x;
	init();
	T->rad=construire(1, n, 0);
	setDim2(T->rad);
	j=1;

	for (i=n;i>0;i--)
	{
		j=((j+m-2)%i)+1;
		x=OS_select(T->rad, j);
		//printf("\n %d", x->key);
		stergere(T, x);
	}
}



int main()
{
//testare pe exemplu mai mic


	/*Nod *x, *y;
	x=(Nod*)malloc(sizeof(Nod));
	y=(Nod*)malloc(sizeof(Nod));

	init();
	T->rad = construire(1, 8, 0);
	setDim2(T->rad);
	afisInordine(T->rad, 0);
	y=T->rad;

	for(int k=0;k<6;k++)
	{
	printf("\n\n");
	x=OS_select(y, 3);
	
	printf("Nodul sters: cheie:%d\n", x->key);
	stergere(T, x);
	

	//y=cautare(T->rad, x->key);
	afisInordine(T->rad, 0);
	}
	*/

	//Josephus(8,3);
	
//exemplul general
	FILE *f = fopen("Josephus.csv", "w");
	int n;
	fprintf(f, "n, operatii\n"); 


	for(n=100;n<=10000;n+=100)
	{
		atribuiri=0;comparatii=0;
		Josephus(n,n/2);
		fprintf(f, "%d, %d\n", n, atribuiri+comparatii);
		printf("%d\n",n);
	}

	fclose(f);
	getch();
}