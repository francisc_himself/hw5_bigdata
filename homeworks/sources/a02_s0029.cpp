//algoritmul bottom-up are eficienta O(n), pe cand cel Top-Down are eficienta O(n ln n);
#include <stdio.h> 
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 10001
int n;
int A[MAX];
int dim_B=0;
int B[MAX]={0};
int cont_B_U=0;
int cont_T_D=0;
void Inint_B()//procedura de initializare a heap-ului ce urmeaza sa fie construit cu metoda Top-Down
{
	dim_B=0;
}
void Pretty_Print(int A[])//se afiseaza doar pentru vectori de 10 elemente
{
	int i,j;
	int dist_max=7;
	for(i=1;i<=4;i++)
	{
		if(i==1)
		{
			for(j=1;j<=dist_max;j++) printf(" ");
			printf("%d",A[i]);
		}
		printf("\n");
		if(i==2)
		{
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i]);
			for(j=1;j<=dist_max;j++) printf(" ");
			printf("%d",A[i+1]);
		}
		printf("\n");
		if(i==3)
		{
			for(j=1;j<=dist_max/4;j++) printf(" ");
			printf("%d",A[i+1]);
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i+2]);
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i+3]);
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i+4]);

		}
		printf("\n");
		if(i==4)
		{
			printf("%d ",A[2*i]);
			printf("%d ",A[2*i+1]);
			printf("%d ",A[2*i+2]);

		}
	}
}
void Reconstituire_Heap(int A[],int i)
{
	int stanga=2*i;//stanga este indicele din sir de la pozitia 2i fata de refereinta
	int dreapta=(2*i)+1;//dreapta este indicele din sir la pozitia 2i+1 fata dereferinta
	int poz_min,aux;
	cont_B_U++;
	if(stanga<=n && A[stanga]<A[i]) //se face comparatie intre elementul de pe pozitia data si fiul sau stang
	{
		poz_min=stanga;	
	}
	else 
	{
		poz_min=i;	
	}
	cont_B_U++;
	if(dreapta<=n && A[dreapta]<A[poz_min])//minimul aflat anterior se compara cu nodul drept
	{
		poz_min=dreapta;
	}
	//ij final poz_min primeste indicele nodului minim
	if(poz_min !=i)//in cazul in care elementul de pe pozitia data nu fost minim
	{
		cont_B_U++;
		aux=A[i];
		A[i]=A[poz_min];                        // se face interschimbare cu minimul, astfel ca minimul ajunge parinte
		A[poz_min]=aux;
		Reconstituire_Heap(A,poz_min);   //se apeleaza recursiv pentru nodul tocmai pus pe post de parinte pentru a verifica daca subarborele sau respecta coonditia de heap
	}
}
void B_U(int A[])
{
	cont_B_U=0;
	int i;
	for(i=n/2;i>=1;i--) Reconstituire_Heap(A,i);
	//se face o parcurgere de la jumatatea sirului, pentru ca prima jumatate a sirului reprezita parintii
	//ce de a doa jumatate a sirului se constituie din frunze
	//frunzele se considera a fi deja heap-uri, de aceea pt ele nu este nevoie de apel
	printf("\n");
}
void Push(int C[],int x)
{
	cont_T_D++;
	cont_T_D++;
	dim_B=dim_B+1;//se construieste un al doilea si pentru a transforma sirul dat intr-un heap
	//se incepe prin a creste dmensiunea lui B
	C[dim_B]=x;//noul element va fi inserat ca si frunza, la sfarsitul sirului
	int i=dim_B;int aux;
	while (i>1 && C[i]<C[i/2])//cata vreme exista mai mult de un element in sir , iar fiul este mai mic decat parintele sau
	{
		cont_T_D++;
		cont_T_D++;
		aux=C[i];
		C[i]=C[i/2];    //se intershimba fiul cu parintele, pt a indeplini conditia de heap
		C[i/2]=aux;
		i=i/2;
	}
}

void T_D(int B[],int A[])
{
	int i;
	cont_T_D=0;
	Inint_B();
	for(i=1;i<=n;i++)
	{
		Push(B,A[i]);//fiecare element din sirul dat se va adauga la heap
	}
}


int main()
{
	n=10;
	srand(time(NULL));
	int i;
	int Copie_A[MAX];
	for(i=1;i<=n;i++)
		A[i]=rand()%10;
	for(i=1;i<=n;i++) Copie_A[i]=A[i];
	printf("Sirul dat este \n ");
	for(i=1;i<=n;i++) printf("%d ",A[i]);
	printf("\n");
	B_U(A);
	printf("Heap-ul dupa bottom-up \n");
	Pretty_Print(A);
	T_D(B,Copie_A);
	printf("\n");
	printf("Heap-ul dupa top-down \n");
	Pretty_Print(B);
	printf("\n Nr de operatii critice pentru bottom-up este %d",cont_B_U);
	printf("\n Nr de operatii critice pentru bottom-up este %d",cont_T_D);
	getch();
	FILE *f=fopen("Resultate5.csv","w");
	fprintf(f,"nr_elemente,Nr_op_Buttom_Up,Nr_op_Top_Down\n");
	n=100;
	while(n<=10000)
	{
		cont_B_U=0;
		cont_T_D=0;
		for(int k=1;k<=5;k++)
		{
		srand(time(NULL));
		for(i=1;i<=n;i++)
		{
		A[i]=rand()%100;
		Copie_A[i]=A[i];
		}
		B_U(A);
		T_D(B,Copie_A);
		}
		fprintf(f,"%d, %d, %d\n",n,cont_B_U/5,cont_T_D/5);
		n=n+100;
	}
	fclose(f);
	return 0;
}