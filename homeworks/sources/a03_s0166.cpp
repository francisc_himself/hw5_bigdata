/* ***ANONIM*** ***ANONIM***cu ***ANONIM***-grupa ***GROUP_NUMBER*** 
Pe baza graficului observam diferenta dintre heapsort si quicksort. In cazul mediu statistic este mai convenabil sa folosim
quicksort, deoarece observam ca suma dintre numarul de atribuiri si comparatii este mai mare la heapsort decat la quicksort.
In cazul cel mai defavorabil eficienta pentru quicksort este de O(n^2), iar  pentru cazul favorabil si mediu eficienta
este de O(nlogn);
*/

#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#define MAX 10001
int mq=0,mh=0;
int mfav,mdef;

int parinte(int i){
	return i/2;
}
int st(int i){
	return 2*i;
}
int dr(int i){
return 2*i+1;
}

//va returna indexul minim dintre un parinte si fii
int index_min(int a[],int i,int dim_a){
     int imin=i;
     mh++;
    if((st(i)<=dim_a) &&(a[i]>a[st(i)]))
		imin=st(i);

	mh++;
	if((dr(i)<=dim_a) &&(a[imin]>a[dr(i)]))
		imin=dr(i);
	
	return imin;
}


//functia de reconstructie a heap-ului
void reconstituie_heap(int a[],int i,int dim_a){
	int index,aux;
	
	index=index_min(a,i,dim_a);
	
	if(index!=i){
	  mh+=3;
	  aux=a[i];
	  a[i]=a[index];
	  a[index]=aux;
	  reconstituie_heap(a,index,dim_a);
	
}
}

//construieste heap bu
void construire_heap(int a[],int dim_a){
for(int i=dim_a/2;i>=1;i--)
	reconstituie_heap(a,i,dim_a);
}
//construire heap_bu
void heap_bu(int a[],int dim_a){
	int aux,m;
	 m=dim_a;//m- dimHeap
	construire_heap(a,dim_a);
	//afisare_sir(a, m);
	for(int i=dim_a;i>=2;i--){
		mh+=3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		m--;
		reconstituie_heap(a,1,m);
	}
}

//functia partition folosita pentru impartirea vectorului in subectori
int partition( int *m, int l, int r) 
{
   int pivot, i, j, aux;//variabilele pivot, i, j, t
    mq++;
   pivot = m[l];//pivotul se initializeaza cu elementul de pe pozitia l din matrice
   i=l-1;	
   j=r+1;   

   while (1) 
   {
	do {
		j=j-1;
		mq++;
	   }
	while (m[j]>pivot);

	do 
		{
		mq++;
		i=i+1;
		}
    while (m[i]<pivot);
	
	if (i<j) {
				mq+=3;
				aux = m[j];
				m[j]=m[i];
				m[i]=aux;
				}
		   else 
			   return j;
   }


}
void quickSort( int *m, int l, int r) //procedura quickSort primeste ca parametri un vector de
                                       //numere intregi+ 2 variabile l si r pentru limitele stanga si dreapta
{
   int j;

   if( l < r ) //compara indexul extremitatii stangi cu cel al extremitatii drepte
   {
    
    j = partition( m, l, r);
    quickSort( m, l, j);
    quickSort( m, j+1, r);
   }
}


int partition1( int *m, int l, int r) 
{
   int pivot, i, j, aux;//variabilele pivot, i, j, t
    mq++;
   pivot = m[(l+r)/2];//pivotul se initializeaza cu elementul de pe pozitia l din matrice
   i=l-1;	
   j=r+1;   

   while (1) 
   {
	do {
		j=j-1;
		mq++;
	   }
	while (m[j]>pivot);

	do 
		{
		mq++;
		i=i+1;
		}
    while (m[i]<pivot);
	
	if (i<j) {
				mq+=3;
				aux = m[j];
				m[j]=m[i];
				m[i]=aux;
				}
		   else 
			   return j;
   }


}
void quickSort1( int *m, int l, int r) //procedura quickSort primeste ca parametri un vector de
                                       //numere intregi+ 2 variabile l si r pentru limitele stanga si dreapta
{
   int j;

   if( l < r ) //compara indexul extremitatii stangi cu cel al extremitatii drepte
   {
    
    j = partition1( m, l, r);
    quickSort1( m, l, j);
    quickSort1( m, j+1, r);
   }
}

   void afisare_sir(int a[],int dim_a){
for(int i=1;i<=dim_a;i++)
	printf("%d ",a[i]);
printf("\n");
}
void genereaza_rand(int v[],long n,int param)
{
	int i;
	srand(param);
	for(i=0;i<n;i++)
		v[i]=rand()%10000;
}
//sirul generat crescator
void genereaza_crescator(int v[],long n)
{
	int i;
	for(i=0;i<n;i++)
		v[i]=i+1;
}

//sirul generat descrescator
void genereaza_descrescator(int v[],long n)
{
	int i;
	for(i=0;i<n;i++)
		v[i]=n-i;
}

int main(){
	//am realizat testarea
	/*int a[]={0,2,8,7,1,3,5,6,4};
	quickSort(a,1,8);
	afisare_sir(a,8);
	getch();*/
	FILE *comp;
	int sir[MAX];
	int x[MAX];
	int i,n;
	int mh1,mq1;
	comp=fopen("quick.csv","w");
	fprintf(comp,"n ,SumaHeapMed,SumaQuickMed,QuickFav, QuickDefav\n");
	for(n=100;n<=MAX;n+=100){
	//cazul defavorabil
		mh=0; mfav=0;
		mq=0;  mdef=0;
	genereaza_crescator(sir,n);
    quickSort(sir,1,n);
	mdef=mq;	
	mq=0;
	//cazul favorabil
	genereaza_descrescator(sir,n);
    quickSort1(sir,1,n);
	mfav=mq;	
	mq=0;
		//cazul mediu
	mh1=0;
	mq1=0;
	  for (int i=0; i<5; i++){
		  genereaza_rand(sir,n,i);
		  for (int j=1; j<n+1; j++) x[j]=sir[j];
		  mh=0;
		  heap_bu(sir,n);
		  mh1=mh1+mh/5;
		   for (int j=1; j<n+1; j++) sir[j]=x[j];
		   mq=0;
		   quickSort(sir,1,n);
		   mq1=mq/5;
	
	  }
	   fprintf(comp,"%d , %d, %d, %d, %d, \n",n,mh1,mq1,mfav,mdef);

	}
}