#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
/*
In aceasta lucrare de laborator am studiat metodele de sortare heapsort si quicksort.
Heapsort este o metoda de sortare ce foloseste structura de heap. Complexitatea acesteia este O(N*log(N)) in toate cele trei
cazuri: favorabil,defavorabil si mediu statistic.
Quicksort este o metoda de sortare ce foloseste principiul divide et impera. Pentru cazul favorabil, aceasta este cea mai
rapida metoda de sortare. Complexitatea ei este O(N*log(N)) in cazul favorabil, O(N) in cazul mediu statistic si
O(N^2) in cazul cel mai putin favorabil.


*/
//variabile globale
static long nra,nrc,nra1,nrc1;



//Heapify Down (functie din laboratorul anterior)

void heapify(int A[],int i,int hp_size) {
int s,d,max,aux;
	s=2*i+1;
	d=2*i+2; 
	nrc++;
	if (s<=hp_size && A[s]>A[i]) {
		max=s;
	} else {
		max=i;
	}
	
	nrc++;
	if (d<=hp_size && A[d]>A[max]) {
		max=d;
	}
	if (max!=i) {
		aux=A[i];
		A[i]=A[max];
		A[max]=aux;
		
		nra+=3;
		heapify(A,max,hp_size);
	}

}
//Constriueste Heap Bottom Up (functie din laboratorul anterior
void buildHeap(int A[],int n,int *size) {
int heapSize;

	(*size)=n-1;
	heapSize=(*size);
	for (int i=n/2;i>=0;i--) {
		heapify(A,i,heapSize);
	}
}



//Heapsort cu construire bottom Up
void heapSort(int A[],int n){
int aux,heapSize;
	buildHeap(A,n,&heapSize);
	for (int i=n-1;i>=1;i--) {
		aux=A[0];
		A[0]=A[i];
		A[i]=aux;
		nra+=3;
		heapSize--;
		heapify(A,0,heapSize);
	}
	
}




//Functie quickSort
void quickSort(int A[], int st, int dr) {  
      int i = st, j = dr;  
      int x;  
      int pivot = A[(st+dr)/2];  
	 
      while (i<=j) {
		nrc1++;
		while (A[i]<pivot) {  
			i++;  
			nrc1++;
			
		}
		
		while (A[j]> pivot) {  
			j--;  
			nrc1++;
		}
		nrc1++;
        if (i<=j) {  
			x= A[i];  
            A[i] =A[j];  
            A[j] =x;  
            i++;  
            j--;  
			nra1+=3;
        }  
      };  
	  
      if (st<j)  
           quickSort(A,st,j);  
      if (i<dr)  
           quickSort(A,i,dr);  
}  
//Generare sir de numere aleatoare
void random(int a[],int n)
{
	for(int i=0;i<n;i++)
		a[i]=rand();
}
//Copiere sir in alt sir, folosita pentru a sorta aceleasi siruri pentru ambele metode
void copysir(int a[], int b[], int n)
{
	for (int i=0;i<n;i++)
		a[i]=b[i];
}




    




//---------------------------------------------------MAIN--------------------------------------------
int main(){
	
	int sir1[10000],sir2[10000];
	FILE *pf,*pf2;
	pf=fopen("Sortari2.csv","w");
	fprintf(pf,"n,Atrib_heapSort,Comp_heapSort,atrib+comp_heapSort,Atrib_quickSort,Comp_quickSort,atrib+comp_quickSort\n");
	pf2=fopen("f1.txt","w");
	random(sir1,10000);
	//generare 5 fisiere cu numere aleatoare
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f2.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f3.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f4.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
	fclose(pf2);
	pf2=fopen("f5.txt","w");
	random(sir1,10000);
	for(int k=0;k<10000;k++)
		fprintf(pf2,"%d ",sir1[k]);
    fclose(pf2);
	for(int k=100;k<=10000;k+=100)
	{
		nra=0;nrc=0;nra1=0;nrc1=0;
			for (int j=1;j<=5;j++) 
			{
				switch (j) 
				{
					case(1): pf2=fopen("f1.txt","r"); break;
					case(2): pf2=fopen("f2.txt","r"); break;
					case(3): pf2=fopen("f3.txt","r"); break;
					case(4): pf2=fopen("f4.txt","r"); break;
					case(5): pf2=fopen("f5.txt","r"); break;
					default: break;
				}
				if(pf2)
				{
					for(int i=0;i<k;i++)
					fscanf_s(pf2,"%d",&sir1[i]);
				}
				copysir(sir2,sir1,k);
				heapSort(sir1,k);
				
				quickSort(sir2,0,k-1);
				fclose(pf2);
				printf("%d%c\r",k/100,'%');
			}
		fprintf(pf,"%d,%d,%d,%d,%d,%d,%d\n",k,nra/5,nrc/5,(nra+nrc)/5,nra1/5,nrc1/5,(nra1+nrc1)/5);
	}
	fclose(pf);
	
	
	
	return 0;


}
