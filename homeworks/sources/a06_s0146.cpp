﻿#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

typedef struct NOD {
   int key;
   int dim;
   NOD *dr;//dreapta
   NOD *st;//stanga
   NOD *p;//parinte
}NOD;

NOD* root;


NOD* BUILD_TREE(int prim,int ultim)
{
	if(prim<=ultim)
	{
		int m=(prim+ultim)/2;
		NOD* ch=(NOD*)malloc(sizeof(NOD));
		ch->key=m;
		ch->dim=ultim-prim+1;
		

		ch->st=BUILD_TREE(prim,m-1);
		if(ch->st!=NULL)
			ch->st->p=ch;

		ch->dr=BUILD_TREE(m+1,ultim);
		if(ch->dr!=NULL)
			ch->dr->p=ch;

		return ch;
	}
	else return NULL;
}

NOD* OS_SELECT (NOD *x,int i)
{
	int r=1;
	if(x->st!=NULL)
		r=x->st->dim+1;

	if (i==r)
		return x;
	else if ((i<r)&&(x->st!=NULL))
			return OS_SELECT(x->st,i);
		else if(x->dr!=NULL) return OS_SELECT(x->dr,i-r);
}


NOD* TREE_MIN(NOD* x)
{
	while(x->st!=NULL)
		x=x->st;
	return x;
}

NOD* TREE_SUCCESOR(NOD* x)
{
	NOD* y;
	if(x->dr!=NULL)
		return TREE_MIN(x->dr);
	y=x->p;
	while((y!=NULL)&&(x=y->dr))
	{
		x=y;
		y=x->p;
	}
	return y;
}

NOD* OS_DELETE(NOD* z)
{
	NOD* y;
	NOD* x;
	if ((z->st==NULL)||(z->dr==NULL))
		y=z;
	else y=TREE_SUCCESOR(z);

	if(y->st!=NULL)
		x=y->st;
	else x=y->dr;

	if(x!=NULL)
		x->p=y->p;

	if(y->p==NULL)
		root=x;
	else if (y==y->p->st)
		y->p->st=x;
	else y->p->dr=x;

	if(y!=z)
		z->key=y->key;

	while((y->st!=NULL)||(y->dr!=NULL))
	{
		y->dim--;
		y=y->p;
	}
	free(y);

	return z;
}


//pretty print
void afisare(NOD* v,int nivel)
{
	if(v!=NULL)
	{
		afisare(v->dr,nivel+1);
		for(int i=1;i<=nivel;i++)
			printf("            ");
		//afisam: nodul(dimensiuea,parintele)
		if (v!=root)
				printf("%d(%d,%d)", v->key,v->dim,v->p->key);
			else printf("%d(%d,null)", v->key,v->dim);
		printf("\n");
		afisare(v->st,nivel+1);
	}
}

void JOSEPHUS(int n,int m) 
{
	root = BUILD_TREE(1,n) ;
	afisare(root,1);
	printf("\n \n");

	int j =1; 
	for (int k =n;k>=1;k--) 
	{
		j =(( j + m - 2) % k) + 1 ;
		NOD* x =OS_SELECT(root, j ); 
		OS_DELETE(x) ;
		afisare(root,1);
	}
}

void main()
{
	JOSEPHUS(8,3);
}