/*
***ANONIM*** ***ANONIM*** Levnete grupa ***GROUP_NUMBER***

in cazul mediu:
la construirea heapului prin metoda buttom-up avem eficienta O(n) iar in cazul construiri top-down avem O(n*log(n))

azul defavorabil:
la construirea heapului prin metoda buttom-up avem eficienta O(n) iar in cazul construiri top - down ar trebui sa obtinem O(n*log(n)) 

*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "math.h"
#include "time.h"



#define MAX_SIZE 10000
#define MINUS_INFINIT -32000000

unsigned long long bu=0,td=0;


//BUTTOM-UP

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}


void max_heapify(int *a, int i,int size)
{
	int l,r,largest,aux;

	l=left(i);
	r=right(i);
	bu++;//
	if (l<= size && a[l]>a[i])
		largest = l;
	else
		largest = i;
	
	bu++;//
	if (r<= size  && a[r]>a[largest])
		largest = r;
	
	if (largest!=i)
	{	bu+=3;//
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;

		max_heapify(a,largest,size);
	}
}

void build_max_heap(int *a,int size,int n)
{
	int i;
	for (i=n/2;i>=1;i--)
		max_heapify(a,i,size);
}



//TOP-Down
int heap_maximum(int *a)
{
	return a[1];
}
int heap_extract_max(int *a,int *size)
{	int max;
	if(*size < 1)
		printf("Eroare");

	max = a[1];
	a[1]=a[*size];
	*size = *size - 1;
	max_heapify(a,1,*size);
	return max;
}


void heap_increase_key(int *a,int i,int cheie)
{	td++;//
	if(cheie < a[i])
		printf("Eroare");

	a[i] = cheie;
	td++;//
	
	td++;//
	while(i>1 && a[parent(i)]<a[i])
	{	td++;//
		td+=3;//

		int aux = a[i];
		a[i]= a[parent(i)];
		a[parent(i)]=aux;
		i = parent(i);
		
	}
	
}
void max_heap_insert(int *a,int cheie,int *size)
{
	(*size) ++;
	a[*size] = MINUS_INFINIT;
	td++;//

	heap_increase_key(a,*size,cheie);

}


int main()
{	//TESTE
	int m=8;
	int dim,dim1,i;

	int h1[]={9,3,10,11,20,5,1,26,56};
	int h2[]={9,3,10,11,20,5,1,26,56};

	//pozitiile incep de la 1 prin urmare primul element,5 nu este luat in considerare
	 dim = m; 
	 printf("Vectorul initial pentru testare:\n");
	 
	  for(i=1;i<=m;i++)
		 printf(" %d ",h1[i]);
     
	  build_max_heap(h1,dim,m);

      printf("\n\nTOP DOWN:\n");
	    for(i=1;i<=m;i++) {
		
        printf(" %d ",h1[i]);  
	     
		}
	     
	
	 printf("\n\nBUTTOM UP:\n");

	dim = 0;
	int b[9];
	for(i=1;i<=m;i++) {
		max_heap_insert(b,h2[i],&dim);
        printf(" %d ",h2[i]);  
	}


	//CAZUL MEDIU
	
	int A[MAX_SIZE+1];
	int B[MAX_SIZE+1];
	int C[MAX_SIZE+1];

	FILE *fisier1;
	fisier1 = fopen("mediu.csv", "w");
	fprintf(fisier1,"n,buttom-up,top-down\n");
	printf("\nCazul mediu!\n");
	int n;
	for(n=100; n<=MAX_SIZE; n += 100)
	{	printf("\n%d / 10 000",n);
		for (int j=1;j<=5; j++)
		{
			//generez aleatoriu
			for (i=1; i<=n; i++)
			{	
				int aux = rand();
				A[i]=aux;
				B[i]=aux;
			}
		build_max_heap(A,n,n);

		dim=0;
		for(i=1;i<=n;i++)
			max_heap_insert(C,B[i],&dim);
	
			
		}
		
		fprintf(fisier1,"%d,%llu,%llu\n",n,bu/5,td/5);
		
		bu=0;
		td=0;
	}
	//CAZUL DEFAVORABIL
		
	bu=0;
	td=0;

	FILE *fisier2;
	fisier2 = fopen("defavorabil.csv", "w");
	fprintf(fisier2,"n,buttom-up,top-down\n");
	printf("\nCazul defavorabil!\n");
	for(n=100; n<=MAX_SIZE; n += 100)
	{		int aux = rand();
			A[1]=aux;
			B[1]=aux;
			for (i=2; i<=n; i++)
			{	
				//a1[n-i+1]=a1[n-i+2]+rand();
				aux = rand();
				A[i]=A[i-1]+aux;
				B[i]=B[i-1]+aux;
			}

		build_max_heap(A,n,n);

		dim=0;
		for(i=1;i<=n;i++)
			max_heap_insert(C,B[i],&dim);
		
		fprintf(fisier2,"%d,%llu,%llu\n",n,bu,td);
		
		bu=0;
		td=0;
	}

	fclose(fisier1);
	fclose(fisier2);
	printf("\n Aplicatia o luat sfarsit");
	
	getch();	
	return 0;
	
}