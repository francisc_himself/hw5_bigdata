#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
#include <time.h>
#include "Profiler.h"
using namespace std;
Profiler profiler ("Graphs");
typedef struct nod
{
	int key;
	int rank;
	nod* parent;
};
typedef struct edge
{
	nod* start;
	nod* stop;
};
nod *varf[20000];
edge *muchie[70000];
int nrv, nre, m,f,u;
void makeset(nod *varf)
{
	varf->parent = varf;
	varf->rank = 0;
	m++;
}
void link (nod* a, nod* b)
{
	if (a->rank > b->rank)
		b->parent = a;
	else
	{
		a->parent = b;
		if(a->rank == b->rank)
			a->rank ++;
	}
}
nod* findset (nod* a)
{
	if (a!=a->parent)
		a->parent = findset(a->parent);
	f++;
	return a->parent;
}
void unnion (nod* a, nod* b)
{
	link(findset(a),findset(b));
	u++;
}
void connectedcomponents(int nrv, int nre)
{
	for(int i=0;i<nrv;i++)
		makeset(varf[i]);
	for(int j=0;j<nre;j++)
		if(findset(muchie[j]->start)!=findset(muchie[j]->stop))
			unnion(muchie[j]->start,muchie[j]->stop);
}
void example(){
	int array[7] = {0,0,0,0,0,0,0};
	int i,j;
	int ok = 0;
	int x;
	nrv=9;
	nre=7;
	for(int i=0;i<nrv;i++){
		varf[i]=(nod*)malloc(sizeof(nod));
		varf[i]->key=i+1;
	}
	muchie[0] = (edge*)malloc(sizeof(edge));
	muchie[0]->start = varf[0];
	muchie[0]->stop = varf[1];

	muchie[1] = (edge*)malloc(sizeof(edge));
	muchie[1]->start = varf[1];
	muchie[1]->stop = varf[2];

	muchie[2] = (edge*)malloc(sizeof(edge));
	muchie[2]->start = varf[2];
	muchie[2]->stop = varf[3];

	muchie[3] = (edge*)malloc(sizeof(edge));
	muchie[3]->start = varf[3];
	muchie[3]->stop = varf[0];

	muchie[4] = (edge*)malloc(sizeof(edge));;
	muchie[4]->start = varf[6];
	muchie[4]->stop = varf[5];
	
	muchie[5] = (edge*)malloc(sizeof(edge));
	muchie[5]->start = varf[5];
	muchie[5]->stop = varf[6];

	muchie[6] = (edge*)malloc(sizeof(edge));
	muchie[6]->start=varf[7];
	muchie[6]->stop=varf[8];

	/*muchie[7] = (edge*)malloc(sizeof(edge));
	muchie[7]->start = varf[3];
	muchie[7]->stop = varf[8];*/
	
	
	connectedcomponents(nrv,nre);

	printf("nodes: \n");
	for (i=0;i<nrv;i++)
		printf(" %d ", varf[i]->key);
	printf("\n edges: \n");
	for(j=0;j<nre;j++)
		printf("(%d - %d) ",muchie[j]->stop->key, muchie[j]->start->key);
	
	printf("\n Connected compoenents \n");
	do{
		ok=1;
		for(i=0;i<nrv;i++){
			if(array[i]==0)
				ok = 0;
			}
		for(i=0;i<nre;i++){
			if(i==0)
			{
				printf(" (%d,%d) ", muchie[i]->stop->key,muchie[i]->start->key);
				x=muchie[i]->start->key;
				array[i]=1;
			}
			else{
				if(muchie[1]->stop->key == x){
					printf(" (%d,%d) ", muchie[i]->stop->key,muchie[i]->start->key);
					x=muchie[i]->start->key;
					array[i]=1;
				}
				else{
					printf("\n");
					if(i<nre){
						printf(" (%d,%d) ",muchie[i]->stop->key,muchie[i]->start->key);
						x=muchie[i]->start->key;
					}
				}
			}

		}
	}while(ok!=0);

}
void main()
{
	/*nrv=10000;
	for(nre = 10000;nre<=60000;nre+=100)
	{	
		m=0;
		f=0;
		u=0;
		for(int i=0;i<nrcv;i++)
			varf[i]=new nod;
		for(int i=0;i<nre;i++)
		{
			muchie[i]=new edge;
			muchie[i]->start=varf[rand()%10000];
			muchie[i]->stop=varf[rand()%10000];
		}
		connectedcomponents(nrv,nre);
		profiler.countOperation("makeset",nre,m);
		profiler.countOperation("find",nre,f);
		profiler.countOperation("union",nre,u);
		int total =f+m+u;
		profiler.countOperation("total",nre,total);

	}
	profiler.showReport();*/

	example();
	getch();
}