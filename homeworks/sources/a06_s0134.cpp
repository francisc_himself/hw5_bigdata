/*
The running time of the algorithm is of O(n logn).
*/
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<fstream>
#include<iostream>

using namespace std;

fstream f("josephus.csv",ios::out);
int n,m;
long nrA=0;
long nrC=0;

typedef struct node
{
	int key;
	int dimension;
	node *left, *right, *parent;
}NODE;
NODE* root;

NODE* OS_SELECT(NODE *x, int i)
{
	int r=1;
	if (x->left != NULL)							//we look for the node that must be eliminated
		r = x->left->dimension+1;					//on left and right branches
	else
		r = 1;
	if (r == i)
		return x;
    else
	{
		if (i < r)
		{
			return OS_SELECT(x->left,i);
		}
        else
		{
			return OS_SELECT(x->right,i-r);
		}
    }
}

NODE* GENERATE(int x, int dim, NODE *l, NODE *r)
{
	NODE *q;
	q= new NODE;
	q->key=x;					//q is the new node and gets the value x
	q->left=l;				//left child is created
	q->right=r;				//right child is created
	q->dimension=dim;			//dimension is added
	nrA+=4;
	if (l!=NULL)
	{
		l->parent = q;		//parent-child relations are done
	}
	if (r!=NULL)
	{
		r->parent=q;
	}
	q->parent=NULL;			//q is the root and has no parent
	nrA++;
	return q;
}

NODE* BUILD(int l, int r)
{
	NODE *ds,*d;
	if (l > r)
		return NULL;
	if (l == r)
		return GENERATE(l, 1, NULL, NULL);		//left tree is generated
	else
	{
		int mid =(l + r)/2;
		ds = BUILD(l, mid-1);
		d  = BUILD(mid+1, r);
		return GENERATE(mid, r-l+1,ds, d);
	}
}

NODE* MINIMUM(NODE* x)  //function used in delete
{
	while(x->left!=NULL)
	{
		x = x->left;
		nrA++;
		nrC++;
	}
	nrC++;
	return x;						//leftmost node is returned (the minimum)
}

NODE* SUCCESOR(NODE* x)
{
	NODE *y;
	if(x->right!=NULL)						//right successor is searched after
	{
		nrC++;
		return MINIMUM(x->right);				//if we still have right children, we look for the minimum
	}
	nrA++;
	y = x->parent;							//y becomes the parent of the node if the prev. search for min is not done
	while (y != NULL && x == y->right)		//if y is not null and x is equal to the right child of y
	{
		nrC+=2;								//x becomes y
		x = y;								//we go upper in the tree
		y = y->parent;
		nrA+=2;
	}
	return y;								//successor is returned
}

NODE* DELETE(NODE* z)
{
	NODE *y;
	NODE *x;

	if ((z->left == NULL) || z->right == NULL)		//check if node is leaf
	{
		nrA++;
		y=z;
	}
	else
		y = SUCCESOR(z);							//if not, we look for its successor
	nrC++;
	if (y->left != NULL)							//successor has left child
	{
		nrA++;
		x = y->left;
	}
	else
	{
		nrA++;										//succesor has right child
		x = y->right;
	}
	nrC++;
	if (x != NULL)
	{
		nrA++;
		x->parent = y->parent;
	}
	nrC++;
	if (y->parent == NULL)
		root = x;
	else
		if (y == y->parent->left)
		{
			nrA++;
			y->parent->left = x;
		}
		else
		{
			nrA++;
			y->parent->right = x;
		}
	nrC+=2;
	if (y != z)
	{
		nrA++;
		z->key = y->key;
	}
	return y;
}

void NEW_DIMENSION(NODE *p)
{
	while (p != NULL)
	{
		nrA++;
		p->dimension--;		//decrease dimension for each node, starting with the one placed in the eliminated node's position or on new position                         
		p=p->parent;
		nrA++;
	}
}
void pretty_print(NODE *p,int level)
{

  if (p!=NULL)
  {
	pretty_print(p->right,level+1);
	for( int i=0;i<=level;i++) 
		cout<<"\t";
	cout<<p->key<<"("<<p->dimension<<")"<<endl;
	pretty_print(p->left,level+1);
	cout<<endl;
}
}
void JOSEPHUS(int n, int m)
{
	NODE *y,*z;
	int aux = n;
	int mid = m;

	root = BUILD(1, n);		//we build the tree for each length 1,n
	//pretty_print(root,0);
	for (int i = 1; i < aux; i++)	//we traverse each time until we reach aux which will decrease each time we eliminate an element
	{
		y = OS_SELECT(root, m);	//select node for deletion
		//cout<<"The node that was deleted is :"<<y->key<<endl;
		z = DELETE(y);			//delete node
		NEW_DIMENSION(z);			//dimension is remade for all nodes starting from the one that was eliminated
		//pretty_print(root,0);
		delete(z);					//free memory
		n--;						//decrease tree length
		if (n > 0)					//next position to be deleted is generated
			m = (m - 1 + mid) % n;
		if (m == 0)
			m = n;
	}

}

int main()
{
	//JOSEPHUS(7,3);
	for(int i=100; i<=10000; i+=300)
	{
		nrA=0;
		nrC=0;
		JOSEPHUS(i,i/2);
		f<<i<<","<<nrA<<","<<nrC<<","<<nrA+nrC<<endl;

	}
	getch();
}
