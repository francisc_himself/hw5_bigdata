#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
//***ANONIM*** ***ANONIM*** gr ***GROUP_NUMBER***
double qsop[100];
double hsop[100];
double nrqs;
double nrhs;
int a[20000],b[20000];
//in cazul tabloului sortat crescator heapsort: complexitate O(nlogn) quicksort: complexitate O(n^2)
//in caz random heapsort: complexitate O(nlogn) quicksort: complexitate O(nlogn)
//in cazul tabloului sortat descrescator heapsort: complexitate O(nlogn) quicksort: complexitate O(n^2)
void copie(int *a,int *b,int length)
{
	for(int i=1;i<=length;i++)
		b[i]=a[i];
}

void afis(int *a,int length)
{
	printf("\n");
	for(int i=1;i<=length;i++)
		printf("%d ",a[i]);
}

void scrie_rez(FILE *f,double *a,int length)
{
	fprintf(f,"\n");
	for(int i=1;i<=length;i++)
		fprintf(f,"%.0f;",a[i]);
}

void bottom_up(int *a,int n)
{
	int i,j,aux,max;
	bool b;
	for(i=n/2;i>=1;i--)
	{
		j=i;
		b=true;
		while(b)
		{
			if(((2*j+1)<=n) && a[j]<a[2*j+1])
			{
				max=2*j+1;
			}else
			{
				max=j;
			}
			if(a[max]<a[2*j]){
				max=2*j;
				nrhs+=1;
			}
			nrhs+=4;
			if(max!=j)
			{
				aux=a[max];
				a[max]=a[j];
				a[j]=aux;
				j=max;
				nrhs+=3;
			}else
			{
				b=false;
			}
		}
	}
}

void heapify(int *a,int n)
{
	int j=1;
	bool b=true;
	int max;
	int aux;
	while(b && j<=n/2)
	{
		if(((2*j+1)<=n) && a[j]<a[2*j+1])
		{
			max=2*j+1;
		}else
		{
			max=j;
		}
		if(a[max]<a[2*j]){
			max=2*j;
			nrhs+=1;
		}
		nrhs+=4;
		if(max!=j)
		{
			aux=a[max];
			a[max]=a[j];
			a[j]=aux;
			j=max;
			nrhs+=3;
		}else
		{
			b=false;
		}
	}
}


void heapsort(int *a,int n)
{
	int i,aux;
	bottom_up(a,n);
	for(i=1;i<=n;i++)
	{
		heapify(a,n-i+1);
		aux=a[1];
		a[1]=a[n-i+1];
		a[n-i+1]=aux;
		nrhs+=3;
	}
}

int part(int *a,int p,int r)
{
	nrqs++;
	int x=a[r];
	int i=p-1;
	int j;
	int aux;
	for(j=p;j<=r-1;j++)
	{
		nrqs++;
		if(a[j]<=x){
			nrqs=nrqs+3;
			i++;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	i++;
	aux=a[i];
	a[i]=a[r];
	a[r]=aux;
	nrqs=nrqs+3;
	return i;
}


void qSort(int *a,int p, int r)
{
	int q=0;
	if(p<r)
	{
		nrqs++;
		q=part(a,p,r);
		qSort(a,p,q-1);
		qSort(a,q+1,r);
	}
}


int main(){
	int i=0,n=10;
	FILE *f;
	srand(time(0));
	//random case
	f=fopen("random.csv","w");
	for(n=100;n<=10000;n+=100)
	{
		nrqs=0;
		nrhs=0;
		for(i=1;i<=n;i++)
		{
			a[i]=rand();
		}
		copie(a,b,n);
		heapsort(b,n);
		copie(a,b,n);
		qSort(b,1,n);
		qsop[n/100]=nrqs;
		hsop[n/100]=nrhs;
	}
	scrie_rez(f,qsop,100);
	scrie_rez(f,hsop,100);
	fclose(f);
	//worst case
	f=fopen("worst.csv","w");
	for(n=100;n<=10000;n+=100)
	{
		nrqs=0;
		nrhs=0;
		a[1]=rand();
		for(i=2;i<=n;i++)
		{
			a[i]=a[i-1]-fabs((double)(rand()%3));
		}
		copie(a,b,n);
		heapsort(b,n);
		copie(a,b,n);
		qSort(b,1,n);
		qsop[n/100]=nrqs;
		hsop[n/100]=nrhs;
	}
	scrie_rez(f,qsop,100);
	scrie_rez(f,hsop,100);
	fclose(f);
	//best case
	f=fopen("best.csv","w");
	for(n=100;n<=10000;n+=100)
	{
		nrqs=0;
		nrhs=0;
		a[1]=rand();
		for(i=2;i<=n;i++)
		{
			a[i]=a[i-1]+fabs((double)(rand()%3));
		}
		copie(a,b,n);
		heapsort(b,n);
		copie(a,b,n);
		qSort(b,1,n);
		qsop[n/100]=nrqs;
		hsop[n/100]=nrhs;
	}
	scrie_rez(f,qsop,100);
	scrie_rez(f,hsop,100);
	fclose(f);

	printf("\n\n####Sfirsit####");
	getch();
	return 0;
}
