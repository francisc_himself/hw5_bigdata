#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include "time.h"
/*
***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
From the graphs we can observe that the bottom-up heap building is faster than the top-down.
Bottom-up has a complexity O(n) , while the top-down has a complexity of O(nlgn).
The bottom-up build heap it's usage is for sorting , the sort is an optimal one (O(nlgn)).
The top-down build heap is used for priority queues , when we need to insert elements not know the dimension of the inserted elements.
The bottom-up build the array must have a fixed dimensions , while the top-down build we set the size dynamically with inserting the elements.
*/
int n, HeapSize=10000;
int a[1000000];
int bupatr,bupcmp,tdownatr,tdowncmp;

void swap(int &x,int &y)
{
    int tmp;
    tmp=x;
    x=y;
    y=tmp;
}
void heapify (int i)
{
    int largest=i,left=2*i,right=2*i+1;

    bupcmp++;
    if (left<=n && a[left]>a[largest])
        largest=left;
    bupcmp++;
    if (right<=n && a[right]>a[largest])
        largest=right;
    if (largest!=i)
    {
        bupatr=bupatr+3;
        swap(a[i],a[largest]);
        heapify(largest);
    }
}

void BottomUp()
{
    for (int i=n/2;i>0;i--)
        heapify(i);
}


void insertValue(int i)
{
    tdowncmp++;
    while(i>1 && a[i/2]<a[i])
    {
        tdownatr=tdownatr+3;
        swap(a[i/2],a[i]);
        i=i/2;
        tdowncmp++;
    }

}

void TopDown()
{
    for (int i=2;i<=n;i++)
        insertValue(i);
}

void print()
{
    int pow=2,level=1,nr=5;
    for (int i=0;i<n;i++)
        printf(" ");
    printf("%d\n\n  ",a[1]);

    for(int i=2;i<=n;)
    {
        level=pow;
        for(int j=0;j<nr;j++)
            printf(" ");
        while(i<=n && level>0)
        {
            printf(" %d ",a[i]);
            i++;
            level--;
        }
    printf("\n\n");
    pow=pow*2;
    nr--;
    }
}



int main()
{

    /*scanf("%d",&n);
    for (int k=1;k<n;k++)
        scanf("%d",&a[k]);*/
    FILE *fs=fopen("avg.csv","w");
	FILE *wc=fopen("worst.csv","w");
	fprintf(fs,"n,Bottom-up,Top-down\n");
	fprintf(wc,"n,Bottom-up,Top-down\n");
    srand(time(NULL));
	for (n=10;n<=11;n=n+100)
	{
	    bupatr=0;bupcmp=0;tdownatr=0;tdowncmp=0;
        for (int i=0;i<=n;i++)
            a[i]=rand()%1000;
        BottomUp();
       print();
       // printf("\n\n");
        for (int i=0;i<=n;i++)
            a[i]=rand()%1000;
        TopDown();
        print();
      fprintf(fs,"%d,%d,%d\n",n,bupcmp+bupatr,tdownatr+tdowncmp);

      for (int i=0;i<n;i++)
        a[i]=i;
      BottomUp();

      for (int i=0;i<n;i++)
        a[i]=i;
      TopDown();

      fprintf(wc,"%d,%d,%d\n",n,bupcmp+bupatr,tdownatr+tdowncmp);
	}
    return 0;
}
