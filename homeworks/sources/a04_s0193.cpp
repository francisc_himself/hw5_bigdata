#include<iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include<fstream>
using namespace std;

fstream f1("lists1.csv",ios::out);
fstream f2("lists2.csv",ios::out);
fstream f3("lists3.csv",ios::out);
fstream f4("lists4.csv",ios::out);
fstream f5("lists5.csv",ios::out);

	typedef struct node
	{
		int info;
		node *next;
	} NODE;

	typedef struct element
	{
		int pos;
		int elem;
	} ELEMENT;

	struct lists
	{
		NODE *first;
		NODE *last;
	};
	struct lists CAP[501];

	NODE *interfirst ;
	NODE *interlast;
	long nrC , nrA;
	ELEMENT Heap[501];
	int a[10001];

void add(NODE **first,NODE **last,int val)//add an element to the list
{
    if((*first)==NULL)// special case if the list is void
    {
        (*first) = new NODE;
        (*first)->info=val;
        (*first)->next=NULL;
        *last=(*first);
    }
    else
    {
        NODE *elem=new NODE;
        elem->next=NULL;
        elem->info=val;
        (*last)->next=elem;
        (*last)=elem;
    }
}

void max_heapify_down(ELEMENT Heap[],int i,int size)// we arrange the heap vector so that all the parents are smaller than their sons
{
    int l,r,minimum;
    ELEMENT aux;
    l=2*i;
    r=2*i+1;
    if ((l<=size)&&(Heap[l].elem < Heap[i].elem))//we save the son that is smaller than the parent
        minimum=l;
    else
        minimum=i;
    if ((r<=size)&&(Heap[r].elem < Heap[minimum].elem))
        minimum=r;
    nrC+=2;
    if (minimum!=i)//we replace for the case we found a son smaller than the parent
    {
        aux=Heap[i];
        Heap[i] = Heap[minimum];
        Heap[minimum]=aux;
        nrA+=2;
        max_heapify_down(Heap,minimum,size);
    }
}

void insert_heap(ELEMENT Heap[],ELEMENT x,int *size)//for adding an element in the heap in a top-down manner
{
    (*size)++; //adds element x to the end
    Heap[(*size)]=x;
    int i=(*size);
    ELEMENT aux;
    nrC++;
    while (i>1 && (Heap[i].elem)<(Heap[i/2].elem))
    {
        nrC++;
        aux=Heap[i];
        Heap[i]=Heap[i/2];
        Heap[i/2]=aux;
        nrA+=2;
        i=i/2;
    }
}

ELEMENT extract_heap(ELEMENT Heap[],int *size)// return the first element in the heap
{
    ELEMENT x;
    if((*size)>0)
    {
        x=Heap[1];
        Heap[1]=Heap[(*size)];
        nrA++;
        (*size)--;
        max_heapify_down(Heap,1,(*size));// we arrange the heap vector so that all the parents are smaller than the sons
    }
    return x;
}

void merge(lists CAP[],int k)
{
    int size = 0;
    ELEMENT v;
    for(int i=1; i<=k; i++)//we add to the vector heap k smallest elements from the lists
    {
        v.pos=i;
        v.elem=CAP[i].first->info;
        nrA++;
        insert_heap(Heap,v,&size);
    }

    ELEMENT pop;
    while (size>0)
    {

        pop = extract_heap(Heap,&size);
        int poz =pop.pos;
        add(&interfirst,&interlast,pop.elem);
        CAP[poz].first=CAP[poz].first->next;
        nrA++;
        nrC++;
        if (CAP[poz].first!=NULL)
        {
            v.pos=pop.pos;
            v.elem=CAP[pop.pos].first->info;
            nrA++;
            insert_heap(Heap,v,&size);//we add to the heap once more the smallest elements
        }
    }
}


void create_k_lists(lists CAP[],int k,int n)
{
    int nr = 0;
    for (int i=1; i<=k; i++)
        CAP[i].first = CAP[i].last = NULL;

    for (int j=1; j<=n/k; j++)

    {
        for (int i=1; i<=k; i++)
        {
            nr=nr+rand()%100;
            add(&CAP[i].first,&CAP[i].last,nr);
        }
    }

}

void list(NODE *first,NODE *last)
{
    NODE *c;
    c=first;
    while(c!=0)//as long as there are still elements in the list
    {
        cout<<c->info<<"  ";
        c=c->next;//we move into the list going to the next address
    }
    cout<<endl;
}

void drop_all(lists CAP[],int k)
{
    NODE *p;
    for(int i=1; i<=k; i++)
    {
        while ( CAP[i].first!=0)
        {
            p=CAP[i].first;
            CAP[i].first=CAP[i].first->next;
            delete p;
        }
        delete CAP[i].first;
        delete CAP[i].last;
    }

    while (interfirst!=NULL)
    {
        p = interfirst;
        interfirst = interfirst->next;
        delete p;
    }
    delete interfirst;
    delete interlast;
}


int main()
{

    int k=0;
    f1<<"n"<<","<<"nrA+nrC"<<",";
    f1<<"\n";
    f2<<"n"<<","<<"nrA+nrC"<<",";
    f2<<"\n";
    f3<<"n"<<","<<"nrA+nrC"<<",";
    f3<<"\n";
    f4<<"n"<<","<<"nrA+nrC"<<",";
    f4<<"\n";
    f5<<"n"<<","<<"nrA+nrC"<<",";
    f5<<"\n";
    for (int f = 1 ; f <=5 ; f++ )
    {
        for (int n = 100 ; n <= 10000; n+=300)
        {

            nrA = nrC = 0;
            for (int l=1; l<=3; l++)
            {
                drop_all(CAP,k);
                nrA = nrC = 0;
                if (l==1)
                    k=5;
                else if (l==2)
                    k=10;
                else
                    k=100;

                create_k_lists(CAP,k,n);
                merge(CAP,k);

                switch(f)
                {
                case(1):

                    f1<<n<<","<<nrA+nrC<<",";
                    break;
                case(2):
                    f2<<n<<","<<nrA+nrC<<",";
                    break;
                case(3):
                    f3<<n<<","<<nrA+nrC<<",";
                    break;
                case(4):
                    f4<<n<<","<<nrA+nrC<<",";
                    break;
                case(5):
                    f5<<n<<","<<nrA+nrC<<",";
                    break;
                default:
                    break;
                }

            }


            switch(f)
            {
            case(1):
                f1<<endl;
                break;
            case(2):
                f2<<endl;
                break;
            case(3):
                f3<<endl;
                break;
            case(4):
                f4<<endl;
                break;
            case(5):
                f5<<endl;
                break;
            default:
                break;
            }
        }

       switch(f)
            {
            case(1):
                f1<<"\n\n\n";
                break;
            case(2):
                f2<<"\n\n\n";
                break;
            case(3):
                f3<<"\n\n\n";
                break;
            case(4):
                f4<<"\n\n\n";
                break;
            case(5):
                f5<<"\n\n\n";
                break;
            default:
                break;
            }
        int   n=10000;
         f1<<"k"<<","<<"nrA+nrC"<<",";
        f1<<"\n";
        f2<<"k"<<","<<"nrA+nrC"<<",";
        f2<<"\n";
        f3<<"k"<<","<<"nrA+nrC"<<",";
        f3<<"\n";
        f4<<"k"<<","<<"nrA+nrC"<<",";
        f4<<"\n";
        f5<<"k"<<","<<"nrA+nrC"<<",";
        f5<<"\n";
       for (int k = 10 ; k<=500 ; k+=10)
            {
                nrC = nrA = 0;
                drop_all(CAP,k);
                create_k_lists(CAP,k,n);
                merge(CAP,k);

                switch(f)
                {
                case(1):
                    f1<<k<<","<<nrA+nrC<<"\n";
                    break;
                case(2):
                    f2<<k<<","<<nrA+nrC<<"\n";
                    break;
                case(3):
                    f3<<k<<","<<nrA+nrC<<"\n";
                    break;
                case(4):
                    f4<<k<<","<<nrA+nrC<<"\n";
                    break;
                case(5):
                    f5<<k<<","<<nrA+nrC<<"\n";
                    break;
                default:
                    break;
                }

            }

    }
    f1.close();
    f2.close();
    f3.close();
    f4.close();
    f5.close();

/*
    //demo
    int k=4;
    int n=20;
    drop_all(CAP,k);
    create_k_lists(CAP,k,n);
    list(CAP[1].first,CAP[1].last);
    list(CAP[2].first,CAP[2].last);
    list(CAP[3].first,CAP[3].last);
    list(CAP[4].first,CAP[4].last);

    merge(CAP,k);
    list(interfirst,interlast);
*/
    return 0;
}

