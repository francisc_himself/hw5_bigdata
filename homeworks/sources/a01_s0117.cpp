// for the best case the best algorithms are insertion sort and bubble sort, having a number of total operations with 1 less then the size of the array,
//and the worst is selection sort
//for the average case for insertion sort and selection sort, even though I obtained a smaller number of operations for insertio sort
//it's hard to tell which one is better, the only certain thing is that bubble sort does much more operations, almost double then insertion and
// selection together
//for the worst case selection sort is by far the best, followed by insertion with a number of operations twice as selection, and again, the worst 
//is bubble sort
//in general, bubble sort is the worst sorting algorithm from these 3, while selection sort is the most constant 
#include <stdio.h>
#include <conio.h>
#include "Profiler.h" // used to generate the graphs
Profiler profiler("demo");
const int max=50000;
const int min=100;
void insertionSort(int a[], int n){ // mothod for insertion sort
	profiler.countOperation("count_insertion",n,0);
	for (int i = 1 ;i<n;i++){
		int k=i-1;
		int buff = a[i];
		while(a[k]>buff && k>=0){
			a[k+1]=a[k];
			profiler.countOperation("assig_insertion",n); // ass ++
			k--;
			profiler.countOperation("count_insertion",n); //cmp++
		}
		profiler.countOperation("count_insertion",n);
		a[k+1]=buff;
		profiler.countOperation("assig_insertion",n); //ass++
	}
}

void selectionSort(int a[], int n){
int aux,min;
for(int i=0 ; i<n-1;i++){
min=i;
for (int j=i+1;j<n;j++){
	profiler.countOperation("count_selection",n);
	if (a[j]<a[min]) min=j;
}
aux = a[min];
a[min]=a[i];
profiler.countOperation("assig_selection",n,3);
a[i]=aux;

}
}

void bubbleSort(int *a, int n){
int swap,i,aux;
profiler.countOperation("assig_bubble",n,0);
do{
	swap=0;
	for(i=0;i<n-1;i++)
	{
		if (a[i]>a[i+1])
		{
			aux=a[i];
			a[i]=a[i+1];
			a[i+1]=aux;
			swap = 1;
			profiler.countOperation("assig_bubble",n,3);
		}
		profiler.countOperation("count_bubble",n);
	}
} while(swap);
}

int* increasing(int n){ //generates increasing array
int i;
int *a=new int[n];
for(i=0;i<n;i++)
a[i]=i+1;
return a;
}

int* decreasing(int n){//generates decreasing array
int i;
int *a=new int[n];
for(i=0;i<n;i++)
a[i]=n-i;
return a;
}

int* worstselection(int n){
	int i;
	int *a=new int[n];
	for(i=0;i<n;i++){
		if (i==n) a[i] = n-i+1;
		else a[i] = i+2;
	}
	return a;
}

int* generateRandom(int n){
int *a= new int[n];
srand(time(NULL));
for (int i=0;i<n;i++)
a[i]=rand()%(max+min)-min;
return a;
}

int* copyString(int* a,int n){
int* b = new int[n];
for(int i=0;i<n;i++)
b[i]=a[i];
return b;
}




void main(){
	/*int a[] = {3,6,3,7,8,2};
	int b[] = {4,5,7,2,3,1};
	int c[] = {3,4,5,2,4,6,8};
	int d[] = {23,34,5,6,4,3};
	int e[] = {2,3,4,2,1,2,3,56};
	int f[] = {45,65,3,2,4};
	selectionSort(f,5);
	insertionSort(e,8);
	insertionSort(c,7);
	selectionSort(d,6);
	insertionSort(a,6);
	selectionSort(b,6);
	profiler.createGroup("assignments","assig_insertion","assig_selection");
	profiler.createGroup("comparisons","count_insertion","count_selection");
	profiler.showReport();
	for (int i=0;i<6;i++)
		printf("%d ",b[i]);
	printf("\n");
	for (int i=0;i<6;i++)
		printf("%d ",a[i]);
	_getch();*/
	

	//average case
	/*for (int j=0;j<5;j++){
		printf("j=%d\n",j);
	for(int i=100;i<10000;i=i+500)
	{
		int *a= generateRandom(i);
		int *b = copyString(a,i);
		int *c= copyString(a,i);
		insertionSort(a,i);
		selectionSort(b,i);
		bubbleSort(c,i);
	} 
	}
	profiler.createGroup("average assignments","assig_insertion","assig_selection","assig_bubble");
	profiler.createGroup("average comparisons","count_insertion","count_selection","count_bubble");
	profiler.addSeries("total_insertion","assig_insertion","count_insertion");
	profiler.addSeries("total_selection","assig_selection","count_selection");
	profiler.addSeries("total_bubble","assig_bubble","count_bubble");
	profiler.createGroup("average total","total_insertion","total_selection","total_bubble");
	
	*/


	//best case bubble sort
	/*for(int i=100;i<10000;i=i+100)
	{
		int *a= increasing(i);
		
		bubbleSort(a,i);
	}
	profiler.createGroup("best case bubble sort","assig_bubble");
	profiler.createGroup("best case bubble sort","count_bubble");
	profiler.addSeries("total_bubble","assig_bubble","count_bubble");
	profiler.createGroup("best case bubble sort","total_bubble");
	*/

	//best case insertion sort
	/*for(int i=100;i<10000;i=i+100)
	{
		int *a= increasing(i);
		
		insertionSort(a,i);
	}
	profiler.createGroup("best case insertion sort","assig_insertion");
	profiler.createGroup("best case insertion sort","count_insertion");
	profiler.addSeries("total_insertion","assig_insertion","count_insertion");
	profiler.createGroup("best case insertion sort","total_insertion");
	*/


	//best case selection sort
	/*for(int i=100;i<10000;i=i+100)
	{
		int *a= increasing(i);
		
		selectionSort(a,i);
	}
	profiler.createGroup("best case selection sort","assig_selection");
	profiler.createGroup("best case selection sort","count_selection");
	profiler.addSeries("total_selection","assig_selection","count_selection");
	profiler.createGroup("best case selection sort","total_seleciton");
	*/


	//worst case bubble sort
	/*for(int i=100;i<10000;i=i+100)
	{
		printf("%d\n",i);
		int *a= decreasing(i);
		
		bubbleSort(a,i);
	}
	profiler.createGroup("worst case bubble sort","assig_bubble");
	profiler.createGroup("worst case bubble sort","count_bubble");
	profiler.addSeries("total_bubble","assig_bubble","count_bubble");
	profiler.createGroup("worst case bubble sort","total_bubble");
	*/

	//worst case insertion sort
	/*for(int i=100;i<10000;i=i+100)
	{
		int *a= decreasing(i);
		
		insertionSort(a,i);
	}
	profiler.createGroup("worst case insertion sort","assig_insertion");
	profiler.createGroup("worst case insertion sort","count_insertion");
	profiler.addSeries("total_insertion","assig_insertion","count_insertion");
	profiler.createGroup("worst case insertion sort","total_insertion");
	*/

	
	////worst case selection!!!!
	/*for(int i=100;i<10000;i=i+100){
		int *a = worstselection(i);
		selectionSort(a,i);
	}
	profiler.createGroup("worst case selection sort","assig_selection");
	profiler.createGroup("worst case selection sort","count_selection");
	profiler.addSeries("total_selection","assig_selection","count_selection");
	profiler.createGroup("worst case selection sort","total_selection");

	*/

	
	profiler.showReport();
}