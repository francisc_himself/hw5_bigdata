#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int N=9973;

int c1=1;
int c2=1;


int succ=0; //succesful operations
int unsucc=0; //unsuccessful operations
long avs=0;
long avsu=0;
int operations=0;
int s1=0;
int s2=0;

//quad hash function
unsigned int hprim(int k ){
return (k % N);
}

int h(int k, int i){
	unsigned int aux=(hprim(k)+c1*i+c2*i*i) % N;
	if(aux<0) {
			aux=aux+N;
	printf("Minus");
	}
	return aux;
}

//insert elements in the hash table
int insert(int *a, int val){
	int i=0;
	//operations=0;
	int key=h(val,i);

	printf(" insert %d\n",key);

	while(a[key]!=-1){
		i++;
		key=h(val,i);
	}

	a[key]=val;
	return key;
}

//search for a element in tha hash table
int search_element(int *a, int val){
	int i=0;
	operations=0;
	int key=h(val,i);
	printf("search %d\n",key);

	//search until we find the element of i is smaller than N
	while((a[key]!=val)&&(a[key]!=-1)&&(i<N)){
		i++;
		operations++;
		key=h(val,i);
		//if(key<0) key=(key+N) %N;
		//printf("%d\n",key);
	}
	if((i>=N)||(a[key]==-1)) return -1;
	else return  key;
}



int main(){
    int *hash= new int[N+1];
    int *value=new int[5];
    value[0]=8000;
    value[1]=8500;
    value[2]=9000;
    value[3]=9500;
    value[4]=9900;

    FILE *f;
    f=fopen("hash.csv","w");

    int aux=0;
    //srand(24);
   // int m=3000;
    int y=0;
    int *searched=new int[3000];
    int found=0;
    fprintf(f,"Filling_Factor avg_eff_found max_eff_found avg_eff_un-found max_eff_un-found\n");

    for(int i=0;i<5;i++){

        succ=0;
        unsucc=0;
        avs=0;
        avsu=0;
        operations=0;
        s1=0;
        s2=0;

        for(int j=0;j<N;j++)
            hash[j]=-1;

        for(int j=0;j<value[i];j++)
        {
            aux=rand()%10000+1;
            if(y<1500){
                searched[y]=aux;
                y++;
            }

		insert(hash,aux);
        }

        for(y=1500;y<3000;y++){
            searched[y]=rand()%10000+10000;
        }

        for(int j=0;j<3000;j++){
            found=search_element(hash,searched[j]);
            if(found>=0){
            //succesful search
                if(succ<operations)
                    succ=operations;
                avs=avs+operations;
                s1++;
            }
            else{
                //unsuccesfull search
                if(unsucc<operations) unsucc=operations;
                avsu=avsu+operations;
                s2++;
            }
            printf("FOUND %d\n",found);
        }

        avs=avs/s1;
        avsu=avsu/s2;
        fprintf(f,"0.%d %d %d %d %d\n",value[i]/100,avs,succ,avsu,unsucc);
    }

    fclose(f);

return 0;
}
