/*
author:***ANONIM***�si ***ANONIM***
  group:***GROUP_NUMBER***
  date:10.04.2013
  program description : the program inserts a key and searches for a specified key in a hash table with open addressing. The hash function is a quadratic 
  one, using the key and the first and second power of the variable i which is the ierator on the table.

  The search operation is done in case of inserted keys and keys that are not inserted in the table. For keys that are inserted in the table the average 
  effort doesn't grow so quickly and the max effort is really low even when the filling factor is 0.99. But when we want to search for keys that are
  not inserted in the table the average effort grows really quick and the max effort is greater as well. Especially when the filling factor is closer to 1.


  */
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler p("lab5");

int HT[10001];
int hash_size=9973;
int op=0;

int h(int key,int i)
{
	return (key + i*23+i*i*3) % hash_size;
}

int hash_insert(int key)
{
	int i=0;
	do
	{
	int j=h(key,i);
	if (HT[j]==-1)
		{
			HT[j]=key;
			return j;
		}
	else i++;
	}
	while(i!=hash_size);
		return -1;
}

int hash_search(int key)
{
	int i=0;
	int j;
	do
	{
		j=h(key,i);
		op++;
		if (HT[j]==key) return j;
		else i++;
	}
	while (HT[j]!=-1 && i<hash_size);
		return -1;
}

int main()
{
	

	int A[20000];
	float alfa=0.99;
	int fail=0;
	int n;
	int max=0;
		int fail1=0;
	int total=0;
	int nr=0;

	for(int y=0;y<5;y++)
	{
		for (int i=0;i<hash_size;i++)
		HT[i]=-1;
	n=(int)hash_size*alfa;
	FillRandomArray(A,n,1,30000,true,0);
	for(int j=0;j<n;j++)
	{
		if (hash_insert(A[j])<0) fail++;
		
	}
	printf("%d\n",fail);
	
	FillRandomArray(A,n,30000,55000,true,0);
	for(int k=0;k<1500; k++)
	{
		if(hash_search(A[rand()%n])<0) 
	   {
		if(op>max) max=op;
		total =total + op;
		nr++;
		
		}
		op=0;
	}
	}
	float avg=(float)total/nr;
	printf("%d %d %d %f\n",max,total/5,nr/5,avg);
return 0;
}
