﻿/* 

***ANONIM***ș ***ANONIM*** grupa ***GROUP_NUMBER***

Implementati corect si eficient metodele Heapsort si Quicksort.

Heap-Sort: O(nlogn)
Quick-Sort: O(nlogn)

Desi teoretic, in cazul mediu, ambele metode au aceeasi complexitate, quick-sort se dovedeste a fi mai eficient, realinzand mai putine operatii.
In caz defavorabil quick sortul are complexitatea O(n^2), precum insertia.
Cazul favorabil este acela in care pivotul este chiar elementul median al sirului si astfel partitiile devin egale.
Cazul defavorbail este acele in care una din partitii contine un element iar cealalta restul elementelor.


*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include<time.h>

#define DIM_MAX 10000 

int dimA, C[DIM_MAX];
long opQS, opHS;

////////////////////////////////////////  SORTAREA_HEAP /////////////////////////
int Parinte(int i)
{
	return (i/2);
}

int Stanga(int i)
{
	return (2*i);
}

int Dreapta(int i)
{
	return (2*i+1);
}

void ReconstituieHeap(int A[], int i)
{
	int s, d, max, aux;

	s=Stanga(i);
	d=Dreapta(i);

	opHS++;
	if(s<=dimA && A[s]>A[i])
		max = s;
	else max = i;

	opHS++;
	if(d<=dimA && A[d]>A[max])
		max = d;

	if(max!=i)
	{
		opHS+=3;
		aux = A[i];
		A[i]=A[max];
		A[max]=aux;
		ReconstituieHeap(A, max);
	}
}

void ConstruiesteHeap(int A[], int n)
{
	dimA = n;
	for(int i= n/2; i>=1; i--)
		ReconstituieHeap(A, i);
}

void HeapSort(int A[], int n)
{
	int i, aux;
	ConstruiesteHeap(A, n);
	for(i=dimA; i>=2; i--)
	{
		opHS+=3;
		aux = A[1];
		A[1] = A[i];
		A[i] = aux;
		dimA--;
		ReconstituieHeap(A, 1);
	}

}

//////////////////////////////////////// QUICK_SORT ///////////////////////////////////
int partitie(int A[], int st, int dr, int pivot)
{
	int i, j, aux;
	opQS++;
	pivot = A[st];
	i = st - 1;
	j = dr + 1;

	while(true){
		
		do{
			j--;
			opQS++;
		}while(A[j]>pivot);

		do{
			i++;
			opQS++;
		}while(A[i]<pivot);

		if(i<j)
		{
			opQS+=3;
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;
		}
		else return j;
	}
}

void QuickSort(int A[], int st, int dr)
{
	int B[DIM_MAX], i;
	int p, pivot;

	if(st < dr )
	{
		p = partitie(A, st, dr, pivot);
		QuickSort(A, st, p);
		QuickSort(A, p+1, dr);
	}
	
}

////////////////////////////////////////  testarea ////////////////////////////////////
void citire(int A[],int B[], int *n)
{
	int i;
	printf("n=");
	scanf("%d", n);

	
	for(i = 1; i<=*n; i++)
	{
		printf("A[%d]=", i);
		scanf("%d", &A[i]);
		C[i]=A[i];
		B[i]=A[i];
	}
}


void afiseaza(int A[], int B[], int n)
{
	int i, j, p=2, spatii=n;

	printf("QUICKSORT\n");
	 for(i = 1; i<=n; i++)
		 printf(" %d    ", B[i]);
	 printf("\nHEAPSORT\n");
	for(i = 1; i<=n; i++)
		 printf(" %d    ", A[i]);
	
}

void Testare()
{
	int n,i;
	int A[DIM_MAX], B[DIM_MAX];
	citire(A, B, &n);
	HeapSort(A, n);
	QuickSort(B, 1, n);
	afiseaza(A, B, n);
	getch();
}

void MediuStatistic()
{
	int A[DIM_MAX], B[DIM_MAX];
	int n,i, caz, m=5, var;
	long sumQS = 0, sumHS = 0;

	FILE *pf;
	pf = fopen("mediu.csv", "w");

	if(!pf)
	{
		perror("Eroare deschidere fisier!");
		exit(1);
	}

	srand(time(NULL));

	fprintf(pf, "n, opQS, opHS\n");
	for(n=100;n<=10000;n+=100)
	{
		sumHS = 0;
		sumQS = 0;
		
		for(caz=1;caz<=m;caz++)
		{
			opHS = 0; 
			opQS = 0;
			
			for(i=1;i<=n;i++)
			{
				var = rand();
				A[i] = var;
				B[i] = var;
			}

			HeapSort(A, n);
			QuickSort(B, 1, n);
			
			sumHS += opHS;
			sumQS += opQS;
		}
		fprintf(pf, "%d, %d, %d\n", n, sumQS/5, sumHS/5 );
	}

	fclose(pf);
	printf("Gata!");
}


void FisierQS()
{
	
	int A[DIM_MAX], B[DIM_MAX], D[DIM_MAX];
	int n,i, caz, aux;
	long m=0, f=0, d=0;

	FILE *pf;
	pf = fopen("quickSort.csv", "w");

	if(!pf)
	{
		perror("Eroare deschidere fisier!");
		exit(1);
	}

	srand(time(NULL));

	fprintf(pf, "n, fav, mediu, defav\n");
	for(n=100;n<=10000;n+=100)
	{
		f=0;
		m=0;
		d=0;			
			for(i=1;i<=n;i++)
				A[i]=rand();

			QuickSort(A, 1, n);
			m+=opQS;
			opQS =0;

			A[1]=rand();
			for(i=2;i<=n;i++)
				A[i]=A[i-1]+rand();

			aux=A[1];
			A[1]=A[n/2];
			A[n/2]=aux;

			QuickSort(A, 1, n);
			f+=opQS;
			opQS=0;

			for(i=1;i<=n;i++)
				B[i]=A[n-i+1];

			QuickSort(B, 1, n);
			d+=opQS;
			opQS=0;


		fprintf(pf, "%d, %d, %d, %d\n", n, f/5, m/5, d/5);
	}

	fclose(pf);

}

int main()
{
	Testare();
	//MediuStatistic();
	//FisierQS();
	return 0;
}
