#include <stdio.h>
#include <conio.h>
#include "Profiler.h"
Profiler profiler("Hash");

int probe(int data,int n){
return (data*data-2*data+20)%n;
}
int quadraticProbe(int i,int data,int n){
return (probe(data,n)+i*i*2+6*i)%n;
}



int insert(int data,int n,int* a){

int j=0;//counter
int i=quadraticProbe(j,data,n);

while(a[i]!=-1 && j!=n){
j++;
i=quadraticProbe(j,data,n);
}
if(j!=n) 
	{ 
		a[i]=data;
		return 1;
	}
	else  return 0;
}



int search(int data,int*a ,int n,int fill,int &effort){
int j=0;
int i=quadraticProbe(j,data,n); 
effort=effort+1;
while(a[i]!=data && j!=n && a[i]!=-1){	
j++;
effort=effort+1;
i=quadraticProbe(j,data,n); 
}


if(a[i]==data ) return 1; else return 0;

}

int main(void){
FILE *fp;
	int currenteffort=0;
	int maxeffort=0,effort=0;
	int aux ;
	
	int a[10007];
	int n=10007;

// DEMO

for(int i=0;i<n;i++)
	{
		a[i]=-1;
	}

printf("Search for 12345 returned %d \n ",search(12345,a,n,1,effort));

insert(12345,n,a);
for (int i=0;i<n;i++ ){
	if(a[i]!=-1) printf("Found an element at index %d : %d \n",i,a[i]);
}
printf("Search for 12345 returned %d \n ",search(12345,a,n,1,effort));
getch();
effort=0;


	
	for(int i=0;i<n;i++)
	{
		a[i]=-1;
	}

	fp = freopen ("log.csf", "w+", stdout);
	printf("Filling factor,AVG Effort found,Max Effort Found,AVF Effort not found,Max Effort not found \n");
	

	
//fill 0.8
 int b[10007];
 FillRandomArray(b,n);
 int fill=(n*8/10);
 for (int i=0;i<fill;i++){
 insert(b[i],n,a);
 }
 for(int i=0;i<1500;i++){
	currenteffort=0;
 search(b[5*i%fill],a,n,80,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }
printf("0.8,%d,%d ",effort/1500,maxeffort);
 //searching for not found elements
effort=0;
maxeffort=0;
for(int i=50001;i<51502;i++){
	currenteffort=0;
 search(i,a,n,80,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }	

printf("%d,%d \n",effort/1500,maxeffort);


//fill 0.85
 maxeffort=0;
 effort=0;

	for(int i=0;i<n;i++)
	{
		a[i]=-1;
	}

 FillRandomArray(b,n);
 fill=(n*85/100);
 for (int i=0;i<fill;i++){
 insert(b[i],n,a);
 }

 //searching for found elements
 for(int i=0;i<1500;i++){
	currenteffort=0;
 search(b[5*i%fill],a,n,85,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }
printf("0.85,%d,%d,",effort/1500,maxeffort);
 //searching for not found elements
effort=0;
maxeffort=0;
for(int i=50001;i<51502;i++){
	currenteffort=0;
 search(i,a,n,85,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }	

printf("%d,%d \n",effort/1500,maxeffort);



//fill 0.9
 maxeffort=0;
 effort=0;

	for(int i=0;i<n;i++)
	{
		a[i]=-1;
	}

 FillRandomArray(b,n);
fill=(n*9/10);
 for (int i=0;i<fill;i++){
 insert(b[i],n,a);
 }

 for(int i=0;i<1500;i++){
	currenteffort=0;
 search(b[6*i%fill],a,n,90,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }
printf("0.9,%d,%d,",effort/1500,maxeffort);
 //searching for not found elements
effort=0;
maxeffort=0;
for(int i=50001;i<51502;i++){
	currenteffort=0;
 search(i,a,n,90,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }	

printf("%d,%d \n",effort/1500,maxeffort);

//fill 0.95
 maxeffort=0;
 effort=0;

	for(int i=0;i<n;i++)
	{
		a[i]=-1;
	}

 FillRandomArray(b,n);
  fill=(n*95/100);
 for (int i=0;i<fill;i++){
 insert(b[i],n,a);
 }

 for(int i=0;i<1500;i++){
	currenteffort=0;
 search(b[6*i%fill],a,n,95,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }
printf("0.95,%d,%d,",effort/1500,maxeffort);
 //searching for not found elements
effort=0;
maxeffort=0;
for(int i=50001;i<51502;i++){
	currenteffort=0;
 search(i,a,n,95,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }	

printf("%d,%d \n",effort/1500,maxeffort);

//fill 0.99
 maxeffort=0;
 effort=0;

	for(int i=0;i<n;i++)
	{
		a[i]=-1;
	}

 FillRandomArray(b,n);
  fill=(n*99/100);
 for (int i=0;i<fill;i++){
 insert(b[i],n,a);
 }

 for(int i=0;i<1500;i++){
	currenteffort=0;
 search(b[6*i%fill],a,n,99,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }
printf("0.99,%d,%d,",effort/1500,maxeffort);
 //searching for not found elements
effort=0;
maxeffort=0;
for(int i=50001;i<51502;i++){
	currenteffort=0;
 search(i,a,n,99,currenteffort);
 	if ( currenteffort > maxeffort ) maxeffort=currenteffort;
	effort+=currenteffort;
 }	

printf("%d,%d \n",effort/1500,maxeffort);






















return 1;
	}