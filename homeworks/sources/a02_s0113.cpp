/*
	Name: ***ANONIM*** ***ANONIM***�n
	Group: ***GROUP_NUMBER***
	Problem specification:  Implement correctly and efficiently two methods for building a heap, 
							namely the bottom-up and the top-down strategies.
	Average case:
		-bottom-up: build_heap takes O(n) time
		-top-down: max_heap_insert takes O(logn), n such operations performed => O(nlogn). 
					The difference between bottom-up and top-down is not too obvious on the chart.
	Worst case:
		-bottom-up: build_heap takes O(n) time
		-top-down: max_heap_insert takes O(logn), n such operations performed => O(nlogn).
		
	Observation: bottom-up and top-down do not build always the same heap.
		Example: A[]={5, 6, 7}
		-bottom-up build-heap:
		    5			   7
		  /  \	  ->     /  \
		 6    7         6    5
		-top-down max_heap_insert:
		5			6			7
			->     /	->	   / \
			      5           5   6

	Memory: in situ sorting algs, so memory complexity O(n)
*/
#include<stdio.h>
#include<math.h>

int n, length, A[10001], aux[10001];
int i;
int T,I,M;
int bottomupcount, topdowncount;

void save_array()
{
	for(int i=1;i<=n;i++)
		aux[i]=A[i];
}

void restore_array()
{
	for(int i=1;i<=n;i++)
		A[i]=aux[i];
}

void max_heapify(int pos)
{
	int left=2*pos;
	int right=left+1;
	int largest;
	if(left <= n && A[left] > A[pos])
		largest=left;
	else largest=pos;
	bottomupcount++;
	if(right <= n && A[right] > A[largest])
		largest=right;
	bottomupcount++;
	if(largest!=pos)
	{
		int aux=A[pos];
		A[pos]=A[largest];
		A[largest]=aux;
		bottomupcount+=3;
		max_heapify(largest);
	}
}

void max_heap_insert()
{
	length++;
	int i=length;
	while(i>1 && A[i/2]<A[i])
	{
		int aux=A[i/2];
		A[i/2]=A[i];
		A[i]=aux;
		i=i/2;
		topdowncount+=4;
	}
	topdowncount++;
}

void bottom_up_build_heap()
{
	for(i=n/2;i>=1;i--)
		max_heapify(i);
}

void top_down_build_heap()
{
	length=1;
	for(i=2;i<=n;i++)
		max_heap_insert();
}

void pretty_print()
{
	int powtwo=1;
	int nr=n/2*2;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=nr/powtwo;j++)
			printf(" ");
		printf("%d", A[i]);
		if(i%(2*powtwo-1)==0)
		{
			printf("\n");
			powtwo*=2;
		}
	}
	printf("\n\n");

}

void pretty_print_rec(int node, int level)
{
	if(node<=n)
	{
		pretty_print_rec(2*node+1, level+1);
		for(int i=1;i<=3*level;i++)
			printf(" ");
		printf("%d\n", A[node]);
		pretty_print_rec(2*node, level+1);

	}
}

void test()
{
	A[1]=4; A[2]=1; A[3]=3; A[4]=2; A[5]=16; A[6]=9; A[7]=10; A[8]=14; A[9]=8; A[10]=7;
	n=10;
	save_array();
	bottom_up_build_heap();
	pretty_print_rec(1, 0);
	restore_array();
	top_down_build_heap();
	pretty_print_rec(1, 0);

}

int main()
{
	test();
	
	//average case
	freopen("heap_average.csv", "w", stdout);
	printf("n,Bottom-up,Top-down\n");
	freopen("average.txt", "r", stdin);
	scanf("%d", &T);
	for(I=1;I<=T;I++)
	{
		bottomupcount=0;
		topdowncount=0;

		for(M=1;M<=5;M++)
		{
			scanf("%d", &n);
			for(i=1;i<=n;i++)
				scanf("%d", &A[i]);
			save_array();
			bottom_up_build_heap();
			restore_array();
			top_down_build_heap();
		}
		printf("%d,%d,%d\n",n,bottomupcount/5, topdowncount/5);
	}

	//worst case
	freopen("heap_worst.csv", "w", stdout);
	printf("n,Bottom-up,Top-down\n");
	freopen("worst.txt", "r", stdin);
	scanf("%d", &T);
	for(I=1;I<=T;I++)
	{
		bottomupcount=0;
		topdowncount=0;
			scanf("%d", &n);
			for(i=1;i<=n;i++)
				scanf("%d", &A[i]);
			save_array();
			bottom_up_build_heap();
			restore_array();
			top_down_build_heap();
		printf("%d,%d,%d\n",n,bottomupcount, topdowncount);
	}

	return 0;
}