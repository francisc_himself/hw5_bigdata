
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>


typedef struct nod {
						int cheie;
						int dim;
						struct nod *st, *dr, *p;
					}NOD;
int cmp=0;
int atr=0;

void prety_print(NOD *radacina, int s)
{
	if(radacina!=NULL)
	{
		for(int j=0; j<s;j++)
			printf("\t");
		printf("%d : %d\n",(*radacina).cheie, (*radacina).dim);
		prety_print((*radacina).st, s+1);
		prety_print((*radacina).dr, s+1);
	}
}

void build_tree(NOD **radacina, NOD *parinte, int stg, int drp)
{	
	if(stg<=drp)
	{	int mijloc=(stg+drp)/2;
		*radacina=(nod *)malloc(sizeof(nod));
		(**radacina).cheie=mijloc;
		(**radacina).dim=drp-stg+1;
		(**radacina).p=parinte;
		build_tree(&(**radacina).st, *radacina, stg, mijloc-1);
		build_tree(&(**radacina).dr, *radacina, mijloc+1, drp);
	}
	else 
	{
		*radacina=NULL;
	}
}

NOD* OS_SELECT(NOD *x, int i)
{int r;
	cmp++;
	if(x->st!=NULL)
		r=x->st->dim+1;
	else r=1;

	cmp++;
	if(i<r)
	{OS_SELECT(x->st, i);}
	else if(i>r)
	{OS_SELECT(x->dr, i-r);}
	else  
		return x;
}
NOD* minim(NOD* x)
{
	cmp++;
	while((*x).st!=NULL)
	{
		cmp++;
		x=(*x).st;
	}
	return x;

}

NOD* succesor(NOD *x)
{
	cmp++;
	if((*x).dr!=NULL)
		return minim((*x).dr);
	NOD* y=(*x).p;
	cmp++;
	while(y!=NULL && x==(*y).dr)
	{
		x=y;
		y=(*x).p;
	}
	return y;
}
void OS_DELETE( NOD **radacina, NOD *z)
{
	NOD *x, *y;
	cmp=cmp+2;
	if((*z).st==NULL || (*z).dr==NULL)
	{
		y=z;
	}
	else y=succesor(z);
	//se actualizeaza dimensiunile
	NOD *aux =y;
	cmp++;
	while((*aux).p!=NULL)
	{
		cmp++;
		(*(*aux).p).dim--;
		atr++;
		aux=(*aux).p;
	}
	cmp++;
	if((*y).st!=NULL)
		x=(*y).st;
	else x=(*y).dr;

	if(x!= NULL)
	{
		(*x).p=(*y).p;
		atr++;
	}
	cmp++;
	if((*y).p==NULL)
	{
		*radacina=x;
		atr++;
	}
	else
	{
		cmp++;
		if(y==(*(*y).p).st)
		{
			(*(*y).p).st=x;
			atr++;
		}
		else
		{
			(*(*y).p).dr=x;
			atr++;
		}
	}
	if(y!=z)
	{
		(*z).cheie=(*y).cheie;
		atr++;
	}

	free(y);
}

void josephus(NOD **radacina, int n, int m)
{
	build_tree(radacina, NULL, 1, n);
	NOD *aux;
	int i=m;
	cmp++;
	while(*radacina !=NULL)
	{
		cmp++;
		aux=OS_SELECT(*radacina, i);
		printf("%d : %d\n",(*aux).cheie, (*aux).dim);
		OS_DELETE(radacina, aux);
		n--;
		if(n!=0)
			i=(i-2+m)%n+1;
	}
}

//----------DEMO-----------
void main()
{   NOD *radacina=NULL;
	build_tree(&radacina,NULL, 1, 7);
	prety_print(radacina, 1);	
	printf("-----------------------\n");
	josephus(&radacina,7,3);
				
}
/*
//--------EVALUARE----------
void main()
{
	cmp=0; atr=0;
	NOD *radacina=NULL;
	int j=1; 
	FILE *f;
	f=fopen("josephus.csv", "w");
	fprintf(f, "n,comparatii,atribuiri,atribuiri+comparatii\n");
	for(int n=100; n<=10000; n=n+100)
	{
		build_tree(&radacina, NULL, 1,n);
		josephus(&radacina,n, n/2);
		fprintf(f, "%d,%d,%d,%d\n", n, cmp, atr, atr+cmp);
		cmp=0; atr=0;
	}
}

*/