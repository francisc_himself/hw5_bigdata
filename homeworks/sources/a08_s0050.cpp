#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//O(V+N)
#define white 0
#define gray 1
#define black 2
#define V_MAX 10000
#define E_Max 60000

typedef struct{
int parent;
int distance;
int color;

}GRAPH_NODE;

typedef struct nod{
int key;
struct nod *next;
}NODE;

typedef struct nod2{
int key;
int rank;
nod2 *p;
}NOD;

NOD nodes[V_MAX];


NODE  *adj[V_MAX];
GRAPH_NODE graph[V_MAX];
int edges1[E_Max];
int edges2[E_Max];
int count;
void Make_Set(NOD *x,int i)
{	count++;
	x->rank=0;
	x->p=x;
	x->key=i;
}

NOD *Find_Set(NOD *x)
{
	if ((*x).p!=x) {(*x).p=Find_Set(x->p);
				count++;}
	return x->p;
}

void Link(NOD *x,NOD *y)
{
count++;
if ((*x).rank>y->rank)
		(*y).p=x;
	else{
		(*x).p=y;
		if ((*x).rank==(*y).rank)
			(*x).rank++;
	}	
}

void Union(NOD *x,NOD *y)
{
Link(Find_Set(x),Find_Set(y));
}

void conected_component(int nrMuchi,int nrN)
{
for (int i=0;i<nrN;i++){
	//nodes[i]=(NOD*)malloc(sizeof(NOD));
	
	Make_Set(&nodes[i],i);
	}
for(int i=0;i<nrMuchi-1;i++)
	{//if(i%500==0)
		//printf("*\n");
int a=edges1[i]/nrN;
int b=edges1[i]%nrN;
	if(Find_Set(&nodes[a])!=Find_Set(&nodes[b]))
	{
		Union(&nodes[a],&nodes[b]);
	}}
}

//void CC(int n, int m)
//{
//	
//for (int i=0;i<n;i++){
//	
//	Make_Set(&nodes[i],i);
//	}
//for(
//}

void generate(int n,int m)
{
memset(adj,0,n*sizeof(NODE*));
FillRandomArray(edges1,m,0,n*n-1,true);
for(int i=0;i<m;i++)
{
int a=edges1[i]/n;
int b=edges1[i]%n;
NODE *nod=(NODE*)malloc(sizeof(NODE));
nod->key=b;
nod->next=adj[a];
adj[a]=nod;
}
}

void afisareG(int n)
{
for(int i=0;i<n;i++)

	printf("%d -> %d \n",edges1[i]/n,edges1[i]%n);

}
void afisareGraph(int n)
{
for(int i=0;i<n;i++)
	printf("%d -> %d rank:%d\n",nodes[i].p->key,nodes[i].key,nodes[i].rank);
}
int main()
{	

		FillRandomArray(edges1,7,0,5*5-1,true,0);
		afisareG(5);
		conected_component(7,5);
		afisareGraph(5);	


	FILE *f =fopen("rez.csv","w");
	for (int numarMuchii=10000;numarMuchii+1 <60000;numarMuchii+=1000){
		FillRandomArray(edges1,numarMuchii,0,V_MAX*V_MAX-1,true,0);
	
			count=0;
		conected_component(numarMuchii,V_MAX);
	 	fprintf(f,"%d,%d\n",numarMuchii,count);
	}
	return 10;
}


