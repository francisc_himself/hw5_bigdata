#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

//timp de executie: O(|E|+|V|)

int cul[200], d[200], p[200], f[200];
int t;
int q=0;

struct nod
{
	int key;
	nod *urm;
};

nod *prim[200];

void creare(int n, int nr)
{
	int ch1,ch2;
	for (int i=0; i<=n; i++)
		prim[i]=NULL;
	for (int i=1; i<=nr; i++)
	{
		ch1=rand()%n+1;
		ch2=rand()%n+1;
		if (prim[ch1]==NULL)
		{
			prim[ch1]=(nod *)malloc(sizeof(nod *));
			prim[ch1]->key=ch2;
			prim[ch1]->urm=NULL;
		}
		else
		{
			nod *p;
			p=(nod *)malloc(sizeof(nod *));
			p->key=ch2;
			p->urm=NULL;
		}
	}
}

void print(int n)
{
	nod *p;
	printf("muchii: \n");
	for (int i=1; i<=n; i++)
	{
		printf("%d: ", i);
		p=prim[i];
		while (p!=NULL)
		{
			printf(" %d ", p->key);
			p=p->urm;
		}
		printf("\n");
	}
}

void dfs_visit(int u)
{
	cul[u]=1;
	d[u]=t;
	t++;
	int v;
	nod *x;
	x=prim[u];
	q++;
	while (x!=NULL)
	{
		v=x->key;
		x=x->urm;
		q++;
		if (cul[v]==0)
		{
			p[v]=u;
			q++;
			printf("%d, %d tree edge\n", u, v);
			printf("visited %d\n", v);
			dfs_visit(v);
		}
		else 
		{
			q++;
			if (cul[v]==1) printf("%d, %d back edge\n", u, v);
			else if (d[u]<f[v]) printf("%d, %d forward edge\n", u, v);
			       else printf("%d, %d transversal edge\n", u, v);
			printf("\n");
		}
	}
	cul[u]=2;
	f[u]=t;
	t++;
}

void dfs(int n)
{
	int u;
	t=0;
	for(int i=1; i<=n; i++)
	{
		f[i]=0;
		d[i]=0;
		p[i]=0;
		cul[i]=0;
	}
	for (u=1; u<=n; u++)
	{
		cul[u]=0;
		p[u]=0;
	}
	for (u=1; u<=n; u++)
	{
		q++;
		if (cul[u]==0)
		{
			printf("visited %d\n", u);
			dfs_visit(u);
		}
	}
}

void main()
{
	FILE *f;
	int n, nr;	//nr varf, nr muchii
	srand(time(NULL));
	
	n=5;
	nr=7;
	creare(n, nr);
	dfs(n);
	print(n);

	f=fopen("1.csv", "w");
	fprintf(f, "nr varf, nr muchii, nr\n");
	for (n=110; n<=200; n=n+10)
	{
		q=0;
		nr=9000;
		creare(n, nr);
		dfs(n);
		fprintf(f, "%d, %d, %d\n", n, nr, q);
	}
	fclose(f);

	f=fopen("2.csv", "w");
	fprintf(f, "nr muchii, nr varf, nr\n");
	for (nr=1000; nr<=5000; nr=nr+100)
	{
		q=0;
		n=100;
		creare(n, nr);
		dfs(n);
		fprintf(f, "%d, %d, %d\n", nr, n, q);
	}
	fclose(f);

	getch();
}