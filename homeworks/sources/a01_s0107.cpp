/*
Lorinczi Istvan
***GROUP_NUMBER***

bubble sort
	-stable sort
	-best case is n comparisons, 0 assignments (for sorted input)
	-worst case is n^2 comparisons and assignments	(for input is sorted in descendig order)
	from the start of the input till the end every two elements are compared
	if the first is greater then the second they are swaped,
	this is done while no swap was made in a cycle

selection sort
	-stable sort
	-best case n^2 comparisons 0 assignments (for sorted input)
	-worst case n^2 comparisons n assignments (for input is sorted in descendig order)
	for n (lenght of the input) times the minimum is selected from the remaining
	partion and is placed after the end of the sorted part

insertion sort
	-stable sort 
	-best case n comparisons and assignments (for sorted input)
	-worst case n^2 comparisons and assignments (for input  2,3,4...n,1 )
	every element is placed at it's place in a sorted array

*/
#include<stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>


int a[11];
int b[10002],n,c[10002];
int r[102][27];
FILE *f;



void bubble(int a[], int n,int cs )
{
	int comp=0;
	int assig=0;

	int swap=2;
	int i=0;
	int j=n;
	int aux=0;

	while (swap>1)
	{
		swap=0;
		for(i=1;i<j;i++)
		{if(a[i]>a[i+1]) {aux=a[i];a[i]=a[i+1];a[i+1]=aux; swap=i; assig+=3;} comp++;}
		j=swap;
	}
	if(cs!=-1)
	{
r[n/100][cs*3]+=assig;
r[n/100][cs*3+1]+=comp;
r[n/100][cs*3+2]+=assig+comp;}
}

void selection(int a[], int n,int cs )
{
	int comp=0;
	int assig=0;

	int i,j,pos,aux;


	for(j=1;j<n;j++)
	{
		pos=j;
		for(i=j+1;i<=n;i++)
		{
			if(a[i]<a[pos]) pos=i;
			comp++;
		}
		if(j!=pos)
		{
			aux=a[j]; a[j]=a[pos]; a[pos]=aux; assig+=3;
		}
	}
	if(cs!=-1)
	{
r[n/100][9+cs*3]+=assig;
r[n/100][9+cs*3+1]+=comp;
r[n/100][9+cs*3+2]+=assig+comp;}
}


void insertion(int a[], int n,int cs)
{

	int comp=0;
	int assig=0;

	int i,j;
	for(i=2;i<=n;i++)
	{
		a[0]=a[i]; assig++;
		j=i;
		while(a[j-1]>a[0])
		{
			a[j]=a[j-1]; assig++; comp++;
			j=j-1;
		}comp++;
		a[j]=a[0]; assig++;
	}if(cs!=-1)
	{
r[n/100][18+cs*3]+=assig;
r[n/100][18+cs*3+1]+=comp;
r[n/100][18+cs*3+2]+=assig+comp;}
}


int main()
{
	int ok=0;
	srand(time(NULL));

	a[1]=4; a[3]=2; a[5]=8; a[7]=1; a[9]=10;
	a[2]=5; a[4]=6; a[6]=9; a[8]=7; a[10]=3;
	bubble(a,10,-1);
	ok=1;
	for(int i=1;i<=10;i++)
	{
		if(a[i]!=i) ok=0;
	}
	printf("bubble %d ",ok);

	a[1]=4; a[3]=2; a[5]=8; a[7]=1; a[9]=10;
	a[2]=5; a[4]=6; a[6]=9; a[8]=7; a[10]=3;
	selection(a,10,-1);
	ok=1;
	for(int i=1;i<=10;i++)
	{
		if(a[i]!=i) ok=0;
	}
	printf("selection %d ",ok);

	a[1]=4; a[3]=2; a[5]=8; a[7]=1; a[9]=10;
	a[2]=5; a[4]=6; a[6]=9; a[8]=7; a[10]=3;
	insertion(a,10,-1);
	ok=1;
	for(int i=1;i<=10;i++)
	{
		if(a[i]!=i) ok=0;
	}
	printf("insertion %d ",ok);

	getch();


	for(int i =1; i<=10000;i++)
			b[i]=i;

	for(n=100; n<=10000;n+=100)
	{
		bubble(b,n,0);
		selection(b,n,0);
		insertion(b,n,0);
		printf("best %d ",n);
	}

	for(int j=1; j<=5;j++)
	{

		for(n=100; n<=10000;n+=100)
		{
			for(int i =1; i<=n;i++)
			b[i]=rand() % 10000;
			for(int i =1; i<=n;i++)
			c[i]=b[i];
			bubble(c,n,1);
			for(int i =1; i<=n;i++)
			c[i]=b[i];
			selection(c,n,1);
			for(int i =1; i<=n;i++)
			c[i]=b[i];
			insertion(c,n,1);
			printf("avg %d ",n);
		}
	}

	for(n=100; n<=10000;n+=100)
		{
			for(int i =1; i<=n;i++)
			b[i]=n-i;
			bubble(b,n,2);
            for(int i =1; i<n;i++)
			b[i]=i+1;
			b[n]=1;
			selection(b,n,2);
            for(int i =1; i<=n;i++)
			b[i]=n-i;
			insertion(b,n,2);
			printf("worst %d ",n);
		}

	for(n=1; n<=100;n++)
	{
		r[n][3]=(r[n][3]/5);
		r[n][4]=(r[n][4]/5);
		r[n][5]=(r[n][5]/5);
		r[n][12]=(r[n][12]/5);
		r[n][13]=(r[n][13]/5);
		r[n][14]=(r[n][14]/5);
		r[n][21]=(r[n][21]/5);
		r[n][22]=(r[n][22]/5);
		r[n][23]= (r[n][23]/5);

	}

	f=fopen("rez.csv","w");

	for(int i=1;i<=100;i++)
	{	fprintf(f,"%d,",i*100);
		for(int j=0;j<26;j++)
			fprintf(f,"%d,",r[i][j]);
		fprintf(f,"%d\n",r[i][26]);
	}

}
