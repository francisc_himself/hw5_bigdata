/*
***ANONIM*** ***ANONIM***-***ANONIM***, grupa ***GROUP_NUMBER***
COMPARARE A DIFERITI ALGORITMI DE SORTARE (BUBBLE SORT, INSERTION SORT, SELECTION SORT)

In ceea ce urmeaza am testat eficienta celor trei algoritmi, comparand performantele lor cu  ajutorul graficelor
generate de profiler.

Algoritmii au fost testati in trei conditii de rulare: cazul mediu statistic, cazul cel mai favorabil si cazul cel mai defavorabil.
Am ajuns la urmatoarele concluzii:

BUBBLE SORT:
Algoritmul are o permformanta buna, iar complexitatea lui este de O(n^2) in cazul mediu statistic (cu vector generat cu elemente aleatoare).
In cazul favorabil, cand vectorul este deja sortat crescator, atinge o complexitate buna, de O(n).
In cazul defavorabil complexitatea este de O(n^2).

INSERTION SORT:
Algoritmul are aceeasi complexitate de O(n^2) indiferent de forma vectorului sortat, din cauza faptului ca vectorul este parcurs
in totalitate la fiecare iteratie a ciclului exterior.

SELECTION SORT:
Algoritmul are complexitatea O(n^2) pentru cazul mediu statistic.
Desi are o performanta usor mai buna pentru cazul favorabil (cand vectorul este sortat crescator - indicele minimului nu trebuie schimbat
la fiecare iteratie) complexitatea ramane de O(n^2).
Pentru cazul defavorabil (cand algoritmul este sortat decrescator - in ordine inversa) pot aparea pierderi usoare de performanta datorita
faptului ca indicele minimului trebuie schimbat la fiecare iteratie a ciclului din interior, complexitatea ramane insa de O(n^2).


*/




#include<stdio.h>
#include<time.h>
#include <stdlib.h> 
#include<string.h>
#include "Profiler.h"
#define MAX 10000
int v[MAX];
char sc[]="selection_comp";
char sa[]="selection_assign";
char ic[]="insertion_comp";
char ia[]="insertion_assign";
char bc[]="bubble_comp";
char ba[]="bubble_assign";
Profiler p("lab2");

void is(int &a,int &b)
{
int x;
x=a;
a=b;
b=x;
}



void genv_avg(int v[],int n)
{
int i;

for(i=0;i<n;i++)
{
	v[i]=rand()%1000;
}
}

void genv_asc(int v[],int n)
{
int i,temp;

v[0]=rand()%1000;

for(i=1;i<n;i++)
{
	temp=rand()%100;
	v[i]=v[i-1]+temp;
}
}


void genv_desc(int v[],int n)
{
int i,temp;

v[0]=rand()%1000;

for(i=1;i<n;i++)
{
	temp=rand()%100;
	v[i]=v[i-1]-temp;
}
}

void afisare_vector(int v[],int n)
{
int i;
for(i=0;i<n;i++)
{
	printf("%d ",v[i]);
}
}


void countFunc(char s[],int mod,int n,int x)
{
	switch(mod)
	{
		case 0:
		strcat(s,"avg_");
		break;
		case 1:
		strcat(s,"best_");
		break;
		case 2:
		strcat(s,"worst_");
		break;
	
	}
	
	p.countOperation(s,n,x);

}

void bubble_sort(int v[],int n,int mod)
{
int i=1,j;
bool sortat=false;

while(!sortat)
{
	sortat=true;
	for(j=0;j<=n-i-1;j++)
	{
		countFunc(bc,mod,n,1);
		
		if(v[j]>v[j+1])
		{
			countFunc(ba,mod,n,4);
			is(v[j],v[j+1]);
			sortat=false;
		}
	}
	i++;
	countFunc(ba,mod,n,2);
}

}

void insertion_sort(int v[],int n,int mod)
{
int i,j,k,x;

for(i=1;i<n;i++)
{
		countFunc(ia,mod,n,2);
		x=v[i];
		j=0;
		
		countFunc(ic,mod,n,1);
		while(v[j]<x)
		{
			countFunc(ic,mod,n,1);
			countFunc(ia,mod,n,1);
			j++;
		}
		
		for(k=i;k>=j+1;k--)
		{
			countFunc(ia,mod,n,1);
			v[k]=v[k-1];
		}
		countFunc(ia,mod,n,1);
		v[j]=x;

}
}


void selection_sort(int v[],int n,int mod)
{
int i,j,iMin;

for(i=0;i<n-2;i++)
{
	countFunc(sa,mod,n,1);
	iMin=i;
	for(j=i+1;j<n;j++)
	{
		countFunc(sc,mod,n,1);
		if(v[j]<v[iMin])
		{
			countFunc(sa,mod,n,1);
			iMin=j;
		}

	}
	countFunc(sa,mod,n,3);
	is(v[iMin],v[i]);

}

}

int main()
{
int n;
srand(time(NULL));




for(int i=0;i<5;i++)
{
	for(n=100;n<1000;n += 100)
	{
		
		//generare de vector cu elemente aleatoare
		//sortare cu cei trei algoritmi
		genv_avg(v,n);
		bubble_sort(v,n,0);
		genv_avg(v,n);
		insertion_sort(v,n,0);
		genv_avg(v,n);
		selection_sort(v,n,0);
		
		//generare vector cu elemente pentru cazul favorabil
		//sortari
		genv_asc(v,n);
		bubble_sort(v,n,1);
		genv_asc(v,n);
		insertion_sort(v,n,1);
		genv_asc(v,n);
		selection_sort(v,n,1);
		
		
		//generare vector pentru cazul defavorabil
		//sortari
		genv_desc(v,n);
		bubble_sort(v,n,2);
		genv_desc(v,n);
		insertion_sort(v,n,2);
		genv_desc(v,n);
		selection_sort(v,n,2);
	}

}

p.createGroup("avg_comp","avg_bubble_comp","avg_insertion_comp","avg_selection_comp");
p.createGroup("avg_assign","avg_bubble_assign","avg_insertion_assign","avg_selection_assign");
p.addSeries("avg_bubble_comp_assign", "avg_bubble_assign","avg_bubble_comp");
p.addSeries("avg_insertion_comp_assign", "avg_insertion_assign","avg_insertion_comp");
p.addSeries("avg_selection_comp_assign", "avg_selection_assign","avg_selection_comp");
p.createGroup("avg_assign_comp","avg_bubble_comp_assign","avg_insertion_comp_assign","avg_selection_comp_assign");


p.createGroup("best_comp","best_bubble_comp","best_insertion_comp","best_selection_comp");
p.createGroup("best_assign","best_bubble_assign","best_insertion_assign","best_selection_assign");
p.addSeries("best_bubble_comp_assign", "best_bubble_assign","best_bubble_comp");
p.addSeries("best_insertion_comp_assign", "best_insertion_assign","best_insertion_comp");
p.addSeries("best_selection_comp_assign", "best_selection_assign","best_selection_comp");
p.createGroup("best_assign_comp","best_bubble_comp_assign","best_insertion_comp_assign","best_selection_comp_assign");



p.createGroup("worst_comp","worst_bubble_comp","worst_insertion_comp","worst_selection_comp");
p.createGroup("worst_assign","worst_bubble_assign","worst_insertion_assign","worst_selection_assign");
p.addSeries("worst_bubble_comp_assign", "worst_bubble_assign","worst_bubble_comp");
p.addSeries("worst_insertion_comp_assign", "worst_insertion_assign","worst_insertion_comp");
p.addSeries("worst_selection_comp_assign", "worst_selection_assign","worst_selection_comp");
p.createGroup("worst_assign_comp","worst_bubble_comp_assign","worst_insertion_comp_assign","worst_selection_comp_assign");

p.showReport();
}