/****ANONIM*** ***ANONIM*** - ***ANONIM***
Grupa ***GROUP_NUMBER***
Assignment 8 - Disjoint Sets*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

typedef struct muuchie{
	int x, y; 
}
MUCHIE;

typedef struct nod{
	int cheie, rang;
	struct nod* tata;
} NOD;

MUCHIE muchie[60001];
NOD *V[10001];   //folosit la determinarea componentelor conexe
int nrOp;

/*Creaza o multime cu un element */
NOD *makeSet(int c)  { //O(1)
	NOD *p;
	p = (NOD*)malloc(sizeof(NOD));
	p->cheie = c;
	p->tata = p;
	p->rang = 0;
	//nrOp+=3;
	return p;
}

/*am folosit euristica COMPRIMAREA DRUMULUI care face ca fiecare nod de pe drumul
de cautare sa pointeze direct spre radacina */
NOD *findSet(NOD *p){  //O(1)
	//nrOp++;
	if(p != p->tata){
		p->tata = findSet(p->tata);
		nrOp++; //ok
	}
	return p->tata;
}

/*am folosit euristica REUNIUNE DUPA RANG, adica radacina 
cu rangul mai mic va indica cu radacina spre rangul mai mare*/
void link(NOD *x, NOD *y)  //O(ma(m,n)) in cel mai rau caz
{
	//op++;
	if(x->rang > y->rang){
		y->tata = x;
		//op++;
	}
	else{
		x->tata = y;
		//op++;
		//op++;
		if(x->rang == y->rang){
			//op++;
			y->rang = y->rang + 1;
		}
	}
	
}

/*Are ca parametri pointeri spre doua radacini*/
void UNION(NOD *x, NOD *y){
	link(findSet(x),findSet(y));
}

void alocaMem(){
	int j;
	for(j = 1; j <= 60001; j++)
		muchie[j]=*(MUCHIE*)malloc(sizeof(MUCHIE));
}

//vedem daca muchia a mai fost generata
bool nuExista(int u, int v, int nrMuchii)  {
	int j;
	for(j = 1; j < nrMuchii; j++){
		if(((muchie[j].x == u) && (muchie[j].y == v)) || ((muchie[j].y == u) && (muchie[j].x == v))){
			return false;
		}
	}
	return true;
}

//construim un numar de muchii nrMuchii intre varfurile nrVarfuri
void construieste(int nrVarfuri, int nrMuchii) {
	int j, u, v, m = 1; //m = numar de muchii generate deja
	//int nre=1;   //numar curent de muchii generate
	while(m <= nrMuchii)
		for(j = 1; j <= nrVarfuri; j++){
			u = (rand() % (nrVarfuri)) + 1;
			v = (rand() % (nrVarfuri)) + 1;
			if((u != v) && (nuExista(u,v,nrMuchii))){
				muchie[m].x = u;
				muchie[m].y = v;
				m++;
			}
		}
}

void connectedComponents(int nrVarfuri, int nrMuchii){
	int j;
	for(j = 1; j <= nrVarfuri; j++){
		nrOp++;//ok
		V[j] = makeSet(j);
	}
	for(j = 1; j <= nrMuchii; j++){
		nrOp = nrOp + 2;//op=op+2
		if(findSet(V[muchie[j].x]) != findSet(V[muchie[j].y])){
			UNION(V[muchie[j].x], V[muchie[j].y]);
			nrOp++;
		}
	}
}

void afisMuchii(int nrMuchii)
{
	int j;
	for(j = 1; j <= nrMuchii; j++){
		printf("[%d, %d]\n", muchie[j].x, muchie[j].y);
	}
}

void afisConex(int nrMuchii, int nrVarfuri)
{
	int i, j;
	for(i = 1; i <= nrVarfuri; i++)
		printf("nodul %d are parintele %d si rangul %d\n", V[i]->cheie, V[i]->tata->cheie, V[i]->rang);

	for(i = 1; i <= nrVarfuri; i++)
	{
		printf("Multimea %d: ", i);
		for(j = 1; j <= nrVarfuri; j++)
		{
			if(findSet(V[j])->cheie == i) //daca cheia setului e egala cu multimea
				printf("%d ", V[j]->cheie);//afisez cheia
			
		}
		printf("\n");
	}
}

void main()
{

	char choose;
	int nrVarfuri = 6;
	int nrMuchii = 3;
	srand(time(NULL));

	printf("D - Demonstratie    R - Rezolva\n");
	scanf("%c", &choose);
	printf("\n");
	switch(choose){
	case 'D':
		construieste(nrVarfuri, nrMuchii);
		afisMuchii(nrMuchii);
		connectedComponents(nrVarfuri,nrMuchii);
		afisConex(nrMuchii, nrVarfuri);
		break;
	case 'R':
		FILE *df = fopen("assign8.txt","w");
		int nrMuchi = 0, nrVarfur = 10000;
		fprintf(df,"muchii op\n");
		for(nrMuchi = 10000; nrMuchi <= 60000; nrMuchi += 1000){
			printf("%d\n", nrMuchi);
			nrOp = 0;
			alocaMem();
			construieste(nrVarfur, nrMuchi);
			connectedComponents(nrVarfur, nrMuchi);
			fprintf(df,"%d %d\n", nrMuchi, nrOp);
		}
		fclose(df);
		printf("The End!%c",7);
		getch();
		break;
	}
	printf("Pentru terminarea programului apasati o tasta.");
	getch();
}