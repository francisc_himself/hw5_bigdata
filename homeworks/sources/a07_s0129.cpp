/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
   Assignment No.7: Multi-way Trees Transformations between different representations
*/



#include<conio.h>
#include<stdio.h>
#include<stdlib.h>


typedef struct multi_tree
{
	int key;
	int nr_child; //dim						//number of children 
	int parent;								//parent
	struct multi_tree *child[100];			//an array of children
} MULTI_TREE;								//a MULTI_TREE results from the P[100] (parent vector) after Transform1() is called

typedef struct binary_tree
{
	int key;
	struct binary_tree *stg_child, *dr_brother;		//legatura catre fiul stang respectiv fratele drept

}BINARY_TREE;		//MULTI TREE represented as a binary tree after Transform2() is called

MULTI_TREE *root1;
MULTI_TREE *a[100];
BINARY_TREE *root2;
BINARY_TREE *b[100];
int P[100], n;  //parent vector and its dim

void Transform1()
{
   int i,aux;
   //init the multi tree array a
   for(i=0;i<n;i++)
	{
	   a[i]=(MULTI_TREE*)malloc(sizeof(MULTI_TREE)); 
	   a[i]->key=i;
	   a[i]->nr_child=0;
	   a[i]->child[1]=NULL;
   }
   for(i=0;i<n;i++)
   {
	   if(P[i]==-1)
	      root1=a[i];
	   else
	   {
			   a[P[i]]->nr_child++;
			   aux=a[P[i]]->nr_child;
			   a[P[i]]->child[aux]=a[i];
	   }
   }
}


 void Transform2(MULTI_TREE *p)
{
	int i;
	
	if(p!=NULL)
	{
		for(i=1;i<=p->nr_child;i++)
		{
			if(i==1)
			{	int j=p->key;
				//printf("suntem la nodul cu valoarea: %d\n", j);
				//printf("adougam ca prim copil pe %d:\n", p->child[1]->key);
				b[j]->stg_child=b[p->child[1]->key];
			}
			else
			{	//printf("adougam ca frate pe %d:\n", p->child[1]->key);
				b[p->child[i-1]->key]->dr_brother=b[p->child[i]->key];
			}
			Transform2(p->child[i]);
		}
	}
	
}

void printfMultiTree1(MULTI_TREE *p)
{
	int i;
	if(p!=NULL)
	 {
		  if(p->nr_child==0)
			    printf("\n%d Does not have any children",p->key);
		  else
		  {
				printf("\nChildren of %d are: ",p->key);
				for(i=1;i<=p->nr_child;i++)
					 printf(" %d ",p->child[i]->key);
				for(i=1;i<=p->nr_child;i++)
					 printfMultiTree1(p->child[i]);
		  }
	 }
}

void printfMultiTree2(MULTI_TREE *p)
{
int i;
	if(p!=NULL)
	 {
		  if(p->nr_child==0)
			  printf("\n%d Does not have any children",p->key);
		  else
		  {
				printf("\nChildren of %d are: ",p->key);
				for(i=1;i<=p->nr_child;i++)
				{	 
					if(i==1)
								printf("%d is the first child of %d",p->child[i]->key, p->key);
					else	if (i==2)
								printf(" %d is the %dnd child of %d",p->child[i]->key, i, p->key);
							else 
								printf(" %d is the %dth child of %d",p->child[i]->key, i, p->key);
				printfMultiTree2(p->child[i]);
				}
		  }
	 }
}

void printBinaryTree(int ind,int h)
{
   int i;
   printf("\n");
   for(i=0;i<=3*h;i++)
	   printf(" ");
   printf("%d",ind);

   if(b[ind]->stg_child!=NULL)
	     printBinaryTree(b[ind]->stg_child->key,h+1);
     if(b[ind]->dr_brother!=NULL)
	     printBinaryTree(b[ind]->dr_brother->key,h);
}

void main()
{
	int i;
	/*scanf("%d", &n); 
	for(i=0;i<n;i++)
		scanf("%d",&P[i]);*/
	///example
	n=9;
	P[0]=1;
	P[1]=6;
	P[2]=4;
	P[3]=1;
	P[4]=6;
	P[5]=6;
	P[6]=-1;
	P[7]=4;
	P[8]=1;
	//T1();
	Transform1();
	printf("\nAfter Transform1: \n");
	printfMultiTree1(root1);
	printf("\n");
	//printfMultiTree2(root1);
	//printf("\n");

	for(i=0;i<n;i++)
	{
		b[i]=(BINARY_TREE*)malloc(sizeof(BINARY_TREE));  
		b[i]->key=i;
		b[i]->stg_child=b[i]->dr_brother=NULL;
	}
	Transform2(root1);

	printf("\nAfter Transform2: \n");
	printf("%d", root1->key);
	printBinaryTree(b[1]->key,0);
	getch();

}