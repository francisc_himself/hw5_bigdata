﻿/*
***ANONIM*** ***ANONIM***-***ANONIM***
grupa ***GROUP_NUMBER***
Observatii legate de algoritmii de sortare implementati:

BubbleSort-caz defavorabil O(n2),caz favorabil O(n) ,caz mediu O(n2)

SelectionSort
-  comparari-pentru un sir cu n elemente, numarul de comparari este acelasi, indiferent de valoarea celor n numere.El este n(n-1)/2
Putem concluziona ca algoritmul SelectSort are un ordin de complexitate patratic indiferent de structura datelor.O(n2)
-din punct de vedere al numarului de atribuiri

 Cazul cel mai favorabil apare atunci cand sirul este deja sortat si nu se executa nici o atribuie.
 Cazul cel mai defavorabil apare atunci cand sirul este sortat invers si se executa cele mai multe atribuiri.

InsertionSort
worst   case О(n2) comparisons, swaps
best    case O(n) comparisons, O(1) swaps
average case О(n2) comparisons, swaps

*/

#include<stdlib.h>
#include<conio.h>
#include<stdio.h>
#include<time.h>
#define MAX 20000
int v[MAX],n,nrel;
void citire(){
printf("\n Da nr.elemente: \n");
	scanf("%d",&n);
	printf("Da un vector:");
	for(int i=1;i<=n;i++)
		scanf("%d",&v[i]);
}

int comparatii,atribuiri, defavorabil[MAX],favorabil[MAX];
FILE *f=fopen("RezultateSortari.xls","w+");

void afisare(){
	for(int i=1;i<=n;i++)
	printf("%d ",v[i]);
}

void generare(int n){
	srand(time(NULL));
	printf("\n Pt n=%d \n",n);
	//fprintf(f,"\n");
	fprintf(f," %d ",n);
	for(int i=1;i<=n;i++)
	{
		v[i]=rand();//nesortat;
		defavorabil[i]=n-i+1;//invers
	    favorabil[i]=i;//sortat
	}
}

void BubbleSort(int v[10000])
{
int sortat=0,i, aux=0,j;
comparatii=0;
atribuiri=0;

while(!sortat){
	sortat=1;
	for( i=1;i<n;i++){
		if(v[i]>v[i+1])
		{
		comparatii++;
		aux=v[i];
		v[i]=v[i+1];
		v[i+1]=aux;
		atribuiri++;
		sortat=0;
		}
		comparatii++;
	}
fprintf(f,"\t%d\t", comparatii);
fprintf(f,"\t%d\t", atribuiri);
fprintf(f,"\t%d\t", atribuiri+comparatii);
}
}
/*
Subvectorul destinaţie este împărţit în doi subvectori, se examinează
relaţia de ordine dintre elementul de la mijlocul subvectorului şi elementul
a[j] şi se stabileşte dacă elementul a[i] va fi inserat în prima jumătate sau
în a doua jumătate. Operaţia de divizare a subvectorului continuă până se
găseşte poziţia în care urmează să fie inserat a[i].

*/
void InsertionSort(int v[10000])
{
	int i,cheie;
	comparatii=0;
	atribuiri=0;
	for(int j=2;j<=n;j++)
	{   
		comparatii++;
		cheie=v[j];//salvez primul element din sursa in cheiee,pt a crea spatiul d inserare
		
		i=j-1;//
		atribuiri++;
		// tot mutam spatiul de inserare in jos pana cand valoarea verificata > decat ce e sub spatiul ala 
		while(i && v[i]>cheie)
		{
		comparatii++;
		//valoarea de inserat nu e unde ar trebui sa fie,deci se shifteaza 
		v[i+1]=v[i];
		i--;
		v[i+1]=cheie;//se pune valoare in spatiul destinat inserarii
		atribuiri++;
		}
	}
fprintf(f,"\t%d\t", comparatii);
fprintf(f,"\t%d\t", atribuiri);
fprintf(f,"\t%d\t", atribuiri+comparatii);
}
//Principiu: reprezinta o varianta a sortarii prin selectie,
//in care determinarea elementului cu cheia minima dintr-o portiune de tablou se reduce la determinarea pozitiei acestuia. 
//Infelul acesta se poate renunta la asignarea x:=a[j] care apare in ciclul "for" controlat de j.
void SelectionSort(int v[])//pt un sir cu n nr,compararile n(n-1)/2
{
	comparatii=0;	//caz favorabil-sir sortat ,defavorabil sirul e sortat invers->cele mai multe atribuiri mediu-nesortat
	atribuiri=0;

	int i,j,min,x;
	for(i=1;i<=n-1;i++)
	{
	min=i;
	atribuiri++;
	 for(j=i+1;j<=n;j++)
	   {
		comparatii++;
		if(v[j]<v[min])	
			min=j;
	    }
			x=v[i];
			v[i]=v[min];
			v[min]=x;
			atribuiri++;
	}
fprintf(f,"\t%d\t", comparatii);
fprintf(f,"\t%d\t", atribuiri);
fprintf(f,"\t%d\t", atribuiri+comparatii);
}


void SelectionSortToateCazurile()
{
	SelectionSort(favorabil);
	SelectionSort(v);
	SelectionSort(defavorabil);
}

void BubbleSortToateCazurile()
{
	BubbleSort(favorabil);
	BubbleSort(v);
	BubbleSort(defavorabil);	
}

void InsertionSortToateCazurile()
{
	InsertionSort(favorabil);
	InsertionSort(v);
	InsertionSort(defavorabil);	
}

void generareDate(){
	fprintf(f,"\n\n Bubble Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");	
	for(n=100;n<10000;n+=100)
	{
		generare(n);
		BubbleSortToateCazurile();
		fprintf(f,"\n");
		afisare();
	}
	fprintf(f,"\n\n Selection Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");
	for(n=100;n<10000;n+=100)
	{
		generare(n);
		SelectionSortToateCazurile();
		fprintf(f,"\n");
		afisare();
	}
	fprintf(f,"\n\n Insertion Sort\t\n");
	fprintf(f,"\tBest\t\t\t\t\t\tAverage\t\t\t\t\t\tWorst\t\t\n");
	fprintf(f,"\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\t\tComparatii\t\tAtribuiri\t\tComp+Atr\n");
	for(n=100;n<10000;n+=100)
	{
		generare(n);
		InsertionSortToateCazurile();
		fprintf(f,"\n");
		afisare();
	}	
}

int main()
{
	 citire();
	BubbleSort(v);
	printf("\n Vectorul sortat prin metoda bulelor: \n");
	afisare();
	//--------------------------------------
	printf("\n Vectorul sortat prin Insertion Sort: \n");
	InsertionSort(v);
	afisare();
	//--------------------------------------
	printf("\n Vectorul sortat prin Selection Sort: \n");
	SelectionSort(v);
	afisare();
	//generare(10);
	//BubbleSort();
	printf("\n Date in curs de generare.Asteptati! \n");
	//generareDate();
	//BubbleSort(v);
	//afisare();
	printf("\n Datele au fost generate in 'RezultateSortari.xls'\n");
	printf("\n Apasati o tasta pentru a termina rularea programului");
	getch();
	return 0;
}

