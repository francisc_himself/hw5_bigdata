/*
La cazul cel mai favorabil sortarea cu selectie si insertie sunt foarte apropiate, desii selectia face mai multe atribuiri
Intre timp bubble sort ramane constanta sub cele 2 sortari, deci e mai eficient

La cazul cel mai defavorabil bubble sort face cele mai multe atribuiri si comparetii, 
iar insertia fiind linear la comparatii, in total e mai eficient decat selectia

La valori aleatorii bubble sort iar face cele mai multe atribuiri si comparatii
La atribuiri selectia face cele mai putine operatii, iar la comparatii insertia, astfel in total cele 2 sortari sunt foarte asemanatoare

In concluzie insertia pare mai eficient decat celelalte doua sortari in fiecare caz.
desi la valori aleatorii selectia e aproape la fel de eficient ca insertia
In cazurile favorabile, sau cand suntem foarte aproape de cazuri favorabile e recomandat sa folosim bubble sort, in celelalte cazuri, insertia
*/

#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "math.h"
#include "time.h"

int a[10000],temp[10000],n=1000;
int atr_i, atr_s, atr_b;
int cmp_i, cmp_s, cmp_b;
int Tatr_i, Tatr_s, Tatr_b;
int Tcmp_i, Tcmp_s, Tcmp_b;
FILE *f;

void insertie(int a[], int n)//merge
{
	int j,x,k,i;
	atr_i=0;
	cmp_i=0;
	for (i=2; i<=n; i++)
	{
		x=a[i];
		atr_i++;
		j=1;
		while (a[j]<x)
		{
			cmp_i++;
			j=j+1;
		}
		cmp_i++; //pentru ultima verificare, cand nu intra in while
		for (k=i; k>=j+1; k--)
		{
			a[k]=a[k-1];
			atr_i++;
		}
		a[j]=x;
		atr_i++;
	}
}

void selectie(int a[], int n)
{
	int min, imin;
	cmp_s=0;
	atr_s=0;
	for(int i=1; i<=n; i++)
	{
		min=a[i];
		atr_s++;
		imin=i;
		for (int j=i+1; j<=n; j++)
		{
			if (min>a[j])
			{
				min=a[j];
				atr_s++;
				imin=j;
			}
			cmp_s++;	
		}
		int aux=a[i];
		a[i]=a[imin];
		a[imin]=aux;
		atr_s=atr_s+3;
	}
}




void interschimbare(int a[], int n)
{
	bool sortat=false;
	int j,aux;
	int i=2;
	atr_b=0;
	cmp_b=0;
	while (!sortat)
	{
		sortat=true;
		for(j=1; j<=n-1; j++)
		{
			if (a[j]>a[j+1]) 
			{
				aux = a[j];
				a[j] = a[j+1];
				a[j+1] = aux;
				atr_b=atr_b+3;
				sortat=false;
			}
			cmp_b++;
		}
	}
}

void afis(int a[], int n)
{
	int i;
	for(i=1;i<=n;i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n");
}

void copy(int b[], int a[], int n)
{
	int i;
	for(i=1; i<=n; i++)
		a[i]=b[i];
}

void generareAVG(int a[], int n)
{
	int i;
	for(i=1; i<=n; i++)
		a[i]=rand() % 10000;
}

void generareBEST (int a[], int n)
{
    for(int i=1;i<=n;i++)
	     a[i]=i;
}

void generareWORST(int a[], int n)
{
    for(int i=1;i<=n;i++)
	     a[i]=n-i;
}

void generareWORST_SEL (int a[], int n)// genereaza ex: 2 4 6 5 3 1
{
	int j, i;
    for(i=2, j=1;j<=n/2;i=i+2, j++)
		 a[j]=i;
	     
	for(i=n/2+2; j<=n; j++, i=i-2)
		a[j]=i;
}

void main()
{
	printf("AVG: \n");
	srand(time(NULL));
	f=fopen("avg.txt","w");
	fprintf(f,"AVG\n");
	fprintf(f,"n, Asel, Csel,Tsel, Ains, Cins, Tins, Abub, Cbub, Tbub\n");
	for(n=100; n<=10000; n=n+100)
	{
		Tatr_i= Tatr_s= Tatr_b=0;
		Tcmp_i= Tcmp_s= Tcmp_b=0;
		for(int i=1;i<=5;i++)
		{
			generareAVG(temp,n);
			//printf("\nsirul originar: \n");
			//afis(temp,n);
		
			//printf("sirul dupa buble: \n");
			copy(temp,a,n);
			interschimbare(a,n);
			Tatr_i+=atr_i;
			Tcmp_i+=cmp_i;
			//afis(a,n);
			
	
			//printf("sirul dupa selectie: \n");
			copy(temp,a,n);
			selectie(a,n);
			Tatr_s+=atr_s;
			Tcmp_s+=cmp_s;
			//afis(a,n);
			
	
			//printf("sirul dupa insertie: \n");
			copy(temp,a,n);
			insertie(a,n);
			Tatr_b+=atr_b;
			Tcmp_b+=cmp_b;
			//afis(a,n);
			
		}	
		printf("\nbuble: atr: %d cmp: %d ",Tatr_b/5,Tcmp_b/5);
		printf("\nselec: atr: %d cmp: %d ",Tatr_s/5,Tcmp_s/5);
		printf("\ninser: atr: %d cmp: %d ",Tatr_i/5,Tcmp_i/5);
		fprintf(f,"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n", n,Tatr_s/5,Tcmp_s/5,Tatr_s/5+Tcmp_s/5,Tatr_i/5,Tcmp_i/5,Tatr_i/5+Tcmp_i/5,Tatr_b/5,Tcmp_b/5,Tatr_b/5+Tcmp_b/5);
		printf("\n");
	}

	printf("\nBEST: \n");
	f=fopen("best.txt","w");
	fprintf(f,"BEST\n");
	fprintf(f,"n, Asel, Csel,Tsel, Ains, Cins, Tins, Abub, Cbub, Tbub\n");
	for(n=100; n<=10000; n=n+100)
	{
		generareBEST(temp,n);
		//printf("\nsirul originar: \n");
		//afis(temp,n);
		
		//printf("sirul dupa buble: \n");
		copy(temp,a,n);
		interschimbare(a,n);
		//afis(a,n);
		printf("\nbuble: atr: %d cmp: %d ",atr_b,cmp_b);
	
		//printf("sirul dupa selectie: \n");
		copy(temp,a,n);
		selectie(a,n);
		//afis(a,n);
		printf("\nselec: atr: %d cmp: %d ",atr_s,cmp_s);

		//printf("sirul dupa insertie: \n");
		copy(temp,a,n);
		insertie(a,n);
		//afis(a,n);
		printf("\ninser: atr: %d cmp: %d ",atr_i,cmp_i);

		printf("\n");
		fprintf(f,"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n", n,atr_s,cmp_s,atr_s+cmp_s,atr_i,cmp_i,atr_i+cmp_i,atr_b,cmp_b,atr_b+cmp_b);

	}

	printf("\nWORST: \n");
	f=fopen("worst.txt","w");
	fprintf(f,"WORST\n");
	fprintf(f,"n, Asel, Csel,Tsel, Ains, Cins, Tins, Abub, Cbub, Tbub\n");
	for(n=100; n<=10000; n=n+100)
	{
		generareWORST(temp,n);
		//printf("\nsirul originar: \n");
		//afis(temp,n);
		
		//printf("sirul dupa buble: \n");
		copy(temp,a,n);
		interschimbare(a,n);
		//afis(a,n);
		printf("\nbuble: atr: %d cmp: %d ",atr_b,cmp_b);
	
		//printf("sirul dupa selectie: \n");
		copy(temp,a,n);
		//generareWORST_SEL(a,n); // !!!!!! pentru functia mea de selectie cazul defavorabil este sirul invers
		selectie(a,n);
		//afis(a,n);
		printf("\nselec: atr: %d cmp: %d ",atr_s,cmp_s);

		//printf("sirul dupa insertie: \n");
		copy(temp,a,n);
		insertie(a,n);
		//afis(a,n);
		printf("\ninser: atr: %d cmp: %d ",atr_i,cmp_i);

		printf("\n");
		fprintf(f,"%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n", n,atr_s,cmp_s,atr_s+cmp_s,atr_i,cmp_i,atr_i+cmp_i,atr_b,cmp_b,atr_b+cmp_b);
	}

	printf("Am terminat ^^");
	getch();
}