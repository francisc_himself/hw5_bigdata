/* 
Nume: ***ANONIM***
Prenume: ***ANONIM*** MIHAI
Grupa: ***GROUP_NUMBER***


Cerinta: 

	You are required to implement correctly and efficiently 3 direct sorting methods (Bubble Sort, 
	Insertion Sort and Selection Sort).


Input data: - secventa de numere  <a1,a2,...,an>
Output data:  - permutarea sirului de intrare a.i. <a1'<=a2'<=...<=an' >

Conclusions: The study and the analysis of three different direct sorting methods was accomplished.  I have used vectors in the
elements operations. 

1st CASE: BEST CASE for Bubble Sort, Insertion Sort, Selection Sort.
				Comparisons: SelectionSort is the slowest, BubbleSort & InsertionSort are best and equal, order of complexity: linear.
				Assignments: BubbleSort is the fastest in assignments, in the middle we have InsertionSort, and the worst is Selectionsort.
				Order of complexity: linear, differs by scalar.
				Operations performed:Insertion has more assignments than SelectionSort, and SelectionSort has more comparisons than InsertionSort. 
				This is why as a total number of operations performed Selection is APROX. equal to InsertionSort. BubbleSort is slightly slower. 
				SelectionSort is the worst in this case: order of complexity: n^2.

2nd CASE: WORST CASE for Bubble Sort, Insertion Sort, Selection Sort.
				Comparisons:All 3 algorithms are very slow. Order of complexity : n^2.
				Assignments: the fastest in SelectionSort, in the middle is InsertionSort, and the slowest is BubbleSort.
				Operations performed: BubbleSort is also the worst(n^2), and Selection Sort is the best.(n^2)

3st CASE: AVERAGE CASE for Bubble Sort, Insertion Sort, Selection Sort.
				Comparisons: BuubleSort is the slowest(n^2), and SelectionSort is the best(n^2). 
				Assignments: The above order is maintained, even though assignments are not as much as comparisons.
				Operations performed: InsertionSort has more assignments than SelectionSort, and SelectionSort has more comparisons than Insertion. 
				This is why as a total of operations performed Selection is equal to Insertion(n^2). Bubble is worse than these two(n^2).


As an order of complexity, it is shown on the graphics that the algorithms have either n^2 complexity, either linear complexity.
	
	
	
	
	
	!!!
	The best method for using from these 3, generally is InsertionSort.
	Selection sort has O(n2) time complexity, making it inefficient on large lists, 
	and generally performs worse than the similar insertion sort. 
	!!!
	
	
Start Date: 07.03.2013
End Date: 11.03.2013
*/

#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <stdlib.h>

#define NMAX 10000;

int ass=0, comp=0;

void display(int m, int vect[]) 
{
	for (int i=0; i<m; i++) 
		printf("%d ",vect[i]);
		printf("\n");
}

void InsertionSort (int m, int vect[10000]) 
{
	int i,j,x;
	for (j=1;j<m;j++) 
	{
		x=vect[j];
		i=j-1;
		ass++;
		int ok=0;
		while((i>=0) && (x<vect[i])) 
		{
			ok=1;
			comp++;
			vect[i+1]=vect[i];
			ass++;
			i=i-1;			
		}
		if (ok==0) comp++;
		vect[i+1]=x;
		ass++;
	}
}

void SelectionSort(int m, int vect[10000]) 
{
	int i,j,poz,x;
	for (i=0;i<m-1;i++) 
	{
		x=vect[i];
		ass++;
		poz=i;
		for(j=i+1;j<m;j++)
		{
			comp++;
			if (vect[j]<x) 
			{
				x=vect[j];
				ass++;
				poz=j;
			}
		}
		vect[poz]=vect[i];
		vect[i]=x;
		ass=ass+2;
	}
}

void BubbleSort(int m,int vect[10000]) 
{
	int i,j,ready,x;

	j=0;
	do {
		ready=1;
		j=j+1;
		for (i=0;i<m-j;i++)
			comp++;
			if (vect[i]>vect[i+1])
			{
				ready=0;
				x=vect[i];
				vect[i]=vect[i+1];
				vect[i+1]=x;
				ass=ass+3;
			}
	} while (ready==0);
}

void GenerateFile() 
{
	FILE *f;
	
	srand((int)clock());
	f=fopen("average.txt","w");
	if (f) {
		for (int i=0; i<10000; i++)
			fprintf(f, "%d ",rand());
	}
	fclose(f);
}

void InsertFile() 
{
	FILE *fileA,*fileB,*fileC,*file1,*file2,*file3;
	int nr[10000],step;

	file1=fopen("best_insertion.txt","w");
	file2=fopen("worst_insertion.txt","w");
	file3=fopen("average_insertion.txt","w");

	step=100;
	while (step<=10000) 
	{
		fileA=fopen("best.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileA,"%d",&nr[i]);
		ass=0;
		comp=0;
		InsertionSort(step,nr);
		fprintf(file1,"%d %d %d\n",step,ass,comp);
		fclose(fileA);
		step=step+100;
		
	}
	

	step=100;
	while (step<=10000) 
	{
		fileB=fopen("worst.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileB,"%d",&nr[i]);
		ass=0;
		comp=0;
		InsertionSort(step,nr);
		fprintf(file2,"%d %d %d\n",step,ass,comp);
		fclose(fileB);
		step=step+100;
	}

	step=100;
	while (step<=10000) 
	{
		fileC=fopen("average.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileC,"%d",&nr[i]);
		ass=0;
		comp=0;
		InsertionSort(step,nr);
		fprintf(file3,"%d %d %d\n",step,ass,comp);
		fclose(fileC);
		step=step+100;
	}

	fclose(file1);
	fclose(file2);
	fclose(file3);
}
void SelectionFile() 
{
	FILE *fileA,*fileB,*fileC,*file1,*file2,*file3;
	int nr[10000],step;

	file1=fopen("best_selection.txt","w");
	file2=fopen("worst_selection.txt","w");
	file3=fopen("average_selection.txt","w");

	step=100;
	while (step<=10000) 
	{
		fileA=fopen("best.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileA,"%d",&nr[i]);
		ass=0;
		comp=0;
		SelectionSort(step,nr);
		fprintf(file1,"%d %d %d\n",step,ass,comp);
		fclose(fileA);
		step=step+100;
	}
	fclose(file1);

	step=100;
	while (step<=10000) 
	{
		fileB=fopen("worst.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileB,"%d",&nr[i]);
		ass=0;
		comp=0;
		SelectionSort(step,nr);
		fprintf(file2,"%d %d %d\n",step,ass,comp);
		fclose(fileB);
		step=step+100;
	}
	fclose(file2);

	step=100;
	while (step<=10000) 
	{
		fileC=fopen("average.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileC,"%d",&nr[i]);
		ass=0;
		comp=0;
		SelectionSort(step,nr);
		fprintf(file3,"%d %d %d\n",step,ass,comp);
		fclose(fileC);
		step=step+100;
	}
	fclose(file3);
}
void BubbleFile() 
{
	FILE *fileA,*fileB,*fileC,*file1,*file2,*file3;
	int nr[10000],step;

	file1=fopen("best_bubble.txt","w");
	file2=fopen("worst_bubble.txt","w");
	file3=fopen("average_bubble.txt","w");

	step=100;
	while (step<=10000) 
	{
		fileA=fopen("best.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileA,"%d",&nr[i]);
		ass=0;
		comp=0;
		BubbleSort(step,nr);
		fprintf(file1,"%d %d %d\n",step,ass,comp);
		fclose(fileA);
		step=step+100;
	}

	step=100;
	while (step<=10000) 
	{
		fileB=fopen("worst.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileB,"%d",&nr[i]);
		ass=0;
		comp=0;
		BubbleSort(step,nr);
		fprintf(file2,"%d %d %d\n",step,ass,comp);
		fclose(fileB);
		step=step+100;
	}

	step=100;
	while (step<=10000) 
	{
		fileC=fopen("average.txt","r");
		for (int i=0; i<step; i++)
			fscanf(fileC,"%d",&nr[i]);
		ass=0;
		comp=0;
		BubbleSort(step,nr);
		fprintf(file3,"%d %d %d\n",step,ass,comp);
		fclose(fileC);
		step=step+100;
	}

	fclose(file1);
	fclose(file2);
	fclose(file3);
}		

void AverageFile() 
{
	FILE *fileA,*fileB,*fileC,*fileD,*fileE,*file1;

	fileA=fopen("average1.txt","r");
	fileB=fopen("average2.txt","r");
	fileC=fopen("average3.txt","r");
	fileD=fopen("average4.txt","r");
	fileE=fopen("average.txt","r");
	file1=fopen("average.txt","w");

	for(int i=0;i<10000;i++) 
	{
		int x,s=0;
		fscanf(fileA,"%d",&x); s=s+x;
		fscanf(fileB,"%d",&x); s=s+x;
		fscanf(fileC,"%d",&x); s=s+x;
		fscanf(fileD,"%d",&x); s=s+x;
		fscanf(fileE,"%d",&x); s=s+x;
		int m=s/5;
		fprintf(file1,"%d ",m);
	}
	fclose(fileA);fclose(fileB);fclose(fileC);fclose(fileD);fclose(fileE);fclose(file1);
}

int main() 
{
	int a[10000],n;

	int test_vector[] = {1,15,4,77,5,88,76,3};
	
														// Demonstration of the working algorithms on a small-sized input here.
	printf("Demo InsertionSort on a small-sized input.\n");
	InsertionSort(8, test_vector);
	display(8, test_vector);
	printf("Demo InsertionSort on a small-sized input.\n");
	SelectionSort(7, test_vector);
	display(8, test_vector);
	printf("Demo BubbleSort on a small-sized input.\n");
	BubbleSort(7, test_vector);
	display(8, test_vector);
	
	AverageFile();
	InsertFile();
	printf("InsertionSort done.\n");
	SelectionFile();
	printf("SelectionSort done.\n");
	BubbleFile();
	printf("BubbleSort done.\n");

	getch();
	return 0;
}