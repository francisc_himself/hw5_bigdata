/*
	Name: ***ANONIM*** ***ANONIM***án
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently linear time transformations between 
					three different representations for a multi-way tree:
					R1: parent representation: for each key you are given the parent key, in a vector.
					R2: multi-way tree representation: for each node you have the key and a vector of 
					children nodes
					R3: binary tree representation: for each node, you have the key, and two pointers: one to 
					the first child node, and one to the brother on the right (i.e. the next brother node)

	Analysis: - transformation from parent representation to multi-way tree representation is straightforward:
				traverse the parent representation and create the adjacency list of the tree
			  - transformation from the multi-way representation to binary tree representation is harder: 
				start from the rightmost brother, create the rooted tree at that vertex, then step to the left borther,
				and link to the right child the tree of the left brother and s.o
			  - this way all transformations are done in O(n);
*/
#include<stdio.h>
#include<stdlib.h>

#define N 100

int n,i,j;
int parent[N];
int *tree[N], root;


struct NODE
{
	int key;
	NODE *left, *right;
}*TreeRoot;

NODE * createBinaryMultiway(int key)
{
	NODE *newNode=new NODE;
	newNode->key=key;
	newNode->left=newNode->right=NULL;
	if(tree[key][0]!=0)
		newNode->left=createBinaryMultiway(tree[key][1]);
	int i;
	NODE *aux1, *aux2=NULL;
	if(tree[key][0]>1)
		aux2=createBinaryMultiway(tree[key][tree[key][0]]);
	for(i=tree[key][0]-1;i>=2;i--)
	{
		aux1=createBinaryMultiway(tree[key][i]);
		aux1->right=aux2;
		aux2=aux1;
	}
	if(aux2!=NULL)
		newNode->left->right=aux2;
	return newNode;
}
void createR3()
{
	TreeRoot=createBinaryMultiway(root);
}

void createR2()
{
	for(i=1;i<=n;i++)
		if(parent[i]!=-1)
			tree[parent[i]][0]++;
	for(i=1;i<=n;i++)
	{
		tree[i]=(int *)malloc((1+tree[i][0])*sizeof(int));
		tree[i][0]=0;
	}
	for(i=1;i<=n;i++)
		if(parent[i]!=-1)
			tree[parent[i]][++tree[parent[i]][0]]=i;
		else root=i;

}

void displayR2()
{
	for(i=1;i<=n;i++)
	{
		printf("%d: ", i);
		for(j=1;j<=tree[i][0];j++)
			printf("%d ", tree[i][j]);
		printf("\n");
	}
	printf("\n");
}

void displayR3(NODE *node, int level)
{
	if(node!=NULL)
	{
		int i;
		for(i=0;i<=3*level;i++)
			printf(" ");
		printf("%d\n", node->key);
		displayR3(node->left, level+1);
		displayR3(node->right, level);
	}
}

int main()
{
	freopen("in.txt", "r", stdin);
	scanf("%d", &n);
	for(i=1;i<=n;i++)
	{
		scanf("%d", &parent[i]);
		tree[i]=(int *)malloc(sizeof(int));
		tree[i][0]=0;
	}
	createR2();
	displayR2();
	createR3();
	displayR3(TreeRoot, 0);
	return 0;
}