#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Profiler.h"
Profiler profiler("demo");
#define n 10000
int m;
typedef struct nod
{
	nod *p;
	int rank;
}node;
node *x[n];
node *y[n];
void makeSet(node *x[],int i)
{
	x[i]=(node *)malloc(sizeof(node));
	x[i]->rank=0;
	x[i]->p=x[i];
	profiler.countOperation("op",m,1);
}
node* findSet(node *x)
{
if(x!=x->p)
	findSet(x->p);
profiler.countOperation("op",m,1);
return x;
}
void link(node *a,node *b)
{
	if(a->rank>b->rank)
				b->p=a;
	else if(a->rank<b->rank)
				a->p=b;
	else 
		b->rank++;
	profiler.countOperation("op",m,1);
}
void unioN(node *a,node *b)
{
	link(findSet(a),findSet(b));
	profiler.countOperation("op",m,1);
}
int main()
{
	
	int graph[n][n];
	for(m=100;m<=6000;m+=100)
	{		
	int aux=0;
		printf("%d\n",m);
		for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
		{
		if((i==j)||(aux>=m))
			graph[i][j]=0;
		if((i!=j)&&(aux<m))
		{
			graph[i][j]=graph[j][i]=1;
			aux++;
		}
		}
		for(int i=0;i<n;i++)
		{
		makeSet(x,i);
		}
		
	for(int i=0;i<n;i++)	
		for(int j=0;j<n;j++)
		{
			if(graph[i][j]==1)
			{
				if(findSet(x[i])!=findSet(x[j]))
				{
					unioN(x[i],x[j]);
				}
			}
		}		
	}		
	profiler.createGroup("OP","op");
	profiler.showReport();
}