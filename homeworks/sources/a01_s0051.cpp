// ***ANONIM*** ***ANONIM*** Melania, gr ***GROUP_NUMBER***
//Eu cred ca pentru cazul mediu cea mai eficienta metoda este a selectiei
//Pentru cazul favorabil este metoda bubble
//Pentru cazul defavorabil metoda insertiei

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"

Profiler profiler("Lab2");

int i,aux,ok,x,j,k,imin;

void buble(int *v,int n)
{
	ok=false;
	profiler.countOperation("assign_buble",n);
	while (!ok)
	{
		ok=true;
		profiler.countOperation("comp_buble",n);
		profiler.countOperation("assign_buble",n);
		for (i=0;i<n-1;i++)
		{
			profiler.countOperation("comp_buble",n);
			if(v[i]>v[i+1]) 
			{
				aux=v[i];
				v[i]=v[i+1];
				v[i+1]=aux;
				ok=false;
				profiler.countOperation("assign_buble",n,4);
			}
		}
	}
}

void insertie(int *v,int n)
{
	for(i=1;i<n;i++)
	{
		profiler.countOperation("comp_insertie",n);
		x=v[i];
		j=0;
		profiler.countOperation("assign_insertie",n,2);
		while(v[j]<x)
		{
			profiler.countOperation("comp_insertie",n);
			j=j+1;
			profiler.countOperation("assign_insertie",n);
		}
		for(k=i;k>=j+1;k--)
		{
			profiler.countOperation("comp_insertie",n);
			v[k]=v[k-1];
			profiler.countOperation("assign_insertie",n);
		}
		v[j]=x;
		profiler.countOperation("assign_insertie",n);
	}
}

void selectie(int *v,int n)
{
	for(i=0;i<n-1;i++)
	{
		profiler.countOperation("comp_selectie",n);
		imin=i;
		profiler.countOperation("assign_selectie",n);
		for(j=i+1;j<n;j++)
		{
			profiler.countOperation("comp_selectie",n);
			if(v[j]<v[imin])
			{
				imin=j;
				profiler.countOperation("assign_selectie",n);
			}
			
		}
		aux=v[imin];
		v[imin]=v[i];
		v[i]=aux;
		profiler.countOperation("assign_selectie",n,3);
	}
}

void afisare(int *v,int n)
{
	printf("Vectorul sortat este: ");
	for(i=0;i<n;i++)
	{
		printf("%d, ",v[i]);
	}
	printf("\n");
}

void main()
{
	int test[]={4,9,1,8,3};
	int test2[20];
	int test3[20];
	int n;
	int v[10000],v1[10000],v2[10000],v3[10000];
	memcpy(test2,test,5*sizeof(int));
	memcpy(test3,test,5*sizeof(int));

	//buble(test,5);
	//afisare(test,5);

	//insertie(test2,5);
	//afisare(test2,5);

	//selectie(test3,5);
	//afisare(test3,5);



	/*for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
		buble(v1,n);
		memcpy(v2,v,n*sizeof(int));
		insertie(v1,n);
		memcpy(v3,v,n*sizeof(int));
		selectie(v1,n);
	}

	
	profiler.addSeries("comp_assig_buble","comp_buble","assign_buble");
	profiler.addSeries("comp_assig_insert","comp_insertie","assign_insertie");
	profiler.addSeries("comp_assig_select","comp_selectie","assign_selectie");

	profiler.createGroup("medie_assign","assign_buble","assign_insertie","assign_selectie");
	profiler.createGroup("medie_comp","comp_buble","comp_insertie","comp_selectie");
	profiler.createGroup("medie_assign_comp","comp_assig_buble","comp_assig_insert","comp_assig_select");
	*/



	// -------BEST------
	//Cazul cel mai favorabil este atunci cand sirul este deja sortat,pt toate cele 3 metode de sortare(bubble,selectie,insertie)

	/* for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,0,10000,true,1);
		memcpy(v1,v,n*sizeof(int));
		buble(v1,n);
		memcpy(v2,v,n*sizeof(int));
		insertie(v1,n);
		memcpy(v3,v,n*sizeof(int));
		selectie(v1,n);
	}
	

	
	profiler.addSeries("comp_assig_buble","comp_buble","assign_buble");
	profiler.addSeries("comp_assig_insert","comp_insertie","assign_insertie");
	profiler.addSeries("comp_assig_select","comp_selectie","assign_selectie");

	profiler.createGroup("medie_assign","assign_buble","assign_insertie","assign_selectie");
	profiler.createGroup("medie_comp","comp_buble","comp_insertie","comp_selectie");
	profiler.createGroup("medie_assign_comp","comp_assig_buble","comp_assig_insert","comp_assig_select");
	*/




	//------WORSE----
	//Cazul cel mai defavorabil pt metodele bubble si insertie este  atunci cand sirul este sortat descrescator
	//Iar pt metoda selectie cazul defavorabil este atunci cand vectorul este sortat crescator doar ca primul element(cel mai mic)este pus la sfarsit

	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,0,10000,true,2);
		memcpy(v1,v,n*sizeof(int));
		buble(v1,n);
		memcpy(v2,v,n*sizeof(int));
		insertie(v1,n);
		
	}
	
	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,0,10000,true,1);
		int t=v[0];
		for (j=1;j<n;j++)
			v[j-1]=v[j];
		v[n-1]=t;
		memcpy(v3,v,n*sizeof(int));
		selectie(v1,n);
	}
	
	profiler.addSeries("comp_assig_buble","comp_buble","assign_buble");
	profiler.addSeries("comp_assig_insert","comp_insertie","assign_insertie");
	profiler.addSeries("comp_assig_select","comp_selectie","assign_selectie");

	profiler.createGroup("medie_assign","assign_buble","assign_insertie","assign_selectie");
	profiler.createGroup("medie_comp","comp_buble","comp_insertie","comp_selectie");
	profiler.createGroup("medie_assign_comp","comp_assig_buble","comp_assig_insert","comp_assig_select");




	profiler.showReport();
}