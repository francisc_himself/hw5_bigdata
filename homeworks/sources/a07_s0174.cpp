#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

/*
*
*
*
*
*/

typedef struct multiwayTree {
	int value;
	int kidsNr;
	int parent;
	struct multiwayTree *child[100];
} multiwayTree;

typedef struct binaryTree {
	int value;
	struct binaryTree *leftKid, *rightKid;
} binaryTree;

multiwayTree *root;
multiwayTree *multiTree[100];
binaryTree *tree[100];
int ParentVector[100];
int n;

void realizeParentRepresentation() {
   int i, aux;

   for(i = 0; i < n; i++){
	   multiTree[i] = (multiwayTree*)malloc(sizeof(multiwayTree));
	   multiTree[i]->value = i;
	   multiTree[i]->kidsNr = 0;
	   multiTree[i]->child[1] = NULL;
   }
   for(i = 0; i < n; i++){
	   if(ParentVector[i] == -1)
                root=multiTree[i];
	   else {
                multiTree[ParentVector[i]]->kidsNr++;
		aux = multiTree[ParentVector[i]]->kidsNr;
		multiTree[ParentVector[i]]->child[aux] = multiTree[i];
	   }
   }
}


 void realizeMultiwayTreeRepresentation(multiwayTree *p) {
	int i;

	if(p != NULL){
		for(i = 1; i <= p->kidsNr; i++) {
			if(i == 1) {
                                 int j = p->value;
                                 tree[j]->leftKid = tree[p->child[1]->value];
			}
			else{
                            tree[p->child[i-1]->value]->rightKid = tree[p->child[i]->value];
			}
			realizeMultiwayTreeRepresentation(p->child[i]);
		}
	}
}



void realizeBinaryTreeRepresentation(int a, int blank) {
   int i;
   printf("\n");
   for(i = 0; i <= 5 * blank; i++)
	   printf(" ");
   printf("%d", a);

   if(tree[a]->leftKid != NULL)
	     realizeBinaryTreeRepresentation(tree[a]->leftKid->value, blank + 1);
   if(tree[a]->rightKid != NULL)
	     realizeBinaryTreeRepresentation(tree[a]->rightKid->value, blank);
}

void read(){
	printf("n= ");
	scanf("%d", &n);
	printf("%d elemente:", n);
	printf("\n", n);
	int i;
	for(i = 0; i < n; i++){
		printf("P[%d]=", i);
		scanf("%d", &ParentVector[i]);
	}
}

int main(){
	int i;
	read();

	n=9;
	ParentVector[0]=1;
	ParentVector[1]=6;
	ParentVector[2]=4;
	ParentVector[3]=1;
	ParentVector[4]=6;
	ParentVector[5]=6;
	ParentVector[6]=-1;
	ParentVector[7]=4;
	ParentVector[8]=1;

	realizeParentRepresentation();

	for(i = 0; i < n; i++){
		tree[i] = (binaryTree*)malloc(sizeof(binaryTree));
		tree[i]->value = i;
		tree[i]->leftKid = tree[i]->rightKid = NULL;
	}
	realizeMultiwayTreeRepresentation(root);

	printf("\nBinary tree representation: \n\n\n");
	printf("%d\n", root->value);
	realizeBinaryTreeRepresentation(tree[1]->value, 0);
	getch();
    return 0;
}
