/*
	Nume : ***ANONIM***-Cornel ***ANONIM***
	Grupa : ***GROUP_NUMBER***
	
	Complexitate : O ( N log K )

	Numarul de operatii in cele 3 cazuri creste liniar, crescand o data cu cresterea numarului de liste
	Numarul de operatii pentru liste de lungime egala are un pas logaritmic
	
*/


#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <fstream>
#include "Profiler.h"
using namespace std;


Profiler P("Assign4");

typedef struct node{
	int val;
	node *next;
} node;

typedef struct positionitionElementent{
	int position;
	int element;
} positionitionElementent;


struct list{
	node *first;
	node *last;
};




struct list lists[501];
node *interfirst ;
node *interlast;
positionitionElementent Heap[10001];
int a[10001];



//Inserare in lista
void insert(node **first,node **last,int val){
	if((*first)==NULL){
		(*first) = new node;
		(*first)->val=val;
		(*first)->next=NULL;
		*last=(*first);
	}
	else{
		node *element=new node;
		element->next=NULL;
		element->val=val;
		(*last)->next=element;
		(*last)=element;
	}
}

//
void max_heapify_down(positionitionElementent Heap[],int i,int size,int &comparisons,int &assignments){
	int left,right,largest;
	positionitionElementent aux;
	left=2*i;
	right=2*i+1;
	if ((left<=size)&&(Heap[left].element < Heap[i].element)){
		largest=left;
	}
	else{
		largest=i;
	}
	if ((right<=size)&&(Heap[right].element < Heap[largest].element)){
		largest=right;
	}
	
	comparisons+=2;

	if (largest!=i)	{
		aux=Heap[i];
		Heap[i] = Heap[largest];
		Heap[largest]=aux;
		assignments+=3;
		max_heapify_down(Heap,largest,size,comparisons,assignments);
	}
}

//inserare in heap
void insertInHeap(positionitionElementent Heap[],positionitionElementent x,int *size,int &comparisons,int &assignments){
	(*size)++;
	Heap[(*size)]=x;
	int i=(*size);
	positionitionElementent aux;
	
	while (i>1 && (Heap[i].element)<(Heap[i/2].element))	{
		comparisons++;
		aux=Heap[i];
		Heap[i]=Heap[i/2];
		Heap[i/2]=aux;
		assignments+=3;
		i=i/2;
	}
	comparisons++;
}

//sterge din heap
positionitionElementent removeFromHeap(positionitionElementent Heap[],int *size,int &comparisons,int &assignments){
	positionitionElementent x;
	if((*size)>0)	{
		x=Heap[1];

		Heap[1]=Heap[(*size)];
		assignments++;
		(*size)--;
		
		max_heapify_down(Heap,1,(*size),comparisons,assignments);
	}
	return x;
}


//interclaseaza liste
void mergeLists(list lists[],int k,int &comparisons,int &assignments){
	int size = 0;
	positionitionElementent v;
	for(int i=1; i<=k; i++)	{
		v.position=i;
		v.element=lists[i].first->val;
		assignments++;
		insertInHeap(Heap,v,&size,comparisons,assignments);
	}
	positionitionElementent pop;
	while (size>0){

		pop = removeFromHeap(Heap,&size,comparisons,assignments);

		int position = pop.position;
		insert(&interfirst,&interlast,pop.element);

		lists[position].first=lists[position].first->next;
		assignments++;
		comparisons++;
		
		if (lists[position].first!=NULL){
			v.position=pop.position;
			v.element=lists[pop.position].first->val;
			assignments++;
			insertInHeap(Heap,v,&size,comparisons,assignments);
		}
	}
}

//creaza liste
void createLists(list lists[],int k,int n){
	int nr = 0;
	
	for (int i=1; i<=k; i++){
		lists[i].first = lists[i].last = NULL;
	}
	int arr[10000];
	for (int j=1; j<=k; j++){
		FillRandomArray(arr,n,1,50000,false,1);
		for (int i=1; i<=n/k; i++){
			insert(&lists[j].first,&lists[j].last,arr[i]);
		}
	}
}

//afisare lista
void print(node *first,node *last){
    node *c;
    c=first;
    while(c!=0){
		cout<<c->val<<" ";
        c=c->next;
    }
	cout<<"\n";
    cout<<endl;
}

void show(list CAP[],int k){
    for (int i =1 ; i<=k;i++){
        print(CAP[i].first,CAP[i].last);
	}
}

//exemplu pt 5, 10 si 100 liste
void example(){
	
	int comparisons,assignments;

	int n=20;
	int k=5;
	createLists(lists,k,n);
	show(lists,k);


	mergeLists(lists,k,comparisons,assignments);

	print(interfirst,interlast);
	
	getch();
}


int main(){
	
	P.createGroup("Total Operatii","Operatii 5 liste","Operatii 10 liste","Operatii 100 liste");
	P.createGroup("Constant length","Operatii");
	int comparisons,assignments;


	int n;


	int k =5;
	for(int index=100;index<=10000;index+=100)
	{
		createLists(lists,k,index);
		comparisons=0;
		assignments=0;
		mergeLists(lists,k,comparisons,assignments);

		P.countOperation("Operatii 5 liste",index,comparisons);
		P.countOperation("Operatii 5 liste",index,assignments);
	}
	
	k=10;
	for(int index=100;index<=10000;index+=100)
	{
		createLists(lists,k,index);
		comparisons=0;
		assignments=0;
		mergeLists(lists,k,comparisons,assignments);

		P.countOperation("Operatii 10 liste",index,comparisons);
		P.countOperation("Operatii 10 liste",index,assignments);
	}

	k=100;
	for(int index=100;index<=10000;index+=100)
	{
		createLists(lists,k,index);
		comparisons=0;
		assignments=0;
		mergeLists(lists,k,comparisons,assignments);

		P.countOperation("Operatii 100 liste",index,comparisons);
		P.countOperation("Operatii 100 liste",index,assignments);
	}
	

	n=10000;
	for(int i=10;i<=500;i+=10)
	{
		createLists(lists,i,n);
		comparisons=0;
		assignments=0;
		mergeLists(lists,i,comparisons,assignments);

		P.countOperation("Operatii",i,comparisons);
		P.countOperation("Operatii",i,assignments);
	}
	
	
	
	P.showReport();
	example();
}