//c1,c2=1
//As alpha grows, the number of found elements also grows and the amount of the not found elements decreases
#include<iostream>
#include<conio.h>
#include<stdio.h>
#include "Profiler.h"

#define null 0
#define m 9973

Profiler profiler("demo");

/*
int insert(int[], int);
int search();
int hash(int, int);
*/
double alfa[]={0.8, 0.85, 0.9, 0.95, 0.99};
int t[20000],arr[20000];
int k, i=1,n,found,notFound;
int in[20000];
int c1=1, c2=1;
int effort;

int hash(int k, int i){
	int l = ((k % m + c1*i + c2*i*i) % m);
	return l;
}

int insert(int t[], int k){
	int j = 0;
	i = 0;
	do
	{
		j = hash(k, i);
		if (t[j] == null){
			t[j] = k;
			return j;
		}
		else i++;
	}
	while (i != m);
	//printf("hash table overflow");
    printf("overflow");
    return -1;
}

int search(int t[], int k){
	int i = 0, j, x=0;
	effort=0;
	do{
		j = hash(k, i);
		effort++;
		if (t[j] == k){
			return j;
		}
		i++;
	}
	while ((t[j]!= null) && (i != m));
	return -3;
}

void initHash(int t[]){
	for (int i=0; i<m; i++)
		t[i] = NULL;
}

void create(){
	int a, index;
	int foundTotal, notFoundTotal;
	int effortFound, effortNotFound;
	int maximF, maximNF;
	
	for(index = 0; index < 5; index++){
	n = m * alfa[index];
	maximF = 0;
	maximNF = 0;
	foundTotal = 0;
	notFoundTotal = 0;
	effortFound=0; effortNotFound=0;
	initHash(t);
	FillRandomArray(arr,n,1,30000,true);

	for(int j = 0; j < n; j++){
		a = insert(t, arr[j]);
		if(a == -1) break;
	}

	//for(int j = 1501; j < 3002; j++)
	//	in[j] = arr[j];
	FillRandomArray(in,3000,1,30000);

	for(int j=0; j<3000; j++){
		a = search(t,in[j]);
		if(a == -3){
			if (effort > maximNF )
				maximNF = effort;				
			effortNotFound+=effort;
			notFoundTotal ++;	
		}
		else {
			if (effort > maximF)
				maximF = effort;
			effortFound+=effort;
			foundTotal ++;
		}
	}
	double mm = effortFound/foundTotal;
	double mm2 = effortNotFound/notFoundTotal;
	printf("%3.2f          %3.2f          %d           %3.2f          %d\n",alfa[index], mm,  maximF, mm2,  maximNF);
	}
	//printf("Maximum effort for Found element: %d \nMaximum effort for not found elements: %d",maximF,maximNF);
}

int main(){
	for (int help = 0; help < 5 ;help++){
	printf("\n\nAlpha    Avg Eff F     Max Eff F     Avg Eff NF     Max Eff NF\n");
	create();
	printf("\n\n");
	}
	getch();
	return 0;
}


