#include <stdio.h>
#include <iostream>
using namespace std;
#include "profiler.h"
Profiler profiler("demo");

int f;

int partition(int *v, int p, int r)
{
	int x=v[r];
	int i=p-1;
	 profiler.countOperation("quicksort", f, 1);

	for (int j=p;j<r;j++)
	{
		profiler.countOperation("quicksort", f, 1);
		if (v[j]<=x)
		{
			i++;
			 profiler.countOperation("quicksort", f, 1);

			if (v[j]!=v[i])
			{
				int aux=v[j];
				v[j]=v[i];
				v[i]=aux;
				 profiler.countOperation("quicksort", f, 3);
			}
			
		}
		
	}
	int aux=v[i+1];
	v[i+1]=v[r];
	v[r]=aux;
	 profiler.countOperation("quicksort", f, 3);

	return i+1;
}


void quicksort(int *v, int p, int r)
{
	
	if (p<r)
	{
		int q=partition(v,p,r);
		// profiler.countOperation("quicksort", f);

		quicksort(v,p,q-1);
		quicksort(v,q+1,r);
	}
	
}

int partitionb(int *v, int p, int r)
{
	int x=v[r];
	int i=p-1;
	 profiler.countOperation("quicksortb", f, 2);

	for (int j=p;j<r;j++)
	{
		if (v[j]<=x)
		{
			i++;
			 profiler.countOperation("quicksortb", f, 2);

			if (v[j]!=v[i])
			{
				int aux=v[j];
				v[j]=v[i];
				v[i]=aux;
				 profiler.countOperation("quicksortb", f, 3);
			}
			
		}
		
	}
	int aux=v[i+1];
	v[i+1]=v[r];
	v[r]=aux;
	 profiler.countOperation("quicksortb", f, 3);

	return i+1;
}


void quicksortb(int *v, int p, int r)
{
	
	if (p<r)
	{
		int q=partitionb(v,p,r);
		 profiler.countOperation("quicksortb", f);

		quicksortb(v,p,q-1);
		quicksortb(v,q+1,r);
	}
	
}

int partitionw(int *v, int p, int r)
{
	int x=v[r];
	int i=p-1;
	 profiler.countOperation("quicksortw", f, 2);

	for (int j=p;j<r;j++)
	{
		if (v[j]<=x)
		{
			i++;
			 profiler.countOperation("quicksortw", f, 2);

			if (v[j]!=v[i])
			{
				int aux=v[j];
				v[j]=v[i];
				v[i]=aux;
				 profiler.countOperation("quicksortw", f, 3);
			}
			
		}
		
	}
	int aux=v[i+1];
	v[i+1]=v[r];
	v[r]=aux;
	 profiler.countOperation("quicksortw", f, 3);

	return i+1;
}

void quicksortw(int *v, int p, int r)
{
	
	if (p<r)
	{
		int q=partitionw(v,p,r);
		 profiler.countOperation("quicksortw", f);

		quicksortw(v,p,q-1);
		quicksortw(v,q+1,r);
	}
	
}

int left(int i ){
    return  2*i;
}

int right(int i){
    return 2*i+1;    
}
void max_heapify(int *a, int p, int i)
{
 int l,r,x,heapsize;
 heapsize=p;
 int largest=0;
 l=left(i);
 r=right(i);
 profiler.countOperation("heapsort", f, 4);
 if ((l<=heapsize) && (a[l]>a[i]))
 {
	 largest=l;
	 profiler.countOperation("heapsort", f);
 }
 else
 {
	 largest=i;
	 profiler.countOperation("heapsort", f);
 }
 
 if ((r<=heapsize) && (a[r]>a[largest]))
 {
	 largest=r;
	 profiler.countOperation("heapsort", f);
 }
 if (largest!=i)
 {
	 x=a[i];
	 a[i]=a[largest];
	 a[largest]=x;
	 profiler.countOperation("heapsort", f,3);
	 max_heapify(a,p,largest);
 }
}


void build_max_heapbu(int *v, int p)
{
	int heapsize=p;
	profiler.countOperation("heapsort",f);
	for (int i=(p/2);i>=1;i--)
	{
		max_heapify(v,p,i);
	}
}
void heapsort(int *v,int p)

{
	int m=p;
	profiler.countOperation("heapsort",f);
	build_max_heapbu( v, m);
	for(int i=m;i>=2;i--)
	{
		int x=v[1];
		v[1]=v[i];
		v[i]=x;
		m--;
		profiler.countOperation("heapsort", f,4);
		max_heapify(v,m ,1);
	}

}

void afisare(int *v, int n)
{
	for(int i=0;i<=n;i++)
	{
		printf("%d ",v[i]);
	}

}


int main()
{
	/*int test[]={0, 1,6,7,2,3,4,8,5,9};
	int p=9;
	int test1[]={0, 1,6,7,2,3,4,8,5,9};
	int p1=9;
	int st=0;
	int dr=p;
	printf("quicksort\n");
	quicksort(test,st,dr);
	afisare(test,p);
	printf("\n heapsort\n");
	heapsort(test1,p);
	afisare(test1,p1);
	printf("\n");*/

	
	int va[10000],vaa[10000],vaaa[10000];
	for (int n=0;n<5;n++){
	for(f=100;f<1000;f+=100)
	{
		

		
		FillRandomArray(va,f);
	    memcpy(vaa,va,f*sizeof(int));
		heapsort(vaa,f);
		memcpy(vaaa,va,f*sizeof(int));
	    quicksort(vaaa,0,f);
		
		
							
	}
	
	}
	
	profiler.createGroup("average","heapsort","quicksort");
	
	
	
	
	int vb[10000],vbb[10000];
	for (int n=0;n<5;n++){
	for(f=100;f<1000;f+=100)
	{
		
		FillRandomArray(vb,f,10,50000,false,1);
		memcpy(vbb,vb,f*sizeof(int));
	    quicksortb(vbb,f/2,f);

	}
	
	profiler.createGroup("best","quicksortb");
	
	
	}
	
	int vc[10000],vcc[10000];
	
    for (int n=0;n<5;n++){
	for(f=100;f<1000;f+=100)
	{
		
		FillRandomArray(vc,f,10,50000,false,2);
		memcpy(vcc,vc,f*sizeof(int));
	    quicksortw(vcc,0,f);

	}
	}
	profiler.createGroup("worst","quicksortw");
	profiler.createGroup("quicksortcompabw","quicksort","quicksortb","quicksortw");
	profiler.addSeries("quicksort","quicksortb","quicksortw");
	profiler.showReport();
	
}