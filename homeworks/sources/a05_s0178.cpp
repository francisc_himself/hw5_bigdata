#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>
#include<time.h>

using namespace std;
ofstream fout;

struct node {
	int value;
	int list;
	struct node *next;
};

long atr, comp;

int left (int i);
int right (int i);
int parent (int i);
void buildMinHeap(node* a[], int n);
void minHeapify (node* a[], int n, int i);
void merge (node *root[], int k, int a[]);
void writeToFile (int n, int k, long atr, long comp, char * fileName);


void main() {
	
	struct node *root2[16];
	for (int i=0;i<16;i++)
		root2[i] =(node*)malloc(sizeof(node));

	int n;
	int x=0;
	int a[10000];
	int a1[20]={1, 3, 5, 7, 9, 2, 4, 6, 8, 10, 3, 7, 9, 11, 13, 4, 6, 8, 10, 12};
	
	for (int ki=0;ki<4;ki++){
		//printf ("Introduceti elementul 1 al listei %d ", (ki+1));
		//scanf ("%d", &x);
		root2[ki]->value=a1[x];
		x++;
		root2[ki]->list=ki;
		node *conductor=(node*)malloc(sizeof(node));
		conductor=root2[ki];
		
		for (int z=1;z<5;z++){
			
			//printf ("Introduceti elementul %d al listei %d ", (z+1), (ki+1));
			//scanf ("%d", &x);
			node *current=(node*)malloc(sizeof(node));
			current->value=a1[x];
			x++;
			current->list=ki;
			current->next=NULL;
			conductor->next=current;
			
			conductor=current;
			*current++;
		}
	}

	
	
	merge(root2, 4, a);

	for (int i=0;i<20;i++)
		printf ("%d ", a[i]);



	struct node *root[2005];
	for (int i=0;i<2000;i++) {
		root[i] =(node*)malloc(sizeof(node));
	}


	int k[3]={5, 10, 100};
	for (int m=0;m<3;m++){
		for (n=100; n<10000; n=n+100){
			srand(time(NULL));
			for (int i=0;i<k[m];i++){
				root[i]->value = rand()%50;
				root[i]->list = i;
				node *conductor = (node*)malloc(sizeof(node));
				conductor=root[i];
				conductor->next=NULL;
				for (int j=1;j<n/k[m];j++){
					node *current = (node*)malloc(sizeof(node));
					current->value = conductor->value + rand()%50;
					current->list=i;
					current->next=NULL;
					conductor->next=current;
					conductor = current;
				//	*current++;
				}
			}
			atr=0;
			comp=0;
			merge (root, k[m], a);
			writeToFile (n, k[m], atr, comp, "avg.csv");
		}
	}

	n=10000;
	int j;
	for (j=10; j<500; j=j+10) {
		srand(time(NULL));
		for (int i=0;i<j;i++) {
			root[i]->value = rand()%50;
			root[i]->list=i;
			node *conductor = (node*)malloc(sizeof(node));
			conductor = root[i];
			for (int l=1;l<n/j;l++){
				node *current = (node*)malloc(sizeof(node));
				current->value = conductor->value + rand()%50;
				current->list=i;
				current->next=NULL;
				conductor->next=current;
				conductor = current;
				*current++;
			}
		}
		atr=0;
		comp=0;
		merge (root, j, a);
		writeToFile (n, j, atr, comp, "avg.csv");

	}

	getch();

}

int left (int i){
	return 2*i;
}

int right (int i) {
	return 2*i+1;
}

int parent (int i) {
	return (int)floor((float)i/2);
}

void buildMinHeap(node* a[], int n) {
	for (int i=(int)floor((float)n/2);i>=1;i--) {
		minHeapify (a, n, i);
	}
}

void minHeapify (node* a[], int n, int i){
	int l, r, min;
	node *aux;
	l=left(i);
	r=right(i);
	if (l<=n && a[l]->value<a[i]->value)
		min = l;
	else
		min = i;
	comp++;
	if (r<=n && a[r]->value<a[min]->value) 
		min = r;
	comp++;
	if (min!=i) {
		aux=a[i];
		a[i]=a[min];
		a[min]=aux;
		atr+=3;
		minHeapify (a, n, min);
	}
}

void merge (struct node *root[], int k, int a[]) {
	int currPos=0, index;
	node *b[510];
	for (int i=0;i<k;i++)
		b[i+1]=root[i];
	int min;
	buildMinHeap (b, k);
	while (k>0) {
		a[currPos]=b[1]->value;
		atr++;
		currPos++;
		node *first = root[b[1]->list];
		if (root[b[1]->list]!=NULL)
			if (root[b[1]->list]->next!=NULL){
				root[b[1]->list]=root[b[1]->list]->next;
				b[1]=root[b[1]->list];
				atr++;
				minHeapify (b, k, 1);
				free (first);
			}
		else{
			b[1]=b[k];
			atr++;
			k--;
			minHeapify (b, k, 1);
		}
	}
}

void writeToFile (int n, int k, long atr, long comp, char * fileName) {
	fout.open (fileName, ios::app );
	fout<<n<<','<<k<<','<<atr<<','<<comp<<','<<(atr+comp)<<'\n';
	fout.close();
}



