
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>

long cHeap;
long aHeap;
long cQuick;
long aQuick;
long heapSize;
/*
Heap Sort vs. Quick Sort:
-the input for each case was generated in the rage from 100 to 10 000,incrementing by 100 so the results are acurate enough
Conclusion:
QuickSort is more efficicent, performing less total operations, while Heapsort performs between sligthly a lot more making the quick sort mush better and efficient

Quick Sort:
	Worst:takes the last element at each partition
	Best:the partition devides the array into 2 equal parts,but unfortunately it does not work as expected
	Average: each partition divides the array into 1 and the rest of the elements
	Overall: Heapsort is an optimal sorting algorithm. In practice quicksort clarly behaves better,even if it is not optimal.

�A good implementation of quicksort IS optimal

*/
void heapify(long a[], int i)  
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    cHeap=cHeap+1;
    if (left<=heapSize && a[left]>a[i]) 
        largest=left;	
    else
        largest=i;

    cHeap=cHeap+1;
    if (right<=heapSize && a[right]>a[largest])
        largest=right;
    if (largest!=i) 
        {
            aHeap+=3;
			//swap root
            aux=a[i];
            a[i]=a[largest];
            a[largest]=aux;
            heapify(a,largest);  
        }
}

void bh_bottomup(long a[],int n)
{
    int i;
	heapSize=n;
    for (i=heapSize/2;i>=1;i--) 
        heapify(a,i); 
}
//he heap sort which is an optimal algorithm 
void heapSort(long a[],int n)
{
    int k;
    long aux;
    bh_bottomup(a,n);
    for (k=n;k>=2;k--)
    {
        aHeap+=3;
        aux=a[1];
        a[1]=a[k];
        a[k]=aux;
        heapSize=heapSize-1;
        heapify(a,1);

    }
}
//partitioning for the quick sort algorithm 
int partition(long a[],int p, int r,int Pivot){

	long x,aux;
	int i,j;
	if(Pivot==1)
		x=a[r];
	else if(Pivot==2)
		x=a[(p+r)/2];
	else if(Pivot==3)
		x=a[p+1];
	aQuick++;
	i=p-1;
	for(j=p;j<=r-1;j++){
		cQuick++;
		if(a[j]<=x)
			{i++;
			aQuick+=3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	aQuick+=3;
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;

	return i+1;
}

void quickSort(long a[],int p, int r,int Pivot)
{	if ( p < r )
    {
        int q = partition(a, p, r,Pivot); 
			quickSort(a, p, q-1,Pivot);
			quickSort(a, q+1, r,Pivot);
    }
}

int main()
{
	srand(time(NULL));
    long h[10001];
    long q[10001];
    int j,k,nr;
    long cHeapeap=0,cQuickuick=0,aHeapeap=0,aQuickuick=0;
    FILE *f,*g,*b,*w;
    f=fopen("averageHQ.csv","w");
	w=fopen("worstquick.csv","w");
	g=fopen("averagequick.csv","w");
	b=fopen("bestquick.csv","w");
	
	long a[11];
	a[0]=0;a[1]=12;a[2]=2;a[3]=1;
	a[4]=2;a[5]=25;a[6]=9;a[7]=13;
	a[8]=8;a[9]=13;a[10]=17;
	
	heapSort(a,10);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n");
	aQuick=cQuick=0;
	quickSort(a,1,10,1);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);


	
	//average case
    for (int i=100;i<=10000;i=i+100)
	{
		cHeap=cQuick=aHeap=aQuick=0;

		for (j=1;j<=5;j++)
		{
			nr=0;
			cHeapeap=cQuickuick=aHeapeap=aQuickuick=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				h[nr]=q[nr]=rand();
			}

			heapSort(h,i);
			quickSort(q,1,i,1);

			cHeapeap+=cHeap;
			cQuickuick+=cQuick;
			aHeapeap+=aHeap;
			aQuickuick+=aQuick;
		}
			fprintf(g,"%d %ld %ld %ld \n",i,cQuickuick/5,aQuickuick/5,cQuickuick/5+aQuickuick/5);
			fprintf(f,"%d %ld %ld %ld %ld %ld %ld\n",i,cHeapeap/5,aHeapeap/5,cHeapeap/5+aHeapeap/5,cQuickuick/5,aQuickuick/5,cQuickuick/5+aQuickuick/5);
	}

	//best case
	for (int i=100;i<=10000;i=i+100)
	{
		cQuick=aQuick=0;
		nr=1;
		for (j=1;j<=i;j++) 
		{
	
			q[nr]=j;
			nr++;
		}
		quickSort(q,1,i,2);
		fprintf(b,"%d %ld %ld %ld \n",i,cQuick,aQuick,cQuick+aQuick);
		
	}

	
	//worst case
	for (int i=100;i<=10000;i=i+100)
	{
		cQuick=aQuick=0;
		nr=1;
		for (j=1;j<=i;j++)
		{
			q[nr]=j;
			nr++;
		}
		quickSort(q,1,i,3);
		fprintf(w,"%d %ld %ld %ld \n",i,cQuick,aQuick,cQuick+aQuick);
		
	}
	
	
	fclose(f);
	fclose(g);
	fclose(b);
	fclose(w);
	//getch();
	return 0;

	
}
