/******************
***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***;
Source:http://en.wikipedia.org/wiki/Quadratic_probing;
Am observat la folosirea tabelelor de dispersie ca odata cu cresterea factorului de umplere creste numarul
de ciocniri atunci cand se gaseste elementul si creste si numarul de ciocniri atunci cand nu se gaseste
elementul.
********************/

#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>
#define SIZE 10007
#define MAX 10000
int searchArray[3000];
float sumF;
float sumNF;
int maxF;
int maxNF;
void initialiseEmpty(int *empty)
{
	for(int i=0;i<SIZE;i++)
	{
		empty[i]=1;
	}
}
int hashValue(int key)
{
	return key%SIZE;
}

int insert(int *hashtable, int key, int *empty)
{

    int j = 0, hk;
    hk = hashValue(key);
    while(j < SIZE) 
    {
        if(empty[hk] == 1)
        {
            hashtable[hk] = key;
            empty[hk] = 0;
            return (hk);
        }
        j++;
        hk = (hashValue(key)+5*j+ 11*j * j) % SIZE;
    }
    return (-1);
}

int search(int *hashtable, int key, int *empty)
{
	int max=0;
	int maxnf=0;
    int j = 0, hk;
    hk = hashValue(key);
    while((j < SIZE )|| (hashtable[hk]=NULL)) 
    {
        if((empty[hk] == 0) && (hashtable[hk] == key))
		{
			max=max+1;
			sumF=sumF+max;
			if(maxF<max)
			{
			maxF=max;
			}
			
			
			//printf("Max %d\n",sumF);
            return (hk);
		}
        j++;
		maxnf=maxnf+1;
		max=max+1;
		//sumF=sumF+1;
		sumNF=sumNF+1;
        hk = (hashValue(key)+5*j+ 11*j * j) % SIZE;
    }
	if(maxNF<maxnf)
	{
		maxNF=maxnf;
	}
	
    return (-1);
}
void insertRandom(int *hashTable,int *empty)
{
	insert(hashTable,rand()%MAX,empty);
}

void main()
{
	int counterNF=0;
	float compara=0.95;
	FILE *f=fopen("Incercare.csv","w");
	int m=3000;
	srand((unsigned int)time(NULL));
	int *empty=(int*)malloc(sizeof(int)*SIZE);
	float alfa=0.8;
	int counterF=0;
	maxF=0;
	maxNF=0;
	sumF=0;
	sumNF=0;
	while(alfa<1)
	{
		int *hashTable=(int*)malloc(sizeof(int)*SIZE);
		initialiseEmpty(empty);
		float beta=(float)(alfa*SIZE);
		int n=(int)(beta);
		printf("N %d\n",n);
			for(int i=0;i<=n;i++)
			{
				insertRandom(hashTable,empty);
			}
			for(int x=0;x<5;x++)
			{
				for(int j=0;j<m;j++)
				{
					int k=search(hashTable,rand()%MAX,empty);
					if(k!=(-1))
					{
						counterF=counterF+1;
						//printf("Counterul este la %d\n",counter);
					}
					else
					{
						counterNF=counterNF+1;
					}
				}
			}
			float x=sumNF/5.0;
			float y=counterNF/5.0;
			float z=x/y;
			fprintf(f,"%f,%f,%f,%d,%d,%d,%d\n",alfa,(sumF/5)/(counterF/5),z,maxF,maxNF,counterF/5,counterNF/5);
			counterF=0;
			counterNF=0;
			sumF=0;
			sumNF=0;
			maxF=0;
			maxNF=0;
		if((int)(alfa*100-95)==0)
		{
			printf("A intrat omule\n");
			alfa=0.99;
		}
		else
		{
			printf("Intra\n");
			alfa=alfa+0.05;
		}
		//printf("%f\n",alfa);
		free(hashTable);
	}
		fclose(f);
	
}