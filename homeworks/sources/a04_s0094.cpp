/*Student: ***ANONIM*** ***ANONIM*** Szonja
Group: ***GROUP_NUMBER***

OBJECTIVE:
	- implement correctly and efficiently an O(nlogk) method for merging k sorted sequences, where n 
	is the total number of elements.
	- use linked lists to represent the k sorted sequences and the output sequence

Interpretation of results:

	The measurements were conducted for 2 different cases, taking 5 measurements in each case
and representing on the graph the sum of operations for these 5 measurements (mean value of
the number of operations cannot be represented with the profiler)


**************** FIRST CASE ***************
	
	- choose, in turn, 3 constant values for k (k1=5, k2=10, k3=100); generate k random sorted lists for 
	each value of k so that the sum of elements in all the lists n varies between 100 and 10000, with an 
	increment of 100
	- generate a chart that represents the sum of assignments and comparisons done by the merging 
	algorithm for each value of k as a curve

GRAPH INTERPRETATION:
	- THE NUMBER OF OPERATIONS INCREASES LINEARLY FOR EACH OF THE THREE VALUES OF K (K1=5,K2=10,K3=100)
	- HOWEVER, THIS INCREASE IS MUCH FASTER AS THE VALUE OF K INCREASES
	- AS A RESULT NUMBER OF OPERATIONS FOR K2 INCREASES MORE RAPIDLY THAN FOR K1, AND IN TURN, NUMBER OF
	OPERATIONS FOR K3 INCREASES FASTER THAN BOTH FOR K2 AND K1
	- SINCE THE EFFICIENCY IS O(n*logk) THE INCREASE IS LINEAR, BECAUSE ONLY THE VALUE OF N CHANGES, 
	WHILE THE VALUE OF THE LOGARITHM IS CONSTANT FOR EACH OF THE THREE CASES, BECAUSE K IS CONSTANT
	
	
**************** SECOND CASE ***************
	
	- Set n = 10.000; the value of k must vary between 10 and 500 with an increment of 10; generate k 
	random sorted lists for each value of k so that the sum of elements in all the lists is 10000
	- test the merging algorithm for each value of k and generate a chart that represents the sum of 
	assignments and comparisons as a curve.

GRAPH INTERPRETATION:
	- THE NUMBER OF OPERATIONS INCREASES LOGARITHMICALLY AS THE VALUE OF K INCREASES
	- SINCE THE EFFICIENCY IS O(n*logk) THE INCREASE IS LOGARITHMIC, BECAUSE ONLY THE VALUE OF K CHANGES, 
	WHICH MAKES THE VALUE OF logk TO CHANGE, WHILE THE VALUE OF N IS CONSTANT FOR EACH OF THE VALUES OF K, 
	
****************CONCLUSIONS********************
 - THE RESULT IS ALWAYS A SORTED LIST
 - EACH OF THE LISTS HAS LENGTH N/K AND THE RESULT LIST HAS LENGTH N 
 - THE EFFICIENCY OF THE ALGORITHM IS O(n*logk)
 - THE NUMBER OF OPERATIONS INCREASES AS THE NUMBER OF LISTS (K) INCREASES, THIS IS DUE TO THE FACT
 THAT THE SIZE OF THE HEAP ALSO INCREASES
 
*/



#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
int n;
int k1,k;
#include "Profiler.h"
#define MAX_SIZE 10001

Profiler profiler("demo");

int v[MAX_SIZE];
//define an element of the list
typedef struct
node{                                
    int key;                     
	struct node  *next;              
}NODE;

NODE *first,*last,*firstfinal,*lastfinal;

//define a list which contains the initial k listst (their first elements) that have to be merged
NODE *LISTS[10000]; 

//define an element of the heap
typedef struct
	heapnode{
		int key;              
        int index; 
}HEAPNODE;

//define the heap
HEAPNODE H[10000];

void exchange(HEAPNODE *a,HEAPNODE *b)       
{ HEAPNODE aux;                    

 aux=(*a);
 (*a)=(*b);
 (*b)=aux;
}

//add a node into a list
void insertIntoList(NODE **first, NODE **last,int x)
{
	NODE *aux;
	aux=(NODE*)malloc(sizeof(NODE));                   
    aux->key=x;                                    
    aux->next=0; 

	//check if list is empty
	if(*first==0)                                
      { 
		  *first=aux;
          *last=aux;
        
      }
	//if not just add it to the end of list
   else 
	   {
		   (*last)->next=aux;
		   	*last=aux;
	}
}

//remove the node which has been placed into heap from its list
void remove(NODE **first)       
{
   NODE *aux; 
   if(*first!=0)
   {
	   aux=*first;               
       *first=(*first)->next;    
       free(aux);               
	                            
   }
  
}


int LEFT(int i)                    
{
  return 2*i;
}

int RIGHT(int i)                  
{
  return 2*i+1;
}

int PARENT(int i)                 
{
  return i/2;
}


//insertion of element in heap
void PushH(HEAPNODE *Heap,HEAPNODE x,int *dimH)   
{                                
  int i;

  
   (*dimH)=(*dimH)+1;                     
   Heap[*dimH]=x; 
   /*if(k1==5)
	 profiler.countOperation("opk1",n,1);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,1);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,1);*/
   profiler.countOperation("op",k,1);
     
   i=(*dimH);
   
 
   while((i>1)&&(Heap[PARENT(i)].key>Heap[i].key))           
   {
      //count three assignments from exchange
	   exchange(&Heap[PARENT(i)],&Heap[i]); 
	 /* if(k1==5)
	 profiler.countOperation("opk1",n,3);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,3);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,3);*/
   profiler.countOperation("op",k,3);
	   i=PARENT(i);
	   //count comparisons in while
	/*   if(k1==5)
	 profiler.countOperation("opk1",n,1);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,1);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,1);*/
	   profiler.countOperation("op",k,1);
      }
   //count one more comparison when exiting the while
  /* if(k1==5)
	 profiler.countOperation("opk1",n,1);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,1);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,1);*/

  profiler.countOperation("op",k,1);

}

void buildHeap(HEAPNODE *Heap,int i,int hsize) 
{  int l=LEFT(i);                           
   int r=RIGHT(i);                               
   int large;

   //count 2 comparisons
   
  /* if(k1==5)
	 profiler.countOperation("opk1",n,1);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,1);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,1);*/
 profiler.countOperation("op",k,1);
   if((l<=hsize)&&(Heap[l].key<Heap[i].key))
       large=l;
   else large=i;
	/*if(k1==5)
	 profiler.countOperation("opk1",n,1);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,1);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,1);*/
   profiler.countOperation("op",k,1);
   if((r<=hsize)&&(Heap[r].key<Heap[large].key))
	   large=r;
  
   if(large!=i)
   {    
	   //count 3 assignments
      exchange(&Heap[i],&Heap[large]); 
	/* if(k1==5)
	 profiler.countOperation("opk1",n,3);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,3);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,3);*/
  profiler.countOperation("op",k,3);
	  buildHeap(Heap,large,hsize);  
   }


}


//get an element from the heap
HEAPNODE PopH(HEAPNODE *Heap,int *dimH)   
{                                  
  HEAPNODE aux;
  aux=Heap[1];
  Heap[1]=Heap[*dimH];
/*  if(k1==5)
	 profiler.countOperation("opk1",n,2);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,2);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,2);*/
  profiler.countOperation("op",k,2);
  (*dimH)=(*dimH)-1; 
  buildHeap(H,1,*dimH);  
  return aux;

}


HEAPNODE get(NODE **NOD,int i)  
{                            
  HEAPNODE x;
  if(*NOD==0) 
  {
	  x.key=0;
	  x.index=-1;
  }
  else{
        x.key=(*NOD)->key;
		x.index=i;
		remove(NOD); 
/*	if(k1==5)
	 profiler.countOperation("opk1",n,2);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,2);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,2);*/
		profiler.countOperation("op",k,2);
  }
return x;
}


void mergeLists(int k,HEAPNODE *Heap)  
{
	int i;
    HEAPNODE x;
	int dimH=0; 


	firstfinal=0; 
    lastfinal=0;

  //build initial heap
    for(i=0;i<k;i++)
  {
	  x=get(&LISTS[i],i);   
      PushH(Heap,x,&dimH);
	 
 }
  
  
  
  while(dimH>0)
  {
     x=PopH(Heap,&dimH);
	 insertIntoList(&firstfinal,&lastfinal,x.key);
	/* if(k1==5)
	 profiler.countOperation("opk1",n,2);
	 else if (k1==10)
		 profiler.countOperation("opk2",n,2);
	 else if (k1==100)
		 profiler.countOperation("opk3",n,2);*/
	  profiler.countOperation("op",k,2);

	//get next element from list which provided the last inserted element into the final list
	 x=get(&LISTS[x.index],x.index);
	
	 if(x.index!=-1)  
		 PushH(Heap,x,&dimH);

	 }
 }







//generate the k lists out of the n elements
void generateLists(int k,int n) 
{
  int i,x,j,p;
 x=0;
 
 for(i=0;i<k;i++)
{
  first=0;
  last=0;
  x=0;
  FillRandomArray(v, n/k, 0, 32000, true, 1);
  p=0;
  for(j=0;j<(n/k);j++)
  { 
	  x=v[p];
	  p++;
	  insertIntoList(&first,&last,x);


  }
  LISTS[i]=first;
  
}
}
 void print(NODE *first)
{
   NODE *aux;
   if(first==0) printf("\n List is void \n");
   else
   { aux=first;

    while(aux!=0)
	{
		printf(" %d",aux->key);
		aux=aux->next;
   }
   }
}


void main(){
	
	//***************SMALL INPUT EXAMPLE***************//
	n=50;
	 k1=5;

	generateLists(k1,n);
	NODE *p;
	for (int k =0;k<k1;k++)
	{
	p=LISTS[k];
	print(p);
	printf("\n");
	}
	mergeLists(k1,H);

	
	for (int k =0;k<k1;k++)
	{
	p=LISTS[k];
	print(p);
	printf("\n");
	}
	printf("Merged lists:\n");
	p=firstfinal;
	print(p);
	printf("\n");

/********FIRST CASE************/
//k1=5; k2=10; k3=100;  n varies between 100->10.000 with an increment of 100
//5 measurements

/*for (int m=0;m<5;m++)
{
	for(n=100;n<=10000;n+=100)
{
	
	k1=5;
	generateLists(k1,n);
	mergeLists(k1,H);
	k1=10;
	generateLists(k1,n);
	mergeLists(k1,H);
	k1=100;
	generateLists(k1,n);
	mergeLists(k1,H);

}
}
	profiler.createGroup("operations_for_merging","opk1","opk2","opk3");
	profiler.showReport();
	*/

/********SECOND CASE************/
//n=10000 and k varies between 10->500 with an increment of 10
//5 measurements
/*	n=10000;
for (int m=0;m<5;m++)
{
	for(k=10;k<=500;k+=10)
	{
	generateLists(k,n);
	mergeLists(k,H);
	
	}
}
	profiler.createGroup("operations_for_merging","op");
	profiler.showReport();*/
	getch();

}