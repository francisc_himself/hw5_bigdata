/**
  From the reports I could conclude that in the average case Insertion Sort was  the fastest of the sorting algorithms which looks linear compared to the other algorithms, 
  Selection Sort and Bubble Sort which were quadratic .
  In the best case Bubble and Insertion sort did very well, both being linear. The assignment+comparison function of Selection sort was quadratic, much slower than the other algorithms. 
  After generating a new report in the best case for Insertion and Bubble sort it was clear that both algorithms were linear and Insertion sort was faster than Bubble sort.
  In the worst case Bubble sort was by far the slowest being quadratic. After regenerating a report in the worst case for Selection and Insertion sort I found out that insertion sort was the fastest of the two, both of them being linear.

  After measuring these algorithms I found that Insertion sort was the best one because it was faster than the other algorithms in the worst and average case.
*/


#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <time.h> 
#include <cstdlib>
#include "Profiler.h"

Profiler profiler("DirectSorting");




int Max_Size=100;


void insertionSort(int* array,int n){

	int buffer=0;
	for (int i=2;i<n;i++){
	buffer=array[i]; profiler.countOperation("AssignmentIS", n);profiler.countOperation("A+C IS", n);
	int j=i-1;
	while ((array[j]>buffer)&&(j>0))
	{
		array[j]=array[j-1]; profiler.countOperation("AssignmentIS", n); profiler.countOperation("ComparisonIS", n);profiler.countOperation("A+C IS", n,2);
		j=j-1;}

		array[j]=buffer; profiler.countOperation("AssignmentIS", n);profiler.countOperation("A+C IS", n);
	
	}
profiler.countOperation("ComparisonIS", n);profiler.countOperation("A+C IS", n);
	
}

void selectionSort(int* array,int n)
{
	int pos=0,aux=0;

	for (int j=0;j<n-1;j++)
	{
 
			pos=j;
 
			for (int i=j+1;i<n;i++)
		{
	
				if (array[i]<array[pos]) 
				{
					pos=i; 
				} 
				profiler.countOperation("ComparisonSS",n);profiler.countOperation("A+CSS", n);
		}
 
		if (j!=pos) 
		{
			array[aux]=array[pos];
			array[pos]=array[j];
			array[j]=array[aux];
			profiler.countOperation("AssignmentSS", n,3);profiler.countOperation("A+CSS", n,3);
		}

	}
}



void bubbleSort(int* array,int n){
 bool swapped=true;

int ind1=0;
int ind2=n-1;
int aux; 
while ((swapped) && (ind1<ind2)){

	swapped=false;
	
	for(int i=ind1;i<ind2;i++){
	if(array[i]>array[i+1]){ 
	 aux=array[i+1];
	 array[i+1]=array[i];
	 array[i]=aux;
	 swapped=true;
	profiler.countOperation("AssignmentBS", n,3);profiler.countOperation("A+C BS", n,3);
	
	}
	profiler.countOperation("ComparisonBS",n);profiler.countOperation("A+C BS", n);
}
ind2--;
for(int i=ind2;i>=ind1;i--){
	if(array[i+1]<array[i]){ 
	 aux=array[i+1];
	 array[i+1]=array[i];
	 array[i]=aux;
	 swapped=true;
	 profiler.countOperation("AssignmentBS", n,3);profiler.countOperation("A+C BS", n,3);
	}
	profiler.countOperation("ComparisonBS",n);profiler.countOperation("A+C BS", n);
}
ind1++;
 }

}






int main(void){
int a[10000],v[10000];

//Uncomment for Average case report generation
/*
for(int i=100;i<=10000;i+=100){


FillRandomArray(v, i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
selectionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
insertionSort(a,i);
bubbleSort(v,i);

FillRandomArray(v, i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
selectionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
insertionSort(a,i);
bubbleSort(v,i);

FillRandomArray(v, i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
selectionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
insertionSort(a,i);
bubbleSort(v,i);

FillRandomArray(v, i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
selectionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
insertionSort(a,i);
bubbleSort(v,i);

FillRandomArray(v, i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
selectionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
insertionSort(a,i);
bubbleSort(v,i);
	}

//profiler.createGroup("Average Case Assignment", "AssignmentBS", "AssignmentIS","AssignmentSS");
//profiler.createGroup("Average Case Comparison", "ComparisonBS", "ComparisonIS","ComparisonSS");
//profiler.createGroup("Average Case Assignment+Comparison", "A+C BS", "A+CSS","A+C IS");
*/

//Uncomment for Best case report generation
/*
for(int i=100;i<=10000;i+=100){
FillRandomArray(v,i,10,50000,1);

selectionSort(v,i);
insertionSort(v,i);
bubbleSort(v,i);
}
//profiler.createGroup("Best Case Assignment", "AssignmentIS","AssignmentSS","AssignmentBS");
//profiler.createGroup("Best Case Comparison", "ComparisonBS", "ComparisonIS","ComparisonSS");
//profiler.createGroup("Best Case Assignment+Comparison","A+C BS", "A+CSS","A+C IS");
//profiler.createGroup("Best Case Assignment+Comparison IS+BS","A+C BS","A+C IS");
*/

//Uncomment for worst case report generation

for(int i=100;i<=10000;i+=100){
FillRandomArray(v,i,10,50000,2);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
selectionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
insertionSort(a,i);
for(int j=0;j<=i;j++){
	a[j]=v[j];}
bubbleSort(a,i);
}
//profiler.createGroup("Worst Case Assignment", "AssignmentIS","AssignmentSS","AssignmentBS");
//profiler.createGroup("Worst Case Comparison", "ComparisonBS", "ComparisonIS","ComparisonSS");
//profiler.createGroup("Worst Case Assignment+Comparison","A+C BS", "A+CSS","A+C IS");
profiler.createGroup("Worst Case Assignment+Comparison SS IS", "A+CSS","A+C IS");




profiler.showReport();
	return 0;
}

