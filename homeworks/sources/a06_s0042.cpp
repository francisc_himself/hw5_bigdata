/*	Program eficient dar care contine multe "chichite" ceea ce duce la erori de compilare si rulare din cauza unei mici neatentii.
	Rularae este rapida chiar daca lucreaza cu arbori de 10000 de noduri pe care trebuie sa il parcurga de mai multe ori , dar tocmai din cauza implementari
structuri de arbore programul ruleaza rapid dat de eficienta operatiilor asupra arborelui , eficienta de O(lg n).Toate operatiile efectuate dau o
asemenea eficienta dar eficienta finala a programului este de O(nlg n) din cauza metodei josephus care contine un for n downto 1 ceea de eficienta finala
	Problema mai putea fi rezolvata folosind liste sau vectori lucru care ducea la scaderea dificultati doar ca se pierdeam la eficienta.
	Program greu dar care ofera o stare de bine atunci cand este finalizat cu succes.

	Student : ***ANONIM*** ***ANONIM*** - ***ANONIM***
	grupa : ***GROUP_NUMBER***
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

//definirea structuri unui nod din arbore
typedef struct nod
{
	int ***ANONIM***;
	int dim;
	nod *stg,*dr,*p;
}NOD;

//variabile pentru a numara asignarile si compararile
int comp,as;
//radacina arbore
NOD *r;
//variabila folosita pentru testarea pe un exemplu (7,3);
int test;

//construire arbore binar 
void construire(NOD *rad,int ***ANONIM***,int dim)
{
	NOD *q;
	q= (NOD*)malloc(sizeof(NOD));
	q->***ANONIM***=***ANONIM***;
	q->dim=dim;
	q->dr=NULL;
	q->stg=NULL;
	as+=4;
	comp++;
	//verificam daca radacina este nula , in caz de adevar se initializesaza radacina
	if(r==NULL) 
		{
			as++;
			r=q;
			r->p=NULL;
			r->dr=NULL;
			r->stg=NULL;
			return;
		}
	comp+=2;
	//verificare conditii pentru a stabili unde trebuie indrodus nodul in arbore
	if((q->***ANONIM***>rad->***ANONIM***) && (rad->dr==NULL))
		{
			as+=2;
			rad->dr=q;
			q->p=rad;
			return;
		}
	comp+=2;
	if((q->***ANONIM***<rad->***ANONIM***) && (rad->stg==NULL))
		{
			as+=2;
			rad->stg=q;
			q->p=rad;
			return;
		}
	comp++;
	if(q->***ANONIM***>rad->***ANONIM***) construire(rad->dr,***ANONIM***,dim);
	comp++;
	if(q->***ANONIM***<rad->***ANONIM***) construire(rad->stg,***ANONIM***,dim);
}

//metoda in care incepe construirea
void startconstruire(int i,int j)
{
	comp++;
	if(i>j) return;
	as++;
	int m=(i+j)/2;
	construire(r,m,j-i+1);
	startconstruire(m+1,j);
	startconstruire(i,m-1);
}
//final construire arbore binar

//OS_Select  realizat cu ajutorul cursurilor si a carti lui Corman
NOD* os_select(NOD *rad,int k){
	int r;
	comp++;
	if(rad->stg != NULL)
		{
			as++;
			r=rad->stg->dim+1;
		}
	else
		{
			as++;
			r=1;
		}
	comp++;
	if(k==r)
		{
			return rad;
		}
	else if(k<r)
		{
			return os_select(rad->stg,k);
		}
	else 
	{
			return os_select(rad->dr,k-r);
		}
}

//cautare minimului arborelului
NOD * minim(NOD *r)
{
	NOD *q;
	q=r;
	as++;
	comp++;
	while(q->stg!=NULL)
		{
		as++;
		q=q->stg;
		}
	return q;
}

//succesor
NOD* succesor(NOD *x)
{		
	comp++;
	if(x->dr!=NULL )
		return minim(x->dr);		
	as++;
	NOD *y=x->p;
	comp++;
	while((y!=NULL)&&(x==y->dr))
		{
			as+=2;
			x=y;
			y=y->p;
			comp++;
		}
	return y;	
}

//preety print folosit pentru testarea exemplului (7,3)
void pretty_print(NOD *rad, int d)
{
	if(rad!=NULL)
	{
		pretty_print(rad->dr,d+1);
		for(int i=0;i<d;i++)
		{
			printf("    ");	
		}
		printf("%d\n",rad->***ANONIM***);
		pretty_print(rad->stg, d+1);
	}
}

//functie folosita pentru a decrementata dimensiunea nodurilor din subarborele din care se sterge un anumit nod 
void decrementeaza(NOD *p)
{
	while(p!=NULL)
	{
		p->dim--;
		p=p->p;
	}
}

//sterge nod
void sterge_nod(NOD *z)
{
  NOD *y,*x;
  comp+=2;
  if ( (z->stg==NULL) || (z->dr==NULL) )
     {	  
		 y=z; 
		 as++;
     }
     else
	 {
		 y=succesor(z); 
		 as++; 
	 }
  
  comp++;
  if (y->stg!=NULL)
  { 
	  x=y->stg;
	  as++; 
  }
     else
  {	 
	  x=y->dr; 
	  as++; 
  }
  comp++;
  if (x!=NULL)
     {   
		 as++;
		 x->p=y->p;
     }
  decrementeaza(y->p);
  comp++;
  if (y->p==NULL)
	{
		r=x;
		as++;
	}
     else{
			comp++;
			if (y==y->p->stg)
			{ 
				y->p->stg=x; 
				as++;
			}
			else
			{ 
				y->p->dr=x; 
				as++; 
			}
	 }
  comp++;
  if (y!=z)
     {   as++;
		 z->***ANONIM***=y->***ANONIM***;
     }
  free(y);
}

//josephus
void josephus(int n,int m)
{	
	startconstruire(1,n);
	as+=2;
	int k,j;
	NOD *x;
	j=1;
	for(k=n;k>0;k--)
	{
		if(test==1)
			{
				pretty_print(r,0);
				printf("\n\n\n\n\n" );
				getch();
			}
		j=((j+m-2)%k)+1;
		x=os_select(r,j);
		sterge_nod(x);
	}
	if (test==1)
	{
		pretty_print(r,0);
	}
}

int main()
{
	//variabila test primeste ***ANONIM***oarea 1 pentru a fi afisat pe ecran rezultatul apelului josephus(7,3)
	test=1;
	josephus(7,3);
	//variabila test devine 0 pentru a rula programul fara intreruperi sau asteparea apasarii unei taste
	test=0;
	//declararea,deschiderea si scrierea in fisier
	FILE *f;
	f=fopen("josephus.csv","w");
	fprintf(f,"n,operatii\n");

	for(int i=100;i<10001;i+=100)
	{
		as=0;
		comp=0;
		josephus(i,i/2);
		fprintf(f,"%d,%d\n",i,as+comp);
	}
	fclose(f);
	//final program
}