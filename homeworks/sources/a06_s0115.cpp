#include "stdio.h"
#include "stdlib.h"
#include "conio.h"
#include "Profiler.h"

Profiler profiler("Josephus");

typedef struct Node                                                
{                                                               
      int key;
	  int size;
      Node *left;
	  Node *right;
	  Node *p;
	  
}NodeT; 

NodeT* root;

int i; int r; int n=100;


// Add introduces elements in the tree, acting as a constructor
NodeT* Add (int a, int b)
{
	if (a<=b)
	{
		NodeT* q;
		profiler.countOperation("Op", n, 1);
		q = (NodeT*)malloc(sizeof(NodeT));
		q->key= (a+b+1)/2; q->size= b-a+1;
		q->left=NULL;q->right=NULL;q->p=NULL;

		q->left = Add(a, ((a+b+1)/2-1));
		if (q->left != NULL)
			q->left->p=q;
		q->right = Add (((a+b+1)/2+1),b);
		if (q->right != NULL)
			q->right->p=q;
		return q;
	}
	return NULL;
}

// Display takes care of the printing process
void Display (NodeT* q, int lev)
{
	int j;
	if (q != NULL)
	{
		Display (q->right, lev+1);
		for (j=0; j<=lev; j++)
	//	 printf ("  ");
	//	printf ("%d(%d)\n", q->key, q->size);
		Display (q->left, lev+1);
	}
}

// OS_Select will mark the node containing the i-th smallest key in the subtree rooted at x
 NodeT* OS_Select (NodeT* x, int i)
{
	if (x->left != NULL)
	 r = x->left->size + 1;
	else 
	    r=1;
	     profiler.countOperation ("Op", n, 2);
	if (i==r)
       return x;
	else
	{
	   profiler.countOperation ("Op", n, 1);
		if (i<r)
	  return OS_Select (x->left, i);
	else return OS_Select (x->right, i-r);
	}
}
 
 // Changesize will ensure that every single subtree will have the appropriate size
  void ChangeSize (NodeT* temporary)
 {
	 while (temporary != NULL)
	 {
		 temporary->size = temporary->size - 1;
		 temporary = temporary->p;
		 profiler.countOperation ("Op", n, 1);
	 }
 }


 NodeT* Tree_Min (NodeT* x);
 NodeT* Successor (NodeT* x);

// Removes the selected node from the tree
 void OS_Delete (NodeT* z)
 {
	 NodeT* y;
	 NodeT* x;
	 if ((z->left == NULL) || (z->right == NULL))
	 {
		 profiler.countOperation ("Op", n, 2);
		 y = z;
	 }
	 else 
		 {
			 profiler.countOperation ("Op", n, 2);
			 y = Successor (z);
	     }
	 if (y->left != NULL)
	 {
		 profiler.countOperation ("Op", n, 2);
		 x = y->left;
	 }
	 else 
	     {
			 profiler.countOperation ("Op", n, 2);
			 x = y->right;
	     }
	 if (x != NULL)
	 {
		 profiler.countOperation ("Op", n, 2);
		 x->p = y->p;
	 }
	 if (y->p == NULL)
	 {
		 profiler.countOperation ("Op", n, 2);
		 root = x;
     }
	 else if (y == y->p->left)
	     {
			 profiler.countOperation ("Op", n, 2);
			 y->p->left = x;
	     }
	 else 
	     {
			 profiler.countOperation ("Op", n, 2);
			 y->p->right = x;
			 
	     }
	 ChangeSize (y->p);
 }

 // x's Successor
 NodeT* Successor (NodeT* x)
 {
	 NodeT* y;
	 profiler.countOperation ("Op", n, 1);
	 if (x->right != NULL)
	 {
		 return Tree_Min (x->right);
     }
	 profiler.countOperation ("Op", n, 1);
	 y = x->p;
	 while ((y != NULL) && (x == y->right))
	 {
		 x = y;
		 y = y->p;
		 return y;
	 }
 }

 // Leftmost leaf
NodeT* Tree_Min (NodeT* x)
{
	profiler.countOperation ("Op", n, 1);
	while (x->left != NULL)
	{
		profiler.countOperation ("Op", n, 1);
		x = x->left;
	}
	return x;
}

void Josephus (int n, int m)
{
  NodeT* x;
  int k;
  k=n;
  i=1;
  root = Add (1, n);

	while (k>0)
	  {
       i = ((i + m - 2) % k) + 1;
	   x = OS_Select (root, i);
	  // printf ("%d ", x->key);
	   OS_Delete(x);
	//   printf("\n");
	 /*  if (k>1)
		   Display (root, 0); */
	   k--;
      }
}


int main()
{
  //Josephus(14,7);
	n=100;
    for(int i = 1; i <= 100; i++)
    {
		Josephus(n,n/2);
		printf("%d\n",n);
        n = n + 100;
    }
	profiler.createGroup("Josephus","Op");
	profiler.showReport();
    return 0;
}
