#include <stdio.h>
#include <stdlib.h>
#include "conio.h"
#include <time.h>

/****ANONIM*** ***ANONIM***	
*gr***GROUP_NUMBER***
*Se cere parcurgerea in latime a grafului
*/


//FILE *f=fopen("parc_graf.txt","w");
int v[11][11];
int a[11],b[11],viz[11];

void creare(int j)	//Creeare arborelui
{
	for(int i=1;i<=j;i++)
	{
		for(int k=1;k<=10;k++)
		{
			if((v[b[i]][k]==1) && (viz[k]==0))	//daca exista drum intre 2 noduri si nu a fost vizitat
			{
				viz[k]=1;	//atunci se viziteaza
				a[k]=b[i];	//a[k] retine tatal nodului k
				b[j+1]=k;
				j++;
			}
		}
	}
}

void pp(int x,int j)	//Pretty Print(Nod, Adancime)
{
	int i;
	for (i=0;i<j;i++)
	{
		printf("  ");	//"_" x adancimea
	}
	printf("%d\n",x);	//nodul dupa "_"
	for (i=1;i<=10;i++)
	{
		if(a[i]==x)		//daca nodul curent este parinte pentru i
		{
			pp(i,j+1);	//Pretty Print(i, adancimea+1);
		}
	}
}

void main()
{
	srand((unsigned)time(NULL));
	for(int i=1;i<=10;i++)	//Initializeaza matricea de adiacenta cu 0
	{
		for(int j=1;j<=10;j++) v[i][j]=0;//rand()%2;
	}
	for(int i=1;i<=10;i++)
	{
		viz[i]=0;	//Initializeaza vectorul de noduri vizitate cu 0
	}
	
	//Introduce in matricea de adiacenta muchiile grafului, la fel ca si in exemplu
	v[1][2]=1;
	v[1][5]=1;
	v[1][6]=1;
	v[2][3]=1;
	v[2][5]=1;
	v[3][4]=1;
	v[5][4]=1;
	
	b[1]=1;
	a[1]=-1;
	viz[1]=1;
	
	creare(2);	//creeaza arborele de parcurgere
	
	for(int i=1;i<=10;i++)
	{
		printf("%d  ",a[i]);
	}
	
	printf("\n\n");
	pp(1,0);	//pretty print
	getch();
}

	