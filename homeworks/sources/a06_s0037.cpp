#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct NOD{
int ch,dim;
 NOD *s,*d,*p;
}NOD;

int a[1000],co,as;
NOD *root=NULL;
//~~~~dim
void dimensiune(NOD *p)
{
	while (p != NULL)
	{
		p->dim--;
		p=p->p;
	}
}
//~~~constr nod
NOD* build_nod(int x, int dim, NOD *stg, NOD *dr)
{
	NOD *q;
	q= (NOD*)malloc(sizeof(NOD));
	q->ch=x;
	q->s=stg;
	q->d=dr;
	q->dim=dim;
	as+=4;
	if (stg!=NULL)
	{
		stg->p = q;
	}
	if (dr!=NULL)
	{
		dr->p=q;
	}
	q->p=NULL;
	as++;
	return q;
}
//~~~~~~~~construire arbore~~~~~~
NOD* build_tree(int stg,int dr){ //insert mij
	NOD *sub_stg,*sub_dr;
	if (stg > dr)
		return NULL;
	if (stg == dr)
		return build_nod(stg, 1, NULL, NULL);
	else
	{
		int m =(stg + dr)/2;
		sub_stg = build_tree(stg, m-1);
		sub_dr  = build_tree(m+1, dr);
		return build_nod(m, dr-stg+1,sub_stg, sub_dr);
	}
} 
//~~~~~~~~~selectul~~~~~~
NOD* os_select(NOD *x,int k){ //cauta elementul de rang i--- appel OS_SELECT(root[T], i).
	int r;	
	//if(x->ch <0 ) return x; //!!
	if(x->s != NULL)
		r=x->s->dim+1;
	else
		r=1;

	 //pot sa fac decrementarea dimensiunii in selectie pt eficienta
	if(k==r){
		//printf("%d %d\n" ,x->ch,x->dim);
		//x->dim--;
		return x;}
	else if(k<r){
		 // x->dim--;
		  return os_select(x->s,k);}
	else {//x->dim--;
		  return os_select(x->d,k-r);}
}
//~~~~~~~~~min~~~~~~
NOD* tree_minimum(NOD* x){
		
		while(x->s!=NULL ){
			//x->dim--;//pot sa fac decrementarea dimensiunii in selectie pt eficienta
			x=x->s;
			
		}
		return x;
		
	}
//~~~~~~~~~succesor~~~~~~
NOD* tree_succesor(NOD *x){
		
		if(x->d!=NULL )
			return tree_minimum(x->d);
		
			NOD *y=x->p;
			while((y!=NULL)&&(x==y->d) ){
				x=y;
				y=y->p;
			}
			return y;
			
		
}

//~~~~~~~~~delete~~~~~~
NOD* tree_delete(NOD *z){
	NOD *y,*x;
	co++;
	if(z==root && z->s!=NULL && z->d==NULL) 
		{root=z->s;
		root->p=NULL;
		return z;}
	if(z==root && z->s==NULL && z->d!=NULL) 
		{root=z->d;
		root->p=NULL;
		return z;}

	if(z->s==NULL || z->d==NULL) {
		y=z; as++;}
	else y=tree_succesor(z);
	co++;
	if(y!=NULL)
		{x=y->s;as++;}
	else {x=y->d;as++;}
	co++;
	if(x!=NULL)
		{x->p=y->p;as++;}
	co++;
	if(y->p==NULL){
		{root=x;as++;}
	}
	else if(y==y->p->s)
		{y->p->s=x;as++;}
		else {y->p->d=x;as++;}
	co++;
	if(y!=z){
		z->ch=y->ch;
		//z->dim=y->dim;
		
	}
	return y;
}


//~~~~~~~~~print~~~~~~
void pretty_print(NOD *x, int d){
	if(x!=NULL){
		 pretty_print(x->d,d+1);
		for(int i=0;i<d;i++){
			printf("    ");	
		}
		printf("%d(%d)\n",x->ch,x->dim);
		 pretty_print(x->s, d+1);
	}
}
//~~~~~~~~~test~~~~~~
void test(){
	int k,n;

	printf("n=" ); scanf("%d",&n);
	for(int i=1;i<=n;i++)
		a[i]=i;

	
	root=build_tree(1,n); 

	pretty_print(root,0);
	printf("\n" );
	printf("k=" ); scanf("%d",&k);
	NOD *z=os_select(root,k);
	printf("\n" );
	tree_delete(z);

	getch();
	pretty_print(root,0);

}
//~~~~~~~~~josephus~~~~~~
void josephus(int n,int m){
	

	for(int i=1;i<=n;i++)
		a[i]=i;

	root=build_tree(1,n);
	int k=m;
	int i,aux=n;
	NOD *x,*y;
	for(i = 1; i < aux; i++)
	{	
		pretty_print(root,0);
		printf("\n\n\n" );
		getch();

		
		x=os_select(root,k);
		y=tree_delete(x);
		dimensiune(y);
		free(y);

		n--;
        k=(k+m-2)%n+1;
	}
	pretty_print(root,0);
}


//~~~~~~~~~main~~~~~~
int main(){
	/*int n,m;
	 FILE *pf;
	 pf=fopen("j.csv","w");
	 fprintf(pf,"n ,as ,comp , as+comp\n"); 

	for(int n=100;n<=10000;n=n+100)
	{	
		printf("%d \n",n);

		as=0;co=0;
		josephus(n,n/2);
		 fprintf(pf,"%d ,%d ,%d , %d\n",n,as,co,as+co); 

	}*/
	josephus(15,15/2);
	//test();
	getch();
return 0;
}