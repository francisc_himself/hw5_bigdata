#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"


typedef struct node
{
    int* data;
    struct node *next;
}NodeT;




Profiler profiler("Merge");


NodeT*  CreateList()
{
    NodeT * n = (NodeT*) malloc(sizeof(NodeT));
    if (n == NULL)
    {
        printf("CreateList error: Not enough memory for Node creation");
        return NULL;
    }
    n->data=NULL;
    n->next=NULL;

    return n;
}

int InsertValue(NodeT* L,int* data)
{
	
	NodeT* n=(NodeT*) malloc(sizeof(NodeT));
	n->data=(int*)malloc(4);
	memcpy(n->data, data,4);
	//(n->data)=&data;
    if (L == NULL )
    {
        printf("InsertAfter error: List or Node not found");
        return 0;
    }
	//printf(" Inserting %d \n",data);
    n->next=L->next;
    L->next=n;
    return 1;
}


void print(NodeT* L){
NodeT* n=(NodeT*) malloc(sizeof(NodeT));
printf("\n");
n=L->next;
while (n->next!=NULL ){
	printf("  %d ",*(n->data));
n=n->next;
}

}


int main(void){
	int a[20000];
	int check;
	int init=1000;
	
	NodeT* SubLists[501];
	int * data=&init;
	int k1=5, k2=10, k3=100;
	NodeT* Final=CreateList();
	int n=100;

	for ( n=100;n<=10000;n+=100){
	


//Creating k1 ordered sublists 

	for(int j=0;j<k1;j++){
	 SubLists[j]=CreateList();
	FillRandomArray(a,n/k1,1,100,false,1);
	for(int i=0;i<n/k1;i++){
		*data=a[i];	
		//printf(" Adding data %d to sublist %d \n",data,j);
	InsertValue(SubLists[j],data);
	profiler.countOperation("k=5", n,6);	
	}
	
	}





//Merging The Lists
	for(int j=0;j<n;j++)//going once for each element needed to be added to  final list

	{
       

			int max=0;
			int maxlist=0;

		for(int q=0;q<k1;q++){ // going through each sublist once
			



		if(SubLists[q]->next==NULL) // if the list is not empty
			{	
				profiler.countOperation("k=5", n);	

				//printf(" \n the list %d is empty \n",q);
			}
		else 
			{
				profiler.countOperation("k=5", n);	
				//printf(" comparing the %d s first element, which is %d to %d  \n",q,*(SubLists[q]->next->data),max);
				//printf(" %d ",*(SubLists[0]->next->data));
				if(*(SubLists[q]->next->data)>max)
					{
						profiler.countOperation("k=5", n);	
						max=*(SubLists[q]->next->data);
						maxlist=q;
					}
			profiler.countOperation("k=5", n);	
			}//finding the global minimum of the lists
		}
		
	NodeT* N=(NodeT*) malloc(sizeof(NodeT));//aux for moving node
	N=SubLists[maxlist]->next;
	SubLists[maxlist]->next=SubLists[maxlist]->next->next;
	N->next=Final->next;
	Final->next=N;
	profiler.countOperation("k=5", n,5);	


//printf("\n added %d to final \n",*(N->data));

	
	
	}





//Creating k2 ordered sublists 

	for(int j=0;j<k2;j++){
	 SubLists[j]=CreateList();
	FillRandomArray(a,n/k2,1,100,false,1);
	for(int i=0;i<n/k2;i++){
		*data=a[i];	
	InsertValue(SubLists[j],data);
	profiler.countOperation("k=10", n,6);	
	}
	
	}




//Merging The Lists
	for(int j=0;j<n;j++)//going once for each element needed to be added to  final list

	{
			int max=0;
			int maxlist=0;

		for(int q=0;q<k2;q++){ // going through each sublist once
		if(SubLists[q]->next==NULL) // if the list is not empty
			{	
				profiler.countOperation("k=10", n);	
			}
		else 
			{
				profiler.countOperation("k=10", n);	
				if(*(SubLists[q]->next->data)>max)
					{
						profiler.countOperation("k=10", n);	
						max=*(SubLists[q]->next->data);
						maxlist=q;
					}
			profiler.countOperation("k=10", n);	
			}//finding the global minimum of the lists
		}
		
	NodeT* N=(NodeT*) malloc(sizeof(NodeT));//aux for moving node
	N=SubLists[maxlist]->next;
	SubLists[maxlist]->next=SubLists[maxlist]->next->next;
	N->next=Final->next;
	Final->next=N;
	profiler.countOperation("k=10", n,5);	

	}



//Creating k3 ordered sublists 

	for(int j=0;j<k3;j++){
	 SubLists[j]=CreateList();
	FillRandomArray(a,n/k3,1,100,false,1);
	for(int i=0;i<n/k3;i++){
		*data=a[i];	
	InsertValue(SubLists[j],data);
	profiler.countOperation("k=100", n,6);	
	}
	
	}


//Merging The Lists
	for(int j=0;j<n;j++)//going once for each element needed to be added to  final list

	{
			int max=0;
			int maxlist=0;

		for(int q=0;q<k3;q++){ // going through each sublist once
		if(SubLists[q]->next==NULL) // if the list is not empty
			{	
				profiler.countOperation("k=100", n);	
			}
		else 
			{
				profiler.countOperation("k=100", n);	
				if(*(SubLists[q]->next->data)>max)
					{
						profiler.countOperation("k=100", n);	
						max=*(SubLists[q]->next->data);
						maxlist=q;
					}
			profiler.countOperation("k=100", n);	
			}//finding the global minimum of the lists
		}
		
	NodeT* N=(NodeT*) malloc(sizeof(NodeT));//aux for moving node
	N=SubLists[maxlist]->next;
	SubLists[maxlist]->next=SubLists[maxlist]->next->next;
	N->next=Final->next;
	Final->next=N;
	profiler.countOperation("k=100", n,5);	
	}

}



//Merging with variable k
n=10000;


for ( k2=10;k2<=500;k2+=10){

//Creating k2 ordered sublists 

	for(int j=0;j<k2;j++){
	 SubLists[j]=CreateList();
	FillRandomArray(a,n/k2,1,10000,false,1);
	for(int i=0;i<n/k2;i++){
		*data=a[i];	
	InsertValue(SubLists[j],data);
	profiler.countOperation("Variable k", k2,6);	
	}
	
	}



//Merging The Lists
	for(int j=0;j<n;j++)//going once for each element needed to be added to  final list

	{
			int max=0;
			int maxlist=0;

		for(int q=0;q<k2;q++){ // going through each sublist once
		if( SubLists[q]->next==NULL ) // if the list is not empty
			{	
				profiler.countOperation("Variable k", k2);	
			}
		else 
			{
				profiler.countOperation("Variable k", k2);	
				if(*(SubLists[q]->next->data)>max)
					{
						profiler.countOperation("Variable k", k2);	
						max=*(SubLists[q]->next->data);
						maxlist=q;
					}
			profiler.countOperation("Variable k", k2);	
			}//finding the global minimum of the lists
		}
		if(SubLists[maxlist]->next!=NULL){	
	NodeT* N=(NodeT*) malloc(sizeof(NodeT));//aux for moving node
	N=SubLists[maxlist]->next;
	SubLists[maxlist]->next=SubLists[maxlist]->next->next;
	N->next=Final->next;
	Final->next=N;
	profiler.countOperation("Variable k", k2,5);	
		}
	}

}



profiler.createGroup("All","k=5","k=10","k=100");

profiler.showReport();


//DEMO




n =20; k2=4;

	for(int j=0;j<k2;j++){
	 SubLists[j]=CreateList();
	FillRandomArray(a,n/k2,1,10000,false,1);
	for(int i=0;i<n/k2;i++){
		*data=a[i];	
	InsertValue(SubLists[j],data);	
	}
	
	}

printf(" \n  Sublist 1: \n");
print(SubLists[0]);
printf(" \n  Sublist 2: \n");
print(SubLists[1]);
printf("  \n Sublist 3: \n");
print(SubLists[2]);
printf(" \n  Sublist 4: \n");
print(SubLists[3]);
 Final=CreateList();

//Merging The Lists
	for(int j=0;j<n;j++)//going once for each element needed to be added to  final list

	{
			int max=0;
			int maxlist=0;

		for(int q=0;q<k2;q++){ // going through each sublist once
		if( SubLists[q]->next==NULL ) // if the list is empty
			{	
			
			}
		else 
			{
			
				if(*(SubLists[q]->next->data)>max)
					{
				
						
						max=*(SubLists[q]->next->data);
						maxlist=q;
					}
			
			}//finding the global minimum of the lists
		}
		

		
	NodeT* N=(NodeT*) malloc(sizeof(NodeT));//aux for moving node
	N=SubLists[maxlist]->next;
	SubLists[maxlist]->next=SubLists[maxlist]->next->next;
	N->next=Final->next;
	Final->next=N;
	
	
	}
	
printf(" \n Final List:  \n");
print(Final);



getch();
}
























 