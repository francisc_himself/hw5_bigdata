//***ANONIM*** ***ANONIM*** 
//grupa ***GROUP_NUMBER***

//Cazul mediu - dupa cum putem vedea din grafice, este evident ca algoritmul QuickSort este mai eficient decat algoritmul HeapSort,  
//eficienta algoritmilor: O(n log n) si pentru QS(variaza) cat si pentru HS(liniar). 
//Best Case-QS- eficienta acestui algoritm este O(n log n)
//Worst Case-QS- eficienta acestui algoritm este O(n^2)
//pseudocodul a fost luat din "Introduction to Algorithms, Second Edition"-T.Cormen.


#include "stdio.h"
#include "conio.h"
#include "stdlib.h"
#include "Profiler.h"

Profiler profiler("Profiler");

int lung, dim_heap, heap_sort[10001], quick_sort[10001];

int stanga(int i)
{
	return 2*i;
}
int dreapta(int i)
{
	return 2*i+1;
}


void maxHeapify(int heap[], int i)
{
	int st, dr, l, aux;
	st = stanga(i);
	dr = dreapta(i);
	profiler.countOperation("Operatii HeapSort",lung);
	if(st <= dim_heap && heap[st] > heap[i])
		l = st;
	else 
		l = i;
	profiler.countOperation("Operatii HeapSort",lung);
	if(dr <= dim_heap && heap[dr] > heap[l])
		l = dr;
	if(l != i)
	{
		profiler.countOperation("Operatii HeapSort",lung,3);
		aux = heap[i];
		heap[i] = heap [l];
		heap[l] = aux;
		maxHeapify(heap, l);
	}
}

void buildMaxHeapBU(int heap[])
{
	dim_heap = lung;
	for(int i = lung/2; i >= 1; i--)
		maxHeapify(heap, i);
}

void heapsort(int heap[])
{
	int aux;
	buildMaxHeapBU(heap);
	for(int i = lung; i >= 2; i--)
	{
		profiler.countOperation("Operatii HeapSort",lung,3);
		aux = heap[1];
		heap[1] = heap[i];
		heap[i] = aux;
		dim_heap --;
		maxHeapify(heap, 1);
	}
}

void copy(int heap[], int x[])
{
	for(int i=1; i <= lung; i++)
	{
		x[i] = heap[i];
		profiler.countOperation("Operatii HeapSort",lung);
	}

}
void afisare(int xy[])
{
	for(int i =1; i <= lung; i++)
		printf("%d ",xy[i]);
}

int partitie(int heap[], int p, int r)
{
	int x, i, aux;
	profiler.countOperation("Operatii QuickSort",lung);
	x = heap[r];
	i = p-1;
	for(int j = p; j <= r-1; j++)
	{
		profiler.countOperation("Operatii QuickSort",lung,1);
		if(heap[j] <= x)
		{
			++i;
			profiler.countOperation("Operatii QuickSort",lung,3);
			aux = heap[i];
			heap[i] = heap[j];
			heap[j] = aux;
		}
	}
	profiler.countOperation("Operatii QuickSort",lung,3);
	aux = heap[i+1];
	heap[i+1] = heap[r];
	heap[r] = aux;
	return i+1;
}

void quicksort(int heap[], int p, int r)
{
	int q;
	if(p < r)
	{
		q = partitie(heap,p,r);
		quicksort(heap, p, q-1);
		quicksort(heap, q+1, r);
	}
}

void averageCase()
{
	for(int i = 1; i <= 5; i++)
	{
		for(lung = 100; lung <= 10000; lung += 100)
		{
			FillRandomArray(heap_sort,lung);
			copy(heap_sort,quick_sort);
			heapsort(heap_sort);
			quicksort(quick_sort,1,lung);
			printf("\n mas : %d, n = %d",i, lung);
			
		}
	}
	profiler.createGroup("Heapsort si Quicksort----AverageCase","Operatii HeapSort","Operatii QuickSort");
	profiler.showReport();
}
void bestCasequick_sort()
{
	for(lung = 100; lung <= 10000; lung += 100)
	{
		FillRandomArray(quick_sort,lung+1);
		//afisare(quick_sort);
		printf(" n= %d\n",lung);
		quicksort(quick_sort,1,lung);
		//afisare(quick_sort);
	}
	profiler.createGroup("Best Quicksort","Operatii QuickSort");
	profiler.showReport();
}
void worstCasequick_sort()
{
	for(lung = 100; lung <= 10000; lung += 100)
	{
		FillRandomArray(quick_sort,lung+1,1,100000,true,1);
		//afisare(quick_sort);
		printf(" n= %d\n",lung);
		quicksort(quick_sort,1,lung);
		//afisare(quick_sort);
	}
	profiler.createGroup("Worst Quicksort","Operatii QuickSort");
	profiler.showReport();
}



void main()
{
	averageCase();
	//bestCasequick_sort();
	//worstCasequick_sort();
	lung = 7;
	FillRandomArray(heap_sort,lung+1,1,100);
	copy(heap_sort,quick_sort);
	printf("\nVector nesortat:\n");
	//afisare(heap_sort);
	//heapsort(heap_sort);
	printf("\nHeapsort:\n");
	afisare(heap_sort);
	printf("\nVector nesortat:\n");
	afisare(quick_sort);
	quicksort(quick_sort,1,lung);
	printf("\nQuicksort:\n");
	afisare(quick_sort);
}