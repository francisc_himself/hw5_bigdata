#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "Profiler.h"
#define NUMAR_VECINI_MAX 30000
#define NUMAR_NODURI_MAX 10000

typedef struct nod_graf{
                        int key;
                        char culoare;
                        struct nod_graf *v[NUMAR_VECINI_MAX];
                        int nr_vecini;
                        int d;
                        struct nod_graf *p;
                        int f;
                        }nod_graf;

typedef struct nod{
                    int key;
                    struct nod *urm;
                    }nod;
nod *prim;
nod_graf *n[NUMAR_NODURI_MAX];
int timp;
int edge[40000][2];
int atr, comp;
int ok=1;

void deq()
{
    nod *p;
    p = prim;
    prim = prim -> urm;
    free(p);
}

void enq(int key)
{
    nod  *p, *q;
    if(prim==NULL)
    {
        prim = (nod *)malloc(sizeof(nod));
        prim->key = key;
        prim->urm = NULL;
    }
    else{
        q = (nod*)malloc(sizeof(nod));
        q->key = key;
        p = prim;
        while(p->urm!=NULL)
            p = p -> urm;
        p -> urm =q;
        q->urm = NULL;
    }
}

void dfs_visit(int nod)
{
    nod_graf *q;
    int i;
	atr++;
    n[nod]->culoare = 'g';
    n[nod]->d = timp = timp+1;
    for(i=1; i<=n[nod]->nr_vecini; i++)
    {
		comp++;
        if(n[nod]->v[i]->culoare == 'a')
        {
			atr++;
            n[nod]->v[i]->p = n[nod];
			printf("%d -> %d: muchie de arbore\n",n[nod]->key, n[nod]->v[i]->key);
            dfs_visit(n[nod]->v[i]->key);
        }
		else{
			comp++;
			if(n[nod]->v[i]->culoare == 'g')
			{
				ok = 0;
				printf("%d -> %d: muchie inapoi\n", n[nod]->key, n[nod]->v[i]->key);
			}
			else
			{	
				comp++;
				if(n[nod]->v[i]->f < n[nod]->d)
					printf("%d -> %d: muchie transversala\n", n[nod]->key, n[nod]->v[i]->key);
				else
					printf("%d -> %d: muchie inainte\n", n[nod]->key, n[nod]->v[i]->key);
			}
		}
    }
	atr++;
    n[nod]->culoare = 'n';
    n[nod]->f = timp = timp+1;
    enq(n[nod]->key);
}

void dfs(int nr_noduri)
{
    int i;
    for(i=1; i<=nr_noduri; i++)
    {
		atr++;
        n[i]->culoare = 'a';
        n[i]->p = NULL;
    }
    timp = 0;
    for(i=1; i<=nr_noduri; i++)
    {
		comp++;
        if(n[i]->culoare == 'a')
            dfs_visit(n[i]->key);
    }
}

void exemplu (int a[], int b[], int nr_muchii,int nr_noduri)
{
    int i,j;
    for(i=1; i<=nr_noduri; i++)
    {
        n[i] = (nod_graf*)malloc(sizeof(nod_graf));
        n[i]->key = i;
        n[i]->nr_vecini = 0;
    }


        for(j=1; j<=nr_muchii; j++)
        {
                   n[a[j]]->nr_vecini ++;
                   n[a[j]]->v[n[a[j]]->nr_vecini] = n[b[j]];
        }

}

int combinari(int n)
{
    int p[10];
    int i,j,k=0;

   for(i=1; i<=2; i++)
        p[i]=i;
    edge[k][1] = p[1];
	edge[k][2] = p[2];
	k++;
    i=2;
    while(i>0)
    {
        p[i]++;
        if(p[i] > n-2+i){
            i--;

        }
        else
        {
            for(j=i+1; j<=2; j++)
                p[j] = 1;
            edge[k][1] = p[1];
			edge[k][2] = p[2];
			k++;
            i=2;
        }

    }

	return k;
}

void eliberare_spatiu()
{
	nod *p;
	p = prim->urm;
	while(p!=NULL)
	{
		deq();
		p = p->urm;
	}
}

void creare_graf(int nr_noduri,int nr_muchii)
{
    int i,j;
    int a[NUMAR_NODURI_MAX];
	int k = combinari(nr_noduri);
	FillRandomArray(a,nr_muchii+1,1,k,false,0);

     for(i=1; i<=nr_noduri; i++)
    {
        free(n[i]);
    }

    for(i=1; i<=nr_noduri; i++)
    {
        n[i] = (nod_graf* )malloc(sizeof(nod_graf));
        n[i]->key = i;
        n[i]->nr_vecini = 0;
    }

    for(j=1; j<=nr_muchii; j++)
        {
            n[edge[j][1]]->nr_vecini ++;
            n[edge[j][1]]->v[n[edge[j][1]]->nr_vecini] = n[edge[j][2]];
        }
}
int main()
{
    int a[8],b[8];
    int i,j;
    a[1]=1;
    a[2]=1;
    a[3]=2;
    a[4]=3;
    a[5]=3;
    a[6]=4;
    a[7]=5;
    a[8]=6;
    b[1]=2;
    b[2]=4;
    b[3]=5;
    b[4]=5;
    b[5]=6;
    b[6]=2;
    b[7]=4;
    b[8]=6;
    exemplu(a,b,8,6);
    for(i=1; i<=6; i++)
    {
        printf("%d: ",n[i]->key);
        for(j=1; j<=n[i]->nr_vecini; j++)
            printf("%d ", n[i]->v[j]->key);
        printf("\n");
    }
    dfs(6);
	if(ok==1){
		nod *p;
		p= prim;
		while(p!=NULL)
		{
			printf("%d ", p->key);
			p = p->urm;
		}
	}
	else{
		printf("fara sortare topologica");
	}
	eliberare_spatiu();
	getch();
	FILE *f;
	f = fopen("analiza.csv", "w");
	fprintf(f,"muchii,atr+comp\n");
	int e;
	for(e=1000; e<= 5000; e=e+100)
	{
		atr = 0; comp = 0;
		creare_graf(100,e);
		dfs(100);
		eliberare_spatiu();
		fprintf(f,"%d, %d\n", e, atr+comp);
	}
	fprintf(f,"\n");
	fprintf(f,"noduri, atr+comp\n");
	for(i=100; i<=200; i=i+10)
	{
		atr = 0; comp = 0;
		creare_graf(i,9000);
		dfs(i);
		eliberare_spatiu();
		fprintf(f, "%d, %d\n", i, atr+comp);
	}
	fclose(f);
	printf("succes");
	getch();
}
