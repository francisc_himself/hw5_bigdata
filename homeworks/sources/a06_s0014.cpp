
#include <stdio.h>
#include <conio.h>
#include <time.h>	
#include <stdlib.h>
#include <math.h>


struct nod{
	int key;
	int dim;	//numarul de noduri care sunt in subarbore.
	nod* st;
	nod* dr;
	nod* p; 	// parintele
};

int a=0, c=0;

//pretty print
void printP( nod* root, int indentare)
{
	if (root != NULL)
	{
		for (int j=0; j< indentare; j++)
			printf("\t");
		printf("%d : %d\n",(*root).key, (*root).dim);
		printP((*root).st, indentare+1);
		printP((*root).dr, indentare+1);
	}
}

//crearea arborele echilibrat
void creare_arb (nod **root, nod * parinte,int st, int dr)
{
	if (st <= dr)
	{
		int middle = (st+dr)/2;
		//insert (root, middle);
		*root = (nod *) malloc (sizeof (nod));
		(**root).key = middle;
		(**root).dim = dr-st+1;
		(**root).p = parinte;
        		creare_arb (&(**root).st, *root, st , middle-1);
		creare_arb (&(**root).dr,*root, middle+1,dr);
	}
	else
	{
            		*root= NULL;
	}
}

/*returneaza un pointer la nodul care contine 
al i-lea cel mai mic cheie din subarborele parcurs la x*/
nod* SO_selectie (nod *x, int i)
{
	int r;
                c++;
	if ((*x).st != NULL)
        		r = (*(*x).st).dim +1;
    	else r = 1;		// nod frunza
	if (i== r)

		return x;
	else
		if (i<r)
			return SO_selectie((*x).st, i);
		else
			return SO_selectie((*x).dr, i-r);
}

/*returneaza pointerul elementului ***ANONIM*** in subarborele parcurs 
pana la x*/
nod * tree_***ANONIM*** (nod* x)
{
	c++;
	while ((*x).st != NULL)
	{                               
		c++;
		x= (*x).st;
	}
	return x;
}

/*returneaza succesorul nodului x daca exista si NIL daca x are cea mai mare cheie din arbore*/
nod* tree_succ (nod * x)
{                                   
	c++;
	if ((*x).dr != NULL)
		return tree_***ANONIM***((*x).dr);
	struct nod* y = (*x).p;
	c++;		// compar doar pe (*y).dr,  insa y nu este din arbore, este doar un pointer
	while ( y!= NULL && x== (*y).dr)
	{                               
		c++;
		x=y;
		y=(*x).p;
	}
	return y;
}

nod* tree_delete(nod ** root, nod * z)
{
	struct nod * y,*x;
	c+= 2;
	if ((*z).st == NULL || (*z).dr == NULL)
	{
		y= z;
	}
	else y = tree_succ(z);
	// actualizez dimensiunea incepand cu tatal nodului ce va fi sters efectiv. ( y= z ||  succesor(z) )
	struct nod *aux = y;
	c++;
	while ((*aux).p != NULL)
	{                               
		c++;
		(*(*aux).p).dim --;         
		a++;
		aux = (*aux).p;
	}
	c++;
	if ((*y).st != NULL)
		x = (*y).st;
	else x =(*y).dr;
 	c++;
	if ( x!= NULL)
	{
		(*x).p = (*y).p;            
		a++;
	}
	c++;
	if ((*y).p == NULL)
	{
		*root = x;                  
		a++;
	}
	else
	{                               
		c++;
		if ( y == (*(*y).p).st)
		{
			(*(*y).p).st=x;     
			a++;
		}
		else 
		{
			(*(*y).p).dr=x;         
			a++;
		}
	}
	c++;
	if (y!=z)
	{
		(*z).key = (*y).key;        
		a++;
	}
	return y;
	free(y);
}

void josephus(nod ** root, int n, int m)
{
	creare_arb ( root, NULL, 1, n);
	printP(*root, 0);
	struct nod *aux;
	int i = m;                              
	c++;
	while ( *root!= NULL)			
	{
		c++;
		aux = SO_selectie(*root, i);
		printf("%d  ", (*aux).key);
		tree_delete(root, aux);
		n--;
		if (n!= 0)
			i= (i-2+m)%n+1;
	}
}

int main()
{
	struct nod * root;
	int n =7;
	FILE *f = fopen("operatii.csv","w");
	/*for (n= 100; n<10001; n+=100)
	{
		a=0;
		c=0;
		josephus(&root, n, n/2);
		fprintf(f,"%d\t%d\t%d\n", a,c,a+c);
	}*/
	/*creare_arb (&root, NULL,1,n);
	printP(root,0);
	tree_delete(&root, root);
	printf("%d \n", (*SO_selectie(root,3)).key);
	printP(root,0);*/

	josephus(&root, 7, 3);
	return 0;
}
