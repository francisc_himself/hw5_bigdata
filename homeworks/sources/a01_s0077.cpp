#include "Profiler.h"

#define MAX_SIZE 1000
#define MAX 10000

Profiler profiler("demo");

void bubbleSort(int v[], int n) { 
	bool swapped = true;
	int j = 0;
	int aux;

while (swapped) {
	swapped = false;
	j++;

     for (int i = 0; i < n - j; i++) {
		profiler.countOperation("bubblesort", n);
           if (v[i] > v[i + 1]) {  
			  profiler.countOperation("bubblesort", n); 
			  aux = v[i]; 
			  v[i] = v[i + 1];
	          v[i + 1] = aux; 
			  swapped = true;

                  }

            }

      }

}

void selectionSort(int v[], int n) {
	int i, j, min, aux;    
	for (i = 0; i < n - 1; i++) {
	 profiler.countOperation("Selection", n);
     min = i;
	
	 for (j = i + 1; j < n; j++)
			profiler.countOperation("Selection", n);
            if (v[j] < v[min])
					  profiler.countOperation("Selection", n);
                        min = j;

            if (min != i) {
				 aux = v[i]; 
				 v[i] = v[min];
				 v[min] = aux; 

            }

      }

}

void insertionSort(int v[], int n) { 
	int i, j, aux;
	for (i = 1; i < n; i++) {
		  profiler.countOperation("Insertion", n); 
            j = i;

    while (j > 0 && v[j - 1] > v[j]) {
		profiler.countOperation("Insertion", n);
        aux = v[j];
		v[j] = v[j - 1];
		v[j - 1] = aux;
        j--;

            }

      }

}

int main(void){

	int vect[MAX];
	for(int i=100;i<1000;i+=50){  
		FillRandomArray(vect,i,100,10000);
		
		bubbleSort(vect,i);
	}

	for(int i=100;i<1000;i+=50){ 
		FillRandomArray(vect,i,100,10000);
		
		selectionSort(vect,i);
	}

	for(int i=100;i<1000;i+=50){
		FillRandomArray(vect,i,100,10000);
		
		insertionSort(vect,i);
	}

	profiler.showReport();
	return 0;
}
