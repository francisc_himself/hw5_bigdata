// lab4(fa).cpp : Defines the entry point for the console application.
/*Student: ***ANONIM*** ***ANONIM***
Group: ***GROUP_NUMBER***
Assignment: Heapsort(average case) and quicksort(best, average and worst cases)
Interpretations:
	The average case was generated randomly, repeating algorithm 5 times and reporting an average. For building a heap, the buttom-up approach was used.
	
	Average case for heapsort and quicksort:
	Quicksort fares better than the other algorithm, though they both grow in the same order (O(nlgn)), though the curve is barely noticable for the heapsort.
	The spikes that appear on the quicksort graph are because of the randomness of choosing the pivot. It is interesting to note that because of this randomness
	there are cases in which quicksort gives less operations as n grows.
	
	All three cases for quicksort:
	From the charts it is easy to see that the average case is very close the best case performance of this algorithm, both running in O(nlgn). On the other
	hand, there is a clear discrepancy in performance when looking at the chart for worst case performance, as it runs in O(n^2) and is clearly distinguishable.
	
*/
#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int num=0;
int choose=1;
int heap_size=0;
int a[10001],b[10001];
int partition(int p, int r)
{
	int val;
	//alegere pivot
	if (choose==1)
	{
		int aux=rand()%(r-p)+p;
		int g=b[r];
		b[r]=b[aux];
		b[aux]=g;
		val=b[aux];
		num+=3;
	}
	else
		if (choose==3)
			val=b[p+1];
		else 
		{
			int aux=(r+p)/2;
			val=b[aux];
		}
	
	num++;
	int i=p-1;
	for(int j=p;j<=r-1;j++)
	{	
		num++;
		if (b[j]<=val)
		{
			i++;
			long aux=b[i];
			b[i]=b[j];
			b[j]=aux;
			num+=3;
		}
	}
	long aux2=b[i+1];
	b[i+1]=b[r];
	b[r]=aux2;
	num+=3;
	return i+1;
}
void quicksort(int p, int r)
{
	if (p<r)
	{
		int q=partition(p,r);
		quicksort(p,q-1);
		quicksort(q+1,r);
	}
}
int right(int i)
{
	return 2*i+1;
}
int left(int i)
{
	return 2*i;
}
void max_heapify(int i)
{
	int largest,l=left(i),r=right(i);
	if ((l<=heap_size)&&(a[l]>a[i]))
		largest=l;
	else largest=i;
	num++;
	if ((r<=heap_size)&&(a[r]>a[largest]))
		largest=r;
	num++;
	if (largest!=i)
	{
		int aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		max_heapify(largest);
		num+=3;
	}
}
void build_heap(int n)
{
	heap_size=n;
	for(int i=n/2;i>=1;i--)
		max_heapify(i);
}
void heapsort(int n)
{
	num=0;
	build_heap(n);
	for(int i=n;i>=2;i--)
	{
		int aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		heap_size--;
		num+=3;
		max_heapify(1);
	}
}
int main()
{
	int n;
	srand(time(NULL));
	FILE *f1,*f2;
	f1=fopen("average.csv","w");
	f2=fopen("quicksort.csv","w");
	fprintf(f2,"n,best,average,worst\n");
	fprintf(f1,"n,heapsort,quicksort\n");
	
	for(int n=100;n<=10000;n=n+100)
	{
		//Heapsort average and quicksort average
		choose=1;
		long sum, sum2;
		sum=0;
		sum2=0;
		for (int j=0;j<5;j++)
		{
			for(int i=1;i<=n;i++)
			{
				a[i]=rand()%10000;
				b[i]=a[i];
			}
			heap_size=0;
			heapsort(n);
			sum=sum+num;

			num=0;
			quicksort(1,n);
			sum2=sum2+num;
		}
		sum/=5;
		sum2/=5;
		fprintf(f1,"%d,%d,%d\n",n,sum,sum2);
	}
	
	for(int n=100;n<=10000;n=n+100)
	{

		for (int i=1;i<=n;i++)
		{
			b[i]=i;
		}
		//Quicksort-best
		num=0;
		choose=2;
		quicksort(1,n);
		fprintf(f2,"%d,%d,",n,num);
		
		//Quicksort average
		choose=1;
		int k=0;
		for (int j=0;j<5;j++)
		{
			num=0;
			for(int i=1;i<=n;i++)
			{
				b[i]=rand()%10000;
			}
			
			quicksort(1,n);
			k+=num;
		}
		k/=5;
		fprintf(f2,"%d,",k);
		
		for (int i=1;i<=n;i++)
		{
			b[i]=i;
		}
		//Quicksort - worst
		num=0;		
		choose=3;
		quicksort(1,n);
		fprintf(f2,"%d\n",num);
	}
	fclose(f2);	
	for (int i=1;i<11; i++)
		scanf("%d",&b[i]);
	quicksort(1,10);
	FILE *f3;
	f3=fopen("test.csv","w");

	for (int i=1; i<11; i++)
		fprintf(f3,"%d,",b[i]);
	fclose(f3);
	return 0;
}