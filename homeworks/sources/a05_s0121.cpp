/*
    Name: ***ANONIM*** ***ANONIM***
	Group: ***GROUP_NUMBER***
	
	Analysis of hash search :
	
	In general, the effort performed increases with the fill factor.
	The number of accesses and effort is greater when the elements are not found.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


int A[7]={3,6,12,5,1,2,55};
//int A[17];
int B[11];
#define m 11
#define N 10007
int H[N];
int apparition[10000];
int n; //8005 8505 9006 9506 9906
int generated[9906];
int accesses, maxeffort, tempeffort, notfound;

int hprime( int k)
{
   // return k % m;
	return k%N;
}


int h(int k, int i){
    int c1=1;
    int c2=1;

    int res=(hprime(k) + c1*i +c2*i*i) % N;
    return res;
}

int hashInsert( int T[], int k)
{
	
    int j;
    int i=0;
    do
    {
        j=h(k,i);
        if (T[j]==NULL)
        {
            T[j]=k;
            return j;
        }
        else i=i+1;
    } while (i!=N);
    printf("Hash table overflow");
    return 0;
}

int hashSearch( int T[], int k)
{
	tempeffort=0;
    int j;
    int i=0;
    do
    {
        j=h(k,i);
		accesses++;
		tempeffort++;
		if(tempeffort>maxeffort)
		{
			maxeffort=tempeffort;
		}
        if (T[j]==k)
            return j;
        i=i+1;
		notfound++;

    }while((T[j]!=NULL) && (i!=N));
    return -1;
}


void main()

{
	srand(time(NULL));
	maxeffort=0;
	/*
    for (int i=0; i<7; i++)
    {
        hashInsert(B,A[i]);
    }

    for (int i=0; i<11; i++)
    {
        if (B[i]!=NULL)

        printf("%d - %d\n",i,
        B[i]);
    }
    printf("\n");
    printf("\n");

    printf("%d\n" ,hashSearch(B,12));
    printf("%d\n" ,hashSearch(B,3));
    printf("%d\n" ,hashSearch(B,55));
    printf("%d\n" ,hashSearch(B,4));


	*/


	//8005 8505 9006 9506 9906


		

	
accesses=0;
	maxeffort=0;
	notfound=0;


	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=8005;


	printf("\n0.8 filling factor\n");

	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}
	
	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8005]);

	}
	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);
	accesses=0;
	maxeffort=0;
	notfound=0;

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8005]+10001);
		

	}

	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);

	for(int i=0; i<10007; i++)
		H[i]=NULL;

	accesses=0;
	maxeffort=0;
	notfound=0; 
	n=8505;
	printf("\n0.85 filling factor\n");
	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8505]);

	}
	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);
	accesses=0;
	maxeffort=0;
	notfound=0;

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8505]+10001);
		

	}

	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);

	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=9006;

	accesses=0;
	maxeffort=0;
	notfound=0;
	printf("\n0.9 filling factor\n");

	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9006]);

	}
	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);
	accesses=0;
	maxeffort=0;
	notfound=0;

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9006]+10001);
		

	}

	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);


	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=9506;

	accesses=0;
	maxeffort=0;
	notfound=0;
	printf("\n0.95 filling factor\n");

	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9506]);

	}
	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);
	accesses=0;
	maxeffort=0;
	notfound=0;

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9506]+10001);
		

	}

	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);

	accesses=0;
	maxeffort=0;
	notfound=0;


	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=9906;
	printf("\n0.99 filling factor\n");
	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9906]);

	}
	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);

	accesses=0;
	maxeffort=0;
	notfound=0;

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9905]+10001);
		

	}

	printf("%d ", accesses/1500);
	printf("%d ", maxeffort);
}