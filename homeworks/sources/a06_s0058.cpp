#include<stdio.h>
#include<malloc.h>



typedef struct nod{
            int inf;
            struct nod *stg, *dr;
			int fii;
}arbore;

int calcFii(arbore *rad)
{
	if(rad==NULL)
	{
		return 0;
	} else
	if((rad-> stg==NULL) && (rad->dr==NULL))
	{
		rad->fii=1;
		return 1;

	}else 
	{
		rad->fii=calcFii(rad->stg)+ calcFii(rad->dr) +1;
		return rad->fii;
	}
}

 arbore *insert(arbore *rad, int key){

if (rad == NULL){
    arbore *p;
    p = (arbore *)malloc(sizeof(arbore));
    p->inf = key;
   

    p->stg = NULL;
    p->dr = NULL;
    return p;
}else{  
        if(rad->inf > key){
       
            rad->stg = insert(rad->stg, key);
        }else{
            
            if (rad->inf < key){
                rad->dr = insert(rad->dr, key);
            }
        }
    }
return rad;
}

void inordine(arbore* p){

    if (p == NULL){
        printf("#");
    }else{inordine(p->stg);
        printf("%d",p->inf);
        
        inordine(p->dr);
    }
}

arbore *sterge(arbore *rad,int key)
{
arbore *p,*tata_p;
arbore *nod_inlocuire,*tata_nod_inlocuire;
  int directie; 
  if(rad==0) return 0; 
  p=rad; tata_p=0;
 
  while((p!=0)&&(p->inf!=key))
	  {
		 if (key<p->inf){    
					 tata_p=p;
					  p=p->stg;
					  directie=1;
		}
		   else {    
			    tata_p=p;
			    p=p->dr;
			    directie=2;
			}
	     }
     if(p==0){
		  printf("\n NU EXISTA NOD CU CHEIA=%d\n",key);
		  return rad;
	          }
      
      if(p->stg==0) nod_inlocuire=p->dr; 
else if(p->dr==0) nod_inlocuire=p->stg;	
	    else {  
		   tata_nod_inlocuire=p;
		   nod_inlocuire=p->dr;       
		   while(nod_inlocuire->stg!=0)
		        {
			tata_nod_inlocuire=nod_inlocuire;
			nod_inlocuire=nod_inlocuire->stg;
		        }
		    if(tata_nod_inlocuire!=p)
			{
			    tata_nod_inlocuire->stg=nod_inlocuire->dr;
			    nod_inlocuire->dr=p->dr;
			 }
		     nod_inlocuire->stg=p->stg;
		  }
        free(p);
        printf("\nNodul de cheie=%d a fost sters!\n",key);
       if(tata_p==0)  return nod_inlocuire; 
       else {
	        if (directie==1) tata_p->stg=nod_inlocuire;
	        else tata_p->dr=nod_inlocuire;
	        return rad;
               }
	   
}


void preordine(arbore *p, int nivel)
{ 
  int i;
  if (p!=0){
		 for(i=0;i<=nivel;i++) printf("  ");
		 printf("%2d\n",p->inf);
		 preordine(p->stg,nivel+1);
		 preordine(p->dr,nivel+1);
	       }
}
void inordine(arbore *p, int nivel)
{ 
  int i;
  if (p!=0){
		 inordine(p->stg,nivel+1);
		 for(i=0;i<=nivel;i++) printf("  ");
		 printf("%d--%d\n",p->inf,p->fii);
		 inordine(p->dr,nivel+1);
	       }
}
void postordine(arbore *p, int nivel)
{ 
  int i;
  if (p!=0){
		 postordine(p->stg,nivel+1);
		 postordine(p->dr,nivel+1);
		 for(i=0;i<=nivel;i++) printf("  ");
		 printf("%2d\n",p->inf);
	       }
}


void insertElementeRec(int *v, int st, int dr, arbore *rad)
{
	if(st<=dr)
	{
		int m=(st+dr)/2;
		rad=insert(rad,v[m]);
		insertElementeRec(v, st,m-1,rad);
		insertElementeRec(v, m+1,dr,rad);
	}
}

arbore *insertElemente(arbore *rad,int *v, int n)
{
	int st=0;
	int dr=n-1;
	
	if(st<=dr)
	{
		int m=(st+dr)/2;
		rad=insert(rad,v[m]);
		insertElementeRec(v, st,m-1,rad);
		insertElementeRec(v, m+1,dr,rad);
	}
	return rad;
}

int nrFii(arbore *rad)
{
	if(rad==NULL) return 0;
	else return rad->fii;
}
int osSelect(arbore *rad, int x)
{
	
	if(x<nrFii(rad->stg))
	{
		return osSelect(rad->stg,x);
	}else if (x==nrFii(rad->stg))
	{
		return rad->inf;
	}
	else return osSelect(rad->dr,x-nrFii(rad->stg)-1);
		


}
int* josephus(int *v, int n,int x)
{
	x--;
	int nr=0;
	arbore *rad=NULL;
	rad=insertElemente(rad,v,n);
	calcFii(rad);
	int *t;
	t=(int *)malloc(n*sizeof(int));
	for(int i=0;i<n;i++)
	{
		nr+=x;
		nr=nr%nrFii(rad);
		t[i]=osSelect(rad,nr);
		
		rad=sterge(rad,t[i]);
		calcFii(rad);
	}

	return t;
}
int main()

{
	/*arbore *rad = NULL;
	rad = insert(rad,10);
	rad = insert(rad,2);
	rad = insert(rad,3);
	rad = insert(rad,1);
	rad = insert(rad,15);
	rad = insert(rad,13);
	rad = insert(rad,20);
	calcFii(rad);
	inordine(rad);

	rad=sterge(rad,20);
	calcFii(rad);
	inordine(rad);
	
	   
     printf("\nVIZITAREA IN INORDINE\n");
     inordine(rad,0);
     
     rad=sterge(rad,10);
	 calcFii(rad);
     printf("\nVIZITAREA IN INORDINE\n");
     inordine(rad,0);
	 */
	arbore *rad=NULL;
	int test[]={1,2,3,4,5,6,7,8,9};
	int x=23;
	int *test2=josephus(test,sizeof(test)/sizeof(int),x);
	for(int i=0; i<sizeof(test)/sizeof(int);i++)
	{
		printf("%d ",test2[i]);
	}

	/*x=osSelect(rad,0);
	x=osSelect(rad,1);
	x=osSelect(rad,100);
	x=osSelect(rad,3);
	x=osSelect(rad,4);
	x=osSelect(rad,71);
	x=osSelect(rad,6);
	x=osSelect(rad,13);
	/*/
    return 0;
}