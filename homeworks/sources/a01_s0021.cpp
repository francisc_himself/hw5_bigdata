#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "time.h"
//#include "math.h"
#define MAX 10000
/*
***ANONIM*** ***ANONIM***cu ***ANONIM***, gr ***GROUP_NUMBER***

In cazul favorabil, pentru metoda bubble, numele de asignari este 0. Numarul
de comparatii este aproximativ egal pentru toate cele 3 metode. Diferenta de
asignari in cazul insertiei si a selectiei este mica.

In cazul defavorabil numarul de comparatii dintre bubble si selectie este aproximativ egal,
fiind foarte mare, iar pentru insertie numaru lde comparatii este mic. In cazul atribuirilor,
la metoda bubble se realizeaza cele mai multe atribuiri, iar la selectie se realizeaza mai putine
atribuiri, iar pe ultimul loc din punct de vedere al numarului de atribuiri este insertia.

Pentru cazul mediu statistic numarul de comparatii este cel mai mare pentru bubble,fiind urmat
de selectie si apoi insertie.Si in cazul atribuirilor metoda bubble are cele mai multe atribuiri,
fiind la fel urmata de selectie si apoi insertie.
*/
long cmp, asig;
void bubble(int a[],long n)
{
	
	int ok = 0;
	int x,i,k=0;
	cmp=0;
	asig=0;
	while(!ok)
	{
		ok = 1;
		for(i=0; i<n-k-1; i++)
		{
			cmp=cmp+1;
		if(a[i]>a[i+1])
		{
			asig=asig+3;	
			x=a[i];
			a[i]=a[i+1];
			a[i+1]=x;
			ok = 0;		
		}
	
		}
		k++;
		
	}
}


void selectie(int a[],long n)
{
	
	int im,i,aux,j;
	cmp=0;
	asig=0;
	for(i=0;i<n-1;i++)
	{
		im=i;
		for(j=i+1;j<n;j++)
		{   cmp=cmp+1;
			if(a[j]<a[im]) 
			
				
			{  im=j;}
			
		}
		asig=asig+3;
		aux=a[im];
		a[im]=a[i];
		a[i]=aux;
		
	}

}

void insertie(int a[],long n)
{
	int i, j, k, x;
	cmp=0;
	asig=0;
	for(i=1; i<n; i++)
	{   asig=asig+1;
		x=a[i];
		j=0;
		cmp=cmp+1;
		while(a[j]<x)
			{   cmp=cmp+1;
				j++;
		}
		
		for(k=i; k>=j; k--)
		{   asig=asig+1;
			a[k]=a[k-1];
		}
		a[j]=x;
		asig=asig+1;
		
	}

}
//sirul generat aleator
void genereaza_rand(int v[],long n,int param)
{
	int i;
	srand(param);
	for(i=0;i<n;i++)
		v[i]=rand()%10000;
}

//sirul generat crescator
void genereaza_crescator(int v[],long n)
{
	int i;
	for(i=0;i<n;i++)
		v[i]=i+1;
}

//sirul generat descrescator
void genereaza_descrescator(int v[],long n)
{
	int i;
	for(i=0;i<n;i++)
		v[i]=n-i;
}

int main()
{
	/*int a[]={9,5 ,4,1,3,7}; int n=6; int i;
	printf("Sortare sir prin bubble:\n");  //in aceasta etapa am testat corectitudinea algoritmilor de sortare
	bubble(a,n);
	for(i=0; i<n; i++)
		printf("%d ",a[i]);

	printf("\nSortare sir prin selectie:\n");
	selectie(a,n);
	for(i=0; i<n; i++)
		printf("%d ",a[i]); 

	printf("\nSortare sir prin insertie:\n");
	insertie(a,n); 
	for(i=0; i<n; i++)
		printf("%d ",a[i]);*/
	int sir[MAX];
	FILE *best, *worst, *med;
	int i;
	long n,asig2,cmp2;
	long medasig,medcomp;
	
	best=fopen("favorabil.csv","w");
	worst=fopen("defavorabil.csv","w");
	med=fopen("mediu.csv","w");
	
	fprintf(best,"n,CompBubble,AtrBubble,SumBubble,CompSel,AtrSel,SumaSel,CompIns,AtrIns,SumaIns\n");
	fprintf(worst,"n,CompBubble,AtrBubble,SumBubble,CompSel,AtrSel,SumaSel,CompIns,AtrIns,SumaIns\n");
	fprintf(med,"n,CompBubble,AtrBubble,SumBubble,CompSel,AtrSel,SumaSel,CompIns,AtrIns,SumaIns\n");
	

	for(n=100;n<=MAX;n+=100)
	{   //cazul favorabil
	    genereaza_crescator(sir,n);
		bubble(sir,n);
		fprintf(best,"%ld,%ld,%ld,%ld,",n,cmp,asig,cmp+asig);
		
		genereaza_crescator(sir,n);
		selectie(sir,n);
		fprintf(best,"%ld,%ld,%ld,",cmp,asig,cmp+asig);

		genereaza_crescator(sir,n);
		insertie(sir,n);
		fprintf(best,"%ld,%ld,%ld\n",cmp,asig,cmp+asig);

		//cazul defavorabil
		genereaza_descrescator(sir,n);
		bubble(sir,n);
		fprintf(worst,"%ld,%ld,%ld,%ld,",n,cmp,asig,cmp+asig);
		
		//genereaza_descrescator(sir,n);
		selectie(sir,n);
		fprintf(worst,"%ld,%ld,%ld,",cmp,asig,cmp+asig);
		
		//genereaza_descrescator(sir,n);
		insertie(sir,n);
		fprintf(worst,"%ld,%ld,%ld\n",cmp,asig,cmp+asig);
		// cazul mediu statistic
		asig2=0;
		cmp2=0;
		for(i=0;i<5;i++)
		{
			genereaza_rand(sir,n,i);
			bubble(sir,n);
			asig2+=asig;
			cmp2+=cmp;
		}
		medasig=asig2/5;
		medcomp=cmp2/5;
		fprintf(med,"%ld,%ld,%ld,%ld,",n,medcomp,medasig,medcomp+medasig);

		asig2=0; cmp2=0;
		for(i=0;i<5;i++)
		{
			genereaza_rand(sir,n,i);
			selectie(sir,n);
			asig2+=asig;
			cmp2+=cmp;
		}
		medasig=asig2/5;
		medcomp=cmp2/5;
		fprintf(med,"%ld,%ld,%ld,",medcomp,medasig,medcomp+medasig);

		asig2=0; cmp2=0;
		for(i=0;i<5;i++)
		{
			genereaza_rand(sir,n,i);
			insertie(sir,n);
			asig2+=asig;
			cmp2+=cmp;
		}
		medasig=asig2/5;
		medcomp=cmp2/5;
		fprintf(med,"%ld,%ld,%ld\n",medcomp,medasig,medcomp+medasig);
	}
	


	fclose(best);
	fclose(worst);
	fclose(med);
	return 0;
}