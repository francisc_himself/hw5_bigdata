#include <stdio.h>
#include<time.h>
#include <stdlib.h> 
#include<string.h>
#include "Profiler.h"
#define MAX 10002


typedef struct _nod
{
	int v;
	struct _nod *next;
} NOD;

NOD *h[MAX];
NOD *t[MAX];
int n,k;
int heap[MAX];
int nr=0;
int rezultat[1000000];
int nv,dbn=0;

//Profiler p("lab6");
		//v[i]=rand()%x;
		//p.countOperation("heaps",n,3);

void initializareListe(int x)
{
	int i;
	for(i=1;i<=x;i++){h[i]=NULL;t[i]=NULL;}
	
}

void adauga(int x)
{
	
	int temp=rand()%50;
	if(t[x]!=NULL)temp+=t[x]->v;
	NOD *nou;
	nou = (NOD *)malloc(sizeof(NOD));
	nou->v=temp;
	nou->next=NULL;
	if(t[x]!=NULL)t[x]->next=nou;
	t[x]=nou;
	

}

void afisare()
{
	int i;
	NOD *hh;
	
	for(i=1;i<=k;i++)
	{
		hh=h[i];
		while(hh!=NULL)
		{
			printf("%d ",hh->v);
			hh=hh->next;
		}
		printf("\n");
	}

}

void creareLista(int k,int nn)
{
	if(nn>0)
	{
		adauga(k);
		if(nn==n){h[k]=t[k];dbn++;}
		creareLista(k,nn-1);
		//p.countOperation("liste",nv,7);
		dbn+=7;
	}
}



void is(int &a,int &b)
{
	int temp;
	temp=a;
	a=b;
	b=temp;
}

void percolate(int n,int k)
{
	int key=h[heap[k]]->v;
	int kk=k;
	while((k>1)&&(key<h[heap[k/2]]->v))
	{
		heap[k]=heap[k/2];
		k=k/2;
		//p.countOperation("liste",nv,3);
		dbn+=3;
	}
	heap[k]=kk;
	//p.countOperation("liste",nv,4);
	dbn+=4;
	
}

void sift(int n,int k)
{
	int son;

	do
	{
		son=0;
		if(2*k<=n)
		{
			son=2*k;
			if(((2*k+1)<=n)&&(h[heap[2*k+1]]->v<h[heap[2*k]]->v))
			{
				son=2*k+1;
				//p.countOperation("liste",nv,1);
				dbn++;
			}
				
			if(h[heap[son]]->v>=h[heap[k]]->v)
			{
				son=0;
				//p.countOperation("liste",nv,1);
				dbn++;
			}
			//p.countOperation("liste",nv,3);
			dbn+=3;
			
		}

		
		if(son!=0)
		{
			is(heap[son],heap[k]);
			k=son;
			//p.countOperation("liste",nv,4);
			dbn+=4;
		}
		//p.countOperation("liste",nv,3);
		dbn+=3;


	}
	while(son!=0);
	
}


void creareHeapuri()
{
	int i,j;
	
	
		for(j=1;j<=k;j++)
		{
			heap[j]=j;
			dbn+=2;//p.countOperation("liste",nv,2);
			percolate(j,j);
		}
		
		while(k>0)
		{
			rezultat[++nr]=h[heap[1]]->v;

			if(h[heap[1]]->next!=NULL)
			{
				h[heap[1]]=h[heap[1]]->next;
				//p.countOperation("liste",nv,1);
				dbn++;
				sift(k,1);
			}
			else
			{
				heap[1]=heap[k];
				k--;
				//p.countOperation("liste",nv,2);
				dbn+=2;
				sift(k,1);

			}
			//p.countOperation("liste",nv,2);
			dbn+=2;

		}
		
	
	
}

void afisareRez()
{
	int i;
	for(i=1;i<=n*k;i++)
		printf("%d ",rezultat[i]);
}

int main()
{

	freopen("rezultat.csv","w",stdout);
	
	srand(time(NULL));

	int i,kk,z;
	
	//printf("Introduceti n (lungimea listei):");
	//scanf("%d",&n);
	//printf("Introduceti k (numarul listelor):");
	//scanf("%d",&k);

	printf("k,n\n");
	for(z=1;z<=3;z++)
	{
		if(z==1){k=5;}
		else
			if(z==2){k=10;}
			else{k=100;}
			for(n=100;n<=1000;n+=100)
			{
	nv=n;
	n=n/k;
	kk=k;
	
	initializareListe(k);

	for(i=1;i<=k;i++)
	{
		creareLista(i,n);
	}

	//afisare();

	creareHeapuri();
	k=kk;
	//printf("\n");
	//afisareRez();

	//printf("\n");

	//p.createGroup("heaps_quicks","heaps","quicks");
	//p.showReport();
	printf("%d,%d\n",k,dbn);

			}}
	
	return 0;
}

