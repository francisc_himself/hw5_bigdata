#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct treeNode
{
    int key;
    int size;
	struct treeNode *left;
	struct treeNode *right;
	struct treeNode *parent;
}Node;

Node* create(int x, int size, Node *l, Node *r)
{
	Node *q;
	q = (Node *) malloc (sizeof(Node));
	q->key=x;

	q->left=l;
	q->right=r;

	q->size=size;

	if(l!=NULL)
		l->parent=q;
	if(r!=NULL)
		r->parent=q;
	q->parent=NULL;
	return q;
}

Node* BuildTree(int l, int r)
{
	Node *p, *q;
	if(l>r)
	{
        return NULL;
	}
	if(l==r)
	{
        return create (l,1,NULL,NULL);
	}
	else
	{
		int mid=(l+r)/2;
		p = BuildTree(l,mid-1);
		q = BuildTree(mid+1,r);
		return create(mid,r-l+1,p,q);
	}
}

void print(Node *p, int level)
{
    int i;
	if(p!=NULL)
	{
		print(p->left,level+1);
		for(i=0;i<=level;i++)
			printf("\t");
        printf("%d\n", p->key);
		print(p->right,level+1);
	}
}

int main()
{
	Node *p;
	int a[100], n, i;
	printf("n=");
	scanf("%d", &n);

	for(i=0;i<n;i++)
	{
	    a[i]=i+1;
	}
	p=BuildTree(a[0],a[n-1]);

	print(p,0);
	getch();


}
