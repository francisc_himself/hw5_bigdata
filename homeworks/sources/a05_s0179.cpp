#include "stdio.h"
#include "conio.h"
#include "stdlib.h"
#include "Profiler.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 5 - Searching in Hash Tables
/***************************************************/

/********************************************************************************
/
/							OBSERVATIONS, INTERPRETATIONS
/
/	1. The algorithm requires more hash table accesses when the key is not found
/	   in the table. 
/	2. The bigger the load factor is (elements in the table/size of the table)
/	   the harder it is to find a given element.
/
/********************************************************************************/

#define inf 999999
#define MAX_NR 10007

int arr[MAX_NR];
int table[MAX_NR];
int ins;
int pos;
int count;
int maxFound;
int sumFound;
int maxNotFound;
int sumNotFound;
int alpha;

Profiler profiler("Hash");

void initTable()
{
	for(int i = 0; i < MAX_NR; i++)
	{
		table[i] = inf;
	}
}

int hash(int key, int it)
{
	int result = (key%MAX_NR + it + it*it)%MAX_NR;
	return result;
}

void insert(int key)
{
	int success = 0;
	int i = 0;
	while(i!=MAX_NR)
	{
		int index = hash(key,i);
		if(table[index] == inf)
		{
			table[index] = key;
			success = 1;
			return;
		}
		else
		{
			i++;
		}
	}
	if(success == 0)
	{
	printf("ERROR. Heap overflow\n");
	}
	return;
}

int search(int key)
{
	int i = 0;
	count = 0;
	int index;
	do
	{
		index = hash(key,i);
		count++;
		if(table[index] == key)
		{
			return index;
		}
		i++;
	}
	while(table[index]!=inf && i!=MAX_NR);
	return inf;
}

void main()
{
	/*
	//PROVING CORRECTNESS
	int pos;
	int key;
	initTable();
	insert(15);
	insert(30);
	insert(86000);
	insert(15000);
	printf("Exactly 10 searches will be performed. Choose wisely\n");
	for(int i = 1; i<= 10; i++)
	{
		printf("search for?   ");
		scanf("%d",&key);
		if((pos = search(key)) != inf)
		{
			printf("value is at position: %d\n\n", pos);
		}
		else
		{
			printf("value not found\n\n");
		}
	}
	*/

	
	//TESTING
	for(int m = 1; m <= 5; m++)
	{
		printf("%d Table initialized\n",m);
		for(alpha = 8000; alpha <= 9500; alpha += 500)
		{
			initTable();
			maxFound = 0;
			sumFound = 0;
			maxNotFound = 0;
			sumNotFound = 0;
			//FillRandomArray(arr,alpha,0,50000,true,0);
			for(int i = 0; i < alpha; i++)
			{
				insert(rand()*rand()%50000+1);
			}
			//search 1500-1500 elements
		for(int j = 1; j <= 1500; j++)
		{
			int value = rand() % 10007;
			if((pos = search(table[value])) != inf)
			{
				//count the values for found operation
				sumFound += count;
				if(count > maxFound)
				{
					maxFound = count;
				}
			}
		}
		for(int j = 1; j <= 1500; j++)
		{
			int value = rand() + 50001;
			if((pos = search(value)) == inf)
			{
				//count the values for not found operation
				sumNotFound += count;
				if(count > maxNotFound)
				{
					maxNotFound = count;
				}
			}
		}
			sumFound = (int)(sumFound/1500);
			profiler.countOperation("AvgFound",alpha,sumFound);
			sumNotFound = (int)(sumNotFound/1500);
			profiler.countOperation("AvgNotFound",alpha,sumNotFound);
			profiler.countOperation("FoundMax",alpha,maxFound);
			profiler.countOperation("NotFoundMax",alpha,maxNotFound);
		}
		//for alpha = 9900
		alpha = 9900;
		initTable();
		maxFound = 0;
		sumFound = 0;
		sumNotFound = 0;
		maxNotFound = 0;
		FillRandomArray(arr,alpha,0,50000,true,0);
		for(int i = 0; i < alpha; i++)
		{
			insert(arr[i]);
		}
		//search 1500-1500 elements
		for(int j = 1; j <= 1500; j++)
		{
			int value = rand() % 10007;
			if((pos = search(table[value])) != inf)
			{
				//count the values for found operation
				sumFound += count;
				if(count > maxFound)
				{
					maxFound = count;
				}
			}
		}
		for(int j = 1; j <= 1500; j++)
		{
			int value = (rand() % 10000) + 15000;
			if((pos = search(value)) == inf)
			{
				//count the values for not found operation
				sumNotFound += count;
				if(count > maxNotFound)
				{
					maxNotFound = count;
				}
			}
		}
		sumFound = (int)(sumFound/1500);
		profiler.countOperation("AvgFound",alpha,sumFound);
		sumNotFound = (int)(sumNotFound/1500);
		profiler.countOperation("AvgNotFound",alpha,sumNotFound);
		profiler.countOperation("FoundMax",alpha,maxFound);
		profiler.countOperation("NotFoundMax",alpha,maxNotFound);
	}
	profiler.createGroup("HashStatistics","AvgFound","FoundMax","AvgNotFound","NotFoundMax");
	profiler.showReport();
	
}