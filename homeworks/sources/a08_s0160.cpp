/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Multimi disjuncte
	Structura de date �multimi disjuncte� considera ca, initial, exista N elemente distincte (numerotate de la 1 la N),
fiecare facand parte dintr-o multime separata. Structura suporta doua operatii:
	1) Union(x,y): uneste multimea din care face parte elementul x cu multimea din care face parte elementul y. 
In urma unirii, elementele din cele doua multimi vor face parte din aceeasi multime.
	2) Find(x): intoarce un identificator al multimii din care face parte elementul x. Operatia Find are urmatoarele proprietati:
		-daca x si y sunt elemente din aceeasi multime, atunci Find(x)=Find(y).
		-daca x si y sunt elemente din multimi diferite, atunci Find(x)!=Find(y).

	Din grafic se observa ca numarul de operatii efectuate variaza liniar fata de numarul de muchii si cu cat numarul de muchii este
mai mare numarul de operatii este mai mare (variaza direct proportional).
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_VARFURI 10000
#define MAX_MUCHII 60000

int operatii;

//structura pentru nod
typedef struct NOD{
	int val;
	int rank;
	struct NOD *p;
}NOD;

//structura pentru muchie
typedef struct _EDGE{
	NOD *u,*v;
}EDGE;

//structura pentru graf
typedef struct _GRAF{
	NOD *V[MAX_VARFURI];
	EDGE *M[MAX_MUCHII];
}GRAF;

//metoda make_set de construire a multimii
void make_set(NOD *x)
{
	x->p=x;
	x->rank=0;
}

//metoda find_set de cautare element din multime
NOD* find_set(NOD *x)
{
	if (x!=x->p)
	{	
		x->p=find_set(x->p);
		operatii++;
	}
	return x->p;
}

//metoda link_set care face legatura intre 2 noduri din multime
void link_set(NOD *x,NOD *y)
{
	if (x->rank > y->rank)
		y->p=x;
	else
		x->p=y;
		if (x->rank == y->rank)
				y->rank=y->rank+1;
}

//metoda union_set
void union_set(NOD *x,NOD *y)
{
	link_set(find_set(x),find_set(y));
}

//metoda SAME_COMPONENT care verifica daca am componente identice
int SAME_COMPONENT(NOD *u,NOD *v)
{
	if(find_set(u)==find_set(v))
	{	
		return 1;
	}
	operatii+=2;
	return 0;
}

//metoda componente_conexe genereaza componente conexe
void componente_conexe(GRAF G,int nr_varfuri,int nr_muchii)
{
	int i;

	for(i=0;i<nr_varfuri;i++)
	{
		make_set(G.V[i]);
		operatii++;
	}
	for(i=0;i<nr_muchii;i++)
	{
		operatii+=2;
		if(find_set(G.M[i]->u)!=find_set(G.M[i]->v))
		{	
			union_set(G.M[i]->u,G.M[i]->v);
			operatii++;
		}
	}
}

int main()
{
	//declaratii
	GRAF G;
	EDGE *muchie1=(EDGE*)malloc(sizeof(EDGE));
	int nr_varfuri, nr_muchii;
	int i,j,k,u1,v1;
	NOD *q;
	FILE *fis_multimi;
	
	//verificare pentru un exemplu simplu
	nr_varfuri=9;
	nr_muchii=7;
	
	//alocare de memorie
	for(int i=0;i<MAX_VARFURI;i++)
		G.V[i]=(NOD*)malloc(sizeof(NOD));
	for(int i=0;i<MAX_MUCHII;i++)
		G.M[i]=(EDGE*)malloc(sizeof(EDGE));
	
	//initializare varfuri
	G.V[0]->val=1;
	G.V[1]->val=2;
	G.V[2]->val=3;
	G.V[3]->val=4;
	G.V[4]->val=5;
	G.V[5]->val=6;
	G.V[6]->val=7;
	G.V[7]->val=8;
	G.V[8]->val=9;
	
	//initializare muchii
	G.M[0]->u=G.V[0];
	G.M[0]->v=G.V[1];
	G.M[1]->u=G.V[1];
	G.M[1]->v=G.V[3];
	G.M[2]->u=G.V[0];
	G.M[2]->v=G.V[2];
	G.M[3]->u=G.V[2];
	G.M[3]->v=G.V[4];
	G.M[4]->u=G.V[5];
	G.M[4]->v=G.V[6];
	G.M[5]->u=G.V[5];
	G.M[5]->v=G.V[7];
	G.M[6]->u=G.V[6];
	G.M[6]->v=G.V[8];

	componente_conexe(G,nr_varfuri,nr_muchii);
	printf("Afisam fiecare element si componenta din care face parte:\n");
	for(i=0;i<nr_varfuri;i++)
	{
		q=find_set(G.V[i]);
		printf("%d in %d\n",G.V[i]->val,q->val);
	}
	
	//pe caz general
	srand(time(NULL));
	fis_multimi=fopen("multimi.csv","w");
	
	for(int i=0;i<10000;i++)
	{	
		G.V[i]->val=i;
	}
	muchie1->u=G.V[0];
	muchie1->v=G.V[1];

	for(k=10000;k<60000;k+=1000)
	{
		operatii=0;
		printf("contor=%d\n",k);
		for(j=0;j<k;j++)
		{
			u1=rand()%10000;
			do
			{
				v1=rand()%10000;
			}
			while(u1==v1);

			muchie1->u=G.V[u1];
			muchie1->v=G.V[v1];
			
			G.M[j]->u=G.V[u1];
			G.M[j]->v=G.V[v1];
		}
		componente_conexe(G,10000,k);
		fprintf(fis_multimi,"%d,%d\n",k,operatii);
	}
	
	fclose(fis_multimi);
	_getch();
	
	return 0;
}

