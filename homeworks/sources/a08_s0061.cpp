//Eficienta algoritmului este O(nr_muchii + nr_varfuri lg nr_varfuri)
//Multimile sunt implementate ca lsi(liste simplu inlantuite)
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#define MAX 10000

typedef struct nod{
int info;//informatia din nod
struct nod *adresa;//urm =adresa urmatorului element din lista;
struct nod *ref;   //ref=referinta adresei catre elementul reprezentant al multimii
}NOD;

NOD *s[MAX];       //multimea de submultimi; fiecare element s[k] va continea adresa catre o submultime sk
int a[MAX][MAX];   //matricea de adiacenta
int m,nr_varfuri,nr_muchii;

/**
 * Metoda creeaza o lsi de elemente punand informatia in noduri si retinand referinta si adresa elementului urmator
 * @param x elementul care se pune ca informatie in nod
 */
NOD*formeazaMultime(int x)
{
  NOD *p;
  p=(NOD*)malloc(sizeof(NOD));
  p->info=x;
  p->adresa=0;
  p->ref=p;
  return p;


}
/**
 * Metoda gaseste multimea in care se afla elemenul x si returneaza un pointer ce contine adresa multimii respective
 * @param x elementul cautat in multime
 */
NOD*gasesteMultime(int x){
  NOD*p;
  //se parcurg submultimile pana la s[k]
  for(int i=0;i<nr_varfuri;i++)
    if(s[i]!=0){
		p=s[i];
		while(p!=0)
			if(p->info==x) return p->ref;
			else p=p->adresa;
	}
  return 0;
}

/**
 * Metoda ce realizeaza stergerea multimii S[k] de care nu mai este nevoie dupa reuniune
 */
void stergeMultime(NOD *p)
{
  for(int i=0;i<nr_varfuri;i++)
	  if(s[i]==p) s[i]=0;
}

/**
 * Metoda ce realizeaza reuniunea a 2 submultimi: cautand multimea cu cele mai multe elemente
 * si retinand numarul de elemente din multimea ce il contine pe x si pe y dupa care adauga
 * elementele multimii mai mici la multimea mai mare si se sterge multimea cu cele mai putine elemente
 */
void reuniuneMultimi(int x,int y)
{
   NOD *p,*q,*p2,*q2;
   int i,j;
   p=gasesteMultime(x); m++;
   q=gasesteMultime(y); m++;
   p2=p;
   q2=q;
   i=1;
   while(p!=0){
	    i++;
		p=p->adresa;
   }
   j=1;
   while(q!=0){
	    j++;
		q=q->adresa;
   }
   p=p2;
   q=q2;
   if(i>=j)
   {
     while(p->adresa!=0)  p=p->adresa;
     p->adresa=q;
     while(q!=0){
	      q->ref=p->ref;
	      q=q->adresa;
       }
        stergeMultime(q2);
    }
   else{
          while(q->adresa!=0)  q=q->adresa;
          q->adresa=p;
          while(p!=0){
            p->ref=q->ref;
            p=p->adresa;
          }
	      stergeMultime(p2);
       }
}

//realizeaza componenete conexe
void componenteConexe(int nr_vf)
{
  int k2;
  for(int i=0;i<nr_vf;i++){
	  k2=i;
      s[i]=formeazaMultime(k2);
      m++;
  }
	for(int k1=0;k1<nr_vf;k1++)
	  for(k2=0;k2<=k1;k2++)
		  if(a[k1][k2]==1){
			  m=m+2;
		      if(gasesteMultime(k1)!=gasesteMultime(k2)){
	               reuniuneMultimi(k1,k2);
	               m++;
				}
		  }
}

void generareMuchiiRandom(int nr_muchii,int nr_vf){
  int x,y,ok;
  for(int i=0;i<nr_muchii;i++)
	  do{
		  ok=0;
          x=rand()%nr_vf;
		  y=rand()%nr_vf;
		  if((a[x][y]==0)&&(a[y][x]==0)&&(x!=y)){
		    a[x][y]=1;
		    a[y][x]=1;
			ok=1;
		  }
	  }while(ok!=1);
}

int main(){
  NOD *p;
  FILE*f=fopen("fisier.txt","w");;
  nr_varfuri=MAX;
  for(nr_muchii=MAX;nr_muchii<60000;nr_muchii=nr_muchii+1000){
	  for(int i=0;i<nr_varfuri;i++) s[i]=0;
      m=0;
      for(int i=0;i<nr_varfuri;i++)
	     for(int j=0;j<nr_varfuri;j++)
		     a[i][j]=0;
      generareMuchiiRandom(nr_muchii,nr_varfuri);
      componenteConexe(nr_varfuri);
      fprintf(f,"%d\t%d\n",nr_muchii,m);

  }
  return 0;
}
