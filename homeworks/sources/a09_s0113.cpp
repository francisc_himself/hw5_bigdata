/*
	Name: ***ANONIM*** ***ANONIM***án
	Group: ***GROUP_NUMBER***

	Requirements: You are required to implement correctly and efficiently the Breadth-First Search (BFS)
				graph algorithm (Section 22.2 from the book1). For graph representation, you should use 
				adjacency lists

	Analysis: - to be able to perform a pretty printing in binary representation of multiway trees,
				I used the code from multiway trees to transform a multiway tree from parent representation
				to binary multiway representation
			  - charts clearly shows a running time of O(V+E)
			  - to verify that edges were not generated twice, 2 techniques were used:
					1) a matrix in[i][j]=1 where edge i-j is present, 0 otherwise. This way I generate edges
						until the edge is not present
					2) since with a small number of vertices and large number of edges the previous method takes
						very much time, a linked list with all possible edges was generated, and after choosing one
						randomly, it is removed from the linked list.
*/
#include<stdio.h>
#include<queue>
#include<vector>
#include<time.h>
#define NMAX 300
enum Color{WHITE, GRAY, BLACK};

int v,e;
Color colors[NMAX];
int parent[NMAX];
int d[NMAX];

using namespace std;

vector<int> adj[NMAX];

int *tree[NMAX], root;
int cnt;

struct NODE
{
	int key;
	NODE *left, *right;
}*TreeRoot;

NODE * createBinaryMultiway(int key)
{
	NODE *newNode=new NODE;
	newNode->key=key;
	newNode->left=newNode->right=NULL;
	if(tree[key][0]!=0)
		newNode->left=createBinaryMultiway(tree[key][1]);
	int i;
	NODE *aux1, *aux2=NULL;
	if(tree[key][0]>1)
		aux2=createBinaryMultiway(tree[key][tree[key][0]]);
	for(i=tree[key][0]-1;i>=2;i--)
	{
		aux1=createBinaryMultiway(tree[key][i]);
		aux1->right=aux2;
		aux2=aux1;
	}
	if(aux2!=NULL)
		newNode->left->right=aux2;
	return newNode;
}
void createR3()
{
	TreeRoot=createBinaryMultiway(root);
}

void createR2()
{
	int i;
	for(i=1;i<=v;i++)
		if(parent[i]!=0 && colors[i]==BLACK)
			tree[parent[i]][0]++;
	for(i=1;i<=v;i++)
		if(colors[i]==BLACK)
		{
			tree[i]=(int *)malloc((1+tree[i][0])*sizeof(int));
			tree[i][0]=0;
	}
	for(i=1;i<=v;i++)
		if(colors[i]==BLACK)
		{
			if(parent[i]!=0)
				tree[parent[i]][++tree[parent[i]][0]]=i;
			else root=i;
		}

}


void displayR3(NODE *node, int level)
{
	if(node!=NULL)
	{
		int i;
		for(i=0;i<=3*level;i++)
			printf(" ");
		printf("%d\n", node->key);
		displayR3(node->left, level+1);
		displayR3(node->right, level);
	}
}

void Bfs(int source)
{
	colors[source]=GRAY;
	d[source]=0;
	parent[source]=0;
	queue<int> q;
	q.push(source);
	cnt+=4;
	int u,i;
	while(!q.empty())
	{
		u=q.front();
		q.pop();
		cnt++;
		for(i=0;i<adj[u].size();i++)
		{
			if(colors[adj[u][i]]==WHITE)
			{
				colors[adj[u][i]]=GRAY;
				d[adj[u][i]]=colors[u]+1;
				parent[adj[u][i]]=u;
				q.push(adj[u][i]);
				cnt+=4;
			}
			cnt++;
		}
		colors[u]=BLACK;
	}

}
int in[NMAX][NMAX];
int chosen[NMAX*NMAX];
vector<pair<int, int> > edges;


void init()
{
	int i;
	for(i=1;i<=v;i++)
	{
		colors[i]=WHITE;
		d[i]=2<<28;
		parent[i]=0;
		tree[i]=(int *)malloc(sizeof(int));
		tree[i][0]=0;
		adj[i].clear();
	}
	for(i=1;i<=v;i++)
		for(int j=1;j<=v;j++)
			in[i][j]=0;
}

void init2()
{
	int i;
		edges.clear();
	for(i=1;i<=v;i++)
		for(int j=i+1;j<=v;j++)
			edges.push_back(make_pair(i,j));
}

void test()
{
	int i;
	v=10;
	init();
	adj[1].push_back(2);
	adj[1].push_back(3);
	adj[2].push_back(5);
	adj[3].push_back(4);
	adj[6].push_back(7);
	adj[6].push_back(8);
	adj[10].push_back(9);
	for(i=1;i<=v;i++)
		if(colors[i]==WHITE)
		{
			Bfs(i);
			createR2();
			createR3();
			displayR3(TreeRoot, 0);
		}

}


int main()
{
	test();
	freopen("1.csv", "w", stdout);
	v=100;
	int i,j;
	printf("v,e,op\n");
	srand(time(NULL));
	for(i=1000;i<=5000;i+=100)
	{
		init();
		cnt=0;
		for(j=1;j<=i;j++)
		{
			int x,y;
			do{
			 x=rand()%v+1;
			 y=rand()%v+1;
			}while(in[x][y]==1);
			in[x][y]=in[y][x]=1;
			adj[x].push_back(y);
			adj[y].push_back(x);
		}

		for(j=1;j<=v;j++)
			if(colors[j]==WHITE)
				Bfs(j);
		printf("%d\n", i);
		printf("%d,%d,%d\n", v, i, cnt);
	}
	
	freopen("2.csv", "w", stdout);
	printf("v,e,op\n");
	e=1000;
	for(i=100;i<=200;i+=10)
	{
		v=i;
		init();
		init2();
		cnt=0;
		for(j=1;j<=e;j++)
		{
			int x,y;
			x=rand()%edges.size();
			pair<int, int> p=edges[x];
			adj[p.first].push_back(p.second);
			adj[p.second].push_back(p.first);
			edges.erase(find(edges.begin(), edges.end(), p));
		}

		for(j=1;j<=v;j++)
			if(colors[j]==WHITE)
				Bfs(j);
		printf("%d,%d,%d\n", v, e, cnt);
	}
	
	return 0;
}