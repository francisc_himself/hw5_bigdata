﻿
/* CONCLUZII:
 - Algoritmul de sortare prin selectie are un ordin de complexitate patratic; numarul de operatii (numarul de atribuiri sau numarul 
de comparatii sau suma lor) este acelasi indiferent de structura datelor de intrare (sir ordonat crescator, descrescator, aleator)
 - Algoritmul de sortare prin insertie are ordin de complexitate nXn. Cazul favorabil pentru acest algoritm este atunci cand sirul
de intrare este sortat descrescator.
 - In cazul algoritmului de sortare prin interschimbare, cazul favorabil este cand sirul de intrare este gata sortat, iar cazul 
defavorabil, atunci cand sirul este sortat descrescator. Ordinul de complexitate este nXn.
*/


#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>

#define MAX 10000

//interschimbare
void interschimbare (int A[], int n, int &nrA, int &nrC)
{
	nrA = 0;
	nrC = 0;
	int j, aux, ordonat = 0;
	while (ordonat == 0)
	 {
		ordonat = 1;
		for (j = 1; j <= n - 1; j++)
		 {
			nrC++;
			if (A[j] > A[j+1])
			 {
				aux = A[j];
				A[j] = A[j+1];
				A[j+1] = aux;
				nrA=nrA+3;
				ordonat = 0;
			}
		}
	}
}

//selectie
void selectie (int A[], int n, int &nrA, int &nrC)
{
	nrA = 0;
	nrC = 0;
	int i, j, imin, aux;
	for (i = 1; i <= n - 1; i++)
	{
		imin = i;
		for (j = i + 1; j <= n; j++)
		{
			nrC++;
			if (A[j] < A[imin])
				imin = j;
		}
		aux = A[i];
		A[i] = A[imin];
		A[imin] = aux;
		nrA=nrA+3;
	}
}

//insertie
void insertie (int A[], int n, int &nrA, int &nrC)
{
	int i, j, k, x;
	for (i = 2; i <= n; i++)
	{
		x = A[i];
		j = 1;
		nrC++;
		while (A[j] < x)
		{
			j = j + 1;
		}
		for (k = i; k >= j + 1; k--)
		{
			A[k] = A[k-1];
			nrA++;
		}
		A[j] = x;
		nrA++;
	}
}

void copiere_sir(int destinatie[], int n, int sursa[])
{
	int i;
	for (i = n; i <= n; i++)
		destinatie[i] = sursa[i];
}



void main()
{
	FILE *f;
	int n, j, i, X[MAX], Y[MAX], atrib[4], comp[4];
	int B[100];
	
	f = fopen("sort.csv","w");
	
	srand (time(NULL));
	
	//antet tabel
	fprintf(f, "n,bubble_bst_ass,bubble_bst_comp,bubble_bst_ass+comp,selection_bst_ass,selection_bst_comp,selection_bst_ass+comp,insertion_bst_ass,insertion_bst_comp,insertion_bst_ass+comp,bubble_av_ass,bubble_av_comp,bubble_av_ass+comp,selection_av_ass,selection_av_comp,selection_av_ass+comp,insertion_av_ass,insertion_av_comp,insertion_av_ass+comp,bubble_ws_ass,bubble_ws_comp,bubble_ws_ass+comp,selection_ws_ass,selection_ws_comp,selection_ws_ass+comp,insertion_ws_ass,insertion_ws_comp,insertion_ws_ass+comp\n");
	
	for (n = 100; n <= 10000; n = n + 200)
	 {
		atrib[1] = 0;
		atrib[2] = 0;
		atrib[3] = 0;
		comp[1] = 0;
		comp[2] = 0;
		comp[3] = 0;

		fprintf(f, "%d,", n);
	
		//valori ordonate crescator => caz favorabil
		for (i = 1 ; i <= n; i++)
			X[i] = i;
	
		//interschimbare favorabil
		interschimbare(X, n, atrib[0], comp[0]);
		fprintf(f, "%d,%d,%d,", atrib[0], comp[0], atrib[0]+comp[0]);

		//selectie favorabil
		selectie(X, n, atrib[0], comp[0]);
		fprintf(f, "%d,%d,%d,", atrib[0], comp[0], atrib[0]+comp[0]);

		//inserare favorabil
		insertie(X, n, atrib[0], comp[0]);
		fprintf(f, "%d,%d,%d,", atrib[0], comp[0], atrib[0]+comp[0]);

		//cazul avrage; m=5
		for (j = 1; j <= 5; j++)
		{
			for (i = 1; i <= n; i++)
			{
				X[i] = rand() % 10000;
				Y[i] = X[i];
			}

			//interschimbare avrage
			interschimbare(X, n, atrib[0], comp[0]);
			atrib[1] = atrib[1] + atrib[0];
			comp[1] = comp[1] + comp[0];
			copiere_sir(X, n, Y);

			//selectie avrage
			selectie(X, n, atrib[0], comp[0]);
			atrib[2] = atrib[2] + atrib[0];
			comp[2] = comp[2] + comp[0];
			copiere_sir(X, n, Y);

			//INSERTION BEST CASE APELARE
			insertie(X, n, atrib[0], comp[0]);
			atrib[3] = atrib[3] + atrib[0];
			comp[3] = comp[3] + comp[0];
			copiere_sir(X, n, Y);
		}

		fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,", atrib[1] / 5, comp[1] / 5, (atrib[1] + comp[1]) / 5, atrib[2] / 5, comp[2] / 5, (atrib[2] + comp[2]) / 5, atrib[3] / 5, comp[3] / 5, (atrib[3] + comp[3]) / 5);

		//valori ordonate descrescator => caz defavorabil
		for (i = 1; i <= n; i++)
		 {
			X[i] = n - i + 1;
			Y[i]=X[i];
		 }

		//interschimbare defavoravil
		interschimbare(X, n, atrib[0], comp[0]);
		fprintf(f, "%d,%d,%d,", atrib[0], comp[0], atrib[0]+comp[0]);
		copiere_sir(X, n, Y);

		//selectie defavorabil
		selectie(X, n, atrib[0], comp[0]);
		fprintf(f, "%d,%d,%d,", atrib[0], comp[0], atrib[0]+comp[0]);
		copiere_sir(X, n, Y);

		//insertie defavorabil
		insertie(X, n, atrib[0], comp[0]);
		fprintf(f, "%d,%d,%d\n", atrib[0], comp[0], atrib[0]+comp[0]);
		copiere_sir(X, n, Y);
	}
	printf("datele au fost scrise\n");

	for (i = 1; i <= 14; i++)
				B[i] = rand() % 10000;
	interschimbare(B,14,atrib[0],comp[0]);
	for (i = 1; i <= 14; i++)
		printf("%d ",B[i]);
	fclose(f);

	getch();
}