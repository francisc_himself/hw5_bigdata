/*
***ANONIM*** ***ANONIM*** Mihai, grupa ***GROUP_NUMBER***
Lucrarea NR 8, Multimi Disjuncte (Disjoint Sets)
In aceasta lucrare de laborator am studiat multimile disjuncte, si functiile de creare, reuniune si cautare.
Dupa implementarea acestor functii am cautat componentele conexe ale grafului si am salvat valorile(numar muchii,numar operatii) in fisierul lab8.csv.
Folosind aceste valori am creat un grafic, dupa care se observa ca numarul de operatii creste liniar;

*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//Structura unui nod al cozii
typedef struct nod {
    int cheie;
    int dim;
    struct nod* urm;
    struct nod* reprez;
} Nod;
//Structura unei muchii
typedef struct muchie {
    int x;
    int y;
} Muchie;
//Variabile globale
Nod* cap[10000];
Nod* coada[10000];
Muchie m[60000];
int v = 10000;
int e;

static int M = 0;

//Creare multime cu reprezentant x
void MakeSet(int x) {
    Nod* p = (Nod*)malloc(sizeof(Nod));
    p -> cheie = x;
    p -> dim = 1;
    p -> urm = NULL;
    p -> reprez = p;
    cap[x] = coada[x] = p;
}

//Gasire reprezentant
int FindSet(int x) {
    return cap[x] -> reprez -> cheie;
}

//Uniunea a doua multimi disjuncte
void Union(int x, int y) {
	Nod* p;
    int i = FindSet(x);
    int k = FindSet(y);
    M += 2;
    int j;
    int aux;

    if (i == k) return;

    if (cap[i] -> dim > cap[k] -> dim) {
        aux = i;
        i = k;
        k = aux;
    }

    coada[k] -> urm = cap[i];
    coada[k] = coada[i];

    p = cap[i];
    while(p) {
        p -> reprez = cap[k];
        p = p -> urm;
    }

    cap[k] -> dim += cap[i] -> dim;
    cap[i] -> dim = 0;
}

void componente_conexe() {
	int i;
	for (i = 0; i < v; i++) {
        MakeSet(i);
        M++;
	}
	for (i = 0; i < e; i++) {
        Union(m[i].x, m[i].y);
        M+=3;//Union apeleaza de 2 ori FindSet, de aceea avem 3 apeluri de functii;
	}
}

int main() {
	int i, j, e1, e2;
	FILE *pf;
	pf=fopen("Lab8.csv","w");
	fprintf(pf,"Edges,Operations\n");
	srand(time(NULL));
	for (e = 10000; e <= 60000; e += 1000) 
	{
		M=0;
		for (j = 0; j < e; j++) 
		{
			e1 = rand()%v;
			e2 = rand()%v;
			m[j].x = e1;
			m[j].y = e2;
		}
		
		componente_conexe();
		fprintf(pf,"%10d,%10d\n", e, M);
	}
	fclose(pf);
}

