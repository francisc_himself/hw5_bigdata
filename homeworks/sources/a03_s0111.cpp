#include <stdio.h>
#include <conio.h>
#include "Profiler.h"
#define LEFT(i) 2*i+1;
#define RIGHT(i) 2*i+2;

/*
Heap sort is an optimal algorithm having a time complexity of O(nlgn) for all three cases, best, average and worst case.
- it is based on the build heap and heapify functions
- since in a max-heap we always have the maximum element on the root, in heap sort we swap the first element with the last one, 
decrement the array and apply heapify for the root, method which sinks the new inserted element in its place
Quicksort behaves better than heap sort in practical situations in average and best case(O(nlgn)); in worst case it has a time complexity of O(n^2)
- a good implementation of quicksort is optimal
- it apply the Divide and Conquer paradigm
- it succesively divide the array into 2 sub-arrays, the middle element being grater than all elements of the first sub-array and smaller than elements of the second one,
so that the middle element is in right place
- by recursive calls each sub-array is divided in the same manner, until finally the array is sorted
In average case quicksort is faster than heap sort, both having a time complexity of O(nlgn)
The worst case for quicksort happens when the pivot is the minimum or maximum element; for heapsort worst case appears when array is sorted
In best case the quicksort is faster than heapsort both having O(nlgn) time complexity;for quicksort best case happens when each partition is divided in half, for heapsort best case appears when array is reversed sorted
Both methods behaves similar in average and best case
*/

Profiler profiler("HW2");

void heapify(int a[],int i,int size,int *op) {
   int l,r,largest,temp;
   l=LEFT(i);
   r=RIGHT(i);
  
  (*op)++;
  if (l<=size && a[l]>a[i] ) 
	largest=l;
  else 
	largest=i;
 
  (*op)++;
  if ( r<=size && a[r]>a[largest] )  
	largest=r;
 
  if ( largest != i ) {
	  temp=a[i];
	  a[i]=a[largest];
	  a[largest]=temp;
	  *op+=3;
	  heapify(a,largest,size,op);
  }
}

void buildHeap(int a[],int size,int *op) {
	 for (int j=(size/2)-1;j>=0;j--) {
		heapify(a,j,size,op);	
	 }
}

 
void heap_sort(int a[],int size,int *op) {
	int i=size-1,aux;
	buildHeap(a,size,op);
	while (i>0) {
		aux=a[i];
		a[i]=a[0];
		a[0]=aux;
		heapify(a,0,--i,op);
	}
}

int partition(int a[],int p,int r,int *op) {
	int x,i,j,aux;
	x=a[r];
	i=p-1;
	(*op)++;
	for (j=p;j<=r-1;j++) {
		(*op)++;
		if (a[j]<=x) {
			i++;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
			*op=*op+3;
		}
	}
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;
	*op=*op+3;

	return i+1;
}


int select(int a[],int p,int r,int i) { 
	int k,q,aux;
	if (p==r) 
		 return a[p];
	q=partition(a,p,r,&aux);
	k=q-p+1;
	if (i<=k)
 	 return select(a,p,q-1,i);
	else 
	 return select(a,q+1,r,i-k);
}

void quicksort(int a[], int p, int r,int *op) {
	 int q;
	 if (p < r) {
		 q=partition(a,p,r,op);
		 quicksort(a,p,q-1,op);
		 quicksort(a,q+1,r,op);
	 }
 }

void findMid(int a[],int p,int r,int *op) {
	int pos=p+(r-p+1)/2;
	int aux=a[r];
	a[r]=a[pos];
	a[pos]=aux;
	(*op)=(*op)+3;
}

void quickSortBest(int a[], int p, int r,int *op) {
	 int q;
	 if (p < r) {
		findMid(a,p,r,op);
		q=partition(a,p,r,op);
		quickSortBest(a,p,q-1,op);
		quickSortBest(a,q+1,r,op);
	 }
 }


int main() {
	int op1,op2,sum1,sum2;
	int a[10000],b[10000],c[10000];
/*	for (int n=100;n<=10000;n+=100) {
	//	sum1=sum2=0;
	//	for(int j=1;j<=5;j++) {
			FillRandomArray(a,n,10,10000,false,1);
			for (int i=0;i<n;i++) {
				b[i]=c[i]=a[i];
			}
			int j=0;
			//heapsort is in best case if array is reversed sorted
			//for(int i=n-1;i>=0;i--) {
			//	b[j]=a[i];
			//	j++;
			//}
			op1=op2=0;
			heap_sort(b,n,&op1);
			quicksort(c,0,n-1,&op2);
	//		quickSortBest(c,0,n-1,&op2);
	//		sum1+=op1;
	//		sum2+=op2;
	//	}
		profiler.countOperation("heap_sort",n,op1);//sum1/5);
		profiler.countOperation("quick_sort",n,op2);//sum2/5);
	}	
	//profiler.createGroup("worst_case","heap_sort","quick_sort");
	profiler.showReport(); */
	FillRandomArray(a,20,10,200,false,0);
	for (int i=0;i<20;i++) {
		b[i]=c[i]=a[i];
	}
	heap_sort(b,20,&op1);
	quicksort(c,0,19,&op2);
	printf("Initial array: \n");
	for (int i=0;i<20;i++)
		printf("%d ",a[i]);
	printf("\nHeapsort:\n");
	for (int i=0;i<20;i++)
		printf("%d ",b[i]);
	printf("\nQuicksort:\n");
	for (int i=0;i<20;i++)
		printf("%d ",c[i]);
getch();
return 0;
}
