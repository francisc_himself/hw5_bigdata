#include "stdio.h"
#include "Profiler.h"
#include "conio.h"
#include "stdlib.h"
#include "stddef.h"

/***************************************************
/	Name: ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group: ***GROUP_NUMBER***
/	Assignemnt: 9 - Breadth First Search
/***************************************************/

#define inf 999999
#define WHITE  0
#define GREY  1
#define BLACK  2
#define PRINTED 5

Profiler profiler("BFSTest");

typedef struct graph
{
	struct graph *next;
	struct graph *p;
	int value;
	int color;
	int d;
}graphT;

typedef graphT* NodePtr;

NodePtr v[201];

typedef struct qNode
{
	struct qNode *next;
	NodePtr graphNode;
}qNodeT;

typedef qNodeT* QPtr;
QPtr first;

int vertexNr = 0;
int edgeNr = 0;

void generateGraph(int edgeNumber, int vertexNumber)
{
	for(int i = 1; i<=vertexNumber; i++)
	{
		v[i] = (NodePtr)malloc(sizeof(graphT));
		v[i]->color = WHITE;
		v[i]->d = inf;
		v[i]->p = NULL;
		v[i]->next = NULL;
		v[i]->value = i;
	}
	int i = 1;
	while(i <= edgeNumber)
	{
		bool found = false;
		int start = rand() % vertexNumber +1;
		int end = rand() % vertexNumber +1;
		if(start!= end)
		{
		NodePtr aux = v[start];
		while(aux->next != NULL)
		{
			if(aux->next->value == v[end]->value)
			{
				found = true;
			}
			aux = aux->next;
		}
		if(!found)
		{
			aux->next = (NodePtr)malloc(sizeof(graphT));
			aux->next->color = v[end]->color;
			aux->next->d = v[end]->d;
			aux->next->p = v[end]->p;
			aux->next->value = v[end]->value;
			aux->next->next = NULL;
			i++;
		}
		}
	}
}

void enQ(NodePtr node)
{
	profiler.countOperation("Operations",vertexNr);
	//profiler.countOperation("Operations",edgeNr);
	if(first == NULL)
	{
		first = (QPtr)malloc(sizeof(qNodeT));
		first->graphNode = node;
		first->next = NULL;
	}
	else
	{
		QPtr aux = first;
		while(aux->next != NULL)
		{
			aux = aux->next;
		}
		QPtr added = (QPtr)malloc(sizeof(qNodeT));
		added->graphNode = node;
		added->next = NULL;
		aux->next = added;
	}
}

NodePtr deQ()
{
	profiler.countOperation("Operations",vertexNr);
	//profiler.countOperation("Operations",edgeNr);
	if(first != NULL)
	{
		NodePtr toReturn = first->graphNode;
		first = first->next;
		return toReturn;
	}
	else
	{
		return NULL;
	}
}

void bfs(NodePtr source, int size)
{
	for(int i = 1; i <= size; i++)
	{
		v[i]->color = WHITE;
		v[i]->d = inf;
		v[i]->p = NULL;
		profiler.countOperation("Operations",vertexNr);
		//profiler.countOperation("Operations",edgeNr);
	}
	profiler.countOperation("Operations",vertexNr);
	//profiler.countOperation("Operations",edgeNr);
	source->color = GREY;
	source->d = 0;
	source->p = NULL;
	first = NULL;
	enQ(source);
	while(first != NULL)
	{
		NodePtr u = deQ();
		NodePtr aux = u;
		while(aux != NULL)
		{
			profiler.countOperation("Operations",vertexNr);
			//profiler.countOperation("Operations",edgeNr);
			if (v[aux->value]->color == WHITE)
			{
				v[aux->value]->color = GREY;
				v[aux->value]->d = u->d + 1;
				v[aux->value]->p = u;
				enQ(v[aux->value]);
				profiler.countOperation("Operations",vertexNr);
				//profiler.countOperation("Operations",edgeNr);
			}
			aux = aux->next;
		}
		u->color = BLACK;
	}
}

void prettyPrint(int index, int level)
{ 
    v[index]->color=PRINTED;
    for (int i=0;i<level;i++) 
		printf("    ");
    printf("%d\n",index);
    for (int i=1; i<=10; i++)
    {
        if (i!=index)
        {
            if (v[i]->p!=NULL && v[i]->color!=PRINTED)
            {
                if (v[i]->p->value==index)
                {
                    printf("   ");
                    v[i]->color=PRINTED;
                    prettyPrint(v[i]->value,level+1);
                }
            }
        }
    }
}

void main()
{
	
	//PROVING CORRECTNESS
	generateGraph(30,10);
	for(int i = 1; i <= 10; i++)
	{
		NodePtr aux = v[i];
		while(aux != NULL)
		{
			printf("%d ->",aux->value);
			aux = aux->next;
		}
		printf("\n");
	}
	bfs(v[1],10);
	for(int i = 1; i <= 10; i++)
	{
		if(v[i]->d == 0)
		{
			prettyPrint(i,0);
		}
	}
	getch();
	

	/*
	//TESING ALG 1
	vertexNr = 100;
	for(edgeNr = 1000; edgeNr <= 5000; edgeNr += 100)
	{
		generateGraph(edgeNr,vertexNr);
		bfs(v[1],vertexNr);
		printf("%d\n",edgeNr);
	}
	profiler.showReport();
	*/
	/*
	//TESTING ALG 2
	edgeNr = 9000;
	for(vertexNr = 100; vertexNr <= 200; vertexNr += 10)
	{
		generateGraph(edgeNr,vertexNr);
		bfs(v[1],vertexNr);
		printf("%d\n",vertexNr);
	}
	profiler.showReport();
	*/
}