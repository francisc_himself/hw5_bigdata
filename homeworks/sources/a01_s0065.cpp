/*

Concluziile sintezei implementarii acestor algoritmi :

1. Bubble Sort

- este unul dintre cele mai simple algoritme
- este ineficient pentru vectori de dimensiuni ***ANONIM***
- in cazul mediu statistic pe grafic se poate constata o crestere exponentiala a numarului de operatii, 
  dintre cele 3 algoritme implementate, acesta atinge numarul maxim de operatii efectuate pentru a sorta
- numarul comparatiilor este aproximativ dublu fata de numarul de atribuiri necesare sortarii
- se dovedeste a fi cel mai eficient dupa cum se poate observa in cazul cel mai favorabil, detectand cel mai rapid un vector sortat
  graficul prezentand o crestere liniara exacta
- in cazul cel mai defavorabil, respectiv cand lista este sortata descrescator, numarul comparatiilor este egal cu cel al atribuirilor
  prezentand pe grafic o crestere exponentiala


2. Insertion Sort

- este mult mai putin eficient decat alti algoritmi ca sortarea prin interclasare
- se dovedeste a fi eficient pentru liste de dimensiuni mici
- in cazul mediu statistic prezinta o caracteristica exponentiala, mai atenuata decat Bubble Sort, si dupa cum se poate vedea
  si din statistici , executa cu 25% mai putine operatii decat acesta
- in cazul cel mai favorabil ( cand lista este sortata crescator) caracteristica este una constanta, liniara
- in cazul cel mai putin favorabil (lista este ordonata descrescator) se executa mai multe comparatii, 
  avand o caracteristica exponentiala, pe cand numarul atribuirilor este liniara

3. Selection Sort

- spre deosebire de Bubble Sort, acesta este constant si in cel mai favorabil caz, dar si in cel mai defavorabil
  deoarece indiferent de faptul ca lista este sortata sau nu, trebuiesc efectuate acelasi numar de selectii
- in cazul mediu statistic prezinta o caracteristica liniara, precum si in cazurile defavorabile si best-case, 
  numarul atribuirilor este liniar, identic, precum si in cazul comparatiilor doar ca caracteristica este exponentiala

*/




#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#define DIM_MAX 10000


int i=0;
int atr_bubble=0;
int cmp_bubble=0;
int sum_bubble=0;
int atr_insertion=0;
int cmp_insertion=0;
int sum_insertion=0;
int atr_selection=0;
int cmp_selection=0;
int sum_selection=0;

void reset()
{
		cmp_bubble = 0;
		atr_bubble = 0;
		sum_bubble = 0;
		cmp_insertion = 0;
		atr_insertion = 0;
		sum_insertion = 0;
		cmp_selection = 0;
		atr_selection = 0;
		sum_selection = 0;
}



void bubbleSort(int *a, int n)
{
      int schimbat = 0;

      int j = 0;
      int temp;

	  do
	  {
            schimbat = 1;
		j=j+1;
            for (int i = 0; i < n - j; i++)
			{
					cmp_bubble++;

                  if (a[i] > a[i + 1])
				  {

                        temp = a[i];
                        a[i] = a[i + 1];
                        a[i + 1] = temp;

						atr_bubble++;

                        schimbat= 0;
                  }
            }
      }
	  while(schimbat==0);

}


void selectionSort(int *a, int n) {
      int i, j, min, tmp;
      for (i = 0; i < n - 1; i++)
	  {
            min = i;
            for (j = i + 1; j < n; j++)
			{       
				cmp_selection++;
                  if (a[j] < a[min])
				  {
                        min = j;
				  }
			}
			if( i != min)
			{
                  tmp = a[i];
                  a[i] = a[min];
                  a[min] = tmp;
			
			atr_selection++;
			}
      }
}

void insertionSort(int *a,int n)
{
	int j;

	for(j=1;j<n;j++)
	{
		int x=a[j];
			int i = j-1;

			cmp_insertion = cmp_insertion+1;
			while(a[i]>x && i>0)
			{
				cmp_insertion++;
				a[i+1]=a[i];
				i=i-1;
				atr_insertion++;
			}
			a[i+1]=x;
				atr_insertion++;
			

	}
}




void genereazaBest(int *a, int n)
	{
		srand((unsigned)time(NULL));

			a[i]= rand() % 1000;
			for(int i=1;i<n;i++)
			{
				a[i] = a[i-1] + rand() % 1000;
			}
	}

void genereazaWorst(int *a, int n)
	{
		srand((unsigned)time(NULL));

			a[i]= rand() % 1000;
			for(int i=1;i<n;i++)
			{
				a[i] = a[i-1] - rand() % 1000;
			}
	}

void genereazaMedium(int *a, int n)
	{
		srand((unsigned)time(NULL));

			
			for(int i=0;i<n;i++)
			{
				a[i] = rand() % 1000;
			}
	}

void copiaza(int *b,int *a, int n)
{
	for(int i=0;i<n;i++)
	{
		b[i]=a[i];
	}
}


void afisareNr(int *a)
{
	for(int i=0;i<DIM_MAX;i++)
	{
		printf("A[%d]=%d\n",i,a[i]);
	}
}

void calculate(int *b,int *a, int n, FILE *fp)
{
		copiaza(b,a,n);
		bubbleSort(b,n);
		sum_bubble = cmp_bubble + atr_bubble;

		copiaza(b,a,n);
		insertionSort(b,n);
		sum_insertion = cmp_insertion + atr_insertion;

		copiaza(b,a,n);
		selectionSort(b,n);	
		sum_selection = cmp_selection + atr_selection;

		fprintf(fp,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,cmp_bubble,atr_bubble,sum_bubble,cmp_insertion,atr_insertion,sum_insertion,cmp_selection,atr_selection,sum_selection);
}

int main()
{

	FILE *fp;
	FILE *f1,*f2;
	f1 = fopen("BestCase.csv","w");
	f2 = fopen("WorstCase.csv","w");
	fp = fopen("MediumCase.csv","w");
	int n=100;

	
	int *a;
	int *b;
	
	
	printf("Starting with %d\n",n);
	while(n<=DIM_MAX)
	{
		printf("%d\n",n);
		a = (int *) malloc(n * sizeof(int));
		b = (int *) malloc(n * sizeof(int));
		for(int i=0;i<5;i++)
		{

			genereazaMedium(a,n);

			copiaza(b,a,n);
			bubbleSort(b,n);
			copiaza(b,a,n);
			insertionSort(b,n);
			copiaza(b,a,n);
			selectionSort(b,n);
			

			
		}

		cmp_bubble = cmp_bubble/5;
		atr_bubble = atr_bubble/5;
		sum_bubble = cmp_bubble + atr_bubble;
		cmp_insertion = cmp_insertion/5;
		atr_insertion = atr_insertion/5;
		sum_insertion = cmp_insertion + atr_insertion;
		cmp_selection = cmp_selection/5;
		atr_selection = atr_selection/5;
		sum_selection = cmp_selection + atr_selection;

	fprintf(fp,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,cmp_bubble,atr_bubble,sum_bubble,cmp_insertion,atr_insertion,sum_insertion,cmp_selection,atr_selection,sum_selection);
	

	
		reset();

		genereazaBest(a,n);
		calculate(b,a,n,f1);
		
		reset();

		genereazaWorst(a,n);
		calculate(b,a,n,f2);

		reset();
	
		free(a);
		free(b);

	n=n+100;
	

    
	}


	
	


	fclose(fp);
	fclose(f1);
	fclose(f2);

	return 0;

}






