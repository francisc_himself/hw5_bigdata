/*Assign 7

***ANONIM*** ***ANONIM*** Razvan
Grupa ***GROUP_NUMBER***

Cerinta : implementati algoritmi liniari de transformare intre cele 3 modalitati de reprezentare ale 
arborilor multicai : cu vector de tati, cu noduri cu mai multi fii sau cu noduri binare cu pointeri spre 
fiu si spre primul frate

Vom folosi 2 structuri, una "multi-nod" si una "nod_binar" si alocam memoria necesara.

La prima transformare : vector -> arbore multicai , vom parcure vectorul si construim fiecare nod in functie
de pozitia si valoarea pe care suntem in acest vector. Radacina are valoarea -1.

La a doua transformare: arbore multicai - arbore binar, vom folosi o functie recursiva care parcurge nodurile, 
verifica daca au fii, trecand la fii in caz afirmativ sau mergand la frati in caz negativ. Aceasta functie 
va seta si pointerii spre fiu respectiv frate pentru nodurile din structura de tip arbore binar.

Eficienta : O(n)

In main vom genera un vector de tati, pe care il vom transforma in arbore multicai, iar apoi acest arbore
va fi transformat intr-unul binar.


*/


#include <conio.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//nod de arbore multicai
typedef struct Multi_node {
	int key;
	int count;
	struct Multi_node *child[50];
}multi_nod;


//nod al arborelui binar
typedef struct bnode{
    int key;
    bnode* st;
    bnode* dr;
}nod_binar;


multi_nod *createMN(int key)
{
	multi_nod *x = (multi_nod*) malloc(sizeof(multi_nod));
	x->key=key;
	for (int i=0; i<40; i++)
	{
		x->child[i] = (multi_nod *)malloc(sizeof(multi_nod));
		x->child[i]->key=-1;
	}
	x->count=0;

	return x;
}

nod_binar *createBN(int key)
{
    nod_binar *x; 
	x = (nod_binar*) malloc(sizeof(nod_binar));
    x->key=key;
    x->st = (nod_binar*) malloc(sizeof(nod_binar));
    x->dr = (nod_binar*) malloc(sizeof(nod_binar));
    //x->st=NULL;
  //  x->dr=NULL;


    return x;
}

void insertMN(multi_nod *parent, multi_nod *child)
{
	parent->child[parent->count]=child;
	parent->count++;
}

//transformare din vector de tati in arbore multi-cai
multi_nod *transf(int t[], int size)
{
	multi_nod *root;
	multi_nod *nodes[50];

	for(int i=0; i<size; i++)
		nodes[i]=createMN(i);

	for(int i=0;i<size;i++)
		if (t[i]!=-1)
			insertMN(nodes[t[i]], nodes[i]);
		else
			root=nodes[i];

	return root;
}

void setkey(nod_binar *q, int k)
{
	q->key=k;
}

//transformare nod multi-cai in nod binar
nod_binar* transf2(multi_nod *q, multi_nod *par, int i)
{
    nod_binar *nodBinar;

    if(q->key!=-1)
    {
        nodBinar = (nod_binar*)malloc(sizeof(nod_binar));
        nodBinar->key=q->key;

        if (q->count!=0)
            nodBinar->st = transf2(q->child[0], q, 0);
        else
            nodBinar->st = NULL;

        if (par==0)
			 nodBinar->dr=NULL;
		else
            nodBinar->dr = transf2(par->child[i+1], par, i+1);
           
    }
    else
        nodBinar=NULL;

    return nodBinar;

}


void pretty_print(multi_nod *root, int nivel=0)
{
	for(int i=0;i<nivel;i++)
	{
		printf("   ");
	}

	printf("%d\n", root->key);

	for(int i=0;i<root->count;i++)
		pretty_print(root->child[i], nivel+1);
}

void pretty_print2(nod_binar *x, int dim)
{
    int i;

	if (x==NULL)
		return;
	
	pretty_print2(x->st, ++dim);

	for(i=0;i<dim;i++)
		printf("   ");

	printf("%d \n", x->key);

	pretty_print2(x->dr, dim);
}


int main()
{
	int t[]={9,5,5,9,-1,4,4,5,5,4,2};
	int size=sizeof(t)/sizeof(t[0]);
	multi_nod *root = createMN(0);
		root=transf(t, size);

	//printf("%d", root->child[1]);

	//pretty_print(root,  0);

	//printf("%d", root->key);

    nod_binar *rad = transf2(root, 0, 0);
    pretty_print2(rad, 0);

	getch();
}
