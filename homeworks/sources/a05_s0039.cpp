#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Declaram variabilele
int hash_lungime=9967;
int hhash_table[9967];
int hash_max=0;

float succes_total,fail_total,suces_max,fail_max;
float nr_acceses=0;
int nr_el_search=3000;
//Declaram functii
/////////////////////////////////////////////////////////////////////////////////////////////////////
/**
Procedura ce intializeaza valoarea tableului Hash la -1
In tabelul Hash vom insera doar valori pozitive deci pentru un slot NULL voc considera valoarea -1
parameters:
> int n-> Pana la care slot punem -1
*/
void nullTheHashTable(int n)
{
    int i;
    for (i=0;i<n;i++)
    {
        hhash_table[i]=-1;
    }
    hash_max=0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
/**
Functia returneaza o noua pozitie. Pe aceasta pozitie returnata vom incerca sa inseram de fiecare data elementul
$value in tabelul de Hash
Used in: addToHashTable , searchElement
parameters:
> value -> un intreg care dorim sa incercam sa adaugam la tabelul de Hash
> s -> lungimea tabelului de hash
> j -> numarul incercarii de introducere in tabel. Cu numarul de incercari creste cu atat j e mai mare
*/
int tryNextHashFunction(int value,int s,int j)
{
    return (value+j*j) % s;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
Functia incearca sa adauge valoarea data ca parametru in tabelul hash si returneaza, in caz de succes indexul la care s-a
introdus valoarea.In cazul in care introducerea nu a avut succes, returneaza -1
> value -> valoarea ce urmeaza sa fie introdus
> s -> Lungimea HashTable-ului
*/
int addToHashTable(int value,int s)
{
    int insert_index;
    int number_of_tries=0;
    while(number_of_tries<s)
    {
        insert_index=tryNextHashFunction(value,s,number_of_tries);
        if (hhash_table[insert_index]==-1)
        {
            hhash_table[insert_index]=value;
            if (value>hash_max)
            {
                hash_max=value;
            }
            return insert_index;
        }
        number_of_tries++;
    }
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////
/**
Functie de cautare a unui element in HashTable.Returneaza index-ul pe care se afla elementul cautat in cazul in care
exista in tabelul de hash. Altfel returneaza -1
parameters:
> value ->Valoarea cautata in Hash
> s -> Lungimea Hash
*/
int searchElement(int value,int s)
{
    int nr_tries;
    int index_where_should_be;
    while(nr_tries<s)
    {
        nr_acceses++;
        index_where_should_be=tryNextHashFunction(value,s,nr_tries);
        if(hhash_table[index_where_should_be]==value)
        {
            return index_where_should_be;
            break;
        }
        if(hhash_table[index_where_should_be]==-1)
        {
            return -1;
            break;
        }
        nr_tries++;
    }
    return -1;
}
//////////////////////////////////////////////////////////////////////////////////////
/**
Procedura de afisare a Hash-ului. Folosit la testare.
parametrii:
> s -> Lungimea Hash-ului
*/
void afisareHashTable(int s)
{
    int i;
    for(i=0;i<s;i++)
    {
        if(hhash_table[i]==-1)
        {
            printf("H[%d]= _empty\n",i);
        }
            else
        {
            printf("H[%d]= %d \n",i,hhash_table[i]);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////
/**
Procedura de testare
*/
void test()
{
    int n,k;
    n=8;
    nullTheHashTable(n);
    k=addToHashTable(16,n);
    k=addToHashTable(0,n);
    k=addToHashTable(32,n);
    k=addToHashTable(15,n);
    k=addToHashTable(31,n);
    afisareHashTable(n);
    printf("\n");
    if(searchElement(16,n)!=-1)
    {
        printf("16 Gasit! \n");
    } else {printf("16 Nu e gasit! \n");}
    if(searchElement(15,n)!=-1)
    {
        printf("15 Gasit \n");
    } else {printf("15 Nu e gasit! \n");}
    if (searchElement(23,n)!=-1)
    {
        printf("23 Gasit! \n");
    } else {printf("23 Nu e gasit! \n");}
}
////////////////////////////////////////////////////////////////////////////////////////////
/**
Process de creare a Hash-ului cu un factor de umplere dat ca parametru
parametrii
> fill_factor -> factorul de umplere
*/
void creareHashTableWithFillFactor(float fill_factor)
{
    int k;
    int n=9967; // Lungimea Hash-ului
    nullTheHashTable(n);
    int nr_elements_to_add=(fill_factor*n);
    int nr_elements_added=0;
    srand(time(NULL));
    while (nr_elements_added<nr_elements_to_add)
    {
        k=rand();
        if(addToHashTable(k,n)!=-1)
        {
            nr_elements_added++;
         //   printf("P.%d %d was added!\n",nr_elements_added,k);
        }
    }

}
///////////////////////////////////////////////////////////////////////////////

void createStatistics()
{
    int i,value,index,search_handler,k,nr_elements_added;
    suces_max=0;
    fail_max=0;
    fail_total=0;
    succes_total=0;
    float alfa=0.8;
    srand(time(NULL));
    FILE *f;
    f=fopen("rez_hash.csv","w");
    fprintf(f,"Filing Factor,Avg.Effort Found,Max.Effort Found,Avg.Effort Not Found,Max.Effort Not Found\n");
    printf("Alfa= %0.2f \n",alfa);
    while (alfa!=1)
    {
    suces_max=0;
    fail_max=0;
    fail_total=0;
    succes_total=0;
    creareHashTableWithFillFactor(alfa);
    //Succes Searches
    for (i=0;i<nr_el_search/2;i++)
    {
        nr_acceses=0;
        index=rand() % hash_lungime;
        if (hhash_table[index]!=-1)
        {
            search_handler=searchElement(hhash_table[index],hash_lungime);
            if (suces_max<nr_acceses)
            {
                suces_max=nr_acceses;
            }
            succes_total=succes_total+nr_acceses;
        }
    }
    //Fail Searches
    for (i=0;i<nr_el_search/2;i++)
    {
        nr_acceses=0;
        value=hash_max+rand() % 10000;
        search_handler=searchElement(value,hash_lungime);
            if (fail_max<nr_acceses)
            {
                fail_max=nr_acceses;
            }
            fail_total=fail_total+nr_acceses;
    }
    fprintf(f,"%0.2f,%0.2f,%0.2f,%0.2f,%0.2f\n",alfa,succes_total/1500,suces_max,fail_total/1500,fail_max);
    if (alfa==(float)0.8)
    {
        alfa=0.85;
        printf("Alfa= %0.2f \n",alfa);
    } else
      {
          if (alfa==(float)0.85)
          {
              alfa=0.90;
              printf("Alfa= %0.2f \n",alfa);
          }
          else
          {
              if (alfa==(float)0.90)
              {
                  alfa=0.95;
                  printf("Alfa= %0.2f \n",alfa);
              }
              else
              {
                  if(alfa==(float)0.95)
                  {
                      alfa=0.99;
                      printf("Alfa= %0.2f \n",alfa);
                  }
                  else
                  {
                      alfa=1;
                      printf("Alfa= %0.2f \n",alfa);
                  }
              }
          }
      }
    }
    fclose(f);
}
int main()
{

    test();
    //createStatistics();
    printf("Finish!\n");
    return 0;
}
