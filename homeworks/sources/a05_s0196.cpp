/*
implementare tabele hashing
*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int dim;
int nr_verif;
int H[20000];       //tabela de dispersie
FILE *f;
int hash(int key,int i)   //functia de calculare a pozitiei in functie de o cheie
{
	int aux=key % dim;
	return (aux+i+3*i^2)% dim;
}

int inserare(int key)  //functia ce insereaza un nou element in tabela de dispersie
{
	int i=0,j;
	do
	{
		j=hash(key,i);
		nr_verif++;
		if(H[j]==-1)
		{
			H[j]=key;
			return j;
		}
		else
			i++;
	}while(i< dim);
	return -1;
}

int cautare(int key)//functia de cautare a unei chei
{
	int i=0,j;
	do
		{
			j=hash(key,i);
			nr_verif++;
			if( H[j]==key )
			{
				return j;
			}
			else
				i++;
		}
	while( (i<dim) && (H[j]!=-1) );
	return -1;
}

void generare(int nr,double fUmplere)   //param: nr elem. si factorul de umplere 
{
	for(int i=0; i<20000; ++i ) H[i]=-1; //intializam toate valorile din tabela de dispersie la -1
	dim=nr;
	int aux,i=0,rez,j;
	double fUmplereTemp;
	do
	{								//inseram pana se ajunge la procentul de alfa% umplere
		aux=rand()%1000+1;          //se generaza o cheie aleator
		rez=inserare(aux);			//se memoreaza in variabila rez rezultatul valorii de inserare care poate fi -1 insFaracautCuSucces sau alta val in caz de cautCuSucces
		i=i+1;						//se memoreaza numarul de inserari
		fUmplereTemp=(double)i/(double)nr;				 //calculeaza factorul de umplere
	}while(fUmplereTemp<fUmplere && rez!=-1);		//insereaza pana cand factorul de umplere calculat este mai mic decat cel trimis ca si parametru
				//daca toate pozitiile sunt ocupate inseram h[j]=j
	if(rez==-1)
	{
		j=0;
		do
		{
			if(H[j]==-1)
			{
				H[j]=j;   //se insereaza pe pozitia j
				i++;
				fUmplereTemp=(double)i/(double)nr;
			}
			j=j+1;
		}while(fUmplereTemp<fUmplere && j<dim);
	}
}
int main()
{
	srand(time(NULL));
	double factor[]={0.8,0.85,0.9,0.95,0.99}; //valorile factorului de umplere
	fopen_s(&f,"out.txt","w");     //fisierul unde se vor pune rezultatele
	int cautCuSucces,insFaracautCuSucces,alfa,k,j,i,index,numar,aux1,aux2;
	for(alfa=0; alfa<5; alfa++ )
	{
		for(k=1;k<=100;k++)
		{
			cautCuSucces=0;
			insFaracautCuSucces=0;
				generare(100*k,factor[alfa]);   //se apeleaza procedura de generare
				aux1=0; aux2=0;
				for(i=0;i<20;++i)
				{
					numar=rand()%1000+1;     //se genereaza un numar pentru a fi inserat
					nr_verif=0;		    //nr_verifle iau valoare 0
					inserare(numar);		    //se insereaza numarul
					aux1=aux1+nr_verif;	//se memoreaza in variabila aux1 nr de verificaro
					nr_verif=0;			//din nou nr de nr_verif ia valoare 0 pentru a vedea numarul de nr_verif la cautarea unui nr
					index=cautare(numar);		//se cauta numarul in tabela ,numarul generat adineauri
					aux2=aux2+nr_verif;	// se memoreaza nr verficari
					H[i]=-1;
				}
				aux1=aux1/20;
				aux2=aux2/20;
				cautCuSucces=cautCuSucces+aux2;      //cautari cu cautCuSucces
				insFaracautCuSucces=insFaracautCuSucces+aux1; //inserari fara cautCuSucces

			fprintf(f,"%1.2lf %d %d %d\n",factor[alfa],100*k,cautCuSucces,insFaracautCuSucces);
		}
	}
	fclose(f);
}
