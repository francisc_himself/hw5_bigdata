#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include "time.h"
Profiler profiler("demo");

typedef struct tip_nod
			{ 
				int key;
                int size;
				struct tip_nod *left;
				struct tip_nod *right;
				struct tip_nod *parent;
		    } TIP_NOD;

//int A[10000]; //sirul ce trebuie permutat
TIP_NOD *rad;
int cmp,atr,n,m;

int niv;

//builds a tree in the interval s-d and it is stabilized the dimension of the right and left branch
TIP_NOD *buildHeap(int s, int d, int dim, int kk)
{ 
	TIP_NOD *p;
	int n=sizeof(TIP_NOD);
	int dim1=0,dim2=0;
	profiler.countOperation("Josephus", kk, 1);
	if (s>d)
	{ 
			dim=0;
   			return NULL;
	}
	else
	{
		p=(TIP_NOD*) malloc(n);
		p->key=(s+d)/2;		
		p->left = NULL;		
		p->right = NULL;		
		p->size=d-s+1;        
		profiler.countOperation("Josephus", kk, 4);
		if(s<d)
		{						
			//se merge recursiv in constructia arborelui pe cele 2 ramuri
			profiler.countOperation("Josephus", kk, 1);
			p->left=buildHeap(s,(s+d)/2-1,dim1,kk);
			if(p->left != NULL)
			{
				p->left->parent = p;  
				profiler.countOperation("Josephus", kk, 1);
			}
			p->right=buildHeap((s+d)/2+1,d,dim2,kk);
			if(p->right != NULL)
			{
				p->right->parent = p; 
				atr++;//
				profiler.countOperation("Josephus", kk, 1);
			}
			cmp+=2;
			profiler.countOperation("Josephus", kk, 2);
			niv++;
			atr+=2;
			profiler.countOperation("Josephus", kk, 2);
		}
	}
	dim=p->size;//se retine dimensiunea
	return p;
}

TIP_NOD *os_select (TIP_NOD *x, int i, int n) // i = cate numeri
{
	int r=1;
	if(x->left!=NULL)
	{
		r=(x->left->size)+1;
		profiler.countOperation("Josephus", n, 2);
	}
	if(i==r){
		profiler.countOperation("Josephus", n, 1);
		return x;
	}
	else if (i<r)
	{
		profiler.countOperation("Josephus", n, 1);
		return os_select(x->left, i, n);
	}
	else 
	{
		profiler.countOperation("Josephus", n, 1);
		return os_select(x->right, i-r, n);
	}
}

void decrease_size (TIP_NOD *x, int n)
{
	while(x!=rad)
	{
		x=x->parent;
		x->size--;
		profiler.countOperation("Josephus", n, 4);
	}
}

// returneaza succesorul stang folosit la rotatie 
TIP_NOD *treemin(TIP_NOD *x, int n)
{
	cmp++;
	while(x->left!=NULL)
	{	
		x=x->left;
		profiler.countOperation("Josephus", n, 3);
	}
	return x;
}

TIP_NOD *succesor (TIP_NOD *x, int n)
{
	profiler.countOperation("Josephus", n, 1);
	if(x->right!=NULL)
		return treemin(x->right, n);
	TIP_NOD *y;
	y=x->parent;
	profiler.countOperation("Josephus", n, 1);
	while ((y!=NULL) && (x==y->right))
	{		
		profiler.countOperation("Josephus", n, 2);
		x=y;
		y=y->parent;
		profiler.countOperation("Josephus", n, 1);
	}
	//printf("\n succesor in succesor: %d\n", y->key);
	return y;
}

//deletes a node from tree
void deleteNode(TIP_NOD **arb, TIP_NOD *z, int n)
{ 
	TIP_NOD *y,*x;
	if ((z->left==NULL)||(z->right==NULL))
	{ 
		profiler.countOperation("Josephus", n, 2);
		y=z; 
	}
	else
	{
		y=succesor(z, n); 
		profiler.countOperation("Josephus", n, 1);
	}
	if (y->left!=NULL)
	{ 
		x=y->left; 
		profiler.countOperation("Josephus", n, 1);
	}
	else
	{	 
		x=y->right; 
		profiler.countOperation("Josephus", n, 1);
	}
	if (x!=NULL)
	{   
		x->parent=y->parent;
		profiler.countOperation("Josephus", n, 1);
	}

	decrease_size(y, n);
	if (y->parent==NULL)
		*arb=x;
	else
	{	
		if (y==y->parent->left)
		{
			y->parent->left=x; 
			profiler.countOperation("Josephus", n, 2);
		}
		else
		{ 
			y->parent->right=x; 
			profiler.countOperation("Josephus", n, 1);
		}
	}
	if (y!=z)
	{
		z->key=y->key;
		profiler.countOperation("Josephus", n, 1);
	}
	free(y);
}

void pretty_print (TIP_NOD* root, int depth)
{
    if(root != NULL)
	{
		pretty_print(root->left, depth+1);
		for(int i=0; i<depth; i++)
		{
			printf("	");
		}
		printf("%d (%d)\n", root->key, root->size);
		pretty_print(root->right, depth+1);
	}
}

void josep (int n, int m)
{
	//TIP_NOD *rad;
	int nr=n;
	rad=buildHeap(1,n,0,n);
	rad->parent=NULL;
	profiler.countOperation("Josephus", n, 1);
	int i=m;
	TIP_NOD *x;
	if(n<50)
			pretty_print(rad, 0);
	
	while((rad->left!=NULL) || (rad->right!=NULL))
	{
		profiler.countOperation("Josephus", n, 2);
		x=os_select(rad, i, n);
		profiler.countOperation("Josephus", n, 1);
		if(n<50) 
				printf("\ndelete node: %d  size: %d\n", x->key, x->size);
		deleteNode(&rad, x, n);
		
		if(n<50)
				pretty_print(rad, 0);
		nr=nr-1;

		 if (nr)
	     {
			i=(i+m-1)%nr;
			if (i==0)
				i=nr;
	     }
	}
	if(n<50) 
			printf("\ndelete node: %d  size: %d\n", rad->key, rad->size);
	free(rad);
}

void main()
{
	josep(7,3);
	int i;

	for(i=100; i<=10000; i+=100)
	{
		cmp=atr=0;
		m=i/2;
		josep(i,m);
	}
	profiler.createGroup("Josep", "Josephus");
	profiler.showReport();
	getch();
}