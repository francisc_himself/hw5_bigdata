// ***ANONIM*** ***ANONIM***-Wilhelm
// Grupa ***GROUP_NUMBER***

//OBS: In cazul mediu Quicksort-ul este mai eficient decat Heapsort-ul, amandoua metodele avand eficienta O(n log n).
// In cazul Quicksort-ului am mai studiat cazurile worst si best. In cazul favorabil sirul este deja sortat eficienta fiind tot O(n log n).
// In cazul defavorabil am ales ca pana la primul pivot, adica la jumatatea sirului elementele sa fie descrescatoare, iar de la pivot, 
// crescatoare, rezultand o eficienta de O(n^2).

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int naHeapSort;
int naQuickSort;
int ncHeapSort;
int ncQuickSort;

void randomMare(int A[],int n)
{
	for(int i=0;i<n;i++)
	{
		A[i] = rand() % 10000;
		// printf("%d ",A[i]);
	}
}

void randomBest(int A[], int n)
{
	A[0] = rand() % 10;
	for(int i=0;i<n;i++)
	{
		A[i] = A[i-1] + rand() % 10;
	}
}

void randomWorst(int A[],int n)
{
	A[0] = rand() % 10000;
	for(int i=0;i<n/2;i++)
	{
		A[i] = A[i-1] - rand() % 10;
	}

	for(int i=n/2;i<n;i++)
	{
		A[i] = A[i-1] + rand() % 10;
	}
}

int parinte (int i)
{
	return i/2;
}

int st (int i)
{
	return 2*i;
}

int dr (int i)
{
	return 2*i+1;
}

// Pretty Print
void pretty_print(int A[],int n)
{
	int i, j, pow=2, level=1, nr=5; 
	
	printf("        %d\n\n ",A[1]); 
	
	for(i=2;i<n;)
	{
		level=pow; 
		for(j=0;j<nr;j++) 
		printf(" "); 
		
		while(i<n && level>0)
		{ 
			printf(" %d ",A[i]);
			i++; 
			level--; 
		}
	
	printf("\n\n");	
	pow = pow * 2; 
	nr--; 
	}
}

// Bottom - up
void reconstructie_heap(int A[],int n, int i, int dim_heap)
{
	int s,d,ind,aux;

	s = st (i);
	d = dr (i);

	ind = i;

	if (s<=dim_heap && A[s]> A[ind])
		ind = s;
	if (d<=dim_heap && A[d]> A[ind])
		ind = d;

	ncHeapSort += 2;

	if (ind != i)
	{
		aux = A[i];
		A[i]=A[ind];
		A[ind] = aux;

		naHeapSort += 3;

		reconstructie_heap (A, n, ind, dim_heap);
	}	
}

void construieste_heap_bu (int A[], int n,int dim_heap)
{
	int i;
	for (i=dim_heap/2; i>=0; i--)
		reconstructie_heap (A, n, i, dim_heap);
}

void copiere (int A[], int B[], int n)
{
	for (int i=1; i<=n; i++)
		B[i] = A[i];
}

// Heap - Sort

void heap_sort (int A[], int n)
{
	int aux;
	int dim_heap=n;
	construieste_heap_bu(A,n,dim_heap);
	for (int i=n; i>=1; i--)
	{
		aux = A[0];
		A[0] = A[i];
		A[i] = aux;

		naHeapSort += 3;

		dim_heap = dim_heap - 1;
		reconstructie_heap (A,n,0,dim_heap);
	}
}

// Quick - Sort

void quick_sort(int A[], int n, int st, int dr)
{
	int i, j, pivot, aux;
	i = st;
	j = dr;

	pivot = A[(st+dr)/2];

	naQuickSort++;

	do
	{
		if(!(A[i]<pivot)){
			ncQuickSort++;
		}else{
			while (A[i]<pivot)
			{
				ncQuickSort++;
				i++;
			}
		}
		
		if(!(A[j]>pivot)){
			ncQuickSort++;
		}else{
			while (A[j]>pivot)
			{
				ncQuickSort++;
				j--;
			}
		}

		if (i<=j)
		{
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;

			naQuickSort += 3;

			i++;
			j--;
		}
	}while (i<=j);

		if (st<j)
			quick_sort(A,n,st,j);
		if (i<dr)
			quick_sort(A,n,i,dr);
}

void cazul_mediu ()
{
	int A[10001],B[10001],C[10001];
	int n;
	int naHeapSort2;
	int naQuickSort2;
	int ncHeapSort2;
	int ncQuickSort2;

	FILE *f1;
	f1 = fopen ("heap_sort.txt","w");
	
	FILE *f2;
	f2 = fopen ("quick_sort.txt","w");

	for (int n=100; n<=10000; n=n+100)
	{
		naHeapSort2 = naQuickSort2 = 0;
		ncQuickSort2 = ncHeapSort2 = 0;

		for (int k=1; k<=5; k++)
		{
			randomMare(A,n);
			printf("%d ",n);

			//heap sort
			naHeapSort = 0;
			ncHeapSort = 0;

			copiere(A,C,n);
			heap_sort(C,n);
			naHeapSort2 += naHeapSort;
			ncHeapSort2 += ncHeapSort;

			//quick sort
			naQuickSort = 0;
			ncQuickSort = 0;

			copiere(A,C,n);
			quick_sort(C,n,0,n-1);
			naQuickSort2 += naQuickSort;
			naQuickSort2 += ncQuickSort;
		}

		naQuickSort/=5;
		ncQuickSort/=5;
		naHeapSort/=5;
		ncHeapSort/=5;

		fprintf (f1,"%d %d %d %d\n",n,naHeapSort,ncHeapSort,naHeapSort+ncHeapSort);
		fprintf (f2,"%d %d %d %d\n",n,naQuickSort,ncQuickSort,naQuickSort+ncQuickSort);
	}

	fclose(f1);
	fclose(f2);
}

void cazul_defavorabil_quick()
{
	int A[10001];
	int n;

	FILE *f1;
	f1 = fopen ("quick_sort_worst.txt","w");

	for (int n=100; n<=10000; n=n+100)
	{
		naQuickSort = 0;
		ncQuickSort = 0;

		randomWorst(A,n);
		printf("%d ",n);

		//quick sort
		quick_sort(A,n,0,n-1);
	
		fprintf (f1,"%d %d %d %d\n",n,naQuickSort,ncQuickSort,naQuickSort+ncQuickSort);
	}

	fclose(f1);
}

void cazul_favorabil_quick()
{
	int A[10001];
	int n;

	FILE *f1;
	f1 = fopen ("quick_sort_best.txt","w");

	for (int n=100; n<=10000; n=n+100)
	{
		naQuickSort = 0;
		ncQuickSort = 0;

		randomBest(A,n);
		printf("%d ",n);

		//quick sort
		quick_sort(A,n,0,n-1);
	
		fprintf (f1,"%d %d %d %d\n",n,naQuickSort,ncQuickSort,naQuickSort+ncQuickSort);
	}

	fclose(f1);
}


int main ()
{
	cazul_mediu ();

	cazul_defavorabil_quick();

	cazul_favorabil_quick();

	/*
	int A[10];

	randomCresc(A,10);
	//heap_sort(A,10);
	quick_sort(A,10,0,9);

	for(int i=0;i<10;i++)
	{
		printf(" %d",A[i]);
	}

	//pretty_print(A,10);*/

	getch();
	return 0;
}