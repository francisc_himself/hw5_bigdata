/**
Assignment No. 2: Analysis & Comparison of Bottom-up and Top-down Build Heap Approaches

Name: ***ANONIM*** ***ANONIM*** Mihai
Group: ***GROUP_NUMBER***

Implementation:
You are required to implement correctly and efficiently two methods for building a heap, namely the bottom-up and the top-down strategies.
You may find any necessary information and pseudo-code in your course notes, or in the book1:
	Bottom-up: section 6.3 (Building a heap)
	Top-down: section 6.5 (Priority queues) and problem 6-1 (Building a heap using insertion)

Start date: 15.03.2013
Finish date: 18.03.2013

Conclusions:

These 2 constructions are almost the same, but have some differences.

	In average case, we are getting the same performance, no matter what approach we implement: the Top Down, or the Bottom Up. 
	The number of operations remains aproximately the same.
	We can mention the O(n)`s: we have O(n)= n lg n.
	
	In worst case, the Bottom Up approach is by far better in performance(as number of operations) than the Top Down. Even so, Top Down has its advantages. 
	It is particulary good when we want to implement a heap which inserts elements on top, for example a Priority Queue.


**/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#define MAX 1000
#define MIN 100
#define STARTER 100
#define ENDER 10000

int Left(int i) {
	return 2*i+1;
}

int Right(int i) {
	return 2*i+2;
}

int Parent(int i) {
	return (i-1)/2;
}

void MaxHeapify(int *x, int n, int k, int* opNr) {   //Buttom Up 
	int l=Left(k);
	int r=Right(k);
	int largest=k;
	int aux;
	(*opNr)++;
	if (l<=n && x[l]>x[k]) {
		largest=l;
	}
	else {
		largest=k;
	}
	(*opNr)++;
	if (r<=n && x[r]>x[largest]) {
		largest=r;
	}

	if (largest != k) {
		aux=x[k];
		x[k]=x[largest];
		x[largest]=aux;
		(*opNr)++;(*opNr)++;(*opNr)++;
		MaxHeapify(x,n,largest, opNr);
	}
}

void InsertInHeap(int *x, int k, int *opNr){    //Top Down
	int aux;
	while ((k>0) && (x[k]>x[Parent(k)])) {
		aux=x[k];
		x[k]=x[Parent(k)];
		x[Parent(k)]=aux;
		(*opNr)+=4;
		k=Parent(k);
	}
	(*opNr)++;
}

void Build(int *x,int n, int* opNr) {
	int i;
	for (i=1;i<n;i++) {
		InsertInHeap(x,i,opNr); //Top down

	}
}

void BuildH(int *x,int n, int* opNr) {
	int i;
	for (i=0; i<n/2; i--) {

		MaxHeapify(x,n,i,opNr); //Bottom up.
	}
}

int BuildHeap(int* x, int n, int *opNr)
{	
	Build(x,n,opNr);
	return 0;
}

int BuildHeapH(int* x, int n, int *opNr)
{	
	BuildH(x,n,opNr);
	return 0;
}

void generate(int* a,int n){
	int i=0;
	for (i=0;i<n;i++) {
		a[i]=rand()%(MAX-MIN)+MIN;
	}
}

void generateWorst(int* a,int n){
	int i=0;
	for (i=0;i<n;i++) {
		a[i]=n-i+1;
	}
}

void PrettyPrint(int *x,int n, int k, int level)
{
	if (k<n){
		PrettyPrint(x,n,Left(k),level+1);
		int i=0;
		for (i=0;i<level;i++) {
			printf("    ");
		}
		printf("%d\n",x[k]);
		PrettyPrint(x,n,Right(k),level+1);
	}
}

int main()
{	

	/**
	 **
	 ** HARD - CODED small-sized input:
	 ** Prints the output of each build heap strategy using a pretty-print type procedure � display the heap as a tree.
	 **
	 **/
	srand(time(NULL));
	int n=10;
	int y=0;
	int x[10];
	int z[10];
	//Sample input for Top Down.
	x[0]=0;
	x[1]=7;
	x[2]=3;
	x[3]=9;
	x[4]=1;
	x[5]=22;
	x[6]=14;
	x[7]=18;
	x[8]=77;
	x[9]=4;

	//Sample input for Button Up.

	z[0]=0;
	z[1]=7;
	z[2]=3;
	z[3]=9;
	z[4]=1;
	z[5]=22;
	z[6]=14;
	z[7]=18;
	z[8]=77;
	z[9]=4;

	int i;
	
	// Building Top Down.
	BuildHeap(x,n,&y);
	
	// Output as a vector:
	printf("Output as a vector:\n\n");
	for (i=0;i<n;i++) {
		printf("%d ", x[i]); 
	}
	printf("\n\n");
	// Output after Top Down.
	printf("\n\nOutput after Top Down:\n\n");
	PrettyPrint(x,n,0,0);
	// Building Buttom Up.
	BuildHeapH(z,n,&y);

	// Output after Buttom Up.
	printf("\n\nOutput after Buttom Up:\n\n");
	PrettyPrint(z,n,0,0);
	printf("\n Finished printing sample.\n\n");
	
	/****************************************************************/
	
	printf("\n Now counting number of operations performed.\n");
	
		/*********************************  Generating for Top Down  ************************************************/
	int aux_op;
	int j;
	FILE* fd;
	if ((fd=fopen("grafic1.csv","w"))==0) {printf("opening output file failed.");}
	int opNorm=0;
	int opWorst=0;
	for (n=STARTER;n<ENDER;n+=100) {
		int* x=(int*)malloc(n*sizeof(int));

		aux_op=0;
		for (j=0;j<5;j++){
			generate(x,n);
			//printf("Sorting AVG for n=%d\n",n);
			opNorm=0;
			BuildHeap(x,n,&opNorm);
			aux_op=(aux_op*j+opNorm)/(j+1);
		}
		//printf("\n%d",opNorm);
		generateWorst(x,n);
		//printf("Sorting WORST for n=%d\n",n);
		opWorst=0;
		BuildHeap(x,n,&opWorst);
		//printf("\n%d",opWorst);
		fprintf(fd,"%d,%d,%d\n",n,aux_op,opWorst);
		
		free(x);

	/*********************************  Generating for Buttom Up  ************************************************/
		
		int q;
		int aux_op2;
	FILE* fq;
	if ((fq=fopen("grafic2.csv","w"))==0) {printf("opening output file failed.");}
	int opNorm2=0;
	int opWorst2=0;
	for (n=STARTER;n<ENDER;n+=100) 
	{
		int* x=(int*)malloc(n*sizeof(int));

		aux_op2=0;
		for (q=0;q<5;q++)
		{
			generate(x,n);
			//printf("Sorting AVG for n=%d\n",n);
			opNorm=0;
			BuildHeapH(x,n,&opNorm2);
			aux_op=(aux_op2*j+opNorm2)/(j+1);
		}
		//printf("\n%d",opNorm2);
		generateWorst(x,n);
		//printf("Sorting WORST for n=%d\n",n);
		opWorst=0;
		BuildHeapH(x,n,&opWorst2);
		//printf("\n%d",opWorst2);
		fprintf(fd,"%d,%d,%d\n",n,aux_op2,opWorst2);
		
		free(x);
	}

	printf("\n Done.");
	getch();

	}
}