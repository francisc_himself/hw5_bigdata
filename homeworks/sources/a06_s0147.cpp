//***ANONIM*** ***ANONIM*** Florin
//***ANONIM*** ***GROUP_NUMBER***

//OBS : Eficienta:O(n log n)

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

typedef struct nod{
	struct nod *parinte; //parinte
	struct nod *st;     //nod stang
	struct nod *dr;     //nod drept
	int key;
	int marine;
}NOD;

int oujoseph[10001];  //pentru afisarea permutari Joseph

int nr_operatii;    //numarul de atribuiri si comparatii

//functia de construire a unui arbore binar de cautare echilibrat
nod *build_balanced_Tree (int st, int dr, int *dim){
	nod *p;

    int dim1 = 0;

	nr_operatii+=2;
	if (st>dr){
        *dim = 0;
        nr_operatii++;
   	    return NULL;
    }
	else{
	    nr_operatii += 5;
		p = (nod*)malloc(sizeof(nod));
	    p->key = (st+dr)/2;
		p->st = NULL;
		p->dr = NULL;
		p->marine = dr-st+1;

		if(st<dr){
			p->st = build_balanced_Tree(st,(st+dr)/2-1,&dim1);

			if(p->st != NULL){
			    p->st->parinte = p;
			    nr_operatii+=2;
            }

            p->dr = build_balanced_Tree((st+dr)/2+1,dr,&dim1);

            if(p->dr != NULL){
                p->dr->parinte = p;
                nr_operatii+=2;
            }

        }
    }

    *dim = p->marine;

    return p;
}
NOD *Tree_min(NOD *p){
	while (p->st != NULL){
		nr_operatii++;
		p = p->st;
	}

	return p;
}

NOD *Tree_succesor(NOD *p){
	NOD *y;

	if (p->dr != NULL){
		return Tree_min (p->dr);
	}
	nr_operatii+=2;
	y = p->parinte;

	while ((y != NULL) && (p == y->dr)){
		p = y;
		y = p->parinte;
		nr_operatii+=2;
	}

	return y;
}

//functie de stergere a unui nod p din arbore
void Tree_delete(nod **root, nod *p){
	NOD *y,*x,*z;
	nr_operatii+=3;
	if ((p->st == NULL) || (p->dr==NULL))
		y = p;
	else
		y = Tree_succesor(p);

	if (y->st != NULL)
			x = y->st;
	else
            x = y->dr;

	nr_operatii+=2;
	if (x != NULL)
		x->parinte = y->parinte;

	nr_operatii+=2;
	if (y->parinte == NULL)
		*root = x;
	else
	{
	    nr_operatii++;
		if (y == (y->parinte)->st)
			(y->parinte)->st = x;
		else
			(y->parinte)->dr = x;
	}

	nr_operatii+=2;
	if (y != p){
		p->key = y->key;
	}
	z=y->parinte;
	while (z != NULL){
		  nr_operatii+=2;
		  z->marine--;
		  z = z->parinte;
	}
	free(y);
}

//functia returneaza pointerul la nodul care contine a i-a cea mai mica key
NOD *OS_Select(NOD *p, int i){
	int r;

	nr_operatii+=2;
	if (p->st != NULL)
		r = ((p->st)->marine)+1;

	else
		r = 1;

	nr_operatii+=2;
	if (r == i)
		return p;
	else{
		if (i < r){
			return OS_Select(p->st,i);
		}
		else{
			return OS_Select(p->dr,i-r);
		}
	}
}


void josephus (int n, int m){
	nod *rad,*p;
	int k = n;
	int j = m;
    int h;
    int ind = 0;

    rad = build_balanced_Tree(1,k,&h);
    rad->parinte = NULL;
    j=1;

    for(k=n; k>=1; k--)
    {
        j = ((j + m - 2) % k)+1; //pozitia nodului care se va selecta se va schimba
          p=OS_Select(rad,j);
        oujoseph[ind] = p->key; //adaugam cheia nodului scos in sirul de ordine a scoaterii din arbore
  //      printf("%d ", p->key);
        ind++;
          Tree_delete(&rad,p); //sterge nodul scos din arbore

    }

}

void Rezultat(){
	int m;
	FILE *f;

	f = fopen ("josephus.txt","w");

	for (int n=100; n<=10000; n=n+100){

		m = n/2;

		nr_operatii = 0;

		josephus(n,m);

		fprintf(f,"%d %d\n",n, nr_operatii);
	}

	fclose(f);
}

int main(){
    josephus(7,3);

	printf("Josephus(7,3) :\n");

	for(int i=0;i<7;i++){
		printf("%d ",oujoseph[i]);
	}

	//Rezultat();
	getche();
	return 0;
}
