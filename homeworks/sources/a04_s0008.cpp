/****ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

Cerinta: Gasiti un algoritm de timp O(nlgk) pentru a interclasa k liste ordonate, unde n este numarul total de elemente din listele de intrare.
Indicatie: se uitilizeaza un heap.

HPush are complexitate O(logk). Repetand de k ori, prima bucla din interclasare are O(klogk).
HPop are complexitate O(logk). Repetand, pentru bucla a doua complexitatea devine O(nlogn).
Dominanta dintre acestea este nlogn, asadar algoritmul va avea O(nlogk).

Pe grafice se observa:
	- cand k variaza pentru un n constant -> O(logk), se vizualizeaza o curba logaritnica.
	- cand n variaza si k are valoare fixata -> O(n), liniara
	- cu cat k mai mic, cu atat se efectueaza mai putine operatii.

*/
#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <time.h>
#include<cstdlib>


#define DIM_MAX 3000

//definesc lista
typedef struct lista {
	int nr;
	struct lista *urm;
}TNod;
 
typedef struct heap{
	int x;
	int i;
}THeap;

//declaratii:
THeap H[DIM_MAX]; //declar heap-ul
TNod *cap[DIM_MAX];
TNod *prim=0, *ultim=0; //capetele listei interclasate
int n, dimH=0;

int op=0;

//crearea unui nod in lista: returnez pointer la nod;
TNod *creare(int cheie)
{
	TNod *p;
	p=(TNod *)malloc(sizeof(TNod));

	if(p==0)
	{
		printf("Eroare alocare spatiu!");
		return 0;
	}

	p->nr = cheie;
	p->urm=NULL;

	return p;
}

int parinte(int i)
{
	return i/2;
}

int Stanga(int i)
{
	return 2*i;
}

int Dreapta(int i)
{
	return (2*i+1);
}
//adaugare in heap
void HPush(int x, int i)
{
	THeap aux;
	dimH++;
	/********/op+=2;
	H[dimH].i = i;
	H[dimH].x = x;

	int j = dimH;
	/********/op++;
	while( j > 1 && H[parinte(j)].x > H[j].x )
	{
		/********/op+=4;
		aux = H[parinte(j)];
		H[parinte(j)] = H[j];
		H[j] = aux;
		j = parinte(j);
	}
}

void ReconstituieHeap(int i)
{
	int s, d, max;
	THeap aux;

	s=Stanga(i);
	d=Dreapta(i);

	/********/op++;
	if(s<=dimH && H[s].x < H[i].x)
		max = s;
	else max = i;

	/********/op++;
	if(d<=dimH && H[d].x < H[max].x)
		max = d;

	if(max!=i)
	{
		/********/op+=3;
		aux = H[i];
		H[i]=H[max];
		H[max]=aux;
		ReconstituieHeap(max);
	}
}
//stergere din arbore
THeap HPop()
{
	THeap x;
	/********/op+=2;
	x = H[1];
	H[1] = H[dimH];
	dimH--;
	ReconstituieHeap(1);
	return x;
	
}

int citesteLista(int i)
{
	int x;
	if(cap[i]==0)
		return 0;
	
	x = cap[i] -> nr;
	cap[i] = cap[i]->urm;
	return x;

}

void scrieLista(int x)
{
	TNod *p;

	if(prim==0)
	{
		prim = creare(x);
		ultim = prim;
	}
	else
		if(prim==ultim)
		{
			p = creare(x);
			prim->urm = p;
			ultim=p;
		}
	
	else
	{
		p = creare(x);
		ultim ->urm = p;
		ultim = p;
	}
}

//interclasarea listelor
void interclasare(int k)
{
	int i, x;
	THeap tmp;

	for(i=1;i<=k;i++)
	{
		/********/op++;
		x = citesteLista(i);
		if(x!=0)
			HPush(x, i);
	}
	while(dimH>0)
	{
		/********/op++;
		tmp = HPop();
		scrieLista(tmp.x);
		/********/op++;
		x = citesteLista(tmp.i);
		if(x!=0)
			HPush(x, tmp.i);
	}
		
		
}

//afisare lista
void afisare(TNod *prim)
{
	TNod *p;

	if(prim==0)
		printf("Lista este vida!");
	else{
		p=prim;
		while(p)
		{
			printf("%d ", p->nr);
			p=p->urm;
		}
		printf("\n");
	}
	getch();
}

int citire()
{
	int i, j=1, x, k;
	TNod *p, *u;

	printf("n=");
	scanf("%d", &n);

	printf("k=");
	scanf("%d", &k);

	for(i=1;i<=k;i++)
	{
		printf("elem %d in lista %d: ", 1, i);
		scanf("%d", &x);
		p = creare(x);
		cap[i] = p;
		u = p;
		for(j=2;j<=n;j++)
		{
			printf("elem %d in lista %d: ", j, i);
			scanf("%d", &x);
			p = creare(x);
			u -> urm = p;
			u = p;

		}
	}
	return k;
}

void testare()
{
	int k;
	k=citire();
	int i;
	for(i=1;i<=k;i++)
		afisare(cap[i]);

	interclasare(k);
	printf("INTERCLASAREA:\n");
	afisare(prim);
}

void genereazaListe(int n, int k, int caz)
{
	int i, j, val;
	int nr, sNr=0;
	TNod *p, *u;

		for(i=1;i<=k;i++)
		{
			val = rand();
			p = creare(val);
			cap[i] = p;
			u = p;
			if(caz==2)
				nr=n/k+1;
			else nr=n/k;
			for(j=2;j<=nr;j++)
			{
				if(caz==2&&sNr>=10000)
					break;
				val = val + rand();
				p = creare(val);
				u -> urm = p;
				u = p;
			}
			sNr+=nr;
		}
}

void grafice()
{
	FILE *pf;
	int opk1=0, opk2=0, opk3=0;
	int i, val, j;
	TNod *p, *u;

	////////////////   1 - n variabil //////////////////////////
	pf = fopen("grafic.csv", "w");

	fprintf(pf, "n, k=5, k=10, k=100\n");
	srand(time(NULL));
	for(n=100; n<=10000; n+=100)
	{
		op=0;
		genereazaListe(n, 5, 1);
		interclasare(5);
		opk1=op;

		op=0;
		genereazaListe(n, 10, 1);
		interclasare(10);
		opk2=op;

		op=0;
		genereazaListe(n, 100, 1);
		interclasare(100);
		opk3=op;

		fprintf(pf, "%d, %d, %d, %d\n", n, opk1, opk2, opk3);

	}
	fclose(pf);

	////////////////// 2 - k variabil /////////////////
	pf = fopen("grafic1.csv", "w");
	int k;
	n=10000;

	fprintf(pf, "k\n");
	srand(time(NULL));
	
	for(k=10; k<=500; k+=10)
	{
		op=0;
		genereazaListe(n, k, 2);
		interclasare(k);

		fprintf(pf, "%d, %d\n", k, op);

	}
	fclose(pf);
}

int main()
{
	testare();
	//grafice();
	return 0;
}