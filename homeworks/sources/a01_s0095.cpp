/*For the best case the comparison is determinant, because being ideal case we will have no assignments 
	In this case we can observe that selection sort is much worse than the other two selections which can be considered equal

	For the average case, in the number of comparisons, the selection sort is equal with bubblesort, and worse 
	than insertion sort. But in the asignments selection sort is greatly better than bubblesort and insertion sort.
	The conclusion is that for average values, selection sort is better than the other two sorting algorithms, 
	insertion being the second,bubblesort being the worst


	For the third case we observe the comparison operations ***ANONIM*** comparable.For the assignement operations the bubblesort and insertion 
	***ANONIM*** comparable, but selection is much more efficient.
*/

#include "stdafx.h"
#include <stdio.h>
#include "Profiler.h"
Profiler p("sort");

#define SWAP(a, b, type) {type aux = a; a = b; b = aux; }

int n;
int arr_b[10000];
int arr_s[10000];
int arr_i[10000];

void bubble_sort( int arr[], int n){
	p.countOperation("BubbleSortAssignments", n, 0);
	p.countOperation("BubbleSortComparison", n, 0);
	bool swapped = true;
	int j = 0;
	int tmp;
	while (swapped) {
		swapped = false;
		j++;
		for (int i = 0; i < n - j; i++) {
			p.countOperation("BubbleSortComparison", n);
			if (arr[i] > arr[i + 1]) {
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				swapped = true;
				p.countOperation("BubbleSortAssignments", n, 3);
			}
		}
	}
}

void insertion_sort( int arr[], int n){
	p.countOperation("InsertionSortComparison", n, 0);
	p.countOperation("InsertionSortAssignments", n, 0);
	int i, j, tmp;
	for (i = 1; i < n; i++) {
		j = i;
		while (j > 0 && arr[j - 1] > arr[j]) {
			p.countOperation("InsertionSortComparison", n);
			tmp = arr[j];
			arr[j] = arr[j - 1];
			arr[j - 1] = tmp;
			j--;
			p.countOperation("InsertionSortAssignments", n, 3);
		}
		p.countOperation("InsertionSortComparison", n);
	}
}

void selection_sort( int arr[], int n){
	p.countOperation("SelectionSortComparisons",n, 0);
	p.countOperation("SelectionSortAssignments", n, 0);
	int i, j, minIndex, tmp;
	for (i = 0; i < n - 1; i++) {
		minIndex = i;
		for (j = i + 1; j < n; j++){
			p.countOperation("SelectionSortComparisons",n, 1);
			if (arr[j] < arr[minIndex]){
				minIndex = j;
			}
		}
		if (minIndex != i) {
			tmp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = tmp;
			p.countOperation("SelectionSortAssignments", n, 3);
		}
	}
}

void best_case(){
	for(n=100; n<10000; n+=100){
		FillRandomArray(arr_b, n, 10, 50000, false, 1);


		selection_sort(arr_b, n);
		bubble_sort(arr_b, n);
		insertion_sort(arr_b, n);
	}

	p.addSeries("BubbleSortTotal", "BubbleSortComparison", "BubbleSortAssignments");
	p.addSeries("InsertionSortTotal", "InsertionSortComparison", "InsertionSortAssignments");
	p.addSeries("SelectionSortTotal", "SelectionSortComparisons", "SelectionSortAssignments");

	p.createGroup("BestCaseComparisonOperations", "BubbleSortComparison", "InsertionSortComparison", "SelectionSortComparisons");
	p.createGroup("BestCaseAssignmentOperations", "BubbleSortAssignments", "InsertionSortAssignments", "SelectionSortAssignments");
	p.createGroup("BestCaseTotalOperations", "BubbleSortTotal", "InsertionSortTotal", "SelectionSortTotal");

	p.showReport();
}

void average_case(){
	int backup[10000];
	for(int i = 0; i < 5; i++){
		for(n=100; n<10000; n+=100){
			FillRandomArray(backup, n, 10, 50000, false, 0);

			memcpy(arr_s, backup, 10000*sizeof(int));
			memcpy(arr_b, backup, 10000*sizeof(int));
			memcpy(arr_i, backup, 10000*sizeof(int));

			selection_sort(arr_s, n);
			bubble_sort(arr_b, n);
			insertion_sort(arr_i, n);
		}
	}
	p.addSeries("BubbleSortTotal", "BubbleSortComparison", "BubbleSortAssignments");
	p.addSeries("InsertionSortTotal", "InsertionSortComparison", "InsertionSortAssignments");
	p.addSeries("SelectionSortTotal", "SelectionSortComparisons", "SelectionSortAssignments");

	p.createGroup("AverageCaseComparisonOperations", "BubbleSortComparison", "InsertionSortComparison", "SelectionSortComparisons");
	p.createGroup("AverageCaseAssignmentOperations", "BubbleSortAssignments", "InsertionSortAssignments", "SelectionSortAssignments");
	p.createGroup("AverageCaseTotalOperations", "BubbleSortTotal", "InsertionSortTotal", "SelectionSortTotal");

	p.showReport();
}

void worst_case(){
	for(n=100; n<10000; n+=100){
		FillRandomArray(arr_b, n, 10, 50000, false, 2);
		memcpy(arr_i, arr_b, 10000*sizeof(int));

		FillRandomArray(arr_s, n, 10, 50000, false, 1);
		SWAP(arr_s[0], arr_s[n-1], int);

		selection_sort(arr_s, n);
		bubble_sort(arr_b, n);
		insertion_sort(arr_i, n);
	}

	p.addSeries("BubbleSortTotal", "BubbleSortComparison", "BubbleSortAssignments");
	p.addSeries("InsertionSortTotal", "InsertionSortComparison", "InsertionSortAssignments");
	p.addSeries("SelectionSortTotal", "SelectionSortComparisons", "SelectionSortAssignments");

	p.createGroup("WorstCaseComparisonOperations", "BubbleSortComparison", "InsertionSortComparison", "SelectionSortComparisons");
	p.createGroup("WorstCaseAssignmentOperations", "BubbleSortAssignments", "InsertionSortAssignments", "SelectionSortAssignments");
	p.createGroup("WorstCaseTotalOperations", "BubbleSortTotal", "InsertionSortTotal", "SelectionSortTotal");

	p.showReport();
}

void main(){

	best_case();

}