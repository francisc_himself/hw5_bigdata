#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler profiler("demo");

#define V_MAX 10000
#define E_MAX 60000

typedef struct NODE{
     int key;
	 NODE *next;
   }NODE;
NODE *adj[V_MAX];
int edges[E_MAX], profilerVar;

typedef struct Q
{
   int data[E_MAX];
   int R,F;
}Q;

typedef struct node
{
   struct node *next;
   int vertex;
}node;

void enqueue(Q *,int);
int dequeue(Q *);
int empty(Q *);
int full(Q *);
void BFS(int);
void readgraph();          //create an adjecency list
void insert(int vi,int vj);     //insert an edge (vi,vj)in adj.list
void DFS(int i);
void generate(int, int);
void average();
void varyE();
void varyV();

int visited[E_MAX];
node *G[10000];              //heads of the linked list
int n,m;                     // no of nodes

void main()
{
   int i,op;
   do
     { printf("\n\n1)Demo\n2)Vary the number of edges\n3)Vary the number of vertices\n4)Quit");
       printf("\nEnter Your Choice: ");
       scanf("%d",&op);
       switch(op)
        { case 1: readgraph();break;
		  case 2:varyE();break;
		  case 3:varyV();break;
         }
      }while(op!=4);
}
void readgraph()
{
   printf("\nEnter no. of vertices :");
   scanf("%d",&n);
   //initialise G[] with NULL
   for(int i=0;i<n;i++)
      G[i]=NULL;
   //read edges and insert them in G[]
   printf("\nEnter no of edges :");
   scanf("%d",&m);
   generate(n,m);
   for(int i=0; i<n;i++)
	   BFS(i);
}

void varyV()
{
	m=9000;
	for(n=100;n<=200;n+=10)
	{
		profilerVar=n;
		printf("n = %d\n",n);
		generate(n,m);
		for(int j=0; j<n;j++)
			BFS(j);
	}
	profiler.createGroup("Vary the number of vertices - BFS","operations");
	profiler.showReport();
}
void varyE()
{
	n=100;
	for(m=1000;m<=5000; m+=100)
	{
		profilerVar=m;
		printf("m= %d\n",m);
		generate(n,m);
		for(int j=0; j<n;j++)
			BFS(j);
	}
	profiler.createGroup("Vary the number of edges - BFS","operations");
	profiler.showReport();
}
void BFS(int v)
{
   int w,i,visited[E_MAX];
   Q q;

   node *p;
   q.R=q.F=-1;              //initialise
   for(i=0;i<n;i++)
      visited[i]=0;
   if(n<90 && m<90)printf("\nThe queue:\n");
   enqueue(&q,v);
   if(n<90 && m<90)printf("%d ",v);
   visited[v]=1;
   while(!empty(&q))
   {
      v=dequeue(&q);
      //insert all unvisited,adjacent vertices of v into queue
      for(p=G[v];p!=NULL;p=p->next)
      {
         w=p->vertex;
         if(visited[w]==0)
         {
            enqueue(&q,w);
            visited[w]=1;
            if(n<90 && m<90) printf("%d ",w);
         }
      }
   }
}

int empty(Q *P)
{
	profiler.countOperation("operations",profilerVar);
   if(P->R==-1)
      return 1;
   return 0;
}

int full(Q *P)
{
	profiler.countOperation("operations",profilerVar);
   if(P->R==E_MAX-1)
      return 1;
   return 0;
}
void generate(int n,int m)//n - nodes, m- edges
{
 memset(adj,0,n*sizeof(NODE *));
 FillRandomArray(edges,m,0,n*n-1,true);
 //parcurgem lista de muchii generate
 for(int i=0;i<m;i++)
 {
	 profiler.countOperation("operations",profilerVar,2);
	 int a=edges[i]/n;
	 int b=edges[i]%n;

	 insert(a,b);
	 insert(b,a);
 }
}

void enqueue(Q *P,int x)
{
	profiler.countOperation("operations",profilerVar);
   if(P->R==-1)
   {
	   profiler.countOperation("operations",profilerVar,2);
       P->R=P->F=0;
       P->data[P->R]=x;
   }
   else
   {
	   profiler.countOperation("operations",profilerVar,2);
       P->R=P->R+1;
       P->data[P->R]=x;
   }
}

int dequeue(Q *P)
{
   int x;
   profiler.countOperation("operations",profilerVar,2);
   x=P->data[P->F];
   if(P->R==P->F)
   {
		profiler.countOperation("operations",profilerVar,2);
		P->R=-1;
		P->F=-1;
   }
   else
   {
	   profiler.countOperation("operations",profilerVar);
	   P->F=P->F+1;
   }
   return x;
}

void insert(int vi,int vj)
{
   node *p,*q;
   //acquire memory for the new node
   q=(node *)malloc(sizeof(node));
   profiler.countOperation("operations",profilerVar);
   q->vertex=vj;
   q->next=NULL;
   //insert the node in the linked list for the vertex no. vi
   if(G[vi]==NULL)
      G[vi]=q;
   else
   {
      // go to the end of linked list
      p=G[vi];
      while(p->next!=NULL)
         p=p->next;
      p->next=q;
   }
}