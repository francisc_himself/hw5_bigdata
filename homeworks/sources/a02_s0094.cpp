/*Student: ***ANONIM*** ***ANONIM*** Szonja
Group: ***GROUP_NUMBER***

Interpretation of results:

The measurements were conducted for 2 different cases:

	
**************** AVERAGE CASE ***************
	
FOR AVERAGE CASE WE HAVE AS INPUT DATA: 
	- AN ARRAY WITH RANDOM UNSORTED ELEMENTS ON WHICH WE APPLY	BOTTOM_UP AND TOP DOWN
	- THE MEASUREMENTS WERE TAKEN 5 TIMES AND THE GRAPH PRESENTS THE SUM OF THE RESULTS OF THESE 5 MEASUREMENTS

GRAPH INTERPRETATION:
	- THE NUMBER OF OPERATIONS FOR BOTH BOTTOM UP AND TOP DOWN INCREASES LINEARLY
	- TOP DOWN HAS A SLIGHTLY GREATER INCREASE THAN THAT OF BOTTOM UP


**************** WORST CASE ***************
	
FOR WORST CASE WE HAVE AS INPUT DATA:
	- NONDECREASING ARRAY ON WHICH WE APPLY BOTTOM_UP AND TOP DOWN
	

GRAPH INTERPRETATION:
	- THE NUMBER OF OPERATIONS FOR BOTH BOTTOM UP AND TOP DOWN INCREASES LINEARLY
	- TOP DOWN HAS A GREATER INCREASE THAN THAT OF BOTTOM UP
	- UNLIKE IN THE AVERAGE CASE, IN WORST CASE THE DIFFERENCE BETWEEN THE TWO METHODS IS MORE VISIBLE,
	TOP DOWN INCREASES FASTER THAN BOTTOM UP


OTHER ASPECTS:
	- EFFICIENCY:
		BOTTOM UP: O(n)
		TOP DOWN: O(nlgn)
		

		
*/


#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<time.h>
#include<fstream>
#include "Profiler.h"
#define MAX_SIZE 10000
int heap_sizeH;
Profiler profiler("demo");
int heapsize=0;
int n;

void pretty_print(int T[],int i,int depth)
{
	if(i<heap_sizeH)
	{ 
	pretty_print(T,(2*i+2),(depth+1));
	for(int j=0;j<depth;j++) printf("	");
		printf("%d", T[i]);
	printf("\n");
	pretty_print(T,2*i+1,(depth+1));
	}

}


void copy(int n,int *sir1,int *sir2) 
{                                      
	                              
	  
	for (int i=0;i<n;i++)
		sir2[i]=sir1[i];
}




int PARENT(int i)   
{
  return (i-1)/2;
}

int LEFT(int i)                    
{
  return 2*i+1;
}

int RIGHT(int i)                   
{
  return 2*i+2;
}

void max_heapify(int *c,int i,int heap_size,int n) 
{  int l=LEFT(i);                           
   int r=RIGHT(i);
   int largest;
   int aux;

   profiler.countOperation("operations_bu",n,0);

   //count one comparison
   profiler.countOperation("operations_bu",n,1);
   if((l<=heap_size)&&(c[l]>c[i]))
       largest=l;
   else largest=i;

   //count one comparison
   profiler.countOperation("operations_bu",n,1);
   if((r<=heap_size)&&(c[r]>c[largest]))
	   largest=r;
  
   if(largest!=i)
   {    
      aux=c[i];
	  c[i]=c[largest];
	  c[largest]=aux;
	  //count 3 assignments
	 profiler.countOperation("operations_bu",heap_size,3);
	  max_heapify(c,largest,heap_size,n);  
   }

   
}

void bottom_up(int *c,int n,int *heap_size) 
{
  (*heap_size)=n;
  int i;
  for(i=(n/2);i>=0;i--)
       max_heapify(c,i,*heap_size,n);
}

void insertHeap(int heap[],int val,int *heap_size)
{
	profiler.countOperation("operations_td",n,0);
	int i;
	//increase size of heap
	(*heap_size)++;
	i=(*heap_size);
	
	while ((i>0) && ( heap[i/2]< val)) //check if child is greater than parent 
	//if true interchange
	{
		heap[i] = heap[i/2];
		i = i/2;
	//count one comparison and one assignment
		profiler.countOperation("operations_td",n,2);
		
	}
	//count the comparison when exiting the while
	profiler.countOperation("operations_td",n,1);

	heap[i] = val;
	//count the assignment
	profiler.countOperation("operations_td",n,1);
}


void top_down(int a[],int heap[],int n,int *heap_size)
{
	for (int i=1;i<=n;i++)
	{
		insertHeap(heap,a[i],heap_size);
	}
}






void main(){

int i;
  
 int v[MAX_SIZE],s[MAX_SIZE],s1[MAX_SIZE];
 
 
 //printf("n=");
 //scanf("%d",&n);

// for(int k=0;k<5;k++)
//{
  for (n=100;n<10000;n=n+100)
  {
 
 FillRandomArray(v, n, 0, 32000, true, 1);
 printf("%d\n",n);

 		  
 //for(i=0;i<n;i++)
	// printf("%d ",v[i]);
// printf("\n");
 
			heap_sizeH=0;
//printf("\nBOTTOM_UP_HEAP\n");
			copy(n,v,s);
            bottom_up(s,n,&heap_sizeH);
		 // pretty_print(s,0,0);
		 // for ( i=0;i<n;i++)
		//	  printf("%d ",s[i]);
		    copy(n,v,s);
//printf("\nT0P_DOWN_HEAP\n");
			copy(n,s,s1);
			heap_sizeH=0;
			
		   top_down(s1,s,n,&heap_sizeH);
		 //pretty_print(s,0,0);
		//  for ( i=0;i<n;i++)
		//	  printf("%d ",s[i]);
 
  //}
 }
profiler.createGroup("operations_for_worst_case","operations_bu","operations_td");
	profiler.showReport();
getch();

}
