/*
	Name: ***ANONIM*** ***ANONIM***
	Group: ***GROUP_NUMBER***
	
	Comparison of the following direct sorting methods: Insertion sort, Selection sort,  Bubble sort.

	-As is, the code runs the algorithms on a small predifined set to check for correctness.
	-To rerun the analysis, the code sections at the bottom have to be uncommented one at a time to perform the appropriate analysis case.

	Analysis of results:
	
	Both selection and insertion sort have O(n^2) performance for worst and average cases, making them bad for large input sets.
	However selection sort has the same performance in its best and worst cases, while insertion sort performs better than selection sort
	in the best case. 
	Selection sort performs fewer swaps and more comparisons, making it the better choice when swaps are expensive.

	Bubble sort also has O(n^2) performance, making it inefficient for large data sets.
	Bubble sort performs best on small data sets, or almost completely sorted sets, having the most efficient best case, followed by insertion sort.
	Selection sort has the largest number of operations in the best case due to the high number of comparisons.

	For overall performance in the average case, bubble sort is the least efficient by far, while insertion sort and selection sort are almost equal.
	In the worst case, bubble sort is the least efficient, insertion and selection sort both performing overall less operations.

	As for stability, all the algorithms are (or can be made) stable by ensuring the proper strict comparison conditions.


*/


#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h> 
//#include "Profiler.h"

int A[20]={3,6,12,0,1,2,55,32,10,12,9,1,14,15,19,20,21,18,31,40};
int B[20]={3,6,12,0,1,2,55,32,10,12,9,1,14,15,19,20,21,18,31,40};
int C[20]={3,6,12,0,1,2,55,32,10,12,9,1,14,15,19,20,21,18,31,40};
int nr=20;
int size;
FILE *f;
FILE *f2;
FILE *f3;

#define MAX_SIZE 10000



void insertionSort(int arr[], int n)
{
	
    int k,i,buff, iass, icomp, itot;
	iass=0; icomp=0; itot=0;
    
    for (i=1; i<n; i++) {
        k=i-1;
		
        buff=arr[i]; iass=iass+1;
        while (arr[k]>buff && k>=0)
        {
			icomp=icomp+1;
            arr[k+1]=arr[k];
			iass=iass+1;
			
            k--; 
			
        }
        icomp=icomp+1;
        arr[k+1]=buff; 
		iass=iass+1;
		
    }
	itot=iass+icomp;
	fprintf(f, "%d,%d,%d,%d\n",n, iass, icomp, itot);
	


}

void selectionSort(int arr[], int n)
{
    int k,i,j,buff,imin,min,sass,scomp,stot;
	sass=0; scomp=0; stot=0;
    for (i=0; i<n-1; i++)
    {
		
        //min=A[i]; 
		imin=i;
		//sass=sass+1;
        for (j=i+1; j<n; j++)
        {
            //comp++;
			scomp=scomp+1;
            if (arr[j]<arr[imin])
            {
				//sass=sass+1;	
                //min=arr[j];
                imin=j;
            }
        }
        if (i!=imin)
        {
            buff=arr[imin];
            arr[imin]=arr[i];
            arr[i]=buff;
			sass=sass+3;
            //ass=ass+3;
        }
       
    }
	stot=sass+scomp;
	fprintf(f2, "%d,%d,%d,%d\n",n, sass, scomp, stot);

}

void bubbleSort(int arr[], int n)
{
   
	int swapped=0; int i,nr,buff,bass,bcomp,btot;
	bass=0; bcomp=0; btot=0;
	nr=n;
	do
	{
		swapped=0;
		for(i=1; i<nr; i++)
		{
			bcomp=bcomp+1;
			if (arr[i-1]>arr[i])
			{
				buff=arr[i-1];
				arr[i-1]=arr[i];
				arr[i]=buff;
				bass=bass+3;
				swapped=1;
			}
		}
		nr=nr-1;



	} while (swapped==1);
		

	btot=bass+bcomp;
	fprintf(f3, "%d,%d,%d,%d\n",n, bass, bcomp, btot);
   
}

void main()
{

	printf("\nOriginal test array: ");
	for (int i=0; i<nr; i++)
    {
        printf("%d ", A[i]);


    }

	f = fopen("out1.txt", "w");
	f2 = fopen("out2.txt", "w");
	f3 = fopen("out3.txt", "w");
	fprintf(f, "n,ins_ass,ins_comp,ins_tot\n");
	fprintf(f2, "n,sel_ass,sel_comp,sel_tot\n");
	fprintf(f3, "n,bub_ass,bub_comp,bub_tot\n");

    
    insertionSort(A, nr);
	selectionSort(B,nr);
    bubbleSort(C, nr);
	printf("\nInsertion sort test: ");
    for (int i=0; i<nr; i++)
    {
        printf("%d ", A[i]);


    }
    printf("\nSelection sort test: ");
    for (int i=0; i<nr; i++)
    {
        printf("%d ", B[i]);


    }
    printf("\nBubble sort test: ");
    for (int i=0; i<nr; i++)
    {
        printf("%d ", C[i]);


    }
    printf("\n");
	 

	//Analysis part starts here

	int v1[MAX_SIZE];
	int v2[MAX_SIZE];
	int v3[MAX_SIZE];
	int i;
	srand (time(NULL));


	// UNCOMMENT THE CODE SECTIONS BELOW TO GET OUTPUT FOR ANALYSYS
	//out1.txt is for Insertion sort
	//out2.txt for Selection sort
	//out3.txt for Bubble sort

/*
	//best case
	
	for (size=100; size<MAX_SIZE; size+=200)
	{
		for (int i=1; i<size; i++)
		{
			v1[i]=i;
			v2[i]=i;
			v3[i]=i;

		}
		//FillRandomArray(v1, size, 10, 50000, false, 1);
		//memcpy(v2, v1, size);
		//memcpy(v3, v1, size); 
 
		
		insertionSort(v1, size);
		selectionSort(v2,size);
		bubbleSort(v3, size);

	}
	printf("got here");

	*/

//average case
	/*
	for (int m=0; m<5; m++)
	{

		for (size=100; size<MAX_SIZE; size+=200)
			{
				for (int i=1; i<size; i++)
					{
						v1[i]=rand() % 10000 +1;
						v2[i]=v1[i];
						v3[i]=v1[i];

					}
		
				insertionSort(v1, size);
				selectionSort(v2,size);
				bubbleSort(v3, size);

			}
		fprintf(f,"\n");
		fprintf(f2,"\n");
		fprintf(f3,"\n");

	}
	*/

/*	//worst case

	for (size=100; size<MAX_SIZE; size+=200)
	{
		for (int i=1; i<size; i++)
		{
			v1[i]=size-i;
			v2[i]=i;
			v3[i]=size-i;

		}
		//FillRandomArray(v1, size, 10, 50000, false, 1);
		//memcpy(v2, v1, size);
		//memcpy(v3, v1, size); 
 
		insertionSort(v1, size);
		//v2[size]=0;
		selectionSort(v2, size);
		
		//v3[size]=0;
		bubbleSort(v3, size);

	} 


	printf("got here"); */

}