#include <conio.h>
#include <fstream>
#include <iostream>
#include "profiler.h"

Profiler profiler("sorting");

using namespace std;

void insertionSort(long numbers[], long n)
{
	int i,j,buff;
	profiler.countOperation("Insertion-ass",n,0);
	profiler.countOperation("Insertion-comp",n,0);
	profiler.countOperation("Insertion-comp+ass",n,0);
	for (i=1;i < n;i++)
		{
			buff=numbers[i];
			profiler.countOperation("Insertion-ass",n,2);
			profiler.countOperation("Insertion-comp+ass",n,2);
			j=i-1;
			profiler.countOperation("Insertion-comp",n);
			profiler.countOperation("Insertion-comp+ass",n);
			while (numbers[j]>buff && j>=0)
				{
					profiler.countOperation("Insertion-comp",n);
					profiler.countOperation("Insertion-ass",n);
					profiler.countOperation("Insertion-comp+ass",n,2);
					numbers[j+1]=numbers[j];
					j=j-1;
				}
		
			numbers[j+1]=buff;
			
		}
	}

	void selectionSort(long numbers[], long n)
	{
		int i,j,pos,temp;
		profiler.countOperation("Selection-ass",n,0);
		profiler.countOperation("Selection-comp",n,0);
		profiler.countOperation("Selection-comp+ass",n,0);
		for (j=0;j < n-1;j++)
		{
			pos=j;
			for (i=j+1;i<n;i++)
			{
				profiler.countOperation("Selection-comp",n);
				profiler.countOperation("Selection-comp+ass",n);
				if (numbers[i]<numbers[pos])
				{
				pos=i;
								
				}
			}
				if (pos != j) 
					{
						temp=numbers[j];
						numbers[j]=numbers[pos];
						numbers[pos]=temp;
						profiler.countOperation("Selection-ass",n,3);
						profiler.countOperation("Selection-comp+ass",n,3);
					}	
				
		}
	}

			
	void bubbleSort(long numbers[], long n)
	{
		int ok,i,aux;
		profiler.countOperation("Bubble-ass",n,0);
		profiler.countOperation("Bubble-comp",n,0);
		profiler.countOperation("Bubble-comp+ass",n,0);
		do
		{
			ok=0;
			for (i=0;i<n-1;i++)
			{
				
				profiler.countOperation("Bubble-comp",n);
				profiler.countOperation("Bubble-comp+ass",n);
				if (numbers[i]>numbers[i+1])
				{	
					profiler.countOperation("Bubble-ass",n,3);
					profiler.countOperation("Bubble-comp+ass",n,3);
					aux=numbers[i];
					numbers[i]=numbers[i+1];
					numbers[i+1]=aux;
					ok=1;
				}
		
		}
			}
		while(ok==1);
	}


	int main()
	{		
		long i,j,k,temp;
		long best[10000],average[10000],average2[10000],worstbub[10000],worstins[10000],worstsel[10000];
		/*for(i = 0;i<10000;i++)
		{
			best[i] = i;
		}
		for (i=100;i<10000;i+=100)
		{
			bubbleSort(best,i);
			insertionSort(best,i);
			selectionSort(best,i);
		}
			
			profiler.createGroup("All Best","Bubble-comp+ass","Selection-comp+ass","Insertion-comp+ass");
			profiler.createGroup("Assignment Best","Bubble-ass","Selection-ass","Insertion-ass");
			profiler.createGroup("Comparison Best","Bubble-comp","Selection-comp","Insertion-comp");
			profiler.showReport();*/
		for (i=100;i<10000;i+=500)
		{
			for (k=0; k<5; k++)
			{
			FillRandomArray(average,i);
			for (j=0; j<i; j++)
			{
				average2[j]=average[j];
			}
			bubbleSort(average,i);
			for (j=0; j<i; j++)
			{
				average[j]=average2[j];
			}
			insertionSort(average,i);
			for (j=0; j<i; j++)
			{
				average[j]=average2[j];
			}
			selectionSort(average,i);
			}
		}

			profiler.createGroup("All Average","Bubble-comp+ass","Selection-comp+ass","Insertion-comp+ass");
			profiler.createGroup("Assignment Average","Bubble-ass","Selection-ass","Insertion-ass");
			profiler.createGroup("Comparison Average","Bubble-comp","Selection-comp","Insertion-comp");
			profiler.showReport();
		/*for(i = 0;i<10000;i++)
		{
			worstbub[i] = i+1;
		}

		for(i = 0;i<10000;i++)
		{
			worstsel[i] = i+1;
		}
		
		for (i=100;i<10000;i+=100)
		{
			
			worstbub[i-1]=0;
			bubbleSort(worstbub,i);
			for(j = 0;j<=i;j++)
			{
				worstins[j] = 10000-j;
			}	
			insertionSort(worstins,i);
					
			
			
			worstsel[i-1]=0;
			selectionSort(worstsel,i);
			
			}
		
			profiler.createGroup("All Worst","Bubble-comp+ass","Selection-comp+ass","Insertion-comp+ass");
			profiler.createGroup("Assignment Worst","Bubble-ass","Selection-ass","Insertion-ass");
			profiler.createGroup("Comparison Worst","Bubble-comp","Selection-comp","Insertion-comp");
		
			profiler.showReport();
		
		
		*/		return 0;
	}