/*
Din graficele obtinute observam ca metoda de sortare
quick sort este mai eficienta decat metoda de soratare
heap sort. la nivelul comparatiilor cele doua se apropie, insa 
numarul asignarilor este semnificativ mai mic in cazul quick sort.
*/
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include "Profiler.h"
#define NMAX 20000

int dim_heap, sir[NMAX],a[NMAX],b[NMAX],ass_qs,comp_qs,ass_hs,comp_hs;

int partitionare(int a[], int st, int dr)
{
	int x,i,j,aux;
	x = a[st];
	i = st-1;
	j = dr+1;
	while(true)
	{
		do{
			j--;
			comp_qs++;
		}while(a[j]>x);
		
		do{
			i++;
			comp_qs++;
		}while(a[i]<x);
		
		if(i<j)
		{ 
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
			ass_qs= ass_qs + 3;
		}
		else return j;
	}
}
void quick_sort (int a[], int st, int dr)
{
	int m;
	if (st<dr)
	{
		m = partitionare(a,st,dr);
		quick_sort(a,st,m);
		quick_sort(a,m+1,dr);
	}
}

void ReBuild(int a[NMAX],int i)
{
    int max,aux,l,r;
	l= 2*i;
	r= 2*i+1;
	comp_hs++;
	if(l<= dim_heap && a[l]>a[i])
		max = l;
	else
		max = i;
	comp_hs++;
	if(r<=dim_heap && a[r]>a[max])
		max = r;
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		ass_hs= ass_hs + 3;
		ReBuild(a,max);
	}
}

void bottom_up(int a[NMAX], int n)
{
	int i;
	dim_heap = n;
    for(i=n/2;i>=1;i--)
	{
		ReBuild(a,i);
		
	}
	
}

void heap_sort(int a[], int n)
{	
	int aux,i;
	bottom_up(a,n);
	for(i=n; i>=2; i--)
	{
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		dim_heap--;
		ReBuild(a,1);
	} 
}
//functie care afiseaza un sir de numere
void print(int a[], int n)
{
	int i;
	for(i=1;i<=n;i++)
		printf("%d ", a[i]);
	printf("\n");
}
//functie care initializeaza un sir de numere la 0
void init (int a[NMAX], int n)
{
	int i;
	for(i=1;i<=n;i++)
		a[i]=0;
}
//functie care copiaza un sir intr-un alt sir
void copy (int a[NMAX], int b[NMAX], int n)
{
	int i;
	for(i=1;i<=n;i++)
		b[i]=a[i];
}
void main()
{
	int i, sum_assQ=0, sum_compQ=0, sum_assH=0, sum_compH=0, k;
	FILE *f;
	//exemplificarea functionarii algoritmilor pe un caz particular
	FillRandomArray(sir,10,1,200,true,0);
	copy(sir,b,10);
	print(sir,10);
	printf("\n");
	printf("Quick_Sort:\n");
	//aplicarea algoritmului quick sort
	quick_sort(sir,1,10);
	print(sir,10);
	printf("\n");
	printf("Heap_Sort:\n");
	//aplicarea algoritmului heap sort
	heap_sort(b,10);
	print(b,10);
	printf("\n");
	
	//deschiderea fisierului pentru scrierea datelor
	f = fopen("analiza.csv","w");
	if(f==NULL){
		perror("Eroare deschidere fisier!");
		exit(1);
	}
	init(b,NMAX);
	//scrierea capuleui de tabel
	fprintf(f,"n, ass_qs, comp_qs, ass_qs + comp_ass, ass_hs, ass_comp, ass_hs + comp_ass\n");
	for(i=100;i<=10000;i=i+100)
	{
		

		for(k=1;k<=5;k++)
		{
			//initializarea contoarelor
			ass_qs=0;comp_qs=0;ass_hs=0;comp_hs=0;
			//generarea unui sir aleator
			FillRandomArray(a,i,1,20000,true,0);
			copy(a,b,i);
			//aplicarea algoritmului quick_sort
			quick_sort(b,1,i);
			//calcularea sumelor pentru a putea calcula media aritmetica
			sum_assQ = sum_assQ + ass_qs;
			sum_compQ= sum_compQ + comp_qs;
			init(b,i);
			copy(a,b,i);
			//aplicarea algoritmului heap sort
			heap_sort(b,i);
			sum_assH = sum_assH + ass_hs;
			sum_compH = sum_compH + comp_hs;
			init(b,i);			
		}
		//scria rezultatelor in fisier
		fprintf(f,"%d, %d, %d, %d, %d,%d,%d\n",i,sum_assQ/5,sum_compQ/5,(sum_assQ + sum_compQ)/5,sum_assH/5, sum_compH/5, (sum_assH + sum_compH)/5);
		
	}
	/*
	printf("\n");
	fprintf(f,"\nn, ass_qs, comp_qs, ass_qs + comp_ass\n");
	init(a,NMAX);
	init(b,NMAX);
	for(i=100;i<=10000; i=i+100)
	{
		ass_qs=0;comp_qs=0;ass_hs=0;comp_hs=0;
		//crearea unui sir descrescator
		FillRandomArray(a,i,1,20000,true,2);
		copy(a,b,i);
		quick_sort(b,1,i);
		init(b,i);
		fprintf(f,"%d, %d, %d, %d\n",i, ass_qs,comp_qs,ass_qs+comp_qs);
	}
	
	printf("\n");
	fprintf(f,"n, ass_qs, comp_qs, ass_qs + comp_ass\n");
	init(a,NMAX);
	init(b,NMAX);
	for(i=100;i<=10000; i=i+100)
	{
		ass_qs=0;comp_qs=0;ass_hs=0;comp_hs=0;
		//crearea unui sir descrescator
		FillRandomArray(a,i,1,20000,true,1);
		copy(a,b,i);
		quick_sort(b,1,i);
		init(b,i);
		fprintf(f,"%d, %d, %d, %d\n",i, ass_qs,comp_qs,ass_qs+comp_qs);
	}*/
	printf("succes!");
	getch();
}