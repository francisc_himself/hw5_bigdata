/* ***ANONIM*** ***ANONIM*** ***ANONIM*** 30225
*
* SOURCES : http://www.imt.ro/romjist/Volum12/Number12_1/pdf/02-MCosulschi.pdf
*	http://samplecodebank.blogspot.ro/2011/05/avl-tree-c-code-example.html
*	http://www.cs.uah.edu/~rcoleman/Common/CodeVault/Code/Code203_Tree.html
*			
*
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max(a,b)    (((a) > (b)) ? (a) : (b))

int atr=0,cmp=0;

typedef struct AvlNode
{
	int data; // key 값
	int size;
	struct AvlNode *left_child, *right_child;
}AvlNode;

AvlNode *root;


int getHeight(AvlNode *node)
{
	cmp++;
	if(node==NULL)
		return -1;
	else
	return node->size;
}


int size(AvlNode *node) 
{ 
	cmp++;
  if (node==NULL) 
    return 0;
  else    
    return(size(node->left_child) + 1 + size(node->right_child));  
}

int getMax(int a, int b)
{
	cmp++;
	if(a<=b)
	{
		return b;
	}
	else
	{
		return a;
	}
}




// RL 회전 (오른쪽-왼쪽으로 회전한다)
//     A                A              
//      \                \                C
//       B      ->        C      ->      / \
//      /                  \            A   B
//     C                    B
//
// ±2를 가지는 A가 부모가 되고 A->right_child인 B가 child가 된다.
// A->right_child에 rotate_LL(B)가 반환하는 값을 대입한다. (B,C에 대해 오른쪽 회전)
// rotate_LL(B)호출시 B와 C가 변화가 생기고 다시 rotate_RR(A)을 호출하면 균형트리가 된다. 

AvlNode* rotate_RL(AvlNode *parent)
{

	AvlNode *child = parent->right_child;
	parent->right_child=child->left_child;//child = n1 parent = n2
	child->left_child=parent;

	parent->size = size(parent);
	child->size = size(child);
	
	atr=atr+5;
	return child;
}

// LR 회전 (왼쪽-오른쪽으로 회전한다)
//     A                 A              
//	  /                 /                  C
//   B         ->      C          ->      / \
//    \               /                  A   B
//     C             B      
//
// ±2를 가지는 A가 부모가 되고 A->left_child인 B가 child가 된다.
// A->left_child에 rotate_RR(B)가 반환하는 값을 대입한다. (B,C에 대해 왼쪽 회전)
// rotate_RR(B)호출시 B와 C가 변화가 생기고 다시 rotate_LL(A)을 호출하면 균형트리가 된다. 

AvlNode* rotate_LR(AvlNode *parent)
{
	//n2 = parent n1 = child
	AvlNode *child;

	child= parent->left_child;
	parent->left_child = child->right_child;
	child->right_child = parent;

	parent->size = size(parent);
	child->size = size(child);

	atr=atr+5;
	return child;
}

// LL 회전 (오른쪽으로 회전한다)
//     A
//    /                B0
//   B        ->      / \
//  /                C   A
// C
//
// ±2를 가지는 A가 부모가 되고 A->left_child인 B가 child가 된다.
// A->left에 B가 가지고 있는 right_child를 대입하고 B의 right_child에 A을 대입한다.

AvlNode* rotate_LL(AvlNode *parent)
{
	atr++;
	parent->left_child=rotate_RL(parent->left_child);
	return rotate_LR(parent);
}

// RR 회전 (왼쪽으로 회전한다)
//     A
//      \               B
//       B     ->      / \
//        \           A   C
//         C
//
// ±2를 가지는 A가 부모가 되고 A->right_child인 B가 child가 된다.
// A->right에 B가 가지고 있는 left_child를 대입하고 B의 left_child에 A을 대입한다.

AvlNode* rotate_RR(AvlNode *parent)
{
	atr++;
	parent->left_child=rotate_LR(parent->left_child);
	return rotate_RL(parent);
}

// 트리의 높이 측정 함수
// 순환호출로 각각의 높이를 구하고 이들 중에서 더 큰값에 1을 더하면 트리의 높이가 된다.
int get_size(AvlNode *node)
{
	int size=0;
	cmp++;
	if(node != NULL)
	{
		size = 1+max(get_size(node->left_child),get_size(node->right_child));
		atr++;
	}
	return size;
}

// 노드의 균형인수 반환 함수
// 왼쪽 서브트리 높이 - 오른쪽 서브트리 높이
int get_balance(AvlNode *node)
{
	cmp++;
	if(node == NULL) return 0;
	return get_size(node->left_child) - get_size(node->right_child);
}

// 균형 트리를 만드는 함수
AvlNode* balance_tree(AvlNode *node)
{
	int size_diff= get_balance(node);
	
	if(size_diff > 1) // 왼쪽 서브트리의 균형을 맞춘다
	{
		if(get_balance((node)->left_child) > 0)
			node = rotate_LL(node);
		else 
			node = rotate_LR(node);
	}
	else if(size_diff < -1) // 오른쪽 서브트리의 균형을 맞춘다
	{
		if(get_balance((node)->right_child) < 0)
			node = rotate_RR(node);
		else
			node = rotate_RL(node);
	}
	return node;
}

// AVL 트리의 삽입 연산
// key에 대해 순환호출을 반복하므로써 트리에 삽입 한 후 균형화 함수를 호출한다.
AvlNode *avl_add(AvlNode *t,int data)
{
	AvlNode *tempNode;

	cmp++;
	if(t==NULL)
	{
		tempNode = (AvlNode *) malloc(sizeof(AvlNode));
		cmp++;
		if(tempNode == NULL)
			return t;

		tempNode->data=data;
		tempNode->size=0;
		tempNode->left_child = tempNode->right_child = NULL;
		
		return tempNode;
	}
	else if(data < t->data)
	{
		atr++;

		t->left_child = avl_add(t->left_child,data);
		cmp++;
		if(getHeight(t->left_child)- getHeight(t->right_child)==2)
		{
			cmp++;
			if(data < t->left_child->data)
			{
				t = rotate_LR(t);
				atr++;
			}
			else
			{
				t = rotate_LL(t);
				atr++;
			}
		}
	}

	else if(data > t->data)
	{
	cmp++;
		atr++;
		t->right_child = avl_add(t->right_child,data);
		/*printf("T->right_child %d size is %d\n",t->right_child->data,t->right_child->size);
		printf("T->left_child %d size is %d\n",t->left_child->data,t->left_child->size);*/
		cmp++;
		if(getHeight(t->right_child)- getHeight(t->left_child)==2)
		{
			cmp++;
			if(data>t->right_child->data)
			{
				t = rotate_RL(t);
			}
			else
			{
				t = rotate_RR(t);//fara t=
			}
		}
	}
	atr++;
	t->size = size(t);
	return t;
}

// AVL 트리 탐색 함수
// 일반 적인 이진 트리의 탐색 함수와 같다. AVL도 이진 탐색 트리의 일종이다.
AvlNode* avl_search(AvlNode *node, int key)
{
	cmp++;
	if(node == NULL) return NULL;
	
	printf("%d->",node->data);

	cmp++;
	if(key == node->data) 
	{
		 printf("        H  = %d",node->size);
		return node;
	}
	else if(key < node->data)
	{
		cmp++;
		avl_search(node->left_child,key);
	}
	else 
		avl_search(node->right_child,key);
	return NULL;
}



AvlNode *inOrderSearch(AvlNode *t, int nr, int k)
{
	if(t!=NULL)
	{
		if(nr!=k)
		{
			inOrderSearch(t->left_child, nr+1, k);
			//printf("%d",t->data);
			inOrderSearch(t->right_child, nr+1, k);
		}
		else
			return t;
	}
}

AvlNode *clearTree(AvlNode *t)
{
	if(t!=NULL)
	{
		clearTree(t->left_child);
		clearTree(t->right_child);
		free(t);
		t = NULL;
	}
	return NULL;
}


AvlNode *get_min(AvlNode *t)
{
	cmp++;
	if(t==NULL)
	{
		return NULL;
	}
	else if(t->left_child==NULL)
	{
		
		return t;
	}
	else
	{
		return get_min(t->left_child);
	}
	cmp++;



}

AvlNode *avl_delete( AvlNode *t,int data)
{
	AvlNode *n;
	AvlNode *nod_tmp;

	cmp++;
	if(t==NULL)
		return NULL;

	cmp++;
	if(data < t->data)
	{
		t->left_child = avl_delete (t->left_child, data);
		atr++;
		cmp++;
		if(getHeight(t->right_child)  - getHeight(t->left_child) >=2)
		{
			cmp++;
			if(t->left_child && (data < t->left_child->data))
			{
				t = rotate_RR(t);
				atr++;
			}
			else
			{
				t = rotate_RL(t);
				atr++;
			}
		}
	}

	else if(data > t->data)
	{
		cmp++;
		atr++;
		t->right_child = avl_delete(t->right_child,data);

		cmp++;

		if(getHeight(t->left_child)- getHeight(t->right_child)>=2)
		{
		cmp++;

			if(t->right_child && (data > t->right_child->data))
			{
				t = rotate_LL(t);
			}
			else
			{
				t = rotate_LR(t);
			}
		}
	}

	else if(t->left_child && t->right_child)
	{
		cmp++;
		atr++;
		nod_tmp = get_min(t->right_child);
		t->data = nod_tmp->data;
		t->right_child = avl_delete( t->right_child,t->data);
	}
	else
	{
		atr++;
		nod_tmp = t;
		if(t->left_child == NULL)
			t = t->right_child;
		else if (t->right_child == NULL)
			t = t->left_child;
		free(nod_tmp);
		nod_tmp = NULL;
	}

	return t;
}


AvlNode *OS_SELECT(AvlNode *node, int data)
{
	int tempSize=1;
	cmp++;
	if(node->left_child!=NULL)
		tempSize = get_size(node->left_child)+1;
	cmp++;
	if(data==tempSize)
	{
		//printf("OS_SELECT: return %d \n",x->data);
		return node;
	}
	else
		if(data<tempSize)
		{
			cmp++;
			return OS_SELECT(node->left_child,data);
		}
		else
			return OS_SELECT(node->right_child,data-tempSize);
}


void josephus(int n, int m)
{
	AvlNode *deleteNode;
	int j=1;
	for(int k=n; k>1;k--)
	{
		j=((j+m-2)%k)+1;
		atr++;
		deleteNode = OS_SELECT(root,j);
		atr++;
		printf("Node Deleted %d \n ",deleteNode->data);
		root=avl_delete( root,deleteNode->data);

	}



	
}


AvlNode* avl_add2(AvlNode *root,int key) 
{ 
    if(root == NULL) 
    { 
        root = (AvlNode*)malloc(sizeof(AvlNode)); 
        if(root == NULL) 
        { 
            printf("fail: memory allocation\n"); 
            exit(-1); 
        } 

        (root)->data = key;     
        (root)->left_child = (root)->right_child = NULL; 
    } 
    else if(key < (root)->data) 
    { 
        (root)->left_child = avl_add2((root)->left_child,key); 
        (root) = balance_tree(root);  
    } 
    else if(key > (root)->data) 
    { 
        (root)->right_child = avl_add2(((root)->right_child), key); 
        (root) = balance_tree(root); 
    } 
    else 
    { 
        printf("fail! - duplicated key\n"); 
        exit(-1); 
    } 
    return root; 
} 
AvlNode *fillTree(int n)
{
	for(int i=1;i<=n;i++)
	{
		root=avl_add(root,i);
		atr++;
	}

	return root;
}


void print(int n)
{
	for(int i=1;i<=n;i++)
	{
		printf("\nSEARCH : %d    ",i);
		
		avl_search(root,i);
		printf("\n");
	}
}
void inOrderPrint(AvlNode *root)
{
	
	if (root->left_child!= NULL) 
		{inOrderPrint(root->left_child);
	printf("%d LEFT IS : %d\n",root->data,root->left_child->data);
	}
	
  if (root->right_child != NULL) 
  {inOrderPrint(root->right_child);
  printf("%d RIGHT IS : %d\n", root->data, root->right_child->data);
  }
  

}




int main()
{
	/*FILE *fp = fopen("avl.csv","w");
	
	srand((unsigned)time(NULL));
	for(int i=10;i<=100;i=i+10)
	{
		atr=0;
		cmp=0;
		fillTree(i);
		josephus(i,i/2);
		fprintf(fp,"%d,%d,%d\n",atr,cmp,atr+cmp);
		printf(" %d \n",i);
		
	}
	
	fclose(fp);

	printf("Finish...");*/
	
	// TEST CODE --- 
	fillTree(7);
	print(7);
	inOrderPrint(root);
	josephus(7,3);

	if(root==NULL)
		printf("ROOT IS NULL");
	else
		printf("Survivor/Root is %d and size is %d",root->data,root->size);
	printf("\nroot is : %d , size is %d",root->data,root->size);

	
	free(root);
	


	
	return 0;
}

