#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct tip_nod {
	int cheie;
	int dim;
	struct tip_nod *stg;
	struct tip_nod *dr;
	struct tip_nod *parinte;
} tip_nod;

int nmax=sizeof(tip_nod), nrA, nrC;
tip_nod *rad, *p;

tip_nod *construireArbore(int stg, int dr) {

	tip_nod *q;
	int m;
	nrC++;
	if (stg<=dr)
	{
		m=(stg+dr)/2;
		q=(tip_nod *)malloc(nmax);
		q->cheie=m;
		q->parinte=NULL;
		nrA+=4;
		q->stg=construireArbore(stg,m-1);
		q->dr=construireArbore(m+1,dr);
		if ((q->stg==NULL)&&(q->dr==NULL)) 
		{
			nrA++;
			q->dim=1;
		}
		else if (q->stg==NULL) 
		{
			nrA++;
			q->dim=q->dr->dim+1;
		}
		else if 
			(q->dr==NULL) 
		{
			nrA++;
			q->dim=q->stg->dim+1;
		}
		else 
		{ 
			nrA++;
			q->dim=q->stg->dim + q->dr->dim + 1; 
		}
		if (q->stg!=NULL) 
		{ 
			nrA++;
			q->stg->parinte=q; 
		}
		if (q->dr!=NULL) 
		{ 
			nrA++;
			q->dr->parinte=q; 
		}
		return q;
	}
	return NULL;
}

tip_nod *so_selectie(tip_nod *q,int i) 
{

	nrC+=3;
	int r;
	if (q==NULL) {
		r=0;
	}
	else if (q->stg!=NULL) 
	{
		r=q->stg->dim+1;
	}
	else {
		r=1;
	}
	if (i==r)
	{
		nrA++;
		return q;
	}
	else 
	{
		if (i<r)
		{
			return so_selectie(q->stg,i); 
		}
		else
		{
			if (q->dr!=NULL)
				return so_selectie(q->dr,i-r); 
		}
	}
}

tip_nod *arboreMinim(tip_nod *x) 
{
	while (x->stg!=NULL)
	{
		nrA++;
		x=x->stg;
	}
	return x;
}

tip_nod *succesor(tip_nod *x) 
{
	tip_nod *y;
	nrC++;
	if (x->dr!=NULL) 
	{ 
		return arboreMinim(x->dr);
	}
	nrA++;
	y=x->parinte;
	while ((y!=NULL)&&(x==y->dr)) 
	{ 
		nrA+=2;
		x=y;
		y=y->parinte;
	}
	return y;
}


tip_nod *stergeArbore(tip_nod *z)
{

	tip_nod *y,*x,*t;
	nrC+=6;
	if (z==NULL) {
		return NULL;
	}
	if ((z->stg==NULL)||(z->dr==NULL))
	{
		nrA++;
		y=z;
	}
	else
	{
		nrA++;
		y=succesor(z);
	}
	if (y->stg!=NULL) 
	{ 
		nrA++;
		x=y->stg; 
	}
	else 
	{ 
		nrA++;
		x=y->dr; 
	}
	if (x!=NULL) 
	{
		nrA++;
		x->parinte=y->parinte; 
	}
	if (y->parinte==NULL) 
	{ 
		nrA++;
		rad=x; 
	}
	else if (y==y->parinte->stg) 
	{ 
		nrA++;
		y->parinte->stg=x; 
	}
	else 
	{ 
		nrA++;
		y->parinte->dr=x; 
	} 
	if (y!=z)
	{
		nrA++;
		z->cheie=y->cheie;
		z->dim--;
	}
    t=z;
	while (t->parinte!=NULL)
	{
		t->parinte->dim--;
		t=t->parinte;
		nrA+=2;
	}
	return y;
}

void afisareArbore(tip_nod *t,int d)
{
	int j;
	if (t==NULL) {return;}
	afisareArbore(t->dr,d+1);
	for (j=0;j<=d;j++)
	{
		printf("     ");
	}
	printf("%d %d\n",t->cheie,t->dim);
	afisareArbore(t->stg,d+1);
}

void Josephus(int n, int m) {

	tip_nod *x, *y;
	int i,dim;
	rad=construireArbore(1,n);
	i=m;
	dim=n;
	while(rad!=NULL)
	{
		nrA++;
		x=so_selectie(rad,i);
		y=stergeArbore(x);
		free(y);
		dim--;
		if (dim!=0) {
			i=(i+m-1)%dim;
		}
		if (i==0) {
			i=dim;
		}
	}
}

void Josephus2(int n, int m) {

	tip_nod *x, *y;
	int i,dimensiune;
	rad=construireArbore(1,n);
	i=m;
	dimensiune = n;
	while(rad!=NULL)
	{
		nrA++;
		x=so_selectie(rad,i);
		printf("%d ",x->cheie);
		y=stergeArbore(x);
		free(y);
		dimensiune--;
		if (dimensiune!=0) {
			i=(i+m-1)%dimensiune;
		}
		if (i==0) {
			i=dimensiune;
		}
	}
}

int main () {	

	FILE *fp;
	int i,m,n;
	tip_nod *x,*y;
	fp=fopen("rezultate.csv","w");
	nrA=0;nrC=0;
		printf("n=");
		scanf("%d",&n);
		printf("m=");
		scanf("%d",&m);

		rad=construireArbore(1,n);
		printf("\n");
		afisareArbore(rad,1);
		printf("\nJ(%d %d)= ",n,m);
		Josephus2(n,m);

		fprintf(fp,"%s, %s, %s, %s\n","n","nrA","nrC","nrT");
		fprintf(fp,"\n");
		for(n=100;n<=10000;n+=100)
		{
			nrA=0;
			nrC=0;
			m=n/2;

			Josephus(n,m);

			fprintf(fp,"%d, %d, %d, %d\n",n,nrA,nrC,nrA+nrC);
			
		}

		fclose(fp);

	getch();
	return 0;
}