/*Am observat in urma creerii graficelor ca din punct de vedere al numarului de atribuiri, in cazul mediu statistic si cazul defavorabil,
metoda BubbleSort este cea mai putin eficienta, iar cea prin selectie este cea mai eficienta. In cazul favorabil, Bubblesort si sortarea prin selectie 
nu au nevoie de nicio atribuire, in timp ce metoda prin inserare este cea mai putin eficienta.

Din punct de vedere a numarului de comparatii, in cazul mediu statistic, metoda prin inserare este cea mai eficienta, in timp ce celelalte doua se afla
la un nivel aproximativ egal de eficienta. In cazul favorabil, metoda prin inserare nu necesita nicio comparatie, iar celelalte doua sunt din nou la un nivel
aproximativ egal de eficienta. In cazul defavorabil, toate metodele sunt aproximativ la fel de eficiente/neeficiente.

Din graficele obtinute pentru totalul de operatii se poate observa ca in toate cele trei cazuri metoda bubblesort este mai dezavantajoasa. Pentru cazul mediu statistic,
metodele prin inserare si prin selectie folosesc aproximativ acelasi numar de operatii. In cazul favorabil e mai utila folosirea metodei prin inserare,
in timp ce in cazul defavorabil, metoda prin selectie este cea mai utila.
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
int nra=0,nrc=0;

void copiere(int sursa[],int destinatie[],int n)//Copiaza sirul sursa in sirul destinatie
{
	for(int i=0;i<n;i++){
		destinatie[i]=sursa[i];
	}
}
void bubbleSort(int a[], int n)//sortare prin metoda bulelor
{
  int i,j,temp;
  
  for (i=(n-1);i>0;i--)
  {
    for (j=1;j<=i;j++)
    {
		nrc++;
		if (a[j-1]>a[j])
      {
        temp=a[j-1];
        a[j-1]=a[j];
        a[j]=temp;
		nra+=3;
      }
    }
  }
}
void inserare(int a[],int n)//sortare prin inserare
{
	int aux,i;
	for(int j=1;j<n;j++)
		{  
			aux=a[j];
			nra++;
            i=j-1;
            while (aux<a[i] && i>=0)
			{
				nrc++;
				nra++;
				a[i+1]=a[i];
                i=i-1;
            }
            a[i+1]=aux;
			nra++;
	}
}
void selectie(int a[], int n)//sortare prin selectie
{
	int i,j,minindex,tmp;
	for(i=0;i<n-1;i++)
	{
		minindex=i;
		for(j=i+1;j<n;j++)
		{
			nrc++;
			if(a[j]<a[minindex])
			{
				minindex=j;
				
			}
		}
		nrc++;
		if(minindex!=i){
			tmp=a[i];
			a[i]=a[minindex];
			a[minindex]=tmp;
			nra=nra+3;
			
		}
	}
}
int main(){
	FILE *pf,*pf2;
	char *numef="atrib.csv",*numef2="comp.csv";
	pf=fopen(numef,"w");
	pf2=fopen(numef2,"w");
	int aleator[10000],crescator[10000],temp[10000],descresc[10000];
	int va[101],vc[101],va1[101],vc1[101],va2[101],vc2[101],va3[101],vc3[101],va4[101],vc4[101],va5[101],vc5[101];
	int va6[101],vc6[101],va7[101],vc7[101],va8[101],vc8[101];
	crescator[0]=0;
	int c=0;
	for(int i=0;i<10000;i++){
		
		aleator[i]=rand()%1000;
		crescator[i]=crescator[i-1]+rand()%200;
		
		
	}
	for(int i=0;i<10000;i++){
		descresc[i]=crescator[9999-i];
	}
	for(int i=100;i<=10000;i+=100){
		copiere(aleator,temp,i);
		bubbleSort(temp,i);//sortare metoda bulelor, caz mediu statistic
		va[i/100]=nra;
		vc[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(aleator,temp,i);
		inserare(temp,i);//sortare metoda inserarii, caz mediu statistic
		va1[i/100]=nra;
		vc1[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(aleator,temp,i);
		selectie(temp,i);//sortare selectie, caz mediu statistic
		va2[i/100]=nra;
		vc2[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(crescator,temp,i);
		bubbleSort(temp,i);//sortare metoda bulelor, caz favorabil
		va3[i/100]=nra;
		vc3[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(crescator,temp,i);
		inserare(temp,i);//sortare metoda inserarii, caz favorabil
		va4[i/100]=nra;
		vc4[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(crescator,temp,i);
		selectie(temp,i);//sortare metoda selectiei, caz favorabil
		va5[i/100]=nra;
		vc5[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(descresc,temp,i);
		bubbleSort(temp,i);//sortare metoda bulelor, caz defavorabil 
		va6[i/100]=nra;
		vc6[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(descresc,temp,i);
		inserare(temp,i);//sortare inserare, caz defavorabil
		va7[i/100]=nra;
		vc7[i/100]=nrc;
		nra=0;
		nrc=0;
		copiere(descresc,temp,i);
		selectie(temp,i);//sortare selectie, caz defavorabil
		va8[i/100]=nra;
		vc8[i/100]=nrc;
		nra=0;
		nrc=0;
		char ch='%';
		printf("Loading%d%c\r",c,ch);
		c++;

	}
	for (int i=0;i<=10000;i=i+100){
		fprintf(pf,"%d,",i);
		fprintf(pf2,"%d,",i);
	}
	fprintf(pf,"\nMSBubble,");
	for(int i=1;i<=100;i++){
		fprintf(pf,"%d,",va[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"\nMSBubble,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"MSIns,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va1[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"MSIns,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc1[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"MSSel,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va2[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"MSSel,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc2[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"CFBubble,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va3[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"CFBubble,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc3[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"CFIns,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va4[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"CFIns,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc4[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"CFSel,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va5[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"CFSel,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc5[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"CDEFBubble,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va6[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"CDEFBubble,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc6[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"CDEFIns,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va7[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"CDEFIns,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc7[i]);
	}
	fprintf(pf2,"\n");
	fprintf(pf,"CDEFSel,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va8[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf2,"CDEFSel,");
	for(int i=1;i<101;i++){
		fprintf(pf2,"%d,",vc8[i]);
	}
	fprintf(pf2,"\n");
	fclose(pf);
	fclose(pf2);
	pf=fopen("atrib+comp.csv","w");
	
	for (int i=0;i<=10000;i=i+100){
		fprintf(pf,"%d,",i);
	}
	fprintf(pf,"\nCMSBubble,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va[i]+vc[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CMSIns,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va1[i]+vc1[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CMSSel,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va2[i]+vc2[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CFBubble,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va3[i]+vc3[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CFIns,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va4[i]+vc4[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CFSel,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va5[i]+vc5[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CDEFBubble");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va6[i]+vc6[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CDEFIns,");

	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va7[i]+vc7[i]);
	}
	fprintf(pf,"\n");
	fprintf(pf,"CDEFSel,");
	for(int i=1;i<101;i++){
		fprintf(pf,"%d,",va8[i]+vc8[i]);
	}
	fclose(pf);
	
	return 0;
}
