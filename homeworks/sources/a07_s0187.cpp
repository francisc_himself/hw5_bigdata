#include <stdio.h>
#include <stdlib.h>
#define m 9

int vectorParinte[m] = {2, 7 ,5, 2, 7, 7 ,-1, 5,2};
int vectorFii[100];
int asign[10];
int n;


typedef struct nod2{
    int key;
	int fii[100];
	int nrCopii;
}NOD2;

typedef struct nod3{
	int key;
	nod3 *fiu;
	nod3 *frate; 
}NOD3;

NOD2 *noduri2[100];
NOD3 *noduri3[100];
int radacina;
NOD3 *t3[100];
//pentru fiecare parinte stiu cati copii are si care sunt, ii adaug in vectorul de fii
//aflu si root
//R1
void parent_multiway_tree() {
  // NOD2 *noduri2[100];
   NOD2 *nod;
   //initializez vectorul
   for(int i=0; i<m ; i++){
		nod=(NOD2*)malloc(sizeof(NOD2));
		nod->key=i;
		nod->nrCopii =0;
		noduri2[i]=nod;
   }
    for (int i=0; i<m; i++) {
		if(vectorParinte[i]>0){
			nod=noduri2[vectorParinte[i]];
			nod->fii[nod->nrCopii]=i+1;
			nod->nrCopii =nod->nrCopii+1;
		}else{
			radacina=i+1;
		}
	}
} 
void multiway_tree_binary()
{
	NOD3 *nod;
	for(int i=1; i<=m ; i++){
		nod=(NOD3*)malloc(sizeof(NOD3));
		nod->key=i;
		nod->frate=NULL;
		nod->fiu=NULL;
		noduri3[i]=nod;
	}
	for(int j=1; j<=m; j++){
		NOD2 *p=noduri2[j];
		if(p!=NULL){
			if(p->nrCopii!=0){
				noduri3[j]->fiu=noduri3[p->fii[0]];
				for(int i=1; i<p->nrCopii; i++){
					noduri3[p->fii[i-1]]->frate=noduri3[p->fii[i]];
				}
			}
		}
	}
} 
void afisareT1()
{
	for(int j=1; j<m; j++){
       NOD2 *p=noduri2[j];
	   if(p!=NULL){
		   if(p->nrCopii==0){
			  printf("\n%d nu are copii",p->key);
		   } else{
				printf("\nFii lui %d sunt:",p->key);
				for(int i=0;i<p->nrCopii;i++)
					 printf(" %d ",p->fii[i]);
			} 
		}
	}
} 
 
void prettyPrint(int key ,int nivel)
{
	for(int i=0; i<nivel; i++){
		printf("    ");
	}
	printf("%d \n",key);
	if(noduri3[key]->fiu!=NULL){
		prettyPrint(noduri3[key]->fiu->key,nivel+1);
	}
	if(noduri3[key]->frate!=NULL){
		prettyPrint(noduri3[key]->frate->key,nivel);
	}
}

void write() {
    for(int j=1; j<=m; j++){
       NOD3 *p=noduri3[j];
	   if(p!=NULL){
			int fiu=-1;
			int frate=-1;
			if(p->fiu!=NULL){
			   fiu=p->fiu->key;
			}
			if(p->frate!=NULL){
			   frate=p->frate->key;
			}
			printf("\n nodul: %d fiul: %d frate: %d",p->key,fiu,frate);
	   }
	}
} 
 
int main() {
    parent_multiway_tree();
	printf("%d",radacina);
    afisareT1();
	

	multiway_tree_binary();
	write();
	printf("\n Pretty Print: \n");
	prettyPrint(radacina,0);
	printf("\n");
    return 0;
}