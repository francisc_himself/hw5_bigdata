/*
***ANONIM*** ***ANONIM***-***ANONIM***  gr.***GROUP_NUMBER***
	Pentru cazul mediu statistic, din punct de vedere al numarului de asignari si comparatii
eficienta sortarii prin metoda heap bottom-up este redusa decat quicksort.
	Cazul cel mai devaforabil pentru quicksort este atunci cand vectorul este sortat iar pivotul dupa care se face 
sortarea este  primul sau ultimul element din vector. Eficienta in acest caz este de O(n^2).
	Cazul favorabil pentru quicksort este atunci cand elementul dupa care are loc sortarea se gaseste
la jumatatea vectorului, iar vectorul trebuie sa fie sortat descrescator.Eficienta in acest caz este O(n*lg(n)).

*/

#include <stdio.h>
#include <stdlib.h> 

#include "Profiler.h"
Profiler profiler("demo"); 

#define MAX 10000
int lung,size;

////--------------------------bottom-up--------------------------////
void afisare(int *v,int n,int k,int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
	for(int i=1;i<=nivel;i++)
	{
		printf("   ");
	}
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);
}

int parinte(int i){
	return i/2;
}
int left(int i){
	return 2*i;
}
int right(int i){
	return 2*i+1;
}

int* reconstructie(int *v,int i, int dim)
{
	int st=left(i);
	int dr=right(i);
	int max;
	if (st<=dim && v[st]>v[i]){max=st;}
	else max=i;
	if(dr<=dim && v[dr]>v[max]){max=dr;}
	profiler.countOperation("bucomp",lung,2);
	if (max!=i){
		int aux=v[i];
		v[i]=v[max];
		v[max]=aux;
		profiler.countOperation("buassign",lung,3);
		reconstructie(v,max,dim);
	}
	return v;
}

void constructie(int *v, int n)
{
	int m=n;
	for(int i=m/2;i>0;i--)
		reconstructie(v,i,n);
}


int* buheapsort(int* v,int n)
{
	int m=n;
	constructie(v,m);
	for(int i=n;i>=2;i--)
	{
		int aux=v[1];
		v[1]=v[i];
		v[i]=aux;
		profiler.countOperation("buassign",lung,3);
		m--;
		reconstructie(v,1,m);
	}
	return v;
}	



////---------------------Quicksort--------------------------////


//sortare pentru best case unde se sorteaza avand ca pivot mijlocul intervalului bazata pe HOARE-PARTITION 
void qsortb(int *v,int st,int dr)
{
	int aux,i,j,x;

	x= v[(st+dr)/2];         //mijlocul intervalului pentru best-case
	profiler.countOperation("qassign",lung,1);
	i = st; j = dr;
	while (i<=j)
	{
		profiler.countOperation("qucomp",lung,2);
		while(v[i] < x)	{
			i++; 
			profiler.countOperation("qucomp",lung,1);
		}
		while(v[j] > x) {
			j--; 
			profiler.countOperation("qucomp",lung,1);
		}
		if(i <= j)                         
		{
			aux = v[i];
			v[i] = v[j];
			v[j] = aux;
			profiler.countOperation("qassign",lung,3);
			i++; j--;
		}
	}
	//cand nu mai avem ce face schimbam intervalul
	if(st < j) qsortb(v,st,j);  //crescator
	if(i < dr) qsortb(v,i,dr);  //crescator
}

int part(int *v,int st,int dr)
{	
	int aux;
	int x=v[dr];
	profiler.countOperation("qassign",lung,1);
	int i=st-1;
	for(int j=st;j<=dr-1;j++)
	{
		if(v[j]<=x)
		{
			i++;
			aux=v[i];
			v[i]=v[j];
			v[j]=aux;
			profiler.countOperation("qassign",lung,3);
		}
		profiler.countOperation("qucomp",lung,1);
	}
	aux=v[i+1];
	v[i+1]=v[dr];
	v[dr]=aux;
	profiler.countOperation("qassign",lung,3);
	return i+1;
}

int* quick(int *v,int st,int dr)
{
	int m;
	if (st<dr) 
	{
		m=part(v,st,dr);
		quick(v,st,m-1);
		quick(v,m+1,dr);
	}
	return v;
}
void main()
{
	int v[MAX],v1[MAX];

	srand(time(NULL));
	printf("alege optiunea:\n 1:mediu statistic\n 2:worst case\n 3:best case\n 4:exit");
	///------cazul mediu statistic 
	/*for(int t=0;t<10;t++){
	for(int n=100;n<2000;n+=100){
	printf("n=%d\n",n);
	FillRandomArray(v,n);
	lung=n;
	memcpy(v1,v,n*sizeof(int));
	buheapsort(v1,lung+1);
	memcpy(v1,v,n*sizeof(int));
	quick(v1,0,lung-1);
	}
	}
	profiler.addSeries("buSeria","buassign","bucomp");
	profiler.addSeries("QSeria","qassign","qucomp");
	profiler.createGroup("SERIA","buSeria","QSeria");*/

	////----------------worst case
	/*for(int n=100;n<2000;n+=100){
	printf("n=%d\n",n);
	FillRandomArray(v,n,1,50000,false,2);
	lung=n;
	memcpy(v1,v,n*sizeof(int));
	buheapsort(v1,lung+1);
	memcpy(v1,v,n*sizeof(int));
	quick(v1,0,lung-1);
	}
	profiler.addSeries("buSeria","buassign","bucomp");
	profiler.addSeries("QSeria","qassign","qucomp");
	profiler.createGroup("SERIA","buSeria","QSeria");*/

	///////////////---------------------best case
	for(int n=100;n<2000;n+=100){
		printf("n=%d\n",n);
		FillRandomArray(v,n,1,50000,false,2);
		lung=n;
		memcpy(v1,v,n*sizeof(int));
		buheapsort(v1,lung+1);
		memcpy(v1,v,n*sizeof(int));
		qsortb(v1,0,lung-1);
	}
	profiler.addSeries("buSeria","buassign","bucomp");
	profiler.addSeries("QSeria","qassign","qucomp");
	profiler.createGroup("SERIA","buSeria","QSeria");




	profiler.showReport();


	//int v[]={0,4,1,3,2,16,9,10,14,8,7};
	//int v1[]={0,4,1,3,2,16,9,10,14,8,7};
	////afisare(v,10,1,0);
	////maxHeap(v,10);
	////heapsort(v,10);
	////for(int i=0;i<11;i++)
	////construieste2(v1,10);
	////*tdheapsort(v1,10);
	////afisare(v1,10,1,0);*/
	//afisare(v,10,1,0);
	//buheapsort(v,10);
	////construieste2(v,10);
	//afisare(v,10,1,0);

	//printf("quick");
	////quick(v1,1,10);
	//qsortb(v1,1,10);
	//for(int i=0;i<11;i++)
	//	printf("%d ",v1[i]);


}
