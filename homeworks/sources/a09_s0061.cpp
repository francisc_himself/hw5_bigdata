#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

typedef struct varf
{
	int nume;
	int color;
	int d;
	struct varf *p;
} varf;


typedef struct lista
{
	varf *v;
	struct lista *urm;
} lista;

lista *prim=NULL;
lista *ultim=NULL;
int n=6;//nr varf
varf * a[100];
int g[100][100];
int lung=1000; //trebuia declarata la infinit
int parinte[100];

void pprint(int par, int tabs)
{
	for (int i = 0; i < 6; i++)
		if (parinte[i] == par)
		{
			for (int j = 0; j < tabs; j++)
				printf("   ");
			printf("%d\n",i+1);
			pprint(i+1, tabs + 1);
		}
}


void creare()
{
	for(int i=1;i<=n;i++)
	{
	varf *u=(varf*)malloc(sizeof(varf));
	u->nume=i;
	a[i]=u;
	}
	g[1][1] = 2;
	g[1][2] = 3;
	g[1][3] = 0;
	g[1][4] = 0;

	g[2][1] = 3;
	g[2][2] = 4;
	g[2][3] = 0;
	g[2][4] = 0;

	g[3][1] = 5;
	g[3][2] = 0;
	g[3][3] = 0;
	g[3][4] = 0;

    g[4][1] = 0;
	g[4][2] = 0;
	g[4][3] = 0;
	g[4][4] = 0;

	g[5][1] = 6;
	g[5][2] = 0;
	g[5][3] = 0;
	g[5][4] = 0;

    g[6][1] = 0;
	g[6][2] = 0;
	g[6][3] = 0;
	g[6][4] = 0;
}

//Culori:alb=0 , gri=1 , negru=2

void bfs()
{	
	lista *curent;
	for(int i=2; i<=n; i++)
	{
		a[i]->color=0;
		a[i]->d=lung;
		a[i]->p=NULL;
	}
	a[1]->color = 1;
	a[1]->d = 0;
	a[1]->p = NULL;

	lista *aux = (lista*)malloc(sizeof(lista));
	prim = aux;
	prim->v = a[1];
	prim->urm = NULL;
	ultim = prim;
	curent = prim;
	int par=0;
	parinte[par]=0;
	while(curent != NULL)
	{		
		varf *u = curent->v;
		int i=1;
		while (g[u->nume][i]!=0)
		{
			varf *vf=a[g[u->nume][i]];
			if(vf -> color == 0)
			{
				vf->color = 1;
				vf->d = u->d + 1;
				vf->p = u;
				a[g[u->nume][i]] = vf;
				par++;
				parinte[par] = u->nume;

				lista *aux = (lista*)malloc(sizeof(lista));
			
				aux->v = vf;
				aux->urm = NULL;
				ultim->urm = aux;
				ultim = aux;
			}
		i++;
		}
		u -> color = 2;
		a[u->nume] = u;
		printf("%d ",u->nume);
		curent = curent->urm;	
	}
	printf("\nBFS \n");
}
void main()
{
	printf("Cream graful:\n");
	creare();
	printf("Facem BFS pentru nodurile\n");
	bfs();
	pprint(0,0);
	getch();
}