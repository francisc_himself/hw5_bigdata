/*
***ANONIM*** Diana ***ANONIM***
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

typedef struct MULTI_NODE
{
	int key;
	int count; //nr fiilor 
	struct MULTI_NODE *child[50];
}MULTI_NODE;

typedef struct BINARY_NODE{
	int key;
	struct BINARY_NODE *left,*right;
}BINARY_NODE;

MULTI_NODE *nodes[50];
BINARY_NODE *nod[50];

MULTI_NODE* createMN(int key)
{
MULTI_NODE* p; 
p=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
p->key=key;
p->count=0;
return p;
} 

void insert_MN(MULTI_NODE *parent,MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
}

MULTI_NODE* transform1(int t[],int size)
{
	MULTI_NODE *root=NULL;
	//MULTI_NODE *nodes[50];
for (int i=0;i<size;i++)
{
	nodes[i]=createMN(i);
}
for(int i=0;i<size;i++)
{
	if(t[i]==-1) root=nodes[i];
	else
	{
		insert_MN(nodes[t[i]],nodes[i]);
	}
}
return root;
}
void pretty_printMN(MULTI_NODE *nod,int nivel=0)
{
	for (int i=0;i<nivel;i++)
		printf("    ");
	printf("%d\n",nod->key);
	for(int i=0;i<nod->count;i++)
	{
		pretty_printMN(nod->child[i],nivel+1);
	}
}

BINARY_NODE* createBN(int key)
{
BINARY_NODE* p; 
p=(BINARY_NODE*)malloc(sizeof(MULTI_NODE));
p->left=NULL;
p->right=NULL;
p->key=key;
return p;
} 

BINARY_NODE *transform2(BINARY_NODE *radB,MULTI_NODE *radM)
{ 
	BINARY_NODE *st,*dr,*aux;
   if(radM!=NULL)
   {
	   if (radM->count==1){
		   st=createBN(radM->child[0]->key);
		   radB->left=transform2(st,radM->child[0]);}
	   else if(radM->count!=0)
	   {
		   st=createBN(radM->child[0]->key);
		   radB->left=transform2(st,radM->child[0]);
		   aux=radB;
		   radB=radB->left;
		   int i=1;
		   while(i<radM->count){
			   dr=createBN(radM->child[i]->key);
			   radB->right=transform2(dr,radM->child[i]);
			   radB=radB->right;
			   i++;

		   }
		   radB=aux;
	   }
	   return radB;
   }
   return NULL;
}
void pretty_printBN(BINARY_NODE *nod,int nivel=0)
{
	if(nod!=0){
	for (int i=0;i<nivel;i++)
		printf("    ");
	printf("%d\n",nod->key);
	pretty_printBN(nod->left,nivel+1);
	pretty_printBN(nod->right,nivel);
     }
}


int main()
{
	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(t[0]);
    //constructie multiay tree
	MULTI_NODE *root=transform1(t,size);
    pretty_printMN(root,0);
	
	//constructie arbore binar
	BINARY_NODE *root1=createBN(root->key);
	root1=transform2(root1,root);
	 pretty_printBN(root1,0);

	return 0;
}