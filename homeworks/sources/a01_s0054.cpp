/****ANONIM*** ***ANONIM*** 

in aceasta lucrare sunt prezentate 3 metode de sortare a unui vector: bubble sort, selectia si insertia.

Metoda bulelor: in toate cele trei cazuri avem aproximativ acelasi numar de comparari. diferenta este la asignari. cazul cel mai
favorabil la aceasta metoda este cand vectorul este crescator, deoarece nu se mai fac asignari. cazul defavorabil este atunci 
cand vectorul este descrescator, deoarece la fiecare comparatie o sa se faca asignari, iar cazul in care vectorul are elemente
aleatoare este apropiat de cel defavorabil din cauza ca numarul de asignari variaza.

Metoda selectiei: cazul favorabil este atunci cand vectorul este descrescator, deoarece minimul va fi mereu la final=>operatii de 
asignare foarte putine. cazul defavorabil este atunci cand vectorul este crescator, deoarece minimul va fi mereu la inceput=>
la fiecare comparatie se va face si asignare

Metoda insertiei: cazul favorabil este atunci cand vectorul este descrescator, deoarece in acest caz while-ul nu s-ar executa
niciodata.iar cazul cel mai defavorabil este atunci cand vectorul este crescator deoarece ciclul while se va executa de fiecare
data.*/


#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("***ANONIM***12345");
void bubble(int v[],int n)
{
	int aux;
    for (int i=1;i<=n-1;i++)
		for(int j=0;j<=n-1;j++)
		{
			if(v[j]>v[j+1]) 
			{
                 
                 aux=v[j];
				 v[j]=v[j+1];
				 v[j+1]=aux;
				 profiler.countOperation("bubble_assign",n,3);
			}
         profiler.countOperation("bubble_comp",n,1);
		}
			

}
void selectie(int v[],int n)
{
	
	int aux1;
   for(int i=0;i<=n;i++)
   {
     int imin=i;
	 for(int j=i+1;j<=n;j++)
	 {
		 if(v[j]<v[imin])
		 {
            
			imin=j;
		    
		 }
		  profiler.countOperation("selectie_comp",n);
	 }
      aux1=v[imin];
	  v[imin]=v[i];
	  v[i]=aux1;
	  profiler.countOperation("selectie_assign",n,3);

   }
}
void insertie(int v[],int n)
{
  for(int i=1;i<=n;i++)
  {
    int x=v[i];
	int j=0;
	while(v[j]<x)
	{
        j++;
		profiler.countOperation("insertie_comp",n,1);
	}
	for(int k=i;k>=j+1;k--)
	{
		v[k]=v[k-1];
		profiler.countOperation("insertie_assign",n,1);
	}
	v[j]=x;
	profiler.countOperation("insertie_assign",n,3);
  }
}

void main()
{

	int n;
	int v[10000],w[10000];
	//Cazul normal- cu elemente aleatoare
	/*for(n=100;n<=3000;n+=100)
	{
		
      FillRandomArray(v,n); 
	  printf("n=%d\n",n);
	  memcpy(w,v,n*sizeof(int));
	  bubble(w,n);
	  memcpy(w,v,n*sizeof(int));
      selectie(w,n);
	  memcpy(w,v,n*sizeof(int));
	  insertie(w,n);
	}*/
	//Cazul favorabil- cu elementele vectorului in ordine crescatoare 
	/*for(n=100;n<=3000;n+=100)
	{
		
      FillRandomArray(v,n,10,50000,false,1);
	  printf("n=%d\n",n);
	  memcpy(w,v,n*sizeof(int));
	  bubble(w,n);
	  memcpy(w,v,n*sizeof(int));
      selectie(w,n);
	  memcpy(w,v,n*sizeof(int));
	  insertie(w,n);
	}*/
	//Cazul defavorabil - cand vectorul este unul descrescator 
	for(n=100;n<=3000;n+=100)
	{
		
     FillRandomArray(v,n,10,50000,false,0);
	  printf("n=%d\n",n);
	  memcpy(w,v,n*sizeof(int));
	  bubble(w,n);
	  memcpy(w,v,n*sizeof(int));
      selectie(w,n);
	  memcpy(w,v,n*sizeof(int));
	  insertie(w,n);
	}
	profiler.createGroup("comparari","bubble_comp","selectie_comp","insertie_comp");
	profiler.createGroup("asignari","bubble_assign","selectie_assign","insertie_assign");
	profiler.createGroup("Rezultat_final","bubble_comp","selectie_comp","insertie_comp","bubble_assign","selectie_assign","insertie_assign");
	profiler.showReport();

}