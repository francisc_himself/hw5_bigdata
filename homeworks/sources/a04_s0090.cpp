// QuickHeap.cpp : Defines the entry point for the console application.
// Description
// This program compares the running time of two different sorting algorithms: heapsort and quicksort
// For comparing the algorithms we perform the operations for 5 times for different length arrays
// Results:
//		Heapsort is better than quicksort beacuse it runs in linear time instead of O(nlogn) which is the complexity of quicksort
//      Evan that, quicksort behaves better for arrays with length less than 1000
// Quicksort analysis:
// Best Case: the elements in the array are in ascending order and the pivot is chosen in the half of the partition
//			  the sorting is linear
// Worst Case: the elements in the array are in ascending order and the pivot is chosen at the beginning of the

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "stdafx.h"
#include "Profiler.h"
Profiler P("heap");



int A[10001] = {15, 20, 7, 8, 3, 3, 4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
int B[10001];
int n;

int parent(int i);
int left(int i);
int right(int i);
void maxHeapify(int A[], int i);
void printHeap(int A[]);
void buildMaxHeapButtomUp(int A[]);
void heapsort(int A[], int n);
int partition(int A[], int p, int r);
int partitionBest(int A[], int p, int r);
int partitionWorst(int A[], int p, int r);
void quicksort(int A[], int p, int r);
void quicksortBest(int A[], int p, int r);
void quicksortWorst(int A[], int p, int r);
int randomPartition(int A[], int p, int r);
void randomQuicksort(int A[], int p, int r);
void averageCase();
void bestCase();
void worstCase();


int main(){
	/*for (int i=0; i<A[0]+1; i++){
		printf("%i ", A[i]);
	}
	srand(time(NULL));
	//printf("\n%i ", rand() % 5);
	heapsort(A, A[0] - 1);
	printf("\n");
	for (int i=0; i<A[0]+2; i++){
			printf("%i ", A[i]);
		}
	system("pause");
	//p.countOperation("Heapsort Operations", n-1, 2);*/
	averageCase();
	bestCase();
	worstCase();
	return 0;
}

int parent(int i){
	return i/2;
}

int left(int i){
	return 2*i;
}

int right(int i){
	return 2*i + 1;
}

void maxHeapify(int A[], int i){
	int l = left(i);
	int r = right(i);
	int max;

	P.countOperation("Heapsort Operations", n-1, 1);
	if ( (l <= A[0]) && (A[l] > A[i]) ){
		max = l;
	} else {
		max = i;
	}
	P.countOperation("Heapsort Operations", n-1, 1);
	if ( (r <= A[0]) && (A[r] > A[max]) ){
		max = r;
	}

	if (max != i){
		P.countOperation("Heapsort Operations", n-1, 3);
		int aux = A[i];
		A[i] = A[max];
		A[max] = aux;
		maxHeapify(A, max);
	}
}

void printHeap(int A[]){
	int max = 1;
	int i = 1, j = 0;
	while (i <= A[0]){
		j = 0;
		while ( (j < max) && (i <= A[0])){
			printf("%i ", A[i]);
			i++;
			j++;
		}
		max *= 2;
		printf("\n");
	}
}

void buildMaxHeapButtomUp(int A[]){
	for (int i = A[0] / 2; i>0; i--){
		maxHeapify(A, i);
	}
}

void heapsort(int A[], int length){
	buildMaxHeapButtomUp(A);
	for (int i = A[0]; i>1; i--){
		P.countOperation("Heapsort Operations", n-1, 3);
		int aux = A[1];
		A[1] = A[i];
		A[i] = aux;
		A[0]--;
		maxHeapify(A, 1);
	}
	A[0] = length;
}

int partition(int A[], int p, int r){
	P.countOperation("Quicksort Operations", n-1, 1);
	int pivot = A[r];
	int i = p - 1;
	int aux;
	for (int j = p; j < r; j++){
		if (A[j] <= pivot){
			i++;
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;
			P.countOperation("Quicksort Operations", n-1, 3);
 		}
		P.countOperation("Quicksort Operations", n-1, 1);
	}
	 P.countOperation("Quicksort Operations", n-1, 3);
	aux = A[i+1];
	A[i+1] = A[r];
	A[r] = aux;
	return i + 1;
}

int partitionBest(int A[], int p, int r){
	P.countOperation("Quicksort Best Operations", n-1, 1);
	int pivot = A[(r + p)/2];
	int i = p - 1;
	int aux;
	for (int j = p; j < r; j++){
		if (A[j] <= pivot){
			i++;
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;
			P.countOperation("Quicksort Best Operations", n-1, 3);
 		}
		P.countOperation("Quicksort Best Operations", n-1, 1);
	}
	 P.countOperation("Quicksort Best Operations", n-1, 3);
	aux = A[i+1];
	A[i+1] = A[r];
	A[r] = aux;
	return i + 1;
}

int partitionWorst(int A[], int p, int r){
	P.countOperation("Quicksort Worst Operations", n-1, 1);
	int pivot = A[p];
	int i = p - 1;
	int aux;
	for (int j = p; j < r; j++){
		if (A[j] <= pivot){
			i++;
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;
			P.countOperation("Quicksort Worst Operations", n-1, 3);
 		}
		P.countOperation("Quicksort Worst Operations", n-1, 1);
	}
	 P.countOperation("Quicksort Worst Operations", n-1, 3);
	aux = A[i+1];
	A[i+1] = A[(r+p)/2];
	A[(r+p)/2] = aux;
	return i + 1;
}

void quicksort(int A[], int p, int r){
	if (p<r){
		int q = partition(A, p, r);
		quicksort(A, p, q-1);
		quicksort(A, q+1, r);
	}
}

void quicksortBest(int A[], int p, int r){
	if (p<r){
		int q = partitionBest(A, p, r);
		quicksortBest(A, p, q-1);
		quicksortBest(A, q+1, r);
	}
}

void quicksortWorst(int A[], int p, int r){
	if (p<r){
		int q = partitionWorst(A, p, r);
		quicksortWorst(A, p, q-1);
		quicksortWorst(A, q+1, r);
	}
}

int randomPartition(int A[], int p, int r){
	srand(time(NULL));
    int pivotIndex = p + rand()%(r - p + 1);
    int pivot = A[pivotIndex];
    int aux = A[pivotIndex];
    A[pivotIndex] = A[r]; // move pivot element to the end
    A[r] = aux;
    pivotIndex = r;
    int i = r;
    for(int j=p; j< r; j++){
        if(A[j] <= pivot){
            i++;
            aux = A[i];
            A[i] = A[j];
            A[j] = aux;
        }
    }
    aux = A[i+1];
    A[i+1] = A[pivotIndex];
    A[pivotIndex] = aux;
    return i + 1;
}

void randomQuicksort(int A[], int p, int r){
	if (p<r){
		int q = randomPartition(A, p, r);
		randomQuicksort(A, p, q-1);
		randomQuicksort(A, q+1, r);
	}
}

void averageCase(){
	for(n=101; n<10002; n+=100){
		for(int i=0; i<5; i++){
			// perform counting for 5 different arrays
			FillRandomArray(A, n, 0, 100000, false, 0);
			A[0] = n - 1;
			for (int j = 0; j<n+2; j++){
				B[j] = A[j];
			}
			heapsort(A, A[0] - 1);
			quicksort(B, 1, B[0]);
		}
	}

	P.createGroup("Average Case Total Operations", "Heapsort Operations", "Quicksort Operations");

	//P.showReport();
}

void bestCase(){
	for(n=101; n<10002; n+=100){
		for (int i = 1; i<n; i++){
			A[i] = i;
		}
		A[0] = n - 1;
		quicksortBest(A, 1, A[0]);
		
	}

	P.createGroup("Quicksort Best Case Total Operations", "Quicksort Best Operations");

	//P.showReport();
}

void worstCase(){
	for(n=101; n<10002; n+=100){
		for (int i = 1; i<n; i++){
			A[i] = i;
		}
		A[0] = n - 1;
		quicksortWorst(A, 1, A[0]);
		
	}

	P.createGroup("Quicksort Worst Case Total Operations", "Quicksort Worst Operations");

	P.showReport();
}

