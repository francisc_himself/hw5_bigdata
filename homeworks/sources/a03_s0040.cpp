#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
#include "Profiler.h"
Profiler profiler ("sortare");
using namespace std;
#define NEG_INF INT_MIN

int length, heap_size;



int left(int i)
{
	return 2*i;
}
int right(int i)
{
	return 2*i+1;
}
int parent(int i)
{
	return i/2;
}


// heapsort

void MAX_HEAPIFY(int *A, int i)
{
	int l, r, largest, aux;
	l = left(i);
	r = right(i);
profiler.countOperation("heapsort",length);
	if(l <= heap_size && A[l] > A[i])
		largest = l;
	else 
		largest = i;
profiler.countOperation("heapsort",length);
	if(r <= heap_size && A[r] > A[largest])
		largest = r;
	if(largest != i)
	{
		profiler.countOperation("heapsort",length,3);
		aux = A[i];
		A[i] = A[largest];
		A[largest] = aux;
		MAX_HEAPIFY(A, largest);
	}

}
void BUILD_MAX_HEAP_BU(int *A)
{
	heap_size = length;
	for(int i = length/2; i >= 1; i--)
		MAX_HEAPIFY(A,i);
}
void HEAPSORT(int *A)
{
	int aux;
	BUILD_MAX_HEAP_BU(A);
	for(int i = length; i >= 2; i--)
	{
		profiler.countOperation("heapsort",length,3);
		aux = A[1];
		A[1] = A[i];
		A[i] = aux;
		heap_size --;
		MAX_HEAPIFY(A, 1);
	}
}

///// QUICKSORT 


int PARTITION(int *B, int p,int r)
{
int x,i,j,aux;
profiler.countOperation("quicksort",length,1);
x=B[r];
i=p-1;
for(j=p;j<=r-1;j++)
{
   profiler.countOperation("quicksort",length,1);
    if(B[j]<=x)
	{
     i++;
	 profiler.countOperation("quicksort",length,3);
	 aux=B[i];
	 B[i]=B[j];
	 B[j]=aux;
     }
}
profiler.countOperation("quicksort",length,3);
aux=B[i+1];
B[i+1]=B[r];
B[r]=aux;
return i+1;
}

void QUICKSORT(int *B,int p,int r)
{ 
	int q;
	if(p<r) {
	        q=PARTITION(B,p,r);
			QUICKSORT(B,p,q-1);
			QUICKSORT(B,q+1,r);
	        }
}

void afisare(int *A,int n,int k,int nivel)
{
	if(k>n) return;
	afisare(A,n,2*k+1,nivel+1);
		for(int i=1;i<=nivel;i++)
		{
			printf("   ");

			//cout<<"   ";
		}
		printf("%d\n",A[k]);
		//cout<<endl<<A[k];
	afisare(A,n,2*k,nivel+1);
}

void afisare2(int *A, int n)
{
int i;
for(i=1;i<=n;i++)
	cout<<A[i]<<"  ";
}


void QUICKSORT_FAVORABIL (int *A, int p, int r)
{

    int i=p, j=r,aux;
    int x=A[(p+r)/2];
	profiler.countOperation("quicksort_favorabil",length,1);
    do
    {   
		
        while (A[i]<x) 
			{
				i++;
				profiler.countOperation("quicksort_favorabil",length,1);
	    	}
        while (A[j]>x)
			{
				j--;
				profiler.countOperation("quicksort_favorabil",length,1);
		    }
        if (i<=j)
        {
            aux=A[i];
			A[i]=A[j];
			A[j]=aux;
            profiler.countOperation("quicksort_favorabil",length,3);
			i++;
			j--;
        }
    } while (i<=j);

    
    if (p<j) QUICKSORT_FAVORABIL(A, p, j);
    if (i<r) QUICKSORT_FAVORABIL(A, i, r);
}


void caz_mediu()
{
	int A[10001],B[10001];
	for(int i=1; i <= 5; i++)
		for(length = 100; length <= 10000; length += 100)
		{
			FillRandomArray(A,length);
		    memcpy(B,A,length*sizeof(int));
			 HEAPSORT(A);
			 QUICKSORT(B,1,length);
             
		}
		
		profiler.createGroup("CAZ_MEDIU","heapsort","quicksort");
	    profiler.showReport();
}

void caz_defavorabil()
{
  int A[10001];
  for(length = 100; length <= 10000; length += 100)
	  {
	    FillRandomArray(A,length,10,50000,false,2);
		QUICKSORT(A,1,length);
      }
  profiler.createGroup("CAZ_DEFAVORABIL","quicksort");
  profiler.showReport();
}

void caz_favorabil()
{
int A[10001];
for(length = 100; length <= 1000; length += 100)
	  {
	    FillRandomArray(A,length,10,50000,false,2);
		QUICKSORT(A,1,length);
      }
  profiler.createGroup("CAZ_FAVORABIL","quicksort_favorabil");
  profiler.showReport();

}

void main()
{
int A[10001],B[10001];
//cout<<"dati nr de elemnte=";
//cin>>length;
//
//for(int i=1;i<=length;i++)
//
//{
//cout<<"dati elemntul "<<i<<" =" ;
//cin>>A[i];
//B[i]=A[i];
//}
//
//
//HEAPSORT(A);
//afisare(A,length,1,0);
//cout<<"sortare quicsort: "<<endl;
//QUICKSORT(B,1,length);
//afisare2(B,length);

//caz_mediu();
//caz_defavorabil();
caz_favorabil();
getch();
}