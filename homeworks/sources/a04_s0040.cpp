#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <iostream>
//#include "Profiler.h"
//Profiler profiler ("sortare");
using namespace std;
#define NEG_INF INT_MIN
 
struct nod
  {int info;
   nod *next;
};

struct Heap_Elt{
	int index;
	nod *elem;
};

nod *head[1000];
nod *tail[1000];
nod *joinedHead,*joinedTail;

Heap_Elt heap[1000];
 
nod *p,*u ;         // acceseaza primul respective ultimul nod
int n, dim=0;  //numarul de noduri
int length, heap_size;

void cre_ad(nod *c)     //functia de creare si adaugare  a unui nou element
{
	//nod *c;
 if(!p)                   //daca lista este vida (p==0) se aloca primul nod
    {p=new nod;
     cout<<"informatie utila :";
     cin>>p->info;
     u=p;            //la creare primul si ultimul nod vor fi identici
     }
 else                             //altfel se adauga un nou element la sfarsit
     { c=new nod;          //se aloca un nou nod
       cout<<"informatia utila :";
       cin>>c->info;        //se completeaza campul informatie utila
       u->next=c;            //se adauga dupa ultimul nod
       u=c;                      //se stabileste noul nod c ca fiind ultimul
       }
u->next=0;                   //campul adresa urmatoare a ultimului nod este 0
     }
 
void afis()  //functia de afisare parcurge elementele cu afisare
{nod *c;
 c=p;                //se porneste de la primul nod din lista
 while(c)           //cat timp c retine o adresa nenula
   {cout<<c->info<<" ";//se afiseza campul informatie utila
    c=c->next;}             //se avanseaza la urmatoarea adresa, la urmatorul nod
cout<<endl;
}
 
void k_liste(nod *c,int n)
{
cout<<"nr de elemente din lista=";
 cin>>n;
 
 for(int i=1;i<=n;i++)
     cre_ad(c);
cout<<endl;
}

void stergere()
{
	nod *c;
while(p!=0)
	{
		c=p;
		p=p->next;
		delete c;
	}
}

int left(int i)
{
	return 2*i;
}
int right(int i)
{
	return 2*i+1;
}
int parent(int i)
{
	return i/2;
}

void HEAPIFY(Heap_Elt heap[], int i)
{
	int l, r, largest;
	Heap_Elt aux;
	l = left(i);
	r = right(i);
//profiler.countOperation("heapsort",length);
	if(l <= heap_size && heap[l].elem > heap[i].elem)
		largest = l;
	else 
		largest = i;
//profiler.countOperation("heapsort",length);
	if(r <= heap_size && heap[r].elem > heap[largest].elem)
		largest = r;
	if(largest != i)
	{
	//	profiler.countOperation("heapsort",length,3);
		aux = heap[i];
		heap[i] = heap[largest];
		heap[largest] = aux;
		HEAPIFY(heap, largest);
	}
}

void H_pop(Heap_Elt x,int dim=0)
{
   
   x=heap[1];
   heap[1]=heap[dim];
   dim=dim-1;
   HEAPIFY(heap,1);
}

void H_push( Heap_Elt x,int nr,int dim)
{
	int j;
	Heap_Elt aux;
	dim++;
	heap[dim]=x;
	j=dim;
	while (j>1 && heap[j].elem<heap[j/2].elem)
	{
		aux=heap[j];
		heap[j]=heap[j/2];
		heap[j/2]=aux;
		j=j/2;
	}
}

//void interclasare(int k)
//{
//Heap_Elt x;
//dim=0;
//int s=0;
//for(int i=1;i<=k;i++)
//	{
//	/*	x.index=i;
//		x.elem=heap[i].elem;*/
//		H_push(heap[i],i+1,dim);
//	}
////for(int i=1;i<=n;i++)
//	  
//Heap_Elt d;
//while(dim>0)
//	{
//		
//		d=H_pop(d,dim);
//		HEAPIFY();
//		s=d.index;
//		cre_ad((&joinedHead),&joinedTail,d.elem->info);
//	}
//
//
//
//}

void afisare2(Heap_Elt heap, int n)
{
int i;

for(i=1;i<=n;i++)
	cout<<heap.elem<<"  ";
}

void main()
{ 
int b,k;
cout<<"dati nr de liste=";
cin>>k;
Heap_Elt *c;
nod z;
for(int i=1;i<=k;i++) 
{
	k_liste(&z,4);
    afis();
	stergere();
}



//k_liste(&z,4);
//HEAPIFY(&heap[1],1);
//H_push(heap[1 ],1,0);
//afisare2(heap[1],4);
getch();
}
 