﻿/***************************************ANONIM*** ***ANONIM*** ,gr.***GROUP_NUMBER*********************************************************
Avrage case:
 - ambii algoritmi au crestere in timp logarimic
- heapsort - O(n log n) 
- quicksort - O(n log n) - este un algoritm bazat pe comparatii,nu este stabil
-- din grafic rezulta ca quicksort este mai eficient decat heapsort, nr de operatii fiind mai mic

Quicksort-Cazul cel mai favorabil apare cand partitionarile sunt echilibrate , pivotul sa fie ales la mijloc.
Sunt log N partitii, si pentru a obtine fiecare partitie,se fac n comparatii si nu mai mult de n/2 interschimburi 
=> complexitate O(N log N)

Quicksort - WORST CASE - complexitatea este O(n^2)=crestere exponetiala .Se realizeaza pt vectorul deja sortat,functia este
o parabola, se repeta procedeura de n-1 ori din cauza pivotului ales tot la marginea intervalului


*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>
#include "Profiler.h"

Profiler profiler("Lab 4");
using namespace std;
#define NEG_INF INT_MIN

int heap[10001],lng, size_h, x[10001], ar[11],ar1[11];

void maxHeapify(int [],int);
void buildMaxHeapBU(int []);
void heapsort(int []);
int Partition(int *a,int p,int r);
void afisare(int *v,int n,int k,int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
	for(int i=0;i<=nivel;i++)
	{
		printf("   ");
	}
	printf("%d\n",v[k]);
	afisare(v,n,2*k,nivel+1);
}
void display(int v[])
{
	for(int i =1; i <= lng; i++)
		printf("%d ",v[i]);
}
int left(int i)	 //returneaza indicele fiului din stanga
{
	return 2*i;
}
int right(int i)  //returneaza indicele fiului din dreapta
{
	return 2*i+1; 
}
int parent(int i)  //returneaza indicele parintelui
{
	return i/2;
}

void heapSort(int heap [])
{
	int  aux;
	buildMaxHeapBU(heap);
	for(int k=lng;k>=2;k--)
	{
		aux=heap[1];
		heap[1]=heap[k];
		heap[k]=aux;	
		size_h--;
		maxHeapify(heap,1);
	}
}
void buildMaxHeapBU(int heap[])			//construiere heap prin metoda buttom-up
{
	size_h = lng;
	for(int i = lng/2; i >= 1; i--)
		maxHeapify(heap, i);
}
void maxHeapify(int heap[], int i)//pentru metoda bottom-up
{
	int l, r, largest, aux;
	l = left(i);
	r = right(i);
	profiler.countOperation("heap",lng);
	if(l <= size_h && heap[l] > heap[i])	
		largest = l;
	else 
		largest = i;
	profiler.countOperation("heap",lng);
	if(r <= size_h && heap[r] > heap[largest])	
		largest = r;
	if(largest != i)
	{
		profiler.countOperation("heap",lng,3);
		aux = heap[i];
		heap[i] = heap [largest];
		heap[largest] = aux;
		maxHeapify(heap, largest);
	}
}
void quickSort(int *a1,int p,int r)
{int q=0;
if(p < r)
{	
	q=Partition(a1,p,r);
	quickSort(a1,p,q-1);
	quickSort(a1,q+1,r);
}
}
int Partition(int *a,int p,int r)
{
	int x=a[r];		profiler.countOperation("quick",lng);
	int i=p-1,aux;
	for (int j=p;j<=r-1;j++)
	{	profiler.countOperation("quick",lng);
	if (a[j]<=x)	
	{	i++;
	aux=a[i];
	a[i]=a[j];
	a[j]=aux;	profiler.countOperation("quick",lng,3);
	}
	}
	profiler.countOperation("quick",lng,3);
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;
	return i+1;
}

//Pentru cazul mediu statistic se repeta măsurătorile de 5 ori si se raporteaza valoarea lor medie
void statistic()
{
	for(int i=1; i <= 5; i++)
		for(lng = 100; lng <= 10000; lng += 100)
		{
			FillRandomArray(heap,lng+1);
			memcpy(x,heap,(lng+1)*sizeof(int));		//pastrez o copie la heap
			heapSort(x);			//sortez heapul cu metoda top-down
			quickSort(heap,1,lng);
		}
		profiler.createGroup("Heap and Quick Sort","heap","quick");
}
void worst() //cand vectorul este sortat descrescator
{int ar[]={0,7,54,2,42,76,654,34,34,3,9};
quickSort(ar,1,10);
afisare(ar,10,1,0);
display(ar);
for(lng = 100; lng <= 5000; lng += 100)
{
	FillRandomArray(x,lng+1,1,100000,true,1);
	quickSort(x,1,lng);
}
profiler.createGroup("Worst Case for Quick Sort","quick");
}
void quickSort_best(int *a,int l,int r)
{	//partitionarea
	int aux,m=(l+r)/2; //m=index pentru mijlocul intervalului
	int x=a[m];  //pivot
		profiler.countOperation("quick",lng);
	int i=l,j=r;
	while (i<=j)
	{
		while(a[i]<x)
		{i++; profiler.countOperation("quick",lng); }
		while(a[j]>x)
		{	j--; profiler.countOperation("quick",lng);}

		if (i<=j)	
		{	//interschimbare
			aux=a[j];
			a[j]=a[i];
			a[i]=aux;	profiler.countOperation("quick",lng,3);
			i++;j--;
		}
	}
	//apel recursiv
	if(l < j) quickSort_best(a,l,j);  
	if(i < r) quickSort_best(a,i,r);
}

void best()
{
	int ar[]={0,7,54,2,42,76,654,4,34,3,9};
	for(int i =1; i <=10; i++)
		printf("%d ",ar[i]);
	quickSort_best(ar,1,10);

	afisare(ar,10,1,0);
	for(int i =1; i <=10; i++)
		printf("%d ",ar[i]);

	for(lng = 100; lng <= 1000; lng += 100)
	{
	FillRandomArray(x,lng+1);
	quickSort_best(x,1,lng);
	//display(x);
	}
	profiler.createGroup("Best Case for Quick Sort","quick");
}
void test()
{
	lng=10;
	//printf("input %d values\n",lng);
	//for(int i=1; i<= lng; i++)
	//scanf_s("%d",ar+i);
	int ar[]={0,7,54,2,42,76,654,4,34,3,9};
	int ar1[]={0,7,54,2,42,76,654,4,34,3,9};
	display(ar);
	heapSort(ar);
	printf("\n\n\n");
	afisare(ar,lng,1,0);
	display(ar);
	quickSort(ar1,1,lng);
	printf("\n\n\n");
	afisare(ar1,lng,1,0);
	display(ar1);
}
int main()
{
	//test();
	//statistic();
	worst();
	//best();
	profiler.showReport();
	_getch();
	return 0;
}