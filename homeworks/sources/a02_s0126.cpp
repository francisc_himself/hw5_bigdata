/*
-in the average case TD heap build performs better than BU
-the average number of assignments is about the same, but BU generally makes more comparisons
-both heap building methods have linear running time of O(n)
-when reconstructHeap is called the running time depends on how low the current element will
go, the worst case being when the element goes down to the leaf level
-
*/
#include<stdio.h>
#include<conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler;
const int VMAX = 10000;
const int VMIN = 0;

int* generateRandom(int n)
{
  int* a=new int[n];
  srand(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=rand()%(VMAX-VMIN) +VMIN;
  return a;
}

void reconstructHeapBU(int heap[],int i,int n)
{
    int left = 2*i;
    int right = 2*i +1;
    int max;
	profiler.countOperation("BU_comp",n);
    if ( (left <= n) && ( heap[left] > heap[i] ) )
        max = left;
    else
        max = i;
        profiler.countOperation("BU_comp",n);
    if ( (right <= n ) && (heap[right] > heap[max]) )
        max = right;
		//profiler.countOperation("BU_comp",n);
    if ( max != i )
    {
        int aux = heap[i];
        heap[i] = heap[max];
        heap[max] = aux;
        profiler.countOperation("BU_atr",n,3);
        reconstructHeapBU(heap,max,n);
    }
}

void constructHeapBU(int heap[],int n)
{
    for (int i = n/2 ; i>0 ; i--)
        reconstructHeapBU(heap,i,n);
}

void pushHeap(int heap[],int val,int *dim, int n)
{
	int i;
	(*dim)++;
	i=(*dim);
	profiler.countOperation("TD_comp",n);
	while ((i>1) && ( heap[i/2]< val))
	{
		heap[i] = heap[i/2];
		i = i/2;
		profiler.countOperation("TD_atr",n);
		profiler.countOperation("TD_comp",n);
	}
	heap[i] = val;
	profiler.countOperation("TD_atr",n);
}

void constructHeapTD(int a[],int heap[],int n,int *dim)
{
	for (int i=1;i<=n;i++)
	{
		pushHeap(heap,a[i],dim,n);
	}
}

void pretty_print(int T[], int i, int level, int n)
{
    if(i<n)
    {
    pretty_print(T, 2*i+1, level+1,n);
    for(int j=1;j<=level;j++)
    printf("          ");
    printf("%d\n",T[i]);
    pretty_print(T,2*i, level+1,n);
    }
}

void heapSort(int a[],int heap[],int n)
{
    constructHeapBU(heap,n);
    while (n!=0)
    {
        heap[n-1] = heap[0];
        heap[0]=heap[n-1];
        n--;
        reconstructHeapBU(heap,0,n);
    }
}

void main()
{
	//test if it works correctly on a small input data
	/*int heap[9900];
	int *a;
	int n,dim;
	n=16;
	int d = 0;
	a = generateRandom(n);
	for (int i=1;i<=n;i++)
		heap[i]=a[i];
	printf("before construction:\n\n");
	pretty_print(heap,1,0,n);
	printf("\n after construction BU:\n\n");
	constructHeapBU(heap,n);
	pretty_print(heap,1,0,n);
	printf("\n after construction TD:\n\n");
	constructHeapTD(a,heap,n,&d);
	pretty_print(heap,1,0,n);
	getch();
*/
	//average case test
	/*
	int heap[10001];
	int *a;
	for (int j=0;j<5;j++)
		for (int n=100;n<10000;n = n+100)
		{
			int d=0;
			a=generateRandom(n);
			for (int i = 1 ; i <= n ; i++)
			heap[i] = a[i];
			constructHeapBU(heap,n);
			constructHeapTD(a,heap,n,&d);
		}
		profiler.createGroup("comparisons","TD_comp","BU_comp");
		profiler.createGroup("assignments","TD_atr","BU_atr");
		profiler.addSeries("totalTD","TD_comp","TD_atr");
		profiler.addSeries("totalBU","BU_comp","BU_atr");
		profiler.createGroup("total","totalTD","totalBU");
		profiler.showReport();
		*/
	//heapsort test
	int *a = generateRandom(10);
	int heap[11];
	for (int i = 0; i<10; i++)
		printf("%d  ",a[i]);
	for (int i=0;i<10;i++)
		heap[i]=a[i];
	printf("\n");
	constructHeapBU(heap,10);
	//pretty_print(heap,1,0,10);
	for (int i = 0; i<10; i++)
		printf("%d  ",a[i]);
	//heapSort(a,heap,10);
	printf("\n");
	for (int i = 0; i<10; i++)
		printf("%d  ",heap[i]);
	getch();

}