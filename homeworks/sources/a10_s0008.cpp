﻿/* Tema 10
***ANONIM***ș ***ANONIM***
grupa ***GROUP_NUMBER***

******** CĂUTAREA ÎN ADÂNCIME **********
*/
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<string.h>

#define NMAX 201

typedef struct vecin
{
	int val;
	char muchie[15];
	struct vecin* urm;
}Tvecin;

typedef struct nod
{
	int val;
	int culoare;
	int d; //=timp descoperire
	int f; //=timp acoperire
	int prec; //=predecesor
	Tvecin *prim;
	Tvecin *ultim;
}Tnod;

/** zona de decrarari globale ***/
Tnod *G[NMAX];
int n, m, k=0;
int S[NMAX]; //vector ce va contine sortarea topologica a nodurilor
int timp = 0;
int aciclic = 1; //presupun ca nu are cicluri
int op=0;

/**functie de initiazizare a grafului**/
void G_init()
{
	int x;

	for(x=1; x<=n; x++)
	{
		/*****/op++;
		G[x] = (Tnod *)malloc(sizeof(Tnod));
		G[x] -> val = x;
		G[x] -> culoare = 0;
		G[x] -> d = 0;
		G[x] -> f = 0;
		G[x] -> prec = NULL;
		G[x] -> prim = G[x]->ultim = NULL;
	}
}

void adauga(int x, int y)
{	
	Tvecin *p;
	p = (Tvecin *)malloc(sizeof(Tvecin));
	p -> val = y;
	p -> urm = NULL;

	//adaug primul vecin
	if(G[x]->prim == NULL)
	{
		G[x] -> prim = p;
		G[x] -> ultim = p;
	}
	else	//adaug la sfarsitul listei
	{
		G[x] -> ultim-> urm = p;
		G[x] -> ultim = p;
	}

}

void afiseaza_lista()
{
	int i;
	Tvecin *p;

	printf("LISTA DE ADIACENTA: ");
	for(i=1; i<=n; i++)
	{
		printf("\n%d : ", i);
		if(G[i] -> prim == NULL)
			printf(" - ");
		else{
			p = G[i]->prim;
			while(p != NULL)
			{
				printf("%d ",p->val);
				p = p->urm;
			}
		}
	}
}

/** Fuctie de citire informatii de la tastatura **/
void citeste()
{
	int i, x, y;

	
	printf("****** PARCURGERE GRAFURI: CAUTARE IN LATIME ********\n");
	printf("nr. noduri = "); scanf("%d", &n);
	printf("nr. muchii = "); scanf("%d", &m);
	G_init();
	printf("introduceti muchiile:\n");
	for(i=1;i<=m;i++)
	{
		scanf("%d %d", &x, &y);
		adauga(x, y);
	}

	afiseaza_lista();
}

void DFS_VISIT(int u)
{
	G[u] -> culoare = 1; //fac nodul gri
	G[u] -> d = ++timp;
	/*****/op+=2;
	Tvecin *v;
	for(v=G[u]->prim; v!=NULL; v = v->urm)
	{
		/*****/op++;
		if(G[v->val]->culoare == 0)
		{
			G[v->val]->prec = u;
			strcpy(v->muchie, "ARBORE");
			DFS_VISIT(v->val);
		}
		else if(G[v->val]->culoare == 1)
			{
			  strcpy(v->muchie, "INAPOI");
			  aciclic = 0;
			}
		else if(G[v->val]->f < G[u]->d)
			strcpy(v->muchie, "TRANSVERSALA");
		else strcpy(v->muchie, "INAINTE");
	}
	G[u]->culoare = 2; //fac nodul negru
	G[u]->f=++timp;
	/*****/op+=2;
	S[++k]=u;
}

void DFS()
{
	int i;
	timp = 0;

	for(i=1; i<=n; i++)
	{	/*****/op++;
		if(G[i]->culoare == 0)
			DFS_VISIT(i);	
	}
}

void afisare_muchii()
{
	int i;

	printf("\n**CLASIFICAREA MUCHIILOR**\n");
	for(i=1; i<=n; i++)
	 {
		 if(G[i]->prim == NULL) continue;
		 
		 Tvecin *v;
		 for(v=G[i]->prim; v!=NULL; v=v->urm)
			 printf("(%d, %d)=%s\n", i, v->val, v->muchie);
	}
}

void sortare_topologica()
{
	if(aciclic==0) printf("Graful nu poate fi sortat!Contine cicluri!!\n");
	else{
	DFS();
	afisare_muchii();
	int i;
	printf("** SORTAREA TOPOLOGICA A NODURILOR **\n");
	for(i=n; i>=1; i--)
		printf("%d ", S[i]);
	}
}

void demo()
{
	citeste();
	sortare_topologica();
}

void generare()
{
	int x, y, j;
	FILE *pf;

	
	/**** VARIAZA NUMARUL DE MUCHII ****/
	pf = fopen("n_fix.csv", "w");
	fprintf(pf, "MUCHII, OPERATII\n");

	n=100;
	for(m=1000; m<=5000; m+=100)
	{
		op=0;
		G_init();
		for(j=1; j<=m; j++)
		{
			x = rand()%n+1;
			do{
				y = rand()%n+1;
			}while(x==y);
			adauga(x, y);
		}
		DFS();
		fprintf(pf, "%d, %d\n", m, op);
	}
	fclose(pf);

	/**** VARIAZA NUMARUL DE MUCHII ****/
	pf = fopen("m_fix.csv", "w");
	fprintf(pf, "NODURI, OPERATII\n");
	
	m=9000;
	for(n=110; n<=200; n+=10)
	{
		op=0;
		G_init();
		for(j=1; j<=m; j++)
		{
			x = rand()%n+1;
			do{
				y = rand()%n+1;
			}while(x==y);
			adauga(x, y);
		}
		adauga(x, y);
		DFS();
		fprintf(pf, "%d, %d\n", n, op);
	}
	fclose(pf);

	printf("Gata!");
}

int main()
{
	//demo();
	generare();
	getch();
	return 0;
}