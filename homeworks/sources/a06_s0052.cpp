/* ***ANONIM*** ***ANONIM*** ***ANONIM*** gr.***GROUP_NUMBER***
	Algoritmul urmator realizeaza efectuarea propriu-zisa a permutarii Josephus.
	Pentru a implementa acest algoritm am ales sa utilizez un arbore binar de cautare, a carui constructie are eficienta O(n).
	Pentru a sterge un element din arbore am utilizat mai intai operatia de OS_select care cauta nodul de sters intr-un timp 
	O(lgn),si mai apoi OS_delete cu eficienta O(h),do***ANONIM***anta OS_select. Ambele se utilizeaza intr-un while de n ori si astfel 
	eficienta algoritmului permutarii Josephus este O(nlgn).
*/


#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("demo"); 

typedef struct _NOD{
	int val;
	int dim;
	struct _NOD *st,*dr,*pr;
}NOD;

NOD *T;
int lung;


NOD* constructie_arbore_echilibrat(int st,int dr)
{
	NOD *x;
	x=(NOD*)malloc(sizeof(NOD));
	NOD *r,*t;
	int m,ds,dd;
	if(st<=dr){
		m=(st+dr)/2; 
		x->val=m;
		x->pr=0;
		x->st=constructie_arbore_echilibrat(st,m-1);
		r=x->st;
		if(r!=NULL){
			r->pr=x;
			ds=r->dim;
		}
		else ds=0;
		x->dr=constructie_arbore_echilibrat(m+1,dr);
		t=x->dr;
		if(t!=NULL){
			dd=t->dim;
			t->pr=x;
		}
		else dd=0;
		x->dim=ds+dd+1;
		x->pr=0;
		return x;
	}else {
		return NULL;
		}
}

NOD* OS_Select(NOD* x, int i)
{
	int r;
	if(x->st!=NULL)
		r=x->st->dim+1;
	else {r=1;
	}profiler.countOperation("comp",lung,1);
	if(i==r){ return x;}
	else {if(i<r) {return OS_Select(x->st,i);}
	else {
		return OS_Select(x->dr,i-r);}
	profiler.countOperation("comp",lung,1);
		  }
		profiler.countOperation("comp",lung,1);

	}


NOD* Tree_***ANONIM***(NOD *x)
{
	while(x->st!=NULL){
		x->dim--;
		x=x->st;
		profiler.countOperation("delete",lung,1);
		profiler.countOperation("comp",lung,1);
		profiler.countOperation("assign",lung,1);
	}
	return x;
}
void Transplant(NOD *u,NOD *v)
{
	if(u->pr==NULL){
		T=v;
		profiler.countOperation("assign",lung,1);
		profiler.countOperation("delete",lung,1);
	}
	else if(u==u->pr->st)
	{
		u->pr->st=v;
		profiler.countOperation("assign",lung,1);
		profiler.countOperation("delete",lung,1);
	}
	else {
		u->pr->dr=v;
		profiler.countOperation("assign",lung,1);
	profiler.countOperation("delete",lung,1);
	}
	profiler.countOperation("comp",lung,2);profiler.countOperation("delete",lung,2);
	if (v!=NULL){
		v->pr=u->pr;
		v->dim=u->dim-1;
		profiler.countOperation("assign",lung,1);profiler.countOperation("delete",lung,1);
	}
	profiler.countOperation("comp",lung,1);profiler.countOperation("delete",lung,1);
}
void OS_Delete(NOD *x)
{
	NOD *y,*z;
	z=x;
	while(x->pr!=NULL)
	{
		profiler.countOperation("assign",lung,1);
		profiler.countOperation("comp",lung,1);
		x=x->pr;
		x->dim--;
		profiler.countOperation("assign",lung,1);
		profiler.countOperation("delete",lung,3);

	}
	x=z;
	 if(x->st==NULL)
		 Transplant(x,x->dr);
	 else if(x->dr==NULL)
		Transplant(x,x->st);
	 else {y=Tree_***ANONIM***(x->dr);
	 profiler.countOperation("assign",lung,1);
	 profiler.countOperation("delete",lung,1);
		   if(y->pr!=x){
			   Transplant(y,y->dr);
			   y->dr=x->dr;
			   y->dr->pr=y;
			   profiler.countOperation("assign",lung,2);
				profiler.countOperation("delete",lung,1);
		   }
		   profiler.countOperation("comp",lung,1);
		   profiler.countOperation("delete",lung,1);
		   Transplant(x,y);
		   y->st=x->st;
		   y->st->pr=y;
		   profiler.countOperation("assign",lung,2);
	 profiler.countOperation("delete",lung,1);
	 }
	free(x);
	profiler.countOperation("comp",lung,2);
profiler.countOperation("delete",lung,1);
}
 


void afisare(NOD *x,int n)
{
	if(x!=0){
		afisare(x->dr,n+1);
	for(int i=1;i<=n;i++)
	{
		printf("   ");
	}
	
	if(x->pr!=NULL)
			printf("%d,%d,p{%d}\n",x->val,x->dim,x->pr->val);
		else
			printf("%d,%d,rad\n",x->val,x->dim);
		afisare(x->st,n+1);
	}
}
void Josephus(int n, int m)
{
	NOD *x;
	x=(NOD*)malloc(sizeof(NOD));
	x=constructie_arbore_echilibrat(1,n);
	
	x->pr=NULL;
	T=x;
	afisare(T,0);
	printf("\n");
	profiler.countOperation("assign",lung,1);
	int i=m;
	while(T!=NULL&&n>0)
	{
		profiler.countOperation("comp",lung,1);
		x=OS_Select(T,i);
		profiler.countOperation("assign",lung,1);
		OS_Delete(x);
		afisare(T,0);
		printf("\n");
		
		n=n-1;
		if(n!=0)
			{i=(i+m-1)%n;}
		else
		{
			printf("gata\n");
		}
		if(i==0){i=n;}
		
	}
}

void main()
{
	Josephus(7,3);
	/*for(int n=100;n<=10000;n+=100)
		{
			int m=n/2;
			lung=n;
			Josephus(n,m);
		}
	profiler.addSeries("Seria","assign","comp");
	profiler.createGroup("SERIA","Seria");
	profiler.showReport();*/
	//int v[]={0,1,2,3,4,5,6,7,8};
	//int n,nivel;
	/*int n=30;
	T=constructie_arbore_echilibrat(1,n);*/
	////nivel=
	/*afisare(T,0);*/
	//NOD *r;
	//r=T->dr;
	/*printf("\n");
	OS_Delete(T);
	afisare(T,0);*/
}