/*
***ANONIM*** Vasile-***ANONIM***, grupa ***GROUP_NUMBER***
Algoritmul de parcurgere in latime al grafului porneste de la nodul sursa si ia primul nod adiacent acestuia.
Algoritmul de parcurgere in latime al grafului ruleaaza recursiv de la nodul sursa pe primul nod adiacent lui.
*/

#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdio.h>

#define NMAX 10001

int cul[NMAX];

typedef struct nod
{
	int value;
	struct nod *next;
} NOD;



typedef struct graf
{
	NOD *noduri[100];
	int n;
	int muchii;
}gf;

// creare lista vida 
void CreareVida(NOD** head, int i)
{
	*head = (NOD*)malloc(sizeof(NOD));
	(*head)->value=i;
	(*head)->next=NULL;
}

int scoateCap(NOD** head, NOD** tail)
{
	int value=(*head)->value;
	if((*head)->next!=NULL)
		*head=(*head)->next;
	else{
		*head=NULL;
		*tail=NULL;
	}
	return value;
}

void InserareNodSfarsit(NOD** head, NOD** tail, NOD *p)
{
	if (*head == NULL) 
		// doar modificam capul si coada listei 
	{
		*head=p;
		*tail=p;
	}
	// adaugam elementul nou la sfarsitul listei 

	else
	{	
		(*tail)->next=p;
		*tail=p;
	}
}

void InserareSfarsit(NOD** head, NOD** tail, int val) 
{ // Alocare si initializare nod 
	//printf("sunt in inserez in lista val");
	NOD *p=(NOD*)malloc(sizeof(NOD));
	p->value = val; 
	p->next = NULL; 
	// daca avem lista vida 
	if (*head == NULL) 
		// doar modificam capul si coada listei 
	{
		*head=p;
		*tail=p;
	}
	// adaugam elementul nou la sfarsitul listei 

	else
	{	
		(*tail)->next=p;
		*tail=p;
	}
}


void bfs(gf g, int s)
{
	NOD *headq,*tailq;
	headq=NULL;
	tailq=NULL;
	for(int i=0;i<g.n;i++)
		cul[i]=0;//alb
	cul[s]=2;//gri
	InserareSfarsit(&headq,&tailq,s);
	while(headq!=NULL)
	{
		int u = scoateCap(&headq,&tailq);
		printf("%d\n",u);
		NOD *p = g.noduri[u]->next;
		while(p!=NULL)
		{
			if(cul[p->value]==0)
			{
				cul[p->value]=2;
				InserareSfarsit(&headq,&tailq,p->value);
			}
			p=p->next;
		}
	}

}

void dfs_visit(gf g, int i)
{
	printf("%d\n",i);
	cul[i]=2;//gri
	NOD *p = g.noduri[i]->next;
	while(p!=NULL)
	{
		if(cul[p->value]==0)
			dfs_visit(g,p->value);
		p=p->next;
	}
	cul[i]=1;//negru
}

void dfs(gf g)
{
	for(int i=0;i<g.n;i++)
		cul[i]=0;
	for(int i=0;i<g.n;i++)
		if(cul[i]==0)
			dfs_visit(g,i);

}

int a[10000][10000];

int main()
{
	gf g;
	int i,a,b,m,n;

	printf("Dati nr de noduri: ");
	scanf("%d",&n);
	g.n=n;
	printf("Dati nr de muchii: ");
	scanf("%d",&m);
	g.muchii=m;

	for(i=0;i<n;i++)
		CreareVida(&g.noduri[i],i);


	for(i=0;i<m;i++)
	{
		scanf("%d %d", &a,&b);
		NOD *q=(NOD*)malloc(sizeof(NOD));
		q->value=b;
		q->next=g.noduri[a]->next;
		g.noduri[a]->next=q;
	}

	//bfs(g,0);
	dfs(g);

	system("pause");
}