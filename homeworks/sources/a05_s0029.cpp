#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#define N 10007


int t[N]={-1};
int ok[N/2]={0};
int max=-1000;
int efort=0,suma;
int nr_operatii;

int h(int key, int i)
{
	int c1=1,c2=1;
	int p=(key  % N + c1 *i +c2*i*i) % N;
	return p;
}

int adaugare(int *t, int key)
{
	int i,j;
    i=0;
    do
	{
    j=h(key,i);
    if (t[j]==-1)
    {
         t[j]=key;
         nr_operatii++;
        return j;
    }
    else i=i+1;
	}
    while(i < N);
	return -1;
}

void cautare(int *t, int key)
{
	efort=0;
	int i,j;
    i=0;
	do
	{
        j=h(key,i);
        efort=efort+1;
        if (t[j]==key) break;
        i=i+1;
	}
    while ((t[j]!=-1)&&(i!=N));

	suma =suma + efort;

	if (efort > max) max=efort;
}


void testare(double alfa)
{
	int x;
	int indice=0;

	int nmax;
	int nmed=0;
	int smed=0;
	int smax=0;
	suma=0;
	max=0;
	int r,j;
	for( r=0;r<=N;r++)
		t[r]=-1;

	for (x=0; x<N/2; x++)
		ok[x]=0;
	nr_operatii = 0;
	for (j=0; j < alfa * N ; j++)
	{

		x=rand() * rand() % 100000;
		adaugare(t,x);
		if (nr_operatii % 6 == 0)
			ok[indice++] = x;
	}

	int g;
	for (g = 0; g < indice; g++)
		cautare(t,ok[g]);
	smed=suma/indice;
	smax=max;

	suma=0;
	max=0;
	//negasite
	int p;
	for ( p=0; p < 1500 ; p++)
	{
		int w=rand() * rand() % 100000 + 100000;
		cautare(t,w);
	}
	nmed=suma/1500;
    nmax=max;

	printf("\n %0.2f\t\t%d\t\t%d\t\t%d\t\t%d",alfa, smed, smax, nmed,nmax);
}
void demo()
{

    int tabel[N];
    int j;
    for(j=0;j<N;j++) tabel[j]=-1;
    adaugare(tabel,10);
    adaugare(tabel,8);
    adaugare(tabel,2);
    adaugare(tabel,8);
    adaugare(tabel,8);
    int i;
    for(i=0;i<=10;i++)
    {
        printf("%d ",tabel[i]);
    }
}
int main()
{
    demo();
	printf("\n Fact.umplere|\tgasiteEFMED|\tgasiteEFMAX|\tnegasiteEfMED|\tnegasitEFMAX|");
	srand(time(NULL));
	testare(0.8);
	testare(0.85);
	testare(0.9);
	testare(0.95);
	testare(0.99);
	getch();
	return 0;
}


