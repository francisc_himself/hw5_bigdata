// Tema1-Sortari.cpp : Defines the entry point for the console application.
// ***ANONIM*** ***ANONIM***, Grupa: ***GROUP_NUMBER***

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <cstdlib>

#define MAX_SIZE 10000

    int mc=5; 
	int h;
	// 3 vectori care vor avea aceleasi valori, fiind apelati de cele 3 functii de sortare
    int v[MAX_SIZE];
	int w[MAX_SIZE];
	int w1[MAX_SIZE];
	int aa1=0; int aa2=0; int aa3=0; int cc1=0; int cc2=0; int cc3=0;

int a1,a2,a3,c1,c2,c3; //atribuiri si comparatii pentru toate cele 3 cazuri: favorabil, mediu si defavorabil

//metoda bulelor

void Bubble_sort(int a[], int m)
{
     int ok, i,j,aux;
	 ok=0;
	 while (ok==0)
	 {
		 ok=1;
		 for (j=1; j<m; j++)
		 {   c1++; 
			 if (a[j]>a[j+1])
			 {
				 ok=0;
				 a1++;
				 aux=a[j];
				 a[j]=a[j+1];
				 a[j+1]=aux;


			 }
		 }
	 }

}



//Sortarea prin selectie

void Selectie(int a[], int m)
{
	int i, j,aux,imin;
	for (i=1; i<m; i++)
	{
		imin=i;
		for (j=i+1; j<=m; j++)
		{   c2++; 
			if (a[imin]>a[j])
				imin=j;
		}
			a2++;
		    aux=a[i];
			a[i]=a[imin];
			a[imin]=aux;

		}


	}

  //Sortarea prin insertie

  void Insertie(int a[], int m)
  {
      int i,j,x,aux,k;
	  for (i=2; i<=m; i++)
	  {
		  x=a[i];
		  j=1;
		  c3++;
		  while (a[j]<x)
			 {
				 j++;
				 c3++;
		     }
		  for (k=i; k>=j+1; k--)
			 { a[k]=a[k-1];
		       a3++;
		     }
		  a[j]=x;
	  }


  }
  





void main()
{
	int p,i,q,j,r;
	printf("Hello world!");

	
	
	FILE *fp;
	//deschiderea fisierului .csv
	fp=fopen("***ANONIM***.csv","w");
	//scrierea headerelor in fisier
    fprintf(fp, "n, bubble_a1_bs, bubble_c1_bs, bubble_a1+c1_bs, selectie_a2_bs, selectie_c2_bs, selectie_a2+b2_bs, insertie_a3_bs, insertie_c3_bs, insertie_a3+c3_bs, bubble_a1_av, bubble_c1_av, bubble_a1+c1_av, selectie_a2_av, selectie_c2_av, selectie_a2+b2_av, insertie_a3_av, insertie_c3_av, insertie_a3+c3_av, bubble_a1_wc, bubble_c1_wc, bubble_a1+c1_wc, selectie_a2_wc, selectie_c2_wc, selectie_a2+b2_wc, insertie_a3_wc, insertie_c3_wc, insertie_a3+c3_wc\n");


	//masor pentru fiecare caz (best, average, worst) atribuirile si comparatiile
	h=100;
	while (h<=10000)
	{ 
		
		fprintf(fp, "%d,", h);
		for (p=1; p<=h; p++)
			v[p]=p;

		//Apelarea celor 3 metode in cazul favorabil
		Bubble_sort(v,h);
		fprintf(fp,"%d, %d, %d,",a1,c1,a1+c1);
		Selectie(v,h);
		fprintf(fp,"%d, %d, %d,",a2,c2,a2+c2);
		Insertie(v,h);
		fprintf(fp,"%d, %d, %d,",a3,c3,a3+c3);
		//Apelarea celor 3 metode in cazul mediu (se apeleaza de 5 ori)
		for (j=1; j<=mc; j++)
		{	for (q=1; q<=h; q++)
			{
			    v[q]=rand()%10000;
				w[q]=v[q];
				w1[q]=v[q];
			}
		
			Bubble_sort(v,h);
			aa1=aa1+a1;
			cc1=cc1+c1;
			Selectie(w,h);
			aa2=aa2+a2;
			cc2=cc2+c2;
			Insertie(w1,h);
            aa3=aa3+a3;
			cc3=cc3+c3;
    }
		fprintf(fp,"%d, %d, %d, %d, %d, %d, %d, %d, %d,",aa1/5, cc1/5, (aa1+cc1)/5, aa2/5, cc2/5, (aa2+cc2)/5, aa3/5, cc3/5, (aa3+cc3)/5);
		//cazul defavorabil, unde sirul este ordonat descrescator
		
		j=1;
		for (r=h; r>=0; r--)
		{
			v[j]=r;
			w[j]=r;
			w1[j]=r;
			j++;
         }
		Bubble_sort(v,h);
		fprintf(fp,"%d, %d, %d,", a1, c1, a1+c1);
		Selectie(w,h);
		fprintf(fp,"%d, %d, %d,", a2, c2, a2+c2);
		Insertie(w1,h);
		fprintf(fp,"%d, %d, %d", a3, c3, a3+c3);
	
     h=h+100;
	 fprintf(fp,"\n");
	}

	fclose(fp);


	//testarea celor 3 functii de sortare
	/*printf("\nDati n: ");
	scanf("%d",&n);
	printf("\nDati numerele: ");
	for (i=1; i<=n; i++)
	{
		scanf("%d",&p);
		v[i]=p;
	}

	printf("\nNoul sir format este: ");
	for (i=1; i<=n; i++)
	{
		printf("%d ",v[i]);
	}
	 printf("\nSirul dupa sortare:");
    //testare bubble sort
	Insertie(v,n);
	for (i=1; i<=n; i++)
	{
		printf("%d ",v[i]);
	}
	*/
	
	 getch();

}