//Name: ***ANONIM*** ***ANONIM***
//Group: ***GROUP_NUMBER***
//Date: 29 march 2013
//laboratorul 5 interclasarea a k liste
/*
Interclasarea a n liste foloseste atat structuri de date de liste dinamice simplu inlantuite, ***ANONIM*** si o structura de heap care ajuta la selectia 
elementului minim dintre cele k liste. Astfel, complexitatea algoritmului necesar scade la O(n*log(k)). n vine de la parcurgerea tuturor elementelor
iar k vine de la durata manipularii structurii de heap

se observa ca pentru n fix si k variabil, functia generata de numararea operatiilor critice este una algoritmica, deci la un moment dat numarul de 
cozi nu mai influenteaza atat de mult rezultatul(vorbim de diferente mici intre numarul de cozi)

pentru numarul de liste fix, se observa comportarea liniara a n = numarul de elemente
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//dimensiunea maxima a numarului de noduri
#define MAX_N 10000
//dimensiunea minima a numarului de noduri
#define MIN_N 100
//dimensiunea maxima a numarului de liste
#define MAX_LIST 500
//dimensiunea minima a numarului de liste
#define MIN_LIST 10
//incrementul nodurilor
#define INC_N 100
//incrementul listelor
#define INC_L 10

typedef struct _NOD
{
    int key;
    int noList;
    struct _NOD *next;
}NOD;


//functie ce genereaza un vector de n elemente aleator
void creareLista(NOD **list, int n, int number){
	int i;
	NOD *newn, *curent;
    curent = (NOD*)malloc(sizeof(NOD));
	(*list) = curent;
	curent->key = 0;
	curent->noList = number;
	curent->next = NULL;

	for (i=1; i<=n; i++){
		newn = (NOD*)malloc(sizeof(NOD));
		newn->key = curent->key + 1 + rand() % 10;
		newn->noList = number;
		newn->next = NULL;
		curent->next = newn;
		curent = newn;
	}

}

void afisareLista(NOD *list){
    NOD *curent;
    curent = list->next;
    printf("Lista %d :",list->noList);
    while(curent!=NULL){
        printf("%d ",curent->key);
        curent = curent->next;
    }
    printf("\n");
}

void extrageElement(NOD *list, NOD **extras){
    if(list->next == NULL){
        (*extras) = NULL;
    }
    else{
        (*extras) = list->next;
        list->next = list->next->next;
    }
}

void stergeLista(NOD **list){
    NOD *curent;
    curent = (*list)->next;
    while(curent!=NULL){
        (*list)->next = curent->next;
        free(curent);
        curent = (*list)->next;
    }
    free(*list);
}

void adaugareElement(NOD *list, int xElem,int xInd){
    NOD *curent, *newn;
    curent = list;
    while(curent->next!=NULL){
        curent = curent->next;
    }
    newn = (NOD*)malloc(sizeof(NOD));
    newn->key = xElem;
    newn->noList = xInd;
    newn->next = NULL;
    curent->next = newn;
}

// parintele lui i
int p(int i){
	return i/2;
}

// fiul din stanga al lui i
int st(int i){
	return i*2;
}

// fiul din dreapta al lui i
int dr(int i){
	return i*2+1;
}

// reconstruieste structura de heap pe ramura din arbore ce are ca radacina nodul i
// la fiecare pas determina minimul dintre radacina, fiu stanga, fiu dreapta, si il pune la radacina, urmand sa apeleze
// functia mai departe, pe fiul cu care s-a schimbat radacina
void reconstituieHeap(int vectElem[], int vectInd[], int i, int dim, double *operatii){
	int l, r, min, aux;

	l = st(i);
	r = dr(i);

    // testeaza fiul din stanga
	if(l <= dim && vectElem[l] < vectElem[i])
	{
		min = l;
	}
	else
	{
		min = i;
	}

    // testeaza fiul din dreapta
	if( r <= dim && vectElem[r] < vectElem[min])
	{
		min = r;
	}
	*operatii += 2;

    // daca minimul nu e pe radacina
	if(min != i)
	{
		aux = vectElem[i];
		vectElem[i] = vectElem[min];
		vectElem[min] = aux;

		aux = vectInd[i];
		vectInd[i] = vectInd[min];
		vectInd[min] = aux;

		*operatii += 3;

		reconstituieHeap(vectElem, vectInd, min, dim, operatii);
	}
}

// initializeaza vectorul de heap
void Hinit(int *dim){
    (*dim) = 0;
}

// adauga un element la structura de heap
// il punem ultimul in lista si il mutam in sus pana i gasim pozitia potrivita
void Hpush(int vectElem[], int vectInd[], int *dim, int xElem, int xInd, double *operatii){
    int i;
    *dim = *dim + 1;
    vectElem[*dim] = xElem;
    vectInd[*dim] = xInd;
    *operatii = *operatii + 1;
    i = *dim;
    while((i>1)&&(xElem<vectElem[p(i)])){
            vectElem[i] = vectElem[p(i)];
            vectInd[i] = vectInd[p(i)];
            *operatii = *operatii + 2;
            i = p(i);
    }
    vectElem[i] = xElem;
    vectInd[i] = xInd;
    *operatii = *operatii + 1;
}

// scoatem nodul radacina, adica minimul, punem ultimul element din vector pe prima pozitie, si reconstruim heapul
void Hpop(int vectElem[], int vectInd[], int *xElem, int *xInd, int *dim, double *operatii){

    if (*dim<=0){
        *xElem = -1;
        *xInd = -1;
    }
    else{
    *xElem = vectElem[1];
    *xInd = vectInd[1];
    *operatii = *operatii + 1;
    vectElem[1] = vectElem[*dim];
    vectInd[1] = vectInd[*dim];
    *operatii = *operatii + 1;
    *dim = *dim - 1;
    reconstituieHeap(vectElem,vectInd,1,*dim,operatii);
    }
}

// pentru fiecare nod care nu e frunza, recostruim heapul
void construiesteHeapBU(int vectElem[], int vectInd[], int dim, double *operatii){
	int i;
	*operatii = 0;
	for(i = p(dim); i>=1; i--){
		reconstituieHeap(vectElem,vectInd,i,dim,operatii);
	}
}

// afiseaza heapul intr-o structura user-friendly
void afisareHeap(int vectElem[], int vectInd[], int dim, int i, int depth){
	int level;
	if(i>dim){
	return;
	}
	afisareHeap(vectElem,vectInd,dim,2*i+1,depth+1);
	for(level=0; level<depth; level++)
		printf("    ");
	printf("%d:%d\n",vectElem[i],vectInd[i]);
	afisareHeap(vectElem,vectInd,dim,2*i,depth+1);
}

void interclasare(NOD *list[], NOD *listconc, int diml, double *operatii) {

    NOD *curent;
    int vectorElem[MAX_LIST+1];
    int vectorInd[MAX_LIST+1];
    int xElem, xInd;
    int dim,i;

    dim = 0;
    for (i = 1; i <= diml; i++){
        extrageElement(list[i],&curent);
        *operatii += 1;
        Hpush(vectorElem,vectorInd,&dim,curent->key,curent->noList,operatii);
        free(curent);
    }
    //printf("\nHeap creat\n");
    //afisareHeap(vectorElem,vectorInd,dim,1,1);

    while((dim>0)){
        Hpop(vectorElem,vectorInd,&xElem,&xInd,&dim,operatii);
        adaugareElement(listconc,xElem,xInd);
        *operatii += 1;
        extrageElement(list[xInd],&curent);
        *operatii += 1;
        if (curent !=NULL){
            Hpush(vectorElem,vectorInd,&dim,curent->key,curent->noList,operatii);
        }
        //printf("dim %d xElem %d\n",dim,xElem);
    }
}

int main(){

	int n = 10, k1, i;
	int k[3]={5,10,100};

    NOD *list[MAX_LIST+1];
    NOD *listconc;



	double operatii;

	FILE *listfile,*listfile1;
	listfile = fopen("interclasarek.csv","w");
	listfile1 = fopen("interclasaren.csv","w");
	fprintf(listfile,"n,k=5 operatii,k=10 operatii,k=100 operatii\n");
	

    for(n = MIN_N; n <= MAX_N; n+=INC_N){
        fprintf(listfile,"%d",n);
        printf("%d \n",n);
    for(k1 = 0; k1<=2; k1++){
        //printf("creare ");
        for(i = 1; i <= k[k1]; i++){
            creareLista(&(list[i]),n/k[k1],i);
        //    printf("%d ",i);
           // afisareLista(list[i]);
        }
        //printf("\n");

        listconc = (NOD*)malloc(sizeof(NOD));
        listconc->key = 0;
        listconc->noList = 0;
        listconc->next = NULL;
        operatii = 0;
        //printf("interclasare\n");
        interclasare(list,listconc,k[k1],&operatii);
        fprintf(listfile,",%f",operatii);
        //afisareLista(listconc);
    }
    fprintf(listfile,"\n");
	}
    fclose(listfile);

	fprintf(listfile1,"k,operatii\n");
	n=MAX_N;
	for(k1 = MIN_LIST; k1 <= MAX_LIST; k1+=INC_L){
        fprintf(listfile1,"%d",k1);
        printf("%d \n",k1);
        //printf("creare ");
        for(i = 1; i <= k1; i++){
            creareLista(&(list[i]),n/k1,i);
        //    printf("%d ",i);
           // afisareLista(list[i]);
        }
        //printf("\n");

        listconc = (NOD*)malloc(sizeof(NOD));
        listconc->key = 0;
        listconc->noList = 0;
        listconc->next = NULL;
        operatii = 0;
        //printf("interclasare\n");
        interclasare(list,listconc,k1,&operatii);
        fprintf(listfile1,",%f\n",operatii);
        //afisareLista(listconc);
    }

}
