/*
	***ANONIM***-Cornel ***ANONIM***

	Assign 6 - Josephus folosind liste circulare

	Complexitate - O(N*N)
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

typedef struct nod
{
	int val;
	struct nod *prec;
	struct nod *urm;

}NOD;

//functie ce adauga N elemente
void adauga(NOD **prim, NOD **ultim, int n)
{
    int i;
	for( i = 1; i<=n ; i++)
	{
			NOD *p = (NOD *)malloc(sizeof(NOD));
			p->val = i;

			if(*prim==NULL)
			{
				p->urm = p;
				p->prec = p;
				*prim=*ultim=p;
			}
			else
			{
				p->urm = *prim;
				p->prec = *ultim;
				(*ultim)->urm = p;
				(*prim)->prec = p;
				*ultim=p;
			}
	}

}

void scoateElement(NOD **prim, NOD **ultim, int *nrElem)
{
		NOD *q, *p = *prim;

		if(p==*prim)
            *prim = (*prim)->urm;
        if(p==*ultim)
            *ultim = (*ultim)->prec;

        p->prec->urm = p->urm;
        p->urm->prec = p->prec;
        q = p;
        p=p->urm;
        free(q);
        (*nrElem)--;
}

void afisare(NOD *prim)
{
	NOD *p = prim;

	do
	{
		printf("%d ", p->val);
		p = p->urm;
	}while(p != prim);

}

void Josephus(int n, int m)
{
	int nrElem = n; //numarul de elemente

	NOD *prim = 0;
	NOD *ultim = 0;
	

	printf("New Josephus (%d, %d)\n", n,m);

	//functie ce adauga n elemente in lista circulara
	adauga(&prim, &ultim, n);

	NOD *aux = prim;
	//scoatem elemente din m in m pana cand ramane doar unul
	while(nrElem > 1)
	{
		//functie ce afiseaza lista curenta
		afisare(aux);

	    int i;
		for(i=0;i<m-1;i++)
			prim = prim->urm;
 
		printf("   ->%d \n", prim->val);
		
		//functie ce scoate elementul curent din lista
		scoateElement(&prim, &ultim, &nrElem);
	}

	printf("%d", prim->val);

}



int main()
{

	//pentru testare
	Josephus(7,3);
	getch();

	//Evaluare
	/*
	for(int i=100;i<=10000;i+=100)
	{
		Josephus(i,i/2);
	}
	*/
	
	return 0;
}
