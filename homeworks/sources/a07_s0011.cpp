/****ANONIM*** ***ANONIM***
Grupa: ***GROUP_NUMBER***
Tema: Tranfmormarea intre diferite reprezentari ale arborilor multicai
Eficienta: O(logn)
*/

#include<iostream>
#include<conio.h>

using namespace std;

int n=9;
int p[]={1,6,4,1,6,6,-1,4,1};

struct nod
{
	int ch;
	int nrFi;
	nod *fi[10]; //pointer la vector de fi
}arbMW[9];
int radacina;

struct nodB 
{
	int ch;
	nodB * fiu;
	nodB * frate;
};
void t1() //transformarea din reprezntare parinte in reprezentare arbore multi cai
{
	for(int i=0;i<n;i++)
	{
		arbMW[i].ch=i;
		if(p[i]==-1)
			radacina=i;
	}
	for(int i=0;i<n;i++)
	{
		if(i!=radacina)
		{
			arbMW[p[i]].fi[arbMW[p[i]].nrFi]=&arbMW[i];
			arbMW[p[i]].nrFi++;
		}
	}
}

void t2( nod *rad, nodB *radB) // construirea arborelui binar din arbore multicai
{	
	if( rad != NULL)
	{
		if ( rad->nrFi > 0)
		{
			radB->fiu=new nodB;
			radB->fiu->ch = rad->fi[0]->ch;
			t2(rad->fi[0],radB->fiu); // apel recursiv pentru copil
			radB = radB->fiu;
			for ( int i = 1; i < rad->nrFi; i++)
			{
				radB->frate=new nodB;
				radB->frate->ch = rad->fi[i]->ch;				
				t2(rad->fi[i],radB->frate); //apel recursiv pentru frate
				radB = radB->frate;
			}
			radB->frate = NULL;
		}
		else 
			radB->fiu = NULL;
		
	}	
}

void afisareMW(nod *rad, int tab)//afisare in modul multicai
{
	if(rad==NULL)
		return;
	for(int i=0;i<tab;i++)
		cout<<"\t";
	cout<<rad->ch<<endl;
	for(int i=0;i<(rad->nrFi);i++)
		afisareMW(rad->fi[i], tab+1);
}

void afisareB( nodB *rad, int tab)// afisare in modul binar
{
	if(rad == NULL) 
		return;
	for ( int i = 0; i < tab; i++ )
		cout<<"\t";	
	tab++;
	cout<<rad->ch<<endl;
	afisareB(rad->fiu,tab);	
	tab--;
	afisareB(rad->frate,tab);
	
}


int main()
{
	cout<<"Vectorul initial de parinti este"<<endl;
	for(int i=0;i<n;i++)
		cout<<p[i]<<" ";
	cout<<endl;
	t1();

	//cout<<"Arborele sub forma multicai este "<<endl;
	//afisareMW(&arbMW[radacina],0);

	nodB *r=new nodB; //facem o copie a radacinii pt ca radacina se pastreaza
	r->ch = arbMW[radacina].ch;
	t2(&arbMW[radacina],r);

	cout<<"Arborele sub forma binara este"<<endl;
	r->frate = NULL;
	afisareB(r,0);	

	getch();
	
	return 0;
}
