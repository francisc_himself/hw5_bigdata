#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
/*
------------------OBSERVATII------------------

Metoda top-down efectueaza mai multe comparari
si asignari fata de metoda bottom-up.
*/
int n,dim;
int BUcount=0,TDcount=0;


int left(int i) {
  return (2 * i) ;
}

int right(int i) {
  return (2 * i) + 1;
}
int parent(int i) {
	return (i / 2);
}
void heapify(int a[],int i) 
{
  int largest;
  int l=left(i);
  int r=right(i);
  BUcount++;
  if ((l<=n)&&(a[l]<a[i])) 
  {
    largest=l;
  }
  else 
  {
    largest=i;
  }
  BUcount++;
  if ((r<=n)&&(a[r]<a[largest])) 
  {
    largest = r;
  }
  if (largest!=i) 
  {
	  BUcount++;
    int aux=a[i];
    a[i]=a[largest];
    a[largest]=aux;
    heapify(a,largest);
  }
}
void constr_heap_bu(int a[])
{
	int i;

	for (i=n/2;i>0;i--) 
	{
		heapify(a,i); 
		
	}
}

void heap_push(int a[],int x)
{ 
	int i,aux;
	dim++;
	TDcount++;
	a[dim]=x;
	i=dim;
	TDcount++;
	while((i>1) && (a[i]<a[parent(i)]))
	{	TDcount++;
		aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		TDcount++;
		i=parent(i);
	}

}


void heap_init(int a[])
{
	dim=0;
}

void constr_heap_td(int b[],int a[])
{	
	 heap_init(b); 
	for(int i=1;i<=n;i++)
	{ 
		heap_push(b,a[i]);	
	}	
}
int main()
{
	int k,i,j;
	FILE *fp;
	int tdcountmedie=0, bucountmedie=0;
    int heap_bu[10100],heap_td[10100],heap_td_aux[10100],vect1[30],vect2[30],vect3[30];
	fp=fopen("rezultat.csv","w");
	if(fp==NULL)
		perror("Eroare deschidere fisier");
	n=10;
	for(i=1;i<=n;i++)
    {printf("a[%d]=",i);
		scanf("%d ",&vect1[i]);
	vect2[i]=vect1[i];
	}
	constr_heap_bu(vect1);
	constr_heap_td(vect3,vect2);
	for(i=1;i<=n;i++)
    printf("%d ",vect1[i]);
	printf("\n");
	for(i=1;i<=n;i++)
    printf("%d ",vect3[i]);
	printf("\n");
   fprintf(fp,"n, heap_bu_count, heap_td_count \n"); 
	
	for(n=100;n<=10000;n+=100)
	{
		printf("%d \n",n);
		for(j=1;j<=5;j++)
		    {	
			FillRandomArray(heap_bu,n,10,50000,true,0);
			for(k=1;k<=n;k++)
				heap_td_aux[k]=heap_bu[k];
			constr_heap_bu(heap_bu);
			 bucountmedie=bucountmedie+BUcount;
			constr_heap_td(heap_td,heap_td_aux);
			tdcountmedie=tdcountmedie+TDcount;
		

		     }
		fprintf(fp, "%d,%d,%d\n",n,bucountmedie/5,tdcountmedie/5);
      }
	
	getch();
	
	return 0;
}