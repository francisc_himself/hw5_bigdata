#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int a[] = { 1, 6, 4, 1, 6, 6, -1, 4, 1};
int n = 9;

int f[9];//will keep the number of sons to be allocated in the multiway transformation
int p[9];

struct multiwayNode
{
    int info;
    int nbSons;
    multiwayNode **sons;
} tree[9];

struct binaryNode
{
    int info;
    binaryNode * son;
    binaryNode * sibling;
};

//vector to multiway

int multiway(int a[])
{
    int multiwayRoot = -1;
    for( int i = 0; i <n; i++)
    {
		tree[i].info = i;
        if( a[i] == -1 )
        {
            multiwayRoot = i;
        }
        else f[a[i]]++;  // number of sons to be alocated

        if ( i != multiwayRoot)
        {
            tree[a[i]].sons = (multiwayNode**)realloc(tree[a[i]].sons,sizeof(multiwayNode*) * f[a[i]]); // memory realloc
            tree[a[i]].sons[ tree[a[i]].nbSons++] = &tree[i];							//allocate another element to the existing ones
        }
    }
    return multiwayRoot;
}


void multiwayPrint(multiwayNode *multiwayRoot, int level)
{
    if(multiwayRoot == NULL) return;
    for ( int i = 0; i < level; i++ )
        printf("\t");
    printf("--->%d\n",multiwayRoot->info);
    for (int i = 0; i < multiwayRoot->nbSons; i++)
        multiwayPrint(multiwayRoot->sons[i],level +1);
}

//multiway to binary


void binary( multiwayNode *multiwayRoot, binaryNode *binaryRoot) // constructing the binary tree
{
    if( multiwayRoot != NULL)
    {
        if ( multiwayRoot->nbSons > 0)
        {
            binaryRoot->son = new binaryNode; // allocating memory for the son
            binaryRoot->son->info = multiwayRoot->sons[0]->info;
            binary(multiwayRoot->sons[0],binaryRoot->son); //recursive call for son
            binaryRoot = binaryRoot->son;
            for ( int i = 1; i < multiwayRoot->nbSons; i++)
            {
                binaryRoot->sibling = new binaryNode; // memory allocation for sibling
                binaryRoot->sibling->info = multiwayRoot->sons[i]->info;
                binary(multiwayRoot->sons[i],binaryRoot->sibling); //recursive call for sibling
                binaryRoot = binaryRoot->sibling;
            }
            binaryRoot->sibling = NULL;
        }
        else binaryRoot->son = NULL;
    }
}


void pretty_print( binaryNode *multiwayRoot, int level)
{
    if(multiwayRoot == NULL) return;
    for ( int i = 0; i < level; i++ ) printf("\t");
    level++;
    printf("--->%d\n",multiwayRoot->info);
    pretty_print(multiwayRoot->son,level);
    level--;
    pretty_print(multiwayRoot->sibling,level);
}

int main()
{
	printf("the initial vector:\n");
	for(int i=0;i<9;i++)
	printf("%d ",a[i]);
    printf("\ntransform from vector to multiway tree\n");
    int multiwayRoot = multiway(a);
    multiwayPrint(&tree[multiwayRoot],0);
    printf("\n\ntransform from multiway to binary tree\n");
    binaryNode* binaryRoot = new binaryNode;
    binaryRoot->info = tree[multiwayRoot].info;
    binaryRoot->sibling = NULL;
    binary(&tree[multiwayRoot],binaryRoot);
    pretty_print(binaryRoot,0);
	getche();
    return 0;
}