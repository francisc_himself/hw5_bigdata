/* ***ANONIM*** ***ANONIM*** Nicoleta, ***GROUP_NUMBER***
Algoritmi folositi: 

Sortarea prin metoda bulelor (Bubble sort)
Cases: best: vector ordonat crescator (nu are loc interschimbarea)
	   avarage: vector neordonat
	   worst: vector ordonat descrescator

Sortarea prin selectie
Cases: best: vector ordonat descrescator (elementul minim se afla in extrema dreapta a vectorului)
	   avarage: vector neordonat
	   worst: vector ordonat crescator

Sortarea prin insertie
Cases: best: vector ordonat descrescator (nu intra in while)
	   avarage: vector neordonat
	   worst: vector ordonat crescator
*/
#include<stdio.h>
#include<iostream>
#include<stdlib.h>
#include "Profiler.h"

Profiler ***ANONIM***("demo");

using namespace std;
#define SIZE 10000
// Functia de sortare Bubble Sort
void bubble(int *a,int n)
{
	int i,j,aux;
	for(i=0;i<n-1;i++)
	{
		for(j=0;j<n-i-1;j++)
			***ANONIM***.countOperation("bubble_comparare",n,1);
			if(a[j]>a[j+1])
			{
				aux=a[j];
				a[j]=a[j+1];
				a[j+1]=aux;
				***ANONIM***.countOperation("bubble_asignare",n,3);
			}
	}

}

// Functia de sortare prin selectie
void selectie(int *a,int n)
{
	int i,j,aux,imin;
	for(i=0;i<n;i++)
		
	{

		imin=i;
		for(j=i+1;j<n;j++)
		{
			if(a[j]<a[imin])
			{
				***ANONIM***.countOperation("selectie_comparare",n,1);
				imin=j;
			}
				***ANONIM***.countOperation("selectie_asignare",n,1);
		}
		aux=a[imin];
		a[imin]=a[i];
		a[i]=aux;
		***ANONIM***.countOperation("selectie_asignare",n,3);
	}
}

//Functia de sortare prin insertie
void insertie(int *a, int n)
{
	int i,j,x;
	for(i=1;i<n;i++)
	{
		x=a[i];
		j=0;
		while(a[j]<x)
		{
			***ANONIM***.countOperation("insertie_comparare",n,1);
			j++;
		}
			***ANONIM***.countOperation("insertie_asignare",n,1);
		for(int k=i;k>=j+1;k--)
			a[k]=a[k-1];
			***ANONIM***.countOperation("insertie_asignare",n,1);
		a[j]=x;
		***ANONIM***.countOperation("insertie_asignare",n,1);
	}
}

void main()
{
	int n, v[SIZE], v1[SIZE];
	int test[]={7,9,1,8,3};
	int test1[]={7,9,1,8,3};
	int test2[]={7,9,1,8,3};
	
	//Sortare (vectori neordonati)
	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n);
		//cout<<"n= "<<n; 
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);

		memcpy(v1,v,n*sizeof(int));
		insertie(v1,n);

		memcpy(v1,v,n*sizeof(int));
		selectie(v1,n);
	}
	


	/*Sortare (vectori ordonati crescator);
	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,10,50000,false,1);
		cout<<"n= "<<n; 
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);

		memcpy(v1,v,n*sizeof(int));
		insertie(v1,n);

		memcpy(v1,v,n*sizeof(int));
		selectie(v1,n);
	}*/
	

	
	/*Sortare (vectori ordonati descrescator)
	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,10,50000,false,2);
		cout<<"n= "<<n; 
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);

		memcpy(v1,v,n*sizeof(int));
		insertie(v1,n);

		memcpy(v1,v,n*sizeof(int));
		selectie(v1,n);
	}*/
	
	/* Testari in consola
	for(int i=0;i<5;i++)
	{
		cout<<test[i]<<" ";
	}
	cout<<endl;
	
	//apelam functia selectie
	selectie(test1,5);
	for(int i=0;i<5;i++)
	{
		cout<<test1[i]<<" ";
	}
	cout<<endl;
	
	//apelam functia insertie
	
	insertie(test2,5);
	for(int i=0;i<5;i++)
	{
		cout<<test2[i]<<" ";
	}
	cout<<endl;
	*/
	***ANONIM***.createGroup("Comparare","bubble_comparare","selectie_comparare","insertie_comparare");
	***ANONIM***.createGroup("Asignare","bubble_asignare","selectie_asignare","insertie_asignare");
	***ANONIM***.showReport();
}