#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

int atr, comp;

struct lista{
    int nr;
    int nr_lista;
    struct lista *urm;
};

//build heap bottom up
int LEFT (int i){
	return 2 * i;
}

int RIGHT (int i){
	return 2 * i + 1;
}

void MIN_HEAPIFY (struct lista A[], int i, int heap_size){
	int l = LEFT (i), lowest;
	int r = RIGHT (i);
	comp++;
	if ((l <= heap_size) && (A[l].nr < A[i].nr)) {
		lowest = l;
	}
	else
		lowest = i;
	comp++;
	if ( (r <= heap_size) && (A[r].nr < A[lowest].nr)) {
		lowest = r;
	}
	if (lowest != i) {
		atr = atr + 3;
		struct lista aux = A[i];
		A[i] = A[lowest];
		A[lowest] = aux;
		MIN_HEAPIFY (A, lowest, heap_size);
	}
}

void BUILD_MIN_HEAP_BOTTOM_UP (struct lista a[], int heap_size){
	int i;
	for (i = heap_size / 2; i > 0; i--)
		MIN_HEAPIFY (a, i, heap_size);
}

void CREARE_HEAP(struct lista a[], struct lista *vector[], int heap_size){
    struct lista *aux;
    int i;
    for(i = 1; i <= heap_size; i++){
        a[i] = *vector[i];
        aux = vector[i];
        vector[i] = vector[i]->urm;
        free(aux);
    }
    BUILD_MIN_HEAP_BOTTOM_UP(a,heap_size);
}

void INTERCLASARE(int destinatie[], struct lista heap[], struct lista *vector[], int heap_size){
    int l = 1, i;
    struct lista *aux;
    comp++;
    while(heap_size != 0){
        comp++;
        destinatie[l] = heap[1].nr;
        atr++;
        l++;
        i = heap[1].nr_lista;
        heap[1] = heap[heap_size];
        atr++;
        heap_size--;
        //BUILD_MIN_HEAP_BOTTOM_UP(heap, heap_size);
        comp++;
        if (vector[i] != NULL){
            heap_size++;
            heap[heap_size] = *vector[i];
            atr++;
            aux = vector[i];
            vector[i] = vector[i]->urm;
            free(aux);
        }
        BUILD_MIN_HEAP_BOTTOM_UP(heap, heap_size);
    }
}

void INITIALIZARE_LISTE(struct lista *vector[], int n, int heap_size){
    //i - nr lista
    int i, j;
    struct lista *aux, *el;
    for(i = 1; i <= heap_size; i++){
        aux = (struct lista*)malloc(sizeof(struct lista));
        aux->nr = rand()%10000;
        aux->nr_lista = i;
        aux->urm = NULL;
        vector[i] = aux;
        for(j = 1; j <= n/heap_size+1; j++){
            aux = (struct lista*)malloc(sizeof(struct lista));
            aux->nr = rand()%10000;
            aux->nr_lista = i;
            el = vector[i];
            while((el->urm != NULL) && (el->urm->nr < aux->nr)){
                el = el->urm;
            }
            aux->urm = el->urm;
            el->urm = aux;
        }
        aux = vector[i];
        vector[i] = vector[i]->urm;
        free(aux);
    }
}

int main()
{
    struct lista *vector[600];
    int n = 100, k, i;
    struct lista a[10002];
    int b[10002];
    //initializare pas pt functia rand
	srand (time(NULL));

	FILE *f, *g;
	f = fopen("liste.csv","w");
	g = fopen("liste1.csv", "w");
	fprintf(f,"n,atr k5,comp k5,atr+comp k5, atr k10,comp k10,atr+comp k10,atr k100,comp k100,atr+comp k100\n");

    /************************EXEMPLU******************************/
    k = 5;
    INITIALIZARE_LISTE(vector, n, k);
    CREARE_HEAP(a, vector, k);
    //interclasare
    INTERCLASARE(b, a, vector, k);
    for (i = 1; i <= n; i++)
        printf("%d ", b[i]);
    /******************************************************************/

    //n - nr total de elemente
    for(n = 100; n <= 9900; n = n + 100){
        atr = 0;
        comp = 0;
        fprintf(f, "%d,", n);
        k = 5;
        INITIALIZARE_LISTE(vector, n, k);
        CREARE_HEAP(a, vector, k);
        INTERCLASARE(b, a, vector, k);
        fprintf(f, "%d,%d,%d,", atr, comp, atr+comp);
        atr = 0;
        comp = 0;
        k = 10;
        INITIALIZARE_LISTE(vector, n, k);
        CREARE_HEAP(a, vector, k);
        INTERCLASARE(b, a, vector, k);
        fprintf(f, "%d,%d,%d,", atr, comp, atr+comp);
        atr = 0;
        comp = 0;
        k = 100;
        INITIALIZARE_LISTE(vector, n, k);
        CREARE_HEAP(a, vector, k);
        INTERCLASARE(b, a, vector, k);
        fprintf(f, "%d,%d,%d\n", atr, comp, atr+comp);
        printf("%d\n",n);
    }
    fclose(f);
    n = 9900;
    fprintf(g, "k,atr,comp,atr+comp\n");
    for(k = 10; k <= 140; k = k + 10){
        atr = 0;
        comp = 0;
        fprintf(g, "%d,", k);
        INITIALIZARE_LISTE(vector, n, k);
        CREARE_HEAP(a, vector, k);
        INTERCLASARE(b, a, vector, k);
        fprintf(g, "%d,%d,%d\n", atr, comp, atr+comp);
        printf("%d\n",k);
    }
    return 0;
}
