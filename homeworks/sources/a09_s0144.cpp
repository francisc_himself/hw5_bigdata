#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

/* BFS este un algoritm de cautare de baza intr-un graf. Alg. lui Dijkstra pt determinarea drumurilor minime de la un nod sursa la celelalte si alg
lui Prim pt determinarea arborelui partial de cost minim folosesc idei similare celor din alg de cautare in latime. De asemenea, si BFS este tot un 
algoritm liniar ( O(|V|+|E|))*/


#define WHITE 0
#define GRAY 1
#define BLACK 2
#define V_MAX 10000
#define E_MAX 60000

//nodurile grafului
typedef struct {
	int parent;
	int distance;
	int color;
}GRAPH_NODE;


//coada
typedef struct _NODE{
	int key;
	struct _NODE *next;
	//int nrm=0;
}NODE;


NODE*prim,*ultim;
NODE * adj[V_MAX];
GRAPH_NODE graph[V_MAX];
int edges[E_MAX];

void generate(int n,int m) //generare liste de adiacenta (random) pt fiecare nod: (n=varfuri;m=muchii)
{
	memset(adj,0,n*sizeof(NODE*)); //seteaza vectorii la 0
	memset(graph,0,n*sizeof(GRAPH_NODE));
	FillRandomArray(edges,m,0,n*n-1,true); //random
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE *nod=new NODE;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;

	}
	
}

void enq(int info){
	NODE* coada;
	coada=(NODE*) malloc(sizeof(NODE));
	coada->key=info;
	coada->next=NULL;

	if(ultim==NULL)
	{
		prim=ultim=coada;
	}
	else {
		ultim->next=coada;
		ultim=coada;
	}
}

int deq() {
	NODE *p;
	int info;
//	printf("+");
	if(prim==NULL)
	{
		return 0;
	}
	else if(prim->next==NULL){
		info=prim->key;
		p=prim;
		prim=ultim=NULL;
		delete p;
	} else
	{
		info=prim->key;
		p=prim;
		prim=prim->next;
		delete p;
	}
	return info;
}
void bfs(NODE*s, int n);

void bfs2(NODE* s,int n){
	 for(int j=0; j<n; j++)
    {   
		
        if(s->key==WHITE)
            bfs(s,j);
    }
}

void bfs(NODE*s, int n)
{   //printf("+");
	if(s==NULL)
		return ;
    for(int i=0;i<n;i++){
		//printf("+");
		
		graph[i].color=WHITE;
		graph[i].distance=100;
		graph[i].parent=0;
   }
	bfs2(s,s->key);
	//printf("+");
	graph[s->key].color=GRAY;
	graph[s->key].distance=0;
	graph[s->key].parent=0;

	enq(s->key);
	int  elem,m=0;
	while(prim)
	{
		elem=deq();
		//pt fiecare varf din lista de adiacenta a lui elem
		while(adj[elem]!=NULL){  
			int v;
			v=adj[elem]->key;
			if(graph[v].color==WHITE){
				graph[v].color=GRAY;
				graph[v].distance=graph[elem].distance+1;
				graph[v].parent=elem;
				enq(v);
            }
			adj[elem]=adj[elem]->next;
		}
		elem=deq();
		graph[elem].color=BLACK;
	}

}

void afisare(NODE* nod){
//	printf("+");
	NODE *v=nod;
	while(v!=NULL){
		printf("%d ",v->key);
		v=v->next;
	}
	printf("\n");
}
/*void prettyPrintMN(NODE*node,int nivel=0)
{
   for(int i=0;i<nivel;i++){
		printf("   ");
        printf("%d \n",node->key);
        for(int i=0;i<node->count;i++)
		{
		    prettyPrintMN(node->child[i],nivel+1);
        }
  }
}*/

int main()
{   
	generate(9,6);
	bfs(*adj,9);
	for(int i=0;i<9;i++)
	{
		printf("%d:",i);
	    afisare(adj[i]);
	}
	return 0;
	//getch();
}