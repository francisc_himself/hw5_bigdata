#include <iostream>
#include <conio.h>
#include "Profiler.h"

Profiler profiler("QuickVsHeap");

int n, size, j=0, a[100000];

void swap(int &x,int &y)
{
    int temp=x;
    x=y;
    y=temp;
}

void heapify(int x)
{
    int left=(2*x);
    int right=(2*x)+1;
    int large;
	profiler.countOperation("CompareHeap", n);
	j++;
    if(a[left]>a[x] && left <= n)
    {
        large=left;
    }
    else
    {
        large=x;
    }
	profiler.countOperation("CompareHeap", n);
    if(a[right]>a[large] && right <= n)
    {
        large=right;
    }
    if(x!=large)
    {
        swap(a[x],a[large]);
		profiler.countOperation("AssignHeap", n, 3);
        heapify(large);
    }
}

void bottom_up()
{
	for(int i = n/2; i>0;i--)
		heapify(i);
}
 

void HeapSort(int n) 
{
	int size = n;
    bottom_up();
    for (int i = size; i > 0; i--)
    {
		swap(a[1],a[i]);
		size = size-1;
		profiler.countOperation("AssignHeap", n, 3);
		heapify(1);
    }

}

void QuickSort(int left, int right) 
{
      int i = left, j = right;
      int pivot =a[(i+j)/2];
 
      while (i <= j) 
	  {		
		    profiler.countOperation("CompareQuick", n);
            while (a[i] < pivot)
			{
				i++;
				profiler.countOperation("CompareQuick", n);
			}
			
			profiler.countOperation("CompareQuick", n);
			while (a[j] > pivot)
			{
                j--;
				profiler.countOperation("CompareQuick", n);
			}
			
			profiler.countOperation("CompareQuick", n);
			if (i <= j) 
			{
                  swap(a[i],a[j]);
				  profiler.countOperation("AssignQuick", n, 3);
                  i++;
                  j--;
            }
      }
 
	  profiler.countOperation("CompareQuick", n);
      if (left < j)
            QuickSort(left, j);
	  profiler.countOperation("CompareQuick", n);
      if (i < right)
            QuickSort(i, right);
}


void main()
{

	for (n=0;n<=10000;n=n+100)
	{
		FillRandomArray(a, n+1, 0, 10000);
		QuickSort(1, n);
	}
	
	for (n=0;n<=10000;n=n+100)
	{
		FillRandomArray(a, n+1, 0, 10000);
		HeapSort(n);
	}
	
	profiler.createGroup("Compare","CompareQuick","CompareHeap");
	profiler.createGroup("Assign","AssignQuick","AssignHeap");

	profiler.addSeries("Heap","CompareHeap","AssignHeap");
	profiler.addSeries("Quick","CompareQuick","AssignQuick");

	profiler.createGroup("C plus A","Quick","Heap");

	profiler.showReport();
	

}