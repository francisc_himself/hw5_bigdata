#include<stdio.h>
#include<stdlib.h>
#include<conio.h>

/**
*   @autor: ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
*   @comentariu: Am observat ca algoritmul ales este unul eficient si
*   ca acest program ruleaza O(nln(n));
*/

typedef struct nod
{
    int number, nivel;
    struct nod *left, *right, *parent;
} node;

node *radacina = NULL;
double operatii[200];

void writeResult(FILE *myFile, double *a, int length){
    int i;
    fprintf(myFile,"\n");
    for(i = 1; i <= length; i++)
        fprintf(myFile, "%.0f;", a[i]);
}

void prettyPrint(node *tree, int size){
    int i;
    if(!tree) return;
    if(tree->right)
        prettyPrint(tree->right,size+1);
    printf("\n");
    for(i = 0; i < 5*size; i++)
    {
        printf(" ");
    }
    printf("%d", tree->number);
    if(tree->left)
        prettyPrint(tree->left,size+1);
    return;
}

void prettyPrintSize(node *tree,int size){
    int i;
    if(!tree) return;
    if(tree->right)
        prettyPrintSize(tree->right, size+1);
    printf("\n");
    for(i = 0; i < 5 * size; i++)
    {
        printf(" ");
    }
    printf("%d", tree->nivel);
    if(tree->left)
        prettyPrintSize(tree->left, size+1);
    return;
}

void insertInTree(int value, int op){
    node *aux = (node*)malloc(sizeof(node));
    aux->nivel = 0;
    aux->number = value;
    aux->right = NULL;
    aux->left = NULL;
    aux->parent = NULL;

    node * root = radacina;
    operatii[op/100] += 7;

    node * y = NULL;

    while(root){
        y = root;
        y->nivel = y->nivel+1;
        if(value <= root->number){
            root = root->left;
        }
        else{
            root = root->right;
        }
        operatii[op/100] += 4;
    }
    aux->parent = y;
    operatii[op/100] += 2;
    if(y)
    {
        operatii[op/100] += 2;
        if(value <= y->number){
            y->left = aux;
        }
        else{
            y->right = aux;
        }
    }
    else{
        radacina = aux;
        operatii[op/100] += 1;
    }
}

void transplant(node *u, node *v,int n){
    operatii[n/100]+=1;
    if(!u->parent)
    {
        radacina =v;
        operatii[n/100]+=1;

    }
    else
    {
        operatii[n/100]+=2;
        if(u == u->parent->left)
            u->parent->left = v;
        else
            u->parent->right = v;
    }
    operatii[n/100]+=1;
    if(v)
    {
        v->parent = u->parent;
        v->nivel= u->nivel;
        operatii[n/100]+=2;
    }
}

node * treeMinimum(node * nod, int n){
    while(nod->left){
        nod = nod->left;
        operatii[n/100]+=1;
    }
    return nod;
}

void deleteNode(node * nod, int n){
    node * y;
    node * aux = nod;
    operatii[n/100] += 1;
    while(aux){
        aux->nivel=aux->nivel -1;
        aux= aux->parent;
        operatii[n/100]+=2;
    }
    operatii[n/100]+=1;
    if(!nod->left){
        transplant(nod,nod->right,n);
    }
    else{
        operatii[n/100]+=1;
        if(!nod->right)
            transplant(nod,nod->left,n);
        else{
            y = treeMinimum(nod->right,n);
            aux = y;
            operatii[n/100]+=3;
            while(aux!=nod){
                aux->nivel=aux->nivel -1;
                aux = aux->parent;
                operatii[n/100]+=3;
            }
            operatii[n/100]+=1;
            if (y->parent != nod){
                transplant(y,y->right,n);
                y->right = nod->right;
                y->right->parent=y;
                operatii[n/100]+=2;
            }
            transplant(nod,y,n);
            y->left = nod->left;
            y->left->parent = y;
            operatii[n/100]+=2;
        }
    }
    free(nod);

}

node * selectNode(node * radacina, int i , int n){
    int r;
    operatii[n/100] += 2;
    if(radacina->left){
        r = radacina->left->nivel + 2;
    }
    else{
        r = 1;
    }
    operatii[n/100]+=1;
    if(i == r){
        return radacina;
    }
    operatii[n/100] += 1;
    if (i < r){
        return selectNode(radacina->left, i, n);
    }
    else{
        return selectNode(radacina->right, i-r, n);
    }
}

void buildTree(int a, int b, int n){
    operatii[n/100] += 1;
    if(a > b)
        return;
    operatii[n/100]+=1;
    int inserted = (a + b)/2;
    insertInTree(inserted, n);
    buildTree(a, inserted - 1, n);
    buildTree(inserted + 1, b, n);
}

void josephus(int n, int m, int print){
    int i;
    radacina = NULL;
    node *nod;
    buildTree(1, n, n);
    int j = 1;
    operatii[n/100]+=2;
    if(print){
        prettyPrint(radacina, 0);
        prettyPrintSize(radacina, 0);
    }

    for(i = n; i > 0; i--){
        j = ((j + m - 2) % i) + 1;
        operatii[n/100]+=1;
        nod = selectNode(radacina, j, n);
        if(print){
            printf("\nNodul selectat este: %d",nod->number);
        }
        deleteNode(nod,n);
        if(print){
            prettyPrint(radacina, 0);
            prettyPrintSize(radacina, 0);
        }
    }
}

int main()
{
    int i;
    FILE *myFile = fopen("result.csv","w");

    josephus(7, 3, 1);
    for(i = 1; i <= 100; i++)
        operatii[i] = 0;
    for(i = 100; i <= 10000; i+=100)
        josephus(i, i/2, 0);
    writeResult(myFile, operatii, 100);
    fclose(myFile);
    getch();
    return 0;
}
