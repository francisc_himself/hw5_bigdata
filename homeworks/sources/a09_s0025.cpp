// ***ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER***
// Tema nr 9: BFS

#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define ALB 0
#define GRI 1
#define NEGRU 2
#define INF 10000

typedef struct Prop
{
	int color, d, parinte;
} PROP;


// A structure to represent an adjacency list node
typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

NODE *prim[60001], *ultim[60001];
PROP prop[60001];
int timp;
int k, nr_op;


int Q[60001];

void BFS (int s, int nrv)
{
	int u;
	for (u=1; u<=nrv; u++)
	{	
		nr_op++;
		if (u!=s)
		{
			nr_op=nr_op+3;
			prop[u].color=ALB;
			prop[u].d=INF;
			prop[u].parinte=0;
		}
	}
	prop[s].color=GRI;
	prop[s].d=0;
	prop[s].parinte=0;
	nr_op=nr_op+3;
	k++;
	Q[k]=s;
	while(k!=0)
	{
		u=Q[1];
		for (int i=1; i<k; i++)
			Q[i]=Q[i+1];
		k--;
		Node *temp;
		temp=prim[u];
		printf("%d ",u);
		while (temp!=NULL)
		{
			int v=temp->val;
			temp=temp->next;
			nr_op++;
			if (prop[v].color==ALB)
			{
				nr_op=nr_op+3;
				prop[v].color=GRI;
				prop[v].d=prop[u].d+1;
				prop[v].parinte=u;
				k++;
				Q[k]=v;
			}
		}
		//k--;
		nr_op++;
		prop[u].color=NEGRU;
	}
}



bool verif (int x, int y)
{
	NODE *p;
	p=prim[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->next;
	}
	return true;

}

void genereaza_lista_adiacenta(int nrv,int nre)  //nrv= numarul de varfuri, nre = numarul de muchii
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		prim[i]=ultim[i]=NULL;
	i=1;
	while (i<=nre)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
				if(prim[u]==NULL)
				{
					prim[u]=(NODE *)malloc(sizeof(NODE *));
					prim[u]->val=v;
					prim[u]->next=NULL;
					ultim[u]=prim[u];
					i++;
				}
				else {
						if (verif (u, v))
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;
							p->next=NULL;
							ultim[u]->next=p;
							ultim[u]=p;
							i++;
						}
					}
		}
	}
}

//print lista de adiacenta
void print(int nrv)
{
	int i;
	NODE *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=prim[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->next;
		}
		printf("\n");
	}
}


	





int main()
{
	int nre, nrv;
	int v,e;
	nrv=5;
	nre=10;
	genereaza_lista_adiacenta(nrv, nre);
	print(nrv);
	printf("\n\n\n");
	BFS(1, nrv);

	FILE *f;
    f=fopen("Rezultate1.csv","w");
    fprintf(f,"nrv,nre,nr_op\n");
	v=100;
    for (e=1000;e<=5000;e=e+100)
    {
        nr_op=0;
        genereaza_lista_adiacenta(v, e);
        BFS(1, v);
        fprintf(f,"%d,%d%d\n",e,v,nr_op);
    }
    fclose(f);


	/*FILE *f1;
    f1=fopen("Rezultate2.csv","w");
    fprintf(f1,"nrv,nre,nr_op\n");
	e=9000;
    for (v=100;v<=200;v=v+10)
    {
        nr_op=0;
        genereaza_lista_adiacenta(v, e);
        BFS(1, v);
        fprintf(f1,"%d,%d%d\n",e,v,nr_op);
    }
    fclose(f1)*/
	getch(); 
    return 0;
}

