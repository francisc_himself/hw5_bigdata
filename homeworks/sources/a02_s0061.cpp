/**
Student: ***ANONIM*** ***ANONIM*** ***ANONIM***
Group: ***GROUP_NUMBER***
Problem specification: Analysis & comparison of bottom-up and top-down build heap approaches
Comments:
  AVERAGE CASE:
   -Input data: an array with random elements; 5 measurements and the graph represents the average of the 5 measurements
   -Comparison:
		Top-down: linearly, faster;
		Bottom-up: linearly, slower then top-down;
  WORST CASE:
   -Input data: for bottom-up is an increasing array of elements; for top-down is a decresing array of elements;
   -Comparison:
		Top-down: linearly, much faster;
		Bottom-up: linearly, slower than top-down;the difference is very obvious here;
Running time: Bottom-up: O(n)
			  Top-down: O(nlgn)
  **/


#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>


FILE *f1;
FILE *f2;
int n,i,key;;
int m[10000];
int g[10000];
int heapsize=0;
int Nr=0;
int Co=0;
int At=0;




void generare(int n,int *m)//procedura de generare a unui vector m de n numere aleatoare
{
    int i;//indice folosit pentru parcurgerea intervalului 1->n
    for(i=0;i<n;i++)
        m[i]=rand()%10000;//se creeaza n elemente aleatoare in intervalul 1->10.000
	    g[i]=m[i];
}

//top down

void incrementarecheie(int *m, int i, int key)
{
int aux;
if (key<m[i]) printf("cheia e mai mica decat cea actuala");
Co++;
m[i]=key;
At++;
while ((i>1) && (m[i/2]<m[i]))//verifica daca fiul e mai mic decat parintele si le interschimba
{
	aux=m[i];
	m[i]=m[i/2];
	m[i/2]=aux;
	At=At+3;
}
	i=i/2;
}

void inseraremaxheap(int *m,int key)
{
heapsize++;//creste dimensiunea heap-ului
m[heapsize]=-1;//elementul de pe pozitia finala a heap-ului e initializat cu -1 
incrementarecheie(m, heapsize, key);
}

void constructieheapmax(int *m)//se construieste un arbore maximal
{
heapsize=1;//se initializeaza dimensiunea cu 1
for (i=2; i<n;i++)
inseraremaxheap(m, m[i]);
}


//bottom up

void reconstituieHEAP(int *m, int n,int i)//procedura de reconstiturire a heap-ului
{
	int heap;
	int imax,le,ri,aux;

heap=0;//heap e o variabila booleana initializata la false
while (!heap) //cat timp heap e falsa
  {
	heap=1;//se face heap true
	le=2*i;//fiul stang e 2*i
	ri=le+1;//fiul drept e 2*i+1
	imax=i;//indexul maxim se initiaza la i
	if ((le < n) && (m[le] > m[imax])) 
	{	Co++;
		imax=le;//se muta indexul maxim in fiul stang daca e mai mare ca radacina
	}
	if ((ri < n) && (m[ri] > m[imax])) 
	{	Co++;	
		imax=ri;//se muta indexul maxim in fiul drept daca e mai mare ca radacina
	}
	if (imax != i) //daca s-a mutat indexul maxim in un fiu si nu mai e in i
	{
		aux=m[i];//se interschimba tatal cu fiul respectiv
		m[i]=m[imax];
		m[imax]=aux;
		At=At+3;
		i=imax;//se muta i in indexul maxim
		heap=0;//heap-ul revina in false
	}
  }
}

void construiesteHEAP(int *m,int n)//construieste heap-ul 
{
	int i;
	for (i=n/2;i>=0;i--)
	reconstituieHEAP(m, n, i);
}

void afis (int m[], int n,int i,int nivel)
{
	
	int j,ri,le;
	if(i<n)
	{
		for(j=0;j<nivel;j++)
		{
			printf ("        ");
	        
		    
		}printf ("%d\n",m[i]);
		nivel++;
	     afis(m,n,2*i,nivel);
	     afis(m,n,2*i+1,nivel);
	}
	
}


void main () 
{	//int k;
	
    //f1 = fopen("TDheap.txt","w");
	f2 = fopen("BUheap.txt","w");
    n=10;//se porneste de la 20 de elemente
	while (n<20)
	{
		At=0;
		Co=0;
		Nr=0;
	//for (k=0;k<5;k++)
		
		{
	generare(n, m);//se genereaza n elemente 
	//constructieheapmax(n);//se construieste maxheap-ul TD	
	construiesteHEAP(m,n);//se construieste heap-ul	BU
    afis(m,n,1,0);
	}
		Nr=At+Co;
	fprintf(f2, "%d %d %d %d\n",n,At/5,Co/5,Nr/5);    //BU
	//fprintf(f1, "%d %d %d %d\n",n,At/5,Co/5,Nr/5);  //TD
	srand(n/(i+1));//se schimba seed-ul random-ului
	n=n+1000;
	}
	//fclose(f1);//inchide fisierul
	fclose(f2);
	_getch();
}
