/*
Lorinczi ***ANONIM***
***GROUP_NUMBER***

in the avarage case the two algorithms have a linear complexity
they differe only in a constant

in the worst case the bottom-up algorithm has a linear complexity
but the top-down algorithm has a complexity of nlogn 
*/

#include<stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <time.h>

int b[10002],c[10002],n,op[4][102];
FILE *f;

void heapify(int a[], int i,int n, int opr,int wrst)
{
	int l;
	int r;
	int m;
	int hlp;
	l=2*i;
	r=2*i+1;
	m=i;

	if(a[l]>a[i] && l<=n) m=l; op[0+wrst][opr]++;
	if(a[r]>a[m] && r<=n) m=r; op[0+wrst][opr]++;
	if(m!=i)
	{hlp=a[m]; a[m]=a[i]; a[i]=hlp; op[0+wrst][opr]+=3;
	heapify(a,m,n,opr,wrst);}
}

void botupheap(int a[], int n, int opr,int wrst)
{
for(int i=n/2;i>=1;i--)
heapify( a,i,n,opr,wrst);
}


void insertkey(int a[], int i,int x, int n,int opr,int wrst)
{ int aux=0; op[2+wrst][opr]++;
if(x<a[i])
{printf("alma");} op[2+wrst][opr]++;
a[i]=x; op[2+wrst][opr]++;
while(i>1 && a[i/2]<a[i])
{ op[2+wrst][opr]++;
aux=a[i/2]; op[2+wrst][opr]++;
a[i/2]=a[i]; op[2+wrst][opr]++;
a[i]=aux; op[2+wrst][opr]++;
i=i/2; 
}
op[2+wrst][opr]+=2;
}

void heapinsert(int a[], int &n, int x,int opr,int wrst)
{
n=n+1;
a[n]=-10;
insertkey(a,n,x,n,opr,wrst);
}


void topdownheap(int a[], int n,int opr,int wrst)
{int i,m;
	m=1;
	for(i=2; i<=n;i++)
	heapinsert(a,m,a[i],opr,wrst);
}




void printkey(int x,int depth)
{
for(int i=1;i<=depth;i++)
printf(" ");
printf("%d\n",x);
}

void rekur(int a[], int n, int i, int depth)
{
if(i*2<=n)
rekur(a,n,i*2,depth+5);
printkey(a[i],depth);
if(i*2+1<=n)
rekur(a,n,i*2+1,depth+5);
}
int main()
{

srand(time(NULL));

//test for bottom-up
b[1]=4; b[3]=2; b[5]=15; b[7]=1; b[9]=10;
b[2]=5; b[4]=6; b[6]=9; b[8]=7; b[10]=14;
b[11]=12; b[12]=13; b[13]=11; b[14]=3; b[15]=8;
botupheap(b,15,101,0);
rekur(b,15,1,3);
printf("top-down\n");
//test for top-down
b[1]=4; b[3]=2; b[5]=15; b[7]=1; b[9]=10;
b[2]=5; b[4]=6; b[6]=9; b[8]=7; b[10]=14;
b[11]=12; b[12]=13; b[13]=11; b[14]=3; b[15]=8;
topdownheap(b,15,101,0);
rekur(b,15,1,3);


for(n=100;n<=10000;n+=100)
{
	for(int j=1; j<=5;j++)
{
for(int i=1;i<=n;i++)
{b[i]=rand() % 20000;
c[i]=b[i];}
botupheap(b,n,n/100,0);
topdownheap(c,n,n/100,0);
	}
for(int i=1;i<=n;i++)
{b[i]=i;
c[i]=i;}
botupheap(b,n,n/100,1);
topdownheap(c,n,n/100,1);
}

for(int i=1;i<=100;i++)
{op[0][i]=op[0][i]/5;
op[2][i]=op[2][i]/5;
}



f=fopen("heap.csv","w");
	fprintf(f,"n,avg botup,wrst botup, avg topdown, wrst topdown\n");
	for(int i=1;i<=100;i++)
	{	fprintf(f,"%d,",i*100);
		fprintf(f,"%d,",op[0][i]);
		fprintf(f,"%d,",op[1][i]);
		fprintf(f,"%d,",op[2][i]);
		fprintf(f,"%d\n",op[3][i]);
	}


getch();
}