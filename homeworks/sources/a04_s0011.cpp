/****ANONIM*** ***ANONIM***
Grupa:***GROUP_NUMBER***
Tema 4 : Interclasarea a k liste folosind heap.
Complexitatea algoritmului este  O(n*log2(k)).

*/

#include<iostream>
#include<conio.h>
#include<time.h>
#include<fstream>

using namespace std;
int dim, op;
ofstream fout("date.csv");

struct elem_heap //structura pentru elementele din heap
{
	int cheie, indice;

};

struct elem_lista //structura pentru elementele din lista
{
	int val;
	elem_lista *urm;
};

elem_lista *prim=NULL, *ultim=NULL, *cap[10001];

/////OPERATII PE LISTA
void adaugare_nod(int ch) //adugare nod in lista
{	
	elem_lista *p= new elem_lista;
	p->val=ch;
	if(prim==NULL) // daca lista este goala, nodul inserat va fi primul nod din lista
	{
		prim=p;
		ultim=prim;
	}
	else
	{
		ultim->urm=p;
		ultim=p;
	}
	ultim->urm=NULL;
}

elem_lista *stergere_nod(int i) //salvare elementul cu indicele i din lista
{
	elem_lista *p=cap[i]->urm;
	if(p!=NULL)
		cap[i]->urm=p->urm;
	return p;
}

void adaugare_lista(int i) //adugare lista in vectorul ce retine capetele listelor
{
	cap[i]=new elem_lista;
	cap[i]->urm=prim;
}

///////OPERATII HEAP
int p(int i)
{
	return i/2;
}

int st(int i)
{
	return 2*i;
}

int dr(int i)
{
	return 2*i+1;
}

void h_init()
{
	dim=0;
}

void reconstructie_heap(elem_heap h[], int i)
{
	int l=st(i);
	int r=dr(i);
	int min;
	elem_heap aux;
	op++;
	if(l<=dim &&h[l].cheie<h[i].cheie)
		min=l;
	else
		min=i;
	op++;
	if(r<=dim && h[r].cheie<h[min].cheie)
		min=r;
	if(min !=i)
	{
		op=op+3;
		aux=h[i];
		h[i]=h[min];
		h[min]=aux;
		reconstructie_heap(h,min);
	}
}


elem_heap h_pop(elem_heap h[])
{
	elem_heap el=h[1];
	op++;
	h[1]=h[dim];
	dim--;
	reconstructie_heap(h,1);
	return el;
}
void h_push(elem_heap h[], int x, int ind)
{
	dim++;
	op++;
	h[dim].cheie=x;
	h[dim].indice=ind;

	int i=dim;
	op++;
	while((i>1) &&(h[p(i)].cheie>h[i].cheie))
	{
		op=op+3;
		elem_heap aux=h[p(i)];
		h[p(i)]=h[i];
		h[i]=aux;
		i=p(i);
	}
}


void interclasare(int k)
{
	int x;
	elem_lista *p;
	elem_heap el;
	elem_heap h[10000];
	prim=NULL;
	ultim=NULL;
	h_init();
	for(int i=1;i<=k;i++)
	{
		x=(stergere_nod(i))->val;
		op++;
		h_push(h,x,i);
	}
	while(dim>0)
	{
		el=h_pop(h);
		op++;
		adaugare_nod(el.cheie);
		op++;
		p=stergere_nod(el.indice);
		if(p!=NULL)
		{
			h_push(h,p->val, el.indice);
			free(p);
		}
	}
}

void genereaza_liste(int k, int n)
{
	int x, y;
	op=0;
	srand(time(NULL));
	for(int i=1;i<=k;i++)
	{
		x=0;
		y=0;
		prim=NULL;
		ultim=NULL;
		for (int j=1;j<=n/k;j++)
		{
			x=y+rand();
			adaugare_nod(x);
			y=x;
		}
		adaugare_lista(i);
	}
	
}

/////////////////////DEMO
void main()
{
	int k=3, n=15;
	elem_lista *q;
	genereaza_liste(k,n);
	for(int i=1;i<=k;i++)
	{
		cout<<"Lista "<<i<<" este ";
		for(q=cap[i]->urm;q!=NULL;q=q->urm)
			cout<<q->val<<" ";
		cout<<endl;
	}
	interclasare(k);
	cout<<"Lista cu cele k liste interclasate este "<<endl;
	for(q=prim;q!=NULL;q=q->urm)
		cout<<q->val<<" ";
	getch();
}

/////////Pt k=5, 10  si 100 variem n
/*void main()
{
	int n,k, opk5=0, opk10=0,opk100=0;
	fout<<"n; k=5; k=10;k=100"<<endl;
	for(n=100;n<=10000;n=n+100)
	{
		k=5;
		op=0;
		for(int i=0;i<5;i++)
		{
			genereaza_liste(k,n);
			interclasare(k);
			opk5=opk5+op;
		}

		k=10;
		op=0;
		for(int i=0;i<5;i++)
		{
			genereaza_liste(k,n);
			interclasare(k);
			opk10=opk10+op;
		}

		k=100;
		op=0;
		for(int i=0;i<5;i++)
		{
			genereaza_liste(k,n);
			interclasare(k);
			opk100=opk100+op;
		}

		fout<<n<<";"<<opk5/5<<";"<<opk10/5<<";"<<opk100/5<<endl;
	}
	fout.close();
}*/



/////////Pt n=10000, variem k intre 1 si 500 cu increment de 10
/*void main()
{
	int n=10000,k;
	fout<<"k; operatii"<<endl;
	for(k=10;k<=500;k=k+10)
	{
		op=0;
		for(int i=0;i<5;i++)
		{
			genereaza_liste(k,n);
			interclasare(k);
		}
		fout<<k<<";"<<op/5<<endl;
	}
	fout.close();
}*/
