#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include<math.h>

typedef struct NOD{
	int valoare;
	NOD *next;
} NOD;
NOD *head[1000];
NOD *tail[1000];
NOD *joinedHead,*joinedTail;
typedef struct HEAP{
	int idx;
	NOD *elem;
}HEAP;
HEAP *heap[1000];
HEAP *aux=new HEAP;
int h_size=0;


// se face inserarea dupa ultimul nod din lista

void inserare_lista(NOD **h,NOD **t,int val)
{
	NOD *p;
	//se aloca memorie pentru elem p de tip NOD
	p=(NOD*)malloc(sizeof(NOD));
	//daca nu s-a alocat 
	if (p==0)
	{
		printf("\n EROARE:nu s-a alocat memorie in heap	");
	}else{
		p->valoare=val;

		//verificam daca lista e vida
		if(*h==0) //lista e vida
		{
			*h=p;
			*t=p;
		}
		else   //lista nu e vida
		{
			(*t)->next=p;
			*t=p;
		}
		p->next=0;}
}


//stergerea primului elem din lista
void sterge_elem(NOD **h)
{
	/*NOD *p;
	if(*h==0){
	printf("lista e vida");
	return;
	}
	p=*h;
	*h=(*h)->next;
	free(p);
	if(*h==0){
	*t=0;
	printf("\nLista a devenit vida dupa stergerea unicului nod\n");
	}*/
	NOD *p;
	if(*h!=0)
	{
		p=*h;
		*h=(*h)->next;
		free(p);
	}
}

void afisare(NOD *h)
{
	NOD *p;
	if (h==0)
	{
		printf("lista e vida");
	}else
	{
		p=h;
		while(p!=0)
		{
			printf(" %d ",p->valoare);
			p=p->next;
		}
	}

}


//void pretty_print(int *v,int n,int k,int nivel)
//{
//	if(k>n) return;
//	pretty_print(v,n,2*k+1,nivel+1);
//		for(int i=1;i<=nivel;i++)
//		{
//			printf("   ");
//		}
//		printf("%d\n",v[k-1]);
//	pretty_print(v,n,2*k,nivel+1);
//}

void heapify(int i)
{
	int l,r,small;
	l=2*i;
	r=2*i+1;
	if (l<=h_size && (heap[l]->elem)->valoare<(heap[i]->elem)->valoare){ small=l;
	}else small=i;
	if(r<=h_size && (heap[r]->elem)->valoare<(heap[i]->elem)->valoare){
		small=r;}
	if (small!=i){
		aux=heap[i];
		heap[i]=heap[small];
		heap[small]=aux;
		heapify(small);
	}
}


HEAP *H_pop(int *h_size)
{ 
	HEAP *x1;
	x1=heap[1];
	heap[1]=heap[*h_size];
	heap[*h_size]=x1;
	(*h_size)--;
	printf("\n pop %d   \n",x1->elem->valoare);
	heapify(1);
	for(int t=1;t<=(*h_size);t++)
	{
		printf("\n h[t]= %d ",(heap[t]->elem)->valoare);
	}
	return x1;
}

void H_push(NOD *x,int nr,int *h_size)
{
	(*h_size)++;
	//printf("%d",*h_size);
	heap[*h_size]->elem= x;
	printf("\n elementul introdus= %d",x->valoare);
	heap[*h_size]->idx=nr;
	printf("   index= %d",nr);
	int i=*h_size;
	printf("\n marime %d\n",i);
	while((i>1) && ( (heap[i]->elem)->valoare <(heap[i/2]->elem)->valoare) )
	{aux=heap[i];
	heap[i]=heap[i/2];
	heap[i/2]=aux;
	i=i/2;
	}



}


void Interclasare(NOD **head,int k, NOD **joinedHead,HEAP **h)
{   
	int j=0;
	h_size=0;
	for(int i=0;i<k;i++)
	{

		H_push(head[i],i+1,&h_size);
		//printf("%d",h_size);

	}
	for(int t=1;t<=(h_size);t++)
	{
		printf("\n %d ",(h[t]->elem)->valoare);
	}
	while(h_size>0)
	{
		//printf("%d",h_size);
		HEAP *y=new HEAP;
		y=H_pop(&h_size);
		j=y->idx;

		printf("\n marimea dupa pop  %d\n",h_size);	 
		inserare_lista(joinedHead,&joinedTail,y->elem->valoare);
		printf("lista este: ");
		afisare(*joinedHead);
		head[j-1]=head[j-1]->next;
		if(&head[j-1]!=NULL){
			y->elem=(y->elem)->next;
			if(y->elem!=0){
				H_push(y->elem,j,&h_size);}
			for(int t=1;t<=(h_size);t++)
			{
				printf("\n %d ",(h[t]->elem)->valoare);
			}
		}

	}
}

void generateL(NOD **h,NOD **t, int k, int n)
{
	int key;
	int i,j;
	for(i=0; i < k; i++)
	{
		key=1;
		for(j=0; j < n/k; j++)
		{		
			key=key+rand() % k;
			inserare_lista(&h[i],&t[i],key);
		}
	}
}


void test()
{
	int k=4,n=20;
	generateL(&head[0],&tail[0],k,n);
	for(int i=0;i<k;i++)
	{	
		afisare(head[i]);
		heap[i+1]=new HEAP;
		printf("\n");
	}

	Interclasare(head,k,&joinedHead,&(*heap));
}

int main()
{ 
	test();
	/*int k=4;
	generateL(head,tail,k,20);
	for(int i=0;i<k;i++){
	afisare(head[i]);
	printf("\n");
	}*/
	/*inserare_lista(&head[0],&tail[0],3);
	inserare_lista(&head[0],&tail[0],7);
	afisare(head[0]);*/
	return 0;
}