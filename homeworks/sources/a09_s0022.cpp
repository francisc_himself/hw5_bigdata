#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <math.h>
#include <string.h>

/*
Procedura BFS mai jos presupune ca graficul de intrare G (V, E) este reprezentat cu ajutorul listelor de adiacenta. 
Se acorda mai multe atribute suplimentare pentru fiecare nod �n grafic. 
Am stocat culoarea ,distanta de la sursa de a v�rful u calculat de algoritm, cat si de fiecare nod.
Algoritmul parcurge graful colorand fiecare nod astfel: ALB, apoi GRI si la final NEGRU.

Timpul total de functionare a procedurii BFS este O(E+V). Astfel, BFS se executa �n timp liniar.
*/

#define ALB 0
#define GRI 1
#define NEGRU 2
#define INF 0


typedef struct status{
	int cul;
	int par;
	int dist;
}STATUS;

typedef struct nod{
	int key;
	struct nod *urm;
}NOD;

typedef struct coada{
	int key;
	struct coada *urm;
}COADA;

COADA *queue;
NOD *edgsH[60001], *edgsT[60001];
//statusul unui nod
STATUS sts[60001];

int que[10001];
//Coada si indicele cozii
int k;
int Q[60001];

int lvl=1;
int operations;

//
void print(int lvl,int v,int u);
//

/*
	BFS - cautarea in latime
	 - src : sursa grafului
	 - vertexs : numarul de noduri
*/
void BFS (int src, int vertexs,int flag){
	
	//parcurgerea fiecarui nod, neluand in considerare sursa
	int u;
	for (u=1; u<=vertexs; u++){
		if (u!=src){
			//marcarea nodurilor in ALB
			sts[u].cul=ALB;
			sts[u].dist=INF;
			sts[u].par=NULL;
		}
	}
	sts[src].cul=GRI;
	sts[src].dist=0;
	sts[src].par=NULL;
	k=1;
	//ENQUEUE(Q,src)
	Q[k]=src;
	operations+=5;
	if(flag!=0)
		printf("(0) %d\n", src);
	while(k!=0){
		lvl++;
      //u=DEQUEUE(Q)
		u=Q[1];

		
		for (int i=1; i<k; i++){
			Q[i]=Q[i+1];
		}
		k--;
		//for each v in C.Adj[u]
		NOD *aux;
		aux=edgsH[u];
		operations++;
		while(aux!=NULL){
			int v=aux->key;
			aux=aux->urm;
			operations+=3;
			if(sts[v].cul==ALB){
				sts[v].cul=GRI;
				sts[v].dist+=1;
				sts[v].par=u;
				
				//ENQUEUE(Q,v)
				k++;
				Q[k]=v;
				if(flag!=0)
					print(lvl,v,u);
				operations+=4;
			}
		}
		sts[u].cul=NEGRU;
		operations+=1;
	}
}
/*
*/
bool ifExists (int x, int y){
	NOD *p;
	p = edgsH[x];
	while(p!=NULL){
		if(y==p->key)
			return true;	
		p=p->urm;
	}

	return false;

}

/*
	generate - generarea random a grafului
	 - vertexs : numarul de varfuri
	 - edges : numarul de edges
*/
void generate(int vertexs,int edges){
	int u,v;
	int i=1;
	//initializarea vectorului de muchii
	for(i=0;i<=vertexs;i++)
		edgsH[i]=edgsT[i]=NULL;
	i=1;
	//generarea efectiva a vectorului de muchii
	while (i<=edges){
		u=rand()%vertexs+1;
		v=rand()%vertexs+1;
		if (u!=v){
				if(edgsH[u]==NULL){
					edgsH[u]=(NOD *)malloc(sizeof(NOD *));
					edgsH[u]->key=v;
					edgsH[u]->urm=NULL;
					edgsT[u]=edgsH[u];

					if(!ifExists(v, u)){
						if(edgsH[v]==NULL){
							edgsH[v]=(NOD *)malloc(sizeof(NOD *));
							edgsH[v]->key=u;
							edgsH[v]->urm=NULL;
							edgsT[v]=edgsH[v];
						}else{
							NOD *p;
							p=(NOD *)malloc(sizeof(NOD *));
							p->key=u;
							p->urm=NULL;

							NOD *qu = edgsH[v];
							while(qu->urm!=NULL){
								qu=qu->urm;	
							}
							qu->urm = p;
							//edgsT[v]->urm=p;
							edgsT[v]=p;
						}
					}
					//i++;
				}else {
					if(!ifExists(u,v)){
						NOD *p;
							p=(NOD *)malloc(sizeof(NOD *));
							p->key=v;
							p->urm=NULL;

							NOD *qu = edgsH[u];
							while(qu->urm!=NULL){
								qu=qu->urm;	
							}
							qu->urm = p;
							edgsT[u]=p;
						if (!ifExists(v, u)){
							if(edgsH[v]!=NULL){
								NOD *p1;
								p1=(NOD *)malloc(sizeof(NOD *));
								p1->key=u;
								p1->urm=NULL;
								NOD *qu1 = edgsH[v];
								while(qu1->urm!=NULL){
									qu1=qu1->urm;	
								}
								qu1->urm = p1;
								edgsT[v]=p1;
							}else{
								edgsH[v]=(NOD *)malloc(sizeof(NOD *));
								edgsH[v]->key=u;
								edgsH[v]->urm=NULL;
								edgsT[v]=edgsH[v];
							}
						}
					}
				}
		}
		i++;
	}
}

void print(int lvl,int v,int u){
	for(int op=0; op<lvl; op++)
					printf(" ");

	printf("(%d) %d \n",u, v);
}

void afis(int vertexs){
	int i;
	NOD *p;
	for(i=1;i<=vertexs;i++)
	{
		printf("%d -> ",i);
		p=edgsH[i];
		while(p!=NULL)
		{
			printf("%d ",p->key);
			p=p->urm;
		}
		printf("\n");
	}
}

void main(){

	int nre, nrv;
	/*
	//TEST
	nrv=5;
	nre=10;
	srand(time(NULL));
	generate(nrv, nre);
	afis(nrv);
	BFS(1, nrv,1);
	*/
	//SIMULATION
	FILE *f;

	f=fopen ("BFS1.csv", "w");
	fprintf(f, "nre, operations\n");
	nrv=100;
	for (nre = 100; nre<=5000; nre=nre+100)
	{
		operations=0;
		generate(nrv, nre);
		BFS(1, nrv,0);
		fprintf(f, "%d, %d\n", nre, operations);
	}
	fclose(f);

	f=fopen ("BFS2.csv", "w");
	fprintf(f, "nrv, operations\n");
	nre=9000;
	for (nrv = 110; nrv<=200; nrv=nrv+10)
	{
		operations=0;
		generate(nrv, nre);
		BFS(1, nrv,0);
		fprintf(f, "%d, %d\n", nrv, operations);
	}
	fclose(f);

}
