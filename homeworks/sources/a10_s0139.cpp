
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include "Profiler.h"


Profiler profiler("DepthFirst");


struct AdjacentNod{
	int neighbour;
	int edgetype;
	struct AdjacentNod *next;
}AdjacentNods;


#define NMIN 110
#define NMAX 211
#define NINC 10
#define EMIN 1000
#define EMAX 5001
#define EINC 100

AdjacentNod **vertices;
AdjacentNod *topological,*go;
int color[NMAX+1];
int parent[NMAX+1];
int d[NMAX+1],f[NMAX+1];
int timed,top;
int edges[EMAX*2+1];
int n,e;


void creareVect(int n, int m){
	int i,j;
	
	int found;
	//printf("creare vect\n");
	AdjacentNod *p,*newn;
		
	topological = (AdjacentNod*)malloc(sizeof(AdjacentNod));
	go = topological;
	go->next = NULL;
	go->neighbour = 0;
	vertices = (AdjacentNod**)malloc(sizeof(AdjacentNod)*(NMAX+1));
	for(i = 0; i < n; i++){
		vertices[i] = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		vertices[i]->neighbour = 0;
		vertices[i]->edgetype = -1;
		vertices[i]->next = NULL;	
	}
	for (i=0; i<m; i+=2){
		
		do{	
			found = 0;
			edges[i]=rand() % (n-1);
			edges[i+1]=rand() % (n-1);
			while(edges[i+1]!=edges[i]){
				edges[i+1]=rand() % (n-1);			
			}
			for(j = 0; j<i; j+=2){
				if ((edges[i]==edges[j] && edges[i+1]==edges[j+1])){
					found = 1;
				}				
			}			
		}while(found == 1);
		p = vertices[edges[i]];
		while(p->next!=NULL){
			p = p->next;
		}
		newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		newn->next = NULL;
		newn->neighbour = 0;
		newn->edgetype = -1;
		p->next = newn;
		p->neighbour = edges[i+1];
		
	}
	//printf("end creare vect\n");
}


void creareTestVect(int n, int m){
	int i,j;
	int edges[NMAX+1] = {
		1,2,
		1,3,
		2,3,
		3,4,
		//4,2,
		1,5,
		5,6,
		6,7,
		//7,6,
		5,8,
		//8,1,
		//9,8,
		//10,8,
		10,9	
	};
	
	AdjacentNod *p,*newn;
		
	topological = (AdjacentNod*)malloc(sizeof(AdjacentNod));
	go = topological;
	go->next = NULL;
	go->neighbour = 0;
	vertices = (AdjacentNod**)malloc(sizeof(AdjacentNod)*(NMAX+1));
	for(i = 0; i < n; i++){
		vertices[i] = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		vertices[i]->neighbour = 0;
		vertices[i]->edgetype = -1;
		vertices[i]->next = NULL;	
	}
	for (i=0; i<m; i+=2){
		
		p = vertices[edges[i]];
		while(p->next!=NULL){
			p = p->next;
		}
		newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		newn->next = NULL;
		newn->neighbour = 0;
		newn->edgetype = -1;
		p->next = newn;
		p->neighbour = edges[i+1];
		
	}
	//printf("end creare vect\n");
}

void DFS_VISIT(int nod){
	AdjacentNod *v,*newn;
	color[nod] = 1;
	timed++;
	d[nod] = timed;
	v = vertices[nod];
	if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n,3);
		}
		else{
			profiler.countOperation("eoperations",e,3);
		}
	while (v->next != NULL){
		if(color[v->neighbour] == 0){
			v->edgetype = 0;   //arbore
			newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
			newn->next = NULL;
			newn->neighbour = 0;
			go->neighbour = v->neighbour;
			go->next = newn;
			go = go->next;
			parent[v->neighbour] = nod;
			DFS_VISIT(v->neighbour);
			if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
		}
		else{
			if(color[v->neighbour] == 1){
				v->edgetype = 1;   //inapoi
				top = 0;
			}
			else{
				if(d[nod]>f[v->neighbour]){
					v->edgetype = 2; //transversala
				}
				else{
					v->edgetype = 3; //inainte;
				}
			}
		}
		v = v->next;
		if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
		
	}
	color[nod] = 2;
	timed++;
	f[nod] = timed;
	if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n,3);
		}
		else{
			profiler.countOperation("eoperations",e,3);
		}
}

void DFS(){
	AdjacentNod *newn;
	int i;
	for(i = 0; i < n; i++){
		color[i]=0;
		parent[i]=-1;
		if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n,2);
		}
		else{
			profiler.countOperation("eoperations",e,2);
		}
	}
	top = 1;
	timed = 0;
	if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
	for(i = 0; i < n; i++){
		if(n == NMIN && e != 5000){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
		if(color[i] == 0){
			newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
			newn->next = NULL;
			newn->neighbour = 0;
			go->neighbour = i;
			go->next = newn;
			go = go->next;			
			DFS_VISIT(i);
		}
	}
}

void printTopologicSort(){
	if(top == 0){
		printf("nu se poate sorta topologic\n");
	}
	else{
		printf("sortarea topologica este:\n");
		go = topological;
		while (go->next!=NULL){
			printf("%d ",go->neighbour);
			go = go->next;
		}
	}
}

int main(){
	int i;
	n = 11;
	e = 9;
	//creareTestVect(n,e*2);
	//DFS();
	//printTopologicSort();
	//getch();

	n = NMIN;
	for (e = EMIN; e < EMAX; e+=EINC){
		printf("e %d",e);
		creareVect(n,2*e);
		for(i = 0; i < n; i++){
			color[i] = 0;
			parent[i] = -1;
		}
				DFS();
		
		
	}	
	
	e = 5000;
	for(n = NMIN; n <= NMAX; n += NINC){
		printf("n %d",n);
		creareVect(n,2*e);
		for(i = 0; i < n; i++){
			color[i] = 0;
			parent[i] = -1;
		}
				DFS();
	}
	profiler.showReport();

}