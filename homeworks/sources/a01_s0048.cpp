/*
urmarind graficele acestor cazuri pentru best case putem observa ca cea mai eficienta sortare este bubble sort care nu realizeaza nici
asignari si nici comparatii daca vectorul este direct ordonoat

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include "Profiler.h"
Profiler profiler ("tema");


int n;
void bubble(int *v,int n)
{ 
 int aux,j,ok,comp=0,assign=0;
 profiler.countOperation("assign_bubble", n,0);
 profiler.countOperation("comp_bub", n,0);
 ok=0;
 while (!ok)
 {
	 ok=1;
	 //profiler.countOperation("assign_bubble", n);
	 for(j=0;j<n-1;j++)
	 {
		 profiler.countOperation("comp_bub", n);
		 if (v[j]>v[j+1])  
		    { 
		      aux=v[j];
			  v[j]=v[j+1];
			  v[j+1]=aux;
			  ok=0;
			  profiler.countOperation("assign_bubble", n,3);
			}   
	 }
 }

}


void selectie(int *v,int n)
{
	int i,j,aux,imin,assign=0,comp=0;
		for(i=0;i<n-1;i++)
		{
			imin=i;
			//profiler.countOperation("assign_selectie", n);
			for (j=i+1;j<n;j++)
			{  
				profiler.countOperation("comp_selectie", n);
				if (v[j]<v[imin])
					imin=j;
				//profiler.countOperation("assign_selectie", n);
				
			}
			aux=v[imin];
			v[imin]=v[i];
			v[i]=aux;
		    profiler.countOperation("assign_selectie", n,3);
				
		}
}

void inserare(int *v,int n)
{
	int i,j,k,x,assign=0,comp=0;
	for(i=1;i<n;i++)
	{
		x=v[i];
		j=0;
		profiler.countOperation("assign_inserare", n,1);
		profiler.countOperation("comp_inserare", n);
	    while(v[j]<x)
		{  
			profiler.countOperation("comp_inserare", n);
			j=j+1;
			//profiler.countOperation("assign_inserare", n);
		}
		for(k=i;k>=j+1;k--)
		{  
	        profiler.countOperation("assign_inserare", n);
			v[k]=v[k-1];
		}
		v[j]=x;
		profiler.countOperation("assign_inserare", n);
	}
} 

void afisare(int*v,int n)
{
  printf("vectorul sortat este:");
  for(int i=0;i<n;i++)
  {
	  printf("%d",v[i]);
  }
}

void worst()
{
	int v[10000],v1[10000],v2[10000],v3[10000];
	for (int n=100;n<2000;n+=100)
	{
		FillRandomArray(v,n,10,50000,false,2);
		memcpy(v1,v,n*sizeof(int));
		selectie(v1,n);

		memcpy(v2,v,n*sizeof(int));
		bubble(v2,n);

		memcpy(v3,v,n*sizeof(int));
		inserare(v3,n);

	}
	profiler.createGroup("comparari worst","comp_bub","comp_selectie","comp_inserare");
	profiler.createGroup("assignari worst","assign_bubble","assign_selectie","assign_inserare");
	profiler.addSeries("Sortare_BubbleSort_worst","assign_bubble","comp_bub");
	profiler.addSeries("Sortare_Selectie_worst","assign_selectie","comp_selectie");
    profiler.addSeries("Sortare_Inserare_worst","assign_inserare","comp_inserare");
    profiler.createGroup("Final_assign_comp_worst","Sortare_BubbleSort_worst","Sortare_Selectie_worst","Sortare_Inserare_worst");
	profiler.showReport();

}

void best()
{
	
	int v[10000],v1[10000],v2[10000],v3[10000];
	for (n=100;n<2000;n+=100)
	{
		FillRandomArray(v,n,10,50000,true,1);
		/*for(int i=0;i<n;i++)
		{
		printf(" %d ",v[i]);
		}*/

		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);

		memcpy(v2,v,n*sizeof(int));
		selectie(v2,n);

		memcpy(v3,v,n*sizeof(int));
		inserare(v3,n);
	}

	profiler.createGroup("comparari best","comp_bub","comp_selectie","comp_inserare");
	profiler.createGroup("assignari best","assign_bubble","assign_selectie","assign_inserare");
	profiler.addSeries("Sortare_BubbleSort_best","assign_bubble","comp_bub");
	profiler.addSeries("Sortare_Selectie_best","assign_selectie","comp_selectie");
	profiler.addSeries("Sortare_Inserare_best","assign_inserare","comp_inserare");
    profiler.createGroup("Final_assign_comp_best","Sortare_BubbleSort_best","Sortare_Selectie_best","Sortare_Inserare_best");
	profiler.showReport();
}

void average()
{
	int i;
	int v[10000],v1[10000],v2[10000],v3[10000];
	for(i=1;i<=5;i++)
	{
		printf("pasul %d\n",i);
	    for (n=100;n<2000;n+=100)
	    {
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
		memcpy(v2,v,n*sizeof(int));
		memcpy(v3,v,n*sizeof(int));
		bubble(v1,n);
		selectie(v2,n);
		inserare(v3,n);
	    }
	}
	profiler.createGroup("comp","comp_bub","comp_selectie","comp_inserare");
	profiler.createGroup("assign","assign_bubble","assign_selectie","assign_inserare");
	profiler.addSeries("Sortare_BubbleSort","assign_bubble","comp_bub");
	profiler.addSeries("Sortare_Selectie","assign_selectie","comp_selectie");
	profiler.addSeries("Sortare_Inserare","assign_inserare","comp_inserare");
    profiler.createGroup("Final_assign_comp","Sortare_BubbleSort","Sortare_Selectie","Sortare_Inserare");
	profiler.showReport();
}
void main()
{ 
    int test[]={4,9,1,8,3};
    int test2[5],test3[5];
	//int v[10000],v1[10000],v2[10000],v3[10000];

	memcpy(test2,test,5*sizeof(int));
	memcpy(test3,test,5*sizeof(int));
	bubble(test,5);
	afisare(test,5);
	inserare(test2,5);
    afisare(test2,5);
	selectie(test3,5);
	afisare(test3,5);

	//best();
    worst();
	// average();
		_getch();

}