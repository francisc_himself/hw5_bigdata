#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

int n,dim;
int hAt=0,hComp=0,qAt=0,qComp=0;

//print vector
void print(int a[],int k) {
 
	for (int i = 1; i <= k; i++) 
	{
		
	printf("%d ",a[i]);
	
	 }
	printf("\n");
	
}

//build heap bottom up
int left(int i) {
  return (2 * i) ;
}

int right(int i) {
  return (2 * i) + 1;
}

void heapify(int a[], int i) {
  
  int l = left(i), great;
  int r = right(i);
  hComp++;
  if (  (l <= n) && (a[l] > a[i])) {
    great = l;
  }
  else {
    great = i;
  }
  hComp++;
  if ( (r <= n) && (a[r] > a[great])) {
    great = r;
  }
  if (great != i) {
    int temp = a[i];
    a[i] = a[great];
    a[great] = temp;
	hAt=hAt+3;
    heapify(a, great);
  }
}

void BuildHeapBU(int a[]){
	int i;
	for (i=n/2; i > 0;i--) 
	{
		heapify(a, i); 
		
	}
}



 void heapsort(int a[],int b[])
  {int k,i;
	hAt=0,hComp=0;
	dim=n;
	while(n!=0){
      //iau elem de pe prima poz il pun in b
		b[n]=a[1];			hAt++;
	//interschimb primu din a cu ultimul
		 	k=a[1];			hAt=hAt+3;
			a[1]=a[n];
			a[n]=k;	
	//sterg ultimul elem din a
		n--;
	 //apelez heapify
		heapify(a,1); // acum am iar struct de heap 
	}
	
	n=dim;	 
 }

 //quick sort
 int partition(int a[],int p, int r){
	 int pivot,i,j,aux;
	 pivot=a[r];
	 i=p-1;
	 for(j=p;j<=r-1;j++){
		 qComp++;
		 if(a[j]<=pivot)
			{ i=i+1;
			 qAt=qAt+3;
			 aux=a[i];
			 a[i]=a[j];
			 a[j]=aux;}
	 }
	 qAt=qAt+3;
	 aux=a[i+1];
	 a[i+1]=a[r];
	 a[r]=aux;

	 return (i+1);
 }

 void quicksort(int a[],int l, int r){
	 int p;
	 if (l<r){
		p=partition(a,l,r);
		quicksort(a,l,p-1);
		quicksort(a,p+1,r);
	 }
	
	
 }


int main(){

	int a[10001],b[10001],c[10001];
	
	FILE *f;

	f=fopen("fisier.csv","w");
	int auxAt[1],auxComp[1];
	

	fprintf(f,"n,hea_at,hea_comp,heap_at+comp,quick_at,quick_comp,quick_at+comp \n"); //cap tabel

	
	
	//int hAt=0,hComp=0,qAt=0,qComp=0;
	for(n=100;n<=10000;n=n+100){

		auxAt[0]=0;auxAt[1]=0;
	    auxComp[0]=0;auxComp[1]=0;

		printf("%d \n",n);
		for(int m=1;m<=5;m++)
		{	int i;
			srand (time(NULL));
			for(i=1;i<=n;i++){				
				a[i]=rand()%1000;
				c[i]=a[i];
			}	

			BuildHeapBU(c);
			hAt=0;hComp=0;
			heapsort(c,b);
			auxAt[0]=auxAt[0]+ hAt;
			auxComp[0]=auxComp[0]+hComp ;

			qAt=0;qComp=0;
			quicksort(a,1,n);
			auxAt[1]=auxAt[1]+ qAt;
			auxComp[1]=auxComp[1]+ qComp;

		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d \n",n,auxAt[0]/5,auxComp[0]/5,(auxAt[0]+auxComp[0])/5 ,  auxAt[1]/5,auxComp[1]/5,(auxAt[1]+auxComp[1])/5 );
		
	}
	
	/*a[1]=2;a[2]=8;a[3]=7;a[4]=1;a[5]=3;a[6]=5;a[7]=6;a[8]=4;
	n=8;

	printf("quick best \n");
	print(a,n);
	qAt=0;qComp=0;
	quicksort(a,1,n);
	printf("at: %d comp: %d \n",qAt,qComp);
	print(a,n);

	printf(" \n");
	a[1]=1;a[2]=2;a[3]=3;a[4]=4;a[5]=5;a[6]=6;a[7]=7;a[8]=8;
	printf("quick worst \n");
	qAt=0;qComp=0;
	print(a,n);
	quicksort(a,1,n);
	printf("at: %d comp: %d \n",qAt,qComp);
	print(a,n);*/
	
	getch();

	
	return 0;
}