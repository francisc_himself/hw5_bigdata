﻿#include <iostream>
#include <conio.h>
#include "Profiler.h"
using namespace std;

int a[100000];
Profiler profiler("demo");


int n, size;


void swap(int &x,int &y)
{
    int temp=x;
    x=y;
    y=temp;
}

int j=0;
void heapify(int x)
{
    int left=(2*x);
    int right=(2*x)+1;
    int large;
	profiler.countOperation("CompareBU",n);
	j++;
    if(a[left]>a[x] && left <= n)
    {
        large=left;
    }
    else
    {
        large=x;
    }
	profiler.countOperation("CompareBU",n);
    if(a[right]>a[large] && right <= n)
    {
        large=right;
    }
    if(x!=large)
    {
        swap(a[x],a[large]);
		profiler.countOperation("AssignBU",n,3);
        heapify(large);
    }
}

void bottom_up(){
	for(int i = n/2-1; i>=0;i--)
		heapify(i);
}










void  HeapIncreaseKey(int  i, int  key)
{
	a[i]=key;
	
	profiler.countOperation("AssignTD",n);
	profiler.countOperation("CompareTD",n);
	while(a[i/2]<a[i]&& i>0)
	{
		swap(a[i],a[i/2]);
		profiler.countOperation("AssignTD",n,3);
		profiler.countOperation("CompareTD",n);
		i=i/2;
	}
	
}




void insert(int  key)
{
	size++;
	a[size]=-100000;
	profiler.countOperation("AssignTD",n,2);
	HeapIncreaseKey(size,key);
}

void top_down()
{
	int i;
	size=0;
	for(i=1;i<n;i++)
		insert(a[i]);
}

void print(int i)
{
	int ok=1;
	double count=0;

	if(i<n)
	{
		print(2*i+1);
		do
		{
			if(i>=pow(2,count) && i<pow(2,count+1))
			{
				ok=0;
			}
			count++;
		}while(ok!=0);
		for(int k= 1;k<count;k++)
			cout<<"     ";
		cout<<a[i]<<" \n";
		print(2*i);

	}
}

void test()
{
	cout<<"bottom up"<<endl;
	n=10;
	for(int i=0;i<n;i++)
	{
		a[i]=i+6*i%11;
		cout<<a[i]<<" ";
	}
	cout<<endl;
	top_down();
	//bottom_up();
	for(int i=0;i<n;i++)
		cout<<a[i]<<" ";
	cout<<endl;
	cout<<endl;
	
	print(1);
	cout<<endl;
	getch();
}


void test2()
{
	cout<<"top down:"<<endl;
	n=10;
	for(int i=0;i<n;i++)
	{
		a[i]=i+6*i%11;
		cout<<a[i]<<" ";
	}
	cout<<endl;
	//top_down();
	bottom_up();
	for(int i=0;i<n;i++)
		cout<<a[i]<<" ";
	cout<<endl;
	cout<<endl;
	print(1);
	getch();
}
void main()
{
	
	
	test();
	test2();
	/*
	for (n=0;n<=10000;n=n+500)
	{
		cout<<n<<endl;
		FillRandomArray(a, n,0,10000);
		top_down();
		FillRandomArray(a, n,0,10000);
		bottom_up();
	}

	profiler.createGroup("Compare","CompareTD","CompareBU");
	profiler.createGroup("Assign","AssignTD","AssignBU");

	profiler.addSeries("BU","CompareBU","AssignBU");
	profiler.addSeries("TD","CompareTD","AssignTD");

	profiler.createGroup("C+A","BU","TD");

	profiler.showReport();
	*/
}