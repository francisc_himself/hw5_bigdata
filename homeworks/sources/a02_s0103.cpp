/*
author:***ANONIM***�si ***ANONIM***
  group:***GROUP_NUMBER***
  date:20.03.2013
  program description : this program implements two methods of building heaps, namely the bottom-up and the top-down strategies

  For the bottom-up strategie the main idea is to start the procedure from the half of the array containing the heap and order the 
  nodes in such a way to get a heap at the end. The heapify() function has this role. It looks for consecutive nodes on a branch and if the 
  node from the lower level is greater it swaps with its parent. The algorithm stops when the heap property is true for the whole array.

  The top-down stragie is based on building the heap from the beginning. The first element is considered to be a heap already, so from the second 
  element to the end the new keys are inserted is such a way that the heap property remains. The idea is to increase the size of the heap and 
  insert the new key in its right position. By going from the leaves to the root we check the heap property and if there is a parent smaller
  than its children swap them.

  average case : in this case both strategies have linear running time.

  worst case : The worst case is when the array is sorted in ascending order, so the last element(max) will be the root.
  The running time for the bottom-up strategie is O(n) because the heapify has O(n) running time and for the top-down strategie is O(nlgn).

  There is difference between the result of the two strategies, because the first one takes half of the array, thus the order of the elements count
  when sinking the root on one of the branches. The second strategie builds the heap from the leaves, so the order counts in a different way. 
*/
#include <stdio.h>
#include "Profiler.h"
Profiler p("lab3");

int bu = 0;
int td = 0;

int parent(int i)
{
	return (int)i/2;
}
int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i+1);
}

void heapify(int heap_size,int i,int A[])
{
	int l = left(i);
	int r = right(i);
	int largest = 0;
	if (l<=heap_size && A[l]>A[i]) largest = l;
		else largest = i;
    if (r<=heap_size && A[r]>A[largest]) largest = r;
	bu=bu+2;
	if (largest != i) 
		{
			int aux = A[i];
			A[i]=A[largest];
			A[largest]=aux;
			bu=bu+3;
			heapify(heap_size,largest,A);
		}
	
}

void build_heap_BU(int n,int A[])
{
	for (int k=n/2;k>0;k--)
	heapify(n,k,A);
}

void heap_increase_key(int i,int key,int A[])
{
	td++;
	if (key<A[i]) printf("New key smaller than current key!");
	{A[i] = key;td++;}
	while (i>1 && A[parent(i)]<A[i])
	{
	td=td+4;
	int aux = A[i];
	A[i] = A[parent(i)];
	A[parent(i)] = aux;
	i = parent(i);
	}
	td++;

}

void heap_insert(int key,int heap_size,int A[])
{
	heap_size++;
	heap_increase_key(heap_size,key,A);
}

void build_heap_TD(int n,int A[])
{
	for (int i=2;i<=n;i++)
		heap_insert(A[i],i-1,A);
}

void print_heap(int v[],int n,int k,int level)
{
	if(right(k)<=n)print_heap(v,n,right(k),level+1);
	for(int i=0;i<=level;i++)printf("    ");
	printf("  %d  \n",v[k]);
	if(left(k)<=n)print_heap(v,n,left(k),level+1);
}

int main()
{
	int i,n;
	int A[10001];
	int B[10001];
	int h[]={0,1,2,3,4,7,8,9,10,14,16,18,19,20,21,22,23,24};

	build_heap_BU(17,h);
	//build_heap_TD(17,h);
	print_heap(h,17,1,1);
	/*avg case
	freopen("top-down.csv","w",stdout);
	for(n=100;n<=10000;n+=100)
	{
		td=0;
	for(int k=0;k<5;k++)
	{
		FillRandomArray(A,n);
		//build_heap_BU(n,A);
		build_heap_TD(n,A);
	}
	printf("%d,%d\n",n,td/5);
	}
	*/
	/*worst case
	freopen("top-down-worst.csv","w",stdout);
	for(n=100;n<=10000;n+=100)
	{
		td=0;
	for (i=0;i<=n;i++)
		A[i]=i;
	//build_heap_BU(n,A);
	build_heap_TD(n,A);
	printf("%d,%d\n",n,td);
	}*/

}