﻿/*
In codul ce urmeaza am implementat cei mai uzuali alg. de sortare:
bubble sort,insertion sort si selection sort, pentru a studia si observa
care dintre acesti algoritmi este cel mai eficient in functie de vetorul 
care trebuie sortat.
Prima data am generat pt fiecare dintre cei algoritmi vectori cu lungimi cumprinse
intre 100 si 5000 si cu valori aleatoare.In total 50 de vectori pt fiecare sortare, lungimea crescand 
cu 100 pe vector. Pentru a intelege si observa mai bine am hotarat sa compar pe rand 
rezultatele obtinute pt lungimile(100 si 1000,1100 si 2000;...4100 si 5000);
La toate pragurile pe care am propsu sa le urmaresc si compar, selection sort este mai eficient,
eficienta sa fii mai evidenta fata de celelate sortari pe masura ce lungimea vectorului crestea.
In final, pe baza datelor obtinute, am obs ca in acest caz , cand vectorul nue ste sortat, cel mai 
rapid este selection sort urmat de insertion sort , si mult in urma acesto doi algoritmi cu o eficienta 
de aproximativ 4 ori mai mica pt vectorul cu 5000 de elemente este bubble sort


In a doua etapa de comparare a eficientei am generat 5 vectori cu lungime de 1000,2000,..., 5000 dar de 
data aceasta sortati crescator pentru a vedea si in acest caz particular eficienta algoritmilor.
In acest caz bubble sort executa un nr de comparari egal cu dimensiunea vectorului in cauza si nici o asignare,
pe cand insertion sort si selection sort fac o multime de operatii, ordinul acestora ajungand la 25 mil respectiv 13 mil(cifre aproximative)
Atunci cand sirul este unul crescator bubble sort este cel mai indicat.


In a treia etapa am generat din nou 5 vectori dar de aceasta data fiind sortati descrescatori.Spre deosebire de cazul precedent 
unde bubble sort a fost cel mai rapid, in acest caz daca folosim bubble sort pe un vector sortat crescator ajungem la un numar 
de aproximativ 62 mil de operatii, aproximativ de 5 ori mai multe ca si la inserion sort.
La al treilea caz inserion sort se descurca cel mai bine urmat de selection sort si din nou mult in urma bubble sort.



Cu toate ca aceste sortari indica bubble sort ca si cel mai neeficient alg de sortare , in cele 
mai multe cazuri se foloseste deoarece esti simplu de scris si de inteles si probail primul
algoritm de sortare invatat de orice ïnformatician"
Dar dupa aceste verificari de eficienta eu sunt de parere ca selection sort este cel mai eficient din
cei trei algoritmi implementati cu toate ca nu a fost primul la vectorul crescator si descrescator


***ANONIM*** ***ANONIM*** ***ANONIM***, UTCN-CTI ,gr:***GROUP_NUMBER***;
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

/*variabilele globale as si co sunt folosite pentru
numararea operatiilor de comparare si asignare*/
int as,co;

/*implementare Bubble sort*/
void bubble(int *v,int n)
{
	int ok,i,j,aux;
	ok=1;
	as=0;
	co=0;
	while(ok==1)
	{
		ok=0;
		for(i=0;i<n-1;i++)
		{
				co++;
				if(v[i]>v[i+1]) 
				{ 
					aux=v[i];
					v[i]=v[i+1];
					v[i+1]=aux;
					as=as+3;
					ok=1;
				}
		}
   }
}

/*implementare sortare prin insertie*/
void insertion(int *v,int n)
{
	int i,x,j,k;
	as=0;
	co=0;
	for(i=1;i<n;i++)
	{
		x=v[i];
		j=0;
		as=as+2;
		co++;
		while(v[j]<x)
		{
			j=j+1;
			as++;
			co++;
		}
		for(k=i;k>j;k--)
		{
			v[k]=v[k-1];
			as++;
		}
		v[j]=x;
		as++;
	}
}

/*implementare sortare prin selectie*/
void selection(int *v,int n)
{	
	int i,imin,aux,j;
	as=0;
	co=0;
	for(i=0;i<n-1;i++)
	{
		imin=i;
		as++;
		for(j=i+1;j<n;j++)
		{   
			co++;
			if(v[j]<v[imin]) 
			{
				imin=j;
				as++;
			}
		}
		aux=v[imin];
		v[imin]=v[i];
		v[i]=aux;
		as=as+3;
	}
}
/*functie de afisare folosita pentru testarea partiala 
a algoritmilor de sortare;afisarea se face in consola*/
void afisare(int *v,int n)
{
	
	int i;
	
		
	for(i=0;i<n;i++)
		printf("%d ,",v[i]);
    printf("\n");	
}
/*functie de generare vectori cu valori aleeatoare
folosind functia rand() din biblioteca stdlib.h*/
void generare_normal(int *v, int n)
{
	int i;
	for(i=0; i<n; i++)
	{
		v[i]=rand();
	}
}
/*functii creare vecotor crescator si descrescator folosind
contorul pentru a initializa sirul respectiv diferenta
dintre dimensiune si contor pentru sirul descrescator*/
void generare_cresc(int *v,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v[i]=i;
	}
}
void generare_desc(int *v,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v[i]=n-i;
	}
}
/*functia main in care sunt apelate functiile de mai sus,
si se realizeaza creearea fisierului csv*/
int main()
{   FILE *f=fopen("afisare.csv","w");
	//int test[]={4,9,1,8,3,-6,100,-7,45,124,654,-6,14,-154};      vectorii au fost folositi in testarea algoritmilor
	//int test1[]={4,9,1,8,3,-6,100,-7,45,124,654,-6,14,-154};
	//int test2[]={4,9,1,8,3,-6,100,-7,45,124,654,-6,14,-154};
	int v[5000];  //vectorul care va fi creeat si sortat 
	
	
	/*fprintf este folosit pentru scrierea in fisier si pentru aranjarea
	in valorilor intr-o ordine dorita pentru o cat mai buna vizualizare 
	si examinare a datelor obtinute din sortare
	Prima etapa este aceea de a genera vectori cu valori aleeatoare,
	sortarea acestora folosind cei trei algoritmi prezentati mai sus,
	si afisarea nr. de elemente,operatiilor de asignare,comparare
	precum si suma celor doua in afisare.csv*/
	
	//bubble sort
	fprintf(f,"bubble sort(vector aleator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=100;i<=5000;i=i+100)
	{
	generare_normal(v,i);
	bubble(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");
	
	//insertion sort
	fprintf(f,"insertion sort(vector aleator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=100;i<=5000;i=i+100)
	{
	generare_normal(v,i);
	insertion(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");
	
	//selection sort
	fprintf(f,"selection sort(vector aleator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=100;i<=5000;i=i+100)
	{
	generare_normal(v,i);
	selection(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");
	
	/*a doua etapa consta in generarea unor vectori sortati
	crescator iar dupa descrescatori.se respecta ordinea instructiunilor
	de la prima etapa doar ca difera vectori sortati(crescator ,descrescator);
	fiecare algoritm este executat avand ca si parametrii un sir sortat crescator
	si lungimea sirului urmat de repetarea executiei lui doar cu sir descrescator 
	ca si parametu*/
	//bubble sort sir crescator
	fprintf(f,"bubble sort(sir sortat crescator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=1000;i<=5000;i=i+1000)
	{
	generare_cresc(v,i);
	bubble(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");
	
	//bubble sort sir descrescator
	fprintf(f,"bubble sort(sir sortat descrescator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=1000;i<=5000;i=i+1000)
	{
	generare_desc(v,i);
	bubble(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");
	
	//insertion sort sir crescator
	fprintf(f,"insertion sort(sir sortat crescator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=1000;i<=5000;i=i+1000)
	{
	generare_cresc(v,i);
	insertion(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");

	//insertion sort sir descrescator
	fprintf(f,"insertion sort(sir sortat descrescator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=1000;i<=5000;i=i+1000)
	{
	generare_desc(v,i);
	insertion(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");

	//selection sort sir crescator
	fprintf(f,"selection sort(sir sortat crescator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=1000;i<=5000;i=i+1000)
	{
	generare_cresc(v,i);
	selection(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");

	//selection sort sir descrescator
	fprintf(f,"selection sort(sir sortat descrescator),\n");
	fprintf(f,"nr. elemente,");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie,\n");
	for(int i=1000;i<=5000;i=i+1000)
	{
	generare_desc(v,i);
	selection(v,i);
	//afisare(v,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",as);
	fprintf(f,"%d,",co);
	fprintf(f,"%d,",as+co);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");
	fprintf(f,"\n");

	//sfarsitul operatiilor de sortare si inchiderea fisierului
	fclose(f);
	return 0;
}
