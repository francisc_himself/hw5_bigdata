//***ANONIM*** ***ANONIM*** Melania
//Grupa ***GROUP_NUMBER***
//DFS:

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler profiler ("tema10");

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define V_MAX 10000
#define E_MAX 60000
#define infinit 100000

typedef struct {
	int color;
	int parent;
	int d;//discovery
	int f;//finish
}TREE_NODE;

typedef struct _NODE{
	int key;
	struct _NODE *next;
}NODE;

NODE * adj[V_MAX];
int edges[E_MAX];
int tail,head,length=V_MAX;
int n,m,u,v;
TREE_NODE g[V_MAX];
int s,op,timp;


void generate(int n,int m)
{
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE *nod=new NODE;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;
	}
}

void DFS_VISIT(TREE_NODE g[],int u)
{
	timp++; //nodul u a fost descoperit
	g[u].d=timp;
	g[u].color=GRAY;
	while(adj[u]!=NULL)//for(v=0;v<n;v++)//each v din Adj[u] //explore edge(u,v)
	{
		v=adj[u]->key;
		//if(g[v].parent==u)
		//{printf("tree edges: \n");
			if(g[v].color==WHITE)
			{
				g[v].parent=u;
				DFS_VISIT(g,v);
			}
		//}
	//	else if((g[u].d>g[v].d)&&(g[u].f<g[v].f))
			//printf("back edges\n");
		//else if((g[u].d<g[v].d)&&(g[u].f>g[v].f))
		//	printf("forward edges\n");
	//	else printf("cross edges\n");
		adj[u]=adj[u]->next;
	}
	g[u].color=BLACK; //u este negru,terminat
	timp++;
	g[u].f=timp;
}


void DFS()
{
	for( u=0;u<n;u++)//each vertex u
	{
		g[u].color=WHITE;
		g[u].parent=-1;
	}
	timp=0;
	for( u=0;u<n;u++)// each vertex u
	{
		if(g[u].color==WHITE)
			DFS_VISIT(g,u);
	}
}

void main()
{
	n=7;
	m=9;
	generate(n,m);
	DFS();
	//afisare graf DFS:
	for(int i=0;i<n;i++)
	{
		printf("%d %d %d %d \n", g[i].color,g[i].d,g[i].f,g[i].parent);
	}
}