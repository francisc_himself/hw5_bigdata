#include<iostream>
#include<conio.h>
#include<fstream>
#include <time.h>

int m;
int op;
//probing == 1 if linear probing; probing == 2 if quadric probing
int hash_insert(int T[],int k,int probing)
{
	int i = 0;
	int j;
	while (i <= m)
	{
		if (probing == 1)
		{
			j = (k + i) % m;
		}
		else 
			j = (k + i * i) % m;
			//j = (k + i) % m + 1;
		if (T[j] == 0)
		{
			T[j] = k;
			return j;
		}
		else i = i + 1;

	}

	return -1;
}

int hash_search(int T[], int k,int probing)
{
	int i = 0;
	int j;
	if (probing == 1)
		{
			j = (k + i) % m;
		}
	else 
		j = (k + i * i) % m;

	while ((T[j] != 0) && (i != m))
	{
		op += 1;

		if (T[j] == k)
		{
			op += 1;
			return j;
		}
		i = i + 1;
		if (probing == 1)
		{
			j = (k + i) % m;
		}
		else 
			j = (k + i * i) % m;

	}
	op += 1;

	return -1;
	
}

int main()
{
	m = 5;
	int t3[5];
	int t1[9973];
	int t2[9973];
	int searched1[9973];
	int searched2[9973];
	int sum1, sum2;
	FILE *f;
	f = fopen("hashtable.txt","w");
	for (int i = 0; i< 5; i++)
	{
		t1[i]=t2[i]=0;
	}

	t3[0] = 7;
	t3[1] = 13;
	t3[2] = 9;
	t3[3] = 2;
	t3[4] = 3;

	for (int i = 0; i < m; i++)
	{
		//int k = rand() % 113;
		hash_insert(t1,t3[i],1);
		hash_insert(t2,t3[i],2);
	}

	printf("Linear Probing\n");
	for (int i = 0; i < m; i++)
	{
		printf("%d ",t1[i]);
	}
	printf("\nQuadric Probing\n");
	for (int i = 0; i < m; i++)
	{
		printf("%d ",t2[i]);
	}

	printf("Serach results\n");
	printf("search elem 13\n");
	printf("%d %d", hash_search(t1,13,1), hash_search(t1,13,2));

	getch();

	m = 9973;

	float a[5] = {0.8,0.85,0.9,0.95,0.99};
	//srand(time(NULL));
	//filling factor
	for (int l = 0; l < 5; l++)
	{
		sum1 = 0;
		sum2 = 0;
		int n = a[l] * m;
		srand(rand() % m);
		printf("%d ", n);
		//test 5 times;
		for (int k = 0; k < 5; k++)
		{
			//srand(time(NULL));
			for (int i = 0; i< n; i++)
			{
				t1[i]=t2[i]=0;
			}
			srand(rand() % m);
			for (int i = 0; i < n; i++)
			{
				int elem = rand() % n + 1;
				hash_insert(t1,elem,1);
				hash_insert(t2,elem,2);

			}

			for (int i = 0; i < 1500; i++)
			{
				int pos = rand() % n;
				searched1[i] = t1[pos];
				//searched2[i] = t2[pos];
			}
			for (int i = 1500; i < 3000; i++)
			{
				int nr = rand() % n + n;
				searched1[i] = nr;
				//searched2[i] = nr;
			}

			for (int i = 0; i < 3000; i++)
			{
				op = 0;
				hash_search(t1,searched1[i],1);
				sum1 += op;
				op = 0;
				hash_search(t2,searched1[i],2);
				sum2 += op;
			}
		}
		fprintf(f,"%g %d %d \n", a[l],sum1 / 5, sum2 / 5);

	}
	fclose(f);


}