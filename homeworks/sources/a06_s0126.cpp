#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include "Profiler.h"

using namespace std;
Profiler profiler;

int counter;
int printlist[10],cnt=0;

typedef struct nod
{
	int key;
	int rank;
	nod *left, *right, *parent;
}NODE;
NODE* root;

void pretty_print(NODE *p,int level)
{

  if (p!=NULL){
		 pretty_print(p->left,level+1);
		 for( int i=0;i<=level;i++) 
			printf("   ");
		 printf("%d\n",p->key);
		 pretty_print(p->right,level+1);
	       }
}

void printList(int printlist[],int cnt){
   printf("\nDeleted keys: \n");
    for ( int i = 0 ; i < cnt ;i++)
        printf("%d ",printlist[i]);
}

NODE* OS_SELECT(NODE *x, int i) //returns a pointer to the node containing the i-th smallest key in the subtree rooted at x
{
	int r=1;
	if (x->left != NULL)							//search the node that must be eliminated
		r = x->left->rank+1;					//rank is the number of nodes that come
													//ahead of x in an inorder tree traversal
	else
		r = 1;
	if (r == i)
		return x;
    else
	{
		if (i < r)
		{
			return OS_SELECT(x->left,i);
		}
        else
		{
			return OS_SELECT(x->right,i-r);
		}
    }
}

NODE* GENERATE(int x, int dim, NODE *left, NODE *right)
{
	NODE *q;
	q= new NODE;
	q->key=x;					//q is the new node and takes the value x
	q->left=left;				//create the left son
	q->right=right;				//create the right son
	q->rank=dim;			//add the rank
	profiler.countOperation("assign",counter,4);
	if (left!=NULL)
	{
		left->parent = q;		//make the parent->son relationship
	}
	if (right!=NULL)
	{
		right->parent=q;
	}
	q->parent=NULL;			//q is the root
	profiler.countOperation("assign",counter);
	return q;
}

NODE* CONSTRUCT(int left, int right)
{
	NODE *ds,*d;
	if (left > right)
		return NULL;
	if (left == right)
		return GENERATE(left, 1, NULL, NULL);
	else
	{
		int mij =(left + right)/2;
		ds = CONSTRUCT(left, mij-1);
		d  = CONSTRUCT(mij+1, right);
		return GENERATE(mij, right-left+1,ds, d);
	}
}

NODE* MINIM(NODE* x)
{
	while(x->left!=NULL)
	{
		x = x->left;
		profiler.countOperation("assign",counter);
		profiler.countOperation("comp",counter);
	}
	profiler.countOperation("comp",counter);
	return x;						//return the bottom left node
}

NODE* SUCCESOR(NODE* x)
{
	NODE *y;
	if(x->right!=NULL)						//check if the right subtree is not empty
	{
		profiler.countOperation("comp",counter);
		return MINIM(x->right);				//look for child in the right subtree
	}
	profiler.countOperation("assign",counter);
	y = x->parent;							//y takes the value of the parent node if the search from above doesnt execute
											
	while (y != NULL && x == y->right)
	{
		profiler.countOperation("comp",counter,2);						//x becomes y
		x = y;								//go to the next node by going up
		y = y->parent;
		profiler.countOperation("assign",counter,2);
	}
	return y;								//return the successor
}

NODE* DELETENODE(NODE* z)
{
	NODE *y;
	NODE *x;

	if ((z->left == NULL) || z->right == NULL)		//check if the node is a leaf
	{
		profiler.countOperation("assign",counter);
		y=z;
	}
	else
		y = SUCCESOR(z);							//if not, check for its successor
	profiler.countOperation("comp",counter);
	if (y->left != NULL)							
	{
		profiler.countOperation("assign",counter);
		x = y->left;
	}
	else
	{
		profiler.countOperation("assign",counter);	
		x = y->right;
	}
	profiler.countOperation("assign",counter);
	if (x != NULL)
	{
		profiler.countOperation("assign",counter);
		x->parent = y->parent;
	}
	profiler.countOperation("comp",counter);
	if (y->parent == NULL)
		root = x;
	else
		if (y == y->parent->left)
		{
			profiler.countOperation("assign",counter);
			y->parent->left = x;
		}
		else
		{
			profiler.countOperation("assign",counter);
			y->parent->right = x;
		}
	profiler.countOperation("comp",counter,2);
	if (y != z)
	{
		profiler.countOperation("assign",counter);
		z->key = y->key;
	}
	return y;
}

void NEW_DIMENSION(NODE *p)
{
	while (p != NULL)
	{
		profiler.countOperation("assign",counter);
		p->rank--;						//for every node decrement the rank by 1 starting with the node on the new position or on the deleted position	
		p=p->parent;
		profiler.countOperation("assign",counter);
	}
}

void JOSEPHUS(int n, int m)
{
	NODE *y,*z;
	int aux = n;
	int mij = m;

	root = CONSTRUCT(1, n);			//build the tree for the rank
	for (int i = 1; i < aux; i++)
	{
		y = OS_SELECT(root, m);		//select the node that needs to be deleted
		z = DELETENODE(y);			//delete the node
		NEW_DIMENSION(z);			//reconstruct the rank of the node starting with the one that was deleted
		delete(z);					//free the memory 
		n--;						//decrement the length of the tree
		if (n > 0)					//generate the next position that needs to be deleted
			m = (m - 1 + mij) % n;
		if (m == 0)
			m = n;
	}
}

void JOSEPHUSprint(int n, int m)
{
    NODE *y,*z;
    int aux = n+1;
    int mij = m;

    root = CONSTRUCT(1, n);		
    printf("\The tree: \n");
    pretty_print(root,0);
    for (int i = 1; i < aux; i++)	
                                    
    {
        y = OS_SELECT(root, m);	    
        printlist[cnt] = y->key;
        cnt++;
        printList(printlist,cnt);
        z = DELETENODE(y);			
        printf("\nThe tree: \n");
        pretty_print(root,0);
        NEW_DIMENSION(z);			
                                   
        delete(z);					
        n--;						
        if (n > 0)					
            m = (m - 1 + mij) % n;
        if (m == 0)
            m = n;
    }

}

int main()
{
	JOSEPHUSprint(9,4);
	_getch();
	//for(int i=100; i<=10000; i+=100)
	//{
	//	counter = i;
	//	JOSEPHUS(i,i/2);
	//}
	//profiler.addSeries("Total Operations","assign","comp");
	//profiler.showReport();
}
