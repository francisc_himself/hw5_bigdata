/* 
When looking at the graph comparing the total number of operations, we observe that the bottom-up approach has fewer total operations with O(n), while the top-down
approach executes more operations with O(nlgn).
The bottom-up approach is faster than the top-down approach to building a heap, but it only works for a fixed dimension, while top-down can be used with a variable
dimension. The bottom-up build heap is mainly used in sorting algorithms, while the top-down build heap is used when creating priority queues.
*/
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include<stdio.h>
#include<conio.h>

using namespace std;

fstream f1("case1.csv",ios::out);
fstream f2("case2.csv",ios::out);
fstream f3("case3.csv",ios::out);
fstream f4("case4.csv",ios::out);
fstream f5("case5.csv",ios::out);
long cmp,atr;

//Random number generator for filling the array
void randomGenerator(int a[],int n)
{
    for (int i = 1 ; i <=n ; i++)
        a[i] = rand();
}

//Heap reconstructionBU
// heap[] : vector ; i : index , n: vector lengthq
void reconstructHeap(int heap[],int i,int n)
{
    int left = 2*i;
    int right = 2*i +1;
    int max;

        cmp++;
    if ( (left <= n) && ( heap[left] > heap[i] ) )
        max = left;
    else
        max = i;

        cmp++;
    if ( (right <= n ) && (heap[right] > heap[max]) )
        max = right;
		cmp++;
    if ( max != i )
    {
        int aux = heap[i];
        heap[i] = heap[max];
        heap[max] = aux;
        atr+=3;
        reconstructHeap(heap,max,n);
    }
}

//Constructing the Heap Bottom-Up
// heap[]:vector ; n: vector length
void constructHeapBU(int heap[],int n)
{
    for (int i = n/2 ; i>0 ; i--)
        reconstructHeap(heap,i,n);
}

/*Sorting the Heap Bottom - Up
 heap[]:vector ; n: vector length; a[]:the vector that needs sorting
void heapSortBU(int a[],int heap[],int n)
{
    constructHeapBU(heap,n);
    while (n!=0)
    {
        a[n] = heap[1];
        heap[1]=heap[n];
        n--;
        atr+=2;
        reconstructHeap(heap,1,n);
    }
}
*/

//Heap Insertion heap[]: the vector in which we insert, val: the value to be inserted
// dim: heap dimension  - TD
void pushHeap(int heap[],int val,int *dim)
{
	int i;
	(*dim)++;
	i=(*dim);
	cmp++;
	while ((i>1) && ( heap[i/2]< val))
	{
		heap[i] = heap[i/2];
		i = i/2;
		atr++;
		cmp++;
	}
	heap[i] = val;
	atr++;
}

//Constructing Heap Top - Down
//a[]: vector , heap[]: vector in which we insert an element from a[]
// n: vector dimension a[],  dim: heap dimension
void constructHeapTD(int a[],int heap[],int n,int *dim)
{
	for (int i=1;i<=n;i++)
	{
		pushHeap(heap,a[i],dim);
	}
}

/*Heap sort Top - Down
void heapSortTD(int a[],int heap[],int n){
int aux;
int dim=0;
	constructHeapTD(a,heap,n,&dim);
	for (int i = n; i>=2 ; i--) {
		aux = heap[1];
		heap[1] = heap[i];
		heap[i] = aux;
		atr+=3;
		dim--;
		reconstructHeap(heap,1,dim);
	}
}*/

// This method print the vector in a nice way
void pretty_print(int T[], int i, int level)
{
    if(i<=12)
    {

    pretty_print(T, 2*i+1, level+1);
    for(int j=1;j<=level;j++)
    cout<<"          ";
    cout<<T[i]<<endl;
    pretty_print(T,2*i, level+1);
    }

}
int main()
{
    int a[10001],heap[10001],heap1[10001];
    int n, whichFile;
	int dim;
    int nC,nA; // temporary comparisons and assignments
               // after the execution of the first sorting method nC = cmp ; nA = atr;

    for ( whichFile = 1 ; whichFile <=5 ; whichFile++ )
    {

        for ( n = 100 ; n <= 10000; n+=300)
        {
            cmp = 0;
			atr = 0;
			nC = 0;
			nA = 0;
			// generates the random numbers from 1 to n
           randomGenerator(a,n); 

			// the heap gets populated
            for (int i = 1 ; i <= n ; i++)
                heap[i] = heap1[i] = a[i];
			// constructing the Top-Down Heap
            int dim=1;
			constructHeapTD(a,heap,n,&dim);
            nC = cmp;
            nA = atr;
            cmp = 0;
			atr =0;
			
            constructHeapBU(heap1,n);

            /*switch(whichFile)
            {
                case(1):f1<<nC+nA<<","<<cmp+atr<<"\n";break;
                case(2):f2<<nC+nA<<","<<cmp+atr<<"\n";break;
                case(3):f3<<nC+nA<<","<<cmp+atr<<"\n";break;
                case(4):f4<<nC+nA<<","<<cmp+atr<<"\n";break;
                case(5):f5<<nC+nA<<","<<cmp+atr<<"\n";break;
                default:break;
            }*/

        }

    }

	n = 12;
	int d=0;
	randomGenerator(a,n); //we generate the vector for which we do the simulation
	for (int i = 1 ; i <= n ; i++)  //we fill the heap
    heap[i]=heap1[i] = a[i];
	
	
	cout<<"Top Down"<<endl<<endl;
	constructHeapTD(a,heap,n,&d);
	pretty_print(heap,1,0);

	cout<<"Bottom UP"<<endl<<endl;
	constructHeapBU(heap1,n);
	pretty_print(heap1,1,0);

	getch();

    return 0;
}
