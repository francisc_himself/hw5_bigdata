/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
  FA - Assignment 3 - Assignment No.4: Merge k Ordered Lists Efficiently
*/

/**Merge sort = ordered sequences -> linked list
use a min_heap to find the minumum (heap array)!!! - use bu to construct
O(klogk) + n*2(lgk) - analiza trebuie facuta si pe k si pe n
when k is constant => logaritm
when n is constant => exponential
analiza e avg - in fiecare lista sa fie elemente sortate dar random
*/

/**If you increase the numbers of elements from the arrays and the number of lists does not change the result of the total comparisons and asigments is linear. 
If you increase the number of lists and the number of elements stays constant, the result will be logaritmic. */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include <time.h>

typedef struct tip_hash {
                int key;
                struct tip_hash *urm;
} TYPE_HASH;

TYPE_HASH *H[10000];

int n, k; //numeber of elements and of arrays
int i,j, dim;
int A[10001], B[10001]; //sirul considerat ca heap si sirul considerat ca rez
int all_ms, rez_index;


////////////////////////////////////////////////////////////////////
/**The list part: */

/**initialize the k lists*/
void init(int k)
{
int i;
for(i=1;i<=k;i++)
        H[i]=0;
}

/**Construirea hash table*/
void insert(int x, int index)
{
TYPE_HASH *p, *q;
int h;
p=(TYPE_HASH*)malloc(sizeof(TYPE_HASH));
q=(TYPE_HASH*)malloc(sizeof(TYPE_HASH));
h=index;
p->key=x;
if(H[h]==NULL)
    {
        H[h]=p;
        p->urm=0;
    }
else
    {
        q=H[h];
        while(q->urm!=0)
                q=q->urm;
        q->urm=p;
        p->urm=NULL;
    }
}

/**print the k arrays as a hash table*/
void print_hash() // afiseaza tabelul de dispersie
/*exemplu: 0: 7
           1: 1 8 15
           2: 9
           3:  */
{
int i;
TYPE_HASH *p;
for(i=1;i<=k;i++)
    {
    if(H[i]!=0)
        {
            printf("%d: ",i);
            p=H[i];
            while(p!=0)
            {
                printf(" %d ", p->key);
                p=p->urm;
            }
            printf("\n");
        }
    else printf("%d\n", i);
    }
}

/**extract the first value from an array*/
int pop_hash ()  //pop_hash(int i) //din a cata lista sa extraga
{
int min=100000;
int indexmin=1;
int i=1;
while(i<=k)
    {
        if(H[i]!=NULL)
        {
            if(min>=H[i]->key)
            {
              min=H[i]->key;
              indexmin=i;
              all_ms++;
            }
        }
        i++;
    }
int poped;
if(min==100000) ///noi cautam aici minimul !!!!! nu trebuie extras minimul
        poped=0;
else
    {
        poped=H[indexmin]->key;
        H[indexmin]=H[indexmin]->urm;
        all_ms+=2;
    }
    return poped;

}

/////////////////////////////////////////////////////////
/**HEAP part*/

int left(int i) {
  return (2 * i) ;
}

int right(int i) {
  return (2 * i) + 1;
}

void heapify(int A[], int i)
{
  int l = left(i), min;
  int r = right(i);
  if ( (l <= dim) && (A[l] < A[i]))
  {
    min = l;
  }
  else
  {
    min = i;
  }

  if ( (r <= dim) && (A[r] < A[min]))
  {
    min= r;
  }
  all_ms+=2;
  if (min != i)
  {
    int temp = A[i];
    A[i] = A[min];
    A[min] = temp;
    all_ms+=3;
    heapify(A, min);
  }
}

int parent(int i)
{
	return (i / 2);
}

void push_heap(int A[], int key)
{
	dim++;
	A[dim]=key;
	//all_ms++;
	int i=dim;
	while(i>1 && A[parent(i)]>A[i])
	{
		int aux=A[parent(i)];
		A[parent(i)]=A[i];
		A[i]=aux;
		i=parent(i);
	}
}

int pop_heap (int A[])
{
    int x=A[1];
    A[1]=A[dim];
    dim--;
    heapify(A, 1);
    return x;
}

//////////////////////////////////////////////////////////////////////
/**MERGE SORT PART*/

void add_to_rez(int x)
{
    rez_index++;
    B[rez_index]=x;
   /* TIP_LISTA *p;
    TIP_LISTA *q;
    q->numar=x;
    q->urm=NULL;
    p=L;
    while(p->urm!=NULL)
            {
                printf("%d", p->numar);
                p=p->urm;
            }
    p->urm=q;*/
    all_ms++;
}


void merge()
{
  int x;
  rez_index=0;
  dim=0;
  int i=0;
  for(i=1; i<=k; i++)//initializeaza heap cu minime o data
        {
            x=pop_hash();
          //  MS++;
            push_heap(A, x);
        }
    while(dim>0)
    {
        x=pop_heap(A);
       // MS++;
        add_to_rez(x);
        //if ((n/k)*k<=rez_indice+n-k) //rez_indice+k-1<n
       // {
            //printf("%d %d \n", rez_indice, (n/k)*k);
        x=pop_hash();
     //   MS++;
        if(x!=0)
                push_heap(A, x);
       // MS++;
       // }
    }
}

void print_rez ()
{
    int i;
    printf("final array: ");
    for(i=1; i<=rez_index; i++)
         printf("%d ", B[i]);
    /*TIP_LISTA *p;
    p=L;
    while(p->urm!=NULL)
            {
                printf("%d", p->numar);
                p=p->urm;
            }*/
    printf("\n");
}

//////////////////////////////////////////////////////////////////////
/**GENERATE PART*/

void last (int index)
{
TYPE_HASH *p=H[index];
while(p->urm!=NULL)
            p=p->urm;
TYPE_HASH *q;
q=(TYPE_HASH*)malloc(sizeof(TYPE_HASH));
q->key=p->key + rand()%100;
q->urm=NULL;
p->urm=q;
}

void generate(int n, int k)
{
int x=0;
srand(time(NULL));
int i,j;
for(i=1;i<=k;i++)
    {
    for(j=1;j<=n/k;j++)
        {
            x=x+rand()%100;
            insert(x, i);
        }
    x=0;
    }
if(n%k!=0)
    for(i=1; i<=n%k; i++)
        last(i);
}


int main()
{
    //run for small example
    n=12;
    k=4;

    srand(time(NULL));

    printf("Nr of arrays: %d \n", k);
    printf("Nr of elements: %d \n", n);
    generate(n,k);
    print_hash();
    merge();
    print_rez();

    /*FILE *f;
    f=fopen("1.txt","w");
    fprintf(f,"n k1=5 k2=10 k3=100\n");
    for(n=100; n<=10000; n=n+100)
        {
            all_ms=0;
            k=5;
            init(k);
            generate(n, k);
            merge();
            fprintf(f,"%d %d ",n, all_ms);

            all_ms=0;
            k=10;
            init(k);
            generate(n, k);
            merge();
            fprintf(f,"%d ",all_ms);

            all_ms=0;
            k=100;
            init(k);
            generate(n, k);
            merge();
            fprintf(f,"%d\n",all_ms);
        }
    fclose(f);

    f=fopen("2.txt","w");
    fprintf(f,"k all_ms \n");
    n=10000;
    k=20;
    for(k=10; k<=500; k=k+10)
    {
        all_ms=0;
        //if(n%k==0)
        //{
            init(k);
            generate(n, k);
            merge();
            fprintf(f, "%d %d\n", k, all_ms);
       // }
    }
    fclose(f);*/

    return 0;
}
