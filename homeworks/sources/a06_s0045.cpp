/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***

Folosirea arborelui binar de cautare echilibrat asigura eficienta de O(n*lgn) cand n si m
nu sunt constante.
Daca am folosi o lista dinamica sau vector eficienta ar fi O(m*n) ~ O(n^2).
Constructia echilibrata este asigurat de o functie recursiva  de tip postordine. Nodul arborelui pe langa informatii obisnuite
mai contine un camp de parinte si un camp de size(nr noduri din stanga + nr noduri din dreapta + 1).
Folosind informatia size putem selecta al m-lea element intr-un timp lg(n) din ce rezulta ca selectarea a n elemente se face
in timp n*lg(n).
Informatia parinte este folosit la actualizarea campului size dupa fiecare stergere din arbore.
*/

#include <stdio.h>
#include <conio.h>

#include "Profiler.h"

#define MAX_SIZE 10100

int op[MAX_SIZE];
int n;

typedef struct NOD {
	int key,dim;
	NOD *st, *dr, *p;
} nod;

nod *build_tree(int st, int dr)
{
	if (st <= dr)
	{
		int m=(st + dr)/2,ds,dd;

		nod *x=(nod *)malloc(sizeof(nod));
		x->key=m;

		nod *aux;

		x->st=build_tree(st, m-1);
		if (x->st == NULL)
		{
			ds=0;

			op[n]++;
		}
		else
		{
			ds=x->st->dim;
			x->st->p=x;

			op[n]+=2;
		}

		x->dr=build_tree(m+1, dr);
		if (x->dr == NULL)
		{
			dd=0;

			op[n]+=1;
		}
		else
		{
			dd=x->dr->dim;
			x->dr->p=x;

			op[n]+=2;
		}

		x->dim=ds+dd+1;

		op[n]+=5;

		return x;
	}
	else
	{
		op[n]++;
		return NULL;
	}
}


void pritty_print_(nod *v, int nivel)
{
	if (v == NULL)
	{
		return;
	}

	pritty_print_(v->st,nivel+1);
	for (int i=1; i<=nivel; i++)
	{
		printf("   ");
	}
	printf("%d-%d\n\n", v->key, v->dim);
	pritty_print_(v->dr,nivel+1);
}

void pritty_print(nod *v, int nivel)
{
	printf("-------------------------------------------------------\n");
	pritty_print_(v,nivel);
	if (v == 0)
	{
		printf("pritty_print: Arbore vid!\n");
	}
	printf("-------------------------------------------------------\n");
}

/*
select the i-th element from the tree
os_select(root,i);
*/
nod * os_select(nod *x, int i)
{
	int r=1;

	if (x->st != NULL)
	{
		r=x->st->dim + 1;

		op[n]++;
	}

	op[n]++;

	if (r == i)
	{
		op[n]++;

		return x;
	}
	else
	if (i < r)
	{
		op[n]++;

		return os_select(x->st, i);
	}
	else
	{
		op[n]++;

		return os_select(x->dr, i-r);
	}
}

/*

@p nodul de sters
@return rad
*/
nod * os_delete(nod *rad, nod *p)
{ 
	nod *nod_inlocuire, *tata_nod_inlocuire;

	tata_nod_inlocuire=p;
	
	if (p->st == 0)
	{	
		nod_inlocuire=p->dr;
		if (nod_inlocuire != 0)
		{
			nod_inlocuire->p=p->p;

			op[n]++;
		}

		op[n]+=2;
	}	
	else 
	if (p->dr == 0) 
	{	
		nod_inlocuire=p->st;
		if (nod_inlocuire != 0)
		{
			nod_inlocuire->p=p->p;

			op[n]++;
		}

		op[n]+=3;
	}
	else
	{		
		nod_inlocuire=p->dr;

		op[n]+=2;

		while(nod_inlocuire->st != 0)
		{
			tata_nod_inlocuire=nod_inlocuire;
			nod_inlocuire=nod_inlocuire->st;

			op[n]+=3;
		}
		if (tata_nod_inlocuire != p)
		{
			if (nod_inlocuire->dr != 0)
			{
				nod_inlocuire->dr->p=tata_nod_inlocuire;

				op[n]++;
			}

			tata_nod_inlocuire->st=nod_inlocuire->dr;
			nod_inlocuire->dr=p->dr;

			op[n]+=3;
		}
		nod_inlocuire->st=p->st;

		p->st->p=nod_inlocuire;
		p->dr->p=nod_inlocuire;
		nod_inlocuire->p=p->p;

		op[n]+=5;
	}

	
	if (p->p == 0) //trebuie sters chiar rad initiala
	{
		op[n]++;

		if (p->st != 0 || p->dr !=0)
		{
			if ((p->st !=0) && (p->dr != 0))
			{
				nod_inlocuire->dim=p->dim;

				if (tata_nod_inlocuire == p && nod_inlocuire != 0)
				{
					(nod_inlocuire->dim)--;

					op[n]++;
				}
			}

			op[n]+=2;

			while (tata_nod_inlocuire != 0)
			{
				tata_nod_inlocuire->dim--;
				tata_nod_inlocuire=tata_nod_inlocuire->p;

				op[n]+=3;
			}
		}

		free(p);

		op[n]++;

		return nod_inlocuire;
	}
	else
	{
		op[n]++;

		if (p == p->p->st)
		{
			p->p->st=nod_inlocuire;

			op[n]+=2;
		}
		else
		{
			p->p->dr=nod_inlocuire;

			op[n]+=2;
		}

		if ((p->st !=0) && (p->dr != 0))
		{
			nod_inlocuire->dim=p->dim;

			if (tata_nod_inlocuire == p && nod_inlocuire != 0)
			{
				(nod_inlocuire->dim)--;

				op[n]+=3;
			}

			op[n]+=2;
		}

		while (tata_nod_inlocuire != 0)
		{
			tata_nod_inlocuire->dim--;
			tata_nod_inlocuire=tata_nod_inlocuire->p;

			op[n]+=3;
		}

		free(p);
		return rad;
	}
}

/*
n-nr elem
m-pasul
afis == 1 -enabled
*/
void josephus(int n, int m, nod **rad,int afis)
{
	nod *aux;
	int i=1;

	*rad=build_tree(1, n);
	(*rad)->p=NULL;

	if (afis == 1)
	{
		pritty_print(*rad,1);
	}

	while (*rad != 0)
	{
		i=((i+m-2) % (*rad)->dim) +1;

		op[n]++;

		aux=os_select(*rad, i);

		if (afis == 1)
		{
			printf("Josephus: %d\n", aux->key);
		}

		*rad=os_delete(*rad, aux);

		if (afis == 1)
		{
			pritty_print(*rad,1);
		}
	}
}


void init()
{
	for (int i=0; i<MAX_SIZE; i++)
	{
		op[i]=0;
	}
}

void main()
{
	FILE *f=fopen("assign6.csv", "w");
	if (f == 0)
	{
		perror("Eroare la deschidere fisier!");
		exit(1);
	}

	fprintf(f,"n,nr. op\n");

	nod *rad;
	nod *aux;
	int k=0;

	////test1
	////test build tree
	//rad=build_tree(1, 10);
	//rad->p=NULL;
	//pritty_print(rad, 1);

	////test os_select



	//for (k=1; k<11; k++)
	//{
	//	aux=os_select(rad, k);
	//	printf("\n%d -lea element:%5d\n", k, aux->key);
	//}

	////test parinte
	//for (k=1; k<11; k++)
	//{
	//	aux=os_select(rad, k);
	//	if (aux->p != 0)
	//		printf("\np[%d]=%d", aux->key, aux->p->key);
	//	else
	//		printf("\np[%d]=%d", aux->key, aux->p);
	//}
	//printf("\n");

	////test os_delete
	//for (k=10; k>0; k--)
	//{
	//	aux=os_select(rad, k);
	//	printf("\n--delete: %d -lea element:%5d\n\n", k, aux->key);
	//	rad=os_delete(rad, aux);
	//	pritty_print(rad, k);

	//	//test parinte
	//	if (rad != 0)
	//	{
	//		for (k=1; k < rad->dim+1; k++)
	//		{
	//			aux=os_select(rad, k);
	//			if (aux->p != 0)
	//				printf("\np[%d]=%d", aux->key, aux->p->key);
	//			else
	//				printf("\np[%d]=%d", aux->key, aux->p);
	//		}
	//	printf("\n");
	//	}
	//	else
	//	{
	//		printf("test_parinte: Arbore vid!\n");
	//	}
	//}

	//test josephus
	josephus(8,3,&rad,1);
	//josephus(10,5,&rad,0);
	//josephus(11,5,&rad,1);
	//n=10;
	//josephus(n,n/2,&rad,1);
	//n=4;
	//josephus(n,n/2,&rad,1);

	//generare date
	init();
	for (n=100; n<=10000; n+=100)
	{
		josephus(n,n/2, &rad, 0);
	}

	for (n=100; n<=10000; n+=100)
	{
		fprintf(f, "%d,%d\n",n,op[n]);
	}	

	fclose(f);
}