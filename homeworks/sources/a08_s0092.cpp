/**
	Student:***ANONIM*** Diana ***ANONIM***
	Grupa:***GROUP_NUMBER***
	Problem specification: Disjoint Sets - Connected Components
	Comments:
	- the usage of linked lists (trees) for the disjoint sets and the path compression and union by rank technik reduces running time and gives the algorithm and efficiency of O(m+nlogn): m=nr of edges, n=nr of vertices
	- make_set has an efficiency of O(1)
	- unio is O(n)
	- the head(root) of the list of the set contains the reprezentative of that 
	- from the chart generated we can deduce that the number of operations si directly proportional to the number of edge
**/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

/*Fiecare nod din graf este reprezentat de un nod, care are campurile key, ranka listei din care face parte, reprezentative dat de catre capul listei
din care face parte si nodul nextator din lista*/

typedef struct nod{
	int key;
	int rank;
	struct nod *reprezentative;
	struct nod *next;
}nod;

typedef struct edge{
	int p;
	int d;
}edge;

nod	*first[10000],*last[10000];
int op,t,j;
edge e[60000];
int matrice[10000][10000]; //matrice reprezinta matriceicea de adiacenta a grafului neordonat generat aleatoriu

//generate_random genereaza un numar intreg aleator intre 0 si 10000
int generate_random() 
{
	return  rand() % 10;//000 ; !!!!!!sa schimb 10 cu 10000
}
void afisare(int x)
{
   int m,n;
   for(m=0;m<x;m++)
   {
     for(n=0;n<x;n++) 
		 printf(" %d ",matrice[m][n]);
	 printf("\n");
   }

}
//generate_graph realizeaza generarea unui graf neorientat cu numarul de muchii dat de parametrul z
void generate_graph(int x)
{
	memset(matrice,0,sizeof(matrice));
	int contor=0;					
	while(contor<x)
	{	
		int nr1 =generate_random();
		int nr2 =generate_random();
		if((matrice[nr1][nr2]==0) && (nr1!=nr2))
		{
			matrice[nr1][nr2]=1;
			matrice[nr2][nr1]=1;
			e[contor].p=nr1;
			e[contor].d=nr2;
			contor++;
		}
	}
}

//find_set returneaza reprezentativeul unui nod, care are cheia egala cu parametrul x al functiei
int find_set(int x)
{   
	op++;
	return first[x]->reprezentative->key;
}

//make_set creeaza o mlaste noua cu un singur element x, care este si reprezentative
void make_set(int x)
{   
	op++;
	first[x]=(nod*)malloc(sizeof(nod));
	first[x]->key=x;
	first[x]->rank=1;
	first[x]->reprezentative=first[x];
	last[x]=first[x];
}

/*reuneste doua mlasti disjuncte,adaugand la finalul listei cu ranka mai mare nodurile listei cu ranka mai mica
si setand pentru nodurile mutate ca si reprezentative reprezentativeul listei mai lungi. 
*/
void link(int x, int y)
{	
	nod *p;
	if(first[x]->rank >= first[y]->rank)
	{   
		last[x]->next=first[y];
		first[x]->rank+=first[y]->rank;
		last[x]=last[y];
		p=first[y];
		for(int i=0;i<first[y]->rank;i++)
		{
			p->reprezentative=first[x];
			p=p->next;
		}
	}
	else	
	{	
		last[y]->next=first[x];
		first[y]->rank+=first[x]->rank;
		last[y]=last[x];
		p=first[x];
		for(int i=0;i<first[x]->rank;i++)
		{ 
			p->reprezentative=first[y];
			p=p->next;
		}
	}
}

//reuneste doua mlasti disjuncte
void unio(edge t)
{   
	op++;
	link(find_set(t.p),find_set(t.d));
}

//conex_component determina componentele conexe ale unui graf neorientat
void connected_component(int t, edge e[])
{  
	for(j=0;j<10;j++)    //initializam varfurile !!!!!!sa schimb 10 cu 10000
		make_set(j);

	for(int i=0;i<t;i++)	//pentru fiecare muchie a grafului verificam daca varfurile ei se afla in aceeasi mlaste
	{						//in caz contrar, reunim mlastile din care acestea fac parte.		
		if((find_set(e[i].p))!=(find_set(e[i].d))) 
		{
			unio(e[i]);
		}
	}
}

void test()
{
	int nr_edges=7;
	int nr_vertices=10;
	for(int i=0;i<nr_vertices;i++)
	     for(int y=0;y<nr_vertices;y++)
		     matrice[i][y]=0;
	printf("\n Before\n");
		afisare(nr_vertices);
	generate_graph(nr_edges);
	printf("\n After\n");
		afisare(nr_vertices);
	connected_component(nr_edges,e);
	printf("Connected components:\n");
	nod* aux;
	for (int i=0;i<10;i++)
	{
		aux=first[i];
		while(aux!=last[i])
		{
			printf("%d",aux->key);
			aux=aux->next;
		}
		printf("%d",aux->key);
		printf("\n");
	}
	getch();
}
int main()
{	
	srand(time(NULL));
	/**
	FILE *pf;
	pf=fopen("disjoint_sets.csv","w");
	fprintf(pf,"nr_edges,nr_op\n");
	
	for(int nr_edges=10000;nr_edges<=60000;nr_edges+=1000)
	{	
		generate_graph(nr_edges);
		op=0;
		connected_component(nr_edges,e);
		fprintf(pf,"%d,%d\n",nr_edges,op);
		for(int i=0;i<10;i++)
			free(first[i]);
	}
	fclose(pf);	
	**/
	test();
}
