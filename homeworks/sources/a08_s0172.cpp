//***ANONIM*** ***ANONIM***
//***GROUP_NUMBER***
//OBS : Eficienta functiei make_set este de O(1), a functiei find_set este de O(n), iar a functiei union_set de O(1).

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

typedef struct node{
	int key;
	struct node * next;
}NODE;

NODE *head[10001], *tail[10001];
int A[10001][10001], null_list[10001];
int v; //nr de varfuri

//nr de apeluri functiile make_set,union_set,find_set
int nr_calls;

void generate_random(int e, int v, int *v1, int *v2){
	srand ( time(NULL) );

	for (int i=1; i<=e; i++){
		*v1 = rand () % v + 1;
		*v2 = rand () % v + 1;

		//generam o muchie random pana satisface conditiile
		while (*v1 == *v2 || A[*v1][*v2] == 1 || A[*v2][*v1] == 1){
			*v1 = rand () % v + 1;
			*v2 = rand () % v + 1;
		}

		printf ("v1=%d \t v2=%d\n", *v1, *v2);
		A[*v1][*v2] = 1;
		A[*v2][*v1] = 1;
	}
}

void print_matrix (int n){
	for (int i=1; i<=n; i++){
		for (int j=1; j<=n; j++){
			printf ("%d ",A[i][j]);
		}
		printf ("\n");
	}
}

//creaza un nod de cheie i
NODE * create_node (int i){
	NODE * p;

	p = (NODE *)malloc(sizeof(NODE));
	p->next = NULL;
	p->key = i;

	return p;
}

//sterge o lista punand 1 pe pozitia corespunzatoare
void delete_list (int ind){
	null_list[ind] = 1;
}

void make_set (int i){
	head[i] = tail[i] = create_node(i);
}

NODE *find_set (int j, int *ind){
	NODE *p;

	for (int i=1; i<=v; i++){
		if (null_list[i] != 1){
			p = head[i];
			
			do{
				if (p->key == j){
					*ind = i;
					return head[i];
				}
			
				p = p->next;
			}while (p!=NULL);
		}
	}
}

void union_set(int u, int v){
	NODE *head1, *head2;
	NODE *tail1, *tail2;
	NODE *p;
	
	int ind1, ind2;

	head1 = find_set(u, &ind1);
	head2 = find_set(v, &ind2);

	tail1 = tail[ind1];
	tail2 = tail[ind2];

	if (head1->key < head2->key){
		tail[ind1]->next = head[ind2];
	
		tail[ind1] = tail[ind2];

		//stergem a doua lista
		delete_list(ind2);
	}
	else{
		tail2->next = head1;
	
		tail[ind2] = tail[ind1];

		//stergem prima lista
		delete_list(ind1);
	}
}

void connected_components(){
	int ind;

	for (int i=1; i<=v; i++){
		nr_calls++;
		make_set (i);
	}

	for (int i=1; i<=v; i++)
		for (int j=1; j<=v; j++){
			if(A[i][j] == 1){
				//avem o muchie
				nr_calls += 2;
				if(find_set(i, &ind) != find_set(j, &ind)){
					nr_calls++;
					union_set(i,j);
				}
			}
		}
}

void evaluate()
{
	FILE *f;
	int v1, v2;

	f = fopen ("disjoint.txt","w");

	v = 10000;
	
	// e = nr de muchii
	for (int e=10000; e<=60000; e=e+1000){
		printf("%d\n",e);

		//generam muchiile intr-un mod aleator
		generate_random(e,v,&v1,&v2);
		
		//initializam cu NULL, respectiv 0 listele inlantuite si vectorul de liste nule
		for (int i=1; i<=v; i++){
			head[i] = tail[i] = NULL;	
			null_list[i] = 0;			
		}

		nr_calls = 0;
		
		connected_components ();

		fprintf (f,"%d %d\n", e, nr_calls);
	}

	fclose(f);
}

void test (){
	NODE *p;
	int e = 5;  //nr muchii
	int v1, v2;
	
	v = 10; 

	//generam muchiile intr-un mod aleator
	printf ("Muchii:\n");
	generate_random(e,v,&v1,&v2);

	printf ("\n\nMatricea muchiilor:\n");
	print_matrix(v);

	connected_components ();
	
	printf ("\nComponente conexe:\n");
	for (int i=1; i<=v; i++){
		if (null_list[i] != 1){
			p = head[i];
			printf ("L[%d] = ",i);
			
			while (p != NULL){
				printf ("%d ", p->key);
				p = p->next;
			}
			printf ("\n");
		}
	}
}

int main (){
	test ();
	//evaluate();
	
	getche ();
	return 0;
}