#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
#include <iostream>
#include "Profiler.h"

Profiler profiler("Disjoint Sets");

using namespace std;

typedef struct Node
{
	int key;
	int rank;
	Node *parent;
};

typedef struct Edge
{
	Node *start;
	Node *end;
};

int vertices, edges;
Node *graph[10001];
Edge *edge[60001];

void makeSet(Node *x)
{
	profiler.countOperation("makeSet", edges, 1);
	x->parent = x;
	x->rank = 0;
}

void link(Node *x, Node *y)
{
	if(x->rank > y->rank)
	{
		y->parent = x;
	}
	else 
	{
		x->parent = y;
	}
	if (x->rank == y->rank)
	{
		y->rank++;
	}
}

Node* findSet(Node *x)
{
	profiler.countOperation("findSet",edges,1);
	if(x != x->parent)
	{
		x->parent = findSet(x->parent);
	}
	return x->parent;
}

void unionSets(Node *x, Node *y)
{
	profiler.countOperation("unionSet",edges,1);
	link(findSet(x), findSet(y));
}

void connectedComponent()
{
	for(int i=0; i<vertices; i++)
	{
		makeSet(graph[i]);
	}
	for( int j=0; j<edges; j++)
	{
		if(findSet(edge[j]->end) != findSet(edge[j]->start))
		{
			unionSets(edge[j]->end, edge[j]->start);
		}
	}
}

void print()
{
	printf("Connected components:\n");
	int print[7]={0,0,0,0,0,0,0}, ok=0, x;
	
	do{
		ok=1;
		for(int i=0;i<7;i++)
		{
			if(print[i]==0)
			{
				ok=0;
			}
		}
		for(int i=0;i<edges;i++)
		{
			if(i==0)
			{
				printf("       %d-%d\n", edge[i]->end->key, edge[i]->start->key);
				x=edge[i]->start->key;
				print[i]=1;
			}
			else
			{
				if(edge[i]->end->key==x)
				{
					printf("       %d-%d\n", edge[i]->end->key, edge[i]->start->key);
					x=edge[i]->start->key;
					print[i]=1;
				}
				else
				{
					printf("\n");
					if(i<edges)
					{	
						printf("       %d-%d\n", edge[i]->end->key, edge[i]->start->key);
						x=edge[i]->start->key;
					}
				}
			}
		}

	}while(ok!=0);
}


void main()
{
	vertices = 9;
	edges=7;
	srand (time(NULL));
	
	for( int i=0; i < vertices; i++ )
	{
		graph[i] = (Node*)malloc(sizeof(Node));
		graph[i]->key=i+1;
	}
	
	edge[0] = (Edge*)malloc(sizeof(Edge));		
	edge[0]->end = graph[0];
	edge[0]->start = graph[1];

	edge[1] = (Edge*)malloc(sizeof(Edge));		
	edge[1]->end = graph[1];
	edge[1]->start = graph[2];
	
	edge[2] = (Edge*)malloc(sizeof(Edge));		
	edge[2]->end = graph[2];
	edge[2]->start = graph[3];
	
	edge[3] = (Edge*)malloc(sizeof(Edge));		
	edge[3]->end = graph[3];
	edge[3]->start = graph[0];
	
	edge[4] = (Edge*)malloc(sizeof(Edge));		
	edge[4]->end = graph[6];
	edge[4]->start = graph[5];

	edge[5] = (Edge*)malloc(sizeof(Edge));		
	edge[5]->end = graph[5];
	edge[5]->start = graph[6];
	
	edge[6] = (Edge*)malloc(sizeof(Edge));		
	edge[6]->end = graph[7];
	edge[6]->start = graph[8];

	connectedComponent();

	printf("Nodes: ");
	for(int i=0;i<vertices;i++)
	{
		printf("%d ", graph[i]->key);
	}

	printf("\n\nEdges:\n");
	for(int i=0; i<edges; i++)
	{
		printf("       %d-%d\n", edge[i]->end->key, edge[i]->start->key);
	}

	print();
	getch();

	/*
	vertices= 10000;
	srand (time(NULL));

	for( edges=10000; edges <= 60000; edges+=1000 )
	{
		profiler.countOperation("makeSet",edges,0);
		profiler.countOperation("unionSet",edges,0);
		profiler.countOperation("findSet",edges,0);
		
		for( int i=0; i < vertices; i++ )
		{
			graph[i] = (Node*)malloc(sizeof(Node));
				
		}

		for( int i=0; i < edges; i++ )
		{
			edge[i] = (Edge*)malloc(sizeof(Edge));
			edge[i]->end = graph[rand()% 10000];
			edge[i]->start = graph[rand()% 10000];
		}
		connectedComponent();	
	}

	profiler.addSeries("Sumaux","makeSet","findSet");
	profiler.addSeries("Sum","Sumaux","unionSet");

	profiler.createGroup("Disjoint Sets","makeSet","findSet","unionSet","Sum");
	profiler.createGroup("Separat","makeSet","unionSet");

	profiler.showReport();
	*/
	
}