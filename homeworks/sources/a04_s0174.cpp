#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

/*
*   Autor: ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
*/

/*
*   Rulind acest program am observat ca complexitatea algoritmului este: O(nlogk)
*   In functie de valoarea lui k, variaza panta dreptei.
*/

struct element{
    int value, listNumber;
};
struct nod{
    struct element element;
    struct nod * urm;
};

element result[20000];
nod * lists[600];
element heap[600];
double nrOfOperations;
double operations[200];


void printElements(element *a, int length)
{
	printf("\n");
	for(int i = 1; i <= length; i++)
		printf("%d ", a[i].value);
}

void writeToFile(FILE *df, double *operations, int length)
{
	fprintf(df, "\n");
	for(int i = 1; i <= length; i++)
		fprintf(df, "%.0f;", operations[i]);
}

void atribuie(element *e1, element *e2)
{
    e1->value = e2->value;
    e1->listNumber = e2->listNumber;
	nrOfOperations += 2;
}


nod * pushToList(nod *l, int n, int lista)
{
    nod *aux1 = (nod *)malloc(sizeof(nod));
    aux1->element.listNumber = lista;
    aux1->element.value = n;
	aux1->urm = NULL;
    nod *aux2 = l;

    if(aux2 == NULL)
    {
        return aux1;
    }
    while(aux2->urm != NULL)
    {
        aux2 = aux2->urm;
    }
    aux2->urm = aux1;
    return l;
}

nod * popFromList(nod *l)
{
	nod *aux = l;
    l = l->urm;
	free(aux);
	nrOfOperations += 4;
    return l;
}

void heapify(element *a, int poz, int n)
{
	int j = poz;
	bool b = true;
	int min;
	element aux;
	while(b && j <= n / 2)
	{
		if(((2*j+1) <= n) && (a[j].value > a[2*j+1].value))
		{
			min = 2 * j + 1;
		}else
		{
			min = j;
		}
		if(a[min].value > a[2 * j].value){
			min = 2 * j;
			nrOfOperations += 1;
		}
		nrOfOperations += 4;
		if(min != j)
		{
			aux.value = a[min].value;
            a[min].value = a[j].value;
            a[j].value = aux.value;
            aux.listNumber = a[min].listNumber;
            a[min].listNumber = a[j].listNumber;
            a[j].listNumber = aux.listNumber;
            j = min;
			nrOfOperations += 7;
		}else
		{
			b = false;
		}
	}
}

void bottomUp(element *a, int n)
{
	int i,j,min;
	element aux;
	bool b;
	for(i = n / 2; i >= 1; i--)
		heapify(a, i, n);
}

void mergeLists(nod **liste, int n, int k)
{
	int list;
	int lung = k;
	int l = 1;
	int poz = 1;
    for(int i = 1; i <= k; i++)
    {
        atribuie(&heap[i], &(liste[i]->element));
        liste[i] = popFromList(liste[i]);
    }
    bottomUp(heap, k);
	while(k > 0)
	{
		atribuie(&result[poz], &heap[1]);
		poz++;
		nrOfOperations++;
		if(liste[heap[1].listNumber] != NULL)
		{
			atribuie(&heap[1], &(liste[heap[1].listNumber]->element));
			heapify(heap, 1, k);
			liste[heap[1].listNumber] = popFromList(liste[heap[1].listNumber]);
		}else
		{

			heap[1] = heap[k];
			k--;
			nrOfOperations += 1;
			heapify(heap, 1, k);
		}


	}

}


int main(){
	FILE *df;
	srand(time(0));
	//#########################
	//test algoritm
	//#########################
    int k = 4;
    int n = 20;
    int lung = n / k;
    int i, j, aux;
    for(i = 1; i <= k; i++)
    {
        aux = rand() % 10000;
		if(i == k) lung += n % k;
		printf("Lista%d :", i);
        for(j = 1; j <= lung; j++)
        {
			aux += abs(rand()) % 300 + 1;
			printf("%d ", aux);
            lists[i] = pushToList(lists[i], aux, i);
        }
		printf("\n");
    }
	mergeLists(lists, n, k);
	printf("Lista interclasata :");
	printElements(result, n);

	//#########################
	//n variable algoritm
	//#########################
	df = fopen("nrandom.csv","w");
	k = 5;
	for(n = 100; n <= 10000; n += 100)
	{
		lung = n / k;
		for(i = 1; i<= k; i++)
		{
			aux = rand()%1000;
			if(i == k) lung += n % k;
			for(j = 1; j <= lung; j++)
			{
				aux += abs(rand()) % 3 + 1;
				lists[i] = pushToList(lists[i], aux, i);
			}
		}
		nrOfOperations = 0;
		mergeLists(lists, n, k);
		operations[n / 100] = nrOfOperations;
	}
	writeToFile(df, operations, 100);

	k = 10;
	for(n = 100; n <= 10000; n += 100)
	{
		lung = n / k;
		for(i = 1; i <= k; i++)
		{
			aux = rand() % 1000;
			if(i == k) lung += n % k;
			for(j = 1; j <= lung; j++)
			{
				aux += abs(rand()) % 3 + 1;
				lists[i] = pushToList(lists[i], aux, i);
			}
		}
		nrOfOperations = 0;
		mergeLists(lists, n, k);
		operations[n / 100] = nrOfOperations;
	}
	writeToFile(df, operations, 100);

	k = 100;
	for(n = 100; n <= 10000; n += 100)
	{
		lung = n / k;
		for(i = 1; i <= k; i++)
		{
			aux = rand() % 1000;
			if(i == k) lung += n % k;
			for(j = 1; j <= lung; j++)
			{
				aux += abs(rand()) % 3 + 1;
				lists[i] = pushToList(lists[i], aux, i);
			}
		}
		nrOfOperations = 0;
		mergeLists(lists, n, k);
		operations[n / 100] = nrOfOperations;
	}
	writeToFile(df, operations, 100);

	fclose(df);

	//#########################
	//k variable algoritm
	//#########################

	df = fopen("krandom.csv", "w");
	n = 10000;
	for(k = 10; k <= 500; k += 10)
	{
		lung = n / k;
		for(i = 1 ; i <= k; i++)
		{
			aux = rand() % 1000;
			if(i == k) lung += n % k;
			for(j = 1; j <= lung; j++)
			{
				aux += abs(rand()) % 3 + 1;
				lists[i] = pushToList(lists[i], aux, i);
			}
		}
		nrOfOperations = 0;
		mergeLists(lists, n, k);
		operations[k / 10] = nrOfOperations;
	}
	writeToFile(df, operations, 50);
	fclose(df);

	getch();
	return 0;
}
