#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


#define vector dvector2

template <class t>
class dvector2
{
	t* data;
	unsigned capacity;
	
	void realloc(unsigned nsize)
	{
		t* ndata = new t[nsize];
		if (data)
		{
			if (capacity >= nsize)
			{
				memcpy(ndata,data,nsize*sizeof(t));
			}
			else
			{
				memset(ndata,0,nsize*sizeof(t));
				memcpy(ndata,data,capacity*sizeof(t));
			}
			delete[] data;
		}
		else
		{
			memset(ndata,0,nsize*sizeof(t));
		}
		data = ndata;
		capacity = nsize;
	}

public:
	
	unsigned size;

	dvector2()
	{
		data = 0;
		capacity = 0;
		size = 0;
	}

	dvector2(dvector2 &other)
	{
		data=0;
		capacity=0;
		size=0;
		realloc(other.capacity);
		for(int i=0; i<other.size; i++)
		{
//			if (other[i]==0) data[i]=0;
//			else
			{
				data[i]=t(other[i]);
			}
		}
		size=other.size;
	}

	dvector2(unsigned size)
	{
		data = 0;
		capacity = 0;
		size = 0;
		realloc(size);
	}

	dvector2(t* vec, unsigned count)
	{
		data = 0;
		capacity = 0;
		size = 0;
		realloc(count);
		size = count;
		for (int i=0; i<count; i++)
		{
			data[i] = vec[i];
		}
	}

	~dvector2()
	{
		if(data) delete[] data;
	}

	void prealloc(unsigned size)
	{
		realloc(size);
	}

	t& operator [] (unsigned index)
	{
		if (index+1>size) 
			size = index+1;

		if (index >= capacity)
		{
			int ncapacity = index + (capacity>>4) + 1;
			this->realloc(ncapacity);
			capacity = ncapacity;
		}

		return data[index];
	}

	void print()
	{
		for (int i=0; i<size; i++)
		{
			printf("%d ",data[i]);
		}
		printf("\n");
	}

};

class multinode
{
public:

	int key;
	vector<multinode> child;

	multinode()
	{
		key = -1;
	}

	multinode(int x)
	{
		key = x;
	}

	multinode* add(int e)
	{
		child[child.size]=e;
		return &child[child.size-1];
	}

	multinode* find(int e)
	{
		if (e == this->key)
		{
			return this;
		}

		for (int i=0; i<child.size; i++)
		{
			if (child[i].key == e)
			{
				return &(child[i]);
			}
		}

		multinode *q = 0;

		for (int i=0; i<child.size; i++)
		{
			q = child[i].find(e);
			if (q != 0) return q;
		}

		return 0;
	}

	void print(int depth = 0)
	{
		for (int i=0; i<depth; i++)
		{
			printf("|  ");
		}
		printf("+- %d\n",key);
		
		for (int i=0; i<child.size; i++)
		{
			child[i].print(depth+1);
		}
	}
};


class multitree
{	 
public:

	multinode *root;

	void build_helper(vector<int> &pv, vector<bool> &done, unsigned index)
	{
		if (!done[index])
		{
			multinode *q = root->find(pv[index]);
			if (!q)
			{
 				build_helper(pv,done,pv[index]);
				q = root->find(pv[index]);
			}
			
			assert(q);
			
			q->add(index);
			done[index]=true;
		}
	}
	
	multitree(vector<int> parent_vec)
	{
		root = 0;
		vector<bool> done;

		for (int i=0; i<parent_vec.size; i++)
		{
			if (parent_vec[i]<0)
			{
				root = new multinode(i);
				done[i] = true;
				break;
			}
		}
		
		for (int i=0; i<parent_vec.size; i++)
		{
			build_helper(parent_vec,done,i);
		}
	}

	multitree()
	{
		root = 0;
	}

	void addTo(int parent_key, int child_key)
	{
		if (parent_key==-1)
		{
			if (!root)
			{
				root = new multinode(parent_key);
			}
			root->add(child_key);
		}
		else if(root)
		{
			multinode *p = root->find(parent_key);
			if (p)
			{
				p->add(child_key);
			}
			else
			{
				fprintf(stderr,"Can't find parent node");
			}
		}
	}

	void print()
	{
		if (root)
		{
			root->print();
		}
		else
		{
			printf("null");
		}
	}
};

class grafnode;

class DFS_info
{
public:
	int color;
	int d;
	int f;
	grafnode* parent;
	DFS_info()
	{
		color=0;
		d=0;
		f=0;
		parent=0;
	}
};

class grafnode
{
public:
	int key;
	vector<grafnode*> neighbour;

	grafnode(grafnode&other)
	{
		(*this)=other;
		neighbour = vector<grafnode*>(other.neighbour);
	}

	grafnode(int key = 0)
	{
		this->key = key;
	}
	grafnode* findNeighbour(grafnode* other)
	{
		for(int i=0; i<neighbour.size; i++)
		{
			if (neighbour[i] == other)
			{
				return neighbour[i];
			}
		}
		return 0;
	}
	void addNeighbour(grafnode* other)
	{
		grafnode* p = findNeighbour(other);
		if (p) return;
		neighbour[neighbour.size] = other;
	}
	void removeNeighbour(grafnode* other)
	{
		for (int i=0; i<neighbour.size; i++)
		{
			if (neighbour[i] == other)
			{
				neighbour[i]=neighbour[neighbour.size-1];
				neighbour[neighbour.size]=0;
				neighbour.size--;
				break;
			}
		}
	}
	~grafnode()
	{

	}
	void print()
	{
		printf("n%d: ",this->key);
		if (neighbour.size == 0)
		{
			printf("null");

		}
		for (int i=0; i<neighbour.size; i++)
		{
			printf("%d ",neighbour[i]->key);
		}
		printf("\n");
	}

	void DFS(vector<DFS_info> &info,int &t)
	{
		if (info[key].color==0)
		{
			t=t+1;
			info[key].color=1;
			info[key].d=t;
			
			for (int i=0; i<neighbour.size; i++)
			{
				if (info[neighbour[i]->key].color==0)
				{
					info[neighbour[i]->key].parent = this;
					neighbour[i]->DFS(info,t);
				}
			}

			t=t+1;
			info[key].color=2;
			info[key].f=t;
		}
	}

};

class disjoint
{
public:
	vector<int> data;
	void make_set(int i)
	{
		data[i]=i;
	}
	int find(int i)
	{
		while (data[i]!=i) i=data[i];
		return i;
	}
	void unite(int i,int j)
	{
		data[find(j)]=find(i);
	}
	void compress()
	{
		for (int i=0; i<data.size; i++)
		{
			data[i]=find(i);
		}
	}
	void print()
	{
		for (int i=0; i<data.size;i++)
		{
			printf("%d %d\n",i,find(i));
		}
	}
};

class graf
{
public:
	vector<grafnode*> node;

	graf()
	{
	}

	~graf()
	{
		for (int i=0; i<node.size; i++)
		{
			delete node[i];
		}
	}

	graf(graf&other)
	{
		node = vector<grafnode*>(other.node);
	}

	void addNode(int key=-1)
	{
		grafnode* p = 0;

		if (key!= -1)
			p = findNode(key);

		if (p)
		{
			fprintf(stderr,"Inserting node %d for the second time\n",key);
			return;
		}

		if (key==-1)
		{
			key = node.size;
			while (findNode(key)) key++;
		}

		node[key]=new grafnode(key);
	}

	grafnode* findNode(int key)
	{
		for (int i=0; i<node.size; i++)
		{
			if(node[i]->key == key)
			{
				return node[i];
			}
		}
		//fprintf(stderr,"Node with %d key was not found\n",key);
		return 0;
	}

	int findNodePos(int key)
	{
		for (int i=0; i<node.size; i++)
		{
			if(node[i]!=0 && node[i]->key == key)
			{
				return i;
			}
		}
		fprintf(stderr,"Node with %d key was not found\n",key);
		return -1;
	}

	void deleteNode(int key)
	{
		int p = findNodePos(key);
		if (p!=-1)
		{
			grafnode* pp = node[p];
			for (int i=0; i<node.size; i++)
			{
				node[i]->removeNeighbour(pp);
			}
			delete pp;
			node[p]=node[node.size-1];
			node.size--;
			return;
		}
		fprintf(stderr,"Can't delete non-existing node\n",key);
	}

	void addEdge(int keya, int keyb)
	{
		grafnode* p0,*p1; 
		p0 = findNode(keya);
		p1 = findNode(keyb);
		if (p0 && p1)
		{
			p0->addNeighbour(p1);
		}
		else
		{
			fprintf(stderr,"Trying to add edge between non-existing nodes\n");
		}
	}

	void removeEdge(int keya, int keyb)
	{
		grafnode* p0,*p1; 
		p0 = findNode(keya);
		p1 = findNode(keyb);
		if (p0 && p1)
		{
			p0->removeNeighbour(p1);
		}
		else
		{
			fprintf(stderr,"Trying to remove edge between non-existing nodes\n");
		}
	}
	void print()
	{
		printf("Graf @%x with has %d nodes\n",this,node.size);
		for(int i=0; i<node.size; i++)
		{
			node[i]->print();
		}
	}

	void conv_neorientat()
	{
		for (int i=0; i<node.size; i++)
		{
			for (int j=0; j<node[i]->neighbour.size; j++)
			{
				node[i]->neighbour[j]->addNeighbour(node[i]);
			}
		}
	}

	disjoint getConnexGroups()
	{
		disjoint s;
		for (int i=node.size-1; i>=0; i--)
		{
			s.make_set(i);
		}
		for (int i=0; i<node.size; i++)
		{
			vector<grafnode*> &q = node[i]->neighbour;
			for (int j=0; j<q.size; j++)
			{
				s.unite(node[i]->key,q[j]->key);
			}
		}
		return s;
	}

	multitree BFS()
	{
		vector<grafnode*> queue;
		vector<multinode*> queue2;

		multitree result;
		result.root = new multinode(-1);

		vector<int> color(node.size);

		for (int i=0; i<node.size; i++)
		{
			grafnode *current = node[i];

			if(color[current->key]==0)
			{
				
				int queue_head=0;
				int queue_tail=0;
				queue[queue_tail] = node[i];
				queue2[queue_tail++] = result.root->add(current->key);
			
				color[current->key]=1;

				while(queue_head!=queue_tail)
				{
					grafnode *p = queue[queue_head];
					multinode *pp = queue2[queue_head++];

					printf("BFS %d\n",p->key);

					for (int j=0; j<p->neighbour.size; j++)
					{
						grafnode* n = p->neighbour[j];
						if (color[n->key]==0)
						{
							pp->add(n->key);
							queue[queue_tail++]=n;
							color[n->key]=1;
						}
					}
					color[p->key]=2;
				}
			}
		}
		return result;
	}

	vector<DFS_info> DFS()
	{
		vector<DFS_info> info;
		int t = 0;
		for (int i=0; i<node.size; i++)
		{
			node[i]->DFS(info,t);
		}
		return info;
	}
};


void main()
{
	graf G;
	for (int i=0; i<6; i++)
	{
		G.addNode(i);
	}

	//G.deleteNode(5);

	/*for (int i=6; i<10; i++)
	{
		G.deleteNode(i);
	}*/

	for (int i=0; i<7; i++)
	{
		int x=0,y=0;
		while(x==y)
		{
			x = rand()%6;
			y = rand()%6;
		}
		G.addEdge(x,y);
	}

	G.print();

	vector<DFS_info> dfs_info = G.DFS();

	for (int i=0; i<dfs_info.size; i++)
	{
		printf("key:%d\tc:%d\td:%d\tf:%d\tp:",i,dfs_info[i].color,dfs_info[i].d,dfs_info[i].f);
		if(dfs_info[i].parent)
			printf("%x\n",dfs_info[i].parent->key);
		else
			printf("null\n");

	}

	//tree edges g.parent==x
	//back edges x.d>y.d && x.f<y.f
	//forward edges x.d<y.d && x.f>y.f
	//cross edges others

	for (int i=0; i<G.node.size; i++)
	{
		grafnode* p = G.node[i];
		for (int j=0; j<p->neighbour.size; j++)
		{
			grafnode* q = p->neighbour[j];
			if (p&&q)
			{
				printf("e:%d->%d\t",p->key,q->key);
				if(dfs_info[q->key].parent==p)
					printf("is tree edge\n");
				else if(dfs_info[p->key].d > dfs_info[q->key].d && dfs_info[p->key].f < dfs_info[q->key].f)
					printf("is back edge\n");
				else if(dfs_info[p->key].d < dfs_info[q->key].d && dfs_info[p->key].f > dfs_info[q->key].f)
					printf("is forward edge\n");
				else 
					printf("is cross edge\n");
			}
		}
	}

}