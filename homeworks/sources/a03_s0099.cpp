#include <conio.h>
#include <stdio.h>
#include <math.h>
#include "Profiler.h"

Profiler profiler("Heap&Quick");

/*Run time analysis:
 Quicksort:
	-Best case: O(n*log(n)) 
	-Avarage case: O(n*log(n))
	-Worst case:
 HeapSort:
	-Best case:
	-Avarage case: O(n*log(n));
	-Worst case: 
	*/


void copyArray(int arr[],int aux[],int n)
{
	for (int i = 0; i< n ;i++)
	{
		aux[i] = arr[i];
	}
}

void maxHeapify(int arr[], int i,int heapsize,int n)
{
	int largest,temp;
	profiler.countOperation("Heap-comp",n,0);
	profiler.countOperation("Heap-ass",n,0);
	int l = 2*i;
	int r = 2*i+1;
	profiler.countOperation("Heap-comp",n,2);
	if (l <= heapsize && arr[l] > arr[i])
	{
		largest = l;
	}
	else{
		largest = i;
	}
	if (r <= heapsize && arr[r] > arr[largest])
	{
		largest = r;
	}
	if (largest != i)
	{
		temp=arr[i];
		arr[i]=arr[largest];
		arr[largest]=temp;
		profiler.countOperation("Heap-ass",n,3);
		maxHeapify(arr,largest,heapsize,n);
	}
}

void buildHeapBU(int arr[],int n)
{
	for(int i = n/2;i >= 1;i--)
	{
		maxHeapify(arr,i,n,n);
	}
}

void heapSort(int arr[],int n)
{
	int temp;
	buildHeapBU(arr,n);
	for(int i = n;i>1;i--)
	{
		temp=arr[1];
		arr[1]=arr[i];
		arr[i]=temp;
		profiler.countOperation("Heap-ass",n,3);
		maxHeapify(arr,1,i-1,n);
	}
}

int partition(int arr[],int p,int r,int n)
{
	int temp;
	int x = arr[r];
	int i = p-1;
	for (int j = p; j < r;j++)
	{
		profiler.countOperation("Quick-comp",n,1);
		if (arr[j] <= x)
		{
			i = i + 1;
			profiler.countOperation("Quick-ass",n,3);
			temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			
		}
	}
	profiler.countOperation("Quick-ass",n,3);
	temp=arr[i+1];
	arr[i+1]=arr[r];
	arr[r]=temp;
	return i+1;
}

int bestPartition(int arr[],int p,int r,int n)
{
	int x = (r+p)/2;
	int temp;
	temp = arr[r];
	arr[r]=arr[x];
	arr[x]=temp;
	profiler.countOperation("Quick-ass",n,3);
	return partition(arr,p,r,n);
}

void quickSortBest(int arr[],int p,int r,int n){
	static int q;
	if (p < r){
		q = bestPartition(arr,p,r,n);
		quickSortBest(arr,p,q-1,n);
		quickSortBest(arr,q+1,r,n);
	}
}

void quickSort(int arr[],int p,int r,int n)
{
	static int q;
	if (p < r)
	{
		q = partition(arr,p,r,n);
		quickSort(arr,p,q-1,n);
		quickSort(arr,q+1,r,n);
	}
}

void printArray(int arr[],int n){
	printf("\n");
	for(int i = 1; i<=n;i++){
		printf(" %d",arr[i]);
	}
}

int main()
{
	int arr [10000] ;
	int aux [10000];
	FillRandomArray(arr,20,0,100,false,0);
	printf("original array:");
	printArray(arr,19);
	copyArray(arr,aux,20);
	printf("\nafter heapsort:");
	heapSort(aux,19);
	printArray(aux,19);
	copyArray(arr,aux,20);
	printf("\noriginal array:");
	printArray(aux,19);
	quickSort(aux,1,19,19);
	printf("\nafter quicksort:");
	printArray(aux,19);
	
	/*for (int i =100;i < 10001; i+=100)
	{
		FillRandomArray(arr,i+1,0,i,false,2);
		printf("%d",i);
		quickSort(arr,1,i,i);
	}*/
	/*for (int i = 100;i < 10001;i += 100)
	{
		for(int j = 0;j < 5;j++)
		{
			FillRandomArray(arr,i+1);
			copyArray(arr,aux,i+1);
			heapSort(aux,i);
			quickSort(arr,1,i,i);
		}
	}
	profiler.addSeries("Heapsort","Heap-comp","Heap-ass");
	profiler.addSeries("Quicksort","Quick-comp","Quick-ass");
	
	profiler.createGroup("All average","Heapsort","Quicksort");
	profiler.createGroup("Assignments average","Heap-ass","Quick-ass");
	profiler.createGroup("Comparisons average","Heap-comp","Quick-comp");
	profiler.showReport();*/
	getch();
}