/************************************
*                                   *
*       ***ANONIM*** ***ANONIM***                *
*       Group ***GROUP_NUMBER***                 *
*       Assignment 3                *
*       Heapsort ***ANONIM*** Quicksort      *
*                                   *
************************************/

#include<iostream>
#include<conio.h>
#include"Profiler.h"

using namespace std;
Profiler profiler("demo");

static int ASS,CMP;

int partition(int a[], int p, int r)
{
	int x = a[r];
		ASS++;
	int i = p-1;
	int aux;
	for(int j=p;j<=r-1;j++)
	{
			CMP++;
		if(a[j]<=x)
		{
			i++;
			aux = a[i];
			a[i] = a[j];
			a[j] = aux;
				ASS+=3;
		}
	}
	aux = a[i+1];
	a[i+1] = a[r];
	a[r] = aux;
		ASS+=3;
	return i+1;
}
void quickSort(int a[], int p, int r)
{
	if(p<r)
	{
		int q = partition(a,p,r);
		quickSort(a,p,q-1);
		quickSort(a,q+1,r);
	}
}

int parent(int i)
{
	return i/2;
}
int left(int i)
{
	return 2*i;
}
int right(int i)
{
	return 2*i+1;
}

void maxHeapify(int a[], int i, int n)
{
	int l = left(i);
	int r = right(i);
	int largest = i;

	if(l <= n)
	{
		CMP++;
		if(a[l]>a[i])
			largest = l;
	}
	if(r <= n)
	{
		CMP++;
		if( a[r]>a[largest])
			largest = r;
	}
	if(largest != i)
	{
		int aux = a[i];
		a[i] = a[largest];
		a[largest] = aux;
		ASS+=3;
		maxHeapify(a,largest,n);
	}
}
void buildMaxHeap(int a[], int n)
{
	for(int i=n/2;i>=1;i--)
		maxHeapify(a,i,n);
}
void heapSort(int a[], int n)
{
	buildMaxHeap(a,n);
	for(int i=n;i>=2;i--)
	{
		int aux = a[i];
		a[i] = a[1];
		a[1] = aux;
		ASS+=3;
		n--;
		maxHeapify(a,1,n);
	}
}

int main()
{
	

	int a[10005],aux[10005];
	int qCMP,qASS,hCMP,hASS;

	//Demo

	/*int testa[100], testaux[100],testn;
	cin>>testn;
	for(int i=1;i<=testn;i++)
	{
		cin>>testa[i];
		testaux[i]=testa[i];
	}
	heapSort(testa, testn);
	cout<<"Heap Sort:"<<endl;
	for(int i=1;i<=testn;i++)
		cout<<testa[i]<<"  ";
	cout<<endl<<"Quick Sort:"<<endl;
	quickSort(testaux,1,testn);
	for(int i=1;i<=testn;i++)
		cout<<testaux[i]<<"  ";
	cout<<endl;
	getch();*/


	for(int n=100;n<=10000;n+=100)
	{
		qCMP = hCMP = qASS = hASS = 0;
		for(int j=1;j<=5;j++) 
		{
			FillR***ANONIM***omArray(a,n,0,10000,true,0);
			
			for(int i=1;i<=n;i++)
				aux[i]=a[i];
			CMP = ASS = 0;
			quickSort(aux,1,n);
			qCMP += CMP;
			qASS += ASS;

			for(int i=1;i<=n;i++)
				aux[i]=a[i];
			CMP = ASS = 0;
			heapSort(aux,n);
			hCMP += CMP;
			hASS += ASS;
		}
		qCMP /= 5;
		qASS /= 5;
		hCMP /= 5;
		hASS /= 5;
		cout<<n<<endl;
		profiler.countOperation("Heap Sort",n,hASS+hCMP);
		profiler.countOperation("Quick Sort",n,qASS+qCMP);
	}
	profiler.createGroup("AVG","Heap Sort","Quick Sort");
	profiler.showReport();
	return 0;
}