#include "stdio.h"
#include "stdlib.h"
#include "Profiler.h"

Profiler profiler("demo");

#define White 0
#define Gray 1
#define Black 2
#define V_max 10000
#define E_max 60000
#define inf 1000000
 
typedef struct _tree{
int color;
//struct _tree *parent; 
int parent;
int d;
int f;
int key;
}Tree_NODE;

typedef struct _NODE{
	int key;
	struct _NODE *next;
}NODE;

typedef struct _graph{
	int n,m;
}Graph;

NODE *adj[V_max];
Tree_NODE graph[V_max];
int edges[E_max];
int times=0;
Tree_NODE vect[100];

int count;

void generate(int n,int m){
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE *nod=new NODE;
		nod->key=b;
		nod->next=adj[a];
		adj[a]=nod;
	}
}

void DFS_Visit(Graph *g,Tree_NODE *u){
	times++;
	profiler.countOperation("operatii",count,1);
	u->d=times;
	u->color=Gray;
	NODE *v=adj[u->key];
	while(v!=NULL){
		profiler.countOperation("operatii",count,1);
		if(graph[v->key].color==White){
			profiler.countOperation("operatii",count,1);
			graph[v->key].parent=u->key; 
			DFS_Visit(g,&graph[v->key]);
		}
		if(v!=NULL)
				v=v->next;
	}
	u->color=Black;
	times++;
	u->f=times;
}

void DFS(Graph *g){
	for(int i=0;i<g->n;i++){
		graph[i].color=White;
		graph[i].parent=NULL;
		graph[i].key=i;
	}
	times=0;
	for(int i=0;i<g->n;i++){
		profiler.countOperation("operatii",count,1);
		if(graph[i].color==0)
			DFS_Visit(g,&graph[i]);
	}
}
void afisAdj(int n){
	
	for(int i=0;i<n;i++){
		NODE *v=adj[i];
		printf("%d",i);
		while(v!= NULL){
			printf("->");
			printf("%d",v->key);
			if(v!=NULL)
			{	
				v=v->next;
			}
		}
		printf("\n");
	}
}
void sortareTopologica(int *n){
	int t=*n;
	Tree_NODE aux;
	for(int i=0;i<t;i++){
		vect[i]=graph[i];
	}
	for(int i=1;i<t-1;i++){
		for(int j=0;j<t-i;j++){
			if(vect[j].d>vect[j+1].d){
				aux=vect[j];
				vect[j]=vect[j+1];
				vect[j+1]=aux;
			}
		}
	}
	for(int i=0;i<t;i++){
		printf("%d[%d]->",vect[i].key,vect[i].d);
	}

}
void afismuchii(int n){
	bool ok=true;
	for(int i=0;i<n;i++){
		NODE *v=adj[i];
		while(v!=NULL){
			Tree_NODE x=graph[i];
			Tree_NODE y=graph[v->key];
			printf("%d",graph[i].key);
			printf("->%d",graph[v->key].key);
			if(y.parent==x.key) printf(" tree edge");
			else if((x.d<y.d)&&(y.d<y.f)&&(y.f<x.f))  printf(" forward edge");
			else if((y.d<=x.d)&&(x.d<x.f)&&(x.f<=y.f)){ok=false; printf(" back edge");}
			else if((y.d<y.f)&&(y.f<x.d)&&(x.d<x.f)) printf(" cross edge");
			v=v->next;
			printf("\n");
		}
	}
	if(ok) sortareTopologica(&n);
}
void print(Tree_NODE*s){
	printf("%d->%d cul=%d d=%d f=%d\n",s->key,s->key,s->color,s->d,s->f);
}

void main(){
	Graph *g;
	g=(Graph*)malloc(sizeof(Graph));
	//generate(5,9);
	//g->n=5;
	//g->m=9;
	//
	//DFS(g);
	//afisAdj(g->n);
	//for(int i=0;i<g->n;i++)
	//	//printf("\ndrum de la %d la %d ",graph[i].key,graph[j].key);
	//print(&graph[i]);
	//afismuchii(g->n);

	//for(int nrM=1000;nrM<=5000;nrM+=100){
	//	count=nrM;
	//	g->n=100;
	//	g->m=nrM;
	//	generate(100,nrM);
	//	DFS(g);
	//}
	//profiler.showReport();

	for(int nrV=100;nrV<=200;nrV+=10){
		count=nrV;
		g->n=nrV;
		g->m=9000;
		generate(nrV,9000);
		DFS(g);
	}
	profiler.showReport();
}