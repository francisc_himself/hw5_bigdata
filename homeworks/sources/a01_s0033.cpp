#include <stdio.h>
#include <conio.h>
#include <cstdlib>
#include <time.h>

#define MAX_SIZE 10000

void sortare_bule (int x[], int n, int &nr_atr, int &nr_comp)
{
	nr_atr = 0;
	nr_comp = 0;
	int j, aux, ok = 0;
	while (ok == 0){
		ok = 1;
		for (j = 1; j <= n - 1; j++){
			nr_comp++;
			if (x[j] > x[j+1]){
				aux = x[j];
				x[j] = x[j+1];
				x[j+1] = aux;
				nr_atr++;
				ok = 0;
			}
		}
	}
}


void selectie (int x[], int n, int &nr_atr, int &nr_comp)
{
	nr_atr = 0;
	nr_comp = 0;
	int i, j, imin, aux;
	for (i = 1; i <= n - 1; i++)
	{
		imin = i;
		for (j = i + 1; j <= n; j++)
		{
			nr_comp++;
			if (x[j] < x[imin])
				imin = j;
		}
		aux = x[i];
		x[i] = x[imin];
		x[imin] = aux;
		nr_atr++;
	}
}

void insertie (int x[], int n, int &nr_atr, int &nr_comp)
{
	int i, j, k, a;
	for (i = 2; i <= n; i++)
	{
		a = x[i];
		j = 1;
		nr_comp++;
		while (x[j] < a)
		{
			j = j + 1;
			nr_comp++;
		}
		for (k = i; k >= j + 1; k--)
		{
			x[k] = x[k-1];
			nr_atr++;
		}
		x[j] = a;
		nr_atr++;
	}
}

void salvare(int s[], int n, int d[])
{
	int j;
	for (j = n; j <= n; j++)
            d[j] = s[j];
}

int main()
{

	FILE *f;
	int n = 100, j, i, a[MAX_SIZE], b[MAX_SIZE], atr[4], comp[4];
	f = fopen("tema.csv","w");//dechidere fisier in modul write
	s***ANONIM*** (time(NULL));
	fprintf(f, "n,bubble_best_atr,bubble_best_comp,bubble_best_atr+comp,selectie_best_atr,selectie_best_comp,selectie_best_atr+comp,insertie_best_atr,insertie_best_comp,insertie_best_atr+comp,bubble_medie_atr,bubble_medie_comp,bubble_medie_atr+comp,selectie_medie_atr,selectie_medie_comp,selectie_medie_atr+comp,insertie_medie_atr,insertie_medie_comp,insertie_medie_atr+comp,bubble_worst_atr,bubble_worst_comp,bubble_worst_atr+comp,selectie_worst_atr,selectie_worst_comp,selectie_worst_atr+comp,insertie_worst_atr,insertie_worst_comp,insertie_worst_atr+comp\n");//prima linie din tabel cu numele coloanelor

	for (n = 100; n <= 10000; n = n + 100)
	{
		atr[1] = 0;
		atr[2] = 0;
		atr[3] = 0;
		comp[1] = 0;//initializare sir
		comp[2] = 0;
		comp[3] = 0;
		fprintf(f, "%d,", n);
		for (i = 1 ; i <= n; i++)
			a[i] = i;


		sortare_bule(a, n, atr[0], comp[0]);//bubble_best
		fprintf(f, "%d,%d,%d,", atr[0], comp[0], atr[0]+comp[0]);
		selectie(a, n, atr[0], comp[0]);//selectie_best
		fprintf(f, "%d,%d,%d,", atr[0], comp[0], atr[0]+comp[0]);
		insertie(a, n, atr[0], comp[0]);//insertie_best
		fprintf(f, "%d,%d,%d,", atr[0], comp[0], atr[0]+comp[0]);
		for (j = 1; j <= 5; j++){
			for (i = 1; i <= n; i++){
				a[i] = ***ANONIM***() % 10000;
				b[i] = a[i];
			}
			sortare_bule(a, n, atr[0], comp[0]);//bubble_medie
			atr[1] = atr[1] + atr[0];
			comp[1] = comp[1] + comp[0];
			salvare(b, n, a);

			selectie(a, n, atr[0], comp[0]);//selectie_medie
			atr[2] = atr[2] + atr[0];
			comp[2] = comp[2] + comp[0];
			salvare(b, n, a);

			insertie(a, n, atr[0], comp[0]);//insertie_medie
			atr[3] = atr[3] + atr[0];
			comp[3] = comp[3] + comp[0];
			salvare(b, n, a);
		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d,%d,%d,", atr[1] / 5, comp[1] / 5, (atr[1] + comp[1]) / 5, atr[2] / 5, comp[2] / 5, (atr[2] + comp[2]) / 5, atr[3] / 5, comp[3] / 5, (atr[3] + comp[3]) / 5);

		for (i = 1; i <= n; i++){
			a[i] = n - i;
			b[i] = n - i;
		}

		sortare_bule(a, n, atr[0], comp[0]);//bubble_worst
		fprintf(f, "%d,%d,%d,", atr[0], comp[0], atr[0]+comp[0]);
		selectie(a, n, atr[0], comp[0]);//selectie_worst
		fprintf(f, "%d,%d,%d,", atr[0], comp[0], atr[0]+comp[0]);
		insertie(a, n, atr[0], comp[0]);//inserare_worst
		fprintf(f, "%d,%d,%d\n", atr[0], comp[0], atr[0]+comp[0]);
	}

	fclose(f);
	getch();
	return 0;

}
