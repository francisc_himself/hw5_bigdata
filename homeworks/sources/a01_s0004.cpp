/*
****ANONIM*** ***ANONIM***
*grupa ***GROUP_NUMBER***
*
*Am avut de implementat 3 metode de sortare si de verificat eficienta fiecarea 
*in cazurile : cel mai favorabil, cel mai defavorabil, mediu statistic.
*
*Am implementat sortarea prin insertie, prin metoda bulelor si prin selectie, fiecare in cate o functie separata
*Pe langa acestea, mai avem 3 generatoare de numere aleatoare, in ordine crescatoare respectiv descrescatoare
*si o functie ce verifica daca un vector este sortat
*
*In main-ul programului avem 3 instructiuni FOR cu ajutorul carora alegem diferite dimensiuni pentru 
*vectorul cu datele de intrare ( intre 100 si 10.000 ), fiecare FOR corespunzand unuia dintre cele 3 cazuri
*in care trebuie testati algoritmii.
*In cazul mediu statistic, masuratorile se repeta de 5 ori, iar in celelalte cate o singura data.
*
*Dupa realizarea sortarilor, vom scrie datele de iesire ( numarul de atribuiri si comparatii pentru fiecare
*metoda in parte ) in fisiere de tip .csv
*
*Ca si concluzii, putem observa ca atat in cazul mediu statistic cat si in cazul cel mai defavorabil,
*metoda bulelor este mai ineficienta decat celelalte doua, care sunt comparabile care si eficienta
*Cazul favorabil este unul foarte particular si rar intalnit in sortarile de zi cu zi.
*
*Complexitati:

Insertie 
- favorabil : O(n)
- mediu : O(n^2)
- defavorabil : O(n^2)

Metoda bulelor
-favorabil O(n)
-mediu O(n^2)
-defavorabil O(n^2)

Selectie
-favorabil O(n^2)
-mediu O(n^2)
-defavorabil O(n^2)


*
*/

#include <stdio.h>
#include <conio.h>
#include  <stdlib.h>
#include <time.h>
#include <math.h>


//generator de numere aleatoare intre 0 si 100.000
void generator_aleator(int* v, int n)
{
	
	int i; //contor
	
	srand (time(NULL));
	for (i=0;i<n;i++)
	{
		
		*(v+i) = rand() % 100000;
	}

}


//generator de numere in ordine crescatoare
void generator_crescator(int* v, int n)
{
	
	
	for (int i=0;i<n;i++)
		*(v+i) =i;
	

}


//generator de numere in ordine descrescatoare
void generator_descrescator(int* v, int n)
{
	
	for (int i=0;i<n;i++)
		*(v+i) =n-i;
	

}



//functie ce verifica daca un vector transmis ca parametru este sau nu sortat
//ok==1 => sortat , ok==0 => nu este sortat
int verificare(int v[], int n)
{
	int i, ok;

	ok=1; // ok=true ( vector sortat )
	for(i=1;i<n;i++)
		if (v[i-1]>v[i]) // in cazul in care gasim doua elemente care nu sunt in ordine, ok=false
			ok=0;

	return ok;
}






//sortare prin insertie
void sortare_insertie(int v[], int n, int &atribuiri, int &comparatii)
{
	int i,j; //contoare
	int aux;	 //variabila intermediara cu ajutorul careia sortam vectorul
	int ok; // variabila de verificare daca vectorul e sortat


	for(i=1;i<n;i++)
	{

		aux=v[i];
		atribuiri++;

		j=i-1;
		//algoritmul propriu-zis
		comparatii++;
		while(j>=0 && v[j]>aux)
		{
			
			v[j+1]=v[j];
			atribuiri++;

			j--;
		}
		v[j+1]=aux;
		atribuiri++;
	}

	/*
	//verificam daca vectorul este sortat
	ok=verificare(v, n); //verificam daca vectorul e sortat

	if (ok==1)
	for(i=0;i<n;i++)
		printf("vectorul este sortat prin insertie");
	else
		printf("vectorul nu e sortat");
		*/
}



//bubblesort
void sortare_bule(int v[], int n, int &atribuiri, int &comparatii)
{
	int i,j; //contoare
	int aux; //variabila auxiliara folosita la interschimbari
	int ok; //variabila de verificare a sortarii
	int sortat=0; //variabila ajutatoare la sortare


	//algoritmul propriu-zis
	for(i=0;i<n-1;i++)
		for(j=i+1;j<n;j++)
		{
			comparatii++;
			if (v[i]>v[j])
			{
				aux=v[i];
				v[i]=v[j];
				v[j]=aux;
				sortat=0;
				atribuiri = atribuiri+3;
			}
		}


	//verificam daca vectorul este sortat
		/*
	ok=verificare(v, n);

	if (ok==1)
	for(i=0;i<n;i++)
		printf("vectorul este sortat prin bubblesort");
	else
		printf("vectorul nu e sortat");

	printf("\n atribuiri: %d, \n comparatii: %d " , atribuiri, comparatii);
	*/
}


//sortarea prin selectie
void sortare_selectie(int v[], int n, int &atribuiri, int &comparatii)
{
	int poz; //variabila auxiliara cu ajutorul careia retinem pozitii din vector
	int aux; //variabila auxiliara cu ajutorul careia facem interschimbari
	int ok; //variabila cu care verificam daca vectorul a fost sortat
	

	//algoritmul propriu-zis
	for(int i=0; i<n-1; i++)
	{
		aux=v[i];
		poz=i;
		atribuiri++;
		for(int j=i+1; j<n; j++)
		{
			comparatii++;
			if (v[j] < aux)
			{
				aux=v[j];
				poz=j;
				atribuiri++;
			}
		}

		v[poz]=v[i];
		v[i]=aux;
		atribuiri=atribuiri+2;
	}

	
	//verficarea sortarii
	/*
	ok=verificare(v, n);

	if (ok==1)
	for(int i=0;i<n;i++)
		printf("vectorul este sortat prin selectie");
	else
		printf("vectorul nu e sortat");
		*/
}





int main()
{
	FILE *f_favorabil, *f_defavorabil, *f_mediu ; // fisierele in care vom scrie rezultatele
	int n; //dimensiune vector
	int v[10000]; //vectorul cu care lucram
	int t[10000], s[10000]; //vectori auxiliari folositi la calcularea datelor in cazul mediu statistic

	int atribuiri_insertie=0;
	int comparatii_insertie=0;

	int atribuiri_bule=0;
	int comparatii_bule=0;

	int atribuiri_selectie=0;
	int comparatii_selectie=0;


	//cream cele 3 fisiere csv
	f_favorabil=fopen("favorabil.csv", "w");
	fprintf(f_favorabil, "n, atribuiri_insertie, comparatii_insertie, suma_insertie, atribuiri_bule, comparatii_bule, suma_bule, atribuiri_selectie, comparatii_selectie, suma_selectie\n");

	f_defavorabil=fopen("defavorabil.csv", "w");
	fprintf(f_defavorabil, "n, atribuiri_insertie, comparatii_insertie, suma_insertie, atribuiri_bule, comparatii_bule, suma_bule, atribuiri_selectie, comparatii_selectie, suma_selectie\n");

	f_mediu=fopen("mediu.csv", "w");
	fprintf(f_mediu, "n, atribuiri_insertie, comparatii_insertie, suma_insertie, atribuiri_bule, comparatii_bule, suma_bule, atribuiri_selectie, comparatii_selectie, suma_selectie\n");


	//cazul cel mai favorabil
	for(n=100;n<=10000;n=n+100)
	{
		//initializare variabile
		atribuiri_insertie=0;
		comparatii_insertie=0;

		atribuiri_bule=0;
	    comparatii_bule=0;

	    atribuiri_selectie=0;
	    comparatii_selectie=0;

		//generarea si sortarea vectorului prin cele 3 metode
		generator_crescator(v, n);
		sortare_insertie(v,n,atribuiri_insertie, comparatii_insertie);
		generator_crescator(v, n);
		sortare_bule(v,n,atribuiri_bule,comparatii_bule);
		generator_crescator(v, n);
		sortare_selectie(v,n,atribuiri_selectie,comparatii_selectie);

		//afisarea rezultatelor
		fprintf(f_favorabil, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n" , n, atribuiri_insertie, comparatii_insertie, comparatii_insertie+atribuiri_insertie,  atribuiri_bule, comparatii_bule, comparatii_bule+atribuiri_bule,   atribuiri_selectie, comparatii_selectie, comparatii_selectie+atribuiri_selectie);

	}


	//cazul cel mai defavorabil
	for(n=100;n<=10000;n=n+100)
	{
		//initializare variabile
		atribuiri_insertie=0;
		comparatii_insertie=0;

		atribuiri_bule=0;
	    comparatii_bule=0;

	    atribuiri_selectie=0;
	    comparatii_selectie=0;

		//generare si sortare numere
		generator_descrescator(v, n);
		sortare_bule(v,n,atribuiri_bule,comparatii_bule);
		generator_descrescator(v, n);
		sortare_insertie(v,n,atribuiri_insertie, comparatii_insertie);
		generator_descrescator(v, n);
		sortare_selectie(v,n,atribuiri_selectie,comparatii_selectie);

		//afisarea datelor
		fprintf(f_defavorabil, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n" , n, atribuiri_insertie, comparatii_insertie, comparatii_insertie+atribuiri_insertie,  atribuiri_bule, comparatii_bule, comparatii_bule+atribuiri_bule,   atribuiri_selectie, comparatii_selectie, comparatii_selectie+atribuiri_selectie);

	}
	


	
	//cazul mediu statistic
	for(n=100;n<=10000;n=n+100)
	{
			//initializare variabile
			atribuiri_insertie=0;
			comparatii_insertie=0;

			atribuiri_bule=0;
			comparatii_bule=0;

			atribuiri_selectie=0;
			comparatii_selectie=0;

			
		//executarea a 5 masuratori pentru acest caz mediu statistic
		for(int j=0;j<5;j++)
		{
			//generare numere
			generator_aleator(v, n);
			//crearea a inca doi vectori identici, pentru a avea aceleasi date de intrare in cele 3 sortari
			for(int k=0; k<n; k++)
			{
				t[k]=v[k];
				s[k]=v[k];
			}

			sortare_bule(v,n,atribuiri_bule,comparatii_bule);
		
			sortare_insertie(t,n,atribuiri_insertie, comparatii_insertie);
		
			sortare_selectie(s,n,atribuiri_selectie,comparatii_selectie);

		}

		//afisarea rezultatelor
		fprintf(f_mediu, "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d \n" , n, atribuiri_insertie/5, comparatii_insertie/5, (comparatii_insertie+atribuiri_insertie)/5,  atribuiri_bule/5, comparatii_bule/5, (comparatii_bule+atribuiri_bule)/5,   atribuiri_selectie/5, comparatii_selectie/5, (comparatii_selectie+atribuiri_selectie)/5);
	}


	
	getch();
	
}