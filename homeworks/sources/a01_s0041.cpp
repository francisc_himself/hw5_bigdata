/* ***ANONIM*** ***ANONIM***-***GROUP_NUMBER***

in cazul favorabil cea mai buna metoda este bubble sort
in cazul defavorabil cea mai buna metoda este insertion sort
 in cazul random cea mai buna metoda este selection sort
*/
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define max 10000
#define ***ANONIM***11 100
#define MAX11 10000
int max1=10000, ***ANONIM***1=100;
int nr_comparari=0, nr_asignari=0;

void random(int v[max],int N1)
{
	int i, nr=0;
	srand(time(NULL));
	for(i=0; i<N1; i++)
	{
		v[i]=***ANONIM***11 + rand() % (MAX11 - ***ANONIM***11);
		printf("\n %d", v[i]);
	}
}

void fav(int a[max],int n)
{
	int i;
	for(i=1;i<=n;i++)
	{
		a[i]=i;
		printf("%d",a[i]);
	}
}

void defav(int a[max],int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		a[i]=n-i;
		printf("%d",a[i]);
	}
}
void Buble_Sort(int n, int a[max])
{
	nr_comparari=0; 
	nr_asignari=0; 
	int i,j,aux;
	for(i=0; i<n-1; i++) 
	for(j=0;j<n-1; j++) 
	{
		nr_comparari=nr_comparari++;
			if (a[j]>a[j+1] )
			 {
				aux=a[j];
				a[j]=a[j+1];
				a[j+1]=aux;
				nr_asignari++;
			}
	}
}
void copie(int n, int a[max], int b[max])
{
	int i;
	for(i=0; i<n; i++)
	{
		a[i]=b[i];
	}
}

void Selectie(int n, int v[max])
{
	int i,l,j,***ANONIM***,aux;
	nr_asignari=0;
	nr_comparari=0;
	for (i=0; i<n-1; i++)
	{	***ANONIM***=v[i];
		l=i;
		for(j=i+1; j<n; j++ )
		{ nr_comparari++;
			if (***ANONIM***>v[j])
			{ 
				***ANONIM***=v[j];
			    l=j;
			}
		}
	aux=v[i];
	v[i]=v[l];
	v[l]=aux;
	nr_asignari++;
	}
}
void Interschimbare(int n, int v[max])
{
	int i,j;
	int x;
	nr_asignari=0; nr_comparari=0;
	for (i=1; i<n; i++)
	{
		x=v[i];
		j=i-1;
		nr_comparari++;
		while ((j>=0)&&(v[j]>x))
		{
			 v[j+1]=v[j];
			 j=j-1;
		}
		v[j+1]=x;
		nr_asignari++;
	}
}

void main(){
	int n=0,i=0,nr1;
	float v1[max];
	int v[max];
	int x[max];
	char a='X',b='X';
	FILE *p=fopen("caz_mediu.csv","w");
	FILE *p1=fopen("caz_fav.csv","w");
	FILE *p2=fopen("caz_defav.csv","w");
while(a!= '2')
    {
         printf("\n 1) Generare aleatoare de numere");
		 printf("\n 2) Exit");
         a=getche();
	 switch (a)
            {
			
		   case '1':
			   {
				   printf("\n 1) Caz favorabil");
				   printf("\n 2) Caz mediu");
				   printf("\n 3) Caz defavorabil");
				   printf("\n 4) Exit");
					b=getche();
				switch (b)
				{
					// Cazul favorabil
					case '1':{


					srand(time(NULL));

					fprintf(p1,"\n ;; bubble;;;;;selectie;;;;;insertie;;;;;");
					fprintf(p1,"\n i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;;i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;;i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;; \n");
					
				   for(i=100;i<=10000; i+=100)
				   {
					   
					   
					   random(v,i);
					   
					

				   Buble_Sort(i,v);
				   fprintf(p1,"%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);


				   
				   Selectie(i,v);
				   fprintf(p1," ;%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);

				   Interschimbare(i,v);
				   fprintf(p1," ;%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);

					
					
				   fprintf(p1,"\n");
			   }
						
							 }
						break;
					
					
					
					case '2':
						{
							//Cazul mediu 
					srand(time(NULL));

					fprintf(p,"\n ;; bubble;;;;;selectie;;;;;insertie");
					fprintf(p,"\n i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;;i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;;i;nr_asignari;nr_comparari;nr_asignari+nr_comparari \n");
					
				   for(i=100;i<10000; i+=100)
				   {
					   random(v,i);
					copie(i,x,v);
				   Buble_Sort(i,x);
				   fprintf(p,"%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);
				   Selectie(i,x);
				   fprintf(p," ;%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);

				   Interschimbare(i,x);
				   fprintf(p," ;%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);
				  fprintf(p,"\n");
			   }
				   break;
						}
					case '3': 
						{
							//Cazul defavorabil
					srand(time(NULL));
					fprintf(p2,"\n ;; bubble;;;;;selectie;;;;;insertie;;;;;");
					fprintf(p2,"\n i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;;i;nr_asignari;nr_comparari; nr_asignari+nr_comparari;;i;nr_asignari;nr_comparari;nr_asignari+nr_comparari \n");
					
				   for(i=100;i<=10000; i+=100)
				   {
					   
					   copie(i,x,v);
					   Buble_Sort(i,v);
					
				   Buble_Sort(i,x);
				   fprintf(p2,"%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);
				   Selectie(i,x);
				   fprintf(p2," ;%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);

				   Interschimbare(i,x);
				   fprintf(p2," ;%d;%d ; %d ; %d;",i,nr_asignari,nr_comparari, nr_asignari+nr_comparari);
				   copie(i,x,v);

					
				   fprintf(p2,"\n");
			   }
						
							 }
						
						break;
					default:    
                        break;
			       }
			   }
		    
        default:   
                    break;
	 }
	 
}
		
	getch();
}