#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
static long atr,comp,atrB,compB;
/*---------------------------------------------PSEUDOCOD----------------------------------------
HEAPIFY(A, i)
1 l <- Stanga(i)
2 r <- Dreapta(i)
3 if l <= heap-size[A] and A[l] > A[i]
4 then maxim <= l
5 else maxim <= i
6 if r <= heap-size[A] and A[r] > A[maxim]
7 then maxim <- r
8 if maxim <> i
19 then exchange A[i] <-> A[maxim]
10 HEAPIFY(A,maxim)

BUILD-HEAP(A)
1 heap-size[A] <- length[A]
2 for i <- [length[A]/2] downto 1
3 do HEAPIFY(A, i)


HEAPSORT(A)
1 BUILD-HEAP(A)
2 for i <- length[A] downto 2
3 do exchange A[1] <-> A[i]
4	 heap-size[A] <- heap-size[A] -1
5	 HEAPIFY(A, 1)

HEAP-INSERT(A,key)
1 heap-size[A] <- heap-size[A] + 1
2 i <- heap-size[A]
3 while i > 1 and A[PARENT(i)] < key
4 do A[i] <- [ A[PARENT(i)]
5	 i <- PARENT(i)
6 A[i] <- key


SortQuick(A[],st,dr)
  
      i <- st;
	  j <- dr;  
      pivot <- A[(st+dr)/2];  
	  while (i<=j) 
	  do
		while (A[i]<pivot) 
		do  
			i<-i+1  
	    while (A[j]> pivot) 
		do 
			j<-j-1;  
        if (i<=j) 
	    then 
		x<- A[i];  
            A[i]<-A[j];  
            A[j]<-x;  
            i<-i+1;  
            j<-j+1;  
      if (st<j) 
      then  
           sortQuick(A,st,j);  
      if (i<dr)
	  then  
           sortQuick(A,i,dr);  
----------------------------------------------------------------------------------------------*/


//-------------------------------------------HEAPSORT------------------------------------------
//Heapify Down
//state este folosit pentru a marca daca heapify folosita pentru constructia bottom up = 1 
//sau daca este folosita in heapsort la aranjare
void heapifyD(int A[],int i,int hp_size,int state) {
int s,d,max,aux;
	s=2*i+1;
	d=2*i+2;
	if (state == 1) (compB)++; 
	comp++;
	if (s<=hp_size && A[s]>A[i]) {
		max=s;
	} else {
		max=i;
	}
	if (state == 1) (compB)++;
	comp++;
	if (d<=hp_size && A[d]>A[max]) {
		max=d;
	}
	if (max!=i) {
		aux=A[i];
		A[i]=A[max];
		A[max]=aux;
		if (state == 1) (atrB)++;
		atr+=3;
		heapifyD(A,max,hp_size,state);
	}

}
//Constriueste Heap Bottom Up
void buildHeapDown(int A[],int n,int *size) {
int heapSize;

	(*size)=n-1;
	heapSize=(*size);
	for (int i=n/2;i>=0;i--) {
		heapifyD(A,i,heapSize,1);
	}
}



//Heapsort cu construire bottom Up
void heapSortDown(int A[],int n){
int aux,heapSize;
	buildHeapDown(A,n,&heapSize);
	for (int i=n-1;i>=1;i--) {
		aux=A[0];
		A[0]=A[i];
		A[i]=aux;
		atr+=3;
		heapSize--;
		heapifyD(A,0,heapSize,0);
	}
	
}

//Heap Insert 

void insHeap(int A[],int x,int *size) {
	int i;
	(*size)++;
	i=(*size);
	
	while (i>0 && A[(int) (i-1)/2]<x) {
		A[i]=A[(int) (i-1)/2];
		i=(int) (i-1)/2;
		comp++;	
		atr++;
		compB++;
		atrB++;
	}
	comp++;
	compB++;
	A[i]=x;
	atr++;
	atrB++;

}
//Construieste Heap Top Down
void buildHeapUp(int A[],int B[],int n,int *size) {
	(*size)=-1; //init heap
	for (int i=0;i<n;i++) {
		insHeap(B,A[i],size);
	}
}

//heapsort cu constructie heap TopDown
void heapSortUp(int A[],int B[],int n) {
	int aux,heapSize;
	buildHeapUp(A,B,n,&heapSize);
	
	for (int i=n-1;i>=1;i--) {
		aux=B[0];
		B[0]=B[i];
		B[i]=aux;
		atr+=3;
		heapSize--;
		heapifyD(B,0,heapSize,0);
	}
	
}

//-------------------------------------------QUICKSORT----------------------------------------------------------- 
void sortQuick(int A[], int st, int dr) {  
      int i = st, j = dr;  
      int x;  
      int pivot = A[(st+dr)/2];  
	 
      while (i<=j) {
		comp++;
		while (A[i]<pivot) {  
			i++;  
			comp++;
			
		}
		comp++;
		while (A[j]> pivot) {  
			j--;  
			comp++;
		}
		
        if (i<=j) {  
			x= A[i];  
            A[i] =A[j];  
            A[j] =x;  
            i++;  
            j--;  
			atr+=3;
        }  
      };  
	  
      if (st<j)  
           sortQuick(A,st,j);  
      if (i<dr)  
           sortQuick(A,i,dr);  
}  
//---------------------------------------- PROCEDURI GENERARE FISIERE -------------------------------------------
//procedura generare fisere cu nr aleatoare
void genRand() {
FILE *f;
int j;
    for (j=1;j<=5;j++) {
        switch (j) {
            case(1): f=fopen("med1.csv","w"); break;
            case(2): f=fopen("med2.csv","w"); break;
            case(3): f=fopen("med3.csv","w"); break;
            case(4): f=fopen("med4.csv","w"); break;
            case(5): f=fopen("med5.csv","w"); break;
            default: break;
        }
        
        srand((int) clock());
        for (int i=1;i<=10000; i++) {
            fprintf(f,"%d ",rand());
        }
        fclose(f);
    }
}



    



//------------------------------------------------EXTRA----------------------------------------------
//procedura pentru citirea numerelor din fisier
void readV(int v[],int n,char file[]) {
	FILE* f;
	f=fopen(file,"r");
    for (int i=0;i<n; i++) {
        fscanf(f,"%d",&v[i]);
    }
    fclose(f);
}

//---------------------------------------------------MAIN--------------------------------------------
int main() {

int A[10001];
int B[10001];

FILE *rand, *build;
    genRand(); //generare fisiere numere aleatoare
    
	
	for (int j=1;j<=3;j++) {
		//acest switch selecteaza fisierele de scriere
        switch (j) {
            case(1): rand=fopen("heapDownRand.csv","w"); build=fopen("bottomUp.csv","w"); break;
            case(2): rand=fopen("heapUpRand.csv","w"); build=fopen("topDown.csv","w");break;
            case(3): rand=fopen("quickRand.csv","w"); break;
            
            default: break;
        }
		for (int n=100;n<=10000;n+=100) {
			
			//caz numere mediu statistic
			atr=0; comp=0;
			atrB=0; compB=0;
			readV(A,n,"med1.csv");
			switch (j) {
				case(1): heapSortDown(A,n); break;
				case(2): heapSortUp(A,B,n); break;
				case(3): sortQuick(A,0,n); break;
				default: break;
			}
			readV(A,n,"med2.csv");
			switch (j) {
				case(1): heapSortDown(A,n); break;
				case(2): heapSortUp(A,B,n); break;
				case(3): sortQuick(A,0,n); break;
				default: break;
			}
			readV(A,n,"med3.csv");
			switch (j) {
				case(1): heapSortDown(A,n); break;
				case(2): heapSortUp(A,B,n); break;
				case(3): sortQuick(A,0,n); break;
				default: break;
			}
			readV(A,n,"med4.csv");
			switch (j) {
				case(1): heapSortDown(A,n); break;
				case(2): heapSortUp(A,B,n); break;
				case(3): sortQuick(A,0,n); break;
				default: break;
			}
			readV(A,n,"med5.csv");
			switch (j) {
				case(1): heapSortDown(A,n); break;
				case(2): heapSortUp(A,B,n); break;
				case(3): sortQuick(A,0,n); break;
				default: break;
			}
			if (j==1 || j==2) {
				fprintf(build,"Loding%d\n",n,atrB/5,compB/5,(atrB+compB)/5);
			}
			fprintf(rand,"Loding%d\n",n,atr/5,comp/5,(atr+comp)/5);
		}
	}



return 0;
}

