/*****ANONIM*** ***ANONIM*** Maria - gr: ***GROUP_NUMBER***
  FA - Assignment 2 - Build Heap Strategies */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <time.h>



int A[10002],B[10001],C[10001],dimB;
int i, n; 
char a[30][30]; //used for pretty print
int bu, td;
int comp_bu, ***ANONIM***_bu;
int ***ANONIM***_td, comp_td;

int parent (int i)
{
	return (i/2);
}

void H_PUSH(int B[],int x)
{
	dimB=dimB+1;
	B[dimB]=x;
	td++;
	int i=dimB;
	while((i>1) && (B[parent(i)]<=B[i]))
	{
		comp_td++;
		int aux;
		aux=B[parent(i)];
		B[parent(i)]=B[i];
		B[i]=aux;
		***ANONIM***_td=***ANONIM***_td+3;
		i=parent(i);
	}
	comp_td++;
}

/**Although the TD method is not as efficient as BU it has the advantage that it ***ANONIM*** sort dynamic allocated (lists). 
That means, you ***ANONIM***d easily add a new element in the array and the heap won't be distroyed (the element will be inserted where it is 
supposed to be).*/

void BuildHeapTD(int A[])
{
	dimB=0; //dimensiunea de la heapul B construit
	comp_td=***ANONIM***_td=0;
	for (i=1; i<=n; i++)
		H_PUSH(B,A[i]);
	for (i=1; i<=n; i++)
		A[i]=B[i];
}
	
int max (int x, int y, int z, int j)
{
if(x>=y)
	{	
		comp_bu++;
		if(x>=z) 
		{
			comp_bu++;
			return j;//x
		}
		else 
		{
			comp_bu++;
			return (2*j);//z
		}
	}
	else 
		if(y>=z) 
		{
			comp_bu++;
			return (2*j+1);//y
		}
	else 
		{
			comp_bu++;
			return (2*j);//z
		}
}

void heapify (int A[], int j)
{
	int indice;
	if((2*j+1<=n) || (2*j<=n))
	     { 
			indice=max(A[j], A[2*j+1], A[2*j], j);
			if (indice!=j) 
			{
					int aux;
					aux=A[j];
					A[j]=A[indice];
					A[indice]=aux;
					***ANONIM***_bu+=3;
					heapify(A, indice);
			}
	}
}

/**The BU method assumes that the array is a binary tree and only afterwords checks if the elements are in the right order for 
a heap structure. */

void BuildHeapBU (int A[])
{
  ***ANONIM***_bu=comp_bu=0;
  for(i=n/2; i>=1; i--)
	  { 
		  heapify (A, i);
	  }
}

//////////////// function for displaying the heap in a "pretty way"

void build_m***ANONIM***ix (int x, int p, int j, int k,int i)
{
	a[p][j]=(char)(((int)'0')+x);
//	printf("%c\n", (char)(x));
	for(int b=1; b<=k; b++)
		 a[p+b][j-b]='/';
    for(int b=1; b<=k; b++)
		 a[p+b][j+b]='\\';
	if(2*i<=n)
		build_m***ANONIM***ix (A[2*i], p+1+k, j-k, k/2, i*2);
	if(2*i+1<=n)
		build_m***ANONIM***ix (A[2*i+1], p+1+k, j+k, k/2, i*2+1);

}

int power (int a)
{
	int i, rez=1;
	 for(i=1;i<=a;i++)
	        rez=rez*2;
	return rez;
}

int detLevel(int n)
{
	int level=0,sum=0;
	while (sum<n)
	{
		sum=sum+power(level);
		level++;
	}
	return level;
}

void pretty_print ()
{
	int level=detLevel(n);
    int p2=power(level); //2^level
    int l=level+p2-2;   // number of lines of the m***ANONIM***ix
    int c=2*p2-1;     // number of colummns of the m***ANONIM***ix
	i=1;
    build_m***ANONIM***ix  (A[1], 1, p2 ,p2/2,1);
	for(i=1;i<=l;i++)
	{
		for(int j=1;j<=c;j++)
			 printf("%c", a[i][j]);
		printf("\n");
	}

}

////////////////////////////////////////////////////

void generate_average (int n)
{
    for(int i=1;i<=n;i++)
	     C[i]=rand()%10000;
}

void main()
{
	/**FILE *f;
	f=fopen("avg.txt", "w");
	fprintf(f,"n TopDownATR TopDownCOMP TopDown BottomUpATR BottomUpCOMP BottomUp\n");
    
	for(n=100; n<=10000; n=n+100)
		  {  
		    int td_tot=0;
			int bu_tot=0;
			int td_comp_tot, td_***ANONIM***_tot, bu_***ANONIM***_tot, bu_comp_tot;
			td_comp_tot=td_***ANONIM***_tot=bu_***ANONIM***_tot=bu_comp_tot=0;
			for(int k=1;k<=5;k++)
			       { 
							comp_bu=***ANONIM***_bu=***ANONIM***_td=comp_td=0; //=td=bu=0;

		        			generate_average(n);
							for(int j=1;j<=n;j++)
								 A[j]=C[j];
							BuildHeapTD(A);
							td_comp_tot+=comp_td;
							td_***ANONIM***_tot+=***ANONIM***_td;
							

							for(int j=1;j<=n;j++)
								 A[j]=C[j];
							BuildHeapBU(A);
							bu_comp_tot+=comp_bu;
							bu_***ANONIM***_tot+=***ANONIM***_bu;
							
			}
		    td_tot=td_comp_tot+td_***ANONIM***_tot; 
			bu_tot=bu_comp_tot+bu_***ANONIM***_tot;
			fprintf(f,"%d %d %d %d %d %d %d\n",n ,td_***ANONIM***_tot/5, td_comp_tot/5, td_tot/5,bu_***ANONIM***_tot/5, bu_comp_tot/5, bu_tot/5);
	}

	fclose(f);
	printf("The End %c", 7);*/

	///////////////////////////////////////////////
	//testing the sortings and the pretty print: 
	s***ANONIM***f("%d", &n);
	for(i=1;i<=n;i++)
		   A[i]=i;
	BuildHeapTD(A);
	printf("\nInput array after TD: \n");
	//generating an ordered (ascending) array 
	for(i=1;i<=n;i++)
            printf("%d ", A[i]);
    printf("\nInput array as a heap: \n");
	pretty_print();
	printf("\n");
	for(i=1;i<=n;i++)
		   A[i]=i;
	BuildHeapBU(A);
	printf("\nInput array after BU: \n");
	for(i=1;i<=n;i++)
            printf("%d ", A[i]);
    printf("\nInput array as a heap: \n");
	pretty_print();
	printf("\n");
	getch(); 
}
