/**
Student: Ciolocoiu 
Group: ***GROUP_NUMBER***

**/

#include <iostream>
#include <stdio.h>
#include <time.h>

long asgn=0, cmp=0;
void bubble(int v[], int n)
{
	int i,j,aux,ok;
	asgn=0;cmp=0;
	do
	{
		ok=0;
		for (i=0;i<n-1;i++)
		{	
			cmp++;
			if (v[i]>v[i+1])
			{
				aux=v[i];
				v[i]=v[i+1];
				v[i+1]=aux;
				ok=1;
				asgn=asgn+3;
			}
		}
	}while(ok==1);
}
void selection(int v[],int n)
{
	int i,j,min,aux;
	asgn=0;cmp=0;
	for (i=0;i<n-1;i++)
	{
		min=i;
		for (j=i+1;j<n;j++)
		{
			cmp++;
			if (v[j]<v[min])
			{
				min=j;					
			}
		}
		if(min!=i)
		{
			asgn=asgn+3;
			aux=v[i];
			v[i]=v[min];
			v[min]=aux;
		}
	}
}

void insertion(int v[], int n)
{
	int i,aux,k;
	asgn=0;cmp=0;
	for(i=1;i<n;i++)
	{
		aux=v[i];
		k=i-1;
		asgn++;
		while(v[k]>aux && k>=0)
			{	
				v[k+1]=v[k];
				k--;
				asgn++;
				cmp++;
			}
		v[k+1]=aux;
		cmp++;
		asgn++;
	}
}


void main()
{
	int v[10000],i,var;
	FILE *f1,*f2,*f3;
	f1=fopen("best.csv","w");
	f2=fopen("avg.csv","w");
	f3=fopen("worst.csv","w");
	fprintf(f1,"n,nrcmpb,nrasgnb,nrcmpb+nrasgnb,nrcmpsel,nrasgnsel,nrcmpsel+nrasgnsel,nrcmpi,nrasgni,nrcmpi+nrasgni\n");
	fprintf(f2,"n,nrcmpb,nrasgnb,nrcmpb+nrasgnb,nrcmpsel,nrasgnsel,nrcmpsel+nrasgnsel,nrcmpi,nrasgni,nrcmpi+nrasgni\n");
	fprintf(f3,"n,nrcmpb,nrasgnb,nrcmpb+nrasgnb,nrcmpsel,nrasgnsel,nrcmpsel+nrasgnsel,nrcmpi,nrasgni,nrcmpi+nrasgni\n");

	for (int n=100;n<=10000;n=n+100)
	 {
	   //best case
			for (int i=0;i<n;i++) v[i]=i;
			bubble(v,n);
			fprintf(f1,"%d,%d,%d,%d,",n,cmp,asgn,asgn+cmp);
			for (int i=0;i<n;i++) v[i]=i;
			selection(v,n);
			fprintf(f1,"%d,%d,%d,",cmp,asgn,asgn+cmp);
			for (int i=0;i<n;i++) v[i]=i;
			insertion(v,n);
			fprintf(f1,"%d,%d,%d\n",cmp,asgn,asgn+cmp);

	  //average case
			long avgba=0,avgia=0,avgsa=0,avgbc=0,avgic=0,avgsc=0;
			int aux1[10000],aux2[10000];
			srand(time(NULL));
			//measuring 5 times
			for(int j=0;j<5;j++) 
			{ 
				for (int i=0;i<n;i++)
				{
						v[i]=rand()%10000;
						aux1[i]=v[i];
						aux2[i]=v[i];
				}
				bubble(v,n);
				avgbc+=cmp;avgba+=asgn;
				selection(aux1,n);
				avgsc+=cmp;avgsa+=asgn;
				insertion(aux2,n);
				avgic+=cmp;avgia+=asgn;
			}
			avgba/=5;
			avgbc/=5;
			avgsa/=5;
			avgsc/=5;
			avgia/=5;
			avgic/=5;
			fprintf(f2,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,avgbc,avgba,avgbc+avgba,avgsc,avgsa,avgsc+avgsa,avgic,avgia,avgic+avgia);

	 //worst case
			for (int i=0;i<n;i++) v[i]=n-1-i;
			bubble(v,n);
			fprintf(f3,"%d,%d,%d,%d,",n,cmp,asgn,asgn+cmp);
			
			for (int i=1;i<n;i++) v[i-1]=i;
			v[n-1]=0;
			selection(v,n);
			fprintf(f3,"%d,%d,%d,",cmp,asgn,asgn+cmp);
			for (int i=0;i<n;i++) v[i]=n-1-i;
			insertion(v,n);
			fprintf(f3,"%d,%d,%d\n",cmp,asgn,asgn+cmp);		
	}
	fclose(f1);
	fclose(f2);
	fclose(f3);
}

