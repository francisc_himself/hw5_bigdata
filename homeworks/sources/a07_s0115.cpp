#include<stdio.h>
#include<conio.h>


int nr;
int P[100];

struct Tree
{
   int val;
   int nrChildren;
   Tree *child[100];
}*A[100],*prim;

struct Bin
{
  int val;
  Bin *brother;
  Bin *son;
}*B[100];

void read()
{
	int i;
	printf("Number of elements "); scanf("%d",&nr);
	for(i=0;i<nr;i++)
		scanf("%d",&P[i]);
}

void display(Tree *p)
{
	int i;
	if(p!=NULL)
	 {
		  if(p->nrChildren==0)
			  printf("\n%d has no children",p->val);
		  else
		  {
				printf("\n%d's sons are:",p->val);
				for(i=1;i<=p->nrChildren;i++)
					 printf(" %d ",p->child[i]->val);
				for(i=1;i<=p->nrChildren;i++)
					 display(p->child[i]);
		  }
	 }
}

void PrettyPrint(int ind,int h)
{
   int i;
   printf("\n");
   for(i=0;i<=3*h;i++)
	   printf(" ");
   printf("%d",ind);

   if(B[ind]->son!=NULL)
	   PrettyPrint(B[ind]->son->val,h+1);
     if(B[ind]->brother!=NULL)
	   PrettyPrint(B[ind]->brother->val,h);
}

void T1()
{
   int i,aux;
   for(i=0;i<nr;i++)
   {
	   if(P[i]==-1)
	   {
	      prim=A[i];
	   }
	   else
	   {
	   aux=++A[P[i]]->nrChildren;
	   A[P[i]]->child[aux]=A[i];
	   }
   }
}

void T2(Tree *p)
{
	int i;
	if(p!=NULL)
	{
		for(i=1;i<=p->nrChildren;i++)
		{
			if(i==1)
				B[p->val]->son=B[p->child[1]->val];
			else
				B[p->child[i-1]->val]->brother=B[p->child[i]->val];

			T2(p->child[i]);
		}
	}
}

void main()
{
	int i;
	read();

	for(i=0;i<=nr;i++)
	{
	   A[i]=new Tree();  //we create the nodes through the multi-way tree
	   A[i]->val=i;
	   A[i]->nrChildren=0;
	   A[i]->child[1]=NULL;


	   B[i]=new Bin();  //we create the nodes from the binary tree
	   B[i]->val=i;
	   B[i]->son=B[i]->brother=NULL;
	}

	T1();
	printf("\nAfter the 1st transformation \n");
	display(prim);

	T2(prim);
	printf("\nAfter the 2nd transformation\n");
	PrettyPrint(prim->val,0);


	getch();

}