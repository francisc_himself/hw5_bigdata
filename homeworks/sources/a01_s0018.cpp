//***ANONIM*** ***ANONIM*** - ***ANONIM***, ***GROUP_NUMBER***
//Tema nr. 1: Analiza ?i Compararea Metodelor Directe de Sortare


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#define NMAX 10000

char fisier_bule_favorabil[50] = "D:\\bule_favorabil.txt";
char fisier_bule_mediu[50] = "D:\\bule_mediu.txt";
char fisier_bule_defavorabil[50] = "D:\\bule_defavorabil.txt";
char fisier_insertie_favorabil[50] = "D:\\insertie_favorabil.txt";
char fisier_insertie_mediu[50] = "D:\\insertie_mediu.txt";
char fisier_insertie_defavorabil[50] = "D:\\insertie_defavorabil.txt";
char fisier_selectie_favorabil[50] = "D:\\selectie_favorabil.txt";
char fisier_selectie_mediu[50] = "D:\\selectie_mediu.txt";
char fisier_selectie_defavorabil[50] = "D:\\selectie_defavorabil.txt";

char fisier_defavorabil[50] = "D:\\defavorabil.txt";

FILE *bf,*bm,*bd,*ifav,*im,*id,*sf,*sm,*sd,*def;



void scrieInFisier(FILE *f,int n, int atr, int comp)
{
	
	fprintf(f,"%d,%d,%d,%d\n",n,atr,comp,atr+comp);
	
}

void bubble(int n,int a[],int& comp, int& atr)

{
int i,j,sortat,x=0;
    j=0;
    do
    {
        sortat=1;
		j=j+1;
        for (i=0;i<n-j;i++){
              comp++;
            if (a[i]>a[i+1])
            {
                sortat=0;
                x=a[i];
                a[i]=a[i+1];
                a[i+1]=x;
                atr=atr+3;
            }
		}
    }
    while (sortat==0);

}


void selectie(int n,int a[],int& comp, int& atr )
{ int x;

  for (int i=0;i<n-1;i++)
  {
    int pozitie=i;
    for (int j=i+1;j<n;j++)
	{
      comp++;
      if (a[j]<a[pozitie])
	  {
        pozitie=j;
	  }
	}
	if(pozitie!=i){
		x=a[i];
		a[i]=a[pozitie];
		a[pozitie]=x;
		atr+=3;
	}
	}
  }

void insertie(int n,int a[],int& comp, int& atr)
{

	int j,x;
    for (int i=0;i<n;i++)
    {
        j=i;
        x=a[i];
        atr++;    
        while ((j>0)&&(x<a[j-1]))
        {
			comp++;  
			atr++;
            a[j]=a[j-1];
            j=j-1;
    }
		atr++;
    a[j]=x;
    }
	
	
}


void genereazaSirCrescator(int n,int a[])
	{
	
		for(int j=1;j<=n;j++)
			a[j]=j;
	}

void genereazaSirDescrescator(int n,int a[])
{
		for(int j=n;j>=1;j--)
			a[j]=n-j;
		
}
void genereazaSirRandom(int n,int a[])
{
	 int ok=1;
	
	srand(time(NULL));
		for(int j=1;j<=n;j++)
		{
					a[j]=rand()%n;	
		}
}




void main(void)
{
	
int a[NMAX];
int comp=0, atr=0,nr_atr_final=0,nr_comp_final=0;
bf = fopen(fisier_bule_favorabil, "w");
bm = fopen(fisier_bule_mediu, "w");
bd = fopen(fisier_bule_defavorabil, "w");
ifav = fopen(fisier_insertie_favorabil, "w");
im = fopen(fisier_insertie_mediu, "w");
id = fopen(fisier_insertie_defavorabil, "w");
sf = fopen(fisier_selectie_favorabil, "w");
sm = fopen(fisier_selectie_mediu, "w");
sd = fopen(fisier_selectie_defavorabil, "w");

def = fopen(fisier_defavorabil, "w");
fprintf(def,"n,bule,selectie,insertie\n");


fprintf(bf,"n,na,nc,na+nc\n");
fprintf(bm,"n,na,nc,na+nc\n");
fprintf(bd,"n,na,nc,na+nc\n");
fprintf(ifav,"n,na,nc,na+nc\n");
fprintf(im,"n,na,nc,na+nc\n");
fprintf(id,"n,na,nc,na+nc\n");
fprintf(sf,"n,na,nc,na+nc\n");
fprintf(sm,"n,na,nc,na+nc\n");
fprintf(sd,"n,na,nc,na+nc\n");

//Cazuri mediu statistice
  for(int i=100;i<=NMAX;i+=200)
  {
		
		for (int k=1;k<=5;k++){
			genereazaSirRandom(i,a);
			bubble(i,a,comp,atr);
			nr_comp_final += comp;
			nr_atr_final += atr;
		}
		scrieInFisier(bm,i,nr_atr_final/5,nr_comp_final/5);


		comp=0;atr=0;
		nr_atr_final=0;nr_comp_final=0;
		for (int k=1;k<=5;k++)
		{
			genereazaSirRandom(i,a);
			selectie(i,a,comp,atr);
			nr_comp_final += comp;
			nr_atr_final += atr;
		}
		scrieInFisier(sm,i,nr_atr_final/5,nr_comp_final/5);


		comp=0;atr=0;
		nr_atr_final=0;nr_comp_final=0;
		for (int k=1;k<=5;k++)
		{
			genereazaSirRandom(i,a);
			insertie(i,a,comp,atr);
			nr_comp_final += comp;
			nr_atr_final += atr;
		}
		scrieInFisier(im,i,nr_atr_final/5,nr_comp_final/5);


		comp=0;atr=0;nr_atr_final=0;nr_comp_final=0;

//Cazuri favorabile
		genereazaSirCrescator(i,a);
		bubble(i,a,comp,atr);
		scrieInFisier(bf,i,atr,comp);

		comp=0;atr=0;
		genereazaSirCrescator(i,a);
		selectie(i,a,comp,atr);
		scrieInFisier(sf,i,atr,comp);

		comp=0;atr=0;
		genereazaSirCrescator(i,a);
		insertie(i,a,comp,atr);
		scrieInFisier(ifav,i,atr,comp);

//Cazuri defavorabile
		
		genereazaSirDescrescator(i,a);
		bubble(i,a,comp,atr);
		scrieInFisier(bd,i,atr,comp);

		comp=0;atr=0;
		genereazaSirDescrescator(i,a);
		selectie(i,a,comp,atr);
		scrieInFisier(sd,i,atr,comp);

		comp=0;atr=0;
		genereazaSirDescrescator(i,a);
		insertie(i,a,comp,atr);
		scrieInFisier(id,i,atr,comp);


}

fclose(bf);
fclose(bm);
fclose(bd);
fclose(ifav);
fclose(im);
fclose(id);
fclose(sf);
fclose(sm);
fclose(sd);
getch();

}
