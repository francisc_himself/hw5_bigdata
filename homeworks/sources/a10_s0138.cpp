/*
	Operatiile de punere si scoatere din coada au complexitatea O(1),deci timpul total dedicat modificarii cozii este O(vf).Timpul alocat scanarii
	listelor de adiacenta este O(muchii),deci timpul total pentru procedura BFS este O(vf+muchii).Am folosit Profiler.h ca si la DFS.In
	cazul in care nu am explicat anumite concepte in cadrul acestui cod,inseamna ca le-am explicat
	in tema cu DFS
*/

#include "Profiler.h"
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include <stdlib.h>

Profiler profiler("BFS");

struct ListaAdiac{
	int vecin;
	struct ListaAdiac *next; //structura pt o lista de adiacenta a unui vf
}Lista;


ListaAdiac **vf;
ListaAdiac *head,*tail;
int parinte[200];
int distanta[200];
int culoare[200];
int n,e;

void creareVect(int n, int m){
	int i,j;
	int muchii[10000];
	int gasit;
	ListaAdiac *p,*newnod;
	vf = (ListaAdiac**)malloc(sizeof(ListaAdiac)*(200));
	for(i = 0; i < n; i++){
		vf[i] = (ListaAdiac*)malloc(sizeof(ListaAdiac));
		vf[i]->vecin = 0;
		vf[i]->next = NULL;
	}
	for (i=0; i<m; i+=2){

		do{	//do-while-ul care construieste muchiile care sunt diferite doua cate doua
			gasit = 0;
			muchii[i]=rand() % (n-2);// un capat al unei muchii
			muchii[i+1]=muchii[i]+rand() % (n-muchii[i]-1)+1; //celalalt capat
			for(j = 0; j<i; j+=2){
				if (muchii[i]==muchii[j] && muchii[i+1]==muchii[j+1]){ //compara daca avem sau nu doua muchii identice
					gasit = 1;
				}
			}
		}while(gasit == 1);
		p = vf[muchii[i]]; //p se pozitioneaza pe lista de adiacenta a nodului muchii[i]
		while(p->next!=NULL){
			p = p->next;
		}
		newnod = (ListaAdiac*)malloc(sizeof(ListaAdiac));
		newnod->next = NULL;
		newnod->vecin = 0;
		p->next = newnod;
		p->vecin = muchii[i+1]; //newnod se pozitioneaza pe lista de adiacenta a vf muchii[i+1],adica a doua extremitate a muchiei orientate ij

	}
}

void enqueue(int x){//functia de adaugare in coada
	ListaAdiac *newnod;
	newnod = (ListaAdiac*)malloc(sizeof(ListaAdiac));
	newnod->vecin = x;
	newnod->next = NULL;

	if(head == NULL){//fiecare lista de adiacenta are capete(santinele)-head si tail
		head = newnod;
		tail = head;
	}
	else{
		tail->next = newnod;
		tail = newnod;
	}
}

int dequeue(){//functia de eliminare din coada
	int x;
	ListaAdiac *p;
	if(head == NULL){
		return -1;
	}
	else{
		x = head->vecin;//  se elimina primul element din lista de adiacenta(cu exceptia head-ului)
		p = head;
		head = head->next;
		free(p);
		return x;
	}
}

void prettyPrint(int n){
	int i,j;
	for (i = 0; i < n; i++){
		for(j = 0; j < distanta[i]; j++){
			printf("    ");
		}
		printf("%d\n",i);
	}
}

void BFS(int start,int a){//functia BFS
	//int i;
	ListaAdiac *v;
	int u;

	culoare[start] = 1;//se coloreaza,fixeaza distanta si parintele radacinii
	distanta[start] = 0;
	parinte[start] = -1;
	head = NULL;
	tail = NULL;
	enqueue(start);
	if(a==0){//a verifica daca suntem in cazul nr_vf=constant sau nr_muchii=constant
			profiler.countOperation("noperations",n,3);
		}
		else{
			profiler.countOperation("eoperations",e,3);
		}
	while (head != NULL){//atata timp cat coada e nenula
		u = dequeue();//se scoate primul element(varf) de adiacenta din coada
		v = vf[u]; //ne pozitionam pe lista de adiacenta a nodului u
		while(v->next!=NULL){ //parcurgem toata lista de adiacenta a lui u
			if (culoare[v->vecin]==0){// daca nodul nu a fost vizitat(e alb)
				culoare[v->vecin] = 1;//nodul devine gri
				distanta[v->vecin] = distanta[u] + 1;//creste distanta fata de distanta nodului u cu 1
				parinte[v->vecin] = u;//u devine parintele nodului curent
				enqueue(v->vecin);//se pune v in coada
				if(a==0){
			profiler.countOperation("noperations",n);
		}
		else{
			profiler.countOperation("eoperations",e);
		}
			}
			v = v->next;//ne pozitionam pe urmatorul nod din lista de adiacenta a lui u
		}
		culoare[u] = 2; // dupa ce sau vizitat toti vecinii lui u acesta devine negru
	}
}

int main(){
	int  i;//teste BFS pentru nr_vf=7 si nr_muchii=15
	//n = 7;
	//e = 15;
	//creareVect(n,2*e);
	//for(i = 0; i < n; i++){
	//	culoare[i] = 0;
	//	distanta[i] = MAXINT;
//		parinte[i] = -1;
//	}
//	for(i = 0; i < n; i++){
//		if(culoare[i]==0){
	//		BFS(i,1);
	//	}
	//}
	n = 100;//cazul in care variaza nr_muchii iar nr_vf=100
	for (e = 1000; e <= 4950; e+=100){//pe site e trebuie sa ajunga la 5000,dar nu e posibil deoarece un graf complet cu 100 de vf are 4950 de muchii
		printf("e %d\n",e);
		creareVect(n,2*e);
		for(i = 0; i < n; i++){
			culoare[i] = 0;
			distanta[i] = MAXINT;
			parinte[i] = -1;
		}

		for(i = 0; i < n; i++){
			if(culoare[i]==0){
				BFS(i,1);

			}
		}

	}

	for(n = 100; n <= 200; n += 10){//cazul in care n variaza de la 100 la 200,iar e e constant
		printf("n %d\n",n);
		creareVect(n,9000);
		for(i = 0; i < n; i++){
			culoare[i] = 0;
			distanta[i] = MAXINT;
			parinte[i] = -1;
		}
		for(i = 0; i < n; i++){
			if(culoare[i]==0){
				BFS(i,0);
			}
		}
	}
	profiler.showReport();

	//getch();
	//prettyPrint(n);
}
