




#include "Profiler.h"
#include <conio.h>
#include <stdio.h>
#define MAX -32000
Profiler profiler("Sorting");
	
void selectsort(int* a, int max_elem)//cod selectsort
{
	profiler.countOperation("selectSort_assign", max_elem, 0);
	int i, j;
	int min, temp;
	for(i=1; i<max_elem-1; i++){
		min = i;
		for(j=i+1; j<max_elem; j++){
			if(a[j] < a[min]){
				min = j;
			}
			profiler.countOperation("selectSort_comp", max_elem);
			profiler.countOperation("selectSort_total", max_elem);
		}
		if(min != i){
		    temp = a[i];
			a[i] = a[min];
			a[min] = temp;
			profiler.countOperation("selectSort_assign", max_elem, 3);
			profiler.countOperation("selectSort_total", max_elem, 3);
			
		}
	}
}

void insertsort(int* a, int max_elem)//cod pt insertsort
{
	profiler.countOperation("insertSort_assign", max_elem, 1);
	int i,j, temp;
	a[0] = MAX;
	for(i=2;i<max_elem-1;i++)
	{
		temp = a[i];
		profiler.countOperation("insertSort_assign", max_elem);
		profiler.countOperation("insertSort_total", max_elem);
		j = i-1;
		while(temp < a[j]){
			profiler.countOperation("insertSort_comp", max_elem);
			profiler.countOperation("insertSort_assign", max_elem);
			profiler.countOperation("insertSort_total", max_elem, 2);
			a[j+1] = a[j];
			j--;
		}
		profiler.countOperation("insertSort_comp", max_elem);
		profiler.countOperation("insertSort_assign", max_elem);
		profiler.countOperation("insertSort_total", max_elem,2);
		a[j+1] = temp;
	}
	
}

void bubblesort(int *a, int max_elem)//cod bubblesort
{
	profiler.countOperation("bubbleSort_assign", max_elem, 0);
	int i, aux, ok=0;
	while( ok == 0){
		ok = 1;
		for(i=1;i<max_elem-1;i++){
			if(a[i]>a[i+1]){
				profiler.countOperation("bubbleSort_comp", max_elem);
				profiler.countOperation("bubbleSort_total", max_elem);
				ok = 0;
				aux = a[i];
				a[i] = a[i+1];
				a[i+1] = aux;
				profiler.countOperation("bubbleSort_assign", max_elem, 3);
				profiler.countOperation("bubbleSort_total", max_elem, 3);
			}
			profiler.countOperation("bubbleSort_comp", max_elem);
			profiler.countOperation("bubbleSort_total", max_elem);
		}
	}
}


void main(){
	int p,i,j,k;
	int v[10000], a[10000];    	
//	for(k=0;k<5;k++){
	for(i=100;i<=10000 ;i=i+500)
	{
		
		//for(j=1;j<=i;j++) // acesta este cel mai bun caz                                               
			//v[j]=j;
		//v[1] = i;// pt toti algoritmii   
		//FillRandomArray(a, i);
		//for(p=1;p<=i;p++){
			//v[p]=a[p];
		//}
		for(j=0;j<=i;j++){
			v[j]=j+1;
		}
		v[1]=i;
			
		
		
		selectsort(v,i);
		//for(p=1;p<=i;p++){
		//	v[p]=a[p];
		//}
		for(j=i;j>=0;j--)//cel mai rau caz pt bubble si insertsort
				v[j]=i-j;
		
		insertsort(v,i);
		//for(p=1;p<=i;p++){
		//	v[p]=a[p];
		//}
		for(j=i;j>=0;j--)//cel mai rau caz pt bubble si insertsort
				v[j]=i-j;
		
	    bubblesort(v,i);
		printf("Gata pt %d\n", i);
		}
	//}

	profiler.createGroup("Comparatii", "bubbleSort_comp", "insertSort_comp", "selectSort_comp");
	profiler.createGroup("Asignari", "bubbleSort_assign", "insertSort_assign", "selectSort_assign");
	profiler.createGroup("total operatii","bubbleSort_total", "insertSort_total", "selectSort_total");
	profiler.showReport();
	printf("Gata!");
	
	getch();
}
