// sort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#define NMAX 50000
int a[NMAX],Asel=0,Csel=0,Aint=0,Cint=0,Ains=0,Cins=0;
// Funtie pentru sortare prin interschimbare
void interschimbare(int b[NMAX],int n)
{
    int j,k;
    int sortat=0; //flag pentru a afla daca sirul este sortat, in cazul cel mai favorabil complexitatea este mai buna
    while(sortat==0)
    {
        sortat=1;
        for(j=1; j<n-1; j++)
        {
            Cint++;
            if(b[j]>b[j+1]) //daca elementul urmator este mai mic, atunci interschimbam
            {
                k=b[j];
                b[j]=b[j+1];
                b[j+1]=k;
                Aint+=3;
                sortat=0; //setam sortat pe 0
            }
        }
    }
}
// Functie pentru sortare prin selectie
void selectie(int b[NMAX],int n)
{
    int i,j,k,minim=0,i_min=1;;
    for(i=1; i<=n-1; i++)
    {
        minim=b[i]; //setam b[i] ca minim
        Asel++;
        i_min=i; //salvam indicele miniumului
        for(j=i+1; j<=n; j++)
        {
            Csel++;
            if(b[j]<minim) //verificam daca gasim un element mai mic decat mainimul
            {
                minim=b[j]; // daca am gasit setam noul minim
                Asel++;
                i_min=j;//reactualizam indicele minim
            }


        }
        b[i_min]=b[i]; //interschimbam elementul minim cu elementul gasit
        b[i]=minim;
        Asel+=2;
    }
}
//Functie pentru sortare prin insertie
void insertie(int b[NMAX], int n)
{
    int i,x,j,k;
    for(i=2; i<=n; i++)
    {
        x=b[i]; //luam elementele pe rand
        Ains++;
        j=1;
        Cins++;
        while(b[j]<x) // cautam un element mai mare decat elementul initial
        {
            j++;
            Cins++;
        }
        for(k=i; k>=j+1; k--)
        {
            b[k]=b[k-1]; // mutam elementele la dreapta
            Ains++;
        }
        b[j]=x; //punem elementul nostru pe pozitia unde trebuie sa se afle
        Ains++;
    }
}
// Generam cazul favorabil -- elemente in ordin crescatoare
void best_gen(int nr)
{
    int i;
    for(i=0; i<nr; i++)
        a[i]=i;
}
// Generam cazul defavorabil --
void worst_gen(int nr)
{
    int i;
    for(i=nr-1; i>=0; i--)
        a[i]=nr-i;
}
// General cazul general cu numere random
void avg_gen(int nr)
{
    int i;
    for(i=0; i<nr; i++)
        a[i]=rand();
}
int main()
{
    //apelez functiile de sortare pentru fiecare caz, pe mai multe siruri
    //apelez functiile de sortare pentru fiecare caz, pe mai multe siruri
    int i;
    FILE *f1,*f2,*f3;
    f1=fopen("best.csv", "w");
    fprintf(f1,"n,Asel,Csel,Asel+Csel,Aint,Cint,Aint+Cint,Ains,Cins,Ains+Cins");

    for(i=100; i<=10000; i=i+100)
    {
        best_gen(i);
        Asel=0,Csel=0,Aint=0,Cint=0,Ains=0,Cins=0;
        interschimbare(a,i);
        selectie(a,i);
        insertie(a,i);
        fprintf(f1,"\n%d, %d, %d, %d, %d,%d, %d, %d, %d, %d", i,Asel,Csel,Asel+Csel,Aint,Cint,Aint+Cint,Ains,Cins,Ains+Cins);
    }
    fclose(f1);
    f2=fopen("worst.csv", "w");
    fprintf(f2,"n,Asel,Csel,Asel+Csel,Aint,Cint,Aint+Cint,Ains,Cins,Ains+Cins");
    for(i=100; i<=10000; i=i+100)
    {
        worst_gen(i);
        Asel=0,Csel=0,Aint=0,Cint=0,Ains=0,Cins=0;
        interschimbare(a,i);
        selectie(a,i);
        insertie(a,i);
        fprintf(f2,"\n%d, %d, %d, %d, %d,%d, %d, %d, %d, %d", i,Asel,Csel,Asel+Csel,Aint,Cint,Aint+Cint,Ains,Cins,Ains+Cins);
    }
    fclose(f2);
    f3=fopen("avg.csv", "w");
    fprintf(f3,"n,Asel,Csel,Asel+Csel,Aint,Cint,Aint+Cint,Ains,Cins,Ains+Cins");
    for(i=100; i<=10000; i=i+100)
    {
        Asel=0,Csel=0,Aint=0,Cint=0,Ains=0,Cins=0;
        for(int j=0; j<5; j++)
        {
            avg_gen(i);
            interschimbare(a,i);
            selectie(a,i);
            insertie(a,i);
            Asel=Asel/5;
            Csel=Csel/5;
            Aint=Aint/5;
            Cint=Cint/5;
            Ains=Ains/5;
            Cins=Cins/5;
        }
        fprintf(f3,"\n%d, %d, %d, %d, %d,%d, %d, %d, %d, %d", i,Asel,Csel,Asel+Csel,Aint,Cint,Aint+Cint,Ains,Cins,Ains+Cins);
    }
    fclose(f3);
    return 0;
}
