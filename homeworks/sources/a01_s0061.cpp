#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <time.h>
#define maxElemente 10000 //max elemente pt vectori


int atribuiri,comparatii,lungTemp;
int increment=500; //incrementul pentru sortari in functie de maximul de elemente pt un vector
FILE*f1; //In fisierul 'f1' se vor afisa graficele pentru cazul favorabil
FILE*f2; //In fisierul 'f2' se vor afisa graficele pentru cazul mediu statistic
FILE*f3; //In fisierul 'f3' se vor afisa graficele pentru cazul defavorabil
int v1[maxElemente];
int v2[maxElemente];
int v3[maxElemente];


void generare_aleatoare(int aux)
{
for (int i=0; i<aux; i++)
	{
		v1[i] = v1[i-1]+rand()%aux;//caz favorabil
		v2[i] = rand() % aux;//caz mediu statistic
		v3[i] = (v3[aux-i]-1)%aux; //caz defavorabil
		printf("\nv1=%d ",v1[i]);
	}

}
/**
*   Sortarea BubbleSort se face in ordine crescatoare prin interschimbari repetate
*/
void BubbleSort(int vector[maxElemente], int varAfisata,FILE*f)
{
	int i, j, aux;

	for (i=lungTemp-1; i>=0; i--)
		for (j=1; j<=i; j++)
			if (vector[j-1]>vector[j])
			{   comparatii++;
				aux=vector[j-1];
				vector[j-1]=vector[j];
				vector[j]=aux;
				atribuiri=atribuiri+3;
			}
	/* Intrucat graficele se vor face pe categorii avem grija de fiecare data sa afisam doar
	variabila pentru graficul curent*/
	if(varAfisata==1) fprintf(f,"%d,",comparatii);
	else if(varAfisata==2) fprintf(f,"%d,",atribuiri);
	else if(varAfisata==3) fprintf(f,"%d,",comparatii+atribuiri);
	else if(varAfisata==4) 
		for (i=1;i<=lungTemp;i++)
		printf(" %d ",vector[i]);
}

void SortareSelectie(int vector[maxElemente], int varAfisata,FILE*f)
{
	int i, j, aux, ***ANONIM***;
	for (i=0; i<lungTemp-1; i++)
	{
		***ANONIM***=i;
		atribuiri++;
		for (j=i+1; j<lungTemp; j++)
			if (vector[j]<vector[***ANONIM***])
			{***ANONIM***=j;
        comparatii++;}
		    aux=vector[***ANONIM***];
		    vector[***ANONIM***]=vector[i];
		    vector[i]=aux;
		atribuiri= atribuiri+3;
	}
	/* Intrucat graficele se vor face pe categorii avem grija de fiecare data sa afisam doar
	variabila pentru graficul curent*/
	if(varAfisata==1) fprintf(f,"%d,",comparatii);
	else if(varAfisata==2) fprintf(f,"%d,",atribuiri);
	else if(varAfisata==3) fprintf(f,"%d,",comparatii+atribuiri);
	else if(varAfisata==4)  
		for (i=1;i<=lungTemp;i++)
		printf(" %d ",vector[i]);
}

void SortareInserare(int vector[maxElemente], int varAfisata,FILE*f)
{
	int i,j,init;

	for (i=1; i<lungTemp; i++)
	{
		init=vector[i]; atribuiri++;
		j=i;
		while ((j>0) && (vector[j-1]>init))
		{	comparatii++;
			vector[j]=vector[j-1];
			atribuiri++;
			j--;
		}
		comparatii++;
		vector[j]=init;
		atribuiri++;
	}
	/* Intrucat graficele se vor face pe categorii avem grija de fiecare data sa afisam doar
	variabila pentru graficul curent*/
	if(varAfisata==1) fprintf(f,"%d,",comparatii);
	else if(varAfisata==2) fprintf(f,"%d,",atribuiri);
	else if(varAfisata==3) fprintf(f,"%d,",comparatii+atribuiri);
	else if(varAfisata==4)  
		for (i=1;i<=lungTemp;i++)
		printf(" %d ",vector[i]);
}

int main(void)
{
	printf("lucreeeez....");
    int comparatii=0, atribuiri=0;
	
	
	//exit(1);//generate random
	//do selection sort
	// print

    f1=fopen("CazFavorabil.csv","w");
    f2=fopen("CazMediuStatistic.csv","w");
    f3=fopen("CazDefavorabil.csv","w");
		generare_aleatoare(lungTemp);
	 BubbleSort(v2,4,f1);
	 printf("\n");
     
	SortareInserare(v3,4,f1);
     printf("\n");
     
	 SortareSelectie(v1,4,f1);
	
	printf("\n");
    //generare grafic
    for (lungTemp=100;lungTemp<=200;lungTemp+=increment)
	{
        /*Caz favorabil*/
        comparatii = atribuiri = 0;
        generare_aleatoare(lungTemp);
            //comparatii
            BubbleSort(v1,1,f1);
			fprintf(f1,"\n");
            SortareInserare(v1,1,f1);
			fprintf(f1,"\n");
            SortareSelectie(v1,1,f1);
			fprintf(f1,"\n");
            //atribuiri
            BubbleSort(v1,2,f1);
			fprintf(f1,"\n");
            SortareInserare(v1,2,f1);
			fprintf(f1,"\n");
            SortareSelectie(v1,2,f1);
			fprintf(f1,"\n");
            //comparatii+atribuiri
            BubbleSort(v1,3,f1);
			fprintf(f1,"\n");
            SortareInserare(v1,3,f1);
			fprintf(f1,"\n");
            SortareSelectie(v1,3,f1);
			fprintf(f1,"\n");

        /*Caz mediu statistic*/
        comparatii = atribuiri = 0;
            //comparatii
            BubbleSort(v2,1,f2);
			fprintf(f2,"\n");
            SortareInserare(v2,1,f2);
			fprintf(f2,"\n");
            SortareSelectie(v2,1,f2);
            fprintf(f2,"\n");
			//atribuiri
            BubbleSort(v2,2,f2);
			fprintf(f2,"\n");
            SortareInserare(v2,2,f2);
            fprintf(f2,"\n");
			SortareSelectie(v2,2,f2);
            fprintf(f2,"\n");
			//comparatii+atribuiri
            BubbleSort(v2,3,f2);
			fprintf(f2,"\n");
            SortareInserare(v2,3,f2);
            fprintf(f2,"\n");
			SortareSelectie(v2,3,f2);
			fprintf(f2,"\n");

        /*Caz defavorabil*/
        comparatii = atribuiri = 0;
            //comparatii
            BubbleSort(v3,1,f3);
			fprintf(f3,"\n");
			SortareInserare(v3,1,f3);
            fprintf(f3,"\n");
			SortareSelectie(v3,1,f3);
            fprintf(f3,"\n");
			//atribuiri
            BubbleSort(v3,2,f3);
			fprintf(f3,"\n");
            SortareInserare(v3,2,f3);
            fprintf(f3,"\n");
			SortareSelectie(v3,2,f3);
            fprintf(f3,"\n");
			//comparatii+atribuiri
            BubbleSort(v3,3,f3);
			fprintf(f3,"\n");
            SortareInserare(v3,3,f3);
            fprintf(f3,"\n");
			SortareSelectie(v3,3,f3);
            fprintf(f3,"\n");  
	}

	printf("\nAm ter***ANONIM***at!! ");
	getch();
return 0;
}
/**
Concluzii:
Complexitatile metodelor de sortare sunt:
BubbleSort: - Cazul favorabil O(n)
            - Cazul mediu statistic O(n^2)
            - Cazul defavorabil O(n^2)
SortareInserare: - Cazul favorabil O(n)
                 - Cazul mediu statistic O(n^2)
                 - Cazul defavorabil O(n^2)
SortareSelectie: - Cazul favorabil O(n^2)
                 - Cazul mediu statistic O(n^2)
                 - Cazul defavorabil O(n^2)

*/
