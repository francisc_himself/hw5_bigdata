// Author: ***ANONIM*** ***ANONIM***
// Group: ***GROUP_NUMBER***

//You are required to implement correctly and efficiently 3 direct sorting methods 
//(Bubble Sort, Insertion Sort � using either linear or binary insertion and Selection Sort)

#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "conio.h"


long a,c; // a for assignments and c for comparisons
int* b;

void resetVar(){
	a=0;c=0;
}

//Selection Sort is stable (because preservers order of elements
// the current implementation is correct, having followed the pseudocode of the alg accurately
// and the efficiency for comparisons is: O(n^2) 
// and the effieciency for assignments is:Best case: 0
//										  Worst case: O(n)
int* selectionSort(int* input, int n)
{
    int i,j=0;
	int minI=0;
	int* x;
	x=(int*)malloc(sizeof(int)*n);
	for(i=0;i<n;i++)
		x[i]=input[i];

    for(i=0; i<n-1; i++){
		minI=i;
        for(j=i+1;j<n;j++){
            c++;
            if(x[j] < x[minI])
               {
                minI = j;
               }
			
			}
		//c++;
		if(minI != i){
			int aux=x[i];
			x[i]=x[minI];
			x[minI]=aux;
			a=a+3;
		}
	}
    return x;
}

//Insertion Sort is stable (because preservers order of elements
// the current implementation is correct, having followed the pseudocode of the alg accurately
// and the efficiency for comparisons is: Best case: O(n)
//										  Worst case: O(n^2)
// and the effieciency for assignments is:Best case: O(n)
//										  Worst case: O(n^2)
int* insertionSort(int* input,int n){
	int i=0,j=0,k=0,aux=0;
	int* x;
	x=(int*)malloc(sizeof(int)*n);
	for(i=0;i<n;i++)
		x[i]=input[i];

	for(i=0;i<n;i++){
		aux=x[i];
		k=i-1;
		a++; // from aux=x[i]
		while(x[k]>aux && k>=0){
			x[k+1]=x[k];
			a++;
			k--;
			c++; // from x[j]>aux
		}
		c++; // another from x[j]>aux that is not counted 
		x[k+1]=aux;
		a++;
	}
	return x;
}

//Bubble Sort is stable (because preservers order of elements
// the current implementation is correct, having followed the pseudocode of the alg accurately
// and the efficiency for comparisons is: Best case: O(n)
//										  Worst case: O(n^2)
// and the effieciency for assignments is:Best case: 0
//										  Worst case: O(n)
int* bubbleSort(int* input, int n){
	bool sorted=false;
	int i=0,aux=0;
	int* x;
	x=(int*)malloc(sizeof(int)*n);
	for(i=0;i<n;i++)
		x[i]=input[i];

	while(sorted==false){
		sorted=true;
		for(i=0;i<n-1;i++){
			c++; // x[i]>x[i+1];
			if(x[i]>x[i+1]){
				aux=x[i];
				x[i]=x[i+1];
				x[i+1]=aux;
				sorted=false;
				a=a+3;
			}
		}
	}
	return x;
}

void generateWorst(int*x, int n)
{
    int i=0;
    for(i=n-1;i>=0;i--){
        x[i]=n-i;
	}
}

void generateBest(int *x, int n){
	int i=0;
	for(i=0;i<n;i++){
		x[i]=i;
	}
}

void generateAverage(int *x, int n){
	int i=0;
	for(i=0;i<n;i++)
		x[i]=rand()%100;
}

void testSortMethods(){
	int i=0;
	int *input,*result;
	// Algorithms testing
	
	int testingDim=10;
	input=(int*)malloc(sizeof(int)*testingDim);
	generateAverage(input, testingDim);
	for(i=0;i<testingDim;i++)
		printf("%d ", input[i]);

	result=bubbleSort(input,testingDim);
	printf("\nBubbleSort\n");
	for(i=0;i<testingDim;i++)
		printf("%d ", result[i]);
	printf("\nSelectionSort\n");
	result=selectionSort(input,testingDim);
	for(i=0;i<testingDim;i++)
		printf("%d ", result[i]);
	printf("\nInsertionSort\n");
	result=insertionSort(input,testingDim);
	for(i=0;i<testingDim;i++)
		printf("%d ", result[i]);
}

void main(){
	//if the sorting methods require testing, a testSortMethods has been supplied with
	testSortMethods();
	int *input;
	int dimension,i=0,j=0;
	FILE *fpBest, *fpAverage, *fpWorst;

	fpBest = fopen("Best.csv","w");
	fpWorst =fopen("Worst.csv","w");
	fpAverage =fopen("Average.csv","w");
	fprintf(fpBest,"Dimension,Bubble A,Bubble C,Bubble A+C,Insertion A,Insertion C,Insertion A+C,Selection A,Selection C,Selection A+C\n");
	fprintf(fpAverage,"Dimension,Bubble A,Bubble C,Bubble A+C,Insertion A,Insertion C,Insertion A+C,Selection A,Selection C,Selection A+C\n");
	fprintf(fpWorst,"Dimension,Bubble A,Bubble C,Bubble A+C,Insertion A,Insertion C,Insertion A+C,Selection A,Selection C,Selection A+C\n");

	for(dimension=100;dimension<=10000;dimension=dimension+100){

		input=(int*)malloc(sizeof(int)*dimension);
		printf("dimension:%d\n",dimension);
		
		generateBest(input,dimension);
		resetVar();
		bubbleSort(input,dimension);
		fprintf(fpBest,"%d,%lu,%lu,%lu,",dimension,a,c,(a+c));
		resetVar();
		insertionSort(input,dimension);
		fprintf(fpBest,"%lu,%lu,%lu,",a,c,(a+c));
		resetVar();
		selectionSort(input,dimension);
		fprintf(fpBest,"%lu,%lu,%lu\n",a,c,(a+c));

		generateWorst(input,dimension);
		resetVar();
		bubbleSort(input,dimension);
		fprintf(fpWorst,"%d,%lu,%lu,%lu,",dimension,a,c,(a+c));
		resetVar();
		insertionSort(input,dimension);
		fprintf(fpWorst,"%lu,%lu,%lu,",a,c,(a+c));
		resetVar();
		selectionSort(input,dimension);
		fprintf(fpWorst,"%lu,%lu,%lu\n",a,c,(a+c));

		resetVar();
		for(i=0;i<5;i++){
			generateAverage(input,dimension);
			bubbleSort(input,dimension);
		}
		fprintf(fpAverage,"%d,%lu,%lu,%lu,",dimension,a/5,c/5,(a+c)/5);
		resetVar();
		for(i=0;i<5;i++){
			generateAverage(input,dimension);
			insertionSort(input,dimension);
		}
		fprintf(fpAverage,"%lu,%lu,%lu,",a/5,c/5,(a+c)/5);
		resetVar();
		for(i=0;i<5;i++){
			generateAverage(input,dimension);
			selectionSort(input,dimension);
		}
		fprintf(fpAverage,"%lu,%lu,%lu\n",a/5,c/5,(a+c)/5);


	}
	fclose(fpBest);
	fclose(fpAverage);
	fclose(fpWorst);
	
	getch();
}