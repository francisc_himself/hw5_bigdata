#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N_MIN_NODURI 100
#define N_MAX_NODURI 200
#define N_MIN_MUCHII 1000
#define N_MAX_MUCHII 5000

int nrOperatii;

typedef struct lista_adiacenta
{
    int vecin;
    int tip_muchie;//0 tree, 1 back, 2 farward / cross
    struct lista_adiacenta *urm;
    struct lista_adiacenta *prec;
} lista_adiacenta;

typedef struct nod_graf
{
    lista_adiacenta *lista;
    int key;
    int d;
    int f;
    int culoare;
    struct nod_graf *precedent;
} nod_graf;

typedef struct lista
{
    nod_graf *nod;
    struct lista *urm;
    struct lista *prec;
}lista;

void inserare_lista(lista **l, nod_graf *nod)
{
    lista *aux = NULL;
    if(*l == NULL)
    {
        *l = (lista*)malloc(sizeof(lista));
        (*l)->nod = nod;
        (*l)->prec = NULL;
        (*l)->urm = NULL;
    }
    else
    {
        aux = (lista*)malloc(sizeof(lista));
        aux->nod = nod;
        aux->prec = NULL;
        aux->urm = *l;
        (*l)->prec = aux;
        *l = aux;
        //printf("%d ", (*l)->nod->key);
    }
}

void dfs_visit(nod_graf *graf[], int nod, int time, lista **l, int *ok)
{
    lista_adiacenta *aux;
    nrOperatii += 3;
    time++;
    graf[nod]->d = time;
    graf[nod]->culoare++;
    aux = graf[nod]->lista;
    nrOperatii++;
    while(aux != NULL)
    {
        nrOperatii += 2;
        if(graf[aux->vecin]->culoare == 0)//alg => tree
        {
            aux->tip_muchie = 0;//tree
            nrOperatii++;
            graf[aux->vecin]->precedent = graf[nod];
            dfs_visit(graf, aux->vecin, time, l, ok);
        }
        else if(graf[aux->vecin]->culoare == 1)//gri => back
        {
            aux->tip_muchie = 1;//back
            *ok = 0;//arborele nu e topologic
        }
        else//negru
        {
            aux->tip_muchie = 2;//farward / cross
        }
        aux = aux->urm;
    }
    nrOperatii += 3;
    graf[nod]->culoare++;
    inserare_lista(l, graf[nod]);
    time++;
    graf[nod]->f = time;
}

void dfs(nod_graf *graf[], int nr_noduri, lista **l, int *ok)
{
    int i, time = 0;
    for(i = 1; i <= nr_noduri; i++)
    {
        graf[i]->culoare = 0;
        graf[i]->precedent = NULL;
        graf[i]->d = nr_noduri;
        graf[i]->f = nr_noduri;
    }
    for(i = 1, *ok = 1; i <= nr_noduri; i++)
    {
        nrOperatii++;
        if(graf[i]->culoare == 0)
            dfs_visit(graf, i, time, l, ok);
    }
}

int creare_muchie(nod_graf **nod, int muchie)
{
    lista_adiacenta *aux = (*nod)->lista, *aux1;
    if (aux != NULL)
    {
        while(aux->urm != NULL)
        {
            if(aux->vecin == muchie)
                return 0;
            aux = aux->urm;
        }
        aux1 = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
        aux1->urm = NULL;
        aux1->prec = aux;
        aux1->vecin = muchie;
        aux1->tip_muchie = 0;
        aux->urm = aux1;
    }
    else
    {
        aux1 = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
        aux1->urm = NULL;
        aux1->prec = NULL;
        aux1->vecin = muchie;
        aux1->tip_muchie = 0;
        (*nod)->lista = aux1;
    }
    return 1;
}

void dealocare_memorie_lista(lista **l)
{
    while (*l != NULL)
    {
        if((*l)->urm != NULL)
        {
            (*l) = (*l)->urm;
            free((*l)->prec);
            (*l)->prec = NULL;
        }
        else//ultimul element
        {
            free(*l);
            *l = NULL;
        }
    }
}

void dealocare_memorie_lista_adiacenta_nod(nod_graf **nod)
{
    lista_adiacenta *aux = (*nod)->lista;
    while (aux != NULL)
    {
        while(aux->urm != NULL)
        {
            aux = aux->urm;
        }
        //verific daca nu cumva am de-aface cu primul element
        //daca nu
        if (aux->prec != NULL)
        {
            aux->prec->urm = NULL;
            free(aux);
            aux = (*nod)->lista;
        }
        //daca da
        else
        {
            free(aux);
            (*nod)->lista = NULL;
            aux = NULL;
        }
    }
}

int main()
{
    nod_graf *graf[N_MAX_NODURI+1];
    int n, m, i, j, vecin, ok, ok1;
    lista *l = NULL;
    lista_adiacenta *aux;
    //ok - variabila care ne spune daca avem sau nu sortare topologica: 1 - da, 0 - nu
    FILE *f = fopen("rezultate.csv", "w");
    fprintf(f, "V,E,nrOp\n");

    srand(time(NULL));

    /***EXEMPLU***/
    for(i = 1; i <= N_MAX_NODURI; i++)
    {
        graf[i] = (nod_graf*)malloc(sizeof(nod_graf));
        graf[i]->culoare = 0;
        graf[i]->d = 0;
        graf[i]->f = 0;
        graf[i]->key = i;
        graf[i]->lista = NULL;
        graf[i]->precedent = NULL;
    }
    /***initializare graf***/
    graf[1]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[1]->lista->vecin = 2;
    graf[1]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[1]->lista->prec = NULL;
    graf[1]->lista->urm->vecin = 4;
    graf[1]->lista->urm->urm = NULL;
    graf[1]->lista->urm->prec = graf[1]->lista;

    graf[2]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[2]->lista->vecin = 5;
    graf[2]->lista->urm = NULL;
    graf[2]->lista->prec = NULL;

    graf[3]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[3]->lista->vecin = 5;
    graf[3]->lista->urm = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[3]->lista->prec = NULL;
    graf[3]->lista->urm->vecin = 6;
    graf[3]->lista->urm->urm = NULL;
    graf[3]->lista->urm->prec = graf[3]->lista;

    //comentare pentru exemplu arbore topologic
    graf[4]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[4]->lista->vecin = 2;
    graf[4]->lista->urm = NULL;
    graf[4]->lista->prec = NULL;

    graf[5]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[5]->lista->vecin = 4;
    graf[5]->lista->urm = NULL;
    graf[5]->lista->prec = NULL;

    //comentare pentru exemplu de arbore topologic
    graf[6]->lista = (lista_adiacenta*)malloc(sizeof(lista_adiacenta));
    graf[6]->lista->vecin = 6;
    graf[6]->lista->urm = NULL;
    graf[6]->lista->prec = NULL;
    /***initializare graf***/
    n = 6;
    dfs(graf, n, &l, &ok);
    printf("listele de adiacenta si etichetele muchiilor:\n");
    printf("(etichetele: 0 - tree, 1 - back, 2 - farward/cross)\n");
    for(i = 1; i <= n; i++)
    {
        printf("%d: ",i);
        aux = graf[i]->lista;
        while(aux != NULL)
        {
            printf("%d(%d); ", aux->vecin, aux->tip_muchie);
            aux = aux->urm;
        }
        printf("\n");
    }
    if(ok == 0)
        printf("arborele nu e topologic\n");
    else
    {
        printf("arborele este topologic\n");
        lista *l1 = l;
        while(l1 != NULL)
        {
            printf("%d ", l1->nod->key);
            l1 = l1->urm;
        }
    }
    dealocare_memorie_lista(&l);
    for(i = 1; i <= n; i++)
        dealocare_memorie_lista_adiacenta_nod(&graf[i]);
    /***EXEMPLU***/

    n = 100;
    m = N_MIN_MUCHII;
    for(m = N_MIN_MUCHII; m <= N_MAX_MUCHII; m = m + 100)
    {
        nrOperatii = 0;
        for(i = 1; i <= n; i++)
        {
            for(j = 1; j <= m/n; j++)
            {
                vecin = rand()%n+1;
                if(vecin != i)
                {
                    ok1 = creare_muchie(&graf[i], vecin);
                    if(ok1 == 0)
                    {
                        j--;
                    }
                }
                else
                    j--;
            }
        }
        dfs(graf, n, &l, &ok);
        dealocare_memorie_lista(&l);
        for(i = 1; i <= n; i++)
        {
            dealocare_memorie_lista_adiacenta_nod(&graf[i]);
        }
        fprintf(f, "%d,%d,%d\n", n, m, nrOperatii);
    }

    m = 9000;
    for(n = 100; n <= 200; n = n + 10)
    {
        nrOperatii = 0;
        for(i = 1; i <= n; i++)
        {
            for(j = 1; j <= m/n; j++)
            {
                vecin = rand()%n+1;
                if(vecin != i)
                {
                    ok = creare_muchie(&graf[i], vecin);
                    if(ok == 0)
                    {
                        j--;
                    }
                }
                else
                    j--;
            }
        }
        dfs(graf, n, &l, &ok);
        dealocare_memorie_lista(&l);
        for(i = 1; i <= n; i++)
        {
            dealocare_memorie_lista_adiacenta_nod(&graf[i]);
        }
        fprintf(f, "%d,%d,%d\n", n, m, nrOperatii);
    }
    return 0;
}
