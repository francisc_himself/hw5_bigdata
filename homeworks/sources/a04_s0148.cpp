#include <stdio.h>
#include <stdlib.h>
#include<conio.h>


typedef struct Nod
{
	long inf;
	struct Nod *urm;
}nod;

nod *a[10000],*listaSolutia[10001];

long atr = 0,comp =0;
long dimHeap,dim;
int k;
long n;

void heapify(nod *c[], int i)
{
	int l,r,min;
	long aux;


	l=2*i;
	r=2*i+1;

	comp ++;
	if (l<=dimHeap && c[l]->inf<c[i]->inf)
		min=l;
	else
		min=i;


	comp +=1;
	if (r<=dimHeap && c[r]->inf<c[min]->inf)
		min=r;

	if (min!=i)
	{

		c[0]=c[i];
		c[i]=c[min];
		c[min]=c[0];
		atr +=3;
		heapify(c,min);
	}

}
void bottomup(nod *c[],int n)
{
	int i;
	dimHeap=n;
	for (i=dimHeap/2;i>=1;i--)
		heapify(c,i);
}

void cazuri(nod *a[],int k, FILE *g)
{
	fprintf(g,"n,atr/5,comp/5,(atr+comp)/5)\n");
	for (int i=100;i<=10000;i=i+100)
	{


		for (int j=1;j<=5;j++)
		{


			for (int l=1;l<=k;l++)
			{
				atr+=3;
				a[l]=new nod;
				a[l]->inf=rand() % (k*2);
				a[l]->urm=NULL;
				nod *primul=new nod;
				primul=a[l];
				for (int m=2;m<=i/k;m++)
				{
					nod *q=new nod;
					q->inf=primul->inf+rand() % (k*2);
					q->urm=NULL;
					primul->urm=q;
					primul=q;
					atr+=4;
				}
			}
			int nr =1;
			dim=k;
			bottomup(a,dim);
			while (dim!=0)
			{
				listaSolutia[nr]=new nod;
				listaSolutia[nr]->inf=a[1]->inf;
				a[1]=a[1]->urm;
				listaSolutia[nr]->urm=NULL;
				nr++;
				atr+=4;
				comp++;

				if (a[1]==NULL)
				{
					a[1]=a[dim];
					dim--;
					dimHeap--;
					atr+=1;
				} 
				heapify(a,1);
			}
		}
		fprintf(g,"%d,%ld,%ld,%ld\n",i,atr/5,comp/5,(atr+comp)/5);
	}
}

void caz_n(nod *a[],int n1, FILE *g)
{
	fprintf(g,"n,atr/5,comp/5,(atr+comp)/5)\n");
	for (k=10;k<=500;k=k+10)
	{

		for (int j=1;j<=5;j++)
		{

			for (int l=1;l<=k;l++)
			{
				atr+=3;
				a[l]=new nod;
				a[l]->inf=rand() % (k*2);
				a[l]->urm=NULL;
				nod *primul=new nod;
				primul=a[l];
				for (int m=2;m<=n1/k;m++)
				{
					atr+=4;
					nod *q=new nod;
					q->inf=primul->inf+rand() % (k*2);
					q->urm=NULL;
					primul->urm=q;
					primul=q;
				}
			}
			int nr =1;
			dim=k;
			bottomup(a,dim);
			while (dim!=0)
			{
				listaSolutia[nr]=new nod;
				listaSolutia[nr]->inf=a[1]->inf;
				a[1]=a[1]->urm;
				listaSolutia[nr]->urm=NULL;
				nr++;
				atr+=3;
				comp++;
				if (a[1]==NULL)
				{
					a[1]=a[dim];
					dim--;
					dimHeap--;
					atr+=1;
				}

				heapify(a,1);
			}
		}
		fprintf(g,"%d,%ld,%ld,%ld\n",k,atr/5,comp/5,(atr+comp)/5);
	}
}


void main()
{
	FILE *f1,*f2,*f3,*f;
	f=fopen("n.csv","w");
	f1=fopen("f1.csv","w");
	f2=fopen("f2.csv","w");
	f3=fopen("f3.csv","w");
	int i,j,nr=1,dim;


	k=4;
	n=20;
	for (i=1;i<=k;i++)
	{

		a[i]=new nod;
		a[i]->inf=rand() % (k*2)+1;
		a[i]->urm=NULL;
		nod *first=new nod;
		first=a[i];
		printf("%d ",a[i]->inf);
		for (j=2;j<=n/k;j++)
		{
			nod *p=new nod;
			p->inf=1+first->inf+rand() % (k*2);
			p->urm=NULL;
			first->urm=p;
			first=p;
			printf("%d ",p->inf);   
		}
		printf("\n");
	}

	dim=k;
	bottomup(a,dim);

	while (dim!=0)
	{
		listaSolutia[nr]=new nod;
		listaSolutia[nr]->inf=a[1]->inf;
		a[1]=a[1]->urm;
		listaSolutia[nr]->urm=NULL;
		nr++;

		if (a[1]==NULL)
		{
			a[1]=a[dim];
			dim--;
			dimHeap--;
		}

		heapify(a,1);


	}


	// afisare
	for(j=1;j<=n;j++)
		printf("%d ",listaSolutia[j]->inf);

	//pt k=5 constant
	k=5;
	atr =0;
	comp=0;
	cazuri(a,k,f1);

	//pt k=10 constant
	k=10;
	atr =0;
	comp=0;
	cazuri(a,k,f2);

	//pt k=100 constant
	k=100;
	atr =0;
	comp=0;
	cazuri(a,k,f3);

	//pt n=10000 constant si k=10...100
	int n1= 10000;
	atr =0;
	comp=0;
	caz_n(a,n1,f);

	fclose(f1);
	fclose(f2);
	fclose(f3);
	fclose(f);
	getch();
}