#include<iostream>
#include<math.h>
#include<conio.h>
#include "Profiler.h"
#define MAX 10000
using namespace std;

Profiler profiler("Heap");

int size,n;
// printare tree - culcat
void listInInorder(int A[],int i,int n){
	
	int ok=1;
	double j=i,count=0;
	if(i<=n){ 

	listInInorder(A,2*i+1,n);

	do{
		if(i>=pow(2,count) && i<pow(2,count+1))
			ok=0;
		count++;
	}while(ok!=0);

	for(int k=1;k<count;k++) cout<<"      "; 

	cout<<A[i]<<" \n";
	listInInorder(A,2*i,n);
	
	}
}
///////////////////////////////////////////////////////////////////////
//Metoda BUTTOM-UP!
void heapify(int A[],int i,int n){
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
// Memoram ultima si penultima pozitie in l respectiv r

	profiler.countOperation("CompM1",n);
	if(l<=n && A[l]>A[i])
	{largest=l;}
	else 
	{largest=i;}

	profiler.countOperation("CompM1",n);
	if(r<=n && A[r]>A[largest])
	{largest=r;}
// Comparam daca valoarea de pe pozitia respectiva mai mare decat i, daca e asa memoram indexul respectiv in valoarea largest

	if(largest!=i)
	{// Interschimbam valoarea lui i cu valoarea lui largest
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		profiler.countOperation("AssignM1",n,3);
		heapify(A,largest,n); // repetam procedura pana la i=1 ;
	}
	

}

//functia build heap care functioneaza impreuna cu heapify
void BuildMaxHeap(int A[],int n){
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}
////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// TOP DOWN METHOD
void HeapIncreaseKey(int A[],int i, int key){
	int aux;

	A[i]=key;
	profiler.countOperation("AssignM2",n);
	profiler.countOperation("CompM2",n);
	while(i>1 && A[i/2]<A[i]){
		profiler.countOperation("CompM2",n);
		aux=A[i/2];
		A[i/2]=A[i];
		A[i]=aux;
		profiler.countOperation("AssignM2",n,3);
		i=i/2;
}
	
}

void MaxHeapInsert(int A[],int key){
	size++;
	A[size]=-36000;   //A[size] = minus infinit!
	profiler.countOperation("AssignM2",n);
	HeapIncreaseKey(A,size,key);
}

void BuildMaxHeap2(int  A[],int n){
	int i;
	size=1;
	for(i=2;i<=n;i++)
		MaxHeapInsert(A,A[i]);
}
///////////////////////////////////////////////////////////////////////////

void main(){

int A[MAX],B[MAX],C[MAX];
int i,j;



for(i=100;i<10000;i=i+200){
	FillRandomArray(A,i,0,10000);
n=i;
	for(int k=1;k<=5;k++){

	for(j=1;j<=n;j++)
	{B[j]=A[j];C[j]=A[j];}

BuildMaxHeap(B,n);
BuildMaxHeap2(C,n);
}
}



profiler.addSeries("SumMet1","CompM1","AssignM1");
profiler.addSeries("SumMet2","CompM2","AssignM2");

profiler.createGroup("Comparisons","CompM1","CompM2");
profiler.createGroup("Assignments","AssignM1","AssignM2");
profiler.createGroup("Sum","SumMet1","SumMet2");

profiler.showReport();

getch();
}
