/*
		Similar algoritmului BFS cu cateva modificari corespunzatoare algoritmului de cautare in adancime.
		Usor de implementat cu exceptia functie Topologie() , functie un pic mai dificila dar am primit ajutorul
	lui Rasa Cosmin.
		Eficienta O(|V|+|E|)
	Student : ***ANONIM*** ***ANONIM*** - ***ANONIM***
	grupa : ***GROUP_NUMBER***
*/

#include "Profiler.h"
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WHITE 0
#define GRAY 1
#define BLACK 2 
#define V_MAX 10000
#define E_MAX 60000


typedef struct _nod{
int key;
struct _nod *next;
}nod;

typedef struct{
	int key;
	int parent;
	int d;//discovery
	int f;//finish
	int color;
}GRAPH_nod;

nod  *adj[V_MAX];
GRAPH_nod graph[V_MAX];
bool topologie;
int TOP[V_MAX];
int edges[E_MAX];
int times;
int count;

void generate(int n,int m)
{
	memset(adj,0,n*sizeof(nod*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		nod *p=(nod*)malloc(sizeof(nod));
		p->key=b;
		p->next=adj[a];
		adj[a]=p;
	}
}

//carte Cormen
void DFS_VISIT(GRAPH_nod u)
{
	times++;
	count++;
	graph[u.key].d=times;
	graph[u.key].color=GRAY;
	nod *v=adj[u.key];
	while(v!=NULL)
	{	
		count++;
		if (graph[v->key].color==WHITE)
		{
			count++;
			graph[v->key].parent=u.key;
			if(u.key!=v->key);
 			DFS_VISIT(graph[v->key]);
		}
		v=v->next;
	}
	graph[u.key].color=BLACK;
	times++;
	graph[u.key].f=times;
}

//carte Cormen
void DFS(int n)
{
	for(int i=0;i<n;i++)
	{
		graph[i].color=WHITE;
		graph[i].parent=0;
	}
	times=0;
	for(int i=0;i<n;i++)
	{	
		count++;
		if (graph[i].color==WHITE) DFS_VISIT(graph[i]);
	}
}

//listare lista de adiacenta
void afisareAdj(int n)
{ 
	for(int i=0;i<n;i++)
	{	
		printf("\n%d",i);
		nod *v=adj[i];
		while(v!=NULL)
		{
			printf("->%d ",v->key);
			v=v->next;
		}
	}
}

void afisareGraf(int n)
{
	printf("\nAfisare graf DFS\n");
	for (int i=0;i<n;i++)
	{
		printf("%d -> %d culoare: %d d=%d  f=%d\n",graph[i].parent,graph[i].key,graph[i].color,graph[i].d,graph[i].f);
	}
}

//carte Cormen
void tip_muchii(int n)
{
	topologie=true;
	for(int i=0;i<n;i++)
	{	
		nod *v=adj[i];
		while(v!=NULL)
		{
			if(graph[v->key].parent==i)
			{
				printf("\n%d -> %d tree edges",i,v->key);
			}
			else 
				if((graph[i].d < graph[v->key].d) && (graph[v->key].d < graph[v->key].f) && (graph[v->key].f < graph[i].f) )
					{
						printf("\n%d -> %d forword edges",i,v->key);
					}
			else 
				if((graph[v->key].d <= graph[i].d) && (graph[i].d < graph[i].f) && (graph[i].f <= graph[v->key].f) )
					{
						printf("\n%d -> %d back edges",i,v->key);	
						topologie=false;
					}
			else 
				if((graph[v->key].d < graph[v->key].f) && (graph[v->key].f < graph[i].d) && (graph[i].d < graph[i].f) )
					{
						printf("\n%d -> %d cross edges",i,v->key);
					}
			else 
			{
				printf("\n%d -> %d ",i,v->key);
			}
			v=v->next;
		}
	}
}

//idee primita de la Rasa Cosmin
void Topologie(int n)
{	
	if(topologie)
	{
		for(int i=0;i<n;i++)
		{	
			TOP[graph[i].d]=graph[i].key;	 	
		}
		printf("\n %d",TOP[1]);
		for(int i=2;i<=n*2;i++)
		{	
			if(TOP[i]!=0)
			printf(" -> %d",TOP[i]);
		}
	}
	else printf("\nexista cicluri\n");
}
///

int main()
{
	//Start Test
	int n=5;
	int m=7;
	generate(n,m);
	afisareAdj(n);
	for(int i=0;i<V_MAX;i++)
		graph[i].key=i;
	DFS(n);
	afisareGraf(n);
	tip_muchii(n);
	Topologie(n);
	//final Test

	FILE *f =fopen("iesireDFS.csv","w");
	fprintf(f,"Muchii,count\n");
	for (int numarMuchii=1000;numarMuchii+1 <6000;numarMuchii+=100)
	{
		generate(100,numarMuchii);
		for(int i=0;i<V_MAX;i++)
			graph[i].key=i;
		count=0;
		DFS(100);	
	 	fprintf(f,"%d,%d\n",numarMuchii,count);
	}

	fprintf(f,"\n\n");

	fprintf(f,"Noduri,count\n");
	for (int numarN=100;numarN <200;numarN+=10)	
	{
		generate(numarN,9000);
		for(int i=0;i<V_MAX;i++)
			graph[i].key=i;
		count=0;
		DFS(numarN);	
	 	fprintf(f,"%d,%d\n",numarN,count);
	}
}