#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"

/*
***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
From the point of view of assignments the slection sort is much better then the other two are almost the same.
From the point of view of comparisons the insertion sort is better then the other two are almost the same.
Summing this up, as we observed in the avarage case the bubble sort is much worse then the seleciton sort and the insertion sort.
The best case for every sorting algorithm is has the form of an already sorted array. Ex: 1 2 3 4 5 6 7 8 9 10 ...
They all behave the same in best case, they only make n comparisons, no associations.
The worst case for the insertion sort and the bubble sort is the reverse ordered array. Ex: ... 10 9 8 7 6 5 4 3 2 1
The worst case for selection sort is tricky, for n=4 it looks like: 3 4 2 1, for n=8 the worst case looks like: 2 3 4 5 6 7 8 1
In the worst case all the sorting functions tend to n^2 associations and comparisons.
*/

int insertiona,insertionc,selectiona,selectionc,bubblea,bubblec;
int n;

void swap(int &a,int &b)
{
	int k;
	k=a;
	a=b;
	b=k;
}

void printa(int a[])
{
	for (int i=0;i<n;i++)
		printf("% d ",a[i]);
	printf("\n");
}

void insertionsort(int a[])
{
	int buff;
	int j;
	for (int i=1;i<n;i++)
	{
		buff=a[i];
		insertiona++;
		j=i-1;
		insertionc++;
		while (a[j]>buff && j>=0)
		{	
		
			insertiona++;
			a[j+1]=a[j];
			j=j-1;
			insertionc++;
		}
		a[j+1]=buff;
		insertiona++;
	}
	
}

void selectionsort(int a[])
{
	int pos,i;
	for (int j=0;j<n-1;j++)
	{
		pos=j;
		for (i=j+1;i<n;i++)
		{	
			selectionc++;
			if (a[i]<a[pos])
			{
				pos=i;
				
			}
		}
		if (j!=pos)
		{
			swap(a[j],a[pos]);
			selectiona+=3;
		}
	}
}



void bubblesort(int a[])
{
	int i=0,len=n,temp;
	int s=1;
	while (s!=0)
	{
		s=0;
		for (i=0;i<len-1;i++)
		{
			bubblec++;
			if (a[i]>a[i+1])
			{
				bubblea++;
				swap(a[i],a[i+1]);
				s=1;
				temp=i+1;
			}
		}
		len=temp;
	}
}

void FillSortedArray(int a[])
{
	for (int i=0;i<n;i++)
	{
		a[i]=i;	
	}
}

void FillWorstArrayIB(int a[])
{
	for (int i=0;i<n;i++)
	{
		a[i]=n-i;	
	}
}

void FillWorstArrayS(int a[])
{
	for (int i=0;i<n-1;i++)
	{
		a[i]=i+1;	
	}
	a[n-1]=0;
}

void sort(int a[])
{
	for (int i=0;i<5;i++){
	//FillSortedArray(a);
	//FillRandomArray(a,n);
		FillWorstArrayIB(a);
	insertionsort(a);
	//FillRandomArray(a,n);
	FillWorstArrayS(a);
	selectionsort(a);
	//FillRandomArray(a,n);
	FillWorstArrayIB(a);
	bubblesort(a);
	}
}

int main()
{
	int a[10000];
	FILE *pa=fopen("a.csv","w");
	FILE *pc=fopen("c.csv","w");
	FILE *ps=fopen("sum.csv","w");
	
	fprintf(pa,"n,insertiona,selectiona,bubblea\n");
	fprintf(pc,"n,insertionc,selectionc,bubblec\n");
	fprintf(ps,"n,insertionall,selectionall,bubbleall\n");
	for (n=100;n<10000;n+=100)
	{
			insertiona=0;
	insertionc=0;
	selectiona=0;
	selectionc=0;
	bubblea=0;
	bubblec=0;

		printf("%d\n",n);
		sort(a);
	
	fprintf(pa,"%d,%d,%d,%d\n",n,insertiona,selectiona,bubblea);
	fprintf(pc,"%d,%d,%d,%d\n",n,insertionc,selectionc,bubblec);
	fprintf(ps,"%d,%d,%d,%d\n",n,insertiona+insertionc,selectiona+selectionc,bubblea+bubblec);

	}
//	profiler.createGroup("sortatributes", "insertiona", "selectiona","bubblea");
//	profiler.showReport();
	//profiler1.createGroup("sortcomparisons", "insertionc", "selectionc","bubblec");
	//profiler1.showReport();
	/*profiler.addSeries("ins","insertiona","insertionc");
	profiler.addSeries("sel","selectiona","selectionc");
	profiler.addSeries("bub","bubblea","bubblec");
	profiler.createGroup("sort sum", "ins", "sel","bub");
	profiler.showReport();*/
	getch();
	return 0;
}