﻿/*
	***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
	In primul rand un arbore multi-cai este un arbore ce poate avea mai mult de 2 fii,informatiile dintr-un nod sunt in ordine crescatoare 
	Keia din primul copil este mai mica decat urmatoarele,iar keia din ultimul copil este mai mare ca celelalte
	Fiecare nod poate avea m-1 informatii si m fii

Eficienta O(n) deoarece se parcurge arborele nod cu nod

*/
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>

// structura arborelui multi cai
typedef struct _MULTI_NOD{
	int cheie;
	int count;
	struct _MULTI_NOD *copil[50];
}MULTI_NOD;

//structura arborelui binar
typedef struct _NOD{
	int cheie;
	struct _NOD *stg,*dr;
}NOD;

//crearea unui nod multi cai
MULTI_NOD*createMN(int key)
{
	
	MULTI_NOD *p;
	p=(MULTI_NOD *)malloc(sizeof(MULTI_NOD));
	p->cheie=key;
	p->count=0;
	return p;
	
}


void inserare(MULTI_NOD* parent,MULTI_NOD* copil)
{
    int i;
    i=parent->count;
	parent->copil[i]=copil;
	parent->count++;
}

//Prima transformare care consta in transformarea unui tablou intr-un arbore multi cai
MULTI_NOD * PrimaTransformare(int t[],int size)
{
	MULTI_NOD *root=NULL;
	MULTI_NOD *nods[50];
	for(int i=0;i<size;i++)
	{
		nods[i]=createMN(i);
		if(t[i]==-1) root=nods[i];
	}
	for(int i=0;i<size;i++)
	{
		if(t[i]!=-1)
		inserare(nods[t[i]],nods[i]);
	}
	return root;
}
// Afisarea arborelui multi-cai
void PrettyPrintMN(MULTI_NOD *nod,int nivel=0)
{
	for(int i =0;i<nivel;i++) printf("   ");
	printf("%d\n",nod->cheie);
	for(int i=0;i<nod->count;i++)
		PrettyPrintMN(nod->copil[i],nivel+1);
}

//atribuirea folosita pentru constructia arborelui binar
void atribuie(NOD**r,int val)
{
	NOD *p;
	p=(NOD *)malloc(sizeof(NOD));
	p->cheie=val;
	p->dr=NULL;
	p->stg=NULL;
	(*r)=p;
}

//A doua transformare dintr-un arbore muulti-cai intr-un arbore binar( transformarea finala)
NOD* DouaTransformare(MULTI_NOD * root,NOD **rootm)
{
	NOD *p;
    int i;
    if(root->count > 0)
    {

		(*rootm)->stg=(NOD*)malloc(sizeof(NOD)); // in cazul in care avem copil atribuim spre stanga
		atribuie(&(*rootm)->stg,root->copil[0]->cheie);
		(*rootm)->stg->stg = NULL;
		
        p = (*rootm)->stg;  //nodul nou inserat il retinem
        for(i = 1; i < root->count; i++)
        {
			//pargurgem nodurile inserate(copii) pentru a putea face legatura dreapta
            p->dr = (NOD*)malloc(sizeof(NOD)); 
			atribuie(&p->dr,root->copil[i]->cheie); 
            DouaTransformare(root->copil[i-1],&p);
			                    
            p = p->dr;   //retinem  partea din dreapta pentru a nu pierde subarborii drepti
        }
    }
	return (*rootm);
}

//Afisarea arborelui  binar
void Pretty_printBN(NOD *rad, int d)
{
	if(rad!=NULL)
	{
		Pretty_printBN(rad->dr,d+1);
		for(int i=0;i<d;i++)
		{
			printf("    ");	
		}
		printf("%d\n",rad->cheie);
		Pretty_printBN(rad->stg, d+1);
	}
}

int main()
{
	int v[]={6,2,7,5,2,7,7,-1,5,2};
	int m=sizeof(v)/sizeof(v[0]);
	MULTI_NOD*root=PrimaTransformare(v,m);
	NOD *rootm=NULL;
	rootm=(NOD*)malloc(sizeof(NOD));
	rootm->cheie=root->cheie;
	rootm->dr=NULL;
	rootm->stg=NULL;
	DouaTransformare(root,&rootm);
	PrettyPrintMN(root);
	printf("\n\n\n");
	Pretty_printBN(rootm,0);
	scanf("%d",&rootm->cheie);
	return 0;
}