// laborator 9.cpp : Defines the entry point for the console application.
// am fost ajutat de Bonczidai Levente,gr ***GROUP_NUMBER***


#include "stdafx.h"
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Profiler.h"

#define WHITE 0
#define GRAY 1
#define BLACK 2
#define V_MAX 10000
#define E_MAX 60000

typedef struct
{
	int parrent;
	int distance;
	int color;
}GRAPH_NODE;

typedef struct _NODE
{
	int key;
	struct _NODE *next;
}NODE;

NODE *adj[V_MAX];
GRAPH_NODE graph[V_MAX];

NODE *prim;
NODE *ultim;

int nr_v;
int nr_e;

int edges[E_MAX];

void adaugamuchie(int a,int b)
{
	NODE *nod= new NODE;
	nod->key=b;
	nod->next=adj[a];
	adj[a]=nod;
}
//n=noduri m=muchii
void generate(int n,int m)
{
	
	FillRandomArray(edges,m,10,n*n-1,true);

	for(int i=0;i<m;i++)
	{
		int a=edges[i]%n;
		int b=edges[i]/n;
		adaugamuchie(a,b);
	}
}

void print_adi()
{
	printf("\n\n adiacenta list \n");
	for (int i=0;i<nr_v;i++)
	{
		printf("\n %d ",i);
		NODE *p=adj[i];
		while((p) !=0)
		{
			printf("%d ",p->key);
			p=p->next;
		}
	}
}
//sterge lista de adiacenta
void clear_edge()
{
	for (int i=0;i<nr_v;i++)
	{
	NODE *p=adj[i];
		while(p !=0)
		{
			NODE *aux=p;
			p=p->next;
			delete aux;
		}
	adj[i]=0;
	}
}

void enque(NODE **prim,NODE **ultim,int x)
{
	if (*prim==0)
	{
		*prim=*ultim=new NODE;
		(*prim)->key=x;
		(*prim)->next=0;
	}
	else
	{
		(*ultim)->next=new NODE;
		*ultim=(*ultim)->next;
		(*ultim)->key=x;
		(*ultim)->next=0;

	}
}

int deque(NODE **prim,NODE **ultim)
{
	if ((*prim)==0)
	{
		return -1;
	}
	
	int x=(*prim)->key;
	NODE *aux=*prim;
	*prim=(*prim)->next;
	delete aux;

	if(*prim==0)
	{
		*ultim=0;
	}

	return x;

}

void BFS(int s)
{
	
	for(int i=0;i<nr_v;i++)
	{
		graph[i].color=WHITE;
		graph[i].distance=INT_MAX;
		graph[i].parrent=-1;
	}
	graph[s].color=GRAY;
	graph[s].distance=0;
	enque(&prim,&ultim,s);

	while(prim!=0)
	{
		int u=deque(&prim,&ultim);
		NODE *p=adj[u];
		while(p !=0)
		{
			if(graph[p->key].color==WHITE)
			{
				graph[p->key].color==GRAY;
				graph[p->key].distance=graph[u].distance+1;
				graph[p->key].parrent=u;

				enque(&prim,&ultim,p->key);
			}
			p=p->next;
			
		}
		graph[u].color=BLACK;
	}
}

void print_parrent(int s)
{
	printf("\n print parrent");
	for (int i=0;i<nr_v;i++)
	{
		printf("%d ",graph[i].parrent);
	}

}

int main ()
{
	memset(adj,0, V_MAX*sizeof(NODE*));
	//test coada
	enque(&prim,&ultim,1);
	enque(&prim,&ultim,2);
	enque(&prim,&ultim,3);
	printf("%d ",deque(&prim,&ultim));
	printf("%d ",deque(&prim,&ultim));
	printf("%d ",deque(&prim,&ultim));
	printf("%d ",deque(&prim,&ultim));

	// test generate
	nr_e=7;
	nr_v=10;
	generate(nr_v,nr_e);
	print_adi();
	
	clear_edge();

	//-------------------------

	nr_e=9;
	nr_v=8;

	adaugamuchie(0,1);
	adaugamuchie(0,2);
	adaugamuchie(2,3);
	adaugamuchie(2,4);
	adaugamuchie(2,5);
	adaugamuchie(4,6);
	adaugamuchie(5,6);
	adaugamuchie(2,7);
	adaugamuchie(7,6);
	print_adi();
	//test BFS


	BFS(0);

	print_parrent(0);
	clear_edge();
	//-------------------------
}




