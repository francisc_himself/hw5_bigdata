/*
*HeapSort vs. QuickSort
****ANONIM*** ***ANONIM***
****GROUP_NUMBER***
*Start 19.03.2013
*
* QuickSort: 
* Worst-case behavior O(n^2)for quicksort occurs when the partitioning routine produces one subproblem 
* with n  1 elements and one with 0 elements.
* Best case: partitioning produces two [n/2] subproblems.
* Average case: random partitioning, close to best case. O (nlgn), depends on partitioning.
* Quicksort is faster in practice than heapsort, although to avoid worst case for quicksort, one 
* could use heapsort.
*
* Heapsort guarantees O(nlogn) running time.
*
*/


#include <stdio.h>
#include <limits.h>
#include <time.h>
#include <stdlib.h>

int COUNT_OPS;
FILE *fTopDown, *fQuickSort;

void swap (int *a, int *b)
{
	int aux = *a;
	*a = *b;
	*b = aux;
	COUNT_OPS += 3;
}

void Display(int arr[], int size)
{
	for (int i = 1; i <= size; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

void Heapify (int heap[], int size, int index)
{
	int left = index*2;
	int right = index*2+1;
	int largest;

	COUNT_OPS += 2;
	largest = (left <= size && heap[left] > heap[index]) ? left : index; //replace > heap[index] with replace < heap[index] for min
	if (right <= size && heap[right] > heap[largest])	//replace > heap[largest] with replace < heap[largest] for min
		largest = right;

	if (largest != index)
	{
		swap(&heap[largest], &heap[index]);
		Heapify(heap, size, largest);
	}
}

//build heap version 2 (top-down)
int HeapMax(int heap[], int size)
{
	return heap[1];
}
int HeapExtractMax(int heap[], int &size)
{
	int max = heap[1];
	heap[1] = heap[size];
	size--;
	Heapify(heap, size, 0);
	return max;
}
void HeapIncreaseKey(int heap[], int size, int index, int new_key)
{
	heap[index] = new_key;
	COUNT_OPS++;
	while (index > 1 && heap[index/2] < heap[index])
	{
		COUNT_OPS++;
		swap(&heap[index/2], &heap[index]);
		index = index / 2;
	}
}
void HeapInsert(int heap[], int &size, int key)
{
	size++;
	heap[size] = INT_MIN; //minus infinity
	HeapIncreaseKey(heap, size, size, key);
}
void BuildHeap2 (int arr[], int size)
{
	int heap_size = 1;
	for (int i = 2; i <= size; i++)
	{
		//printf("Step %d: ", i);
		//Display(arr, i);
		//printf("\n");
		HeapInsert(arr, heap_size, arr[i]);
	}
}
void Heapsort2(int arr[], int size)
{
	int n = size;
	//printf("Build heap...\n");
	BuildHeap2(arr, size);
	//Display(arr, size);
	for (int i = size; i >= 2; i--)
	{
		swap(&arr[1], &arr[i]);
		size--;
		Heapify(arr, size, 1);
	}
}

void GenerateAverageArray(int arr[], int size)
{
	for (int i = 1; i<= size; i++)
	{
		arr[i] = rand();
	}
}
void GenerateWorstArray(int arr[], int size)
{
	for (int i = 1; i <= size; i++)
		arr[i] = i;
}

void TestIterationTopDown(int size)
{
	int a[10001];
	GenerateAverageArray(a, size);
	COUNT_OPS = 0;
	Heapsort2(a, size);
	fprintf(fTopDown, "%d %d\n", size, COUNT_OPS);
	
}

void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      //int pivot = arr[(left + right) / 2]; COUNT_OPS++;
	 int pivot = arr[1]; COUNT_OPS++;
      // partition
	  COUNT_OPS++;
      while (i <= j) {
            while (arr[i] < pivot)
                  i++;
            while (arr[j] > pivot)
                  j--;
            if (i <= j) {
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp; COUNT_OPS+3;
                  i++;
                  j--;
            }
      };
 
      //recursion
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}

void TestIterationQuickSort(int size)
{
	int a[10001];
	//GenerateAverageArray(a,size); 
	GenerateWorstArray(a, size);
	COUNT_OPS = 0;
	quickSort(a, 0, size-1);
	fprintf(fQuickSort, "%d %d\n", size, COUNT_OPS);
}


void main()
{
	srand((unsigned)time(NULL));
	
	fTopDown = fopen("TopDown.txt", "w");
	fQuickSort = fopen("QuickSort.txt", "w");

	for (int size = 100; size <= 10000; size+=100)
	{
		
		//TestIterationTopDown(size);
		TestIterationQuickSort(size);
	}

	fclose(fTopDown);
	/*
	int a[100];
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf ("%d", &a[i]);
	printf("\n");
	printf("Heapsort...\n");
	Heapsort2(a, n);
	Display(a,n);
	

	printf("\n\n");
	printf("Quicksort...\n");
	quickSort(a, 0, n-1);
	Display(a,n);
	
	scanf("%d",&n);*/
}