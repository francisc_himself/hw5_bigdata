/*

	Student:					***ANONIM*** ***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Requirements:				You are required to implement correctly and efficiently two methods for building a heap, 
                                namely the bottom-up and the top-down strategies.
                                                                
                               1.You are required to compare the two build heap procedures in the average case. 
                                Remember that for the average case you have to repeat the measurements m times (m=5) 
                                and report their average; also for the average case, make sure you always use the same 
                                input sequence for the two methods � to make the comparison fair.
                               2.The analysis should be performed:
                               -vary the dimension of the input array (n) between [100�10000],
                                with an increment of maximum 500 (we suggest 100).
                               -for each dimension, generate the appropriate input sequence for the method; 
                               -run the method, counting the operations (assignments and comparisons, 
                                may be counted together for this assignment).
                
            
 Conclusions, Interpretations: I have learned about two distinct methods of creating a heap - Bottom Up approach 
                              (this method goes thruogh the n/2 elements of the array downto 1, starting with the 
                              first non-leaf node and bubbles up the element in the heap) & Top  Down approach 
                              (this method goes through all the elements of the array from 1 to n , in this order 
                              inserts  elements and further  it sinks the element in the heap).
                                          For one element both methods have the same order of growth O(h), h - lgn ,
                               the height of the heap. However , for n elements of an array bottom up performs almost 
                               as efficient as top down ,having t(n)~O(n) , i.e.in linear time.
                               This is true in the average case (building randomly m number of heaps).
                               For worse case we have O(n) for bottom up and O(n*lgn) for top down.                                        
                                       One can notice , comparing the graphs for the worse case that Top Down aproach is O(n*lgn),
                              not linear( one can get a constant value for number of operations if we divide with lgn) and Bottom Up 
                              linear with O(n) ( one can get a constant value for number of operations if we divide with n).
                              For the worse case possible we consider a vector (randomly generated but) already sorted increasingly,
                              and we use this vector to build the heaps with both methods.
                                         Taking into account my lecture notes I learned  that "heapify" build heap is used for sorting , 
                              meanwhile "push /insert-in-heap" approach for priority queues.
               			
*/
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define nmax 10001
#define max 100


int H[nmax],A[nmax];
int n;

int nrcU;//nr. comparari creare heap metoda Bottom_Up
int nrcD;//nr. comparari creare heap metoda Top_Down
int nraU;//nr asignari creare heap metoda Bottom_Up
int nraD;//nr asignari creare heap metoda Top_Down

/*
This function  will contruct the vector randomly generated ,v[] is a copy of A[].
*/

void recons(int t,int v[],int A[])
{
	int i;
	for(i=1;i<=t;i++)
	{
		v[i]=A[i];
	}
}

/*
This function will generate a random vector with values between 0 and max-1.
*/

void random(int t)
{
	int i;
	srand(time(NULL));
	
	for(i=1;i<=t;i++)
	{
		H[i]=rand()% max;
	}
}

/*
This function builds a vector with t 
elements with random values in ascending order.
I have used it to generte the decreasing  random vector
*/

void randomC(int t)
{
	int i;
	srand(time(NULL));
	H[0]=rand()%50;
	for(i=1;i<t;i++)
	{
		H[i]=H[i-1]+rand()%100;

	}
}


int Parent(int i)
{
	return i/2;
}

int Left(int i)
{   
	return 2*i;
}

int Right(int i)
{    
	return 2*i+1;
}



void Heapify(int A[],int i,int n)
{
	int left,right,maxim,aux;
	    left=Left(i);
	    right=Right(i);

	nrcU++;	
	if ((left<=n)&&(A[left]>A[i])) 
		maxim=left;
	else
		maxim=i;
		
	nrcU++;
	if ((right<=n)&&(A[right]>A[maxim]))
		maxim=right;
	
    if (maxim!=i)
	{		
		aux=A[i];
		A[i]=A[maxim];
		A[maxim]=aux;
		nraU=nraU+3;		
		
		Heapify(A,maxim,n);
	}
}



void BuildHeapify(int H[],int n)
{
	int i;
	for(i=n/2;i>=1; i--)
		Heapify(H,i,n);
}

/*
size -  is heap size , from 0 to n-1
key - element to insert ,first H[0],H[1],....,H[n-1]
*/

void InsertInHeap(int size,int key)
{
	int i;	
  //increase the size of the heap
   size++;
   i=size;	
  //key - the element to insert placed at tha position i
  H[size]=key;	
  nraD++;
	   
    nrcD++;
	while((i>1)&&(H[Parent(i)]<key))//while the heap property is not true
	{   nrcD++;
	    //swap son with parent	,indices as well		
		H[i]=H[Parent(i)];       
        i=Parent(i);
	    H[i]=key;
        nraD=nraD+2;
	}
}


/*   i- size of heap
	  H[i] - element to insert 
*/

void BuildHeapInsert(int n)
{   
	int i;
	for(i=1;i<=n;i++)  
	  	InsertInHeap(i-1, H[i]); 
}

void PreOrder(int i, int d)
{     
     if(H[i]) // not a leaf
       {    
            for (int j=0; j<d; j++)
                printf("        ");
                     
       printf("%3d\n",H[i]);
       PreOrder(Left(i), d+1);
       PreOrder(Right(i), d+1);
       }
}

/*
Read n - number of elements adn vector from file.
Test input data for building a heap.
*/

void readVector(char *fname){
	FILE *f=fopen(fname,"r");
	int i;

	fscanf(f,"%d\n",&n);
	for(i=1;i<=n;++i)
		fscanf(f,"%d ",&H[i]);
	fclose(f);
}


int main()
{
        int i,j,ind,t;
	    int aux[nmax];
	    
     	FILE* f1,*f2;
      	f1=fopen("AverageH.csv","w");
      	f2=fopen("WorseH.csv","w");
                    
        printf("Introduce an index :\n\n 0-sort a predefined vector\n\n 1-generate the tests for average/worse case\n ");       
        printf("\nindex=");
    	scanf("%d",&ind);
    	
	if(ind==0)
	  {  printf("Eg.n=10 \n H:4 1 3 2 16 9 10 14 8 7 vector is read from file\n");
	    
	  	  
		            readVector("datain.txt");
		            recons(n,A,H);
		            BuildHeapify(H,n);
                  	printf("\n----------------Bottom Up------------------------\n\n");		        
	             	printf("\n\nBuild Heap -->Bottom Up:\n");
	                
                    for(i=1;i<=n;i++)	 printf("%d ",H[i]);	                
                    printf("\nPreorder Traversal(BU):\n");                   
                    PreOrder(1,0);                   		           	   	           	  
                    recons(n,H,A);                    
                    
                    printf("\n\n----------------Top Down------------------------\n");		        
		            BuildHeapInsert(n);
		            printf("\n\nBuild Heap -->Top Down:\n");             	           		               		     
                    for(i=1;i<=n;i++)    printf("%d ",H[i]);                       
     	            printf("\nPreorder Traversal(TD):\n");           		       
                    PreOrder(1,0);            		     
                    recons(n,H,A);	                                 
	}	
	else
     {
         	t=100;
         	fprintf(f1,"n;nraU;nrcU;nraU+nrcU;nraD;nrcD;nraD+nrcD;\n");
         	//generate 100 test cases , each case having 5 vectors to reapeat the measurements for
             	             
             for(i=1;i<=100;i++)
	          {                       
                     nraU=0;
	                 nraD=0;
	                 nrcU=0;
                     nrcD=0;

                  for(j=1;j<=5;j++)
	                {
                        random(t);   //genereaza H[] random
                        recons(t,A,H);// in A[] keep a copy of H[] vector
	                    printf("Average case test %d bottom up.\n",i);
	                        BuildHeapify(H,t);	                    
	                  	                 	
	                    recons(t,H,A);//H [] is assigned initial vector for a new heap build 
	                    printf("Average case test %d top down.\n",i); 
	                        BuildHeapInsert(t);
	                    recons(t,H,A);
	                    	                 	                 	
                   // n=t;
                   }//end for2
             fprintf(f1,"%d;%d;%d;%d;%d;%d;%d;\n",t,nraU/5,nrcU/5,nraU/5+nrcU/5,nraD/5,nrcD/5,nraD/5+nrcD/5);
	         t=t+100;
	   }//end for1
	   
	   t=100;
	   fprintf(f2,"n;nraU;nrcU;nraU+nrcU;nraD;nrcD;nraD+nrcD;\n");
	   
	   // test for the worse case scenario
     for(i=1;i<=100;i++)
	   {
           nraU=0;
           nraD=0;
           nrcU=0;
           nrcD=0;
           n=t;
           //worse case for Bottom Up approach is when the array is already sorted(increasingly) ,
           // because it has to bubble the element up , at the highest possible level
           randomC(t);
           recons(t,A,H);
           printf("Worse case test %d bottom up(heapify)\n",i);
           BuildHeapify(H,t);           

           recons(t,H,A);//H [] is assigned initial vector for a new heap build     
           
           //worse case for the top down approach is when the array is sorted (increasingly)
           //and all the elements , from lowest to highest values must be sink donw in the tree(leaf level)
           
           randomC(t);
           printf("Worse case test %d top down(insert)\n",i); 
           BuildHeapInsert(t);
           recons(t,H,A);
           
           fprintf(f2,"%d;%d;%d;%d;%d;%d;%d;\n",n,nraU,nrcU,nraU+nrcU,nraD,nrcD,nraD+nrcD);
           t=t+100;
          
       }   
	}//end if(in==0)
    getch();
    fclose(f1);
    fclose(f2); 
}
