/*	
	Construirea unui heap este un subiect foarte interesant si importatnt in domeniul programarii,
mai ales din perspectiva mea ,deoarece acuma este priam data cand am un contact direct cu acesti
algoritmi de construire.
	
	In programul ce urmeaza este prezentat constructia unui heap ("arbore") folosind cele doua metode,
Button Up si Top Down.
	In primul rand un heeap este reprezentat in memorie sun forma unui tablou si nu a unui arbore , care are 
anumite proprietati specifice. 
	Diferenta dintre cei doi algoritmi de constructie , dupa cum le spune si numele , este aceea ca unul incepe
sa construieasca heeapul de jos in sus respectiv de sus in jos.
	
	In constructia Button Up ne pozitionam pe ultimul nod al asa zisului arbore care contine frunze si incepem sa comparam
parintii cu fii , iar in functie de rezultat se face sau nu interschimbarea lor. Dupa finalizarea comparatiilor a unui nod,
cu restul fiilor se trece la nodul aflat pe pozitia mai mica si se reia algoritmul pana gand acesta este finalizat cu succes.
	
	Pentru a construii un heap folosind metoda Top Down e nevoie de inca un tablou care sa mentinaa si sa ordoneze corespunzatori
nodurile heeapului. Se extrage primul element din vector si se pune in poztia de radacina a heepului, dupa care se compara cu nodul
urmator si se schimba in caz de nevoie. Urmatorul element se pune pe ultima pozitie a heeapului si se compara elementele incepand de la
radacina si se fac interschimbarile necesare
	
	Pe baza graficelor rezultate se observa ca numarul operatiilor creste liniar in functie de nr elementelor din tablou.
	Din rezultatele si graficele obtinute reise faptul ca numarul de operatii este relativ mic chiar si la un tablou cu 10.000 de
elemente, si faptul ca in cazul in care vectorii nu sunt sortati metoda Top Down are un mic castig la capitolul viteza fata de metoda
Button Up dar in ansamblu sunt doua metode foarte eficiente
	
	***ANONIM*** ***ANONIM*** ***ANONIM***, UTCN-CTI ,gr:***GROUP_NUMBER***;
*/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

/*declaratiile globale pentru intregi care memoreaza numarul de asignari si comparatii pentru 
BU si TD. Dimensiunea vectorului v care este folosit pentru a construii heapul, vectorul c care
pastrea copia vectorului v plus vectorul h care este folosit la constructiia TD
*/
int n,dim;
int as=0,com=0,asTD=0,comTD=0;
int v[10001],c[10001],h[10001]; 

//creeam copia vectorului
void retine(int *v,int *c,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v[i]=c[i];
	}
}

//generarea numere aleatoare
void generare(int *v, int n)
{
	int i;
	for (i=0;i<n;i++)
		v[i]=rand();
}

//construirea heapului Button Up
void Reconstructie(int v[], int i) 
{ 
  int stg,dr,max;
  stg=2*i;
  dr=(2*i)+1;
  com++;
  if (  (stg <= n) && (v[stg] > v[i])) 
  {
    max = stg;
  }
  else 
  {
    max = i;
  }
  com++;
  if ( (dr <= n) && (v[dr] > v[max])) 
  {
    max = dr;
  }
  com++;
  if (max != i)
  {
	as++;
    int temp = v[i];
    v[i] = v[max];
    v[max] = temp;
    Reconstructie(v, max);
  }
}

void Constructie(int v[])
{
	int i;
	com=0,as=0;
	for (i=n/2; i > 0;i--) 
	{
		Reconstructie(v, i); 
	}
}

//construierea heapului Top Down folosint "H_Push"

void H_Push(int v[],int key)
{ 
	dim++;
	asTD++;
	v[dim]=key;
	int i=dim;
	comTD++;
	while(i>1 && v[i/2]>v[i])
	{	
		asTD++;
		int aux=v[i/2];
		v[i/2]=v[i];
		v[i]=aux;
		i=i/2;
	}
}


void TopDown(int h[] ,int v[])
{
	comTD=0,asTD=0;
	dim=0;
	for(int i=1;i<=n;i++)
	{ 
		H_Push(h,v[i]);
	}
}

//afisarea Preety_print pentru verificarea corectitudinii problemei
void Preety_print(int *v,int k,int nivel)
{
	int i;
	if (k>n)
		return;
	Preety_print(v,2*k+1,nivel+1);
		for(i=1; i<=nivel;i++)
			printf("  ");
		printf("%d\n",v[k]);
	Preety_print(v,2*k,nivel+1);
}


int main()
{
	/*deschiderea fisierului si declararea unor doi vectori inc are se retin
	valorile intermediare de comparatii si asignari care urmeaza sa fie impartite 
	la valoarea 5 pentru a avea o medie de operatii pentru mai multi vectori de aceeiasi lungime*/
	FILE *f;
	f=fopen("vali.csv","w");
	int asignari[1],comparari[1];
	
	
	/*vector folosit pentru a verifica corectitudinea
	int test[7];
	test[1]=20;test[2]=15;test[3]=1;test[4]=3;test[5]=5;test[6]=23;test[7]=65;test[8]=40;n=8;
	Constructie(test);
	TopDown(h,test);
	Preety_print(test,1,1);*/

	//denumirea fiecarei coloane in tabelul ce urmeaza sa fie scris
	fprintf(f,"n, BUas, BUcom,BUas+comp,TDas,TDcom,TDas+comp \n"); 
	//for folosit pentru a returna dimensiunea vectorilor
	for(n=100;n<=10000;n=n+100)
	{
		//initializarea cu 0 a valorilor auxiliare ce vor fi folosite pentru gasirae mediei
		asignari[0]=0;asignari[1]=0;
	    comparari[0]=0;comparari[1]=0;

		//repetarea de 5 ori a generarii unui vector si construiri heapului prin cele doua metode
		for(int m=1;m<=5;m++)
		{	
			generare(v,n);
			retine(c,v,n);
			
			Constructie(c);
			asignari[0]=asignari[0]+ as;
			comparari[0]=comparari[0]+com ;

			TopDown(h,v);
			asignari[1]=asignari[1]+ asTD;
			comparari[1]=comparari[1]+ comTD;
		}
		//afisarea in tabel a rezultatelor obtinute
		fprintf(f,"%d,",n);
		fprintf(f, "%d,%d,%d,",asignari[0]/5,comparari[0]/5,(asignari[0]+comparari[0])/5);	
		fprintf(f,"%d,%d,%d\n", asignari[1]/5,comparari[1]/5,(asignari[1]+comparari[1])/5 );
	}
	//final program
	return 0;
}