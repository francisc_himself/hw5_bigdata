#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>

/* In urma graficelor efectuate se poate observa ca pe masura ce k creste (nr listelor este mai mare), creste si nr operatilor.
De asemenea, se mai poate analiza si cazul in care n este constant (10000), iar k creste pana la 100: 
nr operatilor creste pe masura ce avem mai multe liste*/

typedef struct Node
{
	long info;
	struct Node *urm;
}Nod;


Nod *a[1000];
Nod *lista_finala[10001];
long operatii=0;
long heapSize;

int i,j,l,m,n,dim,nr,r,nn;
   long op;
   int k;
int atr=0,comp=0;

void heapify(Nod *v[], int i)
{
   int l,r,min;

   l=2*i;
   r=2*i+1;
  // atr+=2;
	//determin elementul minim 
   operatii++;
   if (l <= heapSize && v[l]->info < v[i]->info)
   {
       min=l;
       atr++;
	   comp++;
   }
   else
   {
       min=i;
	   atr++;
	   comp++;
   }
   operatii++;
   if (r <= heapSize && v[r]->info < v[min]->info)
   {
       min=r;
   atr++;
   comp++;
   }
   //interschimb elementul minim cu radacina
	if (min!=i)
       {
           operatii+=3;
           v[0]=v[i];
           v[i]=v[min];
		   v[min]=v[0];
		   atr+=3;
           heapify(v,min);
       }
}

void bh_bottomup(Nod *v[],int n)
{
   int i;
   heapSize=n;
  // atr+=1;
   for (i=heapSize/2; i>=1; i--)
       heapify(v,i);
}
void k_5 ( Nod*a[], int k)
{
   //k este constant, k=5
	atr=0;comp=0;
   FILE *f1;
   f1=fopen("k1.csv","w");
   fprintf(f1, "nn,atr/5,comp/5,op/5,k5(atr+comp)/5\n");
   for (nn=100; nn<=10000; nn=nn+100)
   {
       op=0;
		//atr++;
		for (j=1;j<=5;j++)
       {
           operatii=0;
			//atr++;
           for (l=1;l<=k;l++)
           {
			   atr+=3;
               a[l]=new Nod;
               a[l]->info=rand() % (k*2);
               a[l]->urm=NULL;
               Nod *interm=new Nod;
			   interm=a[l];
               for (m=2;m<=nn/k;m++)
               {
				   atr+=4;
                   Nod *q=new Nod;
                   q->info=interm->info+rand() % (k*2);
                   q->urm=NULL;
                   interm->urm=q;
                   interm=q;
               }
           }
           dim=k;
           nr=1;
		 //  atr+=2;
           bh_bottomup(a,dim);
           while (dim!=0)
           {
			   comp++;
               lista_finala[nr]=new Nod;
               lista_finala[nr]->info=a[1]->info;
               a[1]=a[1]->urm;
			   lista_finala[nr]->urm=NULL;
			   nr++;
			   atr+=3;
               if (a[1]==NULL)
               {
				   comp++;
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
				   atr++;
               }
               heapify(a,1);
           }
            op+=operatii;
			atr+=1;
       }
      fprintf(f1, "%d, %d, %d, %ld, %ld\n",nn,atr/5,comp/5,op/5,(atr+comp)/5);
   }
   fclose(f1);
}
void k_100(Nod *a[], int k)
{
  //k constant, k=100
	 FILE *f2;
  atr=0;comp=0;
   f2=fopen("k2.csv","w");
   fprintf(f2, "nn,atr/5,comp/5,op/5,k100(atr+comp)/5\n");
  
   	//k=100;
   for (nn=100; nn<=10000; nn=nn+100)
   {
       op=0;
		//atr++;
		for (j=1;j<=5;j++)
       {	//atr++;
           operatii=0;

           for (l=1;l<=k;l++)
           {
			   atr+=3;
               a[l]=new Nod;
               a[l]->info=rand() % (k*2);
               a[l]->urm=NULL;
               Nod *interm=new Nod;
			   interm=a[l];
               for (m=2;m<=nn/k;m++)
               {
				   atr+=4;
                   Nod *q=new Nod;
                   q->info=interm->info+rand() % (k*2);
                   q->urm=NULL;
                   interm->urm=q;
                   interm=q;
               }
           }

           dim=k;
           nr=1;
		  // atr+=2;

           bh_bottomup(a,dim);

           while (dim!=0)
           {
			   atr+=3;
			   comp++;
               lista_finala[nr]=new Nod;
               lista_finala[nr]->info=a[1]->info;
               a[1]=a[1]->urm;
			   lista_finala[nr]->urm=NULL;
			   nr++;

               if (a[1]==NULL)
               {
				   comp++;
				   atr++;
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }

               heapify(a,1);
           }

            op+=operatii;
			atr++;

       }
       fprintf(f2, "%d, %d, %d, %ld, %ld\n",nn,atr/5, comp/5,op/5,(atr+comp)/5);
   }
   fclose(f2);
  }
void k_10(Nod *a[], int k)
{
   //k constant, k=10, n ia valori de la 100 la 10000
	 FILE *f3;
   
   f3=fopen("k3.csv","w");
   fprintf(f3, "nn,atr/5,comp/5,op/5,k10(atr+comp)/5\n");
   //	k=10;
   atr=0;
   comp=0;
   for (nn=100; nn<=10000; nn=nn+100)
   {
       op=0;
      // atr++;
		for (j=1;j<=5;j++)
       {
           operatii=0;
			//atr++;
           for (l=1;l<=k;l++)
           {
			   atr+=3;
               a[l]=new Nod;
               a[l]->info=rand() % (k*2);
               a[l]->urm=NULL;
               Nod *interm=new Nod;
			   interm=a[l];
               for (m=2; m<=nn/k; m++)
               {
				   atr+=4;
                   Nod *q=new Nod;
                   q->info=interm->info+rand() % (k*2);
                   q->urm=NULL;
                   interm->urm=q;
                   interm=q;
               }
           }

           dim=k;
           nr=1;
			atr+=2;
           bh_bottomup(a,dim);
		   // cat timp mai am elemente in lista si practic mai am liste, pun totul in lista finala
           while (dim!=0)
           {
			   comp++;
			   atr+=3;
               lista_finala[nr]=new Nod;
               lista_finala[nr]->info=a[1]->info;
               a[1]=a[1]->urm;
			   lista_finala[nr]->urm=NULL;
			   nr++;
			   // aici se termina o lista
               if (a[1]==NULL)
               {
				   atr+=3;
				   comp++;
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }

               heapify(a,1);
           }

            op+=operatii;
			atr++;
       }
       fprintf(f3, "%d, %d, %d, %ld, %ld\n",nn,atr/5,comp/5,op/5,(atr+comp)/5);
   }
   fclose(f3);
}
 //n constant; n=10000; k de la 10 la 100
void n_constant( Nod *a[], int nn)
{
	 FILE *g;
   
   g=fopen("n.csv","w");
   fprintf(g, "k,atr/5,comp/5,op/5,nConst(atr+comp)/5\n");
   atr=0;
   comp=0;
   for (k=10; k<=500; k=k+10)
    {
        op=0;
	//	atr++;
		for (j=1;j<=5;j++)
        {
            operatii=0;
			//atr++;
			//primul elem din fiecare lista
            for (l=1;l<=k;l++)
            {
				atr+=3;
                a[l]=new Nod;
                a[l]->info=rand() % (l * 3);
                a[l]->urm=NULL;
                Nod *interm=new Nod;
				interm=a[l];
				//populez listele
                for (m=2; m<=nn/k; m++)
                {
					atr+=4;
                    Nod *q=new Nod;
                    q->info=interm->info+rand() % (l * 3);
                    q->urm=NULL;
                    interm->urm=q;
                    interm=q;
                }
            }

            dim=i;
            nr=1;
			//atr+=2;
			
            bh_bottomup(a,dim);

            while (dim!=0)
            {
				comp++;
				atr+=3;
               lista_finala[nr]=new Nod;
               lista_finala[nr]->info=a[1]->info;
               a[1]=a[1]->urm;
			   lista_finala[nr]->urm=NULL;

                if (a[1]==NULL)
                {
					comp++;
					atr++;
                    a[1]=a[dim];
                    dim--;
					heapSize--;
                }

                heapify(a,1);
            }

             op+=operatii;
			 atr++;

        }
		fprintf(g,"%d, %d, %d, %ld, %ld\n",k,atr/5, comp/5,op/5,(atr+comp)/5);
    }
   fclose(g);
  }
int main()
{  
	FILE *f1,*f2,*f3,*g;
   /*   Initial inserez in fiecare din lista primul element.Apoi inserez restul elementelor in liste */

   k=4;//nr de liste
   n=20;//termeni din toate listele
   //parcurg listele
	for (i=1; i<=k; i++)
           {
                a[i]=new Nod;
                a[i]->info = rand() % (k*3)+1;
                a[i]->urm = NULL;
                Nod *prim = new Nod;
			    prim = a[i];
				printf("%d ",a[i]->info);

               for (j=2; j<=n/k; j++)
               {
                   Nod *q=new Nod;
                   q->info=1+ prim->info + rand()%(k*3);
                   q->urm = NULL;
                   prim->urm = q;
                   prim = q;
				   printf("%d ",q->info);   
               }
			    printf("\n");
           }
           dim = k;
           nr = 1;
           bh_bottomup(a,dim);
		  
           while (dim!=0)
           {
               lista_finala[nr]=new Nod;
               lista_finala[nr]->info = a[1]->info;
               a[1]=a[1]->urm;
			   lista_finala[nr]->urm=NULL;
			   nr++;

               if (a[1]==NULL)
               {
                   a[1]=a[dim];
                   dim--;
				   heapSize--;
               }
				//
               heapify(a,1);
           }
   // afisam lista interclasata
	for(j=1;j<=n;j++)
		printf("%d ",lista_finala[j]->info);
k_5(a, 5);
atr=0;comp=0;
k_10(a,10);
atr=0;comp=0;
k_100(a,100);
atr=0;comp=0;
n_constant(a,10000);
getch();
return 0;
}



