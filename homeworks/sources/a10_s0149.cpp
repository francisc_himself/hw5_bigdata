

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include<iostream>
#include<fstream>

#define ALB 1
#define GRI 2
#define NEGRU 3
using namespace std;
typedef struct nod{
	int info;
	struct nod *urm;
}NOD;

NOD *vecini[10001];
int t[1001];//parintii
int cul[10001];//culorile
int d[10001];//timp de pornire
int f[10001];//timp de terminare
int timp;
int nrop;

//****************************************************************
//GENERAREA GRAFULUI
int vecin(int a,int b){//returneaza daca b este vecinul lui a
	if(vecini[a]==NULL)
		return 0;
	NOD *p = vecini[a];
	while(p!=NULL){
		if(p->info == b)
			return 1;
		p = p->urm;
	}
	return 0;
}



void generate(int n,int nr_e){
	int i;
	for(i=1;i<=n;++i){
		vecini[i]=NULL;
		cul[i]=ALB;
		t[i]=0;

		d[i]=0;
		f[i]=0;
	}
	
	for(i=0;i<nr_e;++i){
		//generam o muchie
		int a,b;
		do{
			a = rand()%n + 1;
			b = rand()%n + 1;
		}while(a == b || vecin(a,b));

			NOD *p = (NOD*)malloc(sizeof(NOD));
	        p->info = b;
	        p->urm = NULL;
	        if(vecini[a]==NULL)
		    vecini[a] = p;
	        else{
		    NOD *q = vecini[a];
		    vecini[a] = p;
		    p->urm = q;
	}
	}
}
//void reset(NOD *p){
//	if(p != NULL){
//		reset(p->urm);
//		free(p);
//	}
//}

void dfs_visit(int u){
	cul[u] = GRI;
	d[u] = timp++;

	NOD *p;
	p = vecini[u];
	nrop += 4;
	int v;
	while(p!=NULL){
		++nrop;
		v = p->info;
		p = p->urm;
		nrop += 2;
		++nrop;
		if(cul[v] == ALB){
			t[v] = u;
			printf("(%d; %d) -tree\n",u,v);
			dfs_visit(v);
			nrop += 2;
		}else if(cul[v] == GRI){
			printf("%d; --> %d -back\n",u,v);
		}else if(f[v] < d[u]){
			printf("%d --> %d -cross\n",u,v);
		}else{
			printf("%d --> %d -forward\n",u,v);
		}
	}
	cul[u] = NEGRU;
	f[u] = timp++;
	nrop += 3;
}

void dfs(int n){
	int i;
	for(i=1;i<=n;++i){
		cul[i]=ALB;
		t[i]=0;
		d[i]=0;
		f[i]=0;
	}
	nrop = 0;//reseteaza numarul de operatii
	timp = 0;
	for(i=1;i<=n;++i){

			dfs_visit(i);
			nrop += 2;}
}

int main(void){
	srand((int)time(NULL));
	fstream f;
	int i;
	f.open ("rezultat_dfs.txt",ios::out);
    for (int j=5000;j<=10000;j+=500)
		{     for (i=100;i<=1000;i+=100)	
				{	generate(i,j);
			dfs(i);
			//	for(int k=1;k<=i;++k)				
			//	reset(vecini[i]);
		}
		f<<i<<"    "<<j<<"    "<< nrop<<"    "<<endl;}
	f.close();
}
