#include <stdio.h>
#include <conio.h>
#include "Profiler.h"
#include <time.h>

#define c1 991
#define c2 9997

/*
	Found Keys:
		The average time grows together with the filling factor. It is a constang growth.
		The maxium effort also depends on the filling factor.

	Not Found Keys:
		The average time grows together with the filling factor faster than the one from the found keys case.
		The maximum, for the filling factor 0.99 is 4 times greater than the one from found keys.
*/
int n=10007;
int T[10007];

int hashAuxilliar(int k){
	return k%n;
}

void initialize(int T[], int len){
	for (int i=0;i<len;i++) T[i]=0;
}
int hashFunction(int k, int i){
	unsigned int hash;
	hash=hashAuxilliar(k);
	hash+=c1*i;
	hash+=c2*i*i;
	hash=hash % n;
	return hash;
}

int hashInsert(int T[], int len, int k){
	int i=0;
	do{
		int j=hashFunction(k,i);
		if (T[j]==0){
			T[j]=k;
			return j;
		}
		else i++;
	}while(i<len);
	return -1;
}

int hashSearch(int T[], int len, int k, int *op){
	int i=0,j;
	do{
		(*op)++;
		j=hashFunction(k,i);
		if (T[j]==k) return j;
		else i++;
	}while(T[j]!=0 && i<len);
	return -1;
}

void simulation(){
	for (int j=0;j<5;j++){
		int len=n;
		float fill;
		if (j==0) fill=0.8f;
		if (j==1) fill=0.85f;
		if (j==2) fill=0.9f;
		if (j==3) fill=0.95f;
		if (j==4) fill=0.99f;
		len=(int)(len*fill);
		//printf("%d\n",len);
		int opF=0,opNF,maxF=0,maxNF;
		int arr[10009],verif;
		int limit=1500;
		//printf("%7.2f\n",(float)len);
		//srand(time(NULL));
		opNF=0;maxNF=0;
		for (int k=0;k<5;k++){
			FillRandomArray(arr,len,10,1000000000,false,0);
			initialize(T,n);
			for (int i=0;i<len;i++){
				verif=hashInsert(T,len,arr[i]);
				if (verif == -1) printf("Can't insert!\n");
			}
			for (int i=0;i<limit;i++){
				int index=rand()%len;
				int aux=opF;
				verif = hashSearch(T,len,arr[index],&opF);
				if (verif==-1) printf("Not ok! Found ");
				if (maxF < opF-aux) maxF=opF-aux;
			}
			initialize(T,n);
			for (int i=0;i<len;i++){
				verif = hashInsert(T,len,arr[i]);
				if (verif == -1) printf("Can't insert!\n");
			}
			for (int i=0;i<limit;i++){
				int value = rand()%1000000000+1000000001;
				int aux=opNF;
				verif = hashSearch(T,len,value,&opNF);
				if (verif != -1) printf("Not ok! Not Found");
				if (maxNF < opNF - aux ) maxNF=opNF - aux;
			}
		}
		//printf("Found: \n");
		printf("%3.2f %6.2f %d\n",fill,(float)(opF/limit)/5,maxF);

		//printf("Not found: \n");
		printf("%3.2f %6.2f %d\n",fill,(float)(opNF/limit)/5,maxNF);

		printf("\n");
	}
}
void main(){
	simulation();
	getchar();
 }