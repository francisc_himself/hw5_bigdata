#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

/**Acest program genereaza un sir de muchii pentru nrv (numarul de varfuri) si pentru nrm(numarul 
de muchii). Folosind functiile make_set, get_set si link se creeaza diferitele componente conexe.
Functia make_set aloca memorie pentru structura definita pentru un nod iar find_set reprezinta
graful ca o structura nod cu legatura spre parinte. Daca parintii unor "seturi" diferite au muchie
intre ei, se face o uniune intre cele doua seturi, care formeaza o componenta conexa. 
Afisarea se realizeaza pentru fiecare nod, printand parintele (nodul de inceput) si toate cheile 
din setul corespunzator lui. */

typedef struct muchie
{
	int x, y; 
}
MUCHIE;

MUCHIE edge[60001];

typedef struct nod
{
	int key, rank;
	struct nod* parent;
} NOD;

NOD *V[10001];

int op;

void init()
{
	int i;
	for(i=1;i<=60001;i++)
		edge[i]=*(MUCHIE*)malloc(sizeof(MUCHIE));
}

bool verif(int x, int y, int nrm)
{
	int i;
	for(i=1; i<nrm; i++)
	{
		if(((edge[i].x==x) && (edge[i].y==y)) || ((edge[i].y==x) && (edge[i].x==y)))
		{
			return false;
		}
	}
	return true;
}

void generate(int nrv, int nrm)
{
	int i,x,y;
	int nre=1;
	while(nre<=nrm)
		for(i = 1; i<=nrv; i++)
		{
			x = (rand()%(nrv))+1;
			y = (rand()%(nrv))+1;
			if((x != y) && (verif(x,y,nrm)))
			{
				edge[nre].x = x;
				edge[nre].y = y;
				nre++;
			}
		}
}

NOD *make_set(int x)
{
	NOD *p;
	p=(NOD*)malloc(sizeof(NOD));
	p->key=x;
	p->parent=p;
	p->rank=0;
	op+=3;//
	return p;
}

NOD *find_set(NOD *p)
{
	op++;//
	if(p!=p->parent)
	{
		p->parent=find_set(p->parent);
		op++;//
	}
	return p->parent;
}

void link(NOD *x, NOD *y)
{
	op++;//
	if(x->rank > y->rank)
	{
		y->parent=x;
		op++;//
	}
	else
	{
		x->parent=y;
		op++;//

		op++;//
		if(x->rank == y->rank)
		{
			op++;//
			y->rank=y->rank+1;
		}
	}
	
}

void _union(NOD *x, NOD *y)
{
	link(find_set(x),find_set(y));
}

void componente_conexe(int nrv, int nrm)
{
	int i;
	for(i=1; i<=nrv; i++)
	{
		op++;//
		V[i]=make_set(i);
	}
	for(i=1;i<=nrm;i++)
	{
		op++;//
		if(find_set(V[edge[i].x])!=find_set(V[edge[i].y]))
		{
			_union(V[edge[i].x], V[edge[i].y]);
			//printf("key: %d, parent: %d, rank: %d\n",V[edge[i].x]->key,V[edge[i].x]->parent->key,V[edge[i].x]->rank);
			//printf("key: %d, parent: %d, rank: %d\n",V[edge[i].y]->key,V[edge[i].y]->parent->key,V[edge[i].y]->rank);
		}
	}
}

void afisare_muchii (int nrm)
{
	int i;
	for(i=1;i<=nrm; i++)
	{
		printf("[%d, %d]\n", edge[i].x, edge[i].y);
	}
}

void afisare_conexa(int nrm, int nrv)
{
	int i,j;
	for(i=1;i<=nrv;i++)
		//printf("nodul %d are parintele %d\n",V[i]->key,V[i]->parent->key,V[i]->rank);

	for(i=1;i<=nrv;i++)
	{
		printf("Multimea %d: ",i);
		for(j=1;j<=nrv;j++)
		{
			if(find_set(V[j])->key==i)
				printf("%d ",V[j]->key);
		}
		printf("\n");
	}
}

void main()
{
	int nrv=6;
	int nrm=3;
	srand(time(NULL));
	generate (nrv, nrm);
	afisare_muchii(nrm);
	componente_conexe(nrv,nrm);
	afisare_conexa(nrm,nrv);


	FILE *f;
	f=fopen("rez.txt","w");
	nrv=10000;
	fprintf(f,"muchii op\n");
	for(nrm=10000; nrm<=60000; nrm+=1000)
	{
		op=0;
		init();
		generate(nrv,nrm);
		componente_conexe(nrv,nrm);
		fprintf(f,"%d %d\n",nrm,op);
	}
	fclose(f);
	printf("Am terminat!%c",7);
	getch();
}