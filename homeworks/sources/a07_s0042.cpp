/*
	Dificultatea programului a constat in functia de transformare a arborelui multi-cai in arbore binar , am intampinat probleme din cauza 
faptului ca am reusit destul de greu sa reusesc sa retin pointer spre adresa nodului din dreapta(ideea a venit tarziu si trebuie sa recunosc ca am avut noroc, deoarece nu am fost sigur ca e o idee buna). 
Program interesant cu eficienta functiei de transformare O(n) deoarece se parcurge arborele multi cai nod cu nod.

Student:***ANONIM*** ***ANONIM*** ***ANONIM***
grupa:***GROUP_NUMBER***
*/
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>

//declarare structura arbore multi cai
typedef struct _MULTI_NOD{
	int cheie;
	int count;
	struct _MULTI_NOD *child[50];
}MULTI_NOD;

//declarare structura arbore binar
typedef struct _NOD{
	int cheie;
	struct _NOD *stg,*dr;
}NOD;

//creare nod multi cai
MULTI_NOD*createMN(int key)
{
	
	MULTI_NOD *p;
	p=(MULTI_NOD *)malloc(sizeof(MULTI_NOD));
	p->cheie=key;
	p->count=0;
	return p;
	
}

//inserare copil
void inserare(MULTI_NOD* parent,MULTI_NOD* child)
{
    int i;
    i=parent->count;
	parent->child[i]=child;
	parent->count++;
}

//transformare tablou tati=>arbore multi cai
MULTI_NOD * transform1(int t[],int size)
{
	MULTI_NOD *root=NULL;
	MULTI_NOD *nods[50];
	for(int i=0;i<size;i++)
	{
		nods[i]=createMN(i);
		if(t[i]==-1) root=nods[i];
	}
	for(int i=0;i<size;i++)
	{
		if(t[i]!=-1)
		inserare(nods[t[i]],nods[i]);
	}
	return root;
}

//metoda de atribuire folosita in constructia arboreluil binar
void atribuie(NOD**r,int ***ANONIM***)
{
	NOD *p;
	p=(NOD *)malloc(sizeof(NOD));
	p->cheie=***ANONIM***;
	p->dr=NULL;
	p->stg=NULL;
	(*r)=p;
}

//transformarea din multi cai=>binar
NOD* transform2(MULTI_NOD * root,NOD **rootb)
{
	NOD *p;
    int i;
    if(root->count > 0)
    {
		//daca are copil atribuim stanga
		(*rootb)->stg=(NOD*)malloc(sizeof(NOD));
		atribuie(&(*rootb)->stg,root->child[0]->cheie);
		(*rootb)->stg->stg = NULL;
		//retinem nodul nou isnerat
        p = (*rootb)->stg;
        for(i = 1; i < root->count; i++)
        {
			//pargurgem copii pentru a putae face legatura dreapta
            p->dr = (NOD*)malloc(sizeof(NOD));
			atribuie(&p->dr,root->child[i]->cheie); 
            transform2(root->child[i-1],&p);
			//retinem dreapta pentru a nu pierde subarborii drepti 
            p = p->dr;
        }
    }
	return (*rootb);
}

void PrettyPrintMN(MULTI_NOD *nod,int nivel=0)
{
	for(int i =0;i<nivel;i++) printf("   ");
	printf("%d\n",nod->cheie);
	for(int i=0;i<nod->count;i++)
		PrettyPrintMN(nod->child[i],nivel+1);
}

void pretty_print(NOD *rad, int d)
{
	if(rad!=NULL)
	{
		pretty_print(rad->dr,d+1);
		for(int i=0;i<d;i++)
		{
			printf("    ");	
		}
		printf("%d\n",rad->cheie);
		pretty_print(rad->stg, d+1);
	}
}
int main()
{
	int t[]={6,2,7,5,2,7,7,-1,5,2};
	int size=sizeof(t)/sizeof(t[0]);
	MULTI_NOD*root=transform1(t,size);
	NOD *rootb=NULL;
	rootb=(NOD*)malloc(sizeof(NOD));
	rootb->cheie=root->cheie;
	rootb->dr=NULL;
	rootb->stg=NULL;
	transform2(root,&rootb);
	//printf("%d",root->child[0]->child[0]->cheie);
	PrettyPrintMN(root);
	printf("\n\n\n");
	pretty_print(rootb,0);
	return 0;
}