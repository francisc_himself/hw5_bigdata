

#include <stdio.h>
#include <conio.h>
#include  <stdlib.h>
#include <time.h>
#include<math.h>


typedef struct _NOD
{
    int key;
    struct _NOD *next;

}NOD;

typedef struct elem
{
    int val;
    int nr_lista;
}ELEM;


NOD *head[500];
NOD *tail[500];
NOD *Lh;
NOD *Lt;
long comp=0,atr=0;

//===============================================================

void generator_aleator(int* v, int n)
{

    int i; //contor

    v[0] = rand() %20;

    for (i=1;i<n;i++)
    {

        *(v+i) = *(v+i-1) + rand() % 10 + 1;
    }

}

int index_min(ELEM* a,int i,int *nr_elem)
{
    if(2*i>*nr_elem)
    {
        return i;
    }

    int min =a[i].val < a[2*i].val?i:(2*i);

    if (2*i+1>*nr_elem) return min;
    min=a[min].val<a[2*i+1].val?min:(2*i+1);
    return min;
}


void Rec_H(ELEM *A, int *n, int i)//reconstructie heapului
{
    int ind=0,x,y;
    ELEM aux;

    x=2*i;
    y=2*i+1;
    ind=index_min(A,i,n);
    comp+=2;


    if(ind!=i)
    {
        aux=A[i];
        A[i]=A[ind];
        A[ind]=aux;
        atr+=3;
    Rec_H(A,n,ind);
    }
}
ELEM H_Pop(ELEM *A,int *n)
{
    ELEM x;
    x=A[1];
    A[1]=A[*n];
//    atr1++;
    (*n)=(*n)-1;
    Rec_H(A,n,1);
    return x;
}

void H_Push(ELEM *A, int *n, ELEM x)
{
    (*n)=(*n)+1;
    A[*n]=x;

	int i=(*n);

    while(i>1 && A[i].val<A[i/2].val )
    {

        ELEM y=A[i];
        A[i]=A[i/2];
        A[i/2]=y;
        i=i/2;
    }
}


void afisare_lista(NOD *h)
{
    NOD *L=h;
    while(L!= NULL )
    {
        printf("%d ",L->key);
        L=L->next;
    }
    printf("\n");
}


void insert(NOD **h, NOD**t,int val)
{
    NOD *p = (NOD *) malloc(sizeof(NOD));
    p->key = val;
    p->next = NULL;

    if(*t == NULL)
    {
        *t = p;
        *h =p;
    }
    else
    {
        (*t)->next = p;
        *t = p;
        if(*h==NULL)
            *h=p;
    }
}

void deleteNod(NOD **h)
{
	NOD *p;
	if(*h!=0)
	{
		p=*h;
		*h=(*h)->next;
		free(p);
	}
}

ELEM read_l(NOD** h, int i)
{
    ELEM c;

	if(*h==0)
	{
		c.val=-1;
		c.nr_lista=-1;
	}
	else{
		c.val=(*h)->key;
		c.nr_lista=i;
		deleteNod(h);
	}


    return c;
}

void stergere_elemente(NOD**h,NOD**t)
{
	NOD** p;

    if(*h!=NULL)
    {
        printf("sterg elementul %d\n",(*h)->key);
        p = h;
        h =&(*h)->next;
        stergere_elemente (h,t);
        free(p);
    }

	if((*t)->next==NULL)
	printf("test");


}

void creare_liste(int n,int k)
{
    int i,j;
    int x[1000];
    for(i=0;i<k;i++)
    {
        generator_aleator(x, n);
        for(j=0;j<n;j++)
        insert(&head[i],&tail[i], x[j]);
    }
}


void interclasare(NOD* h[], int k, ELEM *H)
{
    int i;
    ELEM x;
	int heap_dim=0;

	Lh=0;Lt=0;

    for(i=0;i<k;i++)
    {
       x=read_l(&h[i], i);
       H_Push(H,&heap_dim,x);
    }

    while(heap_dim>0)
    {
        x=H_Pop(H,&heap_dim);
        insert(&Lh, &Lt, x.val);
		x=read_l(&h[x.nr_lista], x.nr_lista);

        if (x.val!=-1)
            H_Push(H,&heap_dim,x);
    }

}


int main()
{
    srand (time(NULL));

    int n,k,l,i;

    int v[]={10,50,100};
    ELEM H[500];
    FILE *fis;
	fis = fopen("grafice.csv","w");
	FILE *fis2;
	fis2 = fopen("grafice2.csv","w");


    n=5;k=3;
    creare_liste(n,k);
    for(i=0;i<k;i++) afisare_lista(head[i]);

    //ELEM H[100];
    interclasare(head, k, H);
    for(i=0;i<k;i++) afisare_lista(head[i]);
    afisare_lista(Lh);

    fprintf(fis, "nr_elem,nr_liste_1,comp_1,atr_1,c+a_1,nr_liste_2,comp_2,atr_2,c+a_2,nr_liste_3,comp_3,atr_3,c+a_3 \n");

	for (n=100;n<=10000;n+=400)
	{
            for (l=0;l<3;l++)
            {
            k=v[l];
			creare_liste(n/k,k);
            interclasare(head, k, H);
			if(l==0) fprintf(fis,"%d, %d, %ld, %ld, %ld, ",n, k, comp,  atr, comp + atr);
			if(l==1) fprintf(fis," %d, %ld, %ld, %ld, ", k, comp,  atr, comp + atr);
			if(l==2) fprintf(fis," %d, %ld, %ld, %ld \n", k, comp,  atr, comp + atr);
			comp=0;
			atr=0;

		}
	}

	getch();

 fprintf(fis2, "nr_liste,comp,atr,comp+atr \n");
 n=10000;
 for(k=10;k<500;k+=10)
 {
 comp=0;
			atr=0;
     creare_liste(n/k,k);
            interclasare(head, k, H);
			 fprintf(fis2,"%d , %ld, %ld, %ld \n", k, comp,  atr, comp + atr);
 }


return 0;
}
