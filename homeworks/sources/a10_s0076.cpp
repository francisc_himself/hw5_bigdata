#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
#define WHITE 0
#define GREY 1
#define BLACK 2


#include "Profiler.h" 
Profiler profiler("BFS");

int distance[10000];
int queue[12000];
int qsize = 0;

void enqueue(int x){// pun in coada
	int i;
	qsize++;
	for(i=qsize;i>=0;i--)
		queue[i+1] = queue[i];
	queue[0] = x;
	
	
}

int dequeue(){// scot din coada
	int i;
	int x;
	x = queue[0];
	for(i=0;i<qsize;i++)
		queue[i] = queue[i+1];
	qsize--;
	return x;
}

struct nod{
	int v;
	nod *next;
};

nod * first[60001], *last[60001];
int c[60001],p[60001],d[60001],f[60001],t;
int V, E;
bool VERTEX;

void generate(int n,int nr) // genereaza lista de adiacenta
{
	srand((unsigned)time(0));
	int i,u,v, dist;
	for(i=0;i<=n;i++)
		first[i]=last[i]=NULL;
	for(i=1;i<=nr;i++)
	{
		u = rand() % n + 1;
		v = rand() % n + 1;
		if(first[u] == NULL)
		{
			first[u]=(nod *)malloc(sizeof(nod *));
			while(v == u)
				v = rand() % n + 1;
			first[u]->v = v;
			first[u]->next = NULL;
			last[u] = first[u];

			if(first[v] == NULL){
				first[v] = (nod *)malloc(sizeof(nod *));
				first[v]->v = u;
				first[v]->next = NULL;
				last[v] = first[v];
			}
			else{
				nod *p, *x;
				p = (nod *)malloc(sizeof(nod *));
				p->v = u;
				last[v]->next = p;
				p->next = NULL;
				last[v] = p;
			}
		}
		else {
			nod *p, *x;
			p = (nod *)malloc(sizeof(nod *));
			label1: x = first[u];
			while(x != NULL){
				if(x->v == v || v == u){
					v = rand() % n + 1;
					goto label1;
				}
				x = x->next; 
			}
			p->v = v;
			p->next = NULL;
			last[u]->next = p;
			last[u] = p;
			
			if(first[v] == NULL){
				first[v] = (nod *)malloc(sizeof(nod *));
				first[v]->v = u;
				first[v]->next = NULL;
				last[v] = first[v];
			}
			
			else {
				nod *p, *x;
				p = (nod *)malloc(sizeof(nod *));
				p->v = u;
				p->next = NULL;
				last[v]->next = p;
				last[v] = p;
			}
		}
	}
}

void initialize(int n)
{
	t=0;
	int i;
	for(i=1;i<=n;i++)
	{
		d[i]=0;// distanta de la sursa la nodul i
		p[i]=0;// nodul precedent
		c[i]=0;// culoarea nodului i
	}
}

void BFS(int s, int n)
{
	int u, firstv ;
	bool done = false;
	nod *current;
	c[s] = WHITE; // initializam culoarea sursei cu alb
	d[s] = 0;// initializam distanta de sursa cu 0
	enqueue(s);
	printf("%d\n", s);
	while(qsize != 0){
		u = dequeue();
		current = first[u];
		if(VERTEX)
			profiler.countOperation("Total - vertex", V, 1);
		else
			profiler.countOperation("Total - edge", E, 1);
		while(current != NULL){
			if(c[current->v] == WHITE){
				for(int i=0;i<=d[u];i++) printf("   ");
				printf("%d\n", current->v);
				c[current->v] = GREY;
				d[current->v] = d[u] + 1;
				p[current->v] = u;
				if(VERTEX)
					profiler.countOperation("Total - vertex", V, 6);
				else
					profiler.countOperation("Total - edge", E, 6);
				
				enqueue(current->v);
			}
			current = current->next;
			if(VERTEX)
				profiler.countOperation("Total - vertex", V, 1);
			else
				profiler.countOperation("Total - edge", E, 1);
		}
		c[u] = BLACK;
		
		}
}

void print(int n)// printam lista de adiacenta
{
	int i;
	nod *p;
	for(i=1; i<=n; i++)
	{
		printf("%d : ",i);
		p=first[i];
		while(p != NULL)
		{
			printf("%d,",p->v);
			p = p->next;
		}
		printf("\n");
	}
}


void test()  
{
	int i=1, n, nr, first;
	bool done = false;
	n=10;
	nr=11;
	generate(n,nr);
	print(n);
	initialize(n);
	for(i=1; i<=n; i++){
		if(c[i] == WHITE){
			BFS(i, n);
		}
	}
}

void main()
{
	test();
	int i;
	//V = 100;
	/*VERTEX = false;
	for(E=1000; E<=4000; E=E+100){
		generate(V, E);
		initialize(V);
		for(i=1; i<=V; i++){
		if(c[i] == WHITE){
			BFS(i, V);
		}
		
		}
		printf("Done for %d\n", E);
	}
	E = 4000;
	VERTEX = true;
	for(V=110; V<=200; V=V+10){
		generate(V, E);
		initialize(V);
		for(i=1; i<=V; i++){
		if(c[i] == WHITE){
			BFS(i, V);
		}
		}
		printf("Done for %d\n", V);
	}*/
	//profiler.showReport();
	printf("\n Done all!");
	getch();
	
}
		





