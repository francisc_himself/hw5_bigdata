//Name: ***ANONIM*** ***ANONIM***
//Group: ***GROUP_NUMBER***
//Date: 5 april 2013
//laboratorul 5 hash-tables
/*
	In acest laborator incercam sa urmarim comportamentul hashTable-urilor. Acestea sunt foarte importante in memorarea informatiilor foarte multe (baze
	de date uriase, pagini web, etc)
	Se observa ca factorul de umplere este foarte putin influentabil pentru elementele ce sunt in hash, care se face in medie in 2-3 operatii intre 80-95 la suta
	 i in 5 operatii la 99 la suta.
	Problema apare la cautarea elementelor care nu se afla in hash. pana la un factor de umplere de 90 la suta, se comporta acceptabil, in medie cu
	13 operatii de cautare, insa peste 90 numarul de operatii de cautare creste, 37 la 95 si peste 100 la 99. 
	In urma acestor analize, este bine sa pastram hashul la 80-90 la suta factor de umplere
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include "Profiler.h"

//dimensiunea hash-table-ului
#define HASH_SIZE 30
//numarul de elemente de cautat
#define SEARCH_SIZE 1500

int hashTable[HASH_SIZE], index[HASH_SIZE], samples[HASH_SIZE+SEARCH_SIZE], indices[SEARCH_SIZE];

int hash(int key){
	return key % HASH_SIZE;
}

int h(int key, int index){
	int c1 = 2;
	int c2 = 1;
	return (hash(key)+c1*index+c2*index*index)%HASH_SIZE;
}

int insert( int key){
	int i,hashCode;
	i = 0;
	while(i<HASH_SIZE){
		hashCode = h(key,i);
		if(hashTable[hashCode]==0){
			hashTable[hashCode] = key;
			index[hashCode] = i+1;
			return hashCode;
		}
		i++;
	}
	return -1;
}

int search(int key, double *operation){
	int i,hashCode;
	i = 0;
	hashCode = h(key,i);
	while(hashTable[hashCode]!=0){
		*operation +=1;
		if (hashTable[hashCode]==key){
			return hashCode;
		}
		else{
			i++;
		}
		hashCode = h(key,i);
	}
	return -1;
}

int main(){
	int i, dim1, fill,j;	
	double operationFound, avgOperationsFound;
	double operationNotFound, avgOperationsNotFound;

	int fillFactor[5] ={80,85,90,95,99};
	
	/* test
	for(i = 1; i<13; i++){
		insert(i*i);
	}

	for(i=0;i<HASH_SIZE;i++){
		printf("%d %d\n",hashTable[i],index[i]);
	}

				printf("%d \n",search(10*10,&operationNotFound));
				printf("%d \n",search(15*15,&operationNotFound));
				*/
	
	for(dim1 = 0; dim1 <=4; dim1++){
		avgOperationsFound = 0;
		avgOperationsNotFound = 0;
		for(j = 0; j<5; j++){	
			fill = (int)((HASH_SIZE*fillFactor[dim1])/100);
			

			for (i = 0; i<HASH_SIZE; i++){
				hashTable[i] = 0;
			}

			for (i = 0; i<HASH_SIZE+SEARCH_SIZE; i++){		
				samples[i] = 0;
			}

			FillRandomArray(samples,fill+SEARCH_SIZE,1,1000000,true,0);
			FillRandomArray(indices,SEARCH_SIZE,0,fill,true,0);

			for (i = 0; i<fill; i++){
				insert(samples[i]);
			}

			operationNotFound = 0;

			for(i=fill; i<fill+SEARCH_SIZE; i++){
				search(samples[i],&operationNotFound);
			}

			operationNotFound /= SEARCH_SIZE; 

			avgOperationsNotFound += operationNotFound;

			operationFound = 0;

			for(i=0; i<SEARCH_SIZE; i++){
				search(samples[indices[i]],&operationFound);
			}

			operationFound /= SEARCH_SIZE; 

			avgOperationsFound += operationFound;

		}
		avgOperationsFound /= 5;
		avgOperationsNotFound /= 5;
		printf("fill: %d , opFound: %f , opNotFound: %f\n",fillFactor[dim1],avgOperationsFound,avgOperationsNotFound);
	}

	printf("Hello World!\n");
	getch();
	return 0;
}



