//[af2013] Assign1_***ANONIM***_***GROUP_NUMBER***5
/*
Am putut observa forma liniara si constanta a grficelor in cazurile defavorabile si favorabile, numarul de
comparatii, atribuiri si sume find unul constant.
Am putut sa observ fluctuatia in jurul unei valori fixe in cazurile medii statistice
*/
#include<stdio.h>
#include<fstream>
using namespace std;

int atrib1=0;
int suma1=0;
int comp1=0;

int atrib2=0;
int suma2=0;
int comp2=0;

int atrib3=0;
int suma3=0;
int comp3=0;

//afisare
void afisare(int a[], int n)
{
	for(int i=0;i<n;i++)
		printf("%d ",a[i]);
	printf("\n");

}

//sortari
//1. Metoda bulelor

void bubbleSort(int a[], int n)
{
	int sortat=0;
																atrib1++;
	int aux;
	while(sortat==0)
	{
		sortat = 1;												atrib1++;
		for(int i=0;i<n-1;i++)
		{														atrib1++;comp1++;suma1++; suma1++;
			if(a[i]>a[i+1])								
			{
				aux = a[i];										atrib1++;comp1++; suma1++;
				a[i] = a[i+1];									atrib1++;suma1++; suma1++; suma1++;
				a[i+1] = aux;									atrib1++;suma1++; suma1++;
				sortat = 0;										atrib1++;
			}
			n--;	
			atrib1++;											atrib1++; suma1++;
		}
	}
}

//2. Sortare prin insertie
/*
void insertie(int a[], int n)
{
	int x, j;
	for(int i=0;i<n;i++)
	{
		x=a[i];
		j=1;
		while(a[j]<x)
			j=j+1;
		for(int k=i;k>j+1;k--)
			a[k]=a[k-1];
		a[j]=x;
	}
}*/

void insertie(int a[], int n)
{
	int key, i;
	for(int j=1;j<n;j++)					
	{
		key=a[j];						atrib2++; atrib2++; comp2++; suma2++;
		i = j-1;						atrib2++; comp2++; comp2++; suma2++;
		while((i>=0)&&(a[i]>key))
		{
			a[i+1]=a[i];				atrib2++; suma2++; suma2++;
			i = i-1;					atrib2++;
		}
		a[i+1] =key;					atrib2++; suma2++;
	}
}

//3. Sortarea prin selectie
void selectie(int a[], int n)
{
	int i;
	int imin=4, aux;					atrib3++;
	for(i=0;i<n;i++)
	{
		imin=i;							atrib3++; atrib3++; comp3++; suma3++; suma3++; atrib3++; comp3++;
		//printf("%d \n",i);
		for(int j=i+1;j<n;j++)
		{								 comp3++; suma3++; suma3++;
			if(a[j]<a[imin])
				imin=j;
			//printf("+");
		}
		aux = a[i];						 suma3++;atrib3++; suma3++; 
		a[i]=a[imin];					atrib3++;  suma3++; suma3++;
		a[imin]=aux;					atrib3++;  suma3++; suma3++;
	}
}


void genRandom(int a[])
{
	for(int i=0;i<100;i++)
		a[i]=rand();
}

void genRandomCresc(int a[])
{
	a[0]=1;
	for(int i=1;i<100;i++)
		a[i]=a[i-1]+rand();
}

void genRandomDescresc(int a[])
{
	a[0]=INT_MAX;
	for(int i=1;i<100;i++)
		a[i]=a[i-1]-rand();
}

void copiereSir(int a[], int b[], int n)
{
	for(int i=0;i<n;i++)
		b[i]=a[i];
}


//
int main()
{
	//int a[10]={5, 7, 9, 1, 3};
	
	//bulele
	//bubbleSort(a,n);
	//insertie(a,n);
	//selectie(a,n);
	
	int n =100;
	int a[150];
	int med1=0, med2=0, med3=0;
	int med11=0, med21=0, med31=0;
	int med12=0, med22=0, med32=0;
	
	int b[150];
	FILE *out1, *out2, *out3;
	out1=fopen("buble.csv","w");
	out2=fopen("selectie.csv","w");
	out3=fopen("inserare.csv","w");
	
	for(int con=0;con<10000;con++)
	{

		atrib1=0;
		suma1=0;
		comp1=0;

		atrib2=0;
		suma2=0;
		comp2=0;

		atrib3=0;
		suma3=0;
		comp3=0;


		for(int i=0;i<5;i++)
		{

			genRandomDescresc(a);
			copiereSir(a,b,150);
			afisare(a,100);
			bubbleSort(a,n);
			med1=med1+atrib1;
			med2=med2+comp1;
			med3=med3+suma1;

			copiereSir(b,a,150);
			insertie(a,n);
			med11=med11+atrib2;
			med21=med21+comp2;
			med31=med31+suma2;

			copiereSir(b,a,150);
			selectie(a,n);
			med12=med12+atrib3;
			med22=med21+comp3;
			med32=med31+suma3;

		}
		med1=med1/5;
		med2=med2/5;
		med3=med3/5;
		med11=med11/5;
		med21=med21/5;
		med31=med31/5;
		med12=med12/5;
		med22=med22/5;
		med32=med32/5;
		fprintf(out1, "%d,%d,%d,%d\n",con,med1,med2,med3);
		fprintf(out2, "%d,%d,%d,%d\n",con,med11,med21,med31);	
		fprintf(out3, "%d,%d,%d,%d\n",con,med12,med22,med32);	
	}

	fclose(out1);
	fclose(out2);
	fclose(out3);
	

	return 0;
}
