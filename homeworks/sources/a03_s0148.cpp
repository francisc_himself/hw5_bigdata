
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>
/*HeapSort este mai slab (dar nu cu mult) decat QuickSort, dar are marele avantaj fata de acesta ca nu este recursiv. 
Algoritmii recursivi consuma o mare cantitate de memorie.
HeapSort are eficienta O(nlog n)
QuickSort are tot O(n log n) dar pe cazul worst are O(n^2)*/


long compH;
long assignH;
long compQ;
long assignQ;
long heapSize;



//generare de numere random
void genRandom(long a[], int inc)
{
	for(int i=0;i<inc;i++)
		a[i]=rand();
}

//generare numere random crescator
void genRandomCresc(long a[], int inc)
{
	a[0]=1;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]+rand();
}

//generare numere random descrescator
void genRandomDescresc(long a[], int inc)
{
	a[0]=LONG_MAX;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]-rand();
}//---------------------------------------------------------------------------


//--------------------------copiere sir
void copiereSir(long b[], long a[], int n)
{
	for(int i=0;i<n;i++)
		b[i]=a[i];
}
//----------------------------

//---------------------------heapify-ul
void heapify(long a[], int i)  
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    compH=compH+1;
    if (left<=heapSize && a[left]>a[i]) 
        largest=left;	
    else
        largest=i;

    compH=compH+1;
    if (right<=heapSize && a[right]>a[largest])
        largest=right;
    if (largest!=i) 
        {
            assignH+=3;
            aux=a[i];
            a[i]=a[largest];
            a[largest]=aux;
            heapify(a,largest);  
        }
}

//----------------------------cpnstructie bottomup la heam
void bh_bottomup(long A[],int n)
{
    int i;
	heapSize=n;
    for (i=heapSize/2;i>=1;i--) 
        heapify(A,i); 
}

//----------------------------------heapsort
void heapSort(long a[],int n)
{
    int k;
    long aux;
    bh_bottomup(a,n);
    for (k=n;k>=2;k--)
    {
        assignH+=3;
        aux=a[1];
        a[1]=a[k];
        a[k]=aux;
        heapSize=heapSize-1;
        heapify(a,1);
    }
}


//----------------------------------------partitionarea
int partition(long a[],int p, int r,int pivot){

	long x,aux;
	int i,j;
	if(pivot==1)
		x=a[r];
	else if(pivot==2)
		x=a[(p+r)/2];
	else if(pivot==3)
		x=a[p+1];
	assignQ++;
	i=p-1;
	for(j=p;j<=r-1;j++){
		compQ++;
		if(a[j]<=x)
			{i++;
			assignQ+=3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	assignQ+=3;
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;

	return i+1;
}


//--------------------------------------------quicksort-ul
void quickSort(long a[],int p, int r,int pivot)
{	if ( p < r )
    {
        int q = partition(a, p, r,pivot); 
			quickSort(a, p, q-1,pivot);
			quickSort(a, q+1, r,pivot);
    }
}


//---------------------------------------------main-ul
int main()
{
	srand(time(NULL));
    long heap[10001];
    long quick[10001];
    int j,k;
    long compHeap=0,compQuick=0,assignHeap=0,assignQuick=0;

	
    FILE *f,*av,*b,*w;

     f=fopen("average.csv","w");
	w=fopen("worst_quicksort.csv","w");
	av=fopen("average_quicksort.csv","w");
	b=fopen("best_quicksort.csv","w");
	
	
	long a[11];
	a[0]=0;
	a[1]=15;
	a[2]=4;
	a[3]=1;
	a[4]=11;
	a[5]=25;
	a[6]=9;
	a[7]=8;
	a[8]=8;
	a[9]=12;
	a[10]=19;
	heapSort(a,10);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n");

	assignQ=compQ=0;
	quickSort(a,1,10,1);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n%d %d ",assignQ,compQ);

	

	
	//cazul mediu statistic
	//luam lungimea sirului cu increment de 100
	//fprintf(av,"i,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5");
	//fprintf(f,"i,compHeap/5,assignHeap/5,compHeap/5+assignHeap/5,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5");
    for (int i=100;i<=10000;i=i+100)
	{
		compH=0;
        assignH=0;
        compQ=0;
        assignQ=0;
		//luam cate 5 cazuri
		for (j=1;j<=5;j++)
		{

			compHeap=0;
			compQuick=0;
			assignHeap=0;
			assignQuick=0;

			//copiem sirul ca sa fie identice pentru ambele metode de sortare
			genRandom(heap,i);
			copiereSir(quick, heap, i);


			//apelam  sortarile
			heapSort(heap,i);
			quickSort(quick,1,i,1);

			compHeap+=compH;
			compQuick+=compQ;
			assignHeap+=assignH;
			assignQuick+=assignQ;
		}

			//scriem in fisier cazul favorabil
			fprintf(av,"%d,%ld,%ld,%ld \n",i,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5);
			fprintf(f,"%d,%ld,%ld,%ld,%ld,%ld,%ld \n",i,compHeap/5,assignHeap/5,compHeap/5+assignHeap/5,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5);
	}

	//cazul favorabil
	//fprintf(b,"i,compQ,assignQ,compQ+assignQ");
	for (int i=100;i<=10000;i=i+100)
	{
		compQ=assignQ=0;

		//generam sirul crescator
		 genRandomCresc(quick,i);
		
		 //sortam
		quickSort(quick,1,i,2);
		fprintf(b,"%d,%ld,%ld,%ld  \n",i,compQ,assignQ,compQ+assignQ);
		
	}

	
	//caz defavorabil
	//fprintf(w,"i,compQ,assignQ,compQ+assignQ");
	for (int i=100;i<=5000;i=i+100)
	{
		compQ=assignQ=0;

		//generam sirul
		genRandomDescresc(quick, i);

		//sortam
		quickSort(quick,1,i,3);
		fprintf(w,"%d,%ld,%ld,%ld  \n",i,compQ,assignQ,compQ+assignQ);
		
	}
	
	
	fclose(f);
	fclose(av);
	fclose(b);
	fclose(w);
	//getch();
	return 0;

	
}
