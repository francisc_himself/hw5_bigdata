# include <stdio.h>
# include <stdlib.h>
#include <conio.h>
# include <iostream>
using namespace::std;


typedef struct _multi_node
{
	 int key;
	 int count;
	 struct _multi_node *child[50];
}MN;

 MN* root;

typedef struct _multi_node2
{
	int key;
	_multi_node2 *child;
	_multi_node2 *brother;
}MN2;

MN2* root2;


MN *createMN(int key){
	MN *p=(MN*)malloc(sizeof(MN));
	p->key=key;
	p->count=0;
	return p;
}

void insertMN(MN *parent,MN *child)
{
	parent->child[parent->count++]=child;
}

//transformarea 1: i-cheia, t[i]-parinte
MN *transformare1(int t[], int size){
	MN *root=NULL;
	MN *nodes[50];
	for(int i=0; i<size;i++){
		nodes[i]=createMN(i);
		if(t[i]==-1)
			root=nodes[i];
	}
	for(int i=0;i<size;i++){
		if(t[i]!=-1)
			insertMN(nodes[t[i]],nodes[i]);
	}
	return root;
}

void alocare1(MN2*root2,MN*root1)
{
	root2->child= (MN2*)malloc(sizeof(MN2));
    root2->child->key=root1->child[0]->key;
	root2=root2->child;
	
}

void alocare2(MN2*root2,MN*root1){
	for(int i=1;i<root1->count;i++)
	{
	root2->brother=(MN2*)malloc(sizeof(MN2));
	root2->brother->key=root1->child[i]->key;
	root2=root2->brother;
  }
}
//transformarea 2
void transformare2(MN*root1,MN2*root2){
	if(root1!=NULL)
	{
		if(root1->count>0)
		{
			//printf("+");
			/*MN2 *copil;
			
			copil = (MN2*)malloc(sizeof(MN2));*/
			//alocare1(root2,root1);
			root2->child= (MN2*)malloc(sizeof(MN2));
			//printf("+");
			root2->child->key=root1->child[0]->key;
			root2=root2->child;
			transformare2(root1->child[0],root2);
			//root2=root2->child;
			//alocare2(root2,root1);
			for(int i=1;i<root1->count;i++)
			{
				root2->brother=(MN2*)malloc(sizeof(MN2));
				root2->brother->key=root1->child[i]->key;
				root2=root2->brother;
				transformare2(root1->child[i],root2);
				//root2=root2->brother;
			}
			root2->brother=NULL;
		}
		else {
			root2->child=NULL;
		}
	}
}


//parametru optional: default 0
void prettyPrint(MN *nod, int nivel=0){
	for(int i=0; i<nivel;i++)
		printf("  ");
		printf("%d\n",nod->key);
		for(int i=0;i<nod->count;i++)
		{
			prettyPrint(nod->child[i],nivel+1);
		}
}



void pretty_print( MN2 *root, int nivel)// afisare in al doilea caz, binar -- pretty
{
    if(root==NULL) 
	    return;
	for (int i = 0; i < nivel; i++ )
		{
			printf("\t");
        }
	 printf("%d\n",root->key);
	 //printf("+");
     pretty_print(root->child,nivel+1);
	 pretty_print(root->brother,nivel);

   }


int main()
{
	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int n=11;
	int size=sizeof(t)/sizeof(t[0]); //se declara una dupa alta, in acelasi bloc

	printf("Vectorul de tati si nodurile coresp:\n");
	for(int i=0;i<11;i++)
		printf("  %d",t[i]);
	printf("\n");
	for(int i=0;i<n;i++)
		printf("  |");
	printf("\n");
	for(int i=0;i<11;i++)
		printf("  %d",i);

    printf("\nTransformarea1:\n");
	MN *root=transformare1(t,size);
	prettyPrint(root);
	
	MN2* root2=(MN2*)malloc(sizeof(MN2));
	root2->key=root->key;
	root2->brother=NULL;

	transformare2(root,root2);
	
	printf("Transformarea 2:\n");
	pretty_print(root2,0);
	return 0;
}

