#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"

#define size 10007

Profiler profiler("HashTable");

int foundCount,foundNr;
int maxFound,maxNotFound;
int notFoundCount,notFoundNr;

void initialize(int arr[],int n)
{
	for(int i = 0;i<n;i++)
	{
		arr[i] = -1;
	}
}


int hashfuncQuad(int data,int i,int n)
{
	return (data % n + i*i) % n;
}

int openInsert(int arr[],int n,int key,int (* hashfunc)(int data,int i,int n))
{
	int i = 0;
	int index = hashfunc(key,i,n);
	while(i < n && arr[index] != -1)
	{
		i++;
		index = hashfunc(key,i,n);
	}
	if (arr[index] == -1)
	{
		arr[index] = key;
		return 1;
	}
	else
	{
		printf("hashtable full/n");
		return -1;
	}
}

int search(int arr[],int n,int key,int (* hashfunc)(int data,int i,int n))
{
	int i = 0;
	int count = 0;
	int index = hashfunc(key,i,n);
	count++;
	while(i < n && arr[index] != key && arr[index] != -1)
	{
		i++;
		count++;
		index = hashfunc(key,i,n);
	}
	if (arr[index] == key)
	{
		foundCount +=count;
		if(count > maxFound)
		{
			maxFound = count;
		}
		return index;
	}
	else
	{
		notFoundCount +=count;
		if(count > maxNotFound)
		{
			maxNotFound = count;
		}
		return -1;
	}
}


void printHTable(int arr[],int n)
{
	for(int i = 0;i < n;i++)
	{
		if(arr[i] != -1)
		{
			printf("\n[%d]=%d",i,arr[i]); 
		}
	}
}

int main()
{
	int arr[size+1];
	int b[2*size+1];
	int n;
	int step;
	
	FillRandomArray(b,2*size,10,50000,true,0);
	initialize(arr,size);
	/*test case*/
	/*for (int i = 0;i < 10;i++)
	{
		openInsert(arr,50,b[i],hashfuncQuad);
	}
		printHTable(arr,50);
	int pos;
	printf("\nsearched: %d",b[5]);
	if((pos = search(arr,50,b[5],hashfuncQuad)) >= 0)
	{
				printf("\nfound at %d",pos);
	}
	else
	{
				printf("\nnot found");
	}
	printf("\nsearched: %d",b[15]);
	if((pos = search(arr,50,b[15],hashfuncQuad)) >= 0)
	{
				printf("\nfound at %d",pos);
	}
	else
	{
				printf("\nnot found");
	}
	*/
	
	/**/
	FILE * out = fopen("output.csv","w");
	fprintf(out,"fillfactor,nr(found),Avg. eff.(found),Max eff.(found),nr(not found),Avg. eff.(not found),Max eff.(not found)\n");
	float fillfactor[5] = {0.8, 0.85, 0.9, 0.95, 0.99};

	for(int k = 0;k<5;k++)
	{
		initialize(arr,size);
		n =(int)((float)size*fillfactor[k]);
		foundCount = 0;
		notFoundCount = 0;
		foundNr = 0;
		notFoundNr = 0;
		maxFound = 0;
		maxNotFound = 0;
		for (int i = 0;i<n;i++)
		{
			openInsert(arr,size,b[i],hashfuncQuad);
			
		}
		step = (int)(n/1500); 
		for (int i = 0;i < 2*n;i += step)
		{
			if(search(arr,size,b[i],hashfuncQuad) >= 0)
			{
				foundNr++;
			}
			else
			{
				notFoundNr++;
			}
		}
		fprintf(out,"%f,%d,%f,%d,%d,%f,%d\n",fillfactor[k],foundNr,(float)foundCount/(float)foundNr,maxFound,notFoundNr,(float)notFoundCount/(float)notFoundNr,maxNotFound);

	}
	fclose(out);
	
	printf("done");
	_getch();
	return 0;
}