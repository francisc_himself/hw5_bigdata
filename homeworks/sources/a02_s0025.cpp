// Tema2.cpp : Defines the entry point for the console application.
// ***ANONIM*** ***ANONIM***, Grupa: ***GROUP_NUMBER***
//In cazul construirii Min-Heap-ului prin tehnica de Bottom-up, apar mai putine atribuiri si comparatii, decat in cazul construirii Min-Heap-ului prin tehnica Top=down. 

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 
//n este dimensiunea min-heap-ului
int n=10;     //lui n i se atribuie valoarea 10 pentru verificarea functionarii algoritmilor
int dim=0;   //dim este dimensiunea min-heap-ului construit prin tehnica top-down
int a[10001];
int b[10001];

//contoarele pentru atribuiri si comparatii
int cbu1=0, cbu2=0, ctd1=0,ctd2=0;

//functie parinte
int parent(int i)
{
	return i/2;

}
//functie pentru indicele din stanga
int left(int i)
{

	return (2*i);
}
//functie pentru indicele din dreapta
int right(int i)
{
    return (2*i)+1;
}




//construire min-heap prin tehnica de bottom-up

//functia MIN_HEAPIFY este pentru intretinerea heap-ului
void MIN_HEAPIFY(int a[],int i)
{
	int l=left(i);
	int r=right(i);
	int min, aux;
	cbu1++;
	if ((l<=n) && (a[l]<a[i]))
		min=l;
	else min=i;
	cbu1++;
	if ((r<=n) && (a[r]<a[min]))
		min=r;
	if (min!=i)
	{   cbu2++; 
		aux=a[i];
		a[i]=a[min];
		a[min]=aux;
		MIN_HEAPIFY(a,min);
    }
	

}

//cu ajutorul functiei MIN_HEAPIFY construim heap-ul prin tehnica Bottom-up

void BUILD_MIN_HEAP_BU(int a[])
{
	int i;
	cbu1=0; cbu2=0;
    for (i=n/2; i>=1; i--)
	  	MIN_HEAPIFY(a,i);


}

//acum facem constuirea min-heap-ului cu tehnica top-down
//initializarea
void H_INIT(int a[])
{
	dim=0;
}

//inserarea nodurilor in arborele binar
void H_PUSH(int a[], int x)
{
	int i, aux;
	ctd2++;
	dim=dim+1;
	a[dim]=x;
	i=dim;
	ctd1++;
	while ((i>1) && (a[i]<b[parent(i)]))
	{
		ctd1++;
		ctd2++;
		aux=a[i];
		a[i]=a[parent(i)];
		a[parent(i)]=aux;
		i=parent(i);

	}

}


//construirea arborelui prin tehnica Top-down
void BUILD_MIN_HEAP_TD(int b[],int a[])
{
	H_INIT(b);
	ctd1=0; ctd2=0;
	int i;
	for (i=1; i<=n; i++)
		H_PUSH(b,a[i]);



}




int main()
{
    
	int c[10001];
	int i;
	int j=0;
	//verificarea functionarii algoritmilor
	for (i=1; i<=n; i++)
		a[i]=n-i;
	for (i=1; i<=n; i++)
		printf("%d ",a[i]);
	printf("\n");
	BUILD_MIN_HEAP_BU(a);
    for (i=1; i<=n; i++)
		printf("%d ",a[i]); 
	printf("\n"); 
	BUILD_MIN_HEAP_TD(b,a);
	for (i=1; i<=dim; i++)
		printf("%d ",b[i]);

	//scrierea atribuirilor si comparatiilor in fisierul "heap_file.csv"
	FILE *fp;
	fp=fopen("heap_file.csv","w");
	int at1,at2,comp1,comp2;

	//scriu in tabel
	fprintf(fp,"n, Bottom-up-at, Bottom-up-comp, Bottom-up-at+comp, Top-down-at, Top-down-comp, Top-down-at+comp");
	fprintf(fp,"\n");
	for (n=100; n<=10000; n=n+100)
	{
		fprintf(fp,"%d,",n);
		at1=0; at2=0; comp1=0; comp2=0;
		j++;
		printf("%d \n",j);
		for (int k=1; k<=5; k++)
		{
			srand(time(NULL));
			for (i=1; i<=n; i++)
			{
				a[i]=rand()%1000;
				c[i]=a[i];
            }
			BUILD_MIN_HEAP_BU(c);
			comp1=comp1+cbu1;
			at1=at1+cbu2;

			BUILD_MIN_HEAP_TD(b,a);
			comp2=comp2+ctd1;
			at2=at2+ctd1;



		}
		fprintf(fp,"%d,%d,%d,%d,%d,%d\n",at1/5,comp1/5,(at1+comp1)/5, at2/5,comp2/5, (at2+comp2)/5);
   }
	fclose(fp);

  getch();


return 0;
}


