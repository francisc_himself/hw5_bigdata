// Tema5-HashTable.cpp : Defines the entry point for the console application.
// ***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***

#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include <time.h>
#define m 9973

int t[m]={-1};
int s[m/2]={0};
int max=-600;
int efort=0,suma;
int pasi;


//functia h, care imi calculeaza locatia in tabela de dispersie unde ar trebui sa inserez elementul
int h(int key, int i)
{
	int c1=1,c2=1;
	int p=(key  % m + c1 *i +c2*i*i) % m;
	return p;
}

//functia de inserare in tabela de dispersie, care imi returneaza locatia in care am inserat elementul in tabela, sau -1 daca am depasit tabela de dispersie

int DISPERSIE_INSEREAZA(int *t, int k)
{
	int i,j;
    i=0;
    do
	{
    j=h(k,i);
    if (t[j]==-1) {
                    t[j]=k;
					pasi++;
                    return j;
    }
    else i=i+1;
	}
    while(i < m);
	return -1; //eroare depasire tabela de dispersie
}

void DISPERSIE_CAUTA(int *t, int k)
{
	efort=0;
	int i,j;
    i=0;
	do
	{
    j=h(k,i);
	efort=efort+1;
    if (t[j]==k) 
		break;
    i=i+1;
	}
    while ((t[j]!=-1)&&(i!=m));
	
	suma =suma + efort;//suma efort

	if (efort > max) //efortul maxim pt operatia de cautare
		max=efort;
}


void testare(double alfa)
{
	int x;
	int k=0;

	int nmax;
	int nmed=0;
	int smed=0;
	int smax=0;
	suma=0;
	max=0;
	for(int r=0;r<=m;r++)
		t[r]=-1;

	for (x=0; x<m/2; x++)
		s[x]=0;
	pasi = 0;
	for (int j=0; j < alfa * m ; j++) {

		x=rand() * rand() % 100000;

	   DISPERSIE_INSEREAZA(t,x);
		if (pasi % 5 == 0)
			s[k++] = x;
		

		
	}
	//cautare 
	for (int x = 0; x < k; x++)
		DISPERSIE_CAUTA(t,s[x]);

	smed=suma/k;
	smax=max;

	suma=0;
	max=0;
	//negasite
	for (int p=0; p < 1500 ; p++){
		int w=rand() * rand() % 100000 + 100000;
	//	printf("%d",w);
		DISPERSIE_CAUTA(t,w);
	}
	nmed=suma/1500; //efort mediu=suma eforturilor/nr elemente
	nmax=max;//nr de eforturi maxim pt o op de search;

	printf("\n %0.2f\t\t%d\t\t%d\t\t%d\t\t%d",alfa, smed, smax, nmed,nmax);
}
int main()
{
	printf("\n Fact.umplere|\tEF.MEDIU-gasite|\tEF.MAX-gasite|\tEF.MEDIU-negasite|\tEF.MAX-negasite|");
	srand(time(NULL));
	testare(0.8);
	testare(0.85);
	testare(0.9);
	testare(0.95);
	testare(0.99);
	getch();
	return 0;
}





