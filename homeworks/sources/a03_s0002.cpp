/****ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER*** 
	Tema4 Heapsort si Quicksort
	Pentru cazul mediu statistic am observat ca Quicksort efectueaza un numar mai mic de 
	asignari si compari decat Heapsort
	In concluzie algoritmul QuickSort pare mai rapid si mai eficient decat HeapSort
	Complexitatea in cazul mediu la ambele tipuri de sortari este O(n*log2(n))
	insa in cazul defavorbil complexitatea la algoritmul Quicksort este O(n^2)
	Daca sirul este deja sortat(caz favorabil) Heapsort este mai putin eficient ca n cazul mediu sattistic,
	dar Quicksort este mai eficient
	Daca sirul este deja sortat descrescator(caz defavorabil)Ouicksort este mai putin eficient ca n cazul mediu sattistic,
	dar Heapsort este mai eficient*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>

#define MAX_SIZE 10001
int k_compHeap,k_asigHeap,k_compQuick,k_asigQuick;

void generare(int sir[],int n) //functie pt generare sir aleator
{
	for(int i=1;i<=n;i++)
		sir[i]=rand();
}

void generare_cresc(int sir[],int n)  //functie pt generare sir crescator
{
	sir[1]=rand()%10;
	for(int i=2;i<=n;i++)
		sir[i]=sir[i-1]+rand()%10;
}

void generare_descresc(int sir[],int n)  //functie pt generare sir descrescator
{
	sir[n]=rand()%10;
	for(int i=n-1;i>=1;i--)
		sir[i]=sir[i+1]+rand()%10;
}

void afisare(int sir[],int n) //functie pt afisare sir
{
	for(int i=1;i<=n;i++)
		printf("%d  ", sir[i]);
	printf("\n");

}

int p(int i)
{
	return i/2;
}


int dr(int i)
{
	return 2*i+1;
}

int st(int i)
{
	return 2*i;
}

void ReconstituieHeap(int A[], int i, int n)
{
	int s,d,ind,aux;
	s=st(i);
	d=dr(i);
	if(s<=n && A[s]>A[i])
		ind=s;
	else
		ind=i;
	k_compHeap+=2;
	if(d<=n && A[d]>A[ind])
	{
		ind=d;
	}
	if(ind!=i)
	{
		aux=A[i];
		A[i]=A[ind];
		A[ind]=aux;
		k_asigHeap+=3;
		ReconstituieHeap(A,ind,n);
	}
}

void construireHeap_BU(int A[], int n)
{
	for(int i = n/2; i>0; i--)	
		ReconstituieHeap(A, i, n);
}

void HEAPSORT(int A[],int n)
{
	construireHeap_BU(A,n);
	int aux;
	for(int i=n;i>1;i--)
	{
		aux=A[1];
		A[1]=A[i];
		A[i]=aux;
		k_asigHeap+=3;
		n=n-1;
		ReconstituieHeap(A,1,n);
	}
}

int Partitionare(int A[], int p, int r)
{
	int x,aux,i,j;
	x=A[r];
	k_asigQuick++;
	i=p-1;
	for(j=p;j<r;j++)
	{
		k_compQuick++;
		if(A[j]<=x)
		{
			i++;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
			k_asigQuick+=3;
		}
	}
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;
	k_asigQuick+=3;
	return i+1;
}

void QUICKSORT(int A[],int p,int r)
{
	int q;
	if(p<r)
	{
		q=Partitionare(A,p,r);
		QUICKSORT(A,p,q-1);
		QUICKSORT(A,q+1,r);
	}
}

int Partitionare2(int A[], int p, int r)
{
	int i,aux;
	i=(p+r)/2;
	aux=A[r];
	A[r]=A[i];
	A[i]=aux;
	return Partitionare(A,p,r);
}

void QUICKSORT2(int A[],int p,int r)
{
	int q;
	if(p<r)
	{
		q=Partitionare2(A,p,r);
		QUICKSORT2(A,p,q-1);
		QUICKSORT2(A,q+1,r);
	}	
}

void main()
{
	srand(time(NULL));
	int A[MAX_SIZE];
	int dim,i,k;
	FILE *f,*g,*h;
	/*printf("dim=");
	scanf("%d", &dim);
	while(dim>MAX_SIZE)
	{
		printf("Dimensuinea introdusa este prea mare!\ndim=");
		scanf("%d", &dim);
	}
	//heapsort
	generare(A,dim);
	afisare(A,dim);
	HEAPSORT(A,dim);
	afisare(A,dim);
	//quicksort
	generare(A,dim);
	afisare(A,dim);
	QUICKSORT(A,1,dim);
	afisare(A,dim);*/

	f=fopen("Heap&Quick.csv","w");
	if(f==NULL)
	{
		perror("EROARE DESCIDERE!");
		exit(1);
	}
	else
	{
		for(dim=100;dim<=10000;dim+=100)
		{
			printf("\ndim= %d", dim); //afisez dimensiunea ca sa vad unde a ajuns rularea
			k_asigHeap=0;
			k_compHeap=0;
			k_asigQuick=0;
			k_compQuick=0;
			for(i=1;i<=5;i++)
			{
				generare(A,dim);
				HEAPSORT(A,dim);
				generare(A,dim);
				QUICKSORT(A,1,dim);
			}
			fprintf(f,"%d,%d,%d,%d,%d,%d,%d \n",dim,k_asigHeap/5,k_compHeap/5,(k_asigHeap+k_compHeap)/5,k_asigQuick/5,k_compQuick/5,(k_asigQuick+k_compQuick)/5);
		}
	}
	fclose(f);
	printf("\nTerminare caz mediu statistic\n");

	g=fopen("Heap&Quick_fav.csv","w");
	if(g==NULL)
	{
		perror("EROARE DESCIDERE!");
		exit(1);
	}
	else
	{
		for(dim=100;dim<=10000;dim+=100)
		{
			printf("\ndim= %d", dim); //afisez dimensiunea ca sa vad unde a ajuns rularea
			k_asigHeap=0;
			k_compHeap=0;
			k_asigQuick=0;
			k_compQuick=0;
			for(i=1;i<=5;i++)
			{
				generare_cresc(A,dim);
				HEAPSORT(A,dim);
				generare_cresc(A,dim);
				QUICKSORT2(A,1,dim);
			}
			fprintf(g,"%d,%d,%d,%d,%d,%d,%d \n",dim,k_asigHeap/5,k_compHeap/5,(k_asigHeap+k_compHeap)/5,k_asigQuick/5,k_compQuick/5,(k_asigQuick+k_compQuick)/5);
		}
	}
	fclose(g);
	printf("\nTerminare caz favorabil\n");

	h=fopen("Heap&Quick_defav.csv","w");
	if(h==NULL)
	{
		perror("EROARE DESCIDERE!");
		exit(1);
	}
	else
	{
		for(dim=100;dim<=10000;dim+=100)
		{
			printf("\ndim= %d", dim); //afisez dimensiunea ca sa vad unde a ajuns rularea
			k_asigHeap=0;
			k_compHeap=0;
			k_asigQuick=0;
			k_compQuick=0;
			for(i=1;i<=5;i++)
			{
				generare_descresc(A,dim);
				HEAPSORT(A,dim);
				generare_descresc(A,dim);
				QUICKSORT(A,1,dim);
			}
			fprintf(h,"%d,%d,%d,%d,%d,%d,%d \n",dim,k_asigHeap/5,k_compHeap/5,(k_asigHeap+k_compHeap)/5,k_asigQuick/5,k_compQuick/5,(k_asigQuick+k_compQuick)/5);
		}
	}
	fclose(h);
	printf("\nTerminare caz defavorabil\n");
	getch();
}