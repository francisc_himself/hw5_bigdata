#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "Profiler.h"
#define NUMAR_VECINI_MAX 30000
#define NUMAR_NODURI_MAX 10000

typedef struct nod{
                        int key;
                        char c;
                        int d;
                        int nr_vecini;
                        struct nod *p;
                        struct nod *n[NUMAR_VECINI_MAX];
                        }nod;

typedef struct nod_c{
                        int key;
                        struct nod_c *urm;
                        }nod_c;

nod_c *prim;
nod *v[NUMAR_NODURI_MAX];
int atr,comp;
int edge[40000][2];

void deq()
{
    nod_c *p;
    p = prim;
    prim = prim -> urm;
    free(p);
}

void enq(int key)
{
    nod_c *p, *q;
    if(prim==NULL)
    {
        prim = (nod_c*)malloc(sizeof(nod));
        prim->key = key;
        prim->urm = NULL;
    }
    else{
        q = (nod_c*)malloc(sizeof(nod_c));
        q->key = key;
        p = prim;
        while(p->urm!=NULL)
            p = p -> urm;
        p -> urm =q;
        q->urm = NULL;
    }
}

void DFS(int s, int nr_noduri,int a)
{
    int i, j;
    nod *u;
    for(i=1; i<=nr_noduri; i++)
    {
        if(v[i]->key != v[s]->key)
        {
            v[i]->c = 'a';
            v[i]->d = 10000;
            v[i]->p = NULL;
			atr++;
        }
    }
    v[s]->c = 'g';
    v[s]->d = 0;
    v[s]->p=NULL;
	atr = atr+3;
    enq(v[s]->key);
    while(prim!=NULL)
    {
        u = v[prim->key];
        for(j=1; j<=u->nr_vecini; j++)
        {
			comp++;
            if(u->n[j]->c == 'a')
            {
                u->n[j]->c = 'g';
                u->n[j]->d = u->d + 1;
                u->n[j]->p = u;
				atr++;
                enq(u->n[j]->key);
            }
        }

        u->c = 'n';
        if(a==1)
            printf("%d ", u->key);
        deq();
    }
}

int combinari(int n)
{
    int p[10];
    int i,j,k=0;

   for(i=1; i<=2; i++)
        p[i]=i;
    edge[k][1] = p[1];
	edge[k][2] = p[2];
	k++;
    i=2;
    while(i>0)
    {
        p[i]++;
        if(p[i] > n-2+i){
            i--;

        }
        else
        {
            for(j=i+1; j<=2; j++)
                p[j] = 1;
            edge[k][1] = p[1];
			edge[k][2] = p[2];
			k++;
            i=2;
        }
        
    }

	return k;
}

void creare_graf(int nr_noduri,int nr_muchii)
{
    int i,j;
    int a[NUMAR_NODURI_MAX], b[NUMAR_NODURI_MAX];
	int k = combinari(nr_noduri);
	FillRandomArray(a,nr_muchii+1,1,k,false,0);
	
     for(i=1; i<=nr_noduri; i++)
    {
        free(v[i]); 
    }

    for(i=1; i<=nr_noduri; i++)
    {
        v[i] = (nod* )malloc(sizeof(nod));
        v[i]->key = i;
        v[i]->nr_vecini = 0;
    }

     for(i=1; i<=nr_noduri; i++)
    {
        for(j=0; j<nr_muchii; j++)
        {
            if(edge[a[j]][1] == v[i]->key)
            {
                v[i]->nr_vecini++;
                v[i]->n[v[i]->nr_vecini] = v[edge[a[j]][2]];
            }
            else if(edge[a[j]][2] == v[i]->key)
            {
                v[i]->nr_vecini++;
                v[i]->n[v[i]->nr_vecini] = v[edge[a[j]][1]];
            }

        }
	}
}
int main()
{
	int i,j, a[NUMAR_NODURI_MAX], b[NUMAR_NODURI_MAX];

   for(i=1; i<=8; i++)
    {
        v[i] = (nod* )malloc(sizeof(nod));
        v[i]->key = i;
        v[i]->nr_vecini = 0;
    }
    a[1] = 1; b[1] = 2;
    a[2] = 1; b[2] = 5;
    a[3] = 2; b[3] = 6;
    a[4] = 3; b[4] = 4;
    a[5] = 3; b[5] = 6;
    a[6] = 3; b[6] = 7;
    a[7] = 4; b[7] = 8;
    a[8] = 6; b[8] = 7;
    a[9] = 7; b[9] = 8;
    
    for(i=1; i<=8; i++)
    {
        for(j=1; j<=9; j++)
        {
            if(a[j] == v[i]->key)
            {
                v[i]->nr_vecini++;
                v[i]->n[v[i]->nr_vecini] = v[b[j]];
            }
            else if(b[j] == v[i]->key)
            {
                v[i]->nr_vecini++;
                v[i]->n[v[i]->nr_vecini] = v[a[j]];
            }

        }
    }

    for(i=1; i<=8; i++)
    {
        printf("%d: ", v[i]->key);
        for(j=1; j<=v[i]->nr_vecini; j++)
            printf("%d ",v[i]->n[j]->key);
        printf("\n");
    }
    printf("\n");
    DFS(2,8,1);
	FILE *f;
	f = fopen("analiza.csv", "w");
	fprintf(f,"muchii,atr+comp\n");
	int e;
	for(e=1000; e<= 5000; e=e+100)
	{
		atr = 0; comp = 0;
		creare_graf(100,e);
		DFS(2,100,0);
		fprintf(f,"%d, %d\n", e, atr+comp);
	}
	fprintf(f,"\n");
	fprintf(f,"noduri, atr+comp\n");
	for(i=100; i<=200; i=i+10)
	{
		atr = 0; comp = 0;
		creare_graf(i,9000);
		DFS(2,i,0);
		fprintf(f, "%d, %d\n", i, atr+comp);
	}
	printf("succes");
	getch();

}
