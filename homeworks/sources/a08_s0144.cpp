#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>
#include "Profiler.h"

Profiler profiler ("tema8");
//srand(time(NULL)); 

/* In acest tip de implementare, FORMEAZA_MULTIME, UNESTE necesita un timp O(1). 
   Pentru a implementa multimile cu euristica reuniunii dupa rang este necesar sa pastram valorile rangurilor. Pentru fiecare nod x retinem valoarea 
  intreaga x->rang (nr de muchii al celui mai lung drum de la x la o frunza descedenta). Rangul initial este 0.Ideea e ca radacina arborelui cu mai putine noduri 
  sa indice spre radacina arborelui cu mai multe noduri
  Pentru euristica de comprimare a drumului, fiecare nod de pe drumul de cautare pointeaza direct spre radacina. Comprimarea drumului nu modifica nici un rang.
  Atunci cand folosim cele 2 euristici, timpul de executie in cazul cel mai defavorabil este O(m*alfa(m,n)), unde alfa(m,n) este inversa functiei lui Ackermann, cu
  o crestere foarte inceata.Aplicatile structurii de date multimii disj, care pot fi luate in considerare, satisac alfa(m,n)<=4; astfel putem considera timpul de 
  executie ca fiind liniar in toate situatile practice.Acest lucru se poate observa si din graficul nostru.
 */

int nr_apel=0;
int apel;

typedef struct Nod
{
	int info, rang;
	Nod *p;
	Nod *urm;

}nod;

int muchie[60000];
nod set[10000];

//int muchie2[60000];
//nod*prim[10000];
//nod*ultim[10000];
int size;

typedef struct Nod2{
	int vf,m;
} nod2;
 

void formeaza_multime(nod *x,int info)
{   
	profiler.countOperation("apeluri",apel,1);
    x->urm=0;
	x->info=info;
	x->p=x;
	x->rang=0;
}

/*void lista(int x, int y)
{
	int dimensiune=prim[x]->rang+prim[y]->rang;
	prim[y]->p=prim[x];
	ultim[x]->urm=ultim[y];
	ultim[x]=ultim[y];
	prim[y]=prim[x];
	prim[x]->rang=dimensiune;
}*/

/*void uneste_multime(int x,int y)
{  
	//apel++;
	if(prim[x]->p->rang <= prim[y]->p->rang)
		lista(x,y);
	else 
		lista(y,x);
		
}*/

void uneste(nod*x,nod*y)
{
	profiler.countOperation("apeluri",apel,1);
	if(x->rang>y->rang)
		y->p=x;
	else {
		   x->p=y;
		   if(x->rang==y->rang)
			   y->rang=y->rang+1;
	     }
}

//gaseste_multime cu comprimarea drumului

nod* gaseste_multime(nod*x)
{   
	profiler.countOperation("apeluri",apel,1);
	if(x->p!=x)
	{
		x->p=gaseste_multime(x->p);
	}
	return x->p;
}


void reuneste_multime(nod* x, nod* y)
{   
	profiler.countOperation("apeluri",apel,1);
	nr_apel=+3;
	uneste(gaseste_multime(x),gaseste_multime(y));
}

bool comp(nod*x,nod*y)
{
	if( (gaseste_multime(x))!= (gaseste_multime(y)) )
		{  //apel++;
			return true;
		}
	else return false;
}

void comp_conexe(nod2 *p)
{  
	int vf=p->vf;
	int m=p->m;
	int i,j,m1,m2;
	for(i=0;i<vf;i++)
	{   
		//apel++;
	    formeaza_multime(&set[i],i);
	}
	FillRandomArray(muchie,p->m,0,p->vf*p->vf-1,true);
	for(j=0;j<m;j++)
	{   
		m1=muchie[j]/vf; //m1=rand()%(vf-1)
		m2=muchie[j]%vf; //m2=rand()%(vf-1)
		
		printf("%d %d---%d\n",j,m1,m2);
		
	 if((comp(&set[m1],&set[m2])==false))
	  {
		 uneste(&set[m1],&set[m2]);
      }
    }
  }
int main()
{   
	int vf=10000;
	nod2 *p=(nod2*)malloc(sizeof(nod2));
	p->m=10;
	p->vf=6;
	//apel=p->m;
	comp_conexe(p);
	//apel=0;
	/*for(int m=10000;m<=60000;m=m+1000)
	{
		p->vf=10000;
		p->m=m;
		apel=m;
		comp_conexe(p);
	}
	profiler.showReport();*/
	
	


}
	     