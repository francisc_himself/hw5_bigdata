#include "Profiler.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

Profiler profiler("demo");
 
typedef struct NODE
{
        int key;  
        int dim;  
        struct NODE *parent;  
        struct NODE *next;  
}NODE;
 
 
// an edge of the graph
typedef struct Edge
{
        int p;
        int d;
}Edge;
 
 
NODE    *head[10000],*end[10000];
Edge edge[60000];  
 
 
int M,V=10000,E;
 
 
bool mat[10000][10000];

int Find_Set(int x)
{  
		//profiler.countOperation("Calls", E, 0);
        profiler.countOperation("Calls", E);
		return head[x]->parent->key;
}
 

void Make_Set(int x)
{  
        profiler.countOperation("Calls", E);
 
       
        head[x]=(NODE*)malloc(sizeof(NODE));
        head[x]->key=x;
        head[x]->dim=1;
        head[x]->parent=head[x];
        end[x]=head[x];
}
 

void Link(int x, int y)
{      
        NODE *p;
        if(head[x]->dim >= head[y]->dim)    
        {  
                end[x]->next=head[y];  
                head[x]->dim+=head[y]->dim;  
                end[x]=end[y];
 
               
                p=head[y];
                for(int i=0;i<head[y]->dim;i++)
                {
                        p->parent=head[x];
                        p=p->next;
                }
        }
        else   
        {      
                end[y]->next=head[x];  
                head[y]->dim+=head[x]->dim;  
                end[y]=end[x];  
 
               
                p=head[x];
                for(int i=0;i<head[x]->dim;i++)
                {
                  p->parent=head[y];
                  p=p->next;
                }
        }
}

void Union(Edge t)
{  
        profiler.countOperation("Calls", E);
        Link(Find_Set(t.p),Find_Set(t.d));
        printf("Union sets: %d with %d \n",t.p,t.d);                                    
}
 

void component(int t, Edge edge[])
{  
       
        for(int j=0;j<t;j++)  
        {      
                Make_Set(j);
               // printf("Make Set: %d \n",j);
        }
 
       
        for(int i=0;i<t;i++)   
        {                                                              
                if((Find_Set(edge[i].p))!=(Find_Set(edge[i].d)))  
                {
                        Union(edge[i]);  
                }
        }
}
 
 
void main()
{
        int i,j;
       
       /* E=10;
        memset(mat,0,sizeof(mat));
 
        // generate E edge
        i=0;                                   
        while(i<E)
        {      
                int nr1 =rand() % 12;
                int nr2 =rand() % 12;
                if( ( mat[nr1][nr2] == 0 ) && (nr1 != nr2) )
                {
                        mat[nr1][nr2]=1;  // setting the adjacency mat
                        mat[nr2][nr1]=1;
                        edge[i].p=nr1;   // inserting the edge
                        edge[i].d=nr2;
                        printf("Edges:%d %d  ",nr1,nr2);
                        i++;
                }
        }
 
       
        component(E,edge);  // determines the connected components of the graph
 
       
       
        getchar();
       
        for(int u=0;u<10;u++)
                free(head[u]);*/
   

  for(E=10000;E<=60000;E+=1000)
  {    

        memset(mat,0,sizeof(mat));

        i=0;                                   
        while(i<E)
        {      
                int nr1 =rand() % 10000;
                int nr2 =rand() % 10000;
                if( ( mat[nr1][nr2] == 0 ) && (nr1 != nr2) )  
                {
                        mat[nr1][nr2]=1;  
                        mat[nr2][nr1]=1;
                        edge[i].p=nr1;  
                        edge[i].d=nr2;
                        i++;
                }
        }
 
       
        component(E,edge); 
 
       profiler.createGroup("Calls", "Calls");
 
       
        //for(int u=0;u<10;u++)
          //      free(head[u]);
  }
 
  profiler.showReport();

 
}