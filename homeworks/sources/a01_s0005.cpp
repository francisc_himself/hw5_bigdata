#include<iostream>
#include<time.h>
#include<conio.h>
/****ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
	Analiza si compararea metodelor de sortare - BubbleSort, SelectionSort, InsertionSort

	Observatii:
	- Cazul mediu statistic:
		Se poate observa ca numarul de asignari pentru Selection Sort variaza liniar, in timp ce, pentru Bubble Sort si 
		Insertion Sort, comportarea este asemenea unei parabole( O(n^2) ). 
		In ceea ce priveste numarul de comparatii, Insertion Sort cel mai eficient dintre algoritmi.
	- Cazul favorabil:
		In cazul Bubble sort nu se efectueaza nicio asignare, iar pentru Selection sort, asignarile variaza liniar.
		Comparatiile variaza, de asemenea, liniar pentru Bubble Sort.
	- Cazul defavorabil:
		Pentru Insertion Sort si Bubble Sort, cazul defavorabil este un sir ordonat descrescator, in timp ce pentru Selection Sort 
		este un sir sortat crescator, dar shift-at la stanga/dreapta cu o pozitie.

	Daca ne referim la stabilitatea algoritmilor, atunci doar Insertion Sort si Bubble sort sunt algoritmi de sortare stabili(2 elemente egale 
	isi pastreaza ordinea, unul fata de celalalt, si inainte si dupa sortare).

*/

long int asignari, comparari;
//Functia imi genereaza un sir de numere aleatoare
void generare_sir(int n, int *v)
{
	int i;
	srand(time(NULL));
	*v=rand()%10000; 
	for(i=1;i<=n;i++)
		*(v+i)=rand()%10000;
}

//Functia imi sorteaza crescator pentru p=0 si descrescator pentru p=1; o folosesc la cazurile favorabil si defavorabil
void cresc_descresc(int n, int* v, int p)
{
	int i, j, t;
	srand(time(NULL));
	for(i=1;i<=n-1;i++)
		for(j=i+1;j<=n;j++)
			switch(p) {
			case 0:
				if(*(v+i)>*(v+j))
				{
					t=*(v+i);
					*(v+i)=*(v+j);
					*(v+j)=t;
				}
				break;
			case 1:
				if(*(v+i)<=*(v+j))
				{
					t=*(v+i);
					*(v+i)=*(v+j);
					*(v+j)=t;
				}
				break;
			default:
				break;
		}
}

//Bubble Sort
void bubble_sort(int n, int*v)
{
	int i, ok=0, t;
	asignari=0;
	comparari=0;
	do{
		ok=1;
		for(i=0;i<n;i++)
		{
			if(*(v+i)>*(v+i+1))
			{
				ok=0;
				t=*(v+i);
				*(v+i)=*(v+i+1);
				*(v+i+1)=t;
				asignari+=3;
			}
			comparari++;
		}
		n--;
	}
	while(ok==0);
}

//Selection Sort
void selection_sort(int n, int* v)
{
	int i, j, i_min, t;
	asignari=0;
	comparari=0;
	for(i=1;i<=n-1;i++)
	{
		i_min=i;
		for(j=i+1;j<=n;j++)
		{
			if(*(v+j)<*(v+i_min))
				i_min=j;
			comparari++;
		}
		t=*(v+i);
		*(v+i)=*(v+i_min);
		*(v+i_min)=t;
		asignari+=3;
	}
}

//Insertion sort
void insertion_sort(int n, int* v)
{
	int i, j, k, x;
	asignari=0;
	comparari=0;
	for(i=2;i<=n;i++)
	{
		x=*(v+i);
		asignari++;
		j=1;
		while(*(v+j)<*(v+i))
		{
			j++;
			comparari++;
		}
		comparari++;
		for(k=i-1;k>=j;k--)
		{
			*(v+k+1)=*(v+k);
			asignari++;
		}
		*(v+j)=x;
		asignari++;
	}
}
//Functia verifica sirul daca este sortat crescator sau nu
int verifica_sir(int n, int* v)
{
	for(int i=1;i<=n-1;i++)
			if(*(v+i)>*(v+i+1))
				return 0;
	return 1;
}


int main ()
{
	int *sir;
	int i, n;
	//amed - media asignarilor;   cmed - media comparatiilor
	long bubble_amed=0, bubble_cmed=0, selection_amed=0, selection_cmed=0, insertion_amed=0, insertion_cmed=0;
	long int suma_a=0, suma_c=0;
	FILE* mediu;
	FILE* favorabil;
	FILE* defavorabil;

	//srand(time(NULL));
	sir=(int*)malloc((10001)*sizeof(int));
	mediu=fopen("mediu.csv", "w");
	fprintf(mediu, "n, bA, bC, bA+C, sA, sC, sA+C, iA, iC, iA+C\n");

	favorabil=fopen("favorabil.csv", "w");
	fprintf(favorabil, "n, bA, bC, sA, sC, iA, iC\n");

	defavorabil=fopen("defavorabil.csv", "w");
	fprintf(defavorabil, "n, bA, bC, sA, sC, iA, iC\n");


	for(n=100;n<=10000;n+=100)
	{
		bubble_amed=0;
		bubble_cmed=0;
		selection_amed=0;
		selection_cmed=0;
		insertion_amed=0;
		insertion_amed=0;
		asignari=0;
		comparari=0;


		//Cazul mediu statistic
		for(i=0;i<5;i++)
		{
			generare_sir(n, sir);
			bubble_sort(n, sir);
			suma_a+=asignari;
			suma_c+=comparari;
		}
		bubble_amed=suma_a/5;
		bubble_cmed=suma_c/5;
		suma_a=0;
		suma_c=0;

		for(i=0;i<5;i++)
		{
			generare_sir(n, sir);
			selection_sort(n, sir);
			suma_a+=asignari;
			suma_c+=comparari;
		}
		selection_amed=suma_a/5;
		selection_cmed=suma_c/5;
		suma_a=0;
		suma_c=0;


		for(i=0;i<5;i++)
		{
			generare_sir(n, sir);
			insertion_sort(n, sir);
			suma_a+=asignari;
			suma_c+=comparari;
		}
		insertion_amed=suma_a/5;
		insertion_cmed=suma_c/5;
		suma_a=0;
		suma_c=0;
		fprintf(mediu, "%ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld\n", n, bubble_amed, bubble_cmed, bubble_amed+bubble_cmed, selection_amed, selection_cmed, selection_amed+selection_cmed, insertion_amed, insertion_cmed, insertion_amed+insertion_cmed);

		//Cazul favorabil
		bubble_amed=0;
		bubble_cmed=0;
		selection_amed=0;
		selection_cmed=0;
		insertion_amed=0;
		insertion_amed=0;
		asignari=0;
		comparari=0;

		generare_sir(n, sir);
		bubble_sort(n, sir);		//sortez sirul pentru cazul favorabil
		bubble_sort(n, sir);
		bubble_amed=asignari;
		bubble_cmed=comparari;
		
		selection_sort(n, sir);
		selection_amed=asignari;
		selection_cmed=comparari;

		insertion_sort(n, sir);
		insertion_amed=asignari;
		insertion_cmed=comparari;
		fprintf(favorabil, "%ld, %ld, %ld, %ld, %ld, %ld, %ld\n", n, bubble_amed, bubble_cmed, selection_amed, selection_cmed, insertion_amed, insertion_cmed);

		//Cazul defavorabil
		asignari=0;
		comparari=0;
		bubble_amed=0;
		bubble_cmed=0;
		selection_amed=0;
		selection_cmed=0;
		insertion_amed=0;
		insertion_amed=0;

		
		generare_sir(n, sir);
		cresc_descresc(n, sir, 1);
		bubble_sort(n, sir);
		bubble_amed=asignari;
		bubble_cmed=comparari;
		
		
		bubble_sort(n, sir);
		*sir=*(sir+n)+1;
		//Generez cazul defavorabil pentru selection sort. Sirul generat este de forma: n, 1, 2, ..., n-1
		selection_sort(n, sir);
		selection_amed=asignari;
		selection_cmed=comparari;

		cresc_descresc(n, sir, 1);
		insertion_sort(n, sir);
		insertion_amed=asignari;
		insertion_cmed=comparari;
		fprintf(defavorabil, "%ld, %ld, %ld, %ld, %ld, %ld, %ld\n", n, bubble_amed, bubble_cmed, selection_amed, selection_cmed, insertion_amed, insertion_cmed);

	}
	fclose(mediu);
	fclose(favorabil);
	fclose(defavorabil);
	return 0;
}
