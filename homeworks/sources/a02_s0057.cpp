/****ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***.
In ceea ce priveste diferenta dintre heap-ul cu bottom_up si heap-ul cu top_down,pot spune ca cel cu bottom_up e mai eficient.
Eficienta o putem observa din numarul de assignari si comparati,numar care e mai mare in cazul top_down-ului,o putem observa din 
grafic sau din codul programului.*/
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
#define Max -10000;
Profiler profiler("demo");
int lungime;
int size;
int parent(int i)
{
	return i/2;
}
int stg(int i)
{
	if(i!=0)
	return 2*i;
}
int dr(int i)
{
	if(i!=0)
	return 2*i+1;
}
void reconstituie(int* a,int i,int n)
{
	int m=n;
  int min;
  int s=stg(i);
  int d=dr(i);
  if((s<=m)&&(a[i]>a[s]))
	  min=s;
  else min=i;
  if((d<=m)&&(a[min]>a[d]))
	  min=d;
  if(i!=0)
  if (min!=i)
  {
     int aux=a[i];
	  a[i]=a[min];
	  a[min]=aux;
      reconstituie(a,min,m-1);
	  profiler.countOperation("assign_bott",lungime,3);
  }
  profiler.countOperation("comp_bott",lungime,2);
 
}


void bottom(int* a,int n)
{
	int m=n;
   for(int i=(n+1)/2;i>=1;i--)
   {
	  
      reconstituie(a,i,m);
	   
   }
   
}
void bottom_up(int* a,int n)
{
	bottom(a,n);
   int m=n;
   int aux=0;
   for(int i=m;i>=2;i--)
   {
      aux=a[1];
	  a[1]=a[i];
	  a[i]=aux;
	  m--;
	  profiler.countOperation("assign_bott",lungime,3);
	  reconstituie(a,1,m);
   }
}
void insert(int* a,int i,int key)
{
	if(key<a[i])
		perror("prea mic");
	profiler.countOperation("comp_top",lungime,1);
	a[i]=key;
	while((i>1)&&(a[parent(i)]>a[i]))
		{
			int aux=a[i];
			a[i]=a[parent(i)];
			a[parent(i)]=aux;
			i=parent(i);
			profiler.countOperation("assign_top",lungime,3);
			profiler.countOperation("comp_top",lungime,1);
		}
	profiler.countOperation("assign_top",lungime,1);
}
void top(int*a,int key)
{
	
    size++;
	a[size]=Max;
	insert(a,size,key);
	profiler.countOperation("assign_top",lungime,1);
}

void top_down(int*a,int n)
{
	size=1;
	//int p=n;
	for(int i=2;i<=n;i++)
	{
		top(a,a[i]);
	}

}
void top_down_re(int* a,int n)
{
	top_down(a,n);
   int m=n;
   int aux=0;
   for(int i=m;i>=2;i--)
   {
      aux=a[1];
	  a[1]=a[i];
	  a[i]=aux;
	  m--;
	  profiler.countOperation("assign_top",lungime,3);
	  reconstituie(a,1,m);
   }
}

void afisare(int*v,int n,int k,int nivel)

{
 if(k>n)
	 return;
 afisare(v,n,2*k+1,nivel+2);
 for(int i=1;i<=nivel;i++)
	 printf(" ");
 printf("%d\n",v[k]);
 afisare(v,n,2*k,nivel+2);
}
void main()
{
	int n;
	int v[10000],v1[10000];
    for(int t=5;t>=1;t--)
	{
	for(n=100;n<2000;n+=100)
	{
	  lungime=n;
	  FillRandomArray(v,n,10,50000,false,1);
	  printf("n=%d\n",n);
	  memcpy(v1,v,n*sizeof(int));	
	  top_down_re(v1,n);
	  }
	}
	size=7;
	profiler.createGroup("assign+comp_bott","assign_bott","comp_bott");
	profiler.createGroup("assign+comp_top","assign_top","comp_top");
	profiler.addSeries("bottom","assign_bott","comp_bott");
	profiler.addSeries("top","assign_top","comp_top");
	profiler.createGroup("serie","bottom","top");

	profiler.showReport();

}