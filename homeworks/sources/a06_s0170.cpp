﻿/* Nume : ***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa : ***GROUP_NUMBER*** CTI 
Permutarea Josephus se defineste dupa cum urmeaza. 
Presupunem ca n persoane sunt aranjate in cerc si ca exista un intreg pozitiv m ≤ n. 
Incepand cu o persoana stabilita se parcurge cercul si fiecare a m/a persoana se scoate din configuratie.
Dupa ce persoana a fost eliminata, numaratoarea continua cu configuratia ramasa. 
Procesul de eliminare continua pina cand toate cele n persoane au fost scoase din configuratie. 
Ordinea in care se elimina persoanele din configuratie va defini permutarea Josephus(n,m) a intregilor 1, 2, …, n.
Eficienta : O(k\log n). sursa : http://en.wikipedia.org/wiki/Josephus_problem

*/
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
typedef struct nod{
	int info;
	int dimensiune;
	struct nod *p;	//parinte
	struct nod *st, *dr;
}NOD;

int result[10001];
int nratribuiri, nrcomparatii;


NOD * Tree_min (NOD *x)
{
	while (x->st != NULL)
	{
		nratribuiri++;
		x = x->st;
	}
	return x;
}

NOD * Tree_succ (NOD *x)
{
	NOD *y;

	nrcomparatii++;
	if (x->dr != NULL)
		return Tree_min (x->dr);

	nratribuiri++;
	y = x->p;
	
	while (y != NULL && x==y->dr)
	{
		nratribuiri+=2;
		x=y;
		y=x->p;
	}
	return y;
}

void scade_dimensiune(nod *z)
{ while (z!=NULL)
	{
      nratribuiri=nratribuiri+2;
	  z->dimensiune--;
	  z=z->p;
	}
}

void Sterge_Arbore (nod **root, nod *z)
{
	NOD *y,*x;

	nrcomparatii = nrcomparatii+5;
	if (z->st == NULL || z->dr==NULL)
	{
		nratribuiri++;
		y = z;
	}
	else
	{
		nratribuiri++;
		y = Tree_succ (z);
	}
	if (y->st != NULL)
	{
		nratribuiri++;
		x = y->st;
	}
	else
	{
		nratribuiri++;
		x = y->dr;
	}

	if (x != NULL)
	{
		nratribuiri++;
		x->p = y->p;
	}

	scade_dimensiune(y->p);

	if (y->p == NULL)  //rad
		*root = x;
	else
	{
		if (y == (y->p)->st)
		{
			nratribuiri++;
			(y->p)->st = x;
		}
		else
		{
			nratribuiri++;
			(y->p)->dr = x;
		}
	}

	if (y != z)
	{
		nratribuiri++;
		z->info = y->info;
	}

	free(y);
}

nod* Construim_arbore_echilibrat (int st, int dr, int *dimensiune)
{
	nod *p;

    int n=sizeof(nod);
    int dimensiune1=0,dimensiune2=0;
    nrcomparatii++;

    if (st>dr)
    {
        *dimensiune=0;
   	    return NULL;
    }

    else
    {
        nratribuiri=nratribuiri+5;

		p=(nod*) malloc(n);
	    p->info=(st+dr)/2;
		p->st = NULL;
		p->dr = NULL;
		p->dimensiune=dr-st+1;

		if(st<dr)
		{
			p->st=Construim_arbore_echilibrat (st,(st+dr)/2-1,&dimensiune1);

			if(p->st != NULL)
			{
			    p->st->p= p;
			    nratribuiri++;
            }

            p->dr=Construim_arbore_echilibrat ((st+dr)/2+1,dr,&dimensiune2);

            if(p->dr != NULL)
            {
                p->dr->p = p;
                nratribuiri++;
            }

        }
    }

    nratribuiri++;
    *dimensiune=p->dimensiune;
    return p;
}

NOD * SO_Selectie (NOD *x, int i)
{
	int r;

	nrcomparatii = nrcomparatii+3;
	nratribuiri++;
	if (x->st!=NULL)
		r = ((x->st)->dimensiune)+1;
	else
		r = 1;

	if (r == i)
		return x;
	else
	{
		if (i < r)
			return SO_Selectie (x->st,i);
		else
			return SO_Selectie (x->dr,i-r);
	}
}

void TIPARESTE (NOD *x, int d)
{

	if (x != NULL)
	{
		TIPARESTE (x->st,d+1);
		for (int i=1; i<=d; i++)
			printf (" ");
		printf ("%d",x->info);
		TIPARESTE (x->dr,d+1);
	}
}

void Josephus (int n, int m)
{
	nod *rad,*x;
    int h,k,element;
    int index=0;
    element=n;
    rad = Construim_arbore_echilibrat(1,element,&h);
    rad->p = NULL;

    k=m;
    while (element>0)
    {
        x=SO_Selectie(rad,k);
        result[index]=x->info;
        index++;
        Sterge_Arbore(&rad,x);
        element--;
        if (element)
        {
            k=(k+m-1)%element;
            if (k==0)
            k=element;
        }
	}
}

void testare ()
{
	Josephus(7,3);
	for(int i=0;i<7;i++) 
		printf("%d ",result[i]);
}

void main()
{
	//Variam n de la 100 la 10000 cu pasul 100; pentru fiecare n alegem m  avand valoarea n/2.
	int n, m;
	FILE *pf;
	testare ();
	pf = fopen ("Josephus_fisier.txt","w");
	
	for (n=100; n<=10000; n=n+100)
	{
		m = n/2;
	
		nratribuiri = nrcomparatii = 0;

		Josephus(n,m);
		
		fprintf (pf,"%d %d %d %d\n",n,nratribuiri, nrcomparatii, nratribuiri+nrcomparatii);
	}

	fclose(pf);
	getche ();

}


