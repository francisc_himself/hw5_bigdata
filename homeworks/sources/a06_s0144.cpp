﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

int suma=0;

/* Permutarea Josephus(n,m) are o eficienta de O(n log n), pt m  ne-constant. Pt  m constant -> O(n) [liste circulare]
	Pentru iteratiile de la 100 la 1000, efortul depus (suma nr de comparatii si asignari) este cuprinsa intre 1746 si 305422  */


typedef struct ARBORE
{ 
 int key; 
 int size;
 struct ARBORE *stg;
 struct ARBORE *drp; 
 struct ARBORE *parinte;
} Arbore;

Arbore* rad;

Arbore* constructie( int stg, int drp, Arbore *parinte)
{ 
	if(stg<=drp)
	{  int m=(stg+drp)/2;
		Arbore *p = (Arbore*)malloc(sizeof(Arbore));
		suma++;
		p->key = m;
		p->size = drp - stg + 1;
		p->stg = constructie(stg, m-1, p);
		p->parinte = parinte;
		p->drp = constructie(m+1, drp, p);
		return p;
		free(p);  // dealocam 
	}
	else return NULL;
	
}

Arbore* OS_SELECT(Arbore*p, int n)
{
	int r;
	suma++;
	if(p->stg!=NULL)
	{
		r=p->stg->size+1;
	}
	else
		{
			r=1;
	}
	if(n==r)
	{
		return p;
	}
	
	else 
	{
		suma++;
		p->size = p->size - 1;
		if(n<r)
		{
		return OS_SELECT(p->stg,n);
		}
		else
			{
				return OS_SELECT(p->drp,n-r);
		}
	}
}

// functie necesare pt stergere: calculeaza minimul intr-un BST
Arbore* minim (Arbore* p)
{  
	while (p->stg != NULL)
	{
		suma=suma+3;
		p->size = p->size - 1;
		p = p->stg;
	}
	return p;
}

//functie necesara pt stergere: calculeaza succesorul intr-un BST
Arbore* succesor (Arbore* p)
{
	suma++;
	if (p->drp != NULL)                //se cauta succesorul in dreapta
		return minim(p);               //daca mai exista in dreapta fii se cauta minimul cel mai de jos

	Arbore *q = p->parinte;             
	while (q != NULL && p == q->drp)   //daca q nu este null si p este egal cu fiul dreapta al lui q    
	{   suma=suma+3;
		p = q;							// p devine q, si se cauta urmatorul in sus
		q = p->parinte;
	}
	return p;                         
}

Arbore* OS_STERGE(Arbore *p, Arbore*q)
{
	Arbore *y;
	Arbore *x;
	suma++;
	if((q->stg==NULL)||(q->drp==NULL))  //verifica daca nodul respectiv este frunza sau nu
	{
		y=q;
	}
	else
	{
		y=succesor(q);  //daca nu, se cauta succesorul lui
	}
	suma++;
	if(y->stg!=NULL)  //daca succesorul are fiu stanga
		{
			x=y->stg;
	}
	else 
		{
			x=y->drp;   //succesorul are fiu dreapta
	}
	suma++;
	if(x!=NULL)
	{
		x->parinte=y->parinte;
	}

	suma++;
	if(y->parinte==NULL)
	{
		rad=x;
	}
	else {
		suma++;
		if(y==y->parinte->stg)
		{
			y->parinte->stg=x;
		}
		else
			{
				y->parinte->drp=x;
		}
	}

	suma++;
	if(y!=q)
	{
		q->key=y->key;
	}
	return y;
}

//JOSEPHUS(n,m) 
//T = BUILD_TREE(n) 
//j ←1 
//for k ←n downto 1 do 
//j ←(( j + m − 2) mod k) + 1 
//x ←OS-SELECT(root[T], j ) 
//print key[x] 
//OS-DELETE(T, x)

void print (Arbore* p);

void josephus(int n, int m)
{
	Arbore*p;
	//rad=constructie();
	int i,j,a;
	j=1;
    a=n;
	for(i=n;i>=1;i--)
	{
		j=j+m-1;
		if(j>a)
		{ 
			j=(j-1)%a+1;
		}
		p=OS_SELECT(rad,j);
		printf("\n Nodul selectat pentru stergere este %d, iar noul arbore devine: \n\n", p->key);
		print(rad);
		OS_STERGE(rad,p);
		print(rad);
		a=a-1;


	}
}



//pt  pretty print
int inaltime(Arbore *p)
{ int hstg, hdrp,h;
	if(p!=NULL)
	{
		hstg=inaltime(p->stg);
		hdrp=inaltime(p->drp);
		if(hstg>hdrp)
			h=hstg+1;
		else
			h=hdrp+1;
		return h;
	}
	else return 0;
	}

	
void print (Arbore* p)
{ int h;
	if (p != NULL)
	{   
		print(p->stg);
		h=inaltime(p);
		for(int i=1;i<=h;i++)
			printf("\t ");
		print(p->stg);
		printf("%d -> %d\n", p->key,p->size);
		 print(p->drp);
	}
}
	
int main()
{
	FILE *f;
	int i;
     f=fopen("permutare.txt","w");
	/*for(i=100;i<=10000;i=i+100)
	 {  
		 suma=0;
		 rad=constructie(1,i,NULL);
		 josephus(i,i/2);
		 fprintf(f,"%d %d \n",i,suma);

	 }*/
	rad=constructie(1,7,NULL);
	printf("Arborele initial este:\n\n");
	print(rad);
	josephus(7,3);
   // print(rad,0);

	 fclose(f);
	 
	 return 0;
	 
	}
