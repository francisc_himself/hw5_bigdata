/****ANONIM*** ***ANONIM*** Mihai, grupa ***GROUP_NUMBER***, Laborator interclasare liste ordonate
In aceasta lucrare de laborator am testat algoritmul de interclasare a k liste ordonate. Pentru aceasta, am pus primul
element din fiecare lista intr-un heap, astfel cheia minima dintre toate listele a devenit varful acestui heap. Acest lucru
se repeta, extragand tot cate un element din heap si adaugand capul listei din care a fost extras minimul, pana cand fiecare
lista devine vida. Fiecare element extras se pune intr-o lista initial vida. Astfel, la final, lista va contine toate
elementele din celelalte liste, ordonate crescator. Complexitatea acestui algoritm este O(N*log(K)), unde n este numarul
total de elemente, iar k este numarul de liste.

Pt testul 1 am folosit, pe rand, 5, 10 respectiv 100 de liste cu un numar total de 100,200,...10000 elemente si am 
calculat numarul de operatii in functie de k. Graficul rezultat arata ca pentru mai putine liste avem mai putine operatii.

Pt testul al doilea am folosit 10, 20,...500 liste cu un numar total de 10000 elemente.
*/

#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
//definitii structuri de date si variabile globale	
int atrib,comp;
//element de heap, care memoreaza cheia si lista din care face parte
typedef struct element_heap
{
	int cheie,lista;									
}HEAP;	
//tip nod lista dublu inlantuita
typedef struct tip_nod
{
	int nr;
	struct tip_nod *prec,*urm;
}TIP_NOD;

HEAP a[1000000]; 
int n=0;         
int k;
TIP_NOD *cap[10000];
TIP_NOD *prim=NULL,*ultim=NULL; 
//functii pentru parinte, fiu stanga si fiu dreapta heap
int parinte(int i)
{
	return i/2;				
}
int stanga(int i)            
{
	return 2*i;				
}
int dreapta(int i)          
{
	return 2*i+1;			
}
//inserare nod in heap
void hPush(int nr,int nrLista)	                   
{										
	n=n+1;
	a[n].cheie=nr;
						   
	a[n].lista=nrLista;
	atrib=atrib+2;
	int i=n;
	comp=comp+1;
	while ((i>1) && ( (a[parinte(i)].cheie) < (a[i].cheie) ) )
	{
		HEAP temp=a[i];
		a[i]=a[parinte(i)];       
		a[parinte(i)]=temp;
		i=parinte(i);
		atrib=atrib+3;
	}
}
//reconstituie heap
void heapify(HEAP a[],int i, int n) 
{
	int l,r;
	int largest=0;
	l=stanga(i);	        
	r=dreapta(i);		          
	comp=comp+1;
	if ((l<=n) && (a[l].cheie>a[i].cheie))
		largest = l;
	else
		largest = i;
	        
	comp=comp+1;
	if ((r<=n) && (a[r].cheie>a[largest].cheie))
	{
		largest = r;
		       
	}
	comp=comp+1;
	if (largest != i)
	{
		HEAP aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		atrib=atrib+3;        
      	heapify(a,largest,n);
    }
}
//extrage minim din heap
HEAP h***ANONIM***() 
{
	HEAP aux;               
	aux.cheie=a[1].cheie;     
	aux.lista=a[1].lista;
	a[1]=a[n];                      
	n=n-1;                          
	atrib=atrib+3;
	heapify(a,1,n);           
return aux;
}
//creare lista vida
void creareLista(TIP_NOD **p)   
{
	 *p=NULL;
     
}
//creare nod de cheie x
TIP_NOD *adauga(int x)   
{
	TIP_NOD *p;
	p=(TIP_NOD*)malloc(sizeof(TIP_NOD));    
	p->nr=x;                                
	
	return p;
}
//insereaza nod inainte de primul 
void insereaza(TIP_NOD **prim,int numar)  
													 
{							
   TIP_NOD *p;
	int s=sizeof(TIP_NOD);
	p=(TIP_NOD*)malloc(s);
	p->nr=numar;
	p->prec=NULL;
   if(*prim==NULL)      
   {
	   *prim=p;			
       p->urm=NULL;
	   
   }
   else						
   {
	   p->urm=*prim;		
	   (*prim)->prec=p;		
	   *prim=p;
	   
   }
}
//inserare nod dupa ultimul
void inserareUltimNod(TIP_NOD **ultim,TIP_NOD *p)  
{
	p->urm=NULL;      
	p->prec=*ultim;   
	(*ultim)->urm=p;
	*ultim=p;
	atrib+=4;
	
}
//sterge primul nod din lista
int extrage_cap_lista(TIP_NOD **prim)  
{
 TIP_NOD *p;
  p=*prim;
  int x=p->nr;
  *prim=(*prim)->urm;			
  free(p);                     
  atrib+=2;
  comp+=1;
  if(*prim!=NULL)
  {
	(*prim)->prec=NULL;
	atrib+=1;
  }
  return x;
}
//sterge toata lista
void stergeLista(TIP_NOD **prim)  
{				                    
TIP_NOD *p;
 while( *prim!=NULL )           
   {
	   p=*prim;
	   *prim=(*prim)->urm;
	   free(p);
	   
   }
}
//creare si populare lista rezultata in urma interclasarii
void listaRezultat(int x)
{
	TIP_NOD *p;
	int s=sizeof(TIP_NOD);
	p=(TIP_NOD*)malloc(s);
	p->nr=x;
	p->urm=NULL;
	p->prec=NULL;
	atrib+=3;
	comp+=1;
	if(prim!=NULL)
	{													
		inserareUltimNod(&ultim,p);			
	}
	else
	{                        
		prim=p;
		ultim=p;
		atrib+=2;
	}
}

//generare k liste, cu un numare total de n elemente=> o lista are n/k elemente
void generare(int k, int n)  
{
	int nr=n/k;
	int i,j,val;
	for(i=1;i<=k;i++)
	{
		creareLista(&cap[i]);
		val=rand()%5+1;
		for(j=1;j<=nr;j++)
		{
			insereaza(&cap[i],val);
			val+=rand()%5+1;
		}
	}
	stergeLista(&prim);   
	creareLista(&prim);	
}	


//algoritmul de interclasare
void merge()   
{
	int val;
	HEAP aux;
	for(int i=1;i<=k;i++)      
	{
		val=extrage_cap_lista(&cap[i]);     
		hPush(val,i);           
		atrib+=2;
	}
	comp+=1;
	while (n>0)  
	{
		aux=h***ANONIM***();    
		listaRezultat(aux.cheie);
		atrib+=1;
		comp+=1;
		if ( (cap[aux.lista])!=0 )  
			{										
				val=extrage_cap_lista(&cap[aux.lista]);	
				hPush(val,aux.lista);
                atrib+=1;
			}
 	}
}



int main ()
{  
	
	int v[]={5,10,100};   
	int tot_atrib;
	int total_comp;
	
	FILE *pf;
	pf=fopen("Test1.csv","w");
	fprintf(pf,"  ");
	for(int i=1;i<=100;i++)fprintf(pf,",n=%d",i*100);
	
	//primul test
	printf("test 1:");
	for(int i=0;i<3;i++)
	{
		k=v[i];
		fprintf(pf,"\nk=%d,",k);
		printf("\nk=%d\n",k);
		for(int nr_elem=100;nr_elem<=10000; nr_elem=nr_elem+100)
		{
			
				printf("%d%c\r",nr_elem/100,'%');
				tot_atrib=0;
				total_comp=0;
				generare(k,nr_elem);
				atrib=0;
				comp=0;
				merge();
				tot_atrib=tot_atrib+atrib; 
				total_comp=total_comp+comp;
				fprintf(pf,"%d,",tot_atrib+total_comp);
		}
	}
	fclose(pf);	
	pf=fopen("Test2.csv","w");
	int nr_elem=10000; 
	printf("\n\nTestul 2:\n");
	fprintf(pf,"k,Operatii\n");
	//testul al doilea
	for(int i=10;i<500;i=i+10)
	{
		k=i;   
		tot_atrib=0;
		total_comp=0;
		generare(k,nr_elem);
		atrib=0;
		comp=0;
		merge();   
		tot_atrib=tot_atrib+atrib;   
		total_comp=total_comp+comp;
		printf("%d%c\r",i/5+2,'%');
		fprintf(pf,"%10d,%10d\n",k,tot_atrib+total_comp);
	}
	fclose(pf);
	printf("\nTerminat cu succes! Apasati o tasta");
	getch();
}
