/****ANONIM*** grupa ***GROUP_NUMBER***
	Dupa cum bine stim acest algoritm bfs este un algoritm de cautare intr-un graf cand cautarea este limitata operatii
	esentiale,adica vizitarea si inspectia unu nod din graf.Complexitatea acestui algoritm este de O(|V|+|E|) deoarece 
	fiecare varf si fiecare muchie sunt vizitate.Dificultati apar la nivelul BFS si al generarii,dar datorita unor indicii
	primite aceastea au fost rezolvate.
*/
#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"
Profiler profiler("demo");
#define white 0
#define gray 1
#define black 2
#define V_MAX 10000
#define E_MAX 60000
#define infinit 10000000
typedef struct GRAPH_NODE{
	struct GRAPH_NODE* parent;
	int distance;
	int color;
	int key;
}GRAPH_NODE;

typedef struct _NODE{
	int key;
	struct _NODE*next;
}NODE;
typedef struct graf{
	int nrNoduri;
	int nrMuchii;
}graf;
NODE *adj[V_MAX];
struct GRAPH_NODE graph[V_MAX];
struct GRAPH_NODE *P[V_MAX];
int edges[E_MAX];
int tail,length,head;
int lung;
void ENQUEUE(GRAPH_NODE* x){
	P[tail]=x;
	if(tail==V_MAX)
		tail=1;
	else tail++;
	length++;
}
GRAPH_NODE* DEQUEUE(){
	GRAPH_NODE* x=P[head];
	if(head==V_MAX)
		head=1;
	else head++;
	length--;
	return x;
}
void BFS(graf *G,GRAPH_NODE *s)
	{

	for(int i=0;i<G->nrNoduri;i++)
	{
		//profiler.countOperation("Numar",lung,1);
		lung++;
		if(&graph[i]!=s){
		graph[i].key=i;
		graph[i].color=white;
		graph[i].distance=infinit;
		graph[i].parent=NULL;
		}
	}
		s->color=gray;
		s->distance=0;
		s->parent=NULL;
	tail=0;
	head=0;
	length=0;
		//profiler.countOperation("Numar",lung,2);
	lung+=2;
	ENQUEUE(s);
	while(length!=0){
		GRAPH_NODE* u=DEQUEUE();
		NODE* v=adj[u->key];
			//profiler.countOperation("Numar",lung,1);
		lung++;
		while(v!=NULL){
			if(graph[v->key].color==white)
			{
		
				graph[v->key].color=gray;
				graph[v->key].distance=u->distance+1;
				graph[v->key].parent=u;
				ENQUEUE(&graph[v->key]);
			}
			
			v=v->next;
		}
		u->color=black;
	}
	//printf("..%d",lung);
}
void generate(int n,int m)
{
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<n;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE*nod=(NODE*)malloc(sizeof(NODE));
		nod->key=b;
		nod->next=adj[a];//pointeaz spre primul element..b
		adj[a]=nod;
	}
}
void print(graf *g,GRAPH_NODE*s,struct GRAPH_NODE*v)
{
	if(v->key==s->key)
		printf("%d,%d",s->key,s->color);
	else if(v->parent==NULL)
			{
				printf("nu este cale de la s la v\n");
				return;
	}
	else{ 
		printf("catre %d,%d\n",v->key,v->color);
		print(g,s,v->parent);
		  
	}
}
void afisare(int x)
{
	for(int i=0;i<x;i++)
	{
		printf("%d",i);
		NODE*a=adj[i];
		while(a!=NULL)
		{
			printf("catre %d/",a->key);
			if(a!=NULL)
				a=a->next;
		}
		printf("\n");
	}
}
void main(){
//	generate(5,9);
	/*for(int i=0;i<10;i++)
	printf("%d",adj[i]->key);*/
	graf *g=(graf*)malloc(sizeof(graf));
	//g->nrMuchii=9;
	//g->nrNoduri=5;

	//BFS(g,&graph[0]);
	//afisare(g->nrNoduri);
	//for(int i=0;i<g->nrNoduri;i++)
	//	for(int j=0;j<g->nrMuchii;j++)
	//		print(g,&graph[j],&graph[i]);
	//print(
	FILE*fd;
	fd=fopen("afis.csv","w");
	fprintf(fd,"numar ; lung\n");
	//for(int i=1000;i<5000;i+=100)
	//{
	//	printf("i=%d\n",i);
	//	lung=i;
	//	g->nrNoduri=100;
	//	g->nrMuchii=i;
	//	generate(g->nrNoduri,g->nrMuchii);
	//	for(int j=0;j<g->nrNoduri;j++)
	//		BFS(g,&graph[j]);
	//	//printf("....%d",lung);
	//	fprintf(fd,"%d ; %d\n",i,lung);
	//}
	for(int i=100;i<200;i+=10)
	{
		printf("i=%d\n",i);
		lung=i;
		g->nrNoduri=i;
		g->nrMuchii=9000;
		generate(g->nrNoduri,g->nrMuchii);
		for(int j=0;j<g->nrNoduri;j++)
			BFS(g,&graph[j]);
		//printf("....%d",lung);
		fprintf(fd,"%d ; %d\n",i,lung);
	}
	//profiler.showReport();
}