/*
Bottom-Up:  given a sequence of data values
	- create a complete binary from the values without any ordering
      place the first value at root, the next two values as root's children,..
      fill values from top to bottom and left to right.
	- for all parents (from the lowest-right-most parent to root)
          heapify the subtree with the parent at root (down-heap)

Top-Down:  start with an empty tree
	- insert one by one data values to the tree at the first available position
	- heapify from the inserted value all the way to root if necessary
      (up-heap)

	==>  **We can observe that the bottom-up approach is a bit more efficient
		 than the top-down approach by relying on possible reverse values (parent-child)
		 that occur in the initial configuration of the data.
		**Another advantage of the bottom-up algorithm when using an array implementation
		is that the initial array is the initial heap that has to be reordered.
	  
*/




#include <stdio.h>
#include  <conio.h>
#include <stdlib.h>

int heapSize=0;
int ass=0;
int comp=0;


int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}

int parent(int i)
{
	return i/2;
}

	void swap(int &a, int &b)
	{
		int aux=b;
			b=a;
			a=aux;

	}

	
	// BOTTOM UP
	void heapify(int *a, int i) // if child smaller than parent==> swap them
	{
		int largest=i;
		int aux;
	int l=left(i);
	int r=right(i);
	comp++;
	if((l<=heapSize)&&(a[l]>a[i])) largest=l;
						  else largest=i;
	comp++;
		if((r<=heapSize)&&(a[r]>a[largest])) largest=r;
			comp++;
		if(largest!=i){
						ass+=3;
						swap(a[largest],a[i]);
						heapify(a,largest);
						}
}


	void buildHeap_btmup(int *a,int n)
	{
		for(int i=n/2; i>0 ;i--)
			heapify(a,i);

	}

	

	
	
	//TOP-DOWN
void increase_key(int *a,int i, int key){
	if(key<a[i]) printf("Error, new key smaller than current key");
	else{
		//assign the new value
		ass++;
	a[i]=key;
	comp++;
	while((i>1)&&(a[parent(i)]<a[i])){
		comp++;
		ass=ass+3;
		swap(a[parent(i)],a[i]);
		i=parent(i);
	}
	}
}

void heap_insert(int *a, int key){
	heapSize++;
	increase_key(a,heapSize,key);
}

void build_heap_top_down(int *a, int n){
	heapSize=1;
	for(int i=2;i<n;i++){
	heap_insert(a,a[i]);
	}

}

//OTHER FUNCTIONS

void create_average(int *a, int n){
int i;
for(i=1;i<n+1;i++){
a[i]=rand()%n;
}
}

void copy_array(int a[], int b[], int n){
	for(int i=1;i<=n;i++)
		b[i]=a[i];
}

void print_array(int a[],int n){
int i;
for(i=1;i<n+1;i++)
	printf("%d ",a[i]);
printf("\n");
printf("\n");
printf("\n");

}

int main(){
	int *x;
	int *y;
	FILE *f=fopen("topdwn_vs_btmup.csv","w");
	for(int n=100;n<=10000;n=n+100)
	{	
	
		fprintf(f,"%d, ",n);
		x=new int[n+1];
		y=new int[n+1];
		ass=0;
		comp=0;
		create_average(x,n);
		copy_array(x,y,n);  // y<-- x to have a fair comparison between them

		buildHeap_btmup(x,n);
		fprintf(f,"%d, ",comp+ass);
		ass=0;
		comp=0;

		copy_array(y,x,n);
		build_heap_top_down(x,n);
		fprintf(f,"%d, ",comp+ass);
		fprintf(f,"\n");

	}
	
	//DEMO btm-up
	int array1[10]={0,8,1,6,4,5,2};
	int array2[10];
	printf("\n");
	printf("array1:");
	print_array(array1,7);
	copy_array(array1,array2, 7);
	printf("array1 after Bottom-up: ");
	buildHeap_btmup(array1,7);
	print_array(array1,7);

	//DEMO top-down
	copy_array(array2,array1, 7);
	printf("array1:");
	print_array(array1,7);
	printf("array1 after Top-Down: ");
	build_heap_top_down(array1,7);
	print_array(array1, 7);



	fclose(f);
		getch();
		



	




}



