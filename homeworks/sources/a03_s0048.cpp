/*
***ANONIM*** Diana ***ANONIM*** grupa***GROUP_NUMBER***
Pentru cazul mediu quicksort este mai eficient decat heapsort dupa cum se vede si din grafic avand o eficienta de O(n log n),quicksort fiind considerat cel mai rapid algoritm de sortare
Cazul cel mai defavorabil are o eficienta de aproximativ O(n^2)
Cazul cel mai favorabil are o eficienta de aproximativ O(nlog n) iar la acest caz vectorul se imparte mereu in jumatate,se face o noua functie care alege pivotul de la mijloc
*/


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"
Profiler profiler ("lab3");

#define NEG_INF INT_MIN

int v[]={4,1,3,2,16,9,10,14,8,7};
int h_size,n;
int v1[]={4,1,3,2,16,9,10,14,8,7};
int v2[]={4,1,3,2,16,9,10,14,8,7};
int a[10000],a1[10000];

//pretty print
void afisare(int *v,int n,int k,int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
		for(int i=1;i<=nivel;i++)
		{
			printf("   ");
		}
		printf("%d\n",v[k-1]);
	afisare(v,n,2*k,nivel+1);
}


void max_heapify(int *a,int i)
{
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
	profiler.countOperation("heapsort", n,1);
	if ((l+1)<h_size && a[l+1]>a[i])
	{
		largest=l+1;
	}else largest=i;
	profiler.countOperation("heapsort", n,1);
	if ( (r+1)<h_size && a[r+1]>a[largest])
	{
		largest=r+1;
	}
	if(largest!=i)
	{
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		profiler.countOperation("heapsort", n,3);
        max_heapify(a,largest);
	}

}
void max_heap_BU(int *a,int length)
{
	h_size=length;
	for (int i=((length-1)/2);i>=0;i--)
	{
		max_heapify(a,i);
	}
}

//sortarea heapsort
void heapsort(int *a,int length)
{
	int aux;
	max_heap_BU(a,length);
	for(int i=length-1;i>=1;i--)
	{
		aux=a[0];
		a[0]=a[i];
		a[i]=aux;
		profiler.countOperation("heapsort", length,3);
	h_size=h_size-1;
	max_heapify(a,0);
	}
}

//partitionarea vectorului pentru quicksort
int partition(int a[],int p,int r)
{
	int i,j,x,aux;
	profiler.countOperation("quick",n,1);
	x=a[r];
	//a[(p+r)/2]=a[r];
	//a[r]=x;
	i=p-1;
	for (j=p;j<=r-1;j++)
	 {
		profiler.countOperation("quick",n,1);
		if (a[j]<=x)
		{
			i=i+1;
			profiler.countOperation("quick",n,3);
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	  }
	profiler.countOperation("quick",n,3);
	aux=a[i+1];
	a[i+1]=a[r];
	a[r]=aux;
	return i+1;
}

//functia pentru sortarea in cel mai bun caz(best case)
void quick_best(int a[],int p, int r)
{
	int aux,left,right,x;
	x=a[(p+r)/2];
	profiler.countOperation("quickBest",n,1);  
	left=p;   
	right=r;
	while(left<=right)  
	{
		while(a[left]<x){ left++;
		                   profiler.countOperation("quickBest",n,1);       }                    
		while(a[right]>x){ right--;
		                     profiler.countOperation("quickBest",n,1); }        
		if(left<=right)
		{
			aux=a[left];
			a[left]=a[right];
			a[right]=aux;
			 profiler.countOperation("quickBest",n,3); 
			left++;
			right--;
		}
	}
	if(p<right) {quick_best(a,p,right);}
	if(left<r) {quick_best(a,left,r);}
}

//functia recursiva pentru quicksort
void Quicksort(int a[],int p,int r)
{ int q=0;
	if (p<r) {
		q=partition(a,p,r);
	   Quicksort(a,p,q-1);
	   Quicksort(a,q+1,r);
	}
}

//grafic pentru worst case la quicksort
void worstQuick()
{
  for (n=100;n<1000;n+=100)
	{
		FillRandomArray(a,n,10,50000,false,2);

		Quicksort(a1,0,n-1);

	}
   profiler.createGroup("WorstCase","quick");
 

}
//grafic pentru average case la quicksort si heapsort
void average()
{
for(int j=0;j<5;j++)
	 {
		 for (n=100;n<10000;n+=100)
	{
		FillRandomArray(a,n);

		memcpy(a1,a,n*sizeof(int));
		heapsort(a,n);
		Quicksort(a1,0,n-1);
	 }
		
	}
      profiler.createGroup("Average","heapsort","quick");
      
}

//grafic pentru best case la quicksort
void bestQuick()
{
	 for (n=100;n<10000;n+=100)
	{
		FillRandomArray(a,n,10,50000,false,2);

		quick_best(a1,0,n-1);

	}
   profiler.createGroup("BestCase","quickBest");
  
}
void afisare_vector(int v[],int n)
{
	for (int i=0;i<=n;i++)
	 {
		 printf(" %d ",v[i]);
	 }
}
void main()
	{
	 int m=10;
	// average();
	// worstQuick();
	 bestQuick();

	 profiler.showReport();
	 //heapsort si quicksort pentru un vector de dim mai mica initializat deja
	 heapsort(v,m);
	 afisare(v,m,1,0);

	 Quicksort(v1,0,9);
	 afisare_vector(v1,9);
	
	 quick_best(v2,0,9);
	 afisare_vector(v2,9);
 
}