#include "Profiler.h"
#include <iostream>
#define MAX_SIZE 10000

Profiler profiler("demo");
using namespace std;


int assigns;
int c, n;
int a[10000];


void bubbleSort()
{
	c=0; assigns=0;
	int ok=0, aux;
	do
	{
		profiler.countOperation("assign_bubble", n,0);
		ok=1;
		for(int i=1; i<n;i++)
		{
			if(a[i-1]>a[i])
			{
				ok=0;
				aux=a[i];
				a[i]=a[i-1];
				a[i-1]=aux;
				profiler.countOperation("assign_bubble", n,3);
			}
			profiler.countOperation("compare_bubble", n);
		}
	}while(ok==0);
}

void seleSort()
{
	profiler.countOperation("assign_select", n,0);
	c=0; assigns=0;
	int i, j;
	int min, aux;
	for(j=0; j<n-1;j++)
	{
		min=j;
		for (i=j+1;i<n;i++)
		{
			if(a[i]<a[min]){
				min=i;
			}
			profiler.countOperation("compare_select", n);
		}
		if(min!=j)
		{
			aux=a[min];
			a[min]=a[j];
			a[j]=aux;
			profiler.countOperation("assign_select", n);
			profiler.countOperation("assign_select", n);
			profiler.countOperation("assign_select", n);
		}
	}
}

void insSort()
{
	int i,k;
	int buff;
	c=0; assigns=0;
	
	for(i=1;i<n;i++)
	{
		k=i;
		buff=a[i];
		profiler.countOperation("assign_insert", n);
		while(a[k-1]>buff && k>0)
		{
			a[k]=a[k-1];
			profiler.countOperation("assign_insert", n);
			profiler.countOperation("compare_insert", n);
			k--;
		}
		profiler.countOperation("compare_insert", n);
		a[k]=buff;
		profiler.countOperation("assign_insert", n);
	}
}

	void bestCase()
	{
	int i;
	cout<<"best case"<<endl;
	for(n=0; n<=MAX_SIZE; n=n+500)
	{
		cout<<"sele"<<n<<endl;
		for(i=0;i<n;i++)
		{
			a[i]=i;
		}
		seleSort();
		
	}


	for(n=0; n<=MAX_SIZE; n=n+500)
	{
		cout<<"ins"<<n<<endl;
		for(i=0;i<n;i++)
		{
			a[i]=i;
		}
		insSort();
		
	}
	
	for(n=0; n<=MAX_SIZE; n=n+500)
	{
		cout<<"bubble"<<n;
		cout<<endl;
		for(i=0;i<n;i++)
		{
			a[i]=i;
		}
		bubbleSort();	
		
	}
}

	void avg()
	{
	int i;
	

	
	for(n=0; n<=MAX_SIZE; n=n+500)
	{
		FillRandomArray(a, n);
		cout<<"bubble"<<n<<endl;
		bubbleSort();
	}
	for(n=0; n<=MAX_SIZE; n=n+500)
	{
		cout<<"ins"<<n<<endl;
		FillRandomArray(a, n);
		insSort();
	}
	for(n=0; n<=MAX_SIZE; n=n+500)
	{
		cout<<"sele"<<n<<endl;
		FillRandomArray(a, n);
		seleSort();
	}

}

	void worst()

	{


	FILE *f;
	f=fopen("out.csv", "w");
	int i;

	srand((unsigned)time(0));
	fprintf(f,"n, bullbe, insert, select, \n");
	for(n=0;n<=MAX_SIZE;n=n+500)
	{
		cout<<"bubble"<<n<<endl;
		for(i=0;i<n;i++)
			a[i]=n-1-i; /// worst case for bubble
		bubbleSort();

		cout<<"ins"<<n<<endl;
		for(i=0;i<n;i++)
			a[i]=n-1-i; /// worst case for ins
		insSort();

		cout<<"sele"<<n<<endl;
		for(i=0;i<n/2;i++)
			a[i]=2*i+1;
		for(i=n/2;i<n;i++)
			a[i]=(n-1-i)*2;
		seleSort();

	}	
}


void main()
{
	
	//bestCase();
	//avg();
	worst();

	profiler.createGroup("Compare","compare_bubble","compare_insert","compare_select");
	profiler.createGroup("Assign","assign_bubble","assign_insert","assign_select");

	profiler.addSeries("Bubble","compare_bubble","assign_bubble");
	profiler.addSeries("Insert","compare_insert","assign_insert");
	profiler.addSeries("Select","compare_select","assign_select");

	profiler.createGroup("C+A","Bubble", "Insert","Select");
	
	


	profiler.showReport();
}