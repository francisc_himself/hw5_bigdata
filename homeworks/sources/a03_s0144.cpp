
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>
/*HeapSort este mai slab (dar nu cu mult) decat QuickSort, dar are marele avantaj fata de acesta ca nu este recursiv. 
Algoritmii recursivi consuma o mare cantitate de memorie.
HeapSort are eficienta O(nlog n)
QuickSort are tot O(n log n) dar pe cazul worst are O(n^2)*/
long compH;
long assignH;
long compQ;
long assignQ;
long heapSize;

void heapify(long A[], int i)  //i=index rad, elem. care trebuie adaugat
{
    int left,right,largest;
    long aux;

    left=2*i;
    right=2*i+1;

    compH=compH+1;
    if (left<=heapSize && A[left]>A[i]) 
        largest=left;	//rad, indexul copilului stg sau drp
    else
        largest=i;

    compH=compH+1;
    if (right<=heapSize && A[right]>A[largest])
        largest=right;
    if (largest!=i) 
        {
            assignH+=3;
            aux=A[i];
            A[i]=A[largest];
            A[largest]=aux;
            heapify(A,largest);  
        }
}

void bh_bottomup(long A[],int n)
{
    int i;
	heapSize=n;
    for (i=heapSize/2;i>=1;i--) 
        heapify(A,i); 
}

void heapSort(long A[],int n)
{
    int k;
    long aux;
    bh_bottomup(A,n);
    for (k=n;k>=2;k--)
    {
        assignH+=3;
        aux=A[1];
        A[1]=A[k];
        A[k]=aux;
        heapSize=heapSize-1;
        heapify(A,1);
    }
}

int partition(long A[],int p, int r,int pivot){

	long x,aux;
	int i,j;
	if(pivot==1)
		x=A[r];
	else if(pivot==2)
		x=A[(p+r)/2];
	else if(pivot==3)
		x=A[p+1];
	assignQ++;
	i=p-1;
	for(j=p;j<=r-1;j++){
		compQ++;
		if(A[j]<=x)
			{i++;
			assignQ+=3;
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
		}
	}
	assignQ+=3;
	aux=A[i+1];
	A[i+1]=A[r];
	A[r]=aux;

	return i+1;
}

void quickSort(long A[],int p, int r,int pivot)
{	if ( p < r )
    {
        int q = partition(A, p, r,pivot); 
			quickSort(A, p, q-1,pivot);
			quickSort(A, q+1, r,pivot);
    }
}

int main()
{
	srand(time(NULL));
    long heap[10001];
    long quick[10001];
    int j,k,nr;
    long compHeap=0,compQuick=0,assignHeap=0,assignQuick=0;
    FILE *f,*av,*b,*w;
    f=fopen("average.txt","w");
	w=fopen("worst_quicksort.txt","w");
	av=fopen("average_quicksort.txt","w");
	b=fopen("best_quicksort.txt","w");
	
	long a[11];
	a[0]=0;
	a[1]=15;
	a[2]=4;
	a[3]=1;
	a[4]=11;
	a[5]=25;
	a[6]=9;
	a[7]=8;
	a[8]=8;
	a[9]=12;
	a[10]=19;
	
	heapSort(a,10);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n");

	assignQ=compQ=0;
	quickSort(a,1,10,1);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("\n%d %d ",assignQ,compQ);


	//average case
    for (int i=100;i<=10000;i=i+100)
	{
		compH=compQ=assignH=assignQ=0;

		for (j=1;j<=5;j++)
		{
			nr=0;
			compHeap=compQuick=assignHeap=assignQuick=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				heap[nr]=quick[nr]=rand();
			}

			heapSort(heap,i);
			quickSort(quick,1,i,1);

			compHeap+=compH;
			compQuick+=compQ;
			assignHeap+=assignH;
			assignQuick+=assignQ;
		}
			fprintf(av,"%d %ld %ld %ld \n",i,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5);
			fprintf(f,"%d %ld %ld %ld %ld %ld %ld\n",i,compHeap/5,assignHeap/5,compHeap/5+assignHeap/5,compQuick/5,assignQuick/5,compQuick/5+assignQuick/5);
	}

	//best case
	for (int i=100;i<=10000;i=i+100)
	{
		compQ=assignQ=0;
		nr=1;
		for (j=1;j<=i;j++) //sortat
		{
	
			quick[nr]=j;
			nr++;
		}
		quickSort(quick,1,i,2);
		fprintf(b,"%d %ld %ld %ld \n",i,compQ,assignQ,compQ+assignQ);
		
	}

	
	//worst case
	for (int i=100;i<=10000;i=i+100)
	{
		compQ=assignQ=0;
		nr=1;
		for (j=1;j<=i;j++) //sortat
		{
			quick[nr]=j;
			nr++;
		}
		quickSort(quick,1,i,3);
		fprintf(w,"%d %ld %ld %ld \n",i,compQ,assignQ,compQ+assignQ);
		
	}
	
	
	fclose(f);
	fclose(av);
	fclose(b);
	fclose(w);
	//getch();
	return 0;

	
}
