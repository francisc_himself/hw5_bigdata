#include <stdio.h>;
#include <stdlib.h>
#include <conio.h>;
#include <math.h>;
#include <time.h>;

typedef struct elem{
	int key;
	int lista;
}Elem;

typedef struct list{
	Elem el;
	struct list *next;
}List;

int n;
int k;
int dimA[10001];
List *a[10001];
Elem H[10001];
int dimH;
int lo[10001];
int dimlo;
int nrOp;

//afisare
void afis(int A[],int n)
{
	int i;
	printf("\nSirul este : \n");

	for(i=1;i<=n; i++)
		printf("%d ",A[i]);
	printf("\n");
}

void afisElem(Elem A[],int n)
{
	int i;
	printf("\nSirul este : \n");

	for(i=1;i<=n; i++)
		printf("H[%d] = (%d, %d) ",i,A[i].key,A[i].lista);
	printf("\n");
}

void afisList(int index,int n)
{
	int j=1;
	List *p = a[index];
	
	printf("\nSirul este : \n");

	while(j<=dimA[index]){
		printf(" %d ",p->el.key);
			p=p->next;
			j++;
		}
	free(p);
	printf("\n");
}


//generarea a k liste ordonate
void genListeOrdonate(int n,int k){
	int i,j;
	srand(time(NULL));
	//aflarea dimensiunii si  initializare cu NULL
	for(i=1;i<k;i++){
		dimA[i] = n/k;
		nrOp++;
		a[i] = NULL;
	};
	a[k] = NULL;
	nrOp++;
	dimA[k] = n/k +n%k;
	//crearea a k liste ordonate
	for(i=1;i<=k;i++){
		int sr;
		List *p;
		List *incep;
		p = (List *)malloc(sizeof(List));
		sr = rand()%100;
		nrOp++;
		p->el.key = sr;
		p->el.lista = i;
		p->next = NULL;
		incep =p;
		for(j=2;j<=dimA[i];j++){
			List *q;
			q = (List *)malloc(sizeof(List));
			sr += rand()%100;
			nrOp++;
			q->el.key = sr;
			q->el.lista = i;
			q->next = NULL;
			p->next = q;
			p = p->next;
			q=q->next;
			free(q);	
		}
		
		p->next = NULL;
		nrOp++;
		a[i] = incep;
		p=p->next;
		incep = p;
		free(p);
		afisList(i,dimA[i]);
		free(incep);
	}
}

//sterge din lista "i" primul element
Elem stergeDinLista(int i){
	Elem el;
	if(dimA[i]!=0){
		nrOp++;
		el = a[i]->el;
		dimA[i]--;
		nrOp++;
		a[i] = a[i]->next;
	}else{
		nrOp++;
		el.key = 0;
		el.lista = 0;
		printf("\n!din lista %d nu se mai pot lua elemente",i);
	}
	return el;
}
//reconstruireHeap
void maxHeapify(Elem H[],int i){
    int largest;
    int stg = 2*i;
    int drp = 2*i+1;

	if((stg<=dimH) && (H[stg].key<H[i].key)){
        largest = stg;
    }else{
        largest = i;
    }
	if((drp<=dimH) && (H[drp].key<H[largest].key)){
        largest = drp;
    }
	nrOp+=2;
    if(largest != i){
		Elem aux = H[i];
		H[i] = H[largest];
        H[largest] = aux;
        nrOp+=3;
		maxHeapify(H,largest);
    }
}
//construireHeap
void buildMaxHeap(Elem H[]){
    int i;
	for(i=dimH/2;i>=1;i--){
        maxHeapify(H,i);
    }
}
//pushInHeap
Elem hPop(Elem H[]){
	Elem x = H[1];
	H[1] = H[dimH];
	nrOp+=2;
	dimH--;
	printf("\nIN H-POP: ");
	afisElem(H,dimH);
	maxHeapify(H,1);
	afisElem(H,dimH);
	return x;
}

int p(int i){
	return i/2;
}

//popFromHeap
void hPush(Elem H[],Elem x){
	dimH++;
	nrOp++;
	H[dimH] = x;
	int i = dimH;
	nrOp++;
	while((i>1) && (H[p(i)].key>H[i].key)){
		Elem aux;
		nrOp+=3;
		aux= H[p(i)];
		H[p(i)] = H[i];
		H[i] = aux;
		i = p(i);
	}
}

//scrie_l
void scrie_l(int lo[],int x){
	dimlo++;
	nrOp++;
	lo[dimlo] = x;
}

//Interclasarea
void interclasare(int k,int lo[],Elem H[]){
	dimH = 0;
	dimlo = 0;
	for(int i=1;i<=n;i++){
		H[i].key = 0;
		H[i].lista = 0;
	}
	for(int i=1;i<=k;i++){
		Elem x = stergeDinLista(i);
		nrOp++;
		if(x.lista != 0){
			hPush(H,x);
		}
	}
	afisElem(H,dimH);

	while(dimH>0){
		Elem x;
		x = hPop(H);
		nrOp++;
		scrie_l(lo,x.key);
		afis(lo,dimlo);
		
		x = stergeDinLista(x.lista);
		nrOp++;
		if (x.lista != 0){
			hPush(H,x);
			afisElem(H,dimH);
		}
	}
}


void main(){
	FILE *f;
	
	/*
	//n=10000 si k=10->500 +10
	f = fopen("nfix_kvar.csv","w");
	n=10000;
	int i;
	fprintf(f,"n,k,op\n");
	for(k=10;k<=500;k+=10){
		nrOp = 0;
		genListeOrdonate(n,k);	
		interclasare(k,lo,H);
		fprintf(f,"%d,%d,%d\n",n,k,nrOp);
	}

	fclose(f);

	//k=5,10,100 in trei valori, iar n=100->10000 +100
	f = fopen("nvar_kfix.csv","w");
	fprintf(f,"k,n,Op, ,k,n,Op, ,k,n,Op");
	int j;
	
		
		for(i=100;i<=10000;i+=100){
			for(j=0;j<3;j++){
				if(j==0) k=5;
				if(j==1) k=10;
				if(j==2) k=100;
				nrOp = 0;
				genListeOrdonate(i,k);	
				interclasare(k,lo,H);
				fprintf(f,"%d,%d,%d, ,",k,i,nrOp);
				
			}	
			fprintf(f,"\n");
		}
		
	fclose(f);
	*/
	//Demo
	printf("Incepem\n");
	nrOp = 0;
	printf("\n	*Generare Siruri ...\n");
	genListeOrdonate(10,3);	
	printf("\n	*S-a terminat generarea\n");
	printf("\n	*Incepem interclasarea\n");
	interclasare(3,lo,H);
	afis(lo,dimlo);
	printf("\nnr de operatii: %d\nAm terminat\n",nrOp);
	getch();
}

