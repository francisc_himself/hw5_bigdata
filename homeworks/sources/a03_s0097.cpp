// Laboratory3.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <string.h>
using namespace std;
int s,c;
int s1,c1;
int s2,c2;

/** 
** Heapsort procedure should take O(nlogn): build heap takes O(n) and each call to max heapify takes O(log n).
** Expected running time of quicksort is O(nlogn), but for worst case it ***ANONIM*** be O(n^2).
** For average comparison, heapsort has a faster growth on the chart than quicksort.
** QuickSort behavior:
** For generating the best case, partitioning should be done using the median. I used 
** an ordered array for generating data. For worst case, pivot was put at last element 
** and elements should be generated in ascending order. This results in an O(n^2) time, 
** which is such a fast growth that it determines the best case to not even appear.
*/

int partition(int a[],int p,int r);
void QuickSort(int list[],int m,int n, int caz);
void swap(int *x,int *y);
int choosePivotMedian(int i,int j );
void HeapSort(int A[], int heapsize);
void bottomUpBuildHeap(int A[], int heapsize);
void maxHeapify(int A[], int i, int heapsize);

void generateWorst(int *x, int n)
{
    int i=0;
	for(i=0;i<n;i++){
		x[i]=i;
	}
}

void generateBest(int *x, int n){
	int i=0;
	for(i=0;i<n;i++){
		x[i]=i;
	}
}

void generateAverage(int *x, int *y, int n){
	int i=0;
	for(i=0;i<n;i++){
		x[i]=rand();
		y[i]=x[i];
	}	
}

void generateAverage(int *x, int n){
	int i=0;
	for(i=0;i<n;i++){
		x[i]=rand();
	}	
}

void printToScreen(int *x,int n){
	int i=0;
	for(i=0;i<n;i++)
		printf("%d ",x[i]);
}

	int *a,*b,*d;
int main()
{
	int *sorted;
	FILE *fpQuickSort,*fpAverage;
    fpQuickSort = fopen("compareQuickSort.csv","w");
    fpAverage = fopen("compareAverage.csv","w");

	srand(2);

	//AVERAGE CASE for both sorts
	int n;
	
	for(n=100;n<10000;n+=100){
		s1=0;
		c1=0;
		s2=0;
		c2=0;
		for(int j=1;j<=5;j++){
			a=(int*)malloc(sizeof(int)*n);
			d=(int*)malloc(sizeof(int)*n);
			generateAverage(a,d,n);
			HeapSort(a,n);
			QuickSort(d,0,n-1,0);
		}
		printf("%d,%lu,%lu\n",n,(s2+c2)/5,(s1+c1)/5);
		fprintf(fpAverage,"%d,%lu,%lu\n",n,(s2+c2)/5,(s1+c1)/5);
	}
	

	// BEST and WORST CASE for QuickSort
	a=(int*)malloc(sizeof(int)*n);
	d=(int*)malloc(sizeof(int)*n);
	
	for(n=100;n<10000;n+=100){
		s1=0;
		c1=0;
		generateBest(d,n);
		QuickSort(d,0,n-1,0);
		printf("%d,%lu\n",n,s1+c1);
		fprintf(fpQuickSort,"%d,%lu,",n,s1+c1);
		s1=0;
		c1=0;
		generateWorst(a,n);
		QuickSort(a,0,n-1,1);
		printf("%d,%lu\n",n,s1+c1);
		fprintf(fpQuickSort,"%lu\n",s1+c1);
	}

	/* small test for algorithms:
	a=(int*)malloc(sizeof(int)*15);
	generateAverage(a,16);
	HeapSort(a,0,14);
	for(int j=0;j<15;j++)
        cout<<a[j]<<" ";
	cout<<endl;
	generateAverage(a,16);
	QuickSort(a,0,14,0);
	for(int j=0;j<15;j++)
        cout<<a[j]<<" ";
	*/
	getch();
	return 0;
}

void swap(int *x,int *y)
{
   s1=s1+3;
   int temp;
   temp = *x;
   *x = *y;
   *y = temp;
}

int choosePivotMedian(int i,int j )
{
   return((i+j) /2);
}

void QuickSort(int list[],int m,int n, int caz)
{
   int key,i,j,k;
   if( m < n )
   {
	  if(caz == 0)
		  k = choosePivotMedian(m,n);
	  else if (caz == 1)
			k = n;
      swap(&list[m],&list[k]);
      key = list[m];
      i = m+1;
      j = n;
      while(i <= j)
      {
		 
         while((i <= n) && (list[i] <= key)){
			 c1++;
			 i++;
		 }
         while((j >= m) && (list[j] > key)){
			 c1++;
			 j--;
		 }
         if( i < j)
                swap(&list[i],&list[j]);
      }
	  // swap two elements
      swap(&list[m],&list[j]);
	  // recursively sort the lesser list

	  QuickSort(list,m,j-1,caz);
      QuickSort(list,j+1,n,caz);
   }
}

int left(int i){
    return 2*i+1;
}

int right(int i){
    return 2*i + 2;
}

int parent(int i){
	return (i-1)/2;
}

void maxHeapify(int A[], int i, int heapsize){
    int l = left(i);
    int r = right(i);
    int largest = 0;

	c2++;
    if(l < heapsize && A[l]>A[i]) {
        largest = l;
    }
    else largest = i;

	c2++;
    if(r < heapsize && A[r]>A[largest]) {
        largest = r;
    }
    if(largest != i){
        int aux = A[i];
        A[i] = A[largest];
        A[largest] = aux;
		s2 += 3;
        maxHeapify(A, largest, heapsize);
    }
}

void bottomUpBuildHeap(int A[], int heapsize){
    for(int i = heapsize/2; i>=0; i--){
        maxHeapify(A, i, heapsize);
    }
}

void HeapSort(int A[], int heapsize){
	bottomUpBuildHeap(A,heapsize);
	for(int i = heapsize-1; i>=1; i--){
		int aux = A[i];
		A[i] = A[0];
		A[0] = aux;
		s2 += 3;
		heapsize--;
		maxHeapify(A,0,heapsize);
	}
}