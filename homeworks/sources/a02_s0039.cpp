#include <stdio.h> 
#include <conio.h>
#include <stdlib.h>
#include <time.h>;
#define MAXIM_LUNGIME 10000
int n=10;    //lungime folosita la metoda de testare
int A[MAXIM_LUNGIME+1];
int dim_B=0; //folosit la TopDown
int B[MAXIM_LUNGIME+1]={0}; //il initalizam
long contor_buttom_up=0;
long contor_top_down=0;
long contor_buttom_up_total=0;
long contor_top_down_total=0;

int dreapta (int i) //dreapta parintelui
{
	return 2*i+1;
}

int stanga (int i) // stanga parintelui
{
	return 2*i;
}

void Pretty_Print(int A[]) // Pretty Print
{
	int i,j;
	int dist_max=7;
	for(i=1;i<=4;i++)
	{
		if(i==1)
		{
			for(j=1;j<=dist_max;j++) printf(" ");
			printf("%d",A[i]);
		}
		printf("\n");
		if(i==2)
		{
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i]);
			for(j=1;j<=dist_max;j++) printf(" ");
			printf("%d",A[i+1]);
		}
		printf("\n");
		if(i==3)
		{
			for(j=1;j<=dist_max/4;j++) printf(" ");
			printf("%d",A[i+1]);
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i+2]);
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i+3]);
			for(j=1;j<=dist_max/2;j++) printf(" ");
			printf("%d",A[i+4]);

		}
		printf("\n");
		if(i==4)
		{
			printf("%d ",A[2*i]);
			printf("%d ",A[2*i+1]);
			printf("%d ",A[2*i+2]);

		}
	}
}
void reconstructie_heap(int A[],int i)
{
	int st=stanga(i);
	int dr=dreapta(i);
	int poz_min,aux;
	contor_buttom_up++;
	if(st<=n && A[st]<A[i]) 
	{
		poz_min=st;	
	}
	else 
	{
		poz_min=i;
	}
	contor_buttom_up++;
	if(dr<=n && A[dr]<A[poz_min])
	{
		poz_min=dr;
	}
	if(poz_min !=i)
	{
		contor_buttom_up++;
		aux=A[i];
		A[i]=A[poz_min];
		A[poz_min]=aux;
		reconstructie_heap(A,poz_min);
	}
}
void heap_buttom_up(int A[])
{
	int i;
	contor_buttom_up=0;
	for(i=n/2;i>=1;i--)  // mergem de la n/2 pt ca accesam doar nodurile care au fii. adiac nodurile care sunt parinti
		{
			reconstructie_heap(A,i);
		}
	//Pretty_Print(A);
	printf("\n");
}
void heap_push(int B[],int x)
{
	contor_top_down=contor_top_down+2;
	dim_B=dim_B+1;
	B[dim_B]=x;
	int i=dim_B;int aux;
	while (i>1 && B[i]<B[i/2])
	{
		contor_top_down=contor_top_down+2;
		aux=B[i];
		B[i]=B[i/2];
		B[i/2]=aux;
		i=i/2;
	}
}

void heap_top_down(int B[])
{
	int i;
	contor_top_down=0;
	dim_B=0;
	for(i=1;i<=n;i++)
	{
		heap_push(B,A[i]);
	}
	//Pretty_Print(B);
}


void testare_sir_mic()
{
	srand(time(NULL));
	int i;
	for(i=1;i<=n;i++)
		A[i]=rand()%10;
	for(i=1;i<=n;i++) printf("%d ",A[i]);
	printf("\n");
	heap_top_down(B);
	Pretty_Print(B);
	printf ("\n");
	heap_buttom_up(A);
	Pretty_Print(A);
	getch();
}

int main()
{
	//testare_sir_mic();

	FILE *f;

	f=fopen("rezultate_heap_lab3.csv","w");
	fprintf(f,"n,nr_operatii_top_down,nr_operatii_buttom_up\n");

	for (n=100;n<=10000;n=n+100)
	{
		contor_buttom_up_total=0;
		contor_top_down_total=0;

		printf("Status: n=%d \n",n);

		int k;
		for (k=1;k<=5;k++)
		{
			contor_buttom_up=0;
			contor_top_down=0;

			int i;
			srand(time(NULL));
			for (int i=1;i<=n;i++)
			{
				A[i]=rand() % 1000;
			}
			heap_top_down(B);
			contor_top_down_total=contor_top_down_total+contor_top_down;
			heap_buttom_up(A);
			contor_buttom_up_total=contor_buttom_up_total+contor_buttom_up;

		}
		fprintf(f,"%d, %ld, %ld \n",n,contor_top_down_total/5,contor_buttom_up_total/5);
	}
	getch();

	return 0;
}