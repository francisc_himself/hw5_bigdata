/**
Nume:***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa:***GROUP_NUMBER***

Concluzii:
	- privind atat algoritmul (codul) se poate concluzion ca metoda Top Down de construirea a heap-ului 
	necesita mai multe atribuiri si comparatii ( deci operatii) decat metoda Bottom Up
	- motivul principal este ca metoda Bottom Up presupune comparatii putine si doar o atribuire ( daca este privita
	ca o interschimbare sau 3 daca este privita ca o operatie)
	- metoda Top Down necesita inserarea minimului dupa care recursiv se compara si atribuie ( permuta ) elemente
	ale heap-ului pentru a se putea insera urmatorul element
	- graficele celor doua metode sunt liniare, aspectele prezentate mai sus fiind reliefate

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#define MAXIM 10000
#define ITERARI 5
int dimensiuneHeap;
int n=100;
int cmpBU,atrBU,cmpTD,atrTD;


void generareValori(int *a,int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%MAXIM;
	}
}


void afisare(int *a,int n)
{

	for(int i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
	printf("\n");
}

void copiereValori(int *a,int *b,int n)
{
	for(int i=0;i<n;i++)
	{
		b[i]=a[i];
	}
}

int parinte(int i)
{
	return i/2;
}

int stanga(int i)
{
	return 2*i++;
}

int dreapta(int i)
{
	
	return 2*i+2 ;
}


void maxHeapify(int *a,int i)
{
	int max;
	int	l=stanga(i);
	int	r=dreapta(i);
	cmpBU=cmpBU+2;
	
	if ((l<=dimensiuneHeap) && (a[l]>a[i]))
	{
		max=l;
	}
	else 
	{
		max=i;
	}
	if ((r<=dimensiuneHeap) && (a[r]>a[max]))
	{
		max=r;
	}
	if(max!=i)
	{
		int x=a[i];
		a[i]=a[max];
		a[max]=x;
		atrBU=atrBU+3;
	
		maxHeapify(a,max);
	}

}

void heapBU(int *a,int n)
{
	dimensiuneHeap=n;
	for(int i=n/2;i>=0;i--)
	{
		maxHeapify(a,i);
	}
}

void incrementCheie(int *a,int i,int cheie)
{
	cmpTD=cmpTD+2;
	if (cheie<a[i])
	{
		printf("Eroare");
		exit(0);
	}
	a[i]=cheie;
	atrTD=atrTD++;
	while((i>0)&&(a[parinte(i)]<a[i]))
	{	
		cmpTD=cmpTD++;
		atrTD=atrTD+3;
		int x=a[i];
		a[i]=a[parinte(i)];
		a[parinte(i)]=x;
		i=parinte(i);
	}
}


void maxHeapInsert(int *a,int cheie)
{
	dimensiuneHeap=dimensiuneHeap++;
	a[dimensiuneHeap]=INT_MIN;
	atrTD=atrTD++;
	incrementCheie(a,dimensiuneHeap,cheie);
}

void heapTD(int *a,int n)
{
	dimensiuneHeap=0;
	for(int i=1;i<n;i++)
		maxHeapInsert(a,a[i]);
}







void prettyPrint(int *a,int n,int i,int nivel)
{
	if(i<n){

	for(int j=0;j<nivel;j++)
		printf("\t");
	printf("%d\n",a[i]);
	prettyPrint(a,n,stanga(i),++nivel);
	prettyPrint(a,n,dreapta(i),++nivel);
	}
}



void main()
{
	int cmpBUtemp=0;
	int atrBUtemp=0;
	int cmpTDtemp=0;
	int atrTDtemp=0;

	FILE *f=fopen("Heap.csv","w");

	

	
	for(n=100;n<=10000;n=n+100)
	{
	int *a=(int*)malloc(sizeof(int)*n);
	int *b=(int*)malloc(sizeof(int)*n);
	dimensiuneHeap=n;
		for(int i=0;i<5;i++)
		{
			generareValori(a,n);
			copiereValori(a,b,n);
			heapBU(a,n);
			heapTD(b,n);
		}
		
		printf("%d ",n);
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d\n",n,atrBU/ITERARI,cmpBU/ITERARI,(cmpBU+atrBU)/ITERARI,atrTD/ITERARI,cmpTD/ITERARI,(cmpTD+atrTD)/ITERARI);
		cmpBU=0;
		atrBU=0;
		cmpTD=0;
		atrTD=0;
		free(a);
	}

}