/*

Arbori multicai

Am avut de implementat o structura de tip arbore multicai. Arborele multicai este un arbore cu mai multi fii. Acestia sunt memorati ca o lista
de fii. Am implementat lista de fii ca o lista dinamica, nu statica, deoarece daca ar fi statica am fi obligati pentru fiecare nod sa asignam
un vector de dimensiune = nr de noduri, ceea ce ar dauna mult memoriei. Fiii memorati ca o lista dinamica nu sunt mai usor de gestionat dar dau
performanta crescuta algoritmului

In cadrul implementarii am avut de transformat dintr-un sir de parinti intr-un arbore multicai, si dintr-un arbore multica intr-un arbore binar,
cu stanga = fiu si dreapta = frate

*/

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

typedef struct MULTI_NOD{
    int key;
    int count;
    int quantity;
    struct MULTI_NOD **child;
}MULTI_NOD;

typedef struct _binary_node{
	int key;
	struct _binary_node *left,*right;
}binary_node;

MULTI_NOD* createMN(int key1){
    MULTI_NOD *newn;
    newn = (MULTI_NOD*)malloc(sizeof(MULTI_NOD));
    newn->key = key1;
    newn->count = -1;
    newn->quantity = 0;
    newn->child = NULL;
	return newn;
}

void insertMN(MULTI_NOD *parent, MULTI_NOD *child){

      parent->count += 1;
    if(parent->quantity == 0){
        parent->child = (MULTI_NOD**)malloc(5*sizeof(MULTI_NOD*));
        parent->quantity = 5;
    }
    if (parent->count >= parent->quantity) {
        parent->quantity *= 2;
		parent->child =  (MULTI_NOD**)realloc(parent->child, parent->quantity);
    }

	parent->child[parent->count] = child;
}

MULTI_NOD* transform(int v[],int size){
	MULTI_NOD *root;
	MULTI_NOD **nodes;
	int i;
	nodes = (MULTI_NOD**)malloc(size*sizeof(MULTI_NOD*));
	for( i = 0; i < size; i++){
		nodes[i] = createMN(i);
	}
	for( i = 0; i < size; i++){
		if(v[i]!=-1){
			insertMN(nodes[v[i]],nodes[i]);
		}
		else{
			root = nodes[i];
		}
	}
	return root;
}


void afisareArbore(MULTI_NOD *nod, int depth){
	int level;
	if(nod == NULL){
	return;
	}
	for(level=0; level<depth; level++)
		printf("       ");
	printf("%d\n",nod->key);
	for(int i = 0; i<= nod->count; i++){
		afisareArbore(nod->child[i],depth+1);
	}
}

void afisareArboreBinar(binary_node *nod, int depth){
	int level;
	if(nod == NULL){
	return;
	}

	afisareArboreBinar(nod->left,depth+1);

	for(level=0; level<depth; level++)
		printf("       ");
	printf("%d\n",nod->key);


	afisareArboreBinar(nod->right,depth);


}

binary_node* CreareArboreBinar(MULTI_NOD* nod, struct MULTI_NOD **child,int dim){
	int i;
	binary_node *bin;
	if(nod == NULL){
		return NULL;
	}
	bin= (binary_node*)malloc(sizeof(binary_node));
	bin->key = nod->key;
	struct MULTI_NOD **brothers;
	brothers = (MULTI_NOD **)malloc(sizeof(MULTI_NOD*));
	for(i=0;i<dim;i++){
		brothers[i]=child[i+1];
	}
	if(nod->count == -1){
		bin->left = NULL;
	}
	else{
		bin->left = CreareArboreBinar(nod->child[0],nod->child,nod->count);
	}
	if(dim <= 0){
		bin->right = NULL;
	}
	else{
		bin->right = CreareArboreBinar(child[1],brothers,dim-1);
	}
	return bin;
}

int main(){
    int v[]={9,5,5,9,-1,4,4,5,5,4,2};
    int size = sizeof(v)/sizeof(v[0]);
	MULTI_NOD *root = transform(v,size);
	afisareArbore(root,1);
	printf("\n");
	printf("\n");
	printf("\n");
	binary_node *rootBin = CreareArboreBinar(root,NULL,-1);
	afisareArboreBinar(rootBin,1);
    //return 0;
	getch();
}
