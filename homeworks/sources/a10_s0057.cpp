/****ANONIM*** ***ANONIM*** grupa ***GROUP_NUMBER***
	Dupa cum bine stim acest algoritm dfs este un algoritm de cautare in adancime intr-un graf cand cautarea este limitata de operatii esentiale,adica vizitarea si inspectia unu nod
	din graf.Complexitatea acestui algoritm este de O(|V|+|E|) deoarece fiecare varf si fiecare muchie sunt vizitate.Dificultati apar la nivelul DFS si al generarii,dar datorita unor
	indicii primite aceastea au fost rezolvate.
*/#include <stdlib.h>
#include <stdio.h>
#include "Profiler.h"
Profiler profiler("demo");
#define V_MAX 10000
#define E_MAX 60000
#define white 0
#define gray 1
#define black 2
typedef struct TREE_NODE{
	int parent;
	int color;
	int d;
	int f;
	int key;
}TREE_NODE;
typedef struct graf{
	int nrNoduri;
	int nrMuchii;
}graf;
typedef struct _NODE{
	int key;
	struct _NODE*next;
}NODE;
typedef struct vect{
	int d;
	int key;
}vect;
NODE*adj[V_MAX];
struct TREE_NODE graph[V_MAX];
int timp;
int edges[E_MAX];
vect v[100];
int lung;
void DFS_VISIT(graf*g,struct TREE_NODE*u)
{
	//profiler.countOperation("Numar",lung,2);
	lung+=2;
	timp++;
	u->d=timp;
	u->color=gray;
	NODE* v=adj[u->key];
	//profiler.countOperation("Numar",lung,1);
		lung++;
	while(v!=NULL){
		if(graph[v->key].color==white){
			graph[v->key].parent=u->key;
			DFS_VISIT(g,&graph[v->key]);
		}
		v=v->next;
	}
	u->color=black;
	timp++;
	u->f=timp;

}
void DFS(graf*g){
	for(int i=0;i<g->nrNoduri;i++)
	{
		//profiler.countOperation("Numar",lung,1);
		lung++;
		graph[i].color=white;
		graph[i].parent=0;
		graph[i].key=i;
	}
	timp=0;
	for(int i=0;i<g->nrNoduri;i++){
		if(graph[i].color==white)
			DFS_VISIT(g,&graph[i]);
	}

}
void generate(int n,int m)
{
	memset(adj,0,n*sizeof(NODE*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<n;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		NODE*nod=(NODE*)malloc(sizeof(NODE));
		nod->key=b;
		nod->next=adj[a];//pointeaz spre primul element..b
		adj[a]=nod;
	}
}
void afisare(int x)
{
	for(int i=0;i<x;i++)
	{
		printf("%d",i);
		NODE*a=adj[i];
		while(a!=NULL)
		{
			printf("catre %d/",a->key);
			if(a!=NULL)
				a=a->next;
		}
		printf("\n");
	}
}
void print(graf *g)//,TREE_NODE*s,struct TREE_NODE*v)
{
	for(int i=0;i<g->nrNoduri;i++){


		printf("%d,%d,%d,%d.....%d\n",graph[i].key,graph[i].color,graph[i].d,graph[i].f,graph[i].parent);


	}

}
void sortare(graf* g){
	//for(int i=0;i<g->nrNoduri;i++)
	//	{			
	int i=0;
	int min=v[0].d;
	int aux;
	for (int i=1;i<g->nrNoduri-1;i++)
		for(int j=0;j<g->nrNoduri-1;j++)
		{
			if(v[j].d>v[j+1].d) 
			{

				vect aux=v[j];
				v[j]=v[j+1];
				v[j+1]=aux;

			}

		}
		for(int i=0;i<g->nrNoduri;i++)
			printf("%d..%d  ",v[i].key,v[i].d);
		printf("\n");

}
void  sort(graf*g,vect vect[]){
	int sort;
	for(int i=0;i<g->nrNoduri;i++){
		sort=0;
		NODE* v=adj[i];
		while(v!=NULL){
			int x=graph[i].key;
			int y=graph[v->key].key;
			if(y==x)
			{
				printf("muchie inainte\n");
				//break;
			}
			else
				if(graph[x].d<graph[y].d&&graph[y].d<graph[x].f&&graph[x].f<graph[y].f){
					printf("muchi tree\n");
				}

				else
					if(graph[x].f>graph[y].f&&graph[x].f>graph[y].d&&graph[x].f>graph[y].f){
						printf("muchie cross\n");
					}
					else
						if(graph[x].d>=graph[y].d&&graph[x].f>graph[x].d&&graph[y].f>=graph[x].f)
						{
							printf("muchie inapoi\n");
							sort=1;
						}

						v=v->next;
		}

		if(sort==1)
		{
			sortare(g);
		}
	}
}

void main(){
	graf *g=(graf*)malloc(sizeof(graf));
	//vect v[100];
	/*g->nrMuchii=5;
	g->nrNoduri=6;
	generate(5,6);
	DFS(g);
	for(int i=0;i<g->nrNoduri;i++){
	v[i].d=graph[i].d;
	v[i].key=graph[i].key;
	}
	print(g);
	sort(g,v);
	afisare(g->nrNoduri);
	*/
	FILE*fd;
	fd=fopen("afis.csv","w");
	fprintf(fd,"I ; lung\n");
	//for(int i=1000;i<5000;i+=100)
	//{
	//	printf("i=%d\n",i);
	//	lung=i;
	//	g->nrNoduri=100;
	//	g->nrMuchii=i;
	//	generate(g->nrNoduri,g->nrMuchii);
	//	//for(int j=0;j<g->nrNoduri;j++)
	//	DFS(g);
	//	//printf("....%d",lung);
	//	fprintf(fd,"%d ; %d\n",i,lung);
	//}
	for(int i=100;i<200;i+=10){
		printf("i=%d\n",i);
		lung=i;
		g->nrNoduri=i;
		g->nrMuchii=9000;
		generate(g->nrNoduri,g->nrMuchii);
		//for(int j=0;j<g->nrNoduri;j++)
		DFS(g);
		//printf("....%d",lung);
		fprintf(fd,"%d ; %d\n",i,lung);
	}
	//profiler.showReport();

}