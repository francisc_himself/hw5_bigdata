
/*
***ANONIM*** ***ANONIM***cu ***ANONIM***-grupa ***GROUP_NUMBER***
Algoritmul lui Joshephus consta in asezarea in cerc a n oameni. Dandu-se un numar m<=n algoritmul presupune scoaterea din joc a oamenilor,
astfel incat vom scoate tot al m-lea om.
Algoritmul are eficienta O(n*logn) si foloseste un arbore binar de cautare.
In cazul in care n=7 si m=3 elementele vor fi scoase in urmatoarea ordine: 3 6 2 7 5 1 4
*/
#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
typedef struct tip_nod 
			{ int numar,dimensiune; 
			  tip_nod *stg,*dr,*parinte;
		    } TIP_NOD;
int v[10000];
FILE *fis1,*fis2;
int c,a;
TIP_NOD *constructie(int s,int d,int *dim)
{TIP_NOD *p;
int dstg=0;
int ddr=0;
 if (s>d)
	  { *dim=0;
   	    return NULL;
	  }
 else{
	p=(TIP_NOD*) malloc(sizeof(TIP_NOD));
	p->numar=(s+d)/2;
	p->stg = NULL;
	p->dr = NULL;
	p->dimensiune=d-s+1;
	if(s<d)
	 {int m=(s+d)/2;c++;
	 p->stg=constructie(s,m-1,&dstg);
		if(p->stg != NULL)
			{
				p->stg->parinte = p;  c++;
			}
	  p->dr=constructie(m+1,d,&ddr);
		if(p->dr != NULL)
			{
				p->dr->parinte = p; c++;
			}
	 }
 }
*dim=p->dimensiune; a++;
  return p;
}
TIP_NOD *minim_arbore(TIP_NOD *arbore)
{while(arbore->stg!=NULL)
   {arbore=arbore->stg; a++;
    }
return arbore;
}
TIP_NOD *succesor_arbore(TIP_NOD *arbore)
{TIP_NOD *y;
	if(arbore->dr!=NULL)
	{return minim_arbore(arbore->dr); c++;}
	y=arbore->parinte;a++;
	while((y!=NULL) && (arbore==y->dr))
	    {arbore=y;
		y=y->parinte;a+=2;
    	}
	return y;
}

void scade_dimensiune(TIP_NOD *z)
{ while (z!=NULL)
	{  
	  z->dimensiune--;
	  z=z->parinte;a++;
	}
}

TIP_NOD* OS_Selectie(TIP_NOD *arbore,int i)
{int r;
	if(arbore->stg !=NULL)
	  {
		  r=arbore->stg->dimensiune+1;c++;
	  }
	else 
	     {
			 r=1;a++;
	     }

	if(i==r)
	  {
		  return arbore;c++;
	  }
	if(i<r)
	 {
		 return OS_Selectie(arbore->stg,i);c++;
	 }
	  else
	   {
		 return  OS_Selectie(arbore->dr,i-r);c++;
	   }
}

void sterge_arbore(TIP_NOD **arbore, TIP_NOD *z)
{TIP_NOD *x,*y;
	if(z->stg==NULL || z->dr==NULL)
	 {y=z;c++;a++;
	 }
	else {y=succesor_arbore(z);a++;
	     }
	if(y->stg!=NULL)
	  {x=y->stg;c++;a++;
	}
	else{x=y->dr; a++;
}	
	if(x!=NULL)
	{x->parinte=y->parinte; a++;c++;}
	scade_dimensiune(y->parinte);
	if(y->parinte==NULL)
	{*arbore=x;a++; c++;
	}
	else if(y==y->parinte->stg)
          {y->parinte->stg=x;a++;c++;
	      }
	     else {y->parinte->dr=x;a++;}
   if(y!=z)
    {z->numar=y->numar;a++;c++;}    
    free(y);
}


void joshepus(int n,int m)
{TIP_NOD *radacina,*y;
  int h,i,elem;
  int index=0;
  radacina=constructie(1,n,&h);a++;
  radacina->parinte=NULL;a++;
  i=m;
  while(n>0)
    {y=OS_Selectie(radacina,i);
	v[index]=y->numar; 
	  index++;
	  sterge_arbore(&radacina,y);
//	  scade_dimensiune(y);
	  n--;
	  if (n)
	     { 
		   i=(i+m-1)%n;
	       if (i==0)
		  i=n;
	     }
	}	
 
}
int main()
{fopen_s(&fis1,"test.txt","w");
 fopen_s(&fis2,"rezultate.csv","w");
// fprintf(fis1," %d elemente stergem al-%d-lea\n",7,3); 
for (int i=0;i<7;i++)
//{fprintf(fis1," %d",v[i]);joshepus(7,3);}
{
joshepus(7,3);
printf("%d ",v[i]);
}
 for (int n=100;n<10000;n+=100)
  {
    a=0;
	c=0;
   int m=n/2;
   joshepus(n,m);
//fprintf(fis2,"%d %d %d %d\n",n,a,c,a+c);
  }
    fclose(fis1);
	fclose(fis2);
	printf("GAta");
	_getch();
	return 0;
}
