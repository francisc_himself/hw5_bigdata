/*for varying n, the time needed for the lists to be merged is O(nlogk)
but, because k is constant, the growth is O(n) - linear
for varying k, with constant n=10000, the growth is logarithmic.
*/

#include<iostream>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include "Profiler.h"
using namespace std;
Profiler profiler("Merge k-Ordered Lists");
typedef struct nod
{
    int info;
    nod *urm;
} NOD;

typedef struct element
{
    int poz;
    int elem;
} ELEMENT;

struct liste
{
    NOD *prim;
    NOD *ultim;
};
struct liste CAP[501];

NOD *first ;
NOD *last;
ELEMENT Heap[501];
int nrC , nrA;
void adding(NOD **prim,NOD **ultim,int val)
{
    if((*prim)==NULL)
    {
        (*prim) = new NOD;
        (*prim)->info=val;
        (*prim)->urm=NULL;
        *ultim=(*prim);
    }
    else
    {
        NOD *elem=new NOD;
        elem->urm=NULL;
        elem->info=val;
        (*ultim)->urm=elem;
        (*ultim)=elem;
    }
}

void max_heapify_down(ELEMENT Heap[],int i,int size)
{
    int l,r,largest;
    ELEMENT aux;
    l=2*i;
    r=2*i+1;
    if ((l<=size)&&(Heap[l].elem < Heap[i].elem))
        largest=l;
    else
        largest=i;
    if ((r<=size)&&(Heap[r].elem < Heap[largest].elem))
        largest=r;
    nrC+=2;
    if (largest!=i)
    {
        aux=Heap[i];
        Heap[i] = Heap[largest];
        Heap[largest]=aux;
        nrA+=3;
        max_heapify_down(Heap,largest,size);
    }
}
void inserare_heap(ELEMENT Heap[],ELEMENT x,int *size)
{
    (*size)++;
    Heap[(*size)]=x;
    int i=(*size);
    ELEMENT aux;
    nrC++;
    while (i>1 && (Heap[i].elem)<(Heap[i/2].elem))
    {
        nrC++;
        aux=Heap[i];
        Heap[i]=Heap[i/2];
        Heap[i/2]=aux;
        nrA+=3;
        i=i/2;
    }
}

ELEMENT extragere_heap(ELEMENT Heap[],int *size)
{
    ELEMENT x;
    if((*size)>0)
    {
        x=Heap[1];
        Heap[1]=Heap[(*size)];
        nrA++;
        (*size)--;
        max_heapify_down(Heap,1,(*size));
    }
    return x;
}


void listmerge(liste CAP[],int k, int &nrA, int &nrC)
{	
    int size = 0;
    ELEMENT v;
    for(int i=1; i<=k; i++)
    {
        v.poz=i;
        v.elem=CAP[i].prim->info;
        nrA++;
        inserare_heap(Heap,v,&size);
    }

    ELEMENT pop;
    while (size>0)
    {

        pop = extragere_heap(Heap,&size);
        int poz =pop.poz;
        adding(&first,&last,pop.elem);
        CAP[poz].prim=CAP[poz].prim->urm;
        nrA++;
        nrC++;
        if (CAP[poz].prim!=NULL)
        {
            v.poz=pop.poz;
            v.elem=CAP[pop.poz].prim->info;
            nrA++;
            inserare_heap(Heap,v,&size);
			
        }
    }
}


void listgeneratee(liste CAP[],int k,int n)
{
    int nr = 0;
    for (int i=1; i<=k; i++)
        CAP[i].prim = CAP[i].ultim = NULL;

    for (int j=1; j<=k; j++) 

    {	nr=rand()%100;
        for (int i=1; i<=n/k; i++)
        {
            nr=nr+rand()%100;
            adding(&CAP[j].prim,&CAP[j].ultim,nr);
        }
    }

}

void print(NOD *prim,NOD *ultim)
{
    NOD *c;
    c=prim;
    while(c!=0)
    {
        cout<<c->info<<"->";
        c=c->urm;
    }
    cout<<endl;
}
void show(liste CAP[],int k)
{
    for (int i =1 ; i<=k;i++)
        print(CAP[i].prim,CAP[i].ultim);
}
int main()
{
	int op;
	int n=30;
    int k=10;
	listgeneratee(CAP,k,n);
	show(CAP,k);
	listmerge(CAP,k,nrC,nrA);
	print(first,last);
	getch();
/*
	profiler.createGroup("Operatii","Ops k=5","Ops k=10","Ops k=100");
	profiler.createGroup("Constant N","Const");
	for(int index=100;index<=10000;index+=100)
	{
		listgeneratee(CAP,k,index);
		nrC=0;
		nrA=0;
		listmerge(CAP,k,nrC,nrA);

		profiler.countOperation("Ops k=5",index,nrC);
		profiler.countOperation("Ops k=5",index,nrA);
	}
    
	k=10;
	for(int index=100;index<=10000;index+=100)
	{
		listgeneratee(CAP,k,index);
		nrC=0;
		nrA=0;
		listmerge(CAP,k,nrC,nrA);

		profiler.countOperation("Ops k=10",index,nrC);
		profiler.countOperation("Ops k=10",index,nrA);
	}
	k=100;
	for(int index=100;index<=10000;index+=100)
	{
		listgeneratee(CAP,k,index);
		nrC=0;
		nrA=0;
		listmerge(CAP,k,nrC,nrA);

		profiler.countOperation("Ops k=100",index,nrC);
		profiler.countOperation("Ops k=100",index,nrA);
	}
	n=10000;
	for(int i=10;i<=500;i+=10)
	{
		listgeneratee(CAP,i,n);
		nrC=0;
		nrA=0;
		listmerge(CAP,i,nrC,nrA);

		profiler.countOperation("Const",i,nrC);
		profiler.countOperation("Const",i,nrA);
	}
	profiler.showReport();*/
    return 0;
}