#include <stdio.h>
#include <stdlib.h>
#include <conio.h> 
#include "Profiler.h"
Profiler profiler("Sorting");

int n,k;

//structuri
struct element {
	int val;		//valoare
	int ind;		//index
};

typedef struct nod{
	int key;	
	struct nod *urm;
}NOD;

//lista
void create_empty_list (NOD **left, NOD **right)
{
	*left = 0;
	*right = 0;
}

void append (NOD **left, NOD **right, int k)//adaugare element in lista
{
	NOD *p;
	p = (NOD *) malloc(sizeof(NOD));
	p->key = k;

	if (*left == 0)
	{
		*left = p;
		*right = p;
	}
	else
	{
		(*right)->urm = p;
		*right = p;
	}
	printf("%d, ",p->key);
	p->urm = 0;
}

NOD* delete_head (NOD **left, NOD **right) //sterge capul listei 
{
	NOD *p;
	
	if (*left == 0)
	{
		return NULL;
	}

	p = *left;
	*left = (*left)->urm;
	return p;
}

void display_list (NOD *left) //scrie toate elementele dintr-o lista
{
	NOD *p;
	p = (NOD *) malloc(sizeof(NOD));
	
	if (left==0)
		printf ("Lista e vida!");
	else
	{
		p = left;
		while (p!=0)
		{
			printf ("%d ",p->key);
			p = p->urm;
		}
	}
	printf ("\n");
}

//heap
int st (int i)//returnare indice stanga
{
	return 2*i;
}

int dr (int i)//returnare indice dreapta
{
	return 2*i+1;
}

int parent (int i)//returnare indice parinte
{
	return i/2;
}

void initH (element v[10001],int *dim_heap)// initializeaza heapul cu dimensiunea 0
{
	*dim_heap = 0;
}

void Hpush (element a[10001], int *dim_a, int valoare, int index)//pune un element in heap
{
	int i;
	element aux;

	*dim_a = *dim_a + 1;
	switch(k)    //cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,1);
			break;
		case 10: profiler.countOperation("Sumk10",n,1);
			break;
		case 100: profiler.countOperation("Sumk100",n,1);
			break;
	}
	//profiler.countOperation("Sum",k,1);   //cazul 2
	a[*dim_a].ind = index;
	a[*dim_a].val = valoare;
	i = *dim_a;
	while (i>1 && a[i].val < a[parent(i)].val)
	{
		switch(k) //cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,4);
			break;
		case 10: profiler.countOperation("Sumk10",n,4);
			break;
		case 100: profiler.countOperation("Sumk100",n,4);
			break;
		
	}
		//profiler.countOperation("Sum",k,4);  //cazul 2
		aux = a[i];
		a[i] = a[parent(i)];
		a[parent(i)] = aux;
		i = parent (i);
	}
	switch(k) //  cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,1);
			break;
		case 10: profiler.countOperation("Sumk10",n,1);
			break;
		case 100: profiler.countOperation("Sumk100",n,1);
			break;
	}
}

void Heapify (element a[10001],int dim_heap, int i)//reconstructie heap
{
	int s,d,ind;
	element aux;

	s = st (i);
	d = dr (i);

	ind = i; 
	//profiler.countOperation("Sum",k,2);   cazul 2
	switch(k)   //cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,2);
			break;
		case 10: profiler.countOperation("Sumk10",n,2);
			break;
		case 100: profiler.countOperation("Sumk100",n,2);
			break;
	}
	if (s<=dim_heap && a[s].val < a[ind].val)
		ind = s;
	if (d<=dim_heap && a[d].val < a[ind].val)
			ind = d;

	if (ind != i)
	{
		switch(k)   // cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,3);
			break;
		case 10: profiler.countOperation("Sumk10",n,3);
			break;
		case 100: profiler.countOperation("Sumk100",n,3);
			break;
		}
		//profiler.countOperation("Sum",k,3);   cazul 2
		aux = a[i];
		a[i] = a[ind];
		a[ind] = aux;

		Heapify (a,dim_heap,ind);
	}	
}


element Hpop (element a[10001], int *dim_a)//scate element de pe heap
{
	element elem;
	if (*dim_a > 0)
	{
		switch(k)    //cazul1
		{
		case 5: profiler.countOperation("Sumk5",n,2);
			break;
		case 10: profiler.countOperation("Sumk10",n,2);
			break;
		case 100: profiler.countOperation("Sumk100",n,2);
			break;
	}
		//profiler.countOperation("Sum",k,2);    cazul 2
		elem = a[1];
		a[1] = a[*dim_a];
		*dim_a = *dim_a -1;
		Heapify (a,*dim_a,1);
	}
	return elem;
}

void merge (NOD* pcap[10001],NOD* ucap[10001], NOD **fout,NOD **lout,element H[10001], int *dim_heap)//interclasare lista
{
	initH (H,dim_heap);
	create_empty_list (fout,lout);
	NOD* p;
	p=(NOD *) malloc(sizeof(NOD));


	for (int i=1; i<=k; i++)
	{
		switch(k)    //cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,1);
			break;
		case 10: profiler.countOperation("Sumk10",n,1);
			break;
		case 100: profiler.countOperation("Sumk100",n,1);
			break;
	}
		//profiler.countOperation("Sum",k,1);    //cazul 2
		if (pcap[i]!=NULL)
		{
			switch(k)  {   //cazul 1
		case 5: profiler.countOperation("Sumk5",n,1);
			break;
		case 10: profiler.countOperation("Sumk10",n,1);
			break;
		case 100: profiler.countOperation("Sumk100",n,1);
			break;
		}
			//profiler.countOperation("Sum",k,1);   //cazul 2
			p = delete_head (&(pcap[i]),&(ucap[i]));
		}
		else
			p = NULL;
		switch(k)     //cazul 1
		{
		case 5: profiler.countOperation("Sumk5",n,1);
			break;
		case 10: profiler.countOperation("Sumk10",n,1);
			break;
		case 100: profiler.countOperation("Sumk100",n,1);
			break;
	}
		//profiler.countOperation("Sum",k,1);               //pt cazul 2
		if (p != NULL)
			Hpush (H,dim_heap,p->key,i);
	}

	while (*dim_heap > 0)
	{
		element elem = Hpop (H,dim_heap);
		append (fout,lout,elem.val);
		NOD *q;
	
		q = delete_head (&(pcap[elem.ind]),&(ucap[elem.ind]));
		switch(k)
		{
		case 5: profiler.countOperation("Sumk5",n,3);
			break;
		case 10: profiler.countOperation("Sumk10",n,3);
			break;
		case 100: profiler.countOperation("Sumk100",n,3);
			break;
	}
		//profiler.countOperation("Sum",k,3);    pt cazul 2
		if (q != NULL)
			Hpush (H,dim_heap,q->key,elem.ind);

	}
	
}




	
int main ()
	{
	
	int j,l,val,c;	//nlim = limita pentru n;  l = na unei liste; llasta = na lastei liste
	NOD *first[500],*last[500];		//listele cu elementele first si last
	NOD *fout,*lout;
	element H[501];
	int dim_heap, index;
	int v[10000];
	n = 10000;
	for(index=0; index<4; index++){
	k = 5;
	/*
	while(k<=100){
		//generam k liste a caror dimensiuni insumate n
		for (n=100; n<=10000; n+=100)  //cazul 1
		//for (k=10; k<=500 ; k+=10)   // cazul 2
		{
			
			//genereaza lungimea listei
			l = n/k;
			if (l * k == n)
			{
				//avem liste de dimensiuni egale
				for (j=1; j<=k; j++)
				{		
					create_empty_list (&first[j],&last[j]);
					FillRandomArray(v, l, 1);
					append (&first[j],&last[j],v[1]);
					for (c=2; c<=l; c++)
					{
						
						
						append (&first[j],&last[j],v[c]);
					}
				}
			}
			else
			{
				if (l != 0)
				{
					//avem k-1 liste de dimensiune egala si o lista mai mare
					for (j=1;j<k;j++)
					{
						create_empty_list (&first[j],&last[j]);
						FillRandomArray(v, l,1,10000,true);
						append (&first[j],&last[j],v[1]);
						for (c=2 ; c<=l; c++)
						{
							append (&first[j],&last[j],v[c]);
						}
					}
					//lungimea ultimei liste
					l = n - (k-1) * l;
					create_empty_list (&first[k],&last[k]);
					FillRandomArray(v, l, 1);
					append (&first[k],&last[k],v[1]);
					for (c=2; c<=l; c++)
					{
						append (&first[j],&last[j],v[c]);
					}
				}
				else
				{
					for (j=1; j<=k; j++)
					{
						create_empty_list (&first[j],&last[j]);
					}
					FillRandomArray(v, n, 1);
					for (c=1; c<=n; c++)		
					{
						//val = rand () %100 +1;
						append (&first[c],&last[c],v[c]);
					}
				}
			}
			merge (first,last,&fout,&lout,H,&dim_heap);
		}
	
		if(k==100) k = 200;
		if(k == 5){
		printf("done for k=5\n");
		k = 10;
	}
	else if(k == 10) {
		printf("Done for k=10");
		k = 100;
		
	}
	}
printf("\nAll done");


profiler.createGroup("Results", "Sumk5", "Sumk10", "Sumk100");// cazul 1
profiler.showReport();
*/
	k=2;l=10;
for (int j=1; j<=k; j++)
				{		
					create_empty_list (&first[j],&last[j]);
					FillRandomArray(v, l, 1,5000,true,1);
					append (&first[j],&last[j],v[0]);
					for (c=1; c<l; c++)
					{
						
						
						append (&first[j],& last[j],v[c]);
					}
				}
			merge (first,last,&fout,&lout,H,&dim_heap);
getch();
return 0;

}


}




