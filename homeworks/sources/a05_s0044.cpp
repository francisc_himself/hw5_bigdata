#include<conio.h>
#include<iostream>
#include<stdio.h>

#define C1 1
#define C2 1
#define N 10007

int T[10007];

int Quadratic(int k, int i){
	return (k % N + C1 * i + C2 * i * i) % N;
}

//initializez tabloul cu 0
void Hash_init(){
	int i;
	for (i = 0; i < N; i++)
		T[i]=0;
}

//insereaza elemente in tablou
void Hash_insert(int k){
	int i=0;
	while ( T[Quadratic(k,i)] != 0)
	{
		i++;
		T[Quadratic(k,i)] = k;
	}
	
}

//cauta un element in tablou
int Hash_search(int T[], int k){
	int i=0,j;
	do{
		j = Quadratic(k, i);
		if (T[j] == k)
			return j;
		i++;
	}while(T[j] != 0 && i < N);
	return 0;
}

//afisare
void Hash_Print(){
	int i;
	for (i = 0; i < N; i++)
	{
		printf ("%d",T[i] );
	}
}

/*/factor de umplere
void inserare(float fill_fact){
	int m,k;
	m = fill_fact * N;
	for (int i=1; i<=m; i++)
		k = rand() * rand() *10000;
	Hash_insert(k);
}	*/
int main(){
	int i,j,k,dim,r,l,m;
	double load[5]={0.8, 0.85, 0.9, 0.95, 0.99};
	for (i=0; i<5; i++)
	{
		dim=load[i]*N;
		for (j = 0; j < dim; j++)
		{
			r = rand() % N ;
			Hash_insert(r);
		}
		
			
	}
	Hash_Print();
}