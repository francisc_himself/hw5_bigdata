#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>

int marime=0;
int b_as,b_co, q_as, q_co ;
int n;
int dimHeap;
int a[10001],b[10001];

int parinte(int i)
{
	return i/2;
}


int stanga(int i)
{
	return 2*i;
}

int dreapta(int i)
{
	return (2*i)+1;
}

void ReconstructieHeap(int a[],int i) 
{
	int st,dr,index,aux;
	st=stanga(i);
	dr=dreapta(i);
	b_co++;
	if(st<=dimHeap && a[st]<a[i])
		index=st;
	else
		index=i;
	b_co++;
	if(dr<=dimHeap && a[dr]<a[index])
		index=dr;
	if(index!=i)
	{
		aux=a[i];
		a[i]=a[index];
		a[index]=aux;
		b_as+=3;
		ReconstructieHeap(a,index);
	}
}

void ConstruiesteHeapBU(int a[]) 
{
	dimHeap=n;
	for(int i=n/2;i>=1;i--)
		ReconstructieHeap(a,i);
}

void HeapSort(int a[])
{
	ConstruiesteHeapBU(a);
	
	int	aux;
	for (int i=n;i>=2; i--)
	{
		b_as=b_as+3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		dimHeap=dimHeap-1;
		ReconstructieHeap(a,1);
	}
}

int Partitie(int a[], int p, int r)
{
	int x,i,j;
	q_as++;
	x=a[p];
	i=p-1;
	j=r+1;
	int aux;
	while (1) 
	{
		
		do
		{
			q_co++;
			j=j-1;

		}while (a[j]<x);

		
		do
		{
			i++;
			q_co++;
		}while (a[i]>x);

	
		if (i<j)
		{
			q_as=q_as+3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		} else return j;	



	}

}


void QuickSort(int a[], int p, int r)
{
	int q;
	if (p<r)
	{
		q=Partitie(a,p,r);
		QuickSort(a,p,q);
		QuickSort(a,q+1,r);
	}

}


int main()
{
	int k;
	
	
	FILE *f=fopen("average.csv","w");
	FILE *g=fopen("worstquick.csv","w");
	FILE *h=fopen("bestquick.csv","w");
	fprintf(f, "n, atribuiribu, compararibu, sumabu, atribuiriquick, comparariquick, sumaquick\n");
	fprintf(g, "n, atribuiriquick, compararquick, sumaquick \n");
	fprintf(h, "n, atribuiriquick, compararquick, sumaquick \n");
	for(n=10;n<=1000;n+=10)
	{
		
		b_as=0;
		b_co=0;
		q_as=0;
		q_co=0;
		
	
		for(int numar=1;numar<=5;numar++)
		{
			
			for(int i=0;i<n;i++)
			{
				srand(time(NULL));
				a[i]=rand();
			}
			
			HeapSort(a);
			QuickSort(a,1,n);

		
		}

		
		fprintf(f,"%d, %d, %d, %d, ", n , b_as, b_co, b_as+b_co);
		fprintf(f,"%d, %d, %d\n ", q_as, q_co, q_as+q_co);

		// avem cele doua sortari in cazul mediu statistic
	    // urmeaza quicksortul in cazul favorabil si defavorabil
		
		k=n;

		for (int i=1; i<=n; i++)
		{
			a[i]=k;
			k--;
		}
		 
		q_as=0;
		q_co=0;
		QuickSort(a,1,n);
		fprintf(h,"%d, %d, %d, %d\n ", n, q_as, q_co, q_as+q_co);

		//cazul defavorabil
		q_as=0;
		q_co=0;

		for (int i=1; i<=n; i++)
		{
			a[i]=i;
		}
		QuickSort(a,1,n);
		fprintf(g,"%d, %d, %d, %d\n ", n, q_as, q_co, q_as+q_co);

		
	}

	fclose(f);
	fclose(g);
	fclose(h);


	return 0;
}