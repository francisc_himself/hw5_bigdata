/*
		In opinia mea , tema aceasta a fost cea mai dificila din acest semestru din cauza dificultati de a intelege cerinta
	si de a implementa lista de adiacenta.Pentru a duce la bun sfarsit algoritmul am ales sa renunt la lista de adiacenta ca 
	reprezentare si am ales sa utlizez un vector , acest lucru crescand dificultate de afisare a testului partial;
		Eficienta programului este de O(m log n);
	Student : ***ANONIM*** ***ANONIM*** - ***ANONIM***
	gr : ***GROUP_NUMBER***
*/
#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V_MAX 10000
#define E_MAX 60000

//definirea structuri pentru nod

typedef struct nod2
{
	int key;
	int rank;
	nod2 *p;
}NOD;

//declararae vectorilor
NOD nodes[V_MAX];
int edges1[E_MAX];
int count;

//functiile necesare , implementate din Cormen
void Make_Set(NOD *x,int i)
{	count++;
	x->rank=0;
	x->p=x;
	x->key=i;
}

NOD *Find_Set(NOD *x)
{
	if ((*x).p!=x) 
	{
		(*x).p=Find_Set(x->p);
		count++;
	}
	return x->p;
}

void Link(NOD *x,NOD *y)
{
	count++;
	if ((*x).rank>y->rank)
			(*y).p=x;
	else
	{
		(*x).p=y;
		if ((*x).rank==(*y).rank)
			(*x).rank++;
	}	
}

void Union(NOD *x,NOD *y)
{
	Link(Find_Set(x),Find_Set(y));
}

void conected_component(int m,int n)
{
	for (int i=0;i<n;i++)
	{
		Make_Set(&nodes[i],i);
	}
	for(int i=0;i<m-1;i++)
		{
			//dupa modelul dat de Dvs.
			int a=edges1[i]/n;
			int b=edges1[i]%n;
			if(Find_Set(&nodes[a])!=Find_Set(&nodes[b]))
			{
				Union(&nodes[a],&nodes[b]);
			}
		}
}

//afisarea 
void afisareG(int n)
{
	for(int i=0;i<n;i++)
		printf("%d -> %d \n",edges1[i]/n,edges1[i]%n);
}

void afisareGraph(int n)
{
	for(int i=0;i<n;i++)
		printf("%d -> %d rank:%d\n",nodes[i].p->key,nodes[i].key,nodes[i].rank);
}

int main()
{	
		//testarea partiala
		FillRandomArray(edges1,7,0,5*5-1,true,0);
		afisareG(5);
		conected_component(7,5);
		afisareGraph(5);	

		//inceput criere in fisier
		FILE *f =fopen("iesireMD.csv","w");
		for (int i=10000;i+1 <60000;i+=1000)
		{
			FillRandomArray(edges1,i,0,V_MAX*V_MAX-1,true,0);
			count=0;
			conected_component(i,V_MAX);
	 		fprintf(f,"%d,%d\n",i,count);
		}
	return 0;
}


