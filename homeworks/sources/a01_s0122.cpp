#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <time.h>
#define MAX 10000

using namespace std;

int asgn,comp;

/*--In the best case scenario, when the array is already sorted the Bubblesort is the most efficient

--In the worst case scenario, when the array is in reverse order the most efficient sorting algorithm is the Selectionsort:




--For the average case:

For random gerated numbers, we can clearly see that the insertion sort and selection sort are 
almost equally efficient and that the bubblesort is clearly the least efficient.


*/



void insertionsort(int a[],int n)
{	
	int i, buff,k;
	
	for (i=1;i<n;i++)
	{
		k=i-1;
		buff=a[i];
		asgn++;
		
		while(a[k]>buff && k>=0)
		{
			a[k+1]=a[k];
			k--;
			asgn++;
			comp++;
			
		}
		comp++;
		a[k+1]=buff;
		asgn++;

	}

	//cout<<"The ordered array is:";

	/*	for(i=0;i<n;i++)
		cout<<a[i]<<" ";
	
	cout<<endl;*/

	//cout<<"Number of assingments for insertion sort:"<<asgn<<endl;
	//cout<<"Number of comparisons for insertion sort:"<<comp<<endl;

}

void selectionsort(int a[], int n)
{
	int i, j, imin;
	int min;
	
	//min=a[0];
	for (i=0;i<n-1;i++)
	{	
		//min=a[i];
		imin=i;
		for (j=i+1;j<n;j++)
			{	
				comp++;
				if(a[j]<a[imin])
				{	
					
					//min=a[j];
					imin=j;
				}
			}
		if(i!=imin)
		{
			min=a[imin];
			a[imin]=a[i];
			a[i]=min;
			asgn=asgn+3;
		}
	}

	//cout<<"The ordered array is:";

	/*for(i=0;i<n;i++)
		cout<<a[i]<<" ";
	
	cout<<endl;*/

	//cout<<"Number of assingments for selection sort:"<<asgn<<endl;
	//cout<<"Number of comparisons for selection sort:"<<comp<<endl;
}

void bubblesort(int arr[], int n) {
      bool swapped = true;
      int j = 0,i;
	 
      int tmp;
      while (swapped) 
	  {
            swapped = false;
            j++;
            for (int i = 0; i < n - j; i++) 
			{	
				comp++;
              if (arr[i] > arr[i + 1]) 
				{
                        tmp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = tmp;
                        swapped = true;
						asgn=asgn+3;
                  }
            }
      }
	
	/*for(i=0;i<n;i++)
		cout<<arr[i]<<" ";
	
	cout<<endl;*/

	//cout<<"Number of assingments for bubble sort:"<<asgn<<endl;
	//cout<<"Number of comparisons for bubble sort:"<<comp<<endl;
}




void main()
{
	int a[MAX],b[MAX],s[MAX],c[MAX], n, i, k, buff;
	n=100;
	srand(time(NULL));
	ofstream f;
	f.open("worst.csv");
	f<<"n,asgnins,compins,asgncomp_ins,asgnselection,compselection,asgncomp_selection,assgnbub,compbub,asgncomp_bub \n";
	while(n!=10000)
	{	
		f<<n<<",";
		for (i=0;i<n;i++)
		{
			a[i]=n-i;
		}
		n=n+100;
		for(i=0;i<n;i++)
		{
			b[i]=a[i];
			c[i]=a[i];
			s[i]=a[i];
		}
		asgn=0;
		comp=0;
		insertionsort(c,n);
		f<<asgn<<","<<comp<<","<<asgn+comp<<",";
		asgn=0;
		comp=0;
		selectionsort(s,n);
		f<<asgn<<","<<comp<<","<<asgn+comp<<",";
		asgn=0;
		comp=0;
		
		bubblesort(b,n);
		f<<asgn<<","<<comp<<","<<asgn+comp<<"\n";
		asgn=0;
		comp=0;

	}
	//getch();

}