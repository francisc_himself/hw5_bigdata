#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

int atr, comp;

//build heap bottom up
int LEFT (int i){
	return 2 * i;
}

int RIGHT (int i){
	return 2 * i + 1;
}

void MIN_HEAPIFY (int A[], int i, int heap_size) {
	int l = LEFT (i), largest;
	int r = RIGHT (i);
	comp++;
	if (  (l <= heap_size) && (A[l] > A[i])) {
		largest = l;
	}
	else
		largest = i;
	comp++;
	if ( (r <= heap_size) && (A[r] > A[largest])) {
		largest = r;
	}
	if (largest != i) {
		atr = atr + 3;
		int aux = A[i];
		A[i] = A[largest];
		A[largest] = aux;
		MIN_HEAPIFY (A, largest, heap_size);
	}
}

void BUILD_MIN_HEAP_BOTTOM_UP (int a[], int heap_size){
	int i;
	atr = 0; comp = 0;
	for (i = heap_size / 2; i > 0; i--)
		MIN_HEAPIFY (a, i, heap_size);
}

void HEAPSORT(int a[], int heap_size){
    BUILD_MIN_HEAP_BOTTOM_UP(a, heap_size);
    int aux, i;
    for(i = heap_size; i >= 2; i--){
        aux = a[1];
        a[1] = a[i];
        a[i] = aux;
        heap_size--;
        MIN_HEAPIFY(a, 1, heap_size);
        atr = atr + 3;
    }
}

//quick-sort

int PARTITION (int A[], int p, int r){
	int x = A[r], i = p - 1, j, aux;
	atr++;
	for (j = p; j <= r - 1; j++){
		comp++;
		if (A[j] <= x){
			i++;
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;
		}
    }
	aux = A[i + 1];
	A[i + 1] = A[r];
	A[r] = aux;
	atr = atr + 3;
	return i + 1;
}

void QUICKSORT (int A[], int p, int r){
	if (p < r){
		int q = PARTITION (A, p, r);
		QUICKSORT (A, p, q - 1);
		QUICKSORT (A, q + 1, r);
	}
}

//copiere sir
void copiere_sir(int sursa[], int destinatie[], int n){
	int i;
	for (i = 1; i <= n; i++)
		destinatie[i] = sursa[i];
}

int main()
{
	int a[10001],b[10001], i, j, atribuiri[2], comparatii[2], heap_size, heap_size1;

//EXEMPLU PENTRU VERIFICAREA ALGORITMULUI
/**********************************************/
	heap_size = 10;
	a[1] = 2; a[2] = 13; a[3] = 9; a[4] = 7; a[5] = 1; a[6] = 3; a[7] = 5; a[8] = 15; a[9] = 3; a[10] = 4;
	printf("vector nesortat: \n");
	for(i = 1; i <= heap_size; i++)
		printf("%d ", a[i]);

	int adancime = 1;
	HEAPSORT(a, heap_size);
	printf("\nvector sortat bottom up: \n");
	for(i = 1; i <= 10; i++){
		printf("%d ", a[i]);
	}
    printf("\n");
	a[1] = 2; a[2] = 13; a[3] = 9; a[4] = 7; a[5] = 1; a[6] = 3; a[7] = 5; a[8] = 15; a[9] = 3; a[10] = 4;
	QUICKSORT(a,1,10);
	printf("\nvector sortat quicksort: \n");
	for(i = 1; i <= 10; i++){
		printf("%d ", a[i]);
	}
/**********************************************/

	FILE *f;
	f = fopen("heap sort bottom_up vs quick-sort.csv","w");

	//initializare pas pt functia rand
	srand (time(NULL));
	//cap tabel
	fprintf(f,"n,atr_heap_sort,comp_heap_sort,total_heap_sort,atr_quicksort,comp_quicksort,total_quicksort\n");

	for (heap_size = 100; heap_size <= 10000; heap_size = heap_size + 100)
	{
	    heap_size1 = heap_size;
		atribuiri[0] = 0;
		atribuiri[1] = 0;
		comparatii[0] = 0;
		comparatii[1] = 0;

		for (j = 1; j <= 5; j++){
			for (i = 1; i <= heap_size; i++){
				a[i] = rand() % 10000;
				b[i] = a[i];
			}
			HEAPSORT(a, heap_size);
			atribuiri[0] = atribuiri[0] + atr;
			comparatii[0] = comparatii[0] + comp;

			//copiere_sir(b, a, heap_size);

            atr = 0;
            comp = 0;
			QUICKSORT(b, 1, heap_size1);
			atribuiri[1] = atribuiri[1] + atr;
			comparatii[1] = comparatii[1] + comp;
		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d\n", heap_size1, atribuiri[0]/5, comparatii[0]/5, (atribuiri[0]+comparatii[0])/5, atribuiri[1]/5, comparatii[1]/5, (atribuiri[1]+comparatii[1])/5);
		printf("gata %d\n", heap_size1);
		heap_size = heap_size1;
	}

	fclose(f);

	printf("\ngata!!!");

	return 0;
}
