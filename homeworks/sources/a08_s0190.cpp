

#include <stdio.h>
#include <stdlib.h>
#include<conio.h>
#include<math.h>
#include<fstream>

using namespace std;

int cms;
int cu;
int cfs;

struct NOD{
	NOD *p;
	int rank;
	int inf;
};

void make_set(NOD *x)
{
	x->p=x;
	x->rank=0;
}

void Link(NOD *x, NOD *y)
{
	//printf("+");
	if(x->rank>y->rank)
	{
		y->p=x;
	}
	else
	{
		x->p=y;
		if(x->rank==y->rank)
			y->rank=y->rank+1;
	}
}

NOD* find_set(NOD *x)
{
	if(x!=x->p)
		x->p = find_set(x->p);
	return x->p;
	/*do
		{
			x=x->p;
		}while(x->p!=x);
		return x=x->p;*/
}

void afisare(NOD *x)
{
		do
		{
			printf("%d ",x->inf);
			x=x->p;
		}while(x->p!=x);
		x=x->p;
		printf("-> p= %d ",x->inf);
}

void Union(NOD *x, NOD *y)
{
	cfs=cfs+2;
	Link(find_set(x),find_set(y));
}

bool same_component(NOD *u, NOD *v)
{
	if(find_set(u)==find_set(v))
		return true;
	return false;
}

void connected_components(int n)
{	
	//fisierul
	FILE *out;
	out = fopen("iesire.csv","w");
	fprintf(out,"cms,cu,cfs\n");
	int conexiuni=10000, k=0;
	for(conexiuni=10000;conexiuni<=60000;conexiuni=conexiuni+1000)
	{
		cms=0;
		cu=0;
		cfs=0;

		NOD *sir[10000];
		for(int i=0;i<n;i++)
		{
			cms++;
			NOD *nodNou;
			nodNou = (NOD*) malloc(sizeof(NOD));
			nodNou->inf=i;
			make_set(nodNou);
			sir[i]=nodNou;
		}
		

		printf("%d \n",conexiuni);
		k=0;
		while(k<conexiuni)
		{
			NOD *c1;
			NOD *c2;

			//luam 2 noduri random
			c1 = sir[ rand()%(9999 - 0) + 0];
			c2 = sir[ rand()%(9999 - 0) + 0];

			//si le unim
			cfs=cfs+2;
			if(find_set(c1)!=find_set(c2))
			{
				cu++;
				Union(c1,c2);
				
			}
			k++;
		}
		fprintf(out,"%d,%d,%d\n",cms,cu,cfs);
	}
	fclose(out);
}

void connected_components_exemplu(int n)
{	
	//fisierul

	NOD *sir[10000];
	for(int i=0;i<n;i++)
	{
		NOD *nodNou;
		nodNou = (NOD*) malloc(sizeof(NOD));
		nodNou->inf=i;
		make_set(nodNou);
		sir[i]=nodNou;
	}

	//conexiuni
	Union(sir[0],sir[1]);
	Union(sir[1],sir[2]);

	Union(sir[3],sir[4]);
	Union(sir[4],sir[5]);	

	printf("afisam prima componenta conexa \n");
	afisare(sir[0]);
	printf("\n");
	afisare(sir[1]);
	printf("\n");
	afisare(sir[2]);
	printf("\n");
	printf("\n");
	printf("afisam a doua componenta conexa \n");
	afisare(sir[3]);
	printf("\n");
	afisare(sir[4]);
	printf("\n");
	afisare(sir[5]);
	printf("\n");

}



int main()
{
	//connected_components(10000);
	connected_components_exemplu(6);
	return 0;
}

