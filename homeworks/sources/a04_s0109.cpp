#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/*Struct node*/
typedef struct node
{

int val;
int source;
node *next;
}node;

/*List definition */
typedef struct list{
	node *head;
	node *tail;
}list;

/*Value from lists*/
typedef struct ord{
 int val;
 int source;
}ord;


int heapsize=0;
int A=0;
int C=0;
int theapsize=0;


/*Returns left child*/
int left(int k){
	return 2*k;
}
/*Returns right child*/
int right(int k){
	return 2*k+1;
}
/*Returns parent*/
int parent(int k){
	return k/2;
}

/*Swaps two values*/
void swap(ord &a, ord &b){
ord aux=a;
a=b;
b=aux;
}

//heap sort bottom up----------------------------------------
/*method that takes a node and knowing that its two aubtrees are heaps creates
a new heap*/

void heapify(ord *a, int i){
int largest=i;

int l=left(i);
int r=right(i);
C++;
if((l<=heapsize)&&(a[l].val>a[i].val)) largest=l;
						  else largest=i;
C++;
if((r<=heapsize)&&(a[r].val>a[largest].val)) largest=r;
C++;
if(largest!=i){
	A=A+3;
	swap(a[largest],a[i]);
	heapify(a,largest);
}
}
/*method that builds a heap starting from its leaves, that are already heaps
and goes up to the root by calling heapify*/

void build_heap(ord *a,int n){
	for(int i=n/2;i>0;i--){
		heapify(a,i);
}
}
/*method that sorts an array building a heap and using the heap property,
and reconstructs the heap at every step with heapify*/

void heap_sort(ord *a, int n){


heapsize=n;
build_heap(a,n);
for(int i=n;i>=2;i--){
	swap(a[1],a[i]);
	A=A+3;
	heapsize--;
	heapify(a,1);
}
}

/*method that inserts a node into the list*/
void insert(node *(&head), node* (&tail), int val, int i){
	if (head==NULL){
	head=new node;
	head->val=val;
	tail=head;
	head->source=i;
	head->next=NULL;
	}
	else
	{
		//printf("da %d",i);	
		node* aux=new node;
		aux->val=val;
		aux->source=i;
		aux->next=NULL;
		tail->next=aux;
		tail=tail->next;
	}


}
/*removes the head of the list*/
void remove_element(node *(&head)){
	if(head!=NULL){
	node *p=head;
	if(head!=NULL) head=head->next;
	free(p);
	}
}

/*checks if the array is ordered*/
int check_ordered(int v[], int n){
bool ok=true;
for(int i=1;i<=n-1;i++)
	if(v[i]>v[i+1]) ok=false;
if (ok==true) return 0;
else return 1;
}

int corect=0;
int aux_corect=0;

/*creates the heap from the selected lists*/
void create_heap(list *liste, int k, ord* a, int* rez){
	theapsize=k;
	int contor=0;
	int aaaux=0;
	int auxk=k;
	for(int i=1;i<=auxk;i++){
		if (liste[i].head!=NULL){
			a[i].val=(liste[i].head)->val;
			a[i].source=(liste[i].head)->source;
		}
		else k--;
	}

	build_heap(a,k);
	
	heap_sort(a,k);
	printf("\n");
	
	while(theapsize>0){
		rez[contor]=a[1].val;
		A++;
		contor++;
		aaaux=a[1].source;
		A++;
		remove_element(liste[aaaux].head);
		if(liste[aaaux].head!=NULL){
			a[1].val=(liste[aaaux].head)->val;
			a[1].source=(liste[aaaux].head)->source;
			A++;
		}
		else
		{
			swap(a[1],a[theapsize]);
			A=A+3;
			theapsize--;
		}
		heapify(a,1);
	//heap_sort(a,theapsize);
	}
	aux_corect=check_ordered(rez,contor-1);
	if(aux_corect!=0)
				{
					printf("\n Programul ordoneaza prost! \n");
					getch();
				}
	corect=corect+check_ordered(rez,contor-1);
}
/* main */
int main(){
int k=10;
int aaux=0;
int n=1000;


int *rez= new int[n*k+1];



ord *a=new ord[k+1];
list* liste=new list[k+1];
for(int i=1;i<=k;i++){
	liste[i].head=NULL;
	liste[i].tail=NULL;
}

int i=0;
FILE *f=fopen("heap.csv","w");
k=5;
for(n=100;n<10000;n=n+100)
{
	k=5;
	a=new ord[k+1];
	liste=new list[k+1];
		for(int ii=1;ii<=k;ii++){
			liste[ii].head=NULL;
			liste[ii].tail=NULL;
		}
	printf("1 %d\n",n);
	C=0;
	A=0;
	rez= new int[n*k+1];
	aaux=0;
	for(int j=0;j<n;j++){
		aaux=aaux+rand()%k;
		i=rand()%k+1;
		insert(liste[i].head,liste[i].tail,aaux,i);
		
	}

	create_heap(liste,k,a,rez);
	fprintf(f,"%d,%d,%d,%d,",n,C,A,C+A);


	k=10;

	a=new ord[k+1];
	liste=new list[k+1];
		for(int ii=1;ii<=k;ii++){
			liste[ii].head=NULL;
			liste[ii].tail=NULL;
		}

	printf("2 %d\n",n);
	C=0;
	A=0;
	rez= new int[n*k+1];
	aaux=0;
	for(int j=0;j<n;j++){
		aaux=aaux+rand()%k;
		i=rand()%k+1;
		insert(liste[i].head,liste[i].tail,aaux,i);
		
}
	create_heap(liste,k,a,rez);
	fprintf(f,"%d,%d,%d,%d,",n,C,A,C+A);

	k=30;

	a=new ord[k+1];
	liste=new list[k+1];
		for(int ii=1;ii<=k;ii++){
			liste[ii].head=NULL;
			liste[ii].tail=NULL;
		}
	printf("3 %d\n",n);
	C=0;
	A=0;
	rez= new int[n*(k+1)+1];
	aaux=0;
	i=0;
	for(int j=0;j<n;j++){
		aaux=aaux+rand()%k;
		i=rand()%k+1;
		insert(liste[i].head,liste[i].tail,aaux,i);
		
	}
	create_heap(liste,k ,a,rez);
	fprintf(f,"%d,%d,%d,%d\n",n,C,A,C+A);
	printf("%d\n",n);
}



fclose(f);

f=fopen("heap2.csv","w");
n=10000;
int raux=0;
for(k=10;k<500;k=k+10)
{
	A=0;
	C=0;
	a=new ord[k+2];
	raux=0;
	liste=new list[k+2];
	for(int i=1;i<=k+1;i++){
		liste[i].head=NULL;
		liste[i].tail=NULL;
}
	printf("%d\n",k);
	for(int i=0;i<n;i++){
		raux=raux+rand()%k;
		int sir=rand()%k+1;
		insert(liste[sir].head,liste[sir].tail,aaux,sir);
	
	}

	
	create_heap(liste,k,a,rez);
	fprintf(f,"%d,%d,%d,%d\n",k,C,A,C+A);
}


fclose(f);
printf("\nArray which have not been ordered: %d\n", corect);
getch();
return 0;
}