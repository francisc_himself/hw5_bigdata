//For hash table is easier to find a value that is already in the array, than a value that was not stored.
//Time complexity for average case: in both insertion and search steps is O(1).
//The finding of the elements decreases inversely proportional to the filling factor.

#include<iostream>
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include"Profiler.h"
#include<iomanip>

Profiler profiler("demo");
using namespace std;

int n, c1, c2, ht[10007],m,a[10007], nf, f, hm[3000], N = 10007;
double alfa;

int hashInsert(int *, int);
int hashSearch(int *,int);
int h(int, int);

int h(int key, int i)
{
    return ((key % N + c1 * i + c2 * i * i) % N);
}
int hashInsert(int *T, int key)
{
    int i = 0, j;
    do{
        j = h(key,i);
        if(T[j] == 0)
        {
            T[j] = key;
            return j;
        }
        else ++i;
    }while(i != N);
	printf("\n T[j] = %d , i= %d, j= %d n=%d\n", T[j],i,j,n);
    printf("hash table overflow");
    return -1;
}
int hashSearch(int *T, int key)
{
    int i=0,j;
    do{
        j = h(key ,i);
        if(T[j] == key)
		{
			f=i+1;
            return j;
		}
        i++;
    }while(T[j] != NULL && i!=N);
	//printf("\n i: %d",i);
	nf = i+1;
    return -2; 
}
void main()
{
	int ans, total_f, total_nf,tt_f, tt_nf;
    c1 = 1;
    c2 = 1;
	m=3000;
	for(int i =1 ; i<6; i++)
	{
		printf("\n\n\n");
		std::cout << std::setw(10) << std::right << "Filling " << std::setw(15) << "Avg Effort" << std::setw(15) << "Max effort " << std::setw(15) <<"Avg effort" << std::setw(15) << "Max effort"<<std::endl;
		std::cout << std::setw(10) << std::right << "factor" << std::setw(15) << " found" << std::setw(15) << " found" << std::setw(15) <<" not found" << std::setw(15) << " not found"<<std::endl;
		
		alfa = 0.8;
		n =alfa * N;
		f = 0;
		nf = 0;
		//printf("\nn = %d",n);
		//getche();
		for(int j=0; j<N; j++)
			ht[j]= 0;
		int x;
		 total_f = 0;
		 total_nf = 0;
		 tt_f = 0;
		 tt_nf =0;
		FillRandomArray(a,n,1, 30000,true);
		for(int j=0; j<n; j++)
			{
			//	printf("\na[%d] : %d",j,a[j]);
				x = hashInsert(ht,a[j]);
			if(x == -1) break;
		}
		for(int j=(m+1)/2; j<m; j++)
			hm[j] = a[rand()%n];
		FillRandomArray(hm,m/2,30000,50000);
		for(int j=0; j<m; j++)
		{
			x = hashSearch(ht,hm[j]);
			if(x == -2) {
				total_nf += nf;
				if(nf > tt_nf)tt_nf = nf;
			}
			else {
				total_f += f;
				if(tt_f < f)tt_f = f;
			}
		}
	std::cout << std::setw(10) << std::right << alfa << std::setw(15) <<2*total_f/m << std::setw(15) << tt_f << std::setw(15) <<2*total_nf/m << std::setw(15) << tt_nf<<std::endl;
	//getche();
		alfa = 0.85;
		n =alfa * N;
		f = 0;
		nf = 0;
	//	printf("\nn = %d",n);
	//	getche();
		for(int j=0; j<N; j++)
			ht[j]= 0;
		 total_f = 0;
		 tt_f = 0;
		 tt_nf = 0;
		 total_nf = 0;
		FillRandomArray(a,n,1,30000,true);
		for(int j=0; j<n; j++)
			{
				//printf("\na[%d] : %d",j,a[j]);
				x = hashInsert(ht,a[j]);
			if(x == -1) break;
		}
		for(int j=(m+1)/2; j<m; j++)
			hm[j] = a[rand()%n];
		FillRandomArray(hm,m/2,30000,50000);
		for(int j=0; j<m; j++)
		{
			x = hashSearch(ht,hm[j]);
			if(x == -2) {
				total_nf += nf;
				if(nf > tt_nf)tt_nf = nf;
			}
			else {
				total_f += f;
				if(tt_f < f)tt_f = f;
			}
		}
	std::cout << std::setw(10) << std::right << alfa << std::setw(15) <<(double)total_f/m << std::setw(15) << tt_f << std::setw(15) <<(double)total_nf/m << std::setw(15) << tt_nf<<std::endl;
	//getche();
		alfa = 0.9;
		n =alfa * N;
		f = 0;
		nf = 0;
		//printf("\nn = %d",n);
		/*getche();*/
		for(int j=0; j<N; j++)
			ht[j]= 0;
		 total_f = 0;
		 total_nf = 0;
		 tt_f = 0;
		 tt_nf = 0;
		FillRandomArray(a,n,1,30000,true);
		for(int j=0; j<n; j++)
			{
				//printf("\na[%d] : %d",j,a[j]);
				x = hashInsert(ht,a[j]);
			if(x == -1) break;
		}
		for(int j=(m+1)/2; j<m; j++)
			hm[j] = a[rand()%n];
		FillRandomArray(hm,m/2,30000,50000);
		for(int j=0; j<m; j++)
		{
			x = hashSearch(ht,hm[j]);
			if(x == -2) {
				total_nf += nf;
				if(nf > tt_nf)tt_nf = nf;
			}
			else {
				total_f += f;
				if(tt_f < f)tt_f = f;
			}
		}
	std::cout << std::setw(10) << std::right << alfa << std::setw(15) <<(double)total_f/m << std::setw(15) << tt_f << std::setw(15) <<(double)total_nf/m << std::setw(15) << tt_nf<<std::endl;
	/*getche();*/
		alfa = 0.95;
		n =alfa * N;
		f = 0;
		nf = 0;
	//	printf("\nn = %d",n);
	//	getche();
		for(int j=0; j<N; j++)
			ht[j]= 0;
		 total_f = 0;
		 tt_f = 0;
		 tt_nf = 0;
		 total_nf = 0;
		FillRandomArray(a,n,1,30000,true);
		for(int j=0; j<n; j++)
			{
	//			printf("\na[%d] : %d",j,a[j]);
				x = hashInsert(ht,a[j]);
			if(x == -1) break;
		}
		for(int j=(m+1)/2; j<m; j++)
			hm[j] = a[rand()%n];
		FillRandomArray(hm,m/2,30000,50000);
		for(int j=0; j<m; j++)
		{
			x = hashSearch(ht,hm[j]);
			if(x == -2) {
				total_nf += nf;
				if(nf > tt_nf)tt_nf = nf;
			}
			else {
				total_f += f;
				if(tt_f < f)tt_f = f;
			}
		}
	std::cout << std::setw(10) << std::right << alfa << std::setw(15) <<(double)total_f/m << std::setw(15) << tt_f << std::setw(15) <<(double)total_nf/m << std::setw(15) << tt_nf<<std::endl;
	//getche();
	alfa = 0.99;
		n =alfa * N;
		f = 0;
		nf = 0;
	//	printf("\nn = %d",n);
		for(int j=0; j<N; j++)
			ht[j]= 0;
		 total_f = 0;
		 total_nf = 0;
		 tt_f = 0;
		 tt_nf = 0;
		FillRandomArray(a,n,1,30000,true);
		for(int j=0; j<n; j++)
			{
				//printf("\na[%d] : %d",j,a[j]);
				x = hashInsert(ht,a[j]);
			if(x == -1) break;
		}
		for(int j=(m+1)/2; j<m; j++)
			hm[j] = a[rand()%n];
		FillRandomArray(hm,m/2,30000,50000);
		for(int j=0; j<m; j++)
		{
			x = hashSearch(ht,hm[j]);
			if(x == -2) {
				total_nf += nf;
				if(nf > tt_nf)tt_nf = nf;
			}
			else {
				total_f += f;
				if(tt_f < f)tt_f = f;
			}
		}
	std::cout << std::setw(10) << std::right << alfa << std::setw(15) <<(double)total_f/m << std::setw(15) << tt_f << std::setw(15) <<(double)total_nf/m << std::setw(15) << tt_nf<<std::endl;
	//getche();
}}