#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>

typedef struct nod_b{
	int key;
	struct nod_b *st;
	struct nod_b *dr;
}NOD_B;

typedef struct nod_m{
	int nrFii;
	int fii[10];
}NOD_M;

NOD_B *rad_b;
NOD_M multicai[20];
int rad,n;

void citire(int p[])
{
	printf("numar noduri: ");scanf("%d",&n);
	printf("dati vectorul de parinti\n");
	for(int i=1;i<=n;i++)
	{
		printf("p[%d]= ",i);
		scanf("%d",&p[i]);
	}
}

void T1_parinte_multicai(int p[])
{
	for(int i=1;i<=n;i++)
		if(p[i]!=-1)
		{
			multicai[p[i]].nrFii++;
			multicai[p[i]].fii[multicai[p[i]].nrFii]=i;
		}
		else
			rad=i;
}

void T2_multicai_binar1(NOD_B **radB)
{
	NOD_B *p,*q;
	
	if(multicai[(*radB)->key].nrFii>0){
		(*radB)->st=(NOD_B *)malloc(sizeof(NOD_B)); 
		(*radB)->st->key=multicai[(*radB)->key].fii[1];
		(*radB)->st->st=NULL;
		(*radB)->st->dr=NULL;
		p=(NOD_B *)malloc(sizeof(NOD_B)); 
		p=(*radB)->st;
		for(int i=1;i<multicai[(*radB)->key].nrFii;i++)
		{
			q=(NOD_B *)malloc(sizeof(NOD_B));
			q->key=multicai[(*radB)->key].fii[i+1];
			q->st=NULL;
			q->dr=NULL;
			p->dr=q;
			T2_multicai_binar1(&p);
			p=p->dr;
		}
	}
}


void pretty_print(NOD_B *r,int dim)
{
	if(r!=NULL)
	{
		
		printf("\n");
		for(int i=0;i<=dim;i++)
			printf(" ");
		printf("%d",r->key);
		pretty_print(r->st,dim+2);
		pretty_print(r->dr,dim);
	}
}


void afisare(int p[])
{
	//T1
	printf("\nreprezentare parinte:\n");
	for(int i=1;i<=n;i++)
		printf("%d ",p[i]);
	//T2
	printf("\n\nreprezentare multicai\n");
	for(int i=1;i<=n;i++)
	{
		printf("%d: ",i);
		for(int j=1;j<=multicai[i].nrFii;j++)
			printf("%d, ",multicai[i].fii[j]);
		printf("\n");
	}
	
}

int main()
{
	int p[20];
	citire(p);
	T1_parinte_multicai(p);
	afisare(p);
	rad_b=(NOD_B *)malloc(sizeof(NOD_B));
	rad_b->st=NULL;
	rad_b->dr=NULL;
	rad_b->key=rad;
	T2_multicai_binar1(&rad_b);
	printf("\nreprezentare binar\n");
	pretty_print(rad_b,0);
	getch();
	return 0;
}