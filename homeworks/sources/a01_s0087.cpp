#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <memory.h>


FILE *file; 
int *a, *b, *c;

void generare(int n){
  a=(int *)calloc(n,sizeof(int));
  b=(int *)calloc(n,sizeof(int));
  c=(int *)calloc(n,sizeof(int));
  for (int i=0; i<n; i++){
     a[i]=rand();
  }
  memcpy(b,a,n);
  memcpy(c,a,n);
}

void bubble(int n){
   int aux;
   int k=0, l=0;
   for (int i=0; i<n-1; i++){
	   if (a[i]<a[i+1]){
	        aux=a[i];
			a[i]=a[i+1];
			a[i+1]=aux;
			k+=3;
	   }
	   l++;
   }
   fprintf(file,"%d\t%d\t%d\t", n,l,k);
}

void insert(int n){
   int k=0, l=0;
   int x, xpoz;
   for (int i=0; i<n-1; i++){
      x=b[i];
	  xpoz=i;
	  while ((xpoz>0)&&(x>b[xpoz-1]))
	  {  b[xpoz]=b[xpoz-1];
	     xpoz=xpoz-1;
		 l++;
		 k++;
	   }
	  b[xpoz]=x;
	  l++;
   }

   fprintf(file,"%d\t%d\t", l,k);
}

void select(int n){
    int k=0, l=0;
	int min,aux;
	for (int i=0; i<n-1; i++){
	   min=i;  
	   for (int j=i+1; j<n-1; j++){
			if (c[j]<c[min])
			{min=j;
			 k++; }
	   }
	   aux=a[min];
	   a[min]=a[i];
	   a[i]=aux;
	   l+=3;
	}
    fprintf(file,"%d\t%d\n", l,k);
}

void main(){
file= fopen("bubble.txt", "w");
int i;
for (i=100; i<=10000; i+=100){
	generare(i);
	bubble(i);
	insert(i);
	select(i);
	free(a);
	free(b);
	free(c);
}
fclose(file);
}