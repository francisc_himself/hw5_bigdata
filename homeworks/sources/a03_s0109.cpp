#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int heapsize=0;
int a=0;
int c=0;

//returns left child
int left(int k){
	return 2*k;
}
//returns right child
int right(int k){
	return 2*k+1;
}
//returns parent node
int parent(int k){
	return k/2;
}

//swaps two integer values
void swap(int &a, int &b){
int aux=a;
a=b;
b=aux;
}

//heap sort bottom up----------------------------------------
/*method that takes a node and knowing that its two aubtrees are heaps creates
a new heap*/

void heapify(int A[],int i)
    {
	int largest=i;
    int l=left(i);
	int r=right(i);
	c++;
	if ((l<=heapsize) && (A[l]>A[i]))
		largest=l;
	else
		largest=i;
	c++;
     if ((r<=heapsize) && (A[r]>A[largest]))
		 largest=r;
	c++;
	 if (largest!=i)
		{a=a+3;
	     swap(A[largest],A[i]);
		 heapify(A,largest);
        }
}
/*method that builds a heap starting from its leaves, that are already heaps
and goes up to the root by calling heapify*/

void build_heap(int A[],int n)
{
	for(int i=n/2;i>0;i--)
	{
		heapify(A,i);
	}
}
/*method that sorts an array building a heap and using the heap property,
and reconstructs the heap at every step with heapify*/

void heap_sort(int A[], int n){
heapsize=n;
build_heap(A,n);
for(int i=n;i>=2;i--){
	swap(A[1],A[i]);
	a=a+3;
	heapsize--;
	heapify(A,1);
}
}



/* rearranges the sub-array A[p...r] in place */
int partition(int A[], int p, int r){
 a++;
 int x=A[r];
 int i=p-1;
for (int j=p;j<=r-1;j++)
{  c++;
   if (A[j]<=x)
   {
	i++;
	swap(A[i],A[j]);
	a=a+3;
   }
 }
swap(A[i+1],A[r]);a=a+3;

return i+1;
}

/* performs the quick sort algorithm on the array A[] */
void quick_sort(int A[],int p,int r){
	int q=0;
	if(p<r){
		q=partition(A,p,r);
		quick_sort(A,p,q-1);
		quick_sort(A,q+1,r);
	}
}

/* creates an array of random integers, with values between 0 and n */
void average(int *A, int n)
{
int i;
for(i=1;i<=n;i++)
A[i]=rand()%n;
}

void ordered(int *A, int n)
{
int i;
for(i=0;i<n;i++)
A[i]=i+1;
}

/*
b<-a;
*/
void copy_array(int a[], int b[], int n)
{
	for(int i=1;i<=n;i++)
		b[i]=a[i];
}

// main
int main()
{
FILE *f;
		int *A;
		int *B;
		int *C;
		f=fopen("heap_vs_quick.csv","w");

	for(int n=100;n<=10000;n=n+100)
	{

	fprintf(f,"%d,",n);
	A=new int[n];
	B=new int[n];
	C=new int[n];
	average(A,n);

	a=0;c=0;
	copy_array(A,B,n);
	heap_sort(A,n);
	fprintf(f,"%d, ",a+c);

	a=0;c=0;
	quick_sort(B,1,n);
	fprintf(f,"%d, ",a+c);
	ordered(C,n);
	a=0;c=0;
	//quick_sort(C,1,n);
	fprintf(f,"\n");
	
}

		int n=20;
		A=new int[n];
		B=new int[n];
		average(A,n);
		for(int i=1;i<=n;i++)
		printf("%d ",A[i]);
		printf("\n");
		copy_array(A,B,n);

		quick_sort(A,1,n);
		printf("Quicksort: ");
		for(int i=1;i<=n;i++)
		printf("%d ",A[i]);
		printf("\n");

		printf("Heapsort: ");
		heap_sort(B,n);
		for(int i=1;i<=n;i++)
		printf("%d ",B[i]);
		printf("\n");
		getch();
		fclose(f);
return 0;
}