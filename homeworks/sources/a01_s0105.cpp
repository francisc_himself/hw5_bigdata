#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#define _CRT_SECURE_NO_DEPRECATE

/*
Analysis of the 3 methods of direct sorting: Bubble Sort, Insertion Sort and Selection Sort
-measurements performed on values from 100 to 10000, with an increment of 100
1.Best case: input array is sorted
ASSIGNMENTS: The charts resulted from the measurements show very clear that in the best case Selection and Bubble Sort perform 0 assignments, 
			 being more efficient than Selection Sort. Insertion Sort perform between 198-19998 assignments
			 O(n) complexity - linear
COMPARISONS: Insertion Sort and Bubble Sort behave more efficiently in this case. Their growth is linear(O(n) complexity); Number of comparisons from 99-9999 for the both
			 Selection sort makes between 4950-49995000 comparisons, quadratic function(O(n^2) complexity)
OVERALL: Analysis of both comparisons and assignments gives: the most efficient algorithn that is Bubble sort performing
			a sum of comparisons and assigments of 99 to 9999 compared with the Insertion sort that gives a sum of 297 to 29 997.As a conclusion the
			worst behaving algorithm is the selection sort(4950 to 49995000 comparisons+ assignments)


2. Average case:
ASSIGNMENTS: Selection Sort performs the minimum of assignments: from 286-29967
			Then, Insertion Sort, from 2668-24873394 and Bubble sort from: 7411-74560189 assignments
COMPARISONS: Insertion sort performs from 2569-24863395 comparisons, then the second algorithm is Selection Sort with 4950-49995000
			and definetely the slowest in this case is Bubble sort with a number between 8850-98122186 comparisons
OVERALL: The slowest algorithm in this case is Bubble sort(16261-172682375 number of assignments+comparisons), then selection sort, being almost
		the same with insertion sort

3. Worst case: the array is reverse sorted
ASSIGNMENTS: Definetely the worst algorithm is Bubble Sort(1850-149985000), behaving as a quadratic function(O(n^2) complexity)
			Then Insertion Sort(O(n^2) complexity) and the Selection sort seems to have the smallest nr of assign.(297-29997)-linear function(O(n) complexity)
COMPARISONS: Most efficient is Selection Sort, then Inserton Sort and the last one is Bubble Sort
			O(n^2) complexity-quadratic functions
OVERALL: Again Seelection Sort is most efficient, then Insertion Sort, and the last is Bubble Sort


Conclusion: After the analysis of these 3 algorithms we can say that the most efficient one is Insertion sort, being a stable algorithm, 
			i.e. does not change the relative order of elements with equal keys, then Selection Sort, that is a linear and stable algorithm and 
			the worst is Bubble Sort that even if it behaves very efficiently in the best case, it is the worst in the avergae and worst case.



*/

/*
-time taken by the INSERTION-SORT depends on the input size
-in INSERTION-SORT, the best case occurs if the array is already sorted
-worst case: the input array is reverse sorted
-best case: the input array is sorted
*/
void insertionSort(long n, long *a,long *c, long A[])
{
	long i,j,buff;
	for(i=2;i<=n;i++)
	{
		(*a)++;
		buff=A[i];  //Insert A[i]  into the sorted sequence A[1,...i-1]
		j=i-1;
		while(A[j]>buff && j>0)
		{
			(*a)++;
			(*c)++;
			A[j+1]=A[j];
			j--;
		}
	(*a)++;
	(*c)++;  //When a for or while loop exits in the usual way, the test is executed one time more than the loop body
	A[j+1]=buff;
	}
}


/*
-first finding the smallest element of A and exchanging it with the element in A[1]. Then find the second smallest
element of A, and exchange it with A[2]. Continue in this manner for the first n1 elements of A
-linear algorithm, stable, generally performs worse than the similar insertion sort
-best calse: already sorted
-worst case: ex: 4321 43|21(half partitioned)
*/
void selectionSort(long n, long *a,long *c, long A[])
{
	long i,j,pos,aux;
	for(j=1;j<=n-1;j++)
	{
		pos=j;
		for(i=j+1;i<=n;i++)
		{
			(*c)++;
			if(A[i]<A[pos])
				pos=i;
		}
		if(j!=pos)
		{
			(*a)+=3;
			aux=A[j];
			A[j]=A[pos];
			A[pos]=aux;
		}
	}
	
}


/*
-inefficient, sorting algorithm; it works by repeatedly swapping adjacent elements that are out of order
-stable if we keep the condition A[i]>A[i+1]
-best case: the input array is sorted
-worst case: reverse order
*/

void bubbleSort(long n, long *a,long *c, long A[])
{
	int ok;
	long i,aux;
	do
	{
		ok=0;
		for(i=1;i<=n-1;i++)
		{
			(*c)++;
			if(A[i]>A[i+1])
			{
				(*a)+=3;
				aux=A[i];
				A[i]=A[i+1];
				A[i+1]=aux;
				ok=1;
			}
		}
	}while(ok==1);

}



int main()
{	
	//just testing the sorting algorithms on a small input
	/*
	long a=0,c=0,x[20],i, n;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d",&x[i]);
	for(i=1;i<=n;i++)
		//insertionSort(n,&a,&c,x);
		//selectionSort(n,&a,&c,x);
		  bubbleSort(n,&a,&c,x);
	for(i=1;i<=6;i++)
		printf(" %ld",x[i]);
	*/

	FILE *worstf,*bestf,*averagef;
	worstf=fopen("worst.txt","w");
	bestf=fopen("best.txt","w");
	averagef=fopen("average.txt","w");

	long as,ai,ab,cs,ci,cb,i,j,k,sumcb,sumci,sumcs,sumab,sumai,sumas;
	long outb[10001],outi[10001],outs[10001];
	long nr;

	//best case: the array is sorted
		for(i=100;i<=10000;i=i+100) //generating values from 100 to 10000, with an increment of 100
		{
			ai=0;as=0;ab=0;
			ci=0;cs=0;cb=0;
			for(j=1;j<=i;j++)
			{
				outb[j]=j;
			}
			bubbleSort(i,&ab,&cb,outb);
			insertionSort(i,&ai,&ci,outb);
			selectionSort(i,&as,&cs,outb);
			
			fprintf(bestf," %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,ab,cb,cb+ab,ai,ci,ci+ai,as,cs,cs+as);
		}


	//worst case	
	for (i=100;i<=10000;i=i+100)
	{
		cb=0;ci=0;cs=0;
		ab=0;ai=0;as=0;
		nr=1;
		for (j=i;j>=1;j--) //reverse order 
		{
			outb[nr]=j;
			outi[nr]=j;
			nr++;
		}

		
		for (j=1;j<i;j++) //different worst case for selection sort
			outs[j]=j+1;
		outs[i]=1;
		

		/*
		k=1;
		for(int j=i; j>i/2; j--)
		{   
			outs[k]=j;
			k++;
		}
		
		for(int j=1; j<=i/2; j++)
			{outs[k]=j;
			k++;
		}
		*/
		bubbleSort(i,&ab,&cb,outb);
		insertionSort(i,&ai,&ci,outi);
		selectionSort(i,&as,&cs,outs);
		fprintf(worstf," %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,ab,cb,cb+ab,ai,ci,ci+ai,as,cs,cs+as);
	}


	//average case: random values, same input sequence for all three sorting methods 
	 for (i=100;i<=10000;i=i+100)
	{
		sumcb=0;sumci=0;sumcs=0;
		sumab=0;sumai=0;sumas=0;
		for (j=1;j<=5;j++)  //we repeat the measurements 5 times
		{
			nr=0;
			cb=0;ci=0;cs=0;
			ab=0;ai=0;as=0;
			for (k=1;k<=i;k++)
			{
				nr++;
				outb[nr]=outi[nr]=outs[nr]=rand();
			}
			bubbleSort(i,&ab,&cb,outb);
			sumcb+=cb;
			sumab+=ab;
			insertionSort(i,&ai,&ci,outi);
			sumci+=ci;
			sumai+=ai;
			selectionSort(i,&as,&cs,outs);
			sumcs+=cs;
			sumas+=as;
		}
			fprintf(averagef," %ld %ld %ld %ld %ld %ld %ld %ld %ld %ld\n",i,sumab/5,sumcb/5,sumcb/5+sumab/5,sumai/5,sumci/5,sumci/5+sumai/5,sumas/5,sumcs/5,sumcs/5+sumas/5);
	}

	//getch();
	return 0;
}