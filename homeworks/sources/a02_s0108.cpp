#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>


long heapsize=0;
long ass=0;
long comp=0;
long a[10000];
long a2[10000];
long a3[10000];

long left(long k)
{
	return 2*k+1;
}
long right(long k)
{
	return 2*k+2;
}
long parent(long k)
{
	return (k-1)/2;
}

void swap(long &a, long &b)
{
    long aux=a;
    a=b;
    b=aux;
}

// building the heap with the bottom up technique.

void heapify(long *a, long i)
{
    long largest=i;
    long l=left(i);
    long r=right(i);

    comp++;

    if((l<heapsize)&&(a[l]>a[i]))
        largest=l;
    else largest=i;

    comp++;

    if((r<heapsize)&&(a[r]>a[largest]))
        largest=r;
    if(largest!=i)
    {
        ass=ass+3;
        swap(a[largest],a[i]);
        heapify(a,largest);
    }
}

//method used in buiding the heap from the bottom, at first it takes the leaves, that are already heaps,
// and calls the heapify method, and goes to the top of the tree.

void buildheap(long *a,long n)
{
	for(long i=n/2;i>0;i--)
	{
		heapify(a,i);
    }


}
/*
void build_heap_top_down(int *a, int n)
{
    int i;
    int k;
    for (i=1;i<n;i++)
    {
        heapsize = i;
        k=i;
        while (k>0 && a[k] > a[parent(k)])
        {
            comp++;
            ass=ass+3;
            swap(a[k],a[parent(k)]);
            k = parent(k);
        }
        comp++;
    }
}*/

//building the heap with the top down technique.

void increase_key(long *a,long i,long key){
	if(key<a[i]) printf("Error, new key smaller than current key");
	else{
		//assign the new value
		ass++;
        a[i]=key;
        /*compare the new value to the value of the parent and move up until find
        a place to place the new value*/
        comp++;
        while((i>1)&&(a[parent(i)]<a[i])){
            comp++;
            ass=ass+3;
            swap(a[parent(i)],a[i]);
            i=parent(i);
        }
	}
}

void max_heap_insert(long *a, long key){
	heapsize++;
	increase_key(a,heapsize,key);
}

void build_heap_top_down(long *a, long n){
	heapsize=1;
	for(long i=2;i<n;i++){
	max_heap_insert(a,a[i]);
	}
}

int main()
{   /*
    int a[20];
    int n;

    printf("n= \n");
    scanf("%d",&n);

    for(int i = 0; i<n; i++)
        scanf("%d",&a[i]);

    for(int i = 0; i<n; i++)
        printf("%d ", a[i]);


   // buildheap(a,n);
   */
    FILE *f = fopen("Avg.csv","w");
    long arrSize = 0;

    fprintf(f,"n ass_bu comp_bu sum_bu ass_td comp_td sum_td\n");
   // long a[10001];
   // long a2[10001];
   // long a3[10001];

    long abu,atd,cbu,ctd;
    long nr;

    for (long i=100;i<10000;i=i+100)
    {
        arrSize = i;

        abu = 0; cbu = 0; atd = 0; ctd = 0;
        for (long j=1;j<=5;j++)
		{
		    nr=0;
            for (long k=1;k<=i;k++)
			{

				nr++;
				a[nr]=rand();
			}
            for (long t=0;t<arrSize;t++)
		    {

		        a2[t] = a[t];
		        a3[t] = a[t];
		    }
            // average for bottom up
            ass =0;
            comp = 0;
            buildheap(a2,arrSize);
            abu = abu + ass;
            cbu = cbu + comp;

            //average for top down
            ass=0;
            comp=0;
            build_heap_top_down(a3,arrSize);
            atd = atd + ass;
            ctd = ctd + ass;

		}

		fprintf (f,"%ld %ld %ld %ld %ld %ld %ld\n",arrSize,abu/5,cbu/5,abu/5+cbu/5,atd/5,ctd/5,atd/5+ctd/5);
    }
}
