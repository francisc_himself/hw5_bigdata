#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
#include <math.h>

/*
In the avarage case the bottomup approach is much better than the top down upproach, both are linear but the buttom up rises slower. 
In the best case there are no associations in the bottomup approach, so in the sum of comparisons and associations it's again better than the top down approach.
In the worst case bottomup is still better, then top down. The second one loses its linearity.
Although the bottomup is better in every case, the top down is very usefull for building priority queues.
*/


int heapupa,heapupc,heapupsum,heapdowna,heapdownc,heapdownsum;
int m;
int loc;

void swap(int &a,int &b)
{
	int k;
	k=a;
	a=b;
	b=k;
}

void FillWorstArray(int a[])
{
	for (int i=0;i<m;i++)
	{
		a[i]=i;	
	}
}

void FillBestArray(int a[])
{
	for (int i=0;i<m;i++)
	{
		a[i]=m-i;	
	}
}

void printheap(int a[])
{
	int powtwo=1;
	int nr=m/2*2;
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=(nr+1)/powtwo;j++)
			printf(" ");
		printf("%d", a[i]);
		if(i%(2*powtwo-1)==0)
		{
			printf("\n");
			powtwo*=2;
		}
	}
	printf("\n\n");

}


void printarray(int a[])
{
	for (int i=1;i<=m;i++)
		printf(" %d",a[i]);
	printf("\n\n");
}


void heapify(int a[],int i)
{
	int largest=i;
	heapupc++;
	if (2*i<=loc && a[largest]<a[2*i])
		largest=2*i;
	heapupc++;
	if (2*i+1<=loc && a[largest]<a[2*i+1])
			largest=2*i+1;
	if (largest!=i)
	{
		heapupa+=3;
		swap(a[largest],a[i]);
		heapify(a,largest);
	}
}

void bottomUpBuildHeap(int a[])
{
	loc=m;
	for (int i=m/2+1;i>0;i--)
	{
		heapify(a,i);
	}
	
}

int popheap(int a[])
{
	int ret=a[1];
	heapdowna++;
	a[1]=a[loc];
	loc--;
	heapify(a,1);
	return ret;
}

void pushheap(int a[],int next)
{
	loc++;
	heapdowna++;
	a[loc]=next;
	int sw;
	int i=loc;
	do
	{
		sw=0;
		heapdownc++;
		if (a[i/2]<a[i])
		{
			swap(a[i/2],a[i]);
			heapdowna+=3;
			i=i/2;
			sw=1;
		}
	}
	while (sw==1 && i>1);
}

void topDownBuildHeap(int a[])
{
	loc=1;
	for (int i=2;i<=m;i++)
	{
		pushheap(a,a[i]);
	}
}

void heapSort(int a[])
{
	for (int i=m;i>=1;i--)
	{
		a[i]=popheap(a);
		heapify(a,1);
	}
}

void sort(int a[])
{
	//for (int i=0;i<5;i++){
	//FillRandomArray(a,m+1,0,100,false,0);
	FillBestArray(a);
	a[0]=0;
	//printf("unsorted array\n");
	//printarray(a);
	bottomUpBuildHeap(a);
	//printf("heap\n");
	//printheap(a);
	/*heapSort(a);
	printf("sorted array\n");
	printarray(a);
*/
	FillBestArray(a);
	//FillRandomArray(a,m+1,0,100,false,0);
	a[0]=0;
	//printf("unsorted array\n");
	//printarray(a);
	topDownBuildHeap(a);
	/*printf("heap\n");
	printheap(a);
	heapSort(a);
	printf("sorted array\n");
	printarray(a);*/

	//}
}



int main()
{
	int a[10000];
	FILE *pa=fopen("heapa.csv","w");
	FILE *pc=fopen("heapc.csv","w");
	FILE *ps=fopen("heaps.csv","w");
	fprintf(pa,"m\theapupa\theapdowna\n");
	fprintf(pc,"m\theapupc\theapdownc\n");
	fprintf(ps,"m\theapupsum\theapdownsum\n");
	for (m=100;m<101;m+=100)
	{
		heapupa=0;
		heapupc=0;
		heapupsum=0;
		heapdowna=0;
		heapdownc=0;
		heapdownsum=0;

		printf("%d\n",m);
		sort(a);
	
	fprintf(pa,"%d\t%d\t%d\n",m,heapupa,heapdowna);
	fprintf(pc,"%d\t%d\t%d\n",m,heapupc,heapdownc);
	heapupsum=heapupa+heapupc;
	heapdownsum=heapdowna+heapdownc;
	fprintf(ps,"%d\t%d\t%d\n",m,heapupsum,heapdownsum);
	}
	getch();
	return 0;
}