/*
***ANONIM*** Diana ***ANONIM*** ***GROUP_NUMBER***

 Din grafic se poate observa ca constructia Bottom-Up este mai eficienta
 decat cea Top-Down,deroarece numarul de com[aratii si atribuiri totale este 
 mai mic.Ambele metode de constructie au o crestere aproximativ liniara.
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "Profiler.h"
Profiler profiler ("lab3");

#define NEG_INF INT_MIN

int v[]={4,1,3,2,16,9,10,14,8,7};
int h_size,n;
int v1[]={4,1,3,2,16,9,10,14,8,7};
void afisare(int *v,int n,int k,int nivel)
{
	if(k>n) return;
	afisare(v,n,2*k+1,nivel+1);
		for(int i=1;i<=nivel;i++)
		{
			printf("   ");
		}
		printf("%d\n",v[k-1]);
	afisare(v,n,2*k,nivel+1);
}


void max_heapify(int *a,int i)
{
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
	profiler.countOperation("ac_bu", n,1);
	if ((l+1)<h_size && a[l+1]>a[i])
	{
		largest=l+1;
	}else largest=i;
	profiler.countOperation("ac_bu", n,1);
	if ( (r+1)<h_size && a[r+1]>a[largest])
	{
		largest=r+1;
	}
	if(largest!=i)
	{
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		profiler.countOperation("ac_bu", n,3);
        max_heapify(a,largest);
	}

}
void max_heap_BU(int *a,int length)
{
	h_size=length;
	for (int i=((length-1)/2);i>=0;i--)
	{
		max_heapify(a,i);
	}
}

void heapsort(int *a,int length)
{
	int aux;
	max_heap_BU(a,length);
	for(int i=length-1;i>=1;i--)
	{
		aux=a[0];
		a[0]=a[i];
		a[i]=aux;
		profiler.countOperation("ac_bu", length,3);
	h_size=h_size-1;
	max_heapify(a,0);
	}
}


void increase_key(int *a,int i,int key)
{
	int aux;
	profiler.countOperation("ac_td", n,1);
	if (key<a[i])
		perror("new key is smaller than current key");
	profiler.countOperation("ac_td", n,1);
	a[i]=key;
	profiler.countOperation("ac_td", n,1);
	while(i>0 && a[(i-1)/2]<a[i])
	{ 
		profiler.countOperation("ac_td", n,4);
		aux=a[i];
		a[i]=a[(i-1)/2];
		a[(i-1)/2]=aux;
		i=(i-1)/2;
	}
}
void max_heap_insert(int *a,int key)
{
	h_size=h_size+1;
	a[h_size]=NEG_INF;
	increase_key(a,h_size,key);
}

void build_max_heap_TD(int *a,int length)
{
	h_size=0;
	for (int i=1;i<length;i++)
	   max_heap_insert(a,a[i]);
}


void main()
	{
     int a[10000],a1[10000];
	 int m=10;
	for(int j=0;j<5;j++)
	 {
		 for (n=100;n<10000;n+=100)
	{
		FillRandomArray(a,n);

		memcpy(a1,a,n*sizeof(int));
		max_heap_BU(a,n);
		build_max_heap_TD(a1,n);

	 }
	}
		//heapsort(v,m);
	    //afisare(v,m,1,0);
	    
	profiler.createGroup("BuildingHeap","ac_bu","ac_td");
	profiler.showReport();
	max_heap_BU(v,m);
	    build_max_heap_TD(v1,m);
		afisare(v,m,1,0);
		afisare(v1,m,1,0);
	}