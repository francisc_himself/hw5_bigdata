/*
***ANONIM*** ***ANONIM*** Gabriel
Assignment 9 - Breadth First Search
Algoritmi fundamentali
Mai 2013

Algoritmul de cautare in latime este intens folosi tin grafuri. Are proprietatea ca la fiecare "pas" (un pas inseamna parcurgerea tuturor
vecinilor determinati la pasul anterior) o sa descopere un nou nivel de adancime a grafului fata de varful de start (origine).

Algoritmiul are complexitatea O(|V|+|E|), deoarece trebuie sa parcurga toate muchiile odata si toate varfurile odata.

Chiar daca graficele au fost facute cu masurarea timpului, se observa o tendinta de crestere a timpului odata cu cresterea atat a nodurilor
***ANONIM*** si a muchiilor.

*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include "Profiler.h"

Profiler profiler("BreadthFirst");

struct AdjacentNod{
	int neighbour;
	struct AdjacentNod *next;
}AdjacentNods;


#define NMIN 110
#define NMAX 211
#define NINC 10
#define EMIN 1000
#define EMAX 5001
#define EINC 100

AdjacentNod **vertices;
AdjacentNod *head,*tail;
int parent[NMAX+1];
int distance[NMAX+1];
int color[NMAX+1];

void creareVect(int n, int m){
	int i,j;
	int edjes[EMAX*2+1];
	int found;
	//printf("creare vect\n");
	AdjacentNod *p,*newn;
	vertices = (AdjacentNod**)malloc(sizeof(AdjacentNod)*(NMAX+1));
	for(i = 0; i < n; i++){
		vertices[i] = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		vertices[i]->neighbour = 0;
		vertices[i]->next = NULL;
	}
	for (i=0; i<m; i+=2){
		
		do{	
			found = 0;
			edjes[i]=rand() % (n-2);
			edjes[i+1]=edjes[i]+rand() % (n-edjes[i]-1)+1;
			for(j = 0; j<i; j+=2){
				if (edjes[i]==edjes[j] && edjes[i+1]==edjes[j+1]){
					found = 1;
				}				
			}			
		}while(found == 1);
		p = vertices[edjes[i]];
		while(p->next!=NULL){
			p = p->next;
		}
		newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		newn->next = NULL;
		newn->neighbour = 0;
		p->next = newn;
		p->neighbour = edjes[i+1];
		
		p = vertices[edjes[i+1]];
		while(p->next!=NULL){
			p = p->next;
		}
		newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
		newn->next = NULL;
		newn->neighbour = 0;
		p->next = newn;
		p->neighbour = edjes[i];
	}
	//printf("end creare vect\n");
}

void enqueue(int x){
	AdjacentNod *newn;
	newn = (AdjacentNod*)malloc(sizeof(AdjacentNod));
	newn->neighbour = x;
	newn->next = NULL;
		
	if(head == NULL){
		head = newn;
		tail = head;
	}
	else{
		tail->next = newn;
		tail = newn;
	}
}

int dequeue(){
	int toReturn;
	AdjacentNod *p;
	if(head == NULL){
		return -1;
	}
	else{
		toReturn = head->neighbour;
		p = head;
		head = head->next;
		free(p);
		return toReturn;
	}
}

void prettyPrint(int n){
	int i,j;
	for (i = 0; i < n; i++){
		for(j = 0; j < distance[i]; j++){
			printf("    ");
		}
		printf("%d\n",i);
	}
}

void BFS(int start,int n){
	//int i;
	AdjacentNod *v;
	int u;
	
	color[start] = 1;
	distance[start] = 0;
	parent[start] = -1;
	//printf("%d\n",start);
	head = NULL;
	tail = NULL;
	enqueue(start);
	while (head != NULL){
		u = dequeue();
		v = vertices[u];
		while(v->next!=NULL){
			if (color[v->neighbour]==0){
				color[v->neighbour] = 1;
				distance[v->neighbour] = distance[u] + 1;
		//		for(j = 0; j < distance[v->neighbour]; j++){
			//		printf("    ");
				//}
				//printf("%d\n",v->neighbour);
				parent[v->neighbour] = u;
				enqueue(v->neighbour);
			}
			v = v->next;
		}
		color[u] = 2;
	}
}

int main(){
	int n, e, i;
	n = 7;
	e = 15;
	creareVect(n,2*e);
	for(i = 0; i < n; i++){
		color[i] = 0;
		distance[i] = MAXINT;
		parent[i] = -1;
	}
	for(i = 0; i < n; i++){
		if(color[i]==0){
			BFS(i,n);
		}
	}
	n = NMIN;
	for (e = EMIN; e <= EMAX; e+=EINC){
		//printf("e %d\n",e);
		creareVect(n,2*e);
		profiler.startTimer("evariabil",e);
		for(i = 0; i < n; i++){
			color[i] = 0;
			distance[i] = MAXINT;
			parent[i] = -1;
		}
		//printf("initialize\n");
		for(i = 0; i < n; i++){
			if(color[i]==0){
				BFS(i,n);
		//		printf("BFS %d\n",i);
			}
		}
		profiler.stopTimer("evariabil",e);
		
	}
	//profiler.showReport();
//		getch();
		
	e = 5000;
	for(n = NMIN; n <= NMAX; n += NINC){
		//printf("n %d\n",n);
		creareVect(n,2*e);
		profiler.startTimer("nvariabil",n);
		for(i = 0; i < n; i++){
			color[i] = 0;
			distance[i] = MAXINT;
			parent[i] = -1;
		}
		for(i = 0; i < n; i++){
			if(color[i]==0){
				BFS(i,n);
			}
		}
		profiler.stopTimer("nvariabil",n);
	}
	profiler.showReport();

	//getch();
	//prettyPrint(n);
}