/******************************************
*                                         *
*             ***ANONIM*** ***ANONIM***                *
*             Group ***GROUP_NUMBER***                 *
*             Assignment #5               *
*             Search ***ANONIM*** insert into      *
*             hash tables                 *
*                                         *
******************************************/


#include <iostream>
#include <conio.h>
#include "Profiler.h"

using namespace std;



int m=10007;
int effortFound,effortNotFound;
int maxEffortFound, maxEffortNotFound;


int hash2(int k)
{
	return k%m;
}

int hashF(int k,int i)
{
	return (3*i+5*i*i+hash2(k)) % m;
}

int hashInsert(int A[], int k)
{
	int i=0,j;
	do
	{
		j=hashF(k,i);
		if(A[j]==NULL)
		{
			A[j]=k;
			return j;
		}
		else i++;
	}while(i!=m);
	return NULL;
}

int hashSearch(int A[], int k)
{
	int effort = 0;
	int i=0,j;
	do
	{
		j=hashF(k,i);
		effort++;
		if(A[j]==k)
		{
			effortFound += effort;
			if(effort > maxEffortFound)
				maxEffortFound = effort;
			return j;
		}
		i++;
		
	}while(A[j]!=NULL && i!=m);
	if(effort > maxEffortNotFound)
		maxEffortNotFound = effort;
	effortNotFound += effort;
	return NULL;
}

int main(void)
{	//test
	/*
	cin>>m;
	int B[100],A[100];
	for(int i=0;i<m;i++)
		B[i]=NULL;
	for(int i=0;i<m;i++)
	{	
		cin>>A[i];
		hashInsert(B,A[i]);
	}
	for(int i=0;i<m;i++)
		if(B[i]!=NULL)
			cout<<B[i]<<"  ";
	int k;
	cin>>k;
	cout<<endl;
	cout<<"elementul "<<k<<" este la pozitia "<<hashSearch(B,k);
	cout<<endl;
	*/
	//evaluation
	sr***ANONIM*** (time(NULL));
	int nMare=10007,nrOFElems=3000,B[10007],A[3000],z,index=0;
	int nrEl=0, aux=0;
	float fillingFactor=0;
	float alpha[]={0.8,0.85,0.9,0.95,0.99};
	for(int i=0;i<5;i++)
	{
		
		fillingFactor = 0;
		index=0;
		effortFound = 0;
		effortNotFound = 0;
		maxEffortFound = 0;
		maxEffortNotFound = 0;
		nrEl = nMare * alpha[i];
		
		
		for(int j=0;j<nMare;j++)
			B[j]=NULL;
		int counter = 1;
		for(int j=0;j<nrEl;j++)
		{
			z=(int) r***ANONIM***()%50000;
			hashInsert(B,z);
		}

		for(index=0; index<nrOFElems; index++)
		{		
			aux=B[r***ANONIM***()%m];
			if(aux != NULL){
				A[index]=aux;
				index++;
			}else {
				index--;
				continue;
			}
			A[index]=r***ANONIM***()%10000+50000;	
		}
		
		for(int j=0;j<index;j++)
			hashSearch(B,A[j]);
		cout<<"FillingFactor = "<<alpha[i]<<" || avg found = "<<(float)(2*effortFound)/index<<" || avg not found = "<<(float)(2*effortNotFound)/index<<endl;
		cout<<"Max Effort Found = "<<maxEffortFound<<" || Max Effort Not Found = "<<maxEffortNotFound<<endl;
		cout<<endl;
				
	
	}

	

	return 0;
}




