/*************
Nume:***ANONIM*** ***ANONIM*** ***ANONIM***
***ANONIM***:***GROUP_NUMBER***
Graficele celor doua metode de construire a unui heap cresc liniar.
Se poate observa de pe graficele rezultate ca metoda topDown de construire a unui heap are mai multe atribuiri
si comparatii decat metoda bottomUp.Acest lucru datorita faptului ca in metoda topDown de construire a heapului
vom insera mereu un element minim dupa care vom face o serie de comparatii si atribuiri in functie de elementul urmator care
trebuie inserat.In metoda bottomUp de construire a unui heap vom aveam putine comparatii si vom avea doar 3 atribuiri,
acelea fiind facute doar la interschimbarea elementelor pentru a putea satisface conditia de Heap.

************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#define MAX 10000
#define FIX 5
int heapSize;
int n=100;
int compbottom,atbottom,comptop,attop;
void inserareValori(int *a,int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%MAX;
	}
}


void afisareValori(int *a,int n)
{

	for(int i=0;i<n;i++)
	{
		printf("%d\n",a[i]);
	}
	printf("\n");
}



int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i+1;
}

int right(int i)
{
	
	return 2*i+2 ;
}


void maxHeapify(int *a,int i)
{
	int largest;
	int	l=left(i);
	int	r=right(i);
	compbottom=compbottom+2;
	
	if ((l<=heapSize) && (a[l]>a[i]))
	{
		largest=l;
	}
	else 
	{
		largest=i;
	}
	if ((r<=heapSize) && (a[r]>a[largest]))
	{
		largest=r;
	}
	if(largest!=i)
	{
		int x=a[i];
		a[i]=a[largest];
		a[largest]=x;
		atbottom=atbottom+3;
	
		maxHeapify(a,largest);
	}

}

void bottomUpHeap(int *a,int n)
{
	heapSize=n;
	for(int i=n/2;i>=0;i--)
	{
		maxHeapify(a,i);
	}
}

void heapIncreaseKey(int *a,int i,int key)
{
	comptop=comptop+2;
	if (key<a[i])
	{
		printf("Eroare");
		exit(0);
	}
	a[i]=key;
	attop=attop+1;
	while((i>0)&&(a[parent(i)]<a[i]))
	{	
		comptop=comptop+1;
		attop=attop+3;
		int x=a[i];
		a[i]=a[parent(i)];
		a[parent(i)]=x;
		i=parent(i);
	}
}

void maxHeapInsert(int *a,int key)
{
	heapSize=heapSize+1;
	a[heapSize]=INT_MIN;
	attop=attop+1;
	heapIncreaseKey(a,heapSize,key);
}
void topDownHeap(int *a,int n)
{
	heapSize=0;
	for(int i=1;i<n;i++)
		maxHeapInsert(a,a[i]);
}
void prettyPrint(int *a,int n,int i,int nivel)
{
	if(i<n){

	for(int j=0;j<nivel;j++)
		printf("\t");
	printf("%d\n",a[i]);
	prettyPrint(a,n,left(i),nivel+1);
	prettyPrint(a,n,right(i),nivel+1);
	}
}
void copiereValori(int *a,int *b,int n)
{
	for(int i=0;i<n;i++)
	{
		b[i]=a[i];
	}
}

void main()
{
	int tempcompb=0,tempatb=0,tempcompt=0,tempatt=0;
	FILE *f=fopen("Heapuri.csv","w");
	int m=10;
	int *c=(int*)malloc(sizeof(int)*m);
	inserareValori(c,m);
	afisareValori(c,m);
	bottomUpHeap(c,m);
	prettyPrint(c,m,0,0);
	free(c);
	int *d=(int*)malloc(sizeof(int)*m);
	inserareValori(d,m);
	afisareValori(d,m);
	topDownHeap(d,m);
	prettyPrint(d,m,0,0);
	free(d);
	
	for(n=100;n<=10000;n=n+100)
	{
	int *a=(int*)malloc(sizeof(int)*n);
	int *b=(int*)malloc(sizeof(int)*n);
	heapSize=n;
		for(int i=0;i<5;i++)
		{
			inserareValori(a,n);
			copiereValori(a,b,n);
			bottomUpHeap(a,n);
			topDownHeap(b,n);
		}
		
		printf("%d ",n);
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d\n",n,compbottom/FIX,comptop/FIX,atbottom/FIX,attop/FIX,(compbottom/FIX)+(atbottom/FIX),(comptop/FIX)+(attop/FIX));
		compbottom=0;
		atbottom=0;
		comptop=0;
		attop=0;
		free(a);
	}

}