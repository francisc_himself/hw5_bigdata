#include<iostream>
#include<conio.h>

using namespace std;
int V=6;

struct nod_lista_adj //structura pentru un nod lista de adiacenta a unui nod
{
	int nr_adj;//nodul adiacent nodului pt care se construieste lista
	nod_lista_adj *urm;//pointer la urmatorul nod din lista
};

struct nod //structura pentru un nod din graf
{
	nod_lista_adj *lista;//pointer la primul nod din lista sa de adiacenta
	int cul;//0=Alb, 1=Gri, 2=Negru
	int dist;//dist de la nod la radacina
	nod *p;
};

struct coada //structura pentru un element din coada(multimea nodurilor nevizitate)
{
	int nr;
	coada *urm;
};

void enq(coada **q,int i)
{
	if(*q==NULL)
	{
		*q=(coada*)malloc(sizeof(coada));
		(*q)->nr=i;
		(*q)->urm=NULL;
	}
	else
		enq(&(*q)->urm,i);
}


int deq(coada **q)//se scoate un element din coada
{
	if(*q!=NULL)
	{
		coada *aux=*q;
		int nr_aux;
		*q=(*q)->urm;
		nr_aux=aux->nr;
		free(aux);
		return nr_aux;
	}
	else
		return -1;
}

void bfs(nod *graf[],int V, int nod_start)
{
	nod_lista_adj *list;
	for(int i=1;i<=V;i++)
	{
		graf[i]->cul=0;
		graf[i]->dist=V;
		graf[i]->p=NULL;
	}
	graf[nod_start]->cul=1;
	graf[nod_start]->dist=0;
	coada *q=NULL;
	enq(&q,nod_start);
	while(q!=NULL)
	{
		int u=deq(&q);
		list=graf[u]->lista;
		while(list!=NULL)//parcurg nodurile din lista de adiacenta
		{
			if(graf[list->nr_adj]->cul==0)
			{
				graf[list->nr_adj]->cul=1;
				graf[list->nr_adj]->dist=graf[u]->dist+1;
				graf[list->nr_adj]->p=graf[u];
				enq(&q,list->nr_adj);
			}
			list=list->urm;
		}
		graf[u]->cul=2;
	}
}

void preety_print(nod *graf[], int V)
{
	for(int i=1;i<=V;i++)
	{
		for(int j=1;j<=graf[i]->dist;j++)
			cout<<" ";
		cout<<i<<endl;
	}
}

int main()
{	
	nod *graf[6];
	for(int i=1;i<=V;i++)
	{
		graf[i]=(nod*)malloc(sizeof(nod));
		graf[i]->cul=0;
		graf[i]->lista=NULL;
	}
	//initializez listele de adiacenta pt fiecare nod din graf=>generez muchiile
	graf[1]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[1]->lista->nr_adj=2;
	graf[1]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[1]->lista->urm->nr_adj=4;
	graf[1]->lista->urm->urm=NULL;

	graf[2]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[2]->lista->nr_adj=1;
	graf[2]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[2]->lista->urm->nr_adj=5;
	graf[2]->lista->urm->urm=NULL;

	graf[3]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[3]->lista->nr_adj=5;
	graf[3]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[3]->lista->urm->nr_adj=6;
	graf[3]->lista->urm->urm=NULL;

	graf[4]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[4]->lista->nr_adj=1;
	graf[4]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[4]->lista->urm->nr_adj=5;
	graf[4]->lista->urm->urm=NULL;

	graf[5]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[5]->lista->nr_adj=2;
	graf[5]->lista->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[5]->lista->urm->nr_adj=3;
	graf[5]->lista->urm->urm=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[5]->lista->urm->urm->nr_adj=4;
	graf[5]->lista->urm->urm->urm=NULL;

	graf[6]->lista=(nod_lista_adj*)malloc(sizeof(nod_lista_adj));
	graf[6]->lista->nr_adj=3;
	graf[6]->lista->urm=NULL;

	bfs(graf,V,2);
	preety_print(graf,V);
	getch();
	return 0;
}









