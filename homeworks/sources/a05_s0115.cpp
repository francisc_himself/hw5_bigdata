#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


int A[7]={5,8,14,7,3,4,57};
//int A[17];
int B[11];
#define m 11
#define N 10007
int H[N];
int apparition[10000];
int n; 
// Values: 8005 8505 9006 9506 9906
int generated[9906];
int access, maxeff, tempeff;

int hprime( int k)
{
	return k%N;
}


int h(int k, int i){
    int c1=1;
    int c2=1;

    int res=(hprime(k) + c1*i +c2*i*i) % N;
    return res;
}

int hashInsert( int T[], int k)
{
	
    int j;
    int i=0;
    do
    {
        j=h(k,i);
        if (T[j]==NULL)
        {
            T[j]=k;
            return j;
        }
        else i=i+1;
    } while (i!=N);
    printf("Hash table overflow");
    return 0;
}

int hashSearch( int T[], int k)
{
	tempeff=0;
    int j;
    int i=0;
    do
    {
        j=h(k,i);
		access++;
		tempeff++;
		if(tempeff>maxeff)
		{
			maxeff=tempeff;
		}
        if (T[j]==k)
            return j;
        i=i+1;

    }while((T[j]!=NULL) && (i!=N));
    return -1;
}


void main()

{
	/*
	srand(time(NULL));
	maxeff=0;
	
	
    for (int i=0; i<7; i++)
    {
        hashInsert(B,A[i]);
    }

    for (int i=0; i<11; i++)
    {
       // if (B[i]!=NULL)

        printf("%d - %d\n",i,B[i]);
    }
    printf("\n");
    printf("\n");

    printf("%d\n" ,hashSearch(B,14)); printf("%d\n" ,hashSearch(B,5));
    printf("%d\n" ,hashSearch(B,57)); printf("%d\n" ,hashSearch(B,6));

	*/
	


	//8005 8505 9006 9506 9906


		

	
    access=0;
	maxeff=0;

	
    for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=8005;


	printf("\nFilling factor 0.8 ->\n");

	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}
	
	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8005]);

	}
	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;


	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8005]+10001);
		

	}

	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;

	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=8505;
	printf("\nFilling factor 0.85 ->\n");
	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8505]);

	}
	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;


	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%8505]+10001);
		

	}

	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;

	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=9006;

	printf("\nFilling factor 0.9 ->\n");

	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9006]);

	}
	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;


	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9006]+10001);
		

	}

	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;


	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=9506;

	printf("\nFilling factor 0.95 ->\n");

	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9506]);

	}
	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;


	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9506]+10001);
		

	}

	printf("%d ", access/1500);
	printf("%d ", maxeff);

	access=0;
	maxeff=0;



	for(int i=0; i<10007; i++)
		H[i]=NULL;

	n=9906;
	printf("\nFilling factor 0.99 ->\n");
	for(int i=0; i<n; i++)
	{
		generated[i]=rand()%100000;
		hashInsert(H,generated[i]);
	}

	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9906]);

	}
	printf("%d ", access/1500);
	printf("%d ", maxeff);

	access=0;
	maxeff=0;


	for(int j=0; j<1500; j++)
	{
		hashSearch(H,generated[rand()%9905]+10001);
		

	}

	printf("%d ", access/1500);
	printf("%d ", maxeff);
	access=0;
	maxeff=0;
	
	
}