	

	#include <stdio.h>
	#include <conio.h>
	#include <queue>
	#include <cstdlib>
	#include <iostream>
	#include <fstream>
	#include <time.h>
	
	using namespace std;
	 

	#define INF 100000
	#define white 0
	#define gray 1
	#define black 2
	 
	FILE* f;

	struct Node{
		int color;
		int dist;
		Node* pred;
	};
	 
	struct NodeG
	{
		int key;
		Node* bfs;
		NodeG* adj[10000];
		int adjNr;
	};
	 
	 

	 
	int nodeNr = 1000;
	int edgeNr = 10000;
	int op;

	NodeG* vertices[10000];
	
	Node* bfsVert[10000];
	 
	/*
	void BFS(NodeG* s)
	{
		queue<NodeG*> q;
		
		for (int i=0; i<nodeNr;i++)
		{
			vertices[i]->bfs->color = white;
			vertices[i]->bfs->dist = INF;
			vertices[i]->bfs->pred = NULL;
		}
			
		s->bfs->color = gray;
		s->bfs->dist = 0;
		s->bfs->pred = NULL;
	 
		
		while (!q.empty()) q.pop();			//empty the queue
		q.push(s);
	 
		while (!q.empty())
		{
			NodeG* t = q.front();
					q.pop();
					//printf("\n%d popped\n",t->key);
			//for each vertex adj to t
			for (int j=0; j<t->adjNr; j++)
			{

				NodeG* v = t->adj[j];
				if (v->bfs->color == white)
				{
					v->bfs->color = gray;
					v->bfs->dist = t->bfs->dist + 1;
					v->bfs->pred = t->bfs;
					q.push(v);
				}

			}
			t->bfs->color = black;
		}

	}

	*/


	void BFS(NodeG* s)
	{
		queue<NodeG*> q;
		
		if(s->bfs->color!=black){
			
		s->bfs->color = gray;
		s->bfs->dist = 0;
		s->bfs->pred = NULL;
		op+=3;
		
		while (!q.empty()) q.pop();			
		q.push(s);
		op+=2;
		while (!q.empty())
		{
			NodeG* t = q.front();
					q.pop();
					op+=2;
					printf("%d ",t->key);
			//for each vertex adj to t
			for (int j=0; j<t->adjNr; j++)
			{
				op+=2;
				NodeG* v = t->adj[j];
				if (v->bfs->color == white)
				{
					v->bfs->color = gray;
					v->bfs->dist = t->bfs->dist + 1;
					v->bfs->pred = t->bfs;
					q.push(v);
					op+=4;
				}

			}
			t->bfs->color = black;
			op++;
		}
		}

	}
	 
	void graphRand()
	{

	for(int i=0; i<nodeNr; i++) 
		{
			vertices[i] = new NodeG();
			vertices[i]->key = i;
			vertices[i]->adjNr = 0;
			vertices[i]->bfs = new Node();

		}
	 
	for(int i=0; i<edgeNr; i++)
	{ 
		
		int redo,a,b;

	do
	{

		redo = 0;
		a = rand()%nodeNr;
		do 
			{
				b = rand()%nodeNr;

			} while (a==b);
	 
			
	 
		for(int j=0; j<vertices[a]->adjNr; j++)
		{
			if (vertices[a]->adj[j] == vertices[b])
			{

							redo = 1; 
			}

		}
	 
/*		for(int j=0; j<vertices[b]->adjNr; j++)
			{
					if (vertices[b]->adj[j] == vertices[a])
					{
							redo = 1; 
					}

			}
*/
	 
	} while (redo == 1);
	 
		vertices[a]->adj[vertices[a]->adjNr]=vertices[b];
		vertices[a]->adjNr++;

		}
		   
	}
	 

	void print(NodeG* root, int height)
	{

	printf("\n");

	if (root!=NULL)
		{

			for (int i=0; i<height; i++)
			{
				printf("  ");
			}

		printf("%d", root->key);
	 
		for(int i=0; i<root->adjNr; i++)
			{
				print(root->adj[i],height+1);
	 
			}

		}
	}
	
	 
	 
	int main()
	{
		srand(time(NULL)); 


		nodeNr = 100;
		edgeNr = 4900;

	f=fopen("out.txt", "w");

		   
	///*	
	for(int i=0; i<nodeNr; i++) 
		{
			vertices[i] = new NodeG();
			vertices[i]->key = i;
			vertices[i]->adjNr = 0;
			vertices[i]->bfs = new Node();
		}
	//	*/
	 
	

	//	/*
			vertices[0]->adj[0] = vertices[1];
			vertices[0]->adjNr++;

			vertices[1]->adj[1] = vertices[2];
			vertices[1]->adj[0] = vertices[3];
			vertices[1]->adjNr=2;

			vertices[2]->adj[0]=vertices[4];
			vertices[2]->adj[1]=vertices[9];
			vertices[2]->adjNr=2;

			vertices[3]->adj[0]=vertices[5];
			vertices[3]->adjNr++;

			vertices[6]->adj[0]=vertices[8];

			vertices[0]->adj[1]=vertices[10];
			vertices[0]->adjNr++;

			vertices[10]->adj[0]=vertices[11];
			vertices[10]->adjNr++;
			
			print(vertices[0],0);

	//	*/

	//	/*
		for (int i=0; i<nodeNr;i++)
		{
			vertices[i]->bfs->color = white;
			vertices[i]->bfs->dist = INF;
			vertices[i]->bfs->pred = NULL;
		}

	//	*/
	//	nodeNr = 10;
	//	edgeNr = 9;
	//	graphRand();
	//	print(vertices[0],0);
		printf("\n\n");
		BFS(vertices[0]);

	// FIRST TEST
	/*

	fprintf(f, "edges,op\n");
	for(edgeNr=1000; edgeNr<5000; edgeNr+=100)
	{
		
		graphRand();
		op=0;
		for (int i=0; i<nodeNr;i++)
		{
			vertices[i]->bfs->color = white;
			vertices[i]->bfs->dist = INF;
			vertices[i]->bfs->pred = NULL;
		}


		for (int i=0; i<nodeNr; i++)
		{
			BFS(vertices[0]);
		}
		fprintf(f, "%d,%d\n",edgeNr,op);
		printf("%d,%d\n",edgeNr,op);

	}

	*/
		
	//SECOND TEST

	/*
	nodeNr = 100;
	edgeNr = 9000;
	fprintf(f, "nodes,op\n");
	for(nodeNr=100; nodeNr<200; nodeNr+=10 )
	{
		
		graphRand();
		op=0;
		for (int i=0; i<nodeNr;i++)
		{
			vertices[i]->bfs->color = white;
			vertices[i]->bfs->dist = INF;
			vertices[i]->bfs->pred = NULL;
		}


		for (int i=0; i<nodeNr; i++)
		{
			BFS(vertices[i]);
		}
		fprintf(f, "%d,%d\n",nodeNr,op);
		printf("%d,%d\n",nodeNr,op);

	}

	*/

		return 0;
	}

