/*

***ANONIM*** ***ANONIM***-L , ***GROUP_NUMBER***

Am testat 3 cazuri diferite:primul caz cu un vector aleator,al doilea unul gata sortat si la al treilea unu sortat in ordine inversa.

In cazul in care vectorul a fost generat aleator cel mai prost s=a comportat bubble sortul. A facut de aproape de 4 ori mai multe asignari 
si comparari ca celalte 2.


In cazul in care vectorul a fost generat crescator,ca si un vector sortat cel mai bine sa comportat bubble sortul facand mai putin de 1% 
din operatile facute de insertion si selection sort.



In cazul in care vectorul a fost generat descrescator adica in ordine inversat cel mai bine s-a comportat insertion sort-ul,dupa aceea 
selection sortul si cel mai prost bubble sort-ul.

In concluzie cel mai bine se comporta insertion sortul si cel mai prost bubble sort-ul.
*/

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

int asi,coi,asb,cob,ass,cos;
void bubble(int *a,int n)
	{	
		int ok=1;
		int i;
		int c;
		 asb=0;
		 cob=0;
		while (ok==1)
			{	
				ok=0;
				for (i=0; i<n-1; i++)
				{
					cob++;
					if (a[i]>a[i+1]) 
					{
					c=a[i];
					a[i]=a[i+1];
					a[i+1]=c;
					ok=1;
					asb=asb+3;
					}
				}
			}
		
	}



void insertion(int *a,int n)
	{
		int i,c,j,k;
		 asi=0;
		 coi=0;
		for(i=1;i<n;i++)
			{
			c=a[i];
			j=0;
			coi++;
			asi=asi+2;
				while (a[j]<c)
					{
						j++;
						asi++;
					}
				for(k=i;k>j;k--)
					{
						a[k]=a[k-1];
						asi++;
						
					}
				a[j]=c;
				asi++;
			}




	}

void selectia(int *a,int n)
	{
		int i,min,j,c;
		 ass=0;
		 cos=0;
		for(i=0;i<n-1;i++)
			{
				min=i;
				ass++;
				for(j=i+1;j<n;j++)
					{
						cos++;
						if(a[j]<a[min])
							{
								min=j;
								ass++;
							}
							
							
					}
				c=a[min];
				a[min]=a[i];
				a[i]=c;
				ass=ass+3;
			}

	}


void afisare(int *a,int n)
{
	int i;
for(i=0;i<n;i++)
		{
		printf("%d,",a[i]);
		}
	
	
}

void random_vect(int *a,int n)
	{
		int i;
		for(i=0; i<n; i++)
		{
		a[i]=rand()%7500;
		}
	}
	void random_vect_up(int *a,int n)
	{
		int i;
		for (i=0;i<n;i++)
		{
		a[i]=i;
		}
	
	
	}

	void random_vect_down(int *a,int n)
	{
		int i;
		for (i=0;i<n;i++)
		{
		a[i]=5000-i;
		}
	
	
	}


int main()
{
	
	int n=5;
	int test[]={1,2,5,4,3};
	int test1[]={1,3,5,2,4};
	int test2[]={1,3,5,2,4};
	//selectia(test,n);
	//afisare(test,n);
	insertion(test1,n);
	afisare(test1,n);
	//bubble(test2,n);
	//afisare(test2,n);


	/*int a[5000];
	
	


	FILE *f=fopen("afisare.csv","w");


	fprintf(f,"bubble sort(vector aleator)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect(a,i);
	bubble(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",asb);
	fprintf(f,"%d,",cob);
	fprintf(f,"%d,",asb+cob);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");

	
	
	fprintf(f,"insertion sort(vector aleator)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect(a,i);
	insertion(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",asi);
	fprintf(f,"%d,",coi);
	fprintf(f,"%d,",asi+coi);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");

	
	
	fprintf(f,"selection sort(vector aleator)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect(a,i);
	selectia(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",ass);
	fprintf(f,"%d,",cos);
	fprintf(f,"%d,",ass+cos);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");









	fprintf(f,"bubble sort(vector crecator)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect_up(a,i);
	bubble(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",asb);
	fprintf(f,"%d,",cob);
	fprintf(f,"%d,",asb+cob);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");


	fprintf(f,"insertion sort(vector crescator)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect_up(a,i);
	insertion(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",asi);
	fprintf(f,"%d,",coi);
	fprintf(f,"%d,",asi+coi);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");

	fprintf(f,"selection sort(vector crescator)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect_up(a,i);
	selectia(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",ass);
	fprintf(f,"%d,",cos);
	fprintf(f,"%d,",ass+cos);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");

	fprintf(f,"bubble sort(vector descresc)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect_down(a,i);
	bubble(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",asb);
	fprintf(f,"%d,",cob);
	fprintf(f,"%d,",asb+cob);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");


	fprintf(f,"insertion sort(vector descresc)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect_down(a,i);
	insertion(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",asi);
	fprintf(f,"%d,",coi);
	fprintf(f,"%d,",asi+coi);
	fprintf(f,"\n");
	}

	fprintf(f,"selection sort(vector descresc)\n");
	fprintf(f,"nr. elemente");
	fprintf(f,"asignari,");
	fprintf(f,"comparari,");
	fprintf(f,"asignare+comparatie\n");
	for(int i=100;i<=5000;i=i+100)
	{
	random_vect_down(a,i);
	selectia(a,i);
	fprintf(f,"%d,",i);
	fprintf(f,"%d,",ass);
	fprintf(f,"%d,",cos);
	fprintf(f,"%d,",ass+cos);
	fprintf(f,"\n");
	}
	fprintf(f,"\n");

fclose(f);
return 0;

*/
}

	
	





