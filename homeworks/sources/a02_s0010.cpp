// Assign2_***ANONIM***_***GROUP_NUMBER***.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// Tema_2_heap.cpp : Defines the entry point for the console application.
//

/*
Se cere implementarea corecta ?i eficienta a doua metode de construire a structurii de
date Heap  �de jos �n sus� (bottom-up) si  �de sus �n jos� (top-down).
Observatii: eficienta: top down: O(nlog2(n))
					   bottom up: O(n)
Folosim metoda TopDown pentru insertii, iar in rest BottomUp din cauza eficientei.
Din grafic observam ca metoda de construire bottom-up este mai eficienta decat cel top-down, in cazul mediu statistic
*/


//#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
int n,dim;

long buComp, buAtrib;
long tdComp, tdAtrib;
int parent (int i)
{
	return (i/2);
}

int left (int i)      
{
	return 2*i;
}

int right(int i)
{
	return (2*i)+1;
}

void initHeap(int a[])
{
	dim=0;
}

void heapify(int a[], int i) {
  
  int l = left(i);
  int r = right(i);
  int ok,temp;
  buComp++;
  if (  (l <= n) && (a[l] > a[i]))
  {
    ok = l;
  }
  else {
    ok = i;
  }
  buComp++;
  if ( (r <= n) && (a[r] > a[ok]))
  {
    ok = r;
  }
  if (ok != i)
  {
	 buAtrib++;
     temp = a[i];
    a[i] = a[ok];
    a[ok] = temp;
    heapify(a, ok);
  }
}

void BuildHeapBU(int a[])
{
	int i;
	buComp=0;
	buAtrib=0;
	for(int i=n/2; i>0; i--)
	{
		heapify(a,i);
	}
}

void heapInsert(int a[],int key){ //heapPush
	dim++;
	tdAtrib++;
	a[dim]=key;
	int i=dim;
	tdComp++;
	while(i>1 && a[parent(i)]>a[i])
	{	tdComp++;
		tdAtrib++;
		int aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}

}

void BuildHeapTD(int b[], int a[])
{
	tdComp=0;
	tdAtrib=0;
	initHeap(b); //sau, mai simplu, dim=0
	for(int i=1; i<=n;i++)
	{
		heapInsert(b,a[i]);
	}
}

void Preety_print(int *v,int k,int nivel)  
{
	int i;
	if (k>n)
		return;
	Preety_print(v,2*k+1,nivel+1);
		for(i=1; i<=nivel;i++)
			printf("  ");
		printf("%d\n",v[k]);
	Preety_print(v,2*k,nivel+1);
}

void Construire()
{
	int a[10001]; 
	int b[10001]; //folosit la metoda TD
	int c[10001];
	int auxAtrib[2];
	int auxComp[2];

	FILE *f=fopen("D:\\rezultat.csv", "w");
	
	fprintf(f, "n, bu_atrib+comp, td_atrib+comp \n"); //capul de tabel
	
	for (n=100; n<=10000; n=n+100)
	{
		auxAtrib[0]=0;
		auxAtrib[1]=0;
		auxComp[0]=0;
		auxComp[1]=0;

		printf("%d \n",n);
	
		for (int m=1; m<=5; m++)
		{
			int i;
			srand( time (NULL));
			for(i=1; i<=n;i++)
			{
				a[i]=rand()%100;
				c[i]=a[i];
			}

		BuildHeapBU(c);
		auxAtrib[0]+= buAtrib;
		auxComp[0]+= buComp;

		BuildHeapTD(b,a);
		auxAtrib[1]+= tdAtrib;
		auxComp[1]+= tdComp;
		}

		fprintf (f, "%d, %d,%d\n ", n, (auxAtrib[0]+auxComp[0])/5 , (auxAtrib[1]+auxComp[1])/5 );
	}
}

int main()
{
	/*vector folosit pentru a verifica corectitudinea*/
	int test1[8];
	int test[8];
	test1[1]=20;test1[2]=15;test1[3]=1;test1[4]=3;test1[5]=5;test1[6]=23;test1[7]=65;test1[8]=40;n=7;
	int test2[8];
	test[1]=20;test[2]=15;test[3]=1;test[4]=3;test[5]=5;test[6]=23;test[7]=65;test[8]=40;n=7;
	
	BuildHeapBU(test);
	Preety_print(test,1,1);
	BuildHeapTD(test2, test1);
	Preety_print(test1,1,1);

	Construire();
	getch();
	return 0;
}
