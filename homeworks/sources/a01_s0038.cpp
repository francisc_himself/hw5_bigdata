/* Tema1: ***ANONIM*** ***ANONIM***, gr.***GROUP_NUMBER***

Cazul favorabil: 
-atribuiri:Buble: nicio atribuire
		   Insertie: 2*n
		   Selectie: 3*n, cele mai multe atribuiri
-comparatii:Buble:n, cele mai putine comparatii
			Insertie:n, cele mai multe comparatii
			Selectie:
-atribuiri+comparatii:Buble:n 
					  Insertie:n^2
					  Selectie:n^2
Buble este cel mai optim pentru cazul favorabil avand o complexitate de O(n) in timp ce Selectia si Inseria au O(n^2)

Cazul mediu statistic si cazul defavorabil au aproximativ aceeasi comportare: 
-atribuiri:Buble: n^2 cele mai multe atribuiri
		   Insertie:~ n^2 cele mai putine atribuiri(la defavorabil aprox n/2);la defavorabil mai putine decat la mediu statistic
		   Selectie:~ 3*n, atribuiri mai putine in cazul defavorabil
-comparatii:Buble:n^2 cele mai multe comparatii
			Insertie:n^2 
			Selectie:n^2
-atribuiri+comparatii:Buble:n^2 cele mai multe comparatii
					  Insertie:n^2 cele mai putine comparatii
					  Selectie:n^2 
Desi toate cele 3 metode au complexitatea O(n^2), buble este cel mai putin optim in cazul mediu statistic si defavorabil. 
Metodele de selectie si insertie au aproape accelasi numar de operatii efectuate.

*/
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <cstdlib>
#include <stdlib.h>

int atrBuble=0,compBuble=0,atrInsr=0,compInsr=0,atrSel=0,compSel=0;

void afisare(int n,int v[])
{
	for(int i=0;i<n;i++)
		printf("%d ",v[i]);
}


void bubleSort(int v[],int n)
{
	int ok=0,aux;
	while(!ok)
	{
		ok=1;
		for(int i=0;i<n-1;i++)
		{	
			compBuble++;
			if(v[i]>v[i+1])
			{
				atrBuble+=3;
				ok=0;
				aux=v[i];
				v[i]=v[i+1];
				v[i+1]=aux;
			}
		}
	}
}

void selectie(int v[],int n)
{
	int imin,aux;
	for(int i=0;i<n-1;i++)
	{
		imin=i;
		for(int j=i+1;j<n;j++)
		{
			compSel++;
			if(v[j]<v[imin])
				imin=j;
		}
		aux=v[i];
		v[i]=v[imin];
		v[imin]=aux;
		atrSel+=3;
	}
}

void inserare(int v[],int n)
{
	int x,j;
	for(int i=1;i<n;i++)
	{
		x=v[i];
		j=0;
		atrInsr++;
		compInsr++;
		while(v[j]<x)
		{
			j++;
			compInsr++;
		}
		for(int k=i;k>j+1;k--)
		{
			atrInsr++;
			v[k]=v[k-1];
			
		}
		v[j]=x;
		atrInsr++;
	}
}



void main()
{
	int n,v[10000],x[10000],y[10000];
	//FILE *f=fopen("mediuNou.csv","w");
	//FILE *f=fopen("defavorabil.csv","w");
	FILE *f=fopen("favorabil.csv","w");

	for(n=100;n<=10000;n+=200)
	{
		
		atrBuble=0;
		compBuble=0;
		atrInsr=0;
		compInsr=0;
		atrSel=0;
		compSel=0;
		//for(int k=1;k<=5;k++)
		//{
			//srand(time(NULL));
			
			for(int j=0;j<n;j++)//generare sir
			{
				v[j]=j; //caz best
				//v[j]=n-j;//caz worst
				//v[j]=rand();
				x[j]=j;
				y[j]=j;
				
			}
			//srand(1);
		
			bubleSort(v,n);
			inserare(x,n);
			selectie(y,n);
		//}
			//caz mediu
			//fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,atrBuble/5,atrInsr/5,atrSel/5,compBuble/5,compInsr/5,compSel/5,(atrBuble+compBuble)/5,(atrInsr+compInsr)/5,(atrSel+compSel)/5);
			//caz favorabil si defavorabil
			fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,atrBuble,atrInsr,atrSel,compBuble,compInsr,compSel,(atrBuble+compBuble),(atrInsr+compInsr),(atrSel+compSel));
	}
	getch();

}
