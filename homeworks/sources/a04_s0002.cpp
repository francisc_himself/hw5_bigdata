/****ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER***
	Assign4_MergeKLists
	
	Eficienta algoritmului de interclasare a k liste este O(n*log(k)) unde n este numarul de elemente totale si k numarul de liste
	Cand k este fixat odata cu cresterea lui n creste si numarul de operatii aproape liniar,
	iar cand n este fixat si k variabil numarul de operatii creste in mod logaritmic
	*/

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
//#include "Profiler.h"

typedef struct NOD
{
	int val;
	struct NOD *next;
}NOD;

NOD *head[1000],*tail[1000];
NOD *joinedHead,*joinedTail;

struct heapElement
{
	int list_id;
	NOD *node;
};

heapElement heap[1000];

int n,k,heap_dim,comp,asig;

//afisare lista
void showList(NOD *head,NOD *tail)
{
    NOD *p;
	if(head==NULL)
        printf("\nLista este vida!\n");
    else
	{	
		p=head;
        while(p!=tail)
        {
            printf("%d ",p->val);
            p=p->next;
        }

        printf("%d \n",tail->val);
    }
}

//inserare in lista
void insert(NOD**head,NOD **tail,int k)
{
    NOD *p;
    p=(NOD *)malloc(sizeof(NOD));
	p->val=k;

    if(*head==NULL)
    {
        *head=p;
        *tail=p;
        p->next=NULL;
    }
    else
    {
		p->next=NULL;
        (*tail)->next=p;
        *tail=p;
    }
}

//stergere lista
void delete_list()
{
    NOD *p;
    while( joinedHead != NULL) 
    {
        p=joinedHead;
        joinedHead=joinedHead->next;
        free(p); 
        /*eliberare spa�iu de memorie */
    }
    joinedTail=NULL;
}

//extragere primul element
NOD *extragere(NOD *head,NOD *tail)
{
	NOD *x;
	if(head!=NULL)
	{
		x=head;
		head=head->next;
		return x;
	}
	return NULL;
}

int p(int i)
{
	return i/2;
}


int dr(int i)
{
	return 2*i+1;
}

int st(int i)
{
	return 2*i;
}

void ReconstituieHeap(heapElement A[], int i, int n)
{
	int s,d,ind;
	int aux1;
	NOD *aux;
	s=st(i);
	d=dr(i);
	comp+=2;
	if((s<=n) && (A[s].node->val<A[i].node->val))
		ind=s;
	else
		ind=i;
	if((d<=n) && (A[d].node->val<A[ind].node->val))
	{
		ind=d;
	}
	if(ind!=i)
	{
		asig+=3;
		aux=A[i].node;
		A[i].node=A[ind].node;
		A[ind].node=aux;
		aux1=A[i].list_id;
		A[i].list_id=A[ind].list_id;
		A[1].list_id=aux1;
		ReconstituieHeap(A,ind,n);
	}
}

void construireHeap_BU(heapElement A[], int n)
{
	for(int i = n/2; i>0; i--)	
		ReconstituieHeap(A, i, n);
}

void creareHeap(heapElement A[], NOD *head[])
{
    NOD *aux;
    int i;
    for(i = 1; i <= heap_dim; i++)
	{
		asig++;
        A[i].node = head[i];
		A[i].list_id=i;
        head[i] = head[i]->next;
    }
    construireHeap_BU(A,heap_dim);
}

void generareListe(NOD **head, NOD **tail,int dim)
{
	int x=0;
	for(int i=1;i<=dim;i++)
	{
		x=x+rand()%101;
		insert(&(*head),&(*tail),x);
	}
}

void interclasare(heapElement A[], NOD *head[])
{
    int i;
    NOD *aux;
    while(heap_dim != 0)
	{
		insert(&joinedHead,&joinedTail,A[1].node->val);
		i = A[1].list_id;
        comp++;
        if (head[i] != NULL)
		{
			A[1].node = head[i];
            head[i] = head[i]->next;
			asig++;
        }
		else
		{
			asig++;
			A[1] = A[heap_dim];
			heap_dim--;
		}
        ReconstituieHeap(A,1,heap_dim);
    }
}

void main()
{
	int list_dim,i;
	FILE *f;
	f = fopen("liste_n.csv","w");
	srand(time(NULL));
	printf("n=");
	scanf("%d",&n);
	printf("k=");
	scanf("%d",&k);
	list_dim=n/k;
	for(i=1;i<=k;i++)
	{
		generareListe(&head[i],&tail[i],list_dim);
		showList(head[i],tail[i]);
	}
	heap_dim=k;
	creareHeap(heap,head);
	printf("\n");
	interclasare(heap,head);
	showList(joinedHead,joinedTail);
	delete_list();
	getch();
	for(n=100; n<=10000; n=n+100)
	{
        asig = 0;
        comp = 0;
        fprintf(f, "%d,", n);
        k = 5;
		list_dim=n/k;
        for(i=1;i<=k;i++)
			generareListe(&head[i],&tail[i],list_dim);
		heap_dim=k;
        creareHeap(heap,head);
        interclasare(heap,head);
		delete_list();
        fprintf(f, "%d,",asig+comp);
        asig = 0;
        comp = 0;
        k = 10;
		list_dim=n/k;
        for(i=1;i<=k;i++)
			generareListe(&head[i],&tail[i],list_dim);
		heap_dim=k;
        creareHeap(heap,head);
        interclasare(heap,head);
		delete_list();
        fprintf(f, "%d,",asig+comp);
        asig=0;
        comp=0;
        k = 100;
		list_dim=n/k;
        for(i=1;i<=k;i++)
			generareListe(&head[i],&tail[i],list_dim);
		heap_dim=k;
        creareHeap(heap,head);
        interclasare(heap,head);
		delete_list();
        fprintf(f, "%d\n",asig+comp);
        printf("n=%d\n",n);
    }
    fclose(f);
	f = fopen("liste_k.csv","w");
	
	for(k=100;k<=500;k+=10)
	{
		n=10000;
		list_dim=n/k;
		asig=0;
        comp=0;
		for(i=1;i<=k;i++)
			generareListe(&head[i],&tail[i],list_dim);
		heap_dim=k;
        creareHeap(heap,head);
        interclasare(heap,head);
		delete_list();
		fprintf(f, "%d,%d\n", k,asig+comp);
		printf("k=%d\n",k);
	}
	fclose(f);
	getch();
}