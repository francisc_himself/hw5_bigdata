/*
Nume : ***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa : 30226
Tema 3
Am avut de realizat Heap Sort si Quick Sort 
Complexitatea pentru cazul favorabil este : O(n) 
Pentru cazul mediu quick sort este net mai eficient

*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

int natd, nabu, nahs, naq;   //na = nr atribuiri (td=top-down, bu=bottom-up, hs=heapsort, q=quick sort)
int nctd, ncbu, nchs, ncq;	 //nc = nr comparatii


void randw(int a[10001],int n)
{
	
	a[1]=rand()*10;
	for(int i=2;i<=n;i++)
		a[i]=a[i-1]-rand()%10;

}

void randb(int a[10001],int n)
{
	a[1]=rand()%100;
	for(int i=2;i<=n;i++)
		a[i]=a[i-1]+rand()%10;
}
int parinte (int i)
{
	return i/2;
}

int st (int i)
{
	return 2*i;
}

int dr (int i)
{
	return 2*i+1;
}

//BOTTOM UP
void reconstructie_heap (int a[10001],int n, int i, int dim_heap)
{
	int s,d,ind,aux;

	s = st (i);
	d = dr (i);

	ind = i;

	if (s<=dim_heap && a[s]<a[ind])
		ind = s;
	ncbu++;
	nchs++;
	if (d<=dim_heap && a[d]<a[ind])
			ind = d;
	ncbu++;
	nchs++;

	if (ind != i)
	{
		nabu+=3;
		nahs++;
		aux = a[i];
		a[i]=a[ind];
		a[ind] = aux;
		reconstructie_heap (a,n,ind,dim_heap);
	}	
}

void construieste_heap (int a[10001], int n,int dim_heap)
{
	int i;
	for (i=dim_heap/2; i>=1; i--)
		reconstructie_heap (a,n,i,dim_heap);
}

void heap_sort (int a[10001], int n)
{
	int aux;
	int dim_heap=n;
	construieste_heap (a,n,dim_heap);
	for (int i=n; i>=2; i--)
	{
		nahs+=3;
		aux = a[1];
		a[1] = a[i];
		a[i] = aux;
		dim_heap = dim_heap - 1;
		reconstructie_heap (a,n,1,dim_heap);
	}
}

void quick (int a[10001], int n, int st, int dr)
{
	int i, j, pivot, aux;
	i = st;
	j = dr;

	naq++;
	pivot = a[(st+dr)/2];

	do
	{
		ncq++;
		while (a[i]<pivot)
		{
			ncq++;
			i++;
		}
		ncq++;
		while (a[j]>pivot)
		{
			ncq++;
			j--;
		}
		if (i<=j)
		{
			naq+=3;
			aux = a[i];
			a[i] = a[j];
			a[j] = aux;
			i++;
			j--;
		}
	}while (i<=j);

		if (st<j)
			quick (a,n,st,j);
		if (i<dr)
			quick (a,n,i,dr);
}

//TOP-DOWN
void initH (int a[10001],int &dim_a)
{
	dim_a = 0;
}

void Hpush (int a[10001], int &dim_a, int x)
{
	int i,aux;

	dim_a = dim_a + 1;
	natd++;
	a[dim_a] = x;
	i = dim_a;
	while (i>1 && a[i]<a[parinte(i)])
	{
		nctd++;
		natd+=3;
		aux = a[i];
		a[i] = a[parinte(i)];
		a[parinte(i)] = aux;
		i = parinte (i);
	}
	nctd++;
}

void construieste_heap_td (int a[10001],int b[10001],int n)
{
	int dim_b;
	initH (b,dim_b);

	for (int i=1; i<=n; i++)
		Hpush (b,dim_b,a[i]);
}

void copy (int a[10001], int c[10001], int n)
{
	for (int i=1; i<=n; i++)
		c[i] = a[i];
}

void cazul_mediu ()
{
	int a[10001],b[10001],c[10001];

	int natdm, nabum, nahsm, naqm;
	int nctdm, ncbum, nchsm, ncqm;

	FILE *pf1;
	pf1 = fopen ("heap_td.txt","w");
	FILE *pf2;
	pf2 = fopen ("heap_bu.txt","w");
	FILE *pf3;
	pf3 = fopen ("heap_sort.txt","w");
	FILE *pf4;
	pf4 = fopen ("quick.txt","w");
	

	for (int n=100; n<=10000; n=n+100)
	{
		natdm = nabum = nahsm = naqm = 0;
		nctdm = ncbum = nchsm = ncqm = 0;

		for (int k=1; k<=5; k++)
		{
			for (int j=1; j<=n; j++)
				a[j] = rand() % 5000 + 1;

			//constructie heap top-down
			natd = 0;
			nctd = 0;
			copy (a,c,n);
			construieste_heap_td (a,b,n);
			natdm += natd;
			nctdm += nctd;

			//constructie heap buttom-up
			nabu = 0;
			ncbu = 0;
			copy (a,c,n);
			construieste_heap (c,n,n);
			nabum += nabu;
			ncbum += ncbu;

			//heap sort
			nahs = 0;
			nchs = 0;
			copy (a,c,n);
			heap_sort (c,n);
			nahsm += nahs;
			nchsm += nchs;

			//quick sort
			naq = 0;
			ncq = 0;
			copy (a,c,n);
			quick (c,n,1,n);
			naqm += naq;
			naqm += ncq;
		}

		natdm/=5;nctdm/=5;
		nabum/=5;ncbum/=5;
		nahsm/=5;nchs/=5;
		naqm/=5;ncqm/=5;

		fprintf (pf1,"%d %d %d %d\n",n,natd,nctd,natd+nctd);
		fprintf (pf2,"%d %d %d %d\n",n,nabu,ncbu,nabu+ncbu);
		fprintf (pf3,"%d %d %d %d\n",n,nahs,nchs,nahs+nchs);
		fprintf (pf4,"%d %d %d %d\n",n,naq,ncq,naq+ncq);
	}
}


void best_worst()
{
	
	int a[10001],b[10001],c[10001];
	int n;
	int  nahsb=0;int naqb=0; int nahsw=0; int naqw=0;
	int  nchsb=0; int ncqb=0; int nchsw=0 ; int ncqw=0;

FILE *pf5;
	pf5 = fopen("best_quick.txt","w");
	FILE *pf6;
	pf6 = fopen("worst_quick.txt","w");
	FILE *pf7;
	pf7 = fopen("best_heap.txt","w");
	FILE *pf8;
	pf8 = fopen("worst_heap.txt","w");
	

	for (int n=100; n<=10000; n=n+100)
	{
		randw(a,n);
		//heap sort
			nahs = 0;
			nchs = 0;
			copy (a,c,n);
			heap_sort (c,n);
			nahsw += nahs;
			nchsw += nchs;

			//quick sort
			naq = 0;
			ncq = 0;
			copy (a,c,n);
			quick (c,n,1,n);
			naqw += naq;
			naqw += ncq;

			fprintf (pf6,"%d %d %d %d\n",n,nahsw,nchsw,nahsw+nchsw);
		fprintf (pf8,"%d %d %d %d\n",n,naqw,ncqw,naqw+ncqw);
	}
	

for (int n=100; n<=10000; n=n+100)
	{
		randb(a,n);
		//heap sort
			nahs = 0;
			nchs = 0;
			copy (a,c,n);
			heap_sort (c,n);
			nahsb += nahs;
			nchsb += nchs;

			//quick sort
			naq = 0;
			ncq = 0;
			copy (a,c,n);
			quick (c,n,1,n);
			naqb += naq;
			naqb += ncq;



			fprintf (pf5,"%d %d %d %d\n",n,nahsb,nchsb,nahsb+nchsb);
		fprintf (pf7,"%d %d %d %d\n",n,naqb,ncqb,naqb+ncqb);
	}
	


}

void test ()
{
	int a[10001],h[10001], n;

	printf ("n=");
	scanf ("%d",&n);
	printf ("\n");
	for (int i=1; i<=n; i++)
	{
		a[i]=rand()%1000+1;
	}

	construieste_heap_td (a,h,n);
	printf ("Rezultat construire heap (TOP-DOWN):\n");
	for (int i=1; i<=n; i++)
		printf ("%d ",h[i]);

	printf ("\n");
}

int main ()
{
	cazul_mediu ();
	best_worst();
	//test ();
	getche ();
	return 0;
}