/****ANONIM*** ***ANONIM***, ***GROUP_NUMBER***, Assign10
In amandoua cazuri (numarul de arce / numarul de varfuri este constant iar cealalta valoare variaza)
numarul de operatii creste liniar depinzand de amandoua parametri. Daca avem mai multe noduri avem
mai multe de parcurs deci cresterea operatiilor este evidenta iar daca avem mai multe arce intr-un
ciclu trebuie testat culoarea mai multor vecini.
In faza de testare a corectitudinii algoritmului folosim afisarea listei de adiacenta, afisarea
tipului pentru fiecare arc(tree, back, forward sau cross) si testarea sortarii topologice in diferite cazuri.
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#include "Profiler.h"

typedef enum{WHITE, GRAY, BLACK}color_type;
typedef enum{TREE, BACK, FORWARD, CROSS}edge_type;

#define V_MAX 300
#define E_MAX 10000

typedef struct
{
	int d_time;
	int f_time;
	int parent;
	color_type color;
}graph_node;

typedef struct _node
{
	int key;
	edge_type type;
	struct _node* next;
}node;

//global variables
node *adj[V_MAX];
graph_node graph[V_MAX];
int edges[E_MAX];
int nrV=0;
int nrE=0;
int time_count=0;
//count op
int countV[V_MAX+10];
int countE[E_MAX+10];
int saveCountAux[E_MAX+10];

//fc aux pt hardcoded graphs
//a-start node, b-end node
void addEdge(int a, int b)
{
	node *nod=new node;
	nod->key=b;
	nod->next=adj[a];
	adj[a]=nod;
}

/* n-noduri, m muchii */
void generate(int n, int m)
{
	int a;
	int b;
	FillRandomArray(edges, m, 1, n*n-1,true);
	for (int i=0; i<m; i++)
	{
		a=edges[i]%n;
		b=edges[i]/n;
		addEdge(a,b);
	}
}

void print_adj_list()
{
	printf("\nAdjacency list:\n");
	for (int i=0; i<nrV; i++)
	{
		printf("%d: ",i);
		for (node *p=adj[i]; p!=0; p=p->next)
		{
			printf("%d ",p->key);
		}
		printf("\n");
	}
	printf("\n");
}

void deleteListAdj()
{
	for (int i=0; i<V_MAX; i++)
	{
		if (adj[i] != 0)
		{
			node *p=adj[i];		
			while (p != 0)
			{			
				node *del=p;
				p=p->next;
				delete del;
			}
			adj[i]=0;
		}
	}
}


void init_counters()
{
	for (int i=0; i<V_MAX; i++)
	{
		countV[i]=0;
	}
	for (int i=0; i<E_MAX; i++)
	{
		countE[i]=0;
	}
}

void finalize()
{
	deleteListAdj();
}

void dfs_visit(int u)
{
	graph[u].color=GRAY;
	graph[u].d_time=++time_count;
	for(node *p=adj[u]; p!=0; p=p->next)
	{
		if (graph[p->key].color == WHITE)
		{
			p->type=TREE;
			graph[p->key].parent=u;

			countE[nrE]+=2;
			countV[nrV]+=3;

			dfs_visit(p->key);
		}
		else
		{
			if (graph[p->key].color == GRAY)
			{
				p->type=BACK;

				countE[nrE]++;
				countV[nrV]++;
			}
			else
			{
				if (graph[p->key].f_time < graph[u].d_time)
				{
					p->type=CROSS;

					countE[nrE]+=2;
					countV[nrV]+=2;
				}
				else
				{
					p->type=FORWARD;

					countE[nrE]+=2;
					countV[nrV]+=2;
				}
			}

			countE[nrE]+=2;
			countV[nrV]+=2;
		}
	}
	graph[u].color=BLACK;
	graph[u].f_time=++time_count;

	countE[nrE]+=4;
	countV[nrV]+=4;
}

void dfs()
{
	for (int u=0; u<nrV; u++)
	{
		graph[u].color=WHITE;
		graph[u].parent=0;
	}
	time_count=0;
	for (int u=0; u<nrV; u++)
	{
		if (graph[u].color == WHITE)
		{
			dfs_visit(u);
		}
		countE[nrE]++;
		countV[nrV]++;
	}
}

void print_edges_types()
{
	printf("Edges types:\n");
	for (int i=0; i< nrE; i++)
	{
		for (node *p=adj[i]; p!=0; p=p->next)
		{
			char tip[sizeof(FORWARD)];
			switch(p->type)
			{
			case TREE:
				strcpy(tip, "TREE");
				break;
			case BACK:
				strcpy(tip, "BACK");
				break;
			case CROSS:
				strcpy(tip, "CROSS");
				break;
			case FORWARD:
				strcpy(tip, "FORWARD");
				break;
			default:
				perror("Edge type error!");
				exit(3);
			}

			printf("%d->%d - %s\n", i, p->key, tip);
		}
	}
}

//is there a back edge between x->y?
bool existBackEdge(int x, int y)
{
	node *p=adj[x];
	while (p != 0)
	{
		if (p->type == BACK)
		{
			return true;
		}
		p=p->next;
	}
	return false;
}

void topological_sort()
{
	int vect_id_aux[V_MAX];
	for (int i=0; i<nrV; i++)
	{
		vect_id_aux[i]=i;
	}

	dfs();

	//sortare dupa final time in ordine descrescatoare
	for (int i=0; i < nrV-1; i++)
	{
		for (int j=i+1; j<nrV; j++)
		{
			if (graph[i].f_time < graph[j].f_time)
			{
				graph_node aux1=graph[i];
				graph[i]=graph[j];
				graph[j]=aux1;

				node *aux2=adj[i];
				adj[i]=adj[j];
				adj[j]=aux2;

				int aux3=vect_id_aux[i];
				vect_id_aux[i]=vect_id_aux[j];
				vect_id_aux[j]=aux3;
			}
		}
	}

	for (int i=nrV-1; i>0; i--)
	{
		for (int j=i-1; j>=0; j--)
		{
			if(existBackEdge(i,j))
			{
				printf("\nThere is no topological sort for the graph\n");
				return;
			}
		}
	}
	printf("\nThe topological sort of the graph: ");
	for (int i=0; i < nrV; i++)
	{
		printf("%d ", vect_id_aux[i]);
	}
	printf("\n");
}

int main()
{
	//initializare adj cu NULL
	memset(adj, 0, V_MAX*sizeof(node *));

	//test
	init_counters();

	nrV=8;
	nrE=13;
	addEdge(0,3);
	addEdge(0,1);
	addEdge(1,3);
	addEdge(1,2);
	addEdge(2,4);
	addEdge(4,1);
	addEdge(3,4);
	addEdge(5,6);
	addEdge(5,7);
	addEdge(6,7);
	addEdge(6,5);
	addEdge(7,0);
	addEdge(7,3);

	//nrV=8;
	//nrE=9;
	//generate(nrV,nrE);

	print_adj_list();
	dfs();
	print_edges_types();
	finalize();

	//test topological sort
	//test exist
	init_counters();
	nrV=4;
	nrE=4;
	addEdge(0,1);
	addEdge(0,2);
	addEdge(1,2);
	addEdge(2,3);
	topological_sort();
	//test doesn't exist
	addEdge(3,0);
	topological_sort();
	finalize();

	//generare date
	printf("Computing...");

	//|V| const
	init_counters();
	nrV=100;
	int cantitate=100;
	for (nrE=1000; nrE<=5000; nrE+=cantitate)
	{
		generate(nrV,nrE);
		dfs();
		finalize();
	}

	//save the count
	memcpy(saveCountAux, countE, sizeof(countE));

	//|E| const
	init_counters();
	nrE=9000;
	cantitate=10;
	for (nrV=110; nrV<=200; nrV+=cantitate)
	{
		generate(nrV,nrE);
		dfs();
		finalize();
	}

	//exportare date
	FILE *f=fopen("assign10.csv", "w");
	if (f == 0)
	{
		perror("Eroare la deschidere fisier!");
		exit(2);
	}

	fprintf(f,"nr edges,nr op\n");
	cantitate=100;
	for (nrE=1000; nrE<=5000; nrE+=cantitate)
	{
		fprintf(f,"%d,%d\n",nrE,saveCountAux[nrE]);
	}

	fprintf(f,"nr vertices,nr op\n");
	cantitate=10;
	for (nrV=110; nrV<=200; nrV+=cantitate)
	{
		fprintf(f,"%d,%d\n",nrV,countV[nrV]);
	}

	fclose(f);

	printf("\nDone!\n");

	return 0;
}