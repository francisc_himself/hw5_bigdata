#include <stdio.h>
#include <conio.h>
#include <queue>
#include <stack>
#include <cstdlib>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;



#define inf 10000
#define white 1
#define gray 2
#define black 3


FILE* f;

struct Node
{
int key;
int discovery;
int finish;
int color;
Node* pred;
Node* adj[10000];
int adjNr;
};



int nodeNr;
int edgeNr;
int op;

Node* vertices[8000];
bool topo;
int countOp =0;

int timestamp;
//queue<int> topolist;
stack<int> topolist;

void DFSVisit(Node *u)
{

    

    printf("dfs at: %d \n", u->key);

	op+=3;
    timestamp++;
    u->discovery = timestamp;
    u->color = gray;

    for (int i=0; i<u->adjNr; i++)
    {

		

        if (u->adj[i]->color == gray)
        {
            topo = 0; 
			printf("back edge %d-%d \n",u->key, u->adj[i]->key);
        }

		if (u->adj[i]->color== black)
		{
			if (u->discovery<u->adj[i]->discovery)
				printf("forward edge %d-%d \n",u->key, u->adj[i]->key);
			else if(u->discovery>=u->adj[i]->discovery)
				printf("cross edge %d-%d \n",u->key, u->adj[i]->key);

		}
   
        if (u->adj[i]->color == white)
        {
            printf("tree edge %d-%d \n", u->key,u->adj[i]->key);
            u->adj[i]->pred = u;
			op++;
            DFSVisit(u->adj[i]);
        }
		op+=2;
    }

    u->color = black;
    timestamp++;

    u->finish = timestamp;
    topolist.push(u->key);
	op+=4;
}

void DFS(Node* s)
{

    timestamp = 0;
    for (int i=0; i<nodeNr; i++)
    {
        if (vertices[i]->color == white) DFSVisit(vertices[i]);
    }
}

void graphRand()
{

for(int i=0; i<nodeNr; i++)
	{
		free(vertices[i]);

		vertices[i] = new Node();
		vertices[i]->key = i;
		vertices[i]->adjNr = 0;
		vertices[i]->color=white;
	}

for(int i=0; i<edgeNr; i++)
{ 
	int retry,a,b;
	do{
		retry = 0;
		a = rand()%nodeNr;
		do {
			b = rand()%nodeNr;
			} while (a==b);


	for(int j=0; j<vertices[a]->adjNr; j++)
	{
		if (vertices[a]->adj[j] == vertices[b])
		{
			retry = 1;
		}
	}

	} while (retry == 1);

	vertices[a]->adj[vertices[a]->adjNr]=vertices[b];
	vertices[a]->adjNr++;
}

}

void prettyPrint(Node* root, int height)
{
printf("\n");
if (root!=NULL)
	{
		for (int x=0; x<height; x++)
		{
			printf(" ");
		}
	printf("%d", root->key);

	for(int i=0; i<root->adjNr; i++)
	{
		prettyPrint(root->adj[i],height+1);

	}
	}
}

int main(){


srand(time(NULL)); //new seed
//nodeNr = 1000;
//edgeNr = 5000;

f=fopen("out.txt", "w");
///*
nodeNr = 12;
edgeNr = 20;

for(int i=0; i<nodeNr; i++) //generate nodes
{
	vertices[i] = new Node();
	vertices[i]->key = i;
	vertices[i]->adjNr = 0;
	vertices[i]->color = white;
}



topo = 1;
vertices[0]->adj[0] = vertices[1];
vertices[0]->adjNr++;
vertices[1]->adj[1] = vertices[2];
vertices[1]->adj[0] = vertices[3];
vertices[1]->adjNr=2;
vertices[2]->adj[0]=vertices[4];
vertices[2]->adj[1]=vertices[9];
vertices[2]->adjNr=2;
vertices[3]->adj[0]=vertices[5];
vertices[3]->adjNr++;
vertices[5]->adj[0]=vertices[1];
vertices[5]->adjNr++;
vertices[6]->adj[0]=vertices[8];

vertices[7]->adj[0]=vertices[10];
vertices[7]->adjNr++;
vertices[10]->adj[0]=vertices[11];
vertices[10]->adjNr++;
vertices[7]->adj[1]=vertices[1];
vertices[7]->adjNr++;
vertices[0]->adj[1]=vertices[5];
vertices[0]->adjNr++;


DFS(vertices[0]);

if (topo ==1) 
	{
		printf("topological sort possible\n");
		while (!topolist.empty()) 
		{
			int z = topolist.top(); 
			printf("%d ", z); 
			topolist.pop();
		}
	}
else printf("topological not possible\n");

//prettyPrint(vertices[0],0);
//*/



//graphRand();
//prettyPrint(vertices[0],0);
//_getch();

//FIRST TEST
/*
nodeNr = 100;

fprintf(f, "edges,op\n");
for(edgeNr=1000; edgeNr<5000; edgeNr+=100)
	{
		
		graphRand();
		op=0;
		


		//for (int i=0; i<nodeNr; i++)
		//{
			DFS(vertices[0]);
		//}
		fprintf(f, "%d,%d\n",edgeNr,op);
		printf("%d,%d\n",edgeNr,op);

	}


*/

//SECOND TEST
/*
edgeNr = 9000;

fprintf(f, "nodes,op\n");
for(nodeNr=110; nodeNr<200; nodeNr+=10)
	{
		
		graphRand();
		op=0;
		


		//for (int i=0; i<nodeNr; i++)
		//{
			DFS(vertices[0]);
		//}
		fprintf(f, "%d,%d\n",nodeNr,op);
		printf("%d,%d\n",nodeNr,op);

	}


*/

return 0;
}