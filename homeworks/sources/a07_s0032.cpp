#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
typedef struct parinte
{
	int nr_child;
	int child[9];
}PAR;
typedef struct multi_way
{
	int key;
	int nr_children;
	struct multi_way *children[9];
}MULTI;
typedef struct binary
{
	int key;
	struct binary *stg,*dr;
}BINARY;

	int n,j;
	MULTI *head;
	PAR nod[9];
	void constr_multi_way(MULTI *node)
	{
	int j;
	int n=node->key;
	for(j=0;j<nod[n-1].nr_child;j++)
	{
		node->children[j]=(MULTI*)malloc(sizeof(MULTI));
		node->children[j]->key = nod[n-1].child[j];
		node->children[j]->nr_children = 0;
        node->nr_children++;
        constr_multi_way(node->children[j]);
    }
}
void multiway_print(MULTI *nod)
{
    int i;
   
	if(nod->nr_children>0)
    {  printf("%d  ",nod->key);
	   for(i=0;i<nod->nr_children; i++)
	   printf("%d ",nod->children[i]->key);
	   printf("\n");
	}
	for(i=0;i<nod->nr_children;i++)
        multiway_print(nod->children[i]);
	  
}

void constr_binary(MULTI *node,BINARY *elem)
{
	BINARY *aux;
	if(node->nr_children>0)
	{
		elem->stg=(BINARY*)malloc(sizeof(BINARY));
	    elem->stg->key=node->children[0]->key;
		elem->stg->stg=NULL;
		elem->stg->dr=NULL;
		aux=elem->stg;
	    for(int i=1;i<node->nr_children;i++)
	    {
			aux->dr=(BINARY*)malloc(sizeof(BINARY));
	        aux->dr->key=node->children[i]->key;
			aux->dr->stg=NULL;
			aux->dr->dr=NULL;
			constr_binary(node->children[i-1],aux);
			aux=aux->dr;
	}
}
}
void binary_pretty_print(BINARY *bin, int space)
{
    int i;
    for(i=0;i<=space;i++)
        printf(" ");
    printf("%d\n",bin->key);
    if(bin->stg!=NULL)
        binary_pretty_print(bin->stg,space+1);
    if(bin->dr!=NULL)
        binary_pretty_print(bin->dr,space);
}
void main()
{
	int i;
	int parent[9];
	parent[1]=1;
	parent[2]=6;
	parent[3]=4;
    parent[4]=1;
    parent[5]=6;
	parent[6]=6;
	parent[7]=-1;
	parent[8]=4;
	parent[9]=1;
	n=9;
	printf("n=9\n");
	for(i=1;i<=n;i++)
		nod[i].nr_child=0;
	for(i=1;i<=n;i++)
		if(parent[i]==-1)
	    {
		    (MULTI *)head=(MULTI *)malloc(sizeof(MULTI));
	        head->key=i;
			head->nr_children=0;
		}
		else
		{	nod[parent[i]].child[nod[parent[i]].nr_child]=i;
			nod[parent[i]].nr_child++;
		}
	  for(i=0;i<n;i++)
		   if(parent[i]==-1)
             constr_multi_way(head);
	  multiway_print(head); 
	   BINARY *elem=(BINARY *)malloc(sizeof(BINARY));
	   elem->key=head->key; 
	   elem->dr=NULL;
	   elem->stg=NULL;
   constr_binary(head,elem);
   printf("\n");
       binary_pretty_print(elem, 0);
   getche();
}