#include "stdafx.h"
#include<iostream>
#include<math.h>
#include<conio.h>
#include "Profiler.h"
#define MAX 10000
using namespace std;

Profiler profiler("Heapsort vs Quicksort");

int size,n;

void Print(int A[],int n){
	int i=1;
	for(i=1;i<=n;i++)
	{ cout<<A[i]<<" "; }
}

//Heapify function - we build the heap bottom-up
void heapify(int A[],int i,int n){
	int l,r,largest,aux;
	l=2*i;
	r=2*i+1;
	
	profiler.countOperation("CompHeapsort",n);
	if(l<=size && A[l]>A[i])
	{largest=l;}
	else 
	{largest=i;}

	profiler.countOperation("CompHeapsort",n);
	if(r<=size && A[r]>A[largest])
	{largest=r;}


	if(largest!=i)
	{
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		profiler.countOperation("AssignHeapsort",n,3);
		heapify(A,largest,n);
	}
	

}


void BuildMaxHeap(int A[],int n){
	size=n;
	for(int i=n/2;i>=1;i--){
		heapify(A,i,n);
	}
}

void HeapSort(int A[]){
	int i,aux;
	BuildMaxHeap(A,n);
	for(i=n;i>=2;i--){
		aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		profiler.countOperation("AssignHeapsort",n,3);
		size--;
		heapify(A,1,n);
	}

}



void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
	profiler.countOperation("AssignQuicksort",n);
      
      while (i <= j) {
				profiler.countOperation("CompQuicksort",n);
            while (arr[i] < pivot){ 
				i++;
				profiler.countOperation("CompQuicksort",n);
			}
		profiler.countOperation("CompQuicksort",n);
            while (arr[j] > pivot)
				{ j--;
				profiler.countOperation("CompQuicksort",n);}

            if (i <= j) {
				profiler.countOperation("AssignQuicksort",n,3);
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      }
 
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}

void quickSort_worst(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[left];
	profiler.countOperation("AssignQuicksort",n);
      
      while (i <= j) {
				profiler.countOperation("CompQuicksort",n);
            while (arr[i] < pivot){ 
				i++;
				profiler.countOperation("CompQuicksort",n);
			}
		profiler.countOperation("CompQuicksort",n);
            while (arr[j] > pivot)
				{ j--;
				profiler.countOperation("CompQuicksort",n);}

            if (i <= j) {
				profiler.countOperation("AssignQuicksort",n,3);
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      }
 
      if (left < j)
            quickSort_worst(arr, left, j);
      if (i < right)
            quickSort_worst(arr, i, right);
}


void quickSort_best(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[left];
	profiler.countOperation("AssignQuicksort",n);
      
      while (i <= j) {
				profiler.countOperation("CompQuicksort",n);
            while (arr[i] < pivot){ 
				i++;
				profiler.countOperation("CompQuicksort",n);
			}
		profiler.countOperation("CompQuicksort",n);
            while (arr[j] > pivot)
				{ j--;
				profiler.countOperation("CompQuicksort",n);}

            if (i <= j) {
				profiler.countOperation("AssignQuicksort",n,3);
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      }
 
      if (left < j)
            quickSort_best(arr, left, j);
      if (i < right)
            quickSort_best(arr, i, right);
}

void main(){

int A[MAX],B[MAX],C[MAX];
int i,j,op;

cout<<"\n1test. \n2.random\n3.worst case.\n 4 best case \n";
cin>>op;

switch(op){
	case 0 :return; break;
	case 1: n=10;
			for(i=1;i<=n;i++)
			{A[i]=rand() % 100;}
			
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

				quickSort(B,1,n);
				HeapSort(C);

				Print(A,n);
				cout<<"\n\n";
				Print(B,n);
				cout<<"\n\n";
				Print(C,n);

			break;
	case 2:	
		
		for(i=100;i<10000;i=i+100){
			FillRandomArray(A,i,0,10000);
				n=i;
			for(int k=1;k<=5;k++){
			for(j=1;j<=n;j++)
				{B[j]=A[j];C[j]=A[j];}

				quickSort(B,1,n);
				HeapSort(C);

		}}

				break;

	case 3:
		for(i=100;i<=10000;i=i+100){
			FillRandomArray(A,i,0,10000,false,2);
			n=i;
		
		   	for(j=1;j<=i;j++)
				{B[j]=A[j];C[j]=A[j];}

					quickSort_worst(B,1,n);
				HeapSort(C);
		   }
		break;
    case 4:
		for(i=100;i<=10000;i=i+100){
			FillRandomArray(A,i,0,10000,false,1);
			n=i;
		
		   	for(j=1;j<=i;j++)
				{B[j]=A[j];C[j]=A[j];}

					quickSort_best(B,1,n);
				HeapSort(C);
		   }
		break;
	default: cout<<"Invalid input"; return;
		break;
}


profiler.addSeries("Heapsort","CompHeapsort","AssignHeapsort");
profiler.addSeries("Quicksort","CompQuicksort","AssignQuicksort");

profiler.createGroup("Comparisons","CompHeapsort","CompQuicksort");
profiler.createGroup("Assignments","AssignHeapsort","AssignQuicksort");
profiler.createGroup("Sum","Heapsort","Quicksort");

if(op==2 || op==3)
	profiler.showReport();
getch();
}
