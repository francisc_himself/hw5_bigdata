#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<fstream>
#include<iostream>
#include<time.h>

using namespace std;

#define noduri 10000

fstream f("disjoint.csv",ios::out);

typedef struct elem
{
	int info, rank;
	elem *p;
};

elem *e[10001];

int x,y;
int a[2][60001];

long counter;  //used for counting the number of calls

// creates a new set whose only member (and thus representative) is x
void makeSet(elem *x)
{
    counter++;
    x->p=x;
    x->rank=1;
}

//returns a pointer to the representative of the (unique) set containing x
elem* findSet(elem *x)
{
    counter++;
    elem* n=x;
    if(n->p!=n)
    {
        x->p=findSet(x->p);
        n=x->p;
    }
    return n;
}

void link(elem *x,elem *y)
{
    counter++;
    if(x->rank>y->rank)
        y->p=x;
    else
    {
        x->p = y;
        if(x->rank==y->rank)
            y->rank++;
    }
}

//unites the dynamic sets that contain x and y, say Sx and Sy, into a
//new set that is the union of these two sets
void Union(elem *x,elem *y)
{
    elem *x1=findSet(x);
    elem *y1=findSet(y);

    if(x1!=y1)
    {
        link(x1,y1);
    }

}

void generate_nodes()
{
    x=rand()%10000+1;
    y=rand()%10000+1;
}

void connected()
{
    int comp[10];
    int c[10];
    for ( int i = 1; i<=9; i++ )
    {
        comp[i]=i;
    }
    for (int i = 1 ; i<=9; i++)
    {
        comp[i] = findSet(e[i])->info;
    }

    for(int i = 1 ; i <=9 ; i++)
    {
        cout<<i<<" "<<comp[i]<<endl;
    }
}

int main()
{
	/*
    srand(time(NULL));
    int i,mk,nr;
    for (mk=10000; mk<=60000; mk=mk+1000)
    {
        nr=10000;
        counter = 0;
        for(i=0; i<mk; i++)
        {
            generate_nodes();
            a[0][i]=x;
            a[1][i]=y;
        }

        for(i=1; i<=nr; i++)
        {
            e[i]=new elem;
            e[i]->info=i;
            makeSet(e[i]);

        }
        for (i=0; i<mk; i++)
        {
            Union(e[a[0][i]],e[a[1][i]]);
        }


        f<<mk<<","<<counter<<endl;
    }
    f.close();
	*/

	
	//demo
	 int nr=9;
    int mk=7;
    int i;
    a[0][0]=1;
    a[1][0]=2;
    
	a[0][1]=2;
    a[1][1]=3;
    
	a[0][2]=3;
    a[1][2]=1;
    
	a[0][3]=4;
    a[1][3]=5;
    
	a[0][4]=5;
    a[1][4]=6;
    
	a[0][5]=6;
    a[1][5]=7;
    
	a[0][6]=8;
    a[1][6]=9;


    for(i=1; i<=nr; i++)
    {
        e[i]= new elem;
        e[i]->info=i;
        makeSet(e[i]);

    }
    for (i=0; i<mk; i++)
    {
        Union(e[a[0][i]],e[a[1][i]]);
    }

    connected();
	getch();
    return 0;
}