/*************************************************************
//http://techforum4u.com/entry.php/573-Algorithm-And-C-Programming-Code-For-BFS-and-DFS
***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***
Complexitatea algoritmului este de O(V+E) adica suma dintre varfurile grafului si arcele grafului.
**************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
#define SIZE 200
#define T 1
#define F 0
#define NULL 0
int atr,comp;
struct edge
{
 int terminal;
 struct edge *next;
};
struct vertex
{
 int visit;
 int vertex_no;
 char info;
 int path_lenth;
 struct edge *edge_ptr;
};
struct q
{
 int info;
 struct q *next;
};
void table(int,int matrix[SIZE][SIZE],struct vertex vert[SIZE]);
struct edge *insert_vertex(int,struct edge *);
void bfs(int ,int *dist,struct vertex vert[SIZE]);
void output(int,int a[SIZE][SIZE]);
void input(int,int a[SIZE][SIZE]);
struct q *insert_queue(int vertex_no,struct q *first);
struct q *delet_queue(int *vertex_no,struct q *first);

struct edge *insert_vertex(int vertex_no,struct edge *first)
{
 struct edge *new1,*current ;
 new1=(struct edge*)malloc(sizeof(struct edge));
 new1->terminal=vertex_no;
 new1->next=NULL;
 if(!first)
   return new1;
 for(current=first;current->next;current=current->next);
 current->next=new1;
 return first;
}

struct q *insert_queue(int vertex_no,struct q *first)
{
 struct q *new1,*current;
 new1=(struct q*)malloc(sizeof(struct q));
 new1->info=vertex_no;
 new1->next=NULL;
 if(!first)
   return new1;
 for(current=first;current->next;current=current->next);
 current->next=new1;
 return first;
}

struct q *delet_queue(int *vertex_no,struct q *first)
{

 if(!first)
   return NULL;
 *vertex_no=first->info;
 first=first->next;
 
 return first;
}

void table(int vertex_num,int matrix[SIZE][SIZE],struct vertex vert[SIZE])
{
 int i,j;
 for(i=0;i<vertex_num;i++)
 {
  vert[i].visit=F;
  vert[i].vertex_no=i+1;
  vert[i].info='A'+i;
  vert[i].path_lenth=0;
  vert[i].edge_ptr=NULL;
  atr=atr+4;
 }
 for(i=0;i<vertex_num;i++)
   for(j=0;j<vertex_num;j++)
    if(matrix[i][j]>0)
     vert[i].edge_ptr=insert_vertex(j,vert[i].edge_ptr);
}

void bfs(int index,int *dist,struct vertex vert[SIZE])
{
 struct q *queue=NULL;
 struct edge *link;
 vert[index].visit=T;
 queue=insert_queue(index,queue);
 atr=atr+2;
 while(queue)
 {
  queue=delet_queue(&index,queue);
  atr=atr+1;
  for(link=vert[index].edge_ptr;link;link=link->next)
  {
	comp++; 
   if(vert[link->terminal].visit==F)
   {
    vert[link->terminal].visit=T;
    vert[link->terminal].path_lenth=vert[index].path_lenth+1;
    queue=insert_queue(link->terminal,queue);
	atr=atr+3;
   }
  }
  }
 
}

void input(int number,int a[SIZE][SIZE],int counter)
{
 int i,j;
 int count=0;
 while(count!=counter)
 {
 for(i=0;i<number;i++)
 {
  for(j=0;j<number;j++)
  {
	  if(a[i,j]==a[j,i])
	  {
		  a[i][j]=rand()%2;
		  count++;
		 if(count==counter)
		 {
			 printf("%d valoare counter iesire\n",count);
			 return;
		 }
      }
  }
 }
}
 printf("%d ",count);
}


void output(int number,int a[SIZE][SIZE])
{
 int i,j;
 printf("\n adjacency matrix is:\n");
 for(i=0;i<number;i++)
 {
  for(j=0;j<number;j++)
   printf("%d\t",a[i][j]);
  printf("\n");
 }
}
void reinitializare(int a[SIZE][SIZE])
{
	for(int i=0;i<SIZE;i++)
	{
		for(int j=0;i<SIZE;i++)
		{
			a[i][j]=0;
		}
	}
}
void main()
{
 int i,number,index,dist,a[SIZE][SIZE];
 struct vertex vert[SIZE];
 struct edge *list;
 FILE *f,*g;
 fopen_s(&f,"Graf.csv","w");
 fopen_s(&g,"Graf2.csv","w");
 srand(time(NULL));
 printf("\ninput the number of vertices in graph:");
 scanf_s("%d",&number);
 for(int i=0;i<number;i++)
 {
	 for(int j=0;j<number;j++)
	 {
		 a[i][j]=0;
	 }
 }
 a[0][0]=0;
 a[0][1]=1;
 a[1][0]=1;
 a[0][3]=1;
 a[3][0]=1;
 a[1][2]=1;
 a[2][1]=1;
 a[3][4]=1;
 a[4][3]=1;
 a[3][5]=1;
 a[5][3]=1;
 a[4][5]=1;
 a[5][4]=1;
 a[5][6]=1;
 a[6][5]=1;
 a[6][7]=1;
 a[7][6]=1;
 a[5][7]=1;
 a[7][5]=1;
 a[4][6]=1;
 a[6][4]=1;
 output(number,a);
 table(number,a,vert);
 printf("\ninput the strating vertex 0-%d:",number-1);
 scanf_s("%d",&index);
 dist=0;
 bfs(index,&dist,vert);
 printf("\n path lenth of the vertex %c",vert[index].info);
 printf("\n vertex length vertex complexity\n");
 for(i=0;i<number;i++)
 {
  printf("\n%c\t\t%d",vert[i].info,vert[i].path_lenth);
  for(list=vert[i].edge_ptr;list;list=list->next)
  {
    printf(" ");
    putchar(list->terminal+'A');
  }
 }
 printf("\n");
 reinitializare(a);
 for(int i=1000;i<=5000;i=i+100)
 {
	 reinitializare(a);
	 input(100,a,i);
	 table(100,a,vert);
	 bfs(rand()%100,&dist,vert);
	 fprintf(f,"%d,%d,%d,%d\n",i,atr,comp,atr+comp);
	 comp=0;
	 atr=0;
 }
 atr=0;
 comp=0;
 for(int i=100;i<=200;i=i+10)
 {
	 reinitializare(a);
	 printf("Merge\n");
	 input(i,a,9000);
	 printf("Merge dupa input\n");
	 table(i,a,vert);
	 bfs(rand()%100,&dist,vert);
	 fprintf(g,"%d,%d,%d,%d\n",i,atr,comp,atr+comp);
	 atr=0;
	 comp=0;
 }
 _getch();
}