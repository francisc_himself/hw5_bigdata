/*
During average case testing of the Bottom Up and Top Down methods of building a heap the main difference between the two is set during the comparisons
count, BottomUp executing more comparisons than TopDown, approximative double the comparisons for the same size of the input array.
From the assignments part of view, the two methods for building a heap are approximative equal in terms of assignment counting.
The total number suggests that the Top Down method for heap building executes much more comparisons and assignments rather than the Bottom up method.
Both methods are linear and have a complexity of O(n).
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler;

const int VMAX = 10000;
const int VMIN = 0;


int* gen_random(int n)
{
  int* a=new int[n];
  srand(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=rand()%(VMAX-VMIN) +VMIN;
  return a;
}


void rec_heap_BU(int heap[],int i,int n)
{
	int aux;
    int l = 2*i;
    int r = 2*i+1;
    int max;
	profiler.countOperation("BottomUp Comparisons",n);
    if ((l<=n)&&(heap[l]>heap[i]))
		max=l;
    else
        max=i;
	  profiler.countOperation("BottomUp Comparisons",n);
    if ((r<=n)&&(heap[r]>heap[max]))
        max=r;
	//profiler.countOperation("BottomUp Comparisons",n);
    if (max!=i)
	{
        aux = heap[i];
        heap[i] = heap[max];
        heap[max] = aux;
		profiler.countOperation("BottomUp Assignments",n,3);
        rec_heap_BU(heap,max,n);
	}
}

void build_heap_BU(int heap[],int n)//BOTTOM UP
{
    for (int i = n/2 ; i>0 ; i--)
        rec_heap_BU(heap,i,n);
}


void push_heap(int heap[],int val,int *dim, int n)
{
	int i;
	(*dim)++;
	i=(*dim);
	profiler.countOperation("TopDown Comparisons",n);
	while ((i>1) && ( heap[i/2]< val))
	{
		heap[i] = heap[i/2];
		i = i/2;
		profiler.countOperation("TopDown Assignments",n);
		profiler.countOperation("TopDown Comparisons",n);
	}
	heap[i] = val;
	profiler.countOperation("TopDown Assignments",n);
}


void build_heap_TD(int a[],int heap[],int n,int *dim) //TOP DOWN
{
	for(int i=1; i<=n; i++)
	{
	push_heap(heap,a[i],dim,n);
	}
}


void print_heap(int t[], int i, int lvl)
{
    if(i<=12)
    {
    print_heap(t, 2*i+1, lvl+1);
    for(int j=1;j<=lvl;j++)
    printf("          ");
    printf("%d\n",t[i]);
    print_heap(t,2*i, lvl+1);
    }
}


void main()
{
	
	int heap[9900];
	int *a;
	int n,dim;
	n=13;
	int d = 0;
	a = gen_random(n);
	for (int i=1;i<=n;i++)
		heap[i]=a[i];
	printf("Before:\n");
	printf("\n");
	print_heap(heap,1,0);
	printf("\n");
	printf("After the construction of the BottomUp:\n");
	printf("\n");
	build_heap_BU(heap,n);
	print_heap(heap,1,0);
	printf("\n");
	printf("After the construction of the TopDown:\n");
	printf("\n");
	build_heap_TD(a,heap,n,&d);
	print_heap(heap,1,0);
	getch();
	

	/*
	int heap[10001]
	int *a;
	for (int j=0;j<5;j++)
		for (int n=100;n<10000;n = n+100)
		{
			int d=0;
			a=gen_random(n);
			for (int i = 1 ; i <= n ; i++)
			heap[i] = a[i];
			build_heap_BU(heap,n);
			build_heap_TD(a,heap,n,&d);
		}
		profiler.createGroup("Comparisons","TopDown Comparisons","BottomUp Comparisons");
		profiler.createGroup("Assignments","TopDown Assignments","BottomUp Assignments");
		profiler.addSeries("Total TopDown","TopDown Comparisons","TopDown Assignments");
		profiler.addSeries("Total BottomUp","BottomUp Comparisons","BottomUp Assignments");
		profiler.createGroup("Total","Total TopDown","Total BottomUp");
		profiler.showReport();
		*/

		}

