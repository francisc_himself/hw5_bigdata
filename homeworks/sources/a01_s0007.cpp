/****ANONIM*** Vasile-***ANONIM***, grupa ***GROUP_NUMBER***
Comparatii:
-in cazul favorabil, sortarea prin metoda bulelor este liniara, iar cea prin selectie si prin insertie sunt patratice;
-in cazul defavorabil, toate cele trei metode sunt patratice;
-in cazul mediu statistic, toate metodele sunt patratice, metoda sortarii prin insertie fiind mai eficienta;

Asignari:
-in cazul favorabil, sortarea prin metoda bulelor este constanta, iar cea prin selectie si prin insertie sunt liniare;
-in cazul defavorabil, metoda bulelor este patratica, iar cea prin selectie si prin insertie sunt liniare;
-in cazul mediu statistic, sortarea prin selectie este liniara, iar celelalte doua sunt patratice, metoda bulelor fiind cea mai ineficienta;

Comparatii + Asignari:
-in cazul favorabil, sortarea prin metoda bulelor este liniara, iar cea prin selectie si prin insertie sunt patratice;
-in cazul defavorabil, toate cele trei metode sunt patratice, insa metoda bulelor este cea mai ineficienta;
-in cazul mediu statistic, toate metodele sunt patratice, metoda bulelor fiind din nou cea mai ineficienta;
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define MIN 100
#define MAX 10001

/*variabile globale in care am numarat comparatiile, respectiv asignarile si care vor fi reinitializate 
la inceputul fiecarui algoritm de sortare: */
long nr_cmp,nr_asig;

//functie de sortare prin metoda bulelor
void sort_bule(int v[],long n)
{
	bool sortat=false;
	int i=0,j,aux;
	
	nr_cmp=0;
	nr_asig=0;
	
	while(!sortat)
	{
		i++;
		sortat=true;
		for(j=0;j<n-i;j++)
		{
			nr_cmp+=1;
			if(v[j]>v[j+1])
			{
				nr_asig+=3;
				aux=v[j];
				v[j]=v[j+1];
				v[j+1]=aux;
				sortat=false;
			}
		}
	}
}

//functie de sortare prin selectie
void sort_selectie(int v[],long n)
{
	int i_min,i,j,aux;
	
	nr_cmp=0;
	nr_asig=0;

	for(i=0;i<n-1;i++)
		{
			i_min=i;
			for(j=i+1;j<n;j++)
			{
				(nr_cmp)+=1;
				if(v[j]<v[i_min])
					i_min=j;
			}
			nr_asig+=3;
			aux=v[i];
			v[i]=v[i_min];
			v[i_min]=aux;
	}
}

//functie de sortare prin insertie
void sort_insertie(int v[],long n)
{
	int i,j,k,x;
	
	nr_cmp=0;
	nr_asig=0;

	for(i=1;i<n;i++)
	{
		nr_asig++;
		x=v[i];
		j=0;
		nr_cmp++;
		while(v[j]<v[i])
		{	
			nr_cmp++;
			j++;
		}
		for(k=i-1;k>j;k--)
		{
			nr_asig++;
			v[k+1]=v[k];
		}
		nr_asig++;
		v[j]=x;
	}
}


//sirul generat aleator
void genereaza_rand(int v[],long n,int parametru_srand)
{
	int i;
	srand(parametru_srand);
	for(i=0;i<n;i++)
		v[i]=rand()%10000;
}

//sirul generat crescator
void genereaza_crescator(int v[],long n)
{
	int i;
	for(i=0;i<n;i++)
		v[i]=i+1;
}

//sirul generat descrescator
void genereaza_descrescator(int v[],long n)
{
	int i;
	for(i=0;i<n;i++)
		v[i]=n-i;
}


int main()
{
	int sir[MAX];
	int i;
	FILE *fav,*defav,*med;
	long n,asig2,comp2;
	long avgasig,avgcomp;

	fav=fopen("favorabil.csv","w");
	defav=fopen("defavorabil.csv","w");
	med=fopen("mediu_statistic.csv","w");
	
	fprintf(fav,"n,bule_comp,bule_asig,bule_comp+asig,sel_comp,sel_asig,sel_comp+asig,insr_comp,insr_asig,insr_comp+asig\n");
	fprintf(defav,"n,bule_comp,bule_asig,bule_comp+asig,sel_comp,sel_asig,sel_comp+asig,insr_comp,insr_asig,insr_comp+asig\n");
	fprintf(med,"n,bule_comp,bule_asig,bule_comp+asig,sel_comp,sel_asig,sel_comp+asig,insr_comp,insr_asig,insr_comp+asig\n");

	for(n=MIN;n<=MAX;n+=100)
	{
		
		//cazul favorabil
		genereaza_crescator(sir,n);
		sort_bule(sir,n);
		fprintf(fav,"%ld,%ld,%ld,%ld,",n,nr_cmp,nr_asig,nr_cmp+nr_asig);

		genereaza_crescator(sir,n);
		sort_selectie(sir,n);
		fprintf(fav,"%ld,%ld,%ld,",nr_cmp,nr_asig,nr_cmp+nr_asig);

		genereaza_crescator(sir,n);
		sort_insertie(sir,n);
		fprintf(fav,"%ld,%ld,%ld\n",nr_cmp,nr_asig,nr_cmp+nr_asig);

		//cazul defavorabil
		genereaza_descrescator(sir,n);
		sort_bule(sir,n);
		fprintf(defav,"%ld,%ld,%ld,%ld,",n,nr_cmp,nr_asig,nr_cmp+nr_asig);

		sort_selectie(sir,n);
		fprintf(defav,"%ld,%ld,%ld,",nr_cmp,nr_asig,nr_cmp+nr_asig);

		sort_insertie(sir,n);
		fprintf(defav,"%ld,%ld,%ld\n",nr_cmp,nr_asig,nr_cmp+nr_asig);

		//cazul mediu statistic
		asig2=0; comp2=0;
		for(i=0;i<5;i++)
		{
			genereaza_rand(sir,n,i);
			sort_bule(sir,n);
			asig2+=nr_asig;
			comp2+=nr_cmp;
		}
		avgasig=asig2/5;
		avgcomp=comp2/5;
		fprintf(med,"%ld,%ld,%ld,%ld,",n,avgcomp,avgasig,avgcomp+avgasig);

		asig2=0; comp2=0;
		for(i=0;i<5;i++)
		{
			genereaza_rand(sir,n,i);
			sort_selectie(sir,n);
			asig2+=nr_asig;
			comp2+=nr_cmp;
		}
		avgasig=asig2/5;
		avgcomp=comp2/5;
		fprintf(med,"%ld,%ld,%ld,",avgcomp,avgasig,avgcomp+avgasig);

		asig2=0; comp2=0;
		for(i=0;i<5;i++)
		{
			genereaza_rand(sir,n,i);
			sort_insertie(sir,n);
			asig2+=nr_asig;
			comp2+=nr_cmp;
		}
		avgasig=asig2/5;
		avgcomp=comp2/5;
		fprintf(med,"%ld,%ld,%ld\n",avgcomp,avgasig,avgcomp+avgasig);
	}

	fclose(fav);
	fclose(defav);
	fclose(med);
	return 0;
}