/*
*************************************ANONIM*** ***ANONIM*** Lucica*************************
 -complexitatea algoritmului este O(n)
 -find_set diminueaza complexitatea deoarece fiecate nod va pointa catre radacina
 -set_union diminueaza complexitatea pt ca se alege nodul cu radacina cu rank mai mare pt a nu creste dimensiunea arborelui
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include"Profiler.h"
int m;      
unsigned int edgesnr;  
int nodesnr;

struct node{
	node *parent;
	int key;
	int rank;
	};
node **nodes;
struct edge {
	int u;
	int v;
	};
edge **edges;
node* make_set(int x) {
	m++;
	node *p=new node;
	p->parent = p;
	p->rank = 0;
	p->key = x;
	return p;
	}

void link(node* u,node* v)
{
	if(u->rank>v->rank)
	{
		v->parent=u;

	}
	else
	{
		u->parent=v;
		if(u->rank==v->rank)
			v->rank++;
	}
}
node* find_set(node* n) {
	m++;
	if (n->key != n->parent->key) {
		n->parent = find_set(n->parent);  
		}
	return n->parent;
	}
void set_union(node *u,node * v) {
	m++;
	 link(find_set(u),find_set(v));
	}
/*
checks if a certain edges exists in the edge set.
*/
//int edge_between(node* u, node* v,int size) {
//	int i;
//	for (i = 0; i <size; i++) {
//		if ((edges[i]->u == u->key) && (edges[i]->v == v->key) || (edges[i]->u == v->key) && (edges[i]->v == u->key)){
//			return -1;
//			}		
//		}
//	return 0;
//	}
int same_component(node* u, node* v)  {
	if (find_set(u) == find_set(v)) {
		return 0;
		}
	else 
		return -1;
	}
void connected_components(node **nodes) {
	for (int i = 0; i < edgesnr; i++) {
		if(find_set(nodes[edges[i]->v])!= find_set(nodes[edges[i]->u]))  {
			set_union(nodes[edges[i]->u],nodes[edges[i]->v]);
			}
		}
}
void generate_nodes(int size) {
	nodes = (node **) malloc(size * sizeof(struct node));  //spatiu pt vector
	for (int i=0; i<size; i++) {
		nodes[i] =(node*) malloc(sizeof(node));  //spatiu pt fiecare nod
		nodes[i] = make_set(i);  //set the parent pointer to itself
		}
}
void generate_edges(int size){
	int arr[60001],key1=0,key2=0;
	edges = (edge**) malloc(size*sizeof(struct edge));
	FillRandomArray(arr,size*2,0,nodesnr*(nodesnr-1),true);
	int j=0;
	for(int i=0;i<size;i++){
			edges[i] =(edge*) malloc(sizeof(edge));
			while(arr[j]%nodesnr==arr[j]/nodesnr )//|| edge_between(nodes[arr[j]%nodesnr],nodes[arr[j]%nodesnr],i)==-1)
			j++;
			edges[i]->u=arr[j]%nodesnr;
			edges[i]->v=arr[j]/nodesnr;
			set_union(nodes[edges[i]->u],nodes[edges[i]->v]);
			j++;
	}
}
int main() {
	FILE *f = fopen("disjointSets.csv","w");
	nodesnr = 20;
	edgesnr=40;

	printf("\nGenerating %d nodes",nodesnr);
	generate_nodes(nodesnr);
	for(int i=0;i<nodesnr;i++)  printf(" %d->%d",nodes[i]->key,nodes[i]->parent->key);

	printf("\nGenerating %d edges",edgesnr);
	generate_edges(edgesnr);
	for(int i=0;i<edgesnr;i++)  printf(" %d-%d",edges[i]->u,edges[i]->v);

	printf("\nFinding connected components for %d nodes and %d edges",nodesnr,edgesnr);
	connected_components(nodes);
	for(int i=0;i<nodesnr;i++)
	printf("\ndupa con comp %d,   %d,   %d",nodes[i]->key,nodes[i]->rank,nodes[i]->parent->key);

	/*nodesnr=10000;
	for (edgesnr = 10000; edgesnr <= 60000; edgesnr+=500) {
		printf("\n %d",edgesnr);
		m = 0;
		generate_nodes(nodesnr);
		generate_edges(edgesnr);
		printf("\nFinding connected components for %d nodes and %d edges",nodesnr,edgesnr);
		connected_components(nodes);
		fprintf(f,"\n%d, %d",edgesnr,m);
		}*/
	fclose(f);
	printf("\nDone!");
	_getch();
}