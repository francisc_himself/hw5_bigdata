#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>

struct node{
	int val,rank;
	node *parent;
};

struct edge{
	int n1,n2;
};

int count;

node* makeSet(int x)
{
	node *p=new node;
	p->val=x;
	p->parent=p;
	p->rank=0;
	return p;
}

void Union(node* n1,node* n2)
{
	if(n1->rank>n2->rank)
	{
		n2->parent=n1;
	}
	else
	{
		n1->parent=n2;
		if(n1->rank==n2->rank)
			n2->rank++;
	}
}

node* findSet(node* n)
{
	if(n->parent!=n)
	{
		count++;
		n->parent=findSet(n->parent);
	}
	else
		return n;
}


void main()
{
	node* nodes[10000];
	edge edges[60000];
	int i,e;
//	FILE *f=fopen("D:\\file1.xls","w");
	//fprintf(f,"E\tcount\n");

	srand(time(NULL));
	for(i=0;i<10;i++)
		{
			nodes[i]=makeSet(i);
			count++;
		}
	for(i=0;i<10;i++)
		{
			edges[i].n1=rand()%10;
			printf("edges[%d].n1 = %d\n",i,edges[i].n1);
			edges[i].n2=rand()%10;
			printf("edges[%d].n2 = %d\n",i,edges[i].n2);
		}
	/*for(i=0;i<10;i++)
		{
			node* node1=findSet(nodes[edges[i].n1]); count++;
			node* node2=findSet(nodes[edges[i].n2]); count++;
				
			if(node1!=node2)
			{

				Union(node1,node2);
				printf("%d ",node1->val);
				printf("%d ",node2->val);
				count++;
			}
		}*/
	/*
	for(e=10000;e<=60000;e+=200)
	{
		count=0;
		srand(time(NULL));
		for(i=0;i<10000;i++)
		{
			nodes[i]=makeSet(i);
			count++;
		}
		for(i=0;i<e;i++)
		{
			edges[i].n1=rand()%10000;
			edges[i].n2=rand()%10000;
		}
		for(i=0;i<e;i++)
		{
			node* node1=findSet(nodes[edges[i].n1]); 
			count++;
			node* node2=findSet(nodes[edges[i].n2]); 
			count++;
			if(node1!=node2)
			{
				Union(node1,node2);
				count++;
			}
		}
		fprintf(f,"%d\t%d\n",e,count);
	}
	*/
	getch();

}