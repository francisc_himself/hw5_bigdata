
/* Media atribupirilor la top-down este mult mai mare decat media atribupirilor la botton-up
Media comparatiilor este relativ apropiata la cele doua metode
Media sumei dintre comparatii si atribupiri la metoda top-down este mai mare decat la botton-up

*/
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
# include<iostream>
# define CRT_SECURE_NO_WARNINGS
using namespace std;
int dim_heap=0;
int bup_a=0,bup_c=0,tdown_a=0,tdown_c=0 ;



void Randm(int a[], int inc)
{
	for(int i=0;i<inc;i++)
		a[i]=rand();
}

void RandmCresc(int a[], int inc)
{
	a[0]=1;
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]+rand();
}

void RandmDescresc(int a[], int inc)
{
	a[0]=INT_MAX; 
	for(int i=1;i<inc;i++)
		a[i]=a[i-1]-rand();
}

int parent(int i)
{
	return i/2;
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i)+1;
}
//constructia heap-ului prin tehnica bottom-up
void max_heapfy(int a[],int i,int n)
{
	int l,r,max,aux;
	l=left(i);
	r=right(i);
	bup_c++;
	if(l<=dim_heap && a[l]<a[i])
		max=l;
	else
		max=i;
	bup_c++;
	if(r<=dim_heap && a[r]<a[max])
		max=r;
	if(max!=i)
	{
		aux=a[i];
		a[i]=a[max];
		a[max]=aux;
		bup_a+=3;
		max_heapfy(a,max,n);
	}
}

void bupild_max_heap(int a[],int n)
{
	dim_heap=n;
	for(int i=n/2;i>=1;i--)
		max_heapfy(a,i,n);
}


//constructia heap-ului prin insertie(Top Down)
void heap_insert(int a[],int key)
{
	int i,aux;
	dim_heap++;
	i=dim_heap;
	tdown_a++;
	a[dim_heap]=key;
	tdown_c++;
	while(i>1 && a[parent(i)]>a[i])
	{
		tdown_c++;
		tdown_a+=3;
		aux=a[parent(i)];
		a[parent(i)]=a[i];
		a[i]=aux;
		i=parent(i);
	}

}

void bupild_max_heap_tdown(int a[],int b[],int n)
{
	dim_heap=0;
	for(int i=1;i<=n;i++)
		heap_insert(b,a[i]);

}

void afisare(int *v,int n,int k,int nivel)
{
	if (k>n)
	    return;
	else
	{
	afisare(v,n,2*k+1,nivel+1);
	for(int i=1;i<nivel;i++)
		cout<<"	 ";
	cout<<v[k]<<endl;
	afisare(v,n,2*k,nivel+1);
	
	}
}

void average()
{
	//srand(time(NULL));
	int a[100000],b[100000];
	FILE *f=fopen("average.csv","w");
    fprintf(f,"n,tdown_a/5,tdown_c/5,bup_a/5,bup_c/5,(tdown_a+tdown_c)/5,(bup_a+bup_c)/5\n");

	for(int n=100;n<=10000;n+=100)
	{
		
		bup_a=0;
		bup_c=0;
		tdown_a=0;
		tdown_c=0;
		for(int k=1;k<=5;k++)
		{
			for(int j=0;j<n;j++)
			{
				a[j]=rand();
			}

			bupild_max_heap_tdown(a,b,n);
			bupild_max_heap(a,n);
		}
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d\n",n,tdown_a/5,tdown_c/5,bup_a/5,bup_c/5,(tdown_a+tdown_c)/5,(bup_a+bup_c)/5);
		
	}fclose(f);
}

void defavorabil()
{
	//srand(time(NULL));
	int a[100000],b[100000];
	FILE *g=fopen("defav.csv","w");
    fprintf(g,"n,tdown_a/5,tdown_c/5,bup_a/5,bup_c/5,(tdown_a+tdown_c)/5,(bup_a+bup_c)/5\n");
	
	for(int n=100;n<=10000;n+=100)
	{
		
		bup_a=0;
		bup_c=0;
		tdown_a=0;
		tdown_c=0;
		for(int k=1;k<=5;k++)
		{
			RandmDescresc(a,n);

			bupild_max_heap_tdown(a,b,n);
			bupild_max_heap(a,n);
		}
		fprintf(g,"%d,%d,%d,%d,%d,%d,%d\n",n,tdown_a/5,tdown_c/5,bup_a/5,bup_c/5,(tdown_a+tdown_c)/5,(bup_a+bup_c)/5);
		
	}fclose(g);
}

int main()
{

	int c[11]={0,20,13,10,7,2,5,1,14,8,7};
	int a[100],b[100];
	

	printf("Constructia top-down este :\n");
	bupild_max_heap_tdown(c,b,10);
	afisare(b,10,1,1);
	printf("Constructia bottom-up este :\n");
	bupild_max_heap(c,10);
    afisare(c,10,1,1);
	//average();
//	defavorabil();
	


	
	
	
	getch();
	return 0;
}