/*    
***ANONIM*** Vasile-***ANONIM***,grupa ***GROUP_NUMBER***
Din graficele obtinute observam ca in cazul in care n variaza eficienta algoritmului este O(n) pentru fiecare din
urmatoarele situatii: k=5,k=10,k=100. De altfel, pentru k=5 numarul de operatii este mai mic decat decat in cazul
in care k=10, respectiv k=100, numarul de operatii crescand odata cu numarul de liste pe care vrem sa le interclasam.
Pentru cazul in care am variat k eficienta algoritmului este de O(nlogk);
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define MIN 100
#define MAX 10001
#define error -1

int efc=0;

typedef struct nod
{
	int value;
	struct nod *next;
}NOD;

int parinte(int i){
	return i/2;
}
int st(int i){
	return 2*i;
}
int dr(int i){
	return 2*i+1;
}

//va returna indexul minim dintre un parinte si fii
int index_min(int a[],int i,int dim_a){
	int imin=i;
	
	efc++;
	if((st(i)<=dim_a) &&(a[i]>a[st(i)]))
		imin=st(i);

	efc++;
	if((dr(i)<=dim_a) &&(a[imin]>a[dr(i)]))
		imin=dr(i);

	return imin;
}

//creare heap vid
void h_init(int v[],int *dim_v)
{
	*dim_v=0;
}

//functia de reconstructie a heap-ului
void reconstituie_heap(int a[],int i,int dim_a){
	int index,aux;

	index=index_min(a,i,dim_a);

	if(index!=i){
		efc+=3;
		aux=a[i];
		a[i]=a[index];
		a[index]=aux;
		reconstituie_heap(a,index,dim_a);
	}
}

//introducerea unui element in HEAP
void h_push(int v[],int x,int *dim_v)
{
	int i,aux;
	(*dim_v)++;
	efc++;
	v[*dim_v]=x;
	i=*dim_v;
	while((i>1)&&(v[parinte(i)]>v[i]))
	{
		efc++;
		efc+=3;
		aux=v[i];
		v[i]=v[parinte(i)];
		v[parinte(i)]=aux;
		i=parinte(i);
	}
}


//extragerea minimului din HEAP
int h_pop(int v[],int *dim_v)
{
	int x;
	if(*dim_v<=0)
		return -1;
	else 
	{
		efc++;
		x=v[1];
		v[1]=v[*dim_v];
		(*dim_v)--;
		reconstituie_heap(v,1,*dim_v);
	}
	return x;
}

void afisare_sir(int a[],int dim_a){
	for(int i=1;i<=dim_a;i++)
		printf("%d ",a[i]);
	printf("\n");
}

void construire_heap(int a[],int dim_a){
	for(int i=dim_a/2;i>=1;i--)
		reconstituie_heap(a,i,dim_a);
}

//construire heap BU
void bottom_up(int a[],int dim_a){
	int aux;
	construire_heap(a,dim_a);
}



// creare lista vida 
void CreareVida(NOD** head, NOD** tail)
{
	*head = NULL;
	*tail = NULL;
}

// Parcurgere si afisare lista simpla 
void Afisare(NOD* head) 
{ 
	NOD *p=(NOD*)malloc(sizeof(NOD));
	p=head;
	// cat timp mai avem elemente 
	// in lista 
	while (p != NULL) 
	{ 
		// afiseaza elementul curent 
		printf("%d ",p->value); 
		// avanseaza la elementul urmator 
		p = p->next; 
	} 
}

// Inserare element la sfarsitul unei liste simplu inlantuite = introdu in COADA
void InserareSfarsit(NOD** head, NOD** tail, int val) 
{
	NOD *p=(NOD*)malloc(sizeof(NOD));
	p->value = val; 
	p->next = NULL; 
	// daca avem lista vida 
	if (*head == NULL) 
		// doar modificam capul si coada listei 
	{
		*head=p;
		*tail=p;
	}
	// adaugam elementul nou la sfarsitul listei 
	else
	{	
		(*tail)->next=p;
		*tail=p;
	}
}

//Extragerea primului element al unei liste simplu inlantuite = scoate din COADA
int ExtrageCap(NOD** head) 
{
	NOD *p=*head;
	int key=p->value;
	*head=(*head)->next;
	return key;
}

void interclasare(NOD *head[],int n,int k,NOD **headFinal, NOD **tailFinal, int hip[])
{
	int x, dim_h=0;
	for(int i=1;i<=k;i++)
	{
		for(int j=1;j<=n/k;j++)
		{
			efc++;
			x=ExtrageCap(&head[i]);
			if(x!=NULL)
				h_push(hip,x,&dim_h);
		}
	}
	
	printf("heap: ");
	for(int i=1;i<=n;i++)
		printf("%d ",hip[i]);
	printf("\n");

	while(dim_h>0)
	{
		efc++;
		x=h_pop(hip,&dim_h);
		InserareSfarsit(headFinal, tailFinal, x);
	}

}

void generare_lista_i(NOD** head, NOD** tail, int dim)
{
	//creare vector de liste
	int i,l,valnod=0;

	valnod=rand()%100;
	for(l=1;l<=dim;l++)
	{
		valnod+=rand()%100;
		InserareSfarsit(head, tail, valnod);
	}
}


int main()
{
	NOD *head[100],*tail[100],*headFinal,*tailFinal;
	int heap[MAX];
	int i,j,n,k;
	FILE *f;

	printf("Dati nr de elemente: ");
	scanf("%d",&n);
	printf("Dati nr de liste: ");
	scanf("%d",&k);

	for(i=1;i<=k;i++)	
	{
		CreareVida(&head[i],&tail[i]);
	}
	
	for(i=1;i<=k;i++)	
	{
		generare_lista_i(&head[i],&tail[i],n/k);
	}

	//afisare liste
	for(i=1;i<=k;i++)
	{
		printf("lista %d:",i);
		Afisare(head[i]);
		printf("\n");
	}

	CreareVida(&headFinal,&tailFinal);
	interclasare(head,n,k,&headFinal,&tailFinal,heap);

	printf("lista finala: ");
		Afisare(headFinal);
		printf("\n");

	system("pause");

	//fclose(f);
}