#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <math.h>
#define NMAX 10000
int a[NMAX]={0};
int b[NMAX]={0};
//Tema Nr. 2: Analiza ?i Compararea a doua metode de construire Heap: �De jos �n sus� (Bottom-up) vs. �De sus �n jos� (Top-down)

char nume_fis1[50]="HeapBottomUp.xls";
char nume_fis2[50]="HeapTopDown.xls";

FILE *f1, *f2;
int dim_heap;

int left(int* i)
{
	return 2 * (*i);
}

int right(int* i)
{
	return 2* (*i) + 1;
}

int parent(int* i)
{
	return ((*i)/2);
}



void afiseazaheap(int* a,int n,int x, int d)
{

	if (x< n) {
	int st=2*x+1;
	int dr=2*x+2;
	afiseazaheap(a,n,dr,d+1);
	for (int x=0;x<d;x++)
		printf("     ");
	printf("%d\n",a[x]);
	afiseazaheap(a,n,st,d+1);
	}
}


void ReconstituieHeap(int* a, int i, int dimensiune,int& atr, int& comp )
{   
	int aux,maxim;
	int fiu_stang = left(&i);
	int fiu_drept = right(&i);
	comp=comp+2;
	if (fiu_stang < dimensiune && a[fiu_stang] > a[i]) {
		maxim = fiu_stang;
	}	
	else {	
		maxim=i;
	}
	if (fiu_drept < dimensiune && a[fiu_drept] > a[maxim])
		{	maxim = fiu_drept;}
	comp++;
	if(maxim!=i)
	{   
		aux=a[i];
		a[i]=a[maxim];
		a[maxim]=aux;
		atr+=3;
		ReconstituieHeap(a,maxim,dimensiune,atr,comp);
	} 
}   
/****************BoTTOM - UP********************/
void insert_heap(int *a,int cheie, int &dim_heap, int &comp, int &attr)//o(logn)
{
	dim_heap++;
	int i=dim_heap;
	comp++;
	while(i>1 && a[int(i/2)] < cheie)
	{	comp++; attr+=2;
		a[i]=a[int(i/2)];
		i=i/2;
		a[i]=cheie;
	}

}


/********************************Bottom-uP*****************************/
void ConstruiesteHeap1( int* a, int dimensiune,int& atr, int& comp )
{   
	int i;
	for (i = dimensiune/2; i >= 1; i--)
		ReconstituieHeap(a, i, dimensiune,atr,comp);

}   

/****************TOP - DOWn*****************/
void ConstruiesteHeap2( int* a,int dimensiune,int& atr, int& comp )//O(nlogn)
{   
	dim_heap=1;
	for(int j=2;j<=dimensiune;j++)
	{
		insert_heap(a,a[j],dim_heap,comp,atr);//insert O(log n)
	}
} 


void genereazaSirRandom(int n,int *a)
{
	 int ok=1;
	
	srand(time(NULL));
		for(int j=1;j<=n;j++)
		{
					a[j]=rand()%n;	
		}
}

void afiseaza(FILE *f,int n, int atr, int comp)
{

	fprintf(f,"%d\t %d\n",n,atr+comp);

}

void main()
{   
	f1=fopen(nume_fis1,"w");
	f2=fopen(nume_fis2,"w");


	//fprintf(f1,"n\t na+nc(bu) \t na+nc(td) \n");
	//fprintf(f2,"n,na+nc(td)\n");


	    int n=10;
		int	atr1=0,comp1=0;
		int	atr2=0,comp2=0;
		int comp=0, atr=0;
		genereazaSirRandom(n,a);
			
			for (int j=0;j<n;j++)
				b[j]=a[j];

				ConstruiesteHeap1(a, n, atr, comp);
				printf("\nBottom Up:\n");
				afiseazaheap(a,n,0,0);

				ConstruiesteHeap2(b, n, atr, comp);
				printf("\nTop Down:\n");
				afiseazaheap(b,n,0,0);


		for(int n=100;n<=NMAX;n+=200)
		{

			for (int k=1; k<=5; k++)
			{			
			genereazaSirRandom(n,a);
			
			for (int j=0;j<n;j++)
				b[j]=a[j];
			ConstruiesteHeap1(a, n, atr, comp );
			atr1=atr1+atr;
			comp1=comp1+comp;
							

			atr=0;
			comp=0;

			ConstruiesteHeap2( b,n,atr,comp);
			atr2=atr2+atr;
			comp2=comp2+comp;
			
			}
			//fprintf(f1,"%d\t %d\t %d\n",n,atr,comp);
			//afiseaza(f1,n,atr1/5,comp1/5);
			//afiseaza(f2,n,atr2/5,comp2/5);

		atr1=0;
		atr2=0;
		comp1=0;
		comp2=0;

}
		fclose(f1);
		fclose(f2);

		getch();
}
