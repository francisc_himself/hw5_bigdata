/*

	***ANONIM*** ***ANONIM*** Daniela 						
Concluzii, interpretari:
	            Functia MAKE-SET are eficienta O(1)
				Functia UNION are eficienta O(n)
				Prin utilizarea listelor inlatuite si a euristicii ponderate se reduce timpul de executie al algoritmului
				Folosind euristica ponderata eficienta intregului algoritm este O(m + n lg n)   m=muchii   n= varfuri 
                Fiecare multime disjuncta este implementata folosind o lista simplu inalntuita, fiecare avand 
				un camp in plus in care se retine adresa elementului reprezentativ al multimii
                                    
*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>

#define VMAX 10001
#define EMAX 60001

int multime[VMAX];
int urm[VMAX];
int ult[VMAX];
int lung[VMAX];

typedef struct
{
	int x;
	int y;
} muchie;

muchie muchii[EMAX];
int nre;
int nrop;

void generate(int n)
{
	nre = 0;
	int i,x,y;

	for(i = 0; i< n; i++)
	{
		x = (rand()%(VMAX-1))+1;
		y = (rand()%(VMAX-1))+1;

		if (x != y)
		{
			muchii[nre].x = x;
			muchii[nre].y = y;
			nre++;
		}
	}
}

void init()
{
	int i;

	for(i=0;i<VMAX;i++)
	{
		multime[i] = -1;
		urm[i] = -1;
		ult[i] = i;
		lung[i] = 0;
	}
}

void constr_multime(int x)
{
	nrop++;
	multime[x] = x;
	lung[x] = 1;
}

int gaseste_multime(int x)
{
	nrop++;
	return multime[x];
}

void reuniune(int x,int y)
{
	nrop++;
	int x1 = gaseste_multime(x);
	int y1 = gaseste_multime(y);
	int aux;

	if(x1 == y1)
		return;

	if(lung[x1] < lung[y1])
	{
		aux = x1;
		x1 = y1;
		y1 = aux;
	}
	int p = y1;

	while(p != -1)
	{
		multime[p] = x1;
		p = urm[p];
	}

	urm[ult[x1]] = y1;
	ult[x1] = ult[y1];
	lung[x1] += lung[y1];
}

void componente()
{
	init();
	int i;

	for(i=1;i<VMAX;i++)
	{
		constr_multime(i);
	}

	for(i=0;i<nre;i++)
	{
		reuniune(muchii[i].x, muchii[i].y);
	}
}

void afisare_componente(int k)
{
	char nume_fis[50];
	
	int i,p;

	for(i=1;i<VMAX;i++)
	{
		if(multime[i] == i)
		{
			printf("Multimea %d:\n",i);
			p = i;

			while(p != -1)
			{
				printf("%d ",p);
				p = urm[p];
			}
		
		}
	}

}

int main()
{
	srand(time(NULL));
	
	int e[] = {10000};
	int i;
	 FILE* pf;

 pf=fopen("fis.txt","w");
	for(i=10000;i<60000;i=i+1000)

	{
		nrop = 0;
		generate(e[i]);
		componente();
		afisare_componente(i);
		fprintf(pf,"nr muchii = %d, nr operatii = %d\n",e[i],nrop);
	}
	fclose(pf);
	getch();

	return 0;
}
