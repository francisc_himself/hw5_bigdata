#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <math.h>

int N = 9967;	 //size-ul hastable-ului
int n;		     //numarul de elemente
int c1 = 3, c2=5;  //constantele pentru calculul functiei de hash
int m = 17;		   //constanta de impartire
int mrand = 3000;  //numarul de elemente de cautat
float nrAcc;	    //numarul de celulate accesate in timpul cautarii
int NIL = -1;


//verificare patratica
int h (int T[9967], int k, int i){
	return ((k % m) + c1*i + c2*i*i)%N;
}

int insertH (int T[9967], int k){
	int i = 0; 
	int ind;
	do {
		ind = h (T, k, i);
		if (T[ind] == NIL){
			return ind;
		}else{
			i++;
		}
	}while (i != N-1);
	printf ("ATENTIE! Capacitate depasita!\n");
	return NIL;
}

int searchH (int T[9967], int k){
	int i = 0;
	int ind;
	do {
		ind = h (T, k, i);
		nrAcc ++;
		if (T[ind] == k){
			return ind;
		}
		i++;
	}while (T[ind] != NIL && i != N-1);
	printf ("ATENTIE! Elementul %d nu s-a gasit!\n", k);
	return NIL;
}


int main ()
{
	int T[9967];
	int ind, k, v;
	int n;
	float a;

	a = 0.8;    //factor de umplere
	N = 20;     //numar de elemente din hashtable
	c1 = 1;     //constanta1
	c2 = 2;     //constanta2
	m = 3;      //numarul de elemente de cautat


	char choose;
	printf("D - Demonstratie     R - Rezolvare\n");
	scanf("%c", &choose);
	switch(choose){
	case 'D':
		//inserez alfa * N elemente
		n = a * N;
	
		//initializez hashtable-ul
		for (k = 0; k < N; k++){
			T[k] = NIL;
		}

		//inserez elementele
		for (k = 0; k < n; k++){
			v = rand() % 50 + 1;	//intervalul intre 1 si 100
			ind = insertH (T, v);
			if (ind != NIL){
				T[ind] = v;
			}
		}
		printf("Factorul de umplere: ");
		printf ("alfa = %f\n", a);
		for (k = 0; k < N; k++){
			printf ("T[%d] = %d ", k, T[k]);
		}
		printf ("\n");
		//printf ("\n");
		printf ("Valoarea de cautat: ");
		printf ("%d\n", v);
		printf ("Valoarea %d se gaseste la cheia: ", v);
		printf ("%d\n", searchH(T,v)); 
		printf ("\n");

		v = 25;
		printf ("Factor de umplere: ");
		printf ("alfa = %f\n", a);
		for (k = 0; k < N; k++){
			printf ("T[%d] = %d ", k, T[k]);
		}
		printf ("\n");
		//printf ("\n");
		printf ("Valoarea de cautat: ");
		printf ("%d\n", v);
		printf ("Valoarea %d se gaseste la cheia: ", v);
		printf ("%d\n",searchH(T,v)); 
		printf("\n");
	
		v = 333;
		printf ("Factor de umplere: ");
		printf ("alfa = %f \n", a);
		for (k = 0; k < N; k++){
			printf ("T[%d]=%d ", k, T[k]);
		}
		printf ("\n");
		printf("Valoarea de cautat: ");
		printf ("%d\n",v);
		printf ("Valoarea %d se gaseste la cheia: ", v);
		printf ("%d\n",searchH(T,v)); 
		break;

	case 'R':
		float accNoMaxFound, accNoMedFound, accNoMaxNFound, accNoMedNFound;
		int T[9967], v[10000], g, ind;
		int x, y, key;		//y este indexul prin hash
		srand(time(NULL));
		float a;		//factor de umplere
		int cont = 0;
		float control[5];
		control[0] = 0.8;
		control[1] = 0.85;
		control[2] = 0.9;
		control[3] = 0.95;
		control[4] = 0.99;
	

		FILE *df;
		df = fopen ("fisier.csv","w");

	
		for(cont = 0; cont < 5; cont++){
			a = control[cont];
			g = 0;

			//inserez n = alfa * 9967 elemente (7973, 8471, 8970, 9468, 9867);
			n = a * N;
	
			//initializez hashtable-ul cu NIL = -1
			for (x = 0; x < N; x++){
				T[x] = NIL;
			}

			//inserez in hash
			for (x = 0; x < n; x++){
				key = rand() % 10000 + 1;	//intervalul 1 - 10000
				v[++g] = key;
				y = insertH (T, key);
				if (y != NIL){
					T[y] = key;
				}
			}
			//caut in prima jumatate 
			accNoMaxFound = 0;
			accNoMedFound = 0;
			for (x = 1; x <= mrand / 2; x++){
				ind = rand()%n + 1;
				nrAcc = 0; 
				searchH (T, v[ind]);
				accNoMedFound += nrAcc;
				if (nrAcc > accNoMaxFound){
					accNoMaxFound = nrAcc;
				}
			}
			accNoMedFound = accNoMedFound / (mrand / 2);

			//caut in a doua jumatate
			accNoMaxNFound = 0;
			accNoMedNFound = 0;
			int srch;
			for (y = 1; y <= mrand - x; y++){
				key = rand() % 10000 + 10001;
				nrAcc = 0; 
				srch = searchH (T, key);
				accNoMedNFound += nrAcc;
				if (nrAcc > accNoMaxNFound){
					accNoMaxNFound = nrAcc;
				}
			}
			accNoMedNFound = accNoMedNFound / (mrand / 2);
			fprintf (df,"%0.2f %0.2f %0.2f %0.2f %0.2f\n", a, accNoMedFound, accNoMaxFound, accNoMedNFound, accNoMaxNFound);
			fclose(df);
		}
		break;
	}
	getche ();
	return 0;
}