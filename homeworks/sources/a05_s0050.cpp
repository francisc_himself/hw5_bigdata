﻿#include <time.h>
#include <list>
#include <string.h>
#include <conio.h>
#include <stdlib.h>
#include "Profiler.h"

/* ***ANONIM*** ***ANONIM***  grupa ***GROUP_NUMBER***
Analizând complexitatea în medie,si dacă presupunem că funcţia hash va distribui uniform valorile,
timpul de execuţie va fi O(1+n/m), unde n/m reprezinta factorul de încărcare a tabelei determinat de numarul pozitiilor ocupate din Tabela Hash.
Cu cat secvenţa de poziţii ocupate devine mai mare, cu atat şi timpul mediu de execuţie al căutării creşte.
*/
#define HASH_SIZE 9973
#define SEARCH_SIZE 3000


int Ta[HASH_SIZE];
int samples[HASH_SIZE+SEARCH_SIZE/2];
int indices[SEARCH_SIZE/2+1];
int accese,accese2;
int h(int k,int i)
{
	int x;
	x=k%HASH_SIZE  +1*i+1*i*i;
	x=x%HASH_SIZE;

	return x;
}

int Hash_insert(int *T,int k)
{
	int i=0;
	do
	{
		int j=h(k,i);
		if (T[j]==-1)  {T[j]=k; return j;}
		else i=i+1;
	}while(i!=HASH_SIZE);
	perror("hash table overflow");
	return 0;
}

int Hash_search(int *T,int k) //intoarce pozitia unde s-a gasit
{
	int i=0;
	int j;
	do
	{
		j=h(k,i);
		if (T[j]==k) { 
			accese=i+1;
			return j;}
		i=i+1;
	}while((i!=HASH_SIZE)&&(T[j]!=-1));
	accese2=i;
	return -1;
}

int main()
{  int v[10]={0,1,2,3,6,5,4,7,8,9};

memset(Ta,255,sizeof(Ta));
printf("%d,\n",Ta[2]);
for (int i=0;i<10;i++)
{
	Hash_insert(Ta,v[i]);
}
for (int i=0;i<50;i++)
{
	printf("%d,",Ta[i]);
}
if (Hash_search(Ta,5)!=-1) printf("\nS-a gasit elem pe poz: %d\n",Hash_search(Ta,5));


///////////////////

FILE *f=fopen("rez.csv","w");
fprintf(f,"caz,efortMediuG,maxEfortG,efortMediuN,maxEfortN\n");
memset(Ta,255,sizeof(Ta));

double n=0.8;//printf("n=%f\n", n);
n=n*HASH_SIZE; //factor de umplere 80%
//printf("n=%d", (int)n);

for (int caz=0;caz<5;caz++)
{	memset(Ta,255,sizeof(Ta));
int acceseGasit=0;
int acceseNegasit=0;
int maxEfortG=0;
int maxEfortN=0;
double efortMediuG=0;
double efortMediuN=0;	

int cazuri;
if (caz==0){
	n=(HASH_SIZE*80)/100;cazuri=80;}
if (caz==1){
	n=(HASH_SIZE*85/100);cazuri=85;}
if (caz==2){
	n=(HASH_SIZE*90/100);cazuri=90;}
if (caz==3){
	n=(HASH_SIZE*95/100);cazuri=95;}
if (caz==4){
	n=(HASH_SIZE*99/100);cazuri=99;}


FillRandomArray(samples,(int)n+SEARCH_SIZE/2,1,100000,true,0);

int depasire=0;
for(int i=0;i<int(n);i++)
{
	if(Hash_insert(Ta,samples[i])==0){ depasire++;} 

}
//printf("\n%d",depasire);

//elemente care se gasesc 1500
accese=0;accese2=0;
FillRandomArray(indices,SEARCH_SIZE/2,0,int (n-1),true,0);
for(int i=0;i<SEARCH_SIZE/2+1;i++)
{	accese=0; //reinitializam la fiecare cautare	
Hash_search(Ta,samples[indices[i]]);
efortMediuG+=accese;
if(accese>maxEfortG) maxEfortG=accese;

if(accese2!=0)printf("\nverificare negasite%d\n",accese2);

}

//elemente care nu se gasesc 1500
accese2=0;accese=0;
FillRandomArray(indices,SEARCH_SIZE/2,int (n),int(n)+SEARCH_SIZE/2-1,true,0);


for(int i=0;i<SEARCH_SIZE/2;i++)
{	accese2=0;
Hash_search(Ta,samples[indices[i]]);
efortMediuN+=accese2;
if(accese2>maxEfortN) maxEfortN=accese2;


if(accese!=0)printf("\nverificare gasite%d\n",accese);
}
char procent='%';
fprintf(f,"%d %c,%.2f,%d,%.2f,%d \n",cazuri,procent,efortMediuG/(SEARCH_SIZE/2),maxEfortG,efortMediuN/(SEARCH_SIZE/2),maxEfortN);
}
}