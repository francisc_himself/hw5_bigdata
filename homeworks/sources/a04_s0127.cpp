/**
 **	Assignment No. 4: Merge k Ordered Lists Efficiently
 **
 **		Name: ***ANONIM*** ***ANONIM*** Mihai
 **		Group: ***GROUP_NUMBER***
 **
 **
 **	Implementation:
 **
 **	You are required to implement correctly and efficiently an O(nlogk) method for merging k sorted sequences, 
 **	where n is the total number of elements. (Hint: use a heap, see seminar no. 2 notes).
 **	Implementation requirements:
 **		Use linked lists to represent the k sorted sequences and the output sequence
 **
 **		Input: k ordered lists of numbers
 **		Output: a permutation of the union of the input sequences
 **	
 **		Start date: 29.03.2013
 **		Finish date: 02.04.2013
 **
 **			Conclusions:
 **
 ** In the first case, when we take 3  constant values for k (k1=5, k2=10, k3=100), for generating k random sorted 
 ** lists for each value of k so that the sum of elements in all the lists n varies between 100 and 10000, we can observe
 ** all three are having linear growths. 
 **
 ** In the second case, we set n = 10.000. The value of k must vary between 10 and 500 with an increment of 10.
 ** We generate k random sorted lists for each value of k so that the sum of elements in all the lists is 10000.
 ** As a conclusion, we cam observe from the graphics that the growth is a logarithmic one in this case, so the
 ** implementation is correct. We expected an O(nlogk)complexity, for merging k sorted sequences.
 **
 **
 **/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define INFINITY 1000000

typedef struct var
{
	int value;
	int arrayIndex;
} Node;

Node *heap;
int *positionArray;
int **arrays;
int *mainArray;
int k=0,n=0;
int noAssignments, noComparisons;

int parent(int i);
int left(int i);
int right(int i);
void resetAC();
void generateArrays();
void buildHeap(int heapSize);
void minHeapify(int i, int heapSize);
void insert(Node element);
void mergeArrays();
Node getElementFromArray(int i, int currentIndex);
void freeArrays();

int main()
{
   /**
	**	Sample Input for algorithm works on a small-sized input (e.g. k=4, n=20).
	**/

	n=20;
	k=4;

	generateArrays();
	mergeArrays();
	for(int i=0; i<20; i++)
	{
	
	printf("%d ",mainArray[i]);
	}
	freeArrays();
	printf("\n");

	getch();

   /**
	**	Constant value for k (k1=5). Generate k random sorted lists for each value of k so that 
	**	the sum of elements in all the lists n varies between 100 and 10000.
	**/

	FILE* a=fopen("k5.csv","w+");
	
	k=5;
	for(int i=100;i<10000;i+=100)
	{
		n=i;	
		resetAC();
		generateArrays();
		mergeArrays();
		freeArrays();
		printf("In case 1, when k= 5 and n is %d\n",n);
		fprintf(a,"%d,%d,%d,%d\n", i,noAssignments,noComparisons,noAssignments+noComparisons);
	}

	fclose(a);
	getch();

   /**
	**	Constant value for k (k2=10). Generate k random sorted lists for each value of k so that 
	**	the sum of elements in all the lists n varies between 100 and 10000.
	**/

	FILE* b=fopen("k10.csv","w+");
	
	k=10;
	for(int i=100;i<10000;i+=100)
	{
		n=i;	
		resetAC();
		generateArrays();
		mergeArrays();
		freeArrays();
		printf("In case 1, when k= 10 and n is %d\n",n);
		fprintf(b,"%d,%d,%d,%d\n", i,noAssignments,noComparisons,noAssignments+noComparisons);
	}

	fclose(b);

	getch();
	
   /**
	**	Constant value for k (k3=100). Generate k random sorted lists for each value of k so that 
	**	the sum of elements in all the lists n varies between 100 and 10000.
	**/

	FILE* c=fopen("k100.csv","w+");
	
	k=100;
	for(int i=100;i<10000;i+=100)
	{
		n=i;	
		resetAC();
		generateArrays();
		mergeArrays();
		freeArrays();
		printf("In case 1, when k= 100 and n is %d\n",n);
		fprintf(c,"%d,%d,%d,%d\n", i,noAssignments,noComparisons,noAssignments+noComparisons);
	}

	fclose(c);
	getch();
	
   /**
	**	Set n = 10.000. The value of k must vary between 10 and 500 with an increment of 10; 
	**	generate k random sorted lists for each value of k so that the sum of elements in all the lists is 10000.
	**/

	FILE* f=fopen("NFixed.csv","w+");
	
	n=10000;

	for(int i=10;i<500;i+=10)
	{
		k=i;	
		resetAC();
		generateArrays();
		mergeArrays();
		freeArrays();
		printf(" In case 2 when n = 10.000 and the value of k is %d\n",k);
		fprintf(f,"%d,%d,%d,%d\n", i,noAssignments,noComparisons,noAssignments+noComparisons);
	}

	fclose(f);

	return 0;
}

void freeArrays()
{
	for(int i=0;i<k;i++)
	{
		free(arrays[i]);
	}

	free(arrays);
	free(heap);
	free(positionArray);
}

void resetAC()
{
	noAssignments=0;
	noComparisons=0;
}

void generateArrays()
{
	srand(time(0));

	int slice = 100;

	if(heap!=NULL)
		heap=NULL;
	if(arrays!=NULL)
		arrays=NULL;
	if(mainArray!=NULL)
		mainArray=NULL;
	if(positionArray!=NULL)
		positionArray=NULL;

	srand(time(0));

	mainArray=(int*)malloc((k*n)*sizeof(int));
	heap=(Node*)malloc((k+1)*sizeof(Node));
	arrays=(int**)malloc((k)*sizeof(int*));
	positionArray=(int*)malloc((k)*sizeof(int));

	for(int i=0;i<k;i++)
		arrays[i]=(int*)malloc((n)*sizeof(int));

	for(int i=0;i<k;i++)
	{
		arrays[i][0] = rand() % slice;
		for(int j=1;j<n;j++)
		{
			arrays[i][j] = rand() % slice + arrays[i][j-1] ;//j*slice;
		}
	}

}

void mergeArrays()
{
	for(int i=0;i<k;i++)
	{
		Node element;
		element.value=arrays[i][0];
		element.arrayIndex=i;
		heap[i+1]=element;
		positionArray[i]=0;
	}

	buildHeap(k);
	
	for(int i=0;i<k*n;i++)
	{
		noAssignments++;
		mainArray[i] = heap[1].value;
		positionArray[heap[1].arrayIndex]++;
		insert(getElementFromArray(heap[1].arrayIndex,positionArray[heap[1].arrayIndex]));
	}

}

Node getElementFromArray(int i, int currentIndex)
{
	Node element;
	element.value=INFINITY;
	element.arrayIndex=-1;

	if(currentIndex<n && i>=0)
	{
		element.value = arrays[i][currentIndex];
		element.arrayIndex = i;
	}

	return element;
}

void insert(Node element)
{
	noAssignments++;
	heap[1]=element;
	minHeapify(1,k);
}

void buildHeap(int heapSize)
{
	for(int i=heapSize/2;i>=1;i--)
	{
		minHeapify(i,heapSize);
	}
}

void minHeapify(int i, int heapSize)
{
	int l=left(i);
	int r=right(i);
	int smallest=0;

	noComparisons += 5;
	noAssignments += 3;

	if(l<=heapSize && heap[l].value<heap[i].value)
		smallest=l;
	else
		smallest=i;

	if(r<=heapSize && heap[r].value<heap[smallest].value)
	{
		smallest = r;
		noAssignments += 1;
	}

	if(smallest!=i)
	{
		noAssignments += 3;

		Node aux = heap[i];
		heap[i] = heap[smallest];
		heap[smallest] = aux;

		minHeapify(smallest, heapSize);
	}
}

int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return 2*i+1;
}

int parent(int i)
{
	return (int)(i/2);
}