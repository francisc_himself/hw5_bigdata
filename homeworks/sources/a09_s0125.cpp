/*
Student: ***ANONIM*** ***ANONIM*** ***ANONIM*** ***ANONIM***
College: Technical University of Cluj Napoca
Date: 21 May 2013
Subject: BFS
*/

#include <stdio.h>
#include <conio.h>
#include <queue>
#include <cstdlib>
#include <time.h>
#include <iostream>
#include <fstream>
using namespace std;

ofstream myfile;

#define INFINITY 10000
#define WHITE 1
#define GRAY 2
#define BLACK 3

static int countOp =0;

struct BFSNode{
int color;
int distance;
BFSNode* pred;
};

struct GraphNode
{
int key;
BFSNode* bfs;
GraphNode* adjacent[10000];
int adjCount;
};


//struct Edge{
//    GraphNode* v1;
//    GraphNode* v2;
//};

int nodeCount;
int edgeCount;
GraphNode* vertArr[8000];
//Edge* edgeArr[10000];
BFSNode* BFSArr[8000];

void BFS(GraphNode* s){
queue<GraphNode*> q;
//for each vertex u belongs to the graph vertices, except the source
if (s->bfs->color!=BLACK){
/*
for (int u=0; u<nodeCount;u++)
{

vertArr[u]->bfs->color = WHITE;
vertArr[u]->bfs->distance = INFINITY;
vertArr[u]->bfs->pred = NULL;
}
*/
//printf("Source: %d", s->key);
s->bfs->color = GRAY;
s->bfs->distance = 0;
s->bfs->pred = NULL;
countOp++;

//Q = NULL;
while (!q.empty()) {q.pop(); countOp++;}
q.push(s); countOp++;

while (!q.empty()){
countOp++;
GraphNode* u = q.front(); countOp++;
q.pop();
countOp++;
printf("\n%d popped\n",u->key);
//for each vertex adjacent to u
for (int x=0; x<u->adjCount; x++)
{
GraphNode* v = u->adjacent[x];
countOp++;
if (v->bfs->color == WHITE){
v->bfs->color = GRAY; countOp++;
v->bfs->distance = u->bfs->distance + 1; countOp++;
v->bfs->pred = u->bfs; countOp++;
q.push(v); countOp++;
}
}
u->bfs->color = BLACK; countOp++;
}
}
}

void generateGraph(){
for(int x=0; x<nodeCount; x++) //generate nodes
{
free(vertArr[x]);

vertArr[x] = new GraphNode();
vertArr[x]->key = x;
vertArr[x]->adjCount = 0;
//vertArr[x]->adjacent = NULL;
vertArr[x]->bfs = new BFSNode();
}

for(int x=0; x<edgeCount; x++){ //generate edges
//printf("%d ",x);
int retry,a,b;
do{
retry = 0;
a = rand()%nodeCount;
do {b = rand()%nodeCount;
} while (a==b);

//printf("%d %d", a, b);

for(int j=0; j<vertArr[a]->adjCount; j++){
if (vertArr[a]->adjacent[j] == vertArr[b]){
retry = 1;// printf("Retrying ");
}
}

//for(int j=0; j<vertArr[b]->adjCount; j++){
//	if (vertArr[b]->adjacent[j] == vertArr[a]){
//	retry = 1; printf("Retrying ");
//	}
//}

} while (retry == 1);

vertArr[a]->adjacent[vertArr[a]->adjCount]=vertArr[b];
vertArr[a]->adjCount++;
}

}

void prettyPrint(GraphNode* root, int height){ 
printf("\n"); 
if (root!=NULL){ 
for (int x=0; x<height; x++){printf(" ");} 
printf("%d", root->key); 

for(int i=0; i<root->adjCount; i++){ 
prettyPrint(root->adjacent[i],height+1); 

}
}
}
// for(int i=0; i<root->count; i++){ 
//printf("%d ", root->children[i]->key); //for (int x=9; x>height;x=x-2) printf(" "); 
//} 


int main(){
srand(time(NULL)); //new seed
nodeCount = 1000;
edgeCount = 5000;


for(int x=0; x<nodeCount; x++) //generate nodes
{
vertArr[x] = new GraphNode();
vertArr[x]->key = x;
vertArr[x]->adjCount = 0;
//vertArr[x]->adjacent = NULL;
vertArr[x]->bfs = new BFSNode();
}

for (int u=0; u<nodeCount;u++)
{
vertArr[u]->bfs->color = WHITE;
vertArr[u]->bfs->distance = INFINITY;
vertArr[u]->bfs->pred = NULL;
}

///*
vertArr[0]->adjacent[0] = vertArr[1];
vertArr[0]->adjCount++;
vertArr[1]->adjacent[1] = vertArr[2];
vertArr[1]->adjacent[0] = vertArr[3];
vertArr[1]->adjCount=2;
vertArr[2]->adjacent[0]=vertArr[4];
vertArr[2]->adjacent[1]=vertArr[9];
vertArr[2]->adjCount=2;
vertArr[3]->adjacent[0]=vertArr[5];
vertArr[3]->adjCount++;
vertArr[6]->adjacent[0]=vertArr[8];
//*/
// generateGraph();
//_getch();
prettyPrint(vertArr[0],0);
BFS(vertArr[0]);
_getch();
//*

/*
myfile.open ("bfs.csv",ios::out);
nodeCount = 100;
for (edgeCount = 1000; edgeCount <5000; edgeCount+=100)
{
countOp=0;
generateGraph();
for (int u=0; u<nodeCount;u++)
{
vertArr[u]->bfs->color = WHITE;
vertArr[u]->bfs->distance = INFINITY;
vertArr[u]->bfs->pred = NULL;
}
for(int i=0; i<nodeCount;i++){
//printf("%d ", i);
BFS(vertArr[i]);
}
myfile << nodeCount << "," << edgeCount << "," << countOp << endl;
}
edgeCount = 9000;
for (nodeCount = 100; nodeCount<200; nodeCount+=10)
{
countOp=0;
generateGraph();
for (int u=0; u<nodeCount;u++)
{
vertArr[u]->bfs->color = WHITE;
vertArr[u]->bfs->distance = INFINITY;
vertArr[u]->bfs->pred = NULL;
}
for(int i=0; i<nodeCount;i++){
//printf("%d ", i);

BFS(vertArr[i]);
}
myfile << nodeCount << "," << edgeCount << "," << countOp << endl;
}

//BFS(vertArr[0]);
_getch();
return 0;
*/
}