#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>


typedef struct nod{
	int cheie;
	struct nod *urm;
}NOD;

typedef struct edge{
	int p;
	int d;
}edge;

int p[100],c[100],d[100],f[100];	//p-predecesor, c-culoare ( 0-alb,1-gri,2-negru ), d -timp 1, f -timp terminare
NOD	*prim[100],*ultim[100];
int m,t,tmp,atr,cmp,atrm,cmpm;
edge e[150];
bool matr[100][100];				//matr reprezinta matricea de adiacenta a grafului ordonat generat aleatoriu

//filtru genereaza un numar intreg aleator intre 0 si h
int filtru(int h) {
	return  rand() % h ;
}

//generare_graf realizeaza generarea unui graf orientat cu numarul de muchii dat de parametrul z si varfuri k
void generare_graf(int z,int k)
{
	memset(matr,0,sizeof(matr));
	int contor=0;					
	while(contor<z)
	{	int nr1 =filtru(k);
	int nr2 =filtru(k);
	if((matr[nr1][nr2]==0) && (nr1!=nr2))
	{
		matr[nr1][nr2]=1;
		e[contor].p=nr1;
		e[contor].d=nr2;
		contor++;
	}
	}
}

/*Functia reprezentare are rolul de a transforma reprezentarea unui graf sub forma de matrice de adiacenta 
in reprezentare sub forma de liste de vecini.
*/
void reprezentare( int t, int v)
{ for(int i=0;i<v;i++)				//initializam capetele listelor
{
	prim[i]=(NOD*)malloc(sizeof(NOD));
	prim[i]->cheie=i;
	prim[i]->urm=NULL;
	ultim[i]=prim[i];
	atr+=2;
}
for(int i=0;i<t;i++)	//parcurgand vectorul de muchii, adaugam vecinii in liste
{
	NOD *p;
	p=(NOD*)malloc(sizeof(NOD));
	p->cheie=prim[e[i].d]->cheie;
	p->urm=NULL;
	ultim[e[i].p]->urm=p;
	ultim[e[i].p]=p;
	atr+=2;
}
}

void dfs_vizita(NOD *v)
{
	c[v->cheie]=1;
	d[v->cheie]=tmp;
	tmp++;
	NOD* q;
	q=prim[v->cheie]->urm;
	atr+=3;
	cmp++;
	while(q!=NULL)
	{  
		if(c[q->cheie]==0)
		{  if(m==1)
			 //muchie de arbore deoarece varful q a fost descoperit explorand muchia (v,q); culoare varf = alb
			 printf("%d		%d		%s\n",v->cheie,q->cheie,"muchie arbore");
			p[q->cheie]=v->cheie;
			atr++;
			dfs_vizita(q);
			}
		else
			if(m==1){cmp++;
		if(c[q->cheie]==1)
		{   //muchie inapoi deoarece varful q este un "stramos" a lui v; culoare varf = gri
			printf("%d		%d		%s\n",v->cheie,q->cheie,"muchie inapoi");
		}
		else{
			cmp++;
			if(d[v->cheie]<d[q->cheie])
				//muchie inainte conecteaza varful v cu un descendent q, dar nu este muchie arbore; culoare varf = negru
				printf("%d		%d		%s\n",v->cheie,q->cheie,"muchie inainte");
			else
				//unesc nodurile v si q,dar cu conditia ca unul sa nu fie stramosul celuilat; culoare varf = negru
				printf("%d		%d		%s\n",v->cheie,q->cheie,"muchie transversala");

		}
		}
		c[v->cheie]=2;
		f[v->cheie]=tmp;
		q=q->urm;
		atr+3;
		cmp+=2;
		tmp++;

	}


}
/* dfs are rolul de a parcurge graful orientat in adancime. La aceasta cautare, muchiile sunt explorate pornind din varful
v cel mai recent descoperit care mai are inca muchii neexplorate, care pleaca din el.Cand toate muchiile care pleaca din v
au fost explorate, cautarea "revine" pe propriile urme, pentru a explora muchiile care pleaca din varful din care v a fost 
descoperit.

*/
void dfs(int v)			
{ 
	memset(c,0,sizeof(c));
	memset(p,0,sizeof(p));
	tmp=0;
	atr++;
	for(int i=0;i<v;i++)
	{cmp++;
	if(c[i]==0)
	{ 		
		dfs_vizita(prim[i]);
	}

	}

}



void main()
{	FILE *pf,*pfm;
pf=fopen("fis.txt","w");
pfm=fopen("fism.txt","w");
srand ( time(NULL) );

/*Pentru o dimensiune a grafului variabila, intre 10 si 100 de noduri cu un pas de 10, 
 calculam numarul de atribuiri si comparatii. 
*/
for(int g=10;g<=100;g+=10)
{	
	generare_graf(g*2,g);
	atr=0;cmp=0;
	reprezentare(g*2,g);
	dfs(g);
	fprintf(pf,"%d		%d		%d		%d\n",g,atr,cmp,atr+cmp);
	for(int u=0;u<g;u++)
		free(prim[u]);
}

/*Pentru 5 rulari diferite, facem media sumei dintre atribuiri si comparatii, in cazul etichetarii muchiilor 
  unui graf orientat cu 10 noduri.
*/
m=1;
for(int i=0;i<5;i++)
{	
	generare_graf(20,10);
	atr=0;cmp=0;
	reprezentare(20,10);
	dfs(5);
	for(int u=0;u<10;u++)
		free(prim[u]);
	atrm+=atr;cmpm+=cmp;
	printf("\n\n");

}
fprintf(pfm,"%d		%d		%d		%d\n",10,atrm/5,cmpm/5,atrm/5+cmpm/5);
fclose(pf);	
fclose(pfm);
getch();
}