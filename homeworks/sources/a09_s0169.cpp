#include <cstdio>
#include <iostream>
#include <queue>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("Assignment 9");
#define white 0
#define gray 1
#define black 2
#define v_max 201
#define e_max 9001
#define inf INT_MAX
using namespace std;
typedef struct _gnod{
	int k;
	int parent;
	int distance;
	int color;
}gnod;
typedef struct _node {
	int key;
	struct _node *next;
}node;
node *adj[v_max];
gnod *graf[v_max];//,*v;
gnod graph[v_max];
int edges[e_max];
int n,m,u,v;
int tail,head,length=10000,testt,c;
void preety_print(gnod **graf)
{
	int i=0;
	for(int i=0;i<n;i++)
		if(graf[i]!=NULL)
		{
			printf("%d,%d,%d\n ",i,graf[i]->distance,graf[i]->color);
			i++;
		}
		printf("\n");
}
void enq(gnod * q[], gnod* s)
{
	q[tail]=s;
	if(tail==length-1)
		tail=0;
	else 
		tail=tail+1;
}

int deq(gnod * q[])
{
	int x;
	x=q[head]->k;
	if(head==length-1)
		head=0;
	else head=head+1;
	return x;
}
void bfs(int s){
	for(u=0;u<n;u++)//each vertex u din G.V-{s}
		//if(u!=graf[s]->k)
		{		//u=adj[u]->key;
			graf[u]=new gnod;
			graf[u]->k=u;
			graf[u]->color=white;
			graf[u]->distance=inf;
			graf[u]->parent=NULL;
			profiler.countOperation("operations",c,3);
		}
		printf("comp con in %d\n",graf[s]->k);
		graf[s]->color=gray;
		graf[s]->distance=0;
		graf[s]->parent=NULL;
		profiler.countOperation("operations",c,3);
		//gnod * q[v_max];
		//memset(q,0,n*sizeof(gnod*));
		queue<int> Q; 
		Q.push(s);
		while(!Q.empty()){
			u = Q.front(); Q.pop();
			node* aux = adj[u];
			while(aux!=NULL){ //each v din G.Adj[u]
				v=aux->key;
				if (graf[v]->color==white)
				{	profiler.countOperation("operations",c,4);
					printf("in %d\n",graf[v]->k);
					graf[v]->color=gray;
					graf[v]->distance=graf[u]->distance+1; 
					graf[v]->parent=u;
					Q.push(v);
				}
				aux=aux->next;
			}
			graf[u]->color=black;
			profiler.countOperation("operations",c);
			printf("          out %d,%d\n",u,graf[u]->distance);
		}
			//	preety_print(graf);
}

void generate(int n,int m){
	memset(adj,0,n*sizeof(node*));
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++){
		int a=edges[i]/n;
		int b=edges[i]%n;
		node *nod=new node;
		nod->key=b;
		nod->next=adj[a];
		while(a==nod->key)
			a=rand()%n;
			adj[a]=nod;
	}
}

void print_adj(node * a[])
{
	for(int i=0;i<n;i++)
	{
		node*aux=a[i];
		printf("%d",i);
		while(aux!=NULL)
		{
			printf("->%d",aux->key);
			aux=aux->next;
		}
		printf("\n");
	}
}
void test(){
	n=7;m=15;
	generate(n,m);
	print_adj(adj);
	for(int i=0;i<n;i++){
		head=tail=0;
		bfs(i);
		printf("\n\n");

	}
}
void vary_v(){
	m=9000;
	for(n=100;n<=200;n+=10)
	{	
		c=n;
		printf("n = %d\n",n);
		generate(n,m);
		for(int i=0; i<n;i++){
			head=tail=0;length=n;
			bfs(i);
		}
	}
	profiler.createGroup("Vary vertices","operations");
	profiler.showReport();
}
void vary_e(){
	n=100;
	for(m=1000;m<=5000; m+=100)
	{
		c=m;
		printf("m= %d\n",m);
		generate(n,m);
		for(int i=0; i<n;i++){
			head=tail=0;length=n;
			bfs(i);
		}
	}
	profiler.createGroup("Vary edges","operations");
	profiler.showReport();
}
int main()
{
	test();
	//vary_e();
	//vary_v();
	_getch();
	return 0;
}