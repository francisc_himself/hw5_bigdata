#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

#define white 0
#define gray 1
#define black 2
#define inf 99999

typedef struct Struct
{
	int color, d, parent,f;
} Struct;

typedef struct Node
{
    int val;
    struct Node* next;
}NODE;

NODE *first[60001], *last[60001];
Struct Struct[60001];
int timp;
int nr_operatii;
bool sort=true;

int Q[60001];

void DFS_visit (int vertex)
{
	Struct[vertex].color=gray;
	Struct[vertex].d=timp;
	timp++;
	Node *aux;
	aux=first[vertex];
	nr_operatii+=4;
	while (aux!=NULL)
		{
			int p=aux->val;
			aux=aux->next;
			nr_operatii+=3;
			if(Struct[p].color==white)
				{
					nr_operatii++;
					Struct[p].parent=vertex;
					printf("(%d,%d) Muchie arbore:\n", vertex, p);
					printf("nod afisat:  %d\n",p);
					DFS_visit(p);
				}
			else
				{
					nr_operatii++;
					if (Struct[p].color==gray)
						{
							sort=false;
							printf("(%d,%d) Muchie inapoi\n", vertex, p);
						}
					else
						{
							nr_operatii++;
							if (Struct[p].f<Struct[vertex].d)
								{printf("(%d,%d) Muchie transversala\n", vertex, p);
							}
							else 
								{printf("(%d,%d) Muchie inainte\n", vertex, p);

							}
						}
				}
			}
			Struct[vertex].color=black;
			Struct[vertex].f=timp;
			timp++;
			nr_operatii+=3;
}
void DFS(int n)
{
	int u;
	for(u = 1 ; u <= n ; u++)
	{
		Struct[u].color=0;
		Struct[u].parent=0;
	}
	for( u = 1 ; u <= n ; u++)
	{
		nr_operatii++;    
		if(Struct[u].color==0)
		{
			DFS_visit(u);
		}
	}
}
bool duplicate (int x, int y)
{
	NODE *p;
	p=first[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false;
		p=p->next;
	}
	return true;
}

void createAdjacencyList(int nr_varfuri,int nr_muchii)
{
	int i,u,v;
	for(i=0;i<=nr_varfuri;i++)
		first[i]=last[i]=NULL;
	i=1;
	while (i<=nr_muchii)
	{
		u=rand()%nr_varfuri+1;
		v=rand()%nr_varfuri+1;
		if (u!=v)
		{
				if(first[u]==NULL)
				{
					first[u]=(NODE *)malloc(sizeof(NODE *));
					first[u]->val=v;
					first[u]->next=NULL;
					last[u]=first[u];
					i++;
				}
				else {
						if (duplicate (u, v) && duplicate(v,u))
						{
							NODE *p;
							p=(NODE *)malloc(sizeof(NODE *));
							p->val=v;
							p->next=NULL;
							last[u]->next=p;
							last[u]=p;
							i++;
						}
					}
		}
	}
}

void print(int nr_varfuri)
{
	int i;
	NODE *p;
	for(i=1;i<=nr_varfuri;i++)
	{
		printf("%d : ",i);
		p=first[i];
		while(p!=NULL)
		{
			printf("%d,",p->val);
			p=p->next;
		}
		printf("\n");
	}
}


void main()
{
	int nr_varfuri=0,nr_muchii=0;

	srand(time(NULL));
	createAdjacencyList(5, 10);
	DFS(1);
	print(5);

	/*int i;
	FILE *f1=fopen("D:\\tema101.xls","w");
	FILE *f2=fopen("D:\\tema102.xls","w");

	for(i=1000;i<5000;i+=100)
	{
		nr_varfuri=100;
		createAdjacencyList(nr_varfuri,i);
		DFS(1);
		fprintf(f1,"%d\t %d\n",i,nr_operatii);
	}
	fclose(f1);
	for(i=140;i<=200;i=i+10)
	{
		nr_muchii=9000;
		createAdjacencyList(i,nr_muchii);
		DFS(1);
		fprintf(f2,"%d\t %d\n",i,nr_operatii);
		nr_operatii=0;
	}
	fclose(f2);	
	*/
    getch();
}
