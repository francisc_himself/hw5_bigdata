//For the case in which k = {5,10,100}, we have a linear growth -> O(nlgn).
//For the second case when k varies, we have an exponential, 
//for which for x->infinity, the function tends to have very small values.
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<math.h>
#include "Profiler.h"

Profiler profiler("demo");
#define MIN_VAL INT_MAX
#define NEG_INF INT_MIN
using namespace std;

int heap_size, n, k, rez[10005],poz;
struct node{
	int data;
	node *next;
};
node *x[10005],*head[10005],*drop[10005];
struct heap{
	int value;
	int index;
};
heap a[10005];


void displayLists();
void createHeap();
void createList();
void displayLists();
void minHeapify(heap *, int );
void heapIncreaseKey(heap *, int , node *);
void minHeapInsert(heap *, node *,int);
void prettyPrint(heap *, int , int );
void displayResult();
void average1();
void average2();

void createList()
{
	int arr[10005];
	node *temp;
	for(int i=1;i<=k;i++)
	{
		FillRandomArray(arr,n+1,1,10000,false,1);
		temp = new node;
		profiler.countOperation("assignments",n,3);
		temp->data = arr[1];
		x[i] = temp;
		head[i] = temp;
		drop[i] = head[i];
		for(int j=2;j<=n+1;j++)
		{
			temp = new node;
			profiler.countOperation("assignments",n,3);
			temp->data = arr[j];
			x[i]->next = temp;
			x[i] = x[i]->next;
		}
		x[i]->next = new node;
		profiler.countOperation("assignments",n);
		x[i]->next = NULL;
	}
}
void displayLists()
{
	for(int i=1; i<=k;i++)
	{
		printf("\nList %d :\n",i);
		profiler.countOperation("assignments",n);
		x[i] = head[i];
		profiler.countOperation("comparisons",n);
		while(x[i]->next != NULL)
		{
			profiler.countOperation("comparisons",n);
			printf("%d ",x[i]->data);
			profiler.countOperation("assignments",n);
			x[i] = x[i]->next;
		}
	}
}
void minHeapify(heap *he, int i)
{
	int l, r, smallest;
	heap aux;
	l = 2*i;
	r = 2*i+1;
	profiler.countOperation("comparisons",n,2);
	if(l <= heap_size && he[l].value < he[i].value)
		smallest = l;
	else 
		smallest = i;
	if(r <= heap_size && he[r].value < he[smallest].value)
		smallest = r;
	if(smallest != i)
	{
		profiler.countOperation("assignments",n,3);
		aux = he[i];
		he[i] = he[smallest];
		he[smallest] = aux;
		minHeapify(he, smallest);
	}
}
void heapIncreaseKey(heap *ah, int i, node *key)
{
	heap aux;
	profiler.countOperation("assignments",n,2);
	ah[i].value = key->data;
	while(i>=1 && ah[i/2].value > ah[i].value)
	{
		profiler.countOperation("assignments",n,3);
		aux = ah[i];
		ah[i] = ah[i/2];
		ah[i/2] = aux;
		i = i/2;
	}
}
void minHeapInsert(heap *ah, node *key, int index)
	{
		heap_size = heap_size +1;
		profiler.countOperation("assignments",n,2);
		ah[heap_size].value = key->data;
		ah[heap_size].index = index;
		heapIncreaseKey(ah,heap_size,key);
}

void prettyPrint(heap *he, int i, int level)
{
	if(i > k)
		return;
	prettyPrint(he,2*i, level+1);
	for(int j=0;j<level;j++)
		printf("         ",level);
	printf("%d\n",he[i].value);
	prettyPrint(he,2*i+1, level+1);	
}
heap heapExtractMin()
{
	heap min;
	if(heap_size < 1)printf("error");
	minHeapify(a,1);
	profiler.countOperation("assignments",n,2);
	min = a[1];
	a[1] = a[heap_size];
	heap_size = heap_size-1;
	minHeapify(a,1);
	return min;
}
void createHeap()
{
	int ind = 0;
	for(int i=1; i<=k;i++)
	{
		profiler.countOperation("assignments",n,6);
		x[i] = head[i];
		a[i].value = x[i]->data;
		a[i].index = i;
		x[i] = x[i]->next;
		rez[++poz] = a[i].value;
		head[i] = x[i];
	} 
	
	heap_size = k;
	for(int j = k/2; j>= 1; j--)
		minHeapify(a,j);
	heap_size = k;
	
	while(heap_size > 0)
	{
		heap temp;
		temp = heapExtractMin();
		profiler.countOperation("assignments",n,3);
		rez[++poz] = temp.value;
		x[temp.index] = head[temp.index];
		profiler.countOperation("comparisons",n);
		if(head[temp.index]->data > 0)
			minHeapInsert(a, head[temp.index], int (temp.index));
		profiler.countOperation("assignments",n);
		head[temp.index] = x[temp.index]->next;
	}
	for(int i=1;i<=k;i++)
	{
		profiler.countOperation("comparisons",n);
		while(x[i]->data >0)
			{
				profiler.countOperation("comparisons",n,2);
				profiler.countOperation("assignments",n);
				rez[++poz] = x[i]->data;
				int auxp = poz,aux;
				while(rez[auxp]<rez[auxp-1])
				{
					profiler.countOperation("assignments",n,3);
					aux = rez[auxp];
					rez[auxp] = rez[auxp-1];
					rez[auxp-1] = aux;
					auxp--;
				}
				x[i] = x[i]->next;
		}
	}
}
void displayResult()
{
	printf("\n Final result:\n");
	for(int i=1; i<=poz; i++)
		printf("%d ",rez[i]);
}

void test()
{
	n = 20; 
	k = 4;
	poz = 0;
	printf("The display for the testing function : \n");
	createList();
	displayLists();
	createHeap();
	printf("\n");
	displayResult();
}
void average1()
{
	int size;
	k=5;
	for(size=100; size<= 10000; size+=100)
	{
		poz = 0;
		n = size/k;
		createList();
		createHeap();
		printf("\n k = %d, n= %d ",k,size);
	}
	k=10;
	for(size=100; size<= 10000; size+=100)
	{
		poz = 0;
		n = size/k;
		createList();
		createHeap();
		printf("\n k = %d, n= %d ",k,size);
	}
	k=100;
	for(size=100; size<10000; size+=100)
	{
		poz = 0;
		n = size/k;
		createList();
		createHeap();
		printf("\n k = %d, n= %d ",k,size);
	}
	profiler.createGroup("AVG k=5,10,100","comparisons","assignments");
	profiler.showReport();
}
void dropAll(node *li[])
{
	node *p;
	for(int i=1;i<=k;i++)
	{
		while(li[i]->next != NULL)
		{
			p = li[i];
			li[i] = li[i]->next;
			delete(p);
		}
		delete(li[i]);
	}
}
void average2()
{
	int size = 9000;
	for(k=10; k<=500; k+=10)
	{
		poz = 0;
		n = size/k;
		createList();
		createHeap();
		printf("\n k = %d, n = %d ",k,n);
	}
	profiler.addSeries("AVG2 k varies","assignments","comparisons");
	profiler.createGroup("Assignments + Comparisons","AVG2","0");
	profiler.showReport();
}
void main()
{
	//average1(); // k - 5,10,100, n - varies 
	//average2(); // k - varies, n=10000
	test();  // the result for the small size input
}