/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

codul de fata implementeaza solutia problemei de interclasare a k liste ordonate.

O. Rezultate asteptate:
	- Alogoritmul ce foloseste un heap pentru a extrage elementul minim dintre primele elemente ale 
	listelor ce trebuie interclasate ar trebui sa aiba o complexitate de ordinul
		O(n * log k)

I. Grafice ale numarului de operatii pentru k = constant;
	- Am considerat cazurile k=5, k=10, k=100. Pentru fiecare caz am construit cate un grafic. 
	Graficele se gasesc in fisierele:
		Report_k_5.html
		Report_k_10.html
		Report_k_100.html

	Interpretare:
		Pentru k = constant numarul de operatii este reprezentat pe grafice sub forma unei drepte. 
		
		Concluzie: pentru k constant, complexitatea algoritmului este O(n);  (*)

II. Grafice ale numarului de operatii pentru n = constant;
	- Am considerat cazul n = 10000. Graficul pentru acest caz se gaseste in fisierul
	Report_n_10000.html

	Interpretare: 
		Pentru n = constant numarul de operatii este reprezentat pe grafice sub o forma asemanatoare
		graficului functiei logaritm in baza 2, cu usoare oscilatii. Oscilatiile sunt cauzate de operatiile pe heap.
		
		Concluzie: pentru n constant, complexitatea algoritmului este O(log k); (**)

III. Observatii finale
	Folosind deductiile (*) si (**) obtinem ca algoritmul are complexitatea O(n * log k). Aceasta implementare obtine
	deci rezultatele asteptate.
*/

#include<stdio.h>
#include "profiler.h"


typedef struct _NOD 
{
	int value;
	struct _NOD *next;
			
}NOD;

typedef struct _HEAP_ELEMENT 
{
	NOD *nod;
	int list_index;
			
}HEAP_ELEMENT;

Profiler profiler("Interclasarea a k liste");
int dimensiune_curenta;

NOD *head[1000];
NOD *tail[1000];

HEAP_ELEMENT heap[10000];
int heapSize;

int nrDeListe, nrDeElemente;
//===============================================================
//functii pentru lista
void showList(NOD *h)
{
	if(h!= NULL)
	{
		printf("%d ",h->value);
		showList(h->next);
	}
	else printf("NULL\n");
}
void insert(NOD **h, NOD**t,int val)
{
	NOD* p = new NOD;
	p->value = val;
	p->next = NULL;
	
	if(*t == NULL)
	{
		*t = p;
		*h =p;
	}
	else 
	{
		(*t)->next = p;
		*t = p;
		if(*h==NULL)
			*h=p;			
	}
}

void deleteListElems(NOD**h,NOD**t)
{
	if(*h!=NULL)
	{
		printf("sterg elementul %d\n",(*h)->value);
		NOD** sters = h;
		h =&(*h)->next;
		deleteListElems (h,t);
		delete *sters;	
	}
}

void deleteList (NOD**h,NOD**t){
	deleteListElems(h,t);
	*h = NULL;
	*t = NULL;
}

NOD* GetFirst(NOD** lista)
{
	NOD* firstNode=NULL;
	if(*lista == NULL)
	{
		return NULL;
	}
	else
	{
		firstNode = *lista;
		*lista = (*lista)->next;
	}
	return firstNode;
}
//================================================================
//functii pentru heap-uri

/*
functie de initializare a heapului

@param: H = pointer la sirul in care este stocat heap-ul
*/

void _hInit (HEAP_ELEMENT H[]){
	heapSize =0;
}


/*
parintele nodului i
@param: i = elementul al carui parinte il cautam
*/
int parinte(int i){
	return i/2;
}


/*
dscendentul stang al lui i
@param i
*/
int stanga(int i){
	return 2*i;
}

/*
decendentul drept al lui i
@param: i
*/
int dreapta(int i){
	return 2*i+1;
}

/*
Returneaza indexul elementului minim dintre H[i],H[a] si H[b]
la apelare functiei a si b vor fi stanga lui i respectiv dreapta lui i, deci tratez aici si cazurile de depasiri de dimensiune
*/



int index_min(HEAP_ELEMENT H[], int i, int a, int b)
{
	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);	
	// numara comparatia de mai jos
	if(a>heapSize){
		return i;
	}

	int min;

	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);	
	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);
	//nrOpBU+=2;
	//numara comparatia si atribuirea
	min = ((H[i].nod->value)<(H[a].nod->value))? i: a;

	//nrOpBU+=1;
	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);
	//numara comparatia de mai jos
	
	if(b>heapSize){
		return min;
	}

	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);
	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);
	// numara comparatia si atribuirea

	min = ((H[min].nod->value)<(H[b].nod->value))? min:b;
	
	return min;
}


void interschimba(HEAP_ELEMENT* a, HEAP_ELEMENT*b)
{
	HEAP_ELEMENT *aux = new HEAP_ELEMENT;
	aux->list_index = a->list_index;
	aux->nod = a ->nod;

	a->list_index = b->list_index;
	a->nod = b ->nod;

	b->list_index = aux->list_index;
	b->nod = aux ->nod;

}

/*
reaconsructia unui heap
*/

void hReconstituie (HEAP_ELEMENT H[],int i){

	/*
	 // cod de debug
	printf("hReconstituie....  %d\n",i);
				
	for(int j=0;j<=H[0];j++)
		printf("%d ",H[j]);
	printf("\n");
	*/

	//profiler.countOperation("Atribuiri_BU",dimensiune_curenta,1);
	// numara atribuire de mai jos
	int index = index_min(H,i,stanga(i),dreapta(i));
	
	profiler.countOperation("Numar de operatii",dimensiune_curenta,1);

	//numara comparatia de mai jos
	
	if(index !=i){
							 // 4 atribuiri
		profiler.countOperation("Numar de operatii",dimensiune_curenta,4);
		//numara atribuirile 
		
		interschimba(&H[i],&H[index]); // 3 atribuiri

		hReconstituie(H,index);
	}
}

/*
constructie bottom up
*/

void hBUBuild(HEAP_ELEMENT H[]){
	int i; //iterator
	for(i=heapSize/2;i>0;i--)
		hReconstituie(H,i);
}


/*
punerea unui element in heap
*/

void hPush(HEAP_ELEMENT H[],HEAP_ELEMENT elem){
	int i;
	profiler.countOperation("Numar de operatii",dimensiune_curenta,3);
						//3 atribuiri 
	//numara atribuirile de mai jos
	heapSize++;							
	H[heapSize].list_index = elem.list_index;
	H[heapSize].nod = elem.nod;
	i = heapSize;						

	profiler.countOperation("Numar de operatii",dimensiune_curenta,2);
								//2 comaratii 
	//numara comparatiile de la intrarea din while
	
	while(i>1 && (H[i].nod->value)< (H[parinte(i)].nod->value)){
		//printf("\ninterschimb %d %d\n",H[i],H[parinte(i)]);

		profiler.countOperation("Numar de operatii",dimensiune_curenta,2);
		profiler.countOperation("Numar de operatii",dimensiune_curenta,4);
		//numara operatiile din while
		interschimba(&H[i],&H[parinte(i)]);
		i = parinte(i);
	}

}

/*
scoatere din heap
*/
HEAP_ELEMENT hPop(HEAP_ELEMENT H[]){
	HEAP_ELEMENT elem;
	
	profiler.countOperation("Numar de operatii",dimensiune_curenta,3);

	elem.nod = H[1].nod;
	elem.list_index= H[1].list_index;
	H[1].list_index=H[heapSize].list_index;
	H[1].nod = H[heapSize].nod;
	heapSize--;
	
	hReconstituie(H,1);
	return elem;
}
//================================================================
//functii pentru algoritmul nostru

NOD* InterclasareKListe(int n,int k)
{
	NOD* rezultatFirst = NULL;
	NOD* rezultatLast = NULL;
	NOD* x;
	HEAP_ELEMENT he;
		
	_hInit(heap);
	for (int i = 1;i<=k;i++)
	{
		profiler.countOperation("Numar de operatii",dimensiune_curenta,1);

		x=GetFirst(&head[i]);
		if(x!=NULL)
		{
			profiler.countOperation("Numar de operatii",dimensiune_curenta,2);

			he.list_index = i;
			he.nod = x;
			hPush(heap,he);
		}
	}
	while(heapSize>0)
	{
		profiler.countOperation("Numar de operatii",dimensiune_curenta,2);

		he = hPop(heap);
		insert(&rezultatFirst,&rezultatLast,he.nod->value);
		
		x = GetFirst(&head[he.list_index]);
		
		if(x!=NULL)
		{
		profiler.countOperation("Numar de operatii",dimensiune_curenta,1);

			(he.nod)->value = x->value;
			(he.nod)->next = x->next;
			hPush(heap,he);
		}
	}
	
	return rezultatFirst;
}

//================================================================
//functii pentru implementarea rezolvarilor la cerinte
void verifica_corectitudine_liste()
{
	NOD* head = NULL;
	NOD* tail = NULL;


	insert(&head,&tail,1);
	insert(&head,&tail,2);
	insert(&head,&tail,3);
	insert(&head,&tail,4);
	insert(&head,&tail,5);

	showList(head);

	deleteList(&head,&tail);
	showList(head);
}

void verifica_corectitudine()
{
	
	insert(&head[1],&tail[1],1);
	insert(&head[1],&tail[1],2);
	insert(&head[1],&tail[1],3);
	insert(&head[1],&tail[1],4);
	insert(&head[1],&tail[1],5);
	

	insert(&head[2],&tail[2],1);
	insert(&head[2],&tail[2],2);
	insert(&head[2],&tail[2],3);
	insert(&head[2],&tail[2],4);
	insert(&head[2],&tail[2],5);

	insert(&head[3],&tail[3],1);
	insert(&head[3],&tail[3],2);
	insert(&head[3],&tail[3],3);
	insert(&head[3],&tail[3],4);
	insert(&head[3],&tail[3],5);

	

	showList(head[1]);
	showList(head[2]);
	showList(head[3]);
	head[0] = InterclasareKListe(5,3);
	showList(head[0]);
	showList(head[1]);
	showList(head[2]);
	showList(head[3]);

}

void ListFromArray (NOD** list_head,NOD** list_tail, int* Array, int size_of_array)
{
	*list_head = NULL;
	*list_tail = NULL;
	for(int i=0;i<=size_of_array;i++)
		insert(list_head,list_tail,Array[i]);
}


void GenerateRandomLists (int k,int n)
{
	int A[10000];
	for(int i=0;i<=k;i++)
	{
		FillRandomArray(A,n,0,10000,true,1);
		ListFromArray(&head[i],&tail[i],A,n);
	}
}

void Task1(int k)
{
	for(dimensiune_curenta=100;dimensiune_curenta<=10000;dimensiune_curenta+=100)
	{
		printf("acum calculam pentru dimensiune = %d...\n",dimensiune_curenta);
		GenerateRandomLists(k,dimensiune_curenta/k);
		InterclasareKListe(dimensiune_curenta/k,k);
	}
	profiler.showReport();
}

void Task2()
{
	int n = 10000;
		for(dimensiune_curenta=10;dimensiune_curenta<=500;dimensiune_curenta+=10)
	{
		printf("acum calculam pentru dimensiune = %d...\n",dimensiune_curenta);
		GenerateRandomLists(dimensiune_curenta,10000/dimensiune_curenta);
		InterclasareKListe(10000/dimensiune_curenta,dimensiune_curenta);
	}
	profiler.showReport();

}

//=======================================================================
int main()
{
	//verifica_corectitudine();
	//Task1(100);
	Task2();
	getchar();
	return 0;
}