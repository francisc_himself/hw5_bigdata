#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>
#include "Profiler.h"

Profiler profiler;
//1 - white
//2 - gray
//3 - black

typedef struct edge{
	int p;
	int d;
}edge;


typedef struct nod{
	nod* next;
	int value;
	int color;
	nod* predecessor;
	int distance;
}nod;


edge e[9001];
nod* start[201];
nod* end[201];
int nr_edges,nr_vertices;

bool ok=true;
int sizeQ=0;
int Q[201];
int tailQ,headQ;
int op=0;
void enqueue(nod*s, int size)
{
	profiler.countOperation("operations",size);
	Q[tailQ]=s->value;
	if (tailQ==sizeQ-1)
		tailQ=1;
	else tailQ++;
}
nod* dequeue(int size)
{
	profiler.countOperation("operations",size);
	int x=Q[headQ];
	if (headQ==sizeQ-1)
		headQ=1;
	else headQ++;
	return start[x];
}

void change_color(int x)
{
	for (int k=0;k<nr_vertices;k++)
	{
		nod* a=start[k];
		while (a!=NULL)
		{
			if (a->value==x)
					a->color=2;
					a=a->next;
		}
	}
}

void BFS(nod* s, int size)
{
	profiler.countOperation("operations",size,3);
	s->color=2;
	s->distance=0;
	s->predecessor=NULL;
	sizeQ=nr_vertices;
	headQ=0;
	tailQ=0;
	enqueue(s,size);
	ok=false;
	while ((tailQ!=headQ)||(ok==false))
	{
		ok=true;
		profiler.countOperation("operations",size);
		nod* u=dequeue(size);
		nod* aux=start[u->value]->next;
		profiler.countOperation("operations",size);
		while (aux!=NULL)
		{
			profiler.countOperation("operations",size);
			if (aux->color==1)
			{
				profiler.countOperation("operations",size,3);
				aux->color=2;
				aux->distance=u->distance+1;
				aux->predecessor=u;
				change_color(aux->value);
				enqueue(aux,size);
			}
			aux=aux->next;
			profiler.countOperation("operations",size);
		}
		u->color=3;
		profiler.countOperation("operations",size);
		change_color(u->value);
	}
	profiler.countOperation("operations",size);
}

int generate_random(int x) 
{
	return  rand()%x;
}

void generate_graph(int nr_vertices,int nr_edges)
{
	int i=0;
	for (i=0;i<nr_vertices;i++)
	{
		start[i]=(nod*)malloc(sizeof(nod));
		start[i]->next=NULL;
		start[i]->value=i;
		end[i]=start[i];
	}
	i=0;
	while(i<nr_edges)
	{	
		int nr1 =generate_random(nr_vertices);
		int nr2 =generate_random(nr_vertices);
		if((nr1!=nr2))
		{
			bool ok=false;
			for (int j=0;j<i;j++)
			{
				if ((e[j].p==nr1)&&(e[j].d==nr2))
				{
					ok=true;
				}
			}
			if (ok==false)
			{	
				e[i].p=nr1;
				e[i].d=nr2;	
				nod* aux=(nod*)malloc(sizeof(nod));
				aux->next=NULL;
				aux->value=e[i].d;
				end[e[i].p]->next=aux;
				end[e[i].p]=aux;
				i++;
			}
		}
	}

}

void prettyprint(int i, int depth, int dep, int s)
{
	if (s<sizeQ)
	{
		for (int j=0;j<depth;j++)
			printf("    "); 
		
		printf("%d", Q[i]);
		printf("\n");
		if(Q[i+1] != NULL)
		if( start[Q[i+1]]->distance > dep )
			prettyprint(i+1,depth+1,start[Q[i+1]]->distance,s+1);
		else 
			prettyprint(i+1,depth,start[Q[i+1]]->distance,s+1);
	}
}

void print_adj_list(int nr_vertices)
{
	for (int i=0;i<nr_vertices;i++)
	{
		nod* aux=start[i];
		while (aux!=NULL)
		{
			if (aux==start[i])
				printf("%d -> ",aux->value);
			else printf("%d ",aux->value);
			aux=aux->next;
		}
		printf("\n");
	}
}

void test()
{
	nr_edges=7;
	nr_vertices=5;
	generate_graph(nr_vertices,nr_edges);
	print_adj_list(nr_vertices);
	
	for (int i=0;i<nr_vertices;i++)
	{
		start[i]->color=1;
		start[i]->distance=INT_MAX;
		start[i]->predecessor=NULL;
		nod* o=start[i]->next;
		while(o!=NULL)
		{
			o->color=1;
			o->distance=INT_MAX;
			o->predecessor=NULL;
			o=o->next;
		}
	}
	for (int i=0;i<nr_vertices;i++)
	{
		if (start[i]->color==1)
		{
			printf("\nBFS din %d:",i);
			BFS(start[i],nr_vertices);
			printf("\nPrettyPrint:\n");
			prettyprint(0,0,0,0);
			for (int j=0;j<sizeQ;j++)
				Q[j]=0;
		}
	}
	getch();
}

int main()
{
	srand(time(NULL));
	test();
	
	//first test - edges variation
	/*nr_vertices=100; 
	for (nr_edges=1000;nr_edges<5000;nr_edges+=100)
	{
		generate_graph(nr_vertices,nr_edges);	
		for (int i=0;i<nr_vertices;i++)
		{
			start[i]->color=1;
			start[i]->distance=INT_MAX;
			start[i]->predecessor=NULL;
			nod* o=start[i]->next;
			while(o!=NULL)
			{
				op+=4;
				o->color=1;
				o->distance=INT_MAX;
				o->predecessor=NULL;
				o=o->next;
			}
		}
		for (int i=0;i<nr_vertices;i++)
		{
			if (start[i]->color==1)
				BFS(start[i],nr_edges);
		}
	}
	profiler.showReport();*/
	
	
	//test 2 - vertices variation
	/*nr_edges=9000;
	for(nr_vertices=100;nr_vertices<=200;nr_vertices+=10)
	{
		generate_graph(nr_vertices,nr_edges);
		for (int i=0;i<nr_vertices;i++)
		{
			profiler.countOperation("operations",nr_vertices,4);
			start[i]->color=1;
			start[i]->distance=INT_MAX;
			start[i]->predecessor=NULL;
			nod* o=start[i]->next;
			while(o!=NULL)
			{
				profiler.countOperation("operations",nr_vertices,4);
				o->color=1;
				o->distance=INT_MAX;
				o->predecessor=NULL;
				o=o->next;
			}
		}
		for (int i=0;i<nr_vertices;i++)
		{
			if (start[i]->color==1)
			{
				BFS(start[i],nr_vertices);
			}
		}
	}
	profiler.showReport();*/
}