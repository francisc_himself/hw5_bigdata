#include "Profiler.h"
#include <stdio.h>
#include <conio.h>
Profiler profiler("demo");

int left(int x)
{
	return 2*x;
}

int right(int x)
{
	return 2*x+1;
}

void heapify(int *A,int i,int n)
{
	int l,r;
	
	int aux;
	int largest;
	l = left(i);
	r = right(i);
	if (l<=n && A[l]>A[i])
	{
		profiler.countOperation("heapsort", n);
		largest = l;
	}
	else largest = i;

	if (r<=n && A[r]>A[largest])
	{
		profiler.countOperation("heapsort", n);
		largest = r;
	}

	if (largest != i)
	{
		aux = A[i];
		A[i] = A[largest];
		A[largest] = aux;
		profiler.countOperation("heapsort", n, 3);
		heapify(A,largest, n);
	}
	
}

void buildHeap(int *A, int n) 
{ 
	int i;
	for (i=n/2; i>=0; i--) 
		heapify(A, i, n); 
} 

void heapSort(int *A, int n)
{
	int i;
	buildHeap(A,n);
	int aux;
	for (i=n; i>=1;i--)
	{
		aux = A[0];
		A[0] = A[i];
		A[i] = aux;
		profiler.countOperation("heapsort", n, 3);
		n--;
		heapify(A,0,n);
	}
	
	
}

int partition(int *A,int p, int r)
{
	int x;
	x = A[r];
	int i=p-1;
	int aux;
	for (int j=p; j<=r-1; j++)
	{
		if (A[j]<=x)
		{
			profiler.countOperation("quicksort", r);
			i++;
			aux = A[i];
			A[i] = A[j];
			A[j] = aux;
			profiler.countOperation("quicksort", r,3);
		}
	}
	aux = A[i+1];
	A[i+1] = A[r];
	A[r] = A[i+1];
	profiler.countOperation("quicksort", r,3);

	return i+1;
}

void quickSort(int *A, int p, int r)
{
	int q;
	if (p<r)
	{
		q = partition(A,p,r);
		//printf("\n %d", q);
		quickSort(A,p,q-1);
		quickSort(A,q+1,r);
	}
}

int main()
{
	int A[]={14,28,17,20,35,20,8,11,25,30,41};
	/*int A[10000];
	int B[10000];
	int n;
	for (int j=1; j<=5; j++)
	{
		for (n=100; n<=10000; n+=100){
			FillRandomArray(A, n);
			for (int i=0;i<n;i++)
				B[i] = A[i];
			heapSort(A, n);	
			quickSort(B,0, n);
			
		}
		
	}
	profiler.createGroup("Sorting", "heapsort","quicksort");
	profiler.showReport();*/
	buildHeap(A,10);
	for (int i=0; i<10; i++)
		printf("%d ",A[i]);
	printf("\n");
	getch();
	return 0;
}

/*#include<stdio.h>
#include <conio.h>
void quicksort(int [10],int,int);

int main(){
  int x[20],size,i;

  printf("Enter size of the array: ");
  scanf("%d",&size);

  printf("Enter %d elements: ",size);
  for(i=0;i<size;i++)
    scanf("%d",&x[i]);

  quicksort(x,0,size-1);

  printf("Sorted elements: ");
  for(i=0;i<size;i++)
    printf(" %d",x[i]);

  getch();
  return 0;
}

void quicksort(int x[10],int first,int last){
    int pivot,j,temp,i;

     if(first<last){
         pivot=first;
         i=first;
         j=last;

         while(i<j){
             while(x[i]<=x[pivot]&&i<last)
                 i++;
             while(x[j]>x[pivot])
                 j--;
             if(i<j){
                 temp=x[i];
                  x[i]=x[j];
                  x[j]=temp;
             }
         }

         temp=x[pivot];
         x[pivot]=x[j];
         x[j]=temp;
         quicksort(x,first,j-1);
         quicksort(x,j+1,last);

    }
}*/