#include<stdio.h>
#include<conio.h>
#include<stdlib.h>

#define NMAX 1000
int nratribuiri,nrcomparari; //initializam numarul de comparari si de atribuiri 

void random (int v[], int n) // functia sirului random
{
	int i;
	for ( i=1;i<=n;i++)
	{
		v[i] = rand()%NMAX+1; //vectorul ia valoarea numerelor random
	}
}

void crescator (int v[], int n) //functia sirului crescator
{
	int i;
	for (i=1;i<=n;i++)
		v[i] = i;  // vectorul ia valoarea lui i
}

void descrescator (int v[], int n) // functia sirului descresctor
{
	int i;
	for(i=1;i<=n;i++)
		v[i] = n-i+1; //vectorul ia valoarea lungimii-i+1
}

void copiere (int v[], int w[], int n)// funtia de copiere
{
	int i;
	for(i=1;i<=n;i++)
		w[i] = v[i]; //aldoilea vector se copiaza in primul
}

void sortare_bule (int v[],int n) //metoda bulelor
{
	int j,k;
	int sortat=0;
	while(!sortat)
	{ 
		sortat=1;
	for(j=1;j<=n-1;j++)
	  {
		nrcomparari = nrcomparari + 1;//contorizam compararile
			if(v[j]>v[j+1])
			{
				k=v[j];
				v[j]=v[j+1];  //interschimbare cu auxiliar k
				v[j+1]=k;
				sortat=0;
				nratribuiri = nratribuiri + 3;//contorizam atribuirile
			}
      }
     }


}

void selectie(int v[], int n) // funtia metodei de selectie
{  
	int i, j, imin,min,aux;
	for (i = 0; i < n - 1; i++) 
	{
		imin = i;
		min=v[i];
		for (j = i + 1; j < n; j++)
		{
			nrcomparari = nrcomparari + 1;// se pune contor pentru a gasi numarul de comparatii
			
			if (min > v[j])
				imin = j;
			    min = v[j];
		}
		
			aux = v[i];
			v[i] = v[imin];  //interschimbare 
			v[imin] = aux;
			nratribuiri = nratribuiri + 3;
		
	}
}

void insertie(int v[], int n) // functie pentru metoda de insertie
{
	int i, j, aux;
	for (i = 1; i < n; i++) {
		j = i;
		nrcomparari= nrcomparari + 1;
		while (j > 0 && v[j - 1] > v[j]) {
			aux = v[j];
			v[j] = v[j - 1];//interschimbare
			v[j - 1] = aux;
			nratribuiri = nratribuiri + 3; //contorizam nr dw atribuiri si comparari
			nrcomparari = nrcomparari + 1;
			j--;
		}
	}
}

int main()
{

	int i,j,a;
	int v[NMAX+1], aux[NMAX+1];// sirul v si sirul auxiliar
	int natr1[NMAX+1],natr2[NMAX+1],natr3[NMAX+1];// nr atribuiri
	int ncomp1[NMAX+1],ncomp2[NMAX+1],ncomp3[NMAX+1];//nr comparari

	FILE *fout = fopen("tema.csv","w");
	
	fprintf(fout,"Random\n");// pt toate functiile random
	random(v,NMAX);
	for(i=100;i<=NMAX;i = i+100)
	{
		copiere(v,aux,i);//copiam pentru a nu se modifica sirul
		nratribuiri = 0;
		nrcomparari = 0;
		sortare_bule(aux,i);// apelam functia sortare
		natr1[(i/100)-1] = nratribuiri;
		ncomp1[(i/100)-1] = nrcomparari;

		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		selectie(aux,i);//apelam functia selectie
		natr2[(i/100)-1] = nratribuiri;
		ncomp2[(i/100)-1] = nrcomparari;

		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		insertie(aux,i);//apelam functia insertie
		natr3[(i/100)-1] = nratribuiri;
		ncomp3[(i/100)-1] = nrcomparari;
	}
	fprintf(fout,"Sortarea Bulelor\n"); //afisare pt metoda bulelor
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr1[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp1[j]);
	fprintf(fout,"\n");
	fprintf(fout,"Selectie\n");//afisare pentru metoda selectiei
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr2[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp2[j]);
	fprintf(fout,"\n");
	fprintf(fout,"Insertie\n");//afisare pentru metoda insertiei
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr3[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp3[j]);
	fprintf(fout,"\n");

//--------------------------------------------------------
	fprintf(fout,"Crescator\n");//pt functii luate crescator
	crescator(v,NMAX);
	for(i=100;i<=NMAX;i = i+100)
	{
		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		sortare_bule(aux,i);//apelam metoda bulelor
		natr1[(i/100)-1] = nratribuiri;
		ncomp1[(i/100)-1] = nrcomparari;

		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		selectie(aux,i);//apelam metoda selectie
		natr2[(i/100)-1] = nratribuiri;
		ncomp2[(i/100)-1] = nrcomparari;

		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		insertie(aux,i);//apelam metoda insertie
		natr3[(i/100)+1] = nratribuiri;
		ncomp3[(i/100)+1] = nrcomparari;
	}
	fprintf(fout,"Sortarea bulelor\n");//afisare
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr1[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp1[j]);
	fprintf(fout,"\n");
	fprintf(fout,"Selectie\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr2[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp2[j]);
	fprintf(fout,"\n");
	fprintf(fout,"Insertie\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr3[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp3[j]);
	fprintf(fout,"\n");

//--------------------------------------------------
	fprintf(fout,"Descrescator\n");//pentru functii luate descrescator
	descrescator(v,NMAX);
	for(i=100;i<=NMAX;i = i+100)
	{
		copiere(v,aux,i);
	    nratribuiri = 0;
		nrcomparari = 0;
		sortare_bule(aux,i);
		natr1[(i/100)-1] = nratribuiri;
		ncomp1[(i/100)-1] = nrcomparari;

		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		selectie(aux,i);
		natr2[(i/100)-1] = nratribuiri;
		ncomp2[(i/100)-1] = nrcomparari;

		copiere(v,aux,i);
		nratribuiri = 0;
		nrcomparari = 0;
		insertie(aux,i);
		natr3[(i/100)-1] = nratribuiri;
		ncomp3[(i/100)-1] = nrcomparari;
	}
	fprintf(fout,"Sortarea bulelor\n");//afisare
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr1[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp1[j]);
	fprintf(fout,"\n");
	fprintf(fout,"Selectie\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr2[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp2[j]);
	fprintf(fout,"\n");
	fprintf(fout,"Insertie\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",natr3[j]);
		fprintf(fout,"\n");
	for (j=0;j<NMAX/100;j++)
			fprintf(fout,"%d,",ncomp3[j]);
	fprintf(fout,"\n");

fclose(fout);
return 0;

}
