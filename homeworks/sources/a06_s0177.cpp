#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include "stdafx.h"
using namespace std;
#include "Profiler.h"
Profiler profiler("demo");

int n;
int m;
int perm[10];
int op;

typedef struct nod
{
    int key;
    int rank;
    nod *left;
	nod *right;
	nod *parent;
} NOD;


NOD* root;


void print(NOD *p,int level)
{

  if (p!=NULL)
  {

		print(p->right,level+1);
		 for( int i=0;i<=level;i++) 
							printf("	");
		printf("%d ",p->key);
		printf("\n");
		
		
		print(p->left,level+1);
   }

}


NOD* new_node(int x, int size, NOD *left, NOD *right)
{
   
	NOD *q;
    q= new NOD;
    q->key=x;					
    q->left=left;				
    q->right=right;				
    q->rank=size;			
	profiler.countOperation("op",op,4);
	profiler.countOperation("op",op,1);
    
	if (left!=NULL)
    {
        left->parent = q;	
		profiler.countOperation("op",op,1);
    }

	profiler.countOperation("op",op,1);
    
	if (right!=NULL)
    {
        right->parent=q;	
		profiler.countOperation("op",op,1);
    }
	
    q->parent=NULL;	
	profiler.countOperation("op",op,1);
   
	return q;

}


NOD* buildTree(int left, int right)
{
    
	NOD *aux,*aux1;
	profiler.countOperation("op",op,1);
    
	if (left > right)
        return NULL;
	profiler.countOperation("op",op,1);

    if (left == right)
        return new_node(left, 1, NULL, NULL);		
    else
    {
        int ***ANONIM*** =(left + right)/2;
		profiler.countOperation("op",op,2);
        aux = buildTree(left, ***ANONIM***-1);
        aux1  = buildTree(***ANONIM***+1, right);
        return new_node(***ANONIM***, right-left+1,aux, aux1);
    }
    
}


NOD* OS_SELECT(NOD *x, int i)
{
    int r=1;
	profiler.countOperation("op",op,1);
    if (x->left != NULL)							
        r = x->left->rank+1;					
    else
        r = 1;
    if (r == i)
        return x;
    else
    {
        if (i < r)
        {
            return OS_SELECT(x->left,i);
        }
        else
        {
            return OS_SELECT(x->right,i-r);
        }
    }
}


NOD* MIN_VALUE(NOD* x)
{
	profiler.countOperation("op",op,1); 
	while(x->left!=NULL)
    {
        x = x->left;
		profiler.countOperation("op",op,1);
	}
	profiler.countOperation("op",op,1);
	return x;						
}


NOD* SUCCESOR(NOD* x)
{
    NOD *y;
	profiler.countOperation("op",op,1);
	if(x->right!=NULL)						
    {
        return MIN_VALUE(x->right);				
    }
	profiler.countOperation("op",op,1);
    y = x->parent;	
	profiler.countOperation("op",op,2);
	while (y != NULL && x == y->right)		
    {
       	x = y;								
        y = y->parent;
		profiler.countOperation("op",op,2);
    }
	profiler.countOperation("op",op,2);
	return y;								
}

NOD* OS_DELETE(NOD* z)
{
    NOD *y;
    NOD *x;
	profiler.countOperation("op",op,1);
    if ((z->left == NULL) || z->right == NULL)		
    {
        profiler.countOperation("op",op,1);
		y=z;
    }
    else
	{
		profiler.countOperation("op",op,1); 
		y = SUCCESOR(z);							
	}
	
	profiler.countOperation("op",op,1);
    if (y->left != NULL)							
    {
        x = y->left;
		profiler.countOperation("op",op,1);
    }
    else
    {                                   
        x = y->right;
		profiler.countOperation("op",op,1);
    }
	profiler.countOperation("op",op,1);
    if (x != NULL)
    {
        x->parent = y->parent;
		profiler.countOperation("op",op,1);
	}
	profiler.countOperation("op",op,1);
    if (y->parent == NULL)
	{
        root = x;
		profiler.countOperation("op",op,1);
	}
    else
	{
		profiler.countOperation("op",op,1);
		if (y == y->parent->left)
		 {
			 profiler.countOperation("op",op,1);
			 y->parent->left = x;
		}
	
    else
    {
        y->parent->right = x;
		profiler.countOperation("op",op,1);
    }
	}
	profiler.countOperation("op",op,1);
    if (y != z)
    {
        z->key = y->key;
		profiler.countOperation("op",op,1);
    }
    return y;
}

void update_rank(NOD *p)
{
    while (p != NULL)
    {

        p->rank--;			
        p=p->parent;
		profiler.countOperation("op",op,2);
    }
}

void print_perm(int perm[],int index){
    printf("\nJosephus Permutation:\n");
    for ( int i = 0 ; i < index ;i++)
        printf("%d ",perm[i]);
}


void joseph_perm(int n, int m)
{
	NOD *y,*z;
	int nAux = n;
	int mij = m;

	root = buildTree(1,n);		
	for (int i = 1; i < nAux; i++)	
                                    
	{
		y = OS_SELECT(root, m);	
		z = OS_DELETE(y);
		update_rank(z);			
		free(z);
		
		n--;						
		if (n > 0)					
			m = (m - 1 + mij) % n;
		if (m == 0)
			m = n;
	}

}


void main()
{
	//Test case
	n=7;
	m=3;
	NOD *y,*z;
    int nAux = n+1;
    int mij = m;
    
    int index=0;
    root = buildTree(1,n);		
    printf("\nTree\n");
    print(root,0);
	for (int i = 1; i < nAux; i++)	
                                   
    {
        y = OS_SELECT(root, m);	    
        
		perm[index] = y->key;
        index++;
        print_perm(perm,index);
        
		z = OS_DELETE(y);
		update_rank(z);	
        
		printf("\nTree\n");
        print(root,0);
        
		free(z);

        n--;						
        if (n > 0)					
            m = (m - 1 + mij) % n;
        if (m == 0)
            m = n;
    }

	//Profiler display
/*	for(op=100; op<=10000; op+=100)
	{
		joseph_perm(op,op/2);

	}
	profiler.createGroup("operatii_pt_pemutarea_joseph","op");
	profiler.showReport(); */
	char c;
	scanf_s("%c",&c);
}