/*
author:***ANONIM***�si ***ANONIM***
  group:***GROUP_NUMBER***
  date:10.04.2013
  program description : the program takes k linked lists which are sorted and merge them in a linked list in descending order.The linked lists are created
  from random elements and are stored in an array. At every step an element from the front of the list and bulids a heap from the elements. The heap is a min-heap 
  and at every step after heapify the first element is stored in the result linked list and in the a new element is put from the list from where the min element 
  was. When a list is empty the heap size is decremented.

  At first 5 lists are generated with the number of elements varying from 100 to 10000 and they are split equally between the 5 lists. Then the same procedure 
  with 10 and 100 lists. We can see that the number of operations increases logarithmically for the same number of lists.
  
  */

#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"

Profiler p("merge");

typedef struct LIST
{
    int data;
    struct LIST *next;
};

typedef struct HEAP
{
    int nr;
    int index;
};

LIST* sentinels[2000];
HEAP heap[2000];
int heap_size;
int op;
int array[10000];

void insert_in_list(int i,int d)
{
	LIST* a = new(LIST);
	a->next=sentinels[i];
	a->data=d;
	sentinels[i]=a;
}

void delete_from_list(int i)
{
	LIST *a=sentinels[i];
	sentinels[i]=a->next;
	free(a);
	//delete a;
	//a=NULL;
}

int parent(int i)
{
	return (int)i/2;
}
int left(int i)
{
	return 2*i;
}

int right(int i)
{
	return (2*i+1);
}

void heapify(int i)
{
	int l = left(i);
	int r = right(i);
	int largest = 0;
	op=op+2;
	if (l<=heap_size && heap[l].nr<heap[i].nr) largest = l;
		else largest = i;
    if (r<=heap_size && heap[r].nr<heap[largest].nr) largest = r;
	if (largest != i) 
		{
			HEAP aux = heap[i];
			heap[i]=heap[largest];
			heap[largest]=aux;
			op=op+3;
			heapify(heap_size);
		}
	
}

void build_heap_BU(int n)
{
	for (int k=n/2;k>0;k--)
	heapify(k);
}

void print_heap(int n,int k,int level)
{
	if(right(k)<=n)print_heap(n,right(k),level+1);
	for(int i=0;i<=level;i++)printf("    ");
	printf("  %d  \n",heap[k].nr);
	if(left(k)<=n)print_heap(n,left(k),level+1);
}

void print_list(int i)
{
	LIST* l=sentinels[i];
	while(l->next!=NULL)
	{
		printf("%d ",l->data);
		l=l->next;
	}
	printf("%d \n",l->data);
}

void merge()
{
		for(int i=1;i<=heap_size;i++)
		{
			heap[i].nr=sentinels[i]->data;
			heap[i].index=i;
			op++;
			delete_from_list(i);
		}
	build_heap_BU(heap_size);
	//print_heap(heap_size,1,1);
	op++;
	insert_in_list(0,heap[1].nr);

	while(heap_size>0)
	{
		if(sentinels[heap[1].index]==NULL)
		{
				heap[1]=heap[heap_size];
				op++;
				heap_size--;
				heapify(1);
				//print_heap(heap_size,1,1);
				insert_in_list(0,heap[1].nr);
				op++;
				//print_list(0);
		}else{
			heap[1].nr=sentinels[heap[1].index]->data;
			op++;
			delete_from_list(heap[1].index);
			heapify(1);
			//print_heap(heap_size,1,1);
			insert_in_list(0,heap[1].nr);
			op++;
			//print_list(0);
		}

	}
	delete_from_list(0);
	//print_list(0);
}

int main()
{
	/*
	int A[10];
	int B[10];
	int C[10];
	int D[10];

	FillRandomArray(A,10,10,100,true,1);
	FillRandomArray(B,10,10,100,true,1);
	FillRandomArray(C,10,10,100,true,1);
	for(int i=5;i>=0;i--)
	{
		insert_in_list(1,A[i]);
		insert_in_list(2,B[i]);
		insert_in_list(3,C[i]);
		//insert_in_list(4,D[i]);
	}
	print_list(1);
	print_list(2);
	print_list(3);
	//print_list(4);
	heap_size=3;
	merge();
	*/
	/*
	int k=100,n;
	FILE *f;
	f = fopen("out.csv","w");
		for(n=100;n<=10000;n+=100)
		{
			
			for(int i=1;i<=k;i++)
			{
				
			FillRandomArray(array,n/k,10,10000,true,1);
			for(int j=n/k;j>=0/k;j--)
				insert_in_list(i,array[j]);
			//print_list(i);
			//printf("\n");
			}
			heap_size=k;
			merge();
			fprintf(f,"%d,%d\n",n,op);
			op=0;
			for(int u=0;u<n;u++)
				delete_from_list(0);
		}


	*/	
	int n=10000;
	int i,k,j;
	FILE *f;
	f = fopen("out1.csv","w");
	for(i=10;i<=500;i=i+10)
	{
		for(k=1;k<=i;k++)
		{
			FillRandomArray(array,n/i,10,1000000,true,1);
			for(j=n/i;j>=0/k;j--)
			insert_in_list(k,array[j]);
		}
		heap_size=i;
		merge();
		fprintf(f,"%d,%d\n",i,op);
		op=0;
		for(int u=0;u<n;u++)
			delete_from_list(0);

	}
return 0;
}