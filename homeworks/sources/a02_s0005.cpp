#include<iostream>
#include<time.h>
#include<conio.h>
/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Construire Heap, metodele BottomUp si TopDown
*/
//Ambele metode au complexitate liniara

int dim1=0, dim2=0;				
long int asignari=0, comparari=0;

void generare_sir(int n, int *v) {		//generez sirul de elemente random
	int i;
	srand(time(NULL));
	//*v=rand()%10000;
	for(i=1;i<=n;i++)
		*(v+i)=rand()%10000;
}

int p(int i) {				//parintele elementului de pe pozitia i
	return i/2;
}

int st(int i) {				//fiul din stanga al elementului de pe pozitia i
	return 2*i;
}

int dr(int i) {				//fiul din dreapta al elementului de pe pozitia i
	return 2*i+1;
}



void reconstructie_heap(int *v, int i) {		//parcurge sirul si verifica daca elementele sunt in ordinea corespunzatoare structurii de heap
	int minim;									//(in cazul nostru, parintele are valoare mai mica decat fiii lui)
	int aux, m;
	minim=i;
	comparari++;
	if(st(i)<=dim1 && *(v+st(i))<*(v+i))
		minim=st(i);
	else
		minim=i;
	comparari++;
	if(dr(i)<=dim1 && *(v+dr(i))< *(v+minim))
		minim=dr(i);
	if(minim!=i) {
		aux=*(v+i);
		*(v+i)=*(v+minim);
		*(v+minim)=aux;
		asignari+=3;
		reconstructie_heap(v, minim);
	}
}

void Hpush(int *v,  int x) {					//se adauga un element in heap(la sfarsit), dupa care se parcurge tot heap-ul si se restabileste
	int i, aux;									//structura de heap, daca este cazul.	
	dim2++;
	*(v+dim2)=x;
	asignari++;
	i=dim2;
	while(i>1 && *(v+i)<*(v+p(i))) {
		aux=*(v+i);
		*(v+i)=*(v+p(i));
		*(v+p(i))=aux;
		i=p(i);
		comparari++;
		asignari+=3;
	}
	comparari++;
}
	
void constructie_td(int *v1,  int *v2) {		//se construieste heap-ul top down, mai exact, se apeleaza Hpush pentru fiecare element din sir1
	asignari=0;									// in sir2
	comparari=0;
	int i;
	for(i=1;i<=dim1;i++)
		Hpush(v2,  *(v1+i));
}

void constructie_bu(int *v,  int n) {			//se construieste heap-ul bottom up, mai exact, se apeleaza reconstructie pentru elementele cu
	asignari=0;									//de la 1 la n/2(pentru frunze nu se mai apeleaza pentru ca ele nu au niciun fiu)
	comparari=0;
	int i;
	dim1=n;
	for(i=n/2;i>=1;i--)
		reconstructie_heap(v,  i);
}

void printx(int *v, int i, int depth, int dim)	//functia de pretty_print
{
	 if (i<=dim) 
	 {
		printx(v,2*i+1,depth+1, dim);
		for (int j=0;j<depth;j++)
		printf("\t");
		printf("%d",*(v+i));
		printf("\n");
		printx(v,2*i,depth+1, dim);		
	 }
}

int main () {
	int *sir1, *sir2;
	long int amed_bu=0, amed_td=0, cmed_bu=0, cmed_td=0;
	int i, j;
	sir1=(int*)malloc((10001)*sizeof(int));
	sir2=(int*)malloc((10001)*sizeof(int));

	FILE *p;
	p=fopen("heap.csv", "w");
	fprintf(p, "n,a_BU,c_BU,a+c_BU,a_TD,c_TD,a+c_TD\n");
	for(i=100; i<=10000;i+=100) {
		for(j=1;j<=5;j++) {
			dim1=i;
			dim2=0;
			generare_sir(i, sir1);
			constructie_td(sir1, sir2);
			amed_td+=asignari;
			cmed_td+=comparari;
			constructie_bu(sir1, i);
			amed_bu+=asignari;
			cmed_bu+=comparari;
		}
		amed_td/=5;
		amed_bu/=5;
		cmed_td/=5;
		cmed_bu/=5;
		fprintf(p, "%d,%d,%d,%d,%d,%d,%d\n", i, amed_bu, cmed_bu, amed_bu+cmed_bu, amed_td, cmed_td, amed_td+cmed_td);
		printf("1");
	}

	fclose(p);
	return 0;
}