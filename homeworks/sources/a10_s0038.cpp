#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<cstdlib>

#define MAX 999999

typedef struct nod_inf{
	int key;
	struct nod_inf *urm;
}NOD_INF;

typedef struct nod{
	int key;
	int culoare;//0:alb,1:gri,2:negru
	int d;//timpul primei descoperiri
	int f;//timpul ultimei descoperiri
	struct nod *parinte;
	struct nod_inf *urm;
}NOD;

NOD *lista[210];
NOD_INF *cap=NULL,*ultim[210],*pr=NULL,*ult=NULL;
int n,nrOp=0,timp=0,s=1,nr;
int ok=0;//pentru sortare topologica(daca ramane 0 avem sortare topologica)

void init()
{
	for(int x=1;x<=n;x++)
	{
		NOD *p=(NOD *)malloc(sizeof(NOD));
		p->key=x;
		p->culoare=0;
		p->parinte=NULL;
		lista[x]=p;
		lista[x]->urm=NULL;
		ultim[x]=NULL;
		nrOp++;
	}
}

void adaugaNod(int x,int y)
{
	NOD_INF *q=(NOD_INF *)malloc(sizeof(NOD_INF));
	q->key=y;
	if(ultim[x]==NULL)
	{
		ultim[x]=q;
		lista[x]->urm=ultim[x];
	}
	else
	{
		ultim[x]->urm=q;
		ultim[x]=ultim[x]->urm;
	}
	ultim[x]->urm=NULL;
}	

void push(int x)
{
	NOD_INF *p=(NOD_INF *)malloc(sizeof(NOD));
	p->key=x;
	if(cap==NULL)
	{
		cap=p;
		cap->urm=NULL;
	}
	else
	{
		p->urm=cap;
		cap=p;
	}
}

NOD_INF *pop()
{
	NOD_INF *p=cap;
	if(cap!=NULL)
		cap=cap->urm;
	return p;
}

void dfs_visit(int i,int k)
{
	lista[i]->culoare=1;
	lista[i]->d=++timp;
	nrOp+=2;
	if(lista[i]->urm!=NULL)
	{
		for(NOD_INF *p=lista[i]->urm;p!=NULL;p=p->urm)
		{
			nrOp++;
			if(lista[p->key]->culoare==0)
			{
				printf("%d-%d muchie arbore\n",i,p->key);
				lista[p->key]->parinte=lista[i];
				dfs_visit(p->key,k);
			}
			else
			{
				if(lista[p->key]->culoare==1)
				{
					printf("%d-%d muchie inapoi\n",i,p->key);
					ok=1;
				}
				else
					if(lista[p->key]->f<lista[p->key]->d)
						printf("%d-%d muchie transversala\n",i,p->key);
					else
						printf("%d-%d muchie inainte\n",i,p->key);
				lista[p->key]->culoare=1;
			}
		}
		
	}
	nrOp+=2;
	lista[i]->culoare=2;
	lista[i]->f=++timp;
	push(i);
}

void dfs()
{
	int k=0;
	timp=0;
	for(int i=1;i<=n;i++)
	{
		nrOp++;
		if(lista[i]->culoare==0)
		{
			k++;
			dfs_visit(i,k);
		}
	}
}

void afisare(){
	for(int i=1;i<=n;i++)
	{
		printf("%d: ",lista[i]->key);
		if(lista[i]->urm!=NULL)
			for(NOD_INF *p=lista[i]->urm;p!=NULL;p=p->urm)
				printf("%d ",p->key);
		printf("\n");
	}
}

void afisare2()
{
	NOD_INF *q;
	if(ok==1)
		printf("\nNu are sortare topologica deoarece are muchie inapoi\n");
	else
	{
		printf("\nSortare topologica\n");
		while((q=pop())!=NULL)
			printf("%d-timp%d ",q->key,lista[q->key]->f);
	}
}


void generare()
{
	FILE *f=fopen("dfs_n_const.csv","w");
	FILE *g=fopen("dfs_m_const.csv","w");
	fprintf(f,"muchii,operatii\n");
	n=100;
	srand(time(NULL));
	//generare pt n constant
	for(int m=1000;m<=5000;m+=100)
	{
		init();
		for(int i=1;i<=m;i++)
		{
			
			int x=1+rand()%n;
			int y=1+rand()%n;
			if(x==y)
				y=1+rand()%n;
			adaugaNod(x,y);
		}
		nrOp=0;
		dfs();		
		fprintf(f,"%d,%d\n",m,nrOp);
	}
	//generare pt m constant
	int m=9000;
	fprintf(g,"nrNoduri,nrOperatii\n");
	for(n=110;n<=200;n+=10)
	{
		init();
		for(int i=1;i<=m;i++)
		{
			
			int x=1+rand()%n;
			int y=1+rand()%n;
			if(x==y)
				y=1+rand()%n;
			adaugaNod(x,y);
		}
		nrOp=0;
		dfs();
		fprintf(g,"%d,%d,\n",n,nrOp);
	}
}

int main()
{
	int m=6;
	n=6;
	int x[]={1,1,2,3,3,4};
	int y[]={2,3,3,4,6,5};
	init();
	for(int i=0;i<m;i++)
		adaugaNod(x[i],y[i]);
	printf("Graful original\n");
	afisare();
	printf("\nTipul muchiilor\n");
	dfs();
	afisare2();
	generare();
	getch();
	return 0;
}

