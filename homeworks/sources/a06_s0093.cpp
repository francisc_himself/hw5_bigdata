/*-------------------------Assignment No. 6: ------------------------------------------------------------
                                      The Josephus Permutation
	Student:					***ANONIM*** ***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Requirements:				You are required to implement correctly and efficiently an O(nlgn)-
                                 time algorithm which outputs the (n,m)-Josephus permutation, 
                                 for when m is not constant (problem 14.2 (b) from the book1).
   Implementation requirements:   You have to use a balanced, augmented Binary Search Tree. 
                                  Each node in the tree holds, besides the necessary information,
                                   also the size field (i.e. the size of the sub-tree rooted at the node). 
                                   First, you have to build a balanced BST containing the keys 1,2,...n 
                                   (hint: use a divide and conquer approach). 
                                   Then, at each step you have to select and delete the k-th element from the tree.
                                   
                                  The pseudo-code for the OS-SELECT procedure can be found in Chapter 14.1 from the book1. 
                                  For OS-DELETE, you may use the deletion from a BST, without increasing the height of 
                                  the tree (why don�t you need to rebalance the tree?). You have to be careful that, 
                                  at each step, the size information in each node be correct. There are several
                                   alternatives to update the size field without increasing the complexity of the 
                                   algorithm (it is up to you to figure this out).
                                   
                                   For BUILD_TREE, you have to write a procedure which builds a balanced BST from 
                                   the keys 1, 2, �, n. Make sure you initialize the size field in each tree node 
                                   in the BUILD_TREE procedure.
                                   
                                   Before you start to work on the algorithms evaluation code, make sure you have 
                                   a correct algorithm! You will have to prove your algorithm
                                   algorithm(s) work on a small-sized input: i.e. for 
                                  
                                   Josephus(7,3), pretty print the augmented BST after each step of your algorithm 
                                   (i.e. initial tree build, then after each OS_DELETE).
                                   Once you are sure your program works correctly, vary n from 100 to 10000 
                                   with step 100; for each n, choose the value of m as n/2.
                                   Evaluate the computational effort as the sum of the comparisons and assignments
                                    performed by your algorithm on each size. Is your algorithm running in O(nlgn) ?
                       
*/


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<conio.h>

typedef struct tip_nod{
		int key;
		int dim;
		struct tip_nod *p,*st,*dr;
	}NOD;
	
NOD *santinela;
int nrop;
int n=7; //n=10000;                //indicates the number of nodes in the tree

void init_santinela(void){
	//initialize santinela
	santinela=(NOD*)malloc(sizeof(NOD));
	santinela->key=0;
	santinela->dim=0;
	santinela->st=NULL;
	santinela->dr=NULL;
	santinela->p=NULL;

}
 
/*build a balanced binary tree */
NOD *BUILD_TREE(int a,int b){
	if(a>b) return santinela;
	int m=(a+b)/2;
	NOD *x=(NOD*)malloc(sizeof(NOD));//allocate new node
	x->key=m;						//the value of the key
	x->p=santinela;					//parent is the santinela
	x->st=BUILD_TREE(a,m-1);
	x->st->p=x;
	x->dr=BUILD_TREE(m+1,b);
	x->dr->p=x;
	x->dim=x->st->dim+x->dr->dim+1;//update size
	return x;
}
/*we compute r, the rank of node x within the subtree rooted at x.*/
NOD *OS_SELECT(NOD *x,int i){						
	if(x==NULL || x->key==0) return NULL;			//base case return
	int r=x->st->dim+1;								//the rank of x = number of nodes on the left subtree + itself
	++nrop;
	if(i==r){
		++nrop;
		return x;
	}else if(i<r){
		++nrop;
		return OS_SELECT(x->st,i);
	}else{
		++nrop;
		return OS_SELECT(x->dr,i-r);
	}
}

NOD *minim(NOD *x){
	while(x->st!=santinela){
		++nrop;
		x=x->st;
	}
	return x;
}

NOD *succesor(NOD *x){
	++nrop;

	if(x->dr!=santinela)
		return minim(x->dr);

	NOD *y=x->p;							//declare a parent
	++nrop;
	while(y!=santinela && x==y->dr){        //go untill there is a change of direction  or i meet the santinela
		x=y;
		y=y->p;
		nrop+=2;
	}
	return y;
}

void OS_DELETE(NOD **rad,NOD *z){
	NOD *y=NULL,*x=NULL;
	                         //z is the new node to delete , y is the phisically deleted one,will replace z
	nrop+=2;                
	if(z->st==santinela || z->dr==santinela){     //if node z is a leaf or a single child either 
		y=z;                                      // assign it to the phisically deleted node y
	}else{
		y=succesor(z);                                //or find replacement - the maxim on the right subtree
	}
	
	//all the nodes above the node which will be deleted will have the size =size-1
	x=y;
	++nrop;
    // so go from the current node upwards in the tree , and decrement its size 
	while(x!=santinela){ // && x!=NULL
		nrop+=2;
		--(x->dim);
		x=x->p;
		
	}
	//x=NULL;
	
	nrop+=2;
	if(y->st!=NULL && y->st!=santinela){   //if y is a single childed node with no child to the left
		x=y->st;                           //then x becomes y's child  to the right
	}else{
		x=y->dr;                           //otherwise x will be y's child to the left
	}
	nrop++;
	if(x!=NULL && x!=santinela){            //if x is not nill we set the grandparent of x/parent of y the parent of x
		x->p=y->p;
        ++nrop;
	}
	nrop++;
	if(y->p==NULL || y->p==santinela){   //case 3 : if y was the root then y's child becomes the new root
		*rad=x;
		++nrop;
	}else if(y==y->p->st){              //and make a link from y 's parent to x , x becomes its child either left or right
		y->p->st=x;
		nrop+=2;
	}else{
		y->p->dr=x;          
		nrop+=2;
	}
	++nrop;

	if(y!=z){                           //copy y's info into z and dealloc y
		z->key=y->key;
		++nrop;
	}
	//printf("deleteNode::%d\n",y->key);
	free(y);
}

void pretty_print(NOD *rooty,int align){
    //rooty- current node
    //align - spaces to the right
   
	if(rooty==santinela) return;
    char s[20];
    int i;
    sprintf(s,"%d ",rooty->key);    //first write in a string to determine its length 
	
    align+=strlen(s);				//increment align with its length 
    printf("%s",s);					//write it effectively
   
	if(rooty->st!=santinela)
        pretty_print(rooty->st,align);      //write the left son next 
   
	if(rooty->dr!=santinela){        //for the right son move to next line       
        printf("\n");
        for(i=0;i<align;++i)         //get the spaces for align
            printf(" ");
        pretty_print(rooty->dr,align);     //now write it
    }
}

void inordine(NOD *p, int nivel)
{
int i;
if ((p!=0)&&(p->key!=0)){
       inordine(p->st,nivel+1);       
	   for(i=0;i<=nivel;i++) printf("*");
       printf("%2d\n",p->key);
	   inordine(p->dr,nivel+1);
     }
}

/*
JOSEPHUS(n,m) 
T = BUILD_TREE(n)  
j ←1 
for k ←n downto 1 do 
     j ←(( j + m − 2) mod k) + 1  
	 x ←OS-SELECT(root[T], j )  
	 print key[x]  
	 OS-DELETE(T, x)  
*/

void JOSEPHUS(int n,int m){
	NOD *x=NULL;
	x=BUILD_TREE(1,n);
	NOD *y=NULL;
	int j=1;

	for(int k=n;k>=1;k--)
	{  
	    j=((j+m-2)%k)+1;
		y=OS_SELECT(x,j);
	printf("\ndelete key %d\n",y->key);
		OS_DELETE(&x,y); 
		nrop+=2;
	}
 //  printf("\n-----------------end Josefus-----------------------\n");
}


int main(void){
	init_santinela();
	FILE *f=fopen("josephson.csv","w");	

	FILE *rez=fopen("rez.txt","w");	
	NOD *x;	
	x=BUILD_TREE(0,n);	
	printf("\ninorder(tree):\n");
    inordine(x,0);	
	//printf("\ninorder(tree):\n");

	printf("\n---------------pretty_print:---------\n");
	pretty_print(x,0);
	//printf("\n---------------pretty_print:---------\n");
	JOSEPHUS(n,(int)(n/2));	
	//fprintf(f,"%d;%d\n");
	//fprintf(rez,"%3d;%3d\n");
  

/*	for(int step=100;step<=n;step+=100)
	{
	     nrop=0;
		 JOSEPHUS(step,(int)step/2);
		 fprintf(f,"%d;%d\n",step,nrop);
		 fprintf(rez,"%d;%d\n",step,nrop);
	}
	printf("nrNodes=%d \n nrop=%d\n",n,nrop);*/
	getche();
	fclose(f);
	fclose(rez);

	return 0;
}
