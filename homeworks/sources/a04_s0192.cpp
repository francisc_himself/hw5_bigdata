#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<conio.h>
/*O(nlogk) k const crestere linearea
Merge runs in time proportional to the sum of the lengths of the lists.
*/
typedef struct Node
{
	long info;
	struct Node *next;
}NodeT;


NodeT *a[10001];

long operations=0;
long heapSize;
NodeT *mList[10001];
int k;

void heapify(NodeT *A[], int i)
{
   int l,r,min;
   long aux;

   l=2*i;
   r=2*i+1;

   operations++;
   if (l<=heapSize && A[l]->info<A[i]->info)
       min=l;
   else
       min=i;

   operations++;
   if (r<=heapSize && A[r]->info<A[min]->info)
       min=r;

	if (min!=i)
       {
           operations+=3;
           A[0]=A[i];
           A[i]=A[min];
		   A[min]=A[0];
           heapify(A,min);
       }
}

void build_bottomup(NodeT *A[],int n)
{
   int i;
   heapSize=n;
   for (i=heapSize/2;i>=1;i--)
       heapify(A,i);
}

Node* insert_back(int val,Node *head)
{
	struct Node *temp,*right;
	temp = (struct Node *)malloc(sizeof(struct Node));
	right = (struct Node *)malloc(sizeof(struct Node));
	temp->info = val;
	temp->next=NULL;
	if (head == NULL)
	{
		head = temp;
		head->next=NULL;
	}
	else
	{
		right= head;
		while(right->next != NULL)
		{
			right=right->next;
		}
		right->next = temp;
	}

	return head;


}

void display_list(Node *head)
{
	Node *temp;
	temp = new Node;
	temp = head;
	while (temp != NULL)
	{
		printf("%d ",temp->info);
		temp = temp->next;
	}
	printf("\n");
}

int main()
{
   //FILE *f1,*f2,*f3,*f4;
   //f1=fopen("file1.txt","w");
   //f2=fopen("file2.txt","w");
   //f3=fopen("file3.txt","w");
   //f4=fopen("file4.txt","w");
   int m,n,size,nr,r,data;
   long op,sumop;
   sr***ANONIM***(time(NULL));
   k = 3;//3 lists
   n = 15;//each with 5 elem
   //Node* test=NULL;
   for (int i = 1; i <= k; i++)
   {
	   a[i] = NULL;
	   data = r***ANONIM***() % 100;
	   for (int j = 1; j <= 5; j++)
	   {
		   a[i] = insert_back(data,a[i]);
		   data += r***ANONIM***() % 100;
	   }
	}
		//print the k sorted lists
	printf("The lists are:\n");
	for (int m = 1; m <= k; m++)
	{
		display_list(a[m]);
	}
	printf("\n");
	size = k;
	nr  = 1;
	build_bottomup(a,size);

	while(size != 0)
	{
		mList[nr] = new Node;
		mList[nr]->info=a[1]->info;
		a[1]=a[1]->next;
		mList[nr]->next=NULL;
		nr++;

		if (a[1]==NULL)
		{
			a[1]=a[size];
			size--;
			heapSize--;
		}
		heapify(a,1);

	}

	printf("The merged list is: ");
	for (int i = 1; i <= n; i++)
		printf("%d ", mList[i]->info);
		//display_list(mList[i]);
	getch();

   /*k=5;
   for (int i=100;i<=10000;i+=100)
   {
		sumop = 0;
		for(int j=1;j<=5;j++)
		{
			operations = 0;
			for(int l=1;l<=k;l++)
			{
				a[l]=NULL;
				data = r***ANONIM***() % k;
				for(int m = 1; m <= i/k; m++)
				{
					a[l] = insert_back(data,a[l]);
					data+=r***ANONIM***()%k;
				}
			}

			size = k;
			nr =1;
			build_bottomup(a,size);

			while (size!=0)
			{
				mList[nr] = new NodeT;
				mList[nr]->info=a[1]->info;
				a[1]=a[1]->next;
				mList[nr]->next=NULL;
				nr++;

				if(a[1]==NULL)
				{
					a[1]=a[size];
					size--;
					heapSize--;
				}
				heapify(a,1);
			}
			sumop+=operations;

		}

		fprintf(f1,"%d %ld\n",i,sumop/5);
	}*/

   /*k=10;
   for (int i=100;i<=10000;i+=100)
   {
		sumop = 0;
		for(int j=1;j<=5;j++)
		{
			operations = 0;
			for(int l=1;l<=k;l++)
			{
				a[l]=NULL;
				data = r***ANONIM***() % k;
				for(int m = 1; m <= i/k; m++)
				{
					a[l] = insert_back(data,a[l]);
					data+=r***ANONIM***()%k;
				}
			}

			size = k;
			nr =1;
			build_bottomup(a,size);

			while (size!=0)
			{
				mList[nr] = new NodeT;
				mList[nr]->info=a[1]->info;
				a[1]=a[1]->next;
				mList[nr]->next=NULL;
				nr++;

				if(a[1]==NULL)
				{
					a[1]=a[size];
					size--;
					heapSize--;
				}
				heapify(a,1);
			}
			sumop+=operations;

		}

		fprintf(f2,"%d %ld\n",i,sumop/5);
   }*/

	/*k=100;
   for (int i=100;i<=10000;i+=100)
   {
		sumop = 0;
		for(int j=1;j<=5;j++)
		{
			operations = 0;
			for(int l=1;l<=k;l++)
			{
				a[l]=NULL;
				data = r***ANONIM***() % k;
				for(int m = 1; m <= i/k; m++)
				{
					a[l] = insert_back(data,a[l]);
					data+=r***ANONIM***()%k;
				}
			}

			size = k;
			nr =1;
			build_bottomup(a,size);

			while (size!=0)
			{
				mList[nr] = new NodeT;
				mList[nr]->info=a[1]->info;
				a[1]=a[1]->next;
				mList[nr]->next=NULL;
				nr++;

				if(a[1]==NULL)
				{
					a[1]=a[size];
					size--;
					heapSize--;
				}
				heapify(a,1);
			}
			sumop+=operations;

		}

		fprintf(f3,"%d %ld\n",i,sumop/5);
	}*/

   //testing for n constant
	//Set n = 10.000; the value of k must vary between 10 ***ANONIM*** 500 with an increment of 10;
   /*for (int i=10;i<=500;i=i+10)
    {
        sumop=0;
		for (int j=1;j<=5;j++)
        {
            operations=0;

            for (int l=1;l<=i;l++)
            {
                a[l]=NULL;
                data=r***ANONIM***() % l;
                for (m=2;m<=10000/i;m++)
                {
                   a[l] = insert_back(data,a[l]);
					data+=r***ANONIM***()%l;
                }
            }

            size=i;
            nr=1;

            build_bottomup(a,size);

            while (size!=0)
            {
               mList[nr]=new NodeT;
               mList[nr]->info=a[1]->info;
               a[1]=a[1]->next;
			   mList[nr]->next=NULL;

                if (a[1]==NULL)
                {
                    a[1]=a[size];
                    size--;
					heapSize--;
                }

                heapify(a,1);
            }

             sumop+=operations;

        }
        fprintf(f4,"%d %ld\n",i,sumop/5);
    }*/


   //fclose(f1);
   //fclose(f2);
   //fclose(f3);
   //fclose(f4);
}












