#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

/**In functie de factorul de umplere, numarul de operatii la inserare difera. 
Functia de cautare se opreste daca gaseste valoarea care doreste sa insereze sau daca gaseste un loc liber
Tabelul de hash cu factorul de amplificare mai mica are mai multe goluri, deci functia de cautare face un efort mai mic
*/

#define c1 1
#define c2 1

typedef struct tip_nod {
                int cheie;
                struct tip_nod *urm;
} TIP_NOD;

int HT[10001]; //tabelul de dispersie
int gasit[1501];
int negasit[1501];
int nr_cautari=1500;//1500;
int N=10007;//nr de elemente = 10007
float alfa[]={0.8, 0.85, 0.9, 0.96, 0.99};//factor de amplificare
int efort;//
int b,c,bm,cm;//b=efort gasit; c=efort negasit; m=maxim;
int maxim; /// pentru generarea cele negasite
int temp[10001]; // un sir temp care contine valorile intainte de inserare in HT[]

int h(int k, int i)
{
    int temp = (k % N + c1*i + c2*i*i) % N;
    return temp;
}

void initializare(int k)
{
    int i;
    for(i=0;i<k;i++)
        HT[i]=NULL;
}

int insert(int k)
{
    int j, i=0;
    do
    {
        j=h(k,i);
		if (HT[j]==k)//daca mai e un k in sir
			return 0;
        if (HT[j]==NULL)
        {
            HT[j]=k;
            return j;
        }
        else
            i++;
    }while (i!=N);
    return 0;//nu se mai poate insera
}

int search(int k)
{
    int i=0;
	efort=0;
    int j;
    do
    {
        j=h(k,i);
        if(HT[j]==k)
            return j;
        i++;
		efort=i+1;////////////////////////
        //printf("\n ind %d %d",i,N);
    }while((HT[j]!=NULL) && (i!=N));
    return 0;
}

void afisare(int HT[])
{
	int i;
	for(i=0; i<N; i++)
	{
		printf("\n%d:",i);
		if(HT[i]!=0) 
			printf(" %d",HT[i]);
	}
}

int gen()
{
	int x=rand();
	if (x>maxim) maxim=x;
    return x;
}

void gengasit()
{
	int i;
	int k= rand()%N;
	for(i=0; i<nr_cautari; i++)
	{
		if (temp[k]==0)
			while(temp[k]==0)
			{
				k=rand()%N;
			}
		gasit[i]=temp[k];
		k=rand()%N;
	}
}

void gennegasit()
{
	int i;
	for(i=0; i<nr_cautari; i++)
	{
		//int x=rand()%n;
		//while(x % 2 ==0)
			//x=rand()%n;
		negasit[i]=maxim + rand();
	}
}

void main()
{
	//VARIABILE
	int n; //numarul de inserat
    int nr=0;//numarul de inserari reusite
    srand(time(NULL));
    int i;// nr de factor de umplere
	int j;// nr element cand cautam
	int k;// de 5 ori pentru avg
    float a;//factoul de umplere curenta
	float baux=0, caux=0; // auxiliare pentru avg
	float btot=0, ctot=0;

	//DEMO
	N=11;
	nr_cautari=5;
	nr=0;
	initializare(10007);
	maxim=0;
	int o=0;
    do
    {
        n=gen();
        if(insert(n))
		{
            nr++;
			temp[o]=n;
			o++;
		}
        a=(float)nr/N;
    }while(a< 0.8 );
	afisare(HT);
	printf("\nAm pun %d numere",nr+1);
	

	gengasit();
	gennegasit();

	for(j=0; j<nr_cautari; j++)// verificam gasirile
	{
		search(gasit[j]);
		printf("\ncaut %d din gasit cu efortul: %d",gasit[j],efort);
		b=efort;
		baux+=b;
		if(b>bm) bm=b; //stockez maximul
		efort=0;
		search(negasit[j]);
		printf("\ncaut %d din negasit cu efortul: %d",negasit[j],efort);
		c=efort;
		caux+=c;
		if(c>cm) cm=c; //stockez maximul
		efort=0;
	}
	printf("\nfactorul: %f, \navggasit: %f, maxgasit: %d,  \navgnegasit: %f maxnegasit: %d\n", a,baux/nr,bm,caux/nr,cm);

	//PROGRAM
	N=10007;
	nr_cautari=1500;
	nr=0;
	initializare(10007);
	maxim=0;
	FILE *f;
	f=fopen("heap.txt","w");
	fprintf(f,"efort avg_efort_gasit max_efort_gasit avg_efort_negasit max_efort_negasit\n");

    for(i=0; i<5; i++)///////////////////pentru fiecare factor de umplere
    {
		int o=0;
        initializare(N);
        do
        {
            n=gen();
            if(insert(n))
			{
                nr++;
				temp[o]=n;
				o++;
			}
            a=(float)nr/N;
        }while(a<alfa[i]);// generam sirul


		for(k=0; k<5; k++)// de 5 ori
		{
			gengasit();
			gennegasit();
			for(j=0; j<nr_cautari; j++)// verificam gasirile
			{
				search(gasit[j]);
				b=efort;
				baux+=b;
				if(b>bm) bm=b; //stockez maximul
				efort=0;
				search(negasit[j]);
				c=efort;
				caux+=c;
				if(c>cm) cm=c; //stockez maximul
				efort=0;
			}
			btot+=baux/nr_cautari;
			ctot+=caux/nr_cautari;
			baux=caux=0;
		}
		//printf("\nbm e %d cm e %d\n",bm, cm);
		fprintf(f,"%f %f %d %f %d\n", alfa[i],btot/5,bm,ctot/5,cm);
        printf("\nAm pus %d numere\n",nr+1);
        nr=bm=cm=btot=ctot=b=c=0;
    }	

    getch();
}
