//***ANONIM*** ***ANONIM***-Wilhelm
//***GROUP_NUMBER***

//OBS : In cazul mediu nr 1, unde nr total de elemente variaza intre 100 - 10000 si k ia pe rand valorile : 5, 10, 100, avem pentru k=5 
// evident o eficienta mai buna decat in celelalte cazuri.
// In cazul mediu 2, unde nr total de elemente (n) este de 10000 si k variaza intre 10 - 500 cu un incremenet de 10, nr de comparatii si
// nr de atribuiri formeaza o functie logaritmica.

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

//Declaram structura unui nod din lista
typedef struct ListItem
{
    long valoare;
    struct ListItem *next;
}ListItem;

ListItem *v[10001];   //Vectorul de liste
ListItem *l_primul,*l_ultimul,*l_current;   //pointeri la primul,ulrimul si elementul curent pentru crearea listei finale ordonate
long nr_operatii;

//Functie de adaugare in Lista Finala Sortata
void addToFinalList(int add_val)
{
    if (l_primul==NULL)
    {
        l_primul=(ListItem*)malloc(sizeof(ListItem*));
        l_primul->valoare=add_val;
        l_primul->next=NULL;
        l_ultimul=l_primul;
    }
        else
    {
        l_current=(ListItem*)malloc(sizeof(ListItem*));
        l_current->valoare=add_val;
        l_current->next=NULL;
        l_ultimul->next=l_current;
        l_ultimul=l_current;
    }
}

//Functie pentru afisarea Listei Finale Sortate
void printFinalList()
{
    ListItem *current;
    current=l_primul;
    while(current!=NULL)
    {
        printf("%ld ",current->valoare);
        current=current->next;
    }
}

int stg(int i)
{
	return 2*i;
}

int dr(int i)
{
	return (2*i)+1;
}

//Verifica daca Heap-ul e gol
//Returneaza 1 daca e gol, else 0
int heapIsEmpty(ListItem *A[],int n)
{
    int i;
    int empty=1;
    for (i=1;i<=n;i++)
    {
        if (A[i]!=NULL)
        {
            empty=0;
        }
    }
    return empty;
}

//Reconstructie Heap (MinHeap)
//A[] vectorul
//i elementul initial
//n lungimea lui A
void Reconstructie_Heap(ListItem *A[],int i,int n)
{
	int stanga=stg(i);
	int dreapta=dr(i);
	int poz_max;
	ListItem *aux;
	poz_max=i;

	nr_operatii++;
	if(stanga<=n && A[stanga]->valoare < A[poz_max]->valoare)
	{
		poz_max=stanga;
	}
	
	nr_operatii++;
	if(dreapta<=n && A[dreapta]->valoare < A[poz_max]->valoare)
	{
		poz_max=dreapta;
	}

	if(poz_max !=i)
	{
		nr_operatii+=3;
		aux=A[i];
		A[i]=A[poz_max];
		A[poz_max]=aux;
		Reconstructie_Heap(A,poz_max,n);
	}
}

//folosim metoda Bottom-Up
void Bottom_Up(ListItem *A[],int n)
{
	int i;
	for(i=n/2;i>=1;i--) 
		Reconstructie_Heap(A,i,n);
}

//Interclasarea
void creareListaFinalaSortata(ListItem *A[],int k1)
{
    int k;
    int element_scos;
    ListItem *aux;
    l_primul=NULL;
    l_current=NULL;
    l_ultimul=NULL;
    Bottom_Up(A,k1);
    k=k1;
    while(heapIsEmpty(A,k)==0)
    {
        element_scos = A[1]->valoare;
        addToFinalList(element_scos);
        if (A[1]->next != NULL)
        {
            nr_operatii++;
            A[1]=A[1]->next;
        }
            else
        {
            nr_operatii+=3;
            aux=A[1];
            A[1]=A[k];
            A[k]=aux;
            k--;
        }
        Reconstructie_Heap(A,1,k);
    }

}

// k=Numarul de Liste
// n=Numarul Total de Elemente
void creareVectorDeListe(int k,int n)
{
    ListItem *primul,*current,*ultimul;
    primul=NULL;
    current=NULL;
    ultimul=NULL;
    srand(time(NULL));
	//construim prima lista. Ea va fi mai lunga decat restul in cazul in care numarul de elemente n nu e divizibil cu numarul de liste k
    for (int i=0;i < n/k + n%k;i++) 
    {
        if (primul==NULL)
        {
            primul=(ListItem*)malloc(sizeof(ListItem*));
            primul->valoare=rand() % 1000;
            primul->next=NULL;
            ultimul=primul;
        }
            else
        {
            current=(ListItem*)malloc(sizeof(ListItem*));
            current->valoare=ultimul->valoare + i + rand() % 100;
            current->next=NULL;
            ultimul->next=current;
            ultimul=current;
        }
    }
    
	v[1]=primul;    //punem prima lista in vectorul de liste
    
	// cream si restul listelor
	for (int i=2;i <= k;i++)
    {
        primul=NULL;
        current=NULL;
        ultimul=NULL;

        for (int j=0;j < n/k;j++)
        {
            if (primul==NULL)
            {
                primul=(ListItem*)malloc(sizeof(ListItem*));
                primul->valoare=rand() % 1000;
                primul->next=NULL;
                ultimul=primul;
            }
                else
            {
                current=(ListItem*)malloc(sizeof(ListItem*));
                current->valoare=ultimul->valoare + j + rand()%100;
                current->next=NULL;
                ultimul->next=current;
                ultimul=current;
            }
        }

        v[i]=primul;
    }
}

//Functie pentru afisarea vectorului de liste
void afisareVectordeListe(ListItem *A[],int k)
{
    ListItem *current;
    current=NULL;
    for (int i=1;i<=k;i++)
    {
        current=A[i];
        
		while(current!=NULL)
        {
            printf("%ld ",current->valoare);
            current=current->next;
        }
        printf("\n");
    }

}

void test()
{
    int n,k;
    n=20;
    k=4;
    creareVectorDeListe(k,n);
    afisareVectordeListe(v,k);
    printf("\n");
    creareListaFinalaSortata(v,k);
    printFinalList();
    printf("\n");
}
void cazul_mediu_1()
{
    int n;
    FILE *f;
    f=fopen("CazMediu_1.csv","w");
    fprintf(f,"n,k1=5,k2=10,k3=100\n");
    int k1=5,k2=10,k3=100;
    for (n=100;n<=10000;n=n+100)
    {
		printf("%d ",n); 
        fprintf(f,"%d,",n);

        nr_operatii=0;
        creareVectorDeListe(k1,n);
        creareListaFinalaSortata(v,k1);
        fprintf(f,"%ld,",nr_operatii);

        nr_operatii=0;
        creareVectorDeListe(k2,n);
        creareListaFinalaSortata(v,k2);
        fprintf(f,"%ld,",nr_operatii);

        nr_operatii=0;
        creareVectorDeListe(k3,n);
        creareListaFinalaSortata(v,k3);
        fprintf(f,"%ld\n",nr_operatii);
    }
    
    nr_operatii=0;
	fclose(f);
}
void cazul_mediu_2()
{
    int n=10000;
    int k;
    FILE *f;
    f=fopen("CazMediu_2.csv","w");
    fprintf(f,"k,nr_operatii\n");
    for (k=10;k<=500;k=k+10)
    {
		printf("%d ",k);
        nr_operatii=0;
        creareVectorDeListe(k,n);
        creareListaFinalaSortata(v,k);
        fprintf(f,"%d,%ld\n",k,nr_operatii);
    }
    
    nr_operatii=0;
	fclose(f);
}
int main()
{
	  //test();
	  cazul_mediu_1();
	  cazul_mediu_2();
	  getch();
	  return 0;
}
