/*Din graficele obtinute dupa rularea programului se observa
ca algoritmul bubble face cele mai multe comparari, 
fiind urmat de selectie, apoi insertion cu cele mai putine comparari.

La capitolul de asignari bubble face cele mai multe apoi insertion
selectie face atat de putine incat pe grafic sta pe linia de 0

Din graficul "algoritmi" observam ca selectie este cel mai
optim algoritm de sortare, urmat de insertion care la randul lui
este urmat de bubble*/
#include <conio.h>

#include <iostream>
using namespace std;
#include "Profiler.h"
Profiler profiler("demo");
void bubble(int *v, int n)
{
bool ok=0;
int x;
while (!ok)
{
 ok=1;
 for (int j=0;j<n-1;j++){
	 profiler.countOperation("bubble_comparare",n);
	 if (v[j]>v[j+1])
	 {
		 profiler.countOperation("bubble_assign",n,4);
	 x=v[j];
	 v[j]=v[j+1];
	 v[j+1]=x;
	 ok=0;
	 }
 }
}
/*for (int i=0;i<n;i++)
	{
		cout << v[i]," ";
	}*/

}
void selectie(int *v, int n)
{
	int imin;
	int z;
for(int i=0;i<n-1;i++)
{
	imin=i;
	for (int j=i+1;j<n;j++)
	{
		profiler.countOperation("selectie_comparare",n);
     if (v[j]<v[imin])
		 
	 {imin=j;
	 profiler.countOperation("selectie_assign",n);
	 }
     }
	profiler.countOperation("selectie_assign",n, 3);
	z=v[imin];
	v[imin]=v[i];
	v[i]=z;
}
/*for (int i=0;i<n;i++)
	{
		cout << v[i]," ";
	}*/
}
void insertie(int *v, int n)
{
	int x;
	int j;
	for (int i=1;i<n;i++)
	{
		profiler.countOperation("insertie_assign",n,2);
		x=v[i];
		j=0;
profiler.countOperation("insertie_comparare",n);
		while (v[j]<x)
		{profiler.countOperation("insertie_assign",n);
			j++;
		profiler.countOperation("insertie_comparare",n);
		}
		for (int k=i;k>j;k--)
		{
			profiler.countOperation("insertie_assign",n);
			v[k]=v[k-1];
		}
		profiler.countOperation("insertie_assign",n);
		v[j]=x;
	}
	
/*for (int i=0;i<n;i++)
	{
		cout << v[i]," ";
	}*/
}
void main()
{
	
	/*int test[]={4,9,8,3,1};
	int test2[]={4,9,8,3,1};
	int test3[]={4,9,8,3,1};
	bubble(test,5);
	selectie(test2,5);
	insertie(test3,5);*/
	int n;
	int v[10000],v1[10000];
	for (int i=0; i<5;i++){
	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);
		memcpy(v1,v,n*sizeof(int));
		selectie(v1,n);
		memcpy(v1,v,n*sizeof(int));
		insertie(v1,n);
	}
	} //asta este pentru cazul random
	/*for (int i=0; i<5;i++){
	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,10,50000, false, 1);
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);
		
	}
	}*/ //bubble caz cel mai favorabil(ordine ascendenta)
	/*for (int i=0; i<5;i++){
	for (n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n,10,50000, false, 2);
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);
		
	}
	}*/ //cazul bubble cel mai defavorabil(ordine descendenta)

	profiler.addSeries("bubble","bubble_comparare","bubble_assign");
	profiler.addSeries("selectie","selectie_comparare","selectie_assign");
	profiler.addSeries("insertie","insertie_comparare","insertie_assign");
	profiler.createGroup("Comp","bubble_comparare","selectie_comparare","insertie_comparare");
	profiler.createGroup("Asign","bubble_assign","selectie_assign","insertie_assign");
	profiler.createGroup("algoritmi","bubble","selectie","insertie");
	profiler.showReport();
	
	
}