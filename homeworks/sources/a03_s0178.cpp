/*
It is obvious that the bottom-up approach is always more efficient than the top-down approach.
In the worst case, top-down has a running time of n*log(n), but for the rest of the cases, both
top-down and bottom-up approaches have a linear execution time.
*/

#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<math.h>
#include<conio.h>
#include<time.h>

using namespace std;
ofstream fout;

void maxHeapify (int a[], int n, int i);
int left (int i);
int right (int i);
int parent (int i);
void buildMaxHeap (int a[], int n);
int maxArr (int a[], int n);
void increaseKey (int a[], int i, int key);
void maxHeapInsert (int a[], int n, int key);
void writeToFile (long long atr, long long comp, char * fileName);
void prettyPrinting (int a[], int n);

long long comparatiitd, comparatiibu, atribuiritd, atribuiribu;




void main(){
	int a[10000], b[10000], c[10000], n, k=1, j=1, m=1;
	printf ("Type in n ");
	scanf("%d", &n);
	printf ("Type in the array ");
	for (int i=1;i<=n;i++) {
		scanf ("%d", &a[i]);
		b[i]=a[i];
	}

	buildMaxHeap (a, n+1);
	
	//pretty print
  
	prettyPrinting (a, n);
  
	printf("\n\n\n");
	
	for (int k=1;k<=n;k++)
		maxHeapInsert (c, k-1, b[k]);

	prettyPrinting (c, n);
	
	




	for (int i=100;i<10000;i=i+100){
		atribuiritd=atribuiribu=comparatiitd=comparatiibu=0;
		for (int k=0;k<5;k++){
			srand(time(NULL));
			for (int j=1; j<=i; j++){
				a[j]=rand();
				b[j]=a[j];
			}
			

			buildMaxHeap(a, i+1);
			for (int y=1;y<=i;y++)
				maxHeapInsert (c, y-1, b[y]);
		}
		fout.open ("average.csv", ios::app );
		fout<<i;
		fout.close();


		comparatiibu=comparatiibu/5;
		atribuiribu=atribuiribu/5;
		writeToFile (atribuiribu, comparatiibu, "average.csv");

		atribuiritd=atribuiritd/5;
		comparatiitd=comparatiitd/5;
		writeToFile (atribuiritd, comparatiitd, "average.csv");

		fout.open ("average.csv", ios::app );
		fout<<'\n';
		fout.close();		
	}

	for (int i=100; i<=10000; i=i+100) {
		for (int j=1;j<=i;j++){
			a[j]=j;
			b[j]=j;
		}

		fout.open ("worst.csv", ios::app );
		fout<<i;
		fout.close();

		atribuiritd=atribuiribu=comparatiitd=comparatiibu=0;

		buildMaxHeap(a, i+1);
		
		for (int y=1;y<=i;y++)
			maxHeapInsert (c, y-1, b[y]);

		writeToFile (atribuiribu, comparatiibu, "worst.csv");
		writeToFile (atribuiritd, comparatiitd, "worst.csv");

		fout.open ("worst.csv", ios::app );
		fout<<'\n';
		fout.close();
	}
	

	getch();
	
}




int left (int i){
	return 2*i;
}




int right (int i) {
	return 2*i+1;
}




int parent (int i) {
	return (int)floor((float)i/2);
}




void maxHeapify (int a[], int n, int i){
	int l, r, largest, aux;
	l=left(i);
	r=right(i);
	if (l<n && a[l]>a[i])
		largest = l;
	else
		largest = i;
	comparatiibu++;
	if (r<n && a[r]>a[largest]) 
		largest = r;
	comparatiibu++;
	if (largest!=i) {
		aux=a[i];
		a[i]=a[largest];
		a[largest]=aux;
		atribuiribu=atribuiribu+3;
		maxHeapify (a, n, largest);
	}
}

void buildMaxHeap(int a[], int n) {
	for (int i=(int)floor((float)n/2);i>=1;i--) {
		maxHeapify (a, n, i);
	}
}

int maxArr (int a[], int n){
	int max=-32000;
	for (int i=1;i<n;i++){
		if (a[i]>max) {
			atribuiritd++;
			max=a[i];
		}
		comparatiitd++;
	}
	return max;
}


void increaseKey (int a[], int i, int key){
	int err=0, aux;
	if (key<a[i]) {
		atribuiritd++;
		err=1;
	}
	comparatiitd++;
	if (err==0) {
		atribuiritd++;
		a[i]=key;
	}
	comparatiitd++;
	while (i>1 && a[parent(i)]<a[i]){
		comparatiitd++;
		atribuiritd=atribuiritd+3;
		aux=a[i];
		a[i]=a[parent(i)];
		a[parent(i)]=aux;
		i=parent(i);
	}
	comparatiitd++;
	
}

void maxHeapInsert (int a[], int n, int key){
	n=n+1;
	a[n]=-32000;
	atribuiritd++;
	increaseKey(a, n, key);
}

void writeToFile (long long atr, long long comp, char * fileName) {
	fout.open (fileName, ios::app );
	fout<<','<<atr<<','<<comp<<','<<(atr+comp);
	fout.close();
}

void prettyPrinting (int a[], int n) {
	int l = n, niv = 0;
    while (l!=0){
	   l=l/2;
	   niv++;	
    }
	int space=2*niv-1, y=0, nrafis=0, nrafis2=0, rowcount=0, row=1, t=9;
	for (int i=0;i<2*niv-1;i++) {
	   if (space>0)
			for (int j=0;j<space;j++){
				 printf (" ");
			}
	   space--;

	   if (i%2==0) {
		   if (i==0){
			   nrafis++;
			   nrafis2++;
			   printf("%d \n", a[1]);
		   }
		   else{
			for (int j=nrafis;j<i+nrafis&&j<n;j++) {
				if (rowcount%2==0){
					printf("%d",a[j+1]);
					int pow=1;
					for (int z=0;z<niv/i;z++)
						pow=pow*3;
					for (int z=0;z<=pow+1;z++)
						printf(" ");
					rowcount++;
				}
				else {
					printf("%d",a[j+1]);
					for (int z=t; z>0; z--)
						printf(" ");
					t=t-4;
					rowcount++;
				}
				nrafis2++;
				}
			nrafis=nrafis2;
			printf("\n");
			rowcount=0;
		   }
	   }
	   if(i%2==1) {
		 /*  row=row*2;
		   for (int x=0;x<row;x++){
			   if (x%2==0){
				   printf("/");
				   int pow=1;
				   for (int z=0;z<niv/i;z++)
					   pow=pow*3;
				   for (int z=0;z<pow;z++)
					   printf(" ");
			   }
			   if (x%2==1){
				   printf("\\ ");
				   int pow=1;
				   for (int z=0;z<niv/i;z++)
					   pow=pow*3;
				   for (int z=0;z<pow*2;z++)
					   printf(" ");
			   }
		   }*/
		   printf("\n");
	   }
   }
}
