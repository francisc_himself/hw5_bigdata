/*
* Student: ***ANONIM*** ***ANONIM*** ***ANONIM***
* Grupa: ***GROUP_NUMBER***
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h> 

//variabile globale
int n,dim;
int hAt=0,hComp=0,qAt=0,qComp=0;

/////////// Crearea heap-ului Bottom-Up //////////////
//returneaza indicele elementului din stanga
int stanga(int j)
{
  return (2*j) ;
}

//returneaza indicele elementului din dreapta
int dreapta(int j)
{
  return (2*j) + 1;
}

void H_Push(int a[], int i) 
{  
  int l = stanga(i);
  int r = dreapta(i);
  int t;

  hComp++;
  if ((l <= n) && (a[l] > a[i])) 
   {
     t = l;
   }
  else 
   {
     t = i;
   }

  hComp++;
  if ((r <= n) && (a[r] > a[t])) 
   {
     t = r;
   }
  if (t!=i) 
   {
    int temp = a[i];
    a[i] = a[t];
    a[t] = temp;
	hAt=hAt+3;
    H_Push(a, t);
   }
}

void Build_H_BU(int a[])
{
	for (int i=n/2; i > 0;i--) 
	{
		H_Push(a, i); 	
	}
}

///////////Crearea array-ului dupa Bottom-Up//////////////
 void H_Sort(int a[], int b[])
  {
	int k;
	hAt=0,hComp=0;
	dim=n;
	while(n!=0)
	 {
	   b[n]=a[1];			
	   hAt++;
	   k=a[1];			
	   hAt=hAt+3;
	   a[1]=a[n];
	   a[n]=k;	
	   n--;
	   //refac heap-ul
	   H_Push(a,1);
	 }
	n=dim;	 
 }

 int partitionare(int a[], int p, int r)
  {
	 int pivot,i,aux;
	 pivot=a[r];
	 i=p-1;
	 for(int j=p; j<=r-1; j++)
	  {
		 qComp++;
		 if(a[j]<=pivot)
		  { 
			i=i+1;
			qAt=qAt+3;
			aux=a[i];
			a[i]=a[j];
	 		a[j]=aux;
		  }
	  }
	 qAt=qAt+3;
	 aux=a[i+1];
	 a[i+1]=a[r];
	 a[r]=aux;
  return (i+1);
 }

 void Quick_Sort(int a[], int l, int r)
  {
	 int p;
	 if (l<r)
	  {
		p = partitionare(a, l, r);
		Quick_Sort(a,l,p-1);
		Quick_Sort(a,p+1,r);
	  } 
  }

int main()
 {
	int a[10001],b[10001],c[10001];
    FILE *f; f=fopen("tabel.csv","w"); 
	int aux0[2],aux1[2];

	fprintf(f,"n,hea_at,hea_comp,heap_at+comp,quick_at,quick_comp,quick_at+comp \n"); 
	srand (time(NULL));
	for(n=100;n<=10000;n=n+100)
	 {
		aux0[0]=0;aux0[1]=0;
	    aux1[0]=0;aux1[1]=0;

		for(int m=1; m<=5; m++)
		 {	
		    int i;
			
			for(i=1; i<=n; i++)
			 {				
			   a[i]=rand()%1000;
			   c[i]=a[i];
			 }	

			Build_H_BU(c);
			hAt=0;
			hComp=0;

			H_Sort(c,b);

			aux0[0] = aux0[0]+ hAt;
			aux1[0] = aux1[0]+ hComp ;

			qAt=0;
			qComp=0;
			Quick_Sort(a,1,n);
			aux0[1] = aux0[1]+ qAt;
			aux1[1] = aux1[1]+ qComp;
		}
		fprintf(f, "%d,%d,%d,%d,%d,%d,%d \n",n,aux0[0]/5,aux1[0]/5,(aux0[0]+aux1[0])/5 ,  aux0[1]/5,aux1[1]/5,(aux0[1]+aux1[1])/5 );
	}
	return 0;
}