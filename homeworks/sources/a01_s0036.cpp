#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <time.h>




int a[10000],b[10000],c[10000];
int n;
int comp,attr,sum;
 long compI=0,attrI=0,compS=0,attrS=0; 
 long compB=0,attrB=0;


//Functie ce implementeaza algoritmul de sortare Bubble - metoda bulelor
void sort_bulelor(int a[],int n)
{
	int i,j; 
	int aux;

	 for (i=1;i<=n-1;i++)  //prima parcurgere a vectorului, cu iteratorul i
		 for (j=i+1;j<=n;j++)       // a doua parcurgere a sirului, incepand de la pozitia i+1, cu iteratorul j
		{
			
			if (a[i]>a[j])       //daca doua elemente nu sunt in ordine crescatoare
			{
				aux=a[i];        //interschimbare
				a[i]=a[j];
				a[j]=aux;
				attrB=attrB+3;
			}
			compB++;
		}
	
}

//Functie ce implementeaza algoritmul de sortare prin insertie
void sort_insertie(int a[],int n)
{
	int i,j,x;

	for (i=2;i<=n;i++)              //parcurgerea vectorului cu iteratorul i
	{
		x=a[i];
		j=i-1;
		compI++;
		while ((x<a[j]) && (j>=0))   //caut pozitia la care ar trebui sa se afle elementul curent pt ca sirul sa fie ordonat
		{
			{
				attrI++;
				a[j+1]=a[j];
				j--;
			}
			attrI++;
		    a[j+1]=x;                //il inserez la pozitia gasita
		}
	}
}

//Functie ce implementeaza algoritmul de sortare prin selectie
void sort_selectie(int a[],int n)
{
	int i,j,im,aux;

	for (i=1;i<=n-1;i++)              //prima parcurgere a vectorului, cu iteratorul i
	{
		im=i;   //presupun elementul curent minim

		for (j=i+1;j<=n;j++)          //a doua parcurgere a vectorului, cu iteratorul j
		{
			compS++;
			if (a[j]<a[im])           //localizez minimul
			{
				im=j;
			}
		}
		if (im!=i)	                  // inserschimb minimul si elementul curent daca acesta nu era minimul
		 {
		  aux=a[i];
		  a[i]=a[im];
		  a[im]=aux;
		  attrS=attrS+3;
		}		
	}
	
}
//Functie care afiseaza vectorul
void afisare(int a[],int n)
{
	int i;
	for(i=1;i<=n;i++)
		printf("%d",a[i]);

}



void main()
{

    FILE *f=fopen("favorabil.csv","w");
	for(n=100;n<=10000;n+=100)
	{
		    compI=0;
			attrI=0;
			compS=0;
			attrS=0; 
            compB=0;
            attrB=0;
		for (int j=0;j<n;j++)
		{
			a[j]=j;//favorabil
			b[j]=j;
			c[j]=j;
		}
		sort_bulelor(a,n);
		sort_insertie(b,n);
		sort_selectie(c,n);
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compB,attrB,compI,attrI,compS,attrS,(compB+attrB),(compI+attrI),(compS+attrS));
	}
    
	/*FILE *f=fopen("defavorabil.csv","w");	
     for(n=100;n<=10000;n+=100)
	{
		    compI=0;
			attrI=0;
			compS=0;
			attrS=0; 
            compB=0;
            attrB=0;
		for (int j=0;j<n;j++)
		{
			
	        a[j]=n-j;//defavorabil
			b[j]=j;
			c[j]=j;
		}
		sort_bulelor(a,n);
		sort_insertie(b,n);
		sort_selectie(c,n);
		fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compB,attrB,compI,attrI,compS,attrS,(compB+attrB),(compI+attrI),(compS+attrS));
	}*/

	 /*FILE *f=fopen("mediu.csv","w");
	  for(n=100;n<=10000;n+=100)
	{
		    compI=0;
			attrI=0;
			compS=0;
			attrS=0; 
            compB=0;
            attrB=0;

		for(int k=1;k<=5;k++)
		{
		 srand(time(NULL));//mediu
		for (int j=0;j<n;j++)
		{
	        a[j]=rand();//mediu
		  	b[j]=j;
			c[j]=j;
		}
		srand(1);
		sort_bulelor(a,n);
		sort_insertie(b,n);
		sort_selectie(c,n);
	}
	    fprintf(f,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",n,compB/5,attrB/5,compI/5,attrI/5,compS/5,attrS/5,(compB+attrB)/5,(compI+attrI)/5,(compS+attrS)/5);
	}*/
 getch();
}