/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Quicksort 
- este un celebru algoritm de sortare, dezvoltat de C. A. R. Hoare si care,
in medie, efectueaza O(n*log n) comparatii pentru a sorta n elemente. 
In cazul cel mai rau, efectueaza O(n^2) comparatii. De obicei, in practica,
quicksort este mai rapid decat ceilalti algoritmi de sortare de complexitate O(n*log n)
deoarece bucla sa interioara are implementari eficiente pe majoritatea arhitecturilor si, 
in plus, in majoritatea implementarilor practice se pot lua, la proiectare, 
decizii ce ajuta la evitarea cazului cand complexitatea algoritmului este de O(n^2)

HeapSort
-este unul din algoritmii de sortare foarte performanti, fiind de clasa O(n�log2(n))
-Mai este cunoscut sub denumirea de �sortare prin metoda ansamblelor�
-HeapSort este un algoritm de sortare �in situ�, adica nu necesita structuri de date suplimentare,
ci sortarea se face folosind numai spatiul de memorie al tabloului ce trebuie sortat

*/


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_SIZE 100000

//crearea unui sir cu elemente aleatoare
void creare_sir(int sir[], int n)
{
	for(int i=1;i<=n;i++)
		sir[i]=rand();
}

//creare sir ordonat in ordine crescatoare
void creare_sir_crescator(int sir[],int n)
{
	int i;
	srand(time(NULL));
	sir[0]=rand()%101;
	for(i=1;i<n;i++)
	{
		sir[i]=sir[i-1]+rand()%101;
	}
}

//creare sir ordonat in ordine descrescatoare
void creare_sir_descrescator(int sir[],int n)
{
	int i;
	srand(time(NULL));
	sir[0]=rand()%101;
	for(i=0;i<n-1;i++)
	{
		sir[i]=sir[i+1]+rand()%101;
	}
}


//metoda de parcurgere de jos in sus pentru ajungerea la varf (radacina)
void max_heapify(int A[], int i, int dimA, int &nr_asignariHS, int &nr_comparariHS)
{
	int l, r, largest, aux;

	l = 2*i;
	r = 2*i + 1;

	if(l <= dimA && A[l] > A[i])
	{
		largest = l;
	}
	else
	{
		largest = i;
	}

	if( r <= dimA && A[r] > A[largest])
	{
		largest = r;
	}
	nr_comparariHS += 2;

	if(largest != i)
	{
		aux = A[i];
		A[i] = A[largest];
		A[largest] = aux;

		nr_asignariHS += 3;

		max_heapify(A, largest, dimA, nr_asignariHS, nr_comparariHS);
	}
}

//constructie Heap metoda Bottom-up
void buildH_BU(int A[], int dimA, int &nr_asignariHS, int &nr_comparariHS)
{
	int i;

	for(i = dimA/2; i>0; i--)
	{	
		max_heapify(A, i, dimA, nr_asignariHS, nr_comparariHS);
	}
}

//Heap_Sort
void Heap_Sort(int A[],int dimA, int &nr_asignariHS, int &nr_comparariHS)
{
	int c,i;
	
	buildH_BU(A, dimA, nr_asignariHS, nr_comparariHS);
	for(i=dimA; i>1;i--)
	{
		c=A[i];
		A[i]=A[1];
		A[1]=c;
		dimA=dimA-1;
		nr_asignariHS+=3;
		max_heapify(A, 1, dimA,nr_asignariHS,nr_comparariHS);
	}
}

//partitionarea de la metoda Quick-Sort
int partitionare(int A[], int st, int dr, int &nr_asignariQS,int &nr_comparariQS)
{
	int aux, i, j, x;
	i=st;
	j=dr;

	x=A[(st + dr) / 2];
	nr_asignariQS++;
	nr_comparariQS++;
	while (i <= j)
	{
		nr_comparariQS++;
		while (A[i] < x)
		{
			i++;
		}
		nr_comparariQS++;
		while (A[j] > x)
		{
			j--;
		}
		nr_comparariQS++;
		if (i <= j)
		{
			aux=A[i];
			A[i]=A[j];
			A[j]=aux;
			nr_asignariQS+=3;
			i++;
			j--;
		}
	}
	return i;
}

//Metoda Quick_Sort
void quick(int A[], int st, int dr, int &nr_asignariQS,int &nr_comparariQS)
{
	int pozitie = partitionare(A, st, dr, nr_asignariQS, nr_comparariQS);

    if (st < pozitie - 1)
		quick(A, st, pozitie - 1, nr_asignariQS, nr_comparariQS);
	if (pozitie < dr)
		quick(A, pozitie, dr, nr_asignariQS, nr_comparariQS);
}

//afisare sir
void afisare_sir(int A[], int dim)
{
	int i;
	for(i=1;i<=dim;i++)
		printf("%d ",A[i]);
	printf("\n");
}

//afisare Heap
//afisare in preordine
void afiseazaH_pre(int A[], int i, int n, int depth)
{
	int k;
	if(i>n)
	{
		return ;
	}
	for(k=0; k<depth; k++)
	{
		printf("    ");
	}
	
	printf("%d \n", A[i]);
	afiseazaH_pre(A, 2*i, n, depth+1);
	afiseazaH_pre(A, 2*i + 1, n, depth+1);
}

//afisare in inordine
void afiseazaH_in(int A[], int i, int n, int depth)
{
	int k;
	if(i>n)
	{
		return ;
	}
	for(k=0; k<depth; k++)
	{
		printf("    ");
	}
	
	afiseazaH_in(A, 2*i, n, depth+1);
	printf("%d \n", A[i]);
	afiseazaH_in(A, 2*i + 1, n, depth+1);
	
}

//afisare in postordine
void afiseazaH_post(int A[], int i, int n, int depth)
{
	int k;
	if(i>n)
	{
		return ;
	}
	for(k=0; k<depth; k++)
	{
		printf("    ");
	}
	
	afiseazaH_post(A, 2*i, n, depth+1);
	afiseazaH_post(A, 2*i + 1, n, depth+1);
	printf("%d \n", A[i]);
}

int main()
{
	FILE *f,*g,*h;
	int A[MAX_SIZE],A1[MAX_SIZE];
	int dim,aux;
	int st,dr;
	int nr_asignariHS=0;
	int nr_comparariHS=0;
	int nr_asignariQS=0;
	int nr_comparariQS=0;
	int i,j;
	
	srand(time(NULL));
	f=fopen("Heap_Sort_Quick_Sort.csv","w");
	g=fopen("Heap_Sort_Quick_Sort_favorabil.csv","w");
	h=fopen("Heap_Sort_Quick_Sort_defavorabil.csv","w");
	
	//caz mediu statistic
	for(dim=100;dim<=10000;dim+=100)
	{
		creare_sir(A,dim);
		printf("\ndim= %d", dim);
		nr_asignariHS=0;
		nr_comparariHS=0;
		nr_asignariQS=0;
		nr_comparariQS=0;
		for (j=1;j<=5;j++)
		{
			creare_sir(A,dim);
			for(i=0;i<=dim;i++)
			{
				A1[i]=A[i];
			}
			st=1;
			dr=dim;				
			Heap_Sort(A1,dim,nr_asignariHS,nr_comparariHS);
			quick(A,st,dr,nr_asignariQS,nr_comparariQS);
		}

		fprintf(f,"%d, %d, %d, %d, %d, %d, %d \n", dim,nr_comparariHS/5,nr_asignariHS/5,(nr_asignariHS+nr_comparariHS)/5,nr_comparariQS/5,nr_asignariQS/5,(nr_asignariQS+nr_comparariQS)/5);
	}
	
	//caz favorabil
	for(dim=100;dim<=10000;dim+=100)
	{
		creare_sir_crescator(A,dim);
		printf("\ndim= %d", dim);
		nr_asignariQS=0;
		nr_comparariQS=0;
		st=1;
		dr=dim;
		quick(A,st,dr,nr_asignariQS,nr_comparariQS);
		fprintf(g,"%d, %d, %d, %d \n", dim,nr_comparariQS,nr_asignariQS,nr_asignariQS+nr_comparariQS);
	}

	//caz defavorabil
	for(dim=100;dim<=10000;dim+=100)
	{
		creare_sir_descrescator(A,dim);
		printf("\ndim= %d", dim);
		nr_asignariQS=0;
		nr_comparariQS=0;
		st=1;
		dr=dim;
		quick(A,st,dr,nr_asignariQS,nr_comparariQS);
		fprintf(h,"%d, %d, %d, %d \n", dim,nr_comparariQS,nr_asignariQS,nr_asignariQS+nr_comparariQS);
	}

	//----------afisare pe ecran ------------------
	//----------afisare pe ecran pentru Heap_Sort-------------
	/*
	printf("dati dimensiunea = ");
	scanf("%d", &dim);
	creare_sir(A,dim);
	//afisare sir initial
	afisare_sir(A,dim);
	Heap_Sort(A,dim,nr_asignariHS,nr_comparariHS);
	//afiseazaH_pre(A, 1, dim, 0);
	afisare_sir(A,dim);
	

	//---------afisare pe ecran pentru Quick_Sort----------------
	
	printf("dati dimensiunea = ");
	scanf("%d", &dim);
	creare_sir(A,dim);
	//afisare sir initial
	afisare_sir(A,dim);
	st=1;
	dr=dim;
	quick(A,st,dr,nr_asignariQS, nr_comparariQS);
	afisare_sir(A,dim);
	//afiseazaH_pre(A, 1, dim, 0);
	*/

	getch();

}