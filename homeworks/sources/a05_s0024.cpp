#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int ok, T[10007], N, accese, efortMaximGasite, efortMaximNegasite;
float alfa, efortMediuGasite, efortMediuNegasite;


//functia de dispersie
int h(int element, int i) {
	int aux;
	//constantele = 2;
	aux = element % N;
	aux = aux + i/2 + i*i/2;		//pagina 272
	aux = aux % N;
	return aux;
}

//functie de inserare in tabelul de dispersie
int inserare(int element) {
	int i,j;
	i=0;
	do
	{
		j = h(element,i);
		if (T[j]==NULL)
		{
			T[j] = element;
			return j;
		}
		else i++;
	}
	while (i<=N);
	return -1;
}


//functie de cautare in tabelul de dispersie
//k reprezinta valoarea de cautat
int cautare(int element) {
	int i,j;
	accese=0;
	i=0;
	do
	{
		j=h(element,i);
		accese++;
		if (T[j]==element)
		{
			return j;
		}
		i++;
	}
	while ((i<=N) && (T[j]!=NULL));
	return -1;
}


void functie(float alfa, int n) {

	int t,i,m,aux,x;
	float efortMediuGasite_temp,efortMediuNegasite_temp;
	
	//initializari
	efortMaximNegasite=-1;
	efortMaximGasite=-1;
	efortMediuNegasite=0;
	efortMediuGasite=0;
	
	//N=numarul de elemente ale tabelului de dispersie
	N=10007;
	int nn=n;
	
	for(t=1;t<=5;t++)
	{
		nn=n;
		//initializare tabela de dispersie cu N locatii
		for (i=0;i<N;i++)
		{
			T[i]=NULL;
		}
		//inseram n elemete in tabel astfel incat sa fie plin in proportie de 80%
		while (nn>0)
		{
			x=rand();
			aux=inserare(x);
			if (aux!=-1)
			{
				nn--;
			}
		}
		
		//cautare elemente ce nu sunt in tabela
		m=2500;
		efortMediuNegasite_temp=0;
		while (m>0)
		{
			x=33000+rand();
			aux=cautare(x);
			if (aux==-1)
			{
				m--;
				//calcul efort
				efortMediuNegasite_temp = efortMediuNegasite_temp + accese;
				if (accese>efortMaximNegasite) {
					
					efortMaximNegasite=accese;
				}
			}
		}
		efortMediuNegasite_temp = efortMediuNegasite_temp/2500;
		efortMediuNegasite = efortMediuNegasite + efortMediuNegasite_temp;
	
		//cautare elemente ce sunt in tabela
		m=2500;
		efortMediuGasite_temp=0;
		while (m>0)
		{
			x=rand()%10007;
			aux=cautare(T[x]);
			if (T[x] != NULL && aux!=-1)
			{
				m--;
				//calcul efort
				efortMediuGasite_temp=efortMediuGasite_temp+accese;
				if (accese>efortMaximGasite) {
					
					efortMaximGasite=accese;
				}
			}
		}
		efortMediuGasite_temp=efortMediuGasite_temp/2500;
		efortMediuGasite=efortMediuGasite+efortMediuGasite_temp;
	}
	efortMediuGasite=efortMediuGasite/5;
	efortMediuNegasite=efortMediuNegasite/5;
}

void main()
{
	FILE *f;
	N=10007;
	f=fopen("date.csv","w");
	int n,x,k,i,aux,m,t;
	float efortMediuGasite_temp,efortMediuNegasite_temp;
	eticheta:
	//printare meniu
	printf("Selectati: Date aleatoare sau date introduse de la tastatura \n1=date aleatoare \n2=date introduse\n");
	scanf("%d",&ok);

	if (ok==1) {//=>random 

		fprintf(f,"%s, %s, %s, %s, %s\n","factorul de umplere","Efort mediu cautare elemente gasite","Efort maxim gasite","Efort mediu cautare elemente negasite","Efort maxim negasite");

		//pt alfa=0.80
		alfa=0.80;
		n=(80*N)/100;
		functie(alfa,n);
		fprintf(f,"%.2f, %.2f, %d, %.2f, %d\n",alfa,efortMediuGasite,efortMaximGasite,efortMediuNegasite,efortMaximNegasite);
		
		//pt alfa=0.85
		N=10007;
		alfa=0.85;
		n=85*N/100;
		functie(alfa,n);
		fprintf(f,"%.2f, %.2f, %d, %.2f, %d\n",alfa,efortMediuGasite,efortMaximGasite,efortMediuNegasite,efortMaximNegasite);
		
		//pt alfa=0.90
		alfa=0.90;
		N=10007;
		n=90*N/100;
		functie(alfa,n);
		fprintf(f,"%.2f, %.2f, %d, %.2f, %d\n",alfa,efortMediuGasite,efortMaximGasite,efortMediuNegasite,efortMaximNegasite);
		
		//pt alfa=0.95
		alfa=0.95;
		N=10007;
		n=95*N/100;
		functie(alfa,n);
		fprintf(f,"%.2f, %.2f, %d, %.2f, %d\n",alfa,efortMediuGasite,efortMaximGasite,efortMediuNegasite,efortMaximNegasite);
		
		//pt alfa=0.99
		alfa=0.99;
		N=10007;
		n=99*N/100;
		functie(alfa,n);
		fprintf(f,"%.2f, %.2f, %d, %.2f, %d\n",alfa,efortMediuGasite,efortMaximGasite,efortMediuNegasite,efortMaximNegasite);
		
		fclose(f);
	}

	//caz particular
	else {
		printf("dati numarul de locatii din tabela de dispersie: N=\n");
		scanf("%d",&N);
		//initializare tabela de dispersie cu N locatii
		for (i=0;i<N;i++)
		{
			T[i]=NULL;
		}
		
		n=N;
		i=2;
		while (n>0)
		{
			x=i;
			aux=inserare(x);
			if (aux!=-1)
			{
				n--;
			}
			i+=3;
		}
		
		//printare tabel
		for (i=0;i<N;i++)
		{
			printf("%d  ",T[i]);
		}

		//verificare cautare
		printf("\nIntroduceti valoarea cautata: ");
		scanf("%d",&x);
		aux=cautare(x);
		if (aux==-1)
		{
			printf("\n                Valoarea nu este in tabela\n");
		}
		else
		{
			printf("\n                Valoarea este in tabela\n");
		}
	}
	printf("\n");
	goto eticheta;
	printf("done`");
	getch();
	return;
}
/*Observam ca cu cat factorul de umplere crestere, cu atat efortul de cautare creste*/


