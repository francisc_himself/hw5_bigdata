//Time complexity of Josephus algorithm is O(nlgn)

#include<iostream>
#include<conio.h>
#include<stdlib.h>
#include<stdio.h>
#include"Profiler.h"

Profiler profiler("demo");

struct node
{
    int value, size;
    node *left, *right, *parent;
};
node *root;

int length;
int a[10001],cnt=0;

void prettyPrint(node *p,int level)
{
	profiler.countOperation("comparisons",length,1);
	if (p!=NULL)
	{
		printf("\n");
		prettyPrint(p->left,level+1);
		for(int i=0;i<=level;i++) 
			printf("          ");
		printf("%d\n", p->value);
		prettyPrint(p->right,level+1);
	}
}

//taken from Cormen, page 341
node* osSelect(node *x, int i)
{
    int r=1;
	profiler.countOperation("comparisons",length);
    if (x->left != NULL)					
        r = x->left->size+1;	
    if (r == i)
        return x;
    else
        if (i < r)
            return osSelect(x->left,i);
        else
            return osSelect(x->right,i-r);
}

node* newNode(int x, int dimension, node *lft, node *rgt)
{
    node *q;
    q= new node;
	profiler.countOperation("assignments",length,3);
    q->value=x;					
    q->left=lft;				
    q->right=rgt;				
    q->size=dimension;			

	profiler.countOperation("comparisons",length,1);
    if (lft!=NULL)
	{
		profiler.countOperation("assignments",length);
        lft->parent = q;
	}
	profiler.countOperation("comparisons",length,1);
    if (rgt!=NULL)
	{
		profiler.countOperation("assignments",length);
        rgt->parent = q;
	}
	profiler.countOperation("assignments",length);
    q->parent=NULL;
    return q;
}

node* buildTree(int lft, int rgt)
{
    node *lfts,*rgts;
    if (lft > rgt)
        return NULL;
    if (lft == rgt)
        return newNode(lft, 1, NULL, NULL);
    else
    {
        int mid =(lft + rgt)/2;
		profiler.countOperation("assignments",length,2);
        lfts = buildTree(lft, mid-1);
        rgts  = buildTree(mid+1, rgt);
        return newNode(mid, rgt-lft+1,lfts, rgts);
    }
}

//taken from Course #6, slide 9
node* minimNode(node* x)
{
	int kek = 0;
    while(x->left!=NULL)
	{
		profiler.countOperation("assignments",length);
		profiler.countOperation("comparisons",length);
        x = x->left;
		kek=1;
	}
	if(kek);
	else profiler.countOperation("comparisons",length);
    return x;					
}

//taken from Course #6, slide 9
node* successor(node* x)
{
    node *y;

	profiler.countOperation("comparisons",length);
    if(x->right!=NULL)		
        return minimNode(x->right);
    y = x->parent;			
	int kek=0;
    while (y != NULL && x == y->right)		
    {			
		profiler.countOperation("comparisons",length,2);	
		profiler.countOperation("assignments",length,2);
		kek=1;
        x = y;								
        y = y->parent;						
    }
	if(kek);
	else profiler.countOperation("comparisons",length,2);
    return y;								
}

//course #6, slide 5
node* del(node* z)
{
    node *x, *y;
	profiler.countOperation("comparisons",length,2);
    if ((z->left == NULL) || z->right == NULL)
	{
		profiler.countOperation("assignments",length);
        y = z;
	}
    else
	{
		profiler.countOperation("assignments",length);
        y = successor(z);
	}
	profiler.countOperation("comparisons",length);
    if (y->left != NULL)
	{
		profiler.countOperation("assignments",length);
        x = y->left;
	}
    else          
	{
		profiler.countOperation("assignments",length);
        x = y->right;
	}
	profiler.countOperation("comparisons",length);
    if (x != NULL)
	{
		profiler.countOperation("assignments",length);
        x->parent = y->parent;
	}
	profiler.countOperation("comparisons",length);
    if (y->parent == NULL)
	{
		profiler.countOperation("assignments",length);
        root = x;
	}
    else 
		{
			profiler.countOperation("comparisons",length);
			if (y == y->parent->left)
			{
				profiler.countOperation("assignments",length);
			 y->parent->left = x;
			}
		    else
			{
				profiler.countOperation("assignments",length);
			    y->parent->right = x;
			}
		}
	profiler.countOperation("comparisons",length);
    if (y != z)
	{
		profiler.countOperation("assignments",length);
		z->value = y->value;
	}
    return y;
}

void display(int *list,int cnt){
    printf("\nDeleted numbers:  ");
    for ( int i = 0 ; i < cnt ;i++)
        printf("%d ",list[i]);
}

void josephus(int n, int m)
{
    node *y,*z;
    int first = n+1, mid = m;

	profiler.countOperation("assignments",length);
    root = buildTree(1, n);				
    if(length < 100)
	{
		printf("\nThe tree: ");
		prettyPrint(root,0);
	}
    for (int i = 1; i < first; i++)	
    {
		profiler.countOperation("assignments",length);
        y = osSelect(root, m);	
		profiler.countOperation("assignments",length);
        a[cnt] = y->value;
        cnt++;
		if(length < 100)
			display(a,cnt);
		profiler.countOperation("assignments",length);
        z = del(y);	
		if(length < 100)
		{
			if(root != NULL)printf("\nThe tree: \n");
			prettyPrint(root,0);
		}
		int kek=0;
        while (z != NULL)
	    {
			 kek=1;
			 profiler.countOperation("comparisons",length);
			 z->size--;	
			 profiler.countOperation("assignments",length);
		     z=z->parent;
        }    
		if(kek);
		else profiler.countOperation("comparisons",length);
        delete(z);					
        n--;						
        if (n > 0)				
            m = (m - 1 + mid) % n;
        if (m == 0)
            m = n;
    }
}

void averageCase()
{
	for(length=100; length<= 10000; length+=100)
	{
		josephus(length,length/2);
		printf("\n length = %d",length);
	}
	profiler.addSeries("Josephus","assignments","comparisons");
	profiler.showReport();
}

int main()
{
	length = 7;
    josephus(7,3);
	getche();
	//averageCase();
}
