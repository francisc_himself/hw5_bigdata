
#include "stdafx.h"
#include<iostream>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#include<fstream>
#define MAX 10000


int B[MAX];
int n=10;
int dimensiune_heap;
int nr_comp[2];
int nr_atrib[2];
int X[MAX],Y[MAX];

int parinte(int i)
{
	return (i/2);
}

int stanga(int i)
{
	return(2*i);
}

int dreapta(int i)
{
	return(2*i+1);
}

void reconstituie_heap(int A[], int i)
{
	int aux,maxim,l,r;
	l=stanga(i);
	r=dreapta(i);
	nr_comp[1]=nr_comp[1]+1;
	if((l<=dimensiune_heap)&&(A[l]>A[i]))
		maxim=l;
	else
		maxim=i;
	nr_comp[1]=nr_comp[1]+1;
	if((r<=dimensiune_heap)&&(A[r]>A[maxim]))
		maxim=r;
	if(maxim!=i)
	{
		nr_atrib[1]=nr_atrib[1]+3;
		aux=A[i];
		A[i]=A[maxim];
		A[maxim]=aux;
	
		reconstituie_heap(A,maxim);
	}
	
}


void construieste_heap(int A[])
{
	dimensiune_heap=n;
	for(int j=n/2;j>=1;j--)
		reconstituie_heap(A,j);

}

void heapsort(int A[])
{
	int aux;
	construieste_heap(A);
	for(int i=n;i>=2;i--)
	 {
		 aux=A[1];
		 A[1]=A[i];
		 A[i]=aux;
		 nr_atrib[1]=nr_atrib[1]+3;
		 dimensiune_heap=dimensiune_heap-1;
		 reconstituie_heap(A,1);
	 } 
	

}

/*int partitie(int A[], int st, int dr)
{
	int x,aux,i,j;
	x=A[st];
	i=st-1;
	j=dr+1;
	while (true)
		   do
			{
			j=j-1;
			nr_comp[2]=nr_comp[2]+1;
			}
		   while (A[j]<=x);
		   do
		   {
			i=i+1;
			nr_comp[2]=nr_comp[2]+1;
		   }
		   while (A[i]>=x);
		   if(i<j)
		   {
			   aux=A[i];
			   A[i]=A[j];
			   A[j]=aux;
			   nr_atrib[2]=nr_atrib[2]+3;
		   }
		   else
			   return j;

}


void quickSort(int A[],int st, int dr)
{
	int m;
	if(st<dr)
	{
		m=partitie(A,st,dr);
		quickSort(A,st,m);
		quickSort(A,m+1,dr);

	}
}*/
/*
int partitie(int A[], int st, int dr)
{
	int x,aux,i,j;
	x=A[st];
	i=st-1;
	for(j=st;j<dr;j++)
	 if(A[j]<=x)
	 {
	  nr_comp[2]=nr_comp[2]+1;
	  i=i+1;
	  aux=A[i];
		A[i]=A[j];
		A[j]=aux;
		nr_atrib[2]=nr_atrib[2]+3;
	 }
	 aux=A[i+1];
	 A[i+1]=A[dr];
	 A[dr]=aux;
	 nr_atrib[2]=nr_atrib[2]+3;
	
	return i+1;

}


void quickSort(int A[],int st, int dr)
{
	int m;
	if(st<dr)
	{
		m=partitie(A,st,dr);
		quickSort(A,st,m-1);
		quickSort(A,m+1,dr);

	}
}
*/


void quickSort(int A[], int left, int right) {
      int i = left, j = right;
      int aux;
      int pivot = A[(left + right) / 2];
 
      //partitionare
      while (i <= j) 
	  {
			
            while (A[i] < pivot)
			{
				  nr_comp[2]=nr_comp[2]+1;
                  i++;
			}
			nr_comp[2]=nr_comp[2]+1;

			      
            while (A[j] > pivot)
			{
				  nr_comp[2]=nr_comp[2]+1;
                  j--;
			}
			nr_comp[2]=nr_comp[2]+1;
		
            if (i <= j) 
			{
                  aux = A[i];
                  A[i] = A[j];
                  A[j] = aux;
				  nr_atrib[2]=nr_atrib[2]+3;
                  i++;
                  j--;
            }
      };
 
      //recursiv 
      if (left < j)
            quickSort(A, left, j);
      if (i < right)
            quickSort(A, i, right);
}

  

void copiere_sir(int destinatie[], int n, int sursa[])
{
	int j;
	for (j = 1; j <= n; j++)
		destinatie[j] = sursa[j];
}




void main()
{
	

	nr_atrib[1]=0;
	nr_comp[1]=0;
	nr_atrib[2]=0;
	nr_comp[2]=0;



	srand (time(NULL));
	for(int i=1;i<=10;i++)
		B[i]=rand()%10000;
	
	quickSort(B,1,n);
	for(int i=1;i<=n;i++)
		printf("%d ",B[i]);
	printf("\n %d",nr_atrib[2]+nr_comp[2]);
	printf("\n");
	n=10;
	B[1]=4;
	B[2]=1;
	B[3]=3;
	B[4]=2;
	B[5]=16;
	B[6]=9;
	B[7]=10;
	B[8]=14;
	B[9]=8;
	B[10]=7;
	for(int j=1;j<=10;j++)
		printf("%d ",B[j]);	
	printf("\n");
	
	heapsort(B);

	for(int j=1;j<=10;j++)
		printf("%d ",B[j]);
	printf("\n %d",nr_atrib[1]+nr_comp[1]);


	FILE *f;
	f = fopen("quick.csv","w");
	
	srand (time(NULL));

	fprintf(f, "n,nr_AtribSiComp_QuickCRESCATOR,nr_AtribSiComp_QuickDESCRESCATOR,nr_AtribSiComp_QUICK,nr_AtribSiComp_HEAP\n");

	for (n = 100; n <= 10000; n = n + 100)
	 {
		nr_atrib[1] = 0;
		nr_atrib[2] = 0;
		nr_comp[1] = 0;
		nr_comp[2] = 0;

		fprintf(f, "%d,", n);

		//valori ordonate crescator => caz favorabil
		for (int i = 1 ; i <= n; i++)
			X[i] = i;
	
		//quickSort in caz favorabil
		quickSort(X, 1, n);
		fprintf(f, "%d,", nr_atrib[2]+nr_comp[2]);

		//valori ordonate descrescator => caz defavorabil
		for (int i = 1; i <= n; i++)
			X[i] = n - i + 1;
		

		nr_atrib[2] = 0;
		nr_comp[2] = 0;

		//quickSort in caz defavoravil
		quickSort(X, 1, n);
		fprintf(f, "%d,", nr_atrib[2]+nr_comp[2]);

		nr_atrib[2] = 0;
		nr_comp[2] = 0;

		for (int j = 1; j <= 5; j++)
		{
			for (int k = 1; k <= n; k++)
			{
				X[k] = rand() % 10000;
				Y[k] = X[k];
			}
			
			// quickSort in caz mediu
			quickSort(X,1,n);
			copiere_sir(X, n, Y);

			// heapsort in caz mediu
			heapsort(X);
			copiere_sir(X, n, Y);

		}

	fprintf(f, "%d,%d\n", (nr_atrib[2]+nr_comp[2]) / 5, (nr_atrib[1]+nr_comp[1]) / 5);

	}
	fclose(f);
	printf("\ndatele au fost scrise\n");

	

	getch();
}

