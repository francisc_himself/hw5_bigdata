/*
It is assumed that the two arrays are ordered and have the same length.
*/

#include <stdio.h>
#include <stdlib.h>

int Median(int a[], int b[], int n)
{
    int median=0;

    if(a[n-1]<=b[0])
    {
        median = a[n-1];
    }
    else if(b[n-1]<=a[0])
    {
        median = b[n-1];
    }
    else if(a[n/2]<b[n/2])
    {
        return Median(a+n/2, b, n-n/2);
    }
    else
    {
        return Median(a, b+n/2, n-n/2);
    }
    return median;
}

int main()
{
    int a[] = {2, 8, 9, 10};
    int b[] = {13, 15, 17, 19};
    int sizeA = 0, sizeB = 0;

    sizeA = sizeof(a)/sizeof(a[0]);
    printf("Median: %d", Median(a, b, sizeA));
}
