#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<cstdlib>

#define MAX 999999

typedef struct nod {
	int key;
	int culoare;//0:alb,1:gri,2:negru
	int dist;
	struct nod *parinte;
	struct nod *urm;
}NOD;

NOD *prim[MAX], *ultim[MAX];
NOD *cap=NULL, *coada=NULL;
int n, nrOp=0, unu=1, nr; 
int comp[10];//primul nod dintr`o componeta
int vect[10];//sa nu afisez un nod de mai multe ori

void init(int n) {
	for(int i=0;i<n;i++)
	{
		prim[i]=NULL;
		ultim[i]=NULL;
	}
}

void adaugaNod(int x,int y) {
	NOD *q=(NOD *)malloc(sizeof(NOD));
	q->key=y;
	q->parinte=NULL;
	if(y!=unu)
	{
		q->culoare=0;
		q->dist=MAX;
	}
	else
	{
		q->culoare=1;
		q->dist=0;
	}
	if(prim[x]==NULL)
	{
		NOD *p=(NOD *)malloc(sizeof(NOD));
		p->key=x;
		p->parinte=NULL;
		if(x!=unu)
		{
			p->culoare=0;
			p->dist=MAX;
		}
		else
		{
			p->culoare=1;
			p->dist=0;
		}
		prim[x]=p;
		ultim[x]=prim[x];
		ultim[x]->urm=NULL;
	}
	ultim[x]->urm=q;
	ultim[x]=ultim[x]->urm;
	ultim[x]->urm=NULL;
}

void enq(NOD *v) {
	NOD *p=(NOD *)malloc(sizeof(NOD));
	p->key=v->key;
	p->culoare=v->culoare;
	p->dist=v->dist;
	p->parinte=v->parinte;
	if(cap==NULL)
		cap=coada=p;
	else
	{
		coada->urm=p;
		coada=coada->urm;
	}
	coada->urm=NULL;
}

NOD *deq() {
	NOD *p=cap;
	if(cap!=NULL)
		cap=cap->urm;
	return p;
}

void bfs(int iterator) {
	enq(prim[iterator]);
	nrOp++;
	while(cap!=NULL&&prim[cap->key]!=NULL)
	{
		NOD *u=deq();
		nrOp++;
		for(NOD *v=prim[u->key]->urm;v!=NULL;v=v->urm)
		{
			if(v->culoare==0)
			{
				v->culoare=1;
				v->dist=u->dist+1;
				v->parinte=u;
				enq(v);
				nrOp++;
			}
		}
		prim[u->key]->culoare=2;
	}
}

void afisare(int i) {
	for(NOD *p=prim[i];p!=NULL;p=p->urm)
	{
		printf("%d ",p->key);
		if(p->urm != NULL) {
			printf("-> ");
		}
		else {
		}
	}
	printf("\n");
}

void afisare_arb(NOD *p) {
	int j=p->key;
	if(p!=NULL)
	{
		while(p!=NULL)
		{
			if(vect[p->key]==0)
			{
				for(int i=0;i<=p->dist;i++)
					printf("  ");
				printf("%d\n",p->key);
				vect[p->key]=1;
			}
			p=p->urm;
		}
		if(prim[j]!=NULL)
			afisare_arb(prim[j]->urm);
	}
}

void generare() {
	FILE *f=fopen("bfs.csv","w");
	FILE *g=fopen("bfs1.csv","w");
	fprintf(f,"noduri,muchii,operatii\n");
	n=100;
	srand(time(NULL));
	//generare pt n constant
	nrOp=0;
	for(int m=1000;m<=5000;m+=100)
	{
		for(int i=1;i<=m;i++)
		{

			int x=1+rand()%n;
			int y=1+rand()%n;
			if(x==y)
				y=1+rand()%n;
			adaugaNod(x,y);
		}
		bfs(1);
		for(int i=2;i<=n;i++)
		{ 
			if(prim[i]!=NULL)
			{
				if(prim[i]->dist==MAX)
				{
					cap=NULL;
					prim[i]->culoare=1;
					prim[i]->dist=0;
					prim[i]->parinte=NULL;
					bfs(i);
				}
			}
		}
		fprintf(f,"%d,%d,%d\n",n,m,nrOp);
	}
	//generare pt m constant
	int m=9000;
	nrOp=0;
	for(int n=100;n<=200;n+=10)
	{
		for(int i=1;i<=m;i++)
		{

			int x=1+rand()%n;
			int y=1+rand()%n;
			if(x==y)
				y=1+rand()%n;
			adaugaNod(x,y);
		}
		bfs(1);
		for(int i=2;i<=n;i++)
		{ 
			if(prim[i]!=NULL)
			{
				if(prim[i]->dist==MAX)
				{
					cap=NULL;
					prim[i]->culoare=1;
					prim[i]->dist=0;
					prim[i]->parinte=NULL;
					bfs(i);
				}
			}
		}
		fprintf(g,"%d,%d,%d\n",n,m,nrOp);
	}
}

int main() {
	int m,ok=0;
	int flag = 0;
eticheta:
	printf("Introduceti 1 pentru demo si 2 pentru generare random: ");
	scanf("%d", &flag);
	if(flag == 1) {
		n=6;
		int x[]={1,1,2,3,4,5};
		int y[]={2,3,4,2,5,6};
		m=6;
		for(int i=0;i<m;i++)
			adaugaNod(x[i],y[i]);

		bfs(1);
		comp[1]=1;
		nr=1;
		vect[1]=0;
		for(int i=2;i<=n;i++)
		{ 
			vect[i]=0;
			if(prim[i]!=NULL)
			{
				if(prim[i]->dist==MAX)
				{
					cap=NULL;
					prim[i]->culoare=1;
					prim[i]->dist=0;
					prim[i]->parinte=NULL;
					bfs(i);
					nr++;
					comp[nr]=i;
				}
			}
		}
		printf("%d\n",nr);
		for(int i=1;i<=n;i++)
			afisare(i);
		printf("\n");
		for(int i=1;i<=nr;i++)
		{
			afisare_arb(prim[comp[i]]);
			printf("\n");
		}
	} else if(flag == 2) {
		generare();
		printf("done`");
	} 
	else {
		goto eticheta;
	}
	getch();
	return 0;
}