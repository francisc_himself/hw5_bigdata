
/*
 * Heap.cpp
 *
 *  Created on: Mar 19, 2013
 *      Author: ***ANONIM***
 *
 *	Description:
 *	This program compares the two mehods of building a heap: Buttom-up and Top-down tehnique.
 *  For each method we count the number of operations that are performed.
 *	We perform computations for two different cases: Average Case and Best Case
 *  Result analysis:
 *  For Average Case:
 *			Buttom up tehnique is better because it performs approximatively half operations than Top-down tehnique
 *	For Worst Case:
 *          Buttom up tehnique behaves evan better!
 *  Conclusions:
 *  Buttom-up tehnique is more efficient than Top-down tehnique because it runs in linear time ( running time is O(n) ),
 *  while running time for Top-Down method is O(n log n).
 */

#include "limits.h"
#include "stdafx.h"
#include "Profiler.h"
Profiler p("heap");

int A[10001] = {14, 20, 7, 8, 3, 5, 4, 1, 8, 2, 16, 9, 10, 14, 8};
int B[10001] = {14, 20, 7, 8, 3, 5, 4, 1, 8, 2, 16, 9, 10, 14, 8};
int n;

int parent(int i);
int left(int i);
int right(int i);
void maxHeapify(int A[], int i);
void printHeap(int A[]);
void buildMaxHeapButtomUp(int A[]);
void heapsort(int A[]);
void heapIncreaseKey(int A[], int i, int key);
void maxHeapInsert(int A[], int key);
void buildMaxHeapTopDown(int A[], int n);
void averageCase();
void worstCase();

int main(){
	
	printf("Initial array\n");
	printHeap(A);
	printf("\nButtom-up \n");
	buildMaxHeapButtomUp(A);
	printHeap(A);
	printf("\nTop-Down \n");
	buildMaxHeapTopDown(B, B[0]);
	printHeap(B);
	
	//averageCase();
	//worstCase();
	system("pause");
	return 0;
}

//returns parent of i
int parent(int i){
	return i/2;
}

//returns left child of i
int left(int i){
	return 2*i;
}

//returns right child of i
int right(int i){
	return 2*i + 1;
}

//selects position in array to add an element
void maxHeapify(int A[], int i){
	int l = left(i);
	int r = right(i);
	int max;

	//A[0] - length of array
	//selects the child with max value
	if ( (l <= A[0]) && (A[l] > A[i]) ){
		max = l;
	} else {
		max = i;
	}

	p.countOperation("Buttom Up Operations", n-1, 2);
	if ( (r <= A[0]) && (A[r] > A[max]) ){
		max = r;
	}

	if (max != i){
		// if child > parent => swap
		p.countOperation("Buttom Up Operations", n-1, 3);
		int aux = A[i];
		A[i] = A[max];
		A[max] = aux;
		maxHeapify(A, max);
	}
}

//displays heap
void printHeap(int A[]){
	int max = 1;
	int i = 1, j = 0;
	while (i <= A[0]){
		j = 0;
		while ( (j < max) && (i <= A[0])){
			printf("%i ", A[i]);
			i++;
			j++;
		}
		max *= 2;
		printf("\n");
	}
}

// creates heap using bottum up tehnique; starts with last parent
// using heapify will maintain the heap property: parent >= child
void buildMaxHeapButtomUp(int A[]){
	for (int i = A[0] / 2; i>0; i--){
		maxHeapify(A, i);
	}
}

//inserts element key in the heap
void heapIncreaseKey(int A[], int i, int key){
	// i - heap size
	p.countOperation("Top Down Operations", n-1, 1);
	// check if key > heap size
	if (key >= A[i]){
		A[i] = key;
		p.countOperation("Top Down Operations", n-1, 1);
		while ( (i>1) && (A[parent(i)] < A[i]) ){
			//swap parent with child if child > parent
			int aux = A[i];
			A[i] = A[parent(i)];
			A[parent(i)] = aux;
			i = parent(i);
			p.countOperation("Top Down Operations", n-1, 4);
		}
		p.countOperation("Top Down Operations", n-1, 1);
	}
}

void maxHeapInsert(int A[], int key){
	A[0]++; //increase heap size
	A[A[0]] = INT_MIN; // last element initialised with very small value
	p.countOperation("Top Down Operations", n-1, 1);
	heapIncreaseKey(A, A[0], key);
}

//creates heap using top-down tehnique
void buildMaxHeapTopDown(int A[], int n){
	A[0] = 1; // heap size = 1
	//insert element which is outside the heap
	for (int i = 2; i<=n; i++ ){
		//insert element i
		maxHeapInsert(A, A[i]);
	}
}

// average case test for both two algorithms
// elements in array are random
void averageCase(){
	for(n=101; n<10002; n+=100){
		for(int i=0; i<5; i++){
			// perform counting for 5 different arrays
			FillRandomArray(A, n, 0, 100, false, 0);
			A[0] = n - 1;
			for (int j = 0; j<n+2; j++){
				B[j] = A[j];
			}
			buildMaxHeapButtomUp(A);
			buildMaxHeapTopDown(B, B[0]);
		}
	}

	p.createGroup("Average Case Total Operations", "Top Down Operations", "Buttom Up Operations");

	p.showReport();
}

//worst case test for both two algorithms
//elements in array are in increasing order
void worstCase(){
	for(n=101; n<10002; n+=100){
		A[0] = n-1; B[0] = n-1;
		for(int i=1; i<n; i++){
			A[i] = i;
			B[i] = i;
		}
			buildMaxHeapButtomUp(A);
			buildMaxHeapTopDown(B, B[0]);
		}

	p.createGroup("Worst Case Total Operations", "Top Down Operations", "Buttom Up Operations");

	p.showReport();
}
