#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

//Definim structura unui nod a arborelui
typedef struct NodItem
{
    int valoare;
    int dimensiune;
    struct NodItem *parinte;
    struct NodItem *stanga;
    struct NodItem *dreapta;

}NodItem;
//Contori
long nr_atribuiri,nr_comparatii;
NodItem *radacina;

//Declaram functii
/////////////////////////////////////////////////////////////////////////////////////////////
/**
    Returneaza Nodul cel mai din stanga (adica minimul)
    @param root= o structura NodItem
    @return = o structura NodItem care reprezinta minimul nod
*/
NodItem* getLeftMostChild(NodItem *root)
{
    while(root->stanga!=NULL) //Pana cand exista in stanga root-ului nod, patrundem in stanga
    {
        root=root->stanga; //root-ul devine elementul din stanga
        nr_atribuiri++;
    }
    return root; //returnam root-ul care va fi elementul cel mai din stanga, adica minimul
}
//////////////////////////////////////////////////////////////////////////////////////////////
/**
    Returneaza Succesorul nodului dat ca parametru.Adica acceseaza nodul din dreapta dupa care patrunde la nodul aflat la cel mai
    in stanga.
    Metoda folosita la stergerea unui nod cu 2 fii.
    @param root = noodul al carui succesor dorim sa aflam
    @return = nodul succesor
*/
NodItem* getInOrderSuccesorNode(NodItem *root)
{
    return getLeftMostChild(root->dreapta);
}
//////////////////////////////////////////////////////////////////////////////////////////////
/**
    Metoda folosita pentru scaderea dimensiunii unui lane. Se da ca parametru un nod de start.
    Urcam de la nod pana la radacina si tot scadem dimensiunea fiecarui nod.
    @param start = punctul de plecare spre radacina.
*/
void decreaseLaneDimensionStartingWithNode(NodItem *start)
{
   while(start!=NULL)
   {
       start->dimensiune=start->dimensiune-1;
       start=start->parinte;
       nr_atribuiri++;
       nr_atribuiri++;
   }
}
//////////////////////////////////////////////////////////////////////////////////////////////////

/**
    Metoda de stergere a unui nod din arbore.
    @param root = radacina arborelui
    @param node = nodul care vrem sa il stergem
*/
void deleteNodeFromTree(NodItem *root,NodItem *node)
{
    nr_comparatii++;
    if(node->dreapta==NULL && node->stanga==NULL) //Daca e nod fara copii, il stergem doar
    {
  ///      decreaseLaneDimensionStartingWithNode(node);
        NodItem *p;
        nr_atribuiri++;
        p=node->parinte;    //Accesam parintele nodului care urmeaza sa fie sters
        nr_comparatii++;
        if (p->stanga==node) //Daca nodul care va fi sters se afla in stanga parintelui
        {
            free(node);
            p->stanga=NULL;     //Pointerul spre elementul din stanga devine NULL
       /*     if(p->dreapta==NULL) //Daca in dreapta nu avea fii, inseamna ca va deveni frunza, scadem dimensiunea pana la rad
            {
                decreaseLaneDimensionStartingWithNode(p);
            } */
        }
        nr_comparatii++;
        if (p->dreapta==node) //Daca nodul care va fi sters se afla in dreapta parintelui
        {
            free(node);        //Stergem nodul din dreapta parintelui
            p->dreapta=NULL;   //Pointerul spre elementul drept devine NULL

       /*     if (p->stanga==NULL) //Daca in stanga nu avea fii, inseamna ca va deveni frunza, scadem dimensiunea pana la radacina
            {
                decreaseLaneDimensionStartingWithNode(p);
            } */
        }
    }
    else //Intram in else. Inseamna ca nodul de sters avea cel ptin un fiu
    {
        nr_comparatii++;
        if (node->stanga==NULL && node->dreapta!=NULL) //Nodul de sters are un fiu in dreapta
        {
            nr_comparatii++;
            if(node->parinte==NULL)   // if is root
            {
                printf("rot");
                nr_atribuiri++;
                radacina=radacina->dreapta;
                free(node);
                radacina->parinte=NULL;

            }
            else
            {
                nr_comparatii++;
                if (node->parinte->stanga==node)
                {
                    nr_atribuiri=nr_atribuiri+2;
    ///                decreaseLaneDimensionStartingWithNode(node); //Scadem dimensiundea incepand cu nod-ul urcat in arbore
                    node->parinte->stanga=node->dreapta;
                    node->dreapta->parinte=node->parinte; ///
                    free(node);
                }
                nr_comparatii++;
                if (node->parinte->dreapta==node)
                {
                    nr_atribuiri=nr_atribuiri+2;
   ///                 decreaseLaneDimensionStartingWithNode(node); //Scadem dimensiundea incepand cu nod-ul urcat in arbore
                    node->parinte->dreapta=node->dreapta; //Inlocuim nodul de sters cu subarborele drept
                    node->dreapta->parinte=node->parinte; ///
                    free(node);
                }
            }
        }
        nr_comparatii++;
        if (node->dreapta==NULL && node->stanga!=NULL) //Nodul de sters are un fiu in stanga
        {
            nr_comparatii++;
            if(node->parinte==NULL)   // if is root
            {
                nr_atribuiri++;
                radacina=radacina->stanga;
                free(node);
                radacina->parinte=NULL;
            }
            else
            {
                nr_comparatii++;
                if(node->parinte->stanga==node)
                {
                    nr_atribuiri=nr_atribuiri+2;
     ///               decreaseLaneDimensionStartingWithNode(node); //Scadem dimensiundea incepand cu nod-ul urcat in arbore
                    node->parinte->stanga=node->stanga;  //Inlocuim nodul de sters cu subarbore stang
                    node->stanga->parinte=node->parinte;
                    free(node);
                }
                nr_comparatii++;
                if (node->parinte->dreapta==node)
                {
                    nr_atribuiri=nr_atribuiri+2;
  ///                  decreaseLaneDimensionStartingWithNode(node); //Scadem dimensiundea incepand cu nod-ul urcat in arbore
                    node->parinte->dreapta=node->stanga;
                    node->stanga->parinte=node->parinte;
                    free(node);
                }
            }
        }
        nr_comparatii++;
        if (node->stanga!=NULL && node->dreapta!=NULL) //Nodul de sters are doi fii. Inlocuim cu succesor
        {
            NodItem *succesor;
            int aux_valoare;
            succesor=getInOrderSuccesorNode(node); //Salvam succesorul
       //     printf("succesor: %d \n",succesor->valoare);
       //     printf("succesor parent: %d \n",succesor->parinte->valoare);
            decreaseLaneDimensionStartingWithNode(succesor); //scadem dimensiunile incepand de la succesor
            aux_valoare=succesor->valoare;
            nr_atribuiri++;
            nr_comparatii++;
            if (succesor->dreapta!=NULL)
            {
                nr_comparatii++;
                if (succesor->parinte!=node)
                {
                    nr_atribuiri++;
                    succesor->parinte->stanga=succesor->dreapta;
                    free(succesor); ///
                }
                else
                {
                    nr_atribuiri++;
                     succesor->parinte->dreapta=succesor->dreapta;
                     free(succesor);///
                }
            }
            else
            {
                nr_comparatii++;
                if (succesor->parinte!=node)
                {
                    nr_atribuiri++;
                    succesor->parinte->stanga=NULL;
                    free(succesor); ///
                }
                else
                {
                    nr_atribuiri++;
                    succesor->parinte->dreapta=NULL;
                    free(succesor); ///
                }
            }
            node->valoare=aux_valoare;
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////
/**
    Meotoda de consturire a arborelui binar de cautare balansat
    @param low = limita inferioara a intervalului de valori
    @param high = limita superioara a intervalului de valori
    @return = radacina arborelui construit
*/
NodItem* buildBalancedSearchTree(int low,int high)
{
    NodItem *root;
    int center=(low+high) / 2; //de fiecare data inseram mijolocul din interval
    if (low>high)
    {
        return NULL;
    }
    root=(NodItem*)malloc(sizeof(NodItem));
    root->valoare=center;
    root->dreapta=NULL;
    root->stanga=NULL;
    root->parinte=NULL;
    root->dimensiune=high-low+1;
    printf("Introducem : %d \n",root->valoare);
    if (low<high)
    {
        root->stanga=buildBalancedSearchTree(low,center-1);
        if (root->stanga!=NULL)
        {
            root->stanga->parinte=root;
        }
        root->dreapta=buildBalancedSearchTree(center+1,high);
        if(root->dreapta!=NULL)
        {
            root->dreapta->parinte=root;
        }
    }
    return root;
}
/////////////////////////////////////////////////////////////////////////////////////////////
/**
    Printeaza arborele ( preety print adica ). La prima apelare parametrul level trebuie sa fie 0
    @param root = radacina arborelui
    @param level = nivelul de afisare
*/
void printTree(NodItem* root,int level)
{
    int i;
    if (root!=NULL)
    {
        printTree(root->dreapta,level+1);
        for(i=0;i<level;i++)
        {
            printf("\t");
        }
        printf("%d<->%d \n",root->valoare,root->dimensiune);
        printTree(root->stanga,level+1);
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
/**
    Returneaza nodul de rank i din subarborele cu radacina root
    @param root = radacina subarborelui in care caut nodul de rank i
    @param i = rank-ul nodului cautat in subarbore
    @return un NodItem* ce reprezinta nodul gasit de grad i in subarborele cu radacina in root
*/
NodItem* SO_Selection(NodItem *root,int i)
{
   int rankk;
   nr_comparatii++;
   if (root->stanga!=NULL)
   {
       rankk=root->stanga->dimensiune+1;
   }
   else
   {
       rankk=1;
   }
   nr_comparatii++;
   if (i==rankk)
   {
       return root;
   }
   else
   {
       nr_comparatii++;
       if(i<rankk)
       {
           return SO_Selection(root->stanga,i);
       }
       else
       {
           return SO_Selection(root->dreapta,i-rankk);
       }
   }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
/**
    Returneaza castigatorul
    @param people = numarul de participanti
    @param passes = numarul de participanti peste care trece numaratoarea
    @return un intreg ce reprezinta castigatorul
*/
int Josephus(int people,int passes)
{
    int k,survivor;
    NodItem *x;
    radacina=buildBalancedSearchTree(1,people);
    radacina->parinte=NULL;
  //  printTree(radacina,0);
    int j=1;
    //printf("Eliminari: \n");
    for (k=people;k>1;k--)
    {
        j=((j+passes-2) % k)+1;
        x=SO_Selection(radacina,j);
        decreaseLaneDimensionStartingWithNode(x);
        printf("--------------------------------------------------\n");
        printf("Dupa Eliminarea lui: %d \n",x->valoare);
        deleteNodeFromTree(radacina,x);
       // printTree(radacina,0);
        printf("--------------------------------------------------\n");
    }
    survivor=radacina->valoare;
    free(radacina);
    return survivor;
}
////////////////////////////////////////////////////////////////////////////////////////////////

void test_joseph()
{
    int n=10000,m=200;
    printf("-------------------------------------\n");
    printf("Survivor: %d\n",Josephus(n,m));
}

void createStatistics()
{
    FILE *f;
    f=fopen("Josephus.csv","w");
    fprintf(f,"n,Comparatii,Atribuiri,Total\n");
    int n;
    for (n=100;n<10000;n=n+500)
    {
        nr_comparatii=0;
        nr_atribuiri=0;
        printf("----------------------------------\n");
        printf("Josepus (%d %d) \n",n,n/2);
        printf("Survivor: %d \n",Josephus(n,n/2));
        fprintf(f,"%d,%ld,%ld,%ld\n",n,nr_comparatii,nr_atribuiri,nr_comparatii+nr_atribuiri);
    }
    fclose(f);

}

void test_creare_arbore()
{
    radacina=buildBalancedSearchTree(1,10000);
    printTree(radacina,0);
}

int main()
{
    //test_creare_arbore();
    test_joseph();
    //createStatistics();
    return 0;
}
