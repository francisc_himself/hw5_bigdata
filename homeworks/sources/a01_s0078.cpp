#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

long random(void) // cu aceasta functie generam un numar long aleator.
{
   srand((long)time(NULL));
   long r = rand();
   return r;
}

void AverageCase(long sir[])// cu aceasta functie cream un sir de numere cu valori aleatoare, care reprezinta cazul mediu favorabil.
{
	long n = 10000;
	long i;
	for(i = 0; i < n; i++)
		sir[i] = random();
}

void BestCase(long sir[])//cu aceasta functie cream un sir de numere ordonate crescator, care reprezinta cazul cel mai favorabil.
{
	long n = 10000;
	long i;
	for(i = 0; i < n; i++)
		sir[i] = i;
}

void WorstCase(long sir[])//cu aceasta functie cream un sir de numere ordonate descrescator, care reprezinta cazul cel mai putin favorabil.
{
	long n = 10000;
	long i;
	for(i = 0; i < n; i++)
		sir[i] = n - i;
}

void bubble_sort(long s[], FILE *df, long n)//aceasta functie sorteaza un sir prin metoda bubble_sort si numara cate atribuiri si comparatii s-au facut.
{
	long i, j;
	long atribuiri = 0, comparatii = 0;
	long *temp = (long*)malloc(sizeof(long)*n);
	long aux;
	for(i = 0; i < n; i++) temp[i] = s[i];

	for(j = 0; j < n; j++)
	{
	for(i = 0; i < n - i; i++)
	{
		if(temp[i + 1] < temp[i]) 
		{
			aux = temp[i];
			temp[i] = temp[i + 1];
			temp[i + 1] = aux;
			atribuiri += 3;
		}
		comparatii += 2;
		atribuiri += 1;
	}
	comparatii += 1;
	}
	fprintf(df, "%d,", atribuiri);
	fprintf(df, "%d,", comparatii);
		
}

void insert_sort(long s[], FILE *df, long n)//aceasta functie sorteaza un sir prin metoda insert_sort si numara cate atribuiri si comparatii s-au facut.
{
	long i, j;
	long atribuiri = 0, comparatii = 0;
	long *temp = (long*)malloc(sizeof(long)*n);
	long aux;
	for(i = 0; i < n; i++) temp[i] = s[i];

	for(j = 1; j < n; j++)
	{
		aux = temp[j];
		i = j-1;
		while((i >= 0) && (temp[i] > aux))
		{
			temp[i + 1] = temp[i];
			i--;
			atribuiri += 2;
			comparatii += 2;
		}
		temp[i + 1] = aux;
		atribuiri += 4;
		comparatii += 1;

	}
	fprintf(df, "%d,", atribuiri);
	fprintf(df, "%d,", comparatii);
	
		
}

void select_sort(long s[], FILE *df, long n)//aceasta functie sorteaza un sir prin metoda selecty_sort si numara cate atribuiri si comparatii s-au facut.
{
	long i, j;
	long atribuiri = 0, comparatii = 0;
	long *temp = (long*)malloc(sizeof(long)*n);
	long min, jmin;
	for(i = 0; i < n; i++) temp[i] = s[i];

	for(i = 0; i < n; i++)
	{
	min = temp[i]; jmin = i;
	atribuiri += 3;
	for(j = i + 1; j < n; j++)
	{
		if(temp[j] < min) 
		{
			min = temp[j];
			jmin = j;
			atribuiri += 2;
			comparatii += 1;
		}
		comparatii += 1;
		atribuiri += 2;
	}
	min = temp[i];
	temp[i] = temp[jmin];
	temp[jmin] = min;
	atribuiri += 3;
	}

	fprintf(df, "%d,", atribuiri);
	fprintf(df, "%d,", comparatii);
		
}

long main()
{
	FILE *f;
	f = fopen("results.csv","w");
	long n = 100;
	long sir[10000];
	long i;

	AverageCase(sir);//testam fiecare metoda de sortare pentru cazul mediu favorabil.
	for(i = 0; i < 100; i++)
	{
		bubble_sort(sir, f, n);
		insert_sort(sir, f, n);
		select_sort(sir, f, n);
		fprintf(f,"\n");
		n += 100;
	}
	n = 100;

	BestCase(sir);//testam fiecare metoda de sortare pentru cazul cel mai favorabil.
	for(i = 0; i < 100; i++)
	{
		bubble_sort(sir, f, n);
		insert_sort(sir, f, n);
		select_sort(sir, f, n);
		fprintf(f,"\n");
		n += 100;
	}
	n = 100;

	WorstCase(sir);//testam fiecare metoda de sortare pentru cazul cel mai putin favorabil.
	for(i = 0; i < 100; i++)
	{
		bubble_sort(sir, f, n);
		insert_sort(sir, f, n);
		select_sort(sir, f, n);
		fprintf(f,"\n");
		n += 100;
	}
		
	fclose(f);
	system("pause");
	return 0;
}