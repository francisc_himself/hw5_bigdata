/* ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***
*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>

#define MAX_SIZE 10001

void generare(int sir[],int n)
{
	srand(time(NULL));
	for(int i=1;i<=n;i++)
		sir[i]=rand();
}

void generare_descresc(int sir[],int n)
{
	srand(time(NULL));
	sir[n]=rand();
	for(int i=n-1;i>=1;i--)
		sir[i]=sir[i+1]+rand();
}

void afisare(int sir[],int n)
{
	for(int i=1;i<=n;i++)
		printf("%d  ", sir[i]);
    printf("\n");

}

//functie de sortare prin metoda bulelor
void sortareaBulelor(int sir[],int n,int &k_asignari1,int &k_comparari1)
{
	int i,j,aux;
	for(i=1;i<n;i++)
		for(j=i+1;j<=n;j++)
		{
			k_comparari1++;
			if(sir[i]>sir[j])
			{
				aux=sir[i];
				sir[i]=sir[j];
				sir[j]=aux;
				k_asignari1+=3;
			}
		}
}

//functie de sortare prin insertie
void insertie(int sir[], int n, int &k_asignari2, int &k_comparari2)
{
	int i,j,k,x;
	for(i=2;i<=n;i++)
	{
		x=sir[i];
		j=1;
		k_comparari2++;
		while (sir[j]<sir[i])
		{
			j++;
			k_comparari2++;
		}
		for(k=i-1;k>=j;k--)
		{
			sir[k+1]=sir[k];
			k_asignari2++;
		}
		sir[j]=x;
		k_asignari2+=2;
	}
}

//functie de sortare prin selectie
void selectie(int sir[], int n, int &k_asignari3, int &k_comparari3)
{
	int i,j,min;
	for(i=1;i<n;i++)
	{
		min=i;
		for(j=i+1;j<=n;j++)
		{
			k_comparari3++;
			if(sir[j]<sir[min])
			{
				min=j;
				sir[i]=sir[min];
				k_asignari3++;
			}	
		}
	}
}

//functie care verifica daca un sir este sortat
int verificare(int sir[],int n)
{
	int ok=1,i;
	for(i=0;i<n-1;i++)
		if(sir[i]>sir[i+1])
		{
			ok=0;
			break;
		}
		return ok;
}
void main()
{
	int sir[MAX_SIZE],dim,k_asignari1,k_comparari1,k_asignari2,k_comparari2,k_asignari3,k_comparari3,i,tip;
	//float kAmed,kCmed;
	FILE *f,*g,*h;
	/*printf("dim=");
	scanf("%d", &dim);
	while(dim>MAX_SIZE)
	{
		printf("Dimensuinea introdusa este prea mare!\ndim=");
		scanf("%d", &dim);
	}
	generare(sir,dim);
	afisare(sir,dim);
	sortareaBulelor(sir,dim);
	afisare(sir,dim);
	if(verificare(sir,dim))
		printf("Sirul este sortat");
	else
		printf("Sirul nu a fost sortat");*/
	/*for(dim=100;dim<=10000;dim+=100)
		{
			kAmed=0;
			kCmed=0;
			for(i=1;i<=5;i++)
			{
				generare(sir,dim);
				sortareaBulelor(sir,dim,k_asignari,k_comparari);
				if(verificare(sir,dim))
				{
					kAmed+=k_asignari;
					kCmed+=k_comparari;
				}
				else
				{
					printf("Eroare de sortare!\n");
					exit(2);
				}
			}
			kAmed=kAmed/5;
			kCmed=kCmed/5;
			fprintf(f,"%d,%f,%f,%f\n",dim,kAmed,kCmed,kAmed+kCmed);
			printf("dim=%d\n",dim);
		}
		fclose(f);
		printf("Finalizare cu succes!\n");*/

	h=fopen("CazMediu.csv","w");
	if(h==NULL)
	{
		perror("EROARE DESCIDERE!");
		exit(1);
	}
	else
	{
		for(dim=100;dim<=10000;dim+=100)
		{
			
			printf("\ndim= %d", dim); //afisez dimensiunea sa vad unde a ajuns rularea
			k_asignari1=0;
			k_comparari1=0;
			k_asignari2=0;
			k_comparari2=0;
			k_asignari3=0;
			k_comparari3=0;
			for(i=1;i<=5;i++)
			{
				generare(sir,dim);
				sortareaBulelor(sir,dim,k_asignari1,k_comparari1);
				generare(sir,dim);
				insertie(sir,dim,k_asignari2,k_comparari2);
				generare(sir,dim);
				selectie(sir,dim,k_asignari3,k_comparari3);
			}
			fprintf(h,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d \n",dim,k_asignari1/5,k_comparari1/5,(k_asignari1+k_comparari1)/5,k_asignari2/5,k_comparari2/5,(k_asignari2+k_comparari2)/5,k_asignari3/5,k_comparari3/5,(k_asignari3+k_comparari3)/5);
		}
	}
	fclose(h);
	printf("\nTerminare caz mediu statistic\n");
	
	h=fopen("CazDefav.csv","w");
	if(h==NULL)
	{
		perror("EROARE DESCIDERE!");
		exit(1);
	}
	else
	{
		for(dim=100;dim<=10000;dim+=100)
		{
			
			printf("\ndim= %d", dim); //afisez dimensiunea sa vad unde a ajuns rularea
			generare_descresc(sir,dim);
			k_asignari1=0;
			k_comparari1=0;
			sortareaBulelor(sir,dim,k_asignari1,k_comparari1);
			k_asignari2=0;
			k_comparari2=0;
			generare_descresc(sir,dim);
			insertie(sir,dim,k_asignari2,k_comparari2);
			k_asignari3=0;
			k_comparari3=0;
			generare_descresc(sir,dim);
			insertie(sir,dim,k_asignari3,k_comparari3);
			fprintf(h,"%d,%d,%d,%d,%d,%d,%d,%d,%d,%d \n",dim,k_asignari1,k_comparari1,k_asignari1+k_comparari1,k_asignari2,k_comparari2,k_asignari2+k_comparari2,k_asignari3,k_comparari3,k_asignari3+k_comparari3);
		}
	}
	fclose(h);
	printf("\nTerminare caz defavorabil\n");

	getch();
}
