//***ANONIM*** ***ANONIM***, grupa ***GROUP_NUMBER***, Assign 8
//Prof Vlad Baja
//Multimi Disjuncte
//Scrierea procedurilor formeaza_multime, gaseste_multime si reuneste pentru multimi disjuncte.
//Complexitatea este O(m+n*lg(n)). Fiecare arbore are n noduri, deci in operatiile reuneste fiecare pointer spre reprezentant poate
//fi modificat de lg(n) ori. Fiecare operatie formeaza_multime si gaseste_multime necesita un timp de O(1) si sunt m operatii
//de acest fel.
//Am implementat codul pentru fiecare procedura, formeaza_multime, gaseste_multime, uneste si reuneste, pe baza pseudocodului din Cormen, care vor fi
//folosite in verificarea componentelor conexe. Am implementat o structua pentru muchii, care contine nodul de unde incepe si unde se termina o muchie.
//Parintii si rangurile nodurilor i-am retinut in structuri de tip vector.
//In main, pentru verificare, introducem numarul de noduri si numarul de muchii, apoi nodurile intre care avem muchie si afisam parintele
//pentru fiecare nod . In fisierul rezultat, disj.csv, pentru un contor de la 10000 la 60000 afisam numarul total de asignari si comparatii
//pe durata intregului algoritm. Dupa cum rezulta si din grafic, algoritmul e unul liniar.
#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
#define max 60001

typedef struct mu
{
	int u;
	int v;
}much;
much muchie[max];
int n,m;
int cont=0; //contor
int p[max],rang[max];

void formeaza_multime(int x)
{
	cont+=2;
	p[x]=x;
	rang[x]=0;
}

void uneste(int x, int y)
{
	cont++;
	if (rang[x]>rang[y])
	{
		p[y]=x;//nu modificam rangul deoarece y este deja descendentul lui x.
	     cont++;
	}
	else
	{
		p[x]=y;
		cont+=2;
		if (rang[x]==rang[y])
		{
			rang[y]=rang[y]+1;//y mai aduauga un descendent.
			cont++;
		}
	}
}

int gaseste_multime(int x)
{
	cont++;
	if (x!=p[x])
		p[x]=gaseste_multime(p[x]);
	return p[x];
}

void reuneste(int x, int y)
{
	uneste(gaseste_multime(x), gaseste_multime(y));
}

void componente_conexe()
{
	int i;//n e numarul de varfuri, m e numarul de muchii
	for(i=0;i<n;i++)
	  formeaza_multime(i);
	for(i=0;i<m;i++)
		if (gaseste_multime(muchie[i].u)!=gaseste_multime(muchie[i].v))
			reuneste(muchie[i].u,muchie[i].v);
}

int aceeasi_componenta(int i)
{
	if (gaseste_multime(muchie[i].u)==gaseste_multime(muchie[i].v))
		return 1;//true
	else return 0;//false
}

void main()
{
    //pentru verificare
	/*printf("introduceti numarul de varfuri:");
	scanf("%d",&n);
	printf("introduceti numarul de muchii:");
	scanf("%d",&m);
	printf("muchii de la ... la ...");
	for (int i=0;i<m;i++)
	  scanf("%d %d", &(muchie[i].u), &(muchie[i].v));

	componente_conexe();
	for (int i=0;i<m;i++)
	  aceeasi_componenta(i);
	for (int i=0;i<n;i++)
	{
		printf("%d %d\n", i,p[i]);
	}*/

	int i,g;
	FILE *f;
	n=10000;
	srand(time(NULL));
	f=fopen("disj.csv","w");
	fprintf(f,"muchii  operatii\n");
	for(m=10000;m<60000;m+=1000)
	{
		cont=0;
		for(i=0;i<m;i++)
		{
	     	do
		    {
			  muchie[i].u=rand()%10000;
			  do
			  {
			    muchie[i].v=rand()%10000;
			  }
			  while (muchie[i].u==muchie[i].v);

			  g = 0;
     		 for (int j=0;j<i;j++)
			   if (((muchie[j].u==muchie[i].u) && (muchie[j].v==muchie[i].v)) ||
				 ((muchie[j].v==muchie[i].u) && (muchie[j].u==muchie[i].v)))
				     g=1;
			}
			while (g==1);
		}
		  componente_conexe();
		  fprintf(f,"%d  %d\n",m, cont);
	}
	printf("sfarsit program!");
	getch();
}




