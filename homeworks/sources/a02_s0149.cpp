﻿#include <stdio.h>
#include <process.h>
#include <stdlib.h>
#include <conio.h>
/******ANONIM*** ***ANONIM*** ,GR ***GROUP_NUMBER***
 **Se cere implementarea  corectă  și  eficientă  a două metode de construire a structurii de date Heap i.e. “de jos în sus” (bottom-up) și “de sus în jos” (top-down)
 *Heapsort este un algoritm de sortare care nu necesita memorie suplimentara (sorteaza vectorul pe loc), care functioneaza in doua etape: 
 *       [A]  transfomarea vectorului de sortat intr-un arbore heap; 
 *       [B]  reducerea  treptata  a  dimensiunii  heap-ului  prin  scoaterea  valorii  din  radacina. 
 *     Valorile rezultate in ordine descrescatoare sint plasate in vector pe spatiul eliberat, de la dreapta la stinga. 
 **Ordinul de complexitate este O(lgN)
 */
FILE *g;
int c;   // retine numarul de operatii critice pentru Bottom-Up
int c1;  // retine numarul de operatii critice pentru Top-Down
int A; //Bottom-Up
int A1; //Top-Down
int lung;

void max_heapify(int *A,int i, int m){
	int l, r, max, aux;

	l = 2*i;
	r = 2*i + 1;
	c++;
	if ((l <= m) && (A[l]>A[i]))
	{  	max = l;A++; 
	}  	else { max = i;A++; }
	c++;
	if ((r<=m) && (A[r]>A[max]))
	{ max = r;A++; }
	if (max != i) {
		aux = A[i];
		A[i] = A[max];
		A[max] = aux;
		A=A+2;
		c=c+3;
		max_heapify(A,max,m);
	}
}

void build_max_heap1(int *A , int m) {
	int i;
	for (i=m/2;i>=0;i--)
		max_heapify(A,i,m);
}

int heap_extract_max(int *A, int *m) { // pop
	int max,aux;
	if (*m < 1)
		printf ( "heap underflow" );
	max = A[1];
	aux = A[1];
	A[1]= A[*m];
	A[*m]=aux;
	m=m-1;
	max_heapify(A, 1, *m);
	return max;
}

void heap_increase_key(int *A, int i, int key) { // push
	int aux;
	if ( key < A[i])
		printf (" error : new key is smaller than current key");
	A[i]=key;
	c1++;
	while ((i > 0) && (A[i/2] < A[i])) {
		aux = A[i];
		A[i]= A[i/2];
		A[i/2]=aux;
		i=i/2;
		A1=A1+2;
		c1=c1+4; }
}

void max_heap_insert(int *A, int key) {
	lung++;
	A[lung] = -1;
	heap_increase_key(A, lung, key);
}

void build_max_heap2(int *A , int m){
	int i;
	lung =0;
	for (i = 1;i < m ; i++) {
		max_heap_insert(A, A[i]);
	}
}

int main( void )
{
	int m=100;		// variabila care retine marimea vectorului care urmeaza sa fie ordonat
	int x;
	int k;
	int q;
	int i;		
	int j;			// variabile auxiliare
	int v[10000];  // vectorul propriu-zis
	int u[10000];
	int z;		

	fopen_s( &g, "Out.csv", "w" );   // fisierul de iesire 

	while (m<=10000)	// testarea dimensiunii maxime 
	{
		c=0;// initializarea contorului de operatii critice
		c1=0;
		A=0;
		A1=0;
		for (q=0;q<1;q++)
		{
			for (j=0;j<m;j++)
			{
				z=rand();		// 	alocarea unor elemente aleatoare vectorului 
				v[j]=z;
				u[j]=z;
			}		   	   

			build_max_heap1(v,m); // bottom up strategy	
			build_max_heap2(u,m); // top down strategy

			srand(m/(q+1));	 // schimbarea seedului  
		}		
		fprintf_s(g,"%d;%d;%d;%d;%d;%d;%d\n",m,A,c/5,A+c/5,A1,c1/5,A1+c1/5); // scrierea in fisierul de iesire a valorilor medii de op. critice

		m=m+100;   // schimbarea dimensiunii vectorului
	}	 
	fclose( g );  
}
