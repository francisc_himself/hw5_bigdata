/****ANONIM*** ***ANONIM***,grupa ***GROUP_NUMBER***
Sunt de parere ca quicksortu-ul este cel mai indicat in toate cazurile,datorita faptului ca prezinta mai putine asignari si
comparatii.In cazul unui sir gata sortat crescator heapsort-ul se afla intr-un caz favorabil,deoarece nu executa asignarile,
iar in caz unui sir descrescator heapsort-ul va trebui sa execute atat comparatii numeroase cat si asignarile.Iar pentru quicksort
cazul cel mai favorabil apare cand partitionarile sunt echilibrate,cazul cel mai defavorabil se produce atunci cand partitiile
sunt maxim dezechilibrate.Ca sa calculam quicksort  "best case"
*/
#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
#define Max -10000;
Profiler profiler("demo");
int lungime;
int size,comp;

int parent(int i)
{
	return i/2;
}
int stg(int i)
{
	if(i!=0)
	return 2*i;
}
int dr(int i)
{
	if(i!=0)
	return 2*i+1;
}
void reconstituie(int* a,int i,int n)
{
	int m=n;
  int max;
  int s=stg(i);
  int d=dr(i);
  profiler.countOperation("bott",lungime,2);
  if((s<=m)&&(a[i]<a[s]))
	  max=s;
  else max=i;
  if((d<=m)&&(a[max]<a[d]))
	  max=d;
  if(i!=0)
  if (max!=i)
  {
	   profiler.countOperation("bott",lungime,3);
     int aux=a[i];
	  a[i]=a[max];
	  a[max]=aux;
      reconstituie(a,max,m-1);
	 
  }
  
 
}


void bottom(int* a,int n)
{
	int m=n;
   for(int i=(n+1)/2;i>=1;i--)
   {
	  
      reconstituie(a,i,m);
	   
   }
   
}
void bottom_up(int* a,int n)
{
	bottom(a,n);
   int m=n;
   int aux=0;
   for(int i=m;i>=2;i--)
   {
	   profiler.countOperation("bott",lungime,3);
      aux=a[1];
	  a[1]=a[i];
	  a[i]=aux;
	  m--;
	  
	  reconstituie(a,1,m);
   }
}
void afisare(int*v,int n,int k,int nivel)

{
 if(k>n)
	 return;
 afisare(v,n,2*k+1,nivel+2);
 for(int i=1;i<=nivel;i++)
	 printf(" ");
 printf("%d\n",v[k]);
 afisare(v,n,2*k,nivel+2);
}
void afisare1(int*v,int n,int k,int nivel)

{
 if(k>n)
	 return;
 afisare(v,n,2*k+1,nivel+2);
 for(int i=1;i<=nivel;i++)
	 printf(" ");
 printf("%d\n",v[k]);
 afisare(v,n,2*k,nivel+2);
}
int Partitionare(int*a,int p,int r)
{
	profiler.countOperation("quick",lungime,1);
  int x=a[r];
  int i=p-1;
  for(int j=p;j<=r-1;j++)
  {
	profiler.countOperation("quick",lungime,1);
    if(a[j]<x)
	{
	profiler.countOperation("quick",lungime,3);
       i++;
	   int aux=a[i];
	   a[i]=a[j];
	   a[j]=aux;
	}
  }
  profiler.countOperation("quick",lungime,3);
  int aux1=a[i+1];
  a[i+1]=a[r];
  a[r]=aux1;
  return i+1;
}
void quicksort(int*a,int p,int r)
{
	int i,j,pivot,aux;
	int q;
	pivot = a[p]; 
	profiler.countOperation("quick",lungime,1);
 i=p;j=r;
 while(i<=j)
 {
	 while(a[i]<pivot)
	 {
		 i++;
		profiler.countOperation("quick",lungime,1);
	 }
	 while(a[j]>pivot)
	 {
		 j--;
		 profiler.countOperation("quick",lungime,1);
		 //comp++;
	 }
	 if(i<=j)
	 {
		 aux=a[i];
		 a[i]=a[j];
		 a[j]=aux;
		 i++;
		 j--;
		 profiler.countOperation("quick",lungime,3);
	 }

 }
 if(p<j)
	 quicksort(a,p,j);
 if(i<r)
	 quicksort(a,i,r);

}

void main()
{
	int n;
	int v[10000],v1[10000];
	int v2[]={0,1,5,2,6,4,3,10};
	int v3[10];
	for(int i=0;i<=7;i++)
	{
		v3[i]=v2[i];
	}
    for(int t=5;t>=1;t--)
	{
	for(n=100;n<2000;n+=100)
	{
	  lungime=n;
	  FillRandomArray(v,n,10,50000,false,1);
	  printf("n=%d\n",n);
	  memcpy(v1,v,n*sizeof(int));	
	  bottom_up(v1,n);
	  memcpy(v1,v,n*sizeof(int));	
	  quicksort(v1,0,n+1);
	  }
	}
	//bottom_up(v3,7);
	//quicksort(v2,0,7);
	//afisare(v3,7,1,0);
	//printf("\n");
	//afisare(v2,7,1,0);
	size=7;
	profiler.createGroup("Afis","quick","bott");
	
	profiler.showReport();

}