#include "stdlib.h"
#include "stdio.h"
#include "conio.h"
#include "time.h"

/*
    Gr. ***GROUP_NUMBER***
        ***ANONIM*** ***ANONIM*** ***ANONIM***

    Started at: 5 march 2013
    Finished at: 11 march 2013

    This source file has served well at generating .csv files with raw data about the number of assignments and
    comparisons each of the common 3 direct sorting methods (insertion, selection, and bubble sort) performs in
    the average case, as well as the worst and best, for arrays of integers ranging from 100 to 10000 elements,
    incremented by 300, and starting at 100 elements. The average scenario has been obtained by randomly
    generating 5 such arrays per step and averaging their results. It is important to note that in order to limit
    the time spent calculating, the range of generated integers is between 1-1000 thus creating a large number of
    duplicates in the arrays. This means that in larger cases the number of operations is slightly smaller than
    it would be with a wider range of integers, but the difference overall is negligible.

    [IMPORTANT]
    I did not manage to figure out the worst case scenario for selection sort. I used the one we were told should
    be the worst case scenario during the seminar, but it has turned out to oddly be about as efficient as the
    average case scenario, if not sometimes better. I is presumably close to the actual worst though.
    [/IMPORTANT]

    Said generated data has been used  to determine their effectiveness. Said raw data and charts have not been
    sent with this source code in the e-mail since it was not specified in the laboratory guidline, but I should
    have all of them on me (or on my mail account) during the next laboratory. Source code has been successfully
    tested for correctness, and all three sorting functions have been coded with stability in mind.

    Other than that, in the average case bubble sort has performed severly worse than both insertion sort and
    selection sort, generally taking twice as many overall computations to finish sorting an array. Whis it has
    performed the best comparisons-wise, this means that it has severely underperformed as far as the number of
    assignments go, and due to them being generally more time- and memory- consuming than comparisons, it would
    seem that bubble sort is the most ineffective of them all, especially considering how its worst case is also
    the worst out of all three, adding up to more than 180 million operations for a 10 000 elements array.

    Selection sort always performs the same (large) number of comparisons no matter which case we are in, while
    keeping assignments to a minimum; this means that while insertion sort may outperform selection sort in
    several cases, selection sort is probably a better choice if we want to implement real-time sorting since it
    doesn't fluctuate much in time spent sorting (which has never varied by more than 0.3 seconds on my PC when
    sorting all 34 arrays from 100 to 10000 elements).

    Insertion sort performs very similar to selection sort in the average case, but would definitely be the
    better choice if we had to (re)sort arrays which have had a small amount of values changed or added, its
    best case scenario being much better than selection sort and close to that particular situation.


    PS:

    I apologize for how messy my code is. It was decent to begin with but as I added more and more tests, I had
    decided not to delete information about how I have obtained my previous results due to creating all the
    necessary data with only one source file. I also didn't bother adding many comments to the code due to it
    being very simple.
*/

static int cc=0, ac=0, a[10001]={0}, n;

void select()
{
	int i, j, t,tmp;
	for (i=1; i<n-1;i++)
	{
		t=i;
		for (j=i+1;j<n;j++)
		{
			cc++;
			if (a[t]>a[j])
			{
				t=j;
			}
		}
		if (t!=i)
		{
			tmp=a[t];
			a[t]=a[i];
			a[i]=tmp;
			ac=ac+3;
		}
	}
}

void insert()
{
	int i, aux, j;
	a[0]=-10000;
	for (i=2; i<n-1; i++)
	{
		aux=a[i];
		ac++;
		j=i-1;
		cc++;
		while (aux<a[j])
		{
			a[j+1]=a[j];
			j--;
			ac++;
			cc++;
		}
		a[j+1]=aux;
		ac++;
	}
}

void bubble()
{
	int i, aux, ok, r, ul,ll;
	ul=n-2;
	ll=1;
	ok=0;
	while (ok==0)              // while the array was not sorted
	{
		ok=1;                  // presume it now is
		for (i=ll;i<=ul;i++)   // i goes from the first unsorted element to the last
		{
			cc++;
			if (a[i]>a[i+1])   // if we find entries in disarray, we swap them and mark the place we swapped
			{                  // and we also take note that the array was not sorted at the start of this pass
				ok=0;
				aux=a[i];
				a[i]=a[i+1];
				a[i+1]=aux;
				ac=ac+3;
				r=i;
			}
		}
		ul=r;                  // last swap is now the last unordered element
		if (ok==1) return;     // if the vector was in fact sorted, we are finished
		cc++;
		for (i=ul;i>ll;i--)    // if not, we repeat the same steps again but this time starting from the last element
		{                      // towards the first one
			if (a[i]<a[i-1])
			{
				ok=0;
				aux=a[i];
				a[i]=a[i-1];
				a[i-1]=aux;
				r=i;
				ac=ac+3;
			}
		}
		ll=r;
		cc++;
	}
}

void generate()
{
	int i;
	for (i=1;i<n;i++)
	{
		//a[i]=rand()%999+1;  // random
		//a[i]=i;             // best case
		//a[i]=n-i;           // insert / bubble worst
		if (i<n/2)           //select worst
		{
		    a[i]=2*i;
		}
		else
		{
		    a[i]=(n-i)*2-1;
		}
	}
}

int main(/*argc, *argv[]*/)
{
	int i,j;
	FILE *f;
	f=fopen("D:/Facultate/an2sem2/FA/test cases/selworst.txt", "w"); //fisierul fiind bubble.txt, insertion.txt sau selection.txt
	srand(time(NULL));
	/*int f;

	switch (argv[1][1])
	{
		case 's':
		break;

		case 'i':
		break;

		case 'b':
		break;

		default:
		break;
	}*/
	/*a[1]=10;      // JUST TESTING
	a[2]=8;         // TESTING DONE
	a[3]=9;         // EVERYTHING SEEMS TO BE OK AND IN THIS PARTICULAR CASE
	a[4]=1;         // INSERTION SORT'S PERFORMED BETTER
	a[5]=3;
	a[6]=11;
	n=7;
	select();       // WHEW
	for (i=1;i<n;i++)
		printf("%d ", a[i]);*/
	/*
	for (i=1;i<6;i++)                                                                   // TEST 2 TRECUT CU BRIO
	{
		fprintf(f,"\nSelection average @ %d elements:", n-1);
		generate();
		select();
		fprintf(f,"\n%d: Assigns: %d; Comparisons: %d", i, ac, cc);
		ac=0;
		cc=0;
	}
	fprintf(f,"\n");
	for (i=1;i<6;i++)
	{
		fprintf(f,"\nInsertion average @ %d elements:", n-1);
		generate();
		insert();
		fprintf(f,"\n%d: Assigns: %d; Comparisons: %d", i, ac, cc);
		ac=0;
		cc=0;
	}
	fprintf(f,"\n");
	for (i=1;i<6;i++)
	{
		fprintf(f,"\nBubble average @ %d elements:", n-1);
		generate();
		bubble();
		fprintf(f,"\n%d: Assigns: %d; Comparisons: %d", i, ac, cc);
		ac=0;
		cc=0;
	}
	for (j=0;j<33;j++)
	{
		n=n+300;
	for (i=1;i<6;i++)
	{
		fprintf(f,"\nSelection average @ %d elements:", n-1);
		generate();
		select();
		fprintf(f,"\n%d: Assigns: %d; Comparisons: %d", i, ac, cc);
		ac=0;
		cc=0;
	}
	fprintf(f,"\n");
	for (i=1;i<6;i++)
	{
		fprintf(f,"\nInsertion average @ %d elements:", n-1);
		generate();
		insert();
		fprintf(f,"\n%d: Assigns: %d; Comparisons: %d", i, ac, cc);
		ac=0;
		cc=0;
	}
	fprintf(f,"\n");
	for (i=1;i<6;i++)
	{
		fprintf(f,"\nBubble average @ %d elements:", n-1);
		generate();
		bubble();
		fprintf(f,"\n%d: Assigns: %d; Comparisons: %d", i, ac, cc);
		ac=0;
		cc=0;
	}
	fprintf(f,"\n");
	}*/

	/*n=101;
	fprintf(f,"n, ass, comp");                  // .CSV FORMAT
	for (i=0;i<34;i++)
	{
		generate();
		bubble();                                // IN FUNCTIE DE TXT VA FI APELAT DE 5 ORI BUBBLE, SELECTION SAU INSERTION
		generate();
		bubble();
		generate();
		bubble();
		generate();
		bubble();
		generate();
		bubble();
		ac=ac/5;                                 // MEDIE
		cc=cc/5;
		fprintf(f,"\n%d, %d, %d", n-1, ac, cc);  // .CSV FORMAT
		n=n+300;
		ac=0;                                    // RESETAM CONTOARELE.
		cc=0;
	}
	*/
	   n=101;
	fprintf(f,"\n\nn, ass, comp");                  // .CSV FORMAT
	for (i=0;i<34;i++)
	{
		generate();
		select();                                // IN FUNCTIE DE TXT VA FI APELAT DE 5 ORI BUBBLE, SELECTION SAU INSERTION
		fprintf(f,"\n%d, %d, %d", n-1, ac, cc);  // .CSV FORMAT
		n=n+300;
		ac=0;                                    // RESETAM CONTOARELE.
		cc=0;
	}
	fclose(f);

	return 0;
}
