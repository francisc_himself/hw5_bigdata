/**
***ANONIM*** ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***

Concluzii:
- desi atat HeapSort cat si QuickSort au o eficienta de O(n log n), QuickSort in cazul worst case are o eficienta de
  O(n^2), cel din urma fiind totusi mai eficient deoarece nu presupune recursivitate
- se observa si din grafice ca in cazul QuickSort, in cazul defavorabil se executa un numar foarte mare de operatii
  in cazul favorabil numarul operatiilor efectuate este jumatate din numarul operatiilor efectuate in cazul mediu
  si o cincime din numarul operatiilor efectuate in cazul cel mai defavorabil
- in cazul mediu, QuickSort-ul este mai eficient deoarece nu presupune recursivitate, astfel numarul de operatii fiind mai mic 
*/



#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<time.h>

int atrHSort;
int cmpHSort;
int sumHSort;

int atrQSort;
int cmpQSort;
int sumQSort;

int heapSize;

void generareValori(int *a,int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
		a[i]=rand()%1000;
	}
}

int parinte(int i)
{
	return i/2;
}

int stanga(int i)
{
	return 2*i+1;
}

int dreapta(int i)
{
	
	return 2*i+2 ;
}

void heapify(int A[], int i)  
{
    int stanga,dreapta,max;
    int temp;

    stanga=2*i;
    dreapta=2*i+1;

    cmpHSort=cmpHSort+1;
    if (stanga<=heapSize && A[stanga]>A[i]) 
        max=stanga;	
    else
        max=i;

    cmpHSort=cmpHSort+1;
    if (dreapta<=heapSize && A[dreapta]>A[max])
        max=dreapta;
    if (max!=i) 
        {
            atrHSort+=3;
            temp=A[i];
            A[i]=A[max];
            A[max]=temp;
            heapify(A,max);  
        }
}

void heapBU(int A[],int n)
{
    int i;
	heapSize=n;
    for (i=heapSize/2;i>=1;i--) 
        heapify(A,i); 
}

void heapSort(int A[],int n)
{
    int k;
    int temp;
    heapBU(A,n);
    for (k=n;k>=2;k--)
    {
        atrHSort+=3;
        temp=A[1];
        A[1]=A[k];
        A[k]=temp;
        heapSize=heapSize-1;
        heapify(A,1);
    }
}

int partition(int A[],int p, int r,int elemPivot){

	int x,temp;
	int i,j;
	if(elemPivot==1)
		x=A[r];
	else if(elemPivot==2)
		x=A[(p+r)/2];
	else if(elemPivot==3)
		x=A[p+1];
	atrQSort++;
	i=p-1;
	for(j=p;j<=r-1;j++){
		cmpQSort++;
		if(A[j]<=x)
			{i++;
			atrQSort=atrQSort + 3;
			temp=A[i];
			A[i]=A[j];
			A[j]=temp;
		}
	}
	atrQSort=atrQSort+3;
	temp=A[i+1];
	A[i+1]=A[r];
	A[r]=temp;

	return i+1;
}

void quickSort(int A[],int p, int r,int elemPivot)
{	if ( p < r )
    {
        int q = partition(A, p, r,elemPivot); 
			quickSort(A, p, q-1,elemPivot);
			quickSort(A, q+1, r,elemPivot);
    }
}


void prettyPrint(int *a,int n,int i,int nivel)
{
	if(i<n){

	for(int j=0;j<nivel;j++)
		printf("\t");
	printf("%d\n",a[i]);
	prettyPrint(a,n,stanga(i),nivel++);
	prettyPrint(a,n,dreapta(i),nivel++);
	}
}


void copiereValori(int *a,int*b,int n)
{
	int i=0;
	for(i=0;i<n;i++)
	{
		a[i]=b[i];
	}

}

int main()
{
    int heap[10001];
    int quick[10001];
    int j,k,nr;
	int n=10;
    int cmpHSortTemp=0,cmpQSortTemp=0,atrHSortTemp=0,atrQSortTemp=0,sumHSortTemp=0,sumQSortTemp=0;
    FILE *avgHeap,*avgQuick,*bestQuick,*worstQuick;

    avgHeap=fopen("AverageHeap.csv","w");
	worstQuick=fopen("WorstCaseQuickSort.csv","w");
	avgQuick=fopen("AverageQuicksort.csv","w");
	bestQuick=fopen("BestCaseQuickSort.csv","w");
	
	/////////////////////////////////////heapSort
	int *a = (int *)malloc(n*sizeof(int));
	int *b = (int *)malloc(n*sizeof(int));
	

	generareValori(a,n);
	copiereValori(b,a,n);
	heapSort(a,10);

	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	printf("\n");
	/////////////////////////////////////////////

	atrQSort=cmpQSort=sumQSort=0;

	quickSort(b,1,10,1);
	for(int i=1;i<=10;i++)
		printf("%d ",a[i]);
	printf("%d,%d,%d\n",atrQSort,cmpQSort,atrQSort+cmpQSort);


	//average case
    for (int i=100;i<=10000;i=i+100)
	{
		cmpHSort=cmpQSort=atrHSort=atrQSort=0;

		for (j=1;j<=5;j++)
		{
			nr=0;
			cmpHSortTemp=cmpQSortTemp=atrHSortTemp=atrQSortTemp=0;

			for (k=1;k<=i;k++)
			{
				nr++;
				heap[nr]=quick[nr]=rand();
			}

			heapSort(heap,i);
			quickSort(quick,1,i,1);

			cmpHSortTemp+=cmpHSort;
			cmpQSortTemp+=cmpQSort;
			atrHSortTemp+=atrHSort;
			atrQSortTemp+=atrQSort;
		}
			fprintf(avgQuick,"%d,%d,%d,%d,%d\n",i,atrQSort,cmpQSort,atrQSort + cmpQSort );
			fprintf(avgHeap,"%d,%d,%d,%d,%d,%d,%d\n",i,atrHSortTemp/5,cmpHSortTemp/5,cmpHSortTemp/5+atrHSortTemp/5,atrQSortTemp/5,cmpQSortTemp/5,cmpQSortTemp/5+atrQSortTemp/5);
	}

	//best case
	for (int i=100;i<=10000;i=i+100)
	{
		cmpQSort=atrQSort=0;
		nr=1;
		for (j=1;j<=i;j++) 
		{
	
			quick[nr]=j;
			nr++;
		}
		quickSort(quick,1,i,2);
		fprintf(bestQuick,"%d,%d,%d,%d \n",i,atrQSort,cmpQSort,cmpQSort+atrQSort);
		
	}

	
	//worst case
	for (int i=100;i<=10000;i=i+100)
	{
		cmpQSort=atrQSort=0;
		nr=1;
		for (j=1;j<=i;j++) 
		{
			quick[nr]=j;
			nr++;
		}
		quickSort(quick,1,i,3);
		fprintf(worstQuick,"%d,%d,%d,%d\n",i,atrQSort,cmpQSort,cmpQSort+atrQSort);
		
	}
	
	
	fclose(avgHeap);
	fclose(avgQuick);
	fclose(bestQuick);
	fclose(worstQuick);
	return 0;

	
}
