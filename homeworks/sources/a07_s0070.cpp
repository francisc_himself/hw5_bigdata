/*
***ANONIM*** ***ANONIM*** - ***ANONIM***
Grupa ***GROUP_NUMBER***
Assignment No.7: Multi-way Trees Transformations between different representations
*/

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

//structura unui nod din arbore
typedef struct node{
    int key;	//cheia
    int nch;	//numar fii
    struct node *left, *right;	//stanga, dreapta
	struct node *child[100];	//pointer spre fiu
}NODE;

//structura unui nod din lista
typedef struct linkedList{
    NODE *listData;                 
    struct linkedList *next;
}linkedList;

linkedList *first, *last;
NODE *root1, *root2;

//Aceasta functie initializeaza un nod
NODE *initNode(int k){
    NODE *t;	

	t = (NODE *)malloc(sizeof(NODE));

    if (t == NULL){
        printf ("\n Eroare la alocarea memoriei! \n");
        return 0;
    }
	
    t->key = k;	//cheia lui nodului va fi egala cu k
    t->nch = 0;	//dupa ce un nod va fi creat, numarul copiilor lui va fi 0

    return t;	//returnez nodul
}

//Aceasta functie formeaza un arbore multicai
void multiWayTree(int father[], int n, NODE *root){
	NODE *son;
	int j;

	//cautam fii radacinii
	for (j = 1; j <= n; j++){
		if (father[j] == root->key){
			son = initNode(j);
			
			root->child[root->nch] = son;	//copilul radacinii va fi noul nod format
			root->nch++;					//cresc numarul de copii
		}
	}

	//apelez multiWayTree pentru fiecare copil al radacinii
	for (j = 0; j < root->nch; j++){
		multiWayTree(father, n, root->child[j]);
	}
}

void Transform1(int father[], int n)
{
    int root = -1;
	int j;

	//cautam radacina
    for (j = 1; (j <= n) && (root == -1); j++){
        if (father[j] == -1){
            root = j;	//gasim radacina
		}
	}
    
	root1 = initNode(root);		//cream un nou nod cu cheia radacinii

	//printf("\nRadacina este %d \n", root1->key);
	
	multiWayTree(father, n, root1);	//construiesc arborele multiway pentru radacina gasita
}

//Punem in coada
int ENQUEUE(NODE *t){
    linkedList *x;
    
	x = (linkedList *)malloc(sizeof(linkedList));
    
	if (x == NULL){
        printf ("\n Eroare la alocarea memoriei! \n");
        return 0;
    }

    x->listData = t;	//nodul din lista = t
    x->next = 0;		//urmatorul este 0
    
	if (first == 0){
        first = x;		//daca lista e vida punem pe x ca primul nod in lista
        last = x;		
    }
    else{				//daca lista nu e vida
        last->next = x;	//il adaugam pe x la sfarsitul listei
        last = x;
    }
    
	return 1;
}

//Scoatem primul nod din coada
NODE *DEQUEUE(){
    linkedList *x;
    NODE *y;
    
	if(first == 0){
        return 0;	//daca lista e vida returnam 0
	}

    x = first;		
    first = first->next;
    
	if (first == 0){
        last = 0;
	}

    y = x->listData;	//salvam continutul din x in y
    
	free (x);			//eliberam memoria alocata pentru x
    
	return y;			//returnam pe y
}

int printT1(NODE* root){
    NODE *t;
    int j;

    first = 0;
    last = 0;

    //daca nu se poate pune radacina in coada returnez 0
	if ((ENQUEUE(root)) == 0){
        return 0;
	}

    do{
        t = DEQUEUE();			//scot primul nod din coada
        if (t!=0){              //daca nu e null
            printf ("%d ",t->key);//ii afisez cheia

            for (j=0; j < t->nch; j++){
                if ((ENQUEUE(t->child[j])) == 0){ 
                    return 0;
				}
			}
        }
    }while (t != 0); 

    return 1;
}

void binaryTree(NODE *root1, NODE *root2){
	int j;
	NODE *t;

	//fiul stang
	if (root2->left != NULL){
		if (root1->child[0]->nch >= 1){
			//setam fiul pentru fiul nodului
			root2->left->left = root1->child[0]->child[0];
		}
		else{
			//nu avem fiu
			root2->left->left = NULL;
		}
	
		t = root2->left;
		//setam fratii primului fiu al nodului
		for (j = 1; j < root1->nch; j++){	
			t->right = root1->child[j];
			t = t->right;
		}

		t->right = NULL;
	}

	//pentru fratele drept
	if (root2->right != NULL){
		if (root2->right->nch >= 1){
			//setam fiul fratelui
			root2->right->left = root2->right->child[0];
		}
		else{
			//fratele nu are fiu
			root2->right->left = NULL;
		}
	}

	if (root2->left != NULL){
		binaryTree(root1->child[0], root2->left);
	}

	if (root2->right != NULL){
		binaryTree(root2->right, root2->right);
	}
}

//Transformarea din reprezentarea multi-cai in reprezentarea binara
void Transform2()
{
	//root2 va contine reprezentarea binara
	root2 = root1;

	if(root1->nch != 0){
		//setam primul fiu al radacinii = 2
		root2->left = root1->child[0];
	}
	else{
		root2->left = NULL;
	}

	//radacina nu are frate
	root2->right = NULL;

	binaryTree(root2, root1);
}

void printT2(NODE *root2, int s){
	int ss = s;

	if (root2 != NULL){
		
		//la inceput nu afisez niciun spatiu, ci pe parcurs ce creste ss
		for (int j = 1; j <= s; j++){
			printf (" ");
		}
		
		printf ("%d\n", root2->key);	//afisez radacina

		if(root2->left != NULL){
			//ss++;	//daca am fiu in stanga radacinii cresc ss si apelez recursiv functia
		}

		printT2(root2->left, ss+1);

		if(root2->right == NULL){
			//ss = ss--;	//daca nu am frate scade ss si apelez recursiv functia
		}

		printT2(root2->right, ss);
	}
}

int main(){

	int nodes[100], dim;
	printf ("Nr de noduri = ");
    scanf ("%d",&dim);

	for (int i = 1; i <= dim; i++){
		printf ("nod[%d]=", i);
		scanf ("%d", &nodes[i]);
	}

	printf ("\nArray-ul cu parinti: ");
	for (int i = 1; i <= dim; i++){
		printf ("%d ", nodes[i]);
	}
	
	printf ("\n");

    Transform1(nodes, dim);
	printf ("\nTransformarea intai: ");
	printT1(root1);

	Transform2();
	printf ("\n\nTransformarea a doua: \n");
	printT2(root2, 0);

	getche ();
	return 0;
}