#include <iostream>
#include <stdio.h>
#include <conio.h>


typedef struct tip_nod{
	int cheie;				//informatia
	struct tip_nod *stg,*dr;
	}TIP_NOD;

TIP_NOD *rad;			//radacina arborelui variabila globala

//functia de inserare recursiva a nodurilor
TIP_NOD *inserare_rec (TIP_NOD *rad, int key){
	TIP_NOD *p;
	int n;
	if ( rad == 0 )
	{
		n = sizeof(TIP_NOD);
		p = (TIP_NOD*)malloc(n);
		p->cheie = key;
		p->stg = 0;
		p->dr = 0;
		return p;
	}
	else
	{
		if ( key < rad->cheie )
			{
			rad->stg = inserare_rec (rad->stg, key);
			}
		else
			{
				if ( key > rad->cheie )
				{
				rad->dr = inserare_rec (rad->dr, key);
				}
			else
				{
				//cheie dubla
				printf ("\n Mai exista un nod de cheie %d", key);
				}
			}
	}
	return rad;
}

TIP_NOD *cautare (TIP_NOD *rad, int key){	//cautarea unui nod de cheie key
	TIP_NOD *p;
	if ( rad == 0 )
		return 0;					//arborele este vid
	while ( p != 0 )
	{
		if ( p->cheie == key )
			return p;				//s-a gasit nodul
		else if ( key < p->cheie )
			p = p->stg;
		else
			p = p->dr;
	}
	return 0;						//nu exista nod de cheie key
}

void afisare (TIP_NOD *p, int nivel){
	int i;
	if ( p!= 0)
	{
		afisare( p->dr, nivel+1);
		for( i = 0; i <= nivel; i++) printf("    ");
		printf ("%2d \n", p->cheie);
		afisare (p->stg, nivel+1);
	}
}

TIP_NOD *sterge_nod (TIP_NOD *rad, int key){	//stergerea unui nod de cheie key
	TIP_NOD *p, *tata_p;			//p nodul de sters, iar tata_p este tatal lui
	TIP_NOD *nod_inlocuire, *tata_nod_inlocuire;
	int directie;					//stg=1 dr=2
	if ( rad == 0 ) return 0;		//arbore vid
	p = rad;
	tata_p = 0;
	while (( p != 0 ) && ( p->cheie != key ))
	{
		if ( key < p->cheie )
		{
			tata_p = p;
			p = p->stg;
			directie = 1;
		}
		else
		{
			tata_p = p;
			p = p->dr;
			directie = 2;
		}
	}
	if ( p == 0 )
	{
		printf ("nu exista nod cu cheia %d", key);
		return rad;
	}

	if ( p->stg == 0) nod_inlocuire = p->dr;		//nodul p nu are fiu stg
	else if ( p->dr == 0) nod_inlocuire = p->stg;	//nodul p nu are fiu dr
		else
		{
			//nodul p are si fiu stg si fiu dr
			tata_nod_inlocuire = p;
			nod_inlocuire = p->dr;
			//se cauta in subarbore dr
			while ( nod_inlocuire->stg != 0 )
				{
					tata_nod_inlocuire = nod_inlocuire;
					nod_inlocuire = nod_inlocuire->stg;
				}
			if ( tata_nod_inlocuire != p )
				{
					tata_nod_inlocuire->stg = nod_inlocuire->dr;
					nod_inlocuire->dr = p->dr;
				}
			nod_inlocuire->stg = p->stg;
		}
	free(p);
	printf("\n nodul de cheie %d a fost sters\n", key);
	if (tata_p == 0) return nod_inlocuire;		//s-a sters chiar radacina
	else
		{
			if (directie == 1)
				tata_p->stg = nod_inlocuire;
			else
				tata_p->dr = nod_inlocuire;
			return rad;
	}
}


void main(){
	TIP_NOD *p;
	int i;
	int key;
	printf ("\n Inserare recursiva");
	//printf ("\n Numar total de noduri=");
	//scanf ("%d", &n);
	rad = 0;
	for ( i = 1; i < 4; i++)
	{
		printf ("\n cheia nodului%d= ",i);
		scanf ("%d", &key);
		rad = inserare_rec (rad, key);
	
	
	
	}
	printf("\n AFISARE: \n");
	afisare(rad,0);


	rad = sterge_nod(rad, key);
	afisare(rad,0);
}