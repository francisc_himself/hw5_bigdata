/****ANONIM*** ***ANONIM***
Grupa: ***GROUP_NUMBER***
Tema: Permutarea Josephus
Eficienta algoritmilor este O(n*logn)
*/


#include<iostream>
#include<conio.h>
#include<fstream>
using namespace std;
ofstream fout("date.csv");
struct elem
{
	int ch, dim;
	elem *st, *dr, *p;
};
elem * root;
int i, op=0;

void inserare(elem **root, int nr, elem* parinte)
{
	if(*root==NULL)
	{
		*root=(elem*) malloc(sizeof(elem));
		(*root)->ch=nr;
		(*root)->dim=1;
		(*root)->st=NULL;
		(*root)->dr=NULL;
		(*root)->p=parinte;
	}
	else
	{
		if(nr<(*root)->ch)
		{
			inserare(&(*root)->st, nr, *root);
		}
		else 
			if(nr>(*root)->ch)
			{
				inserare(&(*root)->dr, nr,*root);
			}
		(*root)->dim=1;
		if((*root)->st != NULL)
			(*root)->dim+=(*root)->st->dim;
		if((*root)->dr != NULL)
			(*root)->dim+=(*root)->dr->dim;
		}
}


void BUILD_TREE(elem **root, int sti,  int dri)
{
		int mj=(sti+dri)/2;
		inserare(root, mj, NULL);
		if(sti<mj) 
			BUILD_TREE(root, sti, mj);
		if(mj+1<=dri)
			BUILD_TREE(root, mj+1, dri);	
}


elem *select(elem *x, int i)
{
	int r;//dimensiunea nodului fiu stanga;
	op++;
	if(x->st!=NULL)
		r=(x->st)->dim+1;
	else
		r=1;
	op++;
	if (i < r)
    {
        select (x->st, i);
    }
    else if (i > r)
    {
        select (x->dr, i-r);
    }
    else 
        return x;
}

elem* tree_minim(elem* x)
{
	op++;
	while((*x).st!=NULL)
	{
		op++;
		x=(*x).st;
	}
	return x;

}

elem* tree_succesor(elem *x)
{
	op++;
	if((*x).dr!=NULL)
		return tree_minim((*x).dr);
	elem* y=(*x).p;
	op++;
	while(y!=NULL && x==(*y).dr)
	{
		x=y;
		y=(*x).p;
	}
	return y;
}
void OS_Delete( elem **root, elem *z)
{
	elem *x, *y;
	op=op+2;
	if((*z).st==NULL || (*z).dr==NULL)
	{
		y=z;
	}
	else y=tree_succesor(z);

	elem *aux =y;
	op++;
	while((*aux).p!=NULL)
	{
		op++;
		(*(*aux).p).dim--;
		op++;
		aux=(*aux).p;
	}
	op++;
	if((*y).st!=NULL)
		x=(*y).st;
	else x=(*y).dr;

	if(x!= NULL)
	{
		(*x).p=(*y).p;
		op++;
	}
	op++;
	if((*y).p==NULL)
	{
		*root=x;
		op++;
	}
	else
	{
		op++;
		if(y==(*(*y).p).st)
		{
			(*(*y).p).st=x;
			op++;
		}
		else
		{
			(*(*y).p).dr=x;
			op++;
		}
	}
	if(y!=z)
	{
		(*z).ch=(*y).ch;
		op++;
	}

	free(y);
}

void pretty_print(elem *root, int i)
{
    int j;
	if(root!=NULL)
	{
		if(root->st != NULL)
			pretty_print(root->st, i+1 );
		for(j = i; j > 0; j--)
			cout<<"    ";
		cout<<root->ch<<"-"<<root->dim<<endl;
		if(root->dr != NULL)
			pretty_print(root->dr, i+1);
	}
}

void inordine(elem *root)
{
	if(root!=NULL)
	{
		if(root->st!=NULL)
			inordine(root->st);
		cout<<root->ch<<" ";
		if(root->dr!=NULL)
			inordine(root->dr);
	}
}

void josephus(int n, int m)
{
	elem * x=new elem;
	
	int i=m;
	BUILD_TREE(&root, 1, n);
	inordine(root);
	cout<<endl;
	pretty_print(root,10);

	op++;
	while(root!=NULL)
	{
		x=select(root,i);
		cout<<"Nodul "<<x->ch<<" a fost sters "<<endl;
		OS_Delete(&root,x);
		inordine(root);
		cout<<endl;
		pretty_print(root,10);
		n--;
		if(n!=0)
			i=((i+m-2)%n)+1;
		
	}
}





/////////////////////////DEMO/////////////////////
void main()
{
	int n=7, m=3;
	josephus(n,m);
	getch();
}

///////////////////////////////////////////
/*void main()
{
	int n, m,i;
	elem *root=new elem;
	fout<<"n;op"<<endl;
	for(n=100;n<=10000;n=n+100)
	{
		op=0;
		m=n/2;
	    i=1;
		root=NULL;
		josephus(n,m);
		fout<<n<<";"<<op<<endl;
	}
	
	fout.close();
}*/

