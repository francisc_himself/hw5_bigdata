#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define V 10000	// variabila de contor pentru numarul de apeluri
int M = 0;		// nodul unui arbore de set disjunct

typedef struct padure
{
    int value;
    struct padure *parent;
    int rank;	//nr de muchii al celui mai lung drum de la x la o frunza descendenta
} p;

// structura unui graf
typedef struct graph_t
{
	int muchii;
	int noduri;
	int *vertex;
	int **edge;
} GRAPH;

/*void print(GRAPH *g)
{
	p *p;
	printf("muchii: \n");
	for (int i=1; i<=g->muchii; i++)
	{
		printf("%d: ", i);
		while (p!=NULL)
		{
			printf(" %d ", p->value);
			p=p->parent;
		}
		printf("\n");
	}
}*/

// formeaza o multime disjuncta
p *MAKE_SET(int value) 
{
	M++; // numar apeluri

	p *x = (p *) malloc(sizeof(p));

	x->value = value;
	x->parent = x;
	x->rank = 0;
    
	return x;
}

// gaseste multime disjuncta
p *FIND_SET(p *x)
{
	M++; // numar apeluri

	if (x != x->parent)
	{
		x->parent = FIND_SET(x->parent);
	}

	return x->parent;
}

//transformam radacina cu rang mai mare in parinte al radacinii cu rang mai mic
void LINK(p *x, p *y)
{
	if (x->rank > y->rank) 
	{
		y->parent = x;
	} 
	else
	{
		x->parent = y;

		if (x->rank == y->rank)
		{
			y->rank++;
		}
	}
}


// reuneste doua multimi disjunce
void UNION(p *x, p *y)
{
	M++; // numar apeluri

	LINK(FIND_SET(x), FIND_SET(y));
}




// gaseste componentele conexe al unui graf
void CONNECTED_COMPONENTS(GRAPH *g, p **set)
{
	for (int i = 0; i < g->muchii; i++)
	{
		set[i] = MAKE_SET(g->vertex[i]);
	}

	for (int i = 0; i < g->noduri; i++)
	{
		p *u = set[g->edge[i][0] - 1];
		p *v = set[g->edge[i][1] - 1];

		if (FIND_SET(u) != FIND_SET(v))
		{
			UNION(u, v);
		}
	}
}

// verifica daca doua noduri apartin aceleiasi componente conexe din graf
bool SAME_COMPONENT(p *u, p *v)
{
	if (FIND_SET(u) == FIND_SET(v))
	{
		return true;
	}
	else
	{
		return false;
	}
}

int main() 
{
	FILE *f = fopen("data.txt", "w");
	srand(time(NULL));
	
	p *set[V];
	GRAPH g;

	g.noduri = 10000;
	g.vertex = (int *) malloc(sizeof(int) * g.muchii);
	for (int i = 0; i < g.muchii; i++)
	{
		g.vertex[i] = i + 1;
	}

	for (int nrEdge = 10000; nrEdge <= 60000; nrEdge += 5000)
	{
		M = 0;

		g.noduri = nrEdge;
		g.edge = (int **) malloc(sizeof(int) * g.noduri);
		for (int i = 0; i < g.noduri; i++)
		{
			g.edge[i] = (int *) malloc(sizeof(int) * 2);
		}

		for (int i = 0; i < g.noduri; i++)
		{
			g.edge[i][0] = rand() % 10000 + 1;
			g.edge[i][1] = rand() % 10000 + 1;
		}

		CONNECTED_COMPONENTS(&g, set);
		
		fprintf(f, "%-10d\t%-10d\n", g.noduri, M);

		for (int i = 0; i < g.noduri; i++)
		{
			free(g.edge[i]);
		}

		free(g.edge);
	}
	
	fclose(f);

	system("pause");
    return 0;
}

