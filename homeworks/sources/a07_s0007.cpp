/*
***ANONIM*** Vasile-***ANONIM***, grupa ***GROUP_NUMBER***
Functia de transformare1(), care are ca parametru un sir de elemente si genereaza un arbore multi-way 
este de eficienta O(n)-unde n este numarul de noduri din arbore-, iar functia transformare2() 
ce creeaza arborele binar corespunzator arborelui multi-way dat ca parametru are eficienta tot O(n).
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

#define MIN 100
#define MAX 10001
#define error -1

typedef struct MultiNode{
	int key;
	int count;
	struct MultiNode *child[50];
}MN;

typedef struct BinTree{
	int key;
	struct BinTree *st,*dr;
}BT;

MN *createMN(int key)
{
	MN *nod=(MN *)malloc(sizeof(MN));
	nod->key=key;
	nod->count=0;
	return nod;
}


void insertMN(MN *parent, MN *child)
{
	parent->child[parent->count++]=child;
}

MN *transformare1(int t[], int size)
{
	MN *root;
	MN *nodes[50];
	for(int i=0;i<size;i++)
		nodes[i]=createMN(i);
	for(int i=0;i<size;i++){
		if(t[i]!=-1){
			insertMN(nodes[t[i]],nodes[i]);
		}
		else
			root=nodes[i];		
}
	return root;
}

void introd_frate(BT *nod,BT *nd)
{
	nod->dr=nd;
}


BT *transformare2(MN *root)
{
	BT *rad=(BT*)malloc(sizeof(BT));
	rad->key=root->key;
	rad->st=NULL;
	rad->dr=NULL;
	if(root->count!=0)
	{
		rad->st=transformare2(root->child[0]);
		BT *aux=rad->st;
		for(int i=1;i<root->count;i++)
		{
			aux->dr=transformare2(root->child[i]);
			aux=aux->dr;
		}
	}
	return rad;
}

void prettyPrint(MN *root, int nivel=0)
{
	for(int i=0;i<nivel;i++)
		printf("  ");
	printf("%d\n",root->key);
	for(int i=0;i<root->count;i++)
		prettyPrint(root->child[i],nivel+1);
}

void prettyPrint2(BT *root, int nivel=0)
{
	if(root!=NULL)
	{
		for(int i=0;i<nivel;i++)
		printf("  ");
		printf("%d\n",root->key);
		prettyPrint2(root->st,nivel+1);
		prettyPrint2(root->dr,nivel);
	}
}

int main()
{
	int t[]={9,5,5,9,-1,4,4,5,5,4,2};
	int size=sizeof(t)/sizeof(t[0]);
	printf("MULTIWAY:\n");
	MN *root=transformare1(t,size);
	prettyPrint(root,0);
	printf("BINAR:\n");
	BT *rad=transformare2(root);
	prettyPrint2(rad);
	system("pause");
	return 0;
}