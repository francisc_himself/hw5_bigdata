#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <TIME.H>
#include <dos.h>
// Permutarea Josephus are ca prim obiectiv scoaterea din "m" in "m" a cate un
// nod din ***ANONIM***e "n" noduri ***ANONIM*** arborelui binar. Scoaterea se face in functie 
// de ordinul a***ANONIM***ui element.
//				1 				| Daca m = 2, ordinea elementelor scoase va fi:
//			8		2 			| ->2->4->6->8->3->7->5->1
//		7				3		|
//			6		4 			|
//				5				|
// Se observa ca permutarea Josephus, prin folosirea unui arbore binar, are ca
// si eficienta n*log(n).

//structura unui nod al unui arbore binar
typedef struct NOD{
	int dimSubArb;
	int key;
	struct NOD *parinte;
	struct NOD *st;
	struct NOD *dr;
}nod;
//numarul de atribuiri si de comparatii
int nrAtrib,nrCmp;
//arborele ce este manevrat in aceasta aplicatie
NOD *tree;
//fisierul in care se scrie
FILE *f;

//initializarea arborelui binar
void initTree(nod **tree){
	*tree  = (nod*)malloc(sizeof(nod));
	*tree = NULL;
}

//cautarea succesorului unui fiu, bazandu-se pe principiul ca
//succesorul unui nod va fi ***ANONIM*** mai din stanga fiu al fiului drept
NOD* successor(NOD *arb){ 
	NOD *y;
	nrCmp++;
	if (arb->dr!=NULL){
		arb=arb->dr;
		while (arb->st!=NULL){
			nrAtrib++;
			arb=arb->st;
		}
		return arb;
	}
	nrAtrib++;
	y=arb->parinte;
	while ( (y!=NULL) && (arb==y->dr) ){
		nrAtrib+=2;
		arb=y;
		y=y->parinte;
	}
	return y;
}

//inserarea unui nod in arborele binar
void insert(nod **tree, nod *item){	
	nrCmp++;
	if((*tree)==NULL){
		nrAtrib++;
		(*tree) = item;
//		if((*tree)->parinte!=NULL)
//			printf("\nkey = %d, dim = %d, tatal = %d",(*tree)->key,(*tree)->dimSubArb,(*tree)->parinte->key);
//		else
//			printf("\nkey = %d, dim = %d, tatal = %d",(*tree)->key,(*tree)->dimSubArb,0);
//		printf("\nnrAtrib = %d nrCmp = %d",nrAtrib,nrCmp);
		return;
	}
	nrCmp++;
	if(item->key<(*tree)->key){
		insert(&(*tree)->st, item);
	}else if(item->key>(*tree)->key){
		insert(&(*tree)->dr, item);
	}

}

//crearea unui nou nod
nod *creareNod(int i,int j,NOD *par){
	if(i<=j){
		nod *curItem;
		curItem  = (nod*)malloc(sizeof(nod));
		curItem->key = (i+j)/2;
		curItem->dimSubArb = j-i+1;
		curItem->parinte = par;
		curItem->dr  = NULL;
		curItem->st  = NULL;
		return curItem;
	}
	return NULL;
}

//construirea arborelui binar
void buildTree(int n,int m,nod *p){
	
	if(n<=m){
		nod *curItem;
		int i = n;
		int j = m;

		curItem = creareNod(i,j,p);
		insert(&tree,curItem);
		buildTree(i,(i+j)/2-1,curItem);
		buildTree((i+j)/2+1,j,curItem);
		
	}
}

//selecteaza un nod in functie de ordinul sau.
//in cazul nostru, ordinul se va determina in functie 
//de inaltimea subarborelui nodului respectiv.
nod *osSelect(nod *tree, int i){
	int r;
	nrCmp++;
	if(tree->st!=NULL){
		r= tree->st->dimSubArb+1;
	}else{
		r=1;
	}
	if(i==r){
		return tree;
	}else if(i<r){
			return osSelect(tree->st,i);
		}else{
			return osSelect(tree->dr,i-r);
	}
}

//scaderea cu cate o unitate a fiecarei dimensiuni a subarborelui unui nod
void shortensTree(NOD *z)
{ while (z!=NULL)
	{
		nrAtrib=nrAtrib+2;
		z->dimSubArb--;
		z=z->parinte;
	}
}

//stergerea unui nod
void osDelete(nod *item){
	NOD *y,*x;
	nrCmp=nrCmp+5;
	if ( (item->st==NULL) || (item->dr==NULL) ){ 
		y=item; 
		nrAtrib++;
     }else{
		 y=successor(item); 
		 nrAtrib++; 
	}
	if (y->st!=NULL){ 
		x=y->st; 
		nrAtrib++; 
	}else{	 
		x=y->dr; 
		nrAtrib++; 
	}
	if (x!=NULL){   
		nrAtrib++;
		x->parinte=y->parinte;
    }
	shortensTree(y->parinte);
	if (y->parinte==NULL){
		tree=x;
	}else{
		if (y==y->parinte->st){ 
			y->parinte->st=x; 
			nrAtrib++;
		}else{ 
			y->parinte->dr=x; 
			nrAtrib++; 
		}
	}
	if (y!=item){   
		nrAtrib++;
		item->key=y->key;
     }
	free(y);
}

//afisarea prietenoasa
void prettyPrint(nod *tree){
	
	if((tree->st)!=NULL){
		prettyPrint(tree->st);
	}
	int i;
	for(i=0;i<tree->dimSubArb;i++){
		printf(" ");
	}
	if (i>1){
		printf(">");
	}
	printf("%d",tree->key);
	if(tree->parinte!=NULL){
		for(i=0;i<(tree->parinte->dimSubArb-tree->dimSubArb-2);i++){
			printf("-");
		}	
	}
	printf("\n");
	
	if(tree->dr != NULL){
		prettyPrint(tree->dr);
	}
	
}

//afisarea cu prettyPrint a unui arbore
void printOut(nod *tree){
	printf("\n\nPrettyPrint\n");
	prettyPrint(tree);
}

//algoritmul permutarii Josephus, cu tot cu scoaterea elementelor din arbore
void josephus(int n, int m,int type){
	initTree(&tree);
	buildTree(1,n,NULL);
	if(type==0)
		printOut(tree);
	int j=1;
	for(int k=n;k>1;k--){
		j = (( j + m - 2) % k) + 1;
		nod *itemD = osSelect(tree,j);
		if(type==0)
			printf("\nElementul ce va fi sters\n	-->%d", itemD->key);
		osDelete(itemD);
		if(type==0)
			printOut(tree);
	
	}
}

//programul principal
void main(){
	printf("Start Aplicatie!\n");
	//Evaluation
	printf("\nStart Evaluation!\n");
	f = fopen("josephus.csv","w");
	fprintf(f,"n,nrAtribuiri,nrComparatii,nrOperatii\n");
	for(int n=100;n<=10000;n+=100){
		if(n%3==1){
			printf(".  %2.0d",n/100);
			printf("\b\b\b\b\b");
			_sleep(100);
		}
		if(n%3==2){
			printf(".. %2.0d",n/100);
			printf("\b\b\b\b\b");
			_sleep(100);
		}
		if(n%3==0){
			printf("...%2.0d",n/100);
			printf("\b\b\b\b\b");
			_sleep(100);
		}
		
		nrAtrib=0;
		nrCmp=0;
		int m=n/10;
		josephus(n,m,1);
		fprintf(f,"%d,%d,%d,%d\n",n,nrAtrib,nrCmp,nrAtrib+nrCmp);
	}
	fclose(f);
	printf("\nEnd Evaluation!");
	//Demo
	printf("\nStart Demo!");
	nrAtrib=0;
	nrCmp=0;
	initTree(&tree);
	nod *rad = tree;
	josephus(8,2,0);
	printf("\nnrAtrib = %d nrCmp = %d",nrAtrib,nrCmp);
	printf("\nEnd Demo!");
	printf("\nStop aplicatie!");
	getch();
}