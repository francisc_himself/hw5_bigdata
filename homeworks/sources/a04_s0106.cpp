#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
/*
->an O(nlogk) method for merging k sorted sequences, where n is the total number of elements(we use heapify),we use lists to rep the sequences.
->using heapify we get O(logk) running time to construct the heap instead of O(k) without using it.
->after we apply hepify in the elements on the first position in the array,constructing a min heap we take the root of the heap 
i.e the min element and add it in the final list
->when one list has no more elements we decrement the size of the heap by 1
-> both k and n may vary, we will make each analysis in turn:
for k constant:
   For k1= 5 sorted lists generated randomly the algorithm takes between 849-85601 total nr. of operations
   For k2=10 sorted lists generated randomly the algorithm takes between 1217-125408 total nr. of operations
   For k3=100 sorted lists generated randomly the algorithm takes between 2744-275742 total nr. of operations
for n constant: n = 10.000; the value of k must vary between 10 and 500 with an increment of 10;
->the total number of operations performed in this case are in the range 92052-348224
*/
typedef struct node
{
	long data;
	struct node *next;
}Node;
/*
struct liste
{
    Node *prim;
    Node *ultim;
};*/
Node *lists[1001];
Node *verif[10],*result[10001];
long op=0;
int k,n;

void heapify(Node *a[], int i, int n)
{
   int l,r,min;
   long aux;

   l=2*i;
   r=2*i+1;

   op++;
   if (l<=n && a[l]->data<a[i]->data)
       min=l;
   else
       min=i;

   op++;
   if (r<=n && a[r]->data<a[min]->data)
       min=r;
   
	if (min!=i)
       {
           op+=3;
           a[0]=a[i];
           a[i]=a[min];
			a[min]=a[0];
           heapify(a,min,n);
       }
}

void bh_bottomup(Node *a[],int n)
{
   int i;
   for (i=n/2;i>=1;i--)
       heapify(a,i,n);
}

int main()
{
   FILE *f,*g,*h,*e;
   f=fopen("k1.txt","w");
   g=fopen("k2.txt","w");
   h=fopen("k3.txt","w");
   e=fopen("n.txt","w");
   int i,j,l,m,size,nr,x;
   long opaux;
   srand(time(NULL));
   //we verify for k=4 and n=20
 
	k=4;
	n=20;
	for (i=1;i<=k;i++)
		{
			verif[i]=new Node;
			verif[i]->data=rand() % (k*3);
			verif[i]->next=NULL;
			Node *aux=new Node;
			aux=verif[i];
			printf("%d ",verif[i]->data);
	for (j=2;j<=n/k;j++)
		{
		Node *q=new Node;
		q->data=aux->data+rand() % (k*3);
		q->next=NULL;
		aux->next=q;
		aux=q;
		printf("%d ",q->data); 
		}
		printf("\n");
	}
		size=k;
		nr=1;
		bh_bottomup(verif,size);

	while (size!=0)
		{
		result[nr]=new Node;
		result[nr]->data=verif[1]->data;
		Node *p;
		p=verif[1];
		verif[1]=verif[1]->next;
		delete(p);
		result[nr]->next=NULL;
		nr++;
			if (verif[1]==NULL)
				{
				verif[1]=verif[size];
				size--;
				}
			heapify(verif,1,size);
			}
// print
for(j=1;j<=n;j++)
printf("%d ",result[j]->data);

int knr[]={5,10,100};
//for k constant
for(x=0;x<3;x++)
{k=knr[x];

   for (i=100;i<=10000;i=i+100)
   {
       opaux=0;

		for (j=1;j<=5;j++)
       {
           op=0;

           for (l=1;l<=k;l++)
           {
               lists[l]=new Node;
               lists[l]->data=rand() % (k * 3);
               lists[l]->next=NULL;
               Node *aux=new Node;
				aux=lists[l];
               for (m=2;m<=i/k;m++)
               {
                   Node *q=new Node;
                   q->data=aux->data+rand() % (k * 3);
                   q->next=NULL;
                   aux->next=q;
                   aux=q;
               }
           }

           size=k;
           nr=1;

           bh_bottomup(lists,size);

           while (size!=0)
           {
			result[nr]=new Node;
			result[nr]->data=lists[1]->data;
			Node *p;
			p=lists[1];
			lists[1]=lists[1]->next;
			delete(p);
			result[nr]->next=NULL;
			nr++;
			if (lists[1]==NULL)
				{
				lists[1]=lists[size];
				size--;
				}
			heapify(lists,1,size);
               
           }

            opaux+=op;

       }
		if(x==0)
			fprintf(f,"%d %ld\n",i,opaux/5);
		if(x==1)
			fprintf(g,"%d %ld\n",i,opaux/5);
		if(x==2)
			fprintf(h,"%d %ld\n",i,opaux/5);

   }
}
//for n constant
n=10000;
for (i=10;i<=500;i=i+10)
    {
        opaux=0;

		for (j=1;j<=5;j++)
        {
            op=0;

            for (l=1;l<=i;l++)
            {
                lists[l]=new Node;
                lists[l]->data=rand() % (l * 3);
                lists[l]->next=NULL;
                Node *aux=new Node;
				aux=lists[l];
                for (m=2;m<=n/i;m++)
                {
                    Node *q=new Node;
                    q->data=aux->data+rand() % (l * 3);
                    q->next=NULL;
                    aux->next=q;
                    aux=q;
                }
            }

            size=i;
            nr=0;

            bh_bottomup(lists,size);

            while (size!=0)
           {
			result[nr]=new Node;
			result[nr]->data=lists[1]->data;
			Node *p;
			p=lists[1];
			lists[1]=lists[1]->next;
			delete(p);
			result[nr]->next=NULL;
			nr++;
			if (lists[1]==NULL)
				{
				lists[1]=lists[size];
				size--;
				}
			heapify(lists,1,size);
               
           }
           

             opaux+=op;

        }
        fprintf(e,"%d %ld\n",i,opaux/5);
    }
fclose(f);
fclose(g);
fclose(h);
fclose(e);
//getch();
return 0;
}
