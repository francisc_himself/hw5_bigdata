#include<iostream>
#include<conio.h>
#include<fstream>

using namespace std;
int k, T[10000], m=9973, c1=30, c2=120; //m e dimensiunea tabelei
float alfa[5];//factorul de umplere
int y[3000]; //vector care tine minte cele 3000 de elemente care vor fi cautate in tabela

ofstream fout("date.csv");

int h(int k, int i)
{
	return (k%m+c1*i+c2*i*i)%m;
}

int dispersie_insereaza(int T[], int k)
{
	int i=0;
	do
	{
		int j=h(k,i);
		if(T[j]==-1)
		{
			T[j]=k;
			return j;
		}
		else
			i++;
	}
	while(i!=m);
	return -1;
}

int dispersie_cauta(int T[], int k, int  *accese)
{
	int i=0;
	int j;
	do
	{
		j=h(k,i);
		if(T[j]==k)
		{
			(*accese)=i+1;
			return j;
		}
		else
			i++;
	}
	while(T[j]!=NULL &&i!=m);
	(*accese)=i+1;
	return -1;
}

void main()
{
	int n; // nr de elemente de inserat astfel incat sa avem un anumit factor de umplere
	int accGasit=0;
	int accNegasit=0;
	int maxEffortG=0;
	int maxEffortN=0;
	int avgEffortG=0;
	int avgEffortN=0;
	int j;
	alfa[0]=0.8;
	alfa[1]=0.85;
	alfa[2]=0.9;
	alfa[3]=0.95;
	alfa[4]=0.99;
	for (int i=0;i<5;i++)
	{
		//accGasit=0;
		//accNegasit=0;
		maxEffortG=0;
		maxEffortN=0;
		//avgEffortG=0;
		//avgEffortN=0;
		n=m*alfa[i];
		
		for (j=0;j<m;j++)
			T[j]=-1;

		int nrDa=0; //nr de numere pe care le putem gasi, aprox 1500

		for ( j=0;j<n;j++) //se populeaza toata tabela
		{
			int rnd=rand()%m+1;
			dispersie_insereaza(T, rnd);
			if (j%7==0)  
			{
				y[nrDa]=rnd; //tine minte tot al 7-lea element din tabela
				nrDa++;
			}
		}

		for (j=nrDa;j<3000;j++)
		{
			int rnd=rand()*m+1+10000;
			y[j]=rnd;	
		}

		for (j=0;j<nrDa;j++) //in prima jumatate a lui y vor fi ele aprox 1500 care vor fi gasite
		{
			dispersie_cauta(T,y[j],&accGasit);
			avgEffortG+=accGasit;
			if (accGasit>maxEffortG)
				maxEffortG=accGasit;
			accGasit=0;
		}
		
		for (j=nrDa;j<3000;j++) //in restul nu se vor gasi elementele cautate
		{
			dispersie_cauta(T,y[j],&accNegasit);
			avgEffortN+=accNegasit;
			if (accNegasit>maxEffortN)
				maxEffortN=accNegasit;
			accNegasit=0;
		}
		avgEffortN=avgEffortN/(3000-nrDa);
		fout<<alfa[i]<<";"<<avgEffortG/nrDa<<";"<<maxEffortG<<";"<<avgEffortN<<";"<<maxEffortN<<endl;
	}
	fout.close();

}
