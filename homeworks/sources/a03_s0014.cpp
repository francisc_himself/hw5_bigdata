#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int heapsize;
FILE *pf;

//genereaza un sir cu elemente aleator
int sirrandom(int n, int sir[])
{
	for (int i=0; i<n; i++)
	{
		sir[i]=rand()%250;
	}
	return 0;
}

//interschimb 2 elemente
void interschimb(int *x,int *y)
{
	int aux;
	aux=*x;
	*x=*y;
	*y=aux;
}

//patritionarea elementelor dupa pivot
int Partitonare(int sir[],int st, int dr, int *A, int *C)
{
	int x=sir[dr];
	int i=st-1;
	int a=0, c=0;
	for (int j=st; j<=dr-1; j++)
	{
		if (sir[j]<=x)
		{
			c++;
			i++;
			interschimb(&sir[i], &sir[j]);
			a+=3;
		}
	}
	interschimb(&sir[i+1], &sir[dr]);
	a+=3;
	*A+=a;
	*C+=c;
	return i+1;
}

//sortarea sirului
void Quicksort(int sir[], int st, int dr, int *A, int *C)
{
	int a=0, c=0;
	int key;
	if (st<dr)
	{
		key=Partitonare(sir, st, dr, &a, &c);
		Quicksort(sir, st, key-1, &a, &c);
		Quicksort(sir, key+1, dr, &a, &c);
	}
	*A+=a;
	*C+=c;
}

int Parinte(int i)
{ return i/2; }

int Stanga(int i)
{ return 2*i; }

int Dreapta(int i)
{ return 2*i+1; }

//initializeaza heapul O(1)
void H_init(int sir[])
{
	heapsize=0;
}

//reconstruiesc heapul astfel incat cel mai mare element
//va fi radacina heapului
void MaxHeapify(int sir[], int i, int *A, int *C)
{
	int l=Stanga(i);
	int r=Dreapta(i);
	int a=0, c=0;
	int max;
	
	c++;
	if ((l<=heapsize) && (sir[l]>sir[i]))
	{max=l;}
	else {max=i;}
	c++;
	if ((r<=heapsize) && (sir[r]>sir[max]))
	{max=r;}
	if (max!=1)
	{
		interschimb(&sir[i], &sir[max]);
		a+=3;
		MaxHeapify(sir, max, &a, &c);
	}
	*A+=a;
	*C+=c;
}

//constructia heap bottom-up
void ConstrMaxHeap(int sir[], int lenght, int *A, int *C)
{
	int a=0, c=0;
	heapsize=lenght;
	for(int i=(lenght/2); i>=1; i--)
		MaxHeapify(sir, i, &a, &c);
	*A=a;
	*C=c;
}

//O(nlg n)
void HeapSort(int sir[], int *A, int *C)
{
	int a=0, c=0;
	ConstrMaxHeap(sir, heapsize, &a, &c);
	for (int i=heapsize; i>=1; i--)
	{
		interschimb(&sir[i], &sir[1]);
		a+=3;
		heapsize=heapsize-1;
		ConstrMaxHeap(sir, heapsize, &a, &c);
	}
	*A+=a;
	*C+=c;
}

void main()
{
	int sir[10000], sir1[10000];
	int A=0, C=0;

	srand(time(NULL));
	H_init(sir1);

	pf=fopen("quick- and heapsort.csv", "w");
	fprintf(pf, " i , Quick , Heap \n");
	for (int i=100; i<=10000; i+=100)
	{
		int ah=0,ch=0,aq=0,cq=0;
		for (int k=0; k<5; k++)
		{
			int aq=0,cq=0;
			A=C=0;
			sirrandom(i, sir);
			Quicksort(sir, 0, i-1, &A, &C);
			aq+=A;
			cq+=C;

			int ah=0,ch=0;
			A=C=0;
			ConstrMaxHeap(sir1, i, &A, &C);
			HeapSort(sir1, &A, &C);
			ah+=A;
			ch+=C;
		}
		fprintf(pf, " %d , %d , %d \n", i, (aq+cq)/5, (ah+ch)/5);
	}
	fclose(pf);
}