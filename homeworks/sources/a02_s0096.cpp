// Author: ***ANONIM*** ***ANONIM***
// Group: ***GROUP_NUMBER***

// You are required to implement correctly and efficiently two methods for building a heap,
// namely the bottom-up and the top-down strategies.

#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int aTD,cTD,aBU,cBU;
void buildMaxHeapBottomUp(int *x, int heapSize);
int* buildMaxHeapTopDown(int* x,int size);
void generateAverage(int*x, int n);
void demo();
void print(int *h, int poz, int level,int size);
void resetVariables();

// We can observe from the charts that the bottom up approach
// has a higher performance than the top down approach
void main(){
	demo();
	
	FILE *buFile;
	buFile = fopen("Result.csv", "w");
	if ( buFile==NULL ){
		perror("error opening files");
	}
	fprintf(buFile,"Size,BottomUp,TopDown");

	int *A, *AuxA;
	int size;
	for ( size=100; size<=10000; size+=100 ){
		printf("\n%d",size);
		A=(int*)malloc(sizeof(int)*size);
		AuxA=(int*)malloc(sizeof(int)*size);
		for(int i=0;i<5;i++){
		generateAverage(A,size);
		AuxA = buildMaxHeapTopDown(A,size);
		buildMaxHeapBottomUp(A,size);
		if(i==4)
		fprintf(buFile, "\n%d,%d,%d", size, (aBU + cBU)/5, (aTD + cTD)/5);

		}
		resetVariables();
	}
}

// proced that returns index of left child of a root
int left(int i){return 2*i+1;}
// proced that returns index ofright child of a root
int right(int i){return 2*i+2;}
// proced that returns index of parent of a child
int parent(int i){ return (i-1)/2;}
//proced that resets the comparison and assignment variables to zero
void resetVariables(){cBU=0;aBU=0;cTD=0;aTD=0;}

void maxHeapify(int *x, int heapSize, int i){

	int l=left(i),r=right(i),largest;
	cBU++;
	if(l<heapSize &&  x[l]>x[i])
		largest = l;
	else largest = i;
	cBU++;
	if(r<heapSize && x[r]>x[largest])
		largest = r;
	if(largest!=i){
		int aux=x[i];
		x[i]=x[largest];
		x[largest]=aux;
		aBU+=3;
		maxHeapify(x,heapSize,largest);
	}
}

void buildMaxHeapBottomUp(int *x, int heapSize){
	for(int i=heapSize/2-1;i>=0;i--)
		maxHeapify(x,heapSize,i);
}

void restoreHeap(int* y, int startingNode)
{
	int pI;
	if(startingNode==0)
		pI=0;
	else
		pI=(startingNode-1)/2;

	cTD++;
	if ( startingNode!=0 && y[startingNode] > y[pI])		// daca copilul e mai mare decat tatal
	{
		int aux = y[startingNode];
		y[startingNode] = y[pI];
		y[pI] = aux;
		aTD=aTD+3;
		// bubble the element up towards the root
		restoreHeap(y, pI);
	}	
}

void insert(int *y, int value, int *ySize) 
{	
	y[ySize[0]]=value;
	aTD++;
	ySize[0]++;
	restoreHeap(y, ySize[0]-1);
}

int* buildMaxHeapTopDown(int* x,int size)
{
	
	int *y=(int*)malloc(sizeof(int)*size);
	int *ySize=(int*)malloc(sizeof(int));
	ySize[0]=0;
	for ( int node=0; node<size; node++ )
	{
		insert(y, x[node], ySize);
	}
	return y;
}

void generateAverage(int *x, int n){
	int i=0;
	for(i=n-1;i>=0;i--)
		x[i]=rand();
}

void demo(){
	int* resTopDown,*A;
	int length=10;
	A=(int*)malloc(sizeof(int)*length);
	resTopDown=(int*)malloc(sizeof(int)*length);

	generateAverage(A,length);
	for(int i=0;i<length;i++){
		printf("%d ",A[i]);
	}
	printf("\nTop Down approach heap:\n");

	resTopDown=buildMaxHeapTopDown(A,length);
	for(int i=0;i<length;i++){
		printf("%d ",resTopDown[i]);
	}
	printf("\n");
	print(resTopDown,1,0,length);

	printf("\nBottom Up approach heap:\n");
	buildMaxHeapBottomUp(A,length);
	for(int i=0;i<length;i++){
		printf("%d ",A[i]);
	}
	printf("\n");
	print(A,1,0,length);

}


void print(int *h, int poz, int level,int size){
	if(poz <= size){
		print(h,2*poz+1,level+1,size);
		for(int i=1;i<=3*level;i++)
			printf(" ");
		printf("%d\n", h[poz-1]);
		print(h,2*poz,level+1,size);
	}
}