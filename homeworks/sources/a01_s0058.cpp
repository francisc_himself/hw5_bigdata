# include <conio.h>
# include <iostream>
# include "Profiler.h"
using namespace std;


Profiler profiler("demo");

void bubble(int *a, int n )
{
	int x;
	bool cond=false;
	while(!cond)
	{
		cond=true;
		for (int j=0;j<n-1;j++)
		{
			profiler.countOperation("bubble_comparare", n);
			if (a[j]>a[j+1])
			{
				profiler.countOperation("bubble_asignare",n, 3);
				x=a[j];
				a[j]=a[j+1];
				a[j+1]=x;
				
				cond=false;

			}
		}
	}
	cout<<"sortatrea bubble: ";
	for(int i=0;i<n;i++)
	{
		cout<<a[i]<<" ";
	}

}
void insertion(int *a, int n)
{

	int x;
	
	for(int i=1;i<n;i++)
	{
		profiler.countOperation("insertion_asignare", n,2);
		x=a[i];
		int j=0;
		profiler.countOperation("insertion_comparare", n);
		while(a[j]<x)
		{
			profiler.countOperation("insertion_asignare", n);
			
			j++;
		}
		for(int k=i;k>j;k--)
			
		{
			profiler.countOperation("insertion_asignare", n);
			a[k]=a[k-1];
			
		}
		profiler.countOperation("insertion_asignare", n);
		a[j]=x;
	}
	cout<<"\n";
	cout<<"sortatrea insertion: ";
	for(int i=0;i<n;i++)
	{
		cout<<a[i]<<" ";
	}
	
}
void selectie(int *a,int n)
{
	int x;
	int imin;
	for (int i=0;i<n-1;i++)
	{
		
		imin=i;
		for(int j=i+1;j<n;j++)
		{
			if (a[j]<a[imin])
			{
				profiler.countOperation("selectie_comparare", n);
				imin=j;
			}
				profiler.countOperation("selectie_asignare", n,3);
				x=a[imin];
				a[imin]=a[i];
				a[i]=x;
			}

	}
	
	cout<<"\n";
	cout<<"sortatrea selectie: ";
	for(int i=0;i<n;i++)
	{
		cout<<a[i]<<" ";
	}
}


int main()
{
	int n;
	int v[10000],v1[10000];
	for(n=100;n<1000;n+=100)
	{
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
		bubble(v1,n);
		memcpy(v1,v,n*sizeof(int));
		selectie(v1,n);
		memcpy(v1,v,n*sizeof(int));
		insertion(v1,n);
	}
	profiler.addSeries("bubble","bubble_comparare","bubble_asignare");
	profiler.addSeries("bubble","selectie_comparare","selectie_asignare");
	profiler.addSeries("insertion","insertion_comparare","insertion_asignare");
	profiler.createGroup("comp","bubble_comparare","insertion_comparare","selectie_comparare");
	profiler.createGroup("asignare","bubble_asignare","insertion_asignare","selectie_asignare");
	profiler.showReport();

	/*int test[]={4,9,1,8,3};
	bubble(test,5);
	int test1[]={4,9,1,8,3};
	insertion(test1,5);
	int test2[]={4,9,1,8,3};
	selectie(test2,5);*/
}