#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>

/**Parcurgerea unui graf in adancime(BFS) se face prin utilizarea unei liste Q. Pentru fiecare 
varf se parcurge primul dintre vecinii lui neparcursi inca. Dupa vizitarea varfului initial 
x1, se exploreaza primul  varf adiacent lui, fie acesta x2 , se trece apoi la primul varf
adiacent cu x2 si care nu a fost parcurs inca , s.a.m.d. Pentru a putea verifica care varf a fost
parcurs sau nu se foloseste urmatoare codificare: alb pentru varfurile neparcurse, gri pentru varful
care e parcurs in acest moment si negru pentru varfurile deja parcurse. 
Fiecare varf se parcurge cel mult odata. Afisarea se realizeaza in timpul parcurgerii. */

#define ALB 0
#define GRI 1
#define NEGRU 2
#define INF 10000

typedef struct vrf
{
	int color, dist, parent;
} VRF;


typedef struct NOD
{
    int val;
    struct NOD* next;
}NOD;

typedef struct Queue 
{
	int val;
	struct Queue *next;
}QUEUE;

QUEUE *start;
NOD *head[60001], *tail[60001];
VRF vrf[60001];
int V[100001];
int timp;
int k, op;
int QUE[10001], nrq;
int nivel=1;

int Q[60001];

void BFS (int s, int nrv)
{
	int u;
	for (u=1; u<=nrv; u++)
		if (u!=s)
		{
			vrf[u].color=ALB;
			vrf[u].dist=INF;
			vrf[u].parent=0;
		}
	vrf[s].color=GRI;
	vrf[s].dist=0;
	vrf[s].parent=0;
	k=1;
	Q[k]=s;
	op+=5;
	printf("%d \n", s);
	while(k!=0)
	{
		u=Q[1];
		nivel++;
		for (int i=1; i<k; i++)
		{
			Q[i]=Q[i+1];
		}
		k--;
		NOD *temp;
		temp=head[u];
		op++;
		while (temp!=NULL)
		{
			int v=temp->val;
			temp=temp->next;
			op+=3;
			if (vrf[v].color==ALB)
			{
				vrf[v].color=GRI;
				vrf[v].dist=vrf[u].dist+1;
				vrf[v].parent=u;
				k++;
				Q[k]=v;
				for(int i=0; i<nivel; i++)
					printf(" ");
				printf("%d \n", v);
				op+=4;
			}
			//nivel++;
		}
		//k--;
		vrf[u].color=NEGRU;
		op++;
	}
}

/*void pretty_print (int parent, int depth)
{
    if(parent != NULL)
	{
		pretty_print(root->, depth+1);
		for(int i=0; i<depth; i++)
		{
			printf("	");
		}
		printf("%d (%d)\n", root->key, root->size);
		pretty_print(root->dr, depth+1);
	}
}*/

bool verif (int x, int y)
{
	NOD *p;
	p=head[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->next;
	}
	return true;
}

void genereaza(int nrv,int nre)  //nrv= numarul de varfuri, nre = numarul de edges
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		head[i]=tail[i]=NULL;
	i=1;
	while (i<=nre)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
			if(head[u]==NULL)
			{
				head[u]=(NOD *)malloc(sizeof(NOD *));
				head[u]->val=v;
				head[u]->next=NULL;
				tail[u]=head[u];
				i++;
			}
			else 
			{
				if (verif (u, v))
				{
					NOD *p;
					p=(NOD *)malloc(sizeof(NOD *));
					p->val=v;
					p->next=NULL;
					tail[u]->next=p;
					tail[u]=p;
					i++;
				}
			}
		}
	}
}


void print(int nrv)
{
	int i;
	NOD *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=head[i];
		while(p!=NULL)
		{
			printf("%d ",p->val);
			p=p->next;
		}
		printf("\n");
	}
}


int main()
{
	int nre, nrv;
	///TEST
	nrv=10;
	nre=20;
	srand(time(NULL));
	genereaza(nrv, nre);
	print(nrv);
	BFS(1, nrv);

	/*
	FILE *f;
	f=fopen ("rez1.txt", "w");
	fprintf(f, "nre op\n");
	nrv=100;
	for (nre = 1000; nre<=5000; nre=nre+100)
	{
		op=0;
		genereaza(nrv, nre);
		BFS(1, nrv);
		fprintf(f, "%d %d\n", nre, op);
	}
	fclose(f);

	f=fopen ("rez2.txt", "w");
	fprintf(f, "nrv op\n");
	nre=9000;
	for (nrv = 100; nrv<=200; nrv=nrv+10)
	{
		op=0;
		genereaza(nrv, nre);
		BFS(1, nrv);
		fprintf(f, "%d %d\n", nrv, op);
	}
	fclose(f);*/

	printf("Am termiant %c\n",7);
	getch(); 
    return 0;
}