#include<iostream>
#include  "Profiler.h"
#include<conio.h>
#define MAX 10000
using namespace std;
Profiler profiler("HeapSort vs QuickSort");
Profiler profiler2("QuickSort Best-Worst");
int size,n;

void Heapify(int A[],int i,int n)
{
	int left,right,largest,aux;
	int comp = 0;
	int assign = 0;
	left=2*i;
	right=2*i+1;
	comp++;
	if(left<=size && A[left]>A[i])
		{
			largest=left;
	}
	else{
		largest=i;
	}
	
	comp++;
	
	if(right<=size && A[right]>A[largest]){
		largest=right;
	}
	if(largest!=i){
		aux=A[i];
		A[i]=A[largest];
		A[largest]=aux;
		assign+=3;
		Heapify(A,largest,n);
	}
	profiler.countOperation("Assignments HeapSort",n,assign);
	profiler.countOperation("Comparisons HeapSort",n, comp);
}


void BuildMaxHeap(int A[],int n)
{
	size=n;
	for(int i=n/2;i>=1;i--){
		Heapify(A,i,n);
	}
}

void HeapSort(int A[])
{
	int aux;
	int assign = 0;
	BuildMaxHeap(A,n);
	for(int i=n;i>=2;i--){
		aux=A[i];
		A[i]=A[1];
		A[1]=aux;
		assign+=3;
		size--;
		Heapify(A,1,n);
	}
	profiler.countOperation("Assignments HeapSort",n,assign);
}

void QuickSort(int arr[], int left, int right) 
{
    int i = left, j = right;
	int assign = 0;
	int comp = 0;
    int tmp;
    int pivot = arr[(left + right) / 2];
	assign++;
	while (i <= j) 
	{	comp++;
		while (arr[i] < pivot)
		{	comp++;
			i++;
		}
		comp++;
		while (arr[j] > pivot)
		{	comp++;
			j--;
		}
        if (i <= j) 
		{	assign++;
			
            tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
	}
	if (left < j)
		QuickSort(arr, left, j);
	if (i < right)
		QuickSort(arr, i, right);
	profiler.countOperation("Assignments QuickSort",n,assign);
	profiler.countOperation("Comparisons QuickSort",n,comp);
}

void AverageCase()
{
	int arr1[MAX],arr2[MAX];
	int i,j;
	for(i=100;i<10000;i+=100)
	{
		FillR***ANONIM***omArray(arr1,i,0,10000);
		FillR***ANONIM***omArray(arr2,i,0,10000);
		for(int k=1;k<=5;k++)
		{	
			n=i;
			QuickSort(arr1,1,n);
			HeapSort(arr2);
		}
	}
	profiler.addSeries("Total HeapSort","Comparisons HeapSort","Assignments HeapSort");
	profiler.addSeries("Total QuickSort","Comparisons QuickSort","Assignments QuickSort");
	profiler.createGroup("Comparisons","Comparisons HeapSort","Comparisons QuickSort");
	profiler.createGroup("Assignments","Assignments HeapSort","Assignments QuickSort");
	profiler.createGroup("Total","Total HeapSort","Total QuickSort");
	profiler.showReport();
}

void QuickBest(int arr[], int left, int right) 
{
    int i = left, j = right;
    int tmp;
    int pivot = arr[(left + right) / 2];
	profiler2.countOperation("Assignments QuickSort Best",n);
    while (i <= j) 
	{
		profiler2.countOperation("Comparisons QuickSort Best",n);
        while (arr[i] < pivot)
		{ 
			i++;
			profiler2.countOperation("Comparisons QuickSort Best",n);
		}
		profiler2.countOperation("Comparisons QuickSort Best",n);
        while (arr[j] > pivot)
		{
			j--;
			profiler2.countOperation("Comparisons QuickSort Best",n);
		}
        if (i <= j) 
		{
			profiler2.countOperation("Assignments QuickSort Best",n,3);
            tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
	}
	if (left < j)
		QuickBest(arr, left, j);
	if (i < right)
		QuickBest(arr, i, right);
}

void QuickWorst(int arr[], int left, int right) 
{
    int i = left, j = right;
    int tmp;
	int assign = 0;
	int comp = 0;
    int pivot = arr[left];
	assign++;
    while (i <= j) 
	{	comp++;
        while (arr[i] < pivot)
		{	comp++;
			i++;
		}
		comp++;
        while (arr[j] > pivot)
		{	comp++;
			j--;
		}
        if (i <= j) 
		{	assign+=3;
			tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
            i++;
            j--;
        }
	}
	if (left < j)
		QuickWorst(arr, left, j);
	if (i < right)
		QuickWorst(arr, i, right);
	profiler2.countOperation("Assignments QuickSort Worst",n,assign);
	profiler2.countOperation("Comparisons QuickSort Worst",n, comp);
}

void bestWorst()
{
int arr1[MAX],arr2[MAX];
	int i,j;
	for(i=100;i<10000;i+=100)
	{
		FillR***ANONIM***omArray(arr1,i,0,10000,false,1);
		FillR***ANONIM***omArray(arr2,i,0,10000,false,1);
		n=i;
		
		QuickBest(arr1,1,n);
		QuickWorst(arr2,1,n);
	}
	
	profiler2.addSeries("QuickSort Best","Comparisons QuickSort Best","Assignments QuickSort Best");
	profiler2.addSeries("QuickSort Worst","Comparisons QuickSort Worst","Assignments QuickSort Worst");
	profiler2.createGroup("Comparison","Comparisons QuickSort Best","Comparisons QuickSort Worst");
	profiler2.createGroup("Assignments","Assignments QuickSort Best","Assignments QuickSort Worst");
	profiler2.createGroup("Best-vs-Worst","QuickSort Best","QuickSort Worst");
	profiler2.showReport();
}

void demo()
{
	n = 10;
	int arr1[10] = {1,5,3,8,5,6,9,3,2,4};
	int arr2[11] = {0,1,5,3,8,5,6,9,3,2,4};

	printf("\nUnsorted: \n\n");
	for(int i=1;i<=10;i++)
		printf("%d ",arr1[i]);
	QuickSort(arr1,0,10);
	HeapSort(arr2);
	cout<<"\nSorted with Quicksort: \n\n";
	for(int i=1;i<=n;i++)
		printf("%d ",arr1[i]);
	cout<<"\nSorted with Heapsort: \n\n";
	for(int i=1;i<=n;i++)
		printf("%d ",arr2[i]);
	getch();
}

void main()
{
//	AverageCase();
//	bestWorst();
	demo();
}

// The QuickSort is better than the HeapSort regarding the assignments ***ANONIM*** comparisons.
// The Assignments number of the QuickSort is way better than thhe Assignments number of the HeapSort.
// The HeapSort increases in nlogn. The The QuickSort has picks, but overall increases in nlogn.

// The Assignment numbers in Worst ***ANONIM*** Best QuickSort cases are almost the same. At Best case it has picks ***ANONIM*** is little smaller. They both increase nlogn
// The Comparissons number in Best case is much smaller than the in the Worst case. In Best case it increases liniarly ***ANONIM*** in the Worst case it increases quadraticaly with picks.
// The Best Case is much better than the Worst case. The best case has liniarly increase, ***ANONIM*** the worst case ha quadratic increase.