#include <stdio.h>
#include <conio.h>
#include "Profiler.h"


/*Josephus algorithm is used when one wants to extract elements from a set at certain positions.
Algorithm implemented below runs in O(nlogn) time:it takes O(n) to build the tree, O(n) to update size and O(logn) to search for minimum elem. in delete procedure and O(logn)
to select the element from the set*/
Profiler profiler("Josephus");
typedef struct tree {
	int n;
	int size;
	struct tree *left,*right,*pt;
} Tree;

int i=1;

Tree *buildTree(int nr,int *op) {
	Tree *p;
	*op=*op+1;
	if (nr == 0) 
		return NULL;
	else {
		p=(Tree*)malloc(sizeof(Tree));
		p->left=buildTree(nr/2,op);
		p->n=i;
		p->size=nr;
		i++;
		p->right=buildTree(nr-nr/2-1,op);
		*op=*op+5;
	}
	return p;
}


void parent(Tree *p,int *op) {
	*op=*op+1;
	if (p!=NULL) {
		*op=*op+2;
		if(p->left !=NULL) {
			p->left->pt=p;
			*op=*op+1;
		}
		if (p->right!=NULL) {
			p->right->pt=p;
			*op=*op+1;
		}
		parent(p->left,op);
		parent(p->right,op);
	}
}

Tree *select(Tree *p,int i,int *op) {
	int r;
	*op=*op+2;
	if (p->left !=NULL) {
		r=p->left->size+1;
		*op=*op+1;
	} else
		r=1;
	if (i==r) 
		return p;
	else if (i<r) {
		*op=*op+1;
		if (p->left != NULL)
			return select(p->left,i,op);
		else
			return select(p,i,op);
	}
	else {
		*op=*op+1;
		if (p->right != NULL)
			return select(p->right,i-r,op);
		else 
			return select(p,i-r,op);
	}
}


void prettyPrint(Tree *p,int nivel) {
	int i;
	if (p!=NULL) {	
		prettyPrint(p->right,nivel+1);
		for (i=0;i<nivel;i++) 
			printf("   ");
		//printf("%d %d\n",p->n,p->size);	
		printf("%d\n",p->n);
		prettyPrint(p->left,nivel+1);
	}
}

Tree *minTree(Tree *p,int *op) {
	*op=*op+1;
	while (p->left != NULL) {
		p=p->left;
		*op=*op+2;
	}
	return p;
}

void transplant(Tree ***r,Tree *u,Tree *v,int *op) {
	*op=*op+1;
	if (u->pt == NULL) {
		**r=v;
		*op=*op+1;
	}
	else if (u == u->pt->left) {
		u->pt->left=v;
		*op=*op+2;
	}
	else {
		u->pt->right=v;
		*op=*op+2;
	}
	*op=*op+1;
	if (v!=NULL) {
		v->pt=u->pt;
		*op=*op+1;
	}
}

void updateSize(Tree *p,int *op) {
	*op=*op+1;
	while (p!=NULL) {
		p->size--;
		p=p->pt;
		*op=*op+3;
	}
}

void deleteTree(Tree **t,Tree *z,int *op) {
	Tree *y;
	*op=*op+1;
	if (z->left == NULL) {
		transplant(&t,z,z->right,op);
		updateSize(z,op);
	}
	else if (z->right == NULL) {
		transplant(&t,z,z->left,op);
		updateSize(z,op);
		*op=*op+1;
	}
	else {
		y=minTree(z->right,op);
		*op=*op+2;
		if (y->pt != z) {
			transplant(&t,y,y->right,op);
			y->right=z->right;
			y->right->pt=y;
			*op=*op+2;
		}
		transplant(&t,z,y,op);
		y->left=z->left;
		y->left->pt=y;
		y->size=z->size;
		*op=*op+3;
		updateSize(y,op);
	}
}

void josephus(int n,int m,int *op) {
	Tree *root,*x;
	root=buildTree(n,op);
	root->pt=NULL;
	*op=*op+1;
	parent(root,op);
	int j=1;
	for (int k=n;k>=1;k--) {
		prettyPrint(root,0);
		j=(j+m-2)%k+1;
		x=select(root,j,op);
		printf("\n\n");
		printf("Num: %d ",x->n);
		printf("\n\n\n");
		deleteTree(&root,x,op);
	}
}

int main() {

	int op=0;
	josephus(7,3,&op);

	/*for(int n=100;n<=10000;n+=100) {
		op=0;
		josephus(n,n/2,&op);
		profiler.countOperation("j_operations",n,op);
	}
	
	profiler.showReport(); */
	

	getch();
	return 0;
}