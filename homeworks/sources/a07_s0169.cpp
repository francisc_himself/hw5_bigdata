/*
**********************ANONIM*** ***ANONIM*** ,gr ***GROUP_NUMBER********************
complexitatea algoritmilor de transformare este o(n)-liniara pentru ca
a- nr ap rec,b-fact de scadere,c-cate bucle
transf1:doua bucle separat => O(n)
transf2:a=0,b=1,c=2   a<b^c => O(n)
*/
#include<stdio.h>
#include<conio.h>
#include<malloc.h>

typedef struct multi_node{
	int key;
	int count;
	struct multi_node* child[50];
}multi_node;

typedef struct binary_node {
    int key;
    binary_node* firstc;
    binary_node* rightb;
}binary_node;

multi_node *create_mn(int key){
	multi_node* aux=(multi_node*)malloc(sizeof(multi_node));
	aux->key=key;
	//aux->child=NULL;
	aux->count=0;
	return aux;
}
binary_node* create_bn(int key) {
    binary_node* nod = (binary_node*) malloc(sizeof(binary_node));
    nod->firstc = NULL;
    nod->rightb = NULL;
    nod->key = key;
    return nod;
}
void insert_mn(multi_node*parent,multi_node*child){
	parent->child[parent->count++]=child;
}

multi_node* transform1(int t[],int size){
	//ni se da un vect corect
	multi_node* root=NULL;
	multi_node* nodes[50];
	for(int i=0;i<size;i++)
		nodes[i]=create_mn(i);
	for(int i=0;i<size;i++)
		if(t[i]==-1) 
			root=nodes[i];
		else
			insert_mn(nodes[t[i]],nodes[i]);
		return root;
	}
binary_node* transform2(multi_node *root,binary_node *rootB) {
    if (root != NULL) {
        int i;
        binary_node * left;
        binary_node * right;
        binary_node * temp;
        switch (root->count) {
        case 0:
            break;
        case 1: //daca avem numai copil stang
            left = create_bn(root->child[0]->key);
            rootB->firstc = transform2(root->child[0],left);
            break;
        default://ambii copii
            left = create_bn(root->child[0]->key);
            rootB->firstc = transform2(root->child[0],left);
            temp= rootB;
            rootB = rootB->firstc;
            for (i = 1; i < root->count;i++) {//pt nodurile din dreapta
                right = create_bn(root->child[i]->key);
                rootB->rightb = transform2(root->child[i],right);
                rootB = rootB->rightb;
            }
            rootB = temp;//se revine la nodul parinte
            break;
        }
        return rootB;
    }
 //   return NULL;
}
void prettyPrintMN (multi_node *nod,int nivel=0)
{
	for(int i=0;i<nivel;i++)
		printf("   ");
		printf("%d\n",nod->key);
	
	for(int i=0;i<nod->count;i++)
		prettyPrintMN(nod->child[i],nivel+1);	
}
void prettyPrintBN (binary_node *nod,int nivel=0)
{
	if(nod!=NULL){
		for(int i=0;i<nivel;i++)
			printf("   ");
		printf("%d\n",nod->key);
		prettyPrintBN(nod->firstc,nivel+1);	
		prettyPrintBN(nod->rightb,nivel);
	}
}

int main(){
		int t[]={3,3,3,-1,2,2,2,2,5,1,1};
		printf("\n vectorul de tati\n");
		 for (int i = 0; i < 11; i++) 
			printf("%d ",t[i]);
		int size=sizeof(t)/sizeof(t[0]);
		multi_node*root=transform1(t,size);
		printf("\n reprezentare multi cai\n");
		prettyPrintMN(root,0);
		binary_node*roott=create_bn(root->key);
		transform2(root,roott);
		printf("\n reprezentare binara\n");
		prettyPrintBN(roott,0);
		_getch();
		return 0;
	}