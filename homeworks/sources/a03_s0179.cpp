#include "conio.h"
#include "stdlib.h"
#include "stdio.h"
#include "Profiler.h"

/*****************************************************************
/
/	Name : ***ANONIM*** ***ANONIM*** ***ANONIM***
/	Group : ***GROUP_NUMBER***
/	Assignment : 3 Advanced sorting methods: Heapsort, Quicksort
/
/*****************************************************************/

/***********************************************************************************
/
/									OBSERVATIONS, INTERPRETATIONS
/
/	1. Compared in average case, quicksort bests heapsort
/	2. Heapsort has the worst case running time of O(nlogn) => optimal algorithm
/	3. Quicksort behaves better than heapsort, though not optimal, its best case
/	   running time is O(nlogn), in the average it is close to this value but in
/	   the worst case it is O(n^2)
/
/***********************************************************************************/

//Definitions of constants/macros

#define MAX_NR 10000

#define left(node) (2*node)
#define right(node) (2*node+1)
#define parent(node) (node/2)

#define SWAP(x,y) int t;t=x;x=y;y=t;

//declarations
int a[MAX_NR];
int heap[MAX_NR+1];
int q[MAX_NR+1];

int heapSize;

//Profiler
Profiler profiler("AdvancedSorting");

int size;
//int size = 10;  //used for proving correctness

void heapify(int node)
{
	int largest = 0;
	int left = left(node);
	int right = right(node);
	profiler.countOperation("heapComparison",size,1);
	if((left <= heapSize) && (heap[left] > heap[node]))
	{
		largest = left;
	}
	else
	{
		largest = node;
	}
	profiler.countOperation("heapComparison",size,1);
	if((right <= heapSize) && (heap[right] > heap[largest]))
	{
		largest = right;
	}
	if (largest != node)
	{
		profiler.countOperation("heapAssignment",size,3);
		SWAP(heap[node],heap[largest]);
		heapify(largest);
	}
}

void bottomup()
{
	//copy array in the "heap"
	for(int i = 0; i < size; i++)
	{
		heap[i+1] = a[i];
	}
	heapSize = size;
	//build the heap
	for(int i = size/2; i >= 1; i--)
	{
		heapify(i);
	}
}

void heapSort()
{
	bottomup();
	for(int i = size; i >=2; i--)
	{
		profiler.countOperation("heapAssignment",size,3);
		SWAP(heap[i],heap[1]);
		heapSize--;
		heapify(1);
	}
}

int partition(int p, int r)
{
	profiler.countOperation("quickAssignment",size,1);
	int x = q[r];
	int i = p - 1;
	for(int j = p; j <= r-1; j++)
	{
		profiler.countOperation("quickComparison",size,1);
		if(q[j] <= x)
		{
			i++;
			profiler.countOperation("quickAssignment",size,3);
			SWAP(q[i],q[j]);
		}
	}
	profiler.countOperation("quickAssignment",size,3);
	SWAP(q[i+1],q[r]);
	return i+1;
}

int randomizedPartition(int p, int r)
{
	int i = (rand()%(r-p+1))+p;
	profiler.countOperation("quickAssignment",size,3);
	SWAP(q[r],q[i]);
	return partition(p,r);
}

void qSort(int p, int r)
{
	if(p<r)
	{
		int pos = randomizedPartition(p,r);
		qSort(p,pos-1);
		qSort(pos+1,r);
	}
}

int hoare(int p, int r)
{
	profiler.countOperation("quickWorstAssignment",size,1);
	int x = q[p];
	int i = p-1;
	int j = r;
	while(true)
	{
		do
		{
			j--;
			profiler.countOperation("quickWorstComparison",size,1);
		}
		while(q[j]>x);
		do
		{
			i++;
			profiler.countOperation("quickWorstComparison",size,1);
		}
		while(q[i]<x);
		if(i<j)
		{
			profiler.countOperation("quickWorstAssignment",size,3);
			SWAP(q[i],q[j]);
		}
		else
		{
			return j+1;
		}
	}
}

void qWorst(int p, int r)
{
	if(p<r+1)
	{
		int pos = hoare(p,r);
		qSort(p,pos);
		qSort(pos,r);
	}
}


void main()
{
	/*
	//Hard-coded part, for proving correctness

	FillRandomArray(a,10,1,10,true,0);
	for(int i = 0; i < size; i++)
	{
		printf("%d ",a[i]);
	}
	printf("\n");
	heapSort();
	for(int i = 1; i <= size; i++)
	{
		printf("%d ",heap[i]);
	}
	//copy the array
	for(int i = 0; i < size; i++)
	{
		q[i+1] = a[i];
	}
	printf("\n\n");
	qSort(1,size);
	for(int i = 1; i <= size; i++)
	{
		printf("%d ",q[i]);
	}
	getch();
	*/

	/*
	// AVERAGE test

	for(int m = 1; m <= 5; m++)
	{
		for(size = 100; size <= 10000; size +=100)
		{
			FillRandomArray(a,size,-2000,5000,false,0);
			heapSort();
			profiler.addSeries("heapTotal","heapComparison","heapAssignment");
			for(int i = 0; i < size; i++)
			{
				q[i+1] = a[i];
			}
			qSort(1,size);
			profiler.addSeries("quickTotal","quickComparison","quickAssignment");
			printf("%d\n",size);
		}
	}


	profiler.createGroup("Comparisons","heapComparison","quickComparison");
	profiler.createGroup("Assignments","heapAssignment","quickAssignment");
	profiler.createGroup("TotalOperations","heapTotal","quickTotal");
	profiler.showReport();
	*/


	
	//QSort WORST CASE

	for(size = 100; size <= 10000; size +=100)
		{
			//FillRandomArray(a,size,-2000,5000,false,1);
			for(int i = 0; i < size; i++)
			{
				q[i+1] = i;
			}
			qWorst(1,size);
			profiler.addSeries("quickWorstTotal","quickWorstComparison","quickWorstAssignment");
			printf("%d\n",size);
		}
	profiler.showReport();
	

	return;
}