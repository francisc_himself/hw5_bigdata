#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int n;
int hashTable[9973];
int effort1;
int maxEffort1;
int effort2;
int maxEffort2;
int found;
int notfound;
float alfa=0;

int hashing2(int x)
{
	return x%9973;    
}
void hashing1(int x, int i)
{
    if (hashTable[(hashing2(x)+i*3+i*i*2)%9973]==-1)
        	hashTable[(hashing2(x)+i*3+i*i*2)%9973]=x;
   else 
		hashing1(x,i+1);
}

void search(int val, int i)
{
    if (hashTable[(hashing2(val)+(i-1)*3+(i-1)*(i-1)*2)%9973]==val)
    {
        effort1=effort1+i;
        if (i>maxEffort1)
					maxEffort1=i;
        found++;
    }
    else if (hashTable[(hashing2(val)+(i-1)*3+(i-1)*(i-1)*2)%9973]==-1 || i==9973)
             {
                 effort2=effort2+i;
                 if (i>maxEffort2) maxEffort2=i;
                 notfound++;
             }
    else
    {
        search(val,i+1);

    }
}
int main()
{
	srand(time(NULL));
    FILE *f=fopen("Assignment5.csv","w");
	fprintf(f,"Filling,Average effort for found,Max effort for found,Average effort for not found,Max effort for not found\n");
	for (alfa=0.8; alfa<=0.96; alfa+=0.05)
	{
	    printf("Alpha= %1.2f\n",alfa);
		effort1=0;
        effort2=0;
        maxEffort1=0;
        maxEffort2=0;
        found=0;
        notfound=0;
	    for(int l=0; l<5; l++)
	    {
	    for(int k=0;k<9973;k++)
	    {
            hashTable[k]=-1;
	    }
        n=0;

        while((float) n/9973<alfa)
        {
            hashing1(rand(),0);
			n++;
        }

        int i=0;
        int j=0;
        while (i<1500)
        {
			j=rand()%9973;
            if (hashTable[j]!=-1)
                {
				search(hashTable[j],1);
                i++;
                }
        }
    
        for (i=0;i<1500;i++)
        {
            search(rand()+40000,1);
        }
	    }
		  
	    float print1=(float) effort1/found;
	    float print2=(float) effort2/notfound;
		found=found/5;
		notfound=notfound/5;
	    printf("Found %d with the effort %d\n",found,effort1/(5*found));
		printf("Not found %d with the effort %d\n",notfound,effort2/(5*notfound));
	    fprintf(f,"%1.2f,%f,%d,%f,%d\n",alfa,print1,maxEffort1,print2,maxEffort2);
		}
		float alfa=0.99;
		effort1=0;
        effort2=0;
        maxEffort1=0;
        maxEffort2=0;
        found=0;
        notfound=0;
	    for(int l=0;l<5; l++)
	    {
	    for(int k=0;k<9973;k++)
	    {
            hashTable[k]=-1;
	    }
        n=0;
        while((float) n/9973<alfa)
        {
            hashing1(rand(),0);

            n++;
        }


        int i=0;
        int j=0;
        while (i<1500)
        {
			j=rand()%9973;
            if (hashTable[j]!=-1)
            {
				search(hashTable[j],1);
				i++;
            }
        }
        for (i=0;i<1500;i++)
        {
            search(rand()+40000,1);
        }
	    }
		
	float print1=(float) effort1/found;
	float print2=(float) effort2/notfound;
	found=found/5;
	notfound=notfound/5;
	printf("Found %d with the effort %d\n",found,effort1/(5*found));
	printf("Not found %d with the effort %d\n",notfound,effort2/(5*notfound));
	fprintf(f,"%1.2f,%f,%d,%f,%d\n",alfa,print1,maxEffort1,print2,maxEffort2);
	fclose(f);
    char c;
	scanf("%c",&c);
	return 0;
}
