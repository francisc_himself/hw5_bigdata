/*
***ANONIM*** ***ANONIM***
gr: ***GROUP_NUMBER***

scopul acestui cod este de a testa 3 metode de sortare :
	- metoda bulelor
	- insertie
	- selectie

Comportament asteptat:

1. metoda bulelor:
		caz_favorabil   O(n)
		caz_mediu	    O(n^2)
		caz_nefaborabil O(n^2)

2. insertie:
		caz_favorabil	O(n) comparatii, O(1) interschimbari
		caz_mediu		O(n^2)
		caz_nefaborabil	O(n^2)

3. selectie:
		caz_favorabil	O(n^2)
		caz_mediu		O(n^2)
		caz_nefaborabil	O(n^2)


Observatii:

I. Comparatii intre algoritmi pentru cazul mediu:
	la testarea pe siruri cu 100 de elemente alese aleator am obtinut in medie urmatoarele rezultate
					nr_atribuiri    nr_comparatii
	met_bulelor		4287.6			2836.4
	insertie		1429.2			1429.2		
	selectie		178.2			2970

	Observam ca met_bulelor este ineficienta din punctul de vedere al atribuirilor, sortarea prin selectie este cea mai eficienta in acest sens. 
	Din punctul de vedere al comparatiilor, selectia si metoda bulelor sunt similare, metoda de sortare prin insertie fiind cea mai eficinta.
	Din punctul de vedere al numarului total de operatii, metoda bulelor este cea mai ineficienta; selectia si insertia se comporta asemanator

II. Grafice pentru cazuri
	
	cazul_favorabil : fisierul cazul_favorabil.htm

	cazul_mediu : fisierul cazul_mediu_statistic.htm

	cazul_defavorabil : fisierul cazul_defavorabil.htm

III. Observatii suplimentare

	a.) Metoda buleleor
		- este cea mai ineficienta dintre metode
		- se poate folosi in cazul in care se cunaste ca numarul de inversiuni din sir este mic. Aceasta deoarece complexitatea in cazul favorabil este O(n). 
		- metoda nu este stabila
		- sortarea este in-place

	b.) Sortare prin insertie
		- dintre metodele patratice este printre cele mai rapide. 
		- in cazul favorabil complexitatea este O(n), dar se fac O(1) atribuiri (avantaj fata de metoda bulelor)
		- metoda este stabila
		- sortarea este in-place

	c.) Sortarea prin selectie
		- este mai eficienta decat metoda bulelor dar in general mai putin eficienta decat sortarea prin insertie
		- sortarea este in-place
		- in cazul favorabil complexitatea este tot O(n)
*/

#include<stdio.h>
#include"Profiler.h"


Profiler profiler("Sortarea sirurilor caz favorabil");

/* variabile pentru partea de comparare I
int nra_bule,nra_ins,nra_sel,nrc_bule,nrc_sel,nrc_ins;
int medie_a[3];
int medie_c[4];
*/

unsigned long long nra_bule,nra_ins,nra_sel,nrc_bule,nrc_sel,nrc_ins;
FILE *fout;

/*
algoritm de sortare cu metoda bulelor

parametrii :
	sir = sir de intregi ce trebuie sortat
	lungime_sir = lungimea sirului
*/



void met_bulelor(int sir[], int lungime_sir)
{
	int n = lungime_sir; // pozitia ultimului element
	int i,aux; //iterator
	bool swapped; // semafor
	do{
		swapped = false;
		for(i=1;i<n;i++)
		{
			//profiler.countOperation("comp_met_bulelor", lungime_sir);
			nrc_bule++;
			if(sir[i-1]>sir[i])
			{
				//profiler.countOperation("atrib_met_bulelor", lungime_sir,3);
				nra_bule+=3;
				aux=sir[i];
				sir[i]=sir[i-1];
				sir[i-1]=aux;
				swapped = true;
			}
		}
			n--;
	}
	while(swapped);
}

/*
algoritm de sortare prin insertie

parametrii :
	sir = sir de intregi ce trebuie sortat
	lungime_sir = lungimea sirului
*/
void insertie(int sir[],int lungime_sir){
	int i,j; // iteratori
	int valoare_de_inserat;
	for(i =1; i<lungime_sir;i++)
	{
		valoare_de_inserat=sir[i];
		j= i-1;
		while(j>=0 && sir[j]>valoare_de_inserat){
			sir[j+1]=sir[j];
			j=j-1;
			nra_ins++;
			nrc_ins++;
			//profiler.countOperation("comp_insertie", lungime_sir);
			//profiler.countOperation("atrib_insertie", lungime_sir);
			}
		sir[j+1]=valoare_de_inserat;
	}
}

/*
algoritm de sortare prin selectie

parametrii :
	sir = sir de intregi ce trebuie sortat
	lungime_sir = lungimea sirului
*/

void selectie(int sir[],int lungime_sir){
	int i,j;//iteratori
	int i_min,aux;
	for(i=0;i<lungime_sir-1;i++)
	{
		i_min=i;
		for(j=i+1;j<lungime_sir;j++)
		{
			nrc_sel++;
			//profiler.countOperation("comp_selectie", lungime_sir);
			if(sir[j]<sir[i_min])
					i_min=j;
		}
		//profiler.countOperation("atrib_selectie", lungime_sir,3);
		nra_sel+=3;
		aux=sir[i];
		sir[i]=sir[i_min];
		sir[i_min]=aux;
	}
}


int main(){
	int sir_random[10001];
	int sir_random2[10001];
	int sir_random3[10001];


	/*codul pentr I
	//generarea mediilor de avg_case
	for(int k=1;k<4;k++){
		int dim =100;
		nra_bule=nra_ins=nra_sel=nrc_bule=nrc_sel=nrc_ins=0;
		FillRandomArray(sir_random, dim);
		// facem copii
		for(int i =0;i<dim;i++)
			sir_random2[i]=sir_random3[i]=sir_random[i];
	
		met_bulelor(sir_random,dim);
		insertie(sir_random2,dim);
		selectie(sir_random3,dim);
		medie_a[0]+= nra_bule; 
		medie_c[0]+=nrc_bule;
		medie_a[1]+= nra_ins; 
		medie_c[1]+=nrc_ins;
		medie_a[2]+= nra_sel; 
		medie_c[2]+=nrc_sel;
	}
	
	for(int i=0;i<3;i++)
		printf("%lf %lf \n",medie_a[i]/5.0,medie_c[i]/5.0);
	system("Pause");
	return 0;
	*/

	fout = fopen("Data_rand.csv","w");
	fprintf(fout,"%s","dim, nra_bule, nra_ins, nra_sel, nrc_bule, nrc_sel, nrc_ins\n");

	/*
	aici este codul pentru generarea graficelor. Am schimpat pe rand tipul de sir pe care il foloses (aleator, ascendent, descendent)
	*/
	for(int dim = 100; dim<=10000; dim+=100 )
	{
		nra_bule=nra_ins=nra_sel=nrc_bule=nrc_sel=nrc_ins=0;
		printf("acum calculam pentru:     %d\n",dim);
		FillRandomArray(sir_random, dim,1);
		
		// facem copii
		for(int i =0;i<dim;i++)
			sir_random2[i]=sir_random3[i]=sir_random[i];
	
		met_bulelor(sir_random,dim);
		if(!IsSorted(sir_random,  dim)){
			printf("fail bule @ %d", dim);
			break;
			}
	
		insertie(sir_random2,dim);
		if(!IsSorted(sir_random2,  dim)){
			printf("fail insertie @ %d", dim);
			break;
		}

		selectie(sir_random3,dim);
		if(!IsSorted(sir_random3,  dim)){
			printf("fail selectie @ %d", dim);
			break;
		}
		fprintf(fout,"%d, %llu, %llu, %llu, %llu, %llu, %llu\n",dim, nra_bule, nra_ins, nra_sel, nrc_bule, nrc_sel, nrc_ins);

	}


	//profiler.addSeries("operatii_met_bulelor","comp_met_bulelor","atrib_met_bulelor");
	//profiler.addSeries("operatii_insertie","comp_insertie","atrib_insertie");
	//profiler.addSeries("operatii_selectie","comp_selectie","atrib_selectie");

	//profiler.createGroup("comparatii","comp_met_bulelor","comp_insertie","comp_selectie");
	//profiler.createGroup("asignari","atrib_met_bulelor","atrib_insertie","atrib_selectie");
	//profiler.createGroup("total","operatii_met_bulelor","operatii_insertie","operatii_selectie");

	//profiler.showReport();
	system("pause");
	return 0;

}
