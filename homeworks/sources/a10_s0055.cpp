#include<iostream>
#include <list>
#include "Profiler.h"
using namespace std;
#define e_max 60000 
class Graph
{
    int V;    
    list<int> *adj;    
	int edges[e_max];
    void DFSUtil(int v, bool visited[]);  
public:
    Graph(int V);   
    void addEdge(int v, int w);   
    void DFS();  
	void generate(int m, int n);
};
 
Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}
 
void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w); 
}
 
void Graph::DFSUtil(int v, bool visited[])
{
    
    visited[v] = true;
    cout << v << " ";
 
   
    list<int>::iterator i;
    for(i = adj[v].begin(); i != adj[v].end(); ++i)
        if(!visited[*i])
            DFSUtil(*i, visited);
}
 
void Graph::DFS()
{
   
    bool *visited = new bool[V];
    for(int i = 0; i < V; i++)
        visited[i] = false;
 
    
    for(int i = 0; i < V; i++)
        if(visited[i] == false)
            DFSUtil(i, visited);
}

void Graph::generate(int n, int m)
{
	FillRandomArray(edges,m,0,n*n-1,true);
	for(int i=0;i<m;i++)
	{
		int a=edges[i]/n;
		int b=edges[i]%n;
		adj[a].push_back(b);
	}
}
 
int main()
{
   // Graph g(4);
    //g.addEdge(0, 1);
    //g.addEdge(0, 2);
    //g.addEdge(1, 2);
    //g.addEdge(2, 0);
    //g.addEdge(2, 3);
//    g.addEdge(3, 3);
 
  //  cout << "Depth First Traversal of graph:\n";
    //g.DFS();
 
	int n,m;
	cout << "Size of graph:";
	cin >> n; cout << endl;
	cout << "Nr of edges:";
	cin >> m; cout << endl;
	Graph g(n);
	g.generate(n,m);
	cout << "DFS visiting order of graph:\n";
	g.DFS();
	return 0;
}