/*
***ANONIM*** ***ANONIM***
gr ***GROUP_NUMBER***

Acest cod are rolul de a efectua transformari intre trei metode de reprezentare a arborilor multi cai.

Metode de reprezentare folosite:
	1. Sir de tati;
	2. MultiNod (structura MULTI_NODE)
	3. Reprezentare binara(structura BINARY_MULTI_NODE)

Transformarea din 1 in 2 este liniara (se parcurge sirul de tati o data);
Transformarea din 2 in 3 este liniara (se parcurge fiecare nod o singura data);

Memorie folosita:
	1. Sir de tati -- foloseste cea mai putina memorie (n*sizeof(int))
	2. MultiNod -- foloseste memorie multa. Implementara mea are problema spatiului irosit
	3. Reprezentare binara -- foloseste de trei ori mai multa memorie decat sirul de tati, dar mult mai putina decat pentru MultiNod 

*/
#include <windows.h>
#include <iostream>
using namespace std;

typedef struct _MULTI_NODE
{
	int Key;
	int Size;
	int Capacity;
	struct _MULTI_NODE **Child;
}MULTI_NODE;


typedef struct _BINARY_MULTI_NODE
{
	int Key;
	struct _BINARY_MULTI_NODE *Left, *Right;
}BINARY_MULTI_NODE, *PBINARY_MULTI_NODE;

MULTI_NODE 
*MakeNode (int Key)
{
	MULTI_NODE *MNode = new MULTI_NODE;
	
	MNode -> Key = Key;
	MNode -> Size = 0;
	//MNode -> Capacity = 50;
	MNode -> Capacity = 1;
	MNode -> Child = (MULTI_NODE **) malloc(MNode-> Capacity* sizeof(MULTI_NODE*));
	return MNode;
}

void 
InsertMultiNode(MULTI_NODE *Parent,
				MULTI_NODE *Child)
{
	if(Parent != NULL && Child != NULL)
	{
		Parent -> Size ++;
		if(Parent ->Size > Parent -> Capacity)
		{
			Parent -> Capacity *= 2;
			Parent -> Child = (MULTI_NODE **)realloc(Parent -> Child, Parent -> Capacity);
		}
		Parent -> Child[Parent -> Size-1] = Child;
	}
}


MULTI_NODE*
TransformNode (int t[], int size)
{
	MULTI_NODE *Root;
	int i;
	MULTI_NODE **Nodes;

	Nodes = (MULTI_NODE**)malloc(size*sizeof(MULTI_NODE*));

	for(i = 0 ; i < size ; i++)
	{
		Nodes[i] = MakeNode (i);
	}

	for(i =0; i< size; i++)
	{
		if(t[i] != -1)
		{
			InsertMultiNode(Nodes[t[i]],Nodes[i]);
		}
		else 
		{
			Root = Nodes[i];
		}
	}

	return Root;
}

void 
PrettyPrint(
		MULTI_NODE *Root,
		int Nivel = 0
		)
{
	for(int i = 0; i < Nivel; i++ )
	{
		printf("     ");
	}
	
	printf("%d\n", Root->Key);

	for(int i =0; i < Root->Size; i++)
	{
		PrettyPrint(Root->Child[i], Nivel + 1);
	}
}


BINARY_MULTI_NODE * TransformNode(MULTI_NODE * Root)
{
	BINARY_MULTI_NODE *p;
	BINARY_MULTI_NODE *BRoot = new BINARY_MULTI_NODE;
	int i;

	BRoot->Key = Root->Key;
	BRoot->Left = NULL;
	BRoot->Right = NULL;

	if(Root->Size == 0)
	{
		return BRoot;
	}

	BRoot->Left = TransformNode(Root->Child[0]);
	
	p = BRoot->Left;
	for(i = 1; i <Root->Size;i++)
	{
		p->Right = TransformNode(Root->Child[i]);
		p=p->Right;
	}

	p->Right = NULL;

	return BRoot;
}

void PreatyPrint(BINARY_MULTI_NODE *Root,int Tab)
{
	int LocalTab;
	if(Root!= NULL)
	{
		LocalTab = Tab;
		
		while(LocalTab >0)
		{
			printf("     ");
			LocalTab --;
		}

		printf("%d \n",Root->Key);
				
		PreatyPrint(Root->Left,Tab+1);
		PreatyPrint(Root->Right,Tab);	
	}
}

int
main()
{
	int t[] = {9,5,5,9,-1,4,4,5,5,4,2};
	int size = sizeof(t)/(sizeof(t[0]));
	MULTI_NODE* Root;
	BINARY_MULTI_NODE *BRoot;
	Root = TransformNode (t,size); 
	PrettyPrint(Root);
	BRoot = TransformNode(Root);
	printf("======================================================\n");
	PreatyPrint(BRoot,0);
	return 0;
}

