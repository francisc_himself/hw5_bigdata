#include <stdlib.h>
#include <stdio.h>
#include <list>
#include<conio.h>
#include <list>
#include <iostream>
#include <cstdio>
using namespace std;

int size = 100; 
int time;
int count;

typedef struct node {

	int value;
	int color;
	int time1,time2;
	list<node*> neighbours;	
}node;

node **graph = new node*[size+1];

void link(int value1, int value2) 
{
	graph[value1]->neighbours.push_back(graph[value2]);	
	count++;
}

void dfs(node *n) {

	if (!n) return;
	n->color=1;
	n->time1 = time;
	list<node*>::iterator it;

	for( it = n->neighbours.begin(); it!= n->neighbours.end(); it++) {
		
		count++;
		if((*it)->color == 0 ){

			printf("Tree edge between %d - %d\n",(*it)->value,n->value);
			time++;
			dfs((*it));
		}

		else 
			if( (*it)->color == 1) 
				printf("Back edge between %d - %d\n",(*it)->value,n->value);

			if( (*it)->color == 2) 
				if( (*it)->time1 < n->time1)
					printf("Cross edge between %d - %d\n",(*it)->value,n->value);
				else 
					printf("Forward edge between %d - %d\n",(*it)->value,n->value);
	}
	n->color = 2;
	time++;
	n->time2 = time;
}


void main() {

	FILE *f;
	f = fopen("assignement10.csv","w");

	int i;
	int m ;
	for (m=10;m<=size;m+=10) {

		count=0;
		for (i=1;i<=m;i++) {
		
			graph[i] = new node;
			graph[i]->value = i;
			graph[i]->color = 0;
		}
	
		for( i=0; i<m*m/2;i++) 
			link(rand()%m+1,rand()%m+1);
		
		for( i=1;i<=m;i++ ) 
			graph[i]->neighbours.unique();

		fprintf(f,"%d;%d\n",m,count);
		

	}

	time=0;	
	dfs(graph[1]);	
	getch();
}
