#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

typedef struct nod{
	int dm;
	int data;
	struct nod *stg, *dr, *pr;
}TYPE;

int rez[10001];			//memorez nodul care iasa afara dupa numarare
int nrAtr, nrCmp;		//numar de atribuiri si numar de comparatii

nod* CAE(int st, int dr, int *dim){     //T(n) = 2T(n/2) + O(1) = O(n);
	int d1 = 0, d2 = 0, n = sizeof(nod);
	nod *q;
    nrCmp++;
    if (st > dr){
        *dim = 0;
   	    return NULL;
    }else{
        nrAtr = nrAtr + 5;
		q = (nod*) malloc(n);
	    q->data = (st + dr) / 2;
		q->stg = NULL;
		q->dr = NULL;
		q->dm = dr - st + 1;
		if(st < dr){
			q->stg = CAE(st, (st + dr) / 2 - 1, &d1);
			if(q->stg != NULL){
			    q->stg->pr = q;
			    nrAtr++;
            }
            q->dr = CAE((st + dr) / 2 + 1, dr, &d2);
            if(q->dr != NULL){
                q->dr->pr = q;
                nrAtr++;
            }
        }
    }
    nrAtr++;
    *dim = q->dm;
    return q;
}

TYPE *findMin(TYPE *p){
	while (p->stg != NULL){
		nrAtr++;
		p = p->stg;
	}
	return p;
}

TYPE *findSuccessor(TYPE *p){
	TYPE *q;
	nrCmp++;
	if (p->dr != NULL){
		return findMin(p->dr);
	}
	nrAtr++;
	q = p->pr;
	while (q != NULL && p == q->dr){
		nrAtr += 2;
		p = q;
		q = p->pr;
	}
	return q;
}

void scade_dim(nod *z){ 
	while (z != NULL){
      nrAtr = nrAtr+2;
	  z->dm--;
	  z = z->pr;
	}
}

void stergeArb(nod **r, nod *p){
	TYPE *q,*z;
	nrCmp = nrCmp+5;
	if (p->stg == NULL || p->dr == NULL){
		nrAtr++;
		q = p;
	}else{
		nrAtr++;
		q = findSuccessor(p);
	}
	if (q->stg != NULL){
		nrAtr++;
		z = q->stg;
	}
	else{
		nrAtr++;
		z = q->dr;
	}
	if (z != NULL){
		nrAtr++;
		z->pr = q->pr;
	}

	scade_dim(q->pr);
	if (q->pr == NULL){  //rad
		*r = z;
	}else{
		if (q == (q->pr)->stg){
			nrAtr++;
			(q->pr)->stg = z;
		}else{
			nrAtr++;
			(q->pr)->dr = z;
		}
	}

	if (q != p){
		nrAtr++;
		p->data = q->data;
	}
	free(q);
}

TYPE *SO_Selecteaza (TYPE *z, int indice){   //O(logn)
	int r;
	nrCmp = nrCmp + 3;
	nrAtr++;
	if (z->stg != NULL){
		r = ((z->stg)->dm) + 1;
	}else{
		r = 1;
	}
	if (r == indice){
		return z;
	}else{
		if (indice < r){
			return SO_Selecteaza (z->stg, indice);
		}else{
			return SO_Selecteaza(z->dr, indice - r);
		}
	}
}

void afisare_prietenoasa(TYPE *x, int d){

	if (x != NULL){
		afisare_prietenoasa(x->stg, d + 1);
		for (int i=1; i<=d; i++){
			printf (" ");
		}
		printf ("%d", x->data);
		afisare_prietenoasa(x->dr, d + 1);
	}
}

void Josephus(int n, int m){ //laborator
	int h, i, e = n, index=0;  //e este numarul de elemente din vector, h este dimensiunea arborelui
	nod *root, *x;
    root = CAE(1, e, &h);  //O(n)
    root->pr = NULL;
    i = m;
    while (e > 0){
        x = SO_Selecteaza(root, i);  //O(logn);
        rez[index] = x->data;
        index++;
        stergeArb(&root, x);		//O(logn)
        e--;
        if (e){
            i = (i + m - 1) % e;
            if (i == 0){
            i = e;
			}
        }
	}
}

int main ()
{
	char choose;

	printf("D - Demonstratie    R - Rezolva\n");
	scanf("%c", &choose);
	printf("\n");
	switch(choose){
	case 'D':
		Josephus(7,3);
		for(int i = 0; i < 7; i++){
			printf("...iesi afara chiar si tu: %d\n", rez[i]);
		}
		break;
	case 'R':
		int n, m;
		FILE *df = fopen ("Josephus.txt","w");
		FILE *dff = fopen ("Josephus.csv","w");
		printf("Va rugam asteptati generarea rezultatelor.\n");
		for (n = 100; n <= 10000; n = n + 100){
			m = n / 2;
			nrCmp = 0;
			nrAtr = 0;
			Josephus(n, m);
			fprintf (df, "%d   %d   %d   %d\n", n, nrAtr, nrCmp, nrAtr+nrCmp);
			fprintf (dff, "%d   %d   %d   %d\n", n, nrAtr, nrCmp, nrAtr+nrCmp);
		}
	}
	printf("Apasati o tasta pentru terminarea programului.");
	getche ();
	return 0;
}