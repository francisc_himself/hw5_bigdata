/*
Hash tableurile fac posibil cautarile rapide pentru 
date mari. Reduce cautarea si inserarea la o complexitate 
constanta. 

Pana la factorul de umplere 50%, elementele se gasesc aprope instant.

Cand factorul de umplere este mai mare de 80%, performanta 
se degrada foarte rapid.

In cazul in care factorul de umplere este prea mare
cel mai simplu este recreeazrea tabelului de dispersie
cu marimi mai mari.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//9973
//ce ati face v


int counter;

class sht
{
	int length;
	int* data;
	int* end;
	int fill;


public:
	sht(int length)
	{
		fill = 0;
		this->length = length;
		data = new int[length];
		memset(data,0,sizeof(int)*length);
		end = data+length;
	}

	~sht()
	{
		delete[] data;
	}

	int hash(int x)
	{
		return x%length;
	}

	void insert(int item)
	{
		int* search_start = data + hash(item);
		int* p = search_start;
		
		while (true)
		{
			if (*p == 0) 
			{
				*p = item;
				fill++;
				counter++;
				break;
			}

			p++;
			counter++;

			if (p == end)
				p = data;

			if (p == search_start)
			{
				fprintf(stderr,"HASH TABLE FULL");
				break;
			}
		}
	}
	
	bool contains(int item)
	{
		int* search_start = data + hash(item);
		int* p = search_start;
		
		while (true)
		{
			counter++;

			if (*p == 0)
				return false;

			if (*p == item) 
				return true;

			p++;
			
			if (p == end)
				p = data;

			if (p == search_start)
				return false;
		}
	}

	float getFillFactor()
	{
		return (float)fill/length;
	}

	int getLength()
	{
		return this->length;
	}

};


int contain_count(int *vec_ptr, unsigned length, int item)
{
	int c=0;
	for (int i=0; i<length; i++)
	{
		if (vec_ptr[i] == item)
		{
			c++;
		}
	}
	return c;
}

void make_unique_ints(int* vec_ptr, unsigned length)
{
	for (int i=0;i<length; i++)
	{
		while (contain_count(vec_ptr,length,vec_ptr[i])>1)
		{
			vec_ptr[i]++;
		}
	}
}


void main()
{

	{
		sht t(9973);

		t.insert(4);
		if (t.contains(4))
		{
			printf("it works\n");
		}

		if (!t.contains(5))
		{
			printf("it really works\n");
		}

		printf("fill factor %f\n",t.getFillFactor());
	}

	//filling factor, avg effort found, max effort found, avg effort not found, max effort not found

	FILE *out = fopen("test.txt","w");
	fprintf(out,"filling factor, avg effort found, max effort found, avg effort not found, max effort not found\n");

	for (float fill_factor = 0.01f; fill_factor <= 1.00f; fill_factor += 0.01)
	{
		printf("%.2f ",fill_factor);

		sht t(9973);
		int to_insert = fill_factor * t.getLength();
		int *inserted = new int[to_insert];

		for (int i=0; i<to_insert; i++)
			inserted[i] = rand()%(9973*16);

		make_unique_ints(inserted,to_insert);

		for (int i=0; i<to_insert; i++)
			t.insert(inserted[i]);
		
		fprintf(out,"%.2f,",fill_factor);


		{
			int max_effort = 0;
			int sum_effort = 0;
			double avg_effort = 0.0;
		
			for (int i=0; i<to_insert; i++)
			{
				counter = 0;
				t.contains(inserted[i]);

				if (max_effort<counter) 
					max_effort = counter;

				sum_effort += counter;
			}

			avg_effort = sum_effort/(double)to_insert;

			fprintf(out,"%.2f,%d,",avg_effort,max_effort);
		}

		{
			int max_effort = 0;
			int sum_effort = 0;
			double avg_effort = 0.0;
		
			for (int i=0; i<to_insert; i++)
			{
				counter = 0;
				t.contains(rand()%(9973*16)+(9973*16));

				if (max_effort<counter) 
					max_effort = counter;

				sum_effort += counter;
			}

			avg_effort = sum_effort/(double)to_insert;

			fprintf(out,"%.2f,%d,",avg_effort,max_effort);
		}

		fprintf(out,"\n");

		delete[] inserted;
	}

	printf("\n");

	fclose(out);

}