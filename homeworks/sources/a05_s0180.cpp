// assignment5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

#define dim 9973
#define m 3000

fstream f("hashtable.csv",ios::out);
long H[dim];
long searchH[3001];

int found_total_effort, notFound_total_effort, max_found, max_notFound, nr_elem_found, nr_elem_notFound;
double found_medium_effort, notFound_medium_effort;

int hashFunction(long k,int i)
{
    int hprim;
    int x;
    hprim=k%dim;
    x=(hprim+(1*i)+(2*i*i))%dim;
    return x;
}

void add(long k)										
{
    int i=0;
    int j=hashFunction(k,i);							
    while ((H[j]!=-1) && (i<dim) )				 		
    {
        i++;
        j=hashFunction(k,i);
    }

    if (i!=dim)
    {
														
        H[j]=k;                     					
    }
    else cout<<"Overflow";
}

void init(long H[],int size)
{
    for (int i = 0 ; i < size ; i++)
        H[i] = -1;
}

int search(long k)
{
    int i=0;
    int j=hashFunction(k,i);
    while ((H[j]!=-1)&&(H[j]!=k) && (i<dim))			
    {
        i++;
        j=hashFunction(k,i);
    }

    if ( (H[j]==-1) || (i == dim) )
    {													
        nr_elem_notFound++;								
        notFound_total_effort+=i;							
        max_notFound=max(max_notFound,i);	
        return -1;
    }

    max_found = max(max_found,i);
    nr_elem_found++;
    found_total_effort+=i;	
	return j;											
}

double fill_factor [] = {0.8, 0.85, 0.9, 0.95, 0.99 };			

int main ()
{ 
   int n;
    long x,y;
    srand((unsigned)time(NULL));
    int cnt_aux=0;

    f<<"Filling factor"<<","<<"Avg. effort FOUND"<<",";
    f<<"Max. effort FOUND"<<","<<"Avg. effort NOT FOUND"<<",";
    f<<"Max. effort NOT FOUND"<<endl;

    for (int i=0; i<5; i++)								
    {
        found_medium_effort=0;
        notFound_medium_effort=0;
        for (int k=1; k<=5; k++)						
        {	
            n=(int)(fill_factor[i]*dim);						
            init(H,dim);                			    
            found_total_effort=notFound_total_effort=nr_elem_found=nr_elem_notFound=0;
            cnt_aux=0;
            for (int j=1; j<=n; j++ )					
            {
                x = (rand()*rand())%100000;
                add(x);
                if (j%7==0)
                {
                    searchH[cnt_aux++] = x;
                }
            }
            for (int j = cnt_aux ; j <=m;j++)
                {
                    searchH[j] = rand() + 100000;
                }
            for (int j=1; j<=m; j++)					
            {
                y = search(searchH[j]);					

            }
            found_medium_effort+=((double)found_total_effort/(double)nr_elem_found);		
            notFound_medium_effort+=((double)notFound_total_effort/(double)nr_elem_notFound);
        }
        found_medium_effort = found_medium_effort /5;				
        notFound_medium_effort = notFound_medium_effort /5;
        f<<fill_factor[i]<<","<<found_medium_effort<<","<<max_found<<","<<notFound_medium_effort<<","<<max_notFound<<endl;
    }
	
	//demo
 /*	int n=7;
	init(H,n);
	add(20);
	add(7);
	add(-4);
	add(3);
	add(21);
	add(29);
	for (int i=0; i<n; i++)
	{
		cout<<H[i]<<" ";
	}
	cout<<endl;
	int j=search(19);  //not found
	cout<<j<<endl;
	j=search(24);  //found
	cout<<j;
	getch(); */
    return 0;
}


