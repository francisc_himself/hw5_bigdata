/**
 ** Assignment No. 5: Search Operation in Hash Tables
 **			- Open Addressing with Quadratic Probing -
 **
 **
 ** Name: ***ANONIM*** ***ANONIM*** Mihai
 ** Group: ***GROUP_NUMBER***
 ** 
 ** Implementation:
 ** 
 ** 	You are required to implement correctly and efficiently the insert 
 **		and search operations in a hash table using open addressing and 
 **		quadratic probing.
 **
 **
 ** 
 **********************************************************************
 ********************** Open Adressing Evaluation *********************
 ** 	
 **		a<n/m<1, a = load factor
 **
 **	� Theorem
 **		average unsuccessful search time is 1/(1- a)
 **	� Theorem
 **		average successful search time is 1/a ln(1/(1- a))
 **
 **********************************************************************
 **
 ** Conclusions:
 **
 **	When an incoming data hash value indicates it should be stored in an 
 ** already-occupied slot, we use Quadratic probing, which is an open addressing
 ** scheme in computer programming for resolving collisions in hash tables. 
 ** Quadratic probing operates by taking the original hash index and adding 
 ** successive values of an arbitrary quadratic polynomial until an open slot is found.
 **
 ** This is an example of the programme's output:
 **	 0.8 :     avg1:0.93       max1:34         avg2:7.29       max2:55
 **	0.85 :     avg1:1.21       max1:45         avg2:10.67      max2:54
 **	 0.9 :     avg1:1.87       max1:84         avg2:18.87      max2:90
 **	0.95 :     avg1:3.06       max1:90         avg2:28.46      max2:154
 **	0.99 :     avg1:3.39       max1:257        avg2:105.04     max2:866
 **
 **
 ** We can observe that the situation gets worse when the filling factor
 ** is getting very close to 100%. As we get closer and closer to 100%,
 ** the efforts of the search procedure are increasing on a higher rate.
 ** 
 **/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <string.h>
#include <math.h>

#define N 9973
#define M 3000
#define ALFAL 8000
#define ALFAH 11000
#define RND 30000
#define B 32 

int t[N];
int n;
int test[3000];

typedef struct cell
{
  char name[20];
} Hash;

int f( char *key )
{
  int i, sum;
  sum = 0;
  for ( i = 0; i < strlen( key ); i++ )
    sum += key[ i ];
  if (sum<0)
    sum=-sum;
  return( sum % B );
}

int QuadraticProbing( char *key, int i )
{
    return (f(key)+(i*i+i)/2)%B; 
}

int LinearProbing( char *key, int i )
{
    return (f(key)+i)%B; 
}

void printArray( int *x, int n )
{
	for (int i=0; i<n; i++)
	{
		printf("%d ", x[i]);
	}
	printf("\n");
}

int HashFind(Hash *table[], char *key)
{
    int index;
    int i;
    for (i = 0; i < B; i++)
    {
        index = QuadraticProbing(key,i);
        if (table[index]!= NULL && strcmp(table[index]->name, key) == 0) // match
                return index; 
    }
    return -1;
}

int HashInsert(Hash *table[], char *key)
{
    int index;
    int i;

    for (i = 0; i < B; i++)
    {
        index = QuadraticProbing(key,i);
        if (table[index]==NULL)
        {
            Hash *he = (Hash*)malloc(sizeof(Hash));
            strcpy(he->name,key);
            table[index] = he;
            return 1; 
        }
    }
    return 0;
}

void Clear(Hash *table[])
{
    int i;
    for (i = 0; i < B; i++)
        table[i] = NULL;
}

int h(int k, int i)
{
    int hprim, x;

    hprim = k % N;
    x = (hprim+(1*i)+(2*i*i)) % N;
    return x;
}

int main() 
{	
	/***************** Sample input *******************/
	/*
		Hash* table[B];

		char input[1000];
		char param[20];
		int return_value;

		Clear(table);
  
        printf(" Insertion\n");
		gets(input);
		char *c = strchr(input,' ');
		c++;
        strncpy(param,c,50);
		return_value = HashInsert(table, param);
        printf("OK\n");
		gets(input);
		char *d = strchr(input,' ');
		d++;
        strncpy(param,d,50);
		return_value = HashInsert(table, param);
        printf("OK\n");
		gets(input);
		char *e = strchr(input,' ');
		e++;
        strncpy(param,e,50);
		return_value = HashInsert(table, param);
        printf("OK\n");
		getch();

	
		printf(" Search\n");
		gets(input);
        return_value = HashFind(table, param);
        printf("Key %s found at index %d\n", param, return_value);
        getch();
   
*/
   
	/********************* Output DATA *****************/

	srand(time(NULL));
	
	int x[N];
	int j,i,k,l,l2;
	
	int maxI1=0, maxI2=0;
	float avgI1=0, avgI2=0;
	int sum1=0, sum2=0;
	double fill[5] = {0.8, 0.85, 0.9, 0.95, 0.99};


	for ( j = 0; j < 5; j++ ) 
	{
		//Reset the array
		for( l = 0; l < N; l++ )
		{
			x[l] =- 1;
		}

		sum1=0;
		sum2=0;
		avgI1=0;
		avgI2=0;
		maxI1=0;
		maxI2=0;

		//Fill the array
		for( l = 0; l < (fill[j]*N); l++ ) 
		{
			k = rand() % RND;
			i = 0;
			while ( x[h(k,i)] !=- 1)
			{
				i++;
			}
			x[h(k,i)] = k;

		}

		//Simulate another 1500 elements already in the array
		l = 0;		
		while ( l < M / 2)
		{
			l2 = rand() % N;
			while ( x[l2] == -1 ) 
			{
				l2 = rand() % N;
			}
			i = 0;
			while ( x[h(x[l2], i)] != x[l2] )
			{
				i++;
			}
			if (maxI1 < i) 
			{
				maxI1 = i;
			}
			sum1 += i;
			l++;
		}
		avgI1 = (float)sum1 / (M/2);

		//Simulate another 1500 elements newly generated
		l=0;
		while ( l < M/2 )
		{
			k=rand() % RND + RND;
			i = 0;
			while (x[h(k,i)]!=k & x[h(k,i)]!=-1) 
			{
				i++;
			}
			if ( maxI2 < i ) 
			{
				maxI2 = i;
			}
			sum2 += i;
			//avgI2=(avgI2*l+i)/(l+1);
			l++;
		}
		avgI2 = (float)sum2 / (M/2);
		
		printf("%d : \tavg1:%.2f \tmax1:%d \tavg2:%.2f \tmax2:%d", j, avgI1, maxI1, avgI2, maxI2 );

		//fprintf(fd,"%d,%.2f\n",j,(float)j/N);
		printf("\n");
	}
	if (i < 0)    // avoid negative value in case of overflow !!!
	i=-i;

	//printArray(x,N);
	printf("Done");
	getch();
	return 0;
}