// laborator2.cpp : Defines the entry point for the console application.
//Toti cei 3 algoritmi implementati au complexitatea O(n^2)
//Comentarii se gasesc pe parcursul codului.
//#include <stdafx.h>
#include <stdio.h> 
#include <conio.h>
#include <stdlib.h>
#include <time.h>;
#define MAX 10000

long comparatie_intersch,comparatie_selectie,comp_insertie_1,atribuire_insertie,comp_insertie_2;
long A[MAX];
long B[MAX];
long C[MAX];
long copie_1[MAX];
long copie_2[MAX];

void interschimbare(long A[], long n)
{
  comparatie_intersch=0;
  long aux;
  long ordonat=0;
  while (ordonat==0)
  {
	  ordonat=1;
	  for(long j=1;j<n;j++)//se fac n-1 parcurgeri =>complexitate O(n)
	  {
       comparatie_intersch=comparatie_intersch+1;
       if(A[j]>A[j+1]) //se face un test de vecinatate, comparatia fiind operatia critica
	   {
		   ordonat=0;
		   aux=A[j];
		   A[j]=A[j+1];
		   A[j+1]=aux;
	   }
	  }
	  //dupa o parcurgere, elementul maxim se afla pe ultima pozitie
	  //la final complexitatea va fi O(n^2)
  }
}

void selectie(long A[],long n)
{
//sirul va fi impartit in doua. Prima parte(destinatia) va fi mereu ordonata, iar a doua(sursa) nu.
	
	comparatie_selectie=0;
	long aux;
	long imin;
	for(long i=1;i<n;i++)
	 {
       imin=i;
	   for(long j=i+1;j<=n;j++)
	   {
		   comparatie_selectie=comparatie_selectie+1;
		   if(A[j]<A[imin])//operatia critica este comparatia
			   imin=j;
	   }
	   //am gasit pozitia elementului minim din a doua parte(sursa) si il voi transfera(minimul) in destinatie
       aux=A[i];
	   A[i]=A[imin];
	   A[imin]=aux;
	   
	}
	
}

void insertie(long A[], long n)
{
	//sirul va fi impartit in doua. Prima parte(destinatia) va fi mereu ordonata, iar a doua(sursa) nu.
	//cazul in care se fac cele mai putine operatii critice este atunci cand sirul dat are elementele in ordine descrescatoare
	comp_insertie_1=0;
	comp_insertie_2=0;
	atribuire_insertie=0;
	long x,j,k;
	for(long i=2;i<=n;i++)
	 {
		x=A[i];
		j=1;
		comp_insertie_1=comp_insertie_1+1;
	  while (A[j]<x)//una din operatiile critice este comparatia
	  {
		j=j+1;
		comp_insertie_2=comp_insertie_2+1;
	  }
	  //compar elementul de pe pozitia i cu elementele din prima parte a sirului(destinatie)
	  for(k=i;k>=j+1;k--)
	  {
		  atribuire_insertie=atribuire_insertie+1;//cealalta operatie critica este atribuirea
		  A[k]=A[k-1];
	  }
      A[j]=x;
	 }
//pentru a imbunatati cautarea, se poate folosi cautarea binara, care are eficienta logaritm in baza 2 din n
	//insertia simpla are complexitatea O(n^2)
}

void main()
{
	long i;
    long k;
	FILE *h=fopen("favorabil.csv","w");
	FILE *g=fopen("defavorbil.csv","w");
	FILE *f=fopen("random5.csv","w");
	
	fprintf(f,"n, nr_comp_interschimbare,nr_comp_selectie, suma_op_critice_insertie\n");
	k=100;
	while(k<=10000)//generez siruri cu nr de elemente de la 100 pana la 10 000, cu pasul 100
	{
		srand(time(NULL));
	for(i=1;i<=k;i++)
      A[i]=rand()%100;//genereaza numere random
	for(i=1;i<=k;i++)
	{
		copie_1[i]=A[i];
		copie_2[i]=A[i];
	}
		interschimbare(A,k);
		selectie(copie_1,k);//daca as fi lasat A, sirul era deja ordonat,in urma apelului metodei de interschimbare
		insertie(copie_2,k);//daca as fi lasat A, sirul era deja ordonat,in urma apelului metodei de selectie
		fprintf(f,"%ld, %ld, %ld, %ld",k,comparatie_intersch,comparatie_selectie,(comp_insertie_1+comp_insertie_2+atribuire_insertie));
		fprintf(f,"\n");
		k=k+100;
	}
	fclose(f);
	
	fprintf(g,"n, nr_comp_interschimbare, nr_comp_selectie, suma_op_critice_insertie\n");
	k=100;
	while(k<=10000)
	{
	
	for(i=1;i<=k;i++)
		B[i]=k-i+1;
		interschimbare(B,k);
	for(i=1;i<=k;i++)//mai generez o data sirul B pentru ca acesta a fost ordonat, in urma apelului metodei de interschimbare
		B[i]=k-i+1;
		selectie(B,k);
	for(i=1;i<=k;i++)//mai generez o data sirul B pentru ca acesta a fost ordonat, in urma apelului metodei de selectie
		B[i]=k-i+1;
		insertie(B,k);
		fprintf(g,"%ld, %ld, %ld, %ld",k,comparatie_intersch,comparatie_selectie,(comp_insertie_1+comp_insertie_2+atribuire_insertie));
		fprintf(g,"\n");
		k=k+100;
	}
	fclose(f);
	fclose(g);
	fprintf(h,"n, nr_comp_interschimbare, nr_comp_selectie, suma_op_critice_insertie\n");
	k=100;
	while(k<=10000)
	{
	
	for(i=1;i<=k;i++)
		C[i]=i;
		interschimbare(C,k);
	for(i=1;i<=k;i++)//mai generez o data sirul B pentru ca acesta a fost ordonat, in urma apelului metodei de interschimbare
		C[i]=i;
		selectie(C,k);
	for(i=1;i<=k;i++)//mai generez o data sirul B pentru ca acesta a fost ordonat, in urma apelului metodei de selectie
		C[i]=i;
		insertie(C,k);
		fprintf(h,"%ld, %ld, %ld, %ld",k,comparatie_intersch,comparatie_selectie,(comp_insertie_1+comp_insertie_2+atribuire_insertie));
		fprintf(h,"\n");
		k=k+100;
	}
	fclose(h);
//exemple pentru siruri cu nr de elemente mic, pentru a testa metodele implementate	
	for(int i=1;i<=5;i++)
		A[i]=rand()%100;
	printf("Sirul inainte de sortarea prin interschimbare \n");
	for(int i=1;i<=5;i++)
	{
		printf("%d ",A[i]);
	}
	interschimbare(A,5);
		printf("\nSirul dupa sortarea prin interschimbare \n");
	for(int i=1;i<=5;i++)
	{
		printf("%d ",A[i]);
	}
	
	for(int i=1;i<=5;i++)
		A[i]=rand()%100;
	printf("Sirul inainte de sortarea prin selectie \n");
	for(int i=1;i<=5;i++)
	{
		printf("%d ",A[i]);
	}
	selectie(A,5);
		printf("\nSirul dupa sortarea prin selectie \n");
	for(int i=1;i<=5;i++)
	{
		printf("%d ",A[i]);
	}

	for(int i=1;i<=5;i++)
		A[i]=rand()%100;
	printf("Sirul inainte de sortarea prin insertie \n");
	for(int i=1;i<=5;i++)
	{
		printf("%d ",A[i]);
	}
	insertie(A,5);
		printf("\nSirul dupa sortarea prin insertie \n");
	for(int i=1;i<=5;i++)
	{
		printf("%d ",A[i]);
	}
	getch();
}

