/*
***ANONIM*** ***ANONIM*** ***ANONIM***
Grupa ***GROUP_NUMBER***

Arbori multicai
Arborii multicai sunt cei in care numarul descendentilor unui nod este mai mare decat 2. Unui nod i se ataseaza un numar
de m chei, ordonate crescator, numarul descendentilor fiind de m+1. Cele m informatii atasate unui nod formeaza o pagina.

In rezolvarea algoritmului am inceput cu introducerea unui vector de tati si am facut 2 transformari, una din reprezentarea 
bazata pe vectorul de parinti in reprezentarea bazata pe arbore multicai si una din  reprezentarea bazata pe vectorul de parinti
in reprezentarea bazata pe arbore binar.

Cele 2 transformari sunt realizate in O(n).
*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

//structura arbore binar
typedef struct _NODE
{
    int cheie;
    struct _NODE *stg;
    struct _NODE *dr;
}NODE;

//structura pentru multinod
typedef struct _MULTI_NODE{
	int key;
	int count;
	struct _MULTI_NODE *child[50];
}MULTI_NODE;

//functia de creare a arborelui multinod
MULTI_NODE* createMN(int key)
{
	MULTI_NODE *x=(MULTI_NODE*)malloc(sizeof(MULTI_NODE));
	x->key=key;
	x->count=0;
	return x;
}

//functia de inserare in multinod
void insertMN(MULTI_NODE *parent, MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
}

//functie de transformare vector de tati in arbore multicai
MULTI_NODE *transform1(int t[], int size)
{
	MULTI_NODE *root=NULL;
	MULTI_NODE *nodes[50];
	
	//cream noduri
	for(int i=0;i<size;i++)
		nodes[i]=createMN(i);
	
	//creare legaturi
	for(int i=0;i<size;i++)
	{
		if(t[i]!=-1)
			insertMN(nodes[t[i]],nodes[i]);
		else
			root=nodes[i];
	}
	return root;
}

//cea de a doua functie de transformare
NODE *transform2(MULTI_NODE *nod)
{
	NODE *q;
	NODE *p = (NODE*)malloc(sizeof(NODE));
    int i;

	p->cheie = nod->key;
    p->stg = NULL;
    p->dr = NULL;
	
	if(nod->count>0)
	{
		p->stg = transform2(nod->child[0]);
		q=p->stg;
		for(i=1;i<nod->count;i++)
		{
			q->dr = transform2(nod->child[i]); 
			q=q->dr;
        }
    }
	return p;
}

//afisare prietenoasa a arborelui multicai
void prettyPrint(MULTI_NODE *nod, int nivel)
{
	for(int i=0;i<nivel;i++)
		printf("    ");
	printf("%d\n",nod->key);
	for(int i=0;i<nod->count;i++)
		prettyPrint(nod->child[i],nivel+1);
}

//afisare prietenoasa a arborelui binar
void afisare_arbore_binar(NODE *rad, int depth)
{
    int k;

	if(rad!=NULL)
	{
		afisare_arbore_binar(rad->stg,depth+1);
		for(k=0;k<depth;k++)
			printf("\t");
		printf("%d \n", rad->cheie);
		afisare_arbore_binar(rad->dr,depth+1);
	}
}

//functia principala
int main()
{
	int t[]={3,0,9,-1,5,6,3,2,9,3,6};
	int size=sizeof(t)/sizeof(t[0]);
	
	//prima transformare
	printf("Arborele dupa prima transformare arata in felul urmator\n");
	MULTI_NODE *root=transform1(t,size);
	prettyPrint(root,0);

	//a doua transformare
	printf("Arborele dupa a doua transformare arata in felul urmator\n");
	NODE *root1=transform2(root);
	afisare_arbore_binar(root1,0);

	_getch();
	return 0;
}