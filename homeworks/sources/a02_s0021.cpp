/* ***ANONIM*** ***ANONIM***cu ***ANONIM***,grupa ***GROUP_NUMBER***
Din punct de vedere al graficului generat, in cazul bottom-up numarul de atribuiri si comparatii este mai mic decat in
cazul top-down, dar top-down este eficient in cazul lucrului cu inserari.
*/

#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

const int NMIN=100; //minimul si maximul
const int NMAX= 10000; 
const int STEP=100;
int cmp,atr,cmp1,atr1,cmp2,atr2;

int parinte(int i){
	return i/2;
}
int st(int i){
	return 2*i;
}
int dr(int i){
return 2*i+1;
}

void afisare_sir(int a[],int dim_a){
for(int i=1;i<=dim_a;i++)
	printf("%d ",a[i]);
printf("\n");
}

//va returna indexul minim dintre un parinte si fii
int index_min(int a[],int i,int dim_a){
     int imin=i;
     cmp+=1;
    if((st(i)<=dim_a) &&(a[i]>a[st(i)]))
		imin=st(i);

	cmp+=1;
	if((dr(i)<=dim_a) &&(a[imin]>a[dr(i)]))
		imin=dr(i);
	
	return imin;
}


//functia de reconstructie a heap-ului
void reconstituie_heap(int a[],int i,int dim_a){
	int index,aux;
	
	index=index_min(a,i,dim_a);
	
	if(index!=i){
	  atr+=3;
	  aux=a[i];
	  a[i]=a[index];
	  a[index]=aux;
	  reconstituie_heap(a,index,dim_a);
	
}
}


//h_pop are rol de a extrage minimul
void h_pop(int a[],int dim_a){
	int x;
	x=a[1];
	atr+=1;
	a[1]=a[dim_a];
	dim_a=dim_a-1;
	reconstituie_heap(a,1,dim_a);

}
//h_push are rol de introducere a unui el in heap
void h_push(int a[],int x, int *dim_a){
   int i,aux;
	(*dim_a)+=1;
	atr+=1;
	a[(*dim_a)]=x;
	i=(*dim_a);
	while(i>1 && a[parinte(i)]>a[i]){
		cmp+=1;
		atr+=3;
		aux=a[parinte(i)];
		a[parinte(i)]=a[i];
		a[i]=aux;
		i=parinte(i);
	
	}
}
//construieste heap bu
void construire_heap(int a[],int dim_a){
for(int i=dim_a/2;i>=1;i--)
	reconstituie_heap(a,i,dim_a);
}
//construire heap_bu
void heap_bu(int a[],int dim_a){
	int aux,m;
	 m=dim_a;//m- dimHeap
	construire_heap(a,dim_a);
	//afisare_sir(a, m);
	for(int i=dim_a;i>=2;i--){
		atr+=3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		m--;
		reconstituie_heap(a,1,m);
	}
}

//construire heap_td
void heap_td(int b[],int dim_b){
	int i;
	int dim_h = 1;
	for(i=2;i<=dim_b;i++)
	{ 
		h_push(b,b[i],&dim_h);
	}
	//afisare_sir(b, dim_b);
}

void genereaza_rand(int a[],int dim_a,int parametru_srand)
{
	int i;

	srand(parametru_srand);
	for(i=0;i<dim_a;i++)
		a[i]=rand()%10000;
}

int main()
{
	/*
	//am realizat testarea
	int a[7]={0,7,2,5,1,3,4};
	int b[7]={0,1,2,4,7,3,5};
	int c[7];
	afisare_sir(a,6);
	printf("\n");
	//heap_bu(a,6);
	heap_td(b,6);
	//afisare_sir(c,6);
	getch();*/
	 
	int ok,d;
  int x[NMAX];
  int a[NMAX];
  char filename[40];
  strcpy(filename,"heap1_heap2");
  strcat(filename,".csv");
  FILE *f=fopen(filename,"w+");
  for (int n=NMIN;n<NMAX;n+=STEP)
  {
	  cmp1=0;  cmp2=0;
	  atr1=0;  atr2=0;
	  for (int i=0; i<5; i++)
	  {
        genereaza_rand(a,n,i);
	    for (int j=1; j<n+1; j++) x[j]=a[j];
		  cmp=0; atr=0; d=n;
	      heap_bu(a,n);
		  cmp1=cmp1+cmp/5;
		  atr1=atr1+atr/5;

		  for (int j=0; j<n; j++) a[j]=x[j];
		  d=n;
		  cmp=0; atr=0;
		  heap_td(a,n);
		  cmp2=cmp2+cmp/5;
	      atr2=atr2+atr/5;
	
	  }
	 fprintf(f,"%d , %d, %d, %d, %d, %d, %d \n",n,cmp1,atr1,cmp1+atr1,cmp2,atr2,cmp2+atr2);
  }
  getch();
  return 0;
}






