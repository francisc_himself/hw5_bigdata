
/*
***ANONIM*** ***ANONIM***
grupa ***GROUP_NUMBER*** sg 2
Tema 7 - reprezentarea unui arbore multicai


r1- reprezentare parinte sub forma de matrice
r2- multicai reprezentare in preordine
r3- arborele binar sub forma de pretty print
*/


#include<conio.h>
#include<stdio.h>
#include<stdlib.h>

int T[]={0, 2, 7, 5, 2, 7, 7,-1, 5, 2}, a[8][8];

typedef struct arbore
{
	int cheie;
	int nrfii;
	struct arbore *fii[];
}nod1;

typedef struct multicai
{
	int cheie;
	struct multicai *st_fiu, *dr_frate;
}nod2;

nod1 *rad1=(nod1 *)malloc(sizeof(nod1));
nod2 *rad2=(nod2 *)malloc(sizeof(nod2));

void r1()
{
	int j;

	for(int i=0; i<10; i++)
	{
		j=0;
		if(T[i]==-1) a[0][0]=i;
		else {	while(a[T[i]][j]!=0) j++;
				a[T[i]][j]=i;
			 }
	}
}

void r2(nod1* p)
{
	int c=p->cheie;
	int j=0, f;

	while(a[c][j]!=0)
	{
		nod1 *q=(nod1 *)malloc(sizeof(nod1));
		q->cheie = a[c][j];
		q->nrfii = 0;
		f=p->nrfii;
		p->fii[f] = q;
		p->nrfii++;
		r2(q);
		j++;
	}
}

void r3(nod1* p, nod2* q)
{
	int f = p->nrfii, j=0;
	q->cheie=p->cheie;

	if(f!=0)
	{
		nod2 *x=(nod2 *)malloc(sizeof(nod2));
		x->cheie=p->fii[j]->cheie;
		j++;
		x->st_fiu=NULL;
		x->dr_frate=NULL;
		q->st_fiu=x;
		f--;

		while(f!=0)
		{
			nod2 *y=(nod2 *)malloc(sizeof(nod2));
			y->cheie = p->fii[j]->cheie;
			j++;
			y->st_fiu=NULL;
			y->dr_frate=NULL;
			x->dr_frate=y;
			x=y;
			f--;
		}

		nod2 *z=(nod2 *)malloc(sizeof(nod2));
		z=q->st_fiu;
		for(int i=0; i<p->nrfii; i++)
		{
			r3(p->fii[i], z);
			z=z->dr_frate;
		}
	}
}

int nivel=0;


void afis_matrice()
{
	for(int i=0; i<8; i++)
	{
		for(int j=0; j<8; j++)
			printf("%d ", a[i][j]);
		printf("\n ");
	}
}

void preord(nod1 *p)
{
	if(p!=NULL)
	{
		printf("%d ", p->cheie);
		for(int i=0; i<p->nrfii; i++)
			preord(p->fii[i]);
	}
}

void pretty_print(nod2* p)
{
	if (p == NULL )
		return;
	for(int i=0; i<nivel; i++)
		printf("   ");

	if (p->cheie != NULL )
		printf("%d\n", p->cheie);

	if(p->st_fiu != NULL)
	{
		nivel++;
		pretty_print(p->st_fiu);
		nivel--;
	}

	while(p->dr_frate != NULL)
	{
		pretty_print(p->dr_frate);
		p=p->dr_frate;
	}
}

void main()
{
	printf(" ");
	r1();
	afis_matrice();

	printf("\n ");

	rad1->cheie = a[0][0];
	rad1->nrfii = 0;
	r2(rad1);
	preord(rad1);


	rad2->cheie = rad1->cheie;
	r3(rad1, rad2);
	printf("\n");
	printf("\n ");
	pretty_print(rad2);

	getch();
}