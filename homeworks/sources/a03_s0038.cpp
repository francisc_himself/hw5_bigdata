#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<time.h>
#include<cstdlib>

int dimensiune_heap,lungime,oper_qmed=0,oper_hmed=0,oper_qfav=0,oper_qdefav=0;
int b[20];

int partitionare(int a[],int st,int dr)
{
	int pivot,i,j,aux;
	oper_qmed++;
	pivot=a[dr];
	i=st-1;
	for(j=st;j<=dr-1;j++)
	{
		oper_qmed++;
		if(a[j]<=pivot)
		{
			i++;
			oper_qmed+=3;
			aux=a[i];
			a[i]=a[j];
			a[j]=aux;
		}
	}
	oper_qmed+=3;
	aux=a[i+1];
	a[i+1]=a[dr];
	a[dr]=aux;
	return i+1;

}
void quickSort(int a[],int st,int dr)
{
	int m;
	if(st<dr)
	{
		m=partitionare( a, st, dr);
		quickSort(a,st,m-1);
		quickSort(a,m+1,dr);
	}
}

//-------------------------------------------------heapsort
int parinte(int i)
{
	return i/2;
}

int stanga(int i)
{
	return 2*i;
}

int dreapta(int i)
{
	return 2*i+1;
}

void reconstituie_heap(int a[],int i)
{
	int l,r,maxim,aux;
	l=stanga(i);
	r=dreapta(i);
	oper_hmed++;
	if(l<=dimensiune_heap && a[l]>a[i])
		maxim=l;
	else
		maxim=i;
	oper_hmed++;
	if(r<=dimensiune_heap && a[r]>a[maxim])
		maxim=r;
	if(maxim!=i)
	{
		aux=a[i];
		a[i]=a[maxim];
		a[maxim]=aux;
		oper_hmed+=3;
		reconstituie_heap(a,maxim);
	}
}

void construieste_heap(int a[])
{
	dimensiune_heap=lungime;
	for(int i=lungime/2;i>=1;i--)
		reconstituie_heap(a,i);
}

void heapsort(int a[])
{
	int aux;
	construieste_heap(a);
	for(int i=lungime;i>=2;i--)
	{
		oper_hmed+=3;
		aux=a[1];
		a[1]=a[i];
		a[i]=aux;
		dimensiune_heap--;
		reconstituie_heap(a,1);
	}
}

void citire_vector(int a[],int n)
{
	for(int i=1;i<=n;i++)
	{
		printf("dati a[%d] ",i);
		scanf("%d",&a[i]);
		b[i]=a[i];
	}	
}

void afisare(int a[],int n)
{
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
}

void generare_aleator_mediu()
{
	int a[10001],b[10001],c[10001],fav[10001];

	FILE *f=fopen("cazmediu.csv","w");
	fprintf(f,"nr elemente,nr operatii heapsort,nr operatii quicksort caz mediu,nr operatii quicksort caz defavorabil,nr operatii quicksort caz favorabil\n");
	for(lungime=100;lungime<=10000;lungime+=100)
	{
		oper_qmed=0;//quicksort caz mediu
		oper_hmed=0;//heapsort caz mediu
		oper_qdefav=0;//quicksort caz defavorabil
		oper_qfav=0;
		for(int j=1;j<lungime;j++)
		{
			c[j]=j;//crescator
			fav[j]=j;
		}
		fav[lungime]=lungime/2;
		quickSort(fav,1,lungime);
		oper_qfav=oper_qmed;
		oper_qmed=0;
		quickSort(c,1,lungime);
		oper_qdefav=oper_qmed;
		oper_qmed=0;
		for(int k=1;k<=5;k++)
		{
			srand(time(NULL));
			for(int j=1;j<=lungime;j++)//generare sir
			{
				a[j]=rand();
				b[j]=a[j];
			}
			srand(1);
			heapsort(a);
			quickSort(b,1,lungime);
		}
		
		fprintf(f,"%d,%d,%d,%d,%d\n",lungime,oper_hmed/5,oper_qmed/5,oper_qdefav,oper_qfav);
		
	}
}



int main()
{
	int a[100],n=8;
	lungime=n;
	//generare_aleator_mediu();
	citire_vector(a,n);
	printf("\nquicksort\n");
	quickSort(a,1,n);
	printf("\nheapsort\n");
	heapsort(b);
	afisare(b,n);
	
	getch();
	return 0;

}