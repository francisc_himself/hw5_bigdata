#include<iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include<conio.h>
#include"Profiler.h"

Profiler profiler("demo");
#define INF INT_MIN
int edgesNo, visited[110];
struct node {
	int key;
	int rank;
	node * next;
	node * prev;
};
struct node *sets[60001][2]; 
struct edge { 
	int node1;
	int node2;
};
void buildSet(int node)   //the set for v
{
	struct node* x = (struct node *)malloc(sizeof(node));
	x->rank = 1;
	x->key = node;
	x->prev= x;
	x->next = NULL;
	sets[node][0] = x;
	sets[node][1] = x;
}
int findSet(int x)
{
	return  (sets[x][0]->prev)->key;
}
void unify( int u, int v)
{
	u = findSet(u);
	v = findSet(v);

	if( sets[u][0]->rank >= sets[v][0]->rank )  
	{
		sets[v][0]->rank = INF;  
		struct node *aux = sets[v][0];

		while( aux != NULL )
		{
			aux->prev = sets[u][0];
			sets[u][0]->rank++;
			sets[u][1]->next = aux;
			sets[u][1] = aux;
			aux = aux->next;
		}
		sets[v][1] = NULL;
	}
	else
	{
		sets[u][0]->rank = INF;
		struct node *aux = sets[u][0];
		while( aux != NULL )
		{
			aux->prev = sets[v][0];
			sets[v][0]->rank++;
			sets[v][1]->next = aux;
			sets[v][1] = aux;
			aux = aux->next;
		}
		sets[u][1] = NULL;
	}
}
void connectedComponents( edge *edges, int edgesN)
{
	for( int i = 0; i < 10001; i++)
	{
		buildSet(i);  // generating the sets
		profiler.countOperation("operations",edgesNo);
	}
	for( int i = 0; i < edgesN; i++)
	{
		if( findSet(edges[i].node1) != findSet(edges[i].node2)) 
		{
			unify(edges[i].node1,edges[i].node2); // unification of the sets
			profiler.countOperation("operations",edgesNo);
		}
		profiler.countOperation("operations",edgesNo,2); 
	}
}
int mat_aux[10001][10001];

void generateEdges( edge *edges, int edgesN)
{
	int count=0;
	for (int i =0; i< 10000; i++) // initialize the edges with 0
        for (int j =0; j< 10000; j++)
            mat_aux[i][j]=0;

	while (count < edgesN)
	{
		
		int v1 = rand()%10000;
		int v2 = rand()%10000;
		if (mat_aux[v1][v2]== 0 && mat_aux[v2][v1]== 0 && v1!=v2)
        {
            mat_aux[v2][v1] = 1;
            mat_aux[v1][v2] = 1;
			edges[count].node1 = v1; // add to the edges array
            edges[count].node2 = v2;
            count++;
        }
	}
}
int main()
{
	
	struct edge edges[60001];
	edgesNo = 10;
	printf("Small size: \n");
	generateEdges(edges, edgesNo);
	connectedComponents(edges, edgesNo);
	for(int i=0;i<edgesNo;i++){
		printf("%d %d \n",edges[i].node1,edges[i].node2);
	}
	getche();
    for (edgesNo = 10000; edgesNo<60000; edgesNo+=1000)
    {
        generateEdges(edges,edgesNo);
        connectedComponents(edges,edgesNo);
        printf("%d\n",edgesNo);
    }
	profiler.createGroup("Disjoint","operations");
	profiler.showReport();
	return 0;
}