/************************************************************************************
*																					*
*     Assignment 1                                                                  *
*     Moldovan ***ANONIM***															        *
*     Group ***GROUP_NUMBER***																	*
*																					*
*																					*		
*	  Observations:																	*
*			Average case:															*
*			the assignments of the bubble and insertion sort have a quadratic growth*
*			while the selection sort's assignments grow linearly. Comparisons are   *
*			all 3 quadratic but insertion is more efficient							*
*																					*
*			Best case:																*
*			All of the algorithms grow linearly when it comes to assignments, but   *
*			when it comes to comparisons selection sort has a quadratic behaviour   *
*			rather than a linear one like bubble and insertion sort algorithms      *
*																					*
*			Worst case:																*
*			when it comes to assignments, selection is the only one growing linearly*
*			while the other two grow quadratically. When comparing, all of them		*
*			are alike, growing quadratically.										*
*																					*
*		I have noticed that when counting operations using the Profiler				*
*		it tends to take significantly longer, that is why i count operations		*
*		using integer variables, and then apply a single countOperation call for    *
*		those variables.															*
************************************************************************************/
#include<fstream>
#include<iostream>
#include<conio.h>
#include<ctime>
#include "Profiler.h"

using namespace std;

Profiler profiler("demo");

int compare,assign;


void selection(int a[], int n)
{
	for(int flag = 0 ; flag < n - 1; flag++)
	{
	
		int pos = flag;
		for(int i = flag + 1; i < n ; i++)
		{
			compare++;
			if(a[i] < a[pos])
				pos = i;
		}
		if(pos != flag)
		{
			int aux = a[pos];
			a[pos] = a[flag];
			a[flag] = aux;
			assign+=3;
		}
	}
}
void insertion(int a[], int n)
{
	for(int flag = 1 ; flag < n ; flag++)
	{
	
		for(int j = flag; j > 0&& compare++ && a[j] < a[j-1] ; j--)
		{
			
			int aux = a[j];
			a[j] = a[j-1];
			a[j-1] = aux;
			
			assign+=3;
		}
		
	}

	

}

void bubble(int a[],int n)
{
	int lowerBound = 0;
	int higherBound = n;
	bool exchangeDone = true;
	while(exchangeDone)
	{
		exchangeDone = false;
		for(int i = lowerBound ; i < higherBound - 1; i++)
		{
			compare++;
			if(a[i] > a[i+1])
			{	
				int aux = a[i];
				a[i] = a[i+1];
				a[i+1] = aux;
			assign+=3;
				exchangeDone = true;
			}
		}
		for(int i = higherBound - 1; i > lowerBound ; i--)
		{
						compare++;

			if(a[i] < a[i-1])
			{	
				int aux = a[i];
				a[i] = a[i-1];
				a[i-1] = aux;
							assign+=3;

				exchangeDone = true;
			}
		}
		lowerBound++;
		higherBound--;
	}
	
}

int main()
{
	int n, a[100005],z,aux[10005];
	
	//Average case
	for(z=0;z<5;z++)
		for(n=100;n<=10000;n+=500)
		{
			compare=assign=0;
			cout<<n<<endl;
			FillRandomArray(a,n,10,50000);
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			selection(aux,n);
			profiler.countOperation("CMP selection", n, compare);
			profiler.countOperation("ASS selection", n, assign);
			compare=assign=0;
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			insertion(aux,n);
			
			profiler.countOperation("CMP insertion", n, compare);
			profiler.countOperation("ASS insertion", n, assign);
			compare=assign=0;
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			bubble(aux,n);
			
			profiler.countOperation("CMP bubble", n, compare);
			profiler.countOperation("ASS bubble", n, assign);
		}
		profiler.addSeries("sumSelection", "CMP selection", "ASS selection");
		profiler.addSeries("sumInsertion", "CMP insertion", "ASS insertion");
		profiler.addSeries("sumBubble", "CMP bubble", "ASS bubble");
		profiler.createGroup("sumOverall", "sumSelection", "sumInsertion", "sumBubble");
		profiler.createGroup("cmpOverall", "CMP selection", "CMP insertion", "CMP bubble");
		profiler.createGroup("assOverall", "ASS selection", "ASS insertion", "ASS bubble");
		profiler.showReport();


//Best case

	for(n=100;n<=10000;n+=500)
		{
			compare=assign=0;
			cout<<n<<endl;
			FillRandomArray(a,n,10,50000,false,1);
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			selection(aux,n);
			profiler.countOperation("CMP selection", n, compare);
			profiler.countOperation("ASS selection", n, assign);
			compare=assign=0;
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			insertion(aux,n);
			
			profiler.countOperation("CMP insertion", n, compare);
			profiler.countOperation("ASS insertion", n, assign);
			compare=assign=0;
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			bubble(aux,n);
			
			profiler.countOperation("CMP bubble", n, compare);
			profiler.countOperation("ASS bubble", n, assign);
		}
		profiler.addSeries("sumSelection", "CMP selection", "ASS selection");
		profiler.addSeries("sumInsertion", "CMP insertion", "ASS insertion");
		profiler.addSeries("sumBubble", "CMP bubble", "ASS bubble");
		profiler.createGroup("sumOverall", "sumSelection", "sumInsertion", "sumBubble");
		profiler.createGroup("cmpOverall", "CMP selection", "CMP insertion", "CMP bubble");
		profiler.createGroup("assOverall", "ASS selection", "ASS insertion", "ASS bubble");
		profiler.showReport();
		

	//Worst case
	

		for(n=100;n<=10000;n+=500)
		{
			compare=assign=0;
			cout<<n<<endl;
			FillRandomArray(a,n,10,50000, false, 1);
			a[n-1]=0;
			selection(aux,n);
			profiler.countOperation("CMP selection", n, compare);
			profiler.countOperation("ASS selection", n, assign);
			compare=assign=0;
			FillRandomArray(a,n,10,50000, false, 2);
			insertion(aux,n);
			
			profiler.countOperation("CMP insertion", n, compare);
			profiler.countOperation("ASS insertion", n, assign);
			compare=assign=0;
			for(int i=0;i<n;i++)
				aux[i]=a[i];
			bubble(aux,n);
			
			profiler.countOperation("CMP bubble", n, compare);
			profiler.countOperation("ASS bubble", n, assign);
		}
		profiler.addSeries("sumSelection", "CMP selection", "ASS selection");
		profiler.addSeries("sumInsertion", "CMP insertion", "ASS insertion");
		profiler.addSeries("sumBubble", "CMP bubble", "ASS bubble");
		profiler.createGroup("sumOverall", "sumSelection", "sumInsertion", "sumBubble");
		profiler.createGroup("cmpOverall", "CMP selection", "CMP insertion", "CMP bubble");
		profiler.createGroup("assOverall", "ASS selection", "ASS insertion", "ASS bubble");
		profiler.showReport();
		return 0;
}