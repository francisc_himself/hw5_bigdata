﻿/*                           ***ANONIM*** ***ANONIM*** ***GROUP_NUMBER***--BFS
Eficienta O(|V|+|e|)
 În teoria grafurilor, cautare prin lățime (BFS), este o strategie de căutare într-un grafic atunci când căutare este limitată la esență două operațiuni: 
 (a) sa viziteze și să inspecteze un nod din-trunun grafic; 
 (b) permite accesul de vizitare a nodurilor care au cavecin pe nodul vizitat în prezent.
 BFS începe de la un nod rădăcină și inspectează toate nodurile vecine. 
 Apoi, pentru fiecare dintre aceste noduri vecine, la rândul său, se inspectează nodurile vecine care au fost Unvisited, și așa mai departe. 
 Compara BFS cu echivalent, dar mai economică pentru memorie iterative aprofundarea adancime de căutare și contrast cu adancime de căutare.

 Algoritmul utilizează o structură de date coadă pentru a stoca rezultatele intermediare, deoarece traverseaza graficul, după cum urmează:

 1     Puneți în coadă nodul rădăcină
 2     Examineaza nodul
      -   Dacă elementul căutat este găsit în acest nod, iesim din căutare și  returnam un rezultat.
      -   În caz contrar, Puneți în coadă de succesori (nodurile copil directe), care nu au fost încă descoperite.
 3    În cazul în care coada este goală, fiecare nod pe grafic a fost examinat - ieși din căutare și a reveni "nu a fost găsit".
 4   În cazul în care coada nu este gola, repetați de la pasul 2.
 */
 #include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<conio.h>


#define White 0
#define Grey 1
#define Black 2
#define INF 10000

typedef struct vrf
{
	int color, dist, parinte;
} VRF;


typedef struct NOD
{
    int val;
    struct NOD* next;
}NOD;

typedef struct Queue 
{
	int val;
	struct Queue *next;
}QUEUE;

QUEUE *start;
NOD *head[60001], *tail[60001];
VRF vrf[60001];
int V[100001];
int timp;
int k, op;
int QUE[10001], nrq;
int nivel=1;

int Q[60001];

void BFS (int s, int nrv)
{
	int u;
	for (u=1; u<=nrv; u++)
		if (u!=s)
		{
			vrf[u].color=White;
			vrf[u].dist=INF;
			vrf[u].parinte=0;
		}
	vrf[s].color=Grey;
	vrf[s].dist=0;
	vrf[s].parinte=0;
	k=1;
	Q[k]=s;
	op+=5;
	printf("%d \n", s);
	while(k!=0)
	{
		u=Q[1];
		nivel++;
		for (int i=1; i<k; i++)
		{
			Q[i]=Q[i+1];
		}
		k--;
		NOD *temp;
		temp=head[u];
		op++;
		while (temp!=NULL)
		{
			int v=temp->val;
			temp=temp->next;
			op+=3;
			if (vrf[v].color==White)
			{
				vrf[v].color=Grey;
				vrf[v].dist=vrf[u].dist+1;
				vrf[v].parinte=u;
				k++;
				Q[k]=v;
				for(int i=0; i<nivel; i++)
					printf(" ");
				printf("%d \n", v);
				op+=4;
			}
		
		}
		
		vrf[u].color=Black;
		op++;
	}
}



bool verif (int x, int y)
{
	NOD *p;
	p=head[x];
	while(p!=NULL)
	{
		if(y==p->val)
			return false; 
		p=p->next;
	}
	return true;
}

void genereaza(int nrv,int nrm)  //nrv= numarul de varfuri, nrm = numarul de edges
{
	int i,u,v;
	for(i=0;i<=nrv;i++)
		head[i]=tail[i]=NULL;
	i=1;
	while (i<=nrm)
	{
		u=rand()%nrv+1;
		v=rand()%nrv+1;
		if (u!=v)
		{
			if(head[u]==NULL)
			{
				head[u]=(NOD *)malloc(sizeof(NOD *));
				head[u]->val=v;
				head[u]->next=NULL;
				tail[u]=head[u];
				i++;
			}
			else 
			{
				if (verif (u, v))
				{
					NOD *p;
					p=(NOD *)malloc(sizeof(NOD *));
					p->val=v;
					p->next=NULL;
					tail[u]->next=p;
					tail[u]=p;
					i++;
				}
			}
		}
	}
}


void print(int nrv)
{
	int i;
	NOD *p;
	for(i=1;i<=nrv;i++)
	{
		printf("%d : ",i);
		p=head[i];
		while(p!=NULL)
		{
			printf("%d ",p->val);
			p=p->next;
		}
		printf("\n");
	}
}


int main()
{
	int nrm, nrv;
	FILE *f;
	nrv=10;
	nrm=20;
	srand(time(NULL));
	genereaza(nrv, nrm);
	print(nrv);
	BFS(1, nrv);
	/*
	f=fopen ("BFS.txt", "w");
	fprintf(f, "nrv  numar operatii\n");
	for (nrm = 1000; nrm+1<6000; nrm=nrm+100)
	{
	
		genereaza(nrv, nrm);
		for (int i=0,i<v,i++)
		
		BFS(nrv);
		fprintf(f, "%d %d\n", nrv, operatii);
	}
	fclose(f);
	*/
	getch(); 
    return 0;
}