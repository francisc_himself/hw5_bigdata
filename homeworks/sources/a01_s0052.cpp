
/****ANONIM*** ***ANONIM***-***ANONIM*** grupa ***GROUP_NUMBER***
	
		Pentru cazul mediu statistic , din punct de vedere al numarului de asignari si comparatii, 
	eficienta sortarii prin selectie este cea mai mare,iar bubble sort e cel mai putin eficient.
		Pentru cazul in care vectorul este sortat crescator, algoritmul bubble este in cazul favorabil,
	deoarece nu are loc nicio asignare. Numarul decomparatii este aproximativ egal pentru cele trei
	metode de sortare. Diferenta dintre numarul de asignari ale algoritmului de insertie si al selectiei
	este relativ mica. Bubble sort este cel mai eficient ,dar nu cu mult mai eficient decat celelalte 
	doua metode.
		Pentru cazul in care vectorul este sortat descrescator, algoritmul de sortare prin insertie este 
	situat in cazul favorabil, deoarece nu are loc nicio comparatie, iar bubble-sort e in cazul cel mai
	defavorabil. Diferenta de eficienta dintre selectie si insertie este mica.
		In opinia mea , observand cele trei cazuri , algoritmul de selectie este cel mai eficient.

*/

#include <stdio.h>
#include <stdio.h>

#include "Profiler.h"
Profiler profiler("demo");

#define MAX 10000


void bubble(int *a,int n)
{ 
	int aux;
	for(int i=1;i<n-1;i++)
	{
		for(int j=0;j<n-i;j++)
		{
			if(a[j]>a[j+1])
			{
				aux=a[j];
				a[j]=a[j+1];
				a[j+1]=aux;
				profiler.countOperation("bubble_assign",n,3);
			}
			profiler.countOperation("bubble_comp",n);
		}
	}
}
void insertion(int *a,int n){
	int x,j;
	for (int i=1;i<n;i++)
	{
	     x=a[i];
		 j=0;
		while (a[j]<x)
		{ 
			j++;
			profiler.countOperation("insertion_comp",n);
		}
			for(int k=i;k>=j+1;k--)
		{	
		
			a[k]=a[k-1];
			profiler.countOperation("insertion_assign",n,1);
		}
		a[j]=x	;
		profiler.countOperation("insertion_assign",n,2);
	}
}

void selection(int *a,int n)
{
	int aux;
	for(int i=0;i<n-1;i++)
	{
		int im=i;
		for(int j=i+1;j<n;j++)
		{
			if(a[j]<a[im])
			{ 
				im=j;			
			}	
			profiler.countOperation("selection_comp",n,1);
		}
		profiler.countOperation("selection_assign",n,3);
		aux=a[im];
		a[im]=a[i];
		a[i]=aux;
	}
}
void main(){
	int v[MAX],v1[MAX];
	srand(time(NULL));
	

	                                               //sortari penru vectorul random
	for(int n=100;n<1000;n+=100){
		printf("n=%d\n",n);
		FillRandomArray(v,n);
		memcpy(v1,v,n*sizeof(int));
			bubble(v1,n);
		memcpy(v1,v,n*sizeof(int));
			insertion(v1,n);
		memcpy(v1,v,n*sizeof(int));
			selection(v1,n);
	}


													//sortari pentru vectorul crescator
	//for(int n=100;n<1000;n+=100){
	//	printf("n=%d\n",n);
	//	FillRandomArray(v,n,10,50000,false,1);
	//	memcpy(v1,v,n*sizeof(int));
	//		bubble(v1,n);
	//	memcpy(v1,v,n*sizeof(int));
	//		insertion(v1,n);
	//	memcpy(v1,v,n*sizeof(int));
	//		selection(v1,n);
	//}

													//sortari pentru vectorul descrescator
	//for(int n=100;n<1000;n+=100){
	//	printf("n=%d\n",n);
	//	FillRandomArray(v,n,10,50000,false,2);
	//	memcpy(v1,v,n*sizeof(int));
	//		//printf(v[i],"\n");
	//		bubble(v1,n);
	//	memcpy(v1,v,n*sizeof(int));
	//		insertion(v1,n);
	//	memcpy(v1,v,n*sizeof(int));
	//		selection(v1,n);
	//}

	profiler.createGroup("assign","bubble_assign","insertion_assign","selection_assign");
	profiler.createGroup("comp","bubble_comp","insertion_comp","selection_comp");
	profiler.addSeries("bubbleSeria","bubble_assign","bubble_comp");
	profiler.addSeries("InsertionSeria","insertion_assign","insertion_comp");
	profiler.addSeries("SelectionSeria","selection_assign","selection_comp");
	profiler.createGroup("SERIA","bubbleSeria","InsertionSeria","SelectionSeria");
	profiler.showReport();

	// verificarea corectitudinii algoritmilor
	/*int v[]={3,5,1,2,4};
	int v1[]={3,5,1,2,4};
	int v2[]={3,5,1,2,4};

	bubble(v,5);
	for(int i=0;i<5;i++)
		printf("%d ",v[i]);
	printf("\ninsertie:");
	insertion(v1,5);
	for(int i=0;i<5;i++)
		printf("%d ",v1[i]);
	printf("\nselectie:");
	selection(v2,5);
	for(int i=0;i<5;i++)
		printf("%d ",v2[i]);*/
}