/* Assign3_***ANONIM***_***GROUP_NUMBER***.cpp
	Heap_Sort - este mai lent decat Quick_Sort pentru cazul mediu statistic
	pentru cazul defavorabil Quick_Sort este lent, are o complexitate de n^2
	pentru cazul favorabil Quick_Sort are complexitate liniara, deci sirul va fi sortat foarte repede
	Heap sort are tot timpul complexitatea O(n log n) 
*/
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static long long nr_op;

void siftDown(int numbers[], int root, int bottom) {
  int maxChild = root * 2 + 1;
  nr_op++;
  if(maxChild < bottom) {
    int otherChild = maxChild + 1;
    nr_op++;
    maxChild = (numbers[otherChild] > numbers[maxChild])?otherChild:maxChild;
  } else {
    nr_op++;
    if(maxChild > bottom) return;
  }
  nr_op++;
  if(numbers[root] >= numbers[maxChild]) return;  
  int temp = numbers[root];
  numbers[root] = numbers[maxChild];
  numbers[maxChild] = temp;
	nr_op++;
  siftDown(numbers, maxChild, bottom);
}


void heapSort(int numbers[], int array_size) {
  int i, temp;
 
  for (i = (array_size / 2); i >= 0; i--) {
    siftDown(numbers, i, array_size - 1);
  }
 
  for (i = array_size-1; i >= 1; i--) {
   
    temp = numbers[0];
    numbers[0] = numbers[i];
    numbers[i] = temp;
	nr_op+=3;;
 
    siftDown(numbers, 0, i-1);
  }
}
 
void swap(int *x,int *y)
{
   int temp;
   temp = *x;
   *x = *y;
   *y = temp;
   nr_op+=3;;
}
 
int choose_pivot(int i,int j )
{
   return(rand()%(j-i)+i);
}

int choose_pivotw(int i,int j )
{
   return(i);
}
 
void quicksort(int list[],int m,int n)
{
   int key,i,j,k;
   if( m < n)
   {
      k = choose_pivot(m,n);
      swap(&list[m],&list[k]);
	  
      key = list[m];
	  nr_op++;
      i = m+1;
      j = n;
      while(i <= j)
      {
		  nr_op++;
		  if((i <= n) && (list[i] <= key))
		  while((i <= n) && (list[i] <= key)) {                
			  i++;          
		  nr_op++;}
		  nr_op++;
		  if((j >= m) && (list[j] > key))
			  while((j >= m) && (list[j] > key)){
				  j--;
				  nr_op++;
				}

         if( i < j)
                swap(&list[i],&list[j]);
      }
      
      swap(&list[m],&list[j]);
      
      quicksort(list,m,j-1);
      quicksort(list,j+1,n);
   }
}

void quicksortb(int list[],int m,int n)
{
   int key,i,j,k;
   if( m < n)
   {
      k = choose_pivotw(m,n);
      swap(&list[m],&list[k]);
	  
      key = list[m];
	
      i = m+1;
      j = n;
      while(i <= j)
      {
		  nr_op++;
		  if((i <= n) && (list[i] <= key))
		  while((i <= n) && (list[i] <= key)) {                
			  i++;          
		  nr_op++;}
		  nr_op++;
		  if((j >= m) && (list[j] > key))
			  while((j >= m) && (list[j] > key)){
				  j--;
				  nr_op++;
				}

         if( i < j)
                swap(&list[i],&list[j]);
      }
      
      swap(&list[m],&list[j]);
      
      quicksort(list,m,j-1);
      quicksort(list,j+1,n);
   }
}

bool IsSorted(int arr[], int size){
		int i;
		for(i=1; i<size; ++i){
			if(arr[i] < arr[i-1]){
				return false;
			}
		}
		return true;
	}


int main()
{
	const int MAX_ELEMENTS = 10003;
   int list[MAX_ELEMENTS],mlist[MAX_ELEMENTS];
	srand(NULL);
   int i = 0;
   int n = 0;
   long long nr_oh,nr_oq;
   FILE *fout=fopen("Q_H_Sort.csv","w");
 
   // generate random numbers and fill them to the list
   for(n=0;n<=10000;n+=400){
   for(i = 0; i < MAX_ELEMENTS; i++ ){
       list[i] = rand();
   }
   for(i=0;i<n;i++)
   mlist[i]=list[i];
   nr_op=0;
   heapSort(list,n);
   if (n%500==0){
	if(IsSorted(list,n)){
		printf("ok");
	}else printf("fail");
	}
   nr_oh=nr_op;
   nr_op=0;
   for(i=0;i<n;i++)
   list[i]=mlist[i];
	quicksort(list,0,n);
	if (n%500==0){
	if(IsSorted(list,n)){
		printf("ok");
	}else printf("fail");
	}
	nr_oq=nr_op;
	 nr_op=0;
	 for(i=0;i<n;i++)
	 list[n-i-1]=mlist[i];
	quicksortb(list,0,n);
	if (n%500==0){
	if(IsSorted(list,n)){
		printf("ok");
	}else printf("fail");
	}

	long long nr_ow=nr_op;

	 //for(i=0;i<n;i++)
	 //list[i]=mlist[i];
	quicksortb(list,0,n);
	if (n%500==0){
	if(IsSorted(list,n)){
		printf("ok");
	}else printf("fail");
	}

	fprintf(fout,"%d,%lld,%lld,%lld \n",n,nr_oh,nr_oq,nr_op,nr_ow);

	}
	return 0;
}