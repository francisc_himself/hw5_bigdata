/*
 Tabela de dispersie este o structura de date care permite cel mai bun timp de cautare intr-o multime neordonata
 Principala diferenta fata de un vector obisnuit este modul in care se determina pozitia (adresa) unde se 
 introduce un nou element. O valoare noua nu se adauga in prima pozitie libera ci intr-o pozitie care
 sa permita regasirea rapida a acestei valori (fara o cautare prin vector).

 ***ANONIM*** Ionut
 grupa ***GROUP_NUMBER***

*/
#include  "stdio.h"
#include  "stdlib.h"
#include "conio.h"
#include "Profiler.h"

Profiler profiler ("demo");
#define HeapSize 10007;

int gasit=0, negasit = 0, max_gasit = 0, max_negasit = 0;
int A[ 10007 ], sc[3001], td[10007];
int length, cautat,gas, negas;
double alfa[]={0.8,0.85,0.9,0.95,0.99};
int n,m=3000;
int atrib , comp , comp1;
int N = 10007; 

// functia de dispersie definita in Cormen la pagina 208
int h ( int elem , int i){
	int j=0;
	j = ( elem % N + 1*i + 2* i*i ) % N;
	return j;
}

//functia de inserare in tabelul de dispersie
// se incearca inserarea valorii 'k' in tabelul de dispersie
//Cormen, pag 207
int hashInsert( int k){
	int i = 0, j;
	do {
		j = h ( k , i );
		if (A [ j ] == 0)
		{
			A[j] = k;
			return j;
		}
		else i++;
	} while ( i != length);
	printf("\nNu mai sunt pozitii libere in tabela de dispersie");
	return -2;
}
//functia pentru cautarea unui element in tabela de dispersie
//numarul de operatii care au fost efectuate pentru ca elementul sa poata fi cautat va fi intors prin parametrul &c
//Cormen, pag 207
int search ( int element){
	int i = 0;
	int j;
	do {
		j = h ( element , i );
		if ( A [ j ] == element )
		{
			gas = i+1;
			 return j;
		}
		i++;
	} while ( ( i !=  N) && ( A[j] != 0 ));
	negas = i+1;
	return -2;
}

void genDate(){
	int  poz, size;
	for(int k=0; k<5; k++)
	{
		size = alfa[k] * N; 
		
		max_gasit = 0;
		max_negasit = 0;
		for(int j=0; j<5;j++)
		{
		//initializam tabela cu valori de 0 pe fiecare pozitie de fiecare data
		for ( int i = 0 ; i < 10007 ; i++ )
			A [ i ] = 0;
		//generam elementele care sa fie inserate in tabela de dispersie
		FillRandomArray(td,size,1,50000,true);
		//inseram elementele in tabela de dispersie; insertia va fi intrurupta in cazul in care nu vor mai fi pozitii in tabela
		for(int i=0; i<size; i++)
		{
			poz = hashInsert(td[i]);
			if(poz == -2) break;
		}
		//definim elementele sirului de cautare
		//a doua jumatate a vectorului cu elemente pe care le cautam in tabela de dispersie este umpluta cu valori care sa fie in tabela
		for(int j=(m+1)/2; j<m; j++)
			sc[j] = A[j];
		//in prima jumatate a sirului de cautare sunt puse elemente din alt interval decat cele din tabela
		FillRandomArray(sc,m/2,50000,100000,true);

		gasit=0;
		negasit = 0;
		gas = 0;
		negas = 0;

		for(int i=0; i<size; i++)
		{
			poz = search(sc[i]);
			if(poz == -2)
			{
				negasit += negas;
				if(negas > max_negasit) max_negasit = negas;
			}
			else {
				gasit += gas;
				if(gas > max_gasit) max_gasit = gas;
			}
	}
		}
		
		
		printf("%f         %lf         %d         %lf          %d\n",alfa[k],(double)gasit/m,max_gasit,(double)negasit/m, max_negasit);
		
		
	}
}
void main(){
	genDate();
	getche();
}