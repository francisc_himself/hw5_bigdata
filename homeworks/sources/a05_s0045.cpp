/*
***ANONIM*** ***ANONIM***, ***GROUP_NUMBER***
Complexitatea algoritmului este O(hash_size) din conditia de bucla: while (t[j] != NULL && i<hash_size).
Efortul mediu de cautare in cazul elementelor prezente in hash table este chiar bun
pentru fiecare valoare a factorului de umplere (efortul mediu este aproximativ 2,3 sau 4).
Efortul mediu de cautare in cazul elementelor care nu sunt prezente in hash table
creste mult mai mult cu cresterea valorii factorului de umplere (eforturile medii sunt 6-9-14-32-98).
Privind rezultatele obervam ca hash table-urile sunt rentabili din punct de vedere a operatiei de cautare
cu exceptia cazului cand factorul de umplere este foarte mare (de ex 0.99) - adica tabelul este aproape plin.
*/

#include <stdio.h>
#include <conio.h>

#include "Profiler.h"

#define hash_size 9973
#define search_size 3000

double alfa;
int n; //nr elemente de inserat/inserate
int samples[hash_size+search_size];
int indices[search_size/2+1];
int t[hash_size]; //hash array
double alfas[]={0.8, 0.85, 0.9, 0.95, 0.99};

int effortF=0; //effort found
int effortNF=0; //effort not found
int buf;
double avgF=0;
double avgNF=0;

int h_aux(int k)
{
	return (k % hash_size);
}

int h(int k, int i)
{
	int c1=1,c2=2;
	int ret=(h_aux(k)+c1*i+c2*i*i) % hash_size;
	
	return ret;
}

int	hash_insert(int *t, int k)
{
	int i=0;
	do
	{
		int j=h(k,i);
		if (t[j] == NULL)
		{
			t[j]=k;
			return j;
		}
		else
		{
			i++;
		}
	}
	while (i<hash_size);
	printf("hash table overflow\n");
}

void afis(int *t, int size)
{
	printf("Hash table:\n");
	for (int i=0; i<size; i++)
	{
		printf("%d ", t[i]);
	}
	printf("\n");
}

int	hash_search(int *t, int k)
{
	int i=0,j;
	do
	{
		j=h(k,i);
		if (t[j] == k)
		{
			effortF++;
			effortNF++;

			return j;
		}
		
		effortF++;
		effortNF++;

		i++;
	}
	while (t[j] != NULL && i<hash_size);
	return NULL;
}

void init()
{
	for (int i=0; i<hash_size; i++)
	{
		t[i]=0;
	}
}

void main()
{
	FILE *f=fopen("assign5.csv", "w");

	fprintf(f,"Filling factor,Avg. Effort - found,Max Effort - found,Avg. Effort - not_found,Max Effort - not_found\n");

	//test inserare
	alfa=0.8;
	n=(int)(alfa*hash_size);
	printf("n=%d\n", n);

	FillRandomArray(samples, n+search_size/2, 1, 1000000, true, 0);

	for (int j=0; j<n; j++)
	{
		hash_insert(t, samples[j]);
	}

	//afis(t,hash_size);

	int aux=hash_search(t, samples[1]);
	printf("Numarul cautat: %d\nGasit pe pozitia: %d\n", samples[1], aux);
	////

	//generare date
	for (aux=0; aux < (sizeof(alfas)/sizeof(double)); aux++) 
	{
		init();
		avgF=avgNF=0;

		alfa=alfas[aux];
		n=(int)(alfa*hash_size);
		printf("n=%d\n", n);

		//inserare
		FillRandomArray(samples, n+search_size/2, 1, 1000000, true, 0);

		for (int j=0; j<n; j++)
		{
			hash_insert(t, samples[j]);
		}

		//cautare
		FillRandomArray(indices, search_size/2, 0, n-1, true, 0);

		//elem care se gasesc
		effortF=0;
		effortNF=0;
		
		for (int i=0; i<(search_size/2-1); i++)
		{
			hash_search(t,samples[indices[i]]);
		}

		avgF+=effortF;
		buf=effortF;

		//elem care nu se gasesc
		effortF=0;
		effortNF=0;

		for (int i=n; i<(n+search_size/2); i++)
		{
			hash_search(t,samples[i]);
		}

		avgNF+=effortNF;

		avgF/=search_size/2;
		avgNF/=search_size/2;
		
		fprintf(f, "%.2f,%.2f,%d,%.2f,%d\n",alfa,avgF,buf,avgNF,effortNF);
	}

	fclose(f);
}