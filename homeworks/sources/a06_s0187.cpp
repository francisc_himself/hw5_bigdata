#include "stdio.h"
#include "conio.h"
#include "stdlib.h"
#include<math.h>
#include<fstream>
//order statistic-tree: este de fapt un arbore rosu-negru cu informatii suplimentare in fiecare nod: nodul din stanga,
//nodul din dreapta,dimensiunea arborelui

typedef struct nod
			{ 
			  int mijloc,dimensiune;
			  nod *stanga,*dreapta,*parinte;
		    } NOD;
FILE *f;
int a[10000];
int comparatii,atribuiri;

NOD* frunza_cea_mai_din_stanga(NOD *arbore)
{
	while (arbore->stanga!=NULL)
	{
		atribuiri += 1;
        arbore=arbore->stanga;
	}
  return arbore;
}
NOD* frunza_cea_mai_din_dreapta(NOD *arbore)
{
	while (arbore->dreapta!=NULL)
	{
		atribuiri += 1;
        arbore=arbore->dreapta;
	}
  return arbore;
}
void parcurgereInordine(NOD *t, int nivel)
{
	if(t!=NULL)
	{
		parcurgereInordine(t->stanga, nivel+1);
		for(int i=0;i<nivel;i++)
			printf("\t");
		printf("%d \n",t->mijloc);
		parcurgereInordine(t->dreapta,nivel+1);
	}
}
NOD *stergeArbore(NOD *t)
{
	if(t!=NULL)
	{
		stergeArbore(t->stanga);
		stergeArbore(t->dreapta);
		free(t);
		t = NULL;
	}
	return NULL;
}

int inaltime(NOD *t)
{
	if(t == NULL)
		return -1;
	else
		return t->dimensiune;
}

//metoda os_select : returneaza nodul care contine a i-a cea mai mica cheie din subarborele al carui radacina este x
//r retine dimensiunea subarborelui stang
NOD* os_select(NOD *x, int i)
{ 
  int rank;
  comparatii=comparatii+3;
  atribuiri+=1;
  if (x->stanga!=NULL)
	  rank=x->stanga->dimensiune+1;
  else rank=1;
  //se retine arborele initial  daca i==r, adica am gasit dimensiunea ceruta
  if (i==rank)
  return x;
  //if dimensiunea ceruta e mai mica se cauta in subarborele stang
  else if (i<rank)
     return os_select(x->stanga,i);
  //if dimensiunea ceruta e mai mare se cauta in subarborele drept
  else return os_select(x->dreapta,i-rank);
}
int os_rank(NOD *T,NOD *x)
{
	int rank=x->stanga->dimensiune+1;
	NOD *y;
	y=x;
	while(y!=T)
	{
		if(y==y->parinte->dreapta)
			rank=y->parinte->stanga->dimensiune+1;
		y=y->parinte;
	}
	return rank;
}
//returneaza succesorul unui nod
NOD* treeSuccesor(NOD *x)
{ 
  NOD *y;
  comparatii++;
  //se returneaza fiul stang al subarborelui drept
  if (x->dreapta!=NULL)
     return frunza_cea_mai_din_stanga(x->dreapta);
  atribuiri++;
  //se retine parintele arborelui x
  y=x->parinte;
  while ((y!=NULL) && (x==y->dreapta))
	{
	  atribuiri+=2;
	  x=y;//arborele devine fostul parinte
	  y=y->parinte;
	}
  return y;//y va fi parintele lui x
}
//metoda de construire a arborelui
//stanga, dreapta, m=(stanga+dreapta)/2 mijlocul
//d_dr=dimensiune dreapta
//se calculeaza dimensiunile subarborilor stang si drept
NOD *construire_arbore_echilibrat(int stg, int dr, int *dimens)
{ 
  NOD *p;
  //int dim=sizeof(NOD);
  int dimens1=0,dimens2=0,d_dr;
  comparatii++;
  atribuiri=atribuiri+5;
			p=(NOD*) malloc(sizeof(NOD));
			//se retine mijlocul intervalului
	        p->mijloc=(stg+dr)/2;
			//subarborii sunt nuli
			p->stanga = NULL;
			p->dreapta = NULL;
			//d_dr=dimens-stg+1;
			//se calculeaza dimensiunea
			p->dimensiune=dr-stg+1;
			//se merge recursiv in constructia arborelui pe cele 2 ramuri
			if(stg < dr){
				p->stanga=construire_arbore_echilibrat(stg,(stg+dr)/2-1,&dimens1);
				if(p->stanga != NULL)
					//retin parintele
				{	p->stanga->parinte = p;
				    atribuiri+=1;
				}
				p->dreapta=construire_arbore_echilibrat((stg+dr)/2+1,dr,&dimens2);
				if(p->dreapta != NULL)
				{	p->dreapta->parinte = p;
				    atribuiri+=1;
				}
			}
	 
  atribuiri++;
  *dimens=p->dimensiune;//noua dimensiune
  return p;
}
void dimensiune_scade(NOD *z)
{
	while (z!=NULL)
	{
      atribuiri=atribuiri+2;
	  z->dimensiune--;
	  z=z->parinte;
	}
}
//metoda sterge nodul unui arbore
void tree_delete(NOD **arbore, NOD *z)
{
	NOD *y,*x;
  comparatii=comparatii+5;
  if ((z->stanga==NULL) || (z->dreapta==NULL))
  {
		 y=z; atribuiri++;
  }
  else
  {
	  y=treeSuccesor(z); atribuiri++;
  }
  if (y->stanga!=NULL)
  {
	  x=y->stanga; atribuiri++;
  }
  else
  {
	  x=y->dreapta; atribuiri++;
  } 
  if (x!=NULL)
  {
	  atribuiri++;
	   x->parinte=y->parinte;
     }
  dimensiune_scade(y->parinte);
 // y->parinte->dimensiune --;
  if (y->parinte==NULL)
     *arbore=x;
  else
  {
	 if (y==y->parinte->stanga)
	 {
		 y->parinte->stanga=x; atribuiri++;
	 }
	 else
	 {
			y->parinte->dreapta=x; atribuiri++;
	 }
    if (y!=z)
     {   atribuiri++;
		 z->mijloc=y->mijloc;
     }
  }
  //eliberez nodul
  free(y);
}
//functia josephus :n numarul de elemente, m pasul de numarare
void josephus(int n, int m)
{
  NOD *root,*x;
  int h,var_k,var_n;
  int i=0;
  var_n=n;
  root=construire_arbore_echilibrat(1,var_n,&h);//construieste arborele cu elemente intre 1 si n si dimensiunea h
  root->parinte=NULL;//parintele radacinii va fi nul

  var_k=m;//se incepe cu numarul corespunzator pasului de numarare
  while (var_n>0)
	{
	  x=os_select(root,var_k);//se retine radacina subarborelui cu dimensiunea x a arborelui
	  a[i]=x->mijloc;//se pune in vector valoarea
	  i++;
	  tree_delete(&root,x);//se sterge nodul x
	  var_n--;
	  if (var_n)
	     {
			 //se face modulo pentru a putea lua elementele in mod circular
		   var_k=(var_k+m-1)%var_n;
	       if (var_k==0)
		      var_k=var_n;
	     }
	}
  
  printf("Ordinea de iesire :\n",n,m,m);
   for (int i=0;i<n;i++)
	printf(" %d",a[i]);
}
int main()
{
  f=fopen("josephus.csv","w");
  fprintf(f,"n, atribuiri, comparatii, suma\n");
  for (int n=100; n<10000; n+=100)
  {
    atribuiri=0;
	comparatii=0;
    int m=n/2;
    //josephus(n,m);

    fprintf(f,"%d, %d, %d, %d \n",n,atribuiri,comparatii,atribuiri+comparatii);
  }
	josephus(7,3);
    fclose(f);
    return 0;
}