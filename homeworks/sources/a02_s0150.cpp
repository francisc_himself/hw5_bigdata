#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<time.h>
//***ANONIM*** ***ANONIM*** gr ***GROUP_NUMBER***
//Observatii
//pentru valori mari :
//complexitatea metodei top_down este de O(nlog(n))
//complexitatea metodei bottom_up devine practic liniara O(n)
double tdop[100];
double buop[100];
int a[20000],b[20000];

void copie(int *a,int *b,int length)
{
	for(int i=1;i<=length;i++)
		b[i]=a[i];
}

void afis(int *a,int length)
{
	printf("\n");
	for(int i=1;i<=length;i++)
		printf("%d ",a[i]);
}

void prettyprint(int *a,int n,int j,int niv)
{
    int i;
    if(j>n) return;
    prettyprint(a,n,2*j+1,niv+1);
    printf("\n");
    for(i=0;i<5*niv;i++)
	{
        printf(" ");
    }
    printf("%d",a[j]);
    prettyprint(a,n,2*j,niv+1);

}

void scrie_rez(FILE *f,double *a,int length)
{
	fprintf(f,"\n");
	for(int i=1;i<=length;i++)
		fprintf(f,"%.0f,",a[i]);
}

void top_down(int *a,int n)
{
    int i,j;
    int aux;
    double cnt=0;
    for(i=1;i<=n;i++)
    {
        j=i;
        while ((j>1))
        {
            cnt+=1;
            if(a[j]<a[j/2])
            {
                aux=a[j];
                a[j]=a[j/2];
                a[j/2]=aux;
                cnt+=3;//liniile 51-56
            }
            j=j/2;

        }

    }
    tdop[n/100]=cnt;
}
void bottom_up(int *a,int n)
{
    int i,j,aux,min;
    double cnt=0;
    bool b;
    for(i=n/2;i>=1;i--)
    {
        j=i;
        b=true;
        while(b && j<=n/2)
        {
            if(((2*j+1)<=n) && a[j]>a[2*j+1])
			{
                min=2*j+1;
            }else
            {
                min=j;
            }
            if(a[min]>a[2*j]){
                min=2*j;
                cnt+=1;
            }
            cnt+=4;
            if(min!=j)
            {
                aux=a[min];
                a[min]=a[j];
                a[j]=aux;
                j=min;
                cnt+=3;
            }else
            {
                b=false;
            }
        }
    }
    buop[n/100]=cnt;
}


int main(){
	int i=0,n=10;
	FILE *f;
	//Caz mediu Statistic
	f=fopen("random.csv","w");
    srand (time(NULL));
    for(i=1;i<=n;i++)
	{
        a[i]=rand();
    }
    copie(a,b,n);
    prettyprint(a,n,1,0);
    bottom_up(b,n);
    printf("\n\n");
    prettyprint(b,n,1,0);
    copie(a,b,n);
    top_down(b,n);
    printf("\n\n");
    prettyprint(b,n,1,0);
    for(n=100;n<=10000;n+=100)
	{
        for(i=1;i<=n;i++)
		{
            a[i]=rand();
        }
        copie(a,b,n);
        bottom_up(b,n);
        copie(a,b,n);
        top_down(b,n);
    }
    scrie_rez(f,buop,100);
    scrie_rez(f,tdop,100);
	fclose(f);
	printf("\n\n####Sfirsit####");
	getch();
	return 0;
}
