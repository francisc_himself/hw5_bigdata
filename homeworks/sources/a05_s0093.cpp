/*-------------------------Assignment No.5 : ------------------------------------------------------------
Search Operation in Hash Tables Open Addressing with Quadratic Probing
	Student:					***ANONIM*** ***ANONIM***
	Grupa:						***GROUP_NUMBER***
	Requirements:				You are required to implement correctly and efficiently the insert and search operations in a
                                hash table using open addressing and quadratic probing.
                                You may find any necessary information and pseudo-code in your course notes, or in the book1,
                                in section 11.4 Open addressing.
   Implementation requirements:   Before you start to work on the algorithms evaluation code, make sure you have a correct
                                  algorithm! You will have to prove your algorithm(s) work on a small-sized input.
                                  You are required to evaluate the search operation for hash tables using open addressing and
                                   quadratic probing, in the average case (remember to perform 5 runs for this). You will do this in
                                  the following manner:
                        case 1:   Select N, the size of your hash table, as a prime number around 10000 (e.g. 9973, or 10007);
                        case 2:   Search, in each case, m random elements (m ~ 3000), such that approximately half
                                  of the searched elements will be found in the table, and the rest will not be found
                                  (in the table). Make sure that you sample uniformly the elements in the found
                                  category, i.e. you should search elements which have been inserted at different
                                  moments with equal probability (there are several ways in which you could ensure
                                  this � it is up to you to figure this out)
                      case 3:      Count the operations performed by the search procedure (i.e. the number of cells accessed during the search) .


*/



#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>

#define N 9973
#define m 3000




long foundTotal,NotfoundTotal,maxFound=0,maxNotFound=0,nrFound,nrNotFound ;
double  avgFound,avgNotFound;

long hTable[N];
long searched[m+1];

double alfa[]={0.8,0.85,0.90,0.95,0.99};

long max(int x, long y){  
    return (x>y)? x:y;
}

//h'(k)=k mod n
int hprim(long key){
	return key%N;
}
//the quad hash function
int hquad(long k, int i)
{
	int C1=1;
	int C2=1;
	int temp = (hprim(k)+C1*i+C2*i*i)%N;
	return temp;
}


int insertInHash(long hTable[], long k)
{int i=0;
 int j;
 do
 {
     j=hquad(k,i);
     if(hTable[j]==-1)
       {
            hTable[j]=k;         
            return j;
       }
     else
       i++;         
 }while(i<N);
  
  return -1; 
  
}


int searchElement(long hTable[], long val)
{
    int i=0,j;
   
    j=hquad(val,i);
    while ((hTable[j]!=-1)&&(hTable[j]!=val) && (i<N))	
    {
        i++;
        j=hquad(val,i);
    }
    
	if((i==N )|| (hTable[j]==-1))
	{ 
        nrNotFound++;
        NotfoundTotal+=i;   
        maxNotFound=max(maxNotFound,i); 
        return -1;
    }
    maxFound=max(maxFound,i);
    nrFound++;    
    foundTotal+=i;
    
    return j;
   
}

void create_test(long nr){ 
	                                  
	for(int i=0;i<N;i++)
		hTable[i]=-1;   
                                  
	int i=0,y=0,temp,rez=0,index;	
	double fu;                      
	    
	for(int i=0; i<nr; i++)
		 rez=insertInHash(hTable,rand()%20000+1);

	do{                                  
            if(y<m/2)
               {
                temp=hTable[rand()%N]; 
				if(temp != -1){
					searched[y]=temp;
					y++;
				}
		       	                     
               }else {
                searched[y]=rand()%10000+20002;
				y++;
             } 
			
	}while(y<=m && rez!=-1);
}

int main()
{  
    srand(time(NULL)); 
    long found=0;
   
    
    FILE *f;
    f=fopen("hashtable.csv","w");	
    if(f)
      printf("Fisierul s-a deschis.");
    
    
	fprintf(f,"FillingFactor;AvgEffortFound;MaxEffortFound;AvgEffortNotFound;MaxEffortNotFound;\n");       	
	
    for (int i=0; i<5; i++)	//execute five runs and calculate average
     {                      	                	
         avgFound=0;
         avgNotFound=0;
         printf("\nalfa[%d]=%lf -> 5 tests\n ",i,alfa[i]);     
        
         for(int q=1;q<=5;q++)   	//5 inserts and 5 searches for every filling factor
           {    long nr=(long)(alfa[i]*N); //the filling factor                
                foundTotal=0;
                NotfoundTotal=0;
                nrFound=0;
                nrNotFound=0;  
				maxFound=0;
				maxNotFound=0;
                                             
                create_test(nr); 
                printf("q=%d nr=%ld\n",q,nr);                 
                int contor=0;               
           
            for(int r=0;r<m;r++)              
                 {
                  found=searchElement(hTable,searched[r]); 
                  if(found>0)
                     contor++;   		                          
                 }         
           printf("contor=%d\n",contor);   
           avgFound   +=((double)foundTotal/(double)nrFound);
           avgNotFound+=((double)NotfoundTotal/(double)nrNotFound);
           
         }                
         
     avgFound=(double)avgFound/5;
     avgNotFound=(double)avgNotFound/5;
     printf("avgFound=%.lf\navgNOTfound=%.lf,maxFound=%ld\nmaxNotFound=%ld \n\n\n",avgFound,avgNotFound,maxFound,maxNotFound);
     printf("/n-----------------End alfa[%d]=%f ------------------\n",i,alfa[i]);                 
     fprintf(f,"%lf;%lf;%ld;%lf;%ld;\n",alfa[i],avgFound,maxFound,avgNotFound,maxNotFound);

   }
	
    fclose(f);
    getche();
    return 0;
 }


/* -------------------------- Demo ---------------------------------
    int k=(int)(alfa[0]*small);
	printf("Insert k=%d keys.\n",k);
	int cc1,cc2=0;

	for(int i=0;i<m;i++)
	   searched[i]=0;
	
    printf("Hash before partition.\n"); 	
    printf("\n\n\n");
   
    create_test(k);                                        
	printf("HashTable after insertion.\n"); 
      	for(int j=0;j<N;j++)
      	     printf("H[%d]=%d\n",j,hTable[j]);
    	     
     printf("Searched vector after insertion.\n"); 
      	for(int j=0;j<m;j++)
      	     printf("%d\n",searched[j]); 
              	     
   printf("\n\n",cc1);
   for(int r=0;r<m;r++)
      {
        found=searchElement(hTable,searched[r]);

        if(found>0)
            {  printf("%d found at index %d\n",hTable[r],found);             
               cc1++; 
            }
        else  if(found<0)
        {
             cc2++; 
             printf("%d NOT found at index %d\n",hTable[r],found);               
        } 
      }          
     printf("cc1=%d\n",cc1);//found
     printf("c2=%d\n\n",cc2);//not found
   */
 
