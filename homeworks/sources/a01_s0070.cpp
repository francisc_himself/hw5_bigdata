

/* ***ANONIM*** ***ANONIM*** - ***ANONIM***
   Grupa ***GROUP_NUMBER***

	In ceea ce priveste cazul mediu statistic, metodele de sortare prin selectie si insertie au aceeasi eficienta. Metoda de  sortare
bubble nu este la fel de eficienta, deoarece are un numar de atribuiri mult mai mare fata de celelalte metode.

	Atunci cand vectorul este crescator este un caz favorabil pentru metoda de sortare bubble deoarece nu necesita nici o interschimbare 
de elemente implicand atribuirile. Toate cele trei metode au numar aproximativ egal de comparari, iar diferenta de eficienta se datoreaza 
diferentelor foarte mici al numarului de atribuiri.

	Atunci cand vectorul este descrescator avem cazul defavorabil pentru metoda de sortare bubble, dar favorabil pentru metoda de sortare prin
insertie, deorece aceasta nu necesita comparatii, iar diferenta de eficienta intre metodele de sortare prin insertie si prin selectie este 
foarte mica.

	In concluzie, putem deduce din grafice ca metoda selectiei are un comportament uniform pe toate cele trei cazuri avand o eficienta 
ridicata, in comparatie cu celelate doua metode de sortare discutate in acest laborator: metoda insertiei si bubble.
*/


#include "Profiler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#define MAX 100

Profiler profiler("demo");

void generate(int* v,int n)
{
	int i;
	for(i=0;i<n;i++)
	{
		v[i]=rand()%MAX;
	}
}

//Metoda selectiei
void selectionSort(int* v,int n)
{
	int aux,imin;
	for(int i=0;i<n;i++)
	{
		imin=i;
		for(int j=i+1;j<n;j++)
		{
			if(v[j]<v[imin]) imin=j;
			profiler.countOperation("selectionSort_CompSelection",n,1);
		}
		if( i != imin)
		{
		aux=v[imin];
		v[imin]=v[i];
		v[i]=aux;
		profiler.countOperation("selectionSort_AssignSelection",n,3);
		}
	}	
}

//Metoda insertiei
void insertionSort(int* v,int n)
{
	int j,x;
	for(int i=0;i<n;i++)
	{
		x=v[i];
		j=0;
		while(v[j]<x)
		{	
			j++;
			profiler.countOperation("insertionSort_CompInsertion",n,1);				
		}
		for(int k=i;k>=j+1;k--)
		{
			v[k]=v[k-1];
			profiler.countOperation("insertionSort_AssignInsertion",n,1);
		}
		if( i != j )
		{
		v[j]=x;
		}
		profiler.countOperation("insertionSort_AssignInsertion",n,2);
	}
}


//Metoda bubble
void bubbleSort(int* v,int n)
{
	printf("\n");
	int i,j,aux;
	for(i=1;i<n;i++)
	{
		for(j=0;j<n-i;j++)
		{
			if (v[j]>v[j+1]) 
			{ 
				aux=v[j];
				v[j]=v[j+1];
				v[j+1]=aux;
				profiler.countOperation("bubbleSort_AssignBubble",n,3);
			}
		profiler.countOperation("bubbleSort_CompBubble",n,1);
		}
	}
}


void main(){
	//srand(time(NULL));//sa nu genereze aceleasi nr aleatoare
	
	/*int v[100];
	int test[]={4, 9, 1, 8, 3};
	int test2[]={4, 9, 1, 8, 3};
	int test3[]={4, 9, 1, 8, 3};

	//generate(test,MAX);
	for (int i=0;i<5;i++){
		printf("%d ",test[i]);	
	}
//printf("\n");
	bubbleSort(test,5);
	printf("\n");
	for (int i=0;i<5;i++){
		printf("%d ",test[i]);	
	}
	selectionSort(test2,5);
	printf("\n");
	for (int i=0;i<5;i++){
		printf("%d ",test2[i]);	
	}
	insertionSort(test3,5);
	printf("\n");
	for (int i=0;i<5;i++){
		printf("%d ",test3[i]);	
	
	}
	//getche();
	
	getchar();
}*/

	int n;
	int v[10000], v1[10000];
	char choose;
	printf("Tasta: r - Random   c - Crescator  d - Descrescator\n Tasta: ");
	scanf("%c",&choose);

	switch(choose){
	//Random
	case 'r':
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
		printf("n=%d\n", n);
		generate(v, n);//fillrandomarray(v,n);
		memcpy(v1, v, n * sizeof(int));
		bubbleSort(v1, n);
		memcpy(v1, v, n * sizeof(int));
		selectionSort(v1, n);
		memcpy(v1, v, n * sizeof(int));
		insertionSort(v1, n);
		}
	}
	break;
	//Sortat crescator
	case 'c':
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
		printf("n=%d\n", n);
		FillRandomArray(v,n,10,50000,false,1);
		memcpy(v1, v, n * sizeof(int));
		bubbleSort(v1, n);
		memcpy(v1, v, n * sizeof(int));
		selectionSort(v1, n);
		memcpy(v1, v, n * sizeof(int));
		insertionSort(v1, n);
		}
	}
	break;
	//Sortat descrescator
	case 'd':
	for(int testt=0;testt<5;testt++)
	{
		for(n=100; n<2000; n+=100)
		{
			printf("n=%d\n", n);
			FillRandomArray(v,n,10,50000,false,2);
			memcpy(v1, v, n * sizeof(int));
			bubbleSort(v1, n);
			memcpy(v1, v, n * sizeof(int));
			selectionSort(v1, n);
			memcpy(v1, v, n * sizeof(int));
			insertionSort(v1, n);
		}
	}

	}
	profiler.createGroup("comp","insertionSort_CompInsertion","bubbleSort_CompBubble","selectionSort_CompSelection");
	profiler.createGroup("assign","selectionSort_AssignSelection","insertionSort_AssignInsertion","bubbleSort_AssignBubble");

	profiler.addSeries("bubleSerie","bubbleSort_CompBubble","bubbleSort_AssignBubble");

	profiler.addSeries("insertionSum","insertionSort_CompInsertion","insertionSort_AssignInsertion");

	profiler.addSeries("selectionSum","selectionSort_CompSelection","selectionSort_AssignSelection");

	profiler.createGroup("SERIE","bubleSerie","insertiobSum","selectionSum");
	profiler.showReport();
	//getch();
}