 #include <stdio.h>
#include <stdlib.h>
#include <conio.h>

typedef struct _BINARY
{
	int key;
	struct _BINARY *stanga;
	struct _BINARY *dreapta;
} BINARY;

BINARY *v2[50];
BINARY *root2;
typedef struct _MULTI_NODE
{
	int key;
	int count;
	struct _MULTI_NODE *child[50];

} MULTI_NODE;

void parcurgere(MULTI_NODE *root)
{
	if(root->count > 0){
		if(root->count == 1)
			v2[root->key]->stanga = v2[root->child[0]->key];
		else {
			v2[root->key]->stanga = v2[root->child[0]->key];
			for( int i=0;i<root->count-1;i++)
				v2[root->child[i]->key]->dreapta = v2[root->child[i+1]->key];
		}
		for(int i=0; i<root->count;i++)
			parcurgere(root->child[i]);
		}
	else return;
}
void transf2(MULTI_NODE *root,int size){
	for(int i=0;i<size;i++){
		BINARY *p = (BINARY*)malloc(sizeof(BINARY));
		p->key = i;
		p->dreapta = NULL;
		p->stanga = NULL;
		v2[i] = p;
	}
	root2 = (BINARY*)malloc(sizeof(BINARY));
	root2 = v2[root ->key];
	parcurgere(root);
}
void pretty_printT2(BINARY *root2,int i)
{
//afisare in preordine a grafului
	int j, aux;
	for(j=0; j<=i;j++)
		printf("     ");
	printf("%d \n",root2->key);
	if(root2->stanga != NULL){
		aux = i+1;
		pretty_printT2(root2->stanga,aux);
	}
	if(root2->dreapta != NULL){
		aux = i+1;
		pretty_printT2(root2->dreapta,aux);
	}
}
MULTI_NODE *createMN (int key)
{
MULTI_NODE *p=(MULTI_NODE*) malloc(sizeof(MULTI_NODE));
p->key=key;
p->count=0;
return p;
}

void insertMN ( MULTI_NODE *parent, MULTI_NODE *child)
{
	parent->child[parent->count++]=child;
}

MULTI_NODE *transf1(int t[], int size)
{
	MULTI_NODE *root=NULL;
	 MULTI_NODE *nodes[50];
	
	for(int i=0; i< size;i++)
	{
		nodes[i]= createMN(i);
		if (t[i]==-1 )
			 root=nodes[i];
	}
		  
    for(int i=0; i<size;i++ )
	{
		if (t[i] !=-1)
		insertMN (nodes[t[i]], nodes[i]);
	}	
	return root;
}
void prettyprintMN (MULTI_NODE *nod ,int nivel=0)
{
	for (int i=0; i<nivel;i++)
		   printf ("   ");
	       printf ("%d\n", nod->key); 
		   for (int i=0; i<nod->count;i++)
		   {
			   prettyprintMN(nod->child[i], nivel+1);
		   }

}
int main()
{
	int t[]={3,3,3,-1,2,2,2,2,5,1,1};
	int size=sizeof(t)/sizeof(t[0]);
	MULTI_NODE *root=transf1(t,size);
	prettyprintMN(root);
	printf("\nReprezentare binara: \n");
	transf2(root,size);
	pretty_printT2(root2,1);
	return 0;
}