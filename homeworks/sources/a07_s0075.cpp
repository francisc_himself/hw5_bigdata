/*
***ANONIM*** ***ANONIM*** Mihai, grupa ***GROUP_NUMBER***
Lucrare Laborator 7, arbori multi cai(multi way trees);
In aceasta lucrare de laborator am studiat arborii multi cai. Pentru constructia unui arbore multi cai am folosit un sir de intregi, a carui lungime
si valori le-am citit din fisierul "***ANONIM***.txt". Apoi, dupa aceasta constructie, am transformat arborele intr-unul binar, folosind functia T2().
Pe langa aceste transformari, in program mai sunt definite si functiile pentru afisare multi cai si binar(pretty print).
*/

#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
int PV[20];
int nr;

//Structura MultiWay si declararea nodurilor
typedef struct TreeNode{
   int val;
   int childNo;
   TreeNode* child[100]; 
}TreeNode;
TreeNode* a[100];
TreeNode* prim; 

//Structura Arbore Binar si declararea nodurilor
typedef struct BinaryNode
{
  int val;
  BinaryNode* brother;
  BinaryNode* son;
}BINARY;
BINARY* b[100];   


//Afisare MultiWay Recursiv
void MultiWayPrint(TreeNode *p){ 
	int i;
	if(p!=NULL) {	
		  if(p->childNo==0)
			  printf("\n%d does not have any children",p->val);
		  else{
				printf("\nChildren of %d are:",p->val);
				for(i=1;i<=p->childNo;i++)
					 printf(" %d ",p->child[i]->val);
				for(i=1;i<=p->childNo;i++)
					 MultiWayPrint(p->child[i]);
		  }
	 }
}

//Afisare Arbore Binar recursiv
void PrettyPrint(int index,int nivel){ 
   int i;
   printf("\n");
   for(i=0;i<=nivel;i++)
	   printf("   ");
   printf("%d",index);

   if(b[index]->son!=NULL)
	   PrettyPrint(b[index]->son->val,nivel+1);
     if(b[index]->brother!=NULL)
	   PrettyPrint(b[index]->brother->val,nivel);
}

//Aici se creeaza arborele multi-way folosind vectorul citit din fisier
void T1(){ 
   int i,aux;
   for(i=0;i<=nr;i++){
	   if(PV[i]==-1)	
			prim=a[i];
	   else{
	   a[PV[i]]->childNo++;		
	   aux = a[PV[i]]->childNo;
	   a[PV[i]]->child[aux]=a[i];
	   }
   }
}
//Se transforma din Multi-way in binar
void T2(TreeNode *p){ 
	int i;
	if(p!=NULL)
	{
		for(i=1;i<=p->childNo;i++)
		{
			if(i==1)
				b[p->val]->son=b[p->child[1]->val];
			else
				b[p->child[i-1]->val]->brother=b[p->child[i]->val];

			T2(p->child[i]);
		}
	}
}

void main(){
	int i;
	//Citire nr si PV[] din fisier
	FILE *pf=fopen("***ANONIM***.txt","r");
	fscanf(pf,"%d",&nr);
	for(i=0;i<=nr;i++){
	   a[i]=(TreeNode*)malloc(sizeof TreeNode);  
	   a[i]->val=i;
	   a[i]->childNo=0;
	   a[i]->child[1]=NULL;

	   b[i]=(BINARY*)malloc(sizeof BINARY);  
	   b[i]->val=i;
	   b[i]->son=b[i]->brother=NULL;
	   
	   fscanf(pf,"%d",&PV[i]);
	   printf("%d ",PV[i]);
	}
	
	
	T1();
	printf("\nAfter the first transformation:");
	MultiWayPrint(prim);

	T2(prim);
	printf("\nAfter the second transformation:\n");
	PrettyPrint(prim->val,0);


	getch();

}