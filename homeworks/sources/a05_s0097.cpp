#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>

using namespace std;

#define cst1 3
#define cst2 7

int op;

int basicHashFct(int key, int n){
	return key % n;
}

// h(k,i) = (h'(k) + c*i + c2*i^2) mod n
// i E (0, n-1)
int hashFunction(int key, int n, int step){
	return (basicHashFct(key,n) + cst1 * step + cst2 *step *step)%n;
}

void insert(int *table, int n, int data){
	int i = 0, spot;
	do {
		spot = hashFunction(data,n,i);
		if(table[spot] == -1){
			table[spot] = data;
			return;
		}
		else
			i++;
	} while (i < n);
}

int search(int *table, int n, int data){
	int i = 0, spot;
	op = 0;
	//because we do not delete, if a null is found before the data,
	//then it is obvious data won't be found
	//(search looks in same sequence of places as insert)
	do {
		spot = hashFunction(data,n,i);
		op++;
		if(table[spot] == data){
			//printf("opF=%d \n", op);
			return 1;
		}
		i++;
	} while(i<n && table[spot] != -1);
	//printf("opN=%d \n", op);
	return 0;
}

int getAverage(int op, int nbElem){
	return op/nbElem;
}

int main(){

	FILE *statistic;
	statistic = fopen("statistic.csv", "w");

	int *table;
	int N = 9973, i,j,k, valSearched[3000], valInserted[9973],
		maxFound = 0, maxNotFound = 0;
	float opFound[5], opNotFound[5], opFoundAll = 0, opNotFoundAll = 0, averageFound, averageNotFound;
	float fillFactor [5] = {0.80, 0.85, 0.9, 0.95, 0.99}, n;

	/* PROOF HASH TABLE WORKS
	n = 100;// n = size of table
	for(i=0; i<n; i++)
		table[i] = -1;

	insert(table,n,3);
	insert(table,n,7);
	insert(table,n,1003);
	insert(table,n,7);
	search(table,n,1003);
	*/
	/* PROOF WE ARE SELECTING APPROPIATE VALUES
	for(k = 0; k<5; k++){
		//5 times for average case
		table = (int*) malloc(sizeof(int)*(N+1));

		for(j=0; j<N; j++)
			table[j] = -1;
		n = floor(fillFactor[i] * N);
		//insert random values in hash table.
		srand(k);
		for(j = 0; j < n; j++){
			valInserted[j] = rand()%10000;
			insert(table,N,valInserted[j]);
		}
		for(int l=0; l<n; l++)
			printf("%d ", table[l]);
		//randomize array of values that were inserted
		int temp, index;
		srand(k);
		for(j=0;j<n;j++){
			index = (int)(rand()%(int)(n-j)) + j;
			temp = valInserted[j];
			valInserted[j] = valInserted[index];
			valInserted[index] = temp;
		}
		//values to be searched to take 50/50 chances of being found
		srand(k);
		for(j=0;j<20;j++){
			if(j<10) {
				valSearched[j] = valInserted[j];
			}
			else {
				valSearched[j] = (int)(rand()%100) + 10001;
			}
		}
		//search
		op = 0;
		for(j=0;j<20;j++){
			if(search(table,N,valSearched[j]) == 0){
				opNotFound[k] += op;
				if(op > maxFound)
					maxFound = op;

			}
			else {
				opFound[k] += op;
				if(op > maxNotFound)
					maxNotFound = op;
			}
			op = 0;
		}
		opFound[k] = opFound[k]/10;
		opNotFound[k] = opNotFound[k]/10;
		free(table);
	}
	for(j=0;j<5;j++) {
		opFoundAll += opFound[j];
		opNotFoundAll += opNotFound[j];
	}
	//printf("opFound=%d \n", opFound[0]);
	averageFound = opFoundAll / 5;
	averageNotFound = opNotFoundAll / 5;

*/
	fprintf(statistic,"Filling Factor, Avg. Effort Found, Max Effort Found, Avg. Effort not found, Max Effort not found\n");
	for(int p = 0; p<5; p++){
		opFound[p] = 0;
		opNotFound[p] = 0;
	}
	for(i=0;i<5;i++){
	//for each fillFactor
	for(k = 0; k<5; k++){
		//5 times for average case
		table = (int*) malloc(sizeof(int)*(N+1));

		for(j=0; j<N; j++)
			table[j] = -1;
		n = floor(fillFactor[i] * N);
		//insert random values in hash table.
		srand(k);
		for(j = 0; j < n; j++){
			valInserted[j] = rand()%50000;
			insert(table,N,valInserted[j]);
		}
		//randomize array of values that were inserted
		int temp, index;
		srand(k);
		for(j=0;j<n;j++){
			index = (int)(rand()%(int)(n-j)) + j;
			temp = valInserted[j];
			valInserted[j] = valInserted[index];
			valInserted[index] = temp;
		}
		//values to be searched to have 50/50 chances of being found
		srand(k);
		for(j=0;j<3000;j++){
			if(j<1500) {
				valSearched[j] = valInserted[j];
			}
			else {
				valSearched[j] = rand() + 1000100;
			}
		}
		//search
		int m = 0, n=0;
		op = 0;
		for(j=0;j<3000;j++){
			if(search(table,N,valSearched[j]) == 0){
				opNotFound[k] += op;
				if(op > maxNotFound)
					maxNotFound = op;
				m++;
			}
			else {
				opFound[k] += op;
				if(op > maxFound)
					maxFound = op;
				n++;
			}
			op = 0;
		}

		opFound[k] = opFound[k]/1500;
		opNotFound[k] = opNotFound[k]/1500;
		opFoundAll += opFound[k];
		opNotFoundAll += opNotFound[k];
		free(table);
	}
	averageFound = opFoundAll / 5.0;
	averageNotFound = opNotFoundAll / 5.0;
	printf("Avarage iterations for already there values: %.0f\n Max iterations for already there values: %d\n\n",averageFound,maxFound);
	printf("Avarage iterations for not there values: %.0f\n Max iterations for not there values: %d\n\n",averageNotFound,maxNotFound);
	fprintf(statistic,"%f,%f,%lu,%f,%lu\n",fillFactor[i],averageFound,maxFound,averageNotFound,maxNotFound);

	opFoundAll = 0;
	opNotFoundAll = 0;
	maxFound = 0;
	maxNotFound = 0;
	for(j = 0; j< 5; j++){
		opFound[j] = 0;
		opNotFound[j] = 0;
	}
}
	printf("done");
	getchar();
	return 1;
}
