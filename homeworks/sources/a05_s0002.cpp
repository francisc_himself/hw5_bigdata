/****ANONIM*** ***ANONIM***, Grupa ***GROUP_NUMBER***
	Assign5_Hash
		Atat in cazul elementelor care se gasesc in Hash, cat si in cazul elemetele care nu se gasesc in Hash
	numarul cautarilor creste odata cu factorul de umplere.
		Pentru aceasi valoare a factorul de umplere numarul de cautari pentru elementelor care nu se gasesc in Hash 
	este mai mare decat numarul da cautari pentru elementele care se gasesc in Hash.
		In cazul elementelor care nu se gasesc in Hash (caz defavoravil) cautarea are eficienta O(1)
		In cazul elementelor care se gasesc in Hash (caz favorabil) cautarea are eficienta O(1)
	*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include "Profiler.h"

#define HASH_SIZE 9973
#define SEARCH_SIZE 3000

int nr_cautari;

int h(int x, int i)
{
	return (x+2*i+i*i)%HASH_SIZE;	
}

void Hash_init(int T[])
{
	for(int i=0;i<HASH_SIZE;i++)
		T[i]=-1;
}

int Hash_insert(int T[],int k)
{
	int i,j;
	i=0;
	do
	{
 		j=h(k,i);
		if(T[j]==-1)
		{
			T[j]=k;
			return j;
		}
		else
			i++;
	}
	while(i!=HASH_SIZE);
	perror("hash table overflow");
	exit(1);
}

int Hash_search(int T[], int k)
{
	int i,j;
	i=0;
	do
	{
		j=h(k,i);
		nr_cautari++;
		if(T[j]==k)
			return j;
		i++;
	}
	while(T[j]!=-1 && i!=HASH_SIZE);
	return -1;
}

void main()
{
	FILE *f;
	int i,j,n;
	int med_Found,med_notFound,max_Found=0,max_notFound=0,cautari;
	int T[HASH_SIZE];
	int samples[HASH_SIZE+SEARCH_SIZE/2];
	int indeces[SEARCH_SIZE/2];
	double a[]={0.8,0.85,0.9,0.95,0.99};
	f=fopen("HashTable.csv","w");
	if(f==NULL)
	{
		perror("EROARE DESCIDERE!");
		exit(1);
	}
	/*Hash_init(T);
	Hash_insert(T,3);
	Hash_insert(T,5);
	Hash_insert(T,20034918);
	if(Hash_search(T,3)!=-1)
		printf("3 se afla in Hash\n");
	else
		printf("3 nu se afla in Hash\n");
	if(Hash_search(T,20034918)!=-1)
		printf("20034918 se afla in Hash\n");
	else
		printf("20034918 nu se afla in Hash\n");
	if(Hash_search(T,4)!=-1)
		printf("4 se afla in Hash\n");
	else
		printf("4 nu se afla in Hash\n");*/
	for(j=0;j<5;j++)
	{
		n=(int)(a[j]*HASH_SIZE);
		FillRandomArray(samples,n+SEARCH_SIZE/2,1,1000000,true,0);
		FillRandomArray(indeces,SEARCH_SIZE/2,0,n-1,true,0);
		Hash_init(T);
		for(i=0;i<n;i++)
		{
			Hash_insert(T,samples[i]);	
		}
		//cazul found
		cautari=0;
		for(i=0;i<SEARCH_SIZE/2;i++)
		{
			nr_cautari=0;
			if(Hash_search(T,samples[indeces[i]])==-1)
			{
				perror("ERROR FOUND!");
				exit(2);
			}
			if(nr_cautari>max_Found)
				max_Found=nr_cautari;
			cautari=cautari+nr_cautari;
		}
		med_Found=cautari/(SEARCH_SIZE/2);
		printf("a=%.2f => Numar mediu si max de cautari pentru cazul Found: %d  %d\n",a[j],med_Found,max_Found);
		//caz not found
		cautari=0;
		for(i=0;i<SEARCH_SIZE/2;i++)
		{
			nr_cautari=0;
			if(Hash_search(T,samples[n+i])!=-1)
			{
				perror("ERROR NOT_FOUND!");
				exit(1);
			}
			if(nr_cautari>max_notFound)
				max_notFound=nr_cautari;
			cautari=cautari+nr_cautari;
		}
		med_notFound=cautari/(SEARCH_SIZE/2);
		printf("a=%.2f => Numar mediu si max de cautari pentru cazul Not_Found: %d  %d\n",a[j],med_notFound,max_notFound);
		fprintf(f,"%.2f,%d,%d,%d,%d\n", a[j],max_Found,med_Found,max_notFound,med_notFound);
	}
	fclose(f);
	getch();
}