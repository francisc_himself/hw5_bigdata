//Tema2 ; (Bottom-up) vs. (Top-down) //
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <conio.h>
int heap_size;
int Nr,Mr;
int m[10000];
int g[10000];
int nroperatii;

int left (int i)   // returneaza pozitia fiului stang
{
	return 2*i;
}
int right(int i)    // returneaza pozitia fiului drept
{
	return 2*i+2;
}
int parinte(int i)  //returneaza pozitia parintelui lui i
{
	return i/2;
}

void heapify(int *v,int i,int heap_size){
	int largest,temp;
	int l=left(i);
	int r=right(i);
	nroperatii++;
	if(l<=heap_size && v[l]>v[i])
		largest=l;
	else
		largest=i;
	nroperatii++;
	if(r<=heap_size && v[r]>v[largest])
		largest=r;
	if (largest!=i)
	{
		nroperatii++;
		temp=v[i];
		v[i]=v[largest];
		v[largest]=temp;
		heapify(v,largest,heap_size);
	}
}



void build_max_heap(int *v, int n)
{
	heap_size = n;
	for(int i=n/2;i>=0;i--)
	{
		heapify(v,i,heap_size);
	}
}



void printPretty(int *v,int n,int k,int nivel)
{
	if (k>n)
	    return;
	else
	{
	printPretty(v,n,2*k+1,nivel+1);
	for(int i=1;i<nivel;i++)
	printf(" ");
	printf("%d",(v[k]));
    printPretty(v,n,2*k,nivel+1);
	}
}


void heapsort ( int x[], int n)
{
int val,i,y,z ;
	build_max_heap(x, n);
	
for(int i=n-1;i>0;i--)
{
    val = x[0];
	x[0] = x[i];
	x[i] = val;
	heap_size = heap_size -1;
	heapify (x,i,n);
}
}

int partition( int *m, int l, int r) 
{
   int pivot, i, j, aux;
   Mr++;
   pivot = m[l];

   i=l-1;	
   j=r+1;   

   while (1) 
   {
	do {
		--j;
		Mr++;
	   }
	while (m[j]>pivot);

	do 
		{
		Mr++;
		++i;
		}
    while (m[i]<pivot);
	
	if (i<j) {
				Mr=Mr+3;
				aux = m[j];
				m[j]=m[i];
				m[i]=aux;
				}
		   else 
			   return j;
   }
}

void quickSort( int *m, int l, int r) 
              
{
   int j;

   if( l < r )
   {
    
    j = partition( m, l, r);
    quickSort( m, l, j-1);
    quickSort( m, j+1, r);
   }
}

void generare(int n,int *m)
{
    int i;
    for(i=1;i<=n;i++)
	{	
        m[i]=rand()%10000;
		g[i]=m[i];
	}
	}

  
void main( )
{
    int arr[10] = {4,1,3,2,16,9,10,14,8,7} ;
    int i ,n;

    printf ( "HeapSort.\n" ) ;
    build_max_heap(arr,10) ;

	printPretty(arr,10,1,1);
    printf ( "\nBottom-up:\n" ) ;
    for ( i = 0 ; i <= 9 ; i++ )
        printf ( "%d\t", arr[i] ) ;

	nroperatii=0;
    heapsort ( arr, 10) ;
    printf ( "\nTop-down:\n" ) ;
    for ( i = 0 ; i <= 9 ; i++ )
        printf ( "%d\t", arr[i] ) ;

	 


	
//{
	
   
       
	int k;
  
	
    n=100;
	while (n<10000)
	{
		Nr=0;Mr=0;
		for (k=0; k<5;k++)
		{
	generare(n, m);
	
	quickSort(g,1,n);
		}

	n=n+300;
	}
	 //for ( n = 100 ; n <= 10000 ; n++ );
	printf ( "\nQuickSort:\n" ) ;
	 printf ( "%d %d %d\n",n,Nr/5,Mr/5 ) ;

//}

	
	
	
	getch();
}
