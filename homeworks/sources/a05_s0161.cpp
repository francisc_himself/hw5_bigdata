//***ANONIM*** Mariana ***ANONIM***, grupa ***GROUP_NUMBER***.
//Prof. Vlad Baja
//Implementare HashTable. 
//Catarea intr-o tabela de dispersie a unui element are o complexitate egala cu O(1), ceea ce indica faptul ca operatiile sunt rapide.
//Fenomenul de coliziune, care inseamna ca doua intrari diferite au aceeasi cheie, se evita prin alegerea urmatoarei pozitii libere.
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

#include "Profiler.h"
#define DIM 3000
#define q 9973

int t[DIM];
int n;
int m=9973;
int key;
double alfa;
int contor;

int r[100];

int h(int k,int i)
{
	return (k+1*i+2*i*i)%m;
}


void hash_insert(int t[],int k)
{
	int j,i=0;

	do
	{
		j=h(k,i);
        contor++;
		if (t[j]==0)
		{

			t[j]=k;
			return;
		}
		else
		{
			i++;
		}
	}
	while (i<m);
}


int hash_search(int t[],int k)
{
	int j,i=0;
int ok=1;
	do
	{
		j=h(k,i);
		contor++;
		if (t[j]==k)
		{
			ok=0;
			return j;
			//break;
		}

		else
		{
	      i++;
		}
}while ((t[j]>0) && (i<m));
     if (ok==1)
		  return 0;

}


void creare_hash(int t[],int n,double alfa)
{
	int i,nr,nr2;

	//numarul de elemente inserate in hash
	m = (int)(n*alfa);

	//introducem in tabela Null
	for (i=0;i<m;i++)
	{
		t[i]=0;
	}

	//introducem elementele
	for (i=0;i<m;i++)
	{
		nr = rand()%100;
		hash_insert(t,nr);
	}


}


int main()
{

	/*initializeaza generatorul de nr aleatoare*/
	srand(time(NULL));

	double alfa[]={0.8,0.85,0.9,0.95,0.99};

	FILE *f=fopen("rezultate.csv","w");

    int samples[q+(DIM/2)];
	int indices[DIM/2];
	int op_succes_maxim=0, op_insucces_maxim=0;
    int cautari, var;
	int fact,k,j,i,index,randy,n1,n2, rez;
    int t[q];
    double med_f, med_notf;

	for(fact=0;fact<5; fact++)
	{

		printf("\n factor=%f\n",alfa[fact]);
		fprintf(f, "factor=%f\n",alfa[fact]);

		op_succes_maxim=0;
		op_insucces_maxim=0;

        creare_hash(t,10000,alfa[fact]);
		//n1=0; //total operatii cu succes
		//n2=0; //total operatii fara succes
		var=m*alfa[fact];
        FillRandomArray(samples, var+(DIM/2), 1, 1000000,  true, 0);
		FillRandomArray(indices, DIM/2,0,var-1, true, 0);
		//fprintf(f,"	cu succes:\n");
		i=0;
		for( i=0;i<n;i++)
		{
			hash_insert(t,samples[i]);
		}

		//Caz NOT FOUND
		cautari=0;
		printf("Cazul NOT FOUND \n");
		for( i=n;i<n+(DIM/2);i++)
		{
			contor=0;
			hash_search(t, samples[i]);

			if(op_insucces_maxim<contor)
				op_insucces_maxim=contor;
			//suma lor
			cautari=cautari+contor;
		}
		med_notf=(double)cautari/(DIM/2);
		printf("nr_op= %d\n",op_insucces_maxim);
		printf("med_nr_op = %1.2lf \n",med_notf);

		cautari=0;
		printf("Cazul FOUND \n");
		for(i=0;i<( DIM/2);i++)
		{
			contor=0;
			hash_search(t, samples[indices[i]]);
			if(op_succes_maxim< contor)
				op_succes_maxim=contor;
			//suma lor
			cautari=cautari+contor;
		}
		med_f=(double)cautari/( DIM/2);
		printf("nr_max_op = %d \n",op_succes_maxim);
		printf("med_nr_op = %1.2lf \n",med_f);
		fprintf(f,"%.2f, %d, %1.2lf, %d,  %1.2lf \n ",alfa[j],op_insucces_maxim,med_notf, op_succes_maxim,med_f);
	/*	while(i<1500)
		       {
					randy=rand()%DIM+1;
					contor=0;
					rez= hash_search(t,randy);
					if(rez!=0) {
						n1+=contor;
						fprintf(f,"%d , %d\n", i+1, contor);
						i++;
					}
					if(contor > op_succes_maxim)
						op_succes_maxim = contor;
				}*/
 //   randy=rand()%100+1;
   // rez= hash_search(t,randy);
	//	if (rez!=0)
		//	printf(" succes: %d",rez);
	//	else printf("fara succes ");

	//	fprintf(f,"	fara succes:\n");
		//i=0;
		/*while(i<1500){
					randy=rand()%DIM+1;
					contor=0;
					rez= hash_search(t,randy);
					if(rez==0) {
						n2+=contor;
						fprintf(f,"%d , %d\n", i+1, contor);
						i++;
					}
					if(contor > op_insucces_maxim)
						op_insucces_maxim = contor;
				}*/

		//n1/=1500; //nr mediu de cautari cu succes
		//n2/=1500; //nr mediu de cautari fara succes


		//fprintf(f,"%f  %d %d %d %d\n",alfa[fact],n1,n2,op_succes_maxim,op_insucces_maxim);

	}

/*printf("\n pentru verificare:");
  creare_hash(r,5,3);
  hash_insert(r,8);
int  re= hash_search(r,8);
  if (re!=0)
	  printf("\n%d",re);
  else printf("\n nu la gasit");
  int w=hash_search(r,1342);
if(w!=0)
    printf("\n %d",w);
  else printf("\n nu la gasit");
	*/
  fclose(f);
  getch();
		return 0;
}
