///////////////////////////////////////////////////////////////////////////////////////////////
// autor ***ANONIM*** ***ANONIM***-***ANONIM***
// student An 2 Politehnica Cluj-Napoca, sectia AC (automatica si calculatoare) grupa 1, (***GROUP_NUMBER***)
//
//Algoritmul face 2 tansformari intre 3 moduri posibile de reprezentare a unui arbore
//Initial arborele este reprezentat cu ajutorul unui vector de tati
//Dupa prima transfomare vom avea reprezentarea de tip strucuta de arbore multi_way, cu cheie pt fiecare nod si vector de copii
//Dupa a doua tranformare vom avea reepretarea de tip binar, cu un fiu si primul frate drept a noului curent
//am imprementat de asemenea 2 functii PreetPrint pentru a avea o imagine a arborilor
//
//complexitatea acestui algoritm este O(n), intrucat parcurgem o singura data fiecare nod ptentru fiecare transformare, facand operatii
//de complecitate O(1) asupra lor
///////////////////////////////////////////////////////////////////////////////////////////////////////////



#include<stdio.h>
#include<stdlib.h>

//structura arborelui multi node
typedef struct _multi_node{
	int key;
	int count;
	struct _multi_node *child[50]; //pt pcte in plus facem *child, ii alocam putina memorie initial iar apoi realoc cu dublu memoriei curente daca se umple
									// cu **child
}MN;

//structura arborelui in reprezentare binara
typedef struct _binary_repr
{
	int key;
	struct _binary_repr *firstChild, *brotherOnTheRight;
}BR;

//aceasta functie adauga un copil unui nod de arbore multi_cale
void insertMN(MN *parent, MN *child)
{
	parent->child[parent->count ++] = child;
}

//creaza un nod de arbore multi care
MN *CreateMN(int key)
{
	MN *a;

	a = (MN*) malloc(sizeof(MN));
	a->key = key;
	a->count = 0;
	a->child[0] = NULL;

	return a;
}

//functia creaza cu ajutorul unui vector de tati (t[]) o structura de arbore multi_cai
MN *tansform1(int t[], int size)
{
	MN *root;
	MN *nodes[50]; // MN **nodes;
	int i;

	//creaza fiecare nod in parte
	for(i = 0; i < size; i++)
	{
		nodes[i] = CreateMN(i);
	}

	//adauga copii corespunzatori fiecarui nod
	for(i = 0; i < size; i++)
	{
		if(t[i] != -1)
		{
			insertMN(nodes[t[i]], nodes[i]);
		}
		else
		{
			root = nodes[i];
		}
	}
	return root;
}

//afiseaza arborele multy_way
void PrettyPrint(MN *root, int nivel=0)
{
	int i;
	for(i = 0; i < nivel; i++)
	{
		printf("  ");
	}
	printf("%d\n", root->key);
	for( i = 0; i < root->count; i++)
		PrettyPrint(root->child[i], nivel+1);
}

//creaza un nod de tip arbore in reprezentare binara
BR *CreazaBR(int key)
{
	BR *nod;

	nod = (BR*) malloc(sizeof(BR));
	nod->key = key;
	nod->brotherOnTheRight = NULL;
	nod->firstChild = NULL;

	return nod;
}

//multy_way - binary, face transformare dintr-o structura de tip arborea multy-way in una de tip arbore reprezentre binara
BR *transform2(MN *node, MN *parent)
{
	BR *root;
	int i;

	if(node == NULL)
		return NULL;

	if(parent == NULL) // suntem in radacina
	{
		root = CreazaBR(node->key);
		if(node->count > 0)
		{
			root->firstChild = transform2(node->child[0], node);  //asignam primul copil
		}			
		//fratele din dreapta ramane NULL fiin radacina
	}
	else //caz general 
	{
		root = CreazaBR(node->key);		
		
		if(node->count > 0)
		{
			root->firstChild = transform2(node->child[0], node); // asignam primul copil
		}
		
		//aflam al catelea copil este pentru a afla daca are frate drept
		for(i=0; i < parent->count; i++)
			if(parent->child[i] == node) 
			{
				i++;
				if(i < parent->count)
				{
					root->brotherOnTheRight = transform2(parent->child[i], parent); //asignam primul frate din dreapta
				}
				break;
			}
	}
	return root;
}
void PrettyPrint2(BR *root, int nivel)
{
	int i;
	//afisam spatii pt aliniere
	for(i=0; i < nivel; i++)
		printf("	");

	//afisam radacina
	printf("%d\n", root->key);
	//mergem recursiv pe copii pt a ii afisa, daca exista
	if(root->firstChild != NULL)
		PrettyPrint2(root->firstChild, nivel+1);
	//afisam recusiv fratii, daca exista
	if(root->brotherOnTheRight != NULL)
		PrettyPrint2(root->brotherOnTheRight, nivel);
}
int main()
{
	// 1)parent -> multy_way
	//int t[] = {9, 5, 5, 9, -1, 4, 4, 5, 5, 4, 2};
	int t[] = {0, 2, 7, 5, 2, 7, 7, -1, 5, 2};
	int size = sizeof(t)/sizeof(t[0]);
	MN *root = tansform1(t, size);
	PrettyPrint(root, 0);

	//2),multy_way -> binary
	BR *root2 = transform2(root, NULL);
	PrettyPrint2(root2, 0);

	return 0;
}