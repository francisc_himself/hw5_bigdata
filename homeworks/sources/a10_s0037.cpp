#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<conio.h>
#include<string.h>

enum colors
{	alb,
	gri,
	negru
}nod_colors;

enum edge
{	arbore,
	inapoi,
	inainte,
	trans
}edge_type;

// que struct
struct queue {
	int items[1024];
	int front, rear;
} q;

typedef struct muchie{
int u,v;
edge tip;
int ver;
}muchie;

int matr[201][201],d[201],p[201],f[201],flag;
int or[201][201];
colors color[201];
muchie m[201];int k;
int at,comp,times,count;
int gi[201];
bool top=true;

// verif daca coada e goala sau nu
int isEmpty() {
	int i = (q.rear == q.front)?1:0;
	return i;
}

// punere in coada
void enq(int item, int length){
	if(q.rear == (length - 1))
		q.rear = 0;
	else 
		q.rear++;

	q.items[q.rear] = item;
}

//scoatere din cap
int deq(int length) {
	if(isEmpty())
		return -1;

	if(q.front == (length - 1))
		q.front = 0;
	else 
		q.front++;

	return q.items[q.front];
}

//ver
int ver(int u, int v){
	for(int i=0;i<k;i++)
	{
		if(m[i].u==u && m[i].v==v)
			return 1;
		if(m[i].u==v && m[i].v==u)
			return 1;	
	}

	return 0;
}

//~~~~~dfs visit~~~~////////////////////////////

void DFS_VISIT(int u,int lungime)
{
	times++;
	count++;
	d[u]=times;
	color[u]=gri;
	
	for(int v=0;v<lungime;v++)
	{	
		if(matr[u][v]==1)
		count++;
		if (color[v]==alb)
		{	if(ver(u,v)==0 && u!=v){m[k].u=u; m[k].v=v; m[k].tip=arbore; k++;} //muchie arbore
			count++;
			p[v]=u;
			if(u!=v);
 				DFS_VISIT(v,lungime);
		}
		else if(color[v]==gri){
			if(ver(u,v)==0 && u!=v ){m[k].u=u; m[k].v=v; m[k].tip=inapoi; k++;} //muchie inapoi
				top=false;
			}
		else if(f[v]<d[u]){
			if(ver(u,v)==0 && u!=v){m[k].u=u; m[k].v=v; m[k].tip=trans; k++; }//muchie transversala
			}
		else {
			if(ver(u,v)==0 && u!=v){m[k].u=u; m[k].v=v; m[k].tip=inainte; k++;} //muchie inainte
			}
				
		
	}
	color[u]=negru;
	times++;
	f[u]=times;
}
//~~~~~dfs ~~~~~~~~~~
void DFS(int lungime)
{
	for(int i=0;i<lungime;i++)
	{
		color[i]=alb;
		p[i]=NULL;
	}
	times=0;
	for(int u=0;u<lungime;u++)
	{	
		count++;
		if (color[u]==alb)
			DFS_VISIT(u,lungime);
	}
}


// ~~~~~~ topologieee ~~~~
void topologic_sort(int lung)
{	
	

		for(int u=0;u<lung;u++)
		{	
			gi[u]=0; 	
		}
		
		for(int u=0;u<lung;u++)
			for(int v=0;v<lung;v++)
			{	
			if(or[u][v]==1)
				gi[v]++;
			}

		for(int u=0;u<lung;u++)
			if(gi[u]==0)
			{enq(u,lung);
			printf("\nenqq %d\n",u);		//
			for(int i=0;i<lung;i++)			//
				printf(" %d  ~~ ",q.items[i]);//
	    	} //coada

		while(!isEmpty()){
			int u=deq(lung);
			printf("\n->deq %d\n",u+1);		//
			for(int i=0;i<lung;i++)			//
				printf(" %d  ~~ ",q.items[i]);//
			for(int v=0;v<lung;v++){
				if(or[u][v]==1)
				{
					gi[v]--;
					if(gi[v]==0)
						{enq(v,lung);
						printf("\nenq%d\n",v);		//
						for(int i=0;i<lung;i++)			//
							printf("%d  ~~ ",q.items[i]);//
						}
				}
			}
		
		
		}

}




void gen(int nr_muchii,int nr_varfuri)
{
	memset(matr,0,sizeof(matr));  //initializare matrice
	if(nr_varfuri*(nr_varfuri-1)/2 < nr_muchii)
	{
		flag=1;
		return;
	}

	int i=0,x,y;
	while(i<nr_muchii)
	{	
		x =rand() % nr_varfuri;
		y =rand() % nr_varfuri;
		if((matr[x][y]==0) && (x!=y) && (matr[y][x]!=1))
		{
			matr[x][y]=1;
			matr[y][x]=1;

			or[x][y]=1; // graf orientat
			i++;
		}

	}
}

void test()
{
	
	int nr_noduri,nr_muchii;
	
	printf("nr noduri= ");
	scanf("%d",&nr_noduri);

	printf("nr muchii= ");
	scanf("%d",&nr_muchii);

	

	gen(nr_muchii,nr_noduri);

	DFS(nr_noduri);

	printf("\nMatrice adiacenta:\n");
	for(int i = 0; i < nr_noduri; i++) 
	{
		for(int j = 0; j < nr_noduri; j++)
			printf("%d ", matr[i][j]);
		printf("\n");
	}

	for(int i = 0; i < nr_noduri; i++)
			printf("p%d f%d  ~~ ", p[i]+1,i+1); //verif vecotr de parinti
	printf("\n\n");

	for(int i = 0; i < nr_noduri; i++)
			printf("nod%d d%d  ~~ ",i+1 , d[i]); //verif vector descoperit
	printf("\n\n");

	for(int i = 0; i < nr_noduri; i++)
			printf("nod%d f%d  ~~ ",i+1 , f[i]); //verif vector final
	printf("\n\n");

	/*//muchiiiiii
	for(int j = 0; j < k; j++)
	{	
		//if(m[k].ver==1)
		//{
			printf("muchia <%d,%d>  ", m[j].u,m[j].v);
			if(m[j].tip==arbore)
				printf("arbore \n");
			else if(m[j].tip==inapoi)
				printf("inapoi \n");
			else if(m[j].tip==inainte)
				printf("inainte \n");
			else printf("trans \n");
		//}
	}*/

	printf("\nGraf orientat:\n");
	for(int i = 0; i < nr_noduri; i++) 
	{
		for(int j = 0; j < nr_noduri; j++)
			printf("%d ", or[i][j]);
		printf("\n");
	}
	topologic_sort(nr_noduri);

	



	
	   
}

//Set |V| = 100 and vary |E| between 1000 and 5000, using a 100 increment.
//Run the BFS algorithm for each <|V|, |E|> pair value and count the 
//number of operations performed; generate the corresponding chart (i.e. the variation of 
//the number of operations with |E|).
void tema1()
{
	FILE *f1;
	f1=fopen("dfs1.csv","w");
	int v=100,e;

	fprintf(f1,"muchii, count \n");

	for(e=1000;e<=4900;e=e+100) //!!!! pt 100 de varfuri nr maxim de muchii in graf este de 4950 nu 5000 !!!!
	{
		gen(e,v);
		at=0;comp=0;
	    DFS(v);
		fprintf(f1,"%d, %d \n",e,count);
	}
	fclose(f1);
}

//Set |E| = 9000 and vary |V| between 100 and 200, using an increment equal to 10. 
//Repeat the procedure above to generate the chart which gives the variation of the 
//number of operations with |V|.
void tema2()
{
	FILE *f2;
	f2=fopen("dfs2.csv","w");

	int v,e=9000;
	fprintf(f2,"noduri, count \n");

	for(v=135;v<=200;v=v+5) //!!!! un graf cu mai 100 v nu poate avea 9000 de muchii !!!!! minim de varfuri 135 pt 9000 de muchii
	{
		gen(e,v);
		at=0;comp=0;
		DFS(v);
		fprintf(f2,"%d, %d \n",v,count);
	}

	

	fclose(f2);
}

void main(int argc, char** argv) 
{
	srand ( time(NULL) );
	
	
	test();
	//tema1();
	//tema2();
	
	getch();
	return;
}