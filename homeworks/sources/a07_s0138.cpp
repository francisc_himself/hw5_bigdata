


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
//#include "Profiler.h"

#define MAX_DIM 100

struct Nod {
	int dim;
	int key;
	struct Nod *st,*dr,*p;
};

Nod *arbore;
int operatii;

Nod* construireArboreEchilibrat(int st, int dr,Nod *p){
	int m, dd, ds;
	Nod *a;
	if (st<=dr){
		m = (st+dr)/2;
		a = (Nod*)malloc(sizeof(Nod));
		a->key = m;
		a->p=p;
		a->st = construireArboreEchilibrat(st,m-1,a);
		a->dr = construireArboreEchilibrat(m+1,dr,a);
		operatii=operatii+6;
		if(a->st == NULL){
			ds = 0;
		}
		else{
			ds = a->st->dim;
		}
		if(a->dr == NULL){
			dd = 0;
		}
		else{
			dd = a->dr->dim;
		}
		a->dim = ds + dd + 1;
		return a;
	}
	else{
		return NULL;
	}
}

void initializare()
{
	arbore = (Nod*) malloc(sizeof(Nod));
	arbore = 0;
	operatii++;
}

void afisareArbore(Nod *nod, int inaltime){
	int level;
	if(nod == NULL){
	return;
	}
	afisareArbore(nod->st,inaltime+1);
	for(level=0; level<inaltime; level++)
		printf("    ");
	printf("%d,%d\n",nod->key,nod->dim);
	afisareArbore(nod->dr,inaltime+1);

}

Nod* minValue(Nod *nod) {
  Nod* current = nod;

  while (current->st != 0) {
    current = current->st;
  }
  return current;
}

Nod * Succesor( Nod *n)
{
  if( n->dr != NULL )
    return minValue(n->dr);
 
  Nod *s = n->p;
  while(s != 0 && n == s->dr)
  {
     n = s;
     s = s->p;
  }
  return s;
}

Nod* OSSelect(Nod *nod, int ind){
	int curent;
	operatii++;
	if(nod != NULL){
		operatii++;
		if(nod->st!= NULL){
			operatii++;
			curent = nod->st->dim + 1;
		}
		else{
			curent = 1;
		}

		if(curent == ind){
			return nod;
		}
		else{
			if( ind < curent){
				return OSSelect(nod->st,ind);
			}		
			else{
				return OSSelect(nod->dr,ind-curent);
			}
		}
	}
	return NULL;
}

Nod *search(Nod *p, int key)
{
	if (p==NULL || key==p->key)
		return p;

	if (key < p->key)
		return search(p->st, key);
	else
		return search(p->dr, key);
}

int setare_dim(Nod *p)
{

if (p==NULL)
return 0;
else
{
if((p->st)!=NULL)
if(p->dr!=NULL)
p->dim=setare_dim(p->st)+setare_dim(p->dr)+1;
else
p->dim=setare_dim(p->st)+1;
else
if(p->dr!=NULL)
p->dim=setare_dim(p->dr)+1;
else
p->dim=1;

}

return p->dim;
}

Nod *OSDelete(Nod *nod, Nod *sters){
	Nod *y,*x;
	operatii+=2;
	if(sters->st==0 || sters->dr==0) y=sters;
	else y=Succesor(sters);
	operatii+=2;
	if(y->st!=0) x=y->st;
	else x=y->dr;
	operatii+=2;
	if(x!=0) x->p=y->p;
	if (y->p==0) arbore=x;
	else if (y==y->p->st) y->p->st=x;
		else y->p->dr=x;
	if(y!=sters) sters->key=y->key;
	setare_dim(nod);
	return y;

}



void Josephus(int n, int m)
{
	int i,j;
	Nod *nod;
	initializare();
	arbore=construireArboreEchilibrat(1, n, 0);
	setare_dim(arbore);
	j=1;

	for (i=n;i>0;i--)
	{
		j=((j+m-2)%i)+1;
		nod=OSSelect(arbore, j);
		//printf("\n %d", x->key);
		OSDelete(arbore,nod);
	}
}

int main(){
	/*Nod *x, *y;
	x=(Nod*)malloc(sizeof(Nod));
	y=(Nod*)malloc(sizeof(Nod));

	initializare();
	arbore = construireArboreEchilibrat(10,27,0);
	//setare_dim(arbore);
	afisareArbore(arbore, 1);
	y=arbore;

	for(int k=0;k<4;k++)
	{
	printf("\n\n");
	x=OSSelect(y, 3);

	printf("Nodul sters: cheie:%d\n", x->key);
	OSDelete(arbore, x);


	//y=cautare(T->rad, x->key);
	afisareArbore(arbore, 1);
	}
	//Josephus(27,3);

	*/
	
	FILE *f = fopen("Josephus.csv", "w+");
	int n;
	fprintf(f, "n, operatii \n");


	for(n=100;  n< 10000; n+=100){

		operatii=0;
		Josephus(n,n/2);
		fprintf(f, "%d, %d\n", n, operatii);
		printf("%d\n",n);
	}

	fclose(f);
	

	getch();
}