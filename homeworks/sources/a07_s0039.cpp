#include <stdio.h>
#include <stdlib.h>

//Declaram structura reprezentarii R2
typedef struct R2_Node
{
    int value;
    short radacina;
    int nr_children;
    int children[100];
}R2_Node;

//Declaram structura reprezentarii R3
typedef struct R3_Node
{
    int value;
    struct R3_Node *first_child;
    struct R3_Node *brother;
}R3_Node;

//Declaram variabile globale

int v[100];    //R1
R2_Node r2_noduri[100];  //R2
R3_Node *root;    //R3

/**
    Procedura de afisare a arborelui in forma R3
    @param root -> radacina arborelui
    @param level -> nr de nivele
*/
void printR3(R3_Node* root, int level)
{
    int i;
    for(i = 0; i <= level; i++)
    {
        printf("\t");
    }
    printf("%d\n", root->value);
    if(root->first_child != NULL)
    {
        printR3(root->first_child,level+1);
    }
    if(root->brother != NULL)
    {
        printR3(root->brother,level);
    }
}

/**
    Metoda de construire a Arborelui de forma R3. Toate nodurile se formeaza aici cu exceptia radacinii, deoarece
    radacina nu are frati ( brothers ).
    @param r2_struct -> o structura de tip R2 din vectorul de structuri r2_noduri[ ]
*/
R3_Node* build_R3_Recursive(R2_Node r2_struct)
{
    int i;
    R3_Node *primul,*current,*ultimul; //O gandim ca un fel de lista.
    primul=NULL;
    current=NULL;
    ultimul=NULL;
    for(i=1;i<=r2_struct.nr_children;i++) //Fratii intre ei sunt legati ca si intr-o lista
    {
        if (primul==NULL)
        {
            primul=(R3_Node*)malloc(sizeof(R3_Node*));
            primul->value=r2_struct.children[i];
            primul->brother=NULL;
            ultimul=primul;
        }
        else
        {
            current=(R3_Node*)malloc(sizeof(R3_Node*));
            current->value=r2_struct.children[i];
            ultimul->brother=current;
            current->brother=NULL;
            ultimul=current;
        }
        //Dupa ce am legat fratii intre ei
        ultimul->first_child=build_R3_Recursive(r2_noduri[r2_struct.children[i]]); //apelam metoda pt urmatorul copil
    }
    return primul;
}

/**
    Procedura de transformare a reprezentarii R2 in R3
    @param n -> numarul nodurilor, adica lungimea vectorului de structuri
*/
void R2toR3(int n)
{
    int i;
    int root_index;
    for(i=1;i<=n;i++) //Cautam in care structura se afla radacina
    {
        if(r2_noduri[i].radacina==1) //Am gasit radacina
        {
            root=(R3_Node*)malloc(sizeof(R3_Node));
            root->value=r2_noduri[i].value;
            r2_noduri[i].value=-123;
            root_index=i; //Retinem indexul pt a sti in care structura se afla radacina, v-om porni de aici
            break;
        }
    }
    root->brother=NULL;
    root->first_child=build_R3_Recursive(r2_noduri[root_index]); //Incepem construirea arborelui de la indexul radacinii
}

/**
    Procedura de transformare a reprezentarii R1 in R2
    @param n-> lungimea vectorului reprezentarii R1
*/
void R1toR2(int n)
{
    int i;
    for(i=1;i<=n;i++)
    {
        r2_noduri[i].value=i;
        if(v[i]==-1)
        {
            r2_noduri[i].radacina=1;
        }
        else
        {
            r2_noduri[v[i]].nr_children++;
            r2_noduri[v[i]].value=v[i];
            r2_noduri[v[i]].children[r2_noduri[v[i]].nr_children]=i;
        }
    }
}
/**
    Procedura de afisare a reprezentarii R2
    @param n->numarul de noduri
*/
void printR2(int n)
{
    int i,j;
    printf("\n==============================================================================================================\n");
    printf("Afisare R2: \n");
    for(i=1;i<=n;i++)
    {
        if(r2_noduri[i].radacina==1)
        {
            printf("%d<r> ->",r2_noduri[i].value);
        }
        else
        {
            printf("%d    -> ",r2_noduri[i].value);
        }
        for(j=1;j<=r2_noduri[i].nr_children;j++)
        {
            printf("%d ;",r2_noduri[i].children[j]);
        }
        printf("\n");
    }
    printf("---------------------------------------------------------------------------------------\n");
}

int main()
{
    int n,i;
    FILE *f;
    f=fopen("input.txt","r");
    fscanf(f,"%d\n",&n);
    for(i=1;i<=n;i++)
    {
        fscanf(f,"%d",&v[i]);
    }
    printf("Afisare R1:\n");

    for(i=1;i<=n;i++)
    {
        printf("%d->%d ; ",i,v[i]);
    }
    printf("\n----------------------------------------------------------------------------------------\n");
    R1toR2(n);
    printR2(n);
    R2toR3(n);
    printf("Afisare R3: \n \n");
    printR3(root,0);
    printf("\n-------------------------------------------------------------------------------------------\n");

    return 0;
}
