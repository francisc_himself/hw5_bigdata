/* ***ANONIM*** Peter

Am facut clase pentru obiectele respective. Nodurile in arborele
multicai se pun intrun vector cu alocare dinamica. Vectorul este
implementat cu tipuri generici si poate fii folosit la aproape orice
proiect.

Reprezentarea ca vectori de tati este cea mai simpla reprezentare pentru
un arbore oarecare. Se pot reprezenta usori orice tip de arbore. Singura
problema este ca operatiile sunt mai grele de implementat si au eficienta 
mai mica. 

Forma arborele multicai cu vectori este mai avantajos deoarece se poate
parcurge structura foarte simplu. Problema este pentru implementare
ar trebui folosit un vector dinamic.

Arborele multicai se pot reprezenta folosind un arbore binar. Important
aici este ca arborele binar este mai simplu de implementat. Parcurgerea
se face pe aceleasi timp. 

*/
#include <string.h>
#include <stdio.h>
#include <assert.h>

#define vector dvector2

template <class t>
class dvector2
{
	t* data;
	unsigned capacity;
	

	void realloc(unsigned nsize)
	{
		t* ndata = new t[nsize];
		if (data)
		{
			if (capacity >= nsize)
			{
				memcpy(ndata,data,nsize*sizeof(t));
			}
			else
			{
				memset(ndata,0,nsize*sizeof(t));
				memcpy(ndata,data,capacity*sizeof(t));
			}
			delete[] data;
		}
		else
		{
			memset(ndata,0,nsize*sizeof(t));
		}
		data = ndata;
		capacity = nsize;
	}

public:
	
	unsigned size;

	dvector2()
	{
		data = 0;
		capacity = 0;
		size = 0;
	}

	dvector2(unsigned size)
	{
		data = 0;
		capacity = 0;
		size = 0;
		realloc(size);
	}

	dvector2(t* vec, unsigned count)
	{
		data = 0;
		capacity = 0;
		size = 0;
		realloc(count);
		size = count;
		for (int i=0; i<count; i++)
		{
			data[i] = vec[i];
		}
	}

	void prealloc(unsigned size)
	{
		realloc(size);
	}

	t& operator [] (unsigned index)
	{
		if (index+1>size) 
			size = index+1;

		if (index >= capacity)
		{
			int ncapacity = index + (capacity>>4) + 1;
			this->realloc(ncapacity);
			capacity = ncapacity;
		}

		return data[index];
	}

	void print()
	{
		for (int i=0; i<size; i++)
		{
			printf("%d ",data[i]);
		}
		printf("\n");
	}

};

class multinode
{
public:

	int key;
	vector<multinode> child;

	multinode()
	{
		key = -1;
	}

	multinode(int x)
	{
		key = x;
	}

	void add(int e)
	{
		child[child.size]=e;
	}

	multinode* find(int e)
	{
		if (e == this->key)
		{
			return this;
		}

		for (int i=0; i<child.size; i++)
		{
			if (child[i].key == e)
			{
				return &(child[i]);
			}
		}

		multinode *q = 0;

		for (int i=0; i<child.size; i++)
		{
			q = child[i].find(e);
			if (q != 0) return q;
		}

		return 0;
	}

	void print(int depth = 0)
	{
		for (int i=0; i<depth; i++)
		{
			printf("|  ");
		}
		printf("+- %d\n",key);
		
		for (int i=0; i<child.size; i++)
		{
			child[i].print(depth+1);
		}
	}
};

class multitree
{	 
public:

	multinode *root;

	void build_helper(vector<int> &pv, vector<bool> &done, unsigned index)
	{
		if (!done[index])
		{
			multinode *q = root->find(pv[index]);
			if (!q)
			{
 				build_helper(pv,done,pv[index]);
				q = root->find(pv[index]);
			}
			
			assert(q);
			
			q->add(index);
			done[index]=true;
		}
	}
	
	multitree(vector<int> parent_vec)
	{
		root = 0;
		vector<bool> done;

		for (int i=0; i<parent_vec.size; i++)
		{
			if (parent_vec[i]<0)
			{
				root = new multinode(i);
				done[i] = true;
				break;
			}
		}
		
		for (int i=0; i<parent_vec.size; i++)
		{
			build_helper(parent_vec,done,i);
		}
	}

	void print()
	{
		if (root)
		{
			root->print();
		}
		else
		{
			printf("null");
		}
	}
};

class binnode
{
public:

	int key;
	binnode *child[2];


	binnode(binnode &x)
	{
		key = x.key;
		for (int i=0; i<2; i++)
			if (x.child[i])
				child[i] = new binnode(*x.child[i]);
			else 
				child[i] = 0;
	}

	binnode()
	{
		key = 0; child[0] = child[1] = 0;
	}

	binnode(int k)
	{
		key = k; child[0] = child[1] = 0;
	}

	~binnode()
	{
		for (int i=0; i<2; i++)
			delete child[i];
	}

	void link(binnode *t, int side)
	{
		assert(!child[side]);
		child[side] = t;
	}

	void linkLeft(binnode *t)
	{
		link(t,0);
	}

	void linkRight(binnode *t)
	{
		link(t,1);
	}

	void unlink(binnode *t)
	{
		for (int i=0; i<2; i++)
			if (child[i] == t)
				child[i] = 0;
	}

	void print(int depth = 0, char dir = '-')
	{
		if (child[0])
				child[0]->print(depth+2,'/');
		for (int i=0; i<depth; i++)
			printf(" ");
		printf("%c%d\n",dir,key);
		if (child[1])
				child[1]->print(depth+2,'\\');
			
	}

	void print2(int depth = 0)
	{
		for (int i=0; i<depth; i++)
			printf("|  ");
		printf("+- %d\n",key);
		for (int i=0; i<2; i++)
			if (child[i])
				child[i]->print2(depth+1-i);
	}

};

class bintree
{
	friend multitree;
	friend multinode;

	binnode *root;

	void build_help(multinode *a, binnode *y)
	{
		for (int i=0; i<a->child.size; i++)
		{
			binnode *z = new binnode(a->child[i].key);
			y->link(z,i<1?i:1);
			build_help(&a->child[i],z);
			y = z;
		}
	}

public:

	bintree()
	{
		root = 0;
	}

	bintree(bintree &t)
	{
		if (t.root)
			root = new binnode(*t.root);
		else
			root = 0;
	}

	bintree(multitree &t)
	{
		if (t.root)
		{
			root = new binnode(t.root->key);
			build_help(t.root,root);
		}
		else
			root = 0;

	}

	bintree(binnode &t)
	{
		root = new binnode(t);
	}

	void print()
	{
		if (root)
			root->print();
		else
			printf("null\n");
	}

	void printTree()
	{
		if (root)
			root->print2();
		else
			printf("null\n");
	}
};

void main()
{
	int pv[] = {3,3,3,-1,2,2,2,2,5,1,1};
	unsigned pvn = sizeof(pv)/sizeof(pv[0]);

	vector<int> t0(pv,pvn);

	printf("vector:\n");
	t0.print();
	printf("multitree:\n");
	multitree t1(t0);
	t1.print();
	printf("bintree:\n");
	bintree t2(t1);
	
	t2.print();
	printf("bintree:\n"); 
	t2.printTree();
}
