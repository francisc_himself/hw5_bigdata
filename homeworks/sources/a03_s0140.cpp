	#include <Conio.h> 
	#include <stdio.h>
	#include <stdlib.h>
	#include <time.h>
	#include <limits.h>
	int St(int i) 
	{ 
		return 2*i ;
	} 
	int Dr(int i)
	{ 
		return 2*i+1; 
	} 
	void Swap(int& a, int& b) 
		{ int c=a; a=b; b=c; } 
	void ReMakeH(int *x, int i, int dH); // Intretinerea propr. de Heap; 
	
	void BuildH (int *x, int n, int dH) // gen. H dintr-un vector neordonat. 
	{ 
	for (int i=n/2; i>=1; i--) 
	ReMakeH(x,i,dH); 
	} 
	int Max3(int* x, int dH, int i) // Max(X[i],X[l],X[r]) 
	{ // i 
	int m, l = St(i), r = Dr(i); // / \ 
	if ((l<=dH) && (x[l]>x[i])) m=l; // l r 
	else m=i; 
	if ((r<=dH) && (x[r]>x[m])) m=r; // X[m] = Max(X[i],X[l],X[r]) 
	return m; // m e { i, l, r } 
	} 
	void ReMakeH(int *x, int i, int dH) // "scufunda" in Heap pe A[i] 
	{ 
	int m=Max3(x,dH,i); 
	if (m!=i) { Swap(x[i],x[m]); 
	ReMakeH(x,m,dH); } 
	} 
	void Heap_Sort (int *x, int n) // Sortarea unui Heap: 
	{ 
	int dH=n; 
	BuildH(x,n,dH); 
	for (int i=n; i>=2; i--) { 
	Swap(x[1],x[i]); 
	 dH--; 
	ReMakeH(x,1,dH); } 
	} 
	void main() 
	{ 
	int n; 
	printf("Introduceti n\n");
	scanf("%d",&n);
	int* x = new int [n+1];
	for (int i=1; i<=n; i++ ) 
		{
			printf("x[%d]=",i);
			scanf("%d",&x[i]);
	}
		for (int i=1; i<=n; i++ ) 
		printf("%d , ",x[i]);
	Heap_Sort (x, n); 
	printf("\n");
	for (int i=1; i<=n; i++ ) 
		printf("%d , ",x[i]);
	delete []x; getch(); 
	} 