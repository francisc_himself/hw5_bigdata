//In the average case for these 2 methods of heap construction we can see from the graphs
//that they have approximate the same number of assignments, but in terms of number of
//comparissons TopDown is better than BottomUp.(BU does almost 2 times more comparissons)
//Per total due to the higher number of comparissons in the BottomUp method, we get that
//TopDown method has a number of assignments + comparissons less than BottomUp.
//It is obvious that both of them are linear.

#include<stdio.h> 
#include<conio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler;
const int VMAX = 10000;
const int VMIN = 0;

int* generateRandom(int n)
{
  int* a=new int[n];
  srand(time(NULL));
  for (int i=0;i<n;i++)
    a[i]=rand()%(VMAX-VMIN) +VMIN;
  return a;
}

int* generateWorst(int n)
{
	int* a = new int[n];
	for (int i = 0 ; i < n ; i ++)
		a[i] = i + 1;
	return a;
}


void reconstructHeapBU(int heap[],int i,int n)
{
    int left = 2*i;
    int right = 2*i +1;
    int max;
	profiler.countOperation("BottomUpComparissons",n);
    if ( (left <= n) && ( heap[left] > heap[i] ) )
        max = left;
	else
		max = i;
        profiler.countOperation("BottomUpComparissons",n);
    if ( (right <= n ) && (heap[right] > heap[max]) )
        max = right;
		//profiler.countOperation("BottomUpComparissons",n);
    if ( max != i )
    {
        int aux = heap[i];
        heap[i] = heap[max];
        heap[max] = aux;
        profiler.countOperation("BottomUpAssignments",n,3);
        reconstructHeapBU(heap,max,n);
    }
}

void constructHeapBU(int heap[],int n)
{
    for (int i = n/2 ; i>0 ; i--)
        reconstructHeapBU(heap,i,n);
}

void pushHeap(int heap[],int val,int *dim, int n)
{
	int i;
	(*dim)++;
	i=(*dim);
	profiler.countOperation("TopDownComparissons",n);
	while ((i>1) && ( heap[i/2]< val))
	{
		heap[i] = heap[i/2];
		i = i/2;
		profiler.countOperation("TopDownAssignments",n);
		profiler.countOperation("TopDownComparissons",n);
	}
	heap[i] = val;
	profiler.countOperation("TopDownAssignments",n);
}

void constructHeapTD(int a[],int heap[],int n,int *dim)
{
	for (int i=1;i<=n;i++)
	{
		pushHeap(heap,a[i],dim,n);
	}
}

void afisare(int T[], int i, int level)
{
    if(i<=22)
    {
    afisare(T, 2*i+1, level+1);
    for(int j=1;j<=level;j++)
    printf("          ");
    printf("%d\n",T[i]);
    afisare(T,2*i, level+1);
    }
}


void main()
{
	
	int heap[10000];
	int *a;
	int n;
	n=23;
	int d = 0;
	a = generateRandom(n);
	for (int i=1;i<=n;i++)
		heap[i]=a[i];
	printf("Initial Heap: \n\n");
	afisare(heap,1,0);
	printf("After Bottom Up Construction: \n\n");
	constructHeapBU(heap,n);
	afisare(heap,1,0);
	printf("After Top Down Construction: \n\n");
	constructHeapTD(a,heap,n,&d);
	afisare(heap,1,0);
	_getch();
	

	/*
	//Average Case
	int heap[10001];
	int *a;
	for (int j=0;j<5;j++)
		for (int n=100;n<10000;n = n+100)
		{
			int d=0;
			a=generateRandom(n);
			for (int i = 1 ; i <= n ; i++)
			heap[i] = a[i];
			constructHeapBU(heap,n);
			constructHeapTD(a,heap,n,&d);
		}
		
		profiler.addSeries("TopDownTotal","TopDownComparissons","TopDownAssignments");
		profiler.addSeries("BottomUpTotal","BottomUpComparissons","BottomUpAssignments");
		profiler.createGroup("Total","TopDownTotal","BottomUpTotal");
		profiler.createGroup("Comparissons","TopDownComparissons","BottomUpComparissons");
		profiler.createGroup("Assignments","TopDownAssignments","BottomUpAssignments");
		profiler.showReport(); */
}